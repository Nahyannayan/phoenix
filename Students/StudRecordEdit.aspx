<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="StudRecordEdit.aspx.vb" Inherits="StudRecordEdit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Src="../UserControls/FeeSponsor.ascx" TagName="FeeSponsor" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">



        function getSibling(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 845px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/ShowSiblingInfo.aspx?id=' + mode;

            if (mode == 'SI') {
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');

                document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];

            }

        }








        function Chk_Enable_M(obj) {

            var instals = eval(obj.checked);
            if (instals == true) {
                document.getElementById('<%=ddlM_BSU_ID.ClientID %>').disabled = false
            }
            else {
                document.getElementById('<%=ddlM_BSU_ID.ClientID %>').disabled = true
            }

        }

        function Chk_Enable_F(obj) {

            var instals = eval(obj.checked);
            if (instals == true) {
                document.getElementById('<%=ddlF_BSU_ID.ClientID %>').disabled = false
            }
            else {
                document.getElementById('<%=ddlF_BSU_ID.ClientID %>').disabled = true
            }

        }




        function UploadPhoto() {
            //        document.forms[0].Submit();
            var filepath = document.getElementById('<%=FUUploadEmpPhoto.ClientID %>').value;
            if ((filepath != '') && (document.getElementById('<%=h_EmpImagePath.ClientID %>').value != filepath)) {
                document.getElementById('<%=h_EmpImagePath.ClientID %>').value = filepath;
        document.forms[0].submit();
    }
}


//added by nahyan on 7thDec2014 to upload parent photo

function UploadFatherPhoto() {
    //        document.forms[0].Submit();
    var filepath = document.getElementById('<%=FUUploadFatherPhoto.ClientID %>').value;
    if ((filepath != '') && (document.getElementById('<%=h_FImagePath.ClientID %>').value != filepath)) {
        document.getElementById('<%=h_FImagePath.ClientID %>').value = filepath;
              document.forms[0].submit();
          }
      }


      function UploadMotherPhoto() {
          //        document.forms[0].Submit();
          var filepath = document.getElementById('<%=FUUploadMotherPhoto.ClientID %>').value;
        if ((filepath != '') && (document.getElementById('<%=h_MImagePath.ClientID %>').value != filepath)) {
            document.getElementById('<%=h_MImagePath.ClientID %>').value = filepath;
            document.forms[0].submit();
        }
    }


    function UploadGuardianPhoto() {
        //        document.forms[0].Submit();
        var filepath = document.getElementById('<%=FLUGuradian.ClientID %>').value;
        if ((filepath != '') && (document.getElementById('<%=h_GImagePath.ClientID %>').value != filepath)) {
            document.getElementById('<%=h_GImagePath.ClientID %>').value = filepath;
            document.forms[0].submit();
        }
    }

    //code added by dhanya 29 apr 08

    function copyFathertoMother(chkThis) {
        var chk_state = chkThis.checked;
        if (chk_state == true) {
            document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
            document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
            document.getElementById('<%=txtMCOMADDR1.ClientID %>').value = document.getElementById('<%=txtFCOMADDR1.ClientID %>').value;
            document.getElementById('<%=txtMCOMADDR2.ClientID %>').value = document.getElementById('<%=txtFCOMADDR2.ClientID %>').value;
            document.getElementById('<%=txtMCOMPOBOX.ClientID %>').value = document.getElementById('<%=txtFCOMPOBOX.ClientID %>').value;
            document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCOMEmirate.ClientID %>').selectedIndex;
            document.getElementById('<%=txtMCOMCity.ClientID %>').value = document.getElementById('<%=txtFCOMCity.ClientID %>').value;
            document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCOMCountry.ClientID %>').selectedIndex;
            document.getElementById('<%=txtMResPhone_Country.ClientID %>').value = document.getElementById('<%=txtFResPhone_Country.ClientID %>').value;
            document.getElementById('<%=txtMResPhone_Area.ClientID %>').value = document.getElementById('<%=txtFResPhone_Area.ClientID %>').value;
            document.getElementById('<%=txtMResPhone_No.ClientID %>').value = document.getElementById('<%=txtFResPhone_No.ClientID %>').value;
            document.getElementById('<%=txtMOffPhone_Country.ClientID %>').value = document.getElementById('<%=txtFOffPhone_Country.ClientID %>').value;
            document.getElementById('<%=txtMOffPhone_Area.ClientID %>').value = document.getElementById('<%=txtFOffPhone_Area.ClientID %>').value;
            document.getElementById('<%=txtMOffPhone_No.ClientID %>').value = document.getElementById('<%=txtFOffPhone_No.ClientID %>').value;
            document.getElementById('<%=txtMMobile_Country.ClientID %>').value = document.getElementById('<%=txtFMobile_Country.ClientID %>').value;
            document.getElementById('<%=txtMMobile_Area.ClientID %>').value = document.getElementById('<%=txtFMobile_Area.ClientID %>').value;
            document.getElementById('<%=txtMMobile_No.ClientID %>').value = document.getElementById('<%=txtFMobile_No.ClientID %>').value;
            document.getElementById('<%=txtMFax_Country.ClientID %>').value = document.getElementById('<%=txtFFax_Country.ClientID %>').value;
            document.getElementById('<%=txtMFax_Area.ClientID %>').value = document.getElementById('<%=txtFFax_Area.ClientID %>').value;
            document.getElementById('<%=txtMFax_No.ClientID %>').value = document.getElementById('<%=txtFFax_No.ClientID %>').value;
            document.getElementById('<%=txtMPRMAddr1.ClientID %>').value = document.getElementById('<%=txtFPRMAddr1.ClientID %>').value;
            document.getElementById('<%=txtMPRMAddr2.ClientID %>').value = document.getElementById('<%=txtFPRMAddr2.ClientID %>').value;
            document.getElementById('<%=txtMPRM_City.ClientID %>').value = document.getElementById('<%=txtFPRM_City.ClientID %>').value;
            document.getElementById('<%=ddlMPRM_Country.ClientID %>').selectedIndex = document.getElementById('<%=ddlFPRM_Country.ClientID %>').selectedIndex;
            document.getElementById('<%=txtMPPermant_Country.ClientID %>').value = document.getElementById('<%=txtFPPRM_Country.ClientID %>').value;
            document.getElementById('<%=txtMPPermant_Area.ClientID %>').value = document.getElementById('<%=txtFPPRM_Area.ClientID %>').value;
            document.getElementById('<%=txtMPPermant_No.ClientID %>').value = document.getElementById('<%=txtFPPRM_No.ClientID %>').value;
            document.getElementById('<%=txtMPRMPOBOX.ClientID %>').value = document.getElementById('<%=txtFPRMPOBOX.ClientID %>').value;
        }
        else {
            document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtMCOMADDR1.ClientID %>').value = "";
            document.getElementById('<%=txtMCOMADDR2.ClientID %>').value = "";
            document.getElementById('<%=txtMCOMPOBOX.ClientID %>').value = "";
            document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtMCOMCity.ClientID %>').value = "";
            document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtMResPhone_Country.ClientID %>').value = "";
            document.getElementById('<%=txtMResPhone_Area.ClientID %>').value = "";
            document.getElementById('<%=txtMResPhone_No.ClientID %>').value = "";
            document.getElementById('<%=txtMOffPhone_Country.ClientID %>').value = "";
            document.getElementById('<%=txtMOffPhone_Area.ClientID %>').value = "";
            document.getElementById('<%=txtMOffPhone_No.ClientID %>').value = "";
            document.getElementById('<%=txtMMobile_Country.ClientID %>').value = "";
            document.getElementById('<%=txtMMobile_Area.ClientID %>').value = "";
            document.getElementById('<%=txtMMobile_No.ClientID %>').value = "";
            document.getElementById('<%=txtMFax_Country.ClientID %>').value = "";
            document.getElementById('<%=txtMFax_Area.ClientID %>').value = "";
            document.getElementById('<%=txtMFax_No.ClientID %>').value = "";
            document.getElementById('<%=txtMPRMAddr1.ClientID %>').value = "";
            document.getElementById('<%=txtMPRMAddr2.ClientID %>').value = "";
            document.getElementById('<%=txtMPRM_City.ClientID %>').value = "";
            document.getElementById('<%=ddlMPRM_Country.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtMPPermant_Country.ClientID %>').value = "";
            document.getElementById('<%=txtMPPermant_Area.ClientID %>').value = "";
            document.getElementById('<%=txtMPPermant_No.ClientID %>').value = "";
            document.getElementById('<%=txtMPRMPOBOX.ClientID %>').value = "";
        }
        return true;
    }

    function copyFathertoGuardian(chkThis) {
        var chk_state = chkThis.checked;
        if (chk_state == true) {
            document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
            document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGCOMADDR1.ClientID %>').value = document.getElementById('<%=txtFCOMADDR1.ClientID %>').value;
            document.getElementById('<%=txtGCOMADDR2.ClientID %>').value = document.getElementById('<%=txtFCOMADDR2.ClientID %>').value;
            document.getElementById('<%=txtGCOMPOBOX.ClientID %>').value = document.getElementById('<%=txtFCOMPOBOX.ClientID %>').value;
            document.getElementById('<%=ddlGCOMEmirate.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCOMEmirate.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGCOMCity.ClientID %>').value = document.getElementById('<%=txtFCOMCity.ClientID %>').value;
            document.getElementById('<%=ddlGCOMCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCOMCountry.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGResPhone_Country.ClientID %>').value = document.getElementById('<%=txtFResPhone_Country.ClientID %>').value;
            document.getElementById('<%=txtGResPhone_Area.ClientID %>').value = document.getElementById('<%=txtFResPhone_Area.ClientID %>').value;
            document.getElementById('<%=txtGResPhone_No.ClientID %>').value = document.getElementById('<%=txtFResPhone_No.ClientID %>').value;
            document.getElementById('<%=txtGOffPhone_Country.ClientID %>').value = document.getElementById('<%=txtFOffPhone_Country.ClientID %>').value;
            document.getElementById('<%=txtGOffPhone_Area.ClientID %>').value = document.getElementById('<%=txtFOffPhone_Area.ClientID %>').value;
            document.getElementById('<%=txtGOffPhone_No.ClientID %>').value = document.getElementById('<%=txtFOffPhone_No.ClientID %>').value;
            document.getElementById('<%=txtGMobile_Country.ClientID %>').value = document.getElementById('<%=txtFMobile_Country.ClientID %>').value;
            document.getElementById('<%=txtGMobile_Area.ClientID %>').value = document.getElementById('<%=txtFMobile_Area.ClientID %>').value;
            document.getElementById('<%=txtGMobile_No.ClientID %>').value = document.getElementById('<%=txtFMobile_No.ClientID %>').value;
            document.getElementById('<%=txtGFax_Country.ClientID %>').value = document.getElementById('<%=txtFFax_Country.ClientID %>').value;
            document.getElementById('<%=txtGFax_Area.ClientID %>').value = document.getElementById('<%=txtFFax_Area.ClientID %>').value;
            document.getElementById('<%=txtGFax_No.ClientID %>').value = document.getElementById('<%=txtFFax_No.ClientID %>').value;
            document.getElementById('<%=txtGPRMAddr1.ClientID %>').value = document.getElementById('<%=txtFPRMAddr1.ClientID %>').value;
            document.getElementById('<%=txtGPRMAddr2.ClientID %>').value = document.getElementById('<%=txtFPRMAddr2.ClientID %>').value;
            document.getElementById('<%=txtGPRM_City.ClientID %>').value = document.getElementById('<%=txtFPRM_City.ClientID %>').value;
            document.getElementById('<%=ddlGPRM_Country.ClientID %>').selectedIndex = document.getElementById('<%=ddlFPRM_Country.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGPPERM_Country.ClientID %>').value = document.getElementById('<%=txtFPPRM_Country.ClientID %>').value;
            document.getElementById('<%=txtGPPERM_Area.ClientID %>').value = document.getElementById('<%=txtFPPRM_Area.ClientID %>').value;
            document.getElementById('<%=txtGPPERM_No.ClientID %>').value = document.getElementById('<%=txtFPPRM_No.ClientID %>').value;
            document.getElementById('<%=txtGPRMPOBOX.ClientID %>').value = document.getElementById('<%=txtFPRMPOBOX.ClientID %>').value;
        }
        else {
            document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGCOMADDR1.ClientID %>').value = "";
            document.getElementById('<%=txtGCOMADDR2.ClientID %>').value = "";
            document.getElementById('<%=txtGCOMPOBOX.ClientID %>').value = "";
            document.getElementById('<%=ddlGCOMEmirate.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGCOMCity.ClientID %>').value = "";
            document.getElementById('<%=ddlGCOMCountry.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGResPhone_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGResPhone_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGResPhone_No.ClientID %>').value = "";
            document.getElementById('<%=txtGOffPhone_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGOffPhone_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGOffPhone_No.ClientID %>').value = "";
            document.getElementById('<%=txtGMobile_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGMobile_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGMobile_No.ClientID %>').value = "";
            document.getElementById('<%=txtGFax_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGFax_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGFax_No.ClientID %>').value = "";
            document.getElementById('<%=txtGPRMAddr1.ClientID %>').value = "";
            document.getElementById('<%=txtGPRMAddr2.ClientID %>').value = "";
            document.getElementById('<%=txtGPRM_City.ClientID %>').value = "";
            document.getElementById('<%=ddlGPRM_Country.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGPPERM_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGPPERM_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGPPERM_No.ClientID %>').value = "";
            document.getElementById('<%=txtGPRMPOBOX.ClientID %>').value = "";
        }
        return true;
    }

    function copyMothertoGuardian(chkThis) {
        var chk_state = chkThis.checked;
        if (chk_state == true) {
            document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex;
            document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGCOMADDR1.ClientID %>').value = document.getElementById('<%=txtMCOMADDR1.ClientID %>').value;
            document.getElementById('<%=txtGCOMADDR2.ClientID %>').value = document.getElementById('<%=txtMCOMADDR2.ClientID %>').value;
            document.getElementById('<%=txtGCOMPOBOX.ClientID %>').value = document.getElementById('<%=txtMCOMPOBOX.ClientID %>').value;
            document.getElementById('<%=ddlGCOMEmirate.ClientID %>').selectedIndex = document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGCOMCity.ClientID %>').value = document.getElementById('<%=txtMCOMCity.ClientID %>').value;
            document.getElementById('<%=ddlGCOMCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGResPhone_Country.ClientID %>').value = document.getElementById('<%=txtMResPhone_Country.ClientID %>').value;
            document.getElementById('<%=txtGResPhone_Area.ClientID %>').value = document.getElementById('<%=txtMResPhone_Area.ClientID %>').value;
            document.getElementById('<%=txtGResPhone_No.ClientID %>').value = document.getElementById('<%=txtMResPhone_No.ClientID %>').value;
            document.getElementById('<%=txtGOffPhone_Country.ClientID %>').value = document.getElementById('<%=txtMOffPhone_Country.ClientID %>').value;
            document.getElementById('<%=txtGOffPhone_Area.ClientID %>').value = document.getElementById('<%=txtMOffPhone_Area.ClientID %>').value;
            document.getElementById('<%=txtGOffPhone_No.ClientID %>').value = document.getElementById('<%=txtMOffPhone_No.ClientID %>').value;
            document.getElementById('<%=txtGMobile_Country.ClientID %>').value = document.getElementById('<%=txtMMobile_Country.ClientID %>').value;
            document.getElementById('<%=txtGMobile_Area.ClientID %>').value = document.getElementById('<%=txtMMobile_Area.ClientID %>').value;
            document.getElementById('<%=txtGMobile_No.ClientID %>').value = document.getElementById('<%=txtMMobile_No.ClientID %>').value;
            document.getElementById('<%=txtGFax_Country.ClientID %>').value = document.getElementById('<%=txtMFax_Country.ClientID %>').value;
            document.getElementById('<%=txtGFax_Area.ClientID %>').value = document.getElementById('<%=txtMFax_Area.ClientID %>').value;
            document.getElementById('<%=txtGFax_No.ClientID %>').value = document.getElementById('<%=txtMFax_No.ClientID %>').value;
            document.getElementById('<%=txtGPRMAddr1.ClientID %>').value = document.getElementById('<%=txtMPRMAddr1.ClientID %>').value;
            document.getElementById('<%=txtGPRMAddr2.ClientID %>').value = document.getElementById('<%=txtMPRMAddr2.ClientID %>').value;
            document.getElementById('<%=txtGPRM_City.ClientID %>').value = document.getElementById('<%=txtMPRM_City.ClientID %>').value;
            document.getElementById('<%=ddlGPRM_Country.ClientID %>').selectedIndex = document.getElementById('<%=ddlMPRM_Country.ClientID %>').selectedIndex;
            document.getElementById('<%=txtGPPERM_Country.ClientID %>').value = document.getElementById('<%=txtMPPermant_Country.ClientID %>').value;
            document.getElementById('<%=txtGPPERM_Area.ClientID %>').value = document.getElementById('<%=txtMPPermant_Area.ClientID %>').value;
            document.getElementById('<%=txtGPPERM_No.ClientID %>').value = document.getElementById('<%=txtMPPermant_No.ClientID %>').value;
            document.getElementById('<%=txtGPRMPOBOX.ClientID %>').value = document.getElementById('<%=txtFPRMPOBOX.ClientID %>').value;
        }
        else {
            document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGCOMADDR1.ClientID %>').value = "";
            document.getElementById('<%=txtGCOMADDR2.ClientID %>').value = "";
            document.getElementById('<%=txtGCOMPOBOX.ClientID %>').value = "";
            document.getElementById('<%=ddlGCOMEmirate.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGCOMCity.ClientID %>').value = "";
            document.getElementById('<%=ddlGCOMCountry.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGResPhone_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGResPhone_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGResPhone_No.ClientID %>').value = "";
            document.getElementById('<%=txtGOffPhone_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGOffPhone_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGOffPhone_No.ClientID %>').value = "";
            document.getElementById('<%=txtGMobile_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGMobile_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGMobile_No.ClientID %>').value = "";
            document.getElementById('<%=txtGFax_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGFax_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGFax_No.ClientID %>').value = "";
            document.getElementById('<%=txtGPRMAddr1.ClientID %>').value = "";
            document.getElementById('<%=txtGPRMAddr2.ClientID %>').value = "";
            document.getElementById('<%=txtGPRM_City.ClientID %>').value = "";
            document.getElementById('<%=ddlGPRM_Country.ClientID %>').selectedIndex = 0;
            document.getElementById('<%=txtGPPERM_Country.ClientID %>').value = "";
            document.getElementById('<%=txtGPPERM_Area.ClientID %>').value = "";
            document.getElementById('<%=txtGPPERM_No.ClientID %>').value = "";
            document.getElementById('<%=txtGPRMPOBOX.ClientID %>').value = "";
        }
        return true;
    }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Student Record
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table align="left">
        <tr>
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                    EnableViewState="False" ValidationGroup="groupM1" />
                
            </td>
        </tr>
        <tr>
            <td align="center">
                Fields Marked with (<font class="text-danger">*</font>) are mandatory</td>
        </tr>
        <tr>
            
            <td width="100%">
                <table align="center" width="100%">
                   
                    <tr>
                        <td align="left">
                            <table align="center" width="100%" >
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Enrollment #</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtStud_No" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Enquiry #</span></td>
                                    
                                    <td  style="text-align: left">
                                        <asp:TextBox ID="txtEQM_ENQID" runat="server"></asp:TextBox></td>
                                    <td align="left">
                                       <span class="field-label"> Academic Year</td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtACD_ID" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label">Grade</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtGRD_ID" runat="server"></asp:TextBox>
                                        </td>
                                    <td align="left">
                                        <span class="field-label">Section</span></td>
                                    
                                    <td  style="text-align: left">
                                        <asp:TextBox ID="txtSCT_ID" runat="server"></asp:TextBox>
                                        </td>
                                    <td align="left">
                                        <span class="field-label">Shift</span></td>
                                    
                                    <td  style="text-align: left">
                                        <asp:TextBox ID="txtShift_ID" runat="server"></asp:TextBox>
                                        </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                       <span class="field-label">Stream</span></td>
                                    
                                    <td  align="left">
                                        <asp:TextBox ID="txtStream" runat="server" ></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Current Status</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtCurrStatus" runat="server"></asp:TextBox>
                                        </td>
                                    <td><asp:LinkButton id="lbtnFeeSpon" runat="server">Set Fee Sponsor</asp:LinkButton></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            
        </tr>
        <tr>
            
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            
            <td> 
            <div>
            <asp:Menu ID="mnuMaster" runat="server" OnMenuItemClick="mnuMaster_MenuItemClick"
                    Orientation="Horizontal" CssClass="menu_a">
                    <Items>
                         
                        
                        <asp:MenuItem  Selected="True" Text="Main" Value="0"></asp:MenuItem>
                        <asp:MenuItem  Text="Passport / Visa" Value="1"></asp:MenuItem>
                        <asp:MenuItem  Text="Parent Details" Value="2"></asp:MenuItem>
                        <asp:MenuItem  Text="Previous School" Value="3"></asp:MenuItem>
                        <asp:MenuItem  Text="Services" Value="4"></asp:MenuItem>
                        <asp:MenuItem  Text="Health" Value="5"></asp:MenuItem>
                        
                        
                        
                    </Items>
                    <StaticMenuItemStyle CssClass="menuItem" />
                    <StaticSelectedStyle CssClass="selectedItem" />
                    <StaticHoverStyle CssClass="hoverItem" />
                    <DynamicSelectedStyle />
                
                </asp:Menu></div>
                <asp:MultiView ID="mvMaster" runat="server" ActiveViewIndex="0">
                
                    <asp:View ID="vwMain" runat="server">
                    <asp:Panel id="plMain_Vw"  runat="server">
                        <table width="99%" class="border">
                                            <tbody><tr align="left">
                                                <td>
                        <table align="center" width="100%">
                            <tr>
                                <td align="left" colspan="4" class="title-bg">
                                    Main</td>
                            </tr>
                            <tr>
                                <td align="left"  width="20%">
                                    <span class="field-label">Fee ID</span></td>
                                
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtFee_ID" runat="server" ></asp:TextBox></td>
                                <td align="center" colspan="2"   rowspan="6">
                                    <asp:Image ID="imgEmpImage" runat="server" Height="215px" ImageUrl="~/Images/Photos/no_image.gif"
                                        Width="205px" /></td>
                            </tr>
                            <tr >
                                <td align="left"  width="20%">
                                    <span class="field-label">MOE Reg#</span></td>
                                
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtMOE_No" runat="server"></asp:TextBox></td>

                                 <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            <tr >
                                <td align="left">
                                    <span class="field-label">Join Grade</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGRD_ID_Join" runat="server"></asp:TextBox></td>

                                  <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            <tr >
                                <td align="left"  >
                                    <span class="field-label">Join Section</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtSCT_ID_JOIN" runat="server"></asp:TextBox></td>

                                  <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            <tr >
                                <td align="left">
                                    <span class="field-label">Join Shift</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtJoin_Shift" runat="server"></asp:TextBox></td>

                                  <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            <tr >
                                <td align="left">
                                    <span class="field-label">Join Stream</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtJoin_Stream" runat="server"></asp:TextBox></td>

                                  <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            <tr >
                                <td align="left">
                                    <span class="field-label">Date of Join<br />
                                    (Ministry)</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtMINDOJ" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgMindoj" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtMINDOJ"
                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Ministry  Date of join in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>&nbsp;<br />
                                    (dd/mmm/yyyy)</td>
                                <td align="left">
                                    <span class="field-label">Add Image</span></td>
                                
                                <td align="left"  >
                                    &nbsp;<asp:FileUpload ID="FUUploadEmpPhoto" runat="server" Height="19px" Width="212px" />

</td>
                            </tr>
                            <tr >
                                <td align="left">
                                    <span class="field-label">Date of Join</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtDOJ" runat="server"></asp:TextBox><br />
                                    (dd/mmm/yyyy)</td>
                                <td align="left">
                                    <span class="field-label">Join Academic Year</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtACD_ID_Join" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Ministry List</span><font class="text-danger">*</font></td>
                               
                                <td align="left">
                                    <asp:DropDownList ID="ddlMini_list" runat="server">
                                        <asp:ListItem Value="Regular">Regular</asp:ListItem>
                                        <asp:ListItem Value="Observer">Observer</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td align="left">
                                    <span class="field-label">Ministry List Type</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMini_list_Type" runat="server">
                                        <asp:ListItem>--</asp:ListItem>
                                        <asp:ListItem Value="Observer-Doc">Observer-Doc</asp:ListItem>
                                        <asp:ListItem Value="Observer-Age">Observer-Age</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Transfer Type</span><font class="text-danger">*</font></td>
                                
                                <td align="left"   >
                                    <asp:DropDownList ID="ddlTran_Type" runat="server">
                                    </asp:DropDownList><br />
                                    </td>
                                    
                                    
                                    <td align="left">
                                    <span class="field-label">Previous School Result</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList id="ddlJoinResult" runat="server">
                                     <asp:ListItem Value="NEW">NEW</asp:ListItem>     
                                     <asp:ListItem Value="PASS">PASS</asp:ListItem>                                                   
                                     <asp:ListItem Value="FAIL">FAIL</asp:ListItem>  
                                </asp:DropDownList>
                                    </td>
                                    
                                    
                                    
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                    <span class="field-label">Known Name</span></td>
                                
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtSTU_KNOWNNAME" runat="server">
                                    </asp:TextBox></td>
                                <td align="left" width="20%">
                                    <span class="field-label">Full Name in English<br />
                                    (First,Middle,Last)</span><span class="text-danger">*</span></td>
                                
                                <td align="left" width="30%">
                                    <table width="90%">

                                        <tr>

                                            <td>
                                                 <asp:TextBox ID="txtFname_E" runat="server" Width="30%"></asp:TextBox>
                                            </td>
                                              <td>
                                                        <asp:TextBox ID="txtMname_E" runat="server" Width="30%"></asp:TextBox>
                                                  </td>

                                             <td>
                                                  <asp:TextBox ID="txtLname_E" runat="server" Width="30%"></asp:TextBox>
                                                  </td>
                                        </tr>
                                    </table>
                                  
                              
                                   

                                </td>
                            </tr>
                            <tr>
                                
                                <td align="left" width="20%">
                                    <span class="field-label">Full Name in Passport</span></td>
                                
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtPassport_Name" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Full Name in Arabic<br />
                                    (First,Middle,Last)</td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFname_A" runat="server" Width="30%"></asp:TextBox>
                                    <asp:TextBox ID="txtMname_A" runat="server" visible="false" Width="30%"></asp:TextBox>
                                    <asp:TextBox ID="txtLname_A" runat="server" visible="false"  Width="30%"></asp:TextBox></td>
                            </tr>
                            
                           
                            
                            <tr>
                                <td align="left"  >
                                     <span class="field-label">Name in English (as in National Id Card)</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtSEmiratesId_ENG" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Name in Arabic (as in National Id Card)</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtSEmiratesId_ARB" runat="server"></asp:TextBox></td>
                            </tr>
                              
                            
                            <tr runat="server" id="trNatNo">
                                <td align="left"  >
                                    <span class="field-label">Nationality No</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtNatNo" runat="server" ></asp:TextBox>
                                    </td>
                                <td align="left">
                                    <span class="field-label">Refugee No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtRefugNo" runat="server"></asp:TextBox></td>
                            </tr>
                            
                            
                            <tr >
                                <td align="left"  >
                                    <span class="field-label">Gender</span><span class="text-danger">*</span></td>
                                
                                <td align="left">
                                    <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" Text="Male" />
                                    <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female" /></td>
                                <td align="left">
                                    <span class="field-label">Religion</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlReligion" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr >
                                <td align="left"  >
                                    <span class="field-label">Date of Birth</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtDob" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtDOB"
                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                    <br />
                                    (dd/mmm/yyyy)</td>
                                <td align="left"  >
                                    <span class="field-label">Place of Birth</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtPob" runat="server"></asp:TextBox></td>
                            </tr>
                              
                           
                            
                            
                            <tr >
                                <td align="left">
                                    <span class="field-label">Country of Birth</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlCountry_Birth" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left">
                                    <span class="field-label">Nationality</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlNationality_Birth" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr >
                                <td align="left"  >
                                    <span class="field-label">Student Email Address</span></td>
                               
                                <td align="left" >
                                    <asp:TextBox ID="txtStu_Email" runat="server" ></asp:TextBox>
                                   </td>

                                  <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            <tr >
                                       <td align="left" >
                                    <span class="field-label">Emergency Contact</span><br />
                                    (Country-Area-Number)</td>
                                <td align="left" > 
                                    <table width="90%">

                                        <tr>
                                      
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtEmerg_Country" runat="server" style="Width:32px;" MaxLength="3"></asp:TextBox></td>
                                     <td align="left" >
                                    <asp:TextBox ID="txtEmerg_Area" runat="server" style="Width:32px;" MaxLength="5"></asp:TextBox></td>
                                     <td align="left" >
                                    <asp:TextBox ID="txtEmerg_PhoneNo" runat="server" style="Width:89px;" MaxLength="10"></asp:TextBox></td>

                                        </tr>
                                    </table>

                                </td>
                                
                                <td align="left" width="20%">
                                    <span class="field-label">Fee Sponsor</span></td>
                               
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtFee_Spon" runat="server"></asp:TextBox></td>
                            </tr>
                            
                             <tr runat="server" id="tr_MinInfo">
                                <td align="left" class="title-bg-lite" colspan="4"> 
                                    Ministry Info</td>
                            </tr>
                             <tr runat="server" id="tr_DOB_POB_Arabic">                             
                                <td align="left"  >
                                    <span class="field-label">Date of Birth (Arabic)</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtDob_Arabic" runat="server"></asp:TextBox>
                                   </td>
                                <td align="left"  >
                                    <span class="field-label">Place of Birth(Arabic)</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtPob_Arabic" runat="server"></asp:TextBox></td>
                            </tr>
                            
                             <tr runat="server" id="tr_BC_DETAILS"> 
                                <td align="left"  >
                                   <span class="field-label"> Birth Certificate<br />
                                    (No.,Date of Issue,Place of Issue)</span></td>
                                
                                <td align="left"   colspan="3">
                                   <asp:TextBox ID="txtBC_NO" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtBC_DOI" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtBC_POI" runat="server"></asp:TextBox></td>
                            </tr>
                            
                            
                           
                             <tr runat="Server" Id="tr_ParentName_Arabic">
                                <td align="left"  >
                                    <span class="field-label">Parent Name(Arabic)</span></td>
                               
                                <td align="left"  colspan="3" >
                                    <asp:TextBox ID="txtPName_Arabic" runat="server"></asp:TextBox>
                                   </td>
                            </tr>
                            
                            
                             <tr runat="Server" Id="tr_ParentAddress_Arabic">
                                <td align="left"  >
                                    <span class="field-label">Parent Address(Arabic)</span></td>
                                
                                <td align="left"  colspan="3" >
                                    <asp:TextBox ID="txtPAddr_Arabic" runat="server"></asp:TextBox>
                                   </td>
                            </tr>
                            
                             <tr runat="Server" Id="tr_ParentOcc_Arabic">
                                <td align="left"  >
                                    <span class="field-label">Parent Occupation(Arabic)</span></td>
                                
                                <td align="left"  colspan="3" >
                                    <asp:TextBox ID="txtPOcc_Arabic" runat="server"></asp:TextBox>
                                   </td>
                            </tr>
                            <tr runat="Server" Id="tr_FamilyName">
                                <td align="left" >
                                    <span class="field-label">Family Name(Arabic)</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFamilyName" runat="server"></asp:TextBox>
                                   </td>
                                 <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            
                            <tr runat="Server" Id="trGFatherName">
                                <td align="left"  >
                                    <span class="field-label">Grandfather Name</span></td>
                               
                                <td align="left" >
                                    <asp:TextBox ID="txtGFather" runat="server"></asp:TextBox>
                                   </td>
                                <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                            </tr>
                            
                             <tr>
                                <td align="left" colspan="4" class="title-bg-lite"> 
                                    Language Info</td>
                            </tr>
                            
                            <tr >
                                <td align="left">
                                    <span class="field-label">First Language(Main)</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlFLang" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="left"  >
                                    <span class="field-label">Other Languages(Specify)</span></td>
                                
                                <td align="left">
                                    <div class="checkbox-list">
                                    <asp:Panel id="plOLang" runat="server">
                                        <asp:CheckBoxList id="chkOLang" runat="server">
                                        </asp:CheckBoxList>
                                    </asp:Panel>
                                        </div>
                                        </td>
                            </tr>
                            
                            <tr >
                                                                                            <td  rowspan="3" align="left" >
                                                                                                <span class="field-label">Proficiency in English</span></td>
                                                                                           
                                                                                            <td  colspan="3" align="left">
                                                                                                <span class="field-label">Reading :</span> 
                                                                                                <asp:RadioButton ID="rbRExc" runat="server" GroupName="Read" CssClass="field-label" Text="Excellent" />
                                                                                                <asp:RadioButton ID="rbRGood" runat="server" GroupName="Read" CssClass="field-label" Text="Good" />
                                                                                                <asp:RadioButton ID="rbRFair" runat="server" GroupName="Read" CssClass="field-label" Text="Fair" />
                                                                                                <asp:RadioButton ID="rbRPoor" runat="server" GroupName="Read" CssClass="field-label" Text="Poor" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td  colspan="4" align="left">
                                                                                                <span class="field-label">Writing :</span> <asp:RadioButton ID="rbWExc" runat="server" GroupName="Write" CssClass="field-label" Text="Excellent" />
                                                                                                <asp:RadioButton ID="rbWGood" runat="server" GroupName="Write" CssClass="field-label" Text="Good" />
                                                                                                <asp:RadioButton ID="rbWFair" runat="server" GroupName="Write" CssClass="field-label" Text="Fair" />
                                                                                                <asp:RadioButton ID="rbWPoor" runat="server" GroupName="Write" CssClass="field-label" Text="Poor" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td  colspan="4" align="left">
                                                                                                <span class="field-label">Speaking :</span>
                                                                                                <asp:RadioButton ID="rbSExc" runat="server" GroupName="Speak" CssClass="field-label" Text="Excellent" />
                                                                                                <asp:RadioButton ID="rbSGood" runat="server" GroupName="Speak" CssClass="field-label" Text="Good" />
                                                                                                <asp:RadioButton ID="rbSFair" runat="server" GroupName="Speak" CssClass="field-label" Text="Fair" />
                                                                                                <asp:RadioButton ID="rbSPoor" runat="server" GroupName="Speak" CssClass="field-label" Text="Poor" />
                                                                                            </td>
                                                                                        </tr>
                            <tr >
                                <td align="left" class="title-bg-lite" colspan="4"> 
                                    Previous School Info</td>
                            </tr>
                            
                            <tr id="trSch_name" runat="server">
                                <td align="left"  >
                                 <span class="field-label">School Name</span></td>
                                
                                <td align="left">
                                   <asp:TextBox ID="txtPSchool_Name1" runat="server"></asp:TextBox></td>
                                  <td align="left" width="20%" ></td>
                                <td align="left"  width="30%" ></td>
                               
                            </tr>
                            <tr id="trSch_Head" runat="server">
                                <td align="left"  >
                                   <span class="field-label">Name of the head teacher</span></td>
                               
                                <td align="left"  colspan="3">
                                     <asp:TextBox ID="txtPSchool_HEAD_NAME1" runat="server"></asp:TextBox></td>
                                
                            </tr><tr  id="trSch_ID" runat="server">
                              <td align="left"  >
                                    <span class="field-label">Student Id</span></td>
                               
                                <td align="left" >
                                   <asp:TextBox ID="txtPSchool_REG_ID1" runat="server">
                                    </asp:TextBox></td>
                                <td align="left">
                                   <span class="field-label">Year</span></td>
                               
                                <td align="left"  >
                                   <asp:DropDownList ID="ddlPSchool_Year1" runat="server" EnableTheming="True">
                                    </asp:DropDownList>  </td>
                            </tr>
                            <tr id="trSch_Lang" runat="server">
                                <td align="left">
                                  <span class="field-label">Language of instruction</span></td>
                                
                                <td align="left">
                                     <asp:TextBox ID="txtPSchool_Medium1" runat="server" Width="187px"></asp:TextBox></td>
                                <td align="left">
                                   <span class="field-label">Board Attended</span></td>
                                
                                <td align="left" >
                                     <asp:DropDownList ID="ddlPBoard_Attend1" runat="server" EnableTheming="True">
                                    </asp:DropDownList> </td>
                            </tr>
                            <tr id="trSch_Add" runat="server">
                                <td align="left">
                                    <span class="field-label">Address</span></td>
                                
                                <td align="left" colspan="3">
                                   <asp:TextBox ID="txtPSchool_ADDRESS1" runat="server" Width="222px"></asp:TextBox></td>
                              
                            </tr>
                            <tr id="trSch_City" runat="server">
                                <td align="left"  >
                                     <span class="field-label">City</span> </td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_City1" runat="server"></asp:TextBox></td>
                                <td align="left"><span class="field-label">Country</span> </td>
                                
                                <td align="left"  >
                                  <asp:DropDownList ID="ddlPSchool_Country1" runat="server" EnableTheming="True">
                                    </asp:DropDownList> </td>
                            </tr>
                                                      <tr id="trSch_Ph" runat="server">
                                                                                            <td  align="left"  >
                                                                                                <span class="field-label">School Phone</span></td>
                                                                                           
                                                                                            <td   align="left">
                                                                                                <asp:TextBox ID="txtPSchool_Ph1" runat="server" MaxLength="20" ></asp:TextBox>
                                                                                                </td>
                                                                                            <td   align="left">
                                                                                                <span class="field-label">School Fax</span></td>
                                                                                           
                                                                                            <td   align="left">
                                                                                                <asp:TextBox ID="txtPSchool_Fax1" runat="server" MaxLength="20" ></asp:TextBox>
                                                                                                </td>
                                                                                        </tr>
                            <tr id="trSch_FRMDT" runat="server">
                            <td align="left">
                                    <span class="field-label">From Date</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPFRM_DT1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgPFRM_DT1" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /></td>
                                <td align="left">
                                    <span class="field-label">To Date</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtPTO_DT1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgPTO_DT1" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /></td>
                            </tr>
                            <tr >
                                <td align="left" colspan="4" class="title-bg-lite"> 
                                    Other Info</td>
                            </tr>
                            <tr >
                                <td align="left"  >
                                    <span class="field-label">Receive SMS</span></td>
                                
                                <td align="left">
                                    <asp:RadioButton ID="rbRcvSms_Yes" CssClass="field-label" runat="server" GroupName="SMS" Text="Yes" />
                                    <asp:RadioButton ID="rbRcvSms_No" CssClass="field-label" runat="server" GroupName="SMS" Text="No" /></td>
                                <td align="left">
                                   <span class="field-label"> Receive Mail</span></td>
                               
                                <td align="left"  >
                                    <asp:RadioButton ID="rdRcvMail_Yes" CssClass="field-label" runat="server" GroupName="Mail" Text="Yes" />
                                    <asp:RadioButton ID="rdRcvMail_No" CssClass="field-label" runat="server" GroupName="Mail" Text="No" /></td>
                            </tr>
                            <tr >
                                <td align="left">
                                    <span class="field-label">Publication/promotion photos and videos</span></td>
                               
                                <td align="left"  colspan="3">
                                    <asp:RadioButton ID="rbPubYES" CssClass="field-label" runat="server" GroupName="Pub" Text="Yes" />
                                    <asp:RadioButton ID="rbPubNo" CssClass="field-label" runat="server" GroupName="Pub" Text="No" />
                                </td>
                            </tr>
                            <tr >
                                <td align="left"  >
                                    <span class="field-label">Comment</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox id="txtStud_Comment" runat="server" MaxLength="300" SkinID="MultiText"
                                        TextMode="MultiLine">
                                    </asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">House</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlHouse_BSU" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            
                            
                            
                        </table>
                                                </td>
                                                </tr>
                                                </tbody>
                            </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="vwPassport" runat="server">
                      <asp:Panel id="plPassport_Vw"  runat="server">
                          <table width="99%" class="border">
                                            <tbody><tr align="left">
                                                <td>
                        <table align="center" width="100%">
                            <tr style="border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px;" >
                                <td align="left"  colspan="6" class="title-bg">
                                    Passport/Visa</td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Passport No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtPNo" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_txtPNo" runat="server" ControlToValidate="txtPNo"
                                Display="Dynamic" ErrorMessage="Please enter the Passport No" InitialValue=" "
                                ValidationGroup="groupM1" EnableClientScript="false" Visible="false" >*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    <span class="field-label">Passport Issue Place</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtPIssPlace" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Passport Issue Date</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtPIssDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgPIssDate" runat="server"  ImageUrl="~/Images/calendar.gif" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                            runat="server" ControlToValidate="txtPIssDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="text-danger">*</asp:RegularExpressionValidator>
                                    <asp:CustomValidator ID="cvIss_date" runat="server" ControlToValidate="txtPIssDate" CssClass="text-danger"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                                <td align="left">
                                    <span class="field-label">Passport Expiry Date</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtPExpDate" runat="server" Width="122px"></asp:TextBox>
                                    <asp:ImageButton ID="imgPExpDate" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                            runat="server" ControlToValidate="txtPExpDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="text-danger">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="cvExp_date" runat="server" ControlToValidate="txtPExpDate" CssClass="text-danger"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Visa No</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtVNo" runat="server"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <span class="field-label">Visa Issue Place</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtVIssPlace" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Visa Issue Date</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtVIssDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgVIssDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6"
                                            runat="server" ControlToValidate="txtVIssDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="text-danger">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator2" runat="server" ControlToValidate="txtVIssDate" CssClass="text-danger"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                                <td align="left">
                                    <span class="field-label">Visa Expiry Date</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtVExpDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgVExpdate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8"
                                            runat="server" ControlToValidate="txtVExpDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1" CssClass="text-danger">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator4" runat="server" ControlToValidate="txtVExpDate" CssClass="text-danger"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Issuing Authority / STP</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtVIssAuth" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfv_txtVIssAuth" runat="server" ControlToValidate="txtVIssAuth"
                                Display="Dynamic" ErrorMessage="Please enter the Issuing Authority / STP" InitialValue=" "
                                ValidationGroup="groupM1" EnableClientScript="false" Visible="false" >*</asp:RequiredFieldValidator>
                                </td>
                              <td></td>
                              <td></td>
                            </tr>
                           <tr><td align="left" ><span class="field-label">National ID</span></td>
                                <td align="left"  >
  <asp:TextBox ID="txtEmiratesID" runat="server" CssClass="input"></asp:TextBox>
<asp:RequiredFieldValidator ID="rtf_txtEmiratesID" runat="server" ControlToValidate="txtEmiratesID"
                                Display="Dynamic" ErrorMessage="Please enter the National ID" InitialValue=" "
                                ValidationGroup="groupM1" EnableClientScript="false" Visible="false" >*</asp:RequiredFieldValidator>
                                </td>
  <td align="left" ><span class="field-label">National ID Expiry Date</span>
                                  </td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtEMIRATES_IDExp_date" runat="server" MaxLength="11"
                                TabIndex="32">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/calendar.gif"   />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbEMIRATES_IDExp_date" runat="server" TargetControlID="txtEMIRATES_IDExp_date"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="ceEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnEMIRATES_IDExp_date" TargetControlID="txtEMIRATES_IDExp_date">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="revEmir_Exp_date"
                                            runat="server" ControlToValidate="txtEMIRATES_IDExp_date" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>
                                
                            
                            <tr id="trPremisesId" runat="server">
                                <td align="left"  >
                                    <span class="field-label">Premises Id</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPremisesId" runat="server"></asp:TextBox></td>
                                    
                               <td></td>   
                                <td></td>
                                    
                            </tr>
                        </table>
                                                </td>
                                                </tr>
                                                </tbody>
                              </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="vwParent" runat="server">
                    <asp:Panel id="plparent_Vw"  runat="server">
                        <table width="99%" class="border">
                                            <tbody><tr align="left">
                                                <td>
                        <table align="center" width="100%">
                            <tr>
                                <td align="left"  colspan="6" class="title-bg">
                                    Parent</td>
                            </tr>
                            <tr id="abc" style="display: none">
                                <td align="left" class="text-danger" >
                                    <span class="field-label">Select Sibling#</span></td>
                                
                                <td align="left"  colspan="4" >
                                    <asp:TextBox ID="txtPar_Sib" runat="server" CssClass="text-danger"></asp:TextBox>
                                    <asp:ImageButton ID="imgbtnSibling" runat="server"  ImageUrl="~/Images/forum_search.gif"
                                       OnClientClick="getSibling('SI');return true;" /></td>
                            </tr>
                            <tr>
                                <td align="left" class="text-danger" >
                                   <span class="field-label"> Primary Contact</span></td>
                                
                                <td align="left"  colspan="4" >
                                    <asp:RadioButton ID="rdFather" runat="server" GroupName="ContactPrimary"
                                        Text="Father" CssClass="field-label text-danger" AutoPostBack="true" />
                                    <asp:RadioButton ID="rdMother" runat="server" GroupName="ContactPrimary" Text="Mother" CssClass="field-label text-danger" AutoPostBack="true" />
                                    <asp:RadioButton ID="rdGuardian" runat="server" GroupName="ContactPrimary" Text="Guardian" CssClass="field-label text-danger" AutoPostBack="true"  /></td>
                            </tr>
                            <tr>
                                <td align="left" class="text-danger" >
                                    <span class="field-label">Preferred Contact</span></td>
                                
                                <td align="left"  colspan="4" >
                                    &nbsp;
                                    <asp:DropDownList ID="ddlPrefContact" runat="server" CssClass="field-label text-danger">
                                        <asp:ListItem>Home Phone</asp:ListItem>
                                        <asp:ListItem>Office Phone</asp:ListItem>
                                        <asp:ListItem>Mobile</asp:ListItem>
                                        <asp:ListItem>Email</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="6" class="title-bg-lite"> 
                                    Parent Details(Father)</td>
                            </tr>
                            <tr>
                                <td align="left" width="20%">
                                  
                                    <span class="field-label">Full Name As Per Passport</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  colspan="2">
                                    <table width="90%">

                                        <tr>

                                            <td>
                                                 <asp:TextBox ID="txtFFirstName" runat="server" Width="100px" placeholder="First Name"></asp:TextBox>
                                            </td>

                                               <td>
                                                        <asp:TextBox ID="txtFMidName" runat="server" Width="100px"></asp:TextBox>
                                            </td>

                                               <td>
                                                                                       <asp:TextBox ID="txtFLastName" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                   
                               

                                    <asp:RequiredFieldValidator ID="rfvFFirstName" runat="server" ControlToValidate="txtFFirstName" Visible="false"
                                Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact father's first name required"
                               ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>
                            <div class="remark">
                                <span style="padding-left: 45px;">First Name</span> <span style="padding-left: 132px;">
                                    Middle Name</span> <span style="padding-left: 120px;">Last Name</span></div>
                            
                                    
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtFFirstName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtFMidName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtFLastName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                                    
                                    </td>
                                    <td colspan="3">
                                    <table width="100%">
                                    <tr>
                                    <td  colspan="2" >
                                    &nbsp;<asp:Image ID="imgFather" runat="server" Height="155px" ImageUrl="~/Images/Photos/no_image.gif"
                                        Width="125px" /></td>
                                    </tr>
                                    <tr>
                                    <td >
                                    <span class="field-label">Add Image</span>
                                    </td>
                                    <td >
                                       &nbsp;<asp:FileUpload ID="FUUploadFatherPhoto" runat="server" />
                                    
                                    </td>
                                    </tr>
                                    </table>
                                    
                                    </td>
                            </tr>
                            
                            <tr><td align="left" ><span class="field-label">National ID</span></td>
                                <td align="left" >
  <asp:TextBox ID="txtFEMIRATES_ID" runat="server" ></asp:TextBox></td>
  <td align="left" ><span class="field-label">National ID Expiry Date</span>
                                  </td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtFEMIRATES_ID_EXPDATE" runat="server" Width="110px" MaxLength="12"
                                >
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnFEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/calendar.gif"   />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbFEMIRATES_IDExp_date" runat="server" TargetControlID="txtFEMIRATES_ID_EXPDATE"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="ceFEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnFEMIRATES_IDExp_date" TargetControlID="txtFEMIRATES_ID_EXPDATE">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="revFEmir_Exp_date"
                                            runat="server" ControlToValidate="txtFEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter father's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>
                            
                            <tr>
                                <td align="left">
                                     <span class="field-label">Name in English (as in National Id Card)</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtFEmiratesId_ENG" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Name in Arabic (as in National Id Card)</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtFEmiratesId_ARB" runat="server"></asp:TextBox></td>
                            </tr>
                                                      
                            <tr>
                                <td align="left">
                                    <span class="field-label">Nationality 1</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlFNationality1" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left">
                                    <span class="field-label">Nationality 2</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlFNationality2" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                            <td align="left"  >
                                    <span class="field-label">Residential Address Country</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFCOMCountry" runat="server" AutoPostBack="true">
                                    </asp:DropDownList></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address City/State</span></td>
                                
                                <td align="left"  >
                                <asp:DropDownList ID="ddlFCity" runat="server" CssClass="dropDownList" AutoPostBack="true">
                                            </asp:DropDownList>
                                 <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                    <asp:TextBox ID="txtFCOMCity" runat="server"></asp:TextBox></td>
                                
                            </tr>
                            <tr>
                            <td align="left"  >
                                    <span class="field-label">Residential Address Area</span></td>
                                
                                <td align="left" >
                                <asp:DropDownList ID="ddlFArea" runat="server" CssClass="dropDownList">
                                            </asp:DropDownList><div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtFCOMADDR2" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Street</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFCOMADDR1" runat="server"></asp:TextBox></td>
                                
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Building</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFBldg" runat="server">
                                    </asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Apartment No.</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFAptNo" runat="server">
                                    </asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">P.O BOX</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFCOMPOBOX" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">City/Emirate</span><span class="text-danger">*</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFCOMEmirate" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            
                            <tr>
                                <td align="left">
                                    <span class="field-label">Home Phone</span></td>
                                
                                <td align="left">
                                
                                    <asp:TextBox ID="txtFResPhone_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFResPhone_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFResPhone_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                                <td align="left">
                                   <span class="field-label"> Office Phone</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtFOffPhone_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFOffPhone_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFOffPhone_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                   <span class="field-label"> Mobile</span></td>
                               
                                <td align="left"  >
                                                                    
                                    <asp:TextBox ID="txtFMobile_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFMobile_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFMobile_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    &nbsp;(Country-Area-Number)</td>
                                <td align="left"  >
                                    <span class="field-label">Fax No</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFFax_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFFax_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFFax_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Overseas Address Line 1</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtFPRMAddr1" runat="server"></asp:TextBox></td>
                                <td align="left">
                                    <span class="field-label">Overseas Address Line 2</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtFPRMAddr2" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left">
                                   <span class="field-label"> Overseas Address City/State</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtFPRM_City" runat="server"></asp:TextBox></td>
                                <td align="left">
                                    <span class="field-label">Overseas Address Country</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlFPRM_Country" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Permanent Phone</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFPPRM_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFPPRM_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFPPRM_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                                <td align="left"  >
                                    <span class="field-label">P.O BOX</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFPRMPOBOX" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">GEMS Staff</span></td>
                               
                                <td align="left">
                                    <asp:CheckBox ID="chkbFGEMSEMP" runat="server" Text="Yes" />&nbsp;
                                    <asp:DropDownList ID="ddlF_BSU_ID" runat="server">
                                    </asp:DropDownList></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Occupation</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFOcc" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Company Name</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList id="ddlFCompany_Name" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    (If you choose other,please specify the Company Name)<br />
                                    <asp:TextBox ID="txtFComp_Name" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Company Address</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFComp_Add" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Email</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFEmail" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="6" class="title-bg-light">
                            
                                    Parent Details(Mother)<span  style="float:right;">
                                   <asp:CheckBox ID="chkCopyF_Details" runat="server" style="clear: left" Text="Copy Father Details" onclick="javascript:return copyFathertoMother(this);" /></span></td>
                            </tr>
                              <tr>
                                <td align="left">
                                  
                                    <span class="field-label">Full Name As Per Passport</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  colspan="2">

                                    <table width="90%">
                                        <tr>

                                            <td>
                                                  <asp:TextBox ID="txtMFirstName" runat="server" Width="100px"></asp:TextBox>

                                            </td>
                                             <td>

                                                 <asp:TextBox ID="txtMMidName" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                             <td>
                                                  <asp:TextBox ID="txtMLastName" runat="server" Width="100px"></asp:TextBox>

                                            </td>
                                        </tr>

                                    </table>
                                  
                                    
                                   


                                    <asp:RequiredFieldValidator ID="rfvMFirstName" runat="server" ControlToValidate="txtMFirstName" Visible="false"
                                Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact mother's first name required"
                               ValidationGroup="groupM1" CssClass="text-danger">*</asp:RequiredFieldValidator>
                            <div class="remark" style="padding-top: 4px;">
                                <span style="padding-left: 45px;">First Name</span> <span style="padding-left: 132px;">
                                    Middle Name</span> <span style="padding-left: 120px;">Last Name</span></div>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtMFirstName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtMMidName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtMLastName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                     <td colspan="2">
                                     <table width="100%">
                                    <tr>
                                    <td  colspan="2" >
                                    <asp:Image ID="imgMother" runat="server" Height="155px" ImageUrl="~/Images/Photos/no_image.gif"
                                        Width="125px" /></td>
                                    </tr>
                                    <tr>
                                    <td >
                                    <span class="field-label">Add Image</span>
                                    </td>
                                    <td >
                                       <asp:FileUpload ID="FUUploadMotherPhoto" runat="server"  />
                                    
                                    </td>
                                    </tr>
                                    </table>
                                    
                                    </td>
                            </tr>
                             <tr><td align="left" ><span class="field-label">National ID</span></td>
                                 <td align="left" >
  <asp:TextBox ID="txtMEMIRATES_ID" runat="server" ></asp:TextBox></td>
  <td align="left" ><span class="field-label">National ID Expiry Date</span>
                                  </td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtMEMIRATES_ID_EXPDATE" runat="server"  MaxLength="12"
                                >
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnMEMIRATES_ID_EXPDATE" runat="server" ImageUrl="~/Images/calendar.gif"   />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FBXMEMIRATES_ID_EXPDATE" runat="server" 
                                                        TargetControlID="txtMEMIRATES_ID_EXPDATE"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="CXMEMIRATES_ID_EXPDATE" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnMEMIRATES_ID_EXPDATE" TargetControlID="txtMEMIRATES_ID_EXPDATE">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="RXVMEMIRATES_ID_EXPDATE"
                                            runat="server" ControlToValidate="txtMEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter mother's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>
                            
                             <tr>
                                <td align="left"  >
                                     <span class="field-label">Name in English (as in National Id Card)</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtMEmiratesId_ENG" runat="server"></asp:TextBox></td>
                                 <td align="left"  >
                                    <span class="field-label">Name in Arabic (as in National Id Card)</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtMEmiratesId_ARB" runat="server"></asp:TextBox></td>
                            </tr>
            
                            
                            <tr>
                                <td align="left">
                                    <span class="field-label">Nationality 1</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlMNationality1" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left">
                                    <span class="field-label">Nationality 2</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlMNationality2" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                             <td align="left">
                                    <span class="field-label">Residential Address Country</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlMCOMCountry" runat="server" AutoPostBack="true">
                                    </asp:DropDownList></td>
                                <td align="left">
                                    <span class="field-label">Residential Address City/State</span></td>
                                
                                <td align="left">
                                <asp:DropDownList ID="ddlMCity" runat="server" CssClass="dropDownList" AutoPostBack="true">
                                            </asp:DropDownList>
                                             <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                    <asp:TextBox ID="txtMCOMCity" runat="server"></asp:TextBox></td>
                                
                            </tr>
                            <tr>
                            <td align="left"  >
                                    <span class="field-label">Residential Address Area</span></td>
                                
                                <td align="left" >
                                <asp:DropDownList ID="ddlMArea" runat="server" CssClass="dropDownList">
                                            </asp:DropDownList>
                                               <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtMCOMADDR2" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Street</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMCOMADDR1" runat="server"></asp:TextBox></td>
                                
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Building</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMBldg" runat="server">
                                    </asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Apartment No.</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMAptNo" runat="server">
                                    </asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">P.O BOX</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMCOMPOBOX" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">City/Emirate</span><span class="text-danger">*</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlMCOMEmirate" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                           
                            <tr>
                                <td align="left">
                                    <span class="field-label">Home Phone</span></td>
                                
                                <td align="left">
                                
                                    <asp:TextBox ID="txtMResPhone_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMResPhone_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMResPhone_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                                <td align="left">
                                    <span class="field-label">Office Phone</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtMOffPhone_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMOffPhone_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMOffPhone_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                   <span class="field-label"> Mobile</span></td>
                               
                                <td align="left"  >
                                                                    
                                    <asp:TextBox ID="txtMMobile_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMMobile_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMMobile_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    &nbsp;(Country-Area-Number)</td>
                                <td align="left"  >
                                    <span class="field-label">Fax No</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMFax_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMFax_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMFax_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Overseas Address Line 1</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMPRMAddr1" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Overseas Address Line 2</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMPRMAddr2" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Overseas Address City/State</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtMPRM_City" runat="server"></asp:TextBox></td>
                                <td align="left">
                                    <span class="field-label">Overseas Address Country</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlMPRM_Country" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Permanent Phone</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMPPermant_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMPPermant_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMPPermant_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                                <td align="left"  >
                                    <span class="field-label">P.O BOX</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMPRMPOBOX" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">GEMS Staff</span></td>
                                
                                <td align="left">
                                    <asp:CheckBox ID="chkbMGEMSEMP" runat="server" Text="Yes" />&nbsp;
                                    <asp:DropDownList ID="ddlM_BSU_ID" runat="server">
                                    </asp:DropDownList></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Occupation</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMOcc" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Company Name</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList id="ddlMCompany_Name" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    (If you choose other,please specify the Company Name)<br />
                                    <asp:TextBox ID="txtMComp_Name" runat="server" Width="205px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Company Address</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtMComp_Add" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Email</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMEmail" runat="server"></asp:TextBox></td>
                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            <tr>
                                <td align="left" class="title-bg-light" colspan="6" >
                                   Parent Details(Guardian) <span style="float:right;">
                                   <asp:CheckBox ID="chkCopyF_Details_Guard" runat="server" Text="Copy Father Details" onclick="javascript:return copyFathertoGuardian(this);"/>
                                    <asp:CheckBox ID="chkCopyM_Details_Guard" runat="server" Text="Copy Mother Details" onclick="javascript:return copyMothertoGuardian(this);"/></span></td>
                            </tr>
                             <tr>
                                <td align="left">
                                  
                                    <span class="field-label">Full Name As Per Passport</span><span class="text-danger">*</span></td>
                               
                                <td align="left"  colspan="2">
                                    <table width="90%">

                                        <tr>
                                            <td>
                                                 <asp:TextBox ID="txtGFirstName" runat="server" Width="30%"></asp:TextBox>
                                            </td>
                                              <td>
                                                       <asp:TextBox ID="txtGMidName" runat="server" Width="30%"></asp:TextBox>
                                            </td>
                                              <td>
                                                    <asp:TextBox ID="txtGLastName" runat="server" Width="30%"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                   
                               
                                  

                                    <asp:RequiredFieldValidator ID="rfvGFirstName" runat="server" ControlToValidate="txtGFirstName" Visible="false"
                                Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact Guardian's first name required"
                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                            <div class="remark" style="padding-top: 4px;">
                                <span style="padding-left: 45px;">First Name</span> <span style="padding-left: 132px;">
                                    Middle Name</span> <span style="padding-left: 120px;">Last Name</span></div>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtGFirstName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtGMidName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtGLastName"  ValidChars="-\./' ">
                            </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                    <td colspan="2">
                                    <table width="100%">
                                    <tr>
                                    <td  colspan="2" >
                                    &nbsp;<asp:Image ID="imgGuardian" runat="server" Height="155px" ImageUrl="~/Images/Photos/no_image.gif"
                                        Width="125px" /></td>
                                    </tr>
                                    <tr>
                                    <td >
                                    <span class="field-label">Add Image</span>
                                    </td>
                                    <td >
                                       &nbsp;<asp:FileUpload ID="FLUGuradian" runat="server" />
                                    
                                    </td>
                                    </tr>
                                    </table>
                                    
                                    </td>
                            </tr>
                            <tr><td align="left" ><span class="field-label">National ID</span></td> 
                                <td align="left" >
  <asp:TextBox ID="txtGEMIRATES_ID" runat="server" ></asp:TextBox></td>
  <td align="left" ><span class="field-label">National ID Expiry Date</span>
                                  </td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtGEMIRATES_ID_EXPDATE" runat="server" MaxLength="12"
                                >
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnGEMIRATES_ID_EXPDATE" runat="server" ImageUrl="~/Images/calendar.gif"   />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FBXGEMIRATES_ID_EXPDATE" runat="server" 
                                                        TargetControlID="txtGEMIRATES_ID_EXPDATE"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="CXGEMIRATES_ID_EXPDATE" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnGEMIRATES_ID_EXPDATE" TargetControlID="txtGEMIRATES_ID_EXPDATE">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="RXVGEMIRATES_ID_EXPDATE"
                                            runat="server" ControlToValidate="txtGEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter guardian's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>
                            
                            <tr>
                                <td align="left">
                                    <span class="field-label">Nationality 1</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlGNationality1" runat="server">
                                    </asp:DropDownList></td>
                                <td align="left">
                                    <span class="field-label">Nationality 2</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlGNationality2" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                             <td align="left"  >
                                    <span class="field-label">Residential Address Country</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlGCOMCountry" runat="server" AutoPostBack="true">
                                    </asp:DropDownList></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address City/State</span></td>
                                
                                <td align="left"  >
                                <asp:DropDownList ID="ddlGCity" runat="server" CssClass="dropDownList" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                    <asp:TextBox ID="txtGCOMCity" runat="server"></asp:TextBox></td>
                               
                            </tr>
                            <tr>
                            <td align="left"  >
                                    <span class="field-label">Residential Address Area</span></td>
                                
                                <td align="left" ><asp:DropDownList ID="ddlGArea" runat="server" CssClass="dropDownList">
                                            </asp:DropDownList>
                                          <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtGCOMADDR2" runat="server"></asp:TextBox>
                                    </td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Street</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGCOMADDR1" runat="server"></asp:TextBox></td>
                                
                            </tr>
                            <tr>
                                <td align="left"  >
                                   <span class="field-label"> Residential Address Building</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGBldg" runat="server">
                                    </asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Residential Address Apartment No.</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGAptNo" runat="server">
                                    </asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">P.O BOX</span><span class="text-danger">*</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtGCOMPOBOX" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">City/Emirate</span><span class="text-danger">*</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlGCOMEmirate" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            
                            <tr>
                                <td align="left">
                                    <span class="field-label">Home Phone</span></td>
                                
                                <td align="left" >
                                
                                    <asp:TextBox ID="txtGResPhone_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGResPhone_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGResPhone_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                                <td align="left">
                                    <span class="field-label">Office Phone</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGOffPhone_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGOffPhone_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGOffPhone_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Mobile</span></td>
                                
                                <td align="left"  >
                                                                    
                                    <asp:TextBox ID="txtGMobile_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGMobile_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGMobile_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    &nbsp;(Country-Area-Number)</td>
                                <td align="left"  >
                                    <span class="field-label">Fax No</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGFax_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGFax_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGFax_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Overseas Address Line 1</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGPRMAddr1" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Overseas Address Line 2</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGPRMAddr2" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Overseas Address City/State</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGPRM_City" runat="server"></asp:TextBox></td>
                                <td align="left">
                                    <span class="field-label">Overseas Address Country</span></td>
                                
                                <td align="left">
                                    <asp:DropDownList ID="ddlGPRM_Country" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Permanent Phone</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGPPERM_Country" runat="server" Width="32px" MaxLength="3"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGPPERM_Area" runat="server" Width="32px" MaxLength="5"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGPPERM_No" runat="server" Width="89px" MaxLength="10"></asp:TextBox><br />
                                    (Country-Area-Number)</td>
                                <td align="left"  >
                                    <span class="field-label">P.O BOX</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGPRMPOBOX" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Occupation</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGOcc" runat="server"></asp:TextBox></td>
                                <td align="left"  >
                                    <span class="field-label">Company Name</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList id="ddlGCompany_Name" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    (If you choose other,please specify the Company Name)<br />
                                    <asp:TextBox ID="txtGComp_Name" runat="server" Width="205px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="field-label">Company Address</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGComp_Add" runat="server"></asp:TextBox></td>
                                <td align="left">
                                    <span class="field-label">Email</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtGEmail" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" class="title-bg-lite" colspan="6">
                                                                    Other Info
                          </td>
                            </tr>
                         <tr>
                                                                                             <td  colspan="6" align="left">
                                                                                                 <div>
                                                                                                     Are there any family circumstances of which you feel we should be aware of?</div>
                                                                                                <div> (eg Deceased parent /divorced/separated / adopted /others if so please give 
                                                                                                 detail)
                                                                                                 </div><br />
                                                                                                     <asp:TextBox ID="txtFamily_NOTE" runat="server"  Rows="2" 
                                                                                                         SkinID="MultiText" TabIndex="22" TextMode="MultiLine" 
                                                                                                     MaxLength="255"></asp:TextBox>
                                                                                                 
                                                                                             </td>
                                                                                         </tr>
                        </table>
                                                </td>
                                                </tr>
                                                </tbody>
                            </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="vwPrevious" runat="server" EnableTheming="True">
                         <asp:Panel id="plPrevious_Vw"  runat="server">
                             <table width="99%" class="border">
                                            <tbody><tr align="left">
                                                <td>
                        <table align="center" width="100%">
                            <tr>
                                <td align="left"  colspan="4" class="title-bg">
                                    Prev School</td>
                            </tr>
                            <tr >
                                <td align="left" colspan="4" class="title-bg-lite">
                                    Previous School Details 2</td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">School Name</span></td>
                                                                    
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_Name2" runat="server"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label">Name of the head teacher</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_HEAD_NAME2" runat="server"></asp:TextBox></td>
                                <td></td>
                                <td></td>                                
                            </tr>
                           <tr>
                                  <td align="left"  >
                                    <span class="field-label">Student Id</span></td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtPSchool_REG_ID2" runat="server">
                                    </asp:TextBox></td>
                                <td align="left" >
                                  <span class="field-label">Year</span> </td>
                               
                                <td align="left" >
                                 <asp:DropDownList ID="ddlPSchool_Year2" runat="server" EnableTheming="True">
                                    </asp:DropDownList></td>
                            </tr>
                           <tr>
                                <td align="left"  >
                                   <span class="field-label">Language of Instruction</span> </td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtPSchool_Medium2" runat="server">
                                    </asp:TextBox></td>
                                <td align="left" >
                                 <span class="field-label">Board Attended</span> </td>
                                
                                <td align="left" >
                                   <asp:DropDownList ID="ddlPBoard_Attend2" runat="server" EnableTheming="True">
                                    </asp:DropDownList> </td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                     <span class="field-label">Address</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_ADDRESS2" runat="server" Width="222px"></asp:TextBox> </td>
                                <td></td>
                                <td></td>
                            </tr>
                           <tr>
                                <td align="left"  >
                                   <span class="field-label">City</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPSchool_City2" runat="server"></asp:TextBox></td>
                                <td align="left" >
                                    <span class="field-label">Country</span></td>
                               
                                <td align="left" >
                                    <asp:DropDownList ID="ddlPSchool_Country2" runat="server" EnableTheming="True">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr  >
                                                                                            <td  align="left">
                                                                                                <span class="field-label">School Phone</span></td>
                                                                                            
                                                                                            <td  align="left">
                                                                                                <asp:TextBox ID="txtPSchool_Ph2" MaxLength="20" runat="server" ></asp:TextBox>
                                                                                                </td>
                                                                                            <td  align="left">
                                                                                                <span class="field-label">School Fax</span></td>
                                                                                            
                                                                                            <td  align="left">
                                                                                                <asp:TextBox ID="txtPSchool_fax2" runat="server" MaxLength="20"></asp:TextBox>
                                                                                              </td>
                                                                                        </tr>
                            <tr>
                            <td align="left">
                                   <span class="field-label"> From Date</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtPFRM_DT2" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="imgPFRM_DT2" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /></td>
                                <td align="left">
                                    <span class="field-label">To Date</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPTO_DT2" runat="server">122px</asp:TextBox>
                                    <asp:ImageButton ID="ImgPTO_DT2" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="getDate(7);return false;" /></td>
                            </tr>
                            
                            
                            
                            <tr >
                                <td align="left" colspan="6" class="title-bg-lite" >
                                    Previous School Details 3</td>
                            </tr>
                             <tr>
                                <td align="left"  >
                                    <span class="field-label">School Name</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_Name3" runat="server"></asp:TextBox></td>
                              <td></td>
                                 <td></td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                   <span class="field-label"> Name of the head teacher</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_HEAD_NAME3" runat="server"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                           <tr>
                                 <td align="left"  >
                                    <span class="field-label">Student Id</span></td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtPSchool_REG_ID3" runat="server">
                                    </asp:TextBox></td>
                                <td align="left" >
                                  <span class="field-label">Year</span> </td>
                               
                                <td align="left" >
                                 <asp:DropDownList ID="ddlPSchool_Year3" runat="server" EnableTheming="True">
                                    </asp:DropDownList></td>
                            </tr>
                           <tr>
                                <td align="left"  >
                                   <span class="field-label">Language of Instruction</span> </td>
                                
                                <td align="left" >
                                   <asp:TextBox ID="txtPSchool_Medium3" runat="server">
                                    </asp:TextBox></td>
                                <td align="left" >
                                 <span class="field-label">Board Attended</span> </td>
                                
                                <td align="left" >
                                   <asp:DropDownList ID="ddlPBoard_Attend3" runat="server" EnableTheming="True">
                                    </asp:DropDownList> </td>
                            </tr>
                            <tr>
                                <td align="left"  >
                                    <span class="field-label"> Address</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPSchool_ADDRESS3" runat="server"></asp:TextBox> </td>
                               <td></td>
                                <td></td>
                            </tr>
                           <tr>
                                <td align="left"  >
                                   <span class="field-label">City</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtPSchool_City3" runat="server"></asp:TextBox></td>
                                <td align="left" >
                                    <span class="field-label">Country</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlPSchool_Country3" runat="server" EnableTheming="True">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr  >
                                                                                            <td  align="left">
                                                                                                <span class="field-label">School Phone</span></td>
                                                                                            
                                                                                            <td  align="left">
                                                                                                <asp:TextBox ID="txtPSchool_Ph3" runat="server" MaxLength="20"></asp:TextBox>
                                                                                                </td>
                                                                                            <td  align="left">
                                                                                                <span class="field-label">School Fax</span></td>
                                                                                           
                                                                                            <td  align="left">
                                                                                                <asp:TextBox ID="txtPSchool_fax3" runat="server" MaxLength="20"></asp:TextBox>
                                                                                                </td>
                                                                                        </tr>
                            <tr>
                            <td align="left">
                                    <span class="field-label">From Date</span></td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPFRM_DT3" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgPFRM_DT3" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /></td>
                                <td align="left">
                                    <span class="field-label">To Date</span></td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtPTo_DT3" runat="server" style="width: 128px" Width="122px"></asp:TextBox>
                                    <asp:ImageButton ID="imgPTo_DT3" runat="server" 
                                        ImageUrl="~/Images/calendar.gif" /></td>
                            </tr>
                        </table>
                                                </td>
                                                </tr>
                                                </tbody>
                                 </table>
                        </asp:Panel>
                    </asp:View>
                     
                     <asp:View ID="vTransport" runat="server">
                     
                        <asp:Panel ID="Panel1" runat="server">
                        
                            <table width="99%" class="border">
                                            <tbody><tr align="left">
                                                <td>
                     
        <table align="center" width="100%"  >
                            
                                    <tr>
                                        <td align="left" class="title-bg" colspan="4" > Transport Details</td>
                                    </tr>
          
           <tr>
                <td style="width: 100px" >
                </td>
                <td align="center">
                </td>
                <td align="center">
                    <b>PickUp</b></td>
                <td align="center">
                    <b>DropOff</b></td>
             
            </tr>
          
          
          
            
            <tr>
                <td align="left">
                    <span class="field-label">Location</span></td>
                
                <td >
                    <asp:Literal ID="ltPickUp_Loc" runat="server"></asp:Literal></td>
                    
                     <td >
                    <asp:Literal ID="ltDropOff_Loc" runat="server"></asp:Literal></td>
            
            </tr>
            
            
            <tr>
                <td align="left">
                    <span class="field-label">SubLocation</span></td>
               
                <td >
                    <asp:Literal ID="ltPickUp_SubLoc" runat="server"></asp:Literal></td>
                    
                     <td >
                    <asp:Literal ID="ltDropOff_SubLoc" runat="server"></asp:Literal></td>
            
            </tr>
            
            <tr>
                <td align="left">
                    <span class="field-label">Bus No</span></td>
                
                <td >
                    <asp:Literal ID="ltPickUp_BusNo" runat="server"></asp:Literal></td>
                    
                     <td >
                    <asp:Literal ID="ltDropOff_BusNo" runat="server"></asp:Literal></td>
            
            </tr>
            
            
            <tr>
                <td align="left">
                    <span class="field-label">Trip</span></td>
                
                <td >
                    <asp:Literal ID="ltPickUp_Trip" runat="server"></asp:Literal></td>
                    
                     <td >
                    <asp:Literal ID="ltDropOff_Trip" runat="server"></asp:Literal></td>
            
            </tr>     
        </table>
                                                </td>
                                                </tr>
                                                </tbody>
                                </table>
               
                        
                
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server">
                            
                            <table align="center"  width="100%" cellpadding="5" cellspacing="0"
                                >
                                <tbody>
                                    <tr>
                                        <td align="left" class="title-bg" >
                                            Extra School Provision</td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="margin:0px;padding:0px;">
                                            <asp:GridView ID="gvServices" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-bordered table-row" 
                        EmptyDataText="No record found"
                        Width="100%">
                         <Columns>
                             <asp:TemplateField HeaderText="Sr.No">
    <ItemTemplate>
                                                            <asp:Label ID="lblsrno"   runat="server" Text='<%# Bind("srno") %>'></asp:Label>
                                                        
                             </ItemTemplate>
                             
          <HeaderStyle HorizontalAlign="Center" />
<ItemStyle HorizontalAlign="center"></ItemStyle>
                             
</asp:TemplateField>
                             <asp:TemplateField HeaderText="Description">
    <ItemTemplate>
                                                            <asp:Label ID="lblService"   runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                                        
                             </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />                   

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                             
</asp:TemplateField>
                             <asp:TemplateField HeaderText="From Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFromDT" runat="server" Text='<%# Bind("FROMDATE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                                  <HeaderStyle HorizontalAlign="Center" />
                             <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>
                             <asp:TemplateField HeaderText="Discontinue Date">
                                                       <ItemTemplate>
                                                            <asp:Label ID="lblDiscontinueDate" runat="server" Text='<%# Bind("DiscontinueDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:TemplateField>
                          
                             <asp:TemplateField HeaderText="Availing">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAVAILING" runat="server" Text='<%# Bind("AVAILING") %>'></asp:Label>
                                                        </ItemTemplate>
                                                                  <HeaderStyle HorizontalAlign="Center" />
                                                     <ItemStyle HorizontalAlign="Center"></ItemStyle>     
                                                    </asp:TemplateField>
                         </Columns>
                         <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                         <RowStyle CssClass="griditem" Wrap="False" />
                         <SelectedRowStyle CssClass="Green" Wrap="False" />
                         <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                         <EmptyDataRowStyle Wrap="False" />
                         <EditRowStyle Wrap="False" />
                    </asp:GridView>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="vwHealth" runat="server">
                    <asp:Panel id="plHealth_vw" runat="server">
                        <table width="99%" class="border">
                                            <tbody><tr align="left">
                                                <td>                      
                         <table align="center"  cellpadding="5" cellspacing="0" width="100%">
                                                         <tbody>
                                                         <tr>
                                <td align="left"  colspan="4" class="title-bg">
                                    Health</td>
                            </tr>
                            
                                                                              
                                                                                        
                                                                                        
                                                                             <tr>
                                                                                            <td align="left" width="25%">
                                                                                               <span class="field-label">Health Card No / Medical Insurance No</span></td>
                                                                                            
                                                                                            <td align="left" width="25%">
                                                                                                <asp:TextBox ID="txtHCard_No" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            </td>
                                                                                             <td align="left" width="25%">
                                                                                                <span class="field-label">Blood Group</span></td>
                                                                                            
                                                                                            <td align="left" width="25%">
                                                                                               <asp:DropDownList ID="ddlBgroup" runat="server">
                                            <asp:ListItem>--</asp:ListItem>
                                            <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                            <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                            <asp:ListItem Value="B+">B+</asp:ListItem>
                                            <asp:ListItem Value="A+">A+</asp:ListItem>
                                            <asp:ListItem Value="B-">B-</asp:ListItem>
                                            <asp:ListItem Value="A-">A-</asp:ListItem>
                                            <asp:ListItem Value="O+">O+</asp:ListItem>
                                            <asp:ListItem Value="O-">O-</asp:ListItem>
                                        </asp:DropDownList>  
                                                                                            </td>
                                                                                           
                                                                                        </tr>
                                                                            <tr>
                                                                                     <td align="left" colspan="6" class="title-bg-lite">
                                                                                          Health Restriction</td>
                                                                            </tr>
                                 <tr>
                                                                                            <td align="left">
                                        <span class="field-label">Allergies</span></td>
                                   
                                    <td align="left">
                                        <asp:RadioButton ID="rbHthAll_Yes" CssClass="field-label" runat="server" GroupName="Allerg" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthAll_No" CssClass="field-label" runat="server" GroupName="Allerg" Text="No" /></td>
                                    <td align="left">
                                        <span class="field-label">Notes</span></td>
                                   
                                    <td align="left">
                                        <asp:TextBox ID="txtHthAll_Note" runat="server" TabIndex="22" TextMode="MultiLine"
                                           Rows="4" CssClass="inputbox_multi" MaxLength="255"></asp:TextBox></td>
                                </tr> 
                                     <tr>
                                                                                        <td align="left">
                                                                                            <span class="field-label">Disabled ?</span>
                                                                                        </td>
                                                                                        
                                                                                        <td align="left">
                                                                                            <asp:RadioButton ID="rbHthDisabled_YES" CssClass="field-label" runat="server" GroupName="HthDisabled" 
                                                                                                Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthDisabled_No" CssClass="field-label" runat="server" GroupName="HthDisabled" 
                                                                                                Text="No" />
                                                                                        </td>
                                                                                        <td align="left">
                                                                                            <span class="field-label">Notes</span>
                                                                                        </td>
                                                                                        
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtHthDisabled_Note" runat="server" CssClass="inputbox_multi" 
                                                                                                 MaxLength="255" Rows="4" SkinID="MultiText" TabIndex="22" 
                                                                                                TextMode="MultiLine"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>           
                                 <tr> 
                                    <td align="left">
                                       <span class="field-label">Special Medication</span></td>
                                    
                                    <td align="left" ">
                                        <asp:RadioButton ID="rbHthSM_Yes" CssClass="field-label" runat="server" GroupName="SPMed" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthSM_No" CssClass="field-label" runat="server" GroupName="SPMed" Text="No" /></td>
                                    <td align="left">
                                       <span class="field-label"> Notes</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtHthSM_Note" runat="server" TabIndex="22" TextMode="MultiLine"
                                            Rows="4" CssClass="inputbox_multi" SkinID="MultiText" 
                                            MaxLength="255"></asp:TextBox></td>
                                </tr>
                                 <tr> 
                                    <td align="left" >
                                       <span class="field-label">Physical Education Restrictions</span></td>
                                    
                                    <td align="left"  >
                                        <asp:RadioButton ID="rbHthPER_Yes" CssClass="field-label" runat="server" GroupName="PhyEd" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthPER_No" CssClass="field-label" runat="server" GroupName="PhyEd" Text="No" /></td>
                                    <td align="left"  >
                                        <span class="field-label">Notes</span></td>
                                    
                                    <td align="left" >
                                        <asp:TextBox ID="txtHthPER_Note" runat="server" TabIndex="22" TextMode="MultiLine"
                                            Rows="4" MaxLength="255"></asp:TextBox></td>
                                </tr>
                                 <tr> 
                                    <td align="left" >
                                         <span class="field-label">Any other information related to health issue the school should be aware of?</span></td>
                                   
                                    <td align="left"  >
                                        <asp:RadioButton ID="rbHthOther_yes" CssClass="field-label" runat="server" GroupName="HthOther" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthOther_No" CssClass="field-label" runat="server" GroupName="HthOther" Text="No" /></td>
                                    <td align="left"  >
                                        <span class="field-label">Notes</span></td>
                                   
                                    <td align="left" >
                                    
                                    <asp:TextBox ID="txtHthOth_Note" runat="server" 
                                            Rows="2" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" 
                                            MaxLength="255"></asp:TextBox>
                                       </td>
                                </tr>
                                 <tr> 
                                    <td align="left" colspan="6" class="title-bg-lite">
                                        Applicant's disciplinary, social, physical or psychological detail</td>
                                    
                                    
                                </tr>                                                
                                 <tr> 
                                    <td align="left">
                                        <span class="field-label">Has the child received any sort of learning support or therapy?</span></td>
                                     
                                     <td align="left">
                                         <asp:RadioButton ID="rbHthLS_Yes" CssClass="field-label" runat="server" GroupName="LearnSp" 
                                             Text="Yes" />
                                         <asp:RadioButton ID="rbHthLS_No" CssClass="field-label" runat="server" GroupName="LearnSp" Text="No" />
                                     </td>
                                     <td align="left">
                                         <span class="field-label">Notes</span></td>
                                     
                                     <td align="left">
                                         <asp:TextBox ID="txtHthLS_Note" runat="server" CssClass="inputbox_multi" 
                                             Rows="4" TabIndex="22" TextMode="MultiLine" 
                                             MaxLength="255"></asp:TextBox>
                                     </td>
                                </tr>
                                 <tr>
                                                                            
                                                                                <td align="left" >
                                                                                 <span class="field-label">  
                                                                                    Does the child require any special education needs?</span></td>
                                                                               
                                                                                <td align="left">
                                                                                    <asp:RadioButton ID="rbHthSE_Yes" CssClass="field-label" runat="server" GroupName="SPneed" 
                                                                                        Text="Yes" />
                                                                                    <asp:RadioButton ID="rbHthSE_No" CssClass="field-label" runat="server" GroupName="SPneed" 
                                                                                        Text="No" />
                                                                                </td>
                                                                                <td align="left">
                                                                                   <span class="field-label"> Notes</span></td>
                                                                                
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtHthSE_Note" runat="server" 
                                                                                        Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" 
                                                                                       MaxLength="255"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                 <tr> 
                                    <td align="left">
                                        <span class="field-label">Does the student require English support as Additional Language program (EAL) ?</span></td>
                                    
                                    <td align="left">
                                        <asp:RadioButton ID="rbHthEAL_Yes" CssClass="field-label" runat="server" GroupName="EAL" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthEAL_No" CssClass="field-label" runat="server" GroupName="EAL" 
                                            Text="No" /></td>
                                    <td align="left">
                                        <span class="field-label">Notes</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtHthEAL_Note" runat="server" TabIndex="22" TextMode="MultiLine"
                                            Rows="4" MaxLength="255"></asp:TextBox></td>
                                </tr>
                                 <tr> 
                                    <td align="left">
                                        <span class="field-label">Has your child's behaviour been any cause for concern in previous schools </span></td>
                                    
                                    <td align="left">
                                        <asp:RadioButton ID="rbHthBehv_Yes" CssClass="field-label" runat="server" GroupName="Behv" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthBehv_No" CssClass="field-label" runat="server" GroupName="Behv" 
                                            Text="No" /></td>
                                    <td align="left">
                                        <span class="field-label">Notes</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtHthBehv_Note" runat="server" TabIndex="22" TextMode="MultiLine"
                                            Rows="4" CssClass="inputbox_multi"  
                                            MaxLength="255"></asp:TextBox></td>
                                </tr>
                                  
                                  <tr> 
                                    <td align="left">
                                        <span class="field-label">Any information related to communication & interaction that the school should be aware of ?</span></td>
                                    
                                    <td align="left">
                                        <asp:RadioButton ID="rbHthComm_Yes" CssClass="field-label" runat="server" GroupName="CommInt" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthComm_No" CssClass="field-label" runat="server" GroupName="CommInt" 
                                            Text="No" /></td>
                                    <td align="left">
                                        <span class="field-label">Notes</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtHthCommInt_Note" runat="server"  TabIndex="22" TextMode="MultiLine"
                                            Width="229px" Rows="4" MaxLength="255"></asp:TextBox></td>
                                </tr>
                                
                                    <tr>
                                                                                     <td align="left" colspan="6" class="title-bg-lite">
                                                                                          Gifted and talented</td>
                                                                            </tr>                                         
                                                                             <tr> 
                                    <td align="left">
                                       <span class="field-label"> Has your child ever been selected for specific enrichment activities?</span></td>
                                                                                            
                                                                                            <td align="left">
                                                                                                <asp:RadioButton ID="rbHthEnr_Yes" CssClass="field-label" runat="server" GroupName="Enr" 
                                                                                                    Text="Yes" />
                                                                                                <asp:RadioButton ID="rbHthEnr_No" CssClass="field-label" runat="server" GroupName="Enr" Text="No" />
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="field-label">Notes</span></td>
                                                                                            
                                                                                            <td align="left">
                                                                                                <asp:TextBox ID="txtHthEnr_note" runat="server" Rows="4" TabIndex="22" TextMode="MultiLine" 
                                                                                                     MaxLength="255"></asp:TextBox>
                                                                                            </td>
                                </tr>
                                 <tr> 
                                    <td align="left">
                                       <span class="field-label"> Is your child musically proficient?</span></td>
                                                                                           
                                                                                            <td align="left">
                                                                                                <asp:RadioButton ID="rbHthMus_Yes" CssClass="field-label" runat="server" GroupName="Music" 
                                                                                                    Text="Yes" />
                                                                                                <asp:RadioButton ID="rbHthMus_No" CssClass="field-label" runat="server" GroupName="Music" Text="No" />
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="field-label">Notes</span></td>
                                                                                            
                                                                                            <td align="left">
                                                                                                <asp:TextBox ID="txtHthMus_Note" runat="server"
                                                                                                    Rows="4" TabIndex="22" TextMode="MultiLine" 
                                                                                                    MaxLength="255"></asp:TextBox>
                                                                                            </td>
                                </tr>
                                 <tr> 
                                    <td align="left">
                                      <span class="field-label">Has your child represented a school or country in sport?</span></td>
                                                                                            
                                                                                            <td align="left">
                                                                                                <asp:RadioButton ID="rbHthSport_Yes" CssClass="field-label" runat="server" GroupName="Sport" 
                                                                                                    Text="Yes" />
                                                                                                <asp:RadioButton ID="rbHthSport_No" CssClass="field-label" runat="server" GroupName="Sport" Text="No" />
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="field-label">Notes</span></td>
                                                                                            
                                                                                            <td align="left">
                                                                                                <asp:TextBox ID="txtHthSport_note" runat="server" 
                                                                                                    Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" 
                                                                                                    MaxLength="255"></asp:TextBox>
                                                                                            </td>
                                </tr>
                           
                                                                        
                                                                             </tbody>
                                                                         </table>
                                                </td>
                                                </tr>
                                                </tbody>
                            </table>
                   </asp:Panel>
                    </asp:View>
                    <asp:View ID="vwOther" runat="server">
                        <asp:Panel ID="plOther" runat="server">
                        
                        </asp:Panel>
                    </asp:View>
                    
                 
                
                </asp:MultiView>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 76px">
            </td>
        </tr>
        <tr>
            
            <td align="center" colspan="4">
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" /></td>
           
        </tr>
        <tr>
            <td>
            </td>
            <td align="center"  style="width: 787px">
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="mpxFeeSponsor" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true"  PopupControlID="plFeeSP"
                    TargetControlID="lbtnFeeSpon">
                </ajaxToolkit:ModalPopupExtender>
                
                <ajaxToolkit:CalendarExtender ID="cldMindoj" TargetControlID="txtMINDOJ" runat="server" PopupButtonID="imgMindoj" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDOB" runat="server" PopupButtonID="imgDOB" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="txtPFRM_DT1" runat="server" PopupButtonID="imgPFRM_DT1" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender3" TargetControlID="txtPTO_DT1" runat="server" PopupButtonID="imgPTO_DT1" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtPIssDate" runat="server" PopupButtonID="imgPIssDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender5" TargetControlID="txtPExpDate" runat="server" PopupButtonID="imgPExpDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender6" TargetControlID="txtVIssDate" runat="server" PopupButtonID="imgVIssDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender7" TargetControlID="txtVExpDate" runat="server" PopupButtonID="imgVExpDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender8" TargetControlID="txtPFRM_DT2" runat="server" PopupButtonID="imgPFRM_DT2" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender9" TargetControlID="txtPTO_DT2" runat="server" PopupButtonID="ImgPTO_DT2" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender10" TargetControlID="txtPFRM_DT3" runat="server" PopupButtonID="imgPFRM_DT3" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender11" TargetControlID="txtPTO_DT3" runat="server" PopupButtonID="imgPTO_DT3" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_EmpImagePath" runat="server" />
                <asp:HiddenField ID="h_FImagePath" runat="server" />
                <asp:HiddenField ID="h_MImagePath" runat="server" />
                <asp:HiddenField ID="h_SliblingID" runat="server" />
                 <asp:HiddenField ID="h_GImagePath" runat="server" />
                
                <asp:HiddenField ID="h_sliblingID_Copy" runat="server" />
                <asp:Panel id="plFeeSP" runat="server" Width="541px" style="display:none" BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px">

                 <uc1:FeeSponsor id="FeeSponsor1"
                    runat="server"></uc1:FeeSponsor>
                </asp:Panel>
               
                    </td>
            <td style="width: 17px">
            </td>
        </tr>
    </table>

           </div>
       </div>
   </div>

</asp:Content>

