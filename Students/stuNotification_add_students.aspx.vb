﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class stuNotification_add_students
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            bindStudentsGrid()
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        bindStudentsGrid()
    End Sub

    Private Sub bindStudentsGrid()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        Dim str_query As String = ""



        Dim str_FullName As String = String.Empty

        Dim FullName As String = String.Empty

        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty

        Try

            If gvPDC.Rows.Count > 0 Then
                txtSearch = gvPDC.HeaderRow.FindControl("txtFullName")
                If txtSearch.Text.Trim <> "" Then
                    FullName = " AND replace(FullName,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FullName = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = FullName

            param(0) = New SqlParameter("@stu_ids", "")
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(2) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GET_STUDENT_LIST_NOTIFICATION]", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPDC.DataSource = ds
                gvPDC.DataBind()
                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "Currently there is no students Exists"
            Else
                gvPDC.DataSource = ds
                gvPDC.DataBind()
                txtSearch.Text = str_FullName
            End If
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub btnSearchFullName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindStudentsGrid()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindStudentsGrid()
    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click
        Dim Ids As String = String.Empty

        For Each gvrow As GridViewRow In gvPDC.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            If chk IsNot Nothing And chk.Checked Then
                Ids += gvPDC.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

            End If
        Next

        ''checking if seat is available

        Ids = Ids.Trim(",".ToCharArray())
        hdnSelected.Value = Ids
        'Session("liEmpList") = Ids
        hdnSelectedTrainersSelection.Value = Ids
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Script11weasd", "setStudentsToParent();", True)


    End Sub
End Class
