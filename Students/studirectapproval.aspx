<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="studirectapproval.aspx.vb" Inherits="Students_studirectapproval" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

<script type="text/javascript">

  function getDocuments() 
 {        
            var sFeatures;
            sFeatures="dialogWidth: 700px; ";
            sFeatures+="dialogHeight: 400px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
           // url='../Students/ShowAcademicInfo.aspx?id='+mode;
         
           url=document.getElementById("<%=hfURL.ClientID %>").value
                     
           window.showModalDialog(url,"", sFeatures);
           return false;
                    
}
          
    </script>
    
<div>
    <table class="matters" border=1 bordercolor="#1b80b6" cellpadding="5">
       <tr id="tr2" runat="server">
                    <td align="right" colspan="7" valign="middle" style="width: 785px">
                        <asp:LinkButton ID="lnkDocs" OnClientClick="javascript:return getDocuments();" ForeColor="red" runat="server" Visible="False">Pending Documents</asp:LinkButton>  
                    </td>
                   </tr>
      <tr class="subheader_img">
           
                <td align="left" colspan="3" style="width: 777px" valign="middle">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Approval</font></td>
            </tr>
            
                              <tr id="trDoc" runat="server">
                    <td align="center" class="matters" colspan="9" >
                  <table cellpadding="5"  >
                  <tr><td colspan=3 class="matters">The following documents has to be collected before approval</td></tr>
                            <tr>
                                <td class="matters">
                                  To Collect
                                    <br />
                                    <asp:ListBox ID="lstNotSubmitted" runat="server" Font-Bold="true" Font-Size="xx-Small" Height="100px"
                                        SelectionMode="Multiple" Style="overflow: auto" Width="200px"></asp:ListBox>
                                </td>
                                <td>
                                    <asp:Button ID="btnRight" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                        Height="15px" OnClick="btnRight_Click" Text=">>" /><br />
                                    <asp:Button ID="btnLeft" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                        Height="15px" OnClick="btnLeft_Click" Text="<<" />
                                </td>
                                <td class="matters">
                                   Collected
                                    <br />
                                    <asp:ListBox ID="lstSubmitted" Font-Bold="true" runat="server" Font-Size="xx-Small" Height="100px"
                                        SelectionMode="Multiple" Style="overflow: auto" Width="200px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                        <asp:Label id="lblmessage" runat="server" ForeColor="Red" Width="306px"></asp:Label></td>
                </tr>
                
         <tr>
                  <td style="width: 100px" align="left" class="matters">
                  Application decision</td>
                    <td align="center" class="matters">:</td>
                  <td style="width: 100px">
                      <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlApplication" Width="202px"> </asp:DropDownList>
                      </td>
                    
                     </tr>
                             
                  <tr>
                  <td style="width: 100px" align="left">
                  Reason</td>
                  <td align="center" class="matters">:</td>
                  <td style="width: 100px">
                  <asp:DropDownList runat="server" ID="ddlReason" Width="202px"> </asp:DropDownList>
                  </td>
                  </tr>
        <tr>
            <td align="left" style="width: 100px">
                Save and Print Offer letter</td>
            <td align="center" class="matters">
                :</td>
            <td align="left" style="width: 100px">
                <asp:CheckBox id="chkPrint" runat="server">
                </asp:CheckBox></td>
        </tr>
        <tr>
            <td colspan=3 >
                <asp:Button class="button" ID="btnapprove" runat="server" Text="Save" />
                <asp:Button class="button" ID="btnCancel" runat="server" Text="Cancel" /></td>
        </tr>
    </table>
</div>
 <asp:HiddenField ID="hfURL" runat="server" />
  <asp:HiddenField ID="HiddenEQSID" runat="server" />
</asp:Content>
