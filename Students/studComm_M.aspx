<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studComm_M.aspx.vb" Inherits="Students_studSubject_M" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <script language="javascript" type="text/javascript">
 function getAcademic() 
 {     
            var sFeatures;
            sFeatures="dialogWidth: 445px; ";
            sFeatures+="dialogHeight: 310px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/ShowAcademicInfo.aspx?id=DT' 
            var oWnd = radopen(url, "pop_dt");
         <%--   if (mode=='')
             {          
            result = window.showModalDialog(url,"", sFeatures);
                                 
            if (result=='' || result==undefined)
            {    return false; 
                 }   
             NameandCode = result.split('___');  
             document.getElementById("<%=txtCode.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfTemp.ClientID %>").value=NameandCode[1];
             }
             --%>
                       
                      
 }

        function OnClientClose(oWnd, args) {
          
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Acad.split('||');
               document.getElementById("<%=txtCode.ClientID %>").value=NameandCode[0];
             document.getElementById("<%=hfTemp.ClientID %>").value=NameandCode[1];
                   __doPostBack('<%= txtCode.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }   

         
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_dt" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>        
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left"  valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="top">

                            <asp:Table ID="tblCommStud" runat="server" CellPadding="7" CellSpacing="0" Height="100%" Width="100%">
                                <asp:TableRow ID="MainHead" runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
                                    <asp:TableCell runat="server" ColumnSpan="3" CssClass="title-bg">
                                        <asp:Label
                                            ID="lblTitle" runat="server" Text="Employee Category" CssClass="field-label"></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow ID="ro1" runat="server"
                                    HorizontalAlign="Left">
                                    <asp:TableCell ID="clID" runat="server" Width="20%">
                                       
                            <asp:Label ID="lblID" runat="server" Text="ID"></asp:Label>
                                    </asp:TableCell>

                                    <asp:TableCell ID="clControl1" runat="server" Width="40%">
                                       
                            <asp:TextBox ID="txtID" runat="server" CssClass="inputbox" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvID" runat="server" ControlToValidate="txtID"
                                            CssClass="error" Display="Dynamic" ErrorMessage="ID required." ForeColor="" ValidationGroup="vg1"></asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                </asp:TableRow>


                                <asp:TableRow ID="ro2" runat="server"
                                    HorizontalAlign="Left">
                                    <asp:TableCell ID="cl2Code" runat="server" Width="20%">
                                        &nbsp;
                            <asp:Label ID="lblCode" runat="server" Text="Code" CssClass="field-label"></asp:Label>
                                    </asp:TableCell>
                                   
                                    <asp:TableCell ID="cl2Control2" runat="server" Width="40%">
                                        &nbsp;
                            <asp:TextBox ID="txtCode" runat="server" CssClass="inputbox" ></asp:TextBox>
                                       <%--<asp:Button ID="btnAcademic" runat="server" Text="..." CssClass="button" OnClientClick="getAcademic();return false;"></asp:Button>--%>
                                        <asp:ImageButton ID="btnAcademic" runat="server" ImageUrl="../Images/cal.gif"
                                OnClientClick="getAcademic();return false;" TabIndex="18"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Code can not be left empty."
                                            ForeColor="" ValidationGroup="vg1"></asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                    <asp:TableCell>

                                    </asp:TableCell>
                                </asp:TableRow>

                                <asp:TableRow ID="ro3" runat="server"
                                    HorizontalAlign="Left">
                                    <asp:TableCell ID="cl3Descr" runat="server">
                                        &nbsp;
                            <asp:Label ID="lblDescr" runat="server" Text="Description" CssClass="field-label"></asp:Label>
                                    </asp:TableCell>
                                  
                                    <asp:TableCell ID="cl3radio1" runat="server">
                                        &nbsp;
                            <asp:TextBox ID="txtDescr" runat="server" CssClass="inputbox" ></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="rfvDescr" runat="server" ControlToValidate="txtDescr"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Description required." ForeColor="" ValidationGroup="vg1">*</asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                </asp:TableRow>


                                <asp:TableRow ID="ro4" runat="server"
                                    HorizontalAlign="Left">
                                    <asp:TableCell ID="cl4Color" runat="server">
                                        &nbsp;
                            <asp:Label ID="lblColor" runat="server" Text="House Color" CssClass="field-label" ></asp:Label>
                                    </asp:TableCell>

                                    <asp:TableCell ID="clrd2" runat="server">
                                        &nbsp;&nbsp;<asp:DropDownList ID="ddlColor" runat="server">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="ro5" runat="server"
                                    HorizontalAlign="Left">
                                    <asp:TableCell ID="clLang5" runat="server">
                                        &nbsp;
                            <asp:Label ID="lblLang" runat="server" Text="Language"  CssClass="field-label"></asp:Label>
                                    </asp:TableCell>
                                   
                                    <asp:TableCell ID="clRd5" runat="server">
                                        <asp:RadioButton ID="rdYes" Text="Yes" GroupName="lang" runat="server" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                        <asp:RadioButton ID="rdNo" Text="No" GroupName="lang" runat="server" />

                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </td>
                    </tr>
                   
                    <tr>
                        <td  valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" ValidationGroup="vg1"/>
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" ValidationGroup="vg1"/>
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vg1"/>
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <asp:HiddenField ID="hfId1" runat="server" />
                            <asp:HiddenField ID="hfTemp" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div> 
</asp:Content>

