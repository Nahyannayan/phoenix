<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudAttendance_AsON.aspx.vb" Inherits="Students_StudStaffAuthorized_Grade_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Attendance Search"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr class="matters">
                                    <td width="20%"><span class="field-label"> As on</span></td>
                                    <td width="30%"><asp:TextBox ID="txtAsOnDate" runat="server"></asp:TextBox> 
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                              Display="Dynamic" ErrorMessage="As on Date required" ForeColor="Red"
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator> 
                                    </td>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click"
                                                    Text="Search" />
                                    </td>
                          
                                </tr>
                                <tr  >
                                    <td align="left" class="matters" colspan="4" valign="top"> <br />
                                        <br />
                                        <asp:GridView ID="gvAuthorizedRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"  />
                                            <Columns>
                                                <asp:TemplateField HeaderText="STUDENT NO">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSTUDENT_IDH" runat="server" Text="STUDENT ID" CssClass="gridheader_text"  ></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtSTU_NO" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSTU_NO" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"  OnClick="btnSearchSTU_NO_Click"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'  ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STUDENT NAME">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSNAMEH" runat="server" Text="Student Name" CssClass="gridheader_text"  ></asp:Label><br />
                                                        <asp:TextBox ID="txtSNAME" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSNAME" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"   OnClick="btnSearchSNAME_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNAME" runat="server" Text='<%# Bind("SNAME") %>'  ></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GRADE">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGRDH" runat="server" Text="Grade" CssClass="gridheader_text" __designer:wfdid="w49"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGRM_DISPLAY" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGRM_DISPLAY" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w51" OnClick="btnSearchGRM_DISPLAY_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRM_DISPLAY" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' __designer:wfdid="w48"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SECTION">
                                                    <HeaderTemplate>
                                                    <asp:Label ID="lblSCT_DESCRH" runat="server" Text="Section" CssClass="gridheader_text"  ></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtSCT_DESCR" runat="server"  ></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearchSCT_DESCR" OnClick="btnSearchSCT_DESCR_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w63"></asp:ImageButton>
                                                   </HeaderTemplate>
                                                         <ItemTemplate>
                                                        <asp:Label ID="lblSCT_DESCR" runat="server" Text='<%# Bind("SCT_DESCR") %>'  ></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STATUS">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSTATUSH" runat="server" Text="Status" CssClass="gridheader_text"  ></asp:Label><br />
                                                        <asp:TextBox ID="txtStatus" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStatus" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"   OnClick="btnSearchStatus_Click"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTATUS" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="REMARKS">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblREMARKSH" runat="server" Text="Remarks" CssClass="gridheader_text" __designer:wfdid="w69"></asp:Label><br />
                                                        <asp:TextBox ID="txtREMARKS" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchREMARKS" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w71" OnClick="btnSearchREMARKS_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                            <asp:Label ID="lblREMARKSH" runat="server" Text='<%# Bind("REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                </table>
                </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_8" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
                     <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtAsOnDate"></ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>

</asp:Content>

