Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_stud_SetMOEGrade
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100452") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    bindAcademic_Grade()
                    PopulateSection()
                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvMOEGrade.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    ''ViewState("slno") = 0

                    ''10jan10
                    btnUpdate.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else
            highlight_grid()
        End If
        ''ViewState("slno") = 0
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblError.Text = ""
        GridBind()

        ''10jan10
        btnUpdate.Visible = True
    End Sub
    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim txtSearchStuName As New TextBox
        txtSearchStuName = gvMOEGrade.HeaderRow.FindControl("txtStudName")
        txtSearchStuName.Text = ""
        lblError.Text = ""
        GridBind()
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim txtSearchStuNo As New TextBox
        txtSearchStuNo = gvMOEGrade.HeaderRow.FindControl("txtStuNoSearch")
        txtSearchStuNo.Text = ""
        lblError.Text = ""
        GridBind()
    End Sub
    Protected Sub ddlgvRegular_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        GridBind()
    End Sub
#Region "PrivateMethods"
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"
        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If
    End Function
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim ds As New DataSet
            If ViewState("GRD_ACCESS") > 0 Then
                str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
                          "GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' AND GRADE_BSU_M.GRM_GRD_ID IN(SELECT DISTINCT GRADE_BSU_M.GRM_GRD_ID FROM  SECTION_M INNER JOIN GRADE_BSU_M ON SECTION_M.SCT_GRD_ID " & _
                          "=GRADE_BSU_M.GRM_GRD_ID AND SECTION_M.SCT_GRM_ID = GRADE_BSU_M.GRM_ID WHERE (SECTION_M.SCT_ID IN (SELECT  ID  FROM  dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  GRADE_SECTION_ACCESS   WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|') AS fnSplitMe_1)))" & _
                          "order by GRADE_M.GRD_DISPLAYORDER "
            Else
                str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
                          "GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()


            ddlMOE_Grade.Items.Clear()
            ddlMOE_Grade.DataSource = ds.Tables(0)
            ddlMOE_Grade.DataTextField = "GRM_DISPLAY"
            ddlMOE_Grade.DataValueField = "GRD_ID"
            ddlMOE_Grade.DataBind()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub PopulateSection()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim GRD_ID As String
            Dim str_Sql As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If

            'If ViewState("GRD_ACCESS") > 0 Then

            '    str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
            '              " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
            '              " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND " & _
            '              " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') AND SECTION_M.SCT_ID IN(SELECT  ID  FROM  dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  GRADE_SECTION_ACCESS  " & _
            '              " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))  order by SECTION_M.SCT_DESCR "
            'Else

            str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
                      " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
                      " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND " & _
                      " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "')  order by SECTION_M.SCT_DESCR "

            'End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    ''Public Function getSerialNo()
    ''    ViewState("slno") += 1
    ''    Return ViewState("slno")
    ''End Function
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvMOEGrade.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvMOEGrade.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvMOEGrade.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvMOEGrade.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvMOEGrade.Rows.Count - 1
            Dim row As GridViewRow = gvMOEGrade.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()

        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                                & " GRM_DISPLAY='" + ddlGrade.SelectedItem.Text + "'," _
                                & " STU_SCT_ID,SCT_DESCR='" + ddlSection.SelectedItem.Text + "' " _
                                & " ,isNULL(STU_BB_GRADE_DES,'')+'/'+isNULL(STU_BB_SECTION_DES,'') as MOE_Grade " _
                                & " FROM STUDENT_M  AS A " _
                                & " WHERE  STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND STU_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                                & " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString _
                                & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "







        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvHouse As New DropDownList
        Dim selectedHouse As String = ""


        If gvMOEGrade.Rows.Count > 0 Then



            txtSearch = gvMOEGrade.HeaderRow.FindControl("txtStuNoSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvMOEGrade.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text




            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        Dim ds As DataSet
        strQuery = strQuery + " ORDER BY STU_PASPRTNAME"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvMOEGrade.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvMOEGrade.DataBind()
            Dim columnCount As Integer = gvMOEGrade.Rows(0).Cells.Count
            gvMOEGrade.Rows(0).Cells.Clear()
            gvMOEGrade.Rows(0).Cells.Add(New TableCell)
            gvMOEGrade.Rows(0).Cells(0).ColumnSpan = columnCount
            gvMOEGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvMOEGrade.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvMOEGrade.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvMOEGrade.HeaderRow.FindControl("txtStuNoSearch")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvMOEGrade.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



       

    End Sub
    Sub SaveData()

        Dim transaction As SqlTransaction
        Dim strQuery As String
        Dim chkSelect As CheckBox
        Dim i As Integer
        Dim lblstuId As Label
        Dim lblstpId As Label
        Dim updatemaster As Boolean = False

        Dim flagAudit As Integer

        If txtMOE_Section.Text = "" Then
            lblError.Text = "Please choose the section"
            Exit Sub
        End If

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                For i = 0 To gvMOEGrade.Rows.Count - 1

                    chkSelect = gvMOEGrade.Rows(i).FindControl("chkSelect")
                    lblstuId = gvMOEGrade.Rows(i).FindControl("lblStuId")
                    If chkSelect.Checked = True Then
                        transaction = conn.BeginTransaction("SampleTransaction")
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + lblstuId.Text)
                        strQuery = "exec Save_StudMOE_GradeSection  " + lblstuId.Text + ",'" + ddlMOE_Grade.SelectedItem.Text + "','" + txtMOE_Section.Text + "'"
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, strQuery)

                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + lblstuId.Text + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = "Record Saved Successfully"
                        transaction.Commit()
                    End If
                Next

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        SaveData()
        GridBind()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        bindAcademic_Grade()
        If ddlGrade.Items.Count <> 0 Then
            PopulateSection()
        End If
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()

    End Sub
    

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        
    End Sub
End Class

