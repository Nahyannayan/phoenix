<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnqDocProceed_M.aspx.vb" Inherits="Students_studEnqDocProceed_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Enquiry Documents Stages
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="left"
                    cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <%--  <table width="100%" id="Table3" border=0>
                  <tr style="font-size: 11pt;">
                   <td width="50%" align="left" class="title">
                       </td>
              </tr>
              </table>--%>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="100%">

                            <%-- <table id="Table1" runat="server" align="center" border="0" bordercolor="#1b80b6"
                                cellpadding="5" cellspacing="0">
                                <tr>
                                    <td colspan="3" align="center">--%>

                            <%-- <table id="Table2" runat="server" align="center" border="0" bordercolor="#1b80b6"
                                            cellpadding="5" cellspacing="0" style="width: 602px">


                                            <tr>
                                                <td colspan="3" align="center">--%>
                            <asp:GridView ID="gvStage" runat="server" CssClass="table table-bordered table-row"
                                AutoGenerateColumns="False" OnRowDataBound="gvStage_RowDataBound"
                                PageSize="20" Width="100%" EmptyDataText="No Records">
                                <Columns>

                                    <asp:TemplateField Visible="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblStgId" runat="server" Text='<%# Bind("PRA_STG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Stage Decription">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblStage" runat="server" Text='<%# Bind("PRO_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkProceed" ToolTip='<%# Eval("PRA_STG_ID") %>' Text="Proceed to next stage without document collection" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                </Columns>

                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <%--  </td>
                                            </tr>
                                        </table>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CausesValidation="False" CssClass="button"
                                Text="Save" />

                            <%--   </td>
                                </tr>
                            </table>--%>
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfEQS_ID" runat="server" />
                            <asp:HiddenField ID="hfSTM_ID" runat="server" />
                            <asp:HiddenField ID="hfEQS_CURRSTATUSORDER" runat="server" />
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /></td>
                    </tr>

                </table>
            </div>
        </div>
    </div>


</asp:Content>

