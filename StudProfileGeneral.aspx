﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudProfileGeneral.aspx.vb" Inherits="StudProfileGeneral" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />

    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/cssfiles/BSUstyles.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/cssfiles/Accordian.css" rel="stylesheet" />

    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="/cssfiles/all-ie-only.css">
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <title>Student Profile</title>
    <style>
        /*.tooltip {
            position: relative;
            float: right;   
        }

            .tooltip > .tooltip-inner {
                background-color: #eebf3f;
                padding: 5px 15px;
                color: rgb(23,44,66);
                font-weight: bold;
                font-size: 13px;
            }

        .popOver + .tooltip > .tooltip-arrow {
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid #eebf3f;
        }*/

        /*section {
           margin:140px auto; 
  height:1000px;
        }

        .progress {
            border-radius: 0;
            overflow: visible;
        }

        .progress-bar {
            background: rgb(23,44,60);
            -webkit-transition: width 1.5s ease-in-out;
            transition: width 1.5s ease-in-out;
        }*/

        .progress {
            height: 20px;
            margin-bottom: 5px;
            overflow: hidden;
            background-color: #f5f5f5;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
        }

        .progress-bar {
            float: left;
            width: 0;
            height: 100%;
            font-size: 12px;
            line-height: 20px;
            color: #fff;
            text-align: center;
            background-color: #428bca;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
            box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
            -webkit-transition: width .6s ease;
            -o-transition: width .6s ease;
            transition: width .6s ease;
        }

        .progress-col1 {
            background-color: #ff7600;
        }

        .progress-col2 {
            background-color: rgb(151,215,0);
        }

        .progress-col3 {
            background-color: rgb(236,179,203);
        }

        .progress-col4 {
            background-color: rgb(218,24,132);
        }

        .progress-striped .progress-bar, .progress-bar-striped {
            background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            -webkit-background-size: 40px 40px;
            background-size: 40px 40px;
        }

        .progress.active .progress-bar, .progress-bar.active {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
        }

        .progress-bar-success {
            background-color: #5cb85c;
        }

        .progress-striped .progress-bar-success {
            background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: -o-linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
            background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
        }

        .box-shadow {
            box-shadow: 5px 4px 10px #ccc;
            border-bottom-right-radius: 10px;
            border-bottom-left-radius: 10px;
        }
    </style>
    <script language="javascript">

        function ShowRPTSETUP_S(id, status) {

            var sFeatures;
            sFeatures = "dialogWidth: 65%; ";
            sFeatures += "dialogHeight: 90%; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '/StudProfileGeneral.aspx?id=' + id + '&status=' + status + '';
            //alert(status) 
            //return ShowWindowWithClose(url, 'search', '65%', '90%')
            var win = window.open(url, '_blank');
            win.focus();
            return false;
            //result = window.showModalDialog(url, "", sFeatures);

            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }


    </script>
</head>
<body class="bg-dark bg-white">
    <script type="text/javascript">
        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({ trigger: 'manual' }).tooltip('hide');
        });

        $(document).ready(function () {
            // if($( window ).scrollTop() > 10){   scroll down abit and get the action   
            $(".progress-bar").each(function () {
                each_bar_width = $(this).attr('aria-valuenow');
                // alert(each_bar_width)
                $(this).width(each_bar_width + '%');
            });

            //  }  
        });

        function GetStudetails() {


            var dataToSend = { ID: '<%=ViewState("STUID")%>', MethodName: 'LoadStudentDetails' };

            var options =
            {
                url: '<%=ResolveUrl("~/StudentSearchProfile.aspx")%>?x=' + new Date().getTime(),
                data: dataToSend,
                dataType: 'JSON',
                type: 'POST',
                async: true,
                success: function (response) {
                    window.location.href = '<%=ResolveUrl("~/StudentSearchProfile.aspx")%>/';
                      //after success will redirect to new page
                  }
            }
                  $.ajax(options);


              }

              function ViewStudentProfDetails(pMode) {

                  var lstrVal;
                  var lintScrVal;

                  var NameandCode;
                  var Id;
                  /*pMode = "STUDETAILSVIEW"*/
                  Id = document.getElementById("<%=hdnStudId.ClientID%>").value
                url = "/Students/StudentProfileView.aspx?mode=" + pMode + "&Id=" + Id;

                // var oWnd = radopen(url, "pop_stuProfile");
                Popup(url);

            }


            function OnClientClose(oWnd, args) {
                //get the transferred arguments


                var arg = args.get_argument();

                if (arg) {

                    NameandCode = arg.Employee.split('|');



                    //document.getElementById(DFUDESCR).value = NameandCode[1];
                    //document.getElementById(DFUDOMID).value = NameandCode[0];

                    //document.getElementById(DFUDESCR).disabled = true;
                    //   window.location.href = "http://localhost:1202/Oasis_Redirect.aspx?Type=" + NameandCode[1] + "&STU=" + NameandCode[0] + "&MNU=" + NameandCode[2] + "&SEC=" + NameandCode[3] + "&USR=" + NameandCode[4];
                    //  window.open('http://localhost:1202/Oasis_Redirect.aspx?Type=' + NameandCode[1] + '&STU=' + NameandCode[0] + '&MNU=' + NameandCode[2] + '&SEC=' + NameandCode[3] + '&USR=' + NameandCode[4] , '_blank');
                    //  window.open('http://localhost:1202/Oasis_Redirect.aspx?Type=1', '_blank');
                    //   window.open('https://support.wwf.org.uk/earth_hour/index.php?type=individual','_blank');
                }
            }

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;

                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }
            function setCustomPosition(sender, args) {

                sender.moveTo(sender.get_left(), sender.get_top());

            }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Student Profile
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <form id="form1" runat="server">
                    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
                    </ajaxToolkit:ToolkitScriptManager>
                    <telerik:RadWindowManager ID="radEmployee" ShowContentDuringLoad="false" VisibleStatusbar="false"
                        ReloadOnShow="true" runat="server" EnableShadow="true">
                        <Windows>
                            <telerik:RadWindow ID="pop_stuProfile" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="900px" Height="700px" OnClientShow="setCustomPosition">
                            </telerik:RadWindow>
                        </Windows>

                    </telerik:RadWindowManager>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="card-header-profile col-md-2 text-center mb-3">
                                <%--<img class="img-thumbnail border-radius mb-2" src="https://school.gemsoasis.com/OASISPHOTOSNEW/OASIS_HR/ApplicantPhoto//900501/empphoto/27932/EMPPHOTO.JPG" alt="Card image cap" >--%>
                                <asp:Image ID="imgStuImage" runat="server" class="img-thumbnail border-radius mb-2 img-profile" ImageUrl="~/Images/Photos/no_image.gif" />

                                <div class="nav flex-column nav-pills text-left" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <%--<a class="nav-link border-bottom active" id="v-pills-main-tab" data-toggle="pill" href="#v-pills-main" role="tab" aria-controls="v-pills-main" aria-selected="true">Main</a>
                    <a class="nav-link border-bottom" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false" >Fee Details</a>
                    <a class="nav-link border-bottom" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Curriculum</a>
                    <a class="nav-link border-bottom" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false" >Attendance</a>
                                    --%>      <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('STUDENTPROFILE');return false;">Main</a>
                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('PARENTPROFILE');return false;">Parent</a>
                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('STUDENTATTENDANCE');return false;">Attendance</a>

                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('STUDENTFEES');return false;">Fees</a>
                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('CURRICULUM');return false;">Curriculum</a>
                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('BEHAVIOUR');return false;">Achievements</a>
                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('LIBRARY');return false;">Library</a>
                                    <a class="nav-link border-bottom" href="#" onclick="javascript:ViewStudentProfDetails('COMMUNICATION');return false;">Communication</a>

                                </div>
                                <div class="nav flex-column nav-pills text-left" id="vtab2" role="tablist" aria-orientation="vertical" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkAttendance" runat="server" Text="Attendance" CssClass="nav-link border-bottom"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkCurriculum" runat="server" Text="curriculum" CssClass="nav-link border-bottom"></asp:LinkButton>

                                </div>
                            </div>
                            <div class="col-md-10">
                                <!-- Outer container for the tab content starts here -->
                                <div class="tab-content" id="v-pills-tabContent">
                                    <!-- First Tab content starts here -->
                                    <div class="tab-pane fade show active" id="v-pills-main" role="tabpanel" aria-labelledby="v-pills-main-tab">

                                        <div class="row mb-3">
                                            <div class="col-sm-6">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Basic Details</h6>
                                                    <div class="card-body">
                                                        <table class="table">
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Name</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="ltStudName" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Student ID</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="ltStudId" runat="server"></asp:Label></span></td>
                                                            </tr>

                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Grade & Section</span></td>

                                                                <td>
                                                                    <asp:Label ID="ltGrd" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Current Status</span></td>

                                                                <td>
                                                                    <asp:Label ID="ltStatus" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Username</span></td>

                                                                <td>
                                                                    <asp:Label ID="ltStudUserName" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">House</span></td>
                                                                <td>
                                                                    <asp:Label ID="ltHouseStu" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="border-bottom" runat="server" id="rowLastAtt" visible="false">
                                                                <td>
                                                                    <span class="profile-label">Last Attendance Date</span>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="ltLastAttDate" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" class="border-bottom" id="rowLeaveDate" visible="false">
                                                                <td>
                                                                    <span class="profile-label">Leave Date</span>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="ltLeaveDate" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <h5 class="card-title"></h5>
                                                        <p class="card-text"></p>
                                                        <asp:HiddenField ID="hdnStudId" runat="server" />
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('STUDENTPROFILE');return false;">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Parent Details / Contact Info</h6>
                                                    <div class="card-body">
                                                        <table class="table">
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Primary Contact</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPrimarycontact" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Name</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPrimaryname" runat="server"></asp:Label></span></td>
                                                            </tr>

                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Mobile</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPrimaryMobile" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Email</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPrimaryEmail" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Username</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblParentUsername" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                        </table>
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('PARENTPROFILE');return false;">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--- Sibling-->
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Sibling Details</h6>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <asp:GridView ID="gvStudChange" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                    CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                                                    PageSize="20" Width="100%" AllowSorting="True">
                                                                    <RowStyle CssClass="griditem" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStu_id" runat="server" Text='<%# Bind("Stu_id") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="center" />
                                                                            <HeaderStyle HorizontalAlign="center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Stud. No">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="center" />
                                                                            <HeaderStyle HorizontalAlign="center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Student Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSNAME" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Grade &amp; Section">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblGrd_Sct" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="center" />
                                                                            <HeaderStyle HorizontalAlign="center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Current Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# bind("Stu_currstatus") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="center" />
                                                                            <HeaderStyle HorizontalAlign="center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="View">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lblView" runat="server" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;STU_ID&quot;) & &quot;','&quot; & Container.DataItem(&quot;Stu_currstatus&quot;) & &quot;');return false;&quot; %>">View</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="center" />
                                                                            <HeaderStyle HorizontalAlign="center" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <SelectedRowStyle />
                                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <!--- fees-->
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Fee Summary</h6>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <telerik:RadHtmlChart runat="server" ID="radFeeTotalPaidChart" Height="250" Skin="Metro">
                                                                    <PlotArea>
                                                                        <Series>
                                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="Y">
                                                                                <TooltipsAppearance DataFormatString="{0:n}" />
                                                                                <LabelsAppearance Visible="true" />
                                                                            </telerik:ColumnSeries>
                                                                        </Series>
                                                                        <XAxis DataLabelsField="X">
                                                                        </XAxis>
                                                                        <YAxis>
                                                                            <LabelsAppearance DataFormatString="{0:n}" />
                                                                        </YAxis>
                                                                    </PlotArea>
                                                                    <Legend>
                                                                        <Appearance Visible="false" />
                                                                    </Legend>
                                                                    <ChartTitle Text="FEE PAID BY MONTH">
                                                                    </ChartTitle>
                                                                </telerik:RadHtmlChart>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <telerik:RadHtmlChart runat="server" ID="radFeePiechart" Height="250" Skin="Metro">
                                                                    <PlotArea>
                                                                        <Series>
                                                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                                                <LabelsAppearance DataFormatString="{0:n}">
                                                                                </LabelsAppearance>
                                                                                <TooltipsAppearance>
                                                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                                                </TooltipsAppearance>

                                                                            </telerik:DonutSeries>

                                                                        </Series>
                                                                        <YAxis>
                                                                        </YAxis>
                                                                    </PlotArea>
                                                                    <Legend>

                                                                        <Appearance Position="Right" Visible="true" />
                                                                    </Legend>
                                                                    <ChartTitle Text="OUTSTANDING AS ON TODAY">
                                                                    </ChartTitle>
                                                                </telerik:RadHtmlChart>
                                                            </div>


                                                        </div>
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('STUDENTFEES');return false;">View More</a>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <!--- attendance-->

                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Attendance Summary</h6>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-6">

                                                                <asp:Repeater ID="rptAttendance" runat="server">

                                                                    <ItemTemplate>
                                                                        <h3 class="card-title mb-0">Attendance for the year</h3>
                                                                        <span class="stud-cont-gray"><%# Eval("acd_year")%></span>
                                                                        <div class="row">

                                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("tot_marked")%></span></div>
                                                                            <div class="col-md-8 col-lg-8">




                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>Present</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("tot_att")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("tot_att")%>'><%# Eval("tot_att")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>

                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>Authorized Absent </b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("tot_leave")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("tot_leave")%>'><%# Eval("tot_leave")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>

                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>Unauthorized absent</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col4" role="progressbar" aria-valuenow='<%# Eval("tot_abs")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("tot_abs")%>'><%# Eval("tot_abs")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>


                                                                            </div>
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <%--   <asp:Literal ID="FCLiteral" runat="server" Visible="false"></asp:Literal>--%>
                                                            </div>
                                                            <div class="col-sm-6">

                                                                <telerik:RadHtmlChart runat="server" ID="radAttendanceChart" Height="250">
                                                                    <PlotArea>
                                                                        <Series>
                                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="TOT_ATT">
                                                                                <TooltipsAppearance />
                                                                                <LabelsAppearance Visible="true" />
                                                                            </telerik:ColumnSeries>
                                                                        </Series>
                                                                        <XAxis DataLabelsField="TMONTH">
                                                                        </XAxis>
                                                                        <YAxis>
                                                                            <LabelsAppearance />
                                                                        </YAxis>
                                                                    </PlotArea>
                                                                    <Legend>
                                                                        <Appearance Visible="false" />
                                                                    </Legend>
                                                                    <ChartTitle Text="ATTENDANCE BY MONTH">
                                                                    </ChartTitle>
                                                                </telerik:RadHtmlChart>
                                                            </div>
                                                        </div>
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('STUDENTATTENDANCE');return false;">View More</a>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>



                                        <!--Merit -->
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Achievements - Behavioural Merits/De Merits</h6>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <asp:Repeater ID="rptAchievements" runat="server">

                                                                    <ItemTemplate>
                                                                        <h3 class="card-title mb-0">Achievements</h3>
                                                                        <span class="stud-cont-gray"><%# Eval("ACDYEAR")%></span>
                                                                        <div class="row">

                                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("STUD_TOTALPERC")%></span></div>
                                                                            <div class="col-md-8 col-lg-8">




                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>School</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("TOTALSCHOOLRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSCHOOLRANK")%>'><%# Eval("TOTALSCHOOLRANK")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>

                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>Student</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("TOTALSTUDENTRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSTUDENTRANK")%>'><%# Eval("TOTALSTUDENTRANK")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>




                                                                            </div>
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                            <div class="col-sm-4">


                                                                <asp:Repeater ID="rptBehaviouralMerits" runat="server">

                                                                    <ItemTemplate>
                                                                        <h3 class="card-title mb-0">Behavioural Merits</h3>
                                                                        <span class="stud-cont-gray"><%# Eval("ACDYEAR")%></span>
                                                                        <div class="row">

                                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("STUD_TOTALPERC")%></span></div>
                                                                            <div class="col-md-8 col-lg-8">




                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>School</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("TOTALSCHOOLRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSCHOOLRANK")%>'><%# Eval("TOTALSCHOOLRANK")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>

                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>Student</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("TOTALSTUDENTRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSTUDENTRANK")%>'><%# Eval("TOTALSTUDENTRANK")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>




                                                                            </div>
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:Repeater>



                                                            </div>
                                                            <div class="col-sm-4">

                                                                <asp:Repeater ID="rptDemerit" runat="server">

                                                                    <ItemTemplate>
                                                                        <h3 class="card-title mb-0">Behavioural De Merits</h3>
                                                                        <span class="stud-cont-gray"><%# Eval("ACDYEAR")%></span>
                                                                        <div class="row">

                                                                            <div class="col-md-2 col-lg-2  align-items-center"><span class="stud-num1"><%# Eval("STUD_TOTALPERC")%></span></div>
                                                                            <div class="col-md-8 col-lg-8">




                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>School</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col1" role="progressbar" aria-valuenow='<%# Eval("TOTALSCHOOLRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSCHOOLRANK")%>'><%# Eval("TOTALSCHOOLRANK")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>

                                                                                <div class="barWrapper">
                                                                                    <span class="progressText"><b>Student</b></span>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar progress-col2" role="progressbar" aria-valuenow='<%# Eval("TOTALSTUDENTRANK")%>' aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="popOver" data-toggle="tooltip" data-placement="top" title='<%# Eval("TOTALSTUDENTRANK")%>'><%# Eval("TOTALSTUDENTRANK")%></span>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>




                                                                            </div>
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('BEHAVIOUR');return false;">View More</a>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        <!--- curriculum-->
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Curriculum</h6>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-12 mb-5">

                                                                <telerik:RadHtmlChart runat="server" ID="RadCurriculumPie" Height="250px">
                                                                    <PlotArea>
                                                                        <Series>
                                                                            <telerik:ColumnSeries Name="Highest" DataFieldY="HIGHEST">

                                                                                <LabelsAppearance Visible="True">
                                                                                </LabelsAppearance>
                                                                            </telerik:ColumnSeries>
                                                                            <telerik:ColumnSeries Name="Lowest" DataFieldY="LOWEST">

                                                                                <LabelsAppearance Visible="True">
                                                                                </LabelsAppearance>
                                                                            </telerik:ColumnSeries>
                                                                            <telerik:ColumnSeries Name="Average" DataFieldY="GRADE_AVG">

                                                                                <LabelsAppearance Visible="True">
                                                                                </LabelsAppearance>
                                                                            </telerik:ColumnSeries>
                                                                            <telerik:ColumnSeries Name="Performance" DataFieldY="RST_MARK">

                                                                                <LabelsAppearance Visible="True">
                                                                                </LabelsAppearance>
                                                                            </telerik:ColumnSeries>
                                                                        </Series>
                                                                        <XAxis DataLabelsField="SBG_SHORTCODE">
                                                                            <%-- <Items>
                                                        <telerik:AxisItem LabelText="ARB" />
                                                        <telerik:AxisItem LabelText="ENG" />
                                                        <telerik:AxisItem LabelText="EVS" />
                                                        <telerik:AxisItem LabelText="FRE" />
                                                        <telerik:AxisItem LabelText="MAT" />
                                                        <telerik:AxisItem LabelText="MS" />
                                                        <telerik:AxisItem LabelText="USS" />
                                                       
                                                    </Items>--%>
                                                                        </XAxis>
                                                                    </PlotArea>
                                                                    <ChartTitle Text="Student Performance">
                                                                    </ChartTitle>
                                                                </telerik:RadHtmlChart>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>CAT4</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Verbal SAS</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblCatVerbalSAS" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Quantitative SAS</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblQuantitativeSAS" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">NonVerbal SAS</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblNonVerbalSAS" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Spatial SAS</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblSpatialSAS" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Overall SAS</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblOverallSAS" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>Asset</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Percentile</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblEngPercentile" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Stanine</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblEngStanine" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Percentile</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblMathsPercentile" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Stenine</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblMathsStenine" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Percentile</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblSciencePercentile" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Stenine</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblScienceStenine" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>AnnualExam</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblEngMark" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblEngGrade" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblMathsMark" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblMathsGrade" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblScienceMark" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblScienceGrade" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>Term1</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTerm1EngMark" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTerm1EngGrade" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTerm1MathsMark" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTerm1MathsGrade" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTerm1ScienceMark" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTerm1ScienceGrade" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>Term2</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label1" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label2" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label3" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label4" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label5" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label6" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>Term3</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label7" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">English Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label8" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label9" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Maths Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label10" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Mark</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label11" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="">Science Grade</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="Label12" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <%--    <div class="col-sm-2">
                                                        <h5>TERM2</h5>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <h5>TERM3</h5>
                                                    </div>--%>
                                                        </div>
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('CURRICULUM');return false;">View More</a>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <!-- transport-->
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Transport</h6>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <table class="table">
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Pickup Point</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblPickupPoint" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Dropoff Point</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblDropoffPoint" runat="server"></asp:Label></span></td>
                                                                    </tr>

                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Pickup Bus No</span></td>

                                                                        <td>
                                                                            <asp:Label ID="lblPickupBusNo" runat="server"></asp:Label></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Dropoff Bus No</span></td>

                                                                        <td>
                                                                            <asp:Label ID="lblDropoffBusNo" runat="server"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-sm-6">

                                                                <telerik:RadHtmlChart runat="server" ID="radTransportFeePie" Height="250" Skin="Metro">
                                                                    <PlotArea>
                                                                        <Series>
                                                                            <telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                                                <LabelsAppearance DataFormatString="{0:n}">
                                                                                </LabelsAppearance>
                                                                                <TooltipsAppearance>
                                                                                    <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                                                </TooltipsAppearance>

                                                                            </telerik:DonutSeries>

                                                                        </Series>
                                                                        <YAxis>
                                                                        </YAxis>
                                                                    </PlotArea>
                                                                    <Legend>

                                                                        <Appearance Position="Right" Visible="true" />
                                                                    </Legend>
                                                                    <ChartTitle Text="OUTSTANDING AS ON TODAY">
                                                                    </ChartTitle>
                                                                </telerik:RadHtmlChart>


                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- health-->
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Health Records</h6>
                                                    <div class="card-body">
                                                        <asp:GridView ID="gvActivity" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                            CssClass="table table-bordered table-row" EmptyDataText="No Activity Enrolled"
                                                            HeaderStyle-Height="30" PageSize="20" Width="100%" Visible="false">
                                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                                            <EmptyDataRowStyle Wrap="False" />
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="Activity">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblActivity" runat="server" Text='<%# Bind("ALD_EVENT_NAME")%>' __designer:wfdid="w40"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Activity Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblActivityType" runat="server" Text='<%# Bind("Payment")%>' __designer:wfdid="w40"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:TemplateField>
                                                            </Columns>

                                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                                            <EditRowStyle Wrap="False" />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                        </asp:GridView>


                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>Health</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Allergies</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsAllergy" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Allergy Details</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblAllergyDetails" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Disabilities</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsDisability" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Disability Details</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblDisabilitydetails" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Special Medication</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsSpclMed" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Medication Notes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblMedNotes" runat="server"></asp:Label></span></td>
                                                                    </tr>

                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">PEd Restriction Notes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblPedRestriction" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Any other health Info</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblAnyhealthInfo" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td>
                                                                            <h5>Learning</h5>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>


                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Learning support/Therapy</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblLIsTherapy" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">LearningTherapyNotes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblTherapyNotes" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">SpclEduNeeds</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsSpclEduNeeds" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">SpclEduNeedsNotes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblSpclEduNeeds" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">EngSupport</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsEngSupport" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Eng Notes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblEngNotes" runat="server"></asp:Label></span></td>
                                                                    </tr>

                                                                    <%--<tr class="border-bottom">
                                                        <td><span class="profile-label">Ever Repeated Failed</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblIsEverRepeated" runat="server"></asp:Label></span></td>
                                                    </tr>
                                                              <tr class="border-bottom">
                                                        <td><span class="profile-label">Repeated Grade</span></td>

                                                        <td><span class="text-lg-left text-dark">
                                                            <asp:Label ID="lblRepeatedGrade" runat="server"></asp:Label></span></td>
                                                    </tr>--%>

                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Behaviour concern</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblbehaviourconcern" runat="server"></asp:Label></span></td>
                                                                    </tr>


                                                                </table>

                                                            </div>
                                                            <div class="col-sm-4">
                                                                <table class="table">
                                                                    <tr class="title-bg">
                                                                        <td colspan="2">
                                                                            <h5>Behavioural & Activities</h5>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Behaviour notes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblbehaviournotes" runat="server"></asp:Label></span></td>
                                                                    </tr>

                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">AnySpecificEnrichemnts</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsAnyspecificEnrichment" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">SpecificEnrichemntsNotes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblEnrichmentNotes" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Musically proficient</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblIsMusicallyProf" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Musically proficientNotes</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblMusicNotes" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Extra CurriculurActivities</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblExtraCurriculurActivities" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                    <tr class="border-bottom">
                                                                        <td><span class="profile-label">Interesting sports</span></td>

                                                                        <td><span class="text-lg-left text-dark">
                                                                            <asp:Label ID="lblInterestingSports" runat="server"></asp:Label></span></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <a href="#" class="btn btn-primary float-right" onclick="javascript:ViewStudentProfDetails('MEDICAL');return false;">View More</a>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                    <!-- First Tab content ends here -->
                                    <!-- Second Tab content starts here -->
                                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                                        <div class="row mb-3">
                                            <div class="col-sm-6">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Basic Details</h6>
                                                    <div class="card-body">
                                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Parent Details / Contact Info</h6>
                                                    <div class="card-body">
                                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Fee Summarys</h6>
                                                    <div class="card-body">
                                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Student Details</h6>
                                                    <div class="card-body">
                                                        <table class="table">
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Name</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblSName" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Student ID</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblSID" runat="server"></asp:Label></span></td>
                                                            </tr>

                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Grade & Section</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblSgrade" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Current Status</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblScurrStatus" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Shift</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblSShift" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Stream</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblsStream" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">House</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblShouse" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Parent Details</h6>
                                                    <div class="card-body">
                                                        <table class="table">
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Primary Contact</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPprimary" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Parent Username</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPusername" runat="server"></asp:Label></span></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Father Name</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblPFname" runat="server"></asp:Label></span></td>
                                                            </tr>

                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Father Mobile</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblFmobile" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Father Email</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblFemail" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Mother Name</span></td>

                                                                <td><span class="text-lg-left text-dark">
                                                                    <asp:Label ID="lblMotherName" runat="server"></asp:Label></span></td>
                                                            </tr>

                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Mother Mobile</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblMMobile" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr class="border-bottom">
                                                                <td><span class="profile-label">Mother Email</span></td>

                                                                <td>
                                                                    <asp:Label ID="lblMEmail" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Second Tab content ends here -->
                                    <!-- Third Tab content starts here- curriculum starts here -->
                                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                        <div>
                                            <ajaxToolkit:ModalPopupExtender ID="mpe" OnOkScript="onPrvOk();" CancelControlID="ImageButton1" DynamicServicePath="" runat="server"
                                                Enabled="True" TargetControlID="btnTarget" PopupControlID="plPrev" BackgroundCssClass="modalBackground" />
                                            <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
                                            <div id="plPrev" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; width: 80%; vertical-align: top; margin-top: 0px;">
                                                <div class="msg_header" style="width: 100%; margin-top: 0px; vertical-align: top">
                                                    <div class="msg" style="text-align: right; margin-top: 0px; background-color: #ffffff; vertical-align: top;">

                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close.png" />
                                                    </div>
                                                </div>

                                                <asp:Panel ID="plPrev1" runat="server" BackColor="White" Height="530px" ScrollBars="vertical" Width="100%">
                                                    <iframe id="ifSibDetail" runat="server" style="background-image: url(../Images/Misc/loading.gif); background-position: center center; background-attachment: fixed; background-repeat: no-repeat;" height="100%" scrolling="yes" marginwidth="0" frameborder="0" width="100%"></iframe>
                                                </asp:Panel>
                                            </div>
                                        </div>


                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Reportcard Details</h6>
                                                    <div class="card-body">
                                                        <!--curriculum reportdetails--->
                                                        <div width="100%" align="left">
                                                            <table class="table table-striped table-condensed table-hover table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
                                                                <tr>
                                                                    <td style="vertical-align: middle">Select Academic Year :
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlAcademicYear" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                                    </td>
                                                                    <td style="vertical-align: middle">
                                                                        <asp:RadioButton runat="server" ID="rdView" Text="View" Checked="True" GroupName="g1"></asp:RadioButton>
                                                                        <asp:RadioButton runat="server" ID="rdDownload" Text="Download" GroupName="g1"></asp:RadioButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <asp:DataList ID="dlReports" runat="server" RepeatColumns="1" CssClass="divcurrNoborder">
                                                            <ItemStyle Wrap="false" />
                                                            <ItemTemplate>
                                                                <div class="divCurrBox">
                                                                    <asp:Image ID="imgNew" ImageUrl="~/Images/FlashingNEW.gif"
                                                                        Style="display: inline; float: left; width: 30px; height: 20px; vertical-align: top;" runat="server" />
                                                                    <asp:Image ID="Image1" runat="server"
                                                                        ImageUrl="~/Images/arrow.png" Style="display: inline; float: left; margin-top: 5px; margin-right: 4px" />
                                                                    <asp:LinkButton ID="lnkReport" runat="server" Text='<%# Eval("RPF_DESCR") + " (Released on " + Eval("RPP_RELEASEDATE", "{0:dd/MMM/yyyy}") + ")"%>' OnClick="lnkReport_Click"></asp:LinkButton>
                                                                    <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_ID") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblRsm" runat="server" Text='<%# Bind("RPF_RSM_ID") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("RPF_DATE") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RPF") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblAcdId" runat="server" Text='<%# Bind("RSM_ACD_ID") %>' Visible="false"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                        <iframe runat="server" id="ifReport" style="display: none;"></iframe>
                                                        <asp:HiddenField ID="hfType" runat="server" />
                                                        <!---->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Third Tab content ends here -->
                                    <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">

                                        <div class="row mb-3">
                                            <div class="col-sm-6">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Attendance Details</h6>
                                                    <div class="card-body">
                                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Attendance Info</h6>
                                                    <div class="card-body">
                                                        <h5 class="card-title">Special title treatment</h6>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                                            <a href="#" class="btn btn-primary float-right">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <div class="card box-shadow">
                                                    <h6 class="card-header p-2 pl-3">Attendance Graph</h6>
                                                    <div class="card-body">
                                                        <h6 class="card-title">Special title treatment</h6>
                                                        <p class="card-text">
                                                            <asp:Label ID="lblOneName" runat="server" Text="ggg"></asp:Label>
                                                            <asp:Button ID="btnReg" runat="server" Text="Ghl" OnClick="btnReg_Click" />
                                                        </p>
                                                        <a href="#" class="btn btn-primary float-right">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- Outer container for the tab content ends here -->
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

</body>
</html>
