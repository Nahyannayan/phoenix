﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports GemBox.Spreadsheet

Partial Class PD_PDStaffReportDownload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim IsPDC = Check_PD_Coordinator()
                    Dim isPDSuperuser As Boolean = Check_PD_SuperUser()
                    If Not (IsPDC Or isPDSuperuser) Then
                        tblNoAccess.Visible = True
                        tbl_AddGroup.Visible = False
                        Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                        ' lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                        lblNoAccess.Text = Errormsg
                    End If

                    Me.Bind_Bsu()

                    'BindTrainerGroups()
                End If


                'Load the search filer parameter values from session variables

                Me.Bind_Employee()
                ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadExcel)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        Else

        End If
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        trGridv.Visible = True
        BindPD_StaffList()
    End Sub

    Protected Sub btnDownloadExcel_Click(sender As Object, e As EventArgs)
        DownloadCourseInfoExcel()
    End Sub
    Protected Sub gvPDEmployee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDEmployee.PageIndex = e.NewPageIndex
        BindPD_StaffList()
    End Sub
    Protected Sub ddlBusinessUnit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        Me.Bind_Employee()
    End Sub

    Sub BindPD_StaffList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlClient.SqlParameter


        Try


            'param(0) = New SqlParameter("@PDC_ID", 0)
            param(0) = New SqlParameter("@EMP_ID", Convert.ToInt32(ddlEmployee.SelectedItem.Value))
            param(1) = New SqlParameter("@Bsu_Id", ddlBusinessUnit.SelectedItem.Value)
            Dim ds As DataSet
            Dim Dt As DataTable
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[PD_T].[Get_Staff_PD_Course_List]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                gvPDEmployee.DataSource = ds.Tables(0)
                gvPDEmployee.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvPDEmployee.DataSource = ds.Tables(0)
                Try
                    gvPDEmployee.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPDEmployee.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPDEmployee.Rows(0).Cells.Clear()
                gvPDEmployee.Rows(0).Cells.Add(New TableCell)
                gvPDEmployee.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDEmployee.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDEmployee.Rows(0).Cells(0).Text = "No records available !!!"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BindPD_StaffList")
        End Try

    End Sub

    Private Sub DownloadCourseInfoExcel()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
            param(0) = New SqlParameter("@EMP_ID", Convert.ToInt32(ddlEmployee.SelectedItem.Value))
            param(1) = New SqlParameter("@Bsu_Id", ddlBusinessUnit.SelectedItem.Value)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[PD_T].[Get_Staff_PD_Course_List]", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else
                ''UtilityObj.Errorlog(ds.Tables(0).Rows.Count, System.Reflection.MethodBase.GetCurrentMethod().Name)

                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)
                Dim currDate As DateTime = Date.Now
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                Dim excelfiledate1 As String = currDate.ToShortDateString().ToString().Replace("/", "_").Replace(" ", "").Replace(":", "")
                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")

                Dim strFilename As String = ddlEmployee.SelectedItem.Text

                Dim stuFilename As String = "PDCourseInfo_" & strFilename & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                ''removing unwanted columns
                ''ws.Columns("A").Delete()
                'ws.Columns("C").Delete()



                ws.Cells(0, 0).Value = "Course Title"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 10000

                ws.Cells(0, 1).Value = "Start Date"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "End Date"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "Attendance"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 7000



                ws.Cells(0, 4).Value = "Eval Form Submitted"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 7000



                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub Bind_Bsu()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT '' AS id, '[SELECT]' AS NAME UNION  SELECT bsu.BSU_ID AS id, bsu.BSU_NAME AS name FROM dbo.BUSINESSUNIT_M bsu Where bsu_id in (select pdc_bsu_id from pd_co_ordinators where pdc_emp_id =" & " )) t ORDER BY NAME"

            Dim param(1) As SqlParameter

            param(0) = New SqlParameter("@usr_id", Session("sUsr_Id"))
            param(1) = New SqlParameter("@LOGGED_IN_BSU_ID", Session("sBsuId"))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[PD_M].[Get_PDC_BusinessUnit]", param)

            If Not ds Is Nothing Then
                ddlBusinessUnit.DataSource = ds.Tables(0)
                ddlBusinessUnit.DataValueField = "Id"
                ddlBusinessUnit.DataTextField = "Name"
                ddlBusinessUnit.DataBind()
                'ddlBusinessUnit.SelectedValue = Session("sBsuId")
            End If

        Catch ex As Exception
            '  Me.lblError.Text = "Error occured while loading business units"
        End Try
    End Sub

    Private Sub Bind_Employee()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim bsuId As String = String.Empty
            If Me.ddlBusinessUnit.SelectedValue = "" Then
                bsuId = "0"
            Else
                bsuId = Me.ddlBusinessUnit.SelectedValue
            End If
            Dim query As String = "Select * From (SELECT 0 AS Id, '[SELECT]' AS NAME Union SELECT E.EMP_ID As Id, ISNULL(e.EMP_FNAME,'') + ' ' + ISNULL(e.EMP_MNAME,'') + ' ' + ISNULL(e.EMP_LNAME,'') AS Name FROM dbo.EMPLOYEE_M E WHERE e.EMP_BSU_ID = '" & bsuId & "' AND E.EMP_STATUS in (1,2,9)) T Order By T.Name"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlEmployee.DataSource = ds.Tables(0)
                ddlEmployee.DataValueField = "Id"
                ddlEmployee.DataTextField = "Name"
                ddlEmployee.DataBind()
            End If

        Catch ex As Exception
            ' Me.lblError.Text = "Error occured while loading region"
        End Try
    End Sub

    Function Check_PD_Coordinator() As Boolean
        Dim IsPDC As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@User_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_By_UserID", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsPDC = True
            End If

        End If
        Return IsPDC
    End Function

    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function


End Class
