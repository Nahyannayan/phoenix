﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class PD_PD_CERTIFICATE_TELLAL
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("ID") IsNot Nothing And Request.QueryString("ID") <> "" Then
                'Dim id As Integer = Convert.ToInt32(Request.QueryString("ID").ToString())

                Dim id As Integer = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                ViewState("CR_ID") = id
                BindCourseDetails(Convert.ToInt16(ViewState("CR_ID")))

            End If
        End If
    End Sub

    Sub BindCourseDetails(ByVal Id As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", Id)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_INFO_BY_CR_ID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    lblCourse.Text = Dt.Rows(0)("CM_TITLE").ToString()
                    lblDate.Text = Dt.Rows(0)("EVENTDATE").ToString()
                    lblPArticipant.Text = Dt.Rows(0)("EMP_NAME").ToString()
                    ''  lblTrainer.Text = Dt.Rows(0)("Trainers").ToString()
                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub
End Class
