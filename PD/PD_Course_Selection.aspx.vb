﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Course_Selection
    Inherits System.Web.UI.Page
    'empId
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("empId") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("empId").ToString
                ViewState("MainID") = MainID

                If Request.QueryString("fromdate") IsNot Nothing Then
                    Dim fromDate As String = Request.QueryString("fromdate").ToString
                    ViewState("fromDate") = fromDate
                    Dim ToDate As String = Request.QueryString("ToDate").ToString
                    ViewState("ToDate") = ToDate
                End If
                bindCourseList()
            End If
        End If
        If hdnSelectedCourse.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvCourseList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCourseList.PageIndex = e.NewPageIndex
        bindCourseList()
    End Sub
    Protected Sub btnSearchCourse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindCourseList()
    End Sub

    Sub bindCourseList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim txtSearch As New TextBox
        Dim courseTitle As String = String.Empty
        Dim FILTER_COND As String = String.Empty
        Dim param(5) As SqlClient.SqlParameter
        Try
            If gvCourseList.Rows.Count > 0 Then

                txtSearch = gvCourseList.HeaderRow.FindControl("txtCourse_Title")

                If txtSearch.Text.Trim <> "" Then
                    courseTitle = "%" & txtSearch.Text.TrimEnd & "%"

                End If
            End If
            FILTER_COND = courseTitle
            param(0) = New SqlClient.SqlParameter("@Emp_id", Session("EmployeeId"))
            param(1) = New SqlClient.SqlParameter("@BSU_ID", "0")
            param(2) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)
            param(3) = New SqlClient.SqlParameter("@FROMDATE", ViewState("fromDate"))
            param(4) = New SqlClient.SqlParameter("@TODATE", ViewState("ToDate"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_BY_DATE_FILTER", param)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
                Dim columnCount As Integer = gvCourseList.Rows(0).Cells.Count
                gvCourseList.Rows(0).Cells.Clear()
                gvCourseList.Rows(0).Cells.Add(New TableCell)
                gvCourseList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCourseList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCourseList.Rows(0).Cells(0).Text = "Currently there is no courses Exists"
            Else
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim CM_IDs As String = String.Empty

        For Each gvrow As GridViewRow In gvCourseList.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            If chk IsNot Nothing And chk.Checked Then
                CM_IDs += gvCourseList.DataKeys(gvrow.RowIndex).Value.ToString() + "|"c

            End If
        Next

        CM_IDs = CM_IDs.Trim(",".ToCharArray())
        hdnSelectedCourse.Value = CM_IDs
        Session("liCMList") = CM_IDs

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('hdnSelectedCourse').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameCode = '" & hdnSelectedCourse.Value & "||" & hdnSelectedCourse.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & hdnSelectedCourse.Value & "||" & hdnSelectedCourse.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("parent.$.fancybox.close();")
        'Response.Write("} </script>")

    End Sub
End Class
