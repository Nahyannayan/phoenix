﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_CO_ORDINATOR_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD00088") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("PDC_ID") = 0
                    BindPD_PD_Co_Ordinators()

                    '' rdoGemsStaff_SelectedIndexChanged(rdoGemsStaff, Nothing)

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights                   

                    ' h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub

    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_Co_Ordinator_Add.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String

            Dim lblPDC_ID As New Label
            lblPDC_ID = TryCast(sender.FindControl("lblPDC_ID"), Label)
            ViewState("PDC_ID") = lblPDC_ID.Text
            lblError.InnerHtml = ""

            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim PDCId As String = Encr_decrData.Encrypt(lblPDC_ID.Text)
            url = String.Format("~\PD\PD_Co_Ordinator_Add.aspx?MainMnu_code={0}&datamode={1}&pdcId={2}", ViewState("MainMnu_code"), ViewState("datamode"), PDCId)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnSearchEMP_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Ordinators()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Ordinators()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_PD_Co_Ordinators()
    End Sub


    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        BindPD_PD_Co_Ordinators()
    End Sub

    Sub BindPD_PD_Co_Ordinators()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlClient.SqlParameter

        Dim str_EMP_NO As String = String.Empty
        Dim str_EMP_Name As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim EMP_NO As String = String.Empty
        Dim EMP_Name As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim Dt As New DataTable
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty

        If gvPDC.Rows.Count > 0 Then

            txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NO")

            If txtSearch.Text.Trim <> "" Then
                EMP_NO = " WHERE replace(PDC_EMP_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                str_EMP_NO = txtSearch.Text.Trim
            End If

            txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_Name")

            If txtSearch.Text.Trim <> "" Then

                EMP_Name = " AND replace(PDC_FNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(PDC_LNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"


                str_EMP_Name = txtSearch.Text.Trim
            End If

            txtSearch = gvPDC.HeaderRow.FindControl("txtBSU_Name")

            If txtSearch.Text.Trim <> "" Then

                BSU_Name = " AND replace(BSU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "


                str_BSU_Name = txtSearch.Text.Trim
            End If

        End If
        FILTER_COND = EMP_NO + EMP_Name + BSU_Name
        param(0) = New SqlParameter("@PDC_ID", 0)
        param(1) = New SqlParameter("@FILTERCONDITION", FILTER_COND)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PD_CORDINATORS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dt = ds.Tables(0)
            gvPDC.DataSource = ds.Tables(0)
            gvPDC.DataBind()
        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            gvPDC.DataSource = ds.Tables(0)
            Try
                gvPDC.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
            ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvPDC.Rows(0).Cells.Clear()
            gvPDC.Rows(0).Cells.Add(New TableCell)
            gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
            gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvPDC.Rows(0).Cells(0).Text = "No Records vailable !!!"
        End If
        txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NO")
        txtSearch.Text = str_EMP_NO

        txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_Name")
        txtSearch.Text = str_EMP_Name

        txtSearch = gvPDC.HeaderRow.FindControl("txtBSU_Name")
        txtSearch.Text = str_BSU_Name
    End Sub
End Class
