﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Participants
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("ID") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("ID").ToString
                ViewState("MainID") = MainID
                bindParticipantsGrid()
            End If
        End If

    End Sub

    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGrid()
    End Sub

    Private Sub bindParticipantsGrid()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("MainID"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List", param)


        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvParticipants.DataSource = ds
            gvParticipants.DataBind()
            Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
            gvParticipants.Rows(0).Cells.Clear()
            gvParticipants.Rows(0).Cells.Add(New TableCell)
            gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
            gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
        Else
            gvParticipants.DataSource = ds
            gvParticipants.DataBind()
        End If

    End Sub
End Class
