﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PD_CERTIFICATE_Before1Sep2015.aspx.vb" Inherits="PD_PD_CERTIFICATE_Before1Sep2015" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
<title>PD certificate 2013-2014</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">td img {display: block;}</style>
<!--Fireworks CS5 Dreamweaver CS5 target.  Created Thu Sep 05 14:16:06 GMT+0400 (Arabian Standard Time) 2013-->
</head>

<body bgcolor="#ffffff" style="text-align:center" onload="printpage()">
<div style="zindex:100">
<span style="position:absolute;top:180px;Left:430px">
<p style="font-weight:bold;color:#000000;font-size:55px;"><asp:Label ID="lblPArticipant" runat="server"></asp:Label></P></span>

<span style="position:absolute;top:390px;Left:430px">
<p style="font-weight:bold;color:#000000;font-size:30px;font-family:Arial Baltic"><asp:Label ID="lblCourse" runat="server"></asp:Label> </P></span>


<span style="position:absolute;top:468px;Left:318px">
<p style="font-weight:bold;color:#000000;font-size:25px;"><asp:Label ID="lblDate" runat="server"> </asp:Label></P></span>

<span style="position:absolute;top:948px;right:58px">
<p style="font-weight:Normal;color:#000000;font-size:20px;text-align:left;font-family:Arial;"><asp:Label ID="lblTrainer" runat="server"></asp:Label> </P></span>

</div>
<div style="z-index:0">

<table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="1600px">
<!-- fwtable fwsrc="PD certificate template 2013-2014.png" fwpage="Page 1" fwbase="PD certificate template 2013-2014.jpg" fwstyle="Dreamweaver" fwdocid = "2061693461" fwnested="0" -->
  <tr>
   <td><img src="images/spacer.gif" width="800" height="1" border="0" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="1" border="0" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="1" border="0" alt="" /></td>
  </tr>

  <tr>
   <td colspan="2"><img name="PDcertificatetemplate20132014_r1_c1" src="images/PD%20certificate%20template%202013-2014_r1_c1.jpg" width="1600px" height="284" border="0" id="PDcertificatetemplate20132014_r1_c1" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="284" border="0" alt="" /></td>
  </tr>
  <tr>
   <td><img name="PDcertificatetemplate20132014_r2_c1" src="images/PD%20certificate%20template%202013-2014_r2_c1.jpg" width="1600px" height="231" border="0" id="PDcertificatetemplate20132014_r2_c1" alt="" /></td>
   <td valign="top"><p style="margin:0px"></p></td>
   <td><img src="images/spacer.gif" width="1" height="231" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="2"><img name="PDcertificatetemplate20132014_r3_c1" src="images/PD%20certificate%20template%202013-2014_r3_c1.jpg" width="1600px" height="429" border="0" id="PDcertificatetemplate20132014_r3_c1" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="429" border="0" alt="" /></td>
  </tr>
  <tr>
   <td colspan="2"><img name="PDcertificatetemplate20132014_r4_c1" src="images/PD%20certificate%20template%202013-2014_r4_c1.jpg" width="1600px" height="237" border="0" id="PDcertificatetemplate20132014_r4_c1" usemap="#m_PD20certificate20template2020132014_r4_c1" alt="" /></td>
   <td><img src="images/spacer.gif" width="1" height="237" border="0" alt="" /></td>
  </tr>
</table></div>
<map name="m_PD20certificate20template2020132014_r4_c1" id="m_PD20certificate20template2020132014_r4_c1">
<area shape="rect" coords="1408,28,1588,167" href="http://www.gemseducation.com" target="_blank" title="GEMS Education" alt="GEMS Education" />
<area shape="rect" coords="1128,28,1392,167" href="http://www.gemseducation.com/philanthropy" target="_blank" alt="" />
</map>
</body>
</html>
