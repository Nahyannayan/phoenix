﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PD_CO_ORDINATORS_ADD.aspx.vb" Inherits="PD_PD_CO_ORDINATORS_ADD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
 <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
  <script type="text/javascript">
        $(document).ready(function () {
            $('[id$=chkHeader]').click(function () {
                $("[id$='chkChild']").attr('checked', this.checked);
            });



            $("<%= btnAddPDC.ClientID %>").click(function () {
                var chkboxrowcount = $("#<%=gvPDC.ClientID%> input[id*='chkChild']:checkbox:checked").size();
                if (chkboxrowcount == 0) {
                    alert("please select at least a record");
                    return false;
                }
                return true;
            });

        });
            </script>
  <style>
  .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
    }
    .GridPager a
    {
        background-color: #f5f5f5; 
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }

  </style>
 <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr class="title">
            <td style="height: 25px" align="left">
                <asp:Literal ID="ltHeader" runat="server" Text="PD CO ORDINATORS"></asp:Literal>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="2" cellspacing="2" width="80%" style="border-style: none;
        border-width: 0px;">
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr id="trAdd">
            <td>
                <div style="text-align: right; margin: 5px; font-weight: bold;">
                   <table>
                         <tr  id="trBSU" runat="server">
                        <td class="matters">
                            Business Unit<font color="maroon">*</font>
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="true" onchange="if(this.selectedIndex == 0)return false;" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvBSU" runat="server" ControlToValidate="ddlBusinessunit" InitialValue="0" ValidationGroup="rfAdd"><font color="red">Please Select </font></asp:RequiredFieldValidator>
                            <br />
                        </td>
                    </tr>
                   </table>
                   </div>
            </td>
        </tr>
        <tr id="trGridv" runat="server">
            <td align="center">
                <table width="100%">
                    <tr class="subheader_img">
                        <td>
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                PD CO ORDINATORS</span></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvPDC" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="20" Width="100%" OnPageIndexChanging="gvPDC_PageIndexChanging" DataKeyNames ="EMP_ID">
                                <Columns>
                                  <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkHeader" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkChild" runat="server" />
                                            <asp:Label ID="lblEMP_ID" runat="server" Text='<%# Bind("EMP_ID") %>' Visible="false"></asp:Label>
                                             <asp:Label ID="lblBsu_id" runat="server" Text='<%# Bind("BSU_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">

                                       <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                            Business Unit
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchBSU_NAME_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# bind("bsu_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp No">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                            EMP NO
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtEMP_NO" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchEMP_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_NO_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp_No" runat="server" Text='<%# bind("EMPNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                            Name
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtEMP_Name" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchEMP_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_Name_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPDC_Fname" runat="server" Text='<%# bind("EMPLOYEE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    

                                    <asp:TemplateField HeaderText="Action" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="ADD"
                                                OnClick="lbtnEdit_Click" ValidationGroup="rfAdd"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                 <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" />
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </td>
                    </tr>

                         <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnAddPDC"    Text="ADD" runat="server" CssClass="button" ValidationGroup="rfAdd" />
               
            </td>
        </tr>
                </table>
            </td>
        </tr>
        </table>
</asp:Content>

