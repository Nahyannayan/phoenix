﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class PD_PDExportOnlineCourse
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim isPD_SuperUser As Boolean = Check_PD_SuperUser()

                    If Not isPD_SuperUser Then
                        tblNoAccess.Visible = True
                        tblData.Visible = False
                        'tblGridView.Visible = False
                        Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                        ' lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                        lblNoAccess.Text = Errormsg
                    End If





                End If

                'show all requests by default


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Protected Sub btnDownloadUserInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadUserInfo.Click
        Try
            DownloadUserInfoExcel()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub

    Protected Sub btnDownloadCourseInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadCourseInfo.Click
        Try
            DownloadCourseInfoExcel()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub

    Private Sub DownloadUserInfoExcel()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet



            Dim CourseDate As DateTime

            CourseDate = Convert.ToDateTime(txtFrom.Text)


            param(0) = New SqlClient.SqlParameter("@CM_EvntDate", CourseDate)
            param(1) = New SqlClient.SqlParameter("@UserID", Session("sUsr_name"))
            param(2) = New SqlClient.SqlParameter("@ReportType", 1)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_PD_ONLINE_COURSE_REPORT", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else


                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)
                Dim excelfiledate As String = CourseDate.ToShortDateString().ToString().Replace("/", "_").Replace(" ", "").Replace(":", "")
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")

                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim stuFilename As String = "PDUserInfo_" & excelfiledate & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                ''removing unwanted columns
                ''  ws.Columns("A").Delete()
                ''ws.Columns("C").Delete()



                ws.Cells(0, 0).Value = "Username"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 10000

                ws.Cells(0, 1).Value = "First Name"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "Middle Name"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "Last Name"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 7000


                ws.Cells(0, 4).Value = "Preferred Name"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 7000
                '' 

                ws.Cells(0, 5).Value = "Gender"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "Email"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("G").Width = 7000

                '''' AS [Password],

                ws.Cells(0, 7).Value = "Password"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("H").Width = 7000

                ws.Cells(0, 8).Value = "Organisation Id"
                ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("I").Width = 7000

                ws.Cells(0, 9).Value = "Organisation Unit Id"
                ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("J").Width = 7000

                ws.Cells(0, 10).Value = "Organisation Unit Name"
                ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("K").Width = 7000

                ws.Cells(0, 11).Value = "Position Id"
                ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("L").Width = 7000

                ws.Cells(0, 12).Value = "Position Name"
                ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("M").Width = 7000
                ''Reports To Position Id
                ''Reports To User Id
                ''Date Joined
                ''Time Zone
                ws.Cells(0, 13).Value = "Reports To Position Id"
                ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("N").Width = 7000

                ws.Cells(0, 14).Value = "Reports To User Id"
                ws.Cells(0, 14).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 14).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("O").Width = 7000

                ws.Cells(0, 15).Value = "Date Joined"
                ws.Cells(0, 15).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 15).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("P").Width = 7000

                ws.Cells(0, 16).Value = "Time Zone"
                ws.Cells(0, 16).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 16).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("Q").Width = 7000

                ws.Cells(0, 17).Value = "Employee Number"
                ws.Cells(0, 17).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 17).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("R").Width = 7000

                ws.Cells(0, 18).Value = "FACILITATORS"
                ws.Cells(0, 18).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 18).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("S").Width = 7000

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub DownloadCourseInfoExcel()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet



            Dim CourseDate As DateTime

            CourseDate = Convert.ToDateTime(txtFrom.Text)


            param(0) = New SqlClient.SqlParameter("@CM_EvntDate", CourseDate)
            param(1) = New SqlClient.SqlParameter("@UserID", Session("sUsr_name"))
            param(2) = New SqlClient.SqlParameter("@ReportType", 2)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_PD_ONLINE_COURSE_REPORT", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else
                ''UtilityObj.Errorlog(ds.Tables(0).Rows.Count, System.Reflection.MethodBase.GetCurrentMethod().Name)

                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                Dim excelfiledate1 As String = CourseDate.ToShortDateString().ToString().Replace("/", "_").Replace(" ", "").Replace(":", "")
                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim stuFilename As String = "PDCourseInfo_" & excelfiledate1 & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                ''removing unwanted columns
                ''ws.Columns("A").Delete()
                'ws.Columns("C").Delete()



                ws.Cells(0, 0).Value = "Owner ID"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 10000

                ws.Cells(0, 1).Value = "User ID"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "Lesson Status"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "Due Date"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 7000



                ws.Cells(0, 4).Value = "Start Date"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 7000



                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function
End Class
