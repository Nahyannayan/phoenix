﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="PD_KTAA_M.aspx.vb" Inherits="PD_PD_ADD_KTA_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameKTAA").fancybox({
                type: 'iframe',
                maxWidth: 300,
                maxHeight: 800,
                fitToView: false,
                width: '75%',
                height: '110%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr class="title">
            <td style="height: 25px" align="left">
                <asp:Literal ID="ltHeader" runat="server" Text=" Key Take-Aways and Actions"></asp:Literal>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="2" cellspacing="2" width="80%" style="border-style: none;
        border-width: 0px;">
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr id="trAdd">
            <td>
                <div style="text-align: right; margin: 5px; font-weight: bold;">
                    <table width="100%" border="1" class="BlueTableView">
                        <tr id="trCourse" runat="server">
                            <td class="matters">
                                Course /Event<font color="maroon">*</font>
                            </td>
                            <td class="matters" align="left">
                                <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                                 <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Fill KTAA" Visible="false" />
                            </td>
                           
                               
                            
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr id="GridV">
            <td>
                <asp:GridView ID="gvParticipants" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvParticipants_PageIndexChanging">
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <Columns>
                        <asp:TemplateField HeaderText="CR_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCRid" runat="server" Text='<%# Bind("CR_ID") %>' __designer:wfdid="w40"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Business Unit">
                            <ItemTemplate>
                                <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BusinessUnit") %>' __designer:wfdid="w40"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMP No">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EMPNO") %>' __designer:wfdid="w40"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPLOYEE") %>' __designer:wfdid="w40"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMAIL") %>' __designer:wfdid="w40"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone">
                            <ItemTemplate>
                                <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("PHONE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation">
                            <ItemTemplate>
                                <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="KTAA">
                            <ItemTemplate>
                                <a id="frameSubActivity" class="frameKTAA" href="PD_KTAA_VIEW.aspx?ID=<%#Eval("CR_ID")%>">
                                    VIEW</a>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
