﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="PD_Attendance_M.aspx.vb" Inherits="PD_PD_Attendance_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

   <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
   
  <script type="text/javascript">
      $(document).ready(function () {
          $('[id$=chkHeader]').click(function () {
              $("[id$='chkChild']").attr('checked', this.checked);
          });


          $("<%= btnPresent.ClientID %>").click(function () {
              var chkboxrowcount = $("#<%=gvParticipants.ClientID%> input[id*='chkChild']:checkbox:checked").size();
              if (chkboxrowcount == 0) {
                  alert("please select at least a record");
                  return false;
              }
              return true;
          });

      });
   
        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)


            if (result == '' || result == undefined) {
                //            document.getElementById("txtDate").value=''; 
                return false;
            }
            if (mode == 1)

                $("#<%=txtFrom.ClientID%>").val(result);

        }

    </script>
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Attendance
        </div>
        <div class="card-body">
            <div class="table-responsive">
  
    <table  width="100%"  id="tblData" runat="server">
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom" colspan="2">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table  width="100%">
                   
                    <tr align="left" style="display:none">
                        <td >
                            <span class="field-label" >    Business Unit<font color="maroon">*</font></span>
                        </td>
                        <td >
                            <asp:DropDownList ID="ddlbusinessunit" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <br />
                        </td>
                    </tr>
                    <tr align="left">
                        <td >
                             <span class="field-label" >   Course<font color="maroon">*</font></span>
                        </td>
                        <td >
                            <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                          
                        </td>
                   
                        <td >
                            <span class="field-label" >    Attendance Date<font color="maroon">*</font></span>
                        </td>
                        <td >
                            <asp:TextBox ID="txtFrom" runat="server" autocomplete="off"></asp:TextBox>
                             <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtFrom" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(1);return false;" Visible="false" />
                            <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                Display="Dynamic" ErrorMessage=" Date required" ValidationGroup="AttGroup" CssClass="error"
                                ForeColor="">*</asp:RequiredFieldValidator>
                          
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="matters" colspan="4">
                            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="button" Width="130px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trGridv" runat="server">
            <td align="center">
                <table width="100%">
                    <tr class="subheader_img">
                        <td>
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                            </span></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvParticipants" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvParticipants_PageIndexChanging" DataKeyNames ="CR_ID">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkHeader" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkChild" runat="server" />
                                            </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CR_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCRid" runat="server" Text='<%# Bind("CR_ID") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                     <HeaderTemplate>
                                          
                                                          <span class="field-label" >   Business Unit</span><br />
                                                    
                                                            <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchBSU_NAME_Click"></asp:ImageButton>
                                                      
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BusinessUnit") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCName" runat="server" Text='<%# Bind("CM_TITLE") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EMP No">
                                      <HeaderTemplate>
                                            <span class="field-label" >
                                                            EMP NO
                                                       </span><br />
                                                            <asp:TextBox ID="txtEMP_NO" runat="server" Width="160px"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearchEMP_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_NO_Click"></asp:ImageButton>
                                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EMPNO") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                    <HeaderTemplate>
                                            <span class="field-label" >
                                                            Name
                                                      </span><br />
                                                            <asp:TextBox ID="txtEMP_Name" runat="server" Width="160px"></asp:TextBox>
                                                      
                                                            <asp:ImageButton ID="btnSearchEMP_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_Name_Click"></asp:ImageButton>
                                                      
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPLOYEE") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                   </asp:TemplateField>
                                   
                                   
                                   <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                            <asp:Label ID="lblAttendance" runat="server" Text='<%# Bind("Attendance") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                   </asp:TemplateField>
                                </Columns>
                               
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </td>
                    </tr>

                                    <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnPresent"    Text="PRESENT" runat="server" CssClass="button" Visible ="false" ValidationGroup="rfAdd" />
                  <asp:Button ID="btnAbsent"    Text="ABSENT" runat="server" CssClass="button" Visible="false" ValidationGroup="rfAdd" />

                  <asp:Button ID="btnApprovedLeave"    Text="APPROVED LEAVE" runat="server" CssClass="button" Visible="false" ValidationGroup="rfAdd" />
               
            </td>
        </tr>
                </table>
            </td>
        </tr>
                </table>
          <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
               
                   
                        <asp:Label id="lblNoAccess" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label>
                  
                        
              
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table> 
                </div></div></div>
</asp:Content>
