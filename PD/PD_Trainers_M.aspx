﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PD_Trainers_M.aspx.vb" Inherits="PD_PD_Trainers_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <script type="text/javascript">
         $(document).ready(function () {

             $(".frameAddPDC").fancybox({
                 type: 'iframe',
                 fitToView: false,
                 width: '85%',
                 height: '95%',
                 autoSize: false,
                 closeClick: false,
                 openEffect: 'none',
                 closeEffect: 'none'
             });
         });
     </script>
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Trainers
        </div>
        <div class="card-body">
            <div class="table-responsive">
  
    <table border="0" width="100%" >
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom">
                <div id="lblError" runat="server" class="error">
                </div>
            </td>
        </tr>
        <tr id="trAdd">
            <td align="left">
                
                                <asp:LinkButton ID="lbtnAdd" runat="server" CausesValidation="false" Text="Add New"
                                    OnClick="lbtnAdd_Click" Visible="false"></asp:LinkButton>
                                     <a id="frameAdd" class="frameAddPDC" href="PD_Trainers_ADD_F.aspx">
                               ADD NEW</a> 
                           
            </td>
        </tr>
        <tr id="trGridv" runat="server">
            <td align="center">
              
                            <asp:GridView ID="gvPDTrainsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="20" Width="100%" OnPageIndexChanging="gvPDTrainsers_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPDC_ID" runat="server" Text='<%# Bind("PDT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp No">
                                        <HeaderTemplate>
                                            <span class="field-label" > 
                                                            Emp No
                                                        </span>
                                                       <br />
                                                            <asp:TextBox ID="txtEMP_NO" runat="server" Width="160px"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearchEMP_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_NO_Click"></asp:ImageButton>
                                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmp_No" runat="server" Text='<%# bind("PDT_EMP_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            <span class="field-label" > 
                                                            Name
                                                       </span>
                                            <br />
                                                            <asp:TextBox ID="txtEMP_Name" runat="server" Width="160px"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearchEMP_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchEMP_Name_Click"></asp:ImageButton>
                                                      
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPDC_Fname" runat="server" Text='<%# bind("EMPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <HeaderTemplate>
                                           <span class="field-label" > 
                                                            Business Unit
                                                       </span><br />
                                                            <asp:TextBox ID="txtBSU_Name" runat="server" Width="160px"></asp:TextBox>
                                                       
                                                            <asp:ImageButton ID="btnSearchBSU_Name" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchBSU_NAME_Click"></asp:ImageButton>
                                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# bind("bsu_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="Remove"
                                                OnClick="lbtnEdit_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                       
            </td>
        </tr>
    </table>
                </div></div></div>
</asp:Content>
