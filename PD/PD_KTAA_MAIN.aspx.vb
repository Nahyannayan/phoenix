﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class PD_PD_KTAA_MAIN
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("ID") IsNot Nothing And Request.QueryString("ID") <> "" Then
                Dim id As Integer = Convert.ToInt32(Request.QueryString("ID").ToString())
                ViewState("CR_ID") = id
                ViewState("KTAA_ID") = ""
                BindRelevanceToRole()
                BindCourseDetails(Convert.ToInt16(ViewState("CR_ID")))
                Session("COURSEACTION") = Nothing
                Bind_KTAA_MASTER(ViewState("CR_ID"))
            End If
            'BindGrid()
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ViewState("SEND_PDC") = 0
        Dim errormsg As String = String.Empty

        If ViewState("KTAA_ID") = "" Then
            'If callTransaction(errormsg) <> 0 Then
            If callTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

            Else

                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))
                resetActions()
            End If
        Else
            If updateTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

            Else

                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))
                resetActions()
            End If
        End If
    End Sub

    Protected Sub btnSavetoPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavetoPDC.Click
        ViewState("SEND_PDC") = 0
        Dim errormsg As String = String.Empty

        If ViewState("KTAA_ID") = "" Then
            'If callTransaction(errormsg) <> 0 Then
            If callTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
                Exit Sub
            Else
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))
                resetActions()
            End If
        Else
            If updateTransactionNew(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
                Exit Sub
            Else
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                Bind_KTAA_MASTER(ViewState("CR_ID"))
                resetActions()
            End If
        End If

        If Not ValidateSendToPDC() Then Exit Sub
        ViewState("SEND_PDC") = 1
        'Dim errormsg As String = String.Empty
        If SendtoPDC(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
            Bind_KTAA_MASTER(ViewState("CR_ID"))
        End If
    End Sub

    Sub BindRelevanceToRole()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim row
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.KTAA_GET_RELEVANCE_ROLE_M")

        If ds.Tables(0).Rows.Count > 0 Then
            If Session("PD_Actions_Relevance") Is Nothing Then
                Session.Add("PD_Actions_Relevance", ds)
            End If

            For Each row In ds.Tables(0).Rows

                Dim str1 As String = row("PD_Rlvnc_DESC")
                Dim str2 As String = row("PD_Rlvnc_ID")
                'Dim FLAG As Boolean = Mid(row("GRD_ID"), InStr(1, row("GRD_ID"), "|"), 1)
                'cbRelevance.Items.Add(New ListItem(str1, str2))
            Next
        End If


    End Sub

    Private Function CreateActionDetailsTable() As DataTable
        Dim dtDt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim PD_KTA_Action_Step As New DataColumn("PD_KTA_Action_Step", System.Type.GetType("System.String"))
            Dim PD_KTA_Success As New DataColumn("PD_KTA_Success", System.Type.GetType("System.String"))
            Dim PD_KTA_Timeline As New DataColumn("PD_KTA_Timeline", System.Type.GetType("System.String"))
            Dim PD_KTA_Resources As New DataColumn("PD_KTA_Resources", System.Type.GetType("System.String"))
            Dim PD_KTA_Relevance As New DataColumn("PD_KTA_Relevance", System.Type.GetType("System.String"))
            Dim PD_KTA_RelevanceText As New DataColumn("PD_KTA_RelevanceText", System.Type.GetType("System.String"))
            Dim Status As New DataColumn("Status", System.Type.GetType("System.String"))


            dtDt.Columns.Add(cUniqueID)
            dtDt.Columns.Add(PD_KTA_Action_Step)
            dtDt.Columns.Add(PD_KTA_Success)
            dtDt.Columns.Add(PD_KTA_Timeline)
            dtDt.Columns.Add(PD_KTA_Resources)
            dtDt.Columns.Add(PD_KTA_Relevance)
            dtDt.Columns.Add(PD_KTA_RelevanceText)
            dtDt.Columns.Add(Status)
            Return dtDt
        Catch ex As Exception
            Return dtDt
        End Try
        Return Nothing
    End Function

    Private Sub GridBindActionDetails()
        'gvEMPDocDetails.DataSource = Session("EMPDOCDETAILS")
        'If Session("EMPSALDETAILS") Is Nothing Then Return
        If Session("COURSEACTION") Is Nothing Then
            'gvCourseActions.DataSource = Nothing
            'gvCourseActions.DataBind()
            Return
        End If

        Dim i As Integer
        Dim strColumnName As String = String.Empty
        Dim dtTempDtl As New DataTable
        dtTempDtl = CreateActionDetailsTable()
        If Session("COURSEACTION").Rows.Count > 0 Then
            For i = 0 To Session("COURSEACTION").Rows.Count - 1
                If (Session("COURSEACTION").Rows(i)("Status").ToString.ToUpper <> "DELETED") Then
                    Dim ldrTempNew As DataRow
                    ldrTempNew = dtTempDtl.NewRow
                    For j As Integer = 0 To Session("COURSEACTION").Columns.Count - 1
                        strColumnName = dtTempDtl.Columns(j).ColumnName
                        If strColumnName = "EDD_RELATION" Then
                            'ldrTempNew.Item(strColumnName) = ddlDependRelation.Items(Session("COURSEACTION").Rows(i)(strColumnName)).Text
                        Else
                            ldrTempNew.Item(strColumnName) = Session("COURSEACTION").Rows(i)(strColumnName)
                        End If
                    Next
                    dtTempDtl.Rows.Add(ldrTempNew)
                End If
            Next
        End If
        'gvCourseActions.DataSource = dtTempDtl
        'gvCourseActions.DataBind()
    End Sub

    

    Private Function callTransaction(ByRef errormsg As String) As Integer

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CR_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim dtCOURSEACTIONDetails As DataTable
                If Session("COURSEACTION") IsNot Nothing Then
                    dtCOURSEACTIONDetails = Session("COURSEACTION")
                    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                End If

                Dim status As String = String.Empty


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@PD_KTA_LEARNIG1", txtlearning1.Text)
                param(1) = New SqlParameter("@PD_KTA_LEARNIG2", txtlearning2.Text)
                param(2) = New SqlParameter("@PD_KTA_LEARNIG3", txtlearning3.Text)

                param(3) = New SqlParameter("@CR_ID", ViewState("CR_ID"))
                param(4) = New SqlParameter("@STATUS", ViewState("SEND_PDC"))

                param(6) = New SqlParameter("@RETID", SqlDbType.Int)
                param(6).Direction = ParameterDirection.Output
                'param(5) = New SqlParameter("@MyKTAATableType", dtCOURSEACTIONDetails)
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(5).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_MASTER", param)
                Dim ReturnFlag As Integer = param(5).Value
                Dim id As Integer = param(6).Value
                ViewState("KTAA_ID") = id
                ' ''setting hdn url 
                'If Convert.ToString(ViewState("CR_ID")) <> "" AndAlso Convert.ToString(Session("KTAA_ID")) <> "" Then
                '    Dim CREqID = ViewState("CR_ID")
                '    Dim KTAAA_ID = Convert.ToString(Session("KTAA_ID"))
                '    hidden_link.Attributes("href") = "PD_KTAA_ADD_More.aspx?id=" & CREqID & "&KTA=" & KTAAA_ID

                'End If

                '' Add actions
                Dim paramA(8) As SqlParameter
                Dim iIndex As Integer
                If id > 0 Then

                    If Session("COURSEACTION") IsNot Nothing Then
                        If Session("COURSEACTION").Rows.Count > 0 Then
                            For iIndex = 0 To Session("COURSEACTION").Rows.Count - 1

                                Dim dr As DataRow = Session("COURSEACTION").Rows(iIndex)

                                paramA(0) = New SqlParameter("@PD_KTA_Action_Step", dr("PD_KTA_Action_Step"))
                                paramA(1) = New SqlParameter("@PD_KTA_Success", dr("PD_KTA_Success"))
                                paramA(2) = New SqlParameter("@PD_KTA_Timeline", dr("PD_KTA_Timeline"))
                                paramA(3) = New SqlParameter("@PD_KTA_Resources", dr("PD_KTA_Resources"))
                                paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                                paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                                paramA(6) = New SqlParameter("@RelvnceIDs", dr("PD_KTA_Relevance"))

                                paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                                paramA(7).Direction = ParameterDirection.ReturnValue

                                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                                iReturnvalue = paramA(7).Value
                                If iReturnvalue <> 0 Then
                                    Exit For

                                End If
                            Next
                        End If
                    End If
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function callTransactionNew(ByRef errormsg As String) As Integer

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CR_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim dtCOURSEACTIONDetails As DataTable
                If Session("COURSEACTION") IsNot Nothing Then
                    dtCOURSEACTIONDetails = Session("COURSEACTION")
                    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                End If

                Dim status As String = String.Empty


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@PD_KTA_LEARNIG1", txtlearning1.Text)
                param(1) = New SqlParameter("@PD_KTA_LEARNIG2", txtlearning2.Text)
                param(2) = New SqlParameter("@PD_KTA_LEARNIG3", txtlearning3.Text)

                param(3) = New SqlParameter("@CR_ID", ViewState("CR_ID"))
                param(4) = New SqlParameter("@STATUS", ViewState("SEND_PDC"))

                param(6) = New SqlParameter("@RETID", SqlDbType.Int)
                param(6).Direction = ParameterDirection.Output
                'param(5) = New SqlParameter("@MyKTAATableType", dtCOURSEACTIONDetails)
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(5).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_MASTER", param)
                Dim ReturnFlag As Integer = param(5).Value
                Dim id As Integer = param(6).Value
                ViewState("KTAA_ID") = id
                ' ''setting hdn url 
                'If Convert.ToString(ViewState("CR_ID")) <> "" AndAlso Convert.ToString(Session("KTAA_ID")) <> "" Then
                '    Dim CREqID = ViewState("CR_ID")
                '    Dim KTAAA_ID = Convert.ToString(Session("KTAA_ID"))
                '    hidden_link.Attributes("href") = "PD_KTAA_ADD_More.aspx?id=" & CREqID & "&KTA=" & KTAAA_ID

                'End If

                '' Add actions
                Dim paramA(8) As SqlParameter
                Dim iIndex As Integer
                If id > 0 Then
                    'If Session("COURSEACTION") IsNot Nothing Then
                    '    If Session("COURSEACTION").Rows.Count > 0 Then
                    '        For iIndex = 0 To Session("COURSEACTION").Rows.Count - 1

                    '            Dim dr As DataRow = Session("COURSEACTION").Rows(iIndex)

                    '            paramA(0) = New SqlParameter("@PD_KTA_Action_Step", dr("PD_KTA_Action_Step"))
                    '            paramA(1) = New SqlParameter("@PD_KTA_Success", dr("PD_KTA_Success"))
                    '            paramA(2) = New SqlParameter("@PD_KTA_Timeline", dr("PD_KTA_Timeline"))
                    '            paramA(3) = New SqlParameter("@PD_KTA_Resources", dr("PD_KTA_Resources"))
                    '            paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                    '            paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                    '            paramA(6) = New SqlParameter("@RelvnceIDs", dr("PD_KTA_Relevance"))

                    '            paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    '            paramA(7).Direction = ParameterDirection.ReturnValue

                    '            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                    '            iReturnvalue = paramA(7).Value
                    '            If iReturnvalue <> 0 Then
                    '                Exit For

                    '            End If
                    '        Next
                    '    End If
                    'End If
                    For iIndex = 0 To gvActions.Rows.Count - 1
                        Dim RelvIds As String = ""
                        Dim row As GridViewRow = gvActions.Rows(iIndex)
                        Dim cbl As CheckBoxList = CType(row.FindControl("cblRelevance"), CheckBoxList)
                        For i As Integer = 0 To cbl.Items.Count - 1
                            If cbl.Items(i).Selected = True Then
                                RelvIds &= cbl.Items(i).Value & "|"
                            End If
                        Next
                        If RelvIds.EndsWith("|") Then RelvIds = RelvIds.TrimEnd("|")

                        'If CType(row.FindControl("txtAction"), TextBox).Text <> Nothing And CType(row.FindControl("txtSuccess"), TextBox).Text <> Nothing Then
                        paramA(0) = New SqlParameter("@PD_KTA_Action_Step", CType(row.FindControl("txtAction"), TextBox).Text)
                        paramA(1) = New SqlParameter("@PD_KTA_Success", CType(row.FindControl("txtSuccess"), TextBox).Text)
                        paramA(2) = New SqlParameter("@PD_KTA_Timeline", CType(row.FindControl("txtTimeLine"), TextBox).Text)
                        paramA(3) = New SqlParameter("@PD_KTA_Resources", CType(row.FindControl("txtResources"), TextBox).Text)
                        paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                        paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                        paramA(6) = New SqlParameter("@RelvnceIDs", RelvIds)

                        paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        paramA(7).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                        iReturnvalue = paramA(7).Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                        'End If
                    Next
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    callTransactionNew = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransactionNew = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransactionNew = "0"

                End If
            Catch ex As Exception
                callTransactionNew = "1"
                errormsg = ex.Message
            Finally
                If callTransactionNew = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransactionNew <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function updateTransaction(ByRef errormsg As String) As Integer


        Dim tran As SqlTransaction

        Dim KTAA_ID As String = ViewState("KTAA_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim dtCOURSEACTIONDetails As DataTable
                If Session("COURSEACTION") IsNot Nothing Then
                    dtCOURSEACTIONDetails = Session("COURSEACTION")
                    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                End If

                Dim status As String = String.Empty


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@PD_KTA_LEARNIG1", txtlearning1.Text)
                param(1) = New SqlParameter("@PD_KTA_LEARNIG2", txtlearning2.Text)
                param(2) = New SqlParameter("@PD_KTA_LEARNIG3", txtlearning3.Text)

                param(3) = New SqlParameter("@PD_KTA_ID", KTAA_ID)
                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_UPDATE_MASTER", param)
                Dim ReturnFlag As Integer = param(4).Value


                '' Delete and insert actions 

                If KTAA_ID > 0 Then
                    Dim paramA(8) As SqlParameter
                    Dim paramD(2) As SqlParameter


                    If Session("COURSEACTION") IsNot Nothing Then
                        If Session("COURSEACTION").Rows.Count > 0 Then
                            ''Delte existing data
                            paramD(0) = New SqlParameter("@KTA_ID", KTAA_ID)
                            paramD(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                            paramD(1).Direction = ParameterDirection.ReturnValue
                            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_DELETE_ACTIONS", paramD)
                            Dim ReturnFlagD As Integer = paramD(1).Value
                            Dim iIndex As Integer
                            For iIndex = 0 To Session("COURSEACTION").Rows.Count - 1
                                ''insert newly added actions
                                Dim dr As DataRow = Session("COURSEACTION").Rows(iIndex)

                                paramA(0) = New SqlParameter("@PD_KTA_Action_Step", dr("PD_KTA_Action_Step"))
                                paramA(1) = New SqlParameter("@PD_KTA_Success", dr("PD_KTA_Success"))
                                paramA(2) = New SqlParameter("@PD_KTA_Timeline", dr("PD_KTA_Timeline"))
                                paramA(3) = New SqlParameter("@PD_KTA_Resources", dr("PD_KTA_Resources"))
                                paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                                paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                                paramA(6) = New SqlParameter("@RelvnceIDs", dr("PD_KTA_Relevance"))

                                paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                                paramA(7).Direction = ParameterDirection.ReturnValue
                                If dr("Status") <> "DELETED" Then
                                    SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                                    iReturnvalue = paramA(7).Value
                                End If

                                If iReturnvalue <> 0 Then
                                    Exit For

                                End If
                            Next
                        End If
                    End If
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    updateTransaction = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    updateTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    updateTransaction = "0"

                End If
            Catch ex As Exception
                updateTransaction = "1"
                errormsg = ex.Message
            Finally
                If updateTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf updateTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function updateTransactionNew(ByRef errormsg As String) As Integer


        Dim tran As SqlTransaction

        Dim KTAA_ID As String = ViewState("KTAA_ID")
        Dim iReturnvalue As Integer
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim dtCOURSEACTIONDetails As DataTable
                If Session("COURSEACTION") IsNot Nothing Then
                    dtCOURSEACTIONDetails = Session("COURSEACTION")
                    dtCOURSEACTIONDetails.Columns.Remove("UniqueID")

                End If

                Dim status As String = String.Empty


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@PD_KTA_LEARNIG1", txtlearning1.Text)
                param(1) = New SqlParameter("@PD_KTA_LEARNIG2", txtlearning2.Text)
                param(2) = New SqlParameter("@PD_KTA_LEARNIG3", txtlearning3.Text)

                param(3) = New SqlParameter("@PD_KTA_ID", KTAA_ID)
                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_UPDATE_MASTER", param)
                Dim ReturnFlag As Integer = param(4).Value


                '' Delete and insert actions 

                If KTAA_ID > 0 Then
                    Dim paramA(8) As SqlParameter
                    Dim paramD(2) As SqlParameter


                    'If Session("COURSEACTION") IsNot Nothing Then
                    'If Session("COURSEACTION").Rows.Count > 0 Then
                    ''Delte existing data
                    paramD(0) = New SqlParameter("@KTA_ID", KTAA_ID)
                    paramD(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    paramD(1).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_DELETE_ACTIONS", paramD)
                    Dim ReturnFlagD As Integer = paramD(1).Value

                    Dim iIndex As Integer
                    'For iIndex = 0 To Session("COURSEACTION").Rows.Count - 1
                    '    ''insert newly added actions
                    '    Dim dr As DataRow = Session("COURSEACTION").Rows(iIndex)

                    '    paramA(0) = New SqlParameter("@PD_KTA_Action_Step", dr("PD_KTA_Action_Step"))
                    '    paramA(1) = New SqlParameter("@PD_KTA_Success", dr("PD_KTA_Success"))
                    '    paramA(2) = New SqlParameter("@PD_KTA_Timeline", dr("PD_KTA_Timeline"))
                    '    paramA(3) = New SqlParameter("@PD_KTA_Resources", dr("PD_KTA_Resources"))
                    '    paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                    '    paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                    '    paramA(6) = New SqlParameter("@RelvnceIDs", dr("PD_KTA_Relevance"))

                    '    paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    '    paramA(7).Direction = ParameterDirection.ReturnValue
                    '    If dr("Status") <> "DELETED" Then
                    '        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                    '        iReturnvalue = paramA(7).Value
                    '    End If

                    '    If iReturnvalue <> 0 Then
                    '        Exit For

                    '    End If
                    'Next
                    For iIndex = 0 To gvActions.Rows.Count - 1
                        Dim RelvIds As String = ""
                        Dim row As GridViewRow = gvActions.Rows(iIndex)
                        Dim cbl As CheckBoxList = CType(row.FindControl("cblRelevance"), CheckBoxList)
                        For i As Integer = 0 To cbl.Items.Count - 1
                            If cbl.Items(i).Selected = True Then
                                RelvIds &= cbl.Items(i).Value & "|"
                            End If
                        Next
                        If RelvIds.EndsWith("|") Then RelvIds = RelvIds.TrimEnd("|")

                        'If CType(row.FindControl("txtAction"), TextBox).Text <> Nothing And CType(row.FindControl("txtSuccess"), TextBox).Text <> Nothing Then
                        paramA(0) = New SqlParameter("@PD_KTA_Action_Step", CType(row.FindControl("txtAction"), TextBox).Text)
                        paramA(1) = New SqlParameter("@PD_KTA_Success", CType(row.FindControl("txtSuccess"), TextBox).Text)
                        paramA(2) = New SqlParameter("@PD_KTA_Timeline", CType(row.FindControl("txtTimeLine"), TextBox).Text)
                        paramA(3) = New SqlParameter("@PD_KTA_Resources", CType(row.FindControl("txtResources"), TextBox).Text)
                        paramA(4) = New SqlParameter("@PD_ACT_OTHER", "")
                        paramA(5) = New SqlParameter("@KTA_ID", ViewState("KTAA_ID"))
                        paramA(6) = New SqlParameter("@RelvnceIDs", RelvIds)

                        paramA(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        paramA(7).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_SAVE_ACTIONS", paramA)
                        iReturnvalue = paramA(7).Value
                        If iReturnvalue <> 0 Then
                            Exit For
                        End If
                        'End If
                    Next
                    'End If
                    'End If
                End If
                ''end add actions 

                If ReturnFlag = -1 Then
                    updateTransactionNew = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    updateTransactionNew = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    updateTransactionNew = "0"

                End If
            Catch ex As Exception
                updateTransactionNew = "1"
                errormsg = ex.Message
            Finally
                If updateTransactionNew = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf updateTransactionNew <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function ValidateSendToPDC() As Boolean
        ValidateSendToPDC = False
        Dim ErrorMsg As String
        'Dim SavedActionsCount As Integer
        'Using CONN As SqlConnection = ConnectionManger.GetOASISConnection
        '    Try
        '        ErrorMsg = "Please enter atleast one action before sending"
        '        SavedActionsCount = SqlHelper.ExecuteScalar(CONN, CommandType.Text, "SELECT COUNT(*) FROM PD_M.KTAA_ACTIONS_M WHERE PD_KTA_ID = " & ViewState("KTAA_ID"))
        '        If SavedActionsCount = 0 Then
        '            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
        '            Exit Function
        '        End If
        '        If SavedActionsCount = 0 Then
        '            Return False
        '        Else
        '            Return True
        '        End If
        '    Catch ex As Exception
        '        ErrorMsg = "Error occured sending the details"
        '        lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
        '        Return False
        '    End Try
        'End Using

        'Validate Key Takeaway Learnings
        If Me.txtlearning1.Text.Trim = Nothing And Me.txtlearning2.Text.Trim = Nothing And Me.txtlearning3.Text.Trim = Nothing Then
            'ErrorMsg = "Please enter atleast one Key Take-away Learning and Save as Draft before sending to PDC"
            ErrorMsg = "Please enter atleast one Key Take-away Learning"
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
            Return False
        End If

        Dim dt As DataTable = Nothing
        If Not Session("COURSEACTION") Is Nothing Then
            dt = CType(Session("COURSEACTION"), DataTable)
            Dim DataEntered As Boolean = False
            For Each row As DataRow In dt.Rows
                If row.Item("PD_KTA_ACTION_STEP") <> "" And row.Item("PD_KTA_SUCCESS") <> "" Then
                    DataEntered = True
                    Exit For
                End If
            Next
            If Not DataEntered Then
                'ErrorMsg = "Please enter atleast one Action Step and Success Criteria and Save as Draft before sending to PDC"
                ErrorMsg = "Please enter atleast one Action Step and Success Criteria"
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
                Return False
            End If
        Else
            'ErrorMsg = "Please enter atleast one Key Take-away Learning and Action Step and Success Criteria and Save as Draft before sending to PDC"
            ErrorMsg = "Please enter atleast one Key Take-away Learning, Action Step and Success Criteria"
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & ErrorMsg & "</div>"
            Return False
        End If
        Return True
    End Function

    Private Function SendtoPDC(ByRef errormsg As String) As Integer

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim KTAA_ID As String = ViewState("KTAA_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@KTAA_ID", ViewState("KTAA_ID"))

                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.UpdateKTAA_STATUS", param)
                Dim ReturnFlag As Integer = param(1).Value

                If ReturnFlag = -1 Then
                    SendtoPDC = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    SendtoPDC = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    SendtoPDC = "0"

                End If
            Catch ex As Exception
                SendtoPDC = "1"
                errormsg = ex.Message
            Finally
                If SendtoPDC = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf SendtoPDC <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function


    Sub BindCourseDetails(ByVal Id As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", Id)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_INFO_BY_CR_ID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    lblActivity.Text = Dt.Rows(0)("CM_TITLE").ToString()
                    lblCourseDate.Text = Dt.Rows(0)("EVENTDATE").ToString()
                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Sub Bind_KTAA_MASTER(ByVal CRId As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", CRId)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_KTAA_BY_REQUEST", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    txtlearning1.Text = Dt.Rows(0)("PD_KTA_LEARNIG1").ToString()
                    txtlearning2.Text = Dt.Rows(0)("PD_KTA_LEARNING2").ToString()
                    txtlearning3.Text = Dt.Rows(0)("PD_KTA_LEARNING3").ToString()
                    ViewState("KTAA_ID") = Dt.Rows(0)("PD_KTA_ID").ToString()
                    Dim PDCStatus As Integer = Convert.ToInt16(Dt.Rows(0)("PD_KTA_Status").ToString())

                    If PDCStatus > 0 Then
                        btnSave.Visible = False
                        btnSavetoPDC.Visible = False
                        'tableActions.Visible = False
                        'Me.gvCourseActions.Columns("6").Visible = False
                    Else
                        btnSavetoPDC.Visible = True
                    End If
                    If ViewState("KTAA_ID") <> "" Then
                        Bind_KTAA_ACTION_DETAILS(ViewState("KTAA_ID"))
                    Else
                        Bind_KTAA_ACTION_DETAILS(0)
                    End If
                Else
                    btnSavetoPDC.Visible = True
                    Bind_KTAA_ACTION_DETAILS(0)
                End If
            Else

            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub

    Sub Bind_KTAA_ACTION_DETAILS(ByVal KTAA_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@KTAA_ID", KTAA_ID)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_KTAA_ACTIONS_BY_KTAID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)
                BindGrid(ds)
                'gvCourseActions.DataSource = ds
                'gvCourseActions.DataBind()

                Session("COURSEACTION") = Dt
            End If

        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Private Sub resetActions()
        'txtTimeline.Text = ""
        'txtTimeLine.Text = ""
        'txtTimeline.Text = ""
        'txtTimeLine.Text = ""
    End Sub

    Private Sub BindGrid(ByVal ds As DataSet)
        If ds.Tables(0).Rows.Count = 0 Then
            Dim tmpRow As DataRow
            ds.Tables(0).Columns.Add("SNo")
            For i As Integer = 1 To 5
                tmpRow = ds.Tables(0).NewRow()
                tmpRow.Item("PD_KTA_ACTION_STEP") = ""
                tmpRow.Item("PD_KTA_SUCCESS") = ""
                tmpRow.Item("PD_KTA_TIMELINE") = ""
                tmpRow.Item("PD_KTA_RESOURCES") = ""
                tmpRow.Item("PD_KTA_RELEVANCE") = ""
                tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                'tmpRow.Item("UniqueId") = i
                tmpRow.Item("SNo") = i
                ds.Tables(0).Rows.Add(tmpRow)
            Next
            ds.AcceptChanges()
            Me.gvActions.DataSource = ds.Tables(0)
            Me.gvActions.DataBind()
        Else
            Dim dv As New DataView(ds.Tables(0), Nothing, "UniqueId Desc", DataViewRowState.CurrentRows)
            ds.Tables.RemoveAt(0)
            ds.Tables.Add(dv.ToTable)
            ds.AcceptChanges()
            ds.Tables(0).Columns.Add("SNo")
            If ds.Tables(0).Rows.Count < 5 Then
                Dim tmpRow As DataRow
                For i As Integer = ds.Tables(0).Rows.Count To 4
                    tmpRow = ds.Tables(0).NewRow()
                    tmpRow.Item("PD_KTA_ACTION_STEP") = ""
                    tmpRow.Item("PD_KTA_SUCCESS") = ""
                    tmpRow.Item("PD_KTA_TIMELINE") = ""
                    tmpRow.Item("PD_KTA_RESOURCES") = ""
                    tmpRow.Item("PD_KTA_RELEVANCE") = ""
                    tmpRow.Item("PD_KTA_RELEVANCETEXT") = ""
                    ds.Tables(0).Rows.Add(tmpRow)
                Next
                ds.AcceptChanges()
            End If
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ds.Tables(0).Rows(i).Item("SNo") = i + 1
            Next
            ds.AcceptChanges()
            Me.gvActions.DataSource = ds.Tables(0)
            Me.gvActions.DataBind()
        End If

        ''Assume we have got nothing to bind as of now. So bind 5 empty rows
        'If ds Is Nothing Then
        '    ds = New DataSet
        '    ds.Tables.Add("Actions")
        '    ds.Tables(0).Columns.Add("Actions")
        '    ds.Tables(0).Columns.Add("Success")
        '    ds.Tables(0).Columns.Add("TimeLine")
        '    ds.Tables(0).Columns.Add("Resources")
        '    ds.Tables(0).Columns.Add("Relevance")
        '    ds.AcceptChanges()
        '    Dim tmpRow As DataRow
        '    For i As Integer = 1 To 5
        '        tmpRow = ds.Tables(0).NewRow()
        '        tmpRow.Item("Actions") = ""
        '        tmpRow.Item("Success") = ""
        '        tmpRow.Item("TimeLine") = ""
        '        tmpRow.Item("Resources") = ""
        '        tmpRow.Item("Relevance") = ""
        '        ds.Tables(0).Rows.Add(tmpRow)
        '    Next
        '    ds.AcceptChanges()
        '    Me.gvActions.DataSource = ds.Tables(0)
        '    Me.gvActions.DataBind()
        'End If

    End Sub

    Protected Sub gvActions_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvActions.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cbl As CheckBoxList = CType(e.Row.FindControl("cblRelevance"), CheckBoxList)
            Dim lbl As Label = CType(e.Row.FindControl("lblRelevanceId"), Label)
            Dim lblSNo As Label = CType(e.Row.FindControl("lblSNo"), Label)
            Dim lblMandatory As Label = CType(e.Row.FindControl("lblMandatory"), Label)
            If Not lblSNo Is Nothing AndAlso CInt(lblSNo.Text) = 1 Then
                lblMandatory.Visible = True
            End If
            If Not cbl Is Nothing AndAlso Not lbl Is Nothing Then
                If Not Session("PD_Actions_Relevance") Is Nothing Then
                    With CType(Session("PD_Actions_Relevance"), DataSet)
                        cbl.DataValueField = "PD_Rlvnc_ID"
                        cbl.DataTextField = "PD_Rlvnc_DESC"
                        cbl.DataSource = .Tables(0)
                        cbl.DataBind()
                    End With
                    Dim tmpStr() As String = IIf(lbl.Text = Nothing, "", lbl.Text).ToString.Split("|")
                    For i As Integer = 0 To tmpStr.Length - 1
                        For j As Integer = 0 To cbl.Items.Count - 1
                            If tmpStr(i) = cbl.Items(j).Value Then
                                cbl.Items(j).Selected = True
                            End If
                        Next
                    Next
                End If
            End If
        End If
    End Sub
End Class
