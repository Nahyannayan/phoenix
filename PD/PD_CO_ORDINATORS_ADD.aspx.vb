﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_CO_ORDINATORS_ADD
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If ViewState("datamode") = "add" Then
                    'btnUpdatePDC.Visible = False
                    ViewState("PDC_ID") = 0
                ElseIf ViewState("datamode") = "edit" Then
                    'btnAddPDC.Visible = False
                    'btnUpdatePDC.Visible = True

                    ViewState("PDC_ID") = Encr_decrData.Decrypt(Request.QueryString("pdcId").Replace(" ", "+"))
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD00089") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    '' ViewState("SEMP_ID") = 0

                    bindBusinessUnits()

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights                   

                    ' h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
      
                Dim lblEMPID As New Label
                Dim lblBsu_id As New Label
                Dim EmpNo As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMP_ID"), Label)
        lblBsu_id = TryCast(sender.FindControl("lblBsu_id"), Label)
        EmpNo = TryCast(sender.FindControl("lblEmp_No"), Label)
                ViewState("EMP_ID") = lblEMPID.Text
                Dim PDC_bsu_id As String
        PDC_bsu_id = ddlBusinessunit.SelectedValue
        Dim errormsg As String
        Dim callTransaction As Integer

        Dim tran As SqlTransaction

        Dim PDC_ID As String = ViewState("PDC_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter
                'param(0) = New SqlParameter("@PDC_ID", 0)

                param(0) = New SqlParameter("@PDC_EMP_ID", lblEMPID.Text.Trim)
                param(1) = New SqlParameter("@EMP_BSU_ID", lblBsu_id.Text.Trim)
                param(2) = New SqlParameter("@PDC_BSU_ID", ddlBusinessunit.SelectedValue)

                'Dim bsuId As String = ddlBusinessunit.SelectedValue


                param(3) = New SqlParameter("@PDC_EMP_NO", EmpNo.Text.Trim)
               

                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_PD_CO_ORDINATORS", param)
                Dim ReturnFlag As Integer = param(4).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"


                    'ResetAll()
                End If
            Catch ex As Exception

                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If

                If errormsg <> "" Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
                Else
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                End If
            End Try

        End Using

    End Sub
    
    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click



        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else


            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
           
        End If
    End Sub
    

    Protected Sub btnSearchEMP_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindGemsStaffListForPDC()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindGemsStaffListForPDC()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindGemsStaffListForPDC()
    End Sub
    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Try
            Dim BSU_ID As String
            BSU_ID = ddlBusinessunit.SelectedValue


            BindGemsStaffListForPDC()


        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        BindGemsStaffListForPDC()
    End Sub

    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@UserID", Session("sUsr_id"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GETBSU_BY_USER_ACCES_BSU", param)



        ddlBusinessunit.DataSource = ds
        ddlBusinessunit.DataTextField = "bsu_name"
        ddlBusinessunit.DataValueField = "bsu_id"
        ddlBusinessunit.DataBind()
        ddlBusinessunit.Items.Insert(0, New ListItem("-Select School-", "0"))
        ddlBusinessunit_SelectedIndexChanged(ddlBusinessunit, Nothing)
    End Sub

    Sub BindGemsStaffListForPDC()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter
        Dim str_EMP_NO As String = String.Empty
        Dim str_EMP_Name As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim EMP_NO As String = String.Empty
        Dim EMP_Name As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Try
            If gvPDC.Rows.Count > 0 Then

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NO")

                If txtSearch.Text.Trim <> "" Then
                    EMP_NO = " AND replace(EMPNO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_EMP_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMP_FNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(EMP_LNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"


                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvPDC.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BSU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                

                str_BSU_Name = txtSearch.Text.Trim
            End If

            End If
            FILTER_COND = EMP_NO + EMP_Name + BSU_Name

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Gems_Employee_For_PDC", param)

            If ds.Tables(0).Rows.Count > 0 Then

                gvPDC.DataSource = ds.Tables(0)
                gvPDC.DataBind()
            Else


                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvPDC.DataSource = ds.Tables(0)
                Try
                    gvPDC.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "No Records vailable !!!"
            End If
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Private Function callTransaction(ByRef errormsg As String) As Integer


        Dim tran As SqlTransaction

        Dim SVG_ID As String = ViewState("SVG_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Dim EMP_IDs As String = String.Empty
            Dim IsNewGroup As Boolean = False

            Dim ReturnFlag As Integer
            Dim param(4) As SqlParameter
            Try



                For Each gvrow As GridViewRow In gvPDC.Rows
                    Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
                    If chk IsNot Nothing And chk.Checked Then
                        EMP_IDs += gvPDC.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

                    End If
                Next

                ''checking if seat is available

                EMP_IDs = EMP_IDs.Trim(",".ToCharArray())
                param(0) = New SqlParameter("@PDC_EMP_IDs", EMP_IDs)
                param(1) = New SqlParameter("@PDC_BSU_ID", ddlBusinessunit.SelectedValue)

                param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_PD_CO_ORDINATORS_BULK", param)
                ReturnFlag = param(2).Value
                callTransaction = ReturnFlag
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message

            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()


                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()

                Else
                    errormsg = ""
                    tran.Commit()
                End If

            End Try
        End Using
    End Function
End Class
