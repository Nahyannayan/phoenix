﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class PD_PD_Course_PArticipants_Report
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div class='alert alert-success'>Record updated successfully !!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD000019") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("CM_ID") = 0

                    bindCourses()
                    '' bindCourseList()

                    tblGridData.Visible = False

                    'Dim IsTRainer As Boolean = Check_PD_Trainer()

                    'If Not IsTRainer Then
                    '    tblNoAccess.Visible = True
                    '    tblData.Visible = False
                    '    Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                    '    lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                    'End If
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try
        Else
            'bindSelectedCourseList(ddlCourse.SelectedValue)
            bindSelectedCourseList(hdnSelectedCourse.Value)
        End If


    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        bindParticipantsGrid()

    End Sub

    Protected Sub gvCourseList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCourseList.PageIndex = e.NewPageIndex
        bindSelectedCourseList(hdnSelectedCourse.Value)
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click

        ParticipantsExcelDownload()

    End Sub

    Protected Sub lbtn_Excel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If hdnSelected.Value <> "" Then
            ParticipantsExcelDownloadByEmpId(hdnSelected.Value)
        Else
            ParticipantsExcelDownload()
        End If
    End Sub

    Protected Sub btnSearchEMP_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindParticipantsGrid()
        For Each li As ListItem In chkCourses.Items
            li.Attributes.Add("someValue", li.Value)
        Next
    End Sub

    Protected Sub imgParticipants_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Dim CourseId1 As String = String.Empty

        'If chkAll.Checked Then
        '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1

        '        If chkCourses.Items(i).Selected = True Then
        '            CourseId1 &= chkCourses.Items(i).Value & "|"
        '        End If

        '    Next

        'Else
        '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
        '        If chkCourses.Items(i).Selected = True Then
        '            CourseId1 &= chkCourses.Items(i).Value & "|"
        '        End If
        '    Next
        'End If
        'imgParticipants.OnClientClick = "javascript:ParticipantsList('" + CourseId1 + "');return true;"

        For Each li As ListItem In chkCourses.Items
            li.Attributes.Add("someValue", li.Value)
        Next
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim CourseId As String = String.Empty

        'If chkAll.Checked Then
        '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1

        '        CourseId &= chkCourses.Items(i).Value & "|"

        '    Next

        'Else
        '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
        '        If chkCourses.Items(i).Selected = True Then
        '            CourseId &= chkCourses.Items(i).Value & "|"
        '        End If
        '    Next
        'End If
        CourseId = hdnSelectedCourse.Value
        lnkDelete.OnClientClick = "javascript:ParticipantsList('" + CourseId + "');return false;"
    End Sub

    Protected Sub txtCourses_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtCourses.Text.Trim.Length >= 1 Then

                bindSelectedCourseList(hdnSelectedCourse.Value)
                txtCourses.Text = "You have selected courses"
                'txtCourses.Visible = False
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lbtnRemoveCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCMID As New Label
        lblCMID = TryCast(sender.FindControl("lblCM_ID"), Label)
        Dim strCMId As String = hdnSelectedCourse.Value
        If lblCMID.Text <> "" Then
            Dim output As String = strCMId.Replace(lblCMID.Text, String.Empty)
            hdnSelectedCourse.Value = output.ToString
            bindSelectedCourseList(output.ToString())
        End If


        If hdnSelectedCourse.Value = "" Then
            txtCourses.Text = ""
        End If
    End Sub
    Protected Sub txtParticipants_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtParticipants.Text.Trim.Length >= 1 Then

                bindParticipantsGridByEmpId(txtParticipants.Text)
                txtParticipants.Text = "You have selected participants"
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gvParticipants_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipants.PageIndex = e.NewPageIndex
        bindParticipantsGrid()
        For Each li As ListItem In chkCourses.Items
            li.Attributes.Add("someValue", li.Value)
        Next
    End Sub

    Sub bindCourses()
        ddlCourse.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try
            param(0) = New SqlClient.SqlParameter("@Emp_id", Session("EmployeeId"))
            param(1) = New SqlClient.SqlParameter("@BSU_ID", ViewState("BSU_ID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_BY_BSU", param)

            If ds IsNot Nothing Then
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlCourse.DataSource = ds
                        ddlCourse.DataTextField = "CM_TITLE"
                        ddlCourse.DataValueField = "CM_ID"
                        ddlCourse.DataBind()
                    End If

                End If
            End If

           
        Catch ex As Exception

        End Try

    End Sub

    

    Sub bindCourseList()
        chkCourses.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try
            param(0) = New SqlClient.SqlParameter("@Emp_id", Session("EmployeeId"))
            param(1) = New SqlClient.SqlParameter("@BSU_ID", ViewState("BSU_ID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_BY_BSU", param)
            Me.chkCourses.DataValueField = "CM_ID"
            Me.chkCourses.DataTextField = "CM_TITLE"
            Me.chkCourses.DataSource = ds
            Me.chkCourses.DataBind()

            For Each li As ListItem In chkCourses.Items
                li.Attributes.Add("someValue", li.Value)
            Next


        Catch ex As Exception

        End Try

    End Sub

    Private Sub bindParticipantsGrid()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(4) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable


            If gvParticipants.Rows.Count > 0 Then

                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_NO")

                If txtSearch.Text.Trim <> "" Then
                    EMP_NO = " AND replace(EMPNO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_EMP_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMP_FNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(EMP_LNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"




                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvParticipants.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BSU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "




                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = EMP_NO + EMP_Name + BSU_Name
            Dim CourseIds As String = String.Empty

            'If chkAll.Checked Then
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If
            '    Next

            'Else
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If
            '    Next
            'End If
            ''commented on 22apr2014 to make course into single 
            'CourseIds = hdnSelectedCourse.Value
            'CourseIds = ddlCourse.SelectedValue
            CourseIds = Replace(hdnSelectedCourse.Value, "||", "|")
            'If CourseIds.EndsWith("|") Then
            '    CourseIds = CourseIds.TrimEnd("|")
            'End If

            param(0) = New SqlClient.SqlParameter("@CM_IDs", CourseIds)

            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)
            param(2) = New SqlClient.SqlParameter("@Attendance", "P")
            param(3) = New SqlClient.SqlParameter("@EmpID", Session("EmployeeId"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List_For_Course", param)

            If ds.Tables.Count <> 0 Then
                If ds.Tables(0).Rows.Count = 0 Then
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    gvParticipants.DataSource = ds
                    gvParticipants.DataBind()
                    Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                    gvParticipants.Rows(0).Cells.Clear()
                    gvParticipants.Rows(0).Cells.Add(New TableCell)
                    gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
                    tblGridData.Visible = False
                Else
                    gvParticipants.DataSource = ds
                    gvParticipants.DataBind()
                    tblGridData.Visible = True

                End If
            Else
                lblError.InnerText = "No Records Available"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ParticipantsExcelDownload()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
           
            FILTER_COND = EMP_NO + EMP_Name + BSU_Name

            Dim CourseIds As String = String.Empty

            'If chkAll.Checked Then
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If

            '    Next

            'Else
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If
            '    Next
            'End If

            ''commented on 22apr2014 to make single selection of course
            ''CourseIds = hdnSelectedCourse.Value
            'CourseIds = ddlCourse.SelectedValue
            CourseIds = Replace(hdnSelectedCourse.Value, "||", "|")
            If CourseIds.EndsWith("|") Then
                CourseIds = CourseIds.TrimEnd("|")
            End If


            param(0) = New SqlClient.SqlParameter("@CM_IDs", CourseIds)



            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)
            ''added by nahyann on 20Oct2015 
            param(2) = New SqlClient.SqlParameter("@EmpID", Session("EmployeeId"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List_For_Course", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                
            Else
                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")

                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")

                Dim strFilename As String = dtEXCEL.Rows(0)("CM_TITLE").ToString()
                strFilename = strFilename.Replace("/", "").Replace("\", "").Replace(":", "").Replace(";", "").Replace(",", "").Replace("?", "")
                ''  strFilename = RemoveSpecialCharacters(strFilename)
                Dim stuFilename As String = "Participants_For_" & strFilename & ".xlsx"
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ' ws.HeadersFooters.AlignWithMargins = True
                ''removing unwanted columns
                ''commented and qadded by nahyan on 19apr2016 for new gembox spreadsheet
                'ws.Columns("A").Delete()
                ws.Columns.Remove(0)
                ws.Columns.Remove(3)

                ws.Columns.Remove(11)
                ''ws.Columns("C").Delete()



                ws.Cells(0, 0).Value = "Title"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 10000

                ws.Cells(0, 1).Value = "Business Unit"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "Employee No"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "Name"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 7000
                
                ws.Cells(0, 4).Value = "Email"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 7000

                ws.Cells(0, 5).Value = "Phone"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "Designation"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("G").Width = 7000

                ws.Cells(0, 7).Value = "Attendance"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("H").Width = 7000

                ws.Cells(0, 8).Value = "EVAL Submitted"
                ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("I").Width = 7000

                ''included by nahyan on 8Oct2018
                ws.Cells(0, 9).Value = "Course Value"
                ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("J").Width = 7000
                ws.Columns("J").Style.NumberFormat = "@"
                ''included by nahyan on 31Oct2018
                ws.Cells(0, 10).Value = "COURSE DATE"
                ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("K").Width = 7000

                'ws.Cells(0, 9).Value = "KTAA Submitted"
                'ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("J").Width = 7000



                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()
                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()

            End If
        Catch ex As Exception

        End Try
    End Sub

   
        Public Shared Function RemoveSpecialCharacters(str As String) As String
            Dim sb As New StringBuilder()
            For Each c As Char In str
            If (c >= "0"c AndAlso c <= "9"c) OrElse (c >= "A"c AndAlso c <= "Z"c) OrElse (c >= "a"c AndAlso c <= "z"c) Then
                sb.Append(c)
            End If
            Next
            Return sb.ToString()
        End Function

    Private Sub ParticipantsExcelDownloadByEmpId(ByVal EmpIDs As String)

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet

            If EmpIDs <> "" Then
                FILTER_COND = " AND EM.EMP_ID IN (" & EmpIDs.Replace("||", "','") & ")"
            End If

            Dim CourseIds As String = String.Empty

            'If chkAll.Checked Then
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1

            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If

            '    Next

            'Else
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If
            '    Next
            'End If
            ''commetned on 22apr2014 to make single selection of course
            ''CourseIds = hdnSelectedCourse.Value
            'CourseIds = ddlCourse.SelectedValue
            CourseIds = Replace(hdnSelectedCourse.Value, "||", "|")
            If CourseIds.EndsWith("|") Then
                CourseIds = CourseIds.TrimEnd("|")
            End If


            param(0) = New SqlClient.SqlParameter("@CM_IDs", CourseIds)

            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)
            ''added by nahyann on 20Oct2015 
            param(2) = New SqlClient.SqlParameter("@EmpID", Session("EmployeeId"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List_For_Course", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else
                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)

                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")

                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim strFilename As String = dtEXCEL.Rows(0)("CM_TITLE").ToString()
                strFilename = strFilename.Replace("/", "").Replace("\", "").Replace(":", "").Replace(";", "").Replace(",", "").Replace("?", "")
                ''  strFilename = RemoveSpecialCharacters(strFilename)
                Dim stuFilename As String = "Participants_For_" & strFilename & ".xlsx"

                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                ''removing unwanted columns
                'ws.Columns("A").Delete()
                'ws.Columns("C").Delete()
                ws.Columns.Remove(1)
                ws.Columns.Remove(3)
                ws.Columns.Remove(11)


                ws.Cells(0, 0).Value = "Title"
                ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("A").Width = 10000

                ws.Cells(0, 1).Value = "Business Unit"
                ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("B").Width = 4000

                ws.Cells(0, 2).Value = "Employee No"
                ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("C").Width = 5000

                ws.Cells(0, 3).Value = "Name"
                ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("D").Width = 7000

                ws.Cells(0, 4).Value = "Email"
                ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("E").Width = 7000

                ws.Cells(0, 5).Value = "Phone"
                ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 6).Value = "Designation"
                ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("F").Width = 7000

                ws.Cells(0, 7).Value = "Attendance"
                ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                ws.Columns("G").Width = 7000

                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                End If

                ef.Save(cvVirtualPath & pathSave)
                Dim path = cvVirtualPath & pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bindParticipantsGridByEmpId(ByVal EmpIDs As String)

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_EMP_NO As String = String.Empty
            Dim str_EMP_Name As String = String.Empty
            Dim str_BSU_Name As String = String.Empty
            Dim EMP_NO As String = String.Empty
            Dim EMP_Name As String = String.Empty
            Dim BSU_Name As String = String.Empty
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable


            If EmpIDs <> "" Then
                FILTER_COND = " AND EM.EMP_ID IN (" & EmpIDs.Replace("||", "','") & ")"
            End If

            Dim CourseIds As String = String.Empty

            'If chkAll.Checked Then
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1

            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If

            '    Next

            'Else
            '    For i As Integer = 0 To Me.chkCourses.Items.Count - 1
            '        If chkCourses.Items(i).Selected = True Then
            '            CourseIds &= chkCourses.Items(i).Value & "|"
            '        End If
            '    Next
            'End If
            ''commented on 22apr2014 
            ''CourseIds = hdnSelectedCourse.Value
            'CourseIds = ddlCourse.SelectedValue
            CourseIds = Replace(hdnSelectedCourse.Value, "||", "|")
            If CourseIds.EndsWith("|") Then
                CourseIds = CourseIds.TrimEnd("|")
            End If

            param(0) = New SqlClient.SqlParameter("@CM_IDs", CourseIds)

            param(1) = New SqlClient.SqlParameter("@FILTER_COND", FILTER_COND)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List_For_Course", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                Dim columnCount As Integer = gvParticipants.Rows(0).Cells.Count
                gvParticipants.Rows(0).Cells.Clear()
                gvParticipants.Rows(0).Cells.Add(New TableCell)
                gvParticipants.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipants.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipants.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
                tblGridData.Visible = False
            Else
                gvParticipants.DataSource = ds
                gvParticipants.DataBind()
                tblGridData.Visible = True

            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub bindSelectedCourseList(ByVal CM_IDs As String)


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim CM_IDfilter As String = String.Empty
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter
        Try

            param(0) = New SqlClient.SqlParameter("@CM_IDs", CM_IDs)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_COURSE_BY_IDs", param)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
                Dim columnCount As Integer = gvCourseList.Rows(0).Cells.Count
                gvCourseList.Rows(0).Cells.Clear()
                gvCourseList.Rows(0).Cells.Add(New TableCell)
                gvCourseList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCourseList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCourseList.Rows(0).Cells(0).Text = "Currently there is no courses Exists"
            Else
                gvCourseList.DataSource = ds
                gvCourseList.DataBind()
            End If


        Catch ex As Exception

        End Try

    End Sub
End Class
