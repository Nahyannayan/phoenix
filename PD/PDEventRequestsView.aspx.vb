Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Partial Class PD_PDEventRequestsView
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim ddlGroup As Object

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    

    Private Sub BindTrainerGroups()
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@INFO_TYPE", "GROUP")
            params(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(2) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(3) = New SqlParameter("@Usr_ID", Session("sUsr_id"))
            params(4) = New SqlParameter("@STUDENT_GROUP_ID", -1)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_TRAINER_ATTENDANCE_GROUP", params)
            Me.ddlGroup.DataTextField = "SGM_DESCR"
            Me.ddlGroup.DataValueField = "SGM_ID"
            Me.ddlGroup.DataSource = ds
            Me.ddlGroup.DataBind()
            'Me.ddlGroup_SelectedIndexChanged(Me.ddlGroup, Nothing)
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting group information"
        End Try
    End Sub

    Private Sub BindTrainerGroupAttendanceStudents()
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@INFO_TYPE", "STUDENTS")
            params(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(2) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(3) = New SqlParameter("@Usr_ID", Session("sUsr_id"))
            Dim GroupId As Integer = Me.ddlGroup.SelectedValue
            params(4) = New SqlParameter("@Student_Group_ID", GroupId)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_TRAINER_ATTENDANCE_GROUP", params)
            Me.gvRequest.DataSource = ds.Tables(0)
            Me.gvRequest.DataBind()
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting student list for the selected group"
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Me.txtBusinessUnit.Text = IIf(Encr_decrData.Decrypt(Request.QueryString("BsuName").Replace(" ", "+")) = "[SELECT]", "All", Encr_decrData.Decrypt(Request.QueryString("BsuName").Replace(" ", "+")))
                    Me.txtRegion.Text = IIf(Encr_decrData.Decrypt(Request.QueryString("Region").Replace(" ", "+")) = "[SELECT]", "All", Encr_decrData.Decrypt(Request.QueryString("Region").Replace(" ", "+")))
                    Me.txtLocation.Text = IIf(Encr_decrData.Decrypt(Request.QueryString("Location").Replace(" ", "+")) = "[SELECT]", "All", Encr_decrData.Decrypt(Request.QueryString("Location").Replace(" ", "+")))
                    Me.txtFromDate.Text = Encr_decrData.Decrypt(Request.QueryString("FromDate").Replace(" ", "+"))
                    Me.txtToDate.Text = Encr_decrData.Decrypt(Request.QueryString("ToDate").Replace(" ", "+"))
                    Me.txtTitle.Text = Encr_decrData.Decrypt(Request.QueryString("Title").Replace(" ", "+"))
                    Me.hfBsuId.Value = Encr_decrData.Decrypt(Request.QueryString("BsuId").Replace(" ", "+"))
                    If Me.hfBsuId.Value = "" Then
                        Me.hfBsuId.Value = Bind_BsuIds()
                    End If

                    Me.hfRegionId.Value = Encr_decrData.Decrypt(Request.QueryString("RegionId").Replace(" ", "+"))
                    Me.hfLocationId.Value = Encr_decrData.Decrypt(Request.QueryString("LocationId").Replace(" ", "+"))
                    Me.hfCmId.Value = Encr_decrData.Decrypt(Request.QueryString("CmId").Replace(" ", "+"))
                    Me.hfEmpId.Value = Encr_decrData.Decrypt(Request.QueryString("EmpId").Replace(" ", "+"))

                    Me.optPending.Checked = True
                    Me.gvRequest.Columns(16).Visible = False
                    Me.gvRequest.Columns(15).Visible = True
                    ltCancel.Visible = False
                    Me.SearchData()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        Else

        End If
    End Sub

    Sub bindMonthPRESENT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2,ISNULL(BSU_bAPP_LEAVE_ABSENT,'False') AS bAPP_LEAVE_ABS " _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
                ViewState("bAPP_LEAVE_ABS") = ds.Tables(0).Rows(0)(2)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnAdd.Click
        Try
            Me.hfBsuId.Value = "Add"
            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Else
                'If ddlGroup.SelectedIndex = -1 Then
                '    lblError.Text = "Group not selected"
                'Else
                '    If ValidateDate() = "0" Then
                '        'Call Add_clicked()
                '        'Call backGround()
                '        Me.BindTrainerGroupAttendanceStudents()
                '        If Me.gvRequest.Rows.Count > 0 Then
                '            Me.btnSearch.Visible = True
                '            Me.Button3.Visible = True
                '        Else
                '            Me.btnSearch.Visible = False
                '            Me.Button3.Visible = False
                '        End If
                '    End If
                'End If
            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub

    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtFromDate.Text <> "" Then
                Dim strfDate As String = txtFromDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"
            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If

            Return ErrorStatus
        Catch ex As Exception
            'UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function

    Sub Add_clicked()
        'Dim ACD_ID As String = ddlRegion.SelectedItem.Value
        'Dim totRow As Integer
        'Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
        'Dim GRD_ID As String = ddlLocation.SelectedValue

        'Dim EMP_ID As String = ViewState("EMP_ID")


        'Dim AttDate As String

        'AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)


        'Try
        '    Dim DupCount As Integer = RecordCount(ACD_ID, SGR_ID, AttDate)

        '    If DupCount = 0 Then
        '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        '        Dim str_Sql As String

        '        str_Sql = "SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID='" & SGR_ID & "' AND SSD_ACD_ID='" & ACD_ID & "' AND SSD_GRD_ID='" & GRD_ID & "'"

        '        totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        '        If totRow > 0 Then
        '            ViewState("datamode") = "add"


        '            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '            Call BindAdd_Attendance()
        '            setControl()
        '        Else

        '            lblError.Text = "Record currently not updated. Please Contact System Admin"
        '        End If
        '    Else

        '        lblError.Text = "Attendances already marked for the given date & Group!!!"
        '    End If

        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message)
        'End Try

    End Sub


    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim RAL_ID As New DataColumn("RAL_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))
            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))
            Dim ContAbs As New DataColumn("ContAbs", System.Type.GetType("System.String"))

            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(RAL_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(MinList)
            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            dtDt.Columns.Add(ContAbs)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    If ddlGroup.SelectedIndex = -1 Then
    '        lblError.Text = "Group not selected"
    '    Else
    '        If ValidateDate() = "0" Then
    '            'Call Edit_clicked()
    '            'Call backGround()
    '            Me.GetMarkedAttendance()
    '        End If
    '    End If
    'End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles Button3.Click
        Try

            'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            '    'clear the textbox and set the default settings
            '    ViewState("datamode") = "view"
            '    Session("dt_ATT_GROUP").Rows.Clear()
            '    gvInfo.DataSource = Session("dt_ATT_GROUP")
            '    gvInfo.DataBind()

            '    ' gvInfo.Visible = False
            '    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            '    btnSave2.Visible = False
            '    btnCancel.Visible = False
            '    btnCancel2.Visible = False
            '    ResetControl()
            'End If
            Me.txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Now.Date)

            Me.gvRequest.DataSource = Nothing
            Me.gvRequest.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    'Sub setControl()
    '    ddlRegion.Enabled = False
    '    ddlSubjectGroup.Enabled = False
    '    ddlLocation.Enabled = False
    '    ddlSubject.Enabled = False
    '    ddlPERIOD.Enabled = False
    '    imgCalendar.Visible = False
    '    txtFromDate.Enabled = False
    'End Sub

    'Sub ResetControl()
    '    ddlRegion.Enabled = True
    '    ddlSubjectGroup.Enabled = True
    '    ddlLocation.Enabled = True
    '    ddlSubject.Enabled = True
    '    ddlPERIOD.Enabled = True
    '    imgCalendar.Visible = True
    '    txtFromDate.Enabled = True
    'End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True

        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub


    Private Function Bind_BsuIds() As String
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Dim param(1) As SqlParameter

            param(0) = New SqlParameter("@usr_id", Session("sUsr_Id"))
            param(1) = New SqlParameter("@LOGGED_IN_BSU_ID", Session("sBsuId"))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[PD_M].[Get_PDC_BusinessUnit]", param)
            Dim dt As New DataTable
            Dim bsuIDs As String = String.Empty
            If Not ds Is Nothing Then
                dt = ds.Tables(0)
                For Each row As DataRow In dt.Rows
                    bsuIDs &= row.Item("Id") & "|"
                Next row

            End If
            Return bsuIDs
        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading business units"
        End Try
    End Function
    



    

    

    Private Sub GetMarkedAttendance()
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(2) = New SqlParameter("@Student_Group_ID", Me.ddlGroup.SelectedValue)
            params(3) = New SqlParameter("@From_Date", String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_MARKED_ATTENDANCE", params)
            Me.gvRequest.DataSource = ds.Tables(0)
            Me.gvRequest.DataBind()
            
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting attendance record for the selected group"
        End Try
    End Sub

    Private Sub Bind_Bsu()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT '' AS id, '[SELECT]' AS NAME UNION  SELECT bsu.BSU_ID AS id, bsu.BSU_NAME AS name FROM dbo.BUSINESSUNIT_M bsu Where bsu_id in (select pdc_bsu_id from pd_co_ordinators where pdc_emp_id =" & " )) t ORDER BY NAME"

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter("@usr_id", Session("sUsr_Id"))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[PD_M].[Get_PDC_BusinessUnit]", param)

            If Not ds Is Nothing Then
                'ddlBusinessUnit.DataSource = ds.Tables(0)
                'ddlBusinessUnit.DataValueField = "Id"
                'ddlBusinessUnit.DataTextField = "Name"
                'ddlBusinessUnit.DataBind()
                'ddlBusinessUnit.SelectedValue = Session("sBsuId")
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading business units"
        End Try
    End Sub

    Private Sub Bind_Region()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT R.R_ID AS id, R.R_REGION_NAME AS name FROM PD_M.REGION R) t ORDER BY NAME"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                'ddlRegion.DataSource = ds.Tables(0)
                'ddlRegion.DataValueField = "Id"
                'ddlRegion.DataTextField = "Name"
                'ddlRegion.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading region"
        End Try
    End Sub

    Private Sub Bind_Location()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT l.L_ID AS id, l.L_DESCR AS name FROM PD_M.LOCATION L) t ORDER BY NAME"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                'ddlLocation.DataSource = ds.Tables(0)
                'ddlLocation.DataValueField = "Id"
                'ddlLocation.DataTextField = "Name"
                'ddlLocation.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading location"
        End Try
    End Sub

    Private Sub SearchData()

        Dim Sgm_Id, Stu_Id, Apd_Id, From_Date, To_Date, Remarks, Ssa_Id, status As String
        Dim ddlStatus As DropDownList

        If txtFromDate.Text.Contains("/") Then
            From_Date = txtFromDate.Text.Split("/")(2) & "-" & txtFromDate.Text.Split("/")(1) & "-" & txtFromDate.Text.Split("/")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("/")(2) & "-" & txtToDate.Text.Split("/")(1) & "-" & txtToDate.Text.Split("/")(0) & " 23:59:59"
        ElseIf txtFromDate.Text.Contains("-") Then
            From_Date = txtFromDate.Text.Split("-")(2) & "-" & txtFromDate.Text.Split("-")(1) & "-" & txtFromDate.Text.Split("-")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("-")(2) & "-" & txtToDate.Text.Split("-")(1) & "-" & txtToDate.Text.Split("-")(0) & " 23:59:59"
        ElseIf txtFromDate.Text <> Nothing And txtToDate.Text <> Nothing Then
            From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text) & " 00:00:01"
            To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text) & " 23:59:59"
        End If

        'From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
        'To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text)

        Dim transaction As SqlTransaction

        Dim Conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand


        Try

            Dim params(9) As SqlParameter
            params(0) = New SqlParameter("@bsu_id", Me.hfBsuId.Value)
            params(1) = New SqlParameter("@region", Me.hfRegionId.Value)
            params(2) = New SqlParameter("@location", Me.hfLocationId.Value)
            params(3) = New SqlParameter("@from_date", From_Date)
            params(4) = New SqlParameter("@to_date", To_Date)
            params(5) = New SqlParameter("@title", Me.txtTitle.Text)

            If Me.optPending.Checked Then
                params(6) = New SqlParameter("@status", "P")
            ElseIf Me.optApproved.Checked Then
                params(6) = New SqlParameter("@status", "A")
            ElseIf Me.optRejected.Checked Then
                params(6) = New SqlParameter("@status", "R")
            ElseIf Me.optCancelled.Checked Then
                params(6) = New SqlParameter("@status", "C")
            End If

            params(7) = New SqlParameter("@CM_ID", Me.hfCmId.Value)
            params(8) = New SqlParameter("@Emp_ID", Me.hfEmpId.Value)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Conn, CommandType.StoredProcedure, "GET_COURSE_REQUESTS", params)

            If Not ds Is Nothing Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("Sr_No") = i + 1
                    i += 1
                Next
                ds.AcceptChanges()
                Me.gvRequest.DataSource = ds.Tables(0)
                Me.gvRequest.DataBind()
                Session("PDEventRequestsView") = ds.Tables(0)
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error Occured While loading the data"
        Finally

        End Try

    End Sub

    Protected Sub optPending_CheckedChanged(sender As Object, e As System.EventArgs) Handles optPending.CheckedChanged
        If Me.optPending.Checked Then
            Me.gvRequest.Columns(13).Visible = False
            Me.gvRequest.Columns(14).Visible = True
            Me.gvRequest.Columns(15).Visible = True
            Me.gvRequest.Columns(16).Visible = False
            'Me.gvRequest.Columns(17).Visible = False
            ltCancel.Visible = False
            Me.SearchData()
        End If
    End Sub

    Protected Sub optApproved_CheckedChanged(sender As Object, e As System.EventArgs) Handles optApproved.CheckedChanged
        If Me.optApproved.Checked Then
            Me.gvRequest.Columns(13).Visible = False
            Me.gvRequest.Columns(14).Visible = False
            Me.gvRequest.Columns(15).Visible = False
            Me.gvRequest.Columns(16).Visible = True
            'Me.gvRequest.Columns(17).Visible = True
            ltCancel.Visible = True
            Me.SearchData()
        End If
    End Sub

    Protected Sub optRejected_CheckedChanged(sender As Object, e As System.EventArgs) Handles optRejected.CheckedChanged
        If Me.optRejected.Checked Then
            Me.gvRequest.Columns(13).Visible = False
            Me.gvRequest.Columns(14).Visible = False
            Me.gvRequest.Columns(15).Visible = False
            Me.gvRequest.Columns(16).Visible = False
            'Me.gvRequest.Columns(17).Visible = False
            ltCancel.Visible = False
            Me.SearchData()
        End If

    End Sub

    Protected Sub optCancelled_CheckedChanged(sender As Object, e As System.EventArgs) Handles optCancelled.CheckedChanged
        If Me.optCancelled.Checked Then
            Me.gvRequest.Columns(13).Visible = False
            Me.gvRequest.Columns(14).Visible = False
            Me.gvRequest.Columns(15).Visible = False
            Me.gvRequest.Columns(16).Visible = False
            'Me.gvRequest.Columns(17).Visible = False
            ltCancel.Visible = False
            Me.SearchData()
        End If

    End Sub

    'Public Sub lblApprove()
    '    Me.ApproveOrReject("A")
    'End Sub

    'Public Sub lblReject()
    '    Me.ApproveOrReject("R")
    'End Sub

    Public Sub ApproveOrReject(ByVal Id As Integer, Status As String)
        'Dim lblId As Label
        Dim Con As SqlConnection
        Dim tr As SqlTransaction

        Try
            'lblId = CType(Me.gvRequest.Rows(RowIndex).FindControl("lblID"), Label)
            Con = ConnectionManger.GetOASISConnection



            Dim str2 As String = "SELECT  COUNT(*) AS 'OPENONLINE'  FROM PD_T.COURSE_REQ CR INNER JOIN PD_M.COURSE_M CM ON CR.CR_CM_ID = CM.CM_ID WHERE  CR.CR_CM_ID = (SELECT CR.CR_CM_ID FROM PD_T.COURSE_REQ CR WHERE CR.CR_ID = " & Id & ")  AND  cast(CM_CLOSE_ONLINE_DT as date) >= cast(GETDATE() as date) "
            With SqlHelper.ExecuteReader(Con, CommandType.Text, str2)
                Do While .Read
                    If .Item("OPENONLINE") = 0 Then
                        Me.lblError.Text = "The registration is currently closed"
                        Exit Sub
                    End If
                Loop
                .Close()
            End With


            If Status = "A" Then 'validation for max capacity field for the course
                Dim str As String = "SELECT  COUNT(*) AS 'Approved' , CM.CM_MAX_CAPACITY AS 'Max_Capacity' FROM PD_T.COURSE_REQ CR INNER JOIN PD_M.COURSE_M CM ON CR.CR_CM_ID = CM.CM_ID WHERE  CR.CR_CM_ID = (SELECT CR.CR_CM_ID FROM PD_T.COURSE_REQ CR WHERE CR.CR_ID = " & Id & ") AND CR.CR_APPR_STATUS = 'A' GROUP BY CM.CM_ID , CM.CM_MAX_CAPACITY"
                With SqlHelper.ExecuteReader(Con, CommandType.Text, str)
                    Do While .Read
                        If .Item("Approved") >= .Item("Max_Capacity") Then
                            Me.lblError.Text = "This course has already reached its max capacity level"
                            Exit Sub
                        End If
                    Loop
                    .Close()
                End With
            End If

            Dim params(2) As SqlParameter
            params(0) = New SqlParameter("@id", Id)
            params(1) = New SqlParameter("@status", Status)
            params(2) = New SqlParameter("@usr_id", Session("sUsr_Id"))

            tr = Con.BeginTransaction
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "COURSE_REQUESTS_APPROVAL", params)

            Dim params1(1) As SqlParameter
            params1(0) = New SqlParameter("@id", Id)
            params1(1) = New SqlParameter("@status", Status)
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "SEND_PD_COURSE_APPROVAL_EMAIL", params1)
            ' tr.Rollback()
            tr.Commit()
            Me.lblError.Text = "Request updated successfully"
        Catch ex As Exception
            Me.lblError.Text = "Error occured while trying to update the request"
            If Not tr Is Nothing Then tr.Rollback()
        Finally

        End Try

    End Sub

    Public Sub CancelRequest(ByVal Id As Integer, ByVal Status As String)
        'Dim lblId As Label
        Dim Con As SqlConnection
        Dim tr As SqlTransaction

        Try
            'lblId = CType(Me.gvRequest.Rows(RowIndex).FindControl("lblID"), Label)
            Con = ConnectionManger.GetOASISConnection

            'If Status = "A" Then 'validation for max capacity field for the course
            '    Dim str As String = "SELECT  COUNT(*) AS 'Approved' , CM.CM_MAX_CAPACITY AS 'Max_Capacity' FROM PD_T.COURSE_REQ CR INNER JOIN PD_M.COURSE_M CM ON CR.CR_CM_ID = CM.CM_ID WHERE  CR.CR_CM_ID = (SELECT CR.CR_CM_ID FROM PD_T.COURSE_REQ CR WHERE CR.CR_ID = " & Id & ") AND CR.CR_APPR_STATUS = 'A' GROUP BY CM.CM_ID , CM.CM_MAX_CAPACITY"
            '    With SqlHelper.ExecuteReader(Con, CommandType.Text, str)
            '        Do While .Read
            '            If .Item("Approved") >= .Item("Max_Capacity") Then
            '                Me.lblError.Text = "This course has already reached its max capacity level"
            '                Exit Sub
            '            End If
            '        Loop
            '        .Close()
            '    End With
            'End If

            Dim params(2) As SqlParameter
            params(0) = New SqlParameter("@id", Id)
            params(1) = New SqlParameter("@status", Status)
            params(2) = New SqlParameter("@usr_id", Session("sUsr_Id"))

            tr = Con.BeginTransaction
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "COURSE_REQUEST_CANCEL_PDC", params)

            Dim params1(1) As SqlParameter
            params1(0) = New SqlParameter("@id", Id)
            params1(1) = New SqlParameter("@status", Status)
            SqlHelper.ExecuteNonQuery(tr, CommandType.StoredProcedure, "SEND_COURSE_REQUEST_CANCELLATION_PDC_EMAIL", params1)

            tr.Commit()
            Me.lblError.Text = "Request cancelled successfully"
        Catch ex As Exception
            Me.lblError.Text = "Error occured while trying to update the request"
            If Not tr Is Nothing Then tr.Rollback()
        Finally

        End Try

    End Sub

    Protected Sub gvRequest_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRequest.PageIndexChanging
        gvRequest.PageIndex = e.NewPageIndex
        If Not Session("PDEventRequestsView") Is Nothing Then
            gvRequest.DataSource = CType(Session("PDEventRequestsView"), DataTable)
            gvRequest.DataBind()
        End If
    End Sub

    Protected Sub gvRequest_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequest.RowCommand
        If e.CommandName = "Select" Then
            If CType(e.CommandSource, LinkButton).Text = "Approve" Then
                Me.ApproveOrReject(Convert.ToInt32(e.CommandArgument), "A")
                Me.SearchData()
            ElseIf CType(e.CommandSource, LinkButton).Text = "Reject" Then
                Me.ApproveOrReject(Convert.ToInt32(e.CommandArgument), "R")
                Me.SearchData()
            ElseIf CType(e.CommandSource, LinkButton).Text = "Cancel" Then
                Me.CancelRequest(Convert.ToInt32(e.CommandArgument), "C")
                Me.SearchData()
            End If
        End If
       
    End Sub

    Protected Sub chkRowHeader_CheckedChanged(sender As Object, e As System.EventArgs)
        Dim chkBox As CheckBox = CType(sender, CheckBox)
        Dim lblId As Integer
        Dim dt As DataTable
        'If chkBox.Checked Then
        'Dim row As GridViewRow
        'For Each row As GridViewRow In Me.gvRequest.Rows
        'CType(row.FindControl("chkRow"), CheckBox).Checked = chkBox.Checked
        ''lblId = CType(row.FindControl("lblId"), Label).Text
        If Not Session("PDEventRequestsView") Is Nothing Then
            dt = CType(Session("PDEventRequestsView"), DataTable)
            For Each row As DataRow In dt.Rows
                row.Item("Selected") = chkBox.Checked
            Next
            dt.AcceptChanges()
            gvRequest.DataSource = dt
            gvRequest.DataBind()
            Session("PDEventRequestsView") = dt
        End If
        'Next
        'End If
    End Sub

    Protected Sub chkRow_CheckedChanged(sender As Object, e As System.EventArgs)
        ' Dim i As Integer = gvRequest.DataKeys(0).Item("ID")
        Dim rowindex As Integer = sender.parent.parent.rowindex()
        Dim lblId As Integer = CType(gvRequest.Rows(rowindex).FindControl("lblId"), Label).Text
        Dim dt As DataTable
        If Not Session("PDEventRequestsView") Is Nothing Then
            dt = CType(Session("PDEventRequestsView"), DataTable)
            For Each row As DataRow In dt.Select("ID = " & lblId)
                row.Item("Selected") = CType(sender, CheckBox).Checked
            Next
            dt.AcceptChanges()
            gvRequest.DataSource = dt
            gvRequest.DataBind()
            Session("PDEventRequestsView") = dt
        End If
    End Sub

    'Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
    'If Not Session("PDEventRequestsView") Is Nothing Then
    '    Session.Remove("PDEventRequestsView")
    'End If
    'End Sub

    Protected Sub chkSelectAll_CheckedChanged(sender As Object, e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Dim dt As DataTable
        If Not Session("PDEventRequestsView") Is Nothing Then
            dt = CType(Session("PDEventRequestsView"), DataTable)
            For Each row As DataRow In dt.Rows
                row.Item("Selected") = CType(sender, CheckBox).Checked
            Next
            dt.AcceptChanges()
            gvRequest.DataSource = dt
            gvRequest.DataBind()
            Session("PDEventRequestsView") = dt
        End If
    End Sub
End Class
