﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class PD_PD_KTAA_VIEW
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("ID") IsNot Nothing And Request.QueryString("ID") <> "" Then
                Dim id As Integer = Convert.ToInt32(Request.QueryString("ID").ToString())
                ViewState("CR_ID") = id
                BindCourseDetails(Convert.ToInt16(ViewState("CR_ID")))

                Bind_KTAA_MASTER(ViewState("CR_ID"))
            End If
        End If
    End Sub

    Sub BindCourseDetails(ByVal Id As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", Id)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_COURSE_INFO_BY_CR_ID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    lblActivity.Text = Dt.Rows(0)("CM_TITLE").ToString()
                    lblCourseDate.Text = Dt.Rows(0)("EVENTDATE").ToString()
                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub

    Sub Bind_KTAA_MASTER(ByVal CRId As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CR_ID", CRId)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_KTAA_BY_REQUEST", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    Dim PDCStatus As Integer = Convert.ToInt16(Dt.Rows(0)("PD_KTA_Status").ToString())

                    If PDCStatus > 0 Then
                        ViewState("KTAA_ID") = Dt.Rows(0)("PD_KTA_ID").ToString()
                        Bind_KTAA_ACTION_DETAILS(ViewState("KTAA_ID"))
                        txtlearning1.Text = Dt.Rows(0)("PD_KTA_LEARNIG1").ToString()
                        txtlearning2.Text = Dt.Rows(0)("PD_KTA_LEARNING2").ToString()
                        txtlearning3.Text = Dt.Rows(0)("PD_KTA_LEARNING3").ToString()
                        trMessage.Visible = False
                    Else
                        tblKTAA.Visible = False
                        trMessage.Visible = True


                    End If
                    

                Else
                    tblKTAA.Visible = False
                    trMessage.Visible = True

                End If
            Else
                tblKTAA.Visible = False
                trMessage.Visible = True

            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub

    Sub Bind_KTAA_ACTION_DETAILS(ByVal KTAA_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@KTAA_ID", KTAA_ID)
            'param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_KTAA_ACTIONS_BY_KTAID", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)
                gvCourseActions.DataSource = ds
                gvCourseActions.DataBind()

                Session("COURSEACTION") = Dt
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub
End Class
