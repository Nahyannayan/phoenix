﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class PD_PD_Calendar_Apply
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("val") IsNot Nothing And Request.QueryString("val") <> "" Then
                Dim id As Integer = Convert.ToInt32(Request.QueryString("val").ToString())
                ViewState("CM_ID") = id
                BindCourseDetails(id)
                bindRegion()

            End If
        End If
    End Sub

    Sub BindCourseDetails(ByVal Id As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@CM_ID", Id)
            param(1) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Course_Info_By_Id", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    lblCSTitle.Text = Dt.Rows(0)("CM_TITLE").ToString()
                    lblDate.Text = Dt.Rows(0)("CM_EVENT_DT").ToString() & " To " & Dt.Rows(0)("CM_EVENT_END_DT").ToString()
                    lblTimeSlot.Text = Dt.Rows(0)("CM_START_TIME").ToString() & " To " & Dt.Rows(0)("CM_END_TIME").ToString()
                    lblMax.Text = Dt.Rows(0)("CM_MAX_CAPACITY").ToString()
                    lblOnlineDate.Text = Dt.Rows(0)("CM_OPEN_ONLINE_DT").ToString() & " To " & Dt.Rows(0)("CM_CLOSE_ONLINE_DT").ToString()
                    lblDetails.Text = Server.HtmlDecode(Dt.Rows(0)("CM_DESCR").ToString())
                    lblTrainer.Text = Dt.Rows(0)("Trainers").ToString()

                    If Dt.Rows(0)("CR_EMP_ID").ToString() <> 0 AndAlso Dt.Rows(0)("CR_APPR_STATUS").ToString() <> "C" Then
                        btnRegsiter.Visible = False

                        lblError.InnerHtml = "You have already registered for this session !!!"

                    ElseIf Convert.ToDateTime(Dt.Rows(0)("CM_OPEN_ONLINE_DT").ToString()) > DateTime.Today Then
                        btnRegsiter.Visible = False
                        lblError.InnerHtml = ">Please check registration date !!!"

                    ElseIf Convert.ToDateTime(Dt.Rows(0)("CM_CLOSE_ONLINE_DT").ToString()) < DateTime.Today Then
                        btnRegsiter.Visible = False
                        lblError.InnerHtml = "Sorry registration closed !!!"

                    Else
                        btnRegsiter.Visible = True
                    End If

                End If
            End If


        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub

    Protected Sub btnRegsiter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegsiter.Click

        Dim CM_ID As String = ViewState("CM_ID")


        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>You have registered successfully !!!</div>"
            btnRegsiter.Visible = False

        End If
    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion.SelectedIndexChanged
        bindLocation()

    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer

        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CM_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim param(7) As SqlParameter

                param(0) = New SqlParameter("@CM_ID", CM_ID)
                param(1) = New SqlParameter("@UserId", Session("sUsr_id"))
                param(2) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
                param(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                param(4) = New SqlParameter("@LocationId", ddlLocation.SelectedValue)
                param(5) = New SqlParameter("@RegionId", ddlRegion.SelectedValue)
                param(6) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(6).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_T.SAVE_COURSE_REQUSET", param)
                Dim ReturnFlag As Integer = param(6).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.!!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SVB_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    'resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved. !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function

    Private Sub bindRegion()
        Try
            Dim regionId As String = String.Empty
            If ddlRegion.SelectedIndex = -1 Then
                regionId = ""
            Else
                regionId = ddlRegion.SelectedItem.Value
            End If
        
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter
            PARAM(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))
           
            Using RegionReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "PD_M.Get_Course_Region", PARAM)
                ddlRegion.Items.Clear()


                If RegionReader.HasRows = True Then

                    ddlRegion.DataSource = RegionReader
                    ddlRegion.DataTextField = "R_REGION_NAME"
                    ddlRegion.DataValueField = "R_ID"
                    ddlRegion.DataBind()
                    'ddlRegion.Items.Insert(0, New ListItem("-Please Select-", "0"))

                End If
            End Using
            ddlRegion_SelectedIndexChanged(ddlRegion, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindLocation()
        Try
            

            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter
            PARAM(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))
            PARAM(1) = New SqlParameter("@Region_ID", ddlRegion.SelectedValue)
            Using LocationReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "PD_M.Get_Course_Location", PARAM)
                ddlLocation.Items.Clear()


                If LocationReader.HasRows = True Then

                    ddlLocation.DataSource = LocationReader
                    ddlLocation.DataTextField = "L_DESCR"
                    ddlLocation.DataValueField = "L_ID"
                    ddlLocation.DataBind()


                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
