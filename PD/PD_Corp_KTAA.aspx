﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PD_Corp_KTAA.aspx.vb" Inherits="PD_PD_Corp_KTAA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD KTAA Download
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" >
                <tr>
            <td class="matters" style="height: 3px" valign="bottom" colspan="6">
                <table width="100%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal ID="ltLabel" runat="server" Text="Search Criteria"></asp:Literal></span>
                        </td>
                    </tr>
                    </table>
                </td>
                    </tr>
        <tr>
            <td > <span class="field-label" > Academic Year</span></td>
            
            <td colspan="2" >

                <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged"></asp:DropDownList>
            </td>

       
                        <td >
                          <span class="field-label" >   Course </span>
                        </td>
                     
                        <td align="left" >
                            <asp:DropDownList ID="ddlCourse" runat="server"  >
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvCourse" runat="server" ErrorMessage="Please select" ControlToValidate="ddlCourse" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
        
        <tr>
            <td colspan="6">

                <asp:Button ID="btnDownloadExcel" runat="server" Text="Download KTAA" CssClass="button"  />
            </td>

        </tr>
        </table>
<table id="tblGridView" runat="server" width="100%">
         <tr>

             <td colspan="6">
                  <%--  <asp:GridView ID="gvKTAAParticipantList" Visible="false"  runat="server" Font-Size="X-Small" HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" Font-Names="Arial">
        <RowStyle HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
        <AlternatingRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="0px" />
                        <Columns>
                            <asp:BoundField DataField="CR_ID" />
                                <asp:BoundField DataField="KTA_LEARNINGS" />
                        </Columns>
         </asp:GridView>--%>

                 <asp:GridView ID="gvKTAAParticipantList" runat="server"  AutoGenerateColumns="False" 
                    Height="100%" Width="100%" EnableModelValidation="True" 
                      HeaderStyle-Height="30" CellPadding="5" CellSpacing="5" CssClass="table table-bordered table-row" >
                    <RowStyle  Height="45px" Font-Size="12px" />
                     <Columns>
                           <asp:TemplateField HeaderText="No." >
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                           <asp:TemplateField HeaderText="CR ID">
                            <ItemTemplate>
                                <asp:Label ID="lblCR_ID" runat="server"  Text='<%# Eval("CR_ID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Title">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server"  Text='<%# Bind("cm_Title")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="BSU">
                            <ItemTemplate>
                                <asp:Label ID="lblbsu" runat="server"  Text='<%# Bind("BSU_SHORTNAME")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Emp Id">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpId" runat="server"  Text='<%# Bind("EMP_ID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="EMP NO">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPNO" runat="server"  Text='<%# Bind("EMPNO")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="EMPLOYEE">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPLoyee" runat="server"  Text='<%# Bind("EMployee")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEMail" runat="server"  Text='<%# Bind("EMD_Email")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Phone">
                            <ItemTemplate>
                                <asp:Label ID="lblEMPPhone" runat="server"  Text='<%# Bind("EMP_OFF_PHONE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Designation">
                            <ItemTemplate>
                                <asp:Label ID="lblDES_DESCR" runat="server" __designer:wfdid="w18" Text='<%# Bind("DES_DESCR")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:Label ID="lblAttendance" runat="server" __designer:wfdid="w18" Text='<%# Bind("Attendance")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="What are your key take-away learnings from the professional development?">
                            <ItemTemplate>
                                <asp:Label ID="lblALearning" runat="server" __designer:wfdid="w18" Text='<%# Bind("KTA_LEARNINGS")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Action Step / Objectives">
                            <ItemTemplate>
                                <asp:Label ID="lblAActions" runat="server" __designer:wfdid="w18" Text='<%# Bind("ActionStep")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Success Criteria /Expected Outcome">
                            <ItemTemplate>
                                <asp:Label ID="lblAsuccess" runat="server" __designer:wfdid="w18" Text='<%# Bind("SuccessCriteria")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Timeline">
                            <ItemTemplate>
                                <asp:Label ID="lblTimeline" runat="server" __designer:wfdid="w18" Text='<%# Bind("TimeLine")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Resources / Preparation / Support needed">
                               <ItemStyle Height="100px" />
                            <ItemTemplate>
                                <asp:Label ID="lblALearning2" runat="server"  Text='<%# Bind("ResourcePrep")%>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Relevance to role                       ">
                            <ItemTemplate>
                                <asp:Label ID="lblARelev" runat="server" Text='<%# Bind("Rel")%>' Width="200px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>



                        
                     </Columns>
                        <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop" Height="15px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Font-Size="11px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Height="55px" />
                 </asp:GridView>
             </td>
         </tr>
         </table>
     <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
               
                   
                        <asp:Label id="lblNoAccess" runat="server" EnableViewState="False"
                            Font-Size="10px"></asp:Label>
                  
                        
              
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
                </div></div></div>
</asp:Content>

