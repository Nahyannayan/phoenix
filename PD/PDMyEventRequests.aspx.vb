Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Partial Class PD_PDMyEventRequests
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim ddlGroup As Object

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlRegion.SelectedItem.Value
            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
                ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
            Else
                ViewState("ACD_STARTDT") = ""
                ViewState("ACD_ENDDT") = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindTrainerGroups()
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@INFO_TYPE", "GROUP")
            params(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(2) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(3) = New SqlParameter("@Usr_ID", Session("sUsr_id"))
            params(4) = New SqlParameter("@STUDENT_GROUP_ID", -1)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_TRAINER_ATTENDANCE_GROUP", params)
            Me.ddlGroup.DataTextField = "SGM_DESCR"
            Me.ddlGroup.DataValueField = "SGM_ID"
            Me.ddlGroup.DataSource = ds
            Me.ddlGroup.DataBind()
            'Me.ddlGroup_SelectedIndexChanged(Me.ddlGroup, Nothing)
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting group information"
        End Try
    End Sub

    'Private Sub BindTrainerGroupAttendanceStudents()
    '    Dim params(4) As SqlParameter
    '    Try
    '        params(0) = New SqlParameter("@INFO_TYPE", "STUDENTS")
    '        params(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
    '        params(2) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
    '        params(3) = New SqlParameter("@Usr_ID", Session("sUsr_id"))
    '        Dim GroupId As Integer = Me.ddlGroup.SelectedValue
    '        params(4) = New SqlParameter("@Student_Group_ID", GroupId)
    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_TRAINER_ATTENDANCE_GROUP", params)
    '        Me.gvRequest.DataSource = ds.Tables(0)
    '        Me.gvRequest.DataBind()
    '    Catch ex As Exception
    '        Me.lblError.Text = "Error occured while getting student list for the selected group"
    '    End Try
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If ViewState("MainMnu_code") = "U000045" Then
                    lnkBackToCalendar.Visible = True

                Else

                    lnkBackToCalendar.Visible = False

                End If
                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    hfDate.Value = DateTime.Now
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Me.Bind_Bsu()
                    Me.Bind_Region()
                    Me.Bind_Location(Me.ddlRegion.SelectedValue)

                    txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    'BindTrainerGroups()
                End If

                'show all requests by default
                Me.optAll.Checked = True
                Me.SearchData("O")
                Me.gvRequest.Columns(6).Visible = True

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub gvRequest_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkKT As HyperLink = DirectCast(e.Row.FindControl("lnkKT"), HyperLink)
            Dim lnkCertificate As HyperLink = DirectCast(e.Row.FindControl("lnkCertificate"), HyperLink)

            Dim lnkCertificatePdf As LinkButton = DirectCast(e.Row.FindControl("lnkCertificatePdf"), LinkButton)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkCertificatePdf)

           

            Dim lblID As Label = DirectCast(e.Row.FindControl("lblID"), Label)
            Dim CR_ID As String = lblID.Text
            lnkKT.NavigateUrl = "~/PD/PD_KTAA_MAIN.aspx?ID=" & CR_ID
            ''nahyan on 5Oct2015
            Dim eventdate As String = GetEventDate(CR_ID)
            If Convert.ToDateTime(eventdate) < "01/Sep/2015" Then
                lnkCertificate.NavigateUrl = "~/PD/PD_CERTIFICATE_Before1Sep2015.aspx?ID=" & CR_ID
            ElseIf Convert.ToDateTime(eventdate) < "30/Aug/2016" Then
                lnkCertificate.NavigateUrl = "~/PD/PD_CERTIFICATE.aspx?ID=" & CR_ID
            ElseIf Convert.ToDateTime(eventdate) <= "30/Sep/2018" Then
                lnkCertificate.NavigateUrl = "~/PD/PD_Certificate_TELLAL_2017.aspx?ID=" & CR_ID
            Else
                lnkCertificate.NavigateUrl = "~/PD/PD_CERTIFICATE_TELLAL.aspx?ID=" & CR_ID
            End If

            ''
        End If
    End Sub

    Protected Sub lnkCertificatePdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim CR_ID As String = sender.CommandArgument.ToString

            ''added by nahyan to find event date is after sep 2015 to display new certificate (5-oct-2015)

            Dim eventdate As String = GetEventDate(CR_ID)
            Dim reportpath As String = String.Empty
            If Convert.ToDateTime(eventdate) < "01/Sep/2015" Then
                reportpath = "Report/PD_CertificateLS_Before1Sep2015.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "30/Aug/2016" Then

                reportpath = "Report/PD_CertificateLS.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "30/Sep/2018" Then
                reportpath = "Report/PD_Certificate_TELLAL_2017.rpt"
            ElseIf Convert.ToDateTime(eventdate) < "30/Sep/2018" Then
                reportpath = "Report/PD_Certificate_TELLAL_2017.rpt"
            Else
                reportpath = "Report/PD_Certificate_TELLAL.rpt"
            End If


            ''ends here 
            Dim param As New Hashtable
            param.Add("@CR_ID", CR_ID)

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "oasis"
                .reportParameters = param
                .reportPath = Server.MapPath(reportpath)
            End With

            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing




            'Dim filepath As String = String.Empty
            'Dim EnqCode As String = String.Empty
            'Dim html As String = ScreenScrapeHtml(Server.MapPath("~\PD\PD_Certificate.htm"))
            'BindCourseDetails(Convert.ToInt32(CR_ID), html)
            'HTMLToPdf(html.ToString, "PD", filepath, True)
        Catch ex As Exception

        End Try
    End Sub

    Sub bindMonthPRESENT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2,ISNULL(BSU_bAPP_LEAVE_ABSENT,'False') AS bAPP_LEAVE_ABS " _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
                ViewState("bAPP_LEAVE_ABS") = ds.Tables(0).Rows(0)(2)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function GetEventDate(ByVal crId As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim eventDate As String = String.Empty

            str_Sql = " SELECT CM_EVENT_DT FROM PD_M.COURSE_M WHERE CM_ID IN (SELECT CR_CM_ID FROM PD_t.COURSE_REQ WHERE CR_ID=" & crId & ")"
            eventDate = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
            Return eventDate
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Function

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlRegion.Items.Clear()
            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlRegion.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACD_ID")))
            End While
            reader.Close()
            ddlRegion.ClearSelection()
            ddlRegion.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            'ddlAcdYear_SelectedIndexChanged(ddlRegion, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnAdd.Click
        Try
            Me.hfDate.Value = "Add"
            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Else
                'If ddlGroup.SelectedIndex = -1 Then
                '    lblError.Text = "Group not selected"
                'Else
                '    If ValidateDate() = "0" Then
                '        'Call Add_clicked()
                '        'Call backGround()
                '        Me.BindTrainerGroupAttendanceStudents()
                '        If Me.gvRequest.Rows.Count > 0 Then
                '            Me.btnSearch.Visible = True
                '            Me.Button3.Visible = True
                '        Else
                '            Me.btnSearch.Visible = False
                '            Me.Button3.Visible = False
                '        End If
                '    End If
                'End If
            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub

    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtFromDate.Text <> "" Then
                Dim strfDate As String = txtFromDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"
            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If

            Return ErrorStatus
        Catch ex As Exception
            'UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function

    Sub Add_clicked()
        'Dim ACD_ID As String = ddlRegion.SelectedItem.Value
        'Dim totRow As Integer
        'Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
        'Dim GRD_ID As String = ddlLocation.SelectedValue

        'Dim EMP_ID As String = ViewState("EMP_ID")


        'Dim AttDate As String

        'AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)


        'Try
        '    Dim DupCount As Integer = RecordCount(ACD_ID, SGR_ID, AttDate)

        '    If DupCount = 0 Then
        '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        '        Dim str_Sql As String

        '        str_Sql = "SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID='" & SGR_ID & "' AND SSD_ACD_ID='" & ACD_ID & "' AND SSD_GRD_ID='" & GRD_ID & "'"

        '        totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        '        If totRow > 0 Then
        '            ViewState("datamode") = "add"


        '            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '            Call BindAdd_Attendance()
        '            setControl()
        '        Else

        '            lblError.Text = "Record currently not updated. Please Contact System Admin"
        '        End If
        '    Else

        '        lblError.Text = "Attendances already marked for the given date & Group!!!"
        '    End If

        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message)
        'End Try

    End Sub


    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim RAL_ID As New DataColumn("RAL_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))
            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))
            Dim ContAbs As New DataColumn("ContAbs", System.Type.GetType("System.String"))

            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(RAL_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(MinList)
            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            dtDt.Columns.Add(ContAbs)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    If ddlGroup.SelectedIndex = -1 Then
    '        lblError.Text = "Group not selected"
    '    Else
    '        If ValidateDate() = "0" Then
    '            'Call Edit_clicked()
    '            'Call backGround()
    '            Me.GetMarkedAttendance()
    '        End If
    '    End If
    'End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles Button3.Click
        'Try

        '    'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
        '    '    'clear the textbox and set the default settings
        '    '    ViewState("datamode") = "view"
        '    '    Session("dt_ATT_GROUP").Rows.Clear()
        '    '    gvInfo.DataSource = Session("dt_ATT_GROUP")
        '    '    gvInfo.DataBind()

        '    '    ' gvInfo.Visible = False
        '    '    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '    '    btnSave2.Visible = False
        '    '    btnCancel.Visible = False
        '    '    btnCancel2.Visible = False
        '    '    ResetControl()
        '    'End If
        '    Me.txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Now.Date)
        '    Me.btnSearch.Visible = False
        '    Me.gvRequest.DataSource = Nothing
        '    Me.gvRequest.DataBind()

        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
    End Sub
    'Sub setControl()
    '    ddlRegion.Enabled = False
    '    ddlSubjectGroup.Enabled = False
    '    ddlLocation.Enabled = False
    '    ddlSubject.Enabled = False
    '    ddlPERIOD.Enabled = False
    '    imgCalendar.Visible = False
    '    txtFromDate.Enabled = False
    'End Sub

    'Sub ResetControl()
    '    ddlRegion.Enabled = True
    '    ddlSubjectGroup.Enabled = True
    '    ddlLocation.Enabled = True
    '    ddlSubject.Enabled = True
    '    ddlPERIOD.Enabled = True
    '    imgCalendar.Visible = True
    '    txtFromDate.Enabled = True
    'End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True

        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) 'Handles gvInfo.RowDataBound
        Try


            Dim ACD_ID As String = ddlRegion.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")

            Dim query As String = " SELECT APD_ID,APD_PARAM_DESCR FROM ( " & _
" SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_PARAM_DESCR) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR]   " & _
 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID    " & _
 " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION  " & _
" SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')'  " & _
"  AS [APD_PARAM_DESCR]   FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A ORDER BY R1" & _
 "Union All " & _
"Select APD_ID, APD_PARAM_DESCR From Oasis_Services.dbo.ECA_Attendance_Param Where APD_BSU_ID = '" & BSU_ID & "' AND APD_ACD_ID = " & ACD_ID


            Dim DS As DataSet = AccessStudentClass.GetATTENDANCE_ROOM_PARAM_D(ACD_ID, BSU_ID)
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                Dim lblMinList As Label = DirectCast(e.Row.FindControl("lblMinList"), Label)
                Dim lblContAbs As Label = DirectCast(e.Row.FindControl("lblContAbs"), Label)
                Dim ltStar As Literal = DirectCast(e.Row.FindControl("ltstar"), Literal)
                If lblMinList.Text.ToUpper <> "REGULAR" Then
                    e.Row.BackColor = Drawing.Color.FromArgb(251, 204, 119)
                End If
                If lblContAbs.Text = "1" Then
                    ltStar.Text = "<font color='red' size='3px'>*</font>"
                Else
                    ltStar.Visible = False
                End If

                ddlSTATUS.Items.Clear()
                If DS.Tables(0).Rows.Count > 0 Then
                    ddlSTATUS.DataSource = DS.Tables(0)
                    ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
                    ddlSTATUS.DataValueField = "APD_ID"

                    ddlSTATUS.DataBind()

                    Dim lst As IEnumerator
                    Dim currentItem As ListItem

                    lst = ddlSTATUS.Items.GetEnumerator()
                    While (lst.MoveNext())
                        currentItem = CType(lst.Current, ListItem)
                        If Left(currentItem.Text, 7) = "PRESENT" Then
                            ddlSTATUS.ClearSelection()
                            ddlSTATUS.Items.FindByValue(currentItem.Value).Selected = True
                            Exit While
                        End If
                    End While

                    'Call backGround()
                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                'Dim HeaderGrid As GridView = DirectCast(sender, GridView)
                'Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim HeaderCell As New TableCell()
                'HeaderCell.Text = ""
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)
                'HeaderCell = New TableCell()
                'HeaderCell.ForeColor = Drawing.Color.Black
                'HeaderCell.Height = 20
                'HeaderCell.Text = "Last Five Day's Attendance History"
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)


                'gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)

                'e.Row.Cells(10).Text = hfDay1.Value
                'e.Row.Cells(11).Text = hfDay2.Value
                'e.Row.Cells(12).Text = hfDay3.Value
                'e.Row.Cells(15).Text = hfDay4.Value
                'e.Row.Cells(16).Text = hfDay5.Value
            End If

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub gvRequest_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequest.RowCommand
        'Dim str As String = "PDEventRequestsView.aspx?"
        ''Encr_decrData.Encrypt(Request.QueryString("datamode").Replace(" ", "+"))
        'If e.CommandName = "Select" Then
        '    Try
        '        Dim lblstatus, lblcsstatus, lblcrid As String

        '        lblstatus = CType(Me.gvRequest.Rows(e.CommandArgument).FindControl("lblStatus"), Label).Text
        '        lblcsstatus = CType(Me.gvRequest.Rows(e.CommandArgument).FindControl("lblCSStatus"), Label).Text
        '        lblcrid = CType(Me.gvRequest.Rows(e.CommandArgument).FindControl("lblID"), Label).Text

        '        If lblstatus <> "Pending" Then 'Or (lblcsstatus <> "S" Or lblcsstatus <> "R") Then
        '            'Me.lblError.Text = "Cannot cancel the PD request as it is either already approved, rejected or ongoing. To cancel the approved or ongoing PD request, kindly contact the PD coordinator."
        '            Me.lblError.Text = "Cannot cancel the PD request as it is either already approved, rejected or cancelled. To cancel the approved request, kindly contact the PD coordinator."
        '            Exit Sub
        '        Else
        '            Dim param(0) As SqlParameter
        '            param(0) = New SqlParameter("@id", lblcrid)
        '            Dim con As SqlConnection = ConnectionManger.GetOASISConnection
        '            SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "SEND_PD_COURSE_CANCELLATION_EMAILS_PDC", param)
        '            If Me.optPending.Checked Then
        '                Me.SearchData("P")
        '            ElseIf Me.optApproved.Checked Then
        '                Me.SearchData("A")
        '            ElseIf optRejected.Checked Then
        '                Me.SearchData("R")
        '            Else
        '                Me.SearchData("O")
        '            End If
        '            lblError.Text = "Selected PD course request cancelled successfully"
        '        End If
        '    Catch ex As Exception
        '        lblError.Text = "Error occured while cancelling selected PD course request"
        '    End Try
        'End If

    End Sub


    Protected Sub gvStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRequest.RowDataBound
        'Try

        '    Dim ACD_ID As String = Session("Current_ACD_ID")
        '    Dim BSU_ID As String = Session("sBsuid")

        '    Dim query As String = " SELECT APD_ID,APD_PARAM_DESCR FROM ( " & _
        '                        " SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_PARAM_DESCR) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR]   " & _
        '                        " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID    " & _
        '                        " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION  " & _
        '                        " SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')'  " & _
        '                        "  AS [APD_PARAM_DESCR]   FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A --ORDER BY R1" & _
        '                        " Union All " & _
        '                        "Select APD_ID, APD_PARAM_DESCR From Oasis_Services.dbo.ECA_Attendance_Param Where APD_BSU_ID = '" & BSU_ID & "' AND APD_ACD_ID = " & ACD_ID

        '    'Dim DS As DataSet = AccessStudentClass.GetATTENDANCE_ROOM_PARAM_D(ACD_ID, BSU_ID)
        '    Dim DS As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, query)

        '    For Each row As DataRow In DS.Tables(0).Rows
        '        If row.Item("APD_PARAM_DESCR").ToString.Contains("(") Then
        '            row.Item("APD_PARAM_DESCR") = row.Item("APD_PARAM_DESCR").ToString.Split("(")(0)
        '        End If
        '    Next
        '    DS.AcceptChanges()

        '    If e.Row.RowType = DataControlRowType.DataRow Then

        '        If e.Row.Cells(2).Text = "3740" Then

        '        End If

        '        Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlAttendanceStatus"), DropDownList)

        '        ddlSTATUS.Items.Clear()
        '        If DS.Tables(0).Rows.Count > 0 Then
        '            ddlSTATUS.DataSource = DS.Tables(0)
        '            ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
        '            ddlSTATUS.DataValueField = "APD_ID"

        '            ddlSTATUS.DataBind()

        '        End If
        '        ddlSTATUS.Items.FindByText(TryCast(e.Row.FindControl("lblApdId"), Label).Text).Selected = True
        '        'ddlSTATUS.SelectedValue = CType(e.Row.FindControl("lblApdId"), Label).Text

        '    ElseIf e.Row.RowType = DataControlRowType.Header Then

        '    End If

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub SearchData(ByVal Status As String)

        Dim Sgm_Id, Stu_Id, Apd_Id, From_Date, To_Date, Remarks, Ssa_Id ', status As String
        Dim ddlStatus As DropDownList

        Me.txtFromDate.Text = "01/Jan/" & Now.Year.ToString
        Me.txtToDate.Text = "31/Dec/" & Now.Year.ToString

        If txtFromDate.Text.Contains("/") Then
            From_Date = txtFromDate.Text.Split("/")(2) & "-" & txtFromDate.Text.Split("/")(1) & "-" & txtFromDate.Text.Split("/")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("/")(2) & "-" & txtToDate.Text.Split("/")(1) & "-" & txtToDate.Text.Split("/")(0) & " 23:59:59"
        ElseIf txtFromDate.Text.Contains("-") Then
            From_Date = txtFromDate.Text.Split("-")(2) & "-" & txtFromDate.Text.Split("-")(1) & "-" & txtFromDate.Text.Split("-")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("-")(2) & "-" & txtToDate.Text.Split("-")(1) & "-" & txtToDate.Text.Split("-")(0) & " 23:59:59"
        Else
            From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text) & " 00:00:01"
            To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text) & " 23:59:59"
        End If

        'From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
        'To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text)

        Dim transaction As SqlTransaction

        Dim Conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand

        Try

            Dim params(8) As SqlParameter
            'params(0) = New SqlParameter("@bsu_id", Me.hfBsuId.Value)
            params(0) = New SqlParameter("@bsu_id", Session("sBsuid"))
            params(1) = New SqlParameter("@region", Me.ddlRegion.SelectedValue)
            params(2) = New SqlParameter("@location", Me.ddlLocation.SelectedValue)
            params(3) = New SqlParameter("@from_date", Me.txtFromDate.Text)
            params(4) = New SqlParameter("@to_date", Me.txtToDate.Text)
            params(5) = New SqlParameter("@title", Me.txtTitle.Text)
            If Status = "P" Then
                params(6) = New SqlParameter("@status", "P")
            ElseIf Status = "A" Then
                params(6) = New SqlParameter("@status", "A")
            ElseIf Status = "R" Then
                params(6) = New SqlParameter("@status", "R")
            ElseIf Status = "O" Then
                params(6) = New SqlParameter("@status", "O")
            End If
            params(7) = New SqlParameter("@usr_id", Session("sUsr_Id"))


            Dim ds As DataSet = SqlHelper.ExecuteDataset(Conn, CommandType.StoredProcedure, "GET_MY_COURSE_REQUESTS", params)

            If Not ds Is Nothing Then
                ds.Tables(0).Columns.Add("COMBINED_DATE", GetType(String))
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("COMBINED_DATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("EVENT_DATE")) & " to " & String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("EVENT_END_DATE"))
                    row.Item("Sr_No") = i + 1
                    i += 1
                Next
                ds.AcceptChanges()
                Me.gvRequest.DataSource = ds.Tables(0)
                Me.gvRequest.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error Occured While loading the data"
        Finally

        End Try

    End Sub

    Private Sub GetMarkedAttendance()
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(2) = New SqlParameter("@Student_Group_ID", Me.ddlGroup.SelectedValue)
            params(3) = New SqlParameter("@From_Date", String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_MARKED_ATTENDANCE", params)
            Me.gvRequest.DataSource = ds.Tables(0)
            Me.gvRequest.DataBind()
            If Me.gvRequest.Rows.Count > 0 Then
                Me.btnSearch.Visible = True
                'Me.Button3.Visible = True
            Else
                Me.btnSearch.Visible = False
                'Me.Button3.Visible = False
            End If
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting attendance record for the selected group"
        End Try
    End Sub


    'Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
    '    Me.SearchData()
    'End Sub

    Private Sub Bind_Bsu()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT '' AS id, '[SELECT]' AS NAME UNION  SELECT bsu.BSU_ID AS id, bsu.BSU_NAME AS name FROM dbo.BUSINESSUNIT_M bsu) t ORDER BY NAME"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlBusinessUnit.DataSource = ds.Tables(0)
                ddlBusinessUnit.DataValueField = "Id"
                ddlBusinessUnit.DataTextField = "Name"
                ddlBusinessUnit.DataBind()
                ddlBusinessUnit.SelectedValue = Session("sBsuId")
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading business units"
        End Try
    End Sub

    Private Sub Bind_Region()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT R.R_ID AS id, R.R_REGION_NAME AS name FROM PD_M.REGION R) t ORDER BY NAME"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlRegion.DataSource = ds.Tables(0)
                ddlRegion.DataValueField = "Id"
                ddlRegion.DataTextField = "Name"
                ddlRegion.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading region"
        End Try
    End Sub

    Private Sub Bind_Location(Optional ByVal RegionId As Integer = 0)
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Dim query As String

            If RegionId = 0 Then
                query = "SELECT 0 AS id, '[SELECT]' AS NAME"
            Else
                query = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT l.L_ID AS id, l.L_DESCR AS name FROM PD_M.LOCATION L Where L.L_R_Id = " & RegionId & ") t ORDER BY NAME"
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlLocation.DataSource = ds.Tables(0)
                ddlLocation.DataValueField = "Id"
                ddlLocation.DataTextField = "Name"
                ddlLocation.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading location"
        End Try
    End Sub

    Protected Sub optPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPending.CheckedChanged
        Me.SearchData("P")
        Me.gvRequest.Columns(6).Visible = False
        Me.gvRequest.Columns(7).Visible = False
        Me.gvRequest.Columns(9).Visible = True
    End Sub

    Protected Sub optApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optApproved.CheckedChanged
        Me.SearchData("A")
        Me.gvRequest.Columns(6).Visible = False
        Me.gvRequest.Columns(7).Visible = False
        Me.gvRequest.Columns(9).Visible = False
    End Sub

    Protected Sub optRejected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optRejected.CheckedChanged
        Me.SearchData("R")
        Me.gvRequest.Columns(6).Visible = False
        Me.gvRequest.Columns(7).Visible = False
        Me.gvRequest.Columns(9).Visible = False
    End Sub

    Protected Sub optAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optAll.CheckedChanged
        Me.SearchData("O")
        Me.gvRequest.Columns(6).Visible = True
        Me.gvRequest.Columns(7).Visible = False
        Me.gvRequest.Columns(9).Visible = True
    End Sub

    Protected Sub gvRequest_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvRequest.RowDeleting
        Dim str As String = "PDEventRequestsView.aspx?"
        'Encr_decrData.Encrypt(Request.QueryString("datamode").Replace(" ", "+"))

        Try
            Dim lblstatus, lblcsstatus, lblcrid As String

            lblstatus = CType(Me.gvRequest.Rows(e.RowIndex).FindControl("lblStatus"), Label).Text
            lblcsstatus = CType(Me.gvRequest.Rows(e.RowIndex).FindControl("lblCSStatus"), Label).Text
            lblcrid = CType(Me.gvRequest.Rows(e.RowIndex).FindControl("lblID"), Label).Text

            If lblstatus <> "Pending" Then 'Or (lblcsstatus <> "S" Or lblcsstatus <> "R") Then
                'Me.lblError.Text = "Cannot cancel the PD request as it is either already approved, rejected or ongoing. To cancel the approved or ongoing PD request, kindly contact the PD coordinator."
                Me.lblError.Text = "Cannot cancel the PD request as it is either already approved, rejected or cancelled. To cancel the approved request, kindly contact the PD coordinator."
                Exit Sub
            Else
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@id", lblcrid)
                Dim con As SqlConnection = ConnectionManger.GetOASISConnection
                SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "SEND_PD_COURSE_CANCELLATION_EMAILS_PDC", param)
                If Me.optPending.Checked Then
                    Me.SearchData("P")
                ElseIf Me.optApproved.Checked Then
                    Me.SearchData("A")
                ElseIf optRejected.Checked Then
                    Me.SearchData("R")
                Else
                    Me.SearchData("O")
                End If
                lblError.Text = "Selected PD course request cancelled successfully"
            End If
        Catch ex As Exception
            lblError.Text = "Error occured while cancelling selected PD course request"
        End Try

    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion.SelectedIndexChanged
        Bind_Location(Me.ddlRegion.SelectedValue)
    End Sub


    Protected Function GetNavigateUrl(ByVal CM_ID As String, ByVal CRID As String) As String

        Dim strUrl As String
        Dim PD_CM_ID As String = Encr_decrData.Encrypt(CM_ID)
        Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
        Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))
        CRID = Encr_decrData.Encrypt(CRID)
        Dim strCourseType As String = ""
        Dim strQuery As String
        Dim strRedirect As String = ""
        Dim strReturnValue As String = ""
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        strQuery = "SELECT CM_COURSE_TYPE_ID FROM OASIS.PD_M.COURSE_M WHERE CM_ID=" & Encr_decrData.Decrypt(PD_CM_ID)
        strCourseType = SqlHelper.ExecuteScalar(CONN, CommandType.Text, strQuery)
        If strCourseType = "1" Then
            strRedirect = "../Survey/comSurKTAAEF.aspx"
        ElseIf strCourseType = "2" Then
            strRedirect = "../Survey/comSurKTAAEFOnline.aspx"
        End If
        If strCourseType <> "" And strRedirect <> "" Then
            strUrl = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}&CRID={4}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID, CRID)
            strReturnValue = "javascript:var popup = window.open('" + strUrl + "', '',''); return false;"
        Else
            strReturnValue = "#"
        End If
        Return strReturnValue
    End Function


    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            Dim PD_CM_ID As String = Encr_decrData.Encrypt(sender.CommandArgument.ToString)
            Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
            Dim EMP_ID As String = Encr_decrData.Encrypt(Session("EmployeeId"))




            Dim strRedirect As String = "~\Survey\comSurPrintEF.aspx"
            Dim mInfo As String = "?PD_CM_VAL=" & PD_CM_ID

            url = String.Format("{0}?PD_CM_VAL={1}&BSU_ID={2}&EMP_ID={3}", strRedirect, PD_CM_ID, BSU_ID, EMP_ID)
            ResponseHelper.Redirect(url, "_blank", "")
        Catch ex As Exception

        End Try
    End Sub
    Protected Function GetNavigateUrl2(ByVal ID As String) As String
        ' For IPAD , double check
        Dim pParms(3) As SqlClient.SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        pParms(1) = New SqlClient.SqlParameter("@CR_ID", ID)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_VALID_CERT", pParms)
            While reader.Read
                Dim lstrEnabled = Convert.ToString(reader("CM_CERTIFICATE_ENABLED"))
                If lstrEnabled = False Then
                    Return ""
                End If
            End While
        End Using




        Dim url As String
        Dim CR_ID As String = Encr_decrData.Encrypt(ID)





        Dim strRedirect As String = "../PD/PD_CERTIFICATE.aspx"
        Dim mInfo As String = "?ID=" & CR_ID

        url = String.Format("{0}?ID={1}", strRedirect, CR_ID)

        Dim str As String = "javascript:var popup = window.open('" + url + "', '',''); return false;"
        Return str
    End Function
    Protected Sub lnkCertific_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            Dim CR_ID As String = Encr_decrData.Encrypt(sender.CommandArgument.ToString)


            ''nahyan on 5Oct2015
            Dim strRedirect As String = String.Empty
            Dim eventdate As String = GetEventDate(sender.CommandArgument.ToString)
            'If Convert.ToDateTime(eventdate) < "01/Sep/2015" Then
            '    strRedirect = "~/PD/PD_CERTIFICATE_Before1Sep2015.aspx"
            'Else
            '    strRedirect = "~/PD/PD_CERTIFICATE.aspx"
            'End If
          
            If Convert.ToDateTime(eventdate) < "01/Sep/2015" Then
                strRedirect = "~/PD/PD_CERTIFICATE_Before1Sep2015.aspx"
            ElseIf Convert.ToDateTime(eventdate) < "30/Aug/2016" Then
                strRedirect = "~/PD/PD_CERTIFICATE.aspx"
            Else
                strRedirect = "~/PD/PD_CERTIFICATE_TELLAL.aspx"
            End If

            ''  Dim strRedirect As String = "~/PD/PD_CERTIFICATE.aspx"
            Dim mInfo As String = "?ID=" & CR_ID

            url = String.Format("{0}?ID={1}", strRedirect, CR_ID)
            ResponseHelper.Redirect(url, "_blank", "")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkBackToCalendar_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/PD/PD_Calendar.aspx?MainMnu_code=" + Encr_decrData.Encrypt("U000045"), False)
        Exit Sub
    End Sub
End Class
