﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="PD_Course_Add.aspx.vb" Inherits="PD_PD_Course_Add" ValidateRequest="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link rel="stylesheet" type="text/css" href="../Scripts/jquery.ui.timepicker.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/jquery-ui-1.9.2.custom.css" />
    <link href="../Scripts/jquery.cleditor.css" rel="stylesheet" type="text/css" />

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.8.3.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />

    <script src="../Scripts/jquery.cleditor.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.timepicker.js" type="text/javascript"></script>

    <script type="text/javascript">
      <%--  $(document).ready(function () {
            pUrl = "PD_Add_Course_Trainers.aspx?ID=CST"

            $(".frameAddTrainer").fancybox({

                'type': 'iframe',
                'width': '100%',
                'height': '100%',
                'hideOnOverlayClick': false,
                'hideOnContentClick': false,
                'overlayOpacity': 0.7,
                'enableEscapeButton': false,
                'href': pUrl,
                onCleanup: function () {
                    var cmID = $("#fancybox-frame").contents().find('#hdnSelected').val();
                    var existingID = $("#<%=hdnSelected.ClientID %>").val();
                if (existingID == '') {
                    document.getElementById('<%=txtTrainerIDs.ClientID%>').value = cmID;
                        $("#<%=h_TrainerId.ClientID%>").val(cmID);

                        // __doPostBack('CustomPostBack', "test");
                        
                    }
                    else {
                    document.getElementById('<%=txtTrainerIDs.ClientID%>').value = existingID + ',' + cmID;
                        $("#<%=h_TrainerId.ClientID%>").val(existingID + '|' + cmID);
                    }
                    __doPostBack('<%= txtTrainerIDs.ClientID%>', 'TextChanged');
            }
        });

   });--%>





        function openWin() {
            var sFeatures, url;


            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


            //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
            url = "PD_Add_Course_Trainers.aspx?ID=CST"
            var oWnd = radopen(url, "pop_student");


        }

        function OnClientClose(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {

                NameandCode = arg.NameCode.split('||');
                document.getElementById('<%=txtTrainerIDs.ClientID%>').value = NameandCode[0];
                document.getElementById('<%=h_TrainerId.ClientID%>').value = NameandCode[0];



                __doPostBack('<%= txtTrainerIDs.ClientID%>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



        $(document).ready(function () {

            $('#<%= txtTime.ClientID %>').timepicker(

    {
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });

    $('#<%= txtTimeTo.ClientID %>').timepicker(

{
    showPeriod: true,
    onHourShow: OnHourShowCallback,
    onMinuteShow: OnMinuteShowCallback
});
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }



});

function FnADD() {


    var elem = $("#<%=trRegion2.ClientID%>")[0];
            var elem2 = $("#<%=trRegion3.ClientID%>")[0];
            var elem3 = $("#<%=trRegion4.ClientID%>")[0];
            var elem4 = $("#<%=trRegion5.ClientID%>")[0];

            if (elem.style.display == 'none') {
                $(elem).show();

            }
            else if (elem2.style.display == 'none')
                $(elem2).show();
            else if (elem3.style.display == 'none')
                $(elem3).show();
            else if (elem4.style.display == 'none')
                $(elem4).show();

        }

        function FnREMOVE(trId) {


            var elem = $("#<%=trRegion2.ClientID%>")[0];
            var elem2 = $("#<%=trRegion3.ClientID%>")[0];
            var elem3 = $("#<%=trRegion4.ClientID%>")[0];
            var elem4 = $("#<%=trRegion5.ClientID%>")[0];

            if (trId == 2) {
                $(elem).hide();

                $("#<%=ddlRegion2.ClientID%>").val(0).attr("selected", "selected");
                $("#<%=ddlLocation2.ClientID%>").val(0).attr("selected", "selected");

            }
            else if (trId == 3) {
                $(elem2).hide();
                $("#<%=ddlRegion3.ClientID%>").val(0).attr("selected", "selected");
                $("#<%=ddlLocation3.ClientID%>").val(0).attr("selected", "selected");
            }
            else if (trId == 4) {
                $(elem3).hide();
                $("#<%=ddlRegion4.ClientID%>").val(0).attr("selected", "selected");
                $("#<%=ddlLocation4.ClientID%>").val(0).attr("selected", "selected");
            }
            else if (trId == 5) {
                $(elem4).hide();

                $("#<%=ddlRegion5.ClientID%>").val(0).attr("selected", "selected");
                $("#<%=ddlLocation5.ClientID%>").val(0).attr("selected", "selected");
            }

}

function CancelCourse() {
    var cStatus = $("#<%=ddlStatus1.ClientID%>").val();

    if (cStatus == "C") {
        return confirm('Do you really want to change the status of this course to cancelled? Please confirm.');
    }

}
    </script>
    <style type="text/css">
        .scrollingCheckBoxList {
            border: 1px #808080 solid;
            margin: 10px 10px 10px 10px;
            height: 20px;
        }

        .scrollingControlContainer {
            overflow-x: hidden;
            overflow-y: scroll;
        }

        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
            width: 2em;
        }

        #ui-timepicker-div {
            background-color: #ffffff;
        }
    </style>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>

    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Add PD Course
           
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:HiddenField ID="hdnSelected" runat="server" />
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <div id="lblError" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table id="tblCategory" width="100%">
                                <tr align="left">
                                    <td width="25%">
                                        <span class="field-label">Code<font color="maroon">*</font></span>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox ID="txtCode" runat="server" MaxLength="20"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Code required" ForeColor=""
                                            SetFocusOnError="True" ValidationGroup="category"></asp:RequiredFieldValidator>


                                    </td>
                                    <td width="25%"></td>
                                    <td width="25%"></td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Title<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="80"></asp:TextBox>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rftitle" runat="server" ControlToValidate="txtTitle"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Title required" ForeColor=""
                                            SetFocusOnError="True" ValidationGroup="category"></asp:RequiredFieldValidator>


                                    </td>

                                    <td width="25%">
                                        <span class="field-label">Type<font color="maroon">*</font></span>
                                    </td>
                                    <td width="25%">
                                        <asp:DropDownList ID="ddlCourseType" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Start Date<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFrom" runat="server" autocomplete="off"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCal1" TargetControlID="txtFrom" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td>
                                        <span class="field-label">End Date<font color="maroon">*</font></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTodate" runat="server" autocomplete="off"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton1" TargetControlID="txtTodate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvEnd" runat="server" ControlToValidate="txtTodate"
                                            Display="Dynamic" ErrorMessage="To Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Time From</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTime" runat="server" autocomplete="off"></asp:TextBox></td>
                                    <td>
                                        <span class="field-label">To </span></td>
                                    <td>
                                        <asp:TextBox ID="txtTimeTo" runat="server" autocomplete="off"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Max Capacity</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMAxCapacity" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Online Apply Date</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOnlineDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton2" TargetControlID="txtOnlineDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvOnline" runat="server" ControlToValidate="txtOnlineDate"
                                            Display="Dynamic" ErrorMessage="Apply Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>

                                    <td>
                                        <span class="field-label">Close Date</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCloseDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton3" TargetControlID="txtCloseDate" CssClass="MyCalendar">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvClose" runat="server" ControlToValidate="txtCloseDate"
                                            Display="Dynamic" ErrorMessage="Close required" ValidationGroup="AttGroup" CssClass="error"
                                            ForeColor="">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Value</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCourseValue" runat="server"></asp:TextBox>
                                    </td>

                                    <td>
                                        <span class="field-label">Value Comments</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtValueComments" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Details</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDetails" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox>


                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <span class="field-label">Description</span>
                                    </td>
                                    <td colspan="3">



                                        <telerik:RadEditor ID="txtDesc" runat="server" EditModes="Design" ToolsFile="~/HelpDesk/xml/FullSetOfTools.xml">
                                        </telerik:RadEditor>

                                    </td>
                                </tr>


                                <tr align="left">
                                    <td>
                                        <span class="field-label">Trainers</span></td>
                                    <td>
                                        <div>
                                            <asp:CheckBoxList ID="chkTrainers" runat="server" EnableTheming="True"
                                                Font-Bold="False"
                                                Visible="False">
                                            </asp:CheckBoxList>
                                            <asp:TextBox ID="txtTrainerIDs" runat="server" OnTextChanged="txtTrainerIDs_TextChanged"  ></asp:TextBox>
                                            <asp:ImageButton ID="imgGetTrainers" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                OnClientClick="openWin();return false;" Visible="false" />
                                            <asp:ImageButton ID="imgParticipants2" runat="server" ImageUrl="~/Images/cal.gif"
                                                CssClass="frameAddTrainer" OnClientClick="openWin();return false;"></asp:ImageButton>
                                            <asp:GridView ID="gvEMPName" runat="server" AutoGenerateColumns="False"
                                                Width="100%" PageSize="5"
                                                EnableModelValidation="True" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("EMP_ID") %>' Font-Bold="False"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Trainer Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTrainerName" runat="server" Text='<%# bind("EMPLOYEE") %>' Css></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DESCR") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemStyle Width="80%" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkbtngrdEMPDelete" runat="server" OnClick="lnkbtngrdEMPDelete_Click">Remove</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:CommandField DeleteText="Remove" ShowDeleteButton="True">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_new" />
                                            </asp:GridView>



                                            <asp:HiddenField ID="h_TrainerId" runat="server" />



                                            &nbsp;
                                        </div>

                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="4">
                                        <table id="tblRegion" width="100%">
                                            <tr id="trRegion1">
                                                <td>
                                                    <span class="field-label">Region</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRegion1" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="field-label">Location</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocation1" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus1" runat="server">
                                                        <asp:ListItem Text="ACTIVE" Value="A"></asp:ListItem>
                                                        <%--<asp:ListItem Text="SCHEDULE" Value="S"></asp:ListItem>
                                            <asp:ListItem Text="RE-SCHEDULED" Value="R"></asp:ListItem>
                                            <asp:ListItem Text="COMPLETED" Value="F"></asp:ListItem>--%>
                                                        <asp:ListItem Text="CANCELLED" Value="C"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnAddMore" class="button" value="ADD MORE" onclick="FnADD()" style="display: none" />
                                                </td>
                                            </tr>
                                            <tr class="trRegion2" runat="server" id="trRegion2" style="display: none">
                                                <td>
                                                    <span class="field-label">Region</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRegion2" runat="server" AutoPostBack="true" onchange="if(this.selectedIndex == 0)return false;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="field-label">Location</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocation2" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus2" runat="server">
                                                        <asp:ListItem Text="ACTIVE" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="SCHEDULE" Value="S"></asp:ListItem>
                                                        <asp:ListItem Text="RE-SCHEDULED" Value="R"></asp:ListItem>
                                                        <asp:ListItem Text="COMPLETED" Value="F"></asp:ListItem>
                                                        <asp:ListItem Text="CANCELLED" Value="C"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnRemove2" class="button" value="REMOVE" onclick="FnREMOVE(2)" />
                                                </td>
                                            </tr>
                                            <tr class="trRegion3" runat="server" id="trRegion3" style="display: none">
                                                <td>
                                                    <span class="field-label">Region</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRegion3" runat="server" AutoPostBack="true" onchange="if(this.selectedIndex == 0)return false;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="field-label">Location</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocation3" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus3" runat="server">
                                                        <asp:ListItem Text="ACTIVE" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="SCHEDULE" Value="S"></asp:ListItem>
                                                        <asp:ListItem Text="RE-SCHEDULED" Value="R"></asp:ListItem>
                                                        <asp:ListItem Text="COMPLETED" Value="F"></asp:ListItem>
                                                        <asp:ListItem Text="CANCELLED" Value="C"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnRemove3" class="button" value="REMOVE" onclick="FnREMOVE(3)" />
                                                </td>
                                            </tr>
                                            <tr class="trRegion4" runat="server" id="trRegion4" style="display: none">
                                                <td>Region
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRegion4" runat="server" AutoPostBack="true" onchange="if(this.selectedIndex == 0)return false;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>Location
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocation4" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus4" runat="server">
                                                        <asp:ListItem Text="ACTIVE" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="SCHEDULE" Value="S"></asp:ListItem>
                                                        <asp:ListItem Text="RE-SCHEDULED" Value="R"></asp:ListItem>
                                                        <asp:ListItem Text="COMPLETED" Value="F"></asp:ListItem>
                                                        <asp:ListItem Text="CANCELLED" Value="C"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnRemove4" class="button" value="REMOVE" onclick="FnREMOVE(4)" />
                                                </td>
                                            </tr>
                                            <tr class="trRegion5" runat="server" id="trRegion5" style="display: none">
                                                <td>Region
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlRegion5" runat="server" AutoPostBack="true" onchange="if(this.selectedIndex == 0)return false;">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>Location
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLocation5" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatus5" runat="server">
                                                        <asp:ListItem Text="ACTIVE" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="SCHEDULE" Value="S"></asp:ListItem>
                                                        <asp:ListItem Text="RE-SCHEDULED" Value="R"></asp:ListItem>
                                                        <asp:ListItem Text="COMPLETED" Value="F"></asp:ListItem>
                                                        <asp:ListItem Text="CANCELLED" Value="C"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <input type="button" id="btnRemove5" class="button" value="REMOVE" onclick="FnREMOVE(5)" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAddCourse" runat="server" Text="Add/Save" CssClass="button" ValidationGroup="category"
                                            Width="130px" />
                                        <asp:Button ID="btnUpdateCourse" runat="server" Text="Update/Save" CssClass="button"
                                            Width="130px" OnClientClick="CancelCourse();" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" CausesValidation="False"
                                            Width="70px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
