﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
'Imports Microsoft.Office.Interop.Excel
'Imports NPOI.HSSF
'Imports NPOI.HSSF.UserModel
'Imports NPOI.HSSF.Util

Partial Class PD_PD_Course_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.QueryString("msg") Is Nothing Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD00085") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("CM_ID") = 0

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    BindPD_Calendar_Events()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub gvCourse_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        Dim isPDC As Boolean = False
        ''checking if logged in user is corporate user
        If Session("sBsuid") <> "999998" AndAlso Session("sBsuid") <> "800140" Then
            isPDC = Check_PD_Coordinator()
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then


            If isPDC Then
                e.Row.Cells(5).Visible = False
                gvCourse.HeaderRow.Cells(5).Visible = False

            End If




            Dim lbtnEnableKTA As LinkButton = DirectCast(e.Row.FindControl("lbtnEnableKTA"), LinkButton)

            Dim lblKTA As Label = DirectCast(e.Row.FindControl("lblKTA"), Label)

            Dim lbtnEnableEvaluation As LinkButton = DirectCast(e.Row.FindControl("lbtnEnableEvaluation"), LinkButton)

            Dim lblEValuation As Label = DirectCast(e.Row.FindControl("lblEValuation"), Label)

            Dim lbtnEnableCertificate As LinkButton = DirectCast(e.Row.FindControl("lbtnEnableCertificate"), LinkButton)

            Dim lblCertificate As Label = DirectCast(e.Row.FindControl("lblCertificate"), Label)

            If lblKTA.Text = "True" Then
                lbtnEnableKTA.Text = "Disable"
            Else
                lbtnEnableKTA.Text = "Enable"
            End If

            If lblEValuation.Text = "True" Then
                lbtnEnableEvaluation.Text = "Disable"
            Else
                lbtnEnableEvaluation.Text = "Enable"
            End If

            If lblCertificate.Text = "True" Then
                lbtnEnableCertificate.Text = "Disable"
            Else
                lbtnEnableCertificate.Text = "Enable"
            End If

            'lbtnEnableKTA.OnClientClick = "javascript:deleteItem('" + lbtnEnableKTA.UniqueID & "');return false;"
        End If


    End Sub

    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_Course_Add.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbtnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub lbtnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim lblCM_ID As New Label()
        lblCM_ID = TryCast(sender.FindControl("lblCM_ID"), Label)
        ViewState("CM_ID") = lblCM_ID.Text

        Dim dt As DataSet = Bind_PD_Participants()

        'Create a dummy GridView
        Dim GridView1 As New GridView()


        If dt.Tables(0).Rows.Count > 0 Then
            GridView1.AllowPaging = False
            GridView1.DataSource = dt
            GridView1.DataBind()
        Else

            dt.Tables(0).Rows.Add(dt.Tables(0).NewRow())

            GridView1.DataSource = dt.Tables(0)
            Try
                GridView1.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = GridView1.Rows(0).Cells.Count
            ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell)
            GridView1.Rows(0).Cells(0).ColumnSpan = columnCount
            GridView1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GridView1.Rows(0).Cells(0).Text = "No participants exists !!!"
        End If


        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
             "attachment;filename=ParticipantsList.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        For i As Integer = 0 To GridView1.Rows.Count - 1
            'Apply text style to each Row
            GridView1.Rows(i).Attributes.Add("class", "textmode")
        Next
        GridView1.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()


    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)



        Try
            Dim url As String

            Dim lblCM_ID As New Label
            lblCM_ID = TryCast(sender.FindControl("lblCM_ID"), Label)
            ViewState("CM_ID") = lblCM_ID.Text
            lblError.InnerHtml = ""

            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim CMId As String = Encr_decrData.Encrypt(lblCM_ID.Text)
            url = String.Format("~\PD\PD_Course_Add.aspx?MainMnu_code={0}&datamode={1}&cmId={2}", ViewState("MainMnu_code"), ViewState("datamode"), CMId)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try

    End Sub

   
    Protected Sub lbtnEnableKTA_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblCMID As New Label
        lblCMID = TryCast(sender.FindControl("lblCM_ID"), Label)
        ViewState("CM_ID") = lblCMID.Text

        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>KTAA Updated successfully !!!</div>"

            BindPD_Calendar_Events()
        End If
     
    End Sub

    Protected Sub lbtnEnableEvaluation_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblCMID As New Label
        lblCMID = TryCast(sender.FindControl("lblCM_ID"), Label)
        ViewState("CM_ID") = lblCMID.Text

        Dim errormsg As String = String.Empty
        If EnableEvaluation(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Evaluation updated successfully !!!</div>"
            BindPD_Calendar_Events()

        End If

    End Sub

    Protected Sub lbtnEnableCertificate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblCMID As New Label
        lblCMID = TryCast(sender.FindControl("lblCM_ID"), Label)
        ViewState("CM_ID") = lblCMID.Text

        Dim errormsg As String = String.Empty
        If EnableCertificate(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>CERTIFICATE Updated successfully !!!</div>"

            BindPD_Calendar_Events()
        End If

    End Sub


    Protected Sub gvCourse_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCourse.PageIndex = e.NewPageIndex
        BindPD_Calendar_Events()
    End Sub

    Protected Sub btnSearchCode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_Calendar_Events()
    End Sub
    Protected Sub btnSearchTitle_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindPD_Calendar_Events()
    End Sub

    Sub BindPD_Calendar_Events()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim str_Title As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Dim TITLE As String = String.Empty
        Dim str_CODE As String = String.Empty
        Dim CODE As String = String.Empty

        Try

            If gvCourse.Rows.Count > 0 Then

                txtSearch = gvCourse.HeaderRow.FindControl("txtTitle")

                If txtSearch.Text.Trim <> "" Then
                    TITLE = " WHERE replace(CM_Title,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_Title = txtSearch.Text.Trim
                End If
                txtSearch = gvCourse.HeaderRow.FindControl("txtCode")

                If txtSearch.Text.Trim <> "" Then
                    CODE = " WHERE replace(CM_Code,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    str_CODE = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = TITLE + CODE
            Dim Dt As New DataTable

            param(0) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
            param(1) = New SqlParameter("@FILTER_COND", FILTER_COND)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Calendar_Events", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                gvCourse.DataSource = ds.Tables(0)
                gvCourse.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvCourse.DataSource = ds.Tables(0)
                Try
                    gvCourse.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCourse.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCourse.Rows(0).Cells.Clear()
                gvCourse.Rows(0).Cells.Add(New TableCell)
                gvCourse.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCourse.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCourse.Rows(0).Cells(0).Text = "Record not available !!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind course grid")
        End Try
    End Sub


    Function Bind_PD_Participants() As System.Data.DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlClient.SqlParameter

        Dim Dt As New System.Data.DataTable

        param(0) = New SqlClient.SqlParameter("@CM_ID", ViewState("CM_ID"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Participants_List", param)

        If ds.Tables(0).Rows.Count > 0 Then



        End If
        Return ds
    End Function

    Function Check_PD_Coordinator() As Boolean
        Dim IsPDC As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@User_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_By_UserID", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)
            IsPDC = True
        End If
        Return IsPDC
    End Function

    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CM_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))
               
                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.KTAA_ENABLE_COURSE_M", param)
                Dim ReturnFlag As Integer = param(1).Value

                


                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function
    
    Private Function EnableEvaluation(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CM_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))

                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.EVALUATION_ENABLE_COURSE_M", param)
                Dim ReturnFlag As Integer = param(1).Value




                If ReturnFlag = -1 Then
                    EnableEvaluation = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    EnableEvaluation = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    EnableEvaluation = "0"

                End If
            Catch ex As Exception
                EnableEvaluation = "1"
                errormsg = ex.Message
            Finally
                If EnableEvaluation = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf EnableEvaluation <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function EnableCertificate(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CM_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter

                param(0) = New SqlParameter("@CM_ID", ViewState("CM_ID"))

                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.CERTIFICATE_ENABLE_COURSE_M", param)
                Dim ReturnFlag As Integer = param(1).Value




                If ReturnFlag = -1 Then
                    EnableCertificate = "-1"
                    errormsg = "Record cannot be saved. !!!"
                ElseIf ReturnFlag <> 0 Then
                    EnableCertificate = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("CM_ID") = "0"
                    ViewState("datamode") = "none"

                    EnableCertificate = "0"

                End If
            Catch ex As Exception
                EnableCertificate = "1"
                errormsg = ex.Message
            Finally
                If EnableCertificate = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf EnableCertificate <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function
End Class
