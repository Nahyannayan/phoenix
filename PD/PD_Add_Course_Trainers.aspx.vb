﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Add_Course_Trainers
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("ID") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("ID").ToString
                ViewState("MainID") = MainID
                bindTrainersGrid()
            End If
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        bindTrainersGrid()
    End Sub

    Private Sub bindTrainersGrid()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        Dim str_query As String = ""


        Dim str_EMP_NO As String = String.Empty
        Dim str_EMP_Name As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim EMP_NO As String = String.Empty
        Dim EMP_Name As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty

        Try
            If gvPDC.Rows.Count > 0 Then

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NO")

                If txtSearch.Text.Trim <> "" Then
                    EMP_NO = " AND replace(EMPNO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_EMP_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMP_FNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(EMP_LNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"


                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvPDC.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BSU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "


                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = EMP_NO + EMP_Name + BSU_Name

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_Trainers", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPDC.DataSource = ds
                gvPDC.DataBind()
                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "Currently there is no Trainers Exists"
            Else
                gvPDC.DataSource = ds
                gvPDC.DataBind()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnSearchEMP_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindTrainersGrid()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindTrainersGrid()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindTrainersGrid()
    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click
        Dim Emp_IDs As String = String.Empty

        For Each gvrow As GridViewRow In gvPDC.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            If chk IsNot Nothing And chk.Checked Then
                Emp_IDs += gvPDC.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

            End If
        Next

        ''checking if seat is available

        Emp_IDs = Emp_IDs.Trim(",".ToCharArray())
        hdnSelected.Value = Emp_IDs
        Session("liEmpList") = Emp_IDs

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('hdnSelected').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameCode = '" & hdnSelected.Value & "||" & hdnSelected.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & hdnSelected.Value & "||" & hdnSelected.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("parent.$.fancybox.close();")
        'Response.Write("} </script>")
        'test

    End Sub
End Class
