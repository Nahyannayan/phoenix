Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Partial Class PD_PDEventRequests
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim ddlGroup As Object

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlRegion.SelectedItem.Value
            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
                ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
            Else
                ViewState("ACD_STARTDT") = ""
                ViewState("ACD_ENDDT") = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindTrainerGroups()
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@INFO_TYPE", "GROUP")
            params(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(2) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(3) = New SqlParameter("@Usr_ID", Session("sUsr_id"))
            params(4) = New SqlParameter("@STUDENT_GROUP_ID", -1)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_TRAINER_ATTENDANCE_GROUP", params)
            Me.ddlGroup.DataTextField = "SGM_DESCR"
            Me.ddlGroup.DataValueField = "SGM_ID"
            Me.ddlGroup.DataSource = ds
            Me.ddlGroup.DataBind()
            'Me.ddlGroup_SelectedIndexChanged(Me.ddlGroup, Nothing)
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting group information"
        End Try
    End Sub

    'Private Sub BindTrainerGroupAttendanceStudents()
    '    Dim params(4) As SqlParameter
    '    Try
    '        params(0) = New SqlParameter("@INFO_TYPE", "STUDENTS")
    '        params(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
    '        params(2) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
    '        params(3) = New SqlParameter("@Usr_ID", Session("sUsr_id"))
    '        Dim GroupId As Integer = Me.ddlGroup.SelectedValue
    '        params(4) = New SqlParameter("@Student_Group_ID", GroupId)
    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_TRAINER_ATTENDANCE_GROUP", params)
    '        Me.gvRequest.DataSource = ds.Tables(0)
    '        Me.gvRequest.DataBind()
    '    Catch ex As Exception
    '        Me.lblError.Text = "Error occured while getting student list for the selected group"
    '    End Try
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    hfDate.Value = DateTime.Now
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim IsPDC = Check_PD_Coordinator()
                    Dim isPDSuperuser As Boolean = Check_PD_SuperUser()
                    If Not (IsPDC Or isPDSuperuser) Then
                        tblNoAccess.Visible = True
                        tbl_AddGroup.Visible = False
                        Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                        ''lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                        lblNoAccess.Text = Errormsg
                    End If

                    Me.Bind_Bsu()
                    Me.Bind_Region()
                    Me.Bind_Location(Me.ddlRegion.SelectedValue)

                    txtCFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtCtodate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now.AddMonths(6))
                    'BindTrainerGroups()
                End If


                'Load the search filer parameter values from session variables
                Me.ddlBusinessUnit.SelectedValue = Session("PDEventRequestsView_BsuId")
                Me.Bind_Employee()
                Me.ddlEmployee.SelectedValue = Session("PDEventRequestsView_EmpId")
                Me.ddlRegion.SelectedValue = Session("PDEventRequestsView_RegionId")
                Me.ddlLocation.SelectedValue = Session("PDEventRequestsView_LocationId")
                Me.txtFromDate.Text = Session("PDEventRequestsView_FromDate")
                Me.txtToDate.Text = Session("PDEventRequestsView_ToDate")
                Me.txtTitle.Text = Session("PDEventRequestsView_Title")
                Me.optUpcoming.Checked = Session("PDEventRequestsView_Upcoming")
                Me.optOnGoing.Checked = Session("PDEventRequestsView_Ongoing")
                Me.optExpired.Checked = Session("PDEventRequestsView_Expired")

                If Me.optUpcoming.Checked = False And Me.optOnGoing.Checked = False And optExpired.Checked = False Then
                    Me.optUpcoming.Checked = True
                End If
                SearchData()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        Else

        End If
    End Sub

    Sub bindMonthPRESENT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2,ISNULL(BSU_bAPP_LEAVE_ABSENT,'False') AS bAPP_LEAVE_ABS " _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
                ViewState("bAPP_LEAVE_ABS") = ds.Tables(0).Rows(0)(2)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlRegion.Items.Clear()
            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlRegion.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACD_ID")))
            End While
            reader.Close()
            ddlRegion.ClearSelection()
            ddlRegion.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            'ddlAcdYear_SelectedIndexChanged(ddlRegion, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnAdd.Click
        Try
            Me.hfDate.Value = "Add"
            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Else
                'If ddlGroup.SelectedIndex = -1 Then
                '    lblError.Text = "Group not selected"
                'Else
                '    If ValidateDate() = "0" Then
                '        'Call Add_clicked()
                '        'Call backGround()
                '        Me.BindTrainerGroupAttendanceStudents()
                '        If Me.gvRequest.Rows.Count > 0 Then
                '            Me.btnSearch.Visible = True
                '            Me.Button3.Visible = True
                '        Else
                '            Me.btnSearch.Visible = False
                '            Me.Button3.Visible = False
                '        End If
                '    End If
                'End If
            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub

    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtFromDate.Text <> "" Then
                Dim strfDate As String = txtFromDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"
            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If

            Return ErrorStatus
        Catch ex As Exception
            'UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function

    Sub Add_clicked()
        'Dim ACD_ID As String = ddlRegion.SelectedItem.Value
        'Dim totRow As Integer
        'Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
        'Dim GRD_ID As String = ddlLocation.SelectedValue

        'Dim EMP_ID As String = ViewState("EMP_ID")


        'Dim AttDate As String

        'AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)


        'Try
        '    Dim DupCount As Integer = RecordCount(ACD_ID, SGR_ID, AttDate)

        '    If DupCount = 0 Then
        '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        '        Dim str_Sql As String

        '        str_Sql = "SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID='" & SGR_ID & "' AND SSD_ACD_ID='" & ACD_ID & "' AND SSD_GRD_ID='" & GRD_ID & "'"

        '        totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        '        If totRow > 0 Then
        '            ViewState("datamode") = "add"


        '            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '            Call BindAdd_Attendance()
        '            setControl()
        '        Else

        '            lblError.Text = "Record currently not updated. Please Contact System Admin"
        '        End If
        '    Else

        '        lblError.Text = "Attendances already marked for the given date & Group!!!"
        '    End If

        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message)
        'End Try

    End Sub
    
    
    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim RAL_ID As New DataColumn("RAL_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))
            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))
            Dim ContAbs As New DataColumn("ContAbs", System.Type.GetType("System.String"))

            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(RAL_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(MinList)
            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            dtDt.Columns.Add(ContAbs)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function

    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    If ddlGroup.SelectedIndex = -1 Then
    '        lblError.Text = "Group not selected"
    '    Else
    '        If ValidateDate() = "0" Then
    '            'Call Edit_clicked()
    '            'Call backGround()
    '            Me.GetMarkedAttendance()
    '        End If
    '    End If
    'End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles Button3.Click
        'Try

        '    'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
        '    '    'clear the textbox and set the default settings
        '    '    ViewState("datamode") = "view"
        '    '    Session("dt_ATT_GROUP").Rows.Clear()
        '    '    gvInfo.DataSource = Session("dt_ATT_GROUP")
        '    '    gvInfo.DataBind()

        '    '    ' gvInfo.Visible = False
        '    '    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '    '    btnSave2.Visible = False
        '    '    btnCancel.Visible = False
        '    '    btnCancel2.Visible = False
        '    '    ResetControl()
        '    'End If
        '    Me.txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Now.Date)
        '    Me.btnSearch.Visible = False
        '    Me.gvRequest.DataSource = Nothing
        '    Me.gvRequest.DataBind()

        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
    End Sub
    'Sub setControl()
    '    ddlRegion.Enabled = False
    '    ddlSubjectGroup.Enabled = False
    '    ddlLocation.Enabled = False
    '    ddlSubject.Enabled = False
    '    ddlPERIOD.Enabled = False
    '    imgCalendar.Visible = False
    '    txtFromDate.Enabled = False
    'End Sub

    'Sub ResetControl()
    '    ddlRegion.Enabled = True
    '    ddlSubjectGroup.Enabled = True
    '    ddlLocation.Enabled = True
    '    ddlSubject.Enabled = True
    '    ddlPERIOD.Enabled = True
    '    imgCalendar.Visible = True
    '    txtFromDate.Enabled = True
    'End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True

        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) 'Handles gvInfo.RowDataBound
        Try


            Dim ACD_ID As String = ddlRegion.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")

            Dim query As String = " SELECT APD_ID,APD_PARAM_DESCR FROM ( " & _
" SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_PARAM_DESCR) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR]   " & _
 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID    " & _
 " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION  " & _
" SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')'  " & _
"  AS [APD_PARAM_DESCR]   FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A ORDER BY R1" & _
 "Union All " & _
"Select APD_ID, APD_PARAM_DESCR From Oasis_Services.dbo.ECA_Attendance_Param Where APD_BSU_ID = '" & BSU_ID & "' AND APD_ACD_ID = " & ACD_ID


            Dim DS As DataSet = AccessStudentClass.GetATTENDANCE_ROOM_PARAM_D(ACD_ID, BSU_ID)
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                Dim lblMinList As Label = DirectCast(e.Row.FindControl("lblMinList"), Label)
                Dim lblContAbs As Label = DirectCast(e.Row.FindControl("lblContAbs"), Label)
                Dim ltStar As Literal = DirectCast(e.Row.FindControl("ltstar"), Literal)
                If lblMinList.Text.ToUpper <> "REGULAR" Then
                    e.Row.BackColor = Drawing.Color.FromArgb(251, 204, 119)
                End If
                If lblContAbs.Text = "1" Then
                    ltStar.Text = "<font color='red' size='3px'>*</font>"
                Else
                    ltStar.Visible = False
                End If

                ddlSTATUS.Items.Clear()
                If DS.Tables(0).Rows.Count > 0 Then
                    ddlSTATUS.DataSource = DS.Tables(0)
                    ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
                    ddlSTATUS.DataValueField = "APD_ID"

                    ddlSTATUS.DataBind()

                    Dim lst As IEnumerator
                    Dim currentItem As ListItem

                    lst = ddlSTATUS.Items.GetEnumerator()
                    While (lst.MoveNext())
                        currentItem = CType(lst.Current, ListItem)
                        If Left(currentItem.Text, 7) = "PRESENT" Then
                            ddlSTATUS.ClearSelection()
                            ddlSTATUS.Items.FindByValue(currentItem.Value).Selected = True
                            Exit While
                        End If
                    End While

                    'Call backGround()
                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                'Dim HeaderGrid As GridView = DirectCast(sender, GridView)
                'Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim HeaderCell As New TableCell()
                'HeaderCell.Text = ""
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)
                'HeaderCell = New TableCell()
                'HeaderCell.ForeColor = Drawing.Color.Black
                'HeaderCell.Height = 20
                'HeaderCell.Text = "Last Five Day's Attendance History"
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)


                'gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)

                'e.Row.Cells(10).Text = hfDay1.Value
                'e.Row.Cells(11).Text = hfDay2.Value
                'e.Row.Cells(12).Text = hfDay3.Value
                'e.Row.Cells(15).Text = hfDay4.Value
                'e.Row.Cells(16).Text = hfDay5.Value
            End If

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub gvRequest_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRequest.PageIndexChanging
        gvRequest.PageIndex = e.NewPageIndex
        If Not Session("PDEventRequests") Is Nothing Then
            gvRequest.DataSource = CType(Session("PDEventRequests"), DataTable)
            gvRequest.DataBind()
        End If
    End Sub

    Protected Sub gvRequest_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequest.RowCommand
        Dim str As String = "PDEventRequestsView.aspx?"
        'Encr_decrData.Encrypt(Request.QueryString("datamode").Replace(" ", "+"))
        If e.CommandName = "Select" Then

            Dim bsuid As String = String.Empty
            If Me.ddlBusinessUnit.SelectedValue <> "" Then
                bsuid = Me.ddlBusinessUnit.SelectedValue & "|"
                'Else
                '    For j As Integer = 0 To Me.ddlBusinessUnit.Items.Count - 1
                '        If Not ddlBusinessUnit.Items(j).Value.ToString = "" Then
                '            bsuid &= ddlBusinessUnit.Items(j).Value.ToString & "|"
                '        End If
                '    Next
            End If

            'str &= "BsuId=" & Encr_decrData.Encrypt(Me.ddlBusinessUnit.SelectedValue) & "&"
            str &= "BsuId=" & Encr_decrData.Encrypt(bsuid) & "&"
            str &= "RegionId=" & Encr_decrData.Encrypt(Me.ddlRegion.SelectedValue) & "&"
            str &= "LocationId=" & Encr_decrData.Encrypt(Me.ddlLocation.SelectedValue) & "&"
            str &= "FromDate=" & Encr_decrData.Encrypt(Me.txtFromDate.Text) & "&"
            str &= "ToDate=" & Encr_decrData.Encrypt(Me.txtToDate.Text) & "&"
            str &= "Title=" & Encr_decrData.Encrypt(Me.txtTitle.Text) & "&"
            str &= "BsuName=" & Encr_decrData.Encrypt(Me.ddlBusinessUnit.SelectedItem.Text) & "&"
            str &= "Region=" & Encr_decrData.Encrypt(Me.ddlRegion.SelectedItem.Text) & "&"
            str &= "Location=" & Encr_decrData.Encrypt(Me.ddlLocation.SelectedItem.Text) & "&"
            Dim i As Integer = e.CommandArgument
            Dim lblcm_id As String = CType(Me.gvRequest.Rows(e.CommandArgument).FindControl("lblcmid"), Label).Text
            str &= "CmId=" & Encr_decrData.Encrypt(lblcm_id) & "&"
            str &= "EmpId=" & Encr_decrData.Encrypt(Me.ddlEmployee.SelectedValue) & "&"
            Response.Redirect(str)
        End If
    End Sub

    

    Protected Sub gvStudents_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRequest.RowDataBound
        'Try

        '    Dim ACD_ID As String = Session("Current_ACD_ID")
        '    Dim BSU_ID As String = Session("sBsuid")

        '    Dim query As String = " SELECT APD_ID,APD_PARAM_DESCR FROM ( " & _
        '                        " SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_PARAM_DESCR) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR]   " & _
        '                        " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID    " & _
        '                        " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION  " & _
        '                        " SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')'  " & _
        '                        "  AS [APD_PARAM_DESCR]   FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A --ORDER BY R1" & _
        '                        " Union All " & _
        '                        "Select APD_ID, APD_PARAM_DESCR From Oasis_Services.dbo.ECA_Attendance_Param Where APD_BSU_ID = '" & BSU_ID & "' AND APD_ACD_ID = " & ACD_ID

        '    'Dim DS As DataSet = AccessStudentClass.GetATTENDANCE_ROOM_PARAM_D(ACD_ID, BSU_ID)
        '    Dim DS As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, CommandType.Text, query)

        '    For Each row As DataRow In DS.Tables(0).Rows
        '        If row.Item("APD_PARAM_DESCR").ToString.Contains("(") Then
        '            row.Item("APD_PARAM_DESCR") = row.Item("APD_PARAM_DESCR").ToString.Split("(")(0)
        '        End If
        '    Next
        '    DS.AcceptChanges()

        '    If e.Row.RowType = DataControlRowType.DataRow Then

        '        If e.Row.Cells(2).Text = "3740" Then

        '        End If

        '        Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlAttendanceStatus"), DropDownList)

        '        ddlSTATUS.Items.Clear()
        '        If DS.Tables(0).Rows.Count > 0 Then
        '            ddlSTATUS.DataSource = DS.Tables(0)
        '            ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
        '            ddlSTATUS.DataValueField = "APD_ID"

        '            ddlSTATUS.DataBind()

        '        End If
        '        ddlSTATUS.Items.FindByText(TryCast(e.Row.FindControl("lblApdId"), Label).Text).Selected = True
        '        'ddlSTATUS.SelectedValue = CType(e.Row.FindControl("lblApdId"), Label).Text

        '    ElseIf e.Row.RowType = DataControlRowType.Header Then

        '    End If

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub SearchData()

        Dim Sgm_Id, Stu_Id, Apd_Id, From_Date, To_Date, Remarks, Ssa_Id, status As String
        Dim ddlStatus As DropDownList

        If txtFromDate.Text.Contains("/") Then
            From_Date = txtFromDate.Text.Split("/")(2) & "-" & txtFromDate.Text.Split("/")(1) & "-" & txtFromDate.Text.Split("/")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("/")(2) & "-" & txtToDate.Text.Split("/")(1) & "-" & txtToDate.Text.Split("/")(0) & " 23:59:59"
        ElseIf txtFromDate.Text.Contains("-") Then
            From_Date = txtFromDate.Text.Split("-")(2) & "-" & txtFromDate.Text.Split("-")(1) & "-" & txtFromDate.Text.Split("-")(0) & " 00:00:01"
            To_Date = txtToDate.Text.Split("-")(2) & "-" & txtToDate.Text.Split("-")(1) & "-" & txtToDate.Text.Split("-")(0) & " 23:59:59"
        ElseIf txtFromDate.Text <> Nothing And txtToDate.Text <> Nothing Then
            From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text) & " 00:00:01"
            To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text) & " 23:59:59"
        End If

        'From_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text)
        'To_Date = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDate.Text)

        Dim transaction As SqlTransaction

        Dim Conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As SqlCommand

        'save selected filter parameters into session variables
        Session("PDEventRequestsView_BsuId") = Me.ddlBusinessUnit.SelectedValue
        Session("PDEventRequestsView_EmpId") = Me.ddlEmployee.SelectedValue
        Session("PDEventRequestsView_RegionId") = Me.ddlRegion.SelectedValue
        Session("PDEventRequestsView_LocationId") = Me.ddlLocation.SelectedValue
        Session("PDEventRequestsView_FromDate") = Me.txtFromDate.Text
        Session("PDEventRequestsView_ToDate") = Me.txtToDate.Text
        Session("PDEventRequestsView_Title") = Me.txtTitle.Text
        Session("PDEventRequestsView_Upcoming") = Me.optUpcoming.Checked
        Session("PDEventRequestsView_Ongoing") = Me.optOnGoing.Checked
        Session("PDEventRequestsView_Expired") = Me.optExpired.Checked

        Try

            Dim params(9) As SqlParameter

            Dim bsuid As String
            If Me.ddlBusinessUnit.SelectedValue <> "" Then
                bsuid = Me.ddlBusinessUnit.SelectedValue & "|"
            Else
                For i As Integer = 0 To Me.ddlBusinessUnit.Items.Count - 1
                    If Not ddlBusinessUnit.Items(i).Value.ToString = "" Then
                        bsuid &= ddlBusinessUnit.Items(i).Value.ToString & "|"
                    End If
                Next
            End If

            'params(0) = New SqlParameter("@bsu_id", Me.ddlBusinessUnit.SelectedValue)
            params(0) = New SqlParameter("@bsu_id", bsuid)
            params(1) = New SqlParameter("@region", Me.ddlRegion.SelectedValue)
            params(2) = New SqlParameter("@location", Me.ddlLocation.SelectedValue)
            params(3) = New SqlParameter("@from_date", From_Date)
            params(4) = New SqlParameter("@to_date", To_Date)
            params(5) = New SqlParameter("@title", Me.txtTitle.Text)

            If Me.optUpcoming.Checked Then
                params(6) = New SqlParameter("@status", "S")
            ElseIf Me.optOnGoing.Checked Then
                params(6) = New SqlParameter("@status", "A")
            ElseIf Me.optExpired.Checked Then
                params(6) = New SqlParameter("@status", "E")
            End If

            params(7) = New SqlParameter("@EMP_ID", Me.ddlEmployee.SelectedValue)
            params(8) = New SqlParameter("@cFromDate", Convert.ToDateTime(txtCFromDate.Text))
            params(9) = New SqlParameter("@cToDate", Convert.ToDateTime(txtCtodate.Text))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Conn, CommandType.StoredProcedure, "GET_COURSE_REQUESTS_STATS", params)

            If Not ds Is Nothing Then
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows
                    row.Item("Sr_No") = i + 1
                    i += 1
                Next
                ds.AcceptChanges()
                Me.gvRequest.DataSource = ds.Tables(0)
                Me.gvRequest.DataBind()
                Session("PDEventRequests") = ds.Tables(0)
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error Occured While loading the data"
        Finally

        End Try

    End Sub

    Private Sub GetMarkedAttendance()
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            params(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            params(2) = New SqlParameter("@Student_Group_ID", Me.ddlGroup.SelectedValue)
            params(3) = New SqlParameter("@From_Date", String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, "ST.GET_MARKED_ATTENDANCE", params)
            Me.gvRequest.DataSource = ds.Tables(0)
            Me.gvRequest.DataBind()
            If Me.gvRequest.Rows.Count > 0 Then
                Me.btnSearch.Visible = True
                'Me.Button3.Visible = True
            Else
                Me.btnSearch.Visible = False
                'Me.Button3.Visible = False
            End If
        Catch ex As Exception
            Me.lblError.Text = "Error occured while getting attendance record for the selected group"
        End Try
    End Sub


    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Me.SearchData()
    End Sub

    Private Sub Bind_Bsu()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT '' AS id, '[SELECT]' AS NAME UNION  SELECT bsu.BSU_ID AS id, bsu.BSU_NAME AS name FROM dbo.BUSINESSUNIT_M bsu Where bsu_id in (select pdc_bsu_id from pd_co_ordinators where pdc_emp_id =" & " )) t ORDER BY NAME"

            Dim param(1) As SqlParameter

            param(0) = New SqlParameter("@usr_id", Session("sUsr_Id"))
            param(1) = New SqlParameter("@LOGGED_IN_BSU_ID", Session("sBsuId"))

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[PD_M].[Get_PDC_BusinessUnit]", param)

            If Not ds Is Nothing Then
                ddlBusinessUnit.DataSource = ds.Tables(0)
                ddlBusinessUnit.DataValueField = "Id"
                ddlBusinessUnit.DataTextField = "Name"
                ddlBusinessUnit.DataBind()
                'ddlBusinessUnit.SelectedValue = Session("sBsuId")
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading business units"
        End Try
    End Sub

    Private Sub Bind_Region()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim query As String = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT R.R_ID AS id, R.R_REGION_NAME AS name FROM PD_M.REGION R) t ORDER BY NAME"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlRegion.DataSource = ds.Tables(0)
                ddlRegion.DataValueField = "Id"
                ddlRegion.DataTextField = "Name"
                ddlRegion.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading region"
        End Try
    End Sub

    Private Sub Bind_Employee()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim bsuId As String = String.Empty
            If Me.ddlBusinessUnit.SelectedValue = "" Then
                bsuId = "0"
            Else
                bsuId = Me.ddlBusinessUnit.SelectedValue
            End If
            Dim query As String = "Select * From (SELECT 0 AS Id, '[SELECT]' AS NAME Union SELECT E.EMP_ID As Id, ISNULL(e.EMP_FNAME,'') + ' ' + ISNULL(e.EMP_MNAME,'') + ' ' + ISNULL(e.EMP_LNAME,'') AS Name FROM dbo.EMPLOYEE_M E WHERE e.emp_status in(1,2) and e.EMP_BSU_ID = '" & bsuId & "') T Order By T.Name"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlEmployee.DataSource = ds.Tables(0)
                ddlEmployee.DataValueField = "Id"
                ddlEmployee.DataTextField = "Name"
                ddlEmployee.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading region"
        End Try
    End Sub

    Private Sub Bind_Location(Optional ByVal RegionId As Integer = 0)
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Dim query As String

            If RegionId = 0 Then
                query = "SELECT 0 AS id, '[SELECT]' AS NAME"
            Else
                query = "SELECT * FROM (SELECT 0 AS id, '[SELECT]' AS NAME UNION  SELECT l.L_ID AS id, l.L_DESCR AS name FROM PD_M.LOCATION L Where L.L_R_Id = " & RegionId & ") t ORDER BY NAME"
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.Text, query)

            If Not ds Is Nothing Then
                ddlLocation.DataSource = ds.Tables(0)
                ddlLocation.DataValueField = "Id"
                ddlLocation.DataTextField = "Name"
                ddlLocation.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured while loading location"
        End Try
    End Sub

   
    Protected Sub optExpired_CheckedChanged(sender As Object, e As System.EventArgs) Handles optExpired.CheckedChanged
        If Me.optExpired.Checked = True Then
            Me.gvRequest.Columns(8).Visible = False
        ElseIf Me.optExpired.Checked = False Then
            Me.gvRequest.Columns(8).Visible = True
        End If
    End Sub

    Protected Sub optOnGoing_CheckedChanged(sender As Object, e As System.EventArgs) Handles optOnGoing.CheckedChanged
        If Me.optExpired.Checked = True Then
            Me.gvRequest.Columns(8).Visible = False
        ElseIf Me.optExpired.Checked = False Then
            Me.gvRequest.Columns(8).Visible = True
        End If
    End Sub

    Protected Sub optUpcoming_CheckedChanged(sender As Object, e As System.EventArgs) Handles optUpcoming.CheckedChanged
        If Me.optExpired.Checked = True Then
            Me.gvRequest.Columns(8).Visible = False
        ElseIf Me.optExpired.Checked = False Then
            Me.gvRequest.Columns(8).Visible = True
        End If
    End Sub

    Protected Sub ddlBusinessUnit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        Me.Bind_Employee()
    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlRegion.SelectedIndexChanged
        Bind_Location(Me.ddlRegion.SelectedValue)
    End Sub

    Function Check_PD_Coordinator() As Boolean
        Dim IsPDC As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@User_ID", Session("EmployeeId"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_By_UserID", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsPDC = True
            End If

        End If
        Return IsPDC
    End Function

    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function
End Class
