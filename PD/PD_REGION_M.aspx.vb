﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_REGION_M
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD00084") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("R_ID") = 0
                    btnUpdateRegion.Visible = False

                    BindRegion()
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                   


                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub btnAddRegion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRegion.Click
        ViewState("R_ID") = 0
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
            BindRegion()
            hdnID.Value = ""
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
        End If
    End Sub

    Protected Sub btnUpdateRegion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateRegion.Click
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
            BindRegion()
            hdnID.Value = ""
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
    End Sub

    Protected Sub lbtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hdnID.Value = "a"
        btnAddRegion.Visible = True

        btnUpdateRegion.Visible = False
        lblError.InnerHtml = ""
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblR_ID As New Label
        Dim lblREGION As New Label
       

        lblR_ID = TryCast(sender.FindControl("lblR_ID"), Label)
        lblREGION = TryCast(sender.FindControl("lblR_NAME"), Label)



        txtRegionName.Text = lblREGION.Text

        ViewState("R_ID") = lblR_ID.Text
        hdnID.Value = ViewState("R_ID")

        btnAddRegion.Visible = False

        btnUpdateRegion.Visible = True
        lblError.InnerHtml = ""

        'btnCancel.Visible = True
        'gvCategory.Columns(3).Visible = False
    End Sub

    Protected Sub gvREGION_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvREGION.PageIndex = e.NewPageIndex
        BindRegion()
    End Sub
    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim R_ID As String = ViewState("R_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(3) As SqlParameter
                param(0) = New SqlParameter("@R_ID", R_ID)
                param(1) = New SqlParameter("@RegionName", txtRegionName.Text.Trim)
                param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_REGION", param)
                Dim ReturnFlag As Integer = param(2).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    ' resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function

    Private Sub BindRegion()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_ALL_REGIONS")
            If ds.Tables(0).Rows.Count > 0 Then

                gvREGION.DataSource = ds.Tables(0)
                gvREGION.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                '  ds.Tables(0).Rows(0)(2) = True
                gvREGION.DataSource = ds.Tables(0)
                Try
                    gvREGION.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvREGION.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvREGION.Rows(0).Cells.Clear()
                gvREGION.Rows(0).Cells.Add(New TableCell)
                gvREGION.Rows(0).Cells(0).ColumnSpan = columnCount
                gvREGION.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvREGION.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub
End Class
