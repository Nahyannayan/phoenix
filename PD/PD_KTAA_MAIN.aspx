﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PD_KTAA_MAIN.aspx.vb" Inherits="PD_PD_KTAA_MAIN" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add My KTAA</title>
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <style type="text/css">
        .style1
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 156px;
        }
        #TheFancybox {
 overflow:scroll;
 -webkit-overflow-scrolling:touch;
 width: 100%;
 height: 100%;  
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="overflow: scroll;-webkit-overflow-scrolling:touch;">
        <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
            style="width: 900px">
            <tr class="subheader_img">
                <td colspan="5">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        Key Take-Aways and Actions</span></font>
                </td>
            </tr>
            <tr id="trLabelError">
                <td align="left" class="matters" valign="bottom" colspan="5">
                    <div id="lblError" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" class="style1">
                    Title
                </td>
                <td class="matters" style="width: 4px">
                    :
                </td>
                <td align="left" class="matters">
                    <asp:Label ID="lblActivity" runat="server"></asp:Label>
                </td>
                <td class="matters">
                    Date
                </td>
                <td align="left" class="matters">
                    <asp:Label ID="lblCourseDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" class="subHeader" colspan="5">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        What are your key take-away learnings from the professional development?</span></font>
                </td>
            </tr>
            <tr valign="middle">
                <td colspan="5">
                    <table width="100%" cellpadding="4" cellspacing="4">
                        <tr>
                            <td align="center" style="font-weight: bold; color: #FF0000" valign="top">
                                *&nbsp;<asp:TextBox ID="txtlearning1" runat="server" TextMode="MultiLine" Width="97.5%" Height="50px" SKINID="MultiText"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="color: #FFFFFF">
                                *
                                <asp:TextBox ID="txtlearning2" runat="server" TextMode="MultiLine" Width="97.5%" Height="50px" SKINID="MultiText"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="color: #FFFFFF">
                                *
                                <asp:TextBox ID="txtlearning3" runat="server" TextMode="MultiLine" Width="97.5%" Height="50px" SKINID="MultiText"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" class="subHeader" colspan="5">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        What actions will you now take to implement the new learning? Consider both self
                        (own practice) and others (departmental / school level impact) when writing action
                        steps.</span></font>
                </td>
            </tr>
            <tr id="tableActions" runat="server">
                <td class="style1" colspan="5" style="width:100%">
                                        <table width="100%" cellpadding="4" cellspacing="4">
                        <tr>
                            <td>
                    <asp:GridView ID="gvActions" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                        Width="100%" EnableModelValidation="True" style="overflow: scroll;">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <Columns>
                            <asp:TemplateField HeaderText="S. No">
                                <ItemTemplate>
                                    <asp:Label ID="lblMandatory" runat="server" Text='*' Font-Bold="True" ForeColor="Red" Visible="False" ></asp:Label>
                                    &nbsp;<asp:Label ID="lblSNo" runat="server" Text='<%# bind("SNo") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action Step / Objectives ">
                                <EditItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("PD_KTA_Action_Step") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAction" runat="server" TextMode="MultiLine"  Text='<%# Bind("PD_KTA_ACTION_STEP")%>' style="width:100%;height:100px;margin:0;padding:0;border-width:0" SkinID="MultiText"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Success Criteria /Expected Outcome">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PD_KTA_Success") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtSuccess" runat="server" SKINID="MultiText" TextMode="MultiLine" style="width:100%;height:100px;margin:0;padding:0;border-width:0" Text='<%# Bind("PD_KTA_SUCCESS")%>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="18%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Timeline">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PD_KTA_Timeline") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTimeLine" runat="server" SKINID="MultiText" TextMode="MultiLine" style="width:100%;height:100px;margin:0;padding:0;border-width:0" Text='<%# Bind("PD_KTA_TIMELINE")%>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=" Resources / Preparation / Support needed">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PD_KTA_Resources") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtResources" runat="server" SKINID="MultiText" TextMode="MultiLine" style="width:100%;height:100px;margin:0;padding:0;border-width:0" Text='<%# Bind("PD_KTA_RESOURCES")%>'></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle Width="18%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=" Relevance to role">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("PD_KTA_RelevanceText") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblRelevanceId" runat="server" Text='<%# bind("PD_KTA_RELEVANCE") %>' Visible="False"></asp:Label>
                                    <asp:CheckBoxList ID="cblRelevance" runat="server"  BorderColor="MenuHighlight" BorderStyle="Solid" BorderWidth="1px" CellPadding="2" CellSpacing="2" CssClass="matters" Height="10px" RepeatColumns="1" Width="100%">
                                    </asp:CheckBoxList>
                                </ItemTemplate>
                                <ItemStyle Width="30%" />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_new" Height="25px" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                            </td>
                        </tr>
                        </table>
                
                    
                </td>
            </tr>
            <tr>
                <td align="left" class="matters" colspan="5" style="text-align: right">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="SAVE AS DRAFT" />&nbsp;
                    <asp:Button ID="btnSavetoPDC" runat="server" Visible="False" CssClass="button" Text="SEND TO PDC" />&nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
