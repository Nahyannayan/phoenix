﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PD_KTAA_VIEW.aspx.vb" Inherits="PD_PD_KTAA_VIEW" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <style type="text/css">
        .style1
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 156px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="1" class="BlueTableView" id="trMessage" runat="server">
             <tr class="subheader_img" >
                <td colspan="6">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        Key Take-Aways and Actions</span></font>
                </td>
            </tr>
            <tr id="tr1">
                <td align="center" class="matters" valign="bottom" colspan="6">
                       <font color="RED" face="Arial, Helvetica, sans-serif" size="4"><span style="font-family: Verdana">
                         Sorry !!! No record exists</span></font>
                    
                </td>
            </tr>
        </table>
        <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
            style="width: 900px" id="tblKTAA" runat="server">
            <tr class="subheader_img" >
                <td colspan="6">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        Key Take-Aways and Actions</span></font>
                </td>
            </tr>
            <tr id="trLabelError">
                <td align="left" class="matters" valign="bottom" colspan="6">
                    <div id="lblError" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" class="style1">
                    Title
                </td>
                <td class="matters" style="width: 4px">
                    :
                </td>
                <td align="left" class="matters" colspan="2">
                    <asp:Label ID="lblActivity" runat="server"></asp:Label>
                </td>
                <td class="matters">
                    Date
                </td>
                <td align="left" class="matters">
                    <asp:Label ID="lblCourseDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" class="subHeader" colspan="6">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        What are your key take-away learnings from the professional development?</span></font>
                </td>
            </tr>
            <tr valign="middle">
                <td colspan="6">
                    <table width="100%" cellpadding="4" cellspacing="4">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtlearning1" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtlearning2" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtlearning3" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" class="subHeader" colspan="6">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        What actions will you now take to implement the new learning? Consider both self
                        (own practice) and others (departmental / school level impact) when writing action
                        steps.</span></font>
                </td>
            </tr>
            <tr>
                <td align="left" class="matters" colspan="6">
                    <asp:GridView ID="gvCourseActions" runat="server" AutoGenerateColumns="False" EmptyDataText="No Action details added yet."
                        Width="100%">
                        <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                        <Columns>
                            <asp:TemplateField HeaderText="Unique ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PD_KTA_Action_Step" HeaderText="Action Step / Objectives "
                                ReadOnly="True" />
                            <asp:BoundField DataField="PD_KTA_Success" HeaderText="Success Criteria /Expected Outcome" />
                            <asp:BoundField DataField="PD_KTA_Timeline" HeaderText="Timeline" />
                            <asp:BoundField DataField="PD_KTA_Resources" HeaderText=" Resources / Preparation / Support needed" />
                            <asp:BoundField DataField="PD_KTA_RelevanceText" HeaderText=" Relevance to role" />
                           
                        </Columns>
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle CssClass="griditem_hilight" />
                        <HeaderStyle CssClass="gridheader_new" Height="25px" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
