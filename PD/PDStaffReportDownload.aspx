﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PDStaffReportDownload.aspx.vb" Inherits="PD_PDStaffReportDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
       <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Courses - Staff Level
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
     <table id="tbl_AddGroup" runat="server"  width="100%">
            <tr>
            <td >
                <table width="100%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal ID="ltLabel" runat="server" Text="Search Criteria"></asp:Literal></span>
                        </td>
                    </tr>
                    <tr id="trBusinessUnit" runat="server">
                        <td align="left" style="height: 21px">
                         <span class="field-label" >   Business Unit<//span>
                        </td>
                       
                        <td align="left" class="subheader" style="width: auto;">
                            <asp:DropDownList ID="ddlBusinessUnit" runat="server" AutoPostBack="True" Width="300px">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                           <span class="field-label" > Staff</span>
                        </td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" Width="200px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
            <td class="matters" style="height: 11px" valign="bottom" colspan="6">
                <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" OnClick="btnSearch_Click" />
                   <asp:Button ID="btnDownloadExcel" runat="server" Text="Download Excel" CssClass="button" OnClick="btnDownloadExcel_Click" />
            </td>
        </tr>
                    </table>
                </td>
                </tr>

           <tr id="trGridv" runat="server" visible="false">
            <td align="center">
                <table width="100%">
                    <tr class="subheader_img">
                        <td>
                           <span class="field-label" >
                                PD Course Info</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvPDEmployee" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="20" Width="100%" OnPageIndexChanging="gvPDEmployee_PageIndexChanging">
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Course Title">
                                       
                                        <ItemTemplate>
                                            <asp:Label ID="lblCMTITLE" runat="server" Text='<%# bind("CM_TITLE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Course Start Date">
                                      
                                        <ItemTemplate>
                                            <asp:Label ID="lblEventDate" runat="server" Text='<%# Bind("CM_EVENT_DT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Course End Date">
                                      
                                        <ItemTemplate>
                                            <asp:Label ID="lblEventEndDate" runat="server" Text='<%# Bind("CM_EVENT_END_DT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Attendance">
                                      
                                        <ItemTemplate>
                                            <asp:Label ID="lblAttendance" runat="server" Text='<%# Bind("Attendance")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Eval Submitted">
                                      
                                        <ItemTemplate>
                                            <asp:Label ID="lblEvalSubmitted" runat="server" Text='<%# Bind("FILL_EF")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </td>
                    </tr>
         </table>
                </td>
               </tr>
         </table>

     <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
                <asp:Label ID="lblNoAccess" runat="server" EnableViewState="False"
                    Font-Size="10px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
                </div></div></div>
</asp:Content>

