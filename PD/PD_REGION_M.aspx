﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PD_REGION_M.aspx.vb" Inherits="PD_PD_REGION_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
  <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>

<script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
 <script type="text/javascript">
     $(document).ready(function () {
         var hiddnCatgry = $("#" + '<%= hdnID.ClientID %>').val();

         if (hiddnCatgry > 0) {
             $("#tblCategory").show("slow");
             $("#trGridv").hide();
             $('#lblError').empty();
             $("#trAdd").hide();
         }

         else if (hiddnCatgry == 'a') {
             $("#tblCategory").show("slow");
             $("#trGridv").hide();
             $('#lblError').empty();
             $("#trAdd").hide();
         }
         else {

             $("#tblCategory").hide();
             $("#trGridv").show("slow");
             $("#trAdd").show("slow");

         }

         $(".frameActivity").fancybox({
             type: 'iframe',
             maxWidth: 300,
             maxHeight: 400,
             fitToView: false,
             width: '55%',
             height: '60%',
             autoSize: false,
             closeClick: false,
             openEffect: 'none',
             closeEffect: 'none'
         });

     });
     function fnAdd() {
         $("input:text").val('');

         $("#tblCategory").show("slow");
         $("#trGridv").hide();
         $('#lblError').empty();
         $("#trAdd").hide();
         $('#lblError').hide();
         //         $("#btnUpdateCategory").hide();
         //         $("#btnAddCategory").show();
     }
     function fnEdit() {
         //$("#dvGridUsers").hide("slow");
         $("#tblCategory").show("slow");
         return true;
     }
     function fnCancel() {

         $("input:text").val('');
         $("#tblCategory").hide("slow");
         $("#gvCategory td:nth-child(3)").show();
         $("#trGridv").show("slow");
         $("#trAdd").show("slow");
         $("#btnUpdateRegion").hide();
         $("#btnAddRegion").show("slow");
     }
  </script>
  <style>
      .GridPager a, .GridPager span {
          display: block;
          height: 15px;
          width: 15px;
          font-weight: bold;
          text-align: center;
          text-decoration: none;
      }

      .GridPager a {
          background-color: #f5f5f5;
          color: #969696;
          border: 1px solid #969696;
      }

      .GridPager span {
          background-color: #A1DCF2;
          color: #000;
          border: 1px solid #3AC0F2;
      }
  </style>
    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Regions
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table border="0" cellpadding="2" cellspacing="2" width="100%" style="border-style: none;
        border-width: 0px;">
        <tr>
            <td align="left" " valign="bottom">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table  id="tblCategory"  cellspacing="2" cellpadding="2" width="100%">
                    <tr align="left">
                        <td >
                             <span class="field-label" >Region Name<font color="maroon">*</font></span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRegionName" runat="server" Width="400px" MaxLength="80"></asp:TextBox>
                            <br />
                            <asp:RequiredFieldValidator ID="rfRegion" runat="server" ControlToValidate="txtRegionName"
                                CssClass="error" Display="Dynamic" ErrorMessage="Name required" ForeColor=""
                                SetFocusOnError="True" ValidationGroup="category">Name required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                   
                    <tr>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAddRegion" runat="server" Text="Add/Save" CssClass="button"
                                ValidationGroup="category" Width="130px"  />
                            <asp:Button ID="btnUpdateRegion" runat="server" Text="Update/Save" CssClass="button"
                                Width="130px" />
                         <%--  <asp:Button ID="btnCancel" runat="server"  Text="Cancel" CssClass="button" CausesValidation="False"
                                Width="70px" />--%>
                                <input type="button" class="button" id="btnCancel1" title="Cancel" value="Cancel" onclick="fnCancel()" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trAdd" >
        <td>
                   <%-- <a href="#" class="lnk" onclick="fnAdd()">Add New</a>--%>
                     <asp:LinkButton ID="lbtnAdd" runat="server" CausesValidation="false" 
                                    Text="Add New" OnClick="lbtnAdd_Click" ></asp:LinkButton>
                    </td>
        </tr>

        <tr id="trGridv" >
            <td align="center">
                <asp:GridView ID="gvREGION" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                   CssClass="table table-bordered table-row" DataKeyNames="R_ID" EmptyDataText="Record not available !!!"
                    HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvREGION_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="HideID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblR_ID" runat="server" Text='<%# Bind("R_ID") %>'></asp:Label>
                                <asp:Label ID="lblR_NAME" runat="server" Text='<%# bind("R_REGION_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Region">
                            <ItemTemplate>
                                <asp:Label ID="lblREGION" runat="server" Text='<%# bind("R_REGION_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" 
                                    Text="Edit" OnClick="lbtnEdit_Click" ></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Locations"  >
                            <ItemTemplate>
                                
                                    <a id="frameSubActivity" class="frameActivity" href="PD_ADD_LOCATIONS.aspx?ID=<%#Eval("R_ID")%>">
                               View/Add</a>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" />
                    <RowStyle CssClass="griditem" Height="25px" />
                    <SelectedRowStyle BackColor="Aqua" />
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
                <asp:HiddenField ID="hdnID" runat="server" />
            </td>
        </tr>
        </table>
                </div>
            </div>
         </div>
</asp:Content>

