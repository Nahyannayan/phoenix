﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Calendar
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Shared dtPD_Calendar As DataTable
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ''ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "CD00009") AndAlso (ViewState("MainMnu_code") <> "U000045")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ''added by nahyan for ESs on 23/10/2018
                    If ViewState("MainMnu_code") = "U000045" Then
                        lnkMyPd.Visible = True
                        lnkBackToESS.Visible = True
                    Else

                        lnkMyPd.Visible = False
                        lnkBackToESS.Visible = False

                    End If
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ''load cal year

                    Dim calyear As Integer = System.DateTime.Now.Year

                    For i As Integer = 0 To 100
                        ddlyear.Items.Add((calyear - i).ToString())
                    Next

                    Dim calmonth As Integer = System.DateTime.Now.Month
                    ddlmonth.Items.FindByValue(calmonth).Selected = True
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    calendarPD.VisibleDate = DateTime.Today
                    calendarPD.SelectedDate = DateTime.Today
                    dtPD_Calendar = BindPD_Calendar_Events()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub calendarPD_DayRender(ByVal sender As Object, ByVal e As DayRenderEventArgs)

        Dim dtDate As New DateTime(e.Day.[Date].Year, e.Day.[Date].Month, e.Day.[Date].Day)


        Dim expression As String = "CM_EVENT_DT >= '" & dtDate & "' AND CM_EVENT_DT <= '" & dtDate & "'"

        If dtPD_Calendar IsNot Nothing AndAlso dtPD_Calendar.Rows.Count > 0 Then
            For Each item As DataRow In dtPD_Calendar.Select(expression)
                Dim content As String = item("CM_START_TIME").ToString() & " - " & item("CM_END_TIME").ToString() & ":- " & item("CM_TITLE").ToString()


                If item("CR_EMP_ID").ToString() <> 0 AndAlso item("CR_APPR_STATUS").ToString() <> "C" Then
                    e.Cell.Controls.Add(New LiteralControl("<br /><a href=""#"" alt=""already registered"" style=""text-decoration:none;color:white;min-width:100px;font-family:century gothic;"" onclick=""GoToPage('" & item("CM_ID") & "')""> <b>" & content & "</b></a>"))
                    e.Cell.CssClass = "calendarDays"

                Else
                    e.Cell.Controls.Add(New LiteralControl("<br /><a href=""#"" style=""text-decoration:none;color:white;min-width:100px;font-family:century gothic;"" onclick=""GoToPage('" & item("CM_ID") & "')""> <b>" & content & "</b></a><br/>"))
                    e.Cell.CssClass = "calendarDays"
                End If


            Next item
        End If


    End Sub

    Protected Sub calendarPD_VisibleMonthChanged(ByVal sender As Object, ByVal e As MonthChangedEventArgs)
        Dim str As String = e.ToString
    End Sub
    Protected Sub calendarPD_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        Dim str As String = e.ToString
    End Sub

    Function BindPD_Calendar_Events() As DataTable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlClient.SqlParameter

        Dim Dt As New DataTable

        param(0) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Calendar_Events_CAL_VIEW", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dt = ds.Tables(0)
        End If
        Return Dt
    End Function

    Protected Sub ddlmonth_SelectedIndexChanged(sender As Object, e As EventArgs)
        calendarPD.VisibleDate = New DateTime(Convert.ToInt32(ddlyear.SelectedValue), Convert.ToInt32(ddlmonth.SelectedValue), 1)
    End Sub

    Protected Sub lnkMyPd_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/PD/PDMyEventRequests.aspx?MainMnu_code=" + Encr_decrData.Encrypt("U000045"), False)
        Exit Sub

    End Sub

    Protected Sub lnkBackToESS_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/ESSDashboard.aspx", False)
        Exit Sub
    End Sub
End Class
