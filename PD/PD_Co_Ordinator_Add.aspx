﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="PD_Co_Ordinator_Add.aspx.vb"
    Inherits="PD_PD_Co_Ordinator_Add" MasterPageFile="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr class="title">
            <td style="height: 25px" align="left">
                <asp:Literal ID="ltHeader" runat="server" Text="PD CO ORDINATORS"></asp:Literal>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="2" cellspacing="2" width="60%" style="border-style: none;
        border-width: 0px;">
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; width: 778px; height: 133px" valign="top">
                <table class="BlueTableView" id="tblCategory" runat="server" border="1" cellspacing="2"
                    cellpadding="2" width="650px" >
                    <tr class="subheader_img">
                        <td align="left" colspan="2" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                PD CO ORDINATORS</span></font>
                        </td>
                    </tr>
                        <tr  id="trBSU" runat="server">
                        <td class="matters">
                            Business Unit<font color="maroon">*</font>
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="true" onchange="if(this.selectedIndex == 0)return false;" OnSelectedIndexChanged="ddlBusinessunit_SelectedIndexChanged">
                            </asp:DropDownList>
                            <br />
                        </td>
                    </tr>
                          <tr id="trGemStaff" runat="server" clientidmode="Static">
                        <td class="matters">
                            Employee
                        </td>
                        <td class="matters" align="left">
                            <asp:DropDownList ID="ddlEmployee" onchange="if(this.selectedIndex == 0)return false;"
                                runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                      <tr>
                        <td class="matters">
                           Employee No
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtEmpNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                               <tr>
                        <td class="matters">
                            First Name
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            Last Name
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            Email
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            Mobile Number
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                          <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAddPDC" runat="server" Text="Add/Save" CssClass="button" ValidationGroup="category"
                                Width="130px"   />
                            <asp:Button ID="btnUpdatePDC" runat="server" Text="Update/Save" CssClass="button"
                                Width="130px"  />
                             <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                            <asp:HiddenField ID="hdnID" runat="server"   />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
