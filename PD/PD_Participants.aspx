﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PD_Participants.aspx.vb"
    Inherits="PD_PD_Participants" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Participants </title>
 <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        function fancyClose() {

            parent.$.fancybox.close();
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table  id="tblCategory" cellspacing="2" cellpadding="2" width="100%" >
            <tr id="trLabelError">
                <td align="left" class="matters" valign="bottom" colspan="2">
                    <div id="lblError" runat="server">
                    </div>
                </td>
            </tr>
            <tr class="subheader_img">
                <td align="left" colspan="2" valign="middle" class="title-bg">
                   <span style="font-family: Verdana">
                        Participants</span>
                </td>
            </tr>
            <tr id="trGridV" >
            <td colspan="2" style="width:100%">
                <table id="Table2" runat="server" align="center" border="0" 
                    cellpadding="5" cellspacing="0" style="width: 100%" >
                   
                    <tr>
                        <td align="center" style="width:100%">
                            <asp:GridView ID="gvParticipants" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging ="gvParticipants_PageIndexChanging"   >
                               
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="CR_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCRid" runat="server" Text='<%# Bind("CR_ID") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BusinessUnit") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP No" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EMPNO") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Name" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPLOYEE") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Email" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMAIL") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("PHONE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Designation">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                   
                                </Columns>
                                  <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
            </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
