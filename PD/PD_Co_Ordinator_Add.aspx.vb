﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Co_Ordinator_Add
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If ViewState("datamode") = "add" Then
                    btnUpdatePDC.Visible = False
                    ViewState("PDC_ID") = 0
                ElseIf ViewState("datamode") = "edit" Then
                    btnAddPDC.Visible = False
                    btnUpdatePDC.Visible = True

                    ViewState("PDC_ID") = Encr_decrData.Decrypt(Request.QueryString("pdcId").Replace(" ", "+"))
                End If
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD00088") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    '' ViewState("SEMP_ID") = 0

                    bindBusinessUnits()

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))


                    If ViewState("PDC_ID") <> 0 Then
                        BindPD_PD_Co_OrdinatorsById()
                    End If

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights                   

                    ' h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub


    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Try
            Dim BSU_ID As String
            BSU_ID = ddlBusinessunit.SelectedValue

            BindGemsStaffListForPDC(BSU_ID)
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmployee.SelectedIndexChanged
        Try

            BindGemsStaffInfoById(ddlEmployee.SelectedValue)
            

        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click
        ViewState("PDC_ID") = 0


        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            Dim msg As String
            msg = "1"
            msg = Encr_decrData.Encrypt(msg)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_CO_ORDINATOR_M.aspx?MainMnu_code={0}&datamode={1}&msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub btnUpdatePDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePDC.Click

        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
           
            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            Dim msg As String
            msg = "1"
            msg = Encr_decrData.Encrypt(msg)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_CO_ORDINATOR_M.aspx?MainMnu_code={0}&datamode={1}&msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
            Response.Redirect(url)
        End If
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_CO_ORDINATOR_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@UserID", Session("sUsr_id"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GETBSU_BY_USER_ACCES_BSU", param)



        ddlBusinessunit.DataSource = ds
        ddlBusinessunit.DataTextField = "bsu_name"
        ddlBusinessunit.DataValueField = "bsu_id"
        ddlBusinessunit.DataBind()
        ddlBusinessunit.Items.Insert(0, New ListItem("-Select School-", "0"))
        ddlBusinessunit_SelectedIndexChanged(ddlBusinessunit, Nothing)
    End Sub

    Sub BindGemsStaffListForPDC(ByVal BSU_ID As String)
        ddlEmployee.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Gems_Staff_For_PDC", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEmployee.DataSource = ds
            ddlEmployee.DataTextField = "EMPLOYEE"
            ddlEmployee.DataValueField = "EMP_ID"
            ddlEmployee.DataBind()

            ddlEmployee.Items.Insert(0, New ListItem("-Select Employee-", "0"))

            If (ViewState("EMP_ID") > 0) Then
                ddlEmployee.SelectedValue = ViewState("EMP_ID")
            End If
        Else
            ddlEmployee.Items.Insert(0, New ListItem("-No Employee Exists-", "0"))

        End If
    End Sub

 

    Sub BindGemsStaffInfoById(ByVal EmpID As Integer)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EMP_ID", EmpID)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Gems_Staff_Info_For_PDC_byId", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)
            txtFName.Text = Dt.Rows(0)("EMP_FNAME").ToString()
            txtLName.Text = Dt.Rows(0)("EMP_LNAME").ToString()
            txtEmail.Text = Dt.Rows(0)("EMAIL").ToString()
            txtMobile.Text = Dt.Rows(0)("PHONE").ToString()
            txtEmpNo.Text = Dt.Rows(0)("EMPNO").ToString()

        End If
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim PDC_ID As String = ViewState("PDC_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(9) As SqlParameter
                param(0) = New SqlParameter("@PDC_ID", PDC_ID)

                param(1) = New SqlParameter("@PDC_FNAME", txtFName.Text.Trim)
                param(2) = New SqlParameter("@PDC_LNAME", txtLName.Text.Trim)
                param(3) = New SqlParameter("@PDC_EMP_ID", ddlEmployee.SelectedValue)

                'Dim bsuId As String = ddlBusinessunit.SelectedValue


                param(4) = New SqlParameter("@PDC_EMAIL_ID", txtEmail.Text.Trim)
                param(5) = New SqlParameter("@PDC_MOB_NO", txtMobile.Text.Trim)
                param(6) = New SqlParameter("@PDC_BSU_ID", ddlBusinessunit.SelectedValue)

                param(7) = New SqlParameter("@PDC_EMP_NO", txtEmpNo.Text.Trim)

                param(8) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_PD_COORDINATOR", param)
                Dim ReturnFlag As Integer = param(8).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    'ResetAll()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function

    Sub BindPD_PD_Co_OrdinatorsById()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlClient.SqlParameter

            Dim Dt As New DataTable

            param(0) = New SqlParameter("@PDC_ID", ViewState("PDC_ID"))
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PD_CORDINATORS", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                txtFName.Text = Dt.Rows(0)("PDC_FNAME").ToString
                txtLName.Text = Dt.Rows(0)("PDC_LNAME").ToString
                txtEmail.Text = Dt.Rows(0)("PDC_EMAIL_ID").ToString
                txtMobile.Text = Dt.Rows(0)("PDC_MOB_NO").ToString
                txtEmpNo.Text = Dt.Rows(0)("PDC_EMP_NO").ToString

                If Not ddlBusinessunit.Items.FindByValue(Dt.Rows(0)("PDC_BSU_ID").ToString) Is Nothing Then
                    ddlBusinessunit.ClearSelection()
                    ddlBusinessunit.Items.FindByValue(Dt.Rows(0)("PDC_BSU_ID").ToString).Selected = True


                    'ddlBusinessunit.SelectedValue = Dt.Rows(0)("SEMP_BSU_ID").ToString()
                    BindGemsStaffListForPDC(Dt.Rows(0)("PDC_BSU_ID").ToString())
                    If Dt.Rows(0)("PDC_EMP_ID") IsNot Nothing AndAlso Dt.Rows(0)("PDC_EMP_ID").ToString() <> "" Then
                        ddlEmployee.SelectedValue = Dt.Rows(0)("PDC_EMP_ID").ToString()

                    End If

                End If

                If Not ddlEmployee.Items.FindByValue(Dt.Rows(0)("PDC_EMP_ID").ToString) Is Nothing Then
                    ddlEmployee.ClearSelection()
                    ddlEmployee.Items.FindByValue(Dt.Rows(0)("PDC_EMP_ID").ToString).Selected = True
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BindPD_PD_Co_OrdinatorsById")
        End Try

    End Sub

End Class
