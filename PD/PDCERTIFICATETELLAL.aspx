﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PDCERTIFICATETELLAL.aspx.vb" Inherits="PD_PDCERTIFICATETELLAL" %>

<html>
<head>
<meta charset="UTF-8">
<title>HTML certificate</title>
<link href="Styles/main.css" rel="stylesheet" type="text/css">
<style type="text/css">
.main_div {
	height: 668px;
	width: 952px;
	margin-top: 150px;
	margin-right: auto;
	margin-left: auto;
	border: 0.3px solid #D6D6D6;
	background-image: url(images/tellal_HTML_certificate952.png);
	-webkit-box-shadow: 0px 0px 10px #DADADA;
	box-shadow: 0px 0px 10px #DADADA;
}
.logo_header {
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	background-color: #FFFFFF;
	background-position: 0px right;
	height: 78px;
	width: 305px;
	margin-top: 50px;
	/* [disabled]float: right; */
	margin-left: 607PX;
	background-image: url(url);
	margin-right: 40px;
}
.header2 {
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
}
.textCtoawar {
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 16px;
	font-style: normal;
	float: left;
	margin-left: 40px;
	color: #868685;
	width: 500px;
	height: 60px;
}
.name_DIV {
	width: 900px;
	margin-left: 40px;
	height: 52px;
	margin-top: 90px;
	color: #868685;
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-variant: normal;
	font-size: 50px;
	margin-bottom: 20px;
	font-style: normal;
	font-weight: lighter;
}
.acknowledge {
	float: left;
	height: auto;
	width: 900px;
	margin-left: 40px;
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	color: #868685;
	font-size: 20px;
	margin-top: 0px;
	font-style: normal;
}
.date_DIV {
	margin-top: 15px;
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 18px;
	font-style: normal;
	line-height: 1.7px;
	color: #868685;
	float: left;
	width: 900px;
	margin-left: 40px;
	height: 20px;
}
</style>
</head>




<body>
<div class="main_div">
  <div class="logo_header"> <img src="images/tellal_HTML_certificate_logo.png" width="303" height="77" alt=""/></div>
  <div class="textCtoawar">CERTIFICATE OF
  <br>
  <span style="color: #BACF71; font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-style: normal; font-size: 30px;">ATTENDANCE</span>
   <br><br>
  <span style="font-size: 14px;">Awarded to</span> </div>
  <div class="name_DIV">Participants Name</span></div>
  <div class="acknowledge"><span style="font-weight: normal">In recognition of successful completion of</span><br>
  <span style="color: #98BC38; font-weight: normal; line-height: 1.8;">TF4_Effective Assessment Strategies
</span> </div>
  <div class="date_DIV">Date of Completion</div>
  
  
  
  
</div>



</body>
</html>
