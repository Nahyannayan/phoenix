﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PD_Certificate_Corporate.aspx.vb" Inherits="PD_PD_Certificate_Corporate" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Certificates
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" >
                <tr>
            <td class="matters" style="height: 3px" valign="bottom" colspan="6">
                <table width= "100%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal ID="ltLabel" runat="server" Text="Search Criteria"></asp:Literal></span>
                        </td>
                    </tr>
                    </table>
                </td>
                    </tr>
        <tr>
            <td >  <span class="field-label" > Academic Year</span></td>
            
            <td >

                <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged"></asp:DropDownList>
            </td>

                        <td >
                          <span class="field-label" >    Course </span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlCourse" runat="server"  Width="400px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvCourse" ValidationGroup="AttGroup" runat="server" ErrorMessage="Please select" ControlToValidate="ddlCourse" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
        <tr id="trSubject" runat="server" visible="false">
                        <td >
                             <span class="field-label" >  From Date</span>
                        </td>
                       
                        <td align="left" style="width: 141px; height: 21px">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="90px"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtFromDate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                Visible="False"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server"
                                    ControlToValidate="txtFromDate" CssClass="error" Display="Dynamic" ErrorMessage="From Date required"
                                    ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 21px" class="matters">
                            <span class="field-label" >   To Date</span>
                        </td>
                      
                        <td align="left" style="height: 21px">
                            <asp:TextBox ID="txtToDate" runat="server" Width="90px"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgCalendar" TargetControlID="txtToDate" CssClass="MyCalendar">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;<asp:ImageButton ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"
                                Visible="False"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="rfvDate0" runat="server" ControlToValidate="txtToDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
        <tr>
            <td colspan="6">

                <asp:Button ID="btnSearch" runat="server" Text="SEARCH" CssClass="button"  ValidationGroup="AttGroup" />
            </td>

        </tr>
        </table>

     <table id="tblGridView" runat="server"  width="100%" >
         <tr>

             <td colspan="6">
                 <asp:GridView ID="gvParticipantList" runat="server"  AutoGenerateColumns="False" CssClass="table table-bordered table-row" OnPageIndexChanging="gvParticipantList_PageIndexChanging"
                    Height="100%" Width="100%" EnableModelValidation="True"  AllowPaging="true" PageSize="25"  HeaderStyle-Height="30"  OnRowDataBound="gvParticipantList_RowDataBound" >
                   
                     <Columns>
                           <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                           <asp:TemplateField HeaderText="Title">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server"  Text='<%# Bind("CM_TITLE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDate" runat="server"  Text='<%# Bind("COMBINED_DATE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Participant">
                            <ItemTemplate>
                                <asp:Label ID="lblParticipant" runat="server"  Text='<%# Bind("EMPLOYEE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="KTAA">
                            <ItemTemplate>
                                <asp:Label ID="lblKTAA" runat="server"  Text='<%# Bind("KTAA_FILLED")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="EVAL">
                            <ItemTemplate>
                                <asp:Label ID="lblEVAL" runat="server"  Text='<%# Bind("EVAL_FILLED")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Certificate">
                            <ItemTemplate>
                                 
                                     <asp:LinkButton ID="lnkCertific" runat="server" 
                                    OnClick="lnkCertific_Click" CommandArgument='<%# Bind("CR_ID") %>'>Print</asp:LinkButton>&nbsp;
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>

                           <asp:TemplateField HeaderText="Certificate (PDF)">
                            <ItemTemplate>
                                  
                                    <asp:LinkButton ID="lnkCertificatePdf" runat="server"
                                    OnClick="lnkCertificatePdf_Click"   CommandArgument='<%# Bind("CR_ID")%>'>Download</asp:LinkButton>&nbsp;
                                    
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                     </Columns>
                        <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop" Height="15px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Font-Size="11px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Height="25px" />
                 </asp:GridView>
             </td>
         </tr>
         </table>

    <table id="tblNoAccess" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%" visible="false">
        <tr>
            <td align="center">
               
                   
                        <asp:Label id="lblNoAccess" runat="server"  EnableViewState="False"
                            Font-Size="10px"></asp:Label>
                  
                        
              
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
                <//div></div></div>
      <cr:crystalreportsource id="rs" runat="server" cacheduration="1">
    </cr:crystalreportsource>
</asp:Content>

