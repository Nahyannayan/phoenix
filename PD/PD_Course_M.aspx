﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="PD_Course_M.aspx.vb" Inherits="PD_PD_Course_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .GridPager a, .GridPager span
        {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }
        .GridPager a
        {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }
        .GridPager span
        {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>
     <link rel="stylesheet" type="text/css" href="Styles/msgBoxLight.css"
         /> 
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.msgBox.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
       
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameParticipant").fancybox({
                type: 'iframe',
                maxWidth: 200,
                maxHeight: 300,
                fitToView: false,
                width: '100%',
                height: '100%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });


        function deleteItem(lnk) {
           
            $.msgBox({
                title: "Confirm Enable PD KTAA",
                content: "Are you sure you want to Enable KTAA For this Course?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No"}],
                success: function (result) {
                    if (result == "Yes") {
                        __doPostBack(lnk, '')
                    }
                }
            });
        }

       
    </script>
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Literal ID="ltHeader" runat="server" Text="PD Courses"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
    <table border="0" cellpadding="2" cellspacing="2" width="100%" style="border-style: none;
        border-width: 0px;">
        <tr>
            <td align="left" class="matters" valign="bottom">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr id="trAdd">
            <td>
               
                    <%-- <a href="#" class="lnk" onclick="fnAdd()">Add New</a>--%>
                    <asp:LinkButton ID="lbtnAdd" runat="server" CausesValidation="false" Text="Add New"
                        OnClick="lbtnAdd_Click"></asp:LinkButton>
                
            </td>
        </tr>
        <tr id="trGridv">
            <td align="center" width="100%">
                <asp:GridView ID="gvCourse" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row" DataKeyNames="CM_ID" EmptyDataText="Record not available !!!"
                    HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvCourse_PageIndexChanging"
                    OnRowDataBound="gvCourse_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="No." >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                <asp:Label ID="lblCM_ID" runat="server" Text='<%# bind("CM_ID") %>' Visible="false"></asp:Label>
                                  <asp:Label ID="lblKTA" runat="server" Text='<%# bind("CM_KTAA_ENABLED") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblEValuation" runat="server" Text='<%# bind("CM_EF_ENABLED") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblCertificate" runat="server" Text='<%# bind("CM_CERTIFICATE_ENABLED") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Title">
                            <HeaderTemplate>
                                
                                               <span class="field-label" >   Title</span><br />
                                            
                                                <asp:TextBox ID="txtTitle" runat="server" ></asp:TextBox>
                                            
                                                <asp:ImageButton ID="btnSearchTitle" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                    ImageAlign="Top" OnClick="btnSearchTitle_Click"></asp:ImageButton>
                                            
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" Text='<%# bind("CM_Title") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Code">
                            <HeaderTemplate>
                               
                                                <span class="field-label" >  Code</span> <br />
                                         
                                                <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                          
                                                <asp:ImageButton ID="btnSearchCode" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                    ImageAlign="Top" OnClick="btnSearchCode_Click"></asp:ImageButton>
                                          
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCode" runat="server" Text='<%# bind("CM_Code") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Region">
                            <ItemTemplate>
                                <asp:Label ID="lblRegion" runat="server" Text='<%# bind("CM_REGION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDate" runat="server" Text='<%# bind("CM_Date") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="Edit"
                                    OnClick="lbtnEdit_Click"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Participants" ShowHeader="False">
                            <ItemTemplate>
                                <a id="framePartcpnt" class="frameParticipant" href="PD_Participants.aspx?ID=<%#Eval("CM_ID")%>">
                                    View</a> |
                                <asp:LinkButton ID="lbtnExcel" runat="server" CausesValidation="false" Text="Download"
                                    OnClick="lbtnExcel_Click"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText=" &nbsp;KTAA ">
                            <ItemTemplate>
                               
                                     <asp:LinkButton ID="lbtnEnableKTA" runat="server" OnClick="lbtnEnableKTA_Click"  onclientclick="return confirm('Are you sure you want to enable KTAA?');" CommandArgument='<%# Eval("CM_ID") %>' Text="Enable"></asp:LinkButton>
                            </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="EVALUATION">
                            <ItemTemplate>
                               
                                     <asp:LinkButton ID="lbtnEnableEvaluation" runat="server" OnClick="lbtnEnableEvaluation_Click"  onclientclick="return confirm('Are you sure you want to enable Evaluation form?');" CommandArgument='<%# Eval("CM_ID") %>' Text="Enable"></asp:LinkButton>
                            </ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="CERTIFICATE">
                            <ItemTemplate>
                               
                                     <asp:LinkButton ID="lbtnEnableCertificate" runat="server" OnClick="lbtnEnableCertificate_Click"  onclientclick="return confirm('Are you sure you want to enable CERTIFICATE?');" CommandArgument='<%# Eval("CM_ID") %>' Text="Enable"></asp:LinkButton>
                            </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                  
                    <RowStyle CssClass="griditem" Height="25px" />
                    <SelectedRowStyle BackColor="Aqua" />
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
                <asp:HiddenField ID="hdnID" runat="server" />
            </td>
        </tr>
    </table>
                </div></div></div>
</asp:Content>
