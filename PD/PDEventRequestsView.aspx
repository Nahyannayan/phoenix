<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="PDEventRequestsView.aspx.vb" Inherits="PD_PDEventRequestsView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Course Requests
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
    <table id="tbl_AddGroup" runat="server" align="center"  width="100%">
        <tr>
            <td align="left">
                <span style="display: block; left: 0px; float: left">
                    <div align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label><span style="color: #c00000">&nbsp;</span></div>
                    <div align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"   ForeColor="" ValidationGroup="AttGroup">
                        </asp:ValidationSummary>
                        <span style="  color: #800000">&nbsp;</span></div>
                </span>
            </td>
        </tr>
        <tr id="gridtable" runat="server" visible="false">
            <td class="matters" style="height: 3px" valign="bottom">
                <table align="center" border="1" width="100%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <span style=" color: #ffffff">
                                <asp:Literal ID="ltLabel" runat="server" Text="Search Criteria"></asp:Literal></span>
                        </td>
                    </tr>
                    <tr id="trBusinessUnit" runat="server">
                        <td align="left" style="height: 21px">
                          <span class="field-label" >    Business Unit</span>
                        </td>
                       
                        <td align="left" class="subheader" style="width: auto;">
                            <asp:TextBox ID="txtBusinessUnit" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td class="subheader">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="trAcademicYear" runat="server">
                        <td align="left" style="height: 21px">
                         <span class="field-label" >     Region</span>
                        </td>
                        
                        <td align="left" style="width: 141px; height: 21px">
                            <asp:TextBox ID="txtRegion" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="left" style="height: 21px">
                            <span class="field-label" >  Location</span>
                        </td>
                       
                        <td align="left" style="height: 21px">
                            <asp:TextBox ID="txtLocation" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left" style="height: 21px">
                           <span class="field-label" >   From Date</span>
                        </td>
                      
                        <td align="left" style="width: 141px; height: 21px">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="90px" ReadOnly="True"></asp:TextBox>
                            &nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                Visible="False"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server"
                                    ControlToValidate="txtFromDate" CssClass="error" Display="Dynamic" ErrorMessage="From Date required"
                                    ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 21px">
                           <span class="field-label" >   To Date</span>
                        </td>
                      
                        <td align="left" style="height: 21px">
                            <asp:TextBox ID="txtToDate" runat="server" Width="90px" ReadOnly="True"></asp:TextBox>
                            &nbsp;<asp:ImageButton ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"
                                Visible="False"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="rfvDate0" runat="server" ControlToValidate="txtToDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="trSubject1" runat="server">
                        <td align="left" style="height: 21px">
                           <span class="field-label" >   Event Title</span>
                        </td>
                     
                        <td align="left" style="width: 141px; height: 21px">
                            <asp:TextBox ID="txtTitle" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td class="subheader">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 11px" valign="bottom">
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 11px" valign="bottom">
                <asp:RadioButton ID="optPending" runat="server" Text="Pending Requests" AutoPostBack="True"
                    GroupName="ApprovalGroup" />
                <asp:RadioButton ID="optApproved" runat="server" Text="Approved Requests" AutoPostBack="True"
                    GroupName="ApprovalGroup" />
                <asp:RadioButton ID="optRejected" runat="server" Text="Rejected Requests" AutoPostBack="True"
                    GroupName="ApprovalGroup" />
                 <asp:RadioButton ID="optCancelled" runat="server" Text="Cancelled Requests" AutoPostBack="True"
                    GroupName="ApprovalGroup" />
            </td>
        </tr>
        
        
        <tr style=" color : Red">
                <td class="matters" align="center" width="50%">
                   &nbsp;
                </td>
            </tr>
            
            
        <tr style=" color : Red">
                <td class="matters" align="center" width="50%">
                    <asp:Literal ID="ltCancel"  runat="server" Visible="false" Text="<font style='color:Red'>*Participants cancellation must be made 3 days prior to the course. Non attendees with unacceptable reasons will be charged for resources  printing and catering. </font>"></asp:Literal>
                </td>
            </tr>
            
            
        <tr>
            <td class="matters" style="height: 11px" valign="bottom" align="left">
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All"
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 11px" valign="bottom">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 7px" valign="bottom" align="center">
                <asp:GridView ID="gvRequest" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    Height="100%" Width="100%" EnableModelValidation="True" AllowPaging="True" DataKeyNames="ID"
                    SkinID="GridViewView">
                  
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" __designer:wfdid="w18" Text='<%# bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkRow" runat="server" AutoPostBack="True" Checked='<%# BIND("SELECTED") %>'
                                    OnCheckedChanged="chkRow_CheckedChanged" />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkRowHeader" runat="server" AutoPostBack="True" OnCheckedChanged="chkRowHeader_CheckedChanged"
                                    Visible="False" />
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SR. NO">
                            <ItemTemplate>
                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("SR_NO") %>' __designer:wfdid="w18"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Title" >
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" _="" Text='<%# bind("Title") %>'></asp:Label>
                                <asp:Literal ID="ltStar0" runat="server" __designer:wfdid="w7"></asp:Literal>
                            </ItemTemplate>
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <HeaderStyle Height="25px" HorizontalAlign="Center" Wrap="True" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Date" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblEventDate" runat="server" __designer:wfdid="w18" Text='<%# Eval("EVENT_DATE", "{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Location &amp; Region" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblLocationRegion" runat="server" __designer:wfdid="w6" Text='<%# bind("Location_Region") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Business Unit">
                            <ItemTemplate>
                                <asp:Label ID="lblBsuName" runat="server" __designer:wfdid="w18" Text='<%# bind("BSU_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Request Date">
                            <ItemTemplate>
                                <asp:Label ID="lblRequestDate" runat="server" __designer:wfdid="w18" Text='<%# Eval("REQUEST_DATE", "{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee No">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNo" runat="server" __designer:wfdid="w18" Text='<%# bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee Name">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpName" runat="server" __designer:wfdid="w18" Text='<%# bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpEmail" runat="server" Text='<%# bind("EMP_EMAIL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks" Visible="False">
                            <ItemTemplate>
                                <asp:TextBox ID="txtRemarks" runat="server" Height="50px" TextMode="MultiLine"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" __designer:wfdid="w18" Text='<%# bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField SelectText="Approve/Reject" ShowSelectButton="True" Visible="False" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table cellpadding="0" style="width: 100%">
                                    <tr>
                                        <td align="center">
                                            <asp:LinkButton ID="lblApprove" runat="server" CommandName="Select" CommandArgument='<%# Eval("ID") %>'>Approve</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table cellpadding="0" style="width: 100%">
                                    <tr>
                                        <td align="center">
                                            <asp:LinkButton ID="lblReject" runat="server" CommandArgument='<%# Eval("ID") %>'
                                                CommandName="Select" OnClientClick="return confirm('Are you sure you want to reject the selected PD request?');">Reject</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table cellpadding="0" style="width: 100%">
                                    <tr>
                                        <td align="center">
                                            <asp:LinkButton ID="lblCancel" runat="server" CommandArgument='<%# Eval("ID") %>'
                                                CommandName="Select" OnClientClick="return confirm('Are you sure you want to cancel the selected PD request?');">Cancel</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop" Height="15px" HorizontalAlign="Center" VerticalAlign="Middle"
                         />
                    <AlternatingRowStyle CssClass="griditem_alternative" Height="25px" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 22px" valign="bottom">
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfBsuId" runat="server" Value="''"></asp:HiddenField>
    <asp:HiddenField ID="hfRegionId" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hfLocationId" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hfCmId" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hfEmpId" runat="server" Value="0"></asp:HiddenField>
</asp:Content>
