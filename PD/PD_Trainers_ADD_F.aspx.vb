﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Trainers_ADD_F
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'If Request.QueryString("val") IsNot Nothing And Request.QueryString("val") <> "" Then
            'Dim id As Integer = Convert.ToInt32(Request.QueryString("val").ToString())
            'ViewState("CM_ID") = id

            BindGemsStaffListForPDC()

        End If
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        BindGemsStaffListForPDC()
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblEMPID As New Label
        Dim lblBsu_id As New Label
        Dim EmpNo As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMP_ID"), Label)
        lblBsu_id = TryCast(sender.FindControl("lblBsu_id"), Label)
        EmpNo = TryCast(sender.FindControl("lblEmp_No"), Label)
        ViewState("EMP_ID") = lblEMPID.Text


        Dim errormsg As String
        Dim callTransaction As Integer

        Dim tran As SqlTransaction

        Dim PDC_ID As String = ViewState("PDT_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter
                'param(0) = New SqlParameter("@PDC_ID", 0)

                param(0) = New SqlParameter("@PDT_EMP_ID", lblEMPID.Text.Trim)
                param(1) = New SqlParameter("@EMP_BSU_ID", lblBsu_id.Text.Trim)


                'Dim bsuId As String = ddlBusinessunit.SelectedValue


                param(3) = New SqlParameter("@PDT_EMP_NO", EmpNo.Text.Trim)


                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_PD_TRAINERS", param)
                Dim ReturnFlag As Integer = param(4).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"


                    'ResetAll()
                End If
            Catch ex As Exception

                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If

                If errormsg <> "" Then
                    lblError.InnerHtml = "<div class='error'>" & errormsg & "</div>"
                Else
                    lblError.InnerHtml = "<div class='alert alert-success'>Record saved successfully !!!</div>"
                End If
            End Try

        End Using

    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click



        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div class='error'>" & errormsg & "</div>"

        Else


            lblError.InnerHtml = "<div class='alert alert-success'>Record saved successfully !!!</div>"

        End If
    End Sub


    Protected Sub btnSearchEMP_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindGemsStaffListForPDC()
    End Sub

    Protected Sub btnSearchEMP_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindGemsStaffListForPDC()
    End Sub

    Protected Sub btnSearchBSU_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindGemsStaffListForPDC()
    End Sub


    Sub BindGemsStaffListForPDC()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter
        Dim str_EMP_NO As String = String.Empty
        Dim str_EMP_Name As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim EMP_NO As String = String.Empty
        Dim EMP_Name As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Try
            If gvPDC.Rows.Count > 0 Then

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NO")

                If txtSearch.Text.Trim <> "" Then
                    EMP_NO = " AND replace(EMPNO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_EMP_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_Name")

                If txtSearch.Text.Trim <> "" Then

                    EMP_Name = " AND replace(EMP_FNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' OR replace(EMP_LNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"


                    str_EMP_Name = txtSearch.Text.Trim
                End If

                txtSearch = gvPDC.HeaderRow.FindControl("txtBSU_Name")

                If txtSearch.Text.Trim <> "" Then

                    BSU_Name = " AND replace(BSU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "


                    str_BSU_Name = txtSearch.Text.Trim
                End If

            End If
            FILTER_COND = EMP_NO + EMP_Name + BSU_Name

            param(0) = New SqlParameter("@FILTERCONDITION", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Gems_Employee_For_PDC_2", param)

            If ds.Tables(0).Rows.Count > 0 Then

                gvPDC.DataSource = ds.Tables(0)
                gvPDC.DataBind()
            Else


                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvPDC.DataSource = ds.Tables(0)
                Try
                    gvPDC.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "No Records vailable !!!"
            End If
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        Dim tran As SqlTransaction



        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Dim EMP_IDs As String = String.Empty
            Dim IsNewGroup As Boolean = False

            Dim ReturnFlag As Integer
            Dim param(4) As SqlParameter
            Try



                For Each gvrow As GridViewRow In gvPDC.Rows
                    Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
                    If chk IsNot Nothing And chk.Checked Then
                        EMP_IDs += gvPDC.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

                    End If
                Next

                ''checking if seat is available

                EMP_IDs = EMP_IDs.Trim(",".ToCharArray())
                param(0) = New SqlParameter("@PDT_EMP_IDs", EMP_IDs)


                param(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(1).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_PD_TRAINERS_BULK", param)
                ReturnFlag = param(1).Value
                callTransaction = ReturnFlag
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message

            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()


                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()

                Else
                    errormsg = ""
                    tran.Commit()
                End If

            End Try
        End Using
    End Function
End Class
