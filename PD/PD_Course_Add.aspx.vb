﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_Course_Add
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If ViewState("datamode") = "add" Then
                    btnUpdateCourse.Visible = False
                    ViewState("CM_ID") = 0
                ElseIf ViewState("datamode") = "edit" Then
                    btnUpdateCourse.Visible = True
                    btnAddCourse.Visible = False

                    ViewState("CM_ID") = Encr_decrData.Decrypt(Request.QueryString("cmId").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CD00085") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    BindRegion(1)
                    BindRegion(2)
                    BindRegion(3)
                    BindRegion(4)
                    BindRegion(5)
                    BindCoursetype()
                    'BindTrainers()
                    Me.gvEMPName.DataSource = Nothing
                    Me.gvEMPName.DataBind()

                    Me.txtMAxCapacity.Text = 30
                    Me.txtTime.Text = "9:00 AM"
                    Me.txtTimeTo.Text = "4:00 PM"

                    If ViewState("CM_ID") <> 0 Then
                        Get_Calendar_Event_ById(Convert.ToInt32(ViewState("CM_ID")))
                    End If

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    ' txtTrainerIDs.Attributes.Add("ReadOnly", "ReadOnly")
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub btnAddCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCourse.Click
        ViewState("CM_ID") = 0

        btnUpdateCourse.Visible = False
        Dim errormsg As String = String.Empty

        Dim stDate As Date, EnDate As Date
        If txtFrom.Text <> "" AndAlso txtTodate.Text <> "" Then
            If IsDate(txtFrom.Text) AndAlso IsDate(txtTodate.Text) Then
                stDate = txtFrom.Text
                EnDate = txtTodate.Text

                If stDate > EnDate Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Start Date should not be greater than End Date.</div>"
                    Exit Sub
                ElseIf EnDate < stDate Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>End Date must be greater than start date</div>"
                    Exit Sub
                End If
            End If
        End If

        If Is_code_Exists() Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Course Code already exists.</div>"
        Else
            If callTransaction(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

            Else

                'lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"

                Dim url As String
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                'define the datamode to view if view is clicked
                ViewState("datamode") = "view"
                'Encrypt the data that needs to be send through Query String
                Dim msg As String
                msg = "1"
                msg = Encr_decrData.Encrypt(msg)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                url = String.Format("~\PD\PD_Course_M.aspx?MainMnu_code={0}&datamode={1}&msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
                Response.Redirect(url)


            End If
        End If

    End Sub

    Protected Sub btnUpdateCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCourse.Click


        btnAddCourse.Visible = False
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            'lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            Dim msg As String
            msg = "1"
            msg = Encr_decrData.Encrypt(msg)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_Course_M.aspx?MainMnu_code={0}&datamode={1}&msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
            Response.Redirect(url)


        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            Dim url As String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\PD\PD_Course_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
        End Try
    End Sub

    Protected Sub ddlRegion1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion1.SelectedIndexChanged
        BindLocation(1)
    End Sub

    Protected Sub ddlRegion2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion2.SelectedIndexChanged
        BindLocation(2)
        trRegion2.Style.Add("display", "")
    End Sub

    Protected Sub ddlRegion3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion3.SelectedIndexChanged
        BindLocation(3)
        trRegion3.Style.Add("display", "")
    End Sub

    Protected Sub ddlRegion4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion4.SelectedIndexChanged
        BindLocation(4)
        trRegion4.Style.Add("display", "")
    End Sub

    Protected Sub ddlRegion5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegion5.SelectedIndexChanged
        BindLocation(5)
        trRegion5.Style.Add("display", "")
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Dim CM_ID As String = ViewState("CM_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(20) As SqlParameter
                Dim RegionLoc As String = ""
                Dim LocStatus = ""
                param(0) = New SqlParameter("@CM_ID", CM_ID)
                param(1) = New SqlParameter("@title", txtTitle.Text)
                param(2) = New SqlParameter("@FromDate", Convert.ToDateTime(txtFrom.Text.Trim))
                param(3) = New SqlParameter("@StartTime", txtTime.Text.Trim)
                param(4) = New SqlParameter("@EndTime", txtTimeTo.Text.Trim)
                param(5) = New SqlParameter("@MaxCapacity", txtMAxCapacity.Text.Trim)
                param(6) = New SqlParameter("@OpenDate", Convert.ToDateTime(txtOnlineDate.Text.Trim))
                param(7) = New SqlParameter("@CloseDate", Convert.ToDateTime(txtCloseDate.Text.Trim))
                param(8) = New SqlParameter("@Description", Server.HtmlEncode(txtDesc.Content))

                param(9) = New SqlParameter("@Details", txtDetails.Text.Trim)
                param(10) = New SqlParameter("@EndDate", txtTodate.Text.Trim)
                param(11) = New SqlParameter("@Code", txtCode.Text.Trim)
                If ddlRegion1.SelectedValue <> 0 Then
                    RegionLoc = ddlRegion1.SelectedValue

                    If ddlLocation1.SelectedValue <> 0 Then
                        RegionLoc = RegionLoc & "|" & ddlLocation1.SelectedValue
                        LocStatus = ddlStatus1.SelectedValue & "|" & ddlLocation1.SelectedValue
                    End If
                End If

                If ddlRegion2.SelectedValue <> 0 Then
                    RegionLoc = RegionLoc & "," & ddlRegion2.SelectedValue

                    If ddlLocation2.SelectedValue <> 0 Then
                        RegionLoc = RegionLoc & "|" & ddlLocation2.SelectedValue
                        LocStatus = LocStatus & "," & ddlStatus2.SelectedValue & "|" & ddlLocation2.SelectedValue
                    End If
                End If
                If ddlRegion3.SelectedValue <> 0 Then
                    RegionLoc = RegionLoc & "," & ddlRegion3.SelectedValue

                    If ddlLocation3.SelectedValue <> 0 Then
                        RegionLoc = RegionLoc & "|" & ddlLocation3.SelectedValue
                        LocStatus = LocStatus & "," & ddlStatus3.SelectedValue & "|" & ddlLocation3.SelectedValue
                    End If
                End If
                If ddlRegion4.SelectedValue <> 0 Then
                    RegionLoc = RegionLoc & "," & ddlRegion4.SelectedValue

                    If ddlLocation4.SelectedValue <> 0 Then
                        RegionLoc = RegionLoc & "|" & ddlLocation4.SelectedValue
                        LocStatus = LocStatus & "," & ddlStatus4.SelectedValue & "|" & ddlLocation4.SelectedValue
                    End If
                End If

                If ddlRegion5.SelectedValue <> 0 Then
                    RegionLoc = RegionLoc & "," & ddlRegion5.SelectedValue

                    If ddlLocation5.SelectedValue <> 0 Then
                        RegionLoc = RegionLoc & "|" & ddlLocation5.SelectedValue
                        LocStatus = LocStatus & "," & ddlStatus5.SelectedValue & "|" & ddlLocation5.SelectedValue
                    End If
                End If
                param(12) = New SqlParameter("@RegionLocation", RegionLoc)
                param(13) = New SqlParameter("@LocationStatus", LocStatus)
                param(14) = New SqlParameter("@CreatedBy", Session("sUsr_id"))

                param(15) = New SqlParameter("@CourseValue", txtCourseValue.Text)
                param(16) = New SqlParameter("@CourseComments", txtValueComments.Text)
                param(19) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(19).Direction = ParameterDirection.ReturnValue

                Dim tmpstr As String = ""
                'For i As Integer = 0 To Me.chkTrainers.Items.Count - 1
                '    If chkTrainers.Items(i).Selected = True Then
                '        tmpstr &= chkTrainers.Items(i).Value & "|"
                '    End If
                'Next
                For i As Integer = 0 To Me.gvEMPName.Rows.Count - 1
                    tmpstr &= CType(Me.gvEMPName.Rows(i).FindControl("lblID"), Label).Text & "|"
                Next

                param(17) = New SqlParameter("@TRAINER_IDS", tmpstr)
                param(18) = New SqlParameter("@CourseType", Convert.ToInt32(ddlCourseType.SelectedValue))

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_T_SAVE_COURSE_M", param)
                Dim ReturnFlag As Integer = param(19).Value


                If CM_ID > 0 Then
                    If ddlStatus1.SelectedValue = "C" Then

                        If ReturnFlag = 0 Then
                            Dim params1(1) As SqlParameter
                            params1(0) = New SqlParameter("@CM_ID", CM_ID)

                            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_T.Send_Course_Cancel_Email", params1)
                        End If

                    End If
                End If
                

                


                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SSC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Sub BindRegion(ByVal region As Integer)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_ALL_REGIONS")
        If region = 1 Then
            ddlRegion1.Items.Clear()
            ddlRegion1.DataSource = ds
            ddlRegion1.DataTextField = "R_REGION_NAME"
            ddlRegion1.DataValueField = "R_ID"
            ddlRegion1.DataBind()
            ddlRegion1_SelectedIndexChanged(ddlRegion1, Nothing)
        ElseIf region = 2 Then
            ddlRegion2.Items.Clear()
            ddlRegion2.DataSource = ds
            ddlRegion2.DataTextField = "R_REGION_NAME"
            ddlRegion2.DataValueField = "R_ID"
            ddlRegion2.DataBind()
            ddlRegion2.Items.Insert(0, New ListItem("-Please Select-", "0"))
            '' ddlRegion2_SelectedIndexChanged(ddlRegion2, Nothing)
        ElseIf region = 3 Then
            ddlRegion3.Items.Clear()
            ddlRegion3.DataSource = ds
            ddlRegion3.DataTextField = "R_REGION_NAME"
            ddlRegion3.DataValueField = "R_ID"
            ddlRegion3.DataBind()
            ddlRegion3.Items.Insert(0, New ListItem("-Please Select-", "0"))
            '' ddlRegion3_SelectedIndexChanged(ddlRegion3, Nothing)

        ElseIf region = 4 Then
            ddlRegion4.Items.Clear()
            ddlRegion4.DataSource = ds
            ddlRegion4.DataTextField = "R_REGION_NAME"
            ddlRegion4.DataValueField = "R_ID"
            ddlRegion4.DataBind()
            ddlRegion4.Items.Insert(0, New ListItem("-Please Select-", "0"))
            '' ddlRegion4_SelectedIndexChanged(ddlRegion4, Nothing)

        ElseIf region = 5 Then
            ddlRegion5.Items.Clear()
            ddlRegion5.DataSource = ds
            ddlRegion5.DataTextField = "R_REGION_NAME"
            ddlRegion5.DataValueField = "R_ID"
            ddlRegion5.DataBind()
            ddlRegion5.Items.Insert(0, New ListItem("-Please Select-", "0"))
            '' ddlRegion5_SelectedIndexChanged(ddlRegion5, Nothing)

        End If



    End Sub

    Sub BindLocation(ByVal region As Integer)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim ds As DataSet
        Dim param(10) As SqlParameter
        If region = 1 Then
            param(0) = New SqlParameter("@regionId", Convert.ToInt32(ddlRegion1.SelectedValue))
        ElseIf region = 2 Then
            param(0) = New SqlParameter("@regionId", Convert.ToInt32(ddlRegion2.SelectedValue))
        ElseIf region = 3 Then
            param(0) = New SqlParameter("@regionId", Convert.ToInt32(ddlRegion3.SelectedValue))
        ElseIf region = 4 Then
            param(0) = New SqlParameter("@regionId", Convert.ToInt32(ddlRegion4.SelectedValue))
        ElseIf region = 5 Then
            param(0) = New SqlParameter("@regionId", Convert.ToInt32(ddlRegion5.SelectedValue))
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_ALL_LOCATIONS_BY_REGION", param)
        If region = 1 Then
            ddlLocation1.Items.Clear()
            ddlLocation1.DataSource = ds
            ddlLocation1.DataTextField = "L_DESCR"
            ddlLocation1.DataValueField = "L_ID"
            ddlLocation1.DataBind()
        ElseIf region = 2 Then
            ddlLocation2.Items.Clear()
            ddlLocation2.DataSource = ds
            ddlLocation2.DataTextField = "L_DESCR"
            ddlLocation2.DataValueField = "L_ID"
            ddlLocation2.DataBind()
            ddlLocation2.Items.Insert(0, New ListItem("-Please Select-", "0"))
        ElseIf region = 3 Then
            ddlLocation3.Items.Clear()
            ddlLocation3.DataSource = ds
            ddlLocation3.DataTextField = "L_DESCR"
            ddlLocation3.DataValueField = "L_ID"
            ddlLocation3.DataBind()
            ddlLocation3.Items.Insert(0, New ListItem("-Please Select-", "0"))
        ElseIf region = 4 Then
            ddlLocation4.Items.Clear()
            ddlLocation4.DataSource = ds
            ddlLocation4.DataTextField = "L_DESCR"
            ddlLocation4.DataValueField = "L_ID"
            ddlLocation4.DataBind()
            ddlLocation4.Items.Insert(0, New ListItem("-Please Select-", "0"))
        ElseIf region = 5 Then
            ddlLocation5.Items.Clear()
            ddlLocation5.DataSource = ds
            ddlLocation5.DataTextField = "L_DESCR"
            ddlLocation5.DataValueField = "L_ID"
            ddlLocation5.DataBind()
            ddlLocation5.Items.Insert(0, New ListItem("-Please Select-", "0"))

        End If



    End Sub

    Sub Get_Calendar_Event_ById(ByVal CM_ID As Integer)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlClient.SqlParameter

            Dim Dt As New DataTable

            param(0) = New SqlParameter("@CM_ID", CM_ID)
            Dim ds As DataSet
            Dim ds2 As DataSet
            Dim ds3 As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Calendar_Events_ById", param)

            ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_Events_Locations_ById", param)

            ds3 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "Select PDT_EMP_ID as Id FROM PD_T.COURSE_TRAINERS inner join  dbo.[PD_M.PD_TRAINERS] ON PD_T.COURSE_TRAINERS.CT_PDT_ID = PDT_ID WHERE CT_CM_ID = " & CM_ID)
            Dim tmpstr As String = ""
            If Not ds3 Is Nothing Then
                For Each row As DataRow In ds3.Tables(0).Rows
                    'If Not Me.chkTrainers.Items.FindByValue(row.Item("Id")) Is Nothing Then
                    '    Me.chkTrainers.Items.FindByValue(row.Item("Id")).Selected = True
                    'End If
                    tmpstr &= row.Item("Id") & ","
                Next
                If tmpstr.EndsWith(",") Then
                    tmpstr = tmpstr.TrimEnd(",")
                End If

                h_TrainerId.Value = tmpstr
                Me.FillTrainerNames(tmpstr)

            End If

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                txtTitle.Text = Dt.Rows(0)("CM_TITLE").ToString
                txtFrom.Text = Dt.Rows(0)("CM_DATE").ToString
                txtTime.Text = Dt.Rows(0)("CM_START_TIME").ToString
                txtTimeTo.Text = Dt.Rows(0)("CM_END_TIME").ToString
                txtMAxCapacity.Text = Dt.Rows(0)("CM_MAX_CAPACITY").ToString
                txtOnlineDate.Text = Dt.Rows(0)("CM_OPEN_ONLINE_DT").ToString
                txtCloseDate.Text = Dt.Rows(0)("CM_CLOSE_ONLINE_DT").ToString
                txtDesc.Content = Server.HtmlDecode(Dt.Rows(0)("CM_DESCR").ToString)
                txtDetails.Text = Dt.Rows(0)("CM_Details").ToString
                txtTodate.Text = Dt.Rows(0)("CM_EVENT_END_DT").ToString
                txtCode.Text = Dt.Rows(0)("CM_CODE").ToString
                If Dt.Rows(0)("CM_COURSE_TYPE_ID").ToString <> "" Then
                    ddlCourseType.Items.FindByValue(Dt.Rows(0)("CM_COURSE_TYPE_ID").ToString).Selected = True
                End If
                txtCourseValue.Text = Dt.Rows(0)("CM_COURSE_VALUE").ToString
                txtValueComments.Text = Dt.Rows(0)("CM_COURSE_COMEMNTS").ToString
            End If

            If ds2.Tables(0).Rows.Count > 0 Then
                Dt = ds2.Tables(0)
                ViewState("countRegion") = ds2.Tables(0).Rows.Count
                If Not ddlRegion1.Items.FindByValue(Dt.Rows(0)("CS_R_ID").ToString) Is Nothing Then
                    ddlRegion1.ClearSelection()
                    ddlRegion1.Items.FindByValue(Dt.Rows(0)("CS_R_ID").ToString).Selected = True
                    ddlLocation1.ClearSelection()
                    ddlLocation1.Items.FindByValue(Dt.Rows(0)("CS_L_ID").ToString).Selected = True
                    ddlStatus1.ClearSelection()
                    ddlStatus1.Items.FindByValue(Dt.Rows(0)("CS_STATUS").ToString).Selected = True
                End If
                If Dt.Rows.Count >= 2 Then
                    If Not ddlRegion2.Items.FindByValue(Dt.Rows(1)("CS_R_ID").ToString) Is Nothing Then
                        ddlRegion2.ClearSelection()
                        ddlRegion2.Items.FindByValue(Dt.Rows(1)("CS_R_ID").ToString).Selected = True
                        ddlRegion2_SelectedIndexChanged(ddlRegion2, Nothing)
                        ddlLocation2.ClearSelection()
                        ddlLocation2.Items.FindByValue(Dt.Rows(1)("CS_L_ID").ToString).Selected = True
                        ddlStatus2.ClearSelection()
                        ddlStatus2.Items.FindByValue(Dt.Rows(1)("CS_STATUS").ToString).Selected = True
                        trRegion2.Style.Add("display", "")
                    End If
                End If

                If Dt.Rows.Count >= 3 Then
                    If Not ddlRegion3.Items.FindByValue(Dt.Rows(2)("CS_R_ID").ToString) Is Nothing Then
                        ddlRegion3.ClearSelection()
                        ddlRegion3.Items.FindByValue(Dt.Rows(2)("CS_R_ID").ToString).Selected = True
                        ddlRegion3_SelectedIndexChanged(ddlRegion2, Nothing)
                        ddlLocation3.ClearSelection()
                        ddlLocation3.Items.FindByValue(Dt.Rows(2)("CS_L_ID").ToString).Selected = True
                        ddlStatus3.ClearSelection()
                        ddlStatus3.Items.FindByValue(Dt.Rows(2)("CS_STATUS").ToString).Selected = True
                        trRegion3.Style.Add("display", "")
                    End If
                End If

                If Dt.Rows.Count >= 4 Then
                    If Not ddlRegion4.Items.FindByValue(Dt.Rows(3)("CS_R_ID").ToString) Is Nothing Then
                        ddlRegion4.ClearSelection()
                        ddlRegion4.Items.FindByValue(Dt.Rows(3)("CS_R_ID").ToString).Selected = True
                        ddlRegion4_SelectedIndexChanged(ddlRegion2, Nothing)
                        ddlLocation4.ClearSelection()
                        ddlLocation4.Items.FindByValue(Dt.Rows(3)("CS_L_ID").ToString).Selected = True
                        ddlStatus4.ClearSelection()
                        ddlStatus4.Items.FindByValue(Dt.Rows(3)("CS_STATUS").ToString).Selected = True
                        trRegion4.Style.Add("display", "")
                    End If
                End If

                If Dt.Rows.Count >= 5 Then
                    If Not ddlRegion5.Items.FindByValue(Dt.Rows(4)("CS_R_ID").ToString) Is Nothing Then
                        ddlRegion5.ClearSelection()
                        ddlRegion5.Items.FindByValue(Dt.Rows(4)("CS_R_ID").ToString).Selected = True
                        ddlRegion5_SelectedIndexChanged(ddlRegion2, Nothing)
                        ddlLocation5.ClearSelection()
                        ddlLocation5.Items.FindByValue(Dt.Rows(4)("CS_L_ID").ToString).Selected = True
                        ddlStatus5.ClearSelection()
                        ddlStatus5.Items.FindByValue(Dt.Rows(4)("CS_STATUS").ToString).Selected = True
                        trRegion5.Style.Add("display", "")
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Get_Calendar_Event_ById")
        End Try
    End Sub
    Sub BindCoursetype()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GetCourseType")
        If Not ds Is Nothing Then
            ddlCourseType.Items.Clear()
            ddlCourseType.DataSource = ds
            ddlCourseType.DataTextField = "COURSE_TYPE_DESC"
            ddlCourseType.DataValueField = "COURSE_TYPE_ID"
            ddlCourseType.DataBind()
        End If
    End Sub

    Sub BindTrainers()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim query As String = "SELECT PDT.PDT_ID AS ID, ISNULL(E.EMP_FNAME,'') + ' ' +  ISNULL(E.EMP_MNAME,'') + ' ' + ISNULL(E.EMP_LNAME,'') AS Name FROM dbo.[PD_M.PD_TRAINERS] PDT INNER JOIN dbo.EMPLOYEE_M E ON PDT.PDT_EMP_ID = E.EMP_ID ORDER BY E.EMP_FNAME "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        If Not ds Is Nothing Then
            Me.chkTrainers.DataValueField = "Id"
            Me.chkTrainers.DataTextField = "Name"
            Me.chkTrainers.DataSource = ds
            Me.chkTrainers.DataBind()

        End If

    End Sub

    Private Function FillTrainerNames(ByVal TrainerIDs As String) As Boolean

        Dim IDs As String() = TrainerIDs.Split(",")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet, dt As DataTable
        Dim param(1) As SqlClient.SqlParameter
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "select EMP_ID as ID, EMPNO as EMPNO, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR, 0 as Level1, 0 as Level2, 0 as Level3, 0 as CollectiveEmail from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        'str_Sql = "SELECT Distinct EMP_ID, PDT.PDT_ID AS ID, ISNULL(E.EMP_FNAME,'') + ' ' +  ISNULL(E.EMP_MNAME,'') + ' ' + ISNULL(E.EMP_LNAME,'') AS Name FROM dbo.[PD_M.PD_TRAINERS] PDT INNER JOIN dbo.EMPLOYEE_M E ON PDT.PDT_EMP_ID = E.EMP_ID Where PDT.PDT_ID IN (" & condition & ") ORDER BY  ISNULL(E.EMP_FNAME,'') + ' ' +  ISNULL(E.EMP_MNAME,'') + ' ' + ISNULL(E.EMP_LNAME,''),PDT_ID,EMP_ID "
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If TrainerIDs <> "" Then
            param(0) = New SqlParameter("@EmpIDs", TrainerIDs)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.Get_PDC_Trainers", param)


            gvEMPName.DataSource = ds
            Session("PD_Add_Course_Trainers") = ds
            gvEMPName.DataBind()

            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
        Else
            Return False
        End If


       
        Return True
    End Function
    Protected Sub txtTrainerIDs_TextChanged(sender As Object, e As EventArgs) Handles txtTrainerIDs.TextChanged
        txtTrainerIDs.Text = ""
        If h_TrainerId.Value <> "" Then
            gvEMPName.Visible = True
            FillTrainerNames(h_TrainerId.Value)
        Else
            gvEMPName.Visible = False
        End If
    End Sub
    Protected Sub imgGetTrainers_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)
        Me.FillTrainerNames(Me.h_TrainerId.Value)
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        Dim dt As DataTable, rowtoremove As DataRow
        lblEMPID = TryCast(sender.FindControl("lblID"), Label)
        If Not lblEMPID Is Nothing Then
            h_TrainerId.Value = h_TrainerId.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            '    gvEMPName.PageIndex = gvEMPName.PageIndex
            '    FillEmpNames(h_EMPID.Value)

            If Not Session("dtTaskEscalationLevelsMasterEmployee") Is Nothing Then
                dt = Session("dtTaskEscalationLevelsMasterEmployee")
                If dt.Select("ID='" & lblEMPID.Text & "'").Length > 0 Then
                    rowtoremove = dt.Select("ID='" & lblEMPID.Text & "'")(0)
                    dt.Rows.Remove(rowtoremove)
                    dt.AcceptChanges()
                    Session("dtTaskEscalationLevelsMasterEmployee") = dt
                    h_TrainerId.Value = ""
                    For Each row As DataRow In dt.Rows
                        h_TrainerId.Value &= row.Item("ID") & "|"
                    Next
                    If h_TrainerId.Value.EndsWith("|") Then
                        h_TrainerId.Value.TrimEnd("|")
                    End If
                    gvEMPName.DataSource = dt
                    gvEMPName.DataBind()
                End If
            End If
        End If
    End Sub

    Protected Sub gvEMPName_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEMPName.RowDeleting
        Try
            Dim id As Integer = CType(Me.gvEMPName.Rows(e.RowIndex).FindControl("lblId"), Label).Text
            Dim ds As DataSet = Session("PD_Add_Course_Trainers")
            If Not ds Is Nothing Then
                ds.Tables(0).Rows.Remove(ds.Tables(0).Select("EMP_ID = " & id)(0))
                ds.AcceptChanges()
                Me.gvEMPName.DataSource = ds
                Session("PD_Add_Course_Trainers") = ds
                Dim trainerID As String = h_TrainerId.Value
                Dim newsd As String = String.Empty
                Dim newsd1 As String = String.Empty
                If trainerID <> "" Then
                    newsd = trainerID.Replace(id, "")
                    newsd1 = newsd.Replace(", ,", ",")
                    newsd1.TrimEnd(", ")
                    h_TrainerId.Value = newsd1.TrimEnd()
                    h_TrainerId.Value = newsd1.TrimStart()

                    If h_TrainerId.Value.EndsWith(",") Then
                        Dim subE As String = h_TrainerId.Value.Substring(0, h_TrainerId.Value.Length - 1)

                        h_TrainerId.Value = subE

                    End If
                    If h_TrainerId.Value.StartsWith(",") Then
                        Dim SubS = h_TrainerId.Value.Substring(1, h_TrainerId.Value.Length - 1)
                        h_TrainerId.Value = SubS
                    End If

                End If
                Me.gvEMPName.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvEMPName_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles gvEMPName.SelectedIndexChanged

    End Sub

    Function Is_code_Exists() As Boolean
        Dim IsCodeEx As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@Code", txtCode.Text)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_Course_Details_By_Code", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)
            IsCodeEx = True
        End If
        Return IsCodeEx
    End Function
End Class
