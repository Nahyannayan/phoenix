﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="PD_Calendar.aspx.vb" Inherits="PD_PD_Calendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
         <style type="text/css">
 .calendarSDays {background: rgb(192,192,192); /* Old browsers */
background: -moz-linear-gradient(top, rgba(192,192,192,1) 0%, rgba(192,192,192,1) 40%, rgba(192,192,192,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(192,192,192,1)), color-stop(40%,rgba(192,192,192,1)), color-stop(100%,rgba(192,192,192,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(192,192,192,1) 0%,rgba(192,192,192,1) 40%,rgba(192,192,192,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(192,192,192,1) 0%,rgba(192,192,192,1) 40%,rgba(192,192,192,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(192,192,192,1) 0%,rgba(192,192,192,1) 40%,rgba(192,192,192,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(192,192,192,1) 0%,rgba(192,192,192,1) 40%,rgba(192,192,192,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d5cea6', endColorstr='#b7ad70',GradientType=0 ); /* IE6-9 */

 }
 .calendarDays
 {
background: rgb(85,149,146); /* Old browsers */
background: -moz-linear-gradient(left, rgba(85,149,146,1) 0%, rgba(85,149,146,1) 0%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(85,149,146,1)), color-stop(0%,rgba(85,149,146,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 0%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 0%); /* Opera 11.10+ */
background: -ms-linear-gradient(left, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 0%); /* IE10+ */
background: linear-gradient(to right, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 0%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=1 ); /* IE6-9 */
 }
  .calendarDaysEmpty
 {
background: rgb(217,217,217); /* Old browsers */
background: -moz-linear-gradient(left, rgba(217,217,217) 0%, rgba(217,217,217,1) 0%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(217,217,217,1)), color-stop(0%,rgba(217,217,2172,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left, rgba(217,217,217,1) 0%,rgba(217,217,217,1) 0%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left, rgba(217,217,217,1) 0%,rgba(217,217,217,1) 0%); /* Opera 11.10+ */
background: -ms-linear-gradient(left, rgba(217,217,217,1) 0%,rgba(217,217,217,1) 0%); /* IE10+ */
background: linear-gradient(to right, rgba(217,217,217,1) 0%,rgba(217,217,217,1) 0%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=1 ); /* IE6-9 */
 }
 .calendarOtherDays
 {
background: rgb(255,255,255); /* Old browsers */
background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(243,243,243,1) 50%, rgba(237,237,237,1) 51%, rgba(255,255,255,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(50%,rgba(243,243,243,1)), color-stop(51%,rgba(237,237,237,1)), color-stop(100%,rgba(255,255,255,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
 }
 .TopHeaders
 {
     background: rgb(13,13,13); /* Old browsers */
 }
 .DayHeaders
 {
background: rgb(85,149,146); /* Old browsers */
/*background: -moz-linear-gradient(left, rgba(167,190,199,1) 0%, rgba(167,190,199,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(85,149,146,1)), color-stop(100%,rgba(85,149,146,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 100%); /* IE10+ */
background: linear-gradient(to right, rgba(85,149,146,1) 0%,rgba(85,149,146,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a7cfdf', endColorstr='#23538a',GradientType=1 ); /* IE6-9 */*/
 }
 .DayDefault
 {
background:  rgb(255,255,255);/* Old browsers */
background: -moz-linear-gradient(left, rgba(240, 248, 255,1) 0%, rgba(240, 248, 255,1) 100%, rgba(240, 248, 255,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(240, 248, 255,1)), color-stop(100%,rgba(240, 248, 255,1)), color-stop(100%,rgba(240, 248, 255,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left, rgba(240, 248, 255,1) 0%,rgba(240, 248, 255,1) 100%,rgba(240, 248, 255,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left, rgba(240, 248, 255,1) 0%,rgba(240, 248, 255,1) 100%,rgba(240, 248, 255,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left, rgba(240, 248, 255,1) 0%,rgba(240, 248, 255,1) 100%,rgba(240, 248, 255,1) 100%); /* IE10+ */
background: linear-gradient(to right, rgba(240, 248, 255,1) 0%,rgba(240, 248, 255,1) 100%,rgba(240, 248, 255,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c3d9ff', endColorstr='#98b0d9',GradientType=1 ); /* IE6-9 */
 }
 .DayDefaultOtherMonth
 {
background: rgb(238,238,238); /* Old browsers */
background: -moz-linear-gradient(left, rgb(238,238,238) 0%, rgba(238,238,238,1) 100%, rgba(238,238,238,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(238,238,238,1)), color-stop(100%,rgba(238,238,238,1)), color-stop(100%,rgba(238,238,238,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left, rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%,rgba(238,238,238,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left, rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%,rgba(238,238,238,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left, rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%,rgba(238,238,238,1) 100%); /* IE10+ */
background: linear-gradient(to right, rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%,rgba(238,238,238,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c3d9ff', endColorstr='#98b0d9',GradientType=1 ); /* IE6-9 */
 }
 #fancybox-wrap {
  margin: -200px 90 170 290px;
}
     .SelectedDayStyle1
        {
            background-color: Green !important;
            border-bottom-color: Green !important;
            border-left-color: Green !important;
            border-right-color: Green !important;
            border-top-color: Green !important;
            color: White;
        }
     .calOtherDay {
    border: 1px solid #e6e6e6;
    height: 24px;
    width: 20px;
    text-decoration: none;
    font-size: 14px;
    text-align: center;
    background: #eeeeee;
}
</style>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
   
    <script type="text/javascript">



        function GoToPage(Id) {
           
            $.fancybox({
                type: 'iframe',
                maxWidth: 300,
                href: 'PD_Calendar_Apply.aspx?val=' + Id,
                maxHeight: 600,
                fitToView: false,
                width: '100%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
             });
         
        }

        function GoToPages(eventId) {
        window.location.href = "PD_Calendar_Apply.aspx?val=" + eventId;
    }
    </script>
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>PD Calendar
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
      <table border="0"  width="100%" >
        <tr>
            <td align="left" class="" valign="bottom" width="50%">
                <div id="lblError" runat="server">
                </div>
            </td>
            <td></td><td></td><td></td>
        </tr>
          <tr>
              <td>
                  <asp:DropDownList ID="ddlmonth" runat="server" OnSelectedIndexChanged="ddlmonth_SelectedIndexChanged" AutoPostBack="true">
                     <asp:ListItem Text="January" Value="1" />
    <asp:ListItem Text="February" Value="2" />
    <asp:ListItem Text="March" Value="3" />
    <asp:ListItem Text="April" Value="4" />
    <asp:ListItem Text="May" Value="5" />
    <asp:ListItem Text="June" Value="6" />
    <asp:ListItem Text="July" Value="7" />
    <asp:ListItem Text="August" Value="8" />
    <asp:ListItem Text="September" Value="9" />
    <asp:ListItem Text="October" Value="10" />
    <asp:ListItem Text="November" Value="11" />
    <asp:ListItem Text="December" Value="12" />
                  </asp:DropDownList></td><td>
                  <asp:DropDownList ID="ddlyear" runat="server">

                  </asp:DropDownList>
              </td>
              <td></td>
              <td align="right"><asp:LinkButton ID="lnkMyPd" runat="server" Text="My PD Requests" OnClick="lnkMyPd_Click"></asp:LinkButton></td>
              
          </tr>
        <tr>
        
        <td colspan="4">

        <asp:Calendar ID="calendarPD" runat="server" BorderStyle="Solid" BorderWidth="0px"  BorderColor="LightGray"
        CellPadding="5" CellSpacing="5" FirstDayOfWeek="Sunday" Height="300px" OnDayRender="calendarPD_DayRender"
        OnVisibleMonthChanged="calendarPD_VisibleMonthChanged" 
        ShowGridLines="True"  Width="100%" 
        ForeColor="WhiteSmoke" SelectionMode="Day" DayNameFormat="Full" Font-Names="Book Antiqua"
        Font-Size="Medium" OnPreRender="calendarPD_PreRender" 
        NextPrevFormat="FullMonth" NextPrevStyle-ForeColor="Gray">
        <DayHeaderStyle CssClass="" BackColor="#8DC24C" BorderColor="LightGray" BorderStyle="Solid" />
        <DayStyle CssClass="" BackColor="white" BorderColor="LightGray" BorderWidth="1" Font-Bold="True"
            Font-Italic="False" Width="100" Font-Size="Smaller" Font-Names="Arial Black"
            Font-Overline="False" Font-Underline="False" ForeColor="#00000" Height="100px"
            HorizontalAlign="left" VerticalAlign="Top" BorderStyle="Solid" />
        <NextPrevStyle  Font-Names="Arial CE" Font-Size="16px" />
        <OtherMonthDayStyle CssClass="DayDefaultOtherMonth" BorderColor="LightGray" BorderStyle="Solid" />
        <SelectedDayStyle CssClass="calendarSDays" ForeColor="Black"  />
        <TitleStyle CssClass="" BackColor="LightGray" ForeColor="Gray"  Height="36" Font-Size="X-Large" Font-Names="Courier New Baltic" Font-Bold="true" BorderColor="LightGray" />
    </asp:Calendar>
        </td>
        </tr>
          <tr>
              <td colspan="4" align="right">
                  <asp:Button ID="lnkBackToESS" runat="server" Text="Back" OnClick="lnkBackToESS_Click" CssClass="button"></asp:Button>
              </td>
          </tr>
        </table>
                </div></div></div>
</asp:Content>

