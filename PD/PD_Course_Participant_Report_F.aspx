﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PD_Course_Participant_Report_F.aspx.vb" Inherits="PD_PD_Course_PArticipant_Report_F" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<base target="_self" />
    <title>Participants </title>
  <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <style>
  .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
    }
    .GridPager a
    {
        background-color: #f5f5f5; 
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }

  </style>
     <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
 <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>

    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
  <script type="text/javascript">
      $(document).ready(function () {
          $('[id$=chkHeader]').click(function () {
              $("[id$='chkChild']").attr('checked', this.checked);
          });
      });
      function GetRadWindow() {
          var oWindow = null;
          if (window.radWindow) oWindow = window.radWindow;
          else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
          return oWindow;
      }
          </script>

</head>
<body onload="listen_window();">
    <form id="form1" runat="server">
    
        <table  id="tblCategory" cellspacing="2" cellpadding="2" width="100%" >
             <tr>
                <td class="title-bg" colspan="4">Participants</td>
            </tr>
            <tr id="trLabelError">
                <td align="left" class="matters" valign="bottom" colspan="2">
                    <div id="lblError" runat="server">
                    </div>
                </td>
            </tr>
             <tr>
        <td class="matters" align="center" colspan="2">
        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="ADD " Width="110px" />
                  
                   <asp:HiddenField ID="hdnSelected" runat="server" />
        </td>
        </tr> 
           
            <tr id="trGridV" >
            <td colspan="2" style="width:100%">
                <table id="tblData"  width="100%" >
                   
                    <tr>
                        <td >
                            <asp:GridView ID="gvParticipants" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging ="gvParticipants_PageIndexChanging" DataKeyNames="EMP_ID" EnableModelValidation="True"   >
                                
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                 <asp:TemplateField HeaderText="Select">
                         <HeaderTemplate>
                                            <asp:CheckBox ID="chkHeader" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkChild" runat="server" />
                                            <asp:Label ID="lblEmp_ID" runat="server" Text='<%# Bind("Emp_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                          
                        </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("BusinessUnit") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="EMP No" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EMPNO") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Name" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("EMPLOYEE") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Email" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("PHONE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Designation">
                                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("DESIGNATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                   
                                </Columns>
                                  <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
            </td>
        </tr>
        </table>
   
    </form>
</body>
</html>
