﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class PD_PD_ADD_LOCATIONS
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("ID") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("ID").ToString
                ViewState("MainID") = MainID
                GetREGION_BYID()
                BIndLocations()
                btnUpdateLOCATION.Visible = False
                btnAddLOCATION.Visible = True
            End If
        End If

    End Sub

    Protected Sub btnAddLOCATION_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddLOCATION.Click
        ViewState("L_ID") = 0


        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
            BIndLocations()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
        End If
    End Sub

    Protected Sub gvLocations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvLocations.PageIndex = e.NewPageIndex
        BIndLocations()
    End Sub
    Protected Sub btnUpdateLOCATION_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateLOCATION.Click



        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
            BIndLocations()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
        End If
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblL_ID As New Label

        Dim lblL_DESCR As New Label
        Dim lblREGIONG As New Label
        Dim lblL_SHORT As New Label

        btnUpdateLOCATION.Visible = True
        btnAddLOCATION.Visible = False

        lblL_ID = TryCast(sender.FindControl("lblL_ID"), Label)
        lblL_DESCR = TryCast(sender.FindControl("lblL_DESCR"), Label)
        lblREGIONG = TryCast(sender.FindControl("lblREGIONG"), Label)
        lblL_SHORT = TryCast(sender.FindControl("lblL_SHORT"), Label)
       

        txtLocation.Text = lblL_DESCR.Text
        txtShort.Text = lblL_SHORT.Text
        lblREGION.Text = lblREGIONG.TExt
        ViewState("L_ID") = lblL_ID.Text
        hdnID.Value = ViewState("L_ID")
        lblError.InnerHtml = ""
        'gvCategory.Columns(3).Visible = False
    End Sub

    Private Sub GetREGION_BYID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable
            param(0) = New SqlClient.SqlParameter("@R_ID", ViewState("MainID"))


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_ALL_REGIONS", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                lblREGION.Text = DT.Rows(0)("R_REGION_NAME")

            End If
        Catch ex As Exception
            lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction



        Dim L_ID As String = ViewState("L_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(5) As SqlParameter
                param(0) = New SqlParameter("@L_ID", L_ID)
                param(1) = New SqlParameter("@Location", txtLocation.Text)
                param(2) = New SqlParameter("@R_ID", ViewState("MainID"))
                param(3) = New SqlParameter("@SHORT", txtShort.Text)

                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_M.SAVE_LOCATION", param)
                Dim ReturnFlag As Integer = param(4).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SVC_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    'resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function

    Private Sub BIndLocations()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@regionId", ViewState("MainID"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[PD_M].[GET_ALL_LOCATIONS_BY_REGION]", param)
            If ds.Tables(0).Rows.Count > 0 Then

                gvLocations.DataSource = ds.Tables(0)
                gvLocations.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                '  ds.Tables(0).Rows(0)(2) = True
                gvLocations.DataSource = ds.Tables(0)
                Try
                    gvLocations.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvLocations.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvLocations.Rows(0).Cells.Clear()
                gvLocations.Rows(0).Cells.Add(New TableCell)
                gvLocations.Rows(0).Cells(0).ColumnSpan = columnCount
                gvLocations.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvLocations.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

End Class
