﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports GemBox.Spreadsheet
Partial Class PD_PD_Corp_KTAA
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    ''ss
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim isPD_SuperUser As Boolean = Check_PD_SuperUser()

                    If Not isPD_SuperUser Then
                        tblNoAccess.Visible = True
                        tbl_AddGroup.Visible = False
                        ' tblGridView.Visible = False
                        Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                        ' lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                        lblNoAccess.Text = Errormsg
                    End If

                  

                    bindAcademicYearFilter()

                End If

                'show all requests by default


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Protected Sub btnDownloadExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadExcel.Click
        Try
            DownloadKTAAExcel()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        'base.VerifyRenderingInServerForm(control);
    End Sub
    Private Sub DownloadKTAAExcel()

        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim FILTER_COND As String = String.Empty
            Dim param(3) As SqlClient.SqlParameter
            Dim ds As DataSet
            'Dim Form1 As New System.Web.UI.HtmlControls.HtmlForm
            'Form1.Controls.Clear()
            'Form1.Controls.Add(gvKTAAParticipantList)
            gvKTAAParticipantList.Visible = True


            param(0) = New SqlClient.SqlParameter("@CM_ID", ddlCourse.SelectedItem.Value)



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_PD_KTAA_DOWNLOAD", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            Else
                ''   UtilityObj.Errorlog(ds.Tables(0).Rows.Count, System.Reflection.MethodBase.GetCurrentMethod().Name)

                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)


                If dtEXCEL.Rows.Count > 0 Then
                    gvKTAAParticipantList.DataSource = ds
                    gvKTAAParticipantList.DataBind()
                End If

                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                Response.ContentEncoding = System.Text.Encoding.UTF8
                Response.AddHeader("content-disposition", "attachment;filename=" + ddlCourse.SelectedItem.Text + ".xls")

                Dim stringWriter As New StringWriter()

                Dim htmlWriter As New HtmlTextWriter(stringWriter)
                gvKTAAParticipantList.HeaderStyle.Font.Bold = True
                gvKTAAParticipantList.HeaderStyle.Font.Size = 10
                gvKTAAParticipantList.HeaderStyle.Height = 50
                gvKTAAParticipantList.RowStyle.Font.Size = 12
                gvKTAAParticipantList.RowStyle.Height = 200
                gvKTAAParticipantList.HeaderStyle.BackColor = Drawing.Color.White
                gvKTAAParticipantList.HeaderStyle.ForeColor = Drawing.Color.Black

                gvKTAAParticipantList.RenderControl(htmlWriter)
                Response.Write("<style> TABLE { border:2px #999; } " & _
                    "TD { border:2px #D5D5D5; text-align:left; } </style>") ''white-space:nowrap;

                Response.Write(stringWriter.ToString())
                Response.Flush()
                Response.End()
                '  gvKTAAParticipantList.Visible = False

                'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                'Dim ef As ExcelFile = New ExcelFile
                'Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")

                'Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                'Dim stuFilename As String = "PDCourseKTAAInfo.xls"
                'ws.InsertDataTable(dtEXCEL, "A1", True)

                ' ''removing unwanted columns
                ' ''ws.Columns("A").Delete()
                ''ws.Columns("C").Delete()



                'ws.Cells(0, 0).Value = "CR ID"
                'ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("A").Width = 4000

                'ws.Cells(0, 1).Value = "Title"
                'ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("B").Width = 6000

                'ws.Cells(0, 2).Value = "Businessunit"
                'ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("C").Width = 5000

                'ws.Cells(0, 3).Value = "Emp ID"
                'ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("D").Width = 4000

                'ws.Cells(0, 4).Value = "Emp No"
                'ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("E").Width = 4000

                'ws.Cells(0, 5).Value = "Employee"
                'ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("F").Width = 7000

                'ws.Cells(0, 6).Value = "Email"
                'ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("G").Width = 7000

                'ws.Cells(0, 7).Value = "Phone"
                'ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("H").Width = 4000

                'ws.Cells(0, 8).Value = "Designation"
                'ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("I").Width = 7000

                'ws.Cells(0, 9).Value = "Attendance"
                'ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("J").Width = 4000

                'ws.Cells(0, 10).Value = "What are your key take-away learnings from the professional development?"
                'ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("K").Width = 7000

                'ws.Cells(0, 11).Value = "Action Step / Objectives"
                'ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("L").Width = 7000

                'ws.Cells(0, 12).Value = "Timeline"
                'ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("M").Width = 7000

                'ws.Cells(0, 13).Value = "Resources / Preparation / Support needed"
                'ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("N").Width = 7000

                'ws.Cells(0, 14).Value = "Relevance to role"
                'ws.Cells(0, 14).Style.Font.Color = Drawing.Color.White
                'ws.Cells(0, 14).Style.FillPattern.SetSolid(Drawing.Color.Black)
                'ws.Columns("O").Width = 7000



                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                'Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                'Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

                'If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                '    ' Create the directory.
                '    Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                'End If

                'ef.SaveXls(cvVirtualPath & pathSave)
                'Dim path = cvVirtualPath & pathSave

                'Dim bytes() As Byte = File.ReadAllBytes(path)
                ''Response.Cache.SetCacheability(HttpCacheability.NoCache)
                'Response.Clear()
                'Response.ClearHeaders()
                'Response.ContentType = "application/octect-stream"
                'Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'Response.BinaryWrite(bytes)
                'Response.Flush()
                'Response.End()


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim AcyID As String
            AcyID = ddlAcdYear.SelectedValue
            bindCourseList(AcyID)

        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub bindAcademicYearFilter()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_ACADEMIC_YEARS")
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ddlAcdYear.DataSource = ds
                ddlAcdYear.DataTextField = "ACY_DESCR"
                ddlAcdYear.DataValueField = "ACY_ID"
                ddlAcdYear.DataBind()
                ''  ddlAcdYear.Items.Insert(0, New ListItem("-All-", "0"))
                ddlAcdYear_SelectedIndexChanged(0, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub bindCourseList(ByVal AcyID As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim txtSearch As New TextBox

        Dim param(5) As SqlClient.SqlParameter
        Try


            param(0) = New SqlClient.SqlParameter("@ACY_ID", AcyID)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_COURSE_BY_ACADEMIC_YEAR", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCourse.DataSource = ds
                ddlCourse.DataTextField = "CM_TITLE"
                ddlCourse.DataValueField = "CM_ID"
                ddlCourse.DataBind()
                ddlAcdYear.Items.Insert(0, New ListItem("-Please Select-", "0"))

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function
End Class
