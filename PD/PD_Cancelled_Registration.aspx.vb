﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Partial Class PD_PD_Cancelled_Registration
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Dim isPD_SuperUser As Boolean = Check_PD_SuperUser()

                    If Not isPD_SuperUser Then
                        tblNoAccess.Visible = True
                        tbl_AddGroup.Visible = False
                        tblGridView.Visible = False
                        Dim Errormsg As String = "SORRY!! YOU HAVE NO ACCESS TO THIS PAGE"
                        lblNoAccess.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & Errormsg & "</div>"
                    End If

                

                    bindAcademicYearFilter()

                End If

                'show all requests by default


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim AcyID As String
            AcyID = ddlAcdYear.SelectedValue
            bindCourseList(AcyID)

        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim ds As DataSet
            Dim DT As DataTable

            If ddlCourse.SelectedItem.Value <> "0" Then
                bindParticipantsGrid(Convert.ToInt32(ddlCourse.SelectedItem.Value))
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvParticipantList.DataSource = ds
                gvParticipantList.DataBind()
                Dim columnCount As Integer = gvParticipantList.Rows(0).Cells.Count
                gvParticipantList.Rows(0).Cells.Clear()
                gvParticipantList.Rows(0).Cells.Add(New TableCell)
                gvParticipantList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipantList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipantList.Rows(0).Cells(0).Text = "Currently there is no cancelled Registrations"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvParticipantList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvParticipantList.PageIndex = e.NewPageIndex
        If ddlCourse.SelectedItem.Value <> "0" Then
            bindParticipantsGrid(Convert.ToInt32(ddlCourse.SelectedItem.Value))
        End If
    End Sub

    Protected Sub lnkRestoreReg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim CR_ID As String = sender.CommandArgument.ToString

            Dim isRestored As Integer = RestoreRegistration(CR_ID)

            If isRestored = "1" Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Registration restored successfully !!!</div>"
                bindParticipantsGrid(Convert.ToInt32(CR_ID))
            Else
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Some error occured while restoring</div>"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub bindAcademicYearFilter()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_CURRENTACADEMIC_YEAR")
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ddlAcdYear.DataSource = ds
                ddlAcdYear.DataTextField = "ACY_DESCR"
                ddlAcdYear.DataValueField = "ACY_ID"
                ddlAcdYear.DataBind()
                ''  ddlAcdYear.Items.Insert(0, New ListItem("-All-", "0"))
                ddlAcdYear_SelectedIndexChanged(0, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub bindCourseList(ByVal AcyID As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim txtSearch As New TextBox

        Dim param(5) As SqlClient.SqlParameter
        Try


            param(0) = New SqlClient.SqlParameter("@ACY_ID", AcyID)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_M.GET_UPCOMING_COURSE_BY_ACADEMIC_YEAR", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCourse.DataSource = ds
                ddlCourse.DataTextField = "CM_TITLE"
                ddlCourse.DataValueField = "CM_ID"
                ddlCourse.DataBind()
                ddlCourse.Items.Insert(0, New ListItem("-Please Select-", "0"))

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Private Sub bindParticipantsGrid(ByVal CM_ID As Integer)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        param(0) = New SqlClient.SqlParameter("@CM_ID", CM_ID)
        param(1) = New SqlClient.SqlParameter("@EMP_NAME", txtEmpName.Text)

        Try
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.GET_CANCELLED_REQUESTS", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.AcceptChanges()
                ds.Tables(0).Columns.Add("COMBINED_DATE", GetType(String))
                gvParticipantList.DataSource = ds
                gvParticipantList.DataBind()
                Dim columnCount As Integer = gvParticipantList.Rows(0).Cells.Count
                gvParticipantList.Rows(0).Cells.Clear()
                gvParticipantList.Rows(0).Cells.Add(New TableCell)
                gvParticipantList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvParticipantList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvParticipantList.Rows(0).Cells(0).Text = "Currently there is no Participants Exists"
            Else
                ds.Tables(0).Columns.Add("COMBINED_DATE", GetType(String))
                Dim i As Integer
                For Each row As DataRow In ds.Tables(0).Rows


                    row.Item("COMBINED_DATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("CM_EVENT_DT")) & " to " & String.Format("{0:" & OASISConstants.DateFormat & "}", row.Item("CM_EVENT_END_DT"))


                    'row.Item("Sr_No") = i + 1
                    i += 1
                Next
                ds.AcceptChanges()

                gvParticipantList.DataSource = ds
                gvParticipantList.DataBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try



    End Sub

    Function Check_PD_SuperUser() As Boolean
        Dim IsAdmin As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@EmpId", Session("EmployeeId"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PD_T.Get_PD_Super_User_By_Id", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                IsAdmin = True
            End If

        End If
        Return IsAdmin
    End Function

    Private Function RestoreRegistration(ByVal CR_ID As String) As Integer
        Dim tran As SqlTransaction
        Dim isReturn As Integer = 0
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection
            tran = CONN.BeginTransaction("SampleTransaction")
            Dim param(3) As SqlClient.SqlParameter

            Try
                param(0) = New SqlParameter("@CR_ID", Convert.ToInt32(CR_ID))

                param(1) = New SqlParameter("@AUD_USER", Session("EmployeeId"))

                param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "PD_T.RESTORE_CANCELLED_PD_REG", param)
                Dim ReturnFlag As Integer = param(2).Value
                If ReturnFlag = 0 Then
                    Dim params1(1) As SqlParameter
                    params1(0) = New SqlParameter("@ID", Convert.ToInt32(CR_ID))
                    params1(1) = New SqlParameter("@status", "A")
                    SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "SEND_PD_COURSE_APPROVAL_EMAIL", params1)

                    tran.Commit()
                    isReturn = 1
                ElseIf ReturnFlag <> 0 Then
                    tran.Rollback()
                    isReturn = 0

                Else

                    tran.Rollback()
                    isReturn = 0
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                tran.Rollback()
            End Try
        End Using
        Return isReturn
    End Function
End Class
