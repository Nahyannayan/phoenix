﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Fusion.aspx.vb" Inherits="Fusion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FusionCharts - Simple</title>
     
    
  <script type="text/javascript" src="fusionCharts/FusionCharts.js"></script>
 

</head>
<body>
    <form id="form1" runat="server">

         <div id="chartContainer">FusionCharts will load here!</div>          
    <script type="text/javascript">
       FusionCharts.setCurrentRenderer('javascript');
       var myChart = new FusionCharts("fusionCharts/Column3D.swf", "myChartId", "400",
                                    "300", "0", "1");
        // myChart.setXMLUrl("C:\Nahyan\dataChart.xml");
       myChart.setXMLData("<chart caption='Weekly Sales Summary' xAxisName='Week' " +
      "yAxisName='Sales' numberPrefix='$'>" +
        "<set label='Week 1' value='14400' />" +
        "<set label='Week 2' value='19600' />" +
        "<set label='Week 3' value='24000' />" +
        "<set label='Week 4' value='15700' />" +
      "</chart>");
    myChart.render("chartContainer");
        
   
    
  
    </script>  
    <div>
    <div style="text-align:center">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </div>
    </form>
</body>
</html>
