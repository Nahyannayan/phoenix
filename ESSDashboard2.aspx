﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ESSDashboard2.aspx.vb" Inherits="ESSDashboard2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style type="text/css">
       /* Module class css goes here */

/* GRID */

.grid { 
    max-width: 1600px; 
    width: 90%; 
    margin: 0 auto; 
}

.four { 
	width: 32.26%; 
}

/* COLUMNS */

.col {
  display: block;
  float:left;
  margin: 1% 0 1% 1.6%;
}

.col:first-of-type { margin-left: 0; }

/* CLEARFIX */

.cf:before,
.cf:after {
    content: " ";
    display: table; 
}

.cf:after { clear: both; }
.cf { *zoom: 1; }

/* GENERAL STYLES FOR BOX AND OVERLAY */

.box {
  display: block;
  width: 100%;
  height: 150px;
  border-radius:10px;
  overflow: hidden;
  background-color: /*#b7cce8 rgba(238, 109, 139, 0.3)*/ #fff;
  text-align: center;
  position: relative;
  line-height: 20px;
  padding:32px 4px 0 4px;
  font-weight:600;
  box-shadow: 4px 4px 6px #999;
}
.overlay a {
        color: #ffffff !important;
    }
.overlay{
  width: 100%;
  height:100%;
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
}

/* SLIDE IN */

.slide-in .overlay{
  background-color: #ff2f92 /*rgba(113,41,108, 0.47)rgba(61,203,232,0.47) #3dcbe8*/ /*rgba(10,35,62, 0.47) rgba(106,221,170,0.47) #6addaa*/;
  /*line-height: 170px;*/
  color: #fff;
  transform: translateX(-100%);
  -webkit-transition: transform 0.01s ease-out;
  -o-transition: transform 0.01s ease-out;
  transition: transform 0.01s ease-out;
  padding:32px 4px 0 4px;
}

.slide-in .box:hover .overlay{
  transform: translateX(0);
}

/* SLIDE UP */

.slide-up .overlay{
  background-color: #203864 /*rgba(10,35,62, 0.47) rgba(106,221,170,0.47) #6addaa*/  /*rgba(113,41,108, 0.47) rgba(61,203,232,0.47) #3dcbe8*/;
  /*line-height: 170px;*/
  color: #fff;
  transform: translateY(100%);
  -webkit-transition: transform 0.02s ease-out;
  -o-transition: transform 0.02s ease-out;
  transition: transform 0.02s ease-out;
  padding:32px 4px 0 4px;
}

.slide-up .box:hover .overlay{
  transform: translateY(0);
}

/* SLIDE DOWN DELAY */

.slide-down-delay .overlay{
  background-color: #00b0f0 /*rgba(111,172,73,0.47) rgba(238,111,140,0.9)/*#ee6f8c*/;
  /*line-height: 170px;*/
  color: #fff;
  transform: translateY(-100%);
  -webkit-transition: transform 0.02s ease-out;
  -o-transition: transform 0.02s ease-out;
  transition: transform 0.02s ease-out;
  padding:32px 4px 0 4px;
}

.slide-down-delay .box:hover .overlay{
  transform: translateY(0);
}

.slide-down-delay .overlay i{
  transform: translateY(-80%);
  opacity: 0;
  -webkit-transition: transform 0.1s linear, opacity 0.1s linear 0.1s;
  -o-transition: transform 0.1s linear, opacity 0.1s linear 0.1s;
  transition: transform 0.1s linear, opacity 0.1s linear 0.1s;
}

.slide-down-delay .box:hover .overlay i{
  transform: translateY(0);
  opacity: 1;
}

/* ROTATE */

.rotate .overlay{
  background-color: #6d94bb;
  line-height: 170px;
  color: #fff;
  transform-origin: 0 0;
  transform: rotate(90deg);
  -webkit-transition: transform 0.5s ease-in-out;
  -o-transition: transform 0.5s ease-in-out;
  transition: transform 0.5s ease-in-out;
}

.rotate .box:hover .overlay{
  transform: rotate(0deg);
}

/* SCALE */

.scale .overlay{
  background-color: #efcb5e;
  line-height: 170px;
  color: #fff;
  transform: translateX(210%) scale(3);
  -webkit-transition: transform 0.6s ease-in-out;
  -o-transition: transform 0.6s ease-in-out;
  transition: transform 0.6s ease-in-out;
}

.scale .box:hover .overlay{
  transform: translateX(0) scale(1);
}

/* FLIP */

.flip .overlay{
  background-color: #009688;
  line-height: 170px;
  color: #fff;
  opacity: 0;
  transform: rotateY(180deg);
  -webkit-transition: transform 0.6s ease-in-out 0.3s, opacity 0.3s ease-in-out;
  -o-transition: transform 0.6s ease-in-out 0.3s, opacity 0.3s ease-in-out;
  transition: transform 0.6s ease-in-out 0.3s, opacity 0.3s ease-in-out;
}

.flip .box:hover .overlay{
  opacity: 1;
  transform: rotateY(0deg);
}

/* SKEW */

.skew .overlay{
  background-color: #f44336;
  line-height: 170px;
  color: #fff;
  opacity: 0;
  transform: skewX(-10deg);
  -webkit-transition: transform 0.3s ease-in-out, opacity 0.3s ease-in-out;
  -o-transition: transform 0.3s ease-in-out, opacity 0.3s ease-in-out;
  transition: transform 0.3s ease-in-out, opacity 0.3s ease-in-out;
}

.skew .box:hover .overlay{
  transform: skewX(0deg);
  opacity: 1;
}

/* CORNER */

.corner-bottom .overlay{
  background-color: #9c27b0;
  line-height: 200px;
  color: #fff;
  transform: translate(100%, 100%);
  -webkit-transition: transform 0.4s ease-in-out;
  -o-transition: transform 0.4s ease-in-out;
  transition: transform 0.4s ease-in-out;
}

.corner-bottom .box:hover .overlay{
   transform: translate(0, 0);
}

/* CORNER */

.corner-top .overlay{
  background-color: #ff5722;
  line-height: 200px;
  color: #fff;
  transform: translate(-100%, -100%);
  -webkit-transition: transform 0.4s ease-in-out;
  -o-transition: transform 0.4s ease-in-out;
  transition: transform 0.4s ease-in-out;
}

.corner-top .box:hover .overlay{
   transform: translate(0, 0);
}



h3.edu-title {
    writing-mode: vertical-rl;
    -ms-writing-mode:bt-rl;
    text-orientation: mixed;
    transform: rotate(180deg);
    margin: 0;
    color: #fff;
    border-bottom-right-radius: 6px;
    padding: 10px 6px;
    border-top-right-radius: 6px;
    background-color: #ff2f92;
}
h3.fin-title {
    writing-mode: vertical-rl;
    -ms-writing-mode:bt-rl;
    text-orientation: mixed;
    transform: rotate(180deg);
    margin: 0;
    color: #fff;
    border-bottom-right-radius: 6px;
    padding: 10px 6px;
    border-top-right-radius: 6px;
    background-color:#00b0f0;
}
h3.adm-title {
    writing-mode: vertical-rl;
    -ms-writing-mode:bt-rl;
    text-orientation: mixed;
    transform: rotate(180deg);
    margin: 0;
    color: #fff;
    border-bottom-right-radius: 6px;
    padding: 10px 6px;
    border-top-right-radius: 6px;
    background-color: #203864;
}
h3.mod-title{
    /*font-weight: 600;
    margin: 10px 0 20px 0;*/
    font-size:20px;
    max-width: 40px;
}
.brd-edu {
    border: 2px solid #ff2f92;
    border-radius: 10px;
    /*padding: 5px;*/
}
.brd-fin {
    border: 2px solid #00b0f0;
    border-radius: 10px;
    /*padding: 5px;*/
}
.brd-adm {
    border: 2px solid #203864;
    border-radius:  10px;
    /*padding: 5px;*/
}

   </style>
   
    <div class="container-fluid mb-5">
        <div class="card">
            <div class="card-body">
        <h2 class="card-title">ESS Dashboard</h2>
    <div class="row">
        
        <div class="col-6 col-md-6 col-sm-12">
            
             <ul class="list-group list-group-flush">
             <li class="list-group-item"><strong>Name :</strong> Rajesh Kumar</li>
             <li class="list-group-item"><strong>Date Of Joining :</strong> xx-xx-xxxx</li>
             <li class="list-group-item"><strong>New Label :</strong> Label Value</li>
             <li class="list-group-item"><strong>New Label :</strong> Label Value</li>
             <li class="list-group-item">&nbsp;</li>
             </ul> 
                
        </div>
        <div class="col-6 col-md-6 col-sm-12">
            
             <ul class="list-group list-group-flush">
             <li class="list-group-item"><strong>New Label :</strong> Label Value</li>
             <li class="list-group-item"><strong>New Label :</strong> Label Value</li>
             <li class="list-group-item"><strong>New Label :</strong> Label Value</li>
             <li class="list-group-item"><strong>New Label :</strong> Label Value</li>
             <li class="list-group-item"><a href="#" class="btn btn-primary float-right">View More</a></li>
             </ul> 
                
        </div>
        </div>
        
                </div>
        </div>

    </div>
    <h3>Layout 1</h3>
     <div class="container">
        <div class="row cf mb-lg-4">
            

    <div class="slide-in four col-lg-4 col-md-4 col-sm-4">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-home fa-3x" aria-hidden="true"></i>
						<p>Leave Application</p>
					</span>
                    <a id="ctl00_cphMasterpage_lbStud_Mag" href="javascript:__doPostBack('ctl00$cphMasterpage$lbStud_Mag','')">
					<div class="overlay">
						<i class="fa fa-home fa-3x" aria-hidden="true"></i>
                        <p>Leave Application</p>
					</div>
                    </a>
				</div>
			</div>

            <div class="slide-in four col-lg-4 col-md-4 col-sm-4">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-user fa-3x" aria-hidden="true"></i>
						<p>PDP</p>
					</span>
                    <a id="ctl00_cphMasterpage_lbStud_Mag" href="javascript:__doPostBack('ctl00$cphMasterpage$lbStud_Mag','')">
					<div class="overlay">
						<i class="fa fa-user fa-3x" aria-hidden="true"></i>
                        <p>PDP</p>
					</div>
                    </a>
				</div>
			</div>

            <div class="slide-in four col-lg-4 col-md-4 col-sm-4">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-users fa-3x" aria-hidden="true"></i>
						<p>PDP New Phase</p>
					</span>
                    <a id="ctl00_cphMasterpage_lbStud_Mag" href="javascript:__doPostBack('ctl00$cphMasterpage$lbStud_Mag','')">
					<div class="overlay">
						<i class="fa fa-users fa-3x" aria-hidden="true"></i>
                        <p>PDP New Phase</p>
					</div>
                    </a>
				</div>
			</div>
           
        </div>
    </div>


    <hr />
    <h3>Layout 2</h3>
    <div class="container-fluid">
        <div class="row justify-content-center cf mb-lg-4 pt-lg-3 brd-fin" style="background-color: #DEFFB4;border: 2px solid #6a923a;border-radius: 10px;">

            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-home fa-3x" style="color:#ff2f92;" aria-hidden="true"></i></h5>
                    <p class="card-text">Leave Application</p>
                </div>
            </div>
            </a>
        </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-user fa-3x" style="color:#00b0f0;" aria-hidden="true"></i></h5>
                    <p class="card-text">PDP</p>
                </div>
            </div>
            </a>
        </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-users fa-3x" style="color:#203864;" aria-hidden="true"></i></h5>
                    <p class="card-text">PDP New Phase</p>
                </div>
            </div>
            </a>
        </div>


             <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-thumbs-up fa-3x" style="color:#ff2f92;" aria-hidden="true"></i></h5>
                    <p class="card-text">Leave Approval</p>
                </div>
            </div>
            </a>
        </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-history fa-3x" style="color:#00b0f0;" aria-hidden="true"></i></h5>
                    <p class="card-text">Leave History</p>
                </div>
            </div>
            </a>
        </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-clipboard fa-3x" style="color:#203864;" aria-hidden="true"></i></h5>
                    <p class="card-text">Payslip</p>
                </div>
            </div>
            </a>
        </div>

             <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-users fa-3x" style="color:#ff2f92;" aria-hidden="true"></i></h5>
                    <p class="card-text">Dependents</p>
                </div>
            </div>
            </a>
        </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-user-plus fa-3x" style="color:#00b0f0;" aria-hidden="true"></i></h5>
                    <p class="card-text">Employee Info</p>
                </div>
            </div>
            </a>
        </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
            <a href="#">
            <div class="card text- bg-ess1 mb-3 text-center"  style="background-color:#ffffff !important; border: 2px solid #6a923a !important;">  <!--style="max-width: 20rem;"-->
                <%--<div class="card-header"></div>--%>
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-users fa-3x" style="color:#203864;" aria-hidden="true"></i></h5>
                    <p class="card-text">Document</p>
                </div>
            </div>
            </a>
        </div>

        </div>
    </div>

    <hr />
    <h3>Layout 3</h3>
    <div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess1 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-home fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Leave Application</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess1 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-user fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">PDP</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess1 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-user fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">PDP - New Phase</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
    </div>
    </div>

    <div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess2 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-home fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Leave Approval</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess2 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-history fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Leave History</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess2 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-clipboard fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Payslip</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
    </div>
    </div>

    <div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess3 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-folder-open fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Document</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess3 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-users fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Dependents</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-md-3">
            <a href="#">
            <div class="card text-white bg-ess3 mb-3 text-center">  <!--style="max-width: 20rem;"-->
                <div class="card-header"><i class="fa fa-user fa-3x text-white" aria-hidden="true"></i></div>
                <div class="card-body">
                    <h5 class="card-title">Employee Info</h5>
                    <p class="card-text">Some quick example text.</p>
                </div>
            </div>
            </a>
        </div>
    </div>
    </div>
</asp:Content>

