
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic

Partial Class LeaveApprovalEmail
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Private m_bIsTerminating As Boolean = False
    Dim id As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not (Request.QueryString("id") Is Nothing) Then
                ViewState("id") = (Request.QueryString("id").Replace(" ", "+"))

                GetEmployeeDetails(ViewState("id"))


            End If
        End If
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim lblEMP_ID As String
            Dim lblAPS_ID, lblActualELA_ID As String
            lblEMP_ID = ViewState("ela_emp_id")
            lblAPS_ID = ViewState("aps_id")
            lblActualELA_ID = ViewState("ELA_id")

            'GetEmpLeaveCounts(hfEmpId.Value, hfElt_ID.Value)
            'GetEmpLeaveCounts(lblActualELA_ID.Text, hfElt_ID.Value)

            'If trMsg.Visible = True And ddLeaveStatus.SelectedValue = "A" And chkSaveAnyway.Checked = False Then
            '    Exit Sub
            'End If
            Dim isHRApproval As Boolean
            Dim ApprStatus As String

            ApprStatus = "A"

            Dim bitDocCheck As Boolean = 0

            retval = SaveLEAVEAPPROVAL(Session("sbsuid"), lblEMP_ID, txtComments.Text.Trim, lblAPS_ID, ApprStatus, Session("sUsr_name"), isHRApproval, stTrans, bitDocCheck)
            If retval = "0" Then
                stTrans.Commit()

                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                Dim flagAudit As Integer = 0
                '    UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, _
                'lblEMP_ID & "-" & txtComments.Text.Trim & "-" & lblAPS_ID & "-" & _
                '"A", _
                ' "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                RibbonRejected.Style.Add("display", "none")
                RibbonApproved.Style.Add("display", "block")
                btn1.Style.Add("display", "none")
                btn2.Style.Add("display", "none")
            Else
                stTrans.Rollback()

            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
                clear()
            End If
        End Try
    End Sub
       
    Private Function SaveLEAVEAPPROVAL(ByVal p_BSU_ID As String, ByVal p_EMP_ID As String, _
    ByVal p_REMARKS As String, ByVal p_APS_ID As Integer, ByVal p_Status As String, _
    ByVal p_USER As String, ByVal isHRApproval As Boolean, ByVal p_stTrans As SqlTransaction, Optional ByVal p_DocCheck As Boolean = 0) As String
        '@return_value = [dbo].[LEAVEAPPROVAL]
        '@BSU_ID = N'XXXXXX',
        '@EMP_ID = N'1003', 
        '@REMARKS = N'VERUTHE 1-3 APP',
        '@APS_ID = 95,
        '@STATUS = N'H',
        '@USER = N'LEVEL 1-3'
        Try
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_EMP_ID
            pParms(2) = New SqlClient.SqlParameter("@REMARKS", SqlDbType.VarChar, 200)
            pParms(2).Value = p_REMARKS
            pParms(3) = New SqlClient.SqlParameter("@APS_ID", SqlDbType.Int)
            pParms(3).Value = p_APS_ID
            pParms(4) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar, 1)
            pParms(4).Value = p_Status
            pParms(5) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 20)
            pParms(5).Value = p_USER
            pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            pParms(7) = New SqlClient.SqlParameter("@IsHRApproval", SqlDbType.Bit)
            pParms(7).Value = isHRApproval
            pParms(8) = New SqlClient.SqlParameter("@IsDocVerified", SqlDbType.Bit)
            pParms(8).Value = p_DocCheck
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "LEAVEAPPROVAL", pParms)
            If pParms(6).Value = "0" Then
                SaveLEAVEAPPROVAL = pParms(6).Value
            Else
                SaveLEAVEAPPROVAL = "1000"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            SaveLEAVEAPPROVAL = "1000"
        End Try
    End Function

    Protected Sub btnReject_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim lblEMP_ID As String
            Dim lblAPS_ID, lblActualELA_ID As String
            lblEMP_ID = ViewState("ela_emp_id")
            lblAPS_ID = ViewState("aps_id")
            lblActualELA_ID = ViewState("ELA_id")

            'GetEmpLeaveCounts(hfEmpId.Value, hfElt_ID.Value)
            'GetEmpLeaveCounts(lblActualELA_ID.Text, hfElt_ID.Value)

            'If trMsg.Visible = True And ddLeaveStatus.SelectedValue = "A" And chkSaveAnyway.Checked = False Then
            '    Exit Sub
            'End If
            Dim isHRApproval As Boolean
            Dim ApprStatus As String

            ApprStatus = "R"

            Dim bitDocCheck As Boolean = 0

            retval = SaveLEAVEAPPROVAL(Session("sbsuid"), lblEMP_ID, txtComments.Text.Trim, lblAPS_ID, ApprStatus, Session("sUsr_name"), isHRApproval, stTrans, bitDocCheck)
            If retval = "0" Then
                stTrans.Commit()

                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, _
                Dim flagAudit As Integer = 0
                '    UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, _
                'lblEMP_ID & "-" & txtComments.Text.Trim & "-" & lblAPS_ID & "-" & _
                '"A", _
                ' "Insert", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                RibbonApproved.Style.Add("display", "none")
                RibbonRejected.Style.Add("display", "block")
                btn1.Style.Add("display", "none")
                btn2.Style.Add("display", "none")

            Else
                stTrans.Rollback()

            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
                clear()
            End If
        End Try

       
    End Sub
    Private Sub GetEmployeeDetails(ByVal id As String)
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As New DataSet()

            Dim params(2) As SqlParameter

            params(0) = New SqlClient.SqlParameter("@APS_id", SqlDbType.VarChar)
            params(0).Value = id




            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[getLeaveApprovalDetails]", params)


            If ds.Tables(0).Rows.Count > 0 Then
                lblEmpName.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                lblLeaveType.Text = ds.Tables(0).Rows(0)("ELT_DESCR").ToString
                lblPeriod.Text = ds.Tables(0).Rows(0)("Leavedate").ToString
                lblDays.Text = ds.Tables(0).Rows(0)("ELA_LEAVEDAYS").ToString
                txtLeaveDetail.Text = ds.Tables(0).Rows(0)("ELA_REMARKS").ToString
                lblAvailed.Text = ds.Tables(0).Rows(0)("Availed").ToString
                lblBalance.Text = ds.Tables(0).Rows(0)("Balance").ToString
                txtComments.Text = ""
                Session("sUsr_name") = ds.Tables(0).Rows(0)("username").ToString
                ViewState("ela_emp_id") = ds.Tables(0).Rows(0)("ela_emp_id").ToString
                ViewState("ELA_id") = ds.Tables(0).Rows(0)("ELA_id").ToString
                ViewState("aps_id") = ds.Tables(0).Rows(0)("APS_id").ToString
                ViewState("aps_status") = ds.Tables(0).Rows(0)("aps_status").ToString
            End If
            If ViewState("aps_status") = "A" Then
                RibbonApproved.Style.Add("display", "block")
                RibbonRejected.Style.Add("display", "none")
                btn1.Style.Add("display", "none")
                btn2.Style.Add("display", "none")
                clear()
            End If
            If ViewState("aps_status") = "R" Then
                RibbonApproved.Style.Add("display", "none")
                RibbonRejected.Style.Add("display", "block")
                btn1.Style.Add("display", "none")
                btn2.Style.Add("display", "none")
                clear()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)

        End Try
    End Sub
    Sub clear()
        Session("sUsr_name") = ""
        ViewState("ela_emp_id") = ""
        ViewState("ELA_id") = ""
        ViewState("aps_id") = ""
        ViewState("aps_status") = ""
    End Sub
End Class

