Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class ChangePassword
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim bPasswordUpdate As Boolean = False
        If Page.IsValid = True Then
            Try
                If txtCurrentPassword.Text = txtNewpassword.Text Then
                    lblError.Text = "Current Password and New Password are Same!!!Please Re Enter the Passwords"
                    txtCurrentPassword.Focus()
                    Exit Sub
                End If
                If Not UtilityObj.PasswordVerify(txtConfPassword.Text, 0, 0, 0) Then
                    lblError.Text = UtilityObj.getErrorMessage("781")
                    Exit Sub
                End If
                'Get the user name and password using sqlDataReader
                Try
                    Dim status As Integer
                    Dim transaction As SqlTransaction
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try
                            'call the class to insert user password
                            status = UtilityObj.VerifyandUpdatePassword(Encr_decrData.Encrypt(txtCurrentPassword.Text), _
                            Encr_decrData.Encrypt(txtNewpassword.Text), Session("sUsr_name"), transaction)
                            If status = 0 Then
                                status = UtilityObj.operOnAudiTable("Change Password", Session("sUsr_name"), "Password Updated", Page.User.Identity.Name.ToString, Me.Page)
                            End If
                            If status = 0 Then
                                bPasswordUpdate = True
                                lblError.Text = String.Format("{0}, your password is updated successfully", Session("sUsr_name").ToUpper)
                                ClientScript.RegisterStartupScript(Me.GetType(), _
       "script", "<script language='javascript'>alert('   Your Password is Changed.\n Enter New Password to Login');  window.close(); </script>")
                            Else
                                lblError.Text = UtilityObj.getErrorMessage(status)
                            End If
                            transaction.Commit()
                            If bPasswordUpdate Then
                                status = VerifyandUpdatePassword(Encr_decrData.Encrypt(txtNewpassword.Text), _
           Session("sUsr_name"))
                                Dim vGLGUPDPWD As New com.ChangePWDWebService
                                vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                                Dim respon As String = vGLGUPDPWD.ChangePassword(Session("sUsr_name").ToUpper, txtNewpassword.Text, txtNewpassword.Text)

                            End If

                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message)
                            transaction.Rollback()
                            lblError.Text = "Error while updating your password"
                        End Try
                    End Using
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, "Change password")
                    lblError.Text = UtilityObj.getErrorMessage("1000")
                End Try



            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Change password")
                lblError.Text = UtilityObj.getErrorMessage("1000")
            End Try
        End If
    End Sub

    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, _
        ByVal username As String) As Integer
        Dim connection As SqlConnection
        Try
            connection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value

        Catch
        Finally
            connection.Close()
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/login.aspx")
    End Sub



    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtCurrentPassword.Text = ""
        txtConfPassword.Text = ""
        txtNewpassword.Text = ""
    End Sub

End Class
