<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TypingDetails.aspx.vb" Inherits="Inventory_TypingDetails" Title="Typing Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitTypeAutoComplete();
            InitEmpAutoComplete();
        });
        function InitializeRequest(sender, args) {
        }
        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitTypeAutoComplete();
            InitEmpAutoComplete();
        }
        function InitTypeAutoComplete() {
            $("#<%=txtType.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/PHOENIX/FileMgmt/TypingDetails.aspx/GetTransactionType",
                        data: "{ 'prefixText': '" + request.term + "','contextKey':'" + GetContextKey() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item,
                                    val: item
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    //$("#<%=hfType.ClientID%>").val(i.item.val);
                    PageMethods.GetDocumentRate(document.getElementById('<%=txtBSU.ClientID %>').value, i.item.label, CallSuccess, CallFailed);
                },
                minLength: 1
            });
        }
        function GetContextKey() {
            var bsuid = '<%= STUBSU_ID%>';
            return bsuid;
        }
        function InitEmpAutoComplete() {
            $("#<%=txtEmployee.ClientID%>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/PHOENIX/FileMgmt/TypingDetails.aspx/GetEmployee",
                        data: "{ 'prefixText': '" + request.term + "','ctxKey':'" + document.getElementById('<%=txtBSU.ClientID %>').value + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item,
                                    val: item
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    //$("#<%=hfType.ClientID%>").val(i.item.val);
                    PageMethods.GetEmpDetails(document.getElementById('<%=txtType.ClientID %>').value, i.item.label, document.getElementById('<%=txtBSU.ClientID %>').value, CallSuccessEmp, CallFailedEmp);
                },
                minLength: 1
            });
        }
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }

        function SetContextKey() {
            var x = document.getElementById('<%=txtBSU.ClientID %>');
            $find('AutoCompleteEx3').set_contextKey(x.value);
            return;
        }

        function GetRate() {
            PageMethods.GetDocumentRate(document.getElementById('<%=txtBSU.ClientID %>').value, document.getElementById('<%=txtType.ClientID %>').value, CallSuccess, CallFailed);
     return;
 }

 function CallSuccess(res) {
     document.getElementById('<%=txtRate.ClientID %>').value = res;
 }

 function CallFailed(res) {
     document.getElementById('<%=txtRate.ClientID %>').value = res; // alert(res.get_message());
 }

 function GetEmpDetails() {
     PageMethods.GetEmpDetails(document.getElementById('<%=txtType.ClientID %>').value, document.getElementById('<%=txtEmployee.ClientID %>').value, document.getElementById('<%=txtBSU.ClientID %>').value, CallSuccessEmp, CallFailedEmp);
     return;
 }

 function CallSuccessEmp(res) {
     document.getElementById('empDetails').innerHTML = res;
 }

 function CallFailedEmp(res) {
     document.getElementById('empDetails').value = res; // alert(res.get_message());
 }
    </script>
    <style type="text/css">
        .textchange {
            padding-left: 8px !important;
            height: 250px !important;
            overflow-x: auto !important;
            background-color: #ffffff !important;
            list-style: none !important;
        }

        . textchange-hover {
            cursor: pointer !important;
            font-weight: bold !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-tasks mr-3"></i>Typing Details
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Panel ID="Pnl" runat="server" DefaultButton="btnSave">

                    <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                        <tr>
                            <td align="left" width="100%">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table align="center" width="100%">

                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDate" PopupButtonID="lnkDate"></ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">BSU</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtBSU" runat="server" MaxLength="100"></asp:TextBox>
                                            <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID="AutoCompleteEx1"
                                                CompletionListCssClass="autocomplete_completionListElement_S1 textchange" CompletionListItemCssClass="autocomplete_listItem_S1"
                                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S2 textchange-hover"
                                                CompletionSetCount="5" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                ServiceMethod="GetBSU" ServicePath="TypingDetails.aspx" TargetControlID="txtBSU">
                                            </ajaxToolkit:AutoCompleteExtender>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Transaction Type</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtType" runat="server" MaxLength="100"></asp:TextBox>
                                            <asp:HiddenField ID="hfType" runat="server" />
                                            <%--<ajaxToolkit:AutoCompleteExtender ID="acType" runat="server" BehaviorID="AutoCompleteEx2"
                                                CompletionListCssClass="autocomplete_completionListElement_S1 textchange" CompletionListItemCssClass="autocomplete_listItem_S1"
                                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S2 textchange-hover"
                                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1" UseContextKey="true"
                                                ServiceMethod="GetTransactionType" ServicePath="/PHOENIX/FileMgmt/TypingDetails.aspx" TargetControlID="txtType">
                                            </ajaxToolkit:AutoCompleteExtender>--%>
                                        </td>
                                        <td align="left" width="20%"><span class="field-label">Employee</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtEmployee" runat="server" MaxLength="100"></asp:TextBox>
                                            <%--<ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" BehaviorID="AutoCompleteEx3"
                                                CompletionListCssClass="autocomplete_completionListElement_S1 textchange" UseContextKey="true"
                                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S2 textchange-hover" CompletionListItemCssClass="autocomplete_listItem_S1"
                                                CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                                ServiceMethod="GetEmployee" ServicePath="TypingDetails.aspx" TargetControlID="txtEmployee">
                                            </ajaxToolkit:AutoCompleteExtender>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Fees</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtRate" runat="server" MaxLength="100"></asp:TextBox></td>
                                        <td align="left" width="20%"><span class="field-label">Notes</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtNotes" runat="server" MaxLength="100"></asp:TextBox></td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters">
                                <span id="empDetails" style="font-family: Verdana"></span></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" UseSubmitBehavior="False" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%" align="center">
                                    <tr>
                                        <td class="title-bg" colspan="4">Employee search</td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
                                            <span class="field-label">FromDate</span></td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgFromDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtFromDate" PopupButtonID="imgFromDate"></ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td align="left" width="20%">
                                            <span class="field-label">ToDate</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtToDate" runat="server" Width="90px"></asp:TextBox>
                                            <asp:ImageButton ID="imgToDate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtToDate" PopupButtonID="imgToDate"></ajaxToolkit:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
                                            <span class="field-label">BSU</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtBSUId" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%">
                                            <span class="field-label">Account</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtAccountId" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
                                            <span class="field-label">Transaction Type</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtTransactionId" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%">
                                            <span class="field-label">Employee</span>
                                        </td>
                                        <td align="left" width="30%">
                                            <asp:TextBox ID="txtEmployeeId" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnFind" runat="server" CssClass="button" Text="Find.." />
                                        </td>
                                    </tr>


                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row" Width="100%"
                                    EmptyDataText="No Data Found" AutoGenerateColumns="False" DataKeyNames="Id" AllowPaging="True" PageSize="10">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="linkEdit" runat="server" OnClick="linkEdit_Click" Text='<%# Bind("trn_id") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="5%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" ItemStyle-Width="40" HeaderText="Date" />
                                        <asp:BoundField DataField="BSU" ItemStyle-Width="50" HeaderText="BSU" />
                                        <asp:BoundField DataField="Account" ItemStyle-Width="100" HeaderText="Account" />
                                        <asp:BoundField DataField="Transaction" ItemStyle-Width="250" HeaderText="Transaction Type" />
                                        <asp:BoundField DataField="Employee" ItemStyle-Width="250" HeaderText="Employee" />
                                        <asp:BoundField DataField="trn_rate" ItemStyle-Width="50" HeaderText="Fees" />
                                        <asp:BoundField DataField="typ_charge" ItemStyle-Width="50" HeaderText="Charge" />
                                        <asp:BoundField DataField="Details" ItemStyle-Width="120" HeaderText="Details" />
                                        <asp:BoundField DataField="User" ItemStyle-Width="120" HeaderText="User" />
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:Button ID="deleteButton" CssClass="button" runat="server" CommandName="Delete" Text="Delete"
                                                    OnClientClick="return confirm('Are you sure you want to delete ?');" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-info">
                                <asp:Repeater runat="server" ID="rptTotals">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCaption" runat="server"><%#DataBinder.Eval(Container.DataItem, "actName")%></asp:Label>
                                        <asp:Label ID="lblValue" runat="server"><%#DataBinder.Eval(Container.DataItem, "actValue")%></asp:Label>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom">
                                <asp:HiddenField ID="h_PRI_ID" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

            </div>
        </div>
    </div>

</asp:Content>

