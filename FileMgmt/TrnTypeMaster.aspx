<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TrnTypeMaster.aspx.vb" Inherits="Inventory_TrnTypeMaster" title="Visa Transaction Type Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }

</script>
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 60%">
        <tr>
            <td align="left" width="100%">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                &nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td class="matters" style="height: 54px; font-weight: bold;" valign="top">
                <table align="center" border="1" width="100%" class="BlueTableView" bordercolor="#1b80b6" cellpadding="5" cellspacing="0">
                    <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                            <span style="font-family: Verdana">
                                Visa Transaction Type Master</span></font></td></tr>                                
                    <tr>
                        <td align="left" class="matters"  >
                            Account Description</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                               <asp:DropDownList ID="ddlAccount" runat="server"></asp:DropDownList></td></tr>
                    <tr>
                        <td align="left" class="matters"  >
                            Transaction Type Description</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                               <asp:TextBox ID="txtTypDescr" runat="server" TabIndex="1" MaxLength="100" 
                                   Width="288px"></asp:TextBox></td></tr>
                    <tr>
                        <td align="left" class="matters"  >
                            Fees</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                               <asp:TextBox ID="txtTypRate" runat="server" TabIndex="1" MaxLength="100" 
                                   Width="88px"></asp:TextBox></td></tr>
                    <tr>
                        <td align="left" class="matters"  >
                            Employee Mandatory</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                               <asp:CheckBox ID="chkEmployee" runat="server"></asp:CheckBox></td></tr>
                    <tr>
                        <td align="left" class="matters"  >
                            Charge</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                               <asp:TextBox ID="txtCharge" runat="server" TabIndex="1" MaxLength="100" 
                                   Width="88px"></asp:TextBox></td></tr></table>
               </td>    
        </tr>
       
        <tr>
            <td class="matters" style="height: 19px" valign="bottom">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
        </tr>
        <tr>
            <td class="matters" valign="bottom" style="height: 52px">
                <asp:HiddenField ID="h_Typ_ID" runat="server" />
          &nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

