Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports System.Collections.Generic

Partial Class Inventory_TypingDetails
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64
    Public Property STUBSU_ID() As String
        Get
            Return ViewState("STU_BSU_ID")
        End Get
        Set(ByVal value As String)
            ViewState("STU_BSU_ID") = value
        End Set
    End Property
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePageMethods = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                acBSU.ContextKey = Session("sBsuid")
                'acType.ContextKey = Session("sBsuid")
                STUBSU_ID = Session("sBsuid")
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "FD00018") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                clearScreen()
                refreshGrid()
                If Request.QueryString("VIEWID") <> "" Then
                    h_PRI_ID.Value = Encr_decrData.Decrypt(Request.QueryString("VIEWID").Replace(" ", "+"))
                    showEdit(h_PRI_ID.Value)
                    btnSave.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub refreshGrid()
        Dim ds As New DataSet
        'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
        'sqlWhere = ""
        'If txtBSUId.Text <> "" Then sqlWhere &= " and TRN_BSU_ID like '%" & txtBSUId.Text & "%' "
        'If txtAccountId.Text <> "" Then sqlWhere &= " and ACT_DESCR like '%" & txtAccountId.Text & "%' "
        'If txtEmployeeId.Text <> "" Then sqlWhere &= " and EMP_NAME like '%" & txtEmployeeId.Text & "%' "
        'If txtTransactionId.Text <> "" Then sqlWhere &= " and TRN_DESCR like '%" & txtTransactionId.Text & "%' "
        'If txtFromDate.Text <> "" Then sqlWhere &= " and TRN_DATE>='" & txtFromDate.Text & "' "
        'If txtToDate.Text <> "" Then sqlWhere &= " and TRN_DATE<='" & txtToDate.Text & "' "
        'If ("MOHAMMEDIQBA.T_GCO,AKHTAR.P_GCO,hamza.mohammed,HAMZA.M_GCO,francis.sebastian,vijay.n,MOHAMMED.ISLAM").Contains(Session("sUsr_id")) Then

        '    sqlWhere &= " and case when TRN_BSU_ID in ('sts','bbt','SHT','S10','STG','SDE','SAR') then case when TRN_DATE<'1-oct-2014' then 1 else 0 end else 1 end=1 "


        'ElseIf ("Javad,MOHAMMED.JAVAD_STS").Contains(Session("sUsr_id")) Then

        '    sqlWhere &= " and case when TRN_BSU_ID in ('sts','bbt','SHT','S10','STG','SDE','SAR') then case when TRN_DATE>='1-oct-2014' then 1 else 0 end else 0 end=1 "

        'End If
        'sqlWhere &= " AND bsu_id IN (select USA_BSU_ID FROM oasis..USERACCESS_S where USA_USR_ID='" & Session("sUsr_id") & "')"
        'txtBSU.Focus()

        'ds = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select TRN_ID ID, TRN_ID, replace(convert(varchar(30), TRN_DATE, 106),' ','/') [Date], TRN_BSU_ID BSU, ACT_DESCR Account, TYP_DESCR [Transaction], EMP_NAME Employee, TRN_RATE, TRN_DESCR Details, isnull(TRN_USRID,'') [User], cast(TYP_CHARGE as int) TYP_CHARGE from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VISACCOUNT on TYP_ACT_ID=ACT_ID LEFT OUTER JOIN VW_EMPLOYEE_V on TRN_EMP_ID=EMP_ID inner join oasis..businessunit_m on bsu_shortname=trn_bsu_id where TRN_DELETED=0 " & sqlWhere & " order by TRN_DATE desc")
        'GridViewShowDetails.DataSource = ds
        'GridViewShowDetails.DataBind()
        'Try
        '    Dim dsRep As DataSet = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select ACT_DESCR+':' ActName, sum(TRN_RATE) ActValue from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VISACCOUNT on TYP_ACT_ID=ACT_ID inner join oasis..businessunit_m on bsu_shortname=trn_bsu_id where TRN_DELETED=0 " & sqlWhere & " GROUP by ACT_DESCR UNION select 'COUNT:', count(*) from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VISACCOUNT on TYP_ACT_ID=ACT_ID inner join oasis..businessunit_m on bsu_shortname=trn_bsu_id where 1=1 and TRN_DELETED=0 " & sqlWhere)
        '    rptTotals.DataSource = dsRep
        '    rptTotals.DataBind()
        'Catch ex As Exception

        'End Try


        If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")

        Dim PARAM(7) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", txtBSUId.Text)
        PARAM(1) = New SqlParameter("@txtAccountId", txtAccountId.Text)
        PARAM(2) = New SqlParameter("@txtEmployeeId", txtEmployeeId.Text)
        PARAM(3) = New SqlParameter("@txtTransactionId", txtTransactionId.Text)
        PARAM(4) = New SqlParameter("@Username", Session("sUsr_id"))
        PARAM(5) = New SqlParameter("@FROMDATE", txtFromDate.Text)
        PARAM(6) = New SqlParameter("@TODATE", txtToDate.Text)

        

        ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetTypingDetails", PARAM)

        If Not ds.Tables(0) Is Nothing Then
            GridViewShowDetails.DataSource = ds.Tables(0)
            GridViewShowDetails.DataBind()
        End If
        


        Try
            If Not ds.Tables(1) Is Nothing Then
                rptTotals.DataSource = ds.Tables(1)
                rptTotals.DataBind()
            End If

        Catch ex As Exception

        End Try



        'SqlHelper.ExecuteNonQuery(CONN, CommandType.StoredProcedure, "[dbo].[SaveReportSubscription]", PARAM)








    End Sub

    Private Sub clearScreen()
        h_PRI_ID.Value = "0"
        txtBSU.Text = ""
        txtEmployee.Text = ""
        txtNotes.Text = ""
        txtRate.Text = ""
        txtType.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Property sqlWhere() As String
        Get
            Return ViewState("sqlWhere")
        End Get
        Set(ByVal value As String)
            ViewState("sqlWhere") = value
        End Set
    End Property

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        saveData()
    End Sub

    Private Sub saveData()
        Dim empDetails As String(), empNo As String = "", empName As String = ""
        lblError.Text = ""
        If txtDate.Text.Trim = "" Then lblError.Text &= "Date,"
        If txtBSU.Text.Trim = "" Then lblError.Text &= "BSU,"
        If txtType.Text.Trim = "" Then lblError.Text &= "Transaction Type,"
        'If txtEmployee.Text.Trim = "" Then lblError.Text &= "Employee,"
        If txtRate.Text.Trim = "" Then lblError.Text &= "Rate,"
        'If SqlHelper.ExecuteScalar(connectionString, CommandType.Text, "select count(*) FROM oasis.dbo.USERACCESS_S inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=USA_BSU_ID where BSU_SHORTNAME='" & txtBSU.Text & "' and USA_USR_ID='" & Session("sUsr_id") & "'") = 0 Then lblError.Text &= "BSU,"
        If lblError.Text = "" Then
            Dim sqlStr As String
            If txtEmployee.Text.Replace("'", "").Contains("-") Then
                empDetails = txtEmployee.Text.Replace("'", "").Split("-")
                empNo = empDetails(0)
                empName = empDetails(1)
            Else
                empNo = ""
                empName = txtEmployee.Text.Replace("'", "")
            End If
            sqlStr = "select (select count(*) FROM OASIS.dbo.BUSINESSUNIT_M where BSU_SHORTNAME='" & txtBSU.Text & "') BSU, "
            sqlStr &= "(SELECT count(*)+(select count(*) FROM VISTRNTYPE where TYP_DESCR='" & txtType.Text & "' AND TYP_DEPOSIT=1) from VW_EMPLOYEE_V where (EMPNO='" & empNo & "' or replace(EMP_NAME,'''','')='" & empName & "')) EMPLOYEE, "
            sqlStr &= "(select count(*) FROM VISTRNTYPE where TYP_DESCR='" & txtType.Text & "') TRNTYPE"
            Dim myReader As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, sqlStr)
            myReader.Read()

            If myReader("BSU") = 0 Then lblError.Text &= "BSU,"
            If myReader("Employee") = 0 Then lblError.Text &= "Employee,"
            If myReader("TrnType") = 0 Then lblError.Text &= "Transaction Type,"

            myReader.Close()
            myReader = Nothing
        End If
        If lblError.Text.Length > 0 Then
            lblError.Text = lblError.Text.Substring(0, lblError.Text.Length - 1) & " Invalid"
            lblError.Visible = True
            txtBSU.Focus()
            Return
        End If
        Try
            Dim pParms(9) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TRN_ID", h_PRI_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TRN_TYP_ID", txtType.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@TRN_BSU_ID", txtBSU.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TRN_DATE", txtDate.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TRN_DESCR", txtNotes.Text, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@TRN_RATE", txtRate.Text, SqlDbType.VarChar)
            pParms(7) = Mainclass.CreateSqlParameter("@TRN_EMPLOYEE", empName, SqlDbType.VarChar)
            pParms(8) = Mainclass.CreateSqlParameter("@TRN_USRID", Session("sUsr_id"), SqlDbType.VarChar)
            pParms(9) = Mainclass.CreateSqlParameter("@EMPNO", empNo, SqlDbType.VarChar)

            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveVISTRANS", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    clearScreen()
                    refreshGrid()
                    lblError.Text = "Data Saved Successfully !!!"
                End If
                Exit Sub

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetBSU(ByVal prefixText As String, ByVal contextKey As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
        Dim str_Sql As String

        str_Sql = " select BSU_SHORTNAME FROM oasis.dbo.BUSINESSUNIT_M where BSU_ID in (select USA_BSU_ID FROM oasis.dbo.USERACCESS_S where USA_USR_ID='" & contextKey & "') and BSU_SHORTNAME like '%" & prefixText & "%' ORDER by BSU_SHORTNAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTransactionType(ByVal prefixText As String, ByVal contextKey As String) As String()
        'UtilityObj.Errorlog("Inside GetTransactionType", "PHOENIX")
        Dim Encr_decrData As New Encryption64
        Dim BSU_ID As String = String.Empty
        If Not IsNumeric(contextKey) Then
            BSU_ID = Encr_decrData.Decrypt(contextKey)
        Else
            BSU_ID = contextKey
        End If

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "select distinct TYP_DESCR FROM VISTRNTYPE inner join OASIS.dbo.businessunit_m WITH(NOLOCK) on bsu_id='" & BSU_ID & "' "
        str_Sql &= "WHERE TYP_DELETED=0 and TYP_DESCR like '%" & prefixText & "%' "
        str_Sql &= "and ((BSU_CITY='DXB' and replace(replace(typ_descr,'AUH',''),'SHJ','')=typ_descr) "
        str_Sql &= "or (BSU_CITY<>'DXB' and replace(typ_descr,BSU_CITY,'')<>typ_descr)) ORDER by TYP_DESCR"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count

        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetEmployee(ByVal prefixText As String, ByVal ctxKey As String) As String()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
        Dim str_Sql As String
        If ctxKey = "" Then
            str_Sql = " select EMPNO+'-'+EMP_NAME+'['+BSU_SHORTNAME+']' FROM VW_EMPLOYEE_V inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=EMP_BSU_ID where (EMP_NAME like '%" & prefixText & "%' or EMPNO like '%" & prefixText & "%') ORDER by EMP_NAME "
        Else
            str_Sql = " select EMPNO+'-'+EMP_NAME FROM VW_EMPLOYEE_V inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=EMP_BSU_ID where BSU_SHORTNAME='" & ctxKey & "' AND (EMP_NAME like '%" & prefixText & "%' or EMPNO like '%" & prefixText & "%') ORDER by EMP_NAME "
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer
        For i2 = 0 To intRows - 1
            items.Add(Convert.ToString(ds.Tables(0).Rows(i2)(0)))
        Next
        Return items.ToArray()
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetDocumentRate(ByVal txtBSU As String, ByVal txtType As String) As String
        Dim Result As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
        Dim documentRate As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "select TYP_RATE from VISTRNTYPE where TYP_DELETED=0 and TYP_DESCR='" & txtType & "'")
        Try
            If documentRate.HasRows Then
                documentRate.Read()
                Result = documentRate(0)
            Else
                Result = "0"
            End If
        Catch ex As Exception
            Return "0" 'ex.Message
        End Try
        documentRate.Close()
        documentRate = Nothing
        Return Result
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetEmpDetails(ByVal txtType As String, ByVal txtEmp As String, ByVal txtBSU As String) As String
        Dim Result As String = "", QRY As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
        Dim empDetails As String(), empNo As String = "", empName As String = ""

        If txtEmp.Replace("'", "").Contains("-") Then
            empDetails = txtEmp.Replace("'", "").Split("-")
            empNo = empDetails(0)
            empName = empDetails(1)
        Else
            empNo = ""
            empName = txtEmp.Replace("'", "")
        End If

        'Result = "select '<td '+case when TYP_DESCR='" & txtType & "' then 'style=""color:red""' else '' end+'>['+TYP_DESCR+'</td><td '+case when TYP_DESCR='" & txtType & "' then 'style=""color:red""' else '' end+'>'+replace(convert(varchar(30), TRN_DATE, 106),' ','/')+'</td><td '+case when TYP_DESCR='" & txtType & "' then 'style=""color:red""' else '' end+'>'+cast(TRN_RATE as varchar)+']</td>' "
        'Result &= "FROM VW_EMPLOYEE_V "
        'Result &= "inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=EMP_BSU_ID inner JOIN VISTRANS on TRN_EMP_ID=EMP_ID INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID "
        'Result &= "where TRN_DELETED=0 and TYP_DELETED=0 and BSU_SHORTNAME='" & txtBSU & "' "
        'If empNo = "" Then
        '    Result &= "and EMP_NAME='" & empName & "' "
        'Else
        '    Result &= "and EMPNO='" & empNo & "' "
        'End If
        'Result &= "ORDER by TRN_DATE DESC"
        QRY = "EXEC dbo.GET_EMPLOYEE_TYPING_DATA @BSU_SHORTNAME = '" & txtBSU.Trim & "',@TYP_DESCR='" & txtType.Trim & "',@EMP_NO='" & empNo & "',@EMP_NAME='" & empName & "'"

        Dim documentRate As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, QRY)
        'Result = "<table><tr>"
        Try
            If documentRate.HasRows Then
                While documentRate.Read()
                    Result &= documentRate(0)
                End While
            Else
                Result = ""
            End If
        Catch ex As Exception
            Return "" 'ex.Message
        End Try
        'Result &= "</tr></table>"
        documentRate.Close()
        documentRate = Nothing
        Return Result
    End Function

    Protected Sub linkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkEdit As LinkButton = sender
        showEdit(linkEdit.Text)
    End Sub

    Private Sub showEdit(ByVal showId As Integer)
        Dim documentRate As SqlDataReader = SqlHelper.ExecuteReader(connectionString, CommandType.Text, "select TRN_ID, replace(convert(varchar(30), TRN_DATE, 106),' ','/') [Date], TRN_BSU_ID BSU, TYP_DESCR [Transaction], EMP_NAME Employee, TRN_RATE, TRN_DESCR Details from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VW_EMPLOYEE_V on TRN_EMP_ID=EMP_ID where TRN_DELETED=0 and TRN_ID='" & showId & "'")

        If documentRate.HasRows Then
            documentRate.Read()
            h_PRI_ID.Value = documentRate("TRN_ID")
            txtBSU.Text = documentRate("BSU")
            txtDate.Text = documentRate("DATE")
            txtEmployee.Text = documentRate("EMPLOYEE")
            txtNotes.Text = documentRate("DETAILS")
            txtRate.Text = documentRate("TRN_RATE")
            txtType.Text = documentRate("TRANSACTION")
        End If
        documentRate.Close()
        documentRate = Nothing
        'bindDetails(linkRecptNo.Text, Session("sBsuid"))
    End Sub

    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        refreshGrid()
    End Sub

    Protected Sub GridViewShowDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewShowDetails.RowDeleting
        lblError.Text = ""
        Dim objConn As New SqlConnection(connectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim pParms(3) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TRN_ID", GridViewShowDetails.DataKeys(e.RowIndex).Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TRN_TYP_ID", 0, SqlDbType.Int)
            pParms(3) = Mainclass.CreateSqlParameter("@TRN_USRID", Session("sUsr_id"), SqlDbType.VarChar)
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveVISTRANS", pParms)
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                stTrans.Commit()
                clearScreen()
                refreshGrid()
                lblError.Text = "Data Saved Successfully !!!"
            End If
            Exit Sub

        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "")
            Errorlog(ex.Message)
            Exit Sub
        Finally
            objConn.Close()
        End Try

        refreshGrid()
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        refreshGrid()
    End Sub

    Protected Sub hfType_ValueChanged(sender As Object, e As EventArgs) Handles hfType.ValueChanged

    End Sub
End Class



