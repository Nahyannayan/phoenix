Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj

Partial Class Inventory_TrnTypeMaster
    Inherits System.Web.UI.Page
    Dim MainObj As Mainclass = New Mainclass()
    Dim connectionString As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Page.Title = OASISConstants.Gemstitle
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "FD00016") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                If Request.QueryString("TYPE") <> "" Then
                    ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
                End If
                If Request.QueryString("viewid") <> "" Then
                    SetDataMode("view")
                    setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                    UtilityObj.beforeLoopingControls(Me.Page)
                Else
                    setModifyHeader(0)
                    SetDataMode("add")
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String) 'setting header data on view/edit
        Try
            fillDropdown(ddlAccount, "SELECT ACT_ID, ACT_DESCR from VISACCOUNT order by ACT_DESCR", "ACT_DESCR", "ACT_ID", False)
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataset(connectionString, CommandType.Text, "select * from VISTRNTYPE where TYP_ID=" & p_Modifyid).Tables(0)
            If dt.Rows.Count > 0 Then
                h_Typ_ID.Value = dt.Rows(0)("TYP_ID").ToString
                txtTypDescr.Text = dt.Rows(0)("TYP_DESCR").ToString
                txtTypRate.Text = dt.Rows(0)("TYP_RATE").ToString
                txtCharge.Text = dt.Rows(0)("TYP_CHARGE").ToString
                chkEmployee.Checked = (dt.Rows(0)("TYP_DEPOSIT") = 0)
                ddlAccount.SelectedValue = dt.Rows(0)("TYP_ACT_ID").ToString
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connectionString)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        If addValue.Equals(True) Then
            'drpObj.Items.Insert(0, " ")
            'drpObj.Items(0).Value = "0"
            'drpObj.SelectedValue = "0"
        End If
    End Sub

    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
        End If
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        txtTypDescr.Enabled = Not mDisable
        ddlAccount.Enabled = Not mDisable
        txtTypRate.Enabled = Not mDisable
        txtCharge.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_Typ_ID.Value = 0
        txtTypDescr.Text = ""
        txtTypRate.Text = "0"
        ddlAccount.SelectedIndex = 0
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            SqlHelper.ExecuteNonQuery(connectionString, CommandType.Text, "update VISTRNTYPE set TYP_DELETED=1 where TYP_ID=" & h_Typ_ID.Value)
            Response.Redirect(ViewState("ReferrerUrl"))

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblError.Text = ""
            If txtTypDescr.Text.Trim = "" Then
                lblError.Text = "Transaction type description cannot be blank"
                Exit Sub
            End If

            Dim pParms(6) As SqlParameter
            pParms(1) = Mainclass.CreateSqlParameter("@TYP_ID", h_Typ_ID.Value, SqlDbType.Int, True)
            pParms(2) = Mainclass.CreateSqlParameter("@TYP_DESCR", txtTypDescr.Text, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@TYP_RATE", txtTypRate.Text, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@TYP_CHARGE", txtCharge.Text, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@TYP_EMPLOYEE", IIf(chkEmployee.Checked, 0, 1), SqlDbType.Int)
            pParms(6) = Mainclass.CreateSqlParameter("@TYP_ACT_ID", ddlAccount.SelectedValue, SqlDbType.VarChar)
            Dim objConn As New SqlConnection(connectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveTRNTYPE", pParms)
                If RetVal = "-1" Then
                    lblError.Text = "Unexpected Error !!!"
                    stTrans.Rollback()
                    Exit Sub
                Else
                    stTrans.Commit()
                    ViewState("EntryId") = pParms(1).Value
                    lblError.Text = "Data Saved Successfully !!!"
                    setModifyHeader(ViewState("EntryId"))
                    SetDataMode("view")
                End If

            Catch ex As Exception
                Errorlog(ex.Message)
                lblError.Text = ex.Message
            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub



End Class



