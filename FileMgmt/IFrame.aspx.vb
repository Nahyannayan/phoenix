﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Net
Imports System.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib.Checksums
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.GZip

Partial Class FM_IFrame
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim FileName As String = "", newImage As Boolean = True
        If Request.QueryString("id").ToString = "0" Then
            FileName = Request.QueryString("filename").ToString
        Else
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim rdrDocument As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "select doi_content_type, Doi_File_Name, Doi_Upload from docimage where doi_id=" & Request.QueryString("id").ToString)
            If rdrDocument.HasRows Then
                rdrDocument.Read()
                FileName = Session.SessionID & rdrDocument("doi_file_name")
                Dim fsDataArray As New System.IO.FileStream(WebConfigurationManager.AppSettings.Item("TempFileFolder") & FileName & ".zip", System.IO.FileMode.Create)
                fsDataArray.Write(rdrDocument("doi_upload"), 0, rdrDocument("doi_upload").Length)
                fsDataArray.Close() : fsDataArray.Dispose()
            End If
            newImage = False
        End If

        If FileName.Length > 0 Then
            Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & FileName & ".zip")
            FileName = UnZip(WebConfigurationManager.AppSettings.Item("TempFileFolder") & FileName & ".zip", WebConfigurationManager.AppSettings.Item("TempFileFolder"))
            If Not newImage Then fileToDelete.Delete()

            Dim ViewServer, ViewURL As String
            ViewServer = Replace(WebConfigurationManager.AppSettings.Item("DocViewerURL"), "*", "&")
            ViewURL = WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & FileName
            'ViewURL = ViewServer & WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & FileName
            If Request.QueryString("type").ToString.Contains("image") Then
                iFrame.Attributes("src") = WebConfigurationManager.AppSettings.Item("TempFileFolderURL") & FileName
            Else
                iFrame.Attributes("src") = ViewURL
            End If
            iFrame.Attributes("width") = "100%"
            iFrame.Attributes("height") = "500px"
            'iFrame.Attributes("width") = "50px"
            'iFrame.Attributes("height") = "50px"
        End If
    End Sub

    Public Shared Function UnZip(ByVal SrcFile As String, ByVal DstFile As String) As String
        Dim fileStreamIn As FileStream = New FileStream(SrcFile, FileMode.Open, FileAccess.Read)
        Dim zipInStream As ZipInputStream = New ZipInputStream(fileStreamIn)
        Dim enTry As ZipEntry = zipInStream.GetNextEntry()
        Dim fileStreamOut As FileStream = New FileStream(DstFile + "\" + enTry.Name, FileMode.Create, FileAccess.Write)
        Dim size As Integer
        Dim buffer() As Byte = New Byte(4096) {}
        Do
            size = zipInStream.Read(buffer, 0, buffer.Length)
            fileStreamOut.Write(buffer, 0, size)
        Loop While size > 0
        zipInStream.Close()
        fileStreamOut.Close()
        fileStreamIn.Close()
        Return enTry.Name
    End Function

End Class
