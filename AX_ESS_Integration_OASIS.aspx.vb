﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports UtilityObj
Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Partial Class AX_ESS_Integration
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ViewState("USR_ID") = ""
                ViewState("USR_PWD") = ""
                ViewState("USR_MENU") = ""
                ViewState("IsDAX") = "0"
                If Not (Request.QueryString("U") Is Nothing) Then
                    ViewState("USR_ID") = Encr_decrData.Decrypt(Request.QueryString("U").Replace(" ", "+"))
                End If

                If Not (Request.QueryString("P") Is Nothing) Then
                    ViewState("USR_PWD") = Request.QueryString("P").Replace(" ", "+")
                End If

                If Not (Request.QueryString("C") Is Nothing) Then
                    ViewState("MENU_ID") = Encr_decrData.Decrypt(Request.QueryString("C").Replace(" ", "+"))
                End If
                If Not (Request.QueryString("DAX") Is Nothing) Then
                    ViewState("IsDAX") = Request.QueryString("DAX").Replace(" ", "+")
                End If

                'ViewState("USR_ID") = "rajesh"
                'ViewState("USR_PWD") = "zqLyS00DNJC5/Misv6giUA=="
                'ViewState("USR_MENU") = "https://school.gemsoasis.com/pdp_v2/pdpdashboard.aspx?MainMnu_code=xYNDeT925sY=&datamode=Zo4HhpVNpXc="
                'ViewState("USR_MENU") = "~\pdp\pdpdashboard.aspx?MainMnu_code=xYNDeT925sY=&datamode=Zo4HhpVNpXc="

                If ViewState("IsDAX") = "1" Then
                    ViewState("USR_MENU") = "/ESSDashboard.aspx"
                    Session("sModule") = "SS"
                Else
                    get_usr_menu()
                End If
                

                Validate_User()


            End If
        Catch myex As ArgumentException

            UtilityObj.Errorlog(myex.Message, "Logoff Process")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Logoff Process")
        End Try

    End Sub

    Sub get_usr_menu()

        Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@LNK_ID", ViewState("MENU_ID"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "get_AX_ESS_LINKS", pParms)
            While reader.Read

                ViewState("USR_MENU") = Convert.ToString(reader("LNK_URL"))
                Session("sModule") = Convert.ToString(reader("LNK_MODULE"))

            End While
        End Using
    End Sub
    Protected Sub Validate_User()
        'check whether user is expired
        Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", ViewState("USR_ID"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "Get_USR_NAME_ACTIVE", pParms)
            While reader.Read
                Session("sUsr_name") = ViewState("USR_ID")
                ViewState("USR_PWD") = Convert.ToString(reader("PASSWORD"))
                Dim lstrActive = Convert.ToString(reader("CURRSTATUS"))
                If lstrActive = "EXPIRED" Then
                    Response.Redirect("https://school.gemsoasis.com/noAccess.aspx")
                End If
            End While
        End Using


        Dim passwordEncr As New Encryption64
        Dim username As String = ViewState("USR_ID")
        Dim password As String = ViewState("USR_PWD")
        Dim tblusername As String = ""
        Dim tblpassword As String = ""
        Dim tblroleid As Integer = 0
        Dim tblbsuper As Boolean = False
        Dim tblbITSupport As Boolean = False
        Dim tblbsuid As String = ""
        Dim tblbUsr_id As String = ""
        Dim tblDisplay_usr As String = ""
        Dim bCount As Integer = 0
        Dim var_F_Year As String = ""
        Dim var_F_Year_ID As String = ""
        Dim var_F_Date As String = ""
        Dim bUSR_MIS As Integer = 0
        Dim USR_bShowMISonLogin As Integer = 0
        Dim tblModuleAccess As String = ""
        Dim USR_FCM_ID As String = ""
        Dim tblReqRole As String = ""
        Dim tblDays As Integer = 0

        Dim tblBSU_Name As String = ""
        Dim tblEMPID As String = ""
        Dim tblCOSTEMP As String = ""
        Dim tblCOSTSTU As String = ""
        Dim tblCOSTOTH As String = ""
        Dim tblUSR_ForcePwdChange As Integer = 0

        Session("tot_bsu_count") = "0"

        Try
            Dim dt As New DataTable
            Dim trycount As Integer
            '''''''''''
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "1000"
                retval = AccessRoleUser.GetUserDetails(username, password, "", "", dt, trycount, stTrans)
                ' retval = AccessRoleUser.GetUserDetails(username, password, HttpContext.Current.Request.UserHostAddress.ToString(), "", dt, trycount, stTrans)

                stTrans.Commit()
                If retval = "0" Then
                    If dt.Rows.Count > 0 Then
                        tblusername = Convert.ToString(dt.Rows(0)("usr_name"))
                        tblDisplay_usr = Convert.ToString(dt.Rows(0)("Display_Name"))
                        tblpassword = passwordEncr.Decrypt(Convert.ToString(dt.Rows(0)("usr_password")).Replace(" ", "+"))
                        tblroleid = Convert.ToInt32(dt.Rows(0)("usr_rol_id"))
                        tblbsuper = IIf(IsDBNull(dt.Rows(0)("usr_bsuper")), False, dt.Rows(0)("usr_bsuper"))
                        tblbITSupport = IIf(IsDBNull(dt.Rows(0)("USR_bITSupport")), False, dt.Rows(0)("USR_bITSupport"))
                        tblbsuid = Convert.ToString(dt.Rows(0)("usr_bsu_id"))
                        tblbUsr_id = Convert.ToString(dt.Rows(0)("usr_id"))
                        tblModuleAccess = Convert.ToString(dt.Rows(0)("USR_MODULEACCESS"))
                        tblReqRole = Convert.ToString(dt.Rows(0)("USR_REQROLE"))
                        tblEMPID = Convert.ToString(dt.Rows(0)("USR_EMP_ID"))
                        USR_FCM_ID = Convert.ToString(dt.Rows(0)("USR_FCM_ID"))
                        tblBSU_Name = Convert.ToString(dt.Rows(0)("BSU_NAME"))
                        tblCOSTEMP = dt.Rows(0)("COSTcenterEMP").ToString()
                        tblCOSTSTU = dt.Rows(0)("COSTcenterSTU").ToString()
                        tblCOSTOTH = dt.Rows(0)("COSTcenterOTH").ToString()
                        tblUSR_ForcePwdChange = dt.Rows(0)("USR_ForcePwdChange")
                        If IsDBNull(dt.Rows(0)("USR_MIS")) Then
                            bUSR_MIS = 0
                        Else
                            bUSR_MIS = Convert.ToInt32(dt.Rows(0)("USR_MIS"))
                        End If
                        If IsDBNull(dt.Rows(0)("USR_bShowMISonLogin")) Then
                            USR_bShowMISonLogin = 0
                        Else
                            USR_bShowMISonLogin = Convert.ToInt32(dt.Rows(0)("USR_bShowMISonLogin"))
                        End If

                        tblDays = CInt(dt.Rows(0)("Days"))
                        Session("lastlogtime") = Convert.ToString(dt.Rows(0)("lastlogtime"))
                    End If
                ElseIf retval = "560" Then
                    'Response.Redirect("common/loginwarning.aspx?un=" & txtUsername.Text.Trim(), False)
                    'HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'm_bIsTerminating = True
                    Session("sUsr_name") = username
                    
                    'DisableUserLogin()
                    Exit Sub
                ElseIf retval = "2600" Then
                    Dim RedirectUsername As String = ViewState("USR_ID")
                    Dim RedirectPassword As String = ViewState("USR_PWD")
                    Dim LogTime As String = Now.ToString("dd-MMM-yyyyHH")
                    Dim RedirectQueryStr As String
                    RedirectQueryStr = RedirectUsername & "|" & RedirectPassword & "|" & LogTime
                    RedirectQueryStr = passwordEncr.Encrypt(RedirectQueryStr)
                    Dim URL As String
                    URL = WebConfigurationManager.AppSettings("OldOasisURL") & "/loginRedirect.aspx?OASISLogin=" & RedirectQueryStr
                    ' Response.Redirect(URL, False)
                
                   
                End If
            Catch ex As Exception
                stTrans.Rollback()
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try

            password = passwordEncr.Decrypt(password.Replace(" ", "+"))
            'check for  password matches
            If password = tblpassword Then
                'if valid hold the required information into the session
                Session("sUsr_id") = tblbUsr_id
                'to used for all the page
                Session("sUsr_name") = tblusername
                Session("sUsr_Display_Name") = tblDisplay_usr
                Session("sroleid") = tblroleid
                Session("sBusper") = tblbsuper
                Session("sBsuid") = tblbsuid
                Session("sBITSupport") = tblbITSupport
                Session("BSU_Name") = tblBSU_Name
                Session("sModuleAccess") = tblModuleAccess
                Session("sReqRole") = tblReqRole
                Session("EmployeeId") = tblEMPID
                Session("counterId") = USR_FCM_ID

                Session("CostEMP") = tblCOSTEMP
                Session("CostSTU") = tblCOSTSTU
                Session("CostOTH") = tblCOSTOTH

                'insert the session id of the cuurently logged in user
                Session("Sub_ID") = "007"

                Session("ForcePwdChange") = tblUSR_ForcePwdChange
                If Session("ForcePwdChange") = 1 Then
                    Response.Redirect("Forcepasswordchange.aspx")
                End If


                'If Request.QueryString("hd") = 1 Then
                '    Session("hdv2") = "1"
                '    Session("ActiveTab") = Request.QueryString("atab")
                '    Session("Task_ID") = Request.QueryString("tlid")
                '    Response.Redirect("~\HelpDesk\Version2\Pages\hdTabs.aspx")

                'End If
                'If Request.QueryString("ss") = 1 Then
                '    Dim url As String
                '    url = String.Format("homepageSS.aspx")
                '    Session("sModule") = "SS"
                '    getSettings(Session("sBsuid"))
                '    Response.Redirect(url)
                'End If
                Using RoleReader As SqlDataReader = AccessRoleUser.GetRoles_modules(tblroleid)
                    While RoleReader.Read
                        Session("ROL_MODULE_ACCESS") = Convert.ToString(RoleReader("ROL_MODULE_ACCESS"))
                    End While
                End Using

                'if success write into the table users_m put the sessionID in  usr_session
                Using collectReader As SqlDataReader = AccessRoleUser.getCollectBank(tblbsuid)
                    While collectReader.Read
                        Session("CollectBank") = Convert.ToString(collectReader("CollectBank"))
                        Session("Collect_name") = Convert.ToString(collectReader("Collect_name"))
                    End While
                End Using
                Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M_ID(tblbsuid)
                    While CurriculumReader.Read
                        Session("CLM") = Convert.ToString(CurriculumReader("BSU_CLM_ID"))
                    End While
                End Using

                Dim SessionFlag As Integer
                If ViewState("MENU_ID") = 25 Then
                    SessionFlag = AccessRoleUser.UpdateSessionID(tblusername, Session.SessionID)
                    If SessionFlag <> 0 Then
                        Throw New ArgumentException("Unable to track your request")
                    End If
                End If
               

                Using UserreaderFINANCIALYEAR As SqlDataReader = AccessRoleUser.GetFINANCIALYEAR()
                    While UserreaderFINANCIALYEAR.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        var_F_Year_ID = Convert.ToString(UserreaderFINANCIALYEAR("FYR_ID"))
                        var_F_Year = Convert.ToString(UserreaderFINANCIALYEAR("FYR_Descr"))
                        var_F_Date = Convert.ToString(UserreaderFINANCIALYEAR("FYR_TODT"))
                    End While
                    'clear of the resource end using
                End Using
                Session("F_YEAR") = var_F_Year_ID
                Session("F_Descr") = var_F_Year
                Session("F_TODT") = Format(var_F_Date, OASISConstants.DateFormat)

                str_conn = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim SQLStr As String
                SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1"
                Dim dtFinYear As DataTable
                dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
                If dtFinYear.Rows.Count > 0 Then
                    Session("F_YEAR") = dtFinYear.Rows(0).Item("FYR_ID")
                    Session("F_Descr") = dtFinYear.Rows(0).Item("FYR_DESCR")
                    Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
                End If


                'Dim OasisSettingsCookie As HttpCookie
                'If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
                '    OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
                'Else
                '    OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
                'End If
                'OasisSettingsCookie.Values("UsrName") = txtUsername.Text
                'OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
                'Response.Cookies.Add(OasisSettingsCookie)
                'If tblDays <= 0 Then
                '    hfExpired.Value = "expired"
                '    Exit Sub
                'Else
                '    hfExpired.Value = ""
                'End If

                Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
                If auditFlag <> 0 Then
                    Throw New ArgumentException("Unable to track your request")
                End If
                'if super admin allow all businessunit access
                Session("USR_MIS") = bUSR_MIS
                Session("USR_bShowMISonLogin") = USR_bShowMISonLogin

                'If USR_bShowMISonLogin = 1 Then
                '    Response.Redirect("~/Management/empManagementMain.aspx", False)
                '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    m_bIsTerminating = True
                '    Exit Sub
                'ElseIf USR_bShowMISonLogin = 2 Then
                '    Response.Redirect("~/Management/empManagementPrincipal.aspx", False)
                '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    m_bIsTerminating = True
                '    Exit Sub
                'End If
                If tblbsuper = True Then

                    ' Response.Redirect("BusinessUnit.aspx")
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        While BUnitreader.Read
                            bCount += 1
                            If bCount = 1 Then
                                Session("BSU_ROUNDOFF") = Convert.ToInt32(BUnitreader("Roundoff"))
                                Session("BSU_CURRENCY") = Convert.ToString(BUnitreader("BSU_CURRENCY"))
                                Session("BSU_PAYYEAR") = Convert.ToInt32(Session("F_YEAR"))
                                Session("BSU_PAYMONTH") = Convert.ToInt32(Month(Date.Today))
                            End If
                        End While
                    End Using
                    Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                        While BSUInformation.Read
                            Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                            Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                            Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                            Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                            Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                        End While
                    End Using
                    Session("tot_bsu_count") = "1"
                    'Response.Redirect("BusinessUnit.aspx", False)
                    Response.Redirect(ViewState("USR_MENU"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    ' m_bIsTerminating = True
                    Exit Sub

                Else
                    'if user with single business unit direct the user to module login page else
                    'allow the user to select the business unit
                    Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                        While BUnitreader.Read
                            bCount += 1
                            If bCount = 1 Then
                                Session("BSU_ROUNDOFF") = Convert.ToInt32(BUnitreader("Roundoff"))
                                Session("BSU_CURRENCY") = Convert.ToString(BUnitreader("BSU_CURRENCY"))
                                Session("BSU_PAYYEAR") = Convert.ToInt32(Session("F_YEAR"))
                                Session("BSU_PAYMONTH") = Convert.ToInt32(Month(Date.Today))
                            End If
                        End While
                    End Using

                    Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(tblbsuid)
                        While BSUInformation.Read
                            Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                            Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                            Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                            Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                            Session("BSU_bMOE_TC") = Convert.ToString(BSUInformation("BSU_bMOE_TC"))
                        End While
                    End Using
                End If
                'check the number of business unit for the current user
                If bCount > 1 Then
                    Session("tot_bsu_count") = "1"
                    'Response.Redirect("BusinessUnit.aspx")
                    Response.Redirect(ViewState("USR_MENU"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    ' m_bIsTerminating = True
                    Exit Sub
                Else
                    getSettings(Session("sBsuid"))
                    'Dim auditFlag As Integer = UtilityObj.operOnAudiTable("Login", "login", "Login", Page.User.Identity.Name.ToString)
                    'If auditFlag <> 0 Then
                    '    Throw New ArgumentException("Unable to track your request")
                    'End If
                    Session("tot_bsu_count") = "0"

                    'If ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                    '    Session("sModule") = "E0"
                    '    Response.Redirect("~\eduShield\eduShield_Home.aspx", False)
                    'Else
                    'Response.Redirect("Modulelogin.aspx", False)
                    'End If
                    Response.Redirect(ViewState("USR_MENU"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'm_bIsTerminating = True
                    Exit Sub
                End If
                'Else
                '
            End If
        Catch sqlEx As SqlException
            'lblResult.Text = "Unable to process your request"
            UtilityObj.Errorlog(sqlEx.Message, "Login1")
        Catch generalEx As Exception
            'lblResult.Text = "Invalid username or password"
            UtilityObj.Errorlog(generalEx.Message, "Login2")
        End Try
    End Sub
    Private Sub getSettings(ByVal BusUnit As String)
        Try
            If Not ((Session("sroleid") = "46") Or (Session("sroleid") = "47")) Then
                Using BSUInformation As SqlDataReader = AccessRoleUser.GetBSUInformation(BusUnit)
                    While BSUInformation.Read
                        Session("CLM") = Convert.ToString(BSUInformation("BSU_CLM_ID"))
                        Session("BSU_CITY") = Convert.ToString(BSUInformation("BSU_CITY"))
                        Session("BSU_COUNTRY_ID") = Trim(Convert.ToString(BSUInformation("BSU_COUNTRY_ID")))
                        Session("BSU_BFlexiblePlan") = Convert.ToString(BSUInformation("BSU_BFlexiblePlan"))
                        Session("BSU_SMS_FROM") = Convert.ToString(BSUInformation("BSU_SMS_FROM"))
                        Session("CollectBank") = Convert.ToString(BSUInformation("CollectBank"))
                        Session("Collect_name") = Convert.ToString(BSUInformation("Collect_name"))
                        Session("BSU_ROUNDOFF") = Convert.ToInt32(BSUInformation("BSU_ROUNDOFF"))
                        Session("BSU_CURRENCY") = Convert.ToString(BSUInformation("BSU_CURRENCY"))
                        Session("BSU_PAYYEAR") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Session("F_YEAR"), BSUInformation("BSU_PAYYEAR")))
                        Session("BSU_PAYMONTH") = Convert.ToInt32(IIf(IsDBNull(BSUInformation("BSU_PAYYEAR")) = True, Month(Date.Today), BSUInformation("BSU_PAYMONTH")))
                        Dim strformat As String = "0."
                        For i As Integer = 1 To CInt(Session("BSU_ROUNDOFF"))
                            strformat = strformat & "0"
                        Next
                        Session("BSU_DataFormatString") = strformat
                    End While
                End Using
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim SQLStr As String
                SQLStr = "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1"
                Dim dtFinYear As DataTable
                dtFinYear = Mainclass.getDataTable(SQLStr, str_conn)
                If dtFinYear.Rows.Count > 0 Then
                    Session("F_YEAR") = dtFinYear.Rows(0).Item("FYR_ID")
                    Session("F_Descr") = dtFinYear.Rows(0).Item("FYR_DESCR")
                    Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(dtFinYear.Rows(0).Item("FYR_ID"))
                End If
            End If

            'Dim OasisSettingsCookie As HttpCookie
            'If Request.Cookies("OASIS_SETTINGS") Is Nothing Then
            '    OasisSettingsCookie = New HttpCookie("OASIS_SETTINGS")
            'Else
            '    OasisSettingsCookie = Request.Cookies("OASIS_SETTINGS")
            'End If
            'OasisSettingsCookie.Expires = DateTime.Now.AddDays(30)
            'Response.Cookies.Add(OasisSettingsCookie)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Business Unit Selection")
        End Try
    End Sub
End Class
