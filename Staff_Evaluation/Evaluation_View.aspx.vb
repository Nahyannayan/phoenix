﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Partial Class Staff_Evaluation_Evaluation_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000126" And ViewState("MainMnu_code") <> "H000991") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindBusinessUnit()
                    BindGrid()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1

        ddl_bsu.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            'ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
            Bind_EvalTemplate()
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_EvalTemplate()
    End Sub
    Public Sub Bind_EvalTemplate()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", IIf(ddl_bsu.SelectedIndex > 0, ddl_bsu.SelectedValue, "00"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
            ddl_EvalTemplate.DataSource = ds
            ddl_EvalTemplate.DataValueField = "EVT_ID"
            ddl_EvalTemplate.DataTextField = "EVT_TEMPLATE_DESC"
            ddl_EvalTemplate.DataBind()
            ddl_EvalTemplate.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
        Catch ex As Exception

        End Try
    End Sub

    Sub BindGrid()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        Dim ds As DataSet
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
        parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        parm(2) = New SqlClient.SqlParameter("@BSU_ID", IIf(ddl_bsu.SelectedIndex > 0, ddl_bsu.SelectedValue, DBNull.Value))
        parm(3) = New SqlClient.SqlParameter("@EVT_ID", IIf(ddl_EvalTemplate.SelectedIndex > 0, ddl_EvalTemplate.SelectedValue, DBNull.Value))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS_VIEW", parm)
        gv_EvaluatorMapping.DataSource = ds
        gv_EvaluatorMapping.DataBind()
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub
    Protected Sub btnClearFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearFilter.Click
        ddl_bsu.SelectedIndex = -1
        ddl_EvalTemplate.SelectedIndex = -1
        BindGrid()
    End Sub
    Protected Sub lnk_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EEM_ID As HiddenField = sender.parent.FindControl("HF_EEM_ID")
        Dim HF_EOB_ID As HiddenField = sender.parent.FindControl("HF_EOB_ID")
        ViewState("EEM_ID") = HF_EEM_ID.Value
        ViewState("EOB_ID") = HF_EOB_ID.Value
        BindPnlGrid(HF_EEM_ID.Value)
        pnl_SelectStaff.Visible = True
    End Sub
    Protected Sub rbl_EvlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbl_EvlStatus.SelectedIndexChanged
        BindPnlGrid(ViewState("EEM_ID"))
    End Sub
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_Search.Click
        BindPnlGrid(ViewState("EEM_ID"))
    End Sub
    Private Sub BindPnlGrid(ByVal EEM_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(5) As SqlClient.SqlParameter
            Dim ds As DataSet
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            parm(2) = New SqlClient.SqlParameter("@EEM_ID", EEM_ID)
            parm(3) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_Name.Text.Trim() <> "", txt_Name.Text.Trim(), DBNull.Value))
            parm(4) = New SqlClient.SqlParameter("@EMP_DES", IIf(txt_desg.Text.Trim() <> "", txt_Name.Text.Trim(), DBNull.Value))
            parm(5) = New SqlClient.SqlParameter("@ERM_STATUS", IIf(rbl_EvlStatus.SelectedValue <> "A", rbl_EvlStatus.SelectedValue, DBNull.Value))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS_VIEW", parm)
            gv_Staff.DataSource = ds
            gv_Staff.DataBind()
        Catch ex As Exception
            lbl_Saveerror.Text = ex.Message
        End Try
    End Sub
    Protected Sub lnkClose_Staff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClose_Staff.Click
        pnl_SelectStaff.Visible = False
    End Sub
    Protected Sub btn_PnlClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_PnlClose.Click
        pnl_SelectStaff.Visible = False
    End Sub
    Protected Sub lnk_delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
      
    End Sub
    Protected Sub lnk_Select_pnl_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EEM_ID As HiddenField = sender.parent.FindControl("HF_EEM_ID")
        Dim URL As String = "TemplateEvaluatorMap.aspx?MainMnu_code=" + Encr_decrData.Encrypt("H000125") + "&EEM_ID=" + Encr_decrData.Encrypt(HF_EEM_ID.Value)
        Response.Redirect(URL)
    End Sub

    Protected Sub gv_Staff_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gv_Staff.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem OrElse e.Item.ItemType = GridItemType.Item Then
            Dim AppStat As String = Nothing
            Dim HF_EVT_ID As HiddenField = DirectCast(e.Item.FindControl("HF_EVT_ID"), HiddenField)
            Dim HF_EEM_ID As HiddenField = DirectCast(e.Item.FindControl("HF_EEM_ID"), HiddenField)
            Dim HF_STE_ID As HiddenField = DirectCast(e.Item.FindControl("HF_STE_ID"), HiddenField)
            Dim HF_ERM_ID As HiddenField = DirectCast(e.Item.FindControl("HF_ERM_ID"), HiddenField)
            Dim lnk_Select_pnl As LinkButton = DirectCast(e.Item.FindControl("lnk_Select_pnl"), LinkButton)
            Dim EVT_ID = Encr_decrData.Encrypt(HF_EVT_ID.Value)
            Dim EEM_ID = Encr_decrData.Encrypt(HF_EEM_ID.Value)
            Dim STE_ID = Encr_decrData.Encrypt(HF_STE_ID.Value)
            Dim EOB_ID = Encr_decrData.Encrypt(ViewState("EOB_ID"))
            Dim ERM_ID = Encr_decrData.Encrypt(HF_ERM_ID.Value)

            Dim redirectUrl As String = Convert.ToString("StaffEvaluation.aspx?EVT_ID=" + EVT_ID + "&EEM_ID=" + EEM_ID + "&STE_ID=" + STE_ID + "&EOB_ID=" + EOB_ID + "&ERM_ID=" + ERM_ID)
            Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
            url = url.Remove(url.IndexOf("?"))
            url = url.Replace("http", "https")
            url = url.Replace("Evaluation_View.aspx", redirectUrl)

            Dim sb As StringBuilder = New StringBuilder("")
            sb.Append("PopupWindow('" + url + "');")
            lnk_Select_pnl.Attributes.Add("onclick", "javascript:return " + sb.ToString() + "")
        End If
    End Sub
    Protected Sub gv_Staff_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gv_Staff.PageIndexChanged
        gv_Staff.CurrentPageIndex = e.NewPageIndex
        gv_Staff.DataBind()
    End Sub
    Protected Sub gv_Staff_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_Staff.NeedDataSource
        BindPnlGrid(ViewState("EEM_ID"))
    End Sub

End Class
