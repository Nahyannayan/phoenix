﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
'ts
Partial Class Staff_Evaluation_M_EvaluationKeys
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000122" And ViewState("MainMnu_code") <> "H000994") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindBusinessUnit()
                    Bind_Grd_EvalKeys()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Public Sub Bind_Grd_EvalKeys()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm As SqlParameter() = New SqlParameter(1) {}
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_KEYS_M", parm)
            gv_EvalKeys.DataSource = ds
            Dim ge As New GridGroupByExpression
            Dim gf As New GridGroupByField
            gf.FieldName = "BSU_NAME"
            gf.HeaderText = "<span style='color:brown;font-weight:bold'>Business unit"

            ge.GroupByFields.Add(gf)
            ge.SelectFields.Add(gf)
            gv_EvalKeys.MasterTableView.GroupByExpressions.Add(ge)
            gv_EvalKeys.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lnk_AddNewKeys_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_AddNewKeys.Click
        ViewState("EKM_ID") = "0"
        pnl_EvalKey.Visible = True
        txtEvaluationkey.Text = ""
    End Sub
    Protected Sub lnkClose_EvalKey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClose_EvalKey.Click
        pnl_EvalKey.Visible = False
        txtEvaluationkey.Text = ""
    End Sub
    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        ViewState("EKM_ID") = "0"
        pnl_EvalKey.Visible = False
        txtEvaluationkey.Text = ""
    End Sub
    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click
        Try
            Dim constring As String = ConnectionManger.GetOASISConnectionString
            Dim param(27) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTION", IIf(ViewState("EKM_ID") = "0", 1, 2))
            param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(1).Direction = ParameterDirection.Output
            param(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            param(3) = New SqlClient.SqlParameter("@EKM_BSU_ID", ddl_bsu.SelectedValue)
            param(4) = New SqlClient.SqlParameter("@EKM_DESC", txtEvaluationkey.Text.Trim())
            param(5) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            If ViewState("EKM_ID") <> "0" Then
                param(6) = New SqlClient.SqlParameter("@EKM_ID", ViewState("EKM_ID"))
            End If
            SqlHelper.ExecuteNonQuery(constring, CommandType.StoredProcedure, "EVA.SAVE_EVAL_KEYS_M", param)
            Dim ReturnFlag As Integer = param(2).Value
            If (ReturnFlag = 0) Then
                lblError.Text = param(1).Value
                pnl_EvalKey.Visible = False
                Bind_Grd_EvalKeys()
            Else
                lbl_Saveerror.Text = param(1).Value
            End If
        Catch ex As Exception
            lbl_Saveerror.Text = ex.Message
        End Try
    End Sub
    Protected Sub lnk_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EKM_ID As HiddenField = sender.parent.FindControl("HF_EKM_ID")
        ViewState("EKM_ID") = HF_EKM_ID.Value

        Dim HF_BSU_ID As HiddenField = sender.parent.FindControl("HF_BSU_ID")
        ddl_bsu.SelectedValue = HF_BSU_ID.Value

        Dim lblEKM_DESC As Label = sender.parent.FindControl("lblEKM_DESC")
        txtEvaluationkey.Text = lblEKM_DESC.Text
        pnl_EvalKey.Visible = True
    End Sub
    Protected Sub lnk_delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_EKM_ID As HiddenField = sender.parent.FindControl("HF_EKM_ID")

            Dim constring As String = ConnectionManger.GetOASISConnectionString
            Dim param(27) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTION", 3)
            param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(1).Direction = ParameterDirection.Output
            param(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            param(3) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            param(4) = New SqlClient.SqlParameter("@EKM_ID", HF_EKM_ID.Value)
            SqlHelper.ExecuteNonQuery(constring, CommandType.StoredProcedure, "EVA.SAVE_EVAL_KEYS_M", param)
            Dim ReturnFlag As Integer = param(2).Value
            If (ReturnFlag = 0) Then
                lblError.Text = param(1).Value
                Bind_Grd_EvalKeys()
            Else
                lblError.Text = param(1).Value
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
End Class
