﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Partial Class Staff_Evaluation_TeacherSubjects
    Inherits System.Web.UI.Page
    Dim studClass As New studClass
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000128" And ViewState("MainMnu_code") <> "H000987") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    If Not Request.QueryString("EEM_ID") Is Nothing Then
                        ViewState("EEM_ID") = Encr_decrData.Decrypt(Request.QueryString("EEM_ID").Replace(" ", "+"))
                    Else
                        ViewState("EEM_ID") = 0
                    End If
                    Bind_GV_StaffSubjects()
                    BindBusinessUnit()
                    Bind_gv_staffEvaluated()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

#Region "Main"
    Protected Sub btn_Searchstaffsub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Searchstaffsub.Click
        Bind_GV_StaffSubjects()
    End Sub
    Protected Sub btn_ClearSearchstaffsub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ClearSearchstaffsub.Click
        txt_staffname_ess.Text = ""
        txt_subject_ess.Text = ""
        Bind_GV_StaffSubjects()
    End Sub
    Protected Sub gv_StaffSubjects_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_StaffSubjects.NeedDataSource
        Bind_GV_StaffSubjects()
    End Sub
    Private Sub Bind_GV_StaffSubjects()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            parm(2) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_staffname_ess.Text.Trim() = "", DBNull.Value, txt_staffname_ess.Text.Trim()))
            parm(3) = New SqlClient.SqlParameter("@SBG_DESCR", IIf(txt_subject_ess.Text.Trim() = "", DBNull.Value, txt_subject_ess.Text.Trim()))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_SUBJECT_M", parm)
            gv_StaffSubjects.DataSource = ds
            gv_StaffSubjects.DataBind()
        Catch ex As Exception
            ' lbldeletemsg.Text = ex.Message
        End Try
    End Sub
    Protected Sub lnk_editStaffSubj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn_addSubjects.Click
        ' pnl_AddStaffSubject.Visible = True
    End Sub
    Protected Sub lnk_deleteStaffSubj_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn_addSubjects.Click
        Try
            Dim HF_ESS_ID As HiddenField = TryCast(sender.parent.FindControl("HF_ESS_ID"), HiddenField)

            Dim constring As String = ConnectionManger.GetOASISConnectionString
            Dim param(8) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTION", 2)
            param(1) = New SqlClient.SqlParameter("@ESS_ID", HF_ESS_ID.Value)
            param(2) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            SqlHelper.ExecuteNonQuery(constring, CommandType.StoredProcedure, "EVA.SAVE_EVAL_STAFF_SUBJECT_M", param)
            lbldeletemsg.Text = "Deleted Sucessfully."
            Bind_GV_StaffSubjects()
        Catch ex As Exception
            lbldeletemsg.Text = ex.Message
        End Try
    End Sub
#End Region



    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
            ddl_bsu.Enabled = False
            Bind_AcademicYear()
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_AcademicYear()
        Bind_Grade()
    End Sub
    Private Sub Bind_AcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@CLM_ID", Session("CLM"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_acdYear.DataSource = ds
            ddl_acdYear.DataValueField = "ACD_ID"
            ddl_acdYear.DataTextField = "ACY_DESCR"
            ddl_acdYear.DataBind()

            Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                         & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + ddl_bsu.SelectedValue + "' AND ACD_CLM_ID=" + Session("CLM")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim li As New RadComboBoxItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            'ddl_acdYear.Items(ddl_acdYear.Items.IndexOf(li)).Selected = True
            ddl_acdYear.Items.FindItemByValue(ds.Tables(0).Rows(0).Item(1)).Selected = True
            'If Not ddl_acdYear.Items.FindItemByValue(Session("ACD_ID")) Is Nothing Then
            '    ddl_acdYear.Items.FindItemByValue(Session("ACD_ID")).Selected = True
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_acdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_acdYear.SelectedIndexChanged
        Bind_Grade()
    End Sub
    Private Sub Bind_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_grade.DataSource = ds
            ddl_grade.DataValueField = "grm_id"
            ddl_grade.DataTextField = "grm_grd_id"
            ddl_grade.DataBind()
            Bind_Subject()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_grade_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_grade.SelectedIndexChanged
        Bind_Subject()
    End Sub
    Private Sub Bind_Subject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@GRD_ID", ddl_grade.SelectedItem.Text)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_subject.DataSource = ds
            ddl_subject.DataValueField = "SBG_ID"
            ddl_subject.DataTextField = "SBG_DESCR"
            ddl_subject.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lnkbtn_addSubjects_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn_addSubjects.Click
        pnl_AddStaffSubject.Visible = True
        clear_pnl_AddStaffSubject()
    End Sub
    Protected Sub lnkbtn_AddStaffSubject_Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn_AddStaffSubject_Close.Click
        pnl_AddStaffSubject.Visible = False
        ViewState("Add_Option") = 0
        'clear_pnl_AddStaffSubject()
    End Sub
    Protected Sub btn_Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Clear.Click
        pnl_AddStaffSubject.Visible = False
        'clear_pnl_AddStaffSubject()
    End Sub
#Region "Panel_staff"
    Sub BindBusinessUnit_Pnl()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu_pnl.DataSource = ds.Tables(0)
        ddl_bsu_pnl.DataValueField = "BSU_ID"
        ddl_bsu_pnl.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu_pnl.DataBind()
        ddl_bsu_pnl.SelectedIndex = -1
        ddl_bsu.Enabled = False
        If Not ddl_bsu_pnl.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu_pnl.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Sub BindGrid_Staff_Pnl()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu_pnl.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_Name.Text.Trim() = "", DBNull.Value, txt_Name.Text.Trim()))
            parm(3) = New SqlClient.SqlParameter("@EMP_DES", IIf(txt_desg.Text.Trim() = "", DBNull.Value, txt_desg.Text.Trim()))
            parm(4) = New SqlClient.SqlParameter("@EMP_REPORTTO", IIf(hf_EMPReportingTo.Value.Trim() = "", DBNull.Value, hf_EMPReportingTo.Value.Trim()))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EMPDETAILS", parm)
            gv_Staff.DataSource = ds
            gv_Staff.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnk_Select_pnl_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub ddl_bsu_pnl_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu_pnl.SelectedIndexChanged
        BindGrid_Staff_Pnl()
    End Sub
    Protected Sub gv_Staff_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gv_Staff.PageIndexChanged
        gv_Staff.CurrentPageIndex = e.NewPageIndex
        gv_Staff.DataBind()
    End Sub
    Protected Sub gv_Staff_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_Staff.NeedDataSource
        BindGrid_Staff_Pnl()
    End Sub

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        Try

            Dim Add_Option As Integer = Convert.ToInt16(ViewState("Add_Option"))


            For Each gr As GridDataItem In gv_Staff.Items
                Dim cbSelect As CheckBox = gr.FindControl("cbSelect")
                If (cbSelect.Checked = True) Then

                    Dim lbl_EMPNAME As Label = TryCast(gr.FindControl("lbl_EMPNAME"), Label)
                    Dim HF_EMP_ID As HiddenField = TryCast(gr.FindControl("HF_EMP_ID"), HiddenField)
                    Dim HF_BSU_ID As HiddenField = TryCast(gr.FindControl("hf_BSU_ID"), HiddenField)
                    Dim lbl_bsu As Label = TryCast(gr.FindControl("lbl_bsu"), Label)
                    Dim HF_DES_ID As HiddenField = TryCast(gr.FindControl("HF_DES_ID"), HiddenField)
                    Dim lblDES_DESCR As Label = TryCast(gr.FindControl("lblDES_DESCR"), Label)
                    Dim find As String = "EMP_ID = '" + HF_EMP_ID.Value + "'"

                    Dim foundRows As DataRow() = DT_StaffEvaluated.Select(find)
                    If foundRows.Length = 0 Then
                        Dim dr As DataRow = DT_StaffEvaluated.NewRow()
                        dr("BSU_ID") = HF_BSU_ID.Value
                        dr("BSU_NAME") = lbl_bsu.Text
                        dr("EMP_NAME") = lbl_EMPNAME.Text
                        dr("EMP_ID") = HF_EMP_ID.Value
                        dr("DES_ID") = HF_DES_ID.Value
                        dr("DES_DESCR") = lblDES_DESCR.Text
                        dr("STATUS") = "ADD"
                        dr("ESS_ID") = 0
                        DT_StaffEvaluated.Rows.Add(dr)
                    End If
                End If
            Next
            Bind_gv_staffEvaluated()
            pnl_SelectStaff.Visible = False
        Catch ex As Exception

        End Try
    End Sub


    Public Property DT_StaffEvaluated() As DataTable
        Get
            If ViewState("StaffEvaluated") Is Nothing Then
                Dim dtStaffEvaluated As New DataTable()
                dtStaffEvaluated.Columns.Add("BSU_ID")
                dtStaffEvaluated.Columns.Add("BSU_NAME")
                dtStaffEvaluated.Columns.Add("EMP_NAME")
                dtStaffEvaluated.Columns.Add("EMP_ID")
                dtStaffEvaluated.Columns.Add("DES_ID")
                dtStaffEvaluated.Columns.Add("DES_DESCR")
                dtStaffEvaluated.Columns.Add("STATUS")
                dtStaffEvaluated.Columns.Add("ESS_ID")

                ViewState("StaffEvaluated") = dtStaffEvaluated
                Return DirectCast(ViewState("StaffEvaluated"), DataTable)
            Else
                Return DirectCast(ViewState("StaffEvaluated"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("StaffEvaluated") = value
        End Set
    End Property
#End Region

    Sub Bind_gv_staffEvaluated()
        Dim find As String = "STATUS <> 'DELETED'"
        gv_staffEvaluated.DataSource = DT_StaffEvaluated.Select(find)
        gv_staffEvaluated.DataBind()
    End Sub
    Protected Sub lnk_deleteStaff_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EMP_ID As HiddenField = TryCast(sender.parent.FindControl("HF_EMP_ID"), HiddenField)
        Dim find As String = "EMP_ID = '" + HF_EMP_ID.Value + "'"
        Dim dr As DataRow() = DT_StaffEvaluated.Select(find)
        dr(0)("STATUS") = "DELETED"

        Bind_gv_staffEvaluated()
    End Sub
    Protected Sub lnk_addStaff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_addStaff.Click
        ViewState("Add_Option") = 2
        BindBusinessUnit_Pnl()
        ddl_bsu_pnl.Enabled = False
        txt_Name.Text = ""
        txt_desg.Text = ""
        clear()
        BindGrid_Staff_Pnl()
        pnl_SelectStaff.Visible = True
    End Sub
    Protected Sub lnkClose_Staff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClose_Staff.Click
        pnl_SelectStaff.Visible = False
        ViewState("Add_Option") = 0
    End Sub
    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        pnl_SelectStaff.Visible = False
        ViewState("Add_Option") = 0
    End Sub
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        BindGrid_Staff_Pnl()
    End Sub

    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset.Click
        clear()
    End Sub
    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            '------START Saving  Staff Subjects----------' 
            If (ddl_bsu.SelectedValue <> "" And ddl_acdYear.SelectedValue <> "" And ddl_grade.SelectedValue <> "" And ddl_subject.SelectedValue <> "") Then
                For Each dr_StaffEvaluated As DataRow In DT_StaffEvaluated.Rows

                    Dim paramEvalKeys(8) As SqlClient.SqlParameter
                    paramEvalKeys(0) = New SqlClient.SqlParameter("@OPTION", IIf(dr_StaffEvaluated("STATUS") = "ADD" Or dr_StaffEvaluated("STATUS") = "EDIT", 1, 2))

                    paramEvalKeys(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    paramEvalKeys(1).Direction = ParameterDirection.ReturnValue
                    paramEvalKeys(2) = New SqlClient.SqlParameter("@EMP_ID", dr_StaffEvaluated("EMP_ID"))
                    paramEvalKeys(3) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
                    paramEvalKeys(4) = New SqlClient.SqlParameter("@GRD_ID", ddl_grade.SelectedItem.Text)
                    paramEvalKeys(5) = New SqlClient.SqlParameter("@GRM_ID", ddl_grade.SelectedValue)
                    paramEvalKeys(6) = New SqlClient.SqlParameter("@SBG_ID", ddl_subject.SelectedValue)
                    paramEvalKeys(7) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))

                    'paramEvalKeys(7) = New SqlClient.SqlParameter("@ESS_ID", dr_StaffEvaluated("ESS_ID"))
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "EVA.SAVE_EVAL_STAFF_SUBJECT_M", paramEvalKeys)
                Next
                sqltran.Commit()
                Bind_GV_StaffSubjects()
                pnl_AddStaffSubject.Visible = False
                lbldeletemsg.Text = "Saved Sucessfully."
                'Else
                '    lbl_Saveerror.Text = param(1).Value
                '    sqltran.Rollback()
                'End If
            Else
                lbl_Savemsg.Text = "Please enter values in all the required fields"
            End If
        Catch ex As Exception
            lbl_Saveerror.Text = ex.Message
            sqltran.Rollback()
        Finally
            ViewState("ESS_ID") = "0"
        End Try

    End Sub

    

    Public Sub clear()
        txt_EMPReportingTo.Text = ""
        txt_desg.Text = ""
        txt_Name.Text = ""
        hf_EMPReportingTo.Value = ""
    End Sub
    Public Sub clear_pnl_AddStaffSubject()
        ddl_acdYear.ClearSelection()
        ddl_acdYear_SelectedIndexChanged(ddl_acdYear, Nothing)
        ddl_grade_SelectedIndexChanged(ddl_grade, Nothing)
        DT_StaffEvaluated.Clear()
        Bind_gv_staffEvaluated()
    End Sub

End Class
