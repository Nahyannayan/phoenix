<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="M_EvaluationMainCriteria.aspx.vb" Inherits="Staff_Evaluation_M_EvaluationMainCriteria" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


         .darkPanlAlumini {
            width: 90%;
            height: 90%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 10%;
            top: 10%;
            position: fixed;
            width: 70%;
        }
         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }

    </style>
    <asp:Label ID="lblError" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Evaluation Main Criteria
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr >
                        <td><span class="field-label">Business unit</span></td>
                        <td>
                            <telerik:RadComboBox ID="ddl_bsu_search" runat="server" RenderMode="Lightweight" Width="100%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td><span class="field-label">Evaluation Template</span></td>
                        <td>
                            <telerik:RadComboBox ID="ddl_EvalTemplate_search" runat="server" RenderMode="Lightweight" Width="100%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr >
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <asp:LinkButton ID="lnk_AddEvalMainCriteria" runat="server" Font-Bold="true">Add Evaluation Main Criteria</asp:LinkButton></td>
                    </tr>
                </table>

                <telerik:RadGrid ID="gv_EvalMainCriteria" runat="server"
                    AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                    GridLines="None"  Width="100%">
                    <MasterTableView>
                        <CommandItemSettings />
                        <RowIndicatorColumn Visible="True">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True">
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Evaluation Template" UniqueName="TemplateColumn_EVT_TEMPLATE_DESC">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HF_EVT_ID" runat="server" Value='<%# Bind("EMC_EVT_ID") %>' />
                                    <asp:Label ID="lbl_EVT_TEMPLATE_DESC" runat="server" Text='<%# Bind("EVT_TEMPLATE_DESC") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Evaluation Main Criteria"
                                UniqueName="TemplateColumn">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HF_EMC_ID" runat="server" Value='<%# Bind("EMC_ID") %>' />
                                    <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("EMC_BSU_ID") %>' />
                                    <asp:Label ID="lblEMC_DESC" runat="server" Text='<%# Bind("EMC_DESC") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Edit"
                                UniqueName="TemplateColumn_VERIFIED">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk_edit" runat="server" OnClick="lnk_edit_Click"
                                        ToolTip="Click here to edit the template">Edit</asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridTemplateColumn HeaderText="Delete"
                                UniqueName="Template_Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk_delete" runat="server" OnClick="lnk_delete_Click">Delete</asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_delete" ConfirmText="Confirm delete"
                                        TargetControlID="lnk_delete" runat="server">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn>
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                    <ItemStyle  HorizontalAlign="Left" />
                    <AlternatingItemStyle  HorizontalAlign="Left" />
                    <FilterMenu>
                    </FilterMenu>
                </telerik:RadGrid>

                <asp:Panel ID="pnl_EvalMainCriteria" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini" style="overflow: auto;">
                        <div class="title-bg">
                            Evaluation Main Criteria
                            <div style="float: right">
                                <asp:LinkButton ForeColor="Red" ID="lnkClose_EvalMainCriteria" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClose_EvalMainCriteria_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <table style="width: 100%;">
                        <tr >
                            <td align="left" width="20%"><span class="field-label">Business Unit<span style="color: red">*</span></span></td>
                            <td align="left" width="30%">
                                <telerik:RadComboBox ID="ddl_bsu" runat="server" RenderMode="Lightweight" Width="90%"
                                    AutoPostBack="True">
                                </telerik:RadComboBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_bsu"
                                    ErrorMessage="Businessunit required" InitialValue="" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Evaluation Template<span style="color: red">*</span></span></td>
                            <td align="left" width="30%">
                                <telerik:RadComboBox ID="ddl_EvalTemplate" runat="server" RenderMode="Lightweight" Width="100%">
                                </telerik:RadComboBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddl_EvalTemplate"
                                    ErrorMessage="Evaluation template required" InitialValue="" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>

                        </tr>
                        <tr >
                            <td align="left"><span class="field-label">Evaluation Main Criteria <span style="color: red">*</span></span></td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="txtEvalMainCriteria" runat="server"  TextMode="MultiLine"
                                    Width="100%" MaxLength="1000"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEvalMainCriteria"
                                    ErrorMessage="Evaluation main criteria required" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr >
                            <td align="left"><span class="field-label">Evaluation Keys <span style="color: red">*</span></span></td>
                            <td align="left" colspan="3">
                                <telerik:RadGrid ID="gv_EvalKeys" runat="server"
                                    AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                                    GridLines="None"  Width="100%">
                                    <MasterTableView>
                                        <CommandItemSettings />
                                        <RowIndicatorColumn Visible="True">
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Visible="True">
                                            <HeaderStyle Width="20px" />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Select" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                                UniqueName="TemplateColumn_select">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ckb_select" runat="server" Checked='<%# Bind("EMK_BACTIVE") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Evaluation Key"
                                                UniqueName="TemplateColumn_EvaluationKey">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HF_EKM_ID" runat="server" Value='<%# Bind("EKM_ID") %>' />
                                                    <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("EKM_BSU_ID") %>' />
                                                    <asp:Label ID="lblEKM_DESC" runat="server" Text='<%# Bind("EKM_DESC") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Point"
                                                UniqueName="TemplateColumn_Point">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txt_EvalScore" runat="server" Width="40px" MaxLength="2" Text='<%# Bind("EMK_SCORE") %>'></asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server"
                                                        TargetControlID="txt_EvalScore"
                                                        FilterType="Numbers" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn>
                                            </EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                    <ItemStyle  HorizontalAlign="Left" />
                                    <AlternatingItemStyle  HorizontalAlign="Left" />
                                    <FilterMenu>
                                    </FilterMenu>
                                </telerik:RadGrid>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Evaluation keys required" Display="Dynamic" ClientValidationFunction="EvalKeys" ValidationGroup="Save"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr  align="center">
                            <td colspan="4">
                                <asp:Button ID="btn_save" runat="server" CssClass="button" Text="Save" ValidationGroup="Save" />

                                <asp:Button ID="btn_cancel" runat="server" CssClass="button" Text="Cancel" />
                                <br />
                                <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </div>

                    
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function EvalKeys(sender, args) {
            var checkBoxSelector = '#<%=gv_EvalKeys.ClientID%> input[id*="ckb_select"]:checkbox';
            var totalCheckboxes = $(checkBoxSelector),
                 checkedCheckboxes = totalCheckboxes.filter(":checked");
            if (checkedCheckboxes.length == 0) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
    </script>
</asp:Content>

