﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Imports System.IO
Imports System.Web.Configuration


Partial Class Staff_Evaluation_StaffEvaluation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        'Session("sUsr_name") = "ARUN"
        'Session("sBsuid") = "999998"
        'Dim EVT_ID As String = "4"
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                'get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'Session("MainMnu_code_pro") = ViewState("MainMnu_code")

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    If Not Request.QueryString("EVT_ID") Is Nothing And Not Request.QueryString("EEM_ID") Is Nothing And Not Request.QueryString("STE_ID") Is Nothing And Not Request.QueryString("EOB_ID") Is Nothing Then
                        ViewState("EVT_ID") = Encr_decrData.Decrypt(Request.QueryString("EVT_ID").Replace(" ", "+"))
                        ViewState("EEM_ID") = Encr_decrData.Decrypt(Request.QueryString("EEM_ID").Replace(" ", "+"))
                        ViewState("STE_ID") = Encr_decrData.Decrypt(Request.QueryString("STE_ID").Replace(" ", "+"))
                        ViewState("EOB_ID") = Encr_decrData.Decrypt(Request.QueryString("EOB_ID").Replace(" ", "+"))
                        'ViewState("ERM_ID") = Encr_decrData.Decrypt(Request.QueryString("ERM_ID").Replace(" ", "+"))
                        lbl_EVL_Date.Text = DateAndTime.Now.ToString("dd-MMM-yyyy")
                        txt_EvlDate.Text = DateAndTime.Now.ToString("dd/MMM/yyyy")
                        Bind_AcademicYear()
                        ViewState("ERM_ID") = LoadEvlResult_Details(ViewState("EEM_ID"), ViewState("STE_ID"), ViewState("EOB_ID"))
                        Load_EvalForm(ViewState("EVT_ID"))
                        Load_StaffDetails(ViewState("STE_ID"))
                        Load_ObserverDetails()

                        If (ViewState("ERM_bCOMPLETED")) Then
                            btnSubmit.Visible = False
                            btnDraft.Visible = False
                            gv_EvalMainCriteria.Enabled = False
                            tb_evalinfo.Disabled = True
                            lblMsg.Text = "This staff is already evaluated."
                        End If
                        ' Else
                        Dim sb As New System.Text.StringBuilder()
                        sb.Append("<script language='javascript'>")
                        sb.Append("radselected('" + ViewState("SelectedRadio") + "');")
                        sb.Append("</script>")
                        'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "JCall1", sb.ToString(), False)
                        Page.ClientScript.RegisterStartupScript(Me.[GetType](), "Script", sb.ToString(), False)


                        





                    Else
                        lbl_Panel_Close.Text = "Loading staff evaluation failed. Please close this window and try again"
                        Panel_Close.Visible = True
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lbl_Panel_Close.Text = "Loading the staff evaluation failed. Please close this window and try again"
                Panel_Close.Visible = True
            End Try
        Else
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<script language='javascript'>")
            sb.Append("radselected('" + ViewState("SelectedRadio") + "');")
            sb.Append("</script>")
            Page.ClientScript.RegisterStartupScript(Me.[GetType](), "Script", sb.ToString(), False)


        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub Load_EvalForm(ByVal EVT_ID As String)
        Bind_EvalMainCriteria(EVT_ID)
    End Sub
    Private Sub Load_StaffDetails(ByVal STE_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 5)
            parm(1) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            If (ds.Tables(0).Rows.Count > 0) Then
                lbl_StaffName.Text = Convert.ToString(ds.Tables(0).Rows(0)("EMP_NAME"))
                lbl_StaffDesg.Text = Convert.ToString(ds.Tables(0).Rows(0)("DES_DESCR"))
                ViewState("EMP_ID") = Convert.ToString(ds.Tables(0).Rows(0)("EMP_ID"))
                'Bind_Grade()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function LoadEvlResult_Details(ByVal EEM_ID As String, ByVal STE_ID As String, ByVal EOB_ID As String) As String
        Dim ERM_ID As String = "0"
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@EEM_ID", EEM_ID)
            parm(2) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
            parm(3) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
            'ERM_ID = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS_M", parm)

            Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS_M", parm)
                While dr.Read()
                    ERM_ID = dr("ERM_ID")
                    ViewState("ERM_bCOMPLETED") = dr("ERM_bCOMPLETED")
                    If (ERM_ID <> "0") Then
                        Bind_AcademicYear(dr("ERM_ACD_ID").ToString())
                        'ddl_acdYear.SelectedValue = dr("ERM_ACD_ID").ToString()
                        'ddl_acdYear_SelectedIndexChanged(ddl_acdYear, Nothing)
                        ddl_grade.SelectedValue = dr("ERM_GRM_ID")
                        ddl_grade_SelectedIndexChanged(ddl_grade, Nothing)
                        ddl_section.SelectedValue = dr("ERM_SEC_ID")
                        ddl_subject.SelectedValue = dr("ERM_SBG_ID")
                        txt_topic.Text = Convert.ToString(dr("ERM_TOPIC"))
                        txtRemarks.Text = Convert.ToString(dr("ERM_REMARKS"))
                        txtClassStrength.Text = Convert.ToString(dr("ERM_CLASSSTRENGTH"))
                        txtAreasImprovement.Text = Convert.ToString(dr("ERM_AREAS_IMPROVEMENT"))
                        txtAverage.Text = Convert.ToString(dr("ERM_AVERAGE"))
                        hdnFileName.Value = Convert.ToString(dr("ERM_IMAGE"))
                        lbl_Rating.Text = Mainclass.getDataValue("select EKM_DESC  FROM EVA.EVAL_KEYS_M  where  ekm_id in(select EMK_EKM_ID from EVA.EVAL_MAINCRITERIA_KEYSMAP where EMK_BSU_ID='" & Session("sBsuid") & "' and EMK_SCORE=round(convert(numeric(12,2)," & txtAverage.Text & "),0)) and EKM_BSU_ID='" & Session("sBsuid") & "'", "OASISConnectionString")

                        Dim iji_filename As String = hdnFileName.Value
                        If Not iji_filename = "" Then
                            hdnFileName.Value = iji_filename.Split(";")(2) + iji_filename.Split(";")(3)
                            hdnContentType.Value = iji_filename.Split(";")(3)
                            btnDocumentLink.Text = iji_filename.Split(";")(0)
                            btnDocumentLink.Visible = True
                            btnDocumentDelete.Visible = True
                            btnUpload.Visible = False
                            UploadDocPhoto.Visible = False
                        Else
                            btnUpload.Visible = True
                            UploadDocPhoto.Visible = True

                        End If

                       


                    Else
                        'btnUpload.Visible = True
                        UploadDocPhoto.Visible = True

                        If Convert.ToString(dr("ERM_COMPLETION_DATE")) <> "" Then
                            Dim datet As DateTime = Convert.ToDateTime(dr("ERM_COMPLETION_DATE"))
                            txt_EvlDate.Text = datet.Date.ToString("dd/MMM/yyyy")
                        End If
                    End If



                End While
            End Using
        Catch ex As Exception

        End Try
        Return ERM_ID
        'btnUpload.Visible = True
        UploadDocPhoto.Visible = True
    End Function
    Private Sub Load_ObserverDetails()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EMPDETAILS", parm)
            If (ds.Tables(0).Rows.Count > 0) Then
                lbl_ObserverName.Text = Convert.ToString(ds.Tables(0).Rows(0)("EMP_NAME"))
                ' lbl_StaffDesg.Text = Convert.ToString(ds.Tables(0).Rows(0)("DES_DESCR"))
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Bind_EvalMainCriteria(ByVal EVT_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@EVT_ID", EVT_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            gv_EvalMainCriteria.DataSource = ds
            gv_EvalMainCriteria.DataBind()

            If (ds.Tables(0).Rows.Count > 0) Then
                lblEvaluationHeading1.Text = Convert.ToString(ds.Tables(0).Rows(0)("EvaluationHeading1"))
                lblEvaluationHeading2.Text = Convert.ToString(ds.Tables(0).Rows(0)("EvaluationHeading2"))
            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub Bind_EvalKeys(ByVal EVT_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@EVT_ID", EVT_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gv_EvalMainCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_EvalMainCriteria.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DL_EvalKeys As DataList = DirectCast(e.Row.FindControl("DL_EvalKeys"), DataList)
            Dim HF_EVT_ID As HiddenField = DirectCast(e.Row.FindControl("HF_EVT_ID"), HiddenField)
            Dim HF_EMC_ID As HiddenField = DirectCast(e.Row.FindControl("HF_EMC_ID"), HiddenField)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            DL_EvalKeys.DataSource = ds
            DL_EvalKeys.DataBind()


            Dim gv_EvalSubCriteria As GridView = DirectCast(e.Row.FindControl("gv_EvalSubCriteria"), GridView)
            Dim parmSubCriteria(4) As SqlClient.SqlParameter
            parmSubCriteria(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
            parmSubCriteria(1) = New SqlClient.SqlParameter("@EVT_ID", HF_EVT_ID.Value)
            parmSubCriteria(2) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            Dim dsSubCriteria As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parmSubCriteria)
            gv_EvalSubCriteria.DataSource = dsSubCriteria
            gv_EvalSubCriteria.DataBind()
        End If
    End Sub

    Protected Sub gv_EvalSubCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim DL_EvalSubCriteriaKeyMap As DataList = DirectCast(e.Row.FindControl("DL_EvalSubCriteriaKeyMap"), DataList)

            Dim rbl_EVAL_SUBKEY_DESC As RadioButtonList = DirectCast(e.Row.FindControl("rbl_EVAL_SUBKEY_DESC"), RadioButtonList)
            Dim HF_ESC_ID As HiddenField = DirectCast(e.Row.FindControl("HF_ESC_ID"), HiddenField)
            Dim HF_EMC_ID As HiddenField = DirectCast(e.Row.FindControl("HF_EMC_ID"), HiddenField)

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 4)
            parm(1) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            parm(2) = New SqlClient.SqlParameter("@ESC_ID", HF_ESC_ID.Value)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            rbl_EVAL_SUBKEY_DESC.DataSource = ds
            rbl_EVAL_SUBKEY_DESC.DataTextField = "ESK_DESC"
            rbl_EVAL_SUBKEY_DESC.DataValueField = "ESK_ID"
            rbl_EVAL_SUBKEY_DESC.DataBind()

            If (ViewState("ERM_ID") <> "0") Then
                Dim parmResult(4) As SqlClient.SqlParameter
                parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
                parmResult(1) = New SqlClient.SqlParameter("@ERM_ID", ViewState("ERM_ID"))
                parmResult(2) = New SqlClient.SqlParameter("@ESC_ID", HF_ESC_ID.Value)
                Dim ERS_ESK_ID As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
                rbl_EVAL_SUBKEY_DESC.SelectedValue = ERS_ESK_ID
                ViewState("SelectedRadio") = ViewState("SelectedRadio") + "||" + rbl_EVAL_SUBKEY_DESC.ClientID
            End If
        End If
    End Sub

    Protected Sub btnDraft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDraft.Click
        SaveEvaluationResult("D")
       
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        SaveEvaluationResult("C")
    End Sub
    Private Sub SaveEvaluationResult(ByVal Savestatus As String)

        Dim EvlDat As DateTime = Convert.ToDateTime(txt_EvlDate.Text)


        If EvlDat.Date <= DateTime.Now.Date Then
            Dim constring As String = ConnectionManger.GetOASISConnectionString
            Dim con As SqlConnection = New SqlConnection(constring)
            con.Open()
            Dim sqltran As SqlTransaction
            sqltran = con.BeginTransaction("trans")
            Try
                '------START Saving  EVAL_STAFF_RESULTS_M----------' 
                Dim param(22) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@OPTION", 1)
                param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                param(1).Direction = ParameterDirection.Output
                param(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(2).Direction = ParameterDirection.ReturnValue
                param(3) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))

                param(4) = New SqlClient.SqlParameter("@ERM_EVT_ID", ViewState("EVT_ID"))
                param(5) = New SqlClient.SqlParameter("@ERM_EEM_ID", ViewState("EEM_ID"))
                param(6) = New SqlClient.SqlParameter("@ERM_EOB_ID", ViewState("EOB_ID"))
                param(7) = New SqlClient.SqlParameter("@ERM_STE_ID", ViewState("STE_ID"))
                param(8) = New SqlClient.SqlParameter("@ERM_STATUS", Savestatus)
                param(9) = New SqlClient.SqlParameter("@ERM_bCOMPLETED", IIf(Savestatus = "C", True, False))


                param(10) = New SqlClient.SqlParameter("@ERM_TOTALSCORE", DBNull.Value)

                param(11) = New SqlClient.SqlParameter("@ERM_ACD_ID", ddl_acdYear.SelectedValue)
                param(12) = New SqlClient.SqlParameter("@ERM_GRD_ID", ddl_grade.SelectedItem.Text)
                param(13) = New SqlClient.SqlParameter("@ERM_GRM_ID", ddl_grade.SelectedValue)
                param(14) = New SqlClient.SqlParameter("@ERM_SCT_ID", ddl_section.SelectedValue)
                param(15) = New SqlClient.SqlParameter("@ERM_SBG_ID", ddl_subject.SelectedValue)
                param(16) = New SqlClient.SqlParameter("@ERM_TOPIC", txt_topic.Text.Trim())
                param(17) = New SqlClient.SqlParameter("@ERM_REMARKS", txtRemarks.Text.Trim())
                param(18) = New SqlClient.SqlParameter("@ERM_COMPLETION_DATE", txt_EvlDate.Text)
                param(19) = New SqlClient.SqlParameter("@ERM_ClassStrength", txtClassStrength.Text)

                ' added by mahesh on 11-01-2016
                param(20) = New SqlClient.SqlParameter("@ERM_AREAS_IMPROVEMENT", txtAreasImprovement.Text.Trim())
                param(21) = New SqlClient.SqlParameter("@ERM_AVERAGE", txtAverage.Text)
                param(22) = New SqlClient.SqlParameter("@ERM_IMAGE", "")




                'If ViewState("ERM_ID") <> "0" Then
                '    param(8) = New SqlClient.SqlParameter("@ERM_ID", ViewState("ERM_ID"))
                'End If
                SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "EVA.SAVE_EVAL_STAFF_RESULTS_M", param)
                Dim ReturnFlag As Integer = param(2).Value
                If (ReturnFlag = 0) Then
                    ViewState("ERM_ID") = param(1).Value

                    For Each gr As GridViewRow In gv_EvalMainCriteria.Rows

                        Dim gv_EvalSubCriteria As GridView = DirectCast(gr.FindControl("gv_EvalSubCriteria"), GridView)
                        Dim HF_EVT_ID As HiddenField = TryCast(gr.FindControl("HF_EVT_ID"), HiddenField)
                        For Each gr_EvalSubCriteria As GridViewRow In gv_EvalSubCriteria.Rows
                            Dim rbl_EVAL_SUBKEY_DESC As RadioButtonList = DirectCast(gr_EvalSubCriteria.FindControl("rbl_EVAL_SUBKEY_DESC"), RadioButtonList)

                            If (rbl_EVAL_SUBKEY_DESC.SelectedIndex > -1) Then
                                Dim ESK_ID_Score As String() = rbl_EVAL_SUBKEY_DESC.SelectedValue.Split("_")
                                Dim HF_ESC_ID As HiddenField = TryCast(gr_EvalSubCriteria.FindControl("HF_ESC_ID"), HiddenField)
                                Dim HF_EMC_ID As HiddenField = TryCast(gr_EvalSubCriteria.FindControl("HF_EMC_ID"), HiddenField)
                                Dim paramResults_s(11) As SqlClient.SqlParameter
                                paramResults_s(0) = New SqlClient.SqlParameter("@OPTION", 1)
                                paramResults_s(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                                paramResults_s(1).Direction = ParameterDirection.Output
                                paramResults_s(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                                paramResults_s(2).Direction = ParameterDirection.ReturnValue
                                paramResults_s(3) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
                                paramResults_s(4) = New SqlClient.SqlParameter("@ERS_ERM_ID", ViewState("ERM_ID"))
                                paramResults_s(5) = New SqlClient.SqlParameter("@ERS_STE_ID", ViewState("STE_ID"))
                                paramResults_s(6) = New SqlClient.SqlParameter("@ERS_EMC_ID", HF_EMC_ID.Value)
                                paramResults_s(7) = New SqlClient.SqlParameter("@ERS_ESC_ID", HF_ESC_ID.Value)
                                paramResults_s(8) = New SqlClient.SqlParameter("@ERS_ESK_ID", ESK_ID_Score(0))
                                paramResults_s(9) = New SqlClient.SqlParameter("@ERS_SCORE", ESK_ID_Score(1))
                                'paramResults_s(10) = New SqlClient.SqlParameter("@ERS_ID", "0")
                                SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "EVA.SAVE_EVAL_STAFF_RESULTS_S", paramResults_s)
                            End If

                        Next
                        ' lblerror.Text = "Saved Sucessfully."               
                    Next

                    sqltran.Commit()
                    If hdnFileName.Value = "" And btnUpload.Text <> "" Then
                        DocumentUpload(ViewState("ERM_ID"))
                    End If

                    lbl_Panel_Close.Text = "Staff evaluation saved sucessfully."
                    Panel_Close.Visible = True
                Else
                    lbl_Saveerror.Text = "Saved Failed."
                    sqltran.Rollback()
                End If
            Catch ex As Exception
                sqltran.Rollback()
            End Try
        Else
            lbl_Saveerror.Text = "Future date is not allowed"
        End If
    End Sub

    Protected Sub btn_Panel_Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Panel_Close.Click

    End Sub
    Private Sub Bind_AcademicYear(Optional ByVal ACD_ID As String = "")
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            parm(2) = New SqlClient.SqlParameter("@CLM_ID", Session("CLM"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_acdYear.DataSource = ds
            ddl_acdYear.DataValueField = "ACD_ID"
            ddl_acdYear.DataTextField = "ACY_DESCR"
            ddl_acdYear.DataBind()

            Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                         & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("sBsuid") + "' AND ACD_CLM_ID=" + Session("CLM")
            Dim dsCurrent As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If (dsCurrent.Tables(0).Rows.Count > 0) Then
                Dim li As New ListItem
                li.Text = dsCurrent.Tables(0).Rows(0).Item(0)
                li.Value = dsCurrent.Tables(0).Rows(0).Item(1)
                ddl_acdYear.Items(ddl_acdYear.Items.IndexOf(li)).Selected = True
            End If
            If ACD_ID <> "" Then
                ddl_acdYear.SelectedValue = ACD_ID
            End If
            Bind_Grade()
            'If Not ddl_acdYear.Items.FindItemByValue(Session("ACD_ID")) Is Nothing Then
            '    ddl_acdYear.Items.FindItemByValue(Session("ACD_ID")).Selected = True
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_acdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_acdYear.SelectedIndexChanged
        Bind_Grade()
    End Sub
    Private Sub Bind_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_grade.DataSource = ds
            ddl_grade.DataValueField = "grm_id"
            ddl_grade.DataTextField = "grm_grd_id"
            ddl_grade.DataBind()
            Bind_Subject()
            Bind_Section()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddl_grade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_grade.SelectedIndexChanged
        Bind_Section()
        Bind_Subject()
    End Sub
    Private Sub Bind_Section()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 5)
            parm(1) = New SqlClient.SqlParameter("@GRM_ID", ddl_grade.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_section.DataSource = ds
            ddl_section.DataValueField = "SCT_ID"
            ddl_section.DataTextField = "SCT_DESCR"
            ddl_section.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Bind_Subject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@GRM_ID", ddl_grade.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@EMP_ID", ViewState("EMP_ID"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_SUBJECT_M", parm)
            ddl_subject.DataSource = ds
            ddl_subject.DataValueField = "SBG_ID"
            ddl_subject.DataTextField = "SBG_DESCR"
            ddl_subject.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Private Sub DocumentUpload(ByVal ERMID As Integer)
        If UploadDocPhoto.FileName <> "" Then
            Dim str_img As String = WebConfigurationManager.AppSettings.Item("TempFileFolder") & "LessonEvaluation\"


            Dim str_tempfilename As String = Session.SessionID & Replace(Replace(Replace(Now.ToString, ":", ""), "/", ""), "\", "") & UploadDocPhoto.FileName
            Dim strFilepath As String = str_img & str_tempfilename


            Try
                UploadDocPhoto.PostedFile.SaveAs(strFilepath)
            Catch ex As Exception
                Exit Sub
            End Try

            If Not IO.File.Exists(strFilepath) Then
                lbl_Saveerror.Text = "Invalid FilePath!!!!"
                Exit Sub
            Else
                Dim ContentType As String = getContentType(strFilepath)
                Dim FileExt As String = str_tempfilename.Substring(str_tempfilename.Length - 4)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 5)
                If FileExt.Substring(0, 1) <> "." Then FileExt = str_tempfilename.Substring(str_tempfilename.Length - 6)
                Try
                    IO.File.Move(strFilepath, str_img & ERMID & "_" & Session("sBsuid") & FileExt)
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString, CommandType.Text, "update EVA.EVAL_STAFF_RESULTS_M set ERM_IMAGE='" & UploadDocPhoto.FileName & ";" & ContentType & ";" & ERMID & "_" & Session("sBsuid") & ";" & FileExt & "' where ERM_ID='" & ERMID & "'")
                    hdnFileName.Value = ERMID & "_" & Session("sBsuid") & FileExt
                    hdnContentType.Value = ContentType
                    btnDocumentLink.Text = UploadDocPhoto.FileName
                    btnDocumentLink.Visible = True
                    btnDocumentDelete.Visible = True
                    UploadDocPhoto.Visible = False
                    btnUpload.Visible = False
                Catch ex As Exception

                End Try
            End If
        ElseIf Not ViewState("imgAsset") Is Nothing Then

        Else
            ViewState("imgAsset") = Nothing
        End If
    End Sub
    Private Function getContentType(ByVal FilePath As String) As String
        Dim filename As String = Path.GetFileName(FilePath)
        Dim ext As String = Path.GetExtension(filename)
        Select Case ext
            Case ".doc"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".docx"
                getContentType = "application/vnd.ms-word"
                Exit Select
            Case ".xls"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".xlsx"
                getContentType = "application/vnd.ms-excel"
                Exit Select
            Case ".jpg"
                getContentType = "image/jpg"
                Exit Select
            Case ".png"
                getContentType = "image/png"
                Exit Select
            Case ".gif"
                getContentType = "image/gif"
                Exit Select
            Case ".pdf"
                getContentType = "application/pdf"
                Exit Select
            Case Else
                getContentType = "text/html"
        End Select
    End Function
    Protected Sub btnDocumentDelete_Click(sender As Object, e As EventArgs) Handles btnDocumentDelete.Click
        If Not (btnDraft.Visible Or btnSubmit.Visible) Then
            lbl_Saveerror.Text = "Cannot delete file"
        Else
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString, CommandType.Text, "update  EVA.EVAL_STAFF_RESULTS_M set ERM_IMAGE='' where ERM_ID='" & ViewState("ERM_ID") & "'")
            Dim fileToDelete As New FileInfo(WebConfigurationManager.AppSettings.Item("TempFileFolder") & "LessonEvaluation\" & hdnFileName.Value)
            Try
                fileToDelete.Delete()
                hdnFileName.Value = ""
                hdnContentType.Value = ""
                btnDocumentLink.Visible = False
                btnDocumentDelete.Visible = False
                'btnUpload.Visible = True
                UploadDocPhoto.Visible = True
            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub txtAverage_TextChanged(sender As Object, e As EventArgs) Handles txtAverage.TextChanged
        lbl_Rating.Text = Mainclass.getDataValue("select EKM_DESC  FROM EVA.EVAL_KEYS_M  where  ekm_id in(select EMK_EKM_ID from EVA.EVAL_MAINCRITERIA_KEYSMAP where EMK_BSU_ID='" & Session("sBsuid") & "' and EMK_SCORE=round(convert(numeric(12,2)," & txtAverage.Text & "),0)) and EKM_BSU_ID='" & Session("sBsuid") & "'", "OASISConnectionString")
    End Sub
    Private Sub GetRating()
        lbl_Rating.Text = Mainclass.getDataValue("select EKM_DESC  FROM EVA.EVAL_KEYS_M  where  ekm_id in(select EMK_EKM_ID from EVA.EVAL_MAINCRITERIA_KEYSMAP where EMK_BSU_ID='" & Session("sBsuid") & "' and EMK_SCORE=round(convert(numeric(12,2)," & txtAverage.Text & "),0)) and EKM_BSU_ID='" & Session("sBsuid") & "'", "OASISConnectionString")
    End Sub

    Protected Sub gv_EvalMainCriteria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gv_EvalMainCriteria.SelectedIndexChanged

    End Sub
End Class
