﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Partial Class Staff_Evaluation_M_EvaluationSubCriteria
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000124" And ViewState("MainMnu_code") <> "H000996") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindBusinessUnit()
                    BindGridEvalSubCriteria()
                    ' Bind_Grd_EvalKeys()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1

        ddl_bsu_search.DataSource = ds.Tables(0)
        ddl_bsu_search.DataValueField = "BSU_ID"
        ddl_bsu_search.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu_search.DataBind()
        ddl_bsu_search.SelectedIndex = -1

        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
            ddl_bsu_search.FindItemByValue(Session("sBsuid")).Selected = True
            Bind_EvalTemplateSearch()
            Bind_EvalTemplate()
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_EvalTemplate()
    End Sub
    Protected Sub ddl_bsu_search_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu_search.SelectedIndexChanged
        Bind_EvalTemplateSearch()
    End Sub
    Sub Bind_EvalTemplateSearch()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
        parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", ddl_bsu_search.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
        ddl_EvalTemplate_search.DataSource = ds
        ddl_EvalTemplate_search.DataValueField = "EVT_ID"
        ddl_EvalTemplate_search.DataTextField = "EVT_TEMPLATE_DESC"
        ddl_EvalTemplate_search.DataBind()

        ddl_EvalTemplate_search.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGridEvalSubCriteria()
    End Sub
    Public Sub Bind_EvalTemplate()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", ddl_bsu.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
            ddl_EvalTemplate.DataSource = ds
            ddl_EvalTemplate.DataValueField = "EVT_ID"
            ddl_EvalTemplate.DataTextField = "EVT_TEMPLATE_DESC"
            ddl_EvalTemplate.DataBind()

            Bind_EvalMainCriteria()
            Bind_Grd_EvalKeys()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_EvalTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_EvalTemplate.SelectedIndexChanged
        Bind_EvalMainCriteria()
        Bind_Grd_EvalKeys()
    End Sub
    Public Sub Bind_EvalMainCriteria()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            parm(2) = New SqlClient.SqlParameter("@EMC_BSU_ID", ddl_bsu.SelectedValue)
            parm(3) = New SqlClient.SqlParameter("@EMC_EVT_ID", ddl_EvalTemplate.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_MAINCRITERIA_M", parm)
            ddl_EvalMainCriteria.DataSource = ds
            ddl_EvalMainCriteria.DataValueField = "EMC_ID"
            ddl_EvalMainCriteria.DataTextField = "EMC_DESC"
            ddl_EvalMainCriteria.DataBind()

            Bind_Grd_EvalKeys()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_EvalMainCriteria_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_EvalMainCriteria.SelectedIndexChanged
        Bind_Grd_EvalKeys()
    End Sub
    Public Sub Bind_Grd_EvalKeys(Optional ByVal ESC_ID As Integer = 0)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            Dim ds As DataSet
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            parm(2) = New SqlClient.SqlParameter("@EKM_BSU_ID", ddl_bsu.SelectedValue)
            parm(3) = New SqlClient.SqlParameter("@EMK_EMC_ID", ddl_EvalMainCriteria.SelectedValue)
            parm(4) = New SqlClient.SqlParameter("@ESK_ESC_ID", ESC_ID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_KEYS_M", parm)

            gv_EvalKeys.DataSource = ds
            gv_EvalKeys.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Sub BindGridEvalSubCriteria()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
        parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        parm(2) = New SqlClient.SqlParameter("@EMC_BSU_ID", ddl_bsu_search.SelectedValue)
        parm(3) = New SqlClient.SqlParameter("@ESC_EVT_ID", IIf(ddl_EvalTemplate_search.SelectedIndex <= 0, DBNull.Value, ddl_EvalTemplate_search.SelectedValue))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_SUBCRITERIA_M", parm)
        gv_EvalMainCriteria.DataSource = ds
        Dim ge As New GridGroupByExpression
        Dim gf As New GridGroupByField
        gf.FieldName = "BSU_NAME"
        gf.HeaderText = "<span style='color:brown;font-weight:bold'>Business unit"
        ge.GroupByFields.Add(gf)
        ge.SelectFields.Add(gf)
        gv_EvalMainCriteria.MasterTableView.GroupByExpressions.Add(ge)

        Dim ge1 As New GridGroupByExpression
        Dim gf1 As New GridGroupByField
        gf1.FieldName = "EVT_TEMPLATE_DESC"
        gf1.HeaderText = "<span style='font-weight:bold'>Evaluation Template</span>"
        ge1.GroupByFields.Add(gf1)
        ge1.SelectFields.Add(gf1)
        gv_EvalMainCriteria.MasterTableView.GroupByExpressions.Add(ge1)

        gv_EvalMainCriteria.DataBind()
    End Sub
    Protected Sub lnk_AddEvalMainCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_AddEvalMainCriteria.Click
        ViewState("ESC_ID") = "0"
        pnl_EvalMainCriteria.Visible = True
        txtEvalSubCriteria.Text = ""
        ddl_bsu.Enabled = True
        ddl_EvalTemplate.Enabled = True
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
            Bind_EvalTemplate()
            Bind_Grd_EvalKeys(0)
        End If
    End Sub
    Protected Sub lnkClose_EvalSubCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClose_EvalSubCriteria.Click
        pnl_EvalMainCriteria.Visible = False
        txtEvalSubCriteria.Text = ""
    End Sub
    Protected Sub lnkClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnl_EvalMainCriteria.Visible = False
        txtEvalSubCriteria.Text = ""
    End Sub
    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        ViewState("ESC_ID") = "0"
        pnl_EvalMainCriteria.Visible = False
        txtEvalSubCriteria.Text = ""
    End Sub
    Protected Sub btn_save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_save.Click

        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            If (ddl_bsu.SelectedValue <> "" And ddl_EvalTemplate.SelectedValue <> "" And txtEvalSubCriteria.Text <> "") Then

                '------START Saving  EVALUATION SUB CRITERIA----------' 
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@OPTION", IIf(ViewState("ESC_ID") = "0", 1, 2))
                param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                param(1).Direction = ParameterDirection.Output
                param(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(2).Direction = ParameterDirection.ReturnValue
                param(3) = New SqlClient.SqlParameter("@ESC_BSU_ID", ddl_bsu.SelectedValue)
                param(4) = New SqlClient.SqlParameter("@ESC_EVT_ID", ddl_EvalTemplate.SelectedValue)
                param(5) = New SqlClient.SqlParameter("@ESC_EMC_ID", ddl_EvalMainCriteria.SelectedValue)
                param(6) = New SqlClient.SqlParameter("@ESC_DESC", txtEvalSubCriteria.Text.Trim())
                param(7) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
                If ViewState("ESC_ID") <> "0" Then
                    param(8) = New SqlClient.SqlParameter("@ESC_ID", ViewState("ESC_ID"))
                End If
                SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "EVA.SAVE_EVAL_SUBCRITERIA_M", param)
                Dim ReturnFlag As Integer = param(2).Value
                '------END Saving  EVALUATION MAIN CRITERIA----------' 
                If (ReturnFlag = 0) Then
                    ViewState("ESC_ID") = param(1).Value
                    For Each gv_EvalKeys_row As GridDataItem In gv_EvalKeys.Items
                        Dim HF_EKM_ID As HiddenField = gv_EvalKeys_row.FindControl("HF_EKM_ID")
                        Dim txt_ESK_DESC As TextBox = gv_EvalKeys_row.FindControl("txt_ESK_DESC")

                        Dim paramEvalKeys(12) As SqlClient.SqlParameter
                        paramEvalKeys(0) = New SqlClient.SqlParameter("@OPTION", 1)
                        paramEvalKeys(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                        paramEvalKeys(1).Direction = ParameterDirection.Output
                        paramEvalKeys(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        paramEvalKeys(2).Direction = ParameterDirection.ReturnValue
                        paramEvalKeys(3) = New SqlClient.SqlParameter("@ESK_EVT_ID", ddl_EvalTemplate.SelectedValue)
                        paramEvalKeys(4) = New SqlClient.SqlParameter("@ESK_EMC_ID", ddl_EvalMainCriteria.SelectedValue)
                        paramEvalKeys(5) = New SqlClient.SqlParameter("@ESK_EKM_ID", HF_EKM_ID.Value)
                        paramEvalKeys(6) = New SqlClient.SqlParameter("@ESK_ESC_ID", ViewState("ESC_ID"))
                        paramEvalKeys(7) = New SqlClient.SqlParameter("@ESK_DESC", txt_ESK_DESC.Text.Trim())
                        paramEvalKeys(8) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
                        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "EVA.SAVE_EVAL_SUBCRITERIA_KEYSMAP", paramEvalKeys)
                    Next

                    lblError.Text = "Saved Sucessfully."
                    sqltran.Commit()
                    pnl_EvalMainCriteria.Visible = False
                    BindGridEvalSubCriteria()
                Else
                    lbl_Saveerror.Text = param(1).Value
                    sqltran.Rollback()
                End If
            Else
                lbl_Saveerror.Text = "Please enter values in all the required fields"
            End If
        Catch ex As Exception
            lbl_Saveerror.Text = ex.Message
            sqltran.Rollback()
        Finally
            ViewState("ESC_ID") = "0"
        End Try
    End Sub
    Protected Sub lnk_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_ESC_ID As HiddenField = sender.parent.FindControl("HF_ESC_ID")
        ViewState("ESC_ID") = HF_ESC_ID.Value

        Dim hf_BSU_ID As HiddenField = sender.parent.FindControl("hf_BSU_ID")
        ddl_bsu.SelectedValue = hf_BSU_ID.Value
        ddl_bsu.Enabled = False
        Bind_EvalTemplate()
        Dim HF_EVT_ID As HiddenField = sender.parent.FindControl("HF_EVT_ID")
        ddl_EvalTemplate.SelectedValue = HF_EVT_ID.Value
        ddl_EvalTemplate.Enabled = False
        Bind_Grd_EvalKeys(ViewState("ESC_ID"))

        Dim lbl_ESC_DESC As Label = sender.parent.FindControl("lbl_ESC_DESC")
        txtEvalSubCriteria.Text = lbl_ESC_DESC.Text
        pnl_EvalMainCriteria.Visible = True
    End Sub
    Protected Sub lnk_delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_EMC_ID As HiddenField = sender.parent.FindControl("HF_EMC_ID")
            Dim constring As String = ConnectionManger.GetOASISConnectionString
            Dim param(27) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTION", 3)
            param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(1).Direction = ParameterDirection.Output
            param(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            param(3) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            param(4) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            SqlHelper.ExecuteNonQuery(constring, CommandType.StoredProcedure, "EVA.SAVE_EVAL_MAINCRITERIA_M", param)
            Dim ReturnFlag As Integer = param(2).Value
            If (ReturnFlag = 0) Then
                lblError.Text = param(1).Value
                BindGridEvalSubCriteria()
            Else
                lblError.Text = param(1).Value
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

  
End Class
