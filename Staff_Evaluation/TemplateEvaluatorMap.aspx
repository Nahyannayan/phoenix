<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TemplateEvaluatorMap.aspx.vb" Inherits="Staff_Evaluation_TemplateEvaluatorMap" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
        var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';





        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);

            var btnadd = $('#<%= btnadd.ClientID %>');
            btnadd.attr('disabled', true);

            if (checkedCheckboxes.length == 0) {
                btnadd.attr('disabled', true);
            }
            else {
                btnadd.removeAttr('disabled');
            }

            $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
        }

        $(document).ready(function () {

            $(allCheckBoxSelector).live('click', function () {
                $(checkBoxSelector).attr('checked', $(this).is(':checked'));
                ToggleCheckUncheckAllOptionAsNeeded();
            });
            $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
            ToggleCheckUncheckAllOptionAsNeeded();
        });

        <%--function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=ERP", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;
        }--%>
    </script>
    <script>
        function GetEMPName() {
            var pMode;
            var NameandCode;
            url = "../Accounts/accShowEmpDetail.aspx?id=ERP";
            var oWnd = radopen(url, "pop_emp");
        }

        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txt_EMPReportingTo.ClientID%>', 'TextChanged');
            }

        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        
         .darkPanlAlumini {
            width: 90%;
            height: 90%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 10%;
            top: 10%;
            position: fixed;
            width: 70%;
        }
         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Template - Evaluator Map
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table style="width: 100%">
                    <tr  align="left">
                        <td width="20%"><span class="field-label">Business unit<span style="color: Red">*</span></span></td>
                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_bsu" runat="server" RenderMode="Lightweight" Width="100%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%"><span class="field-label">Evaluation template<span style="color: Red">*</span></span></td>
                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_EvalTemplate" runat="server" RenderMode="Lightweight"
                                Width="100%">
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddl_EvalTemplate"
                                ErrorMessage="<b>Evaluation template required</b>" ValidationGroup="Save" InitialValue="Select" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr  align="left">
                        <td><span class="field-label">Evaluation panel<span style="color: Red">*</span></span></td>
                        <td colspan="3">
                            <asp:LinkButton ID="lnk_addEvaluator" runat="server" Text="Add observer"></asp:LinkButton>&nbsp;&nbsp;
            <asp:LinkButton ID="lnk_addEvaluatorGroup" runat="server" Text=" |Add observer Group"></asp:LinkButton>
                            <telerik:RadGrid ID="gv_observer" runat="server"
                                AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                                GridLines="None" Width="100%">
                                <MasterTableView>
                                    <CommandItemSettings />
                                    <RowIndicatorColumn Visible="True">
                                        <HeaderStyle Width="20px" />
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="True">
                                        <HeaderStyle Width="20px" />
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Staff Name" UniqueName="TemplateColumn_staffName">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Designation"
                                            UniqueName="TemplateColumn_desg">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Delete"
                                            UniqueName="Template_Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_deleteEvaluator" runat="server" OnClick="lnk_deleteEvaluator_Click">Delete</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_deleteEvaluator" ConfirmText="Confirm delete"
                                                    TargetControlID="lnk_deleteEvaluator" runat="server">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn>
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <HeaderStyle  HorizontalAlign="Left" />
                                <ItemStyle  HorizontalAlign="Left" />
                                <AlternatingItemStyle  HorizontalAlign="Left" />
                                <FilterMenu>
                                </FilterMenu>
                            </telerik:RadGrid>

                        </td>
                    </tr>

                    <tr  align="left">
                        <td><span class="field-label">Evaluation end date<span style="color: Red">*</span>(dd/MMM/yyyy)</span></td>
                        <td>
                            <asp:TextBox ID="txt_EndDate" runat="server"></asp:TextBox>

                            <asp:ImageButton ID="imgEndDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                                 
                        <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                            Format="dd/MMM/yyyy" PopupButtonID="imgEndDate" TargetControlID="txt_EndDate">
                        </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txt_EndDate" PopupButtonID="txt_EndDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_EndDate"
                                ErrorMessage="EndDate required" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txt_EndDate"
                                EnableViewState="False"
                                ErrorMessage="Enter the End Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="Save">*</asp:RegularExpressionValidator>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr  align="left">
                        <td><span class="field-label">Staff to be evaluated</span></td>
                        <td colspan="3">
                            <asp:LinkButton ID="lnk_addStaff" runat="server" Text="Add Staff"></asp:LinkButton>

                            <asp:LinkButton ID="lnk_addStaffGroup" runat="server" Text=" |Add Staff Group"></asp:LinkButton>
                            <div style="overflow: auto">
                                <telerik:RadGrid ID="gv_staffEvaluated" runat="server"
                                    AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                                    GridLines="None" Width="100%">
                                    <MasterTableView>
                                        <CommandItemSettings />
                                        <RowIndicatorColumn Visible="True">
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Visible="True">
                                            <HeaderStyle Width="20px" />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Staff Name" UniqueName="TemplateColumn_staffName">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                    <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Designation"
                                                UniqueName="TemplateColumn_desg">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                    <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                    <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Delete"
                                                UniqueName="Template_Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnk_deleteStaff" runat="server" OnClick="lnk_deleteStaff_Click">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_deleteStaff" ConfirmText="Confirm delete"
                                                        TargetControlID="lnk_deleteStaff" runat="server">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn>
                                            </EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                    <ItemStyle  HorizontalAlign="Left" />
                                    <AlternatingItemStyle  HorizontalAlign="Left" />
                                    <FilterMenu>
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                    <tr  align="left">
                        <td><span class="field-label">Group Name<span style="color: Red">*</span></span></td>
                        <td colspan="3">
                            <asp:TextBox ID="txt_remarks" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_remarks"
                                ErrorMessage="<br /> <b>Remarks required</b>" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr >
                        <td colspan="4" align="center">
                            <asp:Button ID="btn_Save" runat="server" Text="Save" ValidationGroup="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                            <br />
                            <asp:Label ID="lblerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>

                </table>

                <asp:Panel ID="pnl_SelectStaff" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini" >
                        <%--style="overflow:auto;"--%>

                        <div class="title-bg">
                            Select Staff
                            <div style="float: right">
                                <asp:LinkButton ForeColor="Red" ID="lnkClose_Staff" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClose_Staff_Click"></asp:LinkButton>
                            </div>
                        </div>

                        <table style="width: 100%;">
                            <tr  style="color: Black">
                                <td align="left"><span class="field-label">Business Unit<span style="color: red">*</span></span></td>
                                <td align="left" colspan="4">
                                    <telerik:RadComboBox ID="ddl_bsu_pnl" runat="server" RenderMode="Lightweight" Width="100%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddl_bsu_pnl"
                                        ErrorMessage="Select businessunit" InitialValue="" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                </td>

                            </tr>

                            <tr  style="color: Black">
                                <td align="left"><span class="field-label">Name</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Designation</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txt_desg" runat="server"></asp:TextBox>

                                </td>
                            </tr>

                            <tr  style="color: Black">
                                <td align="left"><span class="field-label">Reporting To</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txt_EMPReportingTo" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btn_pickreportingstaff" runat="server"
                                        CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();return false"
                                        Style="width: 16px" Text="Search" ToolTip="Search" />
                                    <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />
                                    &nbsp;<%--<asp:ImageButton ID="btn_Search" runat="server" CausesValidation="False" 
                              ImageUrl="~/Images/forum_search.gif" style="width: 16px" Text="Search" 
                              ToolTip="Search" />--%></td>
                                <td align="left" colspan="2">
                                    <asp:Button ID="btn_Search" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Search" ToolTip="Search" />
                                    <asp:Button ID="btn_reset" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                                </td>
                            </tr>

                            <tr  style="color: Black">

                                <td align="left" colspan="4">
                                    <div style="overflow: auto">
                                        <%----%>
                                        <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False"
                                            CellSpacing="0" EnableTheming="False" GridLines="None" PageSize="10"
                                            Width="100%" AllowPaging="true">
                                            <MasterTableView>
                                                <CommandItemSettings />
                                                <RowIndicatorColumn Visible="True">
                                                    <HeaderStyle Width="10px" />
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn Visible="True">
                                                    <HeaderStyle Width="10px" />
                                                </ExpandCollapseColumn>
                                                <Columns>
                                                    <telerik:GridTemplateColumn
                                                        HeaderText="Select" UniqueName="TemplateColumn">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAll" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="BSU"
                                                        UniqueName="TemplateColumn_BSU">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Staff Name"
                                                        UniqueName="TemplateColumn_staffName">

                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                            <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Designation"
                                                        UniqueName="TemplateColumn_desg">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                            <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                            <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                        Visible="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnk_Select_pnl" runat="server"
                                                                OnClick="lnk_Select_pnl_Click">Select</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn>
                                                    </EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <AlternatingItemStyle  HorizontalAlign="Left" />
                                            <FilterMenu>
                                            </FilterMenu>
                                        </telerik:RadGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr  align="center" style="color: Black">
                                <td colspan="4">
                                    <asp:Button ID="btnadd" runat="server" CssClass="button" Text="Add" />

                                    <asp:Button ID="btn_cancel" runat="server" CssClass="button" Text="Cancel" />

                                    <br />
                                    <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                                </td>
                            </tr>

                        </table>
                </asp:Panel>
            </div>

            <asp:Panel ID="pnl_SelectGroup" runat="server" CssClass="darkPanlAlumini" Visible="false">
                <div class="panel-cover inner_darkPanlAlumini">
                    <%--style="overflow:auto;"--%>

                    <div class="title-bg">
                        Select Group
                    <div style="float: right">
                        <asp:LinkButton ForeColor="Red" ID="lnkClose_StaffGroup" ToolTip="click here to close"
                            runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClose_StaffGroup_Click"></asp:LinkButton>
                    </div>
                    </div>
                    <table style="width: 100%;">
                        <tr  style="color: Black">
                            <td align="left" colspan="4">
                                <div style="overflow: auto; height: 350px;">
                                    <telerik:RadGrid ID="Gvgroup" runat="server" AutoGenerateColumns="False"
                                        CellSpacing="0" EnableTheming="False" GridLines="None" PageSize="10"
                                        Width="100%" AllowPaging="true">
                                        <MasterTableView>
                                            <CommandItemSettings />
                                            <RowIndicatorColumn Visible="True">
                                                <HeaderStyle Width="10px" />
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn Visible="True">
                                                <HeaderStyle Width="10px" />
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridTemplateColumn
                                                    HeaderText="Select" UniqueName="TemplateColumn">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox runat="server" ID="chkAll" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbGroupSelect" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="ID"
                                                    UniqueName="TemplateColumn_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID")%>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Staff Name"
                                                    UniqueName="TemplateColumn_staffName">

                                                    <ItemTemplate>

                                                        <asp:Label ID="lbl_StaffNAME" runat="server" Text='<%# Bind("Staff_List")%>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Group Description"
                                                    UniqueName="TemplateColumn_Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroup_DESCR" runat="server" Text='<%# Bind("Group_Description")%>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                    Visible="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnk_Select_pnl" runat="server"
                                                            OnClick="lnk_Select_pnl_Click">Select</asp:LinkButton>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn>
                                                </EditColumn>
                                            </EditFormSettings>
                                        </MasterTableView>
                                        <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                        <ItemStyle  HorizontalAlign="Left" />
                                        <AlternatingItemStyle  HorizontalAlign="Left" />
                                        <FilterMenu>
                                        </FilterMenu>
                                    </telerik:RadGrid>
                                </div>
                            </td>
                        </tr>
                        <tr  align="center" style="color: Black">
                            <td colspan="4">
                                <asp:Button ID="btnGroupadd" runat="server" CssClass="button" Text="Add" />

                                <asp:Button ID="btnCancel1" runat="server" CssClass="button" Text="Cancel" />

                                <br />
                                <asp:Label ID="Label1" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                            </td>
                        </tr>

                    </table>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

