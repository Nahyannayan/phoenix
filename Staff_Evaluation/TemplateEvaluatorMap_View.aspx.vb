﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Partial Class Staff_Evaluation_TemplateEvaluatorMap_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000125" And ViewState("MainMnu_code") <> "H000997") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindBusinessUnit()
                    BindGrid()
                    ' Bind_Grd_EvalKeys()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1

        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
            Bind_EvalTemplate()
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_EvalTemplate()
    End Sub
    Public Sub Bind_EvalTemplate()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", ddl_bsu.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
            ddl_EvalTemplate.DataSource = ds
            ddl_EvalTemplate.DataValueField = "EVT_ID"
            ddl_EvalTemplate.DataTextField = "EVT_TEMPLATE_DESC"
            ddl_EvalTemplate.DataBind()
            ddl_EvalTemplate.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
        Catch ex As Exception

        End Try
    End Sub

    Sub BindGrid()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        Dim ds As DataSet
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
        parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        parm(2) = New SqlClient.SqlParameter("@EEM_BSU_ID", ddl_bsu.SelectedValue)
        parm(3) = New SqlClient.SqlParameter("@EEM_EVT_ID", IIf(ddl_EvalTemplate.SelectedIndex > 0, ddl_EvalTemplate.SelectedValue, DBNull.Value))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_EVALUATOR_MAP", parm)

        gv_EvaluatorMapping.DataSource = ds
        gv_EvaluatorMapping.DataBind()
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub
    
    Protected Sub lnk_AddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_AddNew.Click
        Dim URL As String = "TemplateEvaluatorMap.aspx?MainMnu_code=" + Encr_decrData.Encrypt("H000125") + "&EEM_ID=" + Encr_decrData.Encrypt("0")
        Response.Redirect(URL)
    End Sub

    Protected Sub lnk_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EEM_ID As HiddenField = sender.parent.FindControl("HF_EEM_ID")
        Dim URL As String = "TemplateEvaluatorMap.aspx?MainMnu_code=" + Encr_decrData.Encrypt("H000125") + "&EEM_ID=" + Encr_decrData.Encrypt(HF_EEM_ID.Value)
        Response.Redirect(URL)
    End Sub

    Protected Sub lnk_delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_EEM_ID As HiddenField = sender.parent.FindControl("HF_EEM_ID")
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTION", 3)
            param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(1).Direction = ParameterDirection.Output
            param(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            param(3) = New SqlClient.SqlParameter("@EEM_ID", HF_EEM_ID.Value)
            param(4) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "EVA.SAVE_EVAL_EVALUATOR_MAP", param)
            BindGrid()
        Catch ex As Exception

        End Try

    End Sub
End Class
