﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Imports InfosoftGlobal
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Staff_Evaluation_Reports_DownloadConsolidatedReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Dim scriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager1.RegisterPostBackControl(Me.btn_export)

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000131") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    BindBusinessUnit()
                    Bind_EvalTemplateSearch()
                    Bind_designation()
                    Bind_AcademicYear()
                    BindStaff()
                    ' Generate chart in Literal Control
                    'If Not Request.QueryString("STE_ID") Is Nothing Then
                    '    ViewState("STE_ID") = Encr_decrData.Decrypt(Request.QueryString("STE_ID").Replace(" ", "+"))
                    '    'Load_StaffDetails(ViewState("STE_ID"))
                    '    'CreatetableReport()
                    '    FCLiteral.Text = CreateChartReport(ViewState("STE_ID"))
                    'Else
                    '    Dim sb As New System.Text.StringBuilder()
                    '    sb.Append("<script language='javascript'>")
                    '    sb.Append("window.close();")
                    '    sb.Append("</script>")
                    '    'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "JCall1", sb.ToString(), False)
                    '    Page.ClientScript.RegisterStartupScript(Me.[GetType](), "Script", sb.ToString(), False)
                    'End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_AcademicYear()
        Bind_Grade()
        BindStaff()
        Bind_EvalTemplateSearch()
    End Sub
    Private Sub Bind_AcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@CLM_ID", Session("CLM"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_acdYear.DataSource = ds
            ddl_acdYear.DataValueField = "ACD_ID"
            ddl_acdYear.DataTextField = "ACY_DESCR"
            ddl_acdYear.DataBind()

            Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                        & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + ddl_bsu.SelectedValue + "' AND ACD_CLM_ID=" + Session("CLM")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddl_acdYear.Items.FindItemByValue(ds.Tables(0).Rows(0).Item(1)).Selected = True

            Bind_Grade()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_acdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_acdYear.SelectedIndexChanged
        Bind_Grade()
    End Sub
    Private Sub Bind_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_grade.DataSource = ds
            ddl_grade.DataValueField = "grm_id"
            ddl_grade.DataTextField = "grm_grd_id"
            ddl_grade.DataBind()
            ddl_grade.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
            Bind_Subject()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_grade_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_grade.SelectedIndexChanged
        Bind_Subject()
    End Sub
    Private Sub Bind_Subject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New SqlClient.SqlParameter("@OPTION", 3)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@GRD_ID", ddl_grade.SelectedItem.Text)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_subject.DataSource = ds
            ddl_subject.DataValueField = "SBG_ID"
            ddl_subject.DataTextField = "SBG_DESCR"
            ddl_subject.DataBind()
            ddl_subject.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Sub Bind_EvalTemplateSearch()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
        parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", ddl_bsu.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
        ddl_EvalTemplate.DataSource = ds
        ddl_EvalTemplate.DataValueField = "EVT_ID"
        ddl_EvalTemplate.DataTextField = "EVT_TEMPLATE_DESC"
        ddl_EvalTemplate.DataBind()
        ddl_EvalTemplate.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
    End Sub
    Sub Bind_designation()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "Select DES_ID,DES_DESCR,DES_RES_ID as Res_ID from empdesignation_m where DES_FLAG='sd' ")
        ddl_designation.DataSource = ds.Tables(0)
        ddl_designation.DataValueField = "DES_ID"
        ddl_designation.DataTextField = "DES_DESCR"
        ddl_designation.DataBind()
        ddl_designation.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
    End Sub
    Sub BindStaff()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            'parm(2) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_Name.Text.Trim() = "", DBNull.Value, txt_Name.Text.Trim()))
            'parm(3) = New SqlClient.SqlParameter("@EMP_DES", IIf(txt_desg.Text.Trim() = "", DBNull.Value, txt_desg.Text.Trim()))
            'parm(4) = New SqlClient.SqlParameter("@EMP_REPORTTO", IIf(hf_EMPReportingTo.Value.Trim() = "", DBNull.Value, hf_EMPReportingTo.Value.Trim()))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EMPDETAILS", parm)
            ddl_emp.DataSource = ds
            ddl_emp.DataValueField = "EMP_ID"
            ddl_emp.DataTextField = "EMP_NAME"
            ddl_emp.DataBind()
            ddl_emp.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
            'ddl_emp.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gv_Staff_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_Staff.NeedDataSource
        Bindgv_Staff()
    End Sub
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        Bindgv_Staff()
    End Sub
    Sub Bindgv_Staff(Optional export As Boolean = False)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(10) As SqlClient.SqlParameter
            Dim ds As DataSet
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@EVT_ID", IIf(ddl_EvalTemplate.SelectedIndex > 0, ddl_EvalTemplate.SelectedValue, DBNull.Value))
            parm(3) = New SqlClient.SqlParameter("@EMP_ID", IIf(ddl_emp.SelectedIndex > 0, ddl_emp.SelectedValue, DBNull.Value))
            parm(4) = New SqlClient.SqlParameter("@DES_ID", IIf(ddl_designation.SelectedIndex > 0, ddl_designation.SelectedValue, DBNull.Value))
            parm(5) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            parm(6) = New SqlClient.SqlParameter("@GRM_ID", IIf(ddl_grade.SelectedIndex > 0, ddl_grade.SelectedValue, DBNull.Value))
            parm(7) = New SqlClient.SqlParameter("@SBG_ID", IIf(ddl_subject.SelectedIndex > 0, ddl_subject.SelectedValue, DBNull.Value))
            parm(8) = New SqlClient.SqlParameter("@EMPReportingTo", IIf(hf_EMPReportingTo.Value <> "", hf_EMPReportingTo.Value, DBNull.Value))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_STAFFEVAL_REPORT_DOWNLOAD", parm)
            gv_Staff.DataSource = ds
            gv_Staff.DataBind()
            If (export = True) Then
                ExportExcel(ds.Tables(0))
            End If
        Catch ex As Exception
            'lbl_Saveerror.Text = ex.Message
        End Try
    End Sub
    Public Sub ExportExcel(dt As DataTable)
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        ' ws.HeadersFooters.AlignWithMargins = True
        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "ExportStaffEvaluation.xlsx")
        'ef.Save(Response.OutputStream, SaveOptions.XlsxDefault)

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "ExportStaffEvaluation.xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset.Click
        ddl_EvalTemplate.ClearSelection()
        ddl_emp.ClearSelection()
        ddl_designation.ClearSelection()
        ddl_acdYear.ClearSelection()
        ddl_grade.ClearSelection()
        ddl_subject.ClearSelection()
        hf_EMPReportingTo.Value = ""
        txt_EMPReportingTo.Text = ""
    End Sub
    Protected Sub lnk_select_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim HF_ERM_ID As HiddenField = TryCast(sender.parent.FindControl("HF_ERM_ID"), HiddenField)
            'Dim HF_ACD_ID As HiddenField = TryCast(sender.parent.FindControl("HF_ACD_ID"), HiddenField)
            Dim HF_EMP_ID As HiddenField = TryCast(sender.parent.FindControl("HF_EMP_ID"), HiddenField)
            'IFrameconsolidatedReport.Attributes("src") = "ConsolidatedReport.aspx?EMP_ID=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&BSU_ID=" + Encr_decrData.Encrypt(ddl_bsu.SelectedValue)

            'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim parm(4) As SqlClient.SqlParameter
            'Dim ds As DataSet
            'parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            'parm(2) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            'parm(1) = New SqlClient.SqlParameter("@EMP_ID", HF_EMP_ID.Value)
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATED_STAFFS", parm)
            'GV_ConsolidatedReport.DataSource = ds
            'GV_ConsolidatedReport.DataBind()
            ' pnl_ConsolidatedReport.Visible = True
        Catch ex As Exception

        End Try
    End Sub


    'Protected Sub GV_ConsolidatedReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles GV_ConsolidatedReport.ItemDataBound
    '    If e.Item.ItemType = GridItemType.AlternatingItem OrElse e.Item.ItemType = GridItemType.Item Then
    '        Dim HF_EEM_ID As HiddenField = DirectCast(e.Item.FindControl("HF_EEM_ID"), HiddenField)
    '        Dim HF_STE_ID As HiddenField = DirectCast(e.Item.FindControl("HF_STE_ID"), HiddenField)
    '        Dim Ll_FChart As Literal = DirectCast(e.Item.FindControl("Ll_FChart"), Literal)
    '        Ll_FChart.Text = CreateChartReport(HF_STE_ID.Value, HF_EEM_ID.Value)
    '    End If
    'End Sub    

    Protected Sub btn_export_Click(sender As Object, e As EventArgs) Handles btn_export.Click
        Bindgv_Staff(True)
    End Sub
End Class
