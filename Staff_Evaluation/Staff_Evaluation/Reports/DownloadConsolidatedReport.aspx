﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DownloadConsolidatedReport.aspx.vb" Inherits="Staff_Evaluation_Reports_DownloadConsolidatedReport" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <script src="../../FusionCharts/FusionCharts.js" type="text/javascript"></script> 

    <script language="javascript" type="text/javascript">
    var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
    var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';

    function ToggleCheckUncheckAllOptionAsNeeded() {
        var totalCheckboxes = $(checkBoxSelector),
         checkedCheckboxes = totalCheckboxes.filter(":checked"),
         noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
         allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);     

        $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
    }

    $(document).ready(function() {

        $(allCheckBoxSelector).live('click', function() {
            $(checkBoxSelector).attr('checked', $(this).is(':checked'));
            ToggleCheckUncheckAllOptionAsNeeded();
        });
        $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
        ToggleCheckUncheckAllOptionAsNeeded();
    });

    function GetEMPName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("../../Accounts/accShowEmpDetail.aspx?id=ERP", "", sFeatures)
        if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
            //
            return true;
        }
        return false;
    }
</script>
<table style="width:80%;" class="BlueTable">
<tr class="subheader_img">
            <td colspan="6" >
                 Evaluation Report 
                 </td>
        </tr>
        <tr class="matters" style="color:Black" >
            <td align="left" style="width: 100px" >
                Business Unit<span style="color: red">*</span></td>
            <td width="1px">
                :</td>             
            <td  align="left">
             <telerik:radcombobox ID="ddl_bsu" runat="server" Skin="Simple" width="90%" 
                    AutoPostBack="True">
             </telerik:radcombobox>

 
            </td> 
              <td align="left" style="width: 130px">
                          Evaluation Template</td>
                          <td align="left">
                              :</td>
                          <td align="left">
                  <telerik:radcombobox ID="ddl_EvalTemplate"  Skin="Simple" runat="server" Width="200px">
                  </telerik:radcombobox>
                
                      </td>
        </tr>
        
        <tr class="matters" style="color:Black" >
            <td align="left" style="width: 100px" >
                Name</td>
            <td width="1px">
                :</td>             
            <td  align="left">
            <telerik:radcombobox ID="ddl_emp"  Skin="Simple" runat="server"  Filter="Contains"  width="90%" >
                  </telerik:radcombobox>
                <%--<asp:TextBox ID="txt_Name" runat="server"  Width="90%"></asp:TextBox>--%>
            </td> 
              <td align="left" style="width: 100px" >
                Designation</td>
            <td width="1px">
                :</td>             
            <td  align="left">
                  <telerik:radcombobox ID="ddl_designation"  Skin="Simple" runat="server" Width="200px">
                  </telerik:radcombobox>
                
            </td> 
        </tr>
        
                  <tr class="matters" style="color:Black">
                      <td align="left" style="width: 100px">
                          Reporting To</td>
                      <td width="1px">
                          :</td>
                      <td align="left">
                          <asp:TextBox ID="txt_EMPReportingTo" runat="server" Width="90%"></asp:TextBox>
                          <asp:ImageButton ID="btn_pickreportingstaff" runat="server" 
                              CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();"
                              style="width: 16px" Text="Search" ToolTip="Search" />
                          <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />
                          &nbsp;
                      </td>
                          <td  style="width:80px">Academic year<span style="color: red">*</span></td>
            <td style="width:1px">:</td>
            <td  style="width:160px"><telerik:radcombobox ID="ddl_acdYear" runat="server" Skin="Simple" width="100%" 
                    AutoPostBack="True">
             </telerik:radcombobox></td>
            </tr>
            <tr class="matters"  style="color:Black;display:none;" >
            <td style="width:60px">Grade<span style="color: red">*</span></td>
            <td style="width:1px">:</td>
            <td><telerik:radcombobox ID="ddl_grade" runat="server" Skin="Simple" width="90%" 
                    AutoPostBack="True">
             </telerik:radcombobox></td>
            <td  style="width:80px">Subject<span style="color: red">*</span></td>
            <td style="width:1px">:</td>
            <td><telerik:radcombobox ID="ddl_subject" runat="server" Skin="Simple" width="100%" 
                    AutoPostBack="True">
             </telerik:radcombobox></td>
            </tr>
        
                  <tr class="matters" style="color:Black">
                      <td align="center" colspan="6" align="center" >
                          <asp:Button ID="btn_Search" runat="server" CausesValidation="False" 
                              CssClass="button" Text="Search" ToolTip="Search" />
                          <asp:Button ID="btn_reset" runat="server" CausesValidation="False" 
                              CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                      &nbsp;<asp:Button ID="btn_export" runat="server" CausesValidation="False" 
                              CssClass="button" Text="Export To Excel" ToolTip="Search" />
                      </td>
                  </tr>
        
        <tr class="matters" style="color:Black" >
           
            <td  align="left" colspan="6" >
            <div style="overflow:auto;height:350px;">
                <telerik:radgrid ID="gv_Staff" runat="server" AutoGenerateColumns="False" 
                    CellSpacing="0" EnableTheming="False" GridLines="None" Skin="Office2007" 
                    Width="90%" AllowPaging="True">
                    <MasterTableView>
                        <CommandItemSettings />
                        <RowIndicatorColumn Visible="True">
                            <HeaderStyle Width="10px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True">
                            <HeaderStyle Width="10px" />
                        </ExpandCollapseColumn>
                        <Columns>
                             <telerik:GridBoundColumn UniqueName="StaffName" DataField="StaffName" HeaderText="StaffName" >
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn UniqueName="EVALUATIONGROUP" DataField="EVALUATIONGROUP" HeaderText="Evaluation Group" >
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn UniqueName="DateObserved" DataField="DateObserved" HeaderText="Date Observed" >
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn UniqueName="Observer" DataField="Observer"  HeaderText="Observer">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn UniqueName="Subject" DataField="Subject"  HeaderText="Subject">
                            </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn UniqueName="SCORE" DataField="SCORE" HeaderText="Score">
                            </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn UniqueName="Remarks" DataField="Remarks" HeaderText="Remarks">
                            </telerik:GridBoundColumn>                            
                        </Columns>
                        <EditFormSettings>
                            <EditColumn>
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                    <ItemStyle Font-Bold="true" HorizontalAlign="Left" />
                    <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                    <FilterMenu>
                    </FilterMenu>
                </telerik:radgrid>
            </div>
            </td> 
        </tr>
        <tr class="matters" align="center"  style="color:Black">
           <td colspan="6">
                
                <br />
                <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" style="vertical-align: middle"  EnableViewState="False"></asp:Label>
            </td>
        </tr>    
        
        </table>
    
    
    
</asp:Content>

