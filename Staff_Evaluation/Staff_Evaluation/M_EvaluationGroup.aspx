﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="M_EvaluationGroup.aspx.vb" Inherits="Staff_Evaluation_Staff_Evaluation_M_EvaluationGroup" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
       
        .darkPanlAlumini {
            width: 90%;
            height: 90%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 10%;
            top: 10%;
            position: fixed;
            width: 70%;
        }
         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }
    </style>
   

    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
        var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';

        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);

            var btnadd = $('#<%= btnadd.ClientID %>');
            btnadd.attr('disabled', true);

            if (checkedCheckboxes.length == 0) {
                btnadd.attr('disabled', true);
            }
            else {
                btnadd.removeAttr('disabled');
            }

            $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
        }

        $(document).ready(function () {

            $(allCheckBoxSelector).live('click', function () {
                $(checkBoxSelector).attr('checked', $(this).is(':checked'));
                ToggleCheckUncheckAllOptionAsNeeded();
            });
            $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
            ToggleCheckUncheckAllOptionAsNeeded();
        });

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../../Accounts/accShowEmpDetail.aspx?id=ERP", "pop_up")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
            //
            return true;
        }
        return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txt_EMPReportingTo.ClientID%>', 'TextChanged');

            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Evaluator Group Master
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table  style="width: 100%">

                   <%-- <tr class="subheader_img">
                        <td colspan="3">Evaluator Group Master</td>
                    </tr>--%>
                    <tr>                        
                            <asp:Label ID="lblerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                    </tr>
                    <tr  align="left">
                        <td ><span class="field-label">Business unit</span><span style="color: Red">*</span></td>
                       
                        <td>
                            <telerik:RadComboBox ID="ddl_bsu" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td ><span class="field-label">Group Name</span><span style="color: Red">*</span></td>
                      
                        <td>
                            <asp:TextBox ID="txt_remarks" runat="server" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_remarks"
                                ErrorMessage="&lt;br /&gt; &lt;b&gt;Group Description required&lt;/b&gt;" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>

                  <%--  <tr  align="left">
                        
                    </tr>--%>

                    <tr  align="left">
                        <td ><span class="field-label">Group Type</span><span style="color: Red">*</span></td>
                       
                        <td>
                            <asp:RadioButtonList ID="rblGroupType" runat="server" AutoPostBack="true"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="1">Observer</asp:ListItem>
                                <asp:ListItem Value="2">Staff</asp:ListItem>

                            </asp:RadioButtonList>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="title-bg">
                            <span class="field-label">Employee List</span><span style="color: Red">*</span>                        
                        </td>
                    </tr>
                    <tr  align="left">
                        
                        <td colspan="4">
                            <asp:LinkButton ID="lnk_addEvaluator" runat="server" Text="Add Staff"></asp:LinkButton>
                            <telerik:RadGrid ID="gv_observer" runat="server"
                                AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                                GridLines="None"  Width="100%">
                                <MasterTableView>
                                    <CommandItemSettings />
                                    <RowIndicatorColumn Visible="True">
                                        <HeaderStyle />
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="True">
                                        <HeaderStyle />
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Staff Name" UniqueName="TemplateColumn_staffName">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Designation"
                                            UniqueName="TemplateColumn_desg">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Delete"
                                            UniqueName="Template_Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_deleteEvaluator" runat="server" OnClick="lnk_deleteEvaluator_Click">Delete</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_deleteEvaluator" ConfirmText="Confirm delete"
                                                    TargetControlID="lnk_deleteEvaluator" runat="server">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn>
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                <ItemStyle  HorizontalAlign="Left" />
                                <AlternatingItemStyle  HorizontalAlign="Left" />
                                <FilterMenu>
                                </FilterMenu>
                            </telerik:RadGrid>

                        </td>
                    </tr>
                    <tr >
                        <td colspan="4" align="center" id="4">
                            <asp:Button ID="btn_Save" runat="server" Text="Save" ValidationGroup="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                           <%-- <br />
                            <asp:Label ID="lblerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>--%>
                        </td>
                    </tr>

                </table>

                <asp:Panel ID="pnl_SelectStaff" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <%--style="overflow:auto;"--%>
                        <div style="display: block; overflow: hidden; padding: 5px; margin: 5px 0;">
                            <div style="float: left">
                                <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                            </div>
                            <div style="float: right">
                                <asp:LinkButton ForeColor="Red" ID="lnkClose_Staff" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClose_Staff_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="4" class="title-bg">
                                    <span class="field-label">Select Staff</span>
                                </td>
                            </tr>
                            <tr style="color: Black">
                                <td align="left"><span class="field-label">Business Unit</span> <span style="color: red">*</span> </td>

                                <td align="left">
                                    <telerik:RadComboBox ID="ddl_bsu_pnl" runat="server" Width="80%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddl_bsu_pnl"
                                        ErrorMessage="Select businessunit" InitialValue="" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left"><span class="field-label">Name</span></td>

                                <td align="left">
                                    <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
                                </td>

                            </tr>

                            <tr style="color: Black">

                                <td align="left"><span class="field-label">Designation</span></td>

                                <td align="left">
                                    <asp:TextBox ID="txt_desg" runat="server"></asp:TextBox>

                                </td>
                                <td align="left"><span class="field-label">Reporting To</span></td>

                                <td align="left">
                                    <asp:TextBox ID="txt_EMPReportingTo" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btn_pickreportingstaff" runat="server"
                                        CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;"
                                        Text="Search" ToolTip="Search" />
                                    <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />
                                    &nbsp;<%--<asp:ImageButton ID="btn_Search" runat="server" CausesValidation="False" 
                              ImageUrl="~/Images/forum_search.gif" style="width: 16px" Text="Search" 
                              ToolTip="Search" />--%></td>
                            </tr>

                            <tr style="color: Black">
                                <td align="center" colspan="4">
                                    <asp:Button ID="btn_Search" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Search" ToolTip="Search" />
                                    <asp:Button ID="btn_reset" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                                </td>
                            </tr>
                            <tr style="color: Black">
                                <td align="left" colspan="4">
                                    <div>
                                        <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False"
                                            CellSpacing="0" EnableTheming="False" GridLines="None" PageSize="10"
                                            Width="100%" AllowPaging="true">
                                            <MasterTableView>
                                                <CommandItemSettings />
                                                <RowIndicatorColumn Visible="True">
                                                    <HeaderStyle />
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn Visible="True">
                                                    <HeaderStyle />
                                                </ExpandCollapseColumn>
                                                <Columns>
                                                    <telerik:GridTemplateColumn
                                                        HeaderText="Select" UniqueName="TemplateColumn">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAll" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="BSU"
                                                        UniqueName="TemplateColumn_BSU">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Staff Name"
                                                        UniqueName="TemplateColumn_staffName">

                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                            <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Designation"
                                                        UniqueName="TemplateColumn_desg">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                            <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                            <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                        Visible="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnk_Select_pnl" runat="server">Select</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn>
                                                    </EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Height="10px" />
                                            <ItemStyle  HorizontalAlign="Left" />
                                            <AlternatingItemStyle HorizontalAlign="Left" />
                                            <FilterMenu>
                                            </FilterMenu>
                                        </telerik:RadGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr align="center" style="color: Black">
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnadd" runat="server" CssClass="button" Text="Add" OnClick="btnadd_Click" />
                                    <asp:Button ID="btn_cancel" runat="server" CssClass="button" Text="Cancel" OnClick="btn_cancel_Click" />
                                    <%-- <br />--%>
                                    
                                </td>
                            </tr>

                        </table>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>

