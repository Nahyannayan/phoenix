﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StaffEvaluation.aspx.vb" Inherits="Staff_Evaluation_StaffEvaluation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::PHOENIX:: Staff Evaluation::</title>
    <%--<link href="cssfiles/StaffEvaluationStyle.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <!-- Bootstrap core CSS-->
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/BSUstyles.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/Accordian.css" rel="stylesheet" />

    <script src="/PHOENIXBETA/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <style type="text/css">
        .style1 {
            width: 100%;
        }

        .fixWidth tr td {
                 width: 300px;
                 vertical-align:top;                 
                 /* font-size: 9px; */
                 /* font-weight:bold; */
                 border-left:1px;
                 border-left-style:dotted;
                 border:1px;
                 border-left-color:gray;
                 height:60px;
    }
.fixWidth tr td:hover
{
	background-color:rgba(0, 0, 0, 0.07); 
}
.selectedOption
{
	background-color:/*rgba(0, 0, 0, 0.07)*/rgba(141, 194, 76, 0.4); 
	color:/*Black*/ #393939;
	font-weight:bold;  
}
.padding-0 {
    padding:0px !important;
}
.border-bold {
    border:2px solid !important;
}
.border-none {
    border-width:0px !important;
}
    </style>

    <script language="javascript" type="text/javascript">
        function radselected(rbl) {
            var res = rbl.split('||');
            for (var i = 0; i < res.length; i++) {
                if (res[i] != "") {
                    $("#" + res[i] + " input:radio:checked").closest('td').addClass('selectedOption');
                    var selectedvalue = $("#" + res[i] + " input:radio:checked").val();
                    if (typeof (selectedvalue) != 'undefined') {
                        var selValue = selectedvalue.split("_");
                        var radiobtnRow = $("#" + res[i] + " input:radio:checked").parents('table').parents('tr')
                        radiobtnRow.each(function () {
                            var firstId = $("span.score:eq(0)", this);
                            firstId.text(selValue[1]);
                            return false;
                        });
                        var radiobtnRow1 = $("#" + res[i] + " input:radio:checked").parents('table').parents('table').parents('table').parents('table');
                        var totalScoreValue = 0;
                        radiobtnRow1.find('span.score').each(function () {
                            var score = $(this).text()
                            if (score != "") {
                                totalScoreValue = parseInt(totalScoreValue) + parseInt(score)
                            }
                        });
                        var totalscore = $(radiobtnRow1).parents('table').parents('div').find('span.totalscore');
                        totalscore.text(totalScoreValue);


                    }
                }
            }
        }

        function aa(rbl) {
            //var selectedvalue = $("#"+ rbl + " :radio:checked").attr('id');
            var selectedvalue = $("#" + $(rbl).attr('id') + " input:radio:checked").val();
            if (typeof (selectedvalue) != 'undefined') {
                var res = selectedvalue.split("_");
                //$(this).closest('table').closest('tbody').closest('td').val("New title");
                var tr_radio = $("#" + $(rbl).attr('id') + " input:radio:checked").closest('tr')

                tr_radio.find('td').each(function () {
                    $(this).removeClass('selectedOption');
                });
                $("#" + $(rbl).attr('id') + " input:radio:checked").closest('td').addClass('selectedOption'); //.attr('class', 'selectedOption');

                var radiobtnRow = $("#" + $(rbl).attr('id') + " input:radio:checked").parents('table').parents('tr')
                radiobtnRow.each(function () {
                    var firstId = $("span.score:eq(0)", this);
                    firstId.text(res[1]);
                    return false;
                });

                var radiobtnRow1 = $("#" + $(rbl).attr('id') + " input:radio:checked").parents('table').parents('table').parents('table');
                var totalScoreValue = 0;
                var Count = 0;
                
                //radiobtnRow1.find('td').each(function()
               radiobtnRow1.find('span.score').each(function ()
                {
                   
                    var score = $(this).text()
                    
                    if (score != "") {

                        totalScoreValue = parseInt(totalScoreValue) + parseInt(score)
                        Count = Count + 1

                    }
                    //alert($(this).html())
                    //                        var score = $(this).text();
                    //                        if (score == "") {
                    //                            alert(score);
                    //                        }
                    //                        if (typeof (score) != 'undefined') {
                    //                            alert(score.html())
                    //                        }

                    //alert($("span.score:eq(0)", this).html());
                    // return false;
                });
                //alert(totalScoreValue);
              
               //var totalscore = $(radiobtnRow1).parents('table').find('span.totalscore');
                var Average = 0.00
                //alert($(totalscore).html())
                document.getElementById("lbl_TotalScore").value = totalScoreValue;
               // totalscore.text(totalScoreValue);
                Average = totalScoreValue / Count
                document.getElementById("txtAverage").value = Average.toFixed();

                //ScriptManager.RegisterStartupScript(this.Page, Page.gettype(), "text", "GetRating()", true);


            }
            return false;
        }

        function showDocument() {
            var sFeatures;

            sFeatures = "dialogWidth: 1200px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "resizable:yes; ";
                <%--contenttype = document.getElementById('<%=hdnContentType.ClientID %>').value;
                filename = document.getElementById('<%=hdnFileName.ClientID %>').value;--%>
                contenttype = document.getElementById("hdnContentType").value;
                filename = document.getElementById("hdnFileName").value;
                result = window.showModalDialog("../../Inventory/IFrameNew.aspx?id=0&path=LessonEvaluation&filename=" + filename + "&contenttype=" + contenttype, "", sFeatures)
                return false;
            }


    </script>
</head>
<body>
    <div class="wrapper">
        <div class="header"></div>
        <div class="clear"></div>

        <form id="form1" runat="server">
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <div style="width: 1325px;margin: 0 auto;">
                <center>
                    <asp:Label ID="lblEvaluationHeading1" runat="server" Text="" CssClass="title-bg"></asp:Label><br />
                    <asp:Label ID="lblEvaluationHeading2" runat="server" Text="" CssClass="title-bg"></asp:Label>
                </center>

                <div style="float: left; width: 100%; margin-top:12px;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table id="tb_evalinfo" runat="server" width="100%">
                                <tr>
                                    <td width="20%">
                                        <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Staff Name"></asp:Label></td>
                                    <td width="30%">
                                        <asp:Label ID="lbl_StaffName" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <asp:Label ID="Label2" runat="server" CssClass="field-label"
                                            Text="Designation"></asp:Label></td>
                                    <td width="30%">
                                        <asp:Label ID="lbl_StaffDesg"
                                            runat="server" CssClass="field-value"></asp:Label></td>
                                    <td width="20%">
                                        <asp:Label ID="Label10" runat="server" CssClass="field-label" 
                                            Text="Academic year"></asp:Label><span style="color: red">*</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddl_acdYear" runat="server" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RFV_Academicyear1" runat="server" Display="Dynamic" ControlToValidate="ddl_acdYear"
                                            ErrorMessage="Academic year required" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RFV_Academicyear2" runat="server" Display="Dynamic" ControlToValidate="ddl_acdYear"
                                            ErrorMessage="Academic year required" ValidationGroup="Draft"></asp:RequiredFieldValidator>
                                    </td>

                                </tr>
                                <tr>
                                    <td width="20%">
                                        <asp:Label ID="Label7" runat="server" CssClass="field-label"
                                            Text="Grade"></asp:Label><span style="color: red">*</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddl_grade" runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RFV_grade1" runat="server" ControlToValidate="ddl_grade" Display="Dynamic"
                                            ErrorMessage="Grade required" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RFV_grade2" runat="server" ControlToValidate="ddl_grade" Display="Dynamic"
                                            ErrorMessage="Grade required" ValidationGroup="Draft"></asp:RequiredFieldValidator>
                                    </td>
                                    <td width="20%">
                                        <asp:Label ID="Label4" runat="server" CssClass="field-label"
                                            Text="Section"></asp:Label><span style="color: red">*</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddl_section" runat="server" Width="100px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RFV_Section1" runat="server" Display="Dynamic" ControlToValidate="ddl_section"
                                            ErrorMessage="Section required" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RFV_Section2" runat="server" Display="Dynamic" ControlToValidate="ddl_section"
                                            ErrorMessage="Section required" ValidationGroup="Draft"></asp:RequiredFieldValidator>
                                    </td>

                                </tr>
                                <tr>
                                    <td width="20%">
                                        <asp:Label ID="Label6" runat="server" CssClass="field-label"
                                            Text="Subject"></asp:Label><span style="color: red">*</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddl_subject" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RFV_subject1" runat="server" Display="Dynamic" ControlToValidate="ddl_subject"
                                            ErrorMessage="Subject required" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RFV_Subject2" runat="server" ControlToValidate="ddl_subject"
                                            ErrorMessage="Subject required" ValidationGroup="Draft"></asp:RequiredFieldValidator>
                                    </td>
                                    <td width="20%">
                                        <asp:Label ID="Label8" runat="server" CssClass="field-label"
                                            Text="Topic"></asp:Label><span style="color: red">*</span></td>
                                    <td width="30%">
                                        <asp:TextBox ID="txt_topic" runat="server" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RFV_Topic1" runat="server" ControlToValidate="txt_topic" Display="Dynamic"
                                            ErrorMessage="Topic required" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RFV_Topic2" runat="server" ControlToValidate="txt_topic" Display="Dynamic"
                                            ErrorMessage="Topic required" ValidationGroup="Draft"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div style="float: right; width: 100%; margin-bottom:12px;" align="right">
                    <table width="100%">
                        <tr>
                            <td align="left" width="20%">
                                <asp:Label ID="Label3" runat="server" CssClass="field-label" Text="Evaluated By"></asp:Label></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lbl_ObserverName" runat="server" CssClass="field-value"></asp:Label></td>
                       
                            <td width="20%" align="left">
                                <asp:Label ID="Label9" runat="server" CssClass="field-label"
                                    Text="Evaluation Date"></asp:Label></td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txt_EvlDate" runat="server"></asp:TextBox>

                                <asp:ImageButton ID="imgEndDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                                 
                        <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                            Format="dd/MMM/yyyy" PopupButtonID="imgEndDate" TargetControlID="txt_EvlDate">
                        </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" TargetControlID="txt_EvlDate" PopupButtonID="txt_EvlDate">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_EvlDate"
                                    ErrorMessage="Date required" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_EvlDate"
                                    ErrorMessage="Date required" ValidationGroup="Draft">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txt_EvlDate"
                                    EnableViewState="False"
                                    ErrorMessage="Enter the End Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="Save">*</asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_EvlDate"
                                    EnableViewState="False"
                                    ErrorMessage="Enter the End Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="Draft">*</asp:RegularExpressionValidator>
                                <asp:Label ID="lbl_EVL_Date" runat="server" CssClass="FontBoldHeader2" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>

                            <td align="left" width="20%">
                                <asp:Label ID="Label11" runat="server" CssClass="field-label"
                                    Text="Class Strength"></asp:Label></td>

                            <td align="left" width="30%">
                                <asp:TextBox ID="txtClassStrength" runat="server"></asp:TextBox></td>
                        <td colspan="2"></td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <asp:GridView ID="gv_EvalMainCriteria" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    Width="100%" ShowHeader="False"  CellPadding="3" 
                    CellSpacing="2">
                    <RowStyle />
                    <Columns>
                        <asp:TemplateField HeaderText="MainCriteria">
                            <ItemTemplate>
                                <div class="container-fluid" >
                                    <div class="row bg-dark">
                                    <div class="col-lg-1 m-auto border-right">
                                            <asp:Label ID="lbl_MainCriteria" runat="server" Text='<%# Eval("EMC_DESC") %>' Width="100%" CssClass="font-weight-bold"></asp:Label>
                                            <asp:HiddenField ID="HF_EVT_ID" runat="server" Value='<%# Eval("EVT_ID") %>' />
                                            <asp:HiddenField ID="HF_EMC_ID" runat="server" Value='<%# Eval("EMC_ID") %>' />
                                   </div>
                                    <div class="col-lg-10 bg-dark m-auto">
                                            <asp:DataList ID="DL_EvalKeys" runat="server" RepeatDirection="Horizontal" CssClass="table border-bold mb-0">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_EvalKeys" runat="server" Text='<%# Eval("EMK_SCORE_DESC") %>' Width="165px" CssClass="font-weight-bold"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:DataList>
                                   </div>
                                   <div class="col-lg-1 m-auto"> 
                                            <asp:Label ID="lbl_HScore" runat="server" Text="Score" CssClass="font-weight-bold"></asp:Label>

                                    </div>
                                   </div>
                                    <div class="row">
                                            <asp:GridView ID="gv_EvalSubCriteria" CssClass="table" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnRowDataBound="gv_EvalSubCriteria_RowDataBound" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            
                                                                        <asp:Label ID="lbl_SubCriteria" runat="server" Text='<%# Eval("ESC_DESC") %>' ></asp:Label>
                                                                        <asp:HiddenField ID="HF_ESC_ID" runat="server" Value='<%# Eval("ESC_ID") %>' />
                                                                        <asp:HiddenField ID="HF_EMC_ID" runat="server" Value='<%# Eval("EMC_ID") %>' />
                                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("EMC_ID") %>' />
                                                            </itemTemplate>
                                                        <ItemStyle Width="10%"  CssClass="bg-dark"  />
                                                            </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                                        <asp:RadioButtonList ID="rbl_EVAL_SUBKEY_DESC" runat="server" RepeatDirection="Horizontal" Width="100%" CssClass="table table-bordered padding-0 m-0" onclick="javascript:aa(this)">
                                                                        </asp:RadioButtonList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                            ErrorMessage="<b>Please select any of the options</b>" ControlToValidate="rbl_EVAL_SUBKEY_DESC" InitialValue="" ValidationGroup="Save"></asp:RequiredFieldValidator>

                                                                   </ItemTemplate>
                                                                        <ItemStyle Width="80%" CssClass="padding-0" VerticalAlign="Top" />
                                                            </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                                        <span class="score"></span>
                                                                    
                                                        </ItemTemplate>
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                      </div> 
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle />
                    <PagerStyle HorizontalAlign="Center" />
                    <SelectedRowStyle Font-Bold="True" />
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>

                <%--<div style="float: left; width: 92%;height:30px; background-color:#92D050; vertical-align:middle;" ><center><asp:Label ID="lbl_HTotalScore" runat="server" Text="Total score" CssClass="FontHeading" ForeColor="White" Font-Bold="true"></asp:Label></center></div>
    <div style="float: right;width: 8%;height:30px; background-color:#00B050;vertical-align:middle;"><center>    <span class="totalscore" style="font-weight:bolder;">&nbsp;&nbsp;</span>    <asp:Label ID="lbl_TotalScore" runat="server" Text="" CssClass="FontHeading" ></asp:Label></center></div>--%>    
                

                <table width="100%">
                    <tr>
                <td float="left" width="20%">
                    
                        <asp:Label ID="lbl_HTotalScore" runat="server" Text="Total score" CssClass="field-label" Font-Bold="true"></asp:Label>
                   

                </td>
                <td float="left" width="10%">
                    
                        <span class="totalscore" style="font-weight: bolder;"> </span>
                        <asp:TextBox ID="lbl_TotalScore" runat="server"  CssClass="FontHeading"></asp:TextBox>
                </td>




                <td  float="left" width="22%">
                    
                        <asp:Label ID="lblh_AvgScore" runat="server" Text="Total Average" CssClass="field-label" ForeColor="" Font-Bold="true"></asp:Label>


                </td>
                <td float="left" width="8%">
                    
                        <%--<span class="totalAverage" style="font-weight:bolder;">&nbsp;&nbsp;</span>--%>
                        <asp:TextBox ID="txtAverage" runat="server" AutoPostBack="true" Text="" CssClass="FontHeading"></asp:TextBox>
                </td>


                <td float="left" width="8%">
                    
                        <asp:Label ID="lblh_Rating" runat="server" Text="Rating" CssClass="field-label" ForeColor="" Font-Bold="true"></asp:Label>


                </td>


                <td float="left" width="32%">
                    
                        <span class="totalRating" style="font-weight: bolder;"> </span>
                        <asp:Label ID="lbl_Rating" runat="server" Text="" CssClass="field-value"></asp:Label>
                </td>
                    </tr>
</table>




                <center>
                    <br />
                    <div style="float: left; width: 100%;" align="left">
                        <table width="100%">
                            <tr>
                                <td width="20%">
                                    <asp:Label ID="Label5" runat="server" CssClass="field-label" Text="Remarks"></asp:Label></td>
                                <td width="30%">
                                    <asp:TextBox ID="txtRemarks" runat="server" Width="100%" TextMode="MultiLine" 
                                        Rows="4"></asp:TextBox></td>
                                 <td width="20%">
                                    <asp:Label ID="Label14" runat="server" CssClass="field-label" Text="Areas of improvement &nbsp;&nbsp;:"></asp:Label></td>

                                <td width="30%">
                                    <asp:TextBox ID="txtAreasImprovement" runat="server" Width="100%" TextMode="MultiLine"
                                        Rows="4"></asp:TextBox></td>
                            </tr>                          

                            <tr>
                                <td>
                                    <asp:Label ID="Label15" runat="server" CssClass="field-label" Text="Upload Document "></asp:Label></td>
                                <td align="left"  style="width: 35%" colspan="1">
                                    <asp:LinkButton ID="btnDocumentLink" Text="view" Visible='false' runat="server" OnClientClick="showDocument();return true;"></asp:LinkButton>

                                    <asp:LinkButton ID="btnDocumentDelete" Text='[ Delete ]' Visible='false' runat="server" OnClientClick="return confirm('Are you sure you want to Delete this image ?');" ForeColor="Red"></asp:LinkButton>
                                    <asp:FileUpload ID="UploadDocPhoto" runat="server" Visible="false" Width="180px" ToolTip='Click "Browse" to select the photo. The file size should be less than 50 KB' />
                                    <asp:HiddenField ID="hdnFileName" runat="server" />
                                    <asp:HiddenField ID="hdnContentType" runat="server" />
                                    <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload" Visible="false" />
                                </td>
                            </tr>


                        </table>
                    </div>
                    <div class="clear"></div>
                    <asp:Button ID="btnDraft" runat="server" Text="SAVE" ToolTip="Save as draft"
                         CssClass="button" ValidationGroup="Draft" />
                    <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" ValidationGroup="Save" CssClass="button" />
                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_btnSubmit" ConfirmText="Once you submit the evaluation details, You will not be able to change"
                        TargetControlID="btnSubmit" runat="server">
                    </ajaxToolkit:ConfirmButtonExtender>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Draft" ShowMessageBox="true" />
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Save" ShowMessageBox="true" />
                    <br />
                    <asp:Label ID="lbl_Saveerror" runat="server" ForeColor="red" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                    <br />
                    <asp:Label ID="lblMsg" runat="server" Text="Click on ‘SAVE’ if you want to come back and make changes later. If you click on ‘SUBMIT’ once all the entries are completed." CssClass="FontBoldHeader2"></asp:Label>

                </center>
            </div>


            <asp:Panel ID="Panel_Close" runat="server" CssClass="darkPanlAlumini" Visible="false">
                <div class="panelQual">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div style="color: #084777; font-size: 11px; font-weight: bold; display: block; font-weight: bold; color: #1B80B6; overflow: hidden; padding: 5px; border: 1px solid #b5cae7; background: url('~/Images/mainHeahingrtbg.png') no-repeat right 0 #e6effc !important; margin: 5px 0;">
                                    <div style="float: left; width: 100%;" align="center">
                                        <asp:Label ID="lbl_Panel_Close" runat="server" ForeColor="red" Text="" EnableViewState="false"></asp:Label>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <center>
                                    <table width="80%">
                                        <tr>
                                            <td align="center">
                                                <asp:Button
                                                    ID="btn_Panel_Close" runat="server" Text="Close this Page" OnClientClick="javascript:window.close();" CssClass="button" />
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </form>
        <div class="footer">
            <div class="in" align="center">Copyright © 2019 GEMS Education. All Rights Reserved.</div>
        </div>
    </div>


    
</body>
</html>
