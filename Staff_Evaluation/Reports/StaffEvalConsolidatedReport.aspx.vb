﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Imports InfoSoftGlobal
Imports System.Web.Configuration

Partial Class Staff_Evaluation_Reports_StaffEvalConsolidatedReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000129" And ViewState("MainMnu_code") <> "H000988") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    BindBusinessUnit()
                    Bind_EvalTemplateSearch()
                    Bind_designation()
                    Bind_AcademicYear()
                    BindStaff()
                    ' Generate chart in Literal Control
                    'If Not Request.QueryString("STE_ID") Is Nothing Then
                    '    ViewState("STE_ID") = Encr_decrData.Decrypt(Request.QueryString("STE_ID").Replace(" ", "+"))
                    '    'Load_StaffDetails(ViewState("STE_ID"))
                    '    'CreatetableReport()
                    '    FCLiteral.Text = CreateChartReport(ViewState("STE_ID"))
                    'Else
                    '    Dim sb As New System.Text.StringBuilder()
                    '    sb.Append("<script language='javascript'>")
                    '    sb.Append("window.close();")
                    '    sb.Append("</script>")
                    '    'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "JCall1", sb.ToString(), False)
                    '    Page.ClientScript.RegisterStartupScript(Me.[GetType](), "Script", sb.ToString(), False)
                    'End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_AcademicYear()
        Bind_Grade()
        BindStaff()
        Bind_EvalTemplateSearch()
    End Sub
    Private Sub Bind_AcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@CLM_ID", Session("CLM"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_acdYear.DataSource = ds
            ddl_acdYear.DataValueField = "ACD_ID"
            ddl_acdYear.DataTextField = "ACY_DESCR"
            ddl_acdYear.DataBind()

            Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                        & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + ddl_bsu.SelectedValue + "' AND ACD_CLM_ID=" + Session("CLM")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddl_acdYear.Items.FindItemByValue(ds.Tables(0).Rows(0).Item(1)).Selected = True

            Bind_Grade()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_acdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_acdYear.SelectedIndexChanged
        Bind_Grade()
    End Sub
    Private Sub Bind_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_grade.DataSource = ds
            ddl_grade.DataValueField = "grm_id"
            ddl_grade.DataTextField = "grm_grd_id"
            ddl_grade.DataBind()
            ddl_grade.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
            Bind_Subject()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_grade_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_grade.SelectedIndexChanged
        Bind_Subject()
    End Sub
    Private Sub Bind_Subject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
            parm(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@GRD_ID", ddl_grade.SelectedItem.Text)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_ACD_GRD_SBG_DETAILS", parm)
            ddl_subject.DataSource = ds
            ddl_subject.DataValueField = "SBG_ID"
            ddl_subject.DataTextField = "SBG_DESCR"
            ddl_subject.DataBind()
            ddl_subject.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Sub Bind_EvalTemplateSearch()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
        parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", ddl_bsu.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
        ddl_EvalTemplate.DataSource = ds
        ddl_EvalTemplate.DataValueField = "EVT_ID"
        ddl_EvalTemplate.DataTextField = "EVT_TEMPLATE_DESC"
        ddl_EvalTemplate.DataBind()
        ddl_EvalTemplate.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
    End Sub
    Sub Bind_designation()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "Select DES_ID,DES_DESCR,DES_RES_ID as Res_ID from empdesignation_m where DES_FLAG='sd' ")
        ddl_designation.DataSource = ds.Tables(0)
        ddl_designation.DataValueField = "DES_ID"
        ddl_designation.DataTextField = "DES_DESCR"
        ddl_designation.DataBind()
        ddl_designation.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
    End Sub
    Sub BindStaff()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            'parm(2) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_Name.Text.Trim() = "", DBNull.Value, txt_Name.Text.Trim()))
            'parm(3) = New SqlClient.SqlParameter("@EMP_DES", IIf(txt_desg.Text.Trim() = "", DBNull.Value, txt_desg.Text.Trim()))
            'parm(4) = New SqlClient.SqlParameter("@EMP_REPORTTO", IIf(hf_EMPReportingTo.Value.Trim() = "", DBNull.Value, hf_EMPReportingTo.Value.Trim()))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EMPDETAILS", parm)
            ddl_emp.DataSource = ds
            ddl_emp.DataValueField = "EMP_ID"
            ddl_emp.DataTextField = "EMP_NAME"
            ddl_emp.DataBind()
            ddl_emp.Items.Insert(0, New RadComboBoxItem("ALL", "0"))
            'ddl_emp.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gv_Staff_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_Staff.NeedDataSource
        Bindgv_Staff()
    End Sub
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        Bindgv_Staff()
    End Sub
    Sub Bindgv_Staff()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(10) As SqlClient.SqlParameter
            Dim ds As DataSet
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@EVT_ID", IIf(ddl_EvalTemplate.SelectedIndex > 0, ddl_EvalTemplate.SelectedValue, DBNull.Value))
            parm(3) = New SqlClient.SqlParameter("@EMP_NAME", IIf(ddl_emp.SelectedIndex > 0, ddl_emp.SelectedItem.Text.Trim(), DBNull.Value))
            parm(4) = New SqlClient.SqlParameter("@DES_ID", IIf(ddl_designation.SelectedIndex > 0, ddl_designation.SelectedValue, DBNull.Value))
            parm(5) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            parm(6) = New SqlClient.SqlParameter("@GRM_ID", IIf(ddl_grade.SelectedIndex > 0, ddl_grade.SelectedValue, DBNull.Value))
            parm(7) = New SqlClient.SqlParameter("@SBG_ID", IIf(ddl_subject.SelectedIndex > 0, ddl_subject.SelectedValue, DBNull.Value))
            parm(8) = New SqlClient.SqlParameter("@EMPReportingTo", IIf(hf_EMPReportingTo.Value <> "", hf_EMPReportingTo.Value, DBNull.Value))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATED_STAFFS", parm)
            gv_Staff.DataSource = ds
            gv_Staff.DataBind()
        Catch ex As Exception
            'lbl_Saveerror.Text = ex.Message
        End Try
    End Sub
    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset.Click
        ddl_EvalTemplate.ClearSelection()
        ddl_emp.ClearSelection()
        ddl_designation.ClearSelection()
        ddl_acdYear.ClearSelection()
        ddl_grade.ClearSelection()
        ddl_subject.ClearSelection()
        hf_EMPReportingTo.Value = ""
        txt_EMPReportingTo.Text = ""
    End Sub
    Protected Sub lnk_select_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim HF_ERM_ID As HiddenField = TryCast(sender.parent.FindControl("HF_ERM_ID"), HiddenField)
            'Dim HF_ACD_ID As HiddenField = TryCast(sender.parent.FindControl("HF_ACD_ID"), HiddenField)

            Dim HF_EMP_ID As HiddenField = TryCast(sender.parent.FindControl("HF_EMP_ID"), HiddenField)
            IFrameconsolidatedReport.Attributes("src") = "ConsolidatedReport.aspx?EMP_ID=" + Encr_decrData.Encrypt(HF_EMP_ID.Value) + "&BSU_ID=" + Encr_decrData.Encrypt(ddl_bsu.SelectedValue)

            'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim parm(4) As SqlClient.SqlParameter
            'Dim ds As DataSet
            'parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            'parm(2) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            'parm(1) = New SqlClient.SqlParameter("@EMP_ID", HF_EMP_ID.Value)
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATED_STAFFS", parm)
            'GV_ConsolidatedReport.DataSource = ds
            'GV_ConsolidatedReport.DataBind()
            pnl_ConsolidatedReport.Visible = True
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lnkbtn_ConsolidatedReport_Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtn_ConsolidatedReport_Close.Click
        pnl_ConsolidatedReport.Visible = False
    End Sub

    'Protected Sub GV_ConsolidatedReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles GV_ConsolidatedReport.ItemDataBound
    '    If e.Item.ItemType = GridItemType.AlternatingItem OrElse e.Item.ItemType = GridItemType.Item Then
    '        Dim HF_EEM_ID As HiddenField = DirectCast(e.Item.FindControl("HF_EEM_ID"), HiddenField)
    '        Dim HF_STE_ID As HiddenField = DirectCast(e.Item.FindControl("HF_STE_ID"), HiddenField)
    '        Dim Ll_FChart As Literal = DirectCast(e.Item.FindControl("Ll_FChart"), Literal)
    '        Ll_FChart.Text = CreateChartReport(HF_STE_ID.Value, HF_EEM_ID.Value)
    '    End If
    'End Sub
    Private Function CreateChartReport(ByVal STE_ID As String, ByVal EEM_ID As String) As String
        Try
            Dim strXML As String
            ' strXML = "<graph caption='Academic Year Performance Chart' subCaption='Staff Evaluation' decimalPrecision='0' showNames='1' numberSuffix=' ' pieSliceDepth='30' formatNumberScale='0' xAxisName='Weekly Observations' yAxisName='Overall Marks' labelDisplay='STAGGER' rotateNames='0' labelPadding='100' >"
            strXML = "<chart caption='Academic Year Performance Chart' subCaption='Staff Evaluation' xAxisName='Observations' yAxisName='Overall Marks' palette='3' showValues='1' numberPrefix='Score-' useRoundEdges='1' labelDisplay='Wrap' showNames='1'>"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parmObservers(4) As SqlClient.SqlParameter
            parmObservers(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parmObservers(1) = New SqlClient.SqlParameter("@EOB_EEM_ID", EEM_ID)
            Dim dsObservers As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS", parmObservers)
            For Each drObservers As DataRow In dsObservers.Tables(0).Rows
                Dim EOB_ID As String = drObservers("EOB_ID")
                Dim parmResult(4) As SqlClient.SqlParameter
                parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
                parmResult(1) = New SqlClient.SqlParameter("@EEM_ID", EEM_ID)
                parmResult(2) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
                parmResult(3) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
                Dim dsResult As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
                For Each drResult As DataRow In dsResult.Tables(0).Rows
                    strXML += "<set label='" + drObservers("EMP_NAME").ToString() + "' value='" + drResult("SCORE").ToString() + "' />"
                Next
            Next
            strXML += "</chart>"

            'Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "SELECT ERS_ESK_ID,ERS_SCORE FROM EVA.EVAL_STAFF_RESULTS_M ERM WITH (NOLOCK)	INNER JOIN EVA.EVAL_STAFF_RESULTS_S ERS WITH (NOLOCK) ON ERM.ERM_ID=ERS.ERS_ERM_ID	WHERE  ERM_STE_ID=" + STE_ID + "") ', parm)
            '    While dr.Read()

            '        strXML += "<set name='" + dr("ERS_ESK_ID").ToString() + "' value='" + dr("ERS_SCORE").ToString() + "' />"
            '    End While
            'End Using
            'strXML += "</graph>"
            '  Dim xmlStr As String = "<chart caption='Top 5 Employees for 2011' palette='3' showValues='0' numberPrefix='$' useRoundEdges='1'><set label='Leverling' value='100524'/><set label='Fuller' value='87790'/><set label='Davolio' value='81898'/><set label='Peacock' value='76438'/><set label='Callahan' value='55091'/></chart>"

            ' FusionCharts.SetRenderer("javascript")

            'FCLiteral.Text = FusionCharts.RenderChart("../../FusionCharts/Column3D.swf", "", strXML, "browser_share_2", "640", "340", False, True)

            Dim ChartID As String = "Chart" + EEM_ID.ToString()
            Literal1.Text = FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + EEM_ID.ToString(), "", strXML, ChartID, "640", "340", False, True)

            'Return FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + EEM_ID.ToString(), "", strXML, "browser_share_2", "640", "340", False, True)
            Return FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + EEM_ID.ToString(), "", strXML, ChartID, "640", "340", False, True)
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Protected Sub lnkbtn_GroupObservers_Close_Click(sender As Object, e As EventArgs) Handles lnkbtn_GroupObservers_Close.Click
        Pnl_Observers.Visible = False

    End Sub
    Protected Sub lnk_Download_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_EMP_ID As HiddenField = TryCast(sender.parent.FindControl("HF_EMP_ID"), HiddenField)
            BindGrid_StaffGroup_Pnl(HF_EMP_ID.Value)
            h_StaffId.Value = HF_EMP_ID.Value
            Pnl_Observers.Visible = True
        Catch ex As Exception

        End Try
    End Sub
    Sub BindGrid_StaffGroup_Pnl(ByVal STE_EMP_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(1) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            parm(1) = New SqlClient.SqlParameter("@STE_EMP_ID", STE_EMP_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[EVA].[GET_OBSERVERSLIST]", parm)
            GVGroup.DataSource = ds
            GVGroup.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Try
            Dim eoBempid As String = ""
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim STE_ID As String = ""
            Dim EEM_ID As String = ""
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(2) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu.SelectedValue)
            parm(1) = New SqlClient.SqlParameter("@EMP_ID", h_StaffId.Value)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATED_STAFFS", parm)

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                STE_ID = STE_ID + ds.Tables(0).Rows(i)(3).ToString() + "|"
                EEM_ID = EEM_ID + ds.Tables(0).Rows(i)(2).ToString() + "|"


            Next

            For Each gr As GridDataItem In GVGroup.Items
                Dim cbSelect As CheckBox = gr.FindControl("cbSelect")
                If (cbSelect.Checked = True) Then
                    Dim HF_ObsNo As HiddenField = TryCast(gr.FindControl("HF_ObsNo"), HiddenField)
                    eoBempid = eoBempid + HF_ObsNo.Value + "|"
                End If
            Next

            'ViewState("STE_ID")


            'Dim HF_EMP_ID As HiddenField = TryCast(sender.parent.FindControl("HF_EMP_ID"), HiddenField)
            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim cmd As New SqlCommand
            'cmd.CommandText = "[EVA].[rptStaffConsolidatedReport]"
            'Dim sqlParam(2) As SqlParameter
            'sqlParam(0) = Mainclass.CreateSqlParameter("@STE_EMP_ID", h_StaffId.Value, SqlDbType.Int)
            'cmd.Parameters.Add(sqlParam(0))
            'sqlParam(1) = Mainclass.CreateSqlParameter("@EOB_EMP_ID", eoBempid, SqlDbType.VarChar)
            'cmd.Parameters.Add(sqlParam(1))

            'sqlParam(2) = Mainclass.CreateSqlParameter("@EOB_bsu_id", Session("sBsuid"), SqlDbType.VarChar)
            'cmd.Parameters.Add(sqlParam(2))

            'cmd.Connection = New SqlConnection(str_conn)
            'cmd.CommandType = CommandType.StoredProcedure

            Dim param As New Hashtable
            param.Add("@STE_EMP_ID", h_StaffId.Value)
            param.Add("@EOB_ID", eoBempid)
            param.Add("@EEM_ID", EEM_ID)
            param.Add("@STE_ID", STE_ID)
            param.Add("@EOB_bsu_id", Session("SBSUID"))

            param.Add("preparedby", Session("susr_name"))
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "oasis"
                .reportParameters = param
                .reportPath = Server.MapPath("../Reports/rptStaffEvalConsolidatedReport.rpt")

            End With
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try




    End Sub
End Class

