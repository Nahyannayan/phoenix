﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="DownloadConsolidatedReport.aspx.vb" Inherits="Staff_Evaluation_Reports_DownloadConsolidatedReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../../FusionCharts/FusionCharts.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
        var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';

        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);

            $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
        }

        $(document).ready(function () {

            $(allCheckBoxSelector).live('click', function () {
                $(checkBoxSelector).attr('checked', $(this).is(':checked'));
                ToggleCheckUncheckAllOptionAsNeeded();
            });
            $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
            ToggleCheckUncheckAllOptionAsNeeded();
        });

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen("../../Accounts/accShowEmpDetail.aspx?id=ERP", "pop_up")
           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txt_EMPReportingTo.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <style type="text/css">
         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }
    </style>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Evaluation Report 
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table style="width: 100%;" >
                    <%--<tr class="subheader_img">
            <td colspan="6" >
                
                 </td>
        </tr>--%>
                    <tr style="color: Black">
                        <td align="left"><span class="field-label">Business Unit </span><span style="color: red">*</span></td>

                        <td align="left">
                            <telerik:RadComboBox ID="ddl_bsu" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>


                        </td>
                        <td align="left"><span class="field-label">Evaluation Template</span></td>

                        <td align="left">
                            <telerik:RadComboBox ID="ddl_EvalTemplate" runat="server" Width="80%">
                            </telerik:RadComboBox>

                        </td>
                    </tr>

                    <tr >
                        <td align="left"><span class="field-label">Name</span></td>

                        <td align="left">
                            <telerik:RadComboBox ID="ddl_emp" runat="server" Filter="Contains" Width="80%">
                            </telerik:RadComboBox>
                            <%--<asp:TextBox ID="txt_Name" runat="server"  Width="90%"></asp:TextBox>--%>
                        </td>
                        <td align="left"><span class="field-label">Designation</span></td>

                        <td align="left">
                            <telerik:RadComboBox ID="ddl_designation" runat="server" Width="80%">
                            </telerik:RadComboBox>

                        </td>
                    </tr>

                    <tr style="color: Black">
                        <td align="left"><span class="field-label">Reporting To</span></td>

                        <td align="left">
                            <asp:TextBox ID="txt_EMPReportingTo" runat="server" Width="80%"></asp:TextBox>
                            <asp:ImageButton ID="btn_pickreportingstaff" runat="server"
                                CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;"
                                Style="width: 16px" Text="Search" ToolTip="Search" />
                            <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />

                        </td>
                        <td><span class="field-label">Academic year</span><span style="color: red">*</span></td>

                        <td>
                            <telerik:RadComboBox ID="ddl_acdYear" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td><span class="field-label">Grade</span><span style="color: red">*</span></td>

                        <td>
                            <telerik:RadComboBox ID="ddl_grade" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td><span class="field-label">Subject</span><span style="color: red">*</span></td>

                        <td>
                            <telerik:RadComboBox ID="ddl_subject" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4" align="center">
                            <asp:Button ID="btn_Search" runat="server" CausesValidation="False"
                                CssClass="button" Text="Search" ToolTip="Search" />
                            <asp:Button ID="btn_reset" runat="server" CausesValidation="False"
                                CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                            <asp:Button ID="btn_export" runat="server" CausesValidation="False"
                                CssClass="button" Text="Export To Excel" ToolTip="Search" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4">
                            <div>
                                <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False"
                                    CellSpacing="0" EnableTheming="False" GridLines="None"
                                    Width="100%" AllowPaging="True">
                                    <MasterTableView>
                                        <CommandItemSettings />
                                        <RowIndicatorColumn Visible="True">
                                            <HeaderStyle />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Visible="True">
                                            <HeaderStyle />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn UniqueName="StaffName" DataField="StaffName" HeaderText="StaffName">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="EVALUATIONGROUP" DataField="EVALUATIONGROUP" HeaderText="Evaluation Group">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="DateObserved" DataField="DateObserved" HeaderText="Date Observed">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="Observer" DataField="Observer" HeaderText="Observer">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="Subject" DataField="Subject" HeaderText="Subject">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="SCORE" DataField="SCORE" HeaderText="Score">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn UniqueName="Remarks" DataField="Remarks" HeaderText="Remarks">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn>
                                            </EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                    <ItemStyle  HorizontalAlign="Left" />
                                    <AlternatingItemStyle  HorizontalAlign="Left" />
                                    <FilterMenu>
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="4">
                            <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

