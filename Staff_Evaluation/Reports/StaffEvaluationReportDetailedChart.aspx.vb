﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Imports InfoSoftGlobal

Partial Class Staff_Evaluation_Reports_StaffEvaluationReportDetailedChart
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        'Session("sUsr_name") = "ARUN"
        'Session("sBsuid") = "999998"
        'Dim EVT_ID As String = "4"
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                'get the menucode to confirm the user is accessing the valid page
                'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'Session("MainMnu_code_pro") = ViewState("MainMnu_code")

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    If Not Request.QueryString("EVT_ID") Is Nothing And Not Request.QueryString("EEM_ID") Is Nothing And Not Request.QueryString("STE_ID") Is Nothing Then
                        ViewState("EVT_ID") = Encr_decrData.Decrypt(Request.QueryString("EVT_ID").Replace(" ", "+"))
                        ViewState("EEM_ID") = Encr_decrData.Decrypt(Request.QueryString("EEM_ID").Replace(" ", "+"))
                        ViewState("STE_ID") = Encr_decrData.Decrypt(Request.QueryString("STE_ID").Replace(" ", "+"))
                        'ViewState("EOB_ID") = Encr_decrData.Decrypt(Request.QueryString("EOB_ID").Replace(" ", "+"))
                        'ViewState("ERM_ID") = Encr_decrData.Decrypt(Request.QueryString("ERM_ID").Replace(" ", "+"))

                        ViewState("ERM_ID") = LoadEvlResult_Details(ViewState("EEM_ID"), ViewState("STE_ID"), ViewState("EOB_ID"))
                        Load_EvalForm(ViewState("EVT_ID"))
                        Load_StaffDetails(ViewState("STE_ID"))
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub Load_EvalForm(ByVal EVT_ID As String)
        Bind_EvalMainCriteria(EVT_ID)
    End Sub
    Private Sub Load_StaffDetails(ByVal STE_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 5)
            parm(1) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            If (ds.Tables(0).Rows.Count > 0) Then
                lbl_StaffName.Text = Convert.ToString(ds.Tables(0).Rows(0)("EMP_NAME"))
                lbl_StaffDesg.Text = Convert.ToString(ds.Tables(0).Rows(0)("DES_DESCR"))
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function LoadEvlResult_Details(ByVal EEM_ID As String, ByVal STE_ID As String, ByVal EOB_ID As String) As String
        Dim ERM_ID As String = "0"
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@EEM_ID", EEM_ID)
            parm(2) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
            parm(3) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
            'ERM_ID = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS_M", parm)

            Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS_M", parm)
                While dr.Read()
                    ERM_ID = dr("ERM_ID")
                    ViewState("ERM_bCOMPLETED") = dr("ERM_bCOMPLETED")
                End While
            End Using
        Catch ex As Exception

        End Try
        Return ERM_ID
    End Function
    Private Sub Bind_EvalMainCriteria(ByVal EVT_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@EVT_ID", EVT_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            gv_EvalMainCriteria.DataSource = ds
            gv_EvalMainCriteria.DataBind()

            If (ds.Tables(0).Rows.Count > 0) Then
                lblEvaluationHeading1.Text = Convert.ToString(ds.Tables(0).Rows(0)("EvaluationHeading1"))
                lblEvaluationHeading2.Text = Convert.ToString(ds.Tables(0).Rows(0)("EvaluationHeading2"))
            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub Bind_EvalKeys(ByVal EVT_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parm(1) = New SqlClient.SqlParameter("@EVT_ID", EVT_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gv_EvalMainCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gv_EvalMainCriteria.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim DL_EvalKeys As DataList = DirectCast(e.Row.FindControl("DL_EvalKeys"), DataList)
            Dim HF_EVT_ID As HiddenField = DirectCast(e.Row.FindControl("HF_EVT_ID"), HiddenField)
            Dim HF_EMC_ID As HiddenField = DirectCast(e.Row.FindControl("HF_EMC_ID"), HiddenField)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim parm(4) As SqlClient.SqlParameter
            'parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            'parm(1) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            'DL_EvalKeys.DataSource = ds
            'DL_EvalKeys.DataBind()


            Dim gv_EvalSubCriteria As GridView = DirectCast(e.Row.FindControl("gv_EvalSubCriteria"), GridView)
            Dim parmSubCriteria(4) As SqlClient.SqlParameter
            parmSubCriteria(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
            parmSubCriteria(1) = New SqlClient.SqlParameter("@EVT_ID", HF_EVT_ID.Value)
            parmSubCriteria(2) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            Dim dsSubCriteria As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parmSubCriteria)
            gv_EvalSubCriteria.DataSource = dsSubCriteria
            gv_EvalSubCriteria.DataBind()
        End If
    End Sub
    Public counter As Integer
    Protected Sub gv_EvalSubCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim DL_EvalSubCriteriaKeyMap As DataList = DirectCast(e.Row.FindControl("DL_EvalSubCriteriaKeyMap"), DataList)

            'Dim rbl_EVAL_SUBKEY_DESC As RadioButtonList = DirectCast(e.Row.FindControl("rbl_EVAL_SUBKEY_DESC"), RadioButtonList)
            Dim HF_ESC_ID As HiddenField = DirectCast(e.Row.FindControl("HF_ESC_ID"), HiddenField)
            Dim HF_EMC_ID As HiddenField = DirectCast(e.Row.FindControl("HF_EMC_ID"), HiddenField)
            Dim lit_chart As Literal = DirectCast(e.Row.FindControl("lit_chart"), Literal)


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 4)
            parm(1) = New SqlClient.SqlParameter("@EMC_ID", HF_EMC_ID.Value)
            parm(2) = New SqlClient.SqlParameter("@ESC_ID", HF_ESC_ID.Value)



            Dim strXML As String = ""
            strXML = "<chart caption='Academic Year Performance Chart' subCaption='Staff Evaluation' xAxisName='Weekly Observations' yAxisName='Overall Marks' palette='3' showValues='1' numberPrefix='Score-' useRoundEdges='1' labelDisplay='Wrap' showNames='1' yAxisMaxValue='5' >"
            Dim parmObservers(4) As SqlClient.SqlParameter
            parmObservers(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parmObservers(1) = New SqlClient.SqlParameter("@EOB_EEM_ID", ViewState("EEM_ID"))
            Dim dsObservers As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS", parmObservers)
            For Each drObservers As DataRow In dsObservers.Tables(0).Rows
                Dim EOB_ID As String = drObservers("EOB_ID")
                Dim parmResult(4) As SqlClient.SqlParameter
                parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 4)
                parmResult(1) = New SqlClient.SqlParameter("@ESC_ID", HF_ESC_ID.Value)
                parmResult(2) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
                parmResult(3) = New SqlClient.SqlParameter("@STE_ID", ViewState("STE_ID"))
                Dim dsResult As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
                For Each drResult As DataRow In dsResult.Tables(0).Rows
                    strXML += "<set label='" + drObservers("EMP_NAME").ToString() + "' value='" + drResult("ERS_SCORE").ToString() + "' />"
                Next
            Next
            strXML += "</chart>"
            counter = counter + 1
            Dim ChartID As String = "Chart" + counter.ToString()

            lit_chart.Text = FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + counter.ToString(), "", strXML, ChartID, "640", "340", False, True)


            'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            'rbl_EVAL_SUBKEY_DESC.DataSource = ds
            'rbl_EVAL_SUBKEY_DESC.DataTextField = "ESK_DESC"
            'rbl_EVAL_SUBKEY_DESC.DataValueField = "ESK_ID"
            'rbl_EVAL_SUBKEY_DESC.DataBind()

            'If (ViewState("ERM_ID") <> "0") Then
            '    Dim parmResult(4) As SqlClient.SqlParameter
            '    parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            '    parmResult(1) = New SqlClient.SqlParameter("@ERM_ID", ViewState("ERM_ID"))
            '    parmResult(2) = New SqlClient.SqlParameter("@ESC_ID", HF_ESC_ID.Value)
            '    Dim ERS_ESK_ID As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
            '    rbl_EVAL_SUBKEY_DESC.SelectedValue = ERS_ESK_ID
            '    ViewState("SelectedRadio") = ViewState("SelectedRadio") + "||" + rbl_EVAL_SUBKEY_DESC.ClientID
            'End If
        End If
    End Sub

    Protected Sub btn_Panel_Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Panel_Close.Click

    End Sub
End Class
