﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Imports InfoSoftGlobal
Partial Class Staff_Evaluation_Reports_ConsolidatedReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm(4) As SqlClient.SqlParameter
        Dim ds As DataSet
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
        parm(2) = New SqlClient.SqlParameter("@BSU_ID", Encr_decrData.Decrypt(Request.QueryString("BSU_ID").Replace(" ", "+")))
        parm(1) = New SqlClient.SqlParameter("@EMP_ID", Encr_decrData.Decrypt(Request.QueryString("EMP_ID").Replace(" ", "+")))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATED_STAFFS", parm)
        GV_ConsolidatedReport.DataSource = ds
        GV_ConsolidatedReport.DataBind()
        'pnl_ConsolidatedReport.Visible = True
    End Sub
    Protected Sub GV_ConsolidatedReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles GV_ConsolidatedReport.ItemDataBound
        If e.Item.ItemType = GridItemType.AlternatingItem OrElse e.Item.ItemType = GridItemType.Item Then
            Dim HF_EEM_ID As HiddenField = DirectCast(e.Item.FindControl("HF_EEM_ID"), HiddenField)
            Dim HF_STE_ID As HiddenField = DirectCast(e.Item.FindControl("HF_STE_ID"), HiddenField)
            Dim Ll_FChart As Literal = DirectCast(e.Item.FindControl("Ll_FChart"), Literal)
            Dim lblEVT_TEMPLATE_DESC As Label = DirectCast(e.Item.FindControl("lblEVT_TEMPLATE_DESC"), Label)
            Dim LBL_EEM_REMARKS As Label = DirectCast(e.Item.FindControl("LBL_EEM_REMARKS"), Label)
            
            Ll_FChart.Text = CreateChartReport(HF_STE_ID.Value, HF_EEM_ID.Value, lblEVT_TEMPLATE_DESC.Text, LBL_EEM_REMARKS.Text)
        End If
    End Sub
    Private Function CreateChartReport(ByVal STE_ID As String, ByVal EEM_ID As String, ByVal caption As String, ByVal subCaption As String) As String
        Try
            Dim strXML As String
            ' strXML = "<graph caption='Academic Year Performance Chart' subCaption='Staff Evaluation' decimalPrecision='0' showNames='1' numberSuffix=' ' pieSliceDepth='30' formatNumberScale='0' xAxisName='Weekly Observations' yAxisName='Overall Marks' labelDisplay='STAGGER' rotateNames='0' labelPadding='100' >"
            strXML = "<chart caption='" + caption + "' subCaption='" + subCaption + "' xAxisName='Observations' yAxisName='Overall Marks' palette='3' showValues='1' numberPrefix='Score-' useRoundEdges='1' labelDisplay='Wrap' showNames='1'>"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parmObservers(4) As SqlClient.SqlParameter
            parmObservers(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parmObservers(1) = New SqlClient.SqlParameter("@EOB_EEM_ID", EEM_ID)
            parmObservers(2) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
            Dim dsObservers As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS", parmObservers)
            For Each drObservers As DataRow In dsObservers.Tables(0).Rows
                Dim EOB_ID As String = drObservers("EOB_ID")
                Dim parmResult(4) As SqlClient.SqlParameter
                parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
                parmResult(1) = New SqlClient.SqlParameter("@EEM_ID", EEM_ID)
                parmResult(2) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
                parmResult(3) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
                Dim dsResult As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
                For Each drResult As DataRow In dsResult.Tables(0).Rows
                    strXML += "<set label='" + drObservers("EMP_NAME").ToString() + "' value='" + drResult("SCORE").ToString() + "' />"
                Next
            Next
            strXML += "</chart>"

            'Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "SELECT ERS_ESK_ID,ERS_SCORE FROM EVA.EVAL_STAFF_RESULTS_M ERM WITH (NOLOCK)	INNER JOIN EVA.EVAL_STAFF_RESULTS_S ERS WITH (NOLOCK) ON ERM.ERM_ID=ERS.ERS_ERM_ID	WHERE  ERM_STE_ID=" + STE_ID + "") ', parm)
            '    While dr.Read()

            '        strXML += "<set name='" + dr("ERS_ESK_ID").ToString() + "' value='" + dr("ERS_SCORE").ToString() + "' />"
            '    End While
            'End Using
            'strXML += "</graph>"
            '  Dim xmlStr As String = "<chart caption='Top 5 Employees for 2011' palette='3' showValues='0' numberPrefix='$' useRoundEdges='1'><set label='Leverling' value='100524'/><set label='Fuller' value='87790'/><set label='Davolio' value='81898'/><set label='Peacock' value='76438'/><set label='Callahan' value='55091'/></chart>"

            ' FusionCharts.SetRenderer("javascript")

            'FCLiteral.Text = FusionCharts.RenderChart("../../FusionCharts/Column3D.swf", "", strXML, "browser_share_2", "640", "340", False, True)

            Dim ChartID As String = "Chart" + EEM_ID.ToString()
            ' Literal1.Text = FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + EEM_ID.ToString(), "", strXML, ChartID, "640", "340", False, True)

            'Return FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + EEM_ID.ToString(), "", strXML, "browser_share_2", "640", "340", False, True)
            Return FusionCharts.RenderChart("../../FusionCharts/Column3D.swf?NoCache=" + EEM_ID.ToString(), "", strXML, ChartID, "540", "340", False, True)

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub GV_ConsolidatedReport_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles GV_ConsolidatedReport.NeedDataSource

    End Sub
End Class
