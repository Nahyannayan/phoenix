﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffEvalConsolidatedReport.aspx.vb" Inherits="Staff_Evaluation_Reports_StaffEvalConsolidatedReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../../FusionCharts/FusionCharts.js" type="text/javascript"></script>

    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 10%;
            top: 10%;
            position: fixed;
            width: 70%;
        }

         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
    <%--var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
    var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';--%>

        var allCheckBoxSelector = '#<%=GVGroup.ClientID%> input[id*="chkAll"]:checkbox';
        var checkBoxSelector = '#<%=GVGroup.ClientID%> input[id*="cbSelect"]:checkbox';






        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);

            $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
        }

        $(document).ready(function () {

            $(allCheckBoxSelector).live('click', function () {
                $(checkBoxSelector).attr('checked', $(this).is(':checked'));
                ToggleCheckUncheckAllOptionAsNeeded();
            });
            $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
            ToggleCheckUncheckAllOptionAsNeeded();
        });



        function GetEMPName() {

            var NameandCode;
            var result;
            result = radopen("../../Accounts/accShowEmpDetail.aspx?id=ERP", "pop_up")
           <%-- if (result != '' && result != undefined) {
                NameandCode = result.split('___');
               document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }
        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txt_EMPReportingTo.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Evaluation Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table style="width: 100%;">
                    <%--<tr class="subheader_img">
            <td colspan="6" >
                 
                 </td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span><span style="color: red">*</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="ddl_bsu" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>


                        </td>
                        <td align="left" width="20%"><span class="field-label">Evaluation Template</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="ddl_EvalTemplate" runat="server" Width="80%">
                            </telerik:RadComboBox>

                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Name</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="ddl_emp" runat="server" Filter="Contains" Width="80%">
                            </telerik:RadComboBox>
                            <%--  <telerik:GridTemplateColumn  
                                HeaderText="Select" UniqueName="TemplateColumn">
                                         <HeaderTemplate>
                              <asp:CheckBox runat="server" ID="chkAll" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="cbSelect" runat="server" />
                        </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Designation</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="ddl_designation" runat="server" Width="80%">
                            </telerik:RadComboBox>

                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reporting To</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txt_EMPReportingTo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btn_pickreportingstaff" runat="server"
                                CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;"
                                Style="width: 16px" Text="Search" ToolTip="Search" />
                            <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />
                            &nbsp;
                        </td>
                        <td width="20%"><span class="field-label">Academic year</span><span style="color: red">*</span></td>

                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_acdYear" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td width="20%"><span class="field-label">Grade</span><span style="color: red">*</span></td>

                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_grade" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%"><span class="field-label">Subject</span><span style="color: red">*</span></td>

                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_subject" runat="server" Width="80%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4" align="center">
                            <asp:Button ID="btn_Search" runat="server" CausesValidation="False"
                                CssClass="button" Text="Search" ToolTip="Search" />
                            <asp:Button ID="btn_reset" runat="server" CausesValidation="False"
                                CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                        </td>
                    </tr>

                    <tr>

                        <td align="left" colspan="4">
                            <div style="overflow: auto;">
                                <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False"
                                    CellSpacing="0" EnableTheming="False" GridLines="None"
                                    AllowPaging="True">
                                    <MasterTableView>
                                        <CommandItemSettings />
                                        <RowIndicatorColumn Visible="True">
                                            <HeaderStyle />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn Visible="True">
                                            <HeaderStyle />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <%--  <telerik:GridTemplateColumn  
                                HeaderText="Select" UniqueName="TemplateColumn">
                                         <HeaderTemplate>
                              <asp:CheckBox runat="server" ID="chkAll" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="cbSelect" runat="server" />
                        </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>

                                            <telerik:GridTemplateColumn HeaderText="BSU"
                                                UniqueName="TemplateColumn_BSU">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Staff Name"
                                                UniqueName="TemplateColumn_staffName">

                                                <ItemTemplate>

                                                    <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Designation"
                                                UniqueName="TemplateColumn_desg">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                    <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                    <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                Visible="true">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                    <%--     <asp:HiddenField ID="HF_ERM_ID" runat="server" Value='<%# Bind("ERM_ID") %>' />
                                <asp:HiddenField ID="HF_ACD_ID" runat="server" Value='<%# Bind("ERM_ACD_ID") %>' />--%>
                                                    <asp:LinkButton ID="lnk_select" runat="server" OnClick="lnk_select_Click">Select</asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Download Report" UniqueName="Template_Download"
                                                Visible="true">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnk_Download" runat="server" OnClick="lnk_Download_Click">Print</asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn>
                                            </EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                    <ItemStyle  HorizontalAlign="Left" />
                                    <AlternatingItemStyle  HorizontalAlign="Left" />
                                    <FilterMenu>
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </div>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="4">
                            <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>

                </table>


                <asp:Panel ID="pnl_ConsolidatedReport" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <div style="float: left;">
                                Consolidated Report
                            </div>
                            <div style="float: right">
                                <asp:LinkButton ForeColor="Red" ID="lnkbtn_ConsolidatedReport_Close" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False"></asp:LinkButton>
                            </div>
                        </div>

                        <iframe runat="server" id="IFrameconsolidatedReport" width="100%" height="100%"></iframe>
                        <%--<telerik:RadGrid ID="GV_ConsolidatedReport" runat="server" AutoGenerateColumns="False" 
                    CellSpacing="0" EnableTheming="False" GridLines="None" Skin="Office2007"  PageSize="10" 
                    Width="90%" AllowPaging="true">
                    <MasterTableView>
                        <CommandItemSettings />
                        <RowIndicatorColumn Visible="True">
                            <HeaderStyle />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True">
                            <HeaderStyle />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn  
                                HeaderText="Select" UniqueName="TemplateColumn">
                                         <HeaderTemplate>
                              
                        </HeaderTemplate>
                        <ItemTemplate>
                        <asp:Label ID="lblEVT_TEMPLATE_DESC" runat="server" Text='<%# Bind("EVT_TEMPLATE_DESC") %>' /><br />
                        <asp:Label ID="LBL_EEM_REMARKS" runat="server" Text='<%# Bind("EEM_REMARKS") %>' />
                         <asp:HiddenField ID="HF_EEM_ID" runat="server" Value='<%# Bind("EEM_ID") %>' />
                         <asp:HiddenField ID="HF_STE_ID" runat="server" Value='<%# Bind("STE_ID") %>' />
                         <asp:Literal ID="Ll_FChart" runat="server"></asp:Literal>
                            
                        </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            </Columns> 
                            </MasterTableView> 
                            </telerik:RadGrid>--%>

                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        <%-- <div style="overflow:auto;height:800px;width:100%"> 
             </div> --%>
                    </div>
                </asp:Panel>




                <asp:Panel ID="Pnl_Observers" runat="server" CssClass="darkPanlAlumini" Visible="false">

                    <div class="panel-cover inner_darkPanlAlumini">
                        <%--style="overflow:auto;"--%>
                        <div>
                            <div class="title-bg">
                                Select Observer
                            </div>
                            <div style="float: right">
                                <asp:LinkButton ForeColor="Red" ID="lnkbtn_GroupObservers_Close" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkbtn_GroupObservers_Close_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <table style="width: 100%;">
                            <tr style="color: Black">
                                <td align="left">
                                    <div style="overflow: auto;">
                                        <%----%>
                                        <telerik:RadGrid ID="GVGroup" runat="server" AutoGenerateColumns="False"
                                            CellSpacing="0" EnableTheming="False" GridLines="None" PageSize="10"
                                            AllowPaging="true">
                                            <MasterTableView>
                                                <CommandItemSettings />
                                                <RowIndicatorColumn Visible="True">
                                                    <HeaderStyle />
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn Visible="True">
                                                    <HeaderStyle />
                                                </ExpandCollapseColumn>
                                                <Columns>
                                                    <telerik:GridTemplateColumn
                                                        HeaderText="Select" UniqueName="TemplateColumn">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAll" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Employee No"
                                                        UniqueName="TemplateColumn_BSU">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_NO" runat="server" Text='<%# Bind("[EMPNO]")%>' />
                                                            <asp:HiddenField ID="HF_ObsNo" runat="server" Value='<%# Bind("[EMPNO]")%>' />
                                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("[ID]")%>' />

                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>

                                                    <telerik:GridTemplateColumn HeaderText="Employee Name"
                                                        UniqueName="TemplateColumn_desg">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_Name" runat="server" Text='<%# Bind("[EMP_NAME]")%>' />



                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                        Visible="False">
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn>
                                                    </EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                            <ItemStyle  HorizontalAlign="Left" />
                                            <AlternatingItemStyle  HorizontalAlign="Left" />
                                            <FilterMenu>
                                            </FilterMenu>
                                        </telerik:RadGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download Report" />
                                </td>
                            </tr>

                        </table>

                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h_StaffId" runat="server" Value="0" />

                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>

            </div>
        </div>
    </div>

</asp:Content>

