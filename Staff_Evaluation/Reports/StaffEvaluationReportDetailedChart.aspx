﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StaffEvaluationReportDetailedChart.aspx.vb" Inherits="Staff_Evaluation_Reports_StaffEvaluationReportDetailedChart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::GEMS OASIS:: Staff Evaluation::Detailed Report::</title>
    <%--<link href="../cssfiles/StaffEvaluationStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 

    <script src="../../FusionCharts/FusionCharts.js" type="text/javascript"></script>
    <style type="text/css">
        .style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="wrapper">
       <div class="header"></div>
        <div class="clear"></div>

        <form id="form1" runat="server">
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <div>
                <center>
                    <asp:Label ID="lblEvaluationHeading1" runat="server" Text="" CssClass="title-bg"></asp:Label><br />
                    <asp:Label ID="lblEvaluationHeading2" runat="server" Text="" CssClass="title-bg" ></asp:Label>
                </center>

                <div style="float: left; width: 100%;">
                    <table width="100%" >
                        <tr>
                            <td width="20%">
                                <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Staff Name"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_StaffName" runat="server" CssClass="field-value"></asp:Label></td>
                            <td width="20%">
                                <asp:Label ID="Label2" runat="server" CssClass="field-label" Text="Designation"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_StaffDesg" runat="server" CssClass="field-value"></asp:Label></td>
                        </tr>
                       
                    </table>
                </div>
                <%--<div style="float: right; width: 50%;" align="right">
                </div>--%>
               <%-- <div class="clear"></div>--%>
                <asp:GridView ID="gv_EvalMainCriteria" runat="server" AutoGenerateColumns="False"
                    Width="100%" ShowHeader="False"  CssClass="table table-bordered table-row"
                     CellPadding="3" 
                    CellSpacing="2">
                    <RowStyle />
                    <Columns>
                        <asp:TemplateField HeaderText="MainCriteria">
                            <ItemTemplate>
                                <table class="style1" width="100%">
                                    <tr style="background-color: #CEDCD1;">
                                        <td style="width: 100%" colspan="2">
                                            <asp:Label ID="lbl_MainCriteria" runat="server" Text='<%# Eval("EMC_DESC") %>' CssClass="FontBold"></asp:Label>
                                            <asp:HiddenField ID="HF_EVT_ID" runat="server" Value='<%# Eval("EVT_ID") %>' />
                                            <asp:HiddenField ID="HF_EMC_ID" runat="server" Value='<%# Eval("EMC_ID") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;" colspan="3">
                                            <asp:GridView ID="gv_EvalSubCriteria" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnRowDataBound="gv_EvalSubCriteria_RowDataBound" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td style="width: 10%; background-color: #D3D3D3;">
                                                                        <asp:Label ID="lbl_SubCriteria" runat="server" Text='<%# Eval("ESC_DESC") %>' Width="142px" CssClass="FontBold"></asp:Label>
                                                                        <asp:HiddenField ID="HF_ESC_ID" runat="server" Value='<%# Eval("ESC_ID") %>' />
                                                                        <asp:HiddenField ID="HF_EMC_ID" runat="server" Value='<%# Eval("EMC_ID") %>' />
                                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("EMC_ID") %>' />
                                                                    </td>
                                                                    <td style="width: 80%; vertical-align: top;">
                                                                        <asp:Literal ID="lit_chart" runat="server"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle  />
                    <PagerStyle  HorizontalAlign="Center" />
                    <SelectedRowStyle  Font-Bold="True"  />
                    <HeaderStyle Font-Bold="True" />
                </asp:GridView>

                <center>
                    <br />
                    <asp:Button
                        ID="btn_Panel_Close" runat="server" Text="Close this Page" OnClientClick="javascript:window.close();" />

                    <br />
                    <asp:Label ID="lblMsg" runat="server" Text="" CssClass="FontBoldHeader2"></asp:Label>

                </center>
            </div>
        </form>
        <div class="footer">
            <div class="in" align="center">Copyright © 2014 GEMS Education. All Rights Reserved.</div>
        </div>
    </div>
</body>
</html>
