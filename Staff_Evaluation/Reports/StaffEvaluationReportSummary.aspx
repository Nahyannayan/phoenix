﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StaffEvaluationReportSummary.aspx.vb" Inherits="Staff_Evaluation_Reports_StaffEvaluationReportSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::GEMS OASIS:: Staff Evaluation::Detailed Report::</title>
    <link href="../cssfiles/StaffEvaluationStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../../FusionCharts/FusionCharts.js" type="text/javascript"></script>

</head>
<body>
    <div class="wrapper">
        <div class="header"></div>
        <div class="clear"></div>
        <form id="form1" runat="server">

            <div>
                <center>
                    <asp:Label ID="lblEvaluationHeading1" runat="server" Text="" CssClass="title-bg"></asp:Label><br />
                    <asp:Label ID="lblEvaluationHeading2" runat="server" Text="" CssClass="title-bg"></asp:Label>
                </center>

                <div style="float: left; width: 100%;">

                    <table id="tb_evalinfo" runat="server" width="100%">
                        <tr>
                            <td width="20%">
                                <asp:Label ID="Label1" runat="server" CssClass="field-label" Text="Staff Name"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_StaffName" runat="server" CssClass="field-value"></asp:Label></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <asp:Label ID="Label2" runat="server" CssClass="field-label" 
                                    Text="Designation"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_StaffDesg" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                            <td width="20%">
                                <asp:Label ID="Label10" runat="server" CssClass="field-label"  Width="120px"
                                    Text="Academic year"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_AcademicYear" runat="server" CssClass="field-value"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td width="20%">
                                <asp:Label ID="Label7" runat="server" CssClass="field-label"
                                    Text="Grade"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_Grade" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                            <td width="20%">
                                <asp:Label ID="Label4" runat="server" CssClass="field-label"
                                    Text="Section"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_Section" runat="server" CssClass="field-value"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td width="20%">
                                <asp:Label ID="Label6" runat="server" CssClass="field-label"
                                    Text="Subject"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_Subject" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                            <td width="20%">
                                <asp:Label ID="Label8" runat="server" CssClass="field-label"
                                    Text="Topic"></asp:Label></td>
                            <td width="30%">
                                <asp:Label ID="lbl_Topic" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                        </tr>

                    </table>


                </div>
                <div style="float: right; width: 100%;" align="right">
                    <table width="100%">
                        <tr>
                            <td width="20%">
                                <asp:Label ID="Label9" runat="server" CssClass="field-label" 
                                    Text="Evaluation Date"></asp:Label></td>
                            <td align="left" width="30%">
                                <asp:Label ID="lbl_EVL_Date" runat="server" CssClass="field-value" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>

                            <td width="20%">
                                <asp:Label ID="Label11" runat="server" CssClass="field-label"
                                    Text="Class Strength"></asp:Label></td>

                            <td align="left" width="30%">
                                <asp:Label ID="lbl_ClassStrength" runat="server" CssClass="field-value"
                                    Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                               <span class="field-label"><asp:Literal ID="ltl_observer" runat="server"></asp:Literal></span> </td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <center>
                    <%--<div style="float: left; width: 20%;" align="left" valign="bottom">
                    </div>
                    <div style="float: right; width: 80%;" align="left">
                    </div>--%>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                        CellPadding="4" >
                        <RowStyle  />
                        <FooterStyle  Font-Bold="True"  />
                        <PagerStyle  HorizontalAlign="Center" />
                        <SelectedRowStyle Font-Bold="True"  />
                        <HeaderStyle  Font-Bold="True"  />
                        <EditRowStyle />
                        <AlternatingRowStyle  />
                    </asp:GridView>
                    <%--<div style="float: left; width: 20%;" align="left" valign="bottom">
                    </div>
                    <div style="float: right; width: 80%;" align="left">
                    </div>--%>
                    <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>

                    <asp:Button ID="btn_Panel_Close" runat="server" Text="Close this Page" OnClientClick="javascript:window.close();" />
                    <br />
                    <asp:Label ID="lblMsg" runat="server" Text="" CssClass="error"></asp:Label>
                </center>
            </div>
        </form>
        <div class="footer">
            <div class="in" align="center">Copyright © 2018 GEMS Education. All Rights Reserved.</div>
        </div>
        <%--</div>--%>
        </div>
</body>
</html>
