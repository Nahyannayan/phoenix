﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConsolidatedReport.aspx.vb" Inherits="Staff_Evaluation_Reports_ConsolidatedReport" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <script src="../../FusionCharts/FusionCharts.js" type="text/javascript"></script> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
       
        <br />
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    
       
    <telerik:RadGrid ID="GV_ConsolidatedReport" runat="server" AutoGenerateColumns="False" 
                    CellSpacing="0" EnableTheming="False" GridLines="None" Skin="Office2007"  PageSize="10" 
                    Width="90%" AllowPaging="true">
                    <MasterTableView>
                        <CommandItemSettings />
                        <RowIndicatorColumn Visible="True">
                            <HeaderStyle Width="10px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True">
                            <HeaderStyle Width="10px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn  
                                HeaderText="Select" UniqueName="TemplateColumn">
                                         <HeaderTemplate>
                              
                        </HeaderTemplate>
                        <ItemTemplate>
                        <asp:Label ID="lblEVT_TEMPLATE_DESC" runat="server" Text='<%# Bind("EVT_TEMPLATE_DESC") %>' Visible="false"   /><br />
                        <asp:Label ID="LBL_EEM_REMARKS" runat="server" Text='<%# Bind("EEM_REMARKS") %>'  Visible="false"/>
                         <asp:HiddenField ID="HF_EEM_ID" runat="server" Value='<%# Bind("EEM_ID") %>' />
                         <asp:HiddenField ID="HF_STE_ID" runat="server" Value='<%# Bind("STE_ID") %>' />
                         <asp:Literal ID="Ll_FChart" runat="server"></asp:Literal>
                            
                        </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            </Columns> 
                            </MasterTableView> 
                            </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
