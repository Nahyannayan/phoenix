﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Imports InfoSoftGlobal
Partial Class Staff_Evaluation_Reports_StaffEvaluationReportSummary
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ' Generate chart in Literal Control
                    If Not Request.QueryString("STE_ID") Is Nothing Then
                        ViewState("STE_ID") = Encr_decrData.Decrypt(Request.QueryString("STE_ID").Replace(" ", "+"))
                        Load_StaffDetails(ViewState("STE_ID"))
                        CreatetableReport()
                        FCLiteral.Text = CreateChartReport(ViewState("STE_ID"))
                    Else
                        Dim sb As New System.Text.StringBuilder()
                        sb.Append("<script language='javascript'>")
                        sb.Append("window.close();")
                        sb.Append("</script>")
                        'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "JCall1", sb.ToString(), False)
                        Page.ClientScript.RegisterStartupScript(Me.[GetType](), "Script", sb.ToString(), False)
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub Load_StaffDetails(ByVal STE_ID As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 6)
            parm(1) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_DETAILS_ALL", parm)
            If (ds.Tables(0).Rows.Count > 0) Then
                ViewState("EVT_ID") = Convert.ToString(ds.Tables(0).Rows(0)("EEM_EVT_ID"))
                ViewState("EEM_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STE_EEM_ID"))

                lblEvaluationHeading1.Text = Convert.ToString(ds.Tables(0).Rows(0)("EVT_TEMPLATE_DESC"))
                lblEvaluationHeading2.Text = Convert.ToString(ds.Tables(0).Rows(0)("EEM_REMARKS"))
                lbl_StaffName.Text = Convert.ToString(ds.Tables(0).Rows(0)("EMP_NAME"))
                lbl_StaffDesg.Text = Convert.ToString(ds.Tables(0).Rows(0)("DES_DESCR"))
                lbl_AcademicYear.Text = Convert.ToString(ds.Tables(0).Rows(0)("ACY_DESCR"))
                lbl_ClassStrength.Text = Convert.ToString(ds.Tables(0).Rows(0)("ERM_CLASSSTRENGTH"))
                lbl_Grade.Text = Convert.ToString(ds.Tables(0).Rows(0)("GRM_DISPLAY"))
                lbl_Section.Text = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                'lbl_ObserverName.Text = Convert.ToString(ds.Tables(0).Rows(0)("DES_DESCR"))
                lbl_Subject.Text = Convert.ToString(ds.Tables(0).Rows(0)("SBG_DESCR"))
                lbl_Topic.Text = Convert.ToString(ds.Tables(0).Rows(0)("ERM_TOPIC"))

                If Convert.ToString(ds.Tables(0).Rows(0)("ERM_COMPLETION_DATE")) <> "" Then
                    Dim datet As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)("ERM_COMPLETION_DATE"))
                    lbl_EVL_Date.Text = datet.Date.ToString("dd/MMM/yyyy")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function CreateChartReport(ByVal STE_ID As String) As String
        Try
            Dim strXML As String
            ' strXML = "<graph caption='Academic Year Performance Chart' subCaption='Staff Evaluation' decimalPrecision='0' showNames='1' numberSuffix=' ' pieSliceDepth='30' formatNumberScale='0' xAxisName='Weekly Observations' yAxisName='Overall Marks' labelDisplay='STAGGER' rotateNames='0' labelPadding='100' >"
            strXML = "<chart caption='Academic Year Performance Chart' subCaption='Staff Evaluation' xAxisName='Weekly Observations' yAxisName='Overall Marks' palette='3' showValues='1' numberPrefix='Score-' useRoundEdges='1' labelDisplay='Wrap' showNames='1'>"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parmObservers(4) As SqlClient.SqlParameter
            parmObservers(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parmObservers(1) = New SqlClient.SqlParameter("@EOB_EEM_ID", ViewState("EEM_ID"))
            parmObservers(2) = New SqlClient.SqlParameter("@STE_ID", ViewState("STE_ID"))
            Dim dsObservers As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS", parmObservers)
            For Each drObservers As DataRow In dsObservers.Tables(0).Rows
                Dim EOB_ID As String = drObservers("EOB_ID")
                Dim parmResult(4) As SqlClient.SqlParameter
                parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 3)
                parmResult(1) = New SqlClient.SqlParameter("@EEM_ID", ViewState("EEM_ID"))
                parmResult(2) = New SqlClient.SqlParameter("@STE_ID", STE_ID)
                parmResult(3) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
                Dim dsResult As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
                For Each drResult As DataRow In dsResult.Tables(0).Rows
                    strXML += "<set label='" + drObservers("EMP_NAME").ToString() + "' value='" + drResult("SCORE").ToString() + "' />"
                Next
            Next
            strXML += "</chart>"


            'Using dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "SELECT ERS_ESK_ID,ERS_SCORE FROM EVA.EVAL_STAFF_RESULTS_M ERM WITH (NOLOCK)	INNER JOIN EVA.EVAL_STAFF_RESULTS_S ERS WITH (NOLOCK) ON ERM.ERM_ID=ERS.ERS_ERM_ID	WHERE  ERM_STE_ID=" + STE_ID + "") ', parm)
            '    While dr.Read()

            '        strXML += "<set name='" + dr("ERS_ESK_ID").ToString() + "' value='" + dr("ERS_SCORE").ToString() + "' />"
            '    End While
            'End Using
            'strXML += "</graph>"
            '  Dim xmlStr As String = "<chart caption='Top 5 Employees for 2011' palette='3' showValues='0' numberPrefix='$' useRoundEdges='1'><set label='Leverling' value='100524'/><set label='Fuller' value='87790'/><set label='Davolio' value='81898'/><set label='Peacock' value='76438'/><set label='Callahan' value='55091'/></chart>"

            ' FusionCharts.SetRenderer("javascript")

            'FCLiteral.Text = FusionCharts.RenderChart("../../FusionCharts/Column3D.swf", "", strXML, "browser_share_2", "640", "340", False, True)

            Return FusionCharts.RenderChart("../../FusionCharts/Column3D.swf", "", strXML, "browser_share_2", "640", "340", False, True)

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Dim dtDT As New DataTable
    Dim dt As New DataTable
    Dim dtCommon As New DataTable
    Private Sub CreatetableReport()
        Dim dtFocusArea As New DataTable
        dtFocusArea.Columns.Add("ESC_ID") 'ESC_ID
        dtFocusArea.Columns.Add("FocusArea") 'ESC_DESC

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parm As SqlParameter() = New SqlParameter(1) {}
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
        parm(1) = New SqlClient.SqlParameter("@ESC_EVT_ID", ViewState("EVT_ID"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_SUBCRITERIA_M", parm)
        If (ds.Tables(0).Rows.Count > 0) Then ''GET_EVAL_SUBCRITERIA_M

            For Each dr As DataRow In ds.Tables(0).Rows
                Dim drFocusArea As DataRow
                drFocusArea = dtFocusArea.NewRow
                drFocusArea("FOCUSAREA") = Convert.ToString(dr("ESC_DESC"))
                drFocusArea("ESC_ID") = Convert.ToString(dr("ESC_ID"))
                dtFocusArea.Rows.Add(drFocusArea)
            Next

            dtDT.Columns.Add("ObserversID")  'EOB_ID
            dtDT.Columns.Add("ObserversName") 'EMP_NAME

            Dim parmObservers As SqlParameter() = New SqlParameter(3) {}
            parmObservers(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
            parmObservers(1) = New SqlClient.SqlParameter("@EOB_EEM_ID", ViewState("EEM_ID"))
            parmObservers(2) = New SqlClient.SqlParameter("@STE_ID", ViewState("STE_ID"))
            Dim dsObservers As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_OBSERVERS", parmObservers)
            Dim j As Integer = 0
            If (dsObservers.Tables(0).Rows.Count > 0) Then
                For Each dr As DataRow In dsObservers.Tables(0).Rows
                    j = j + 1
                    Dim drObservers As DataRow
                    drObservers = dtDT.NewRow
                    drObservers("ObserversID") = Convert.ToString(dr("EOB_ID"))
                    drObservers("ObserversName") = "Observ" + j.ToString()  'Convert.ToString(dr("EMP_NAME"))
                    dtDT.Rows.Add(drObservers)

                    ltl_observer.Text = ltl_observer.Text + "Observ" + j.ToString() + "-->" + Convert.ToString(dr("EMP_NAME")) + "<br/>"
                Next
            End If
            ltl_observer.Text = "<table><tr><td align='left' class='FontBoldHeader2'>" + ltl_observer.Text + "</td></tr></Table>"
            dt.Columns.Add("ESC_ID")
            dt.Columns.Add("FocusArea")
            dt.Columns.Add("Average")
            For intDT As Integer = 0 To dtDT.Rows.Count - 1
                dt.Columns.Add(dtDT.Rows(intDT).Item("ObserversName"))
                dt.Columns.Add(dtDT.Rows(intDT).Item("ObserversID"))
            Next

            For intProd As Integer = 0 To dtFocusArea.Rows.Count - 1
                Dim dr As System.Data.DataRow = dt.NewRow()
                dr("ESC_ID") = dtFocusArea.Rows(intProd).Item("ESC_ID")
                dr("FocusArea") = dtFocusArea.Rows(intProd).Item("FocusArea")
                dr("Average") = 0.0
                dt.Rows.Add(dr)
            Next

            For intObservers As Integer = 0 To dt.Rows.Count - 1
                Dim ESC_ID As String = dt.Rows(intObservers).Item("ESC_ID")
                Dim averg As Decimal = 0.0
                Dim ZeroCount As Integer = 0
                For intDT As Integer = 0 To dtDT.Rows.Count - 1
                    Dim EOB_ID As String = dtDT.Rows(intDT).Item("ObserversID")
                    Dim parmResult As SqlParameter() = New SqlParameter(4) {}
                    parmResult(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 4)
                    parmResult(1) = New SqlClient.SqlParameter("@ESC_ID", ESC_ID)
                    parmResult(2) = New SqlClient.SqlParameter("@EOB_ID", EOB_ID)
                    parmResult(3) = New SqlClient.SqlParameter("@STE_ID", ViewState("STE_ID"))
                    Dim dsResult As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_STAFF_RESULTS", parmResult)
                    If (dsResult.Tables(0).Rows.Count > 0) Then
                        For Each dr As DataRow In dsResult.Tables(0).Rows
                            dt.Rows(intObservers).Item(EOB_ID) = Convert.ToString(dr("ERS_SCORE"))
                            averg = averg + Convert.ToInt16(dr("ERS_SCORE"))
                        Next
                    Else
                        dt.Rows(intObservers).Item(EOB_ID) = 0
                        ZeroCount = ZeroCount + 1
                    End If
                    'Convert.ToString((IIf(Convert.ToString(dt.Rows(intObservers)("Average")) = "", 0, Convert.ToInt16(dt.Rows(intObservers)("Average"))) + Convert.ToInt16(dr("ERS_SCORE"))) / j)
                Next

                Dim avg As Decimal = 0.0
                Try
                    avg = (averg / (j - ZeroCount))
                Catch ex As Exception

                End Try
                dt.Rows(intObservers)("Average") = Convert.ToString(Math.Round(avg, 2))
            Next


            Dim i As Integer = 0
            For col As Integer = 0 To dt.Columns.Count - 1
                i = i + 1
                If (i < 3 And i <> 1) Then
                    'Declare the bound field and allocate memory for the bound field.
                    Dim bfield As New BoundField()
                    'Initalize the DataField value.
                    bfield.DataField = Convert.ToString(dt.Columns(col))
                    'Initialize the HeaderText field value.
                    bfield.HeaderText = Convert.ToString(dt.Columns(col))
                    'Add the newly created bound field to the GridView.
                    GridView1.Columns.Add(bfield)
                Else
                    If (i < dt.Columns.Count) Then
                        If ((i Mod 2) = 0 And Convert.ToString(dt.Columns(col)) <> "Average") Then
                            'Declare the bound field and allocate memory for the bound field.
                            Dim bfield As New BoundField()
                            'Initalize the DataField value.
                            bfield.DataField = Convert.ToString(dt.Columns(col + 1))
                            'Initialize the HeaderText field value.
                            bfield.HeaderText = Convert.ToString(dt.Columns(col))
                            'Add the newly created bound field to the GridView.
                            GridView1.Columns.Add(bfield)
                        End If
                        If (Convert.ToString(dt.Columns(col)) = "Average") Then
                            Dim bfield As New BoundField()
                            bfield.DataField = Convert.ToString(dt.Columns(col))
                            bfield.HeaderText = Convert.ToString(dt.Columns(col))
                            GridView1.Columns.Add(bfield)
                        End If
                    End If
                End If
            Next
            'For Each col As DataColumn In dt.Columns
            '    i = i + 1
            '    If (i < 2) Then
            '        'Declare the bound field and allocate memory for the bound field.
            '        Dim bfield As New BoundField()
            '        'Initalize the DataField value.
            '        bfield.DataField = col.ColumnName
            '        'Initialize the HeaderText field value.
            '        bfield.HeaderText = col.ColumnName
            '        'Add the newly created bound field to the GridView.
            '        GridView1.Columns.Add(bfield)
            '    Else
            '        'Declare the bound field and allocate memory for the bound field.
            '        Dim bfield As New BoundField()
            '        'Initalize the DataField value.
            '        bfield.DataField = col.ColumnName
            '        'Initialize the HeaderText field value.
            '        bfield.HeaderText =  dt.Columns(col
            '        'Add the newly created bound field to the GridView.
            '        GridView1.Columns.Add(bfield)
            '    End If

            'Next
            GridView1.DataSource = dt
            GridView1.DataBind()
        End If ''GET_EVAL_SUBCRITERIA_M
    End Sub



    Private Function sum(ByVal a As Integer, ByVal b As Integer) As Integer
        Return a + b
    End Function
End Class
