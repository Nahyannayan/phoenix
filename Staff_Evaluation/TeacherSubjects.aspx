﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TeacherSubjects.aspx.vb" Inherits="Staff_Evaluation_TeacherSubjects" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
        var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';

        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);

            var btnadd = $('#<%= btnadd.ClientID %>');
        btnadd.attr('disabled', true);

        if (checkedCheckboxes.length == 0) {
            btnadd.attr('disabled', true);
        }
        else {
            btnadd.removeAttr('disabled');
        }

        $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
    }

    $(document).ready(function () {

        $(allCheckBoxSelector).live('click', function () {
            $(checkBoxSelector).attr('checked', $(this).is(':checked'));
            ToggleCheckUncheckAllOptionAsNeeded();
        });
        $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
        ToggleCheckUncheckAllOptionAsNeeded();
    });

    function GetEMPName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = radopen("../Accounts/accShowEmpDetail.aspx?id=ERP", "pop_up")
       <%-- if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
            //
            return true;
        }
        return false;--%>
    }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
              document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
              document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txt_EMPReportingTo.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>
    <style>
        /*.col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }*/

        .darkPanlAlumini {
            width: 90%;
            height: 90%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 10%;
            top: 10%;
            position: fixed;
            width: 70%;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
        .RadComboBox_Default .rcbReadOnly .rcbArrowCellRight {
            /* background-position: 3px -10px; */
            border: solid black;
            border-width: 0 1px 1px 0;
            display: inline-block;
            padding: 0px;
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            width: 7px;
            height: 7px;
            overflow: hidden;
            margin-top: 15px;
            margin-left: -15px;
        }
        .RadComboBox .rcbArrowCell a {
    width: 18px;
    height: 22px;
    position: relative;
    outline: 0;
    font-size: 0;
    line-height: 1px;
    text-decoration: none;
    text-indent: 9999px;
    display: block;
    overflow: hidden;
    cursor: default;
}
        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image:none !important;
        }
        .RadComboBox_Default {
            width: 100% !important;
        }
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image:url(/images/calender.gif)!important;
            width: 30px;
            height: 30px;
        }
        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active{
            background-image:url(/images/calender.gif)!important;
            width: 30px;
            height: 30px;
            background-position: 0;
        }

        .RadPicker { width:80% !important;
        }
        table.RadCalendar_Default {
            background:#ffffff !important;
        }

         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }
    </style>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Teacher Subjects
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table style="width: 100%">
                    <%--<tr class="subheader_img">
            <td>
                 
                 </td>
        </tr>--%>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtn_addSubjects" runat="server">Add</asp:LinkButton>
                        </td>
                    </tr>
                </table>

                <table  style="width: 100%">
                    <tr >
                        <td ><span class="field-label">Staff name  </span></td>
                        
                        <td>
                            <asp:TextBox ID="txt_staffname_ess" runat="server"></asp:TextBox>
                        </td>
                        <td ><span class="field-label">Subject</span></td>
                       
                        <td>
                            <asp:TextBox ID="txt_subject_ess" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btn_Searchstaffsub" runat="server" Text="Search" CssClass="button" />
                            <asp:Button ID="btn_ClearSearchstaffsub" runat="server" Text="Clear" CssClass="button" />
                            <br />
                            <asp:Label ID="lbldeletemsg" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <telerik:RadGrid ID="gv_StaffSubjects" runat="server" AllowPaging="true" PageSize="25" AllowSorting="true" AllowFilteringByColumn="true"
                                AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                                GridLines="None"  ShowGroupPanel="True">
                                <ClientSettings AllowDragToGroup="True">
                                </ClientSettings>
                                <MasterTableView>
                                    <CommandItemSettings />
                                    <RowIndicatorColumn Visible="True">
                                        <HeaderStyle  />
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="True">
                                        <HeaderStyle  />
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="BSU_SHORTNAME" AllowFiltering="false"
                                            HeaderText="Business unit"
                                            UniqueName="BSU_NAME">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ACY_DESCR"
                                            HeaderText="Academic year"
                                            UniqueName="ACY_DESCR">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="GRM_DISPLAY"
                                            HeaderText="Grade"
                                            UniqueName="GRM_DISPLAY">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EMP_NAME"
                                            HeaderText="Staff name"
                                            UniqueName="EMP_NAME">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SBG_DESCR"
                                            HeaderText="Subject"
                                            UniqueName="SBG_DESCR">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ESS_CREATED_DATE"
                                            DataFormatString="{0:dd/MMM/yyyy hh:mm tt}" HeaderText="Created Date"
                                            UniqueName="column">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Edit" Visible="false" Groupable="false"
                                            UniqueName="TemplateColumn_VERIFIED">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_editStaffSubj" runat="server" OnClick="lnk_editStaffSubj_Click"
                                                    ToolTip="Click here to edit the template">Edit</asp:LinkButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Delete" Groupable="false" AllowFiltering="false"
                                            UniqueName="Template_Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_deleteStaffSubj" runat="server" OnClick="lnk_deleteStaffSubj_Click">Delete</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_deleteStaff" ConfirmText="Confirm delete"
                                                    TargetControlID="lnk_deleteStaffSubj" runat="server">
                                                </ajaxToolkit:ConfirmButtonExtender>
                                                <asp:HiddenField ID="HF_ESS_ID" runat="server" Value='<%# Bind("ESS_ID") %>' />
                                                <asp:HiddenField ID="hf_BSU_IDStaffSubj" runat="server"
                                                    Value='<%# Bind("BSU_ID") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn>
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                <ItemStyle  HorizontalAlign="Left" />
                                <AlternatingItemStyle  HorizontalAlign="Left" />
                                <FilterMenu>
                                </FilterMenu>
                                <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                            </telerik:RadGrid>

                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnl_AddStaffSubject" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini" >
                        <div >
                            <div style="float: right">
                                <asp:ImageButton ImageUrl="~/Images/close.png" ID="lnkbtn_AddStaffSubject_Close" ToolTip="click here to close"
                                    runat="server"  CausesValidation="False" OnClick="lnkbtn_AddStaffSubject_Close_Click"></asp:ImageButton>
                            </div>
                            <div class="title-bg">
                                Add teacher subjects
                            </div>
                            
                        </div>
                        <table  style="width: 100%">
                            <tr >
                                <td align="left" width="20%"><span class="field-label">Business unit</span><span class="text-danger">*</span></td>
                               
                                <td align="left" width="30%">
                                    <telerik:RadComboBox ID="ddl_bsu" runat="server" Width="100%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Academic year</span><span class="text-danger">*</span></td>
                                
                                <td align="left" width="30%">
                                    <telerik:RadComboBox ID="ddl_acdYear" runat="server"  Width="100%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr >
                                <td><span class="field-label">Grade</span><span class="text-danger">*</span></td>
                                
                                <td>
                                    <telerik:RadComboBox ID="ddl_grade" runat="server"  Width="100%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>
                                </td>
                                <td ><span class="field-label">Subject</span><span class="text-danger">*</span></td>
                               
                                <td>
                                    <telerik:RadComboBox ID="ddl_subject" runat="server"  Width="100%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>

                            <tr >
                                <td ><span class="field-label">Staffs</span></td>
                               
                                <td colspan="4">
                                    <asp:LinkButton ID="lnk_addStaff" runat="server" Text="Add Staff"></asp:LinkButton>
                                    <div style="overflow: auto;">
                                        <telerik:RadGrid ID="gv_staffEvaluated" runat="server"
                                            AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                                            GridLines="None" Width="100%">
                                            <MasterTableView>
                                                <CommandItemSettings />
                                                <RowIndicatorColumn Visible="True">
                                                    <HeaderStyle  />
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn Visible="True">
                                                    <HeaderStyle />
                                                </ExpandCollapseColumn>
                                                <Columns>
                                                    <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Staff Name" UniqueName="TemplateColumn_staffName">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                            <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Designation"
                                                        UniqueName="TemplateColumn_desg">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                            <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                            <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Delete"
                                                        UniqueName="Template_Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnk_deleteStaff" runat="server" OnClick="lnk_deleteStaff_Click">Delete</asp:LinkButton>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_deleteStaff" ConfirmText="Confirm delete"
                                                                TargetControlID="lnk_deleteStaff" runat="server">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn>
                                                    </EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                            <ItemStyle  HorizontalAlign="Left" />
                                            <AlternatingItemStyle  HorizontalAlign="Left" />
                                            <FilterMenu>
                                            </FilterMenu>
                                        </telerik:RadGrid>
                                    </div>
                                </td>
                            </tr>

                            <tr >
                                <td colspan="4" align="center">
                                    <asp:Button ID="btn_Save" runat="server" Text="Save" CssClass="button" />
                                    <asp:Button ID="btn_Clear" runat="server" Text="Cancel" CssClass="button" />
                                    <br />
                                    <asp:Label ID="lbl_Savemsg" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                                </td>
                            </tr>

                        </table>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnl_SelectStaff" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini" >
                        <div >
                            <div style="float: right">
                                <asp:ImageButton ID="lnkClose_Staff" ToolTip="click here to close"
                                    runat="server" ImageUrl="~/Images/close.png" CausesValidation="False"></asp:ImageButton>
                            </div>
                            
                            <div class="title-bg">
                                Select Staff
                            </div>
                            
                        </div>
                        <table style="width: 100%;">
                            <tr  >
                                <td align="left" width="20%"><span class="field-label">Business Unit</span><span class="text-danger">*</span></td>
                              
                                <td align="left" width="30%">
                                    <telerik:RadComboBox ID="ddl_bsu_pnl" runat="server"  Width="100%"
                                        AutoPostBack="True">
                                    </telerik:RadComboBox>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddl_bsu_pnl"
                                        ErrorMessage="Select businessunit" InitialValue="" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                                </td>
                                <td width="20%"></td>
                                <td width="30%"></td>
                            </tr>

                            <tr  >
                                <td align="left" width="20%"><span class="field-label">Name</span></td>
                                
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txt_Name" runat="server" ></asp:TextBox>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Designation</span></td>
                                
                                <td align="left"width="30%">
                                    <asp:TextBox ID="txt_desg" runat="server" ></asp:TextBox>

                                </td>
                            </tr>

                            <tr  >
                                <td align="left"><span class="field-label">Reporting To</span></td>
                               
                                <td align="left" colspan="2">
                                    <asp:TextBox ID="txt_EMPReportingTo" runat="server" Width="90%"></asp:TextBox>
                                    <asp:ImageButton ID="btn_pickreportingstaff" runat="server"
                                        CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();return false;"
                                         Text="Search" ToolTip="Search" />
                                    <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />
                                    
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btn_Search" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Search" ToolTip="Search" />
                                    <asp:Button ID="btn_reset" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                                </td>
                            </tr>

                            <tr  >

                                <td align="left" colspan="4">
                                    <div style="overflow: auto; height: 350px;">
                                        <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False"
                                            CellSpacing="0" EnableTheming="False" GridLines="None" PageSize="10"
                                            Width="90%" AllowPaging="true">
                                            <MasterTableView>
                                                <CommandItemSettings />
                                                <RowIndicatorColumn Visible="True">
                                                    <HeaderStyle  />
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn Visible="True">
                                                    <HeaderStyle  />
                                                </ExpandCollapseColumn>
                                                <Columns>
                                                    <telerik:GridTemplateColumn
                                                        HeaderText="Select" UniqueName="TemplateColumn">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox runat="server" ID="chkAll" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="BSU"
                                                        UniqueName="TemplateColumn_BSU">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Staff Name"
                                                        UniqueName="TemplateColumn_staffName">

                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                            <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Designation"
                                                        UniqueName="TemplateColumn_desg">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                            <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                            <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                        Visible="False">
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn>
                                                    </EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                            <ItemStyle  HorizontalAlign="Left" />
                                            <AlternatingItemStyle  HorizontalAlign="Left" />
                                            <FilterMenu>
                                            </FilterMenu>
                                        </telerik:RadGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr  align="center" >
                                <td colspan="4">
                                    <asp:Button ID="btnadd" runat="server" CssClass="button" Text="Add" />

                                    <asp:Button ID="btn_cancel" runat="server" CssClass="button" Text="Cancel" />

                                    <br />
                                    <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                                </td>
                            </tr>

                        </table>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>

