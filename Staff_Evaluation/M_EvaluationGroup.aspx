﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="M_EvaluationGroup.aspx.vb" Inherits="Staff_Evaluation_Staff_Evaluation_M_EvaluationGroup" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

 <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var allCheckBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="chkAll"]:checkbox';
        var checkBoxSelector = '#<%=gv_Staff.ClientID%> input[id*="cbSelect"]:checkbox';

        function ToggleCheckUncheckAllOptionAsNeeded() {
            var totalCheckboxes = $(checkBoxSelector),
             checkedCheckboxes = totalCheckboxes.filter(":checked"),
             noCheckboxesAreChecked = (checkedCheckboxes.length === 0),
             allCheckboxesAreChecked = (totalCheckboxes.length === checkedCheckboxes.length);

            var btnadd = $('#<%= btnadd.ClientID %>');
        btnadd.attr('disabled', true);

        if (checkedCheckboxes.length == 0) {
            btnadd.attr('disabled', true);
        }
        else {
            btnadd.removeAttr('disabled');
        }

        $(allCheckBoxSelector).attr('checked', allCheckboxesAreChecked);
    }

    $(document).ready(function () {

        $(allCheckBoxSelector).live('click', function () {
            $(checkBoxSelector).attr('checked', $(this).is(':checked'));
            ToggleCheckUncheckAllOptionAsNeeded();
        });
        $(checkBoxSelector).live('click', ToggleCheckUncheckAllOptionAsNeeded);
        ToggleCheckUncheckAllOptionAsNeeded();
    });

    function GetEMPName() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=ERP", "", sFeatures)
        if (result != '' && result != undefined) {
            NameandCode = result.split('___');
            document.getElementById('<%=HF_EMPReportingTo.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=txt_EMPReportingTo.ClientID %>').value = NameandCode[0];
            //
            return true;
        }
        return false;
    }
</script>
    <style type="text/css">
         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }
    </style>

    <table class="BlueTable" style="width:90%">
       
        <tr class="subheader_img">
            <td colspan="3">
                Evaluator Group Master</td>
        </tr>

        <tr class="matters" align="left">
            <td style="width:140px" >Business unit<span style="color:Red">*</span></td>
            <td>:</td>
            <td><telerik:RadComboBox ID="ddl_bsu" runat="server" Skin="Simple" width="50%" 
                    AutoPostBack="True">
             </telerik:RadComboBox></td>
        </tr>

        <tr class="matters" align="left">
        <td style="width:140px;">Group Name<span style="color:Red">*</span></td>
        <td style="width:1px" >:</td>
        <td>
            <asp:TextBox ID="txt_remarks" runat="server"  Width="90%"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_remarks"
                    ErrorMessage="&lt;br /&gt; &lt;b&gt;Group Description required&lt;/b&gt;" ValidationGroup="Save"  Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>

           <tr class="matters" align="left">
        <td style="width:140px;">Group Type<span style="color:Red">*</span></td>
        <td style="width:1px" >:</td>
        <td>
            <asp:RadioButtonList ID="rblGroupType" runat="server" AutoPostBack="true" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="1">Observer</asp:ListItem>
                    <asp:ListItem Value="2">Staff</asp:ListItem>
                    
                </asp:RadioButtonList>
            </td>
        </tr>

        <tr class="matters" align="left">
        <td style="width:140px" >Employee List<span style="color:Red">*</span></td>
        <td style="width:1px"  >:</td>
        <td>
            <asp:LinkButton ID="lnk_addEvaluator" runat="server" Text="Add Staff" ></asp:LinkButton>
            <telerik:RadGrid ID="gv_observer" runat="server" 
                AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False" 
                GridLines="None" Skin="Office2007" Width="90%" >
                <MasterTableView>
                    <CommandItemSettings />
                    <RowIndicatorColumn Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                            <ItemTemplate>
                                <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />                                
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Staff Name" UniqueName="TemplateColumn_staffName">
                            <ItemTemplate>
                                <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Designation" 
                            UniqueName="TemplateColumn_desg">
                            <ItemTemplate>
                                <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Delete" 
                            UniqueName="Template_Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_deleteEvaluator" runat="server" onclick="lnk_deleteEvaluator_Click">Delete</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_deleteEvaluator" ConfirmText="Confirm delete"
                                TargetControlID="lnk_deleteEvaluator" runat="server">
                                 </ajaxToolkit:ConfirmButtonExtender>  
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn>
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                <ItemStyle  HorizontalAlign="Left" />
                <AlternatingItemStyle  HorizontalAlign="Left" />
                <FilterMenu>
                </FilterMenu>
            </telerik:RadGrid>
             
            </td>
        </tr>
       
        
        
        
        <tr class="matters">      
            <td colspan="3" align="center">
            <asp:Button ID="btn_Save" runat="server" Text="Save" ValidationGroup="Save" CssClass="button" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button"/>
            <br /> 
             <asp:Label ID="lblerror" runat="server" CssClass="error" style="vertical-align: middle"  EnableViewState="False"></asp:Label>
          
            </td>
        </tr>
        
    </table>
    
    <asp:Panel ID="pnl_SelectStaff" runat="server" CssClass="darkPanlAlumini" Visible="false" >
        <div class="panelQual" style="height:800px; top:35%; "  ><%--style="overflow:auto;"--%>
            <div style="color: #084777; font-size: 11px; font-weight: bold; display: block; font-family: Verdana, Arial, Helvetica, sans-serif;
                font-weight: bold; color: #1B80B6; overflow: hidden; padding: 5px; border: 1px solid #b5cae7;
                background: url('~/Images/mainHeahingrtbg.png') no-repeat right 0 #e6effc !important;
                margin: 5px 0;">
                <div style="float: left; width: 97%;">
                    Select Staff</div>
                <div style="float: right">
                    <asp:LinkButton ForeColor="Red" ID="lnkClose_Staff" ToolTip="click here to close"
                        runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClose_Staff_Click"></asp:LinkButton>
                </div>              
            </div>
              <table style="width:100%;" >
        <tr class="matters" style="color:Black" >
            <td align="left" style="width: 100px" >
                Business Unit<span style="color: red">*</span></td>
            <td width="1px">
                :</td>             
            <td  align="left" colspan="4" >
             <telerik:RadComboBox ID="ddl_bsu_pnl" runat="server" Skin="Simple" width="90%" 
                    AutoPostBack="True">
             </telerik:RadComboBox>

             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddl_bsu_pnl"
                                                ErrorMessage="Select businessunit" InitialValue="" ValidationGroup="Save">*</asp:RequiredFieldValidator>
            </td> 
             
        </tr>
        
        <tr class="matters" style="color:Black" >
            <td align="left" style="width: 100px" >
                Name</td>
            <td width="1px">
                :</td>             
            <td  align="left">
                <asp:TextBox ID="txt_Name" runat="server"  Width="120px"></asp:TextBox>
            </td> 
              <td align="left" style="width: 100px" >
                Designation</td>
            <td width="1px">
                :</td>             
            <td  align="left">
                  <asp:TextBox ID="txt_desg" runat="server" Width="120px" ></asp:TextBox>
                
            </td> 
        </tr>
        
                  <tr class="matters" style="color:Black">
                      <td align="left" style="width: 100px">
                          Reporting To</td>
                      <td width="1px">
                          :</td>
                      <td align="left" colspan="3">
                          <asp:TextBox ID="txt_EMPReportingTo" runat="server" Width="90%"></asp:TextBox>
                          <asp:ImageButton ID="btn_pickreportingstaff" runat="server" 
                              CausesValidation="False" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName();"
                              style="width: 16px" Text="Search" ToolTip="Search" />
                          <asp:HiddenField ID="hf_EMPReportingTo" runat="server" />
                          &nbsp;<%--<asp:ImageButton ID="btn_Search" runat="server" CausesValidation="False" 
                              ImageUrl="~/Images/forum_search.gif" style="width: 16px" Text="Search" 
                              ToolTip="Search" />--%></td>
                      <td align="left">
                          <asp:Button ID="btn_Search" runat="server" CausesValidation="False" 
                              CssClass="button" Text="Search" ToolTip="Search" />
                          <asp:Button ID="btn_reset" runat="server" CausesValidation="False" 
                              CssClass="button" Text="Clear" ToolTip="Clear Filters" />
                      </td>
                  </tr>
        
        <tr class="matters" style="color:Black" >
           
            <td  align="left" colspan="6" >
            <div style="overflow:auto;height:350px;"><%----%>
                <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False" 
                    CellSpacing="0" EnableTheming="False" GridLines="None" Skin="Office2007"  PageSize="10" 
                    Width="90%" AllowPaging="true">
                    <MasterTableView>
                        <CommandItemSettings />
                        <RowIndicatorColumn Visible="True">
                            <HeaderStyle Width="10px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True">
                            <HeaderStyle Width="10px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn  
                                HeaderText="Select" UniqueName="TemplateColumn">
                                         <HeaderTemplate>
                              <asp:CheckBox runat="server" ID="chkAll" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="cbSelect" runat="server" />
                        </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="BSU" 
                                UniqueName="TemplateColumn_BSU">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Staff Name" 
                                UniqueName="TemplateColumn_staffName">
                             
                                <ItemTemplate>
                                    <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                    <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Designation" 
                                UniqueName="TemplateColumn_desg">                              
                                <ItemTemplate>                                  
                                    <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                    <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                    <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select" 
                                Visible="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk_Select_pnl" runat="server" >Select</asp:LinkButton>                                    
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn>
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                    <ItemStyle  HorizontalAlign="Left" />
                    <AlternatingItemStyle  HorizontalAlign="Left" />
                    <FilterMenu>
                    </FilterMenu>
                </telerik:RadGrid>
            </div>
            </td> 
        </tr>
        <tr class="matters" align="center"  style="color:Black">
           <td colspan="6">
           <asp:Button ID="btnadd" runat="server" CssClass="button" Text="Add"/>
                
                <asp:Button ID="btn_cancel" runat="server" CssClass="button" Text="Cancel"/>
                
                <br />
                <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" style="vertical-align: middle"  EnableViewState="False"></asp:Label>
            </td>
        </tr>    
        
        </table>
        </div>       
    </asp:Panel>
</asp:Content>

