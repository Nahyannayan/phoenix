﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports Telerik.Web.UI
Partial Class Staff_Evaluation_Staff_Evaluation_M_EvaluationGroup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Session("MainMnu_code_pro") = ViewState("MainMnu_code")

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000119") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    'If Not Request.QueryString("GTH_ID") Is Nothing Then
                    '    ViewState("GTH_ID") = Encr_decrData.Decrypt(Request.QueryString("GTH_ID").Replace(" ", "+"))
                    'Else
                    '    ViewState("GTH_ID") = 0
                    'End If


                    If Request.QueryString("viewid") Is Nothing Then
                        ViewState("GTH_ID") = "0"
                    Else
                        ViewState("GTH_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    End If
                    BindBusinessUnit()
                    Bind_EVALUATOR_MAP()
                    Bind_gv_observer()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else

        End If
        ViewState("slno") = 0
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu.DataSource = ds.Tables(0)
        ddl_bsu.DataValueField = "BSU_ID"
        ddl_bsu.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu.DataBind()
        ddl_bsu.SelectedIndex = -1
        If Not ddl_bsu.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu.Items.FindItemByValue(Session("sBsuid")).Selected = True
            Bind_EvalTemplate()
        End If
    End Sub
    Protected Sub ddl_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu.SelectedIndexChanged
        Bind_EvalTemplate()
    End Sub

    Sub Bind_EvalTemplate()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim parm(4) As SqlClient.SqlParameter
        'parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 2)
        'parm(1) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        'parm(2) = New SqlClient.SqlParameter("@EVT_BSU_ID", ddl_bsu.SelectedValue)
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVALUATION_TEMPLATE_M", parm)
        'ddl_EvalTemplate.DataSource = ds
        'ddl_EvalTemplate.DataValueField = "EVT_ID"
        'ddl_EvalTemplate.DataTextField = "EVT_TEMPLATE_DESC"
        'ddl_EvalTemplate.DataBind()
        'ddl_EvalTemplate.Items.Insert(0, New RadComboBoxItem("Select", "0"))
    End Sub

    Sub Bind_EVALUATOR_MAP()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(1) As SqlClient.SqlParameter
            Dim ds As DataSet
            parm(1) = New SqlClient.SqlParameter("@GTH_ID", ViewState("GTH_ID"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[EVA].[GET_EVAL_GROUPTYPE_H]", parm)
            If (ds.Tables(0).Rows.Count > 0) Then

                ddl_bsu.SelectedValue = Convert.ToString(ds.Tables(0).Rows(0)("GTH_BSU_ID"))
                txt_remarks.Text = Convert.ToString(ds.Tables(0).Rows(0)("GTH_DESCR"))
                If Convert.ToString(ds.Tables(0).Rows(0)("GTH_TYPE")) = "1" Then
                    rblGroupType.SelectedValue = 1
                Else
                    rblGroupType.SelectedValue = 2
                End If

                Dim parm_OBSERVER(1) As SqlClient.SqlParameter
                ds = New DataSet
                parm_OBSERVER(1) = New SqlClient.SqlParameter("@GTH_ID", ViewState("GTH_ID"))
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EVAL_GROUPTYPE_D", parm_OBSERVER)
                For Each dr_Evaluator As DataRow In ds.Tables(0).Rows
                    Dim dr As DataRow = DT_Evaluator.NewRow()
                    dr("BSU_ID") = Convert.ToString(dr_Evaluator("BSU_ID"))
                    dr("BSU_NAME") = Convert.ToString(dr_Evaluator("BSU_NAME"))
                    dr("EMP_NAME") = Convert.ToString(dr_Evaluator("EMP_NAME"))
                    dr("EMP_ID") = Convert.ToString(dr_Evaluator("EMP_ID"))
                    dr("DES_ID") = Convert.ToString(dr_Evaluator("DES_ID"))
                    dr("DES_DESCR") = Convert.ToString(dr_Evaluator("DES_DESCR"))
                    dr("STATUS") = "EDIT"
                    dr("GTD_ID") = Convert.ToString(dr_Evaluator("GTD_ID"))
                    DT_Evaluator.Rows.Add(dr)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub Bind_gv_observer()
        Dim find As String = "STATUS <> 'DELETED'"
        gv_observer.DataSource = DT_Evaluator.Select(find)
        gv_observer.DataBind()
    End Sub
    Protected Sub lnk_deleteEvaluator_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_EMP_ID As HiddenField = TryCast(sender.parent.FindControl("HF_EMP_ID"), HiddenField)
        Dim find As String = "EMP_ID = '" + HF_EMP_ID.Value + "'"
        Dim dr As DataRow() = DT_Evaluator.Select(find)
        dr(0)("STATUS") = "DELETED"
        Bind_gv_observer()

    End Sub
    Protected Sub lnk_addEvaluator_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_addEvaluator.Click
        ViewState("Add_Option") = 1
        BindBusinessUnit_Pnl()
        txt_Name.Text = ""
        txt_desg.Text = ""
        ddl_bsu_pnl.Enabled = True
        clear()
        BindGrid_Staff_Pnl()
        pnl_SelectStaff.Visible = True
    End Sub
    Protected Sub lnkClose_Staff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClose_Staff.Click
        pnl_SelectStaff.Visible = False
        ViewState("Add_Option") = 0
    End Sub
    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        pnl_SelectStaff.Visible = False
        ViewState("Add_Option") = 0
    End Sub
    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(constring)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        Try
            If (gv_observer.Items.Count > 0) Then
                Dim param(5) As SqlClient.SqlParameter
                param(1) = Mainclass.CreateSqlParameter("@GTH_ID", ViewState("GTH_ID"), SqlDbType.Int, True)
                param(2) = Mainclass.CreateSqlParameter("@GTH_DESCR", txt_remarks.Text, SqlDbType.VarChar)
                param(3) = Mainclass.CreateSqlParameter("@GTH_TYPE", rblGroupType.SelectedValue, SqlDbType.Int)
                param(4) = Mainclass.CreateSqlParameter("@GTH_BSU_ID", ddl_bsu.SelectedValue, SqlDbType.VarChar)
                param(5) = Mainclass.CreateSqlParameter("@GTH_USER", Session("sUsr_name"), SqlDbType.VarChar)
                Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "EVA.SAVE_EVAL_GROUPTYPE_H", param)
                If (RetVal = "0") Then
                    ViewState("GTH_ID") = param(1).Value
                    Dim param1(4) As SqlClient.SqlParameter
                    For Each dr_Evaluator As DataRow In DT_Evaluator.Rows
                        Try
                            param1(1) = Mainclass.CreateSqlParameter("@GTD_ID", IIf(dr_Evaluator("STATUS") = "ADD" Or dr_Evaluator("STATUS") = "EDIT", 0, 1), SqlDbType.Int, True)
                            param1(2) = Mainclass.CreateSqlParameter("@GTD_GTH_ID", ViewState("GTH_ID"), SqlDbType.Int)
                            param1(3) = Mainclass.CreateSqlParameter("@STATUS", dr_Evaluator("STATUS"), SqlDbType.VarChar)
                            param1(4) = Mainclass.CreateSqlParameter("@GTD_EMP_ID", dr_Evaluator("EMP_ID"), SqlDbType.Int)
                            Dim RetVal1 As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "EVA.SAVE_EVAL_GROUPTYPE_D", param1)
                        Catch ex As Exception
                            lblerror.Text = ex.Message
                            stTrans.Rollback()
                        End Try
                    Next
                    stTrans.Commit()
                    lblerror.Text = "Saved Sucessfully."
                Else
                    lblerror.Text = param(1).Value
                    stTrans.Rollback()
                End If
            Else
                lblerror.Text = "Please enter values in all the required fields"
            End If
        Catch ex As Exception
            lblerror.Text = ex.Message
            stTrans.Rollback()
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

#Region "Panel_staff"
    Sub BindBusinessUnit_Pnl()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAMEwithshort from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddl_bsu_pnl.DataSource = ds.Tables(0)
        ddl_bsu_pnl.DataValueField = "BSU_ID"
        ddl_bsu_pnl.DataTextField = "BSU_NAMEwithshort"
        ddl_bsu_pnl.DataBind()
        ddl_bsu_pnl.SelectedIndex = -1
        If Not ddl_bsu_pnl.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddl_bsu_pnl.Items.FindItemByValue(Session("sBsuid")).Selected = True
        End If
    End Sub
    Sub BindGrid_Staff_Pnl()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim parm(4) As SqlClient.SqlParameter
            parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", 1)
            parm(1) = New SqlClient.SqlParameter("@BSU_ID", ddl_bsu_pnl.SelectedValue)
            parm(2) = New SqlClient.SqlParameter("@EMP_NAME", IIf(txt_Name.Text.Trim() = "", DBNull.Value, txt_Name.Text.Trim()))
            parm(3) = New SqlClient.SqlParameter("@EMP_DES", IIf(txt_desg.Text.Trim() = "", DBNull.Value, txt_desg.Text.Trim()))
            parm(4) = New SqlClient.SqlParameter("@EMP_REPORTTO", IIf(hf_EMPReportingTo.Value.Trim() = "", DBNull.Value, hf_EMPReportingTo.Value.Trim()))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "EVA.GET_EMPDETAILS", parm)
            gv_Staff.DataSource = ds
            gv_Staff.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddl_bsu_pnl_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_bsu_pnl.SelectedIndexChanged
        BindGrid_Staff_Pnl()
    End Sub
    Protected Sub gv_Staff_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gv_Staff.PageIndexChanged
        gv_Staff.CurrentPageIndex = e.NewPageIndex
        gv_Staff.DataBind()
    End Sub
    Protected Sub gv_Staff_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gv_Staff.NeedDataSource
        BindGrid_Staff_Pnl()
    End Sub
    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        Try
            Dim Add_Option As Integer = Convert.ToInt16(ViewState("Add_Option"))
            For Each gr As GridDataItem In gv_Staff.Items
                Dim cbSelect As CheckBox = gr.FindControl("cbSelect")
                If (cbSelect.Checked = True) Then

                    Dim lbl_EMPNAME As Label = TryCast(gr.FindControl("lbl_EMPNAME"), Label)
                    Dim HF_EMP_ID As HiddenField = TryCast(gr.FindControl("HF_EMP_ID"), HiddenField)
                    Dim HF_BSU_ID As HiddenField = TryCast(gr.FindControl("hf_BSU_ID"), HiddenField)
                    Dim lbl_bsu As Label = TryCast(gr.FindControl("lbl_bsu"), Label)
                    Dim HF_DES_ID As HiddenField = TryCast(gr.FindControl("HF_DES_ID"), HiddenField)
                    Dim lblDES_DESCR As Label = TryCast(gr.FindControl("lblDES_DESCR"), Label)
                    Dim find As String = "EMP_ID = '" + HF_EMP_ID.Value + "'"
                    If (Add_Option = 1) Then     'Adding Evaluator
                        Dim foundRows As DataRow() = DT_Evaluator.Select(find)
                        If foundRows.Length = 0 Then
                            Dim dr As DataRow = DT_Evaluator.NewRow()
                            dr("BSU_ID") = HF_BSU_ID.Value
                            dr("BSU_NAME") = lbl_bsu.Text
                            dr("EMP_NAME") = lbl_EMPNAME.Text
                            dr("EMP_ID") = HF_EMP_ID.Value
                            dr("DES_ID") = HF_DES_ID.Value
                            dr("DES_DESCR") = lblDES_DESCR.Text
                            dr("STATUS") = "ADD"
                            dr("GTD_ID") = 0
                            DT_Evaluator.Rows.Add(dr)
                        End If
                    ElseIf (Add_Option = 2) Then 'Adding Staff to be Evaluated
                        Dim foundRows As DataRow() = DT_StaffEvaluated.Select(find)
                        If foundRows.Length = 0 Then
                            Dim dr As DataRow = DT_StaffEvaluated.NewRow()
                            dr("BSU_ID") = HF_BSU_ID.Value
                            dr("BSU_NAME") = lbl_bsu.Text
                            dr("EMP_NAME") = lbl_EMPNAME.Text
                            dr("EMP_ID") = HF_EMP_ID.Value
                            dr("DES_ID") = HF_DES_ID.Value
                            dr("DES_DESCR") = lblDES_DESCR.Text
                            dr("STATUS") = "ADD"
                            dr("STE_ID") = 0
                            DT_StaffEvaluated.Rows.Add(dr)
                        End If
                    End If
                End If
            Next

            If (Add_Option = 1) Then    'Bind Observer
                Bind_gv_observer()
            ElseIf (Add_Option = 2) Then 'Bind Staff to be Evaluated
                'Bind_gv_staffEvaluated()
            End If
            pnl_SelectStaff.Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Public Property DT_Evaluator() As DataTable
        Get
            If ViewState("Evaluator") Is Nothing Then
                Dim dtEvaluator As New DataTable()
                dtEvaluator.Columns.Add("BSU_ID")
                dtEvaluator.Columns.Add("BSU_NAME")
                dtEvaluator.Columns.Add("EMP_NAME")
                dtEvaluator.Columns.Add("EMP_ID")
                dtEvaluator.Columns.Add("DES_ID")
                dtEvaluator.Columns.Add("DES_DESCR")
                dtEvaluator.Columns.Add("STATUS")
                dtEvaluator.Columns.Add("GTD_ID")

                ViewState("Evaluator") = dtEvaluator
                Return DirectCast(ViewState("Evaluator"), DataTable)
            Else
                Return DirectCast(ViewState("Evaluator"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("Evaluator") = value
        End Set
    End Property
    Public Property DT_StaffEvaluated() As DataTable
        Get
            If ViewState("StaffEvaluated") Is Nothing Then
                Dim dtStaffEvaluated As New DataTable()
                dtStaffEvaluated.Columns.Add("BSU_ID")
                dtStaffEvaluated.Columns.Add("BSU_NAME")
                dtStaffEvaluated.Columns.Add("EMP_NAME")
                dtStaffEvaluated.Columns.Add("EMP_ID")
                dtStaffEvaluated.Columns.Add("DES_ID")
                dtStaffEvaluated.Columns.Add("DES_DESCR")
                dtStaffEvaluated.Columns.Add("STATUS")
                dtStaffEvaluated.Columns.Add("STE_ID")

                ViewState("StaffEvaluated") = dtStaffEvaluated
                Return DirectCast(ViewState("StaffEvaluated"), DataTable)
            Else
                Return DirectCast(ViewState("StaffEvaluated"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("StaffEvaluated") = value
        End Set
    End Property
#End Region

    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        BindGrid_Staff_Pnl()
    End Sub
    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reset.Click
        clear()
    End Sub
    Public Sub clear()
        txt_EMPReportingTo.Text = ""
        txt_desg.Text = ""
        txt_Name.Text = ""
        hf_EMPReportingTo.Value = ""
    End Sub
End Class
