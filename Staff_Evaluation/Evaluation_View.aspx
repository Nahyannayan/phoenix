<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Evaluation_View.aspx.vb" Inherits="Staff_Evaluation_Evaluation_View" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        var tempObj = '';
        function PopupWindow(url) {
            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {
                    window.open(url);
                }
            }
            catch (e) { }

            return false;
        }
    </script>
    <style>
        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 90%;
            display: inline-block;
            list-style-type: none;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
         .darkPanlAlumini {
            width: 90%;
            height: 90%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 10%;
            top: 10%;
            position: fixed;
            width: 70%;
        }

         .RadGrid_Default .rgPager .rgPagerButton {
            
    width: auto !important;
    min-width: 10%;
    padding: 4px !important;
    background-image: none !important;
    height: auto !important;
    margin: 4px;
    cursor: pointer;
    color: #fff;
    font-weight: bold;
    border-style: none;
    background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%);
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%);
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
        }
        html body .riSingle [type='text'].riTextBox {
            padding : 0px !important;
        }

    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Lesson Evaluation
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Label ID="lblError" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>

                <table style="width: 100%">
                    <tr >
                        <td width="20%"><span class="field-label">Business unit</span></td>
                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_bsu" runat="server" RenderMode="Lightweight" Width="100%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%"><span class="field-label">Evaluation Template</span></td>
                        <td width="30%">
                            <telerik:RadComboBox ID="ddl_EvalTemplate" runat="server" RenderMode="Lightweight" Width="100%"
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr >
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" />
                            <asp:Button ID="btnClearFilter" runat="server" Text="No Filter" CssClass="button" />
                        </td>
                    </tr>

                </table>

                <telerik:RadGrid ID="gv_EvaluatorMapping" runat="server"
                    AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False"
                    GridLines="None"  Width="100%">
                    <MasterTableView>
                        <CommandItemSettings />
                        <RowIndicatorColumn Visible="True">
                            <HeaderStyle />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True">
                            <HeaderStyle />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="EEM_REMARKS" HeaderText="Evaluation Group"
                                UniqueName="column1">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Evaluation Template" UniqueName="TemplateColumn_EVT_TEMPLATE_DESC">
                                <ItemTemplate>

                                    <asp:Label ID="lbl_EVT_TEMPLATE_DESC" runat="server" Text='<%# Bind("EVT_TEMPLATE_DESC") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Business unit" UniqueName="TemplateColumn_BSU">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_NAME") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridBoundColumn DataField="EEM_CREATED_DATE"
                                DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Created Date"
                                UniqueName="column">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EEM_ENDDATE"
                                DataFormatString="{0:dd/MMM/yyyy}" HeaderText="End Date"
                                UniqueName="column">
                            </telerik:GridBoundColumn>

                            <telerik:GridTemplateColumn HeaderText="View"
                                UniqueName="TemplateColumn_VERIFIED">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HF_EEM_ID" runat="server" Value='<%# Bind("EEM_ID") %>' />
                                    <asp:HiddenField ID="hf_BSU_ID" runat="server" Value='<%# Bind("EEM_BSU_ID") %>' />
                                    <asp:HiddenField ID="HF_EVT_ID" runat="server" Value='<%# Bind("EEM_EVT_ID") %>' />
                                    <asp:HiddenField ID="HF_EOB_ID" runat="server" Value='<%# Bind("EOB_ID") %>' />

                                    <asp:LinkButton ID="lnk_edit" runat="server" OnClick="lnk_edit_Click"
                                        ToolTip="Click here to edit the template">View</asp:LinkButton>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <%--       <telerik:GridTemplateColumn HeaderText="Delete" 
                            UniqueName="Template_Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_delete" runat="server" onclick="lnk_delete_Click">Delete</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_lnk_delete" ConfirmText="Confirm delete"
                                TargetControlID="lnk_delete" runat="server">
                                 </ajaxToolkit:ConfirmButtonExtender>  
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn>
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                    <ItemStyle  HorizontalAlign="Left" />
                    <AlternatingItemStyle  HorizontalAlign="Left" />
                    <FilterMenu>
                    </FilterMenu>
                </telerik:RadGrid>

                <asp:Panel ID="pnl_SelectStaff" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">

                        <div class="title-bg">
                            Select Staff
                            <div style="float: right">
                                <asp:LinkButton ForeColor="Red" ID="lnkClose_Staff" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClose_Staff_Click"></asp:LinkButton>
                            </div>
                        </div>

                        <table style="width: 100%;">

                            <tr  style="color: Black">
                                <td align="left"><span class="field-label">Staff Name</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txt_Name" runat="server"></asp:TextBox>
                                </td>
                                <td align="left"><span class="field-label">Designation</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txt_desg" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btn_Search" runat="server" Text="Search"
                                        ImageUrl="~/Images/forum_search.gif" CausesValidation="False"
                                        Style="width: 16px" />

                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td align="left" >
                                    <asp:RadioButtonList ID="rbl_EvlStatus" runat="server"
                                        RepeatDirection="Horizontal" AutoPostBack="True" Font-Bold="True">
                                        <asp:ListItem Text="ALL" Value="A" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="P"></asp:ListItem>
                                        <asp:ListItem Text="Drafted" Value="D"></asp:ListItem>
                                        <asp:ListItem Text="Completed" Value="C"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>

                            <tr  style="color: Black">

                                <td align="left" colspan="4">
                                    <div>
                                        <telerik:RadGrid ID="gv_Staff" runat="server" AutoGenerateColumns="False"
                                            CellSpacing="0" EnableTheming="False" GridLines="None" 
                                            Width="100%" AllowPaging="true" PageSize="30">
                                            <MasterTableView>
                                                <CommandItemSettings />
                                                <RowIndicatorColumn Visible="True">
                                                    <HeaderStyle Width="10px" />
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn Visible="True">
                                                    <HeaderStyle Width="10px" />
                                                </ExpandCollapseColumn>
                                                <Columns>
                                                    <telerik:GridTemplateColumn HeaderText="BSU"
                                                        UniqueName="TemplateColumn_BSU">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_bsu" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Staff Name"
                                                        UniqueName="TemplateColumn_staffName">

                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                                            <asp:Label ID="lbl_EMPNAME" runat="server" Text='<%# Bind("EMP_NAME") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Designation"
                                                        UniqueName="TemplateColumn_desg">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_BSU_ID" runat="server" Value='<%# Bind("BSU_ID") %>' />
                                                            <asp:HiddenField ID="HF_DES_ID" runat="server" Value='<%# Bind("DES_ID") %>' />
                                                            <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR") %>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn DataField="ERM_STATUS"
                                                        HeaderText="Status"
                                                        UniqueName="column">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridTemplateColumn HeaderText="Select" UniqueName="Template_Select"
                                                        Visible="true">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="HF_EEM_ID" runat="server" Value='<%# Bind("EEM_ID") %>' />
                                                            <asp:HiddenField ID="HF_EVT_ID" runat="server" Value='<%# Bind("EEM_EVT_ID") %>' />
                                                            <asp:HiddenField ID="HF_STE_ID" runat="server" Value='<%# Bind("STE_ID") %>' />
                                                            <asp:HiddenField ID="HF_ERM_ID" runat="server" Value='<%# Bind("ERM_ID") %>' />
                                                            <asp:LinkButton ID="lnk_Select_pnl" runat="server" OnClick="lnk_Select_pnl_Click">Select</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                                <EditFormSettings>
                                                    <EditColumn>
                                                    </EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                            <ItemStyle  HorizontalAlign="Left" />
                                            <AlternatingItemStyle  HorizontalAlign="Left" />
                                            <FilterMenu>
                                            </FilterMenu>
                                        </telerik:RadGrid>
                                    </div>
                                </td>
                            </tr>
                            <tr  align="center" style="color: Black">
                                <td colspan="4">
                                    <asp:Button ID="btn_PnlClose" runat="server" Text="Close" CssClass="button" />
                                    <br />
                                    <asp:Label ID="lbl_Saveerror" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>
                                </td>
                            </tr>

                        </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>

