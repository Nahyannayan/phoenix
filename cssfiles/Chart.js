﻿function checkDate() { }
function ajaxToolkit_CalendarExtenderClientShowing(e) {
    if (!e.get_selectedDate() || !e.get_element().value)
        e._selectedDate = (new Date()).getDateOnly();
}

//--do drilldown-------------------------------------------------------------------------------
function drill(level, keyid) {
    document.getElementById("siteLoader").style.display = "block";
    document.getElementById("container").style.display = "none";
    var selBSU = ''; var selFEE = '';
    var inputsFees = document.getElementById('rblFees').getElementsByTagName("input");
    for (var i = 0; i < inputsFees.length; i++) if (inputsFees[i].checked) break;

    var inputsSummary = document.getElementById('rblSummaryType').getElementsByTagName("input");
    for (var j = 0; j < inputsSummary.length; j++) if (inputsSummary[j].checked) break;

    var inputsGroup = document.getElementById('rblGroup').getElementsByTagName("input");
    for (var k = 0; k < inputsGroup.length; k++) if (inputsGroup[k].checked) break;

    var inputsBSUId = document.getElementById('tvBusinessunit');
    var inputsBSU = document.getElementById('tvBusinessunit').getElementsByTagName("input");
    for (var a = 0; a < inputsBSU.length; a++)
        if (inputsBSU[a].checked) selBSU = selBSU + "'" + inputsBSU[a].parentElement.innerText + "',";
    var inputsFEEId = document.getElementById('tvFees');
    var inputsFEE = document.getElementById('tvFees').getElementsByTagName("input");
    for (a = 0; a < inputsFEE.length; a++)
        if (inputsFEE[a].checked) selFEE = selFEE + "'" + inputsFEE[a].parentElement.innerText + "',";

    var asondate = document.getElementById('txtAsOnDate').value;
    var comparedate = document.getElementById('txtCompareDate').value;
    var topselect = document.getElementById('slider1').value;

    var txtSlab1 = document.getElementById('txtSlab1').value;
    var txtSlab2 = document.getElementById('txtSlab2').value;
    var txtSlab3 = document.getElementById('txtSlab3').value;
    var txtSlab4 = document.getElementById('txtSlab4').value;
    var txtSlab5 = document.getElementById('txtSlab5').value;
    var txtSlab6 = document.getElementById('txtSlab6').value;

    PageMethods.GetfeeAgeing('p', selBSU, selFEE, i, j, k, level, keyid, 0, asondate, comparedate, topselect, txtSlab1, txtSlab2, txtSlab3, txtSlab4, txtSlab5, txtSlab6, CallSuccess, CallFailed);
}

function CallSuccess(res) {
    var mySplitResult = res.split("!");
    updateChartXML("pie1Id", mySplitResult[0]);
    updateChartXML("chartId", mySplitResult[4]);
    document.getElementById('mygridasondate').innerHTML = mySplitResult[1];
    if (mySplitResult[3].length == 0) {
        document.getElementById('mychartcomparedate').style.setAttribute('display', 'none');
    }
    else {
        updateChartXML("pie2Id", mySplitResult[3]);
        document.getElementById('mychartcomparedate').style.setAttribute('display', '');
    }
    document.getElementById("siteLoader").style.display = "none";
    document.getElementById("container").style.display = "";
}

//--show pie chart-------------------------------------------------------------------------------
function CallMe() {
    var inputsFees = document.getElementById('rblFees').getElementsByTagName("input");
    for (var i = 0; i < inputsFees.length; i++) if (inputsFees[i].checked) break;

    var inputsSummary = document.getElementById('rblSummaryType').getElementsByTagName("input");
    for (var j = 0; j < inputsSummary.length; j++) if (inputsSummary[i].checked) break;

    var asondate = document.getElementById('txtAsOnDate').value;
    var comparedate = document.getElementById('txtCompareDate').value;
    var topselect = document.getElementById('slider1').value;

    PageMethods.GetfeeAgeing('p', i, j, "", topselect, asondate, comparedate, 5, CallSuccess, CallFailed);
}
function CallSuccessObs(res) {
    var mySplitResult = res.split("!");
    updateChartXML("pie1Id", mySplitResult[0]);
    updateChartXML("chartId", mySplitResult[0]);
    document.getElementById('mygridasondate').innerHTML = mySplitResult[1];
}

//--show bar chart for business unit-------------------------------------------------------------------------------
function CallMeChild(childid) {
    var inputsFees = document.getElementById('rblFees').getElementsByTagName("input");
    for (var i = 0; i < inputsFees.length; i++) if (inputsFees[i].checked) break;

    var inputsSummary = document.getElementById('rblSummaryType').getElementsByTagName("input");
    for (var j = 0; j < inputsSummary.length; j++) if (inputsSummary[i].checked) break;

    var asondate = document.getElementById('txtAsOnDate').value;
    var comparedate = document.getElementById('txtCompareDate').value;
    var topselect = document.getElementById('slider1').value;

    PageMethods.GetfeeAgeing('p', i, j, childid, 0, asondate, comparedate, 5, CallSuccess, CallFailed);
    if (comparedate.length == 0) {
        document.getElementById('mychartcompare').style.setAttribute('display', 'none');
        document.getElementById('mychartdetail').style.setAttribute('display', '');
        PageMethods.GetfeeAgeing('b', i, j, "", 0, asondate, comparedate, topselect, CallSuccessChild, CallFailed);
    }
    else {
        document.getElementById('mychartcompare').style.setAttribute('display', '');
        document.getElementById('mychartdetail').style.setAttribute('display', 'none');
        PageMethods.GetfeeAgeing('b', i, j, childid, 0, asondate, comparedate, topselect, CallSuccessCompare, CallFailed);
    }

}
function CallSuccessChild(res) { updateChartXML("chartId", res); }

function CallFailed() { alert(res.get_message()); }

function init() {
    var chart1 = new FusionCharts("../FusionCharts/FCF_Pie3D.swf?ChartNoDataText=Please select a record above", "pie1Id", "700", "350");
    chart1.setDataXML("<graph></graph>");
    chart1.render("mychartasondate");

    var chart2 = new FusionCharts("../FusionCharts/FCF_Pie3D.swf?ChartNoDataText=Please select a record above", "pie2Id", "700", "350");
    chart2.setDataXML("<graph></graph>");
    chart2.render("mychartcomparedate");

    var chart3 = new FusionCharts("../FusionCharts/FCF_MSColumn3D.swf?ChartNoDataText=Please select a record above", "chartId", "1000", "200");
    chart3.setDataXML("<graph></graph>");
    chart3.render("mychartdetail");

    document.getElementById('mychartcomparedate').style.setAttribute('display', 'none');
    document.getElementById("siteLoader").style.display = "none";
    //document.getElementById('mychartdetail').style.setAttribute('display', 'none');
}
function change_chk_state(src) {
    var chk_state = (src.checked);
    for (i = 0; i < document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].type == 'checkbox') {
            document.forms[0].elements[i].checked = chk_state;
        }
    }
}
function client_OnTreeNodeChecked() {
    var obj = window.event.srcElement;
    var treeNodeFound = false;
    var checkedState;
    if (obj.tagName == "INPUT" && obj.type == "checkbox") {
        //                //
        //                if  ( obj.title=='All' && obj.checked ) alert(obj.checked);
        //                change_chk_state(obj); }
        //                //                
        var treeNode = obj;
        checkedState = treeNode.checked;
        do {
            obj = obj.parentElement;
        } while (obj.tagName != "TABLE")
        var parentTreeLevel = obj.rows[0].cells.length;
        var parentTreeNode = obj.rows[0].cells[0];
        var tables = obj.parentElement.getElementsByTagName("TABLE");
        var numTables = tables.length
        if (numTables >= 1) {
            for (i = 0; i < numTables; i++) {
                if (tables[i] == obj) {
                    treeNodeFound = true;
                    i++;
                    if (i == numTables) {
                        return;
                    }
                }
                if (treeNodeFound == true) {
                    var childTreeLevel = tables[i].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel) {
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                    }
                    else {
                        return;
                    }
                }
            }
        }
    }
}

function printTable() {
    var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
    disp_setting += "scrollbars=yes,width=1280, height=600, left=100, top=25";
    var content_vlue = document.getElementById('mygridasondate').innerHTML;
    //var header = document.getElementById('header').innerHTML;
    //var subheader = document.getElementById('subheader').innerHTML;
    //var filterby = document.getElementById('filterby').innerHTML;
    var docprint = window.open("", "", disp_setting);
    docprint.document.open();
    var HeaderRow = '<table width="100%" cellpadding="0" cellspacing="0"><tr><td class="title" align="center" >';
    docprint.document.write('<html><head>');
    docprint.document.write('<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />');
    docprint.document.write('<script language="javascript" type="text/javascript">function setColour(element, colour) {} function drill(level, keyid) {} </script>');
    docprint.document.write('</head><body onLoad="self.print();self.close();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');
    docprint.document.write(HeaderRow);
    //docprint.document.write(header & subheader & filterby)
    docprint.document.write(content_vlue);
    docprint.document.write('</center></body></html>');
    docprint.document.close();
    docprint.focus();
}

function TableToExcel() {
    var strCopy = document.getElementById("mygridasondate").innerHTML;
    window.clipboardData.setData("Text", strCopy);
    var objExcel = new ActiveXObject("Excel.Application");
    objExcel.visible = true;

    var objWorkbook = objExcel.Workbooks.Add;
    var objWorksheet = objWorkbook.Worksheets(1);
    objWorksheet.Paste;
}



var isShift = false;
function isNumeric(keyCode) {
    if (keyCode == 16) isShift = true;

    return ((keyCode >= 48 && keyCode <= 57 || keyCode == 8 || keyCode == 39 || keyCode == 46 || keyCode == 37 ||
            (keyCode >= 96 && keyCode <= 105)) && isShift == false);
}

function keyUP(keyCode) {
    if (keyCode == 16)
        isShift = false;

}

function ajaxToolkit_CalendarExtenderClientShowing(e) {
    if (!e.get_selectedDate() || !e.get_element().value)
        e._selectedDate = (new Date()).getDateOnly();

}

function checkDate(sender, args) {
}

function setColour(element, colour) {
    if (element.style) element.style.color = colour;
}

window.onload = init;







// http://bontragerconnection.com/ and http://www.willmaster.com/
// Version: July 28, 2007
var cX = 0; var cY = 0; var rX = 0; var rY = 0;
function UpdateCursorPosition(e){ cX = e.pageX; cY = e.pageY;}
function UpdateCursorPositionDocAll(e){ cX = event.clientX; cY = event.clientY;}
if(document.all) { document.onmousemove = UpdateCursorPositionDocAll; }
else { document.onmousemove = UpdateCursorPosition; }
function AssignPosition(d) {
if(self.pageYOffset) {
	rX = self.pageXOffset;
	rY = self.pageYOffset;
	}
else if(document.documentElement && document.documentElement.scrollTop) {
	rX = document.documentElement.scrollLeft;
	rY = document.documentElement.scrollTop;
	}
else if(document.body) {
	rX = document.body.scrollLeft;
	rY = document.body.scrollTop;
	}
if(document.all) {
	cX += rX; 
	cY += rY;
	}
d.style.left = (cX+10) + "px";
d.style.top = (cY+10) + "px";
}
function HideContent(d) {
if(d.length < 1) { return; }
document.getElementById(d).style.display = "none";
}
function ShowContent(d,e) {
if(d.length < 1) { return; }
var ddd = document.getElementById("Div1");
var dd = document.getElementById(d);
var str = ddd.innerHTML;
str = str.replace(/@@@/gi, e);
dd.innerHTML = str;
AssignPosition(dd);
dd.style.display = "block";
}
document.onclick = check;
function check(e) {
    var target = (e && e.target) || (event && event.srcElement);
    var obj = document.getElementById('uniquename2');
    if (target != obj) { obj.style.display = 'none' } ;
} 
function getajaxdata(flag, id) {
    Showdata(flag, id);
    //alert(id);
}

function ReverseContentDisplay(d) {
if(d.length < 1) { return; }
var dd = document.getElementById(d);
AssignPosition(dd);
if(dd.style.display == "none") { dd.style.display = "block"; }
else { dd.style.display = "none"; }
}

function Showdata(mode, STU_ID) {
    var sFeatures, url;
    sFeatures = "dialogWidth:750px; dialogHeight: 400px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
    var NameandCode;
    var result;
    var asondate = document.getElementById("txtAsOnDate").value; 
    if (mode == 1) {
        url = "../common/PaymentHistory.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
        sFeatures = sFeatures.replace('400px', '650px');
        sFeatures = sFeatures.replace('750px', '850px');
    }
    else if (mode == 2)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=PARENTDETAILS&stuid=" + STU_ID + "&asOnDate=" + asondate;
    else if (mode == 3)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=SIBBLINGS&stuid=" + STU_ID + "&asOnDate=" + asondate;
    else if (mode == 4)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=STUDENTDETAILS&stuid=" + STU_ID + "&asOnDate=" + asondate;
    else if (mode == 5)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=COMMUNICATION&stuid=" + STU_ID + "&asOnDate=" + asondate;
    else if (mode == 6)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=REMARKS&stuid=" + STU_ID + "&asOnDate=" + asondate;
    else if (mode == 7)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=OUTSTANDING&stuid=" + STU_ID + "&asOnDate=" + asondate;
    else if (mode == 8)
        url = "../Fees/FeeAgeingBoxPopup.aspx?id=LEDGER&stuid=" + STU_ID + "&asOnDate=" + asondate;
    result = window.showModalDialog(url, "", sFeatures);
    return false;
} 

//-->
