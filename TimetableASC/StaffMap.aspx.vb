﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Imports System.Data

Partial Class TimetableASC_StaffMap
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load   

        If Page.IsPostBack = False Then
            Try

                ViewState("Acd_Id_Sel") = Encr_decrData.Decrypt(Request.QueryString("ACDID").Replace(" ", "+"))
                ViewState("Timegrid_ID_Sel") = Encr_decrData.Decrypt(Request.QueryString("Timegrid_ID").Replace(" ", "+"))

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("SET_TYPE") = "0"

                binddropdown()
                bindMissingTeacher_data(ViewState("Acd_Id_Sel"), ViewState("SET_TYPE"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Protected Sub bindMissingTeacher_data(ByVal acd_id As Int32, ByVal settype As Integer)
        Dim CONN As String = ConnectionManger.GetPhoenix_ASCConnectionString
        Dim ds As New DataSet
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", acd_id)
        PARAM(2) = New SqlParameter("@SET_TYPE", settype)
        PARAM(3) = New SqlParameter("@timegrid_id", Convert.ToInt32(ViewState("Timegrid_ID_Sel").ToString()))
        ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "GET_TIMETABLE_TEACHERS", PARAM)

        If ds.Tables(0).Rows.Count > 0 Then
            gvTeacherMap.DataSource = ds.Tables(0)
            gvTeacherMap.DataBind()
        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
            gvTeacherMap.DataSource = ds.Tables(0)
            Try
                gvTeacherMap.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvTeacherMap.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvTeacherMap.Rows(0).Cells.Clear()
            gvTeacherMap.Rows(0).Cells.Add(New TableCell)
            gvTeacherMap.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTeacherMap.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTeacherMap.Rows(0).Cells(0).Text = "No Records Available !!!"
            Dim imgMap As Image = DirectCast(gvTeacherMap.Rows(0).FindControl("imgMap"), Image)
            If Not imgMap Is Nothing Then
                imgMap.Visible = False
            End If
        End If

    End Sub

    Protected Sub binddropdown()
        Session("TTStaff_map") = Nothing
        Dim CONN As String = ConnectionManger.GetPhoenix_ASCConnectionString
        Dim ds As New DataSet
        Dim UNTISTEACHER As New List(Of ListItem)
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "GETOASIS_TEACHER", PARAM)
        UNTISTEACHER.Add(New ListItem("Teacher Not Mapped", "-1"))
        If ds.Tables(0).Rows.Count > 0 Then
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                UNTISTEACHER.Add(New ListItem(ds.Tables(0).Rows(I)("ENAME"), ds.Tables(0).Rows(I)("SHORTNAME")))
            Next
        End If
        Session("TTStaff_map") = UNTISTEACHER
    End Sub

    Protected Sub gvTeacherMap_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlOasis_teacher_Name As DropDownList = DirectCast(e.Row.FindControl("ddlOasis_teacher_Name"), DropDownList)
                Dim TEACHER_ID As HiddenField = DirectCast(e.Row.FindControl("hidTCH_ID"), HiddenField)
                Dim lblTTM_TCH_ID As Label = DirectCast(e.Row.FindControl("lblTTM_TCH_ID"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)

                'Session("TTStaff_map")
                If Not Session("TTStaff_map") Is Nothing Then
                    ddlOasis_teacher_Name.Items.Clear()
                    ddlOasis_teacher_Name.DataSource = Session("TTStaff_map")
                    ddlOasis_teacher_Name.DataTextField = "Text"
                    ddlOasis_teacher_Name.DataValueField = "Value"
                    ddlOasis_teacher_Name.DataBind()

                    For Each ITEM As ListItem In ddlOasis_teacher_Name.Items
                        If TEACHER_ID.Value = "" Or TEACHER_ID.Value Is Nothing Or ITEM.Value = "-1" Then
                        Else
                            If ITEM.Value.ToUpper.Contains(TEACHER_ID.Value.ToUpper.ToString) Then
                                ITEM.Selected = True
                                imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                Exit For
                            ElseIf ITEM.Text.ToUpper.Contains(lblTTM_TCH_ID.Text.ToUpper) Then
                                ITEM.Selected = True
                                imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                Exit For
                            End If
                        End If
                       
                    Next
                End If
            End If
        Catch ex As Exception
            Dim strmessage As String = ex.Message
        End Try
    End Sub


    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim ddlUntis_Staff_Name As DropDownList
        Dim lblTTM_TEACHER As Label
        Dim STR_XML As New StringBuilder

        For Each row As GridViewRow In gvTeacherMap.Rows
            ddlUntis_Staff_Name = row.FindControl("ddlOasis_teacher_Name")
            lblTTM_TEACHER = row.FindControl("lblSrNo")
            If ddlUntis_Staff_Name.SelectedValue.ToString.Trim <> "-1" Then
                STR_XML.Append(lblTTM_TEACHER.Text & "#" & ddlUntis_Staff_Name.SelectedValue.ToString.Trim & "|")
            End If
        Next

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetPhoenix_ASCConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("Acd_Id_Sel"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@STAFF", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVETEACHER_S_MAPPING", PARAM)
                ReturnFlag = PARAM(4).Value


                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If


            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function


    Protected Sub saveMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then

            bindMissingTeacher_data(ViewState("Acd_Id_Sel"), ViewState("SET_TYPE"))
            lblErr.Text = "Record Saved Successfully"

        Else
            lblErr.Text = errorMessage
        End If
    End Sub

    Protected Sub backProcessing_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim url As String
            url = String.Format("\TimetableASC\Timetable_upload.aspx")
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub

   
    Protected Sub gvTeacherMap_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvTeacherMap.PageIndex = e.NewPageIndex
        bindMissingTeacher_data(ViewState("Acd_Id_Sel"), ViewState("SET_TYPE"))
    End Sub
End Class
