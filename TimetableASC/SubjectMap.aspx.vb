﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TimetableIntegration_SubjectMap
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try

                ViewState("Acd_Id_Sel") = Encr_decrData.Decrypt(Request.QueryString("ACDID").Replace(" ", "+"))
                ViewState("Timegrid_ID_Sel") = Encr_decrData.Decrypt(Request.QueryString("Timegrid_ID").Replace(" ", "+"))
                ViewState("SET_TYPE") = "0"                              
                bindMissingSubject_data(ViewState("Acd_Id_Sel"), ViewState("SET_TYPE"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try

            Dim url As String
            url = String.Format("\TimetableASC\Timetable_upload.aspx")
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub



#Region "Process subject mapping "
    Sub bindMissingSubject_data(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetPhoenix_ASCConnectionString
            Dim ds As New DataSet
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            PARAM(3) = New SqlParameter("@timegrid_id", Convert.ToInt32(ViewState("Timegrid_ID_Sel").ToString()))
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "GET_TIMETABLE_SUBJECTS", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSubjectMap.DataSource = ds.Tables(0)
                gvSubjectMap.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvSubjectMap.DataSource = ds.Tables(0)
                Try
                    gvSubjectMap.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSubjectMap.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSubjectMap.Rows(0).Cells.Clear()
                gvSubjectMap.Rows(0).Cells.Add(New TableCell)
                gvSubjectMap.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubjectMap.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubjectMap.Rows(0).Cells(0).Text = "No Records Available !!!"
                Dim imgMap As Image = DirectCast(gvSubjectMap.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvSubjectMap_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlOasis_Subject_Name As DropDownList = DirectCast(e.Row.FindControl("ddlOasis_Subject_Name"), DropDownList)
                Dim lblTTM_SUBJECT As HiddenField = DirectCast(e.Row.FindControl("hidTTM_SHORTCODE"), HiddenField)
                Dim lblTTM_SUBJECTNAME As Label = DirectCast(e.Row.FindControl("lblTTM_SUBJECT"), Label)
                Dim lblTTM_GRD_ID As Label = DirectCast(e.Row.FindControl("lblTTM_GRD_ID"), Label)
                Dim lblTTM_Stream As Label = DirectCast(e.Row.FindControl("lblTTM_Stream"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)
                Dim hidGRD_ID As HiddenField = DirectCast(e.Row.FindControl("hidGRD_ID"), HiddenField)
                Dim hidTTM_SBG_MAPPED_ID As HiddenField = DirectCast(e.Row.FindControl("hidTTM_SBG_MAPPED_ID"), HiddenField)
                Dim DS As DataSet = bindSubjectDdl(hidGRD_ID.Value, lblTTM_Stream.Text)
                If Not DS Is Nothing And Not DS.Tables(0) Is Nothing And Not DS.Tables(0).Rows Is Nothing And DS.Tables(0).Rows.Count > 0 Then
                    ddlOasis_Subject_Name.Items.Clear()
                    ddlOasis_Subject_Name.DataSource = DS.Tables(0)
                    ddlOasis_Subject_Name.DataTextField = "SUBJECT"
                    ddlOasis_Subject_Name.DataValueField = "ID"
                    ddlOasis_Subject_Name.DataBind()
                    If Not ddlOasis_Subject_Name.Items.FindByValue(hidTTM_SBG_MAPPED_ID.Value) Is Nothing Then
                        ddlOasis_Subject_Name.Items.FindByValue(hidTTM_SBG_MAPPED_ID.Value).Selected = True
                        imgMap.ImageUrl = "~/Images/arrowgreen.png"
                    Else
                        For Each ITEM As ListItem In ddlOasis_Subject_Name.Items
                           If ITEM.Text.ToUpper.Contains("(" + lblTTM_SUBJECT.Value.ToUpper + ")") = True Then
                                ITEM.Selected = True
                                imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                Exit For
                            ElseIf ITEM.Text.ToUpper.Contains(lblTTM_SUBJECTNAME.Text.ToUpper) Then
                                ITEM.Selected = True
                                imgMap.ImageUrl = "~/Images/arrowgreen.png"
                                Exit For
                            End If
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Function bindSubjectDdl(ByVal grade As String, ByVal stream As String) As DataSet
        Dim ds_subject As DataSet = New DataSet
        Try
            '[TM].[GETOASIS_SUBJECT]
            Dim str_conn = ConnectionManger.GetPhoenix_ASCConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ViewState("Acd_Id_Sel"))
            PARAM(2) = New SqlParameter("@GRD_ID", grade)
            PARAM(3) = New SqlParameter("@SET_TYPE", 0)
            PARAM(4) = New SqlParameter("@STREAM", stream)
            ds_subject = SqlHelper.ExecuteDataset(str_conn, "[GETOASIS_SUBJECT]", PARAM)
            Return ds_subject
        Catch ex As Exception
            Return ds_subject
        End Try
    End Function
    Protected Sub btnSaveMap_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMap.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then

            ' bindMissingSubject_data(ViewState("acd_id"), ViewState("SET_TYPE"))
            bindMissingSubject_data(ViewState("Acd_Id_Sel"), ViewState("SET_TYPE"))
            lblErr.Text = "Record Saved Successfully"

        Else
            lblErr.Text = errorMessage
        End If
    End Sub
    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim STR_XML As New StringBuilder
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetPhoenix_ASCConnection

            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim ddlOasis_Subject_Name As DropDownList
                Dim lblTTM_SUBJECT As Label
                Dim lblTTM_STM_ID As Label
                For Each row As GridViewRow In gvSubjectMap.Rows
                    ddlOasis_Subject_Name = row.FindControl("ddlOasis_Subject_Name")
                    lblTTM_SUBJECT = row.FindControl("lblTTM_SUBJECT")
                    lblTTM_STM_ID = row.FindControl("lblTTM_Stream")
                    If ddlOasis_Subject_Name.SelectedValue.ToString.Trim <> "-1" Then
                        STR_XML.Append(lblTTM_SUBJECT.Text & "&" & lblTTM_STM_ID.Text & "%" & ddlOasis_Subject_Name.SelectedValue.ToString.Trim & "|")
                    End If
                Next

                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ViewState("Acd_Id_Sel"))
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@SUB_GRP", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVESUBJECT_MAPPING", PARAM)
                ReturnFlag = PARAM(4).Value
                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If
            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using
    End Function
#End Region

   
End Class
