﻿<%@ Page Language="VB" AutoEventWireup="true"  CodeFile="TeacherMap.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="TimetableASC_TeacherMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        function validateSub() {
            var flag = false;
            var dropdowns = new Array(); //Create array to hold all the dropdown lists.
            var imgArr = new Array(); //Create array to hold all the dropdown lists.
            var gridview = document.getElementById('<%=gvTeacherMap.ClientID%>'); //GridView1 is the id of ur gridview.
            dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
            var imgArr = gridview.getElementsByTagName('img');

            for (var i = 0; i < dropdowns.length; i++) {

                if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                {

                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowgreen.png"
                    flag = true;
                }
                else {

                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowred.png"

                }
            }
        }



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="TEACHER MASTER"></asp:Literal>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left" colspan="4">
                            <span class="error">
                                <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></span>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">

                            <asp:Panel ID="plTeacherMap" runat="server" ScrollBars="Vertical" Width="100%">
                                <asp:GridView ID="gvTeacherMap" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvTeacherMap_RowDataBound"
                                    CssClass="table table-bordered table-row" Height="100%"
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Class">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_TCH_ID" runat="server" Text='<%# Bind("TEACHER")%>'></asp:Label>
                                                <asp:HiddenField ID="hidTCH_ID" runat="server" Value='<%# Bind("TEACHER_ID")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Map it with OASIS">
                                            <ItemTemplate>
                                                <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OASIS Available Subject Group">
                                            <EditItemTemplate></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlOasis_teacher_Name" runat="server" onchange="validateSub()" CssClass="ddlanswer">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" />
                                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center"
                                        VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </asp:Panel>



                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">

                            <asp:Button ID="saveMapping" runat="server" Text="Map Teacher" OnClick="saveMapping_Click" />
                            <asp:Button ID="backProcessing" runat="server" Text="Back" OnClick="backProcessing_Click" />

                        </td>


                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
