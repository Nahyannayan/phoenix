﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ClassMap.ascx.vb" Inherits="TimetableIntegration_UserControl_ClassMap" %>

<table width="100%">
    <tr valign="bottom">
        <td align="left" valign="middle">
            <span class="error">
                <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></span>
        </td>
    </tr>
    <tr>
        <td width="20%">
            <span class="field-label">Academic year</span>
        </td>
        <td width="30%">
            <asp:DropDownList ID="ddlAcdYear" runat="server" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td width="20%">
            <span class="field-label">ASC Format</span>
        </td>
        <td width="30%">
            <asp:DropDownList ID="ddlFormats" runat="server">
                <asp:ListItem Value="format1">ASC Format 1</asp:ListItem>
                <asp:ListItem Value="format2">ASC Format 2</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="left" colspan="4">
            <asp:Panel ID="plClassMap" runat="server" ScrollBars="Vertical" Width="100%">
                <asp:GridView ID="gvClassMap" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row" Height="100%" Width="100%">
                    <RowStyle CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField HeaderText="Uploaded Class">
                            <ItemTemplate>
                                <asp:Label ID="lbl_CLASS" runat="server" Text='<%# Bind("Class")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lbl_CLASS_TOMAP" runat="server" Text='<%# Bind("ClassToMap")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lbl_CLASS_DISPLAY" runat="server" Text='<%# Bind("ClassDisplay")%>'></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Map it with OASIS">
                            <ItemTemplate>
                                <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OASIS Available Class">
                            <EditItemTemplate></EditItemTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlUntis_CS_Name" runat="server" onchange="validate()" CssClass="ddlanswer">
                                </asp:DropDownList>

                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="Green" />
                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </asp:Panel>
        </td>

    </tr>
    <tr>
        <td align="center" colspan="4">

            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" 
                CssClass="button" Text="Back" />

        </td>
    </tr>
</table>

