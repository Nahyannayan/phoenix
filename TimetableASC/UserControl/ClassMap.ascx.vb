﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class TimetableIntegration_UserControl_ClassMap
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try


            If Page.IsPostBack = False Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("SET_TYPE") = "0"
                Dim arr As String() = New String(2) {}

                ViewState("Acd_Id_Sel") = Encr_decrData.Decrypt(Request.QueryString("ACDID").Replace(" ", "+"))
                ViewState("Timegrid_ID_Sel") = Encr_decrData.Decrypt(Request.QueryString("Timegrid_ID").Replace(" ", "+"))

                'If Not Session("ACD_TYPE_TT") Is Nothing Then
                '    arr = Session("ACD_TYPE_TT").ToString.Split("|")
                '    ViewState("acd_id") = arr(0)
                '    ViewState("SET_TYPE") = arr(1)
                'End If
                BindAcademicYear()
                bind_dropdown(ddlAcdYear.SelectedValue, ViewState("SET_TYPE"))
                bindClassData_OASIS(ddlAcdYear.SelectedValue, ViewState("SET_TYPE"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        bind_dropdown(ddlAcdYear.SelectedValue, ViewState("SET_TYPE"))
        bindClassData_OASIS(ddlAcdYear.SelectedValue, ViewState("SET_TYPE"))
    End Sub
    Protected Sub BindAcademicYear()
        Dim dt As DataTable
        dt = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        ddlAcdYear.DataSource = dt
        ddlAcdYear.DataTextField = "ACY_DESCR"
        ddlAcdYear.DataValueField = "ACD_ID"
        ddlAcdYear.DataBind()
        For Each rowACD As DataRow In dt.Rows
            If rowACD("ACD_ID") = ViewState("Acd_Id_Sel") Then
                ddlAcdYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Public Sub bind_dropdown(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Session("TTClass_map") = Nothing
        Dim CONN As String = ConnectionManger.GetPhoenix_ASCConnectionString
        Dim ds As New DataSet
        Dim UNTISCLASS As New List(Of ListItem)
        Dim PARAM(3) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
        ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[GETOASIS_CLASS]", PARAM)
        UNTISCLASS.Add(New ListItem("Class Not Mapped", "-1"))
        If ds.Tables(0).Rows.Count > 0 Then
            For I As Integer = 0 To ds.Tables(0).Rows.Count - 1
                UNTISCLASS.Add(New ListItem(ds.Tables(0).Rows(I)("CLASS"), ds.Tables(0).Rows(I)("CLASS_ID")))
            Next

        End If
        Session("TTClass_map") = UNTISCLASS
    End Sub
    Sub bindClassData_OASIS(ByVal ACD_ID As String, ByVal SET_TYPE As String)
        Try
            Dim CONN As String = ConnectionManger.GetPhoenix_ASCConnectionString
            Dim ds As New DataSet
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            '  PARAM(2) = New SqlParameter("@SET_TYPE", SET_TYPE)
            PARAM(2) = New SqlParameter("@timegrid_id", Convert.ToInt32(ViewState("Timegrid_ID_Sel").ToString()))
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[dbo].[GETTIMETABLE_CLASS]", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvClassMap.DataSource = ds.Tables(0)
                gvClassMap.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                gvClassMap.DataSource = ds.Tables(0)
                Try
                    gvClassMap.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvClassMap.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvClassMap.Rows(0).Cells.Clear()
                gvClassMap.Rows(0).Cells.Add(New TableCell)
                gvClassMap.Rows(0).Cells(0).ColumnSpan = columnCount
                gvClassMap.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvClassMap.Rows(0).Cells(0).Text = "No Class Available !!!"
                Dim imgMap As Image = DirectCast(gvClassMap.Rows(0).FindControl("imgMap"), Image)
                If Not imgMap Is Nothing Then
                    imgMap.Visible = False
                End If
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvClassMap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClassMap.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlUntis_CS_Name As DropDownList = DirectCast(e.Row.FindControl("ddlUntis_CS_Name"), DropDownList)
                Dim lbl_CLASS As Label = DirectCast(e.Row.FindControl("lbl_CLASS"), Label)
                Dim lbl_CLASS_TOMAP As Label = DirectCast(e.Row.FindControl("lbl_CLASS_TOMAP"), Label)
                Dim imgMap As Image = DirectCast(e.Row.FindControl("imgMap"), Image)
                If Not Session("TTClass_map") Is Nothing Then

                    ddlUntis_CS_Name.Items.Clear()
                    ddlUntis_CS_Name.DataSource = Session("TTClass_map")
                    ddlUntis_CS_Name.DataTextField = "Text"
                    ddlUntis_CS_Name.DataValueField = "Value"
                    ddlUntis_CS_Name.DataBind()
                    ddlUntis_CS_Name.Attributes.Add("onchange", "validate('" & gvClassMap.ClientID & "')")
                    Dim lblTTM_CLASS_rem As String = lbl_CLASS.Text.Replace("#", "")
                    If Not ddlUntis_CS_Name.Items.FindByText(lbl_CLASS_TOMAP.Text) Is Nothing Then
                        ddlUntis_CS_Name.ClearSelection()
                        ddlUntis_CS_Name.Items.FindByText(lbl_CLASS_TOMAP.Text).Selected = True
                        imgMap.ImageUrl = "~/Images/arrowgreen.png"
                    ElseIf Not ddlUntis_CS_Name.Items.FindByText(lbl_CLASS.Text) Is Nothing Then
                        ddlUntis_CS_Name.ClearSelection()
                        ddlUntis_CS_Name.Items.FindByText(lbl_CLASS.Text).Selected = True
                        imgMap.ImageUrl = "~/Images/arrowgreen.png"
                    End If
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            Dim url As String
            url = String.Format("\TimetableASC\Timetable_upload.aspx")
            Response.Redirect(url)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CALLTRANSACTION(errorMessage)
        If str_err = "0" Then

            bind_dropdown(ddlAcdYear.SelectedValue, ViewState("SET_TYPE"))
            bindClassData_OASIS(ddlAcdYear.SelectedValue, ViewState("SET_TYPE"))

            lblErr.Text = "Record Saved Successfully"

        Else
            lblErr.Text = errorMessage
        End If
    End Sub

    Private Function CALLTRANSACTION(ByRef errorMessage As String) As Integer
        Dim ReturnFlag As Integer
        Dim sysErr As String = String.Empty
        Dim ddlUntis_CS_Name As DropDownList
        Dim lblTTM_CLASS As Label
        Dim lblTTM_CLASS_TOMAP As Label
        Dim STR_XML As New StringBuilder

        For Each row As GridViewRow In gvClassMap.Rows
            ddlUntis_CS_Name = row.FindControl("ddlUntis_CS_Name")
            lblTTM_CLASS = row.FindControl("lbl_CLASS_DISPLAY")
            lblTTM_CLASS_TOMAP = row.FindControl("lbl_CLASS_TOMAP")
            If ddlUntis_CS_Name.SelectedValue.ToString.Trim <> "-1" Then
                lblTTM_CLASS.Text = lblTTM_CLASS.Text.Replace(" ", "")
                STR_XML.Append(lblTTM_CLASS.Text & "$" & ddlUntis_CS_Name.SelectedValue.ToString.Trim & "|")
            End If

        Next

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetPhoenix_ASCConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim PARAM(5) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcdYear.SelectedValue)
                PARAM(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlClient.SqlParameter("@SET_TYPE", ViewState("SET_TYPE"))
                PARAM(3) = New SqlClient.SqlParameter("@CLASS", STR_XML.ToString)
                PARAM(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVECLASS_MAPPING", PARAM)
                ReturnFlag = PARAM(4).Value
                If ReturnFlag = 11 Then
                    CALLTRANSACTION = 1
                ElseIf ReturnFlag <> 0 Then
                    CALLTRANSACTION = 1
                    errorMessage = "Error occured while saving !!!"
                End If

            Catch ex As Exception
                sysErr = ex.Message
                CALLTRANSACTION = 1
                errorMessage = "Error occured while saving !!!"
            Finally
                If CALLTRANSACTION <> 0 Then
                    UtilityObj.Errorlog(sysErr)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

  
End Class

