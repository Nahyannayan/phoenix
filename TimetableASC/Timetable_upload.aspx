﻿<%@ Page Language="VB" AutoEventWireup="true" EnableEventValidation="true" CodeFile="Timetable_upload.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="TimetableASC_Timetable_upload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .buttonnew {
            border-radius: 4px !important;
            background: linear-gradient(to bottom,#6a923a 0%, #6a923a 44%, #6a923a 100%) !important;
            /*background: linear-gradient(to right, #67b26b, #4ca2cb) !important;*/
            border: none !important;
            color: #FFFFFF !important;
            text-align: center !important;
            text-transform: uppercase !important;
            font-size: 14px !important;
            width: 180px !important;
            transition: all 0.4s !important;
            cursor: pointer !important;
            margin: 5px !important;
            padding: 8px;
        }

            .buttonnew:hover {
                background: linear-gradient(to bottom,#90c14f 0%, #90c14f 44%, #90c14f 100%) !important;
            }

            .buttonnew span {
                cursor: pointer !important;
                display: inline-block !important;
                position: relative !important;
                transition: 0.4s !important;
            }

                .buttonnew span:after {
                    content: '\00bb' !important;
                    position: absolute !important;
                    opacity: 0 !important;
                    top: 0 !important;
                    right: -20px !important;
                    transition: 0.5s !important;
                }

            .buttonnew:hover span {
                padding-right: 25px !important;
            }

                .buttonnew:hover span:after {
                    opacity: 1 !important;
                    right: 0 !important;
                }
                 .darkPanlvisible,
        .darkPanl2,
        .darkPanl {
            max-height: 800px;
            position: fixed;
            left: 0px;
            top: 0px;
            background: url(../Images/dark.png) 0 0 !important;
            display: none;
            width: 100%;
            height: 100%;
            z-index: 10000;
        }

        .darkPanlvisible {
            display: block;
        }
        
        .panelRef {
            background-color: #FFFFFF;
            border: 3px solid #D8E5F8;
            margin: auto auto auto auto;
            overflow-x: hidden;
            overflow-y: hidden;
            left: 25%;
            top: 20%;
            padding: 10px 10px 10px 10px;
            position: absolute;
            margin-left: -30px;
            margin-top: -50px;
            width:50%;
        }

        .panelRef_l {
            background-color: #FFFFFF;
            border: 3px solid #D8E5F8;
            margin: auto auto auto auto;
            overflow-x: hidden;
            overflow-y: hidden;
            left: 50%;
            top: 50%;
            padding: 10px 10px 10px 10px;
            position: absolute;
            margin-left: -30px;
            margin-top: -50px;
        }

        iframe {
            border-width: 0px !important;
            border-style: none !important;
            border-color: none !important;
            border-image: none !important;
        }
    </style>

    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calendar mr-3"></i>
            <asp:Label ID="lblType" runat="server" Text="Upload ASC Timetable" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div>
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
                </div>
                <table id="tbl" runat="server" width="100%">
                    <tr>
                        <td width="20%">
                            <span class="field-label">Academic year</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAcdYear" runat="server" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td width="20%">
                            <span class="field-label">ASC Format</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlFormats" runat="server">
                                <asp:ListItem Value="format1">ASC Format 1</asp:ListItem>
                                <asp:ListItem Value="format2">ASC Format 2</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Timegrid</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlUploadFor" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                            <br />
                            <asp:LinkButton ID="lbtnAddTimeGrid" runat="server">Add / Edit Timegrid</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">File to upload </span>
                        </td>
                        <td width="30%">
                            <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="false" accept=".xml" />
                            <asp:Button ID="btnSave" runat="server" Text="Upload XML file" OnClientClick="OnClickupload();" OnClick="btnUpload_Click" CssClass="button" />
                            <asp:Label ID="lblStatus" Text="Files are uploading..." runat="server" Style="display: none"></asp:Label>

                            <script>
                                function OnClickupload() {

                                    document.getElementById("<%=btnSave.ClientID%>").style.visibility = 'hidden';
                                    document.getElementById("<%=lblStatus.ClientID%>").style.display = 'block';
                                }
                            </script>
                        </td>
                        <td width="20%">

                            <asp:Button ID="btnMap" runat="server" CssClass="button" Text="All Mappings" />

                        </td>
                        <td width="30%"></td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvtimetableAScSchedule" runat="server" CssClass="table table-bordered table-row" AllowPaging="true" PageSize="20" OnPageIndexChanging="gvtimetableAScSchedule_PageIndexChanging">
                            </asp:GridView>

                        </td>
                    </tr>

                </table>
                <asp:Panel ID="pnlAssmt" runat="server" BackColor="white" CssClass="panel-cover">
                    <div align="right" style="height: 30px;">
                        <asp:ImageButton ID="lnkcls" runat="server" ImageUrl="~/Images/close.png" AlternateText="Close" OnClientClick="ClosePanel();" Style="border-width: 0px; float: right; position: absolute; top: 0; right: 0;"></asp:ImageButton>
                    </div>

                    <asp:Button ID="btnMapClass" Text="Map Class" CssClass="buttonnew" runat="server" OnClientClick="ClosePanel();" OnClick="btnMapClass_Click" />
                    <asp:Button ID="btnMapTeacher" Text="Map Teacher" CssClass="buttonnew" runat="server" OnClientClick="ClosePanel();" OnClick="btnMapTeacher_Click" />
                    <asp:Button ID="btnMapSubject" Text="Map Subject" CssClass="buttonnew" runat="server" OnClientClick="ClosePanel();" OnClick="btnMapSubject_Click" />


                </asp:Panel>

                <ajaxToolkit:ModalPopupExtender ID="MPOI" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true" PopupControlID="pnlAssmt"
                    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="btnMap">
                </ajaxToolkit:ModalPopupExtender>
                <%--Added by vikranth on 29th July 2020--%>
                <asp:Panel ID="plReopen" runat="server" Visible="false" CssClass="darkPanlvisible">
                    <div class="panelRef">
                        <div class="msg_header">
                            <div class="msg">
                                <span class="title-bg">Add / Edit Timegrid</span>
                                <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;">
                                    <asp:ImageButton ID="Imageclose" runat="server" ImageUrl="../Images/closeme.png"
                                        Style="margin-top: -1px; vertical-align: top;" OnClientClick="hideTimeGrid()" /></span>
                            </div>
                        </div>
                        <br />
                        <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover"
                            ScrollBars="Both">
                            <div>
                                <table style="width: 100%">
                                    <tr>
                                        <td align="left" width="10%"><span class="field-label">Timegrid</span> </td>
                                        <td align="left" width="50%">
                                            <asp:TextBox ID="txtTGrid"
                                                runat="server"></asp:TextBox></td>
                                    </tr>
                                </table>
                                <div align="center">
                                    <asp:Button ID="btnAddSaveGrid" runat="server" Text="Add / Save" CssClass="button"
                                        ValidationGroup="time"></asp:Button>
                                    <asp:Button ID="btnUpdateSaveGrid" runat="server" CssClass="button"
                                        Text="Update / Save" ValidationGroup="time" />
                                    <asp:Button ID="btnCancelSaveGrid" runat="server" CssClass="button"
                                        Text="Cancel" ValidationGroup="time" />
                                </div>
                                <div align="left">
                                    <asp:Label ID="ltGridError" runat="server"></asp:Label>
                                </div>
                                <div>
                                    <asp:GridView ID="gvTime" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row"
                                        Width="100%">
                                        <RowStyle CssClass="griditem" />
                                        <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Timegrid">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTS_MULTI_TIMEGRID" runat="server" Text='<%# Bind("TS_MULTI_TIMEGRID") %>'></asp:Label>
                                                    <asp:Label ID="lblTS_ID" runat="server" Text='<%# Bind("TS_ID") %>' Visible="false"></asp:Label>

                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEditGRP" runat="server" OnClick="lbtnEditGRP_Click">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle BackColor="Khaki" />
                                        <HeaderStyle CssClass="gridheader_new" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfTS_ID" runat="server" Value="0" />
                <asp:HiddenField ID="hfDiv" runat="server" Value="0" />
                <%--End by vikanth on 29th July 2020--%>
            </div>
        </div>
    </div>
    <script>
        function ClosePanel() {

            document.getElementById("<%=pnlAssmt.ClientID%>").style.visibility = "hidden";
        }
        function hideTimeGrid() {
            document.getElementById("<%=plReopen.ClientID %>").style.display = 'none';
        }
    </script>
</asp:Content>
