﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ClassMap.aspx.vb" Inherits="TimetableIntegration_ClassMap" MasterPageFile="~/mainMasterPage.master" %>
<%@ Register src="~/TimetableASC/UserControl/ClassMap.ascx" TagName="Class_Map" TagPrefix="CLMap1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


     <script language="javascript" type="text/javascript">
         function validate(GridView1) {
             var flag = false;
             var dropdowns = new Array(); //Create array to hold all the dropdown lists.
             var imgArr = new Array(); //Create array to hold all the dropdown lists.
             var gridview = document.getElementById(GridView1); //GridView1 is the id of ur gridview.
             dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
             var imgArr = gridview.getElementsByTagName('img');

             for (var i = 0; i < dropdowns.length; i++) {
                 if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                 {
                    
                     imgArr.item(i).src = "../Images/arrowgreen.png"
                     flag = true;
                 }
                 else {
                     
                     imgArr.item(i).src = "../Images/arrowred.png"


                 }
             }
         }

    </script>



        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-clock-o mr-3"></i>
                <asp:Literal ID="ltHeader" runat="server" Text="CLASS MASTER"></asp:Literal>
            </div>
            <div class="card-body">
                <div class="table-responsive m-auto">


                    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                        cellspacing="0" width="100%">

                        <tr>
                            <td align="left" colspan="4">

                                <asp:Label ID="lblErr" runat="server" EnableViewState="false"></asp:Label>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <CLMap1:Class_Map ID="clMapclass1" runat="server" />
                            </td>
                        </tr>


                    </table>
                </div>
            </div>
        </div>

    </asp:content>

