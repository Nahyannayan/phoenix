﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="true" CodeFile="SubjectMap.aspx.vb"
    Inherits="TimetableIntegration_SubjectMap" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
       

        function validateSub() {
            var flag = false;
            var dropdowns = new Array(); //Create array to hold all the dropdown lists.
            var imgArr = new Array(); //Create array to hold all the dropdown lists.
            var gridview = document.getElementById('<%=gvSubjectMap.ClientID%>'); //GridView1 is the id of ur gridview.
            dropdowns = gridview.getElementsByTagName('select'); //Get all dropdown lists contained in GridView1.
            var imgArr = gridview.getElementsByTagName('img');

            for (var i = 0; i < dropdowns.length; i++) {

                if (dropdowns.item(i).className == "ddlanswer" && dropdowns.item(i).value != '-1') //If dropdown has no selected value
                {

                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowgreen.png"
                    flag = true;
                }
                else {

                    //imgArr.item(i).style.display="none"
                    imgArr.item(i).src = "../Images/arrowred.png"


                }
            }
        }



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-clock-o mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="SUBJECT MASTER"></asp:Literal>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>
                        <td align="left" colspan="4">
                            <span class="error">
                                <asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></span>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                        </td>
                    </tr>
                  

                    <tr>
                        <td colspan="4">

                            <asp:Panel ID="plSubjectMap" runat="server" ScrollBars="Vertical" Width="100%">
                                <asp:GridView ID="gvSubjectMap" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvSubjectMap_RowDataBound"
                                    CssClass="table table-bordered table-row" Height="100%"
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr.No">
                                            <ItemTemplate>

                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("R1") %>'></asp:Label>
                                            </ItemTemplate>

                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Class">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_GRD_ID" runat="server" Text='<%# Bind("GRADE")%>'></asp:Label>
                                                <asp:HiddenField ID="hidGRD_ID" runat="server" Value='<%# Bind("GRADE")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Stream">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_Stream" runat="server" Text='<%# Bind("STREAM")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate></EditItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Subject">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTTM_SUBJECT" runat="server" Text='<%# Bind("SUBJECT")%>'></asp:Label>
                                                <asp:HiddenField ID="hidTTM_SHORTCODE" runat="server" Value='<%# Bind("SHORTCODE")%>' />
                                                <asp:HiddenField ID="hidTTM_SBG_MAPPED_ID" runat="server" Value='<%# Bind("SBG_MAPPED_ID")%>' />
                                            </ItemTemplate>
                                            <EditItemTemplate></EditItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Map it with OASIS">
                                            <ItemTemplate>
                                                <asp:Image ID="imgMap" runat="server" ImageUrl="~/Images/arrowred.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OASIS Available Subject Group">
                                            <EditItemTemplate></EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlOasis_Subject_Name" runat="server" onchange="validateSub()" CssClass="ddlanswer">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" />
                                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center"
                                        VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </asp:Panel>

                          

                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                           
                            <asp:Button ID="btnSaveMap" runat="server" CssClass="button" Text="Process Mapping" OnClick="btnSaveMap_Click1" />
                            
                            <asp:Button ID="btnBack" runat="server" CausesValidation="False"
                                CssClass="button" Text="Back" />
                        </td>


                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

