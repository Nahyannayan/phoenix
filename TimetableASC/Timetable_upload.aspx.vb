﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Imports System.Collections
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Globalization
Imports System.Xml
Imports System.Net
Imports System.Xml.Linq

Partial Class TimetableASC_Timetable_upload
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowMessage("", True)
        If Session("sbsuid") = "" Then
            Response.Redirect("~\noAccess.aspx")
        Else
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnSave)
            Page.Form.Attributes.Add("enctype", "multipart/form-data")

            If IsPostBack = False Then
                BindAcademicYear()
                BindTIMETABLE_SET() 'Added by vikranth on 29th July 2020
                bindTimetableGrid()
            End If
        End If
    End Sub
    Protected Function GetLogId() As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetPhoenix_ASCConnectionString
            'Dim strsql As String = "select max(logid) from timetableschedules"
            Dim strsql As String = "select max([AutoID]) from [dbo].[UploadsLogs]"
            Dim logid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strsql)
            logid = logid + 1
            Return logid
        Catch ex As Exception
            Return 0
        End Try

    End Function
    Protected Sub BindAcademicYear()
        Dim dt As DataTable
        dt = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        ddlAcdYear.DataSource = dt
        ddlAcdYear.DataTextField = "ACY_DESCR"
        ddlAcdYear.DataValueField = "ACD_ID"
        ddlAcdYear.DataBind()
        For Each rowACD As DataRow In dt.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcdYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub

    Protected Sub bindTimetableGrid()
        '  TTASC_GetTimetableDetails
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetPhoenix_ASCConnectionString
        Dim sqlparam(3) As SqlParameter
        sqlparam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
        sqlparam(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcdYear.SelectedValue, SqlDbType.BigInt)
        Dim timegridId As Integer = 0
        If ddlUploadFor.SelectedValue <> "" Then
            timegridId = ddlUploadFor.SelectedValue
        End If
        sqlparam(2) = Mainclass.CreateSqlParameter("@timegrid_id", timegridId, SqlDbType.BigInt)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.TTASC_GetTimetableDetails", sqlparam)
        If ds.Tables.Count > 0 Then
            gvtimetableAScSchedule.DataSource = ds
            gvtimetableAScSchedule.DataBind()
            If (ds.Tables(0).Rows.Count > 0) Then
                btnMap.Visible = True
                pnlAssmt.Visible = True
            Else
                btnMap.Visible = False
                pnlAssmt.Visible = False
            End If
        End If
    End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs)
        Try
            '  ShowMessage("File is getting uploaded. Please wait....", False)
            'if file is existing in the upload
            If FileUpload1.HasFile Then
                'finding file in xdocument format
                Dim Doc As XDocument = XDocument.Load(FileUpload1.FileContent)
                Dim ds As New DataSet
                Dim reader As XmlReader = Doc.CreateReader
                'reading file xml into dataset ( in form of tables) 
                ds.ReadXml(reader)
                'upload method
                Dim logid As Integer = GetLogId()
                uploadfile(ds, logid)
            Else
                'error if file is missing in upload control
                ShowMessage("You must select xml file ", True)
            End If
        Catch ex As Exception
            ShowMessage("Error occured while upload", True)
        End Try

    End Sub
    Protected Sub CheckexistStatus(ByRef strans As SqlTransaction)
        Dim sqlparam(2) As SqlParameter
        sqlparam(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlAcdYear.SelectedValue, SqlDbType.Int)
        sqlparam(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("SBSUID"), SqlDbType.VarChar)
        sqlparam(2) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
        SqlHelper.ExecuteNonQuery(strans, "[dbo].[CHECK_TIMETABLE_EXISTS_ONUPLOAD]", sqlparam)
    End Sub

    Protected Sub uploadfile(ByVal ds As DataSet, ByVal logid As Integer)
        Dim str_conn As String = ConnectionManger.GetPhoenix_ASCConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction

            'check about the existing record, if exists then delete 

            CheckexistStatus(strans)

            ' Code starting for upload of xml to timetable asc tables 
            If ddlFormats.SelectedValue = "format1" Then
                'inserting into table 'periods'
                Insert_periods(ds.Tables("period"), strans, logid)

                'insreting into table 'daysdefs'
                Insert_daysdefs(ds.Tables("daysdef"), strans, logid)

                'inserting into table 'weeksdefs'
                Insert_weeksdefs(ds.Tables("weeksdef"), strans, logid)

                'inserting into table 'termsdefs'
                Insert_termsdefs(ds.Tables("termsdef"), strans, logid)

                'inserting into table 'subjects'
                Insert_subjects(ds.Tables("subject"), strans, logid)

                'inserting into table 'teachers'
                Insert_teachers(ds.Tables("teacher"), strans, logid)

                'inserting into table 'classrooms'
                Insert_classrooms(ds.Tables("classroom"), strans, logid)

                'inserting into table 'groups'
                Insert_groups(ds.Tables("group"), strans, logid)

                'inserting into table 'students'
                Insert_students(ds.Tables("student"), strans, logid)

                'insert into table 'studentsubjects'
                Insert_studentsubjects(ds.Tables("studentsubject"), strans, logid)

                'insert into table 'lessons'
                Insert_lessons(ds.Tables("lesson"), strans, logid)

                'inserting into table 'cards'
                Insert_cards(ds.Tables("card"), strans, logid)

                'inserting into table 'grades'
                Insert_grades(ds.Tables("grade"), strans, logid)

                'inserting into table 'classes'
                Insert_classes(ds.Tables("class"), strans, logid)

            ElseIf ddlFormats.SelectedValue = "format2" Then
                'inserting into table 'periods'
                Insert_periods(ds.Tables("period"), strans, logid)

                'inserting into table 'days'
                Insert_Days(ds.Tables("day"), strans, logid)

                'inserting into table 'subjects'
                Insert_subjects(ds.Tables("subject"), strans, logid)

                'inserting into table 'teachers'
                Insert_teachers(ds.Tables("teacher"), strans, logid)

                'inserting into table 'classrooms'
                Insert_classrooms(ds.Tables("classroom"), strans, logid)

                'inserting into table 'students'
                Insert_students(ds.Tables("student"), strans, logid)

                'insert into table 'studentsubjects'
                Insert_studentsubjects(ds.Tables("studentsubject"), strans, logid)

                'insert into table 'lessons'
                Insert_lessons(ds.Tables("lesson"), strans, logid)

                'inserting into table 'cards'
                Insert_cards(ds.Tables("card"), strans, logid)

                'inserting into table 'grades'
                Insert_grades(ds.Tables("grade"), strans, logid)

                'inserting into table 'classes'
                Insert_classes(ds.Tables("class"), strans, logid)

                'inserting into table 'TeacherSubjectClasses'
                Insert_TeacherSubjectClass(ds.Tables("TeacherSubjectClass"), strans, logid)

                'inserting into table 'TimeTableSchedules'
                Insert_TimeTableSchedules(ds.Tables("TimeTableSchedule"), strans, logid)

            End If

            strans.Commit()
            bindTimetableGrid()
            ShowMessage("File has been uploaded successfully ", False)
        Catch ex As Exception
            strans.Rollback()

        End Try
    End Sub

    Protected Sub Insert_periods(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'UPLOAD_PERIODS'
        '@name VARCHAR(50) = null,
        '@short VARCHAR(50) = null ,
        '@period INT ,
        '@starttime VARCHAR(5),
        '@endtime VARCHAR(5),
        '@schoolid VARCHAR(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(8) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@period", If(dt.Columns.Contains("period"), drow("period"), ""), SqlDbType.Int)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@starttime", If(dt.Columns.Contains("starttime"), drow("starttime"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@endtime", If(dt.Columns.Contains("endtime"), drow("endtime"), ""), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@schoolid", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_PERIODS", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub Insert_Days(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'create PROCEDURE UPLOAD_days
        '@name vARCHAR(100),
        '@short varchar(100),
        '@day varchar(100),
        '@School_Id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(6) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@day", If(dt.Columns.Contains("day"), drow("day"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@School_Id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_days", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try



    End Sub

    Protected Sub Insert_daysdefs(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'CREATE PROCEDURE UPLOAD_daysdef
        '@id VARCHAR(16),
        '@name VARCHAR(50),
        '@short VARCHAR(40),
        '@days vARCHAR(40),
        '@schoolid VARCHAR(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(7) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@days", If(dt.Columns.Contains("days"), drow("days"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@schoolid", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_daysdef", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub Insert_weeksdefs(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_weeksdef
        '@id VARCHAR(16),
        '@name VARCHAR(50),
        '@short VARCHAR(40),
        '@weeks int,
        '@schoolid VARCHAR(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(6) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@weeks", If(dt.Columns.Contains("weeks"), drow("weeks"), ""), SqlDbType.Int)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@schoolid", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_weeksdef", sqlparam)
                    Next
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Insert_termsdefs(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_termsdefs
        '@id VARCHAR(16),
        '@name VARCHAR(50),
        '@short VARCHAR(40),
        '@terms int,
        '@schoolid VARCHAR(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(7) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@terms", If(dt.Columns.Contains("terms"), drow("terms"), ""), SqlDbType.Int)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@schoolid", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)

                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_termsdefs", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Insert_subjects(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_subjects
        '@id VARCHAR(16),
        '@name VARCHAR(60),
        '@short VARCHAR(60),
        '@partnerid vARCHAR(60) = null,
        '@schoolid VARCHAR(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(6) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@partnerid", If(dt.Columns.Contains("partner_id"), drow("partner_id"), ""), SqlDbType.VarChar)
                        'sqlparam(3) = Mainclass.CreateSqlParameter("@partnerid", "0", SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@schoolid", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        'sqlparam(7) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_subjects", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub Insert_teachers(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'ALTER PROCEDURE UPLOAD_teacher
        '@id VARCHAR(16),
        '@firstname VARCHAR(50) = null,
        '@lastname VARCHAR(50) = null,
        '@name vARCHAR(50),
        '@short VARCHAR(50),
        '@gender varchar(50),
        '@color varchar(50),
        '@email varchar(50) = null,
        '@mobile varchar(50) = null,
        '@partnerid varchar(50) = null,
        '@school_id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(13) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@firstname", If(dt.Columns.Contains("firstname"), drow("firstname"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@lastname", If(dt.Columns.Contains("lastname"), drow("lastname"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@gender", If(dt.Columns.Contains("gender"), drow("gender"), ""), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@color", If(dt.Columns.Contains("color"), drow("color"), ""), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@email", If(dt.Columns.Contains("email"), drow("email"), ""), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@mobile", If(dt.Columns.Contains("mobile"), drow("mobile"), ""), SqlDbType.VarChar)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@partnerid", If(dt.Columns.Contains("partner_id"), drow("partner_id"), ""), SqlDbType.VarChar)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(11) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(12) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(13) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_teacher", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Insert_classrooms(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'ALTER PROCEDURE UPLOAD_classrooms
        '@id VARCHAR(16),
        '@name vARCHAR(50),
        '@short VARCHAR(50),
        '@capacity varchar(50),
        '@partnerid varchar(50) = null,
        '@school_id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(8) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@capacity", If(dt.Columns.Contains("capacity"), drow("capacity"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@partnerid", If(dt.Columns.Contains("partner_id"), drow("partner_id"), ""), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_classrooms", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Insert_groups(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'create PROCEDURE UPLOAD_groups
        '@id VARCHAR(16),
        '@name vARCHAR(50),
        '@classid VARCHAR(16),
        '@studentids varchar(50),
        '@entireclass int,
        '@divisiontag int,
        '@studentcount varchar(30),
        '@school_id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(10) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@classid", If(dt.Columns.Contains("classid"), drow("classid"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@studentids", If(dt.Columns.Contains("studentids"), drow("studentids"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@entireclass", If(dt.Columns.Contains("entireclass"), drow("entireclass"), ""), SqlDbType.Int)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@divisiontag", If(dt.Columns.Contains("divisiontag"), drow("divisiontag"), ""), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@studentcount", If(dt.Columns.Contains("studentcount"), drow("studentcount"), ""), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_groups", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Insert_students(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'ALTER PROCEDURE UPLOAD_students
        '@id varchar(60),
        '@classid VARCHAR(60),
        '@name vARCHAR(60),
        '@number int = null,
        '@email varchar(60) = null,
        '@mobile varchar(60) = null,
        '@partner_id varchar(60) = null,
        '@firstname varchar(60) = null,
        '@lastname varchar(60) = null,
        '@school_id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(11) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@classid", If(dt.Columns.Contains("classid"), drow("classid"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@number", If(dt.Columns.Contains("number"), drow("number"), ""), SqlDbType.Int)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@email", If(dt.Columns.Contains("email"), drow("email"), ""), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@mobile", If(dt.Columns.Contains("mobile"), drow("mobile"), ""), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@partner_id", If(dt.Columns.Contains("partner_id"), drow("partner_id"), ""), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@firstname", If(dt.Columns.Contains("firstname"), drow("firstname"), ""), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@lastname", If(dt.Columns.Contains("lastname"), drow("lastname"), ""), SqlDbType.VarChar)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(11) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_students", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Insert_studentsubjects(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_studentsubjects
        '@subjectid VARCHAR(60),
        '@seminargroup vARCHAR(60),
        '@importance vARCHAR(60) = null,
        '@alternatefor varchar(60) = null,
        '@school_id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(6) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@subjectid", If(dt.Columns.Contains("subjectid"), drow("subjectid"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@seminargroup", If(dt.Columns.Contains("seminargroup"), drow("seminargroup"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@importance", If(dt.Columns.Contains("importance"), drow("importance"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@alternatefor", If(dt.Columns.Contains("alternatefor"), drow("alternatefor"), ""), SqlDbType.Int)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_studentsubjects", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Insert_lessons(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_lessons
        '@id VARCHAR(60),
        '@classids vARCHAR(50),
        '@subjectid vARCHAR(16),
        '@periodspercard int = null,
        '@periodsperweek numeric(3,1),
        '@teacherids varchar(50) = null,
        '@classroomids nvarchar(max) = null,
        '@groupids varchar(50) = null,
        '@capacity varchar(50),
        '@seminargroup varchar(50),
        '@termsdefid varchar(50) = null,
        '@weeksdefid varchar(50) = null,
        '@daysdefid varchaR(50) = null,
        '@partnerid varchar(50) = null,
        '@school_id varchar(50),
        '@teacherid varchar(50) =null,
        '@studentids varchar(50) = null
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(19) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@classids", If(dt.Columns.Contains("classids"), drow("classids"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@subjectid", If(dt.Columns.Contains("subjectid"), drow("subjectid"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@periodspercard", If(dt.Columns.Contains("periodspercard"), drow("periodspercard"), 0), SqlDbType.Int)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@periodsperweek", If(dt.Columns.Contains("periodsperweek"), drow("periodsperweek"), ""), SqlDbType.Decimal)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@teacherids", If(dt.Columns.Contains("teacherids"), drow("teacherids"), ""), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@classroomids", If(dt.Columns.Contains("classroomids"), drow("classroomids"), ""), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@groupids", If(dt.Columns.Contains("groupids"), drow("groupids"), ""), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@capacity", If(dt.Columns.Contains("capacity"), drow("capacity"), ""), SqlDbType.VarChar)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@seminargroup", If(dt.Columns.Contains("seminargroup"), drow("seminargroup"), ""), SqlDbType.VarChar)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@termsdefid", If(dt.Columns.Contains("termsdefid"), drow("termsdefid"), ""), SqlDbType.VarChar)
                        sqlparam(11) = Mainclass.CreateSqlParameter("@weeksdefid", If(dt.Columns.Contains("weeksdefid"), drow("weeksdefid"), ""), SqlDbType.VarChar)
                        sqlparam(12) = Mainclass.CreateSqlParameter("@daysdefid", If(dt.Columns.Contains("daysdefid"), drow("daysdefid"), ""), SqlDbType.VarChar)
                        sqlparam(13) = Mainclass.CreateSqlParameter("@partnerid", If(dt.Columns.Contains("partner_id"), drow("partner_id"), ""), SqlDbType.VarChar)
                        sqlparam(14) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(15) = Mainclass.CreateSqlParameter("@teacherid", If(dt.Columns.Contains("teacherid"), If(drow("teacherid") Is Nothing, "", drow("teacherid")), ""), SqlDbType.VarChar)
                        sqlparam(16) = Mainclass.CreateSqlParameter("@studentids", If(dt.Columns.Contains("studentids"), If(drow("studentids") Is Nothing, "", drow("studentids")), ""), SqlDbType.VarChar)
                        sqlparam(17) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(18) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(19) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_lessons", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Insert_cards(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_cards
        '@lessonid VARCHAR(60),
        '@classroomids vARCHAR(60) = null,
        '@period int,
        '@weeks int = null,
        '@terms int = null,
        '@days int = null,
        '@school_id varchar(50),
        '@day varchar(50) = null,
        '@subjectid varchar(50) = null,
        '@teacherid varchar(50) = null,
        '@classroomid varchar(50) = null,
        '@classids varchar(50) = null,
        '@studentids varchar(50) = null
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(15) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@lessonid", If(dt.Columns.Contains("lessonid"), drow("lessonid"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@classroomids", If(dt.Columns.Contains("classroomids"), drow("classroomids"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@period", If(dt.Columns.Contains("period"), drow("period"), 0), SqlDbType.Int)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@weeks", If(dt.Columns.Contains("weeks"), drow("weeks"), 0), SqlDbType.Int)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@terms", If(dt.Columns.Contains("terms"), drow("terms"), 0), SqlDbType.Int)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@days", If(dt.Columns.Contains("days"), drow("days"), 0), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@school_id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@day", If(dt.Columns.Contains("day"), drow("day"), ""), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@subjectid", If(dt.Columns.Contains("subjectid"), drow("subjectid"), ""), SqlDbType.VarChar)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@teacherid", If(dt.Columns.Contains("teacherid"), drow("teacherid"), ""), SqlDbType.VarChar)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@classroomid", If(dt.Columns.Contains("classroomid"), drow("classroomid"), ""), SqlDbType.VarChar)
                        sqlparam(11) = Mainclass.CreateSqlParameter("@classids", If(dt.Columns.Contains("classids"), drow("classids"), ""), SqlDbType.VarChar)
                        sqlparam(12) = Mainclass.CreateSqlParameter("@studentids", If(dt.Columns.Contains("studentids"), drow("studentids"), ""), SqlDbType.VarChar)
                        sqlparam(13) = Mainclass.CreateSqlParameter("@ACD_ID", CInt(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(14) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(15) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_cards", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Insert_grades(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_grades
        '@id VARCHAR(60),
        '@name vARCHAR(60),
        '@short varchar(50) = null,
        '@grade varchar(50) =  null,
        '@School_Id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(8) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@grade", If(dt.Columns.Contains("grade"), drow("grade"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@School_Id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@studentids", If(dt.Columns.Contains("studentids"), drow("studentids"), ""), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@acd_id", CInt(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_grades", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Insert_classes(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'alter PROCEDURE UPLOAD_classes
        '@id VARCHAR(16),
        '@name vARCHAR(60),
        '@short varchar(50),
        '@teacherid varchar(20),
        '@classroomids varchar(20) = null,
        '@grade varchar(50) = null,
        '@partnerid varchar(30) = null,
        '@School_Id varchar(50),
        '@gradeid varchar(50) = null
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(11) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@id", If(dt.Columns.Contains("id"), drow("id"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@name", If(dt.Columns.Contains("name"), drow("name"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@short", If(dt.Columns.Contains("short"), drow("short"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@teacherid", If(dt.Columns.Contains("teacherid"), drow("teacherid"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@classroomids", If(dt.Columns.Contains("classroomids"), drow("classroomids"), ""), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@grade", If(dt.Columns.Contains("grade"), drow("grade"), ""), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@partnerid", If(dt.Columns.Contains("partner_id"), drow("partner_id"), ""), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@School_Id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@gradeid", If(dt.Columns.Contains("gradeid"), drow("gradeid"), ""), SqlDbType.VarChar)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.BigInt)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.BigInt)
                        sqlparam(11) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_classes", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Insert_TeacherSubjectClass(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'create PROCEDURE UPLOAD_TeacherSubjectClasses
        '@optionalClassID vARCHAR(100),
        '@subjectGradeId varchar(100),
        '@classid varchar(100),
        '@teacherid varchar(100),
        '@School_Id varchar(50)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(7) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@optionalClassID", If(dt.Columns.Contains("optionalClassID"), drow("optionalClassID"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@subjectGradeId", If(dt.Columns.Contains("subjectGradeId"), drow("subjectGradeId"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@classid", If(dt.Columns.Contains("classid"), drow("classid"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@teacherid", If(dt.Columns.Contains("teacherid"), drow("teacherid"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@School_Id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@acd_id", Convert.ToInt32(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_TeacherSubjectClasses", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Insert_TimeTableSchedules(ByVal dt As DataTable, ByRef strans As SqlTransaction, ByVal logid As Integer)
        'create PROCEDURE UPLOAD_TimeTableSchedules
        '@DayID vARCHAR(100),
        '@Period varchar(100),
        '@LengthID varchar(100),
        '@SchoolRoomID varchar(100),
        '@SubjectGradeID varchar(50),
        '@ClassID varchar(100),
        '@OptionalClassID varchar(100),
        '@TeacherID varchar(100),
        '@School_ID varchar(100)
        Try
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    For Each drow In dt.Rows
                        Dim sqlparam(11) As SqlParameter
                        sqlparam(0) = Mainclass.CreateSqlParameter("@DayID", If(dt.Columns.Contains("DayID"), drow("DayID"), ""), SqlDbType.VarChar)
                        sqlparam(1) = Mainclass.CreateSqlParameter("@Period", If(dt.Columns.Contains("Period"), drow("Period"), ""), SqlDbType.VarChar)
                        sqlparam(2) = Mainclass.CreateSqlParameter("@LengthID", If(dt.Columns.Contains("LengthID"), drow("LengthID"), ""), SqlDbType.VarChar)
                        sqlparam(3) = Mainclass.CreateSqlParameter("@SchooloomID", If(dt.Columns.Contains("SchoolRoomID"), drow("SchoolRoomID"), ""), SqlDbType.VarChar)
                        sqlparam(4) = Mainclass.CreateSqlParameter("@SubjectGradeID", If(dt.Columns.Contains("SubjectGradeID"), drow("SubjectGradeID"), ""), SqlDbType.VarChar)
                        sqlparam(5) = Mainclass.CreateSqlParameter("@ClassID", If(dt.Columns.Contains("ClassID"), drow("ClassID"), ""), SqlDbType.VarChar)
                        sqlparam(6) = Mainclass.CreateSqlParameter("@OptionalClassID", If(dt.Columns.Contains("OptionalClassID"), drow("OptionalClassID"), ""), SqlDbType.VarChar)
                        sqlparam(7) = Mainclass.CreateSqlParameter("@TeacherID", If(dt.Columns.Contains("TeacherID"), drow("TeacherID"), ""), SqlDbType.VarChar)
                        sqlparam(8) = Mainclass.CreateSqlParameter("@School_Id", Session("sbsuid"), SqlDbType.VarChar)
                        sqlparam(9) = Mainclass.CreateSqlParameter("@acd_id", CInt(ddlAcdYear.SelectedValue), SqlDbType.Int)
                        sqlparam(10) = Mainclass.CreateSqlParameter("@log_id", logid, SqlDbType.Int)
                        sqlparam(11) = Mainclass.CreateSqlParameter("@timegrid_id", Convert.ToInt32(ddlUploadFor.SelectedValue), SqlDbType.Int)
                        SqlHelper.ExecuteNonQuery(strans, "UPLOAD_TimeTableSchedules", sqlparam)
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvtimetableAScSchedule_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvtimetableAScSchedule.PageIndex = e.NewPageIndex
        bindTimetableGrid()
    End Sub

    Protected Sub ddlAcdYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        bindTimetableGrid()
    End Sub

    Protected Sub btnMapClass_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("~/TimetableASC/ClassMap.aspx?ACDID={0}&Timegrid_ID={1}", Encr_decrData.Encrypt(ddlAcdYear.SelectedValue), Encr_decrData.Encrypt(ddlUploadFor.SelectedValue))
        Response.Redirect(URL)
    End Sub

    Protected Sub btnMapTeacher_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("~/TimetableASC/StaffMap.aspx?ACDID={0}&Timegrid_ID={1}", Encr_decrData.Encrypt(ddlAcdYear.SelectedValue), Encr_decrData.Encrypt(ddlUploadFor.SelectedValue))
        Response.Redirect(URL)
    End Sub

    Protected Sub btnMapSubject_Click(sender As Object, e As EventArgs)
        Dim URL As String = String.Format("~/TimetableASC/SubjectMap.aspx?ACDID={0}&Timegrid_ID={1}", Encr_decrData.Encrypt(ddlAcdYear.SelectedValue), Encr_decrData.Encrypt(ddlUploadFor.SelectedValue))
        Response.Redirect(URL)
    End Sub

    'Added by vikranth on 29th July 2020
    Protected Sub lbtnAddTimeGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAddTimeGrid.Click

        resettime()
        GRIDBIND_Time()
        If gvTime.Rows.Count > 0 Then
            gvTime.Columns(1).Visible = True
        End If

        plReopen.Visible = True
    End Sub

    Private Sub resettime()
        ltGridError.Text = ""
        hfTS_ID.Value = "0"
        txtTGrid.Text = ""
        btnAddSaveGrid.Visible = True
        btnUpdateSaveGrid.Visible = False
        btnCancelSaveGrid.Visible = False
    End Sub
    Private Sub GRIDBIND_Time()
        Try
            Dim STATUS As String = String.Empty

            Dim ds As New DataSet

            Dim str_conn As String = ConnectionManger.GetPhoenix_ASCConnectionString
            Dim sqlparam(1) As SqlParameter
            sqlparam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETTIMETABLE_SETTINGS", sqlparam)

            If ds.Tables(0).Rows.Count > 0 Then


                gvTime.DataSource = ds.Tables(0)
                gvTime.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)

                'ds.Tables(0).Rows(0)("flag") = False

                gvTime.DataSource = ds.Tables(0)
                Try
                    gvTime.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTime.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvTime.Rows(0).Cells.Clear()

                gvTime.Rows(0).Cells.Add(New TableCell)
                gvTime.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTime.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTime.Rows(0).Cells(0).Text = "No timegrid available !!!"

            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub BindTIMETABLE_SET()
        Dim str_conn As String = ConnectionManger.GetPhoenix_ASCConnectionString
        Dim str_Sql As String
        ddlUploadFor.Items.Clear()

        str_Sql = "SELECT TS_ID,TS_MULTI_TIMEGRID FROM dbo.TIMETABLE_SETTINGS WITH(NOLOCK) WHERE TS_BSU_ID='" & Session("sBsuid") & "'"

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then

            ddlUploadFor.DataSource = ds.Tables(0)
            ddlUploadFor.DataTextField = "TS_MULTI_TIMEGRID"
            ddlUploadFor.DataValueField = "TS_ID"
            ddlUploadFor.DataBind()
        Else
            ddlUploadFor.Items.Add(New ListItem("Single Timegrid", "0"))


        End If


    End Sub

    Protected Sub btnCancelSaveGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSaveGrid.Click

        resettime()
        gvTime.Columns(1).Visible = True
        plReopen.Visible = True
    End Sub
    Protected Sub btnAddSaveGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSaveGrid.Click
        If Page.IsValid = True Then
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            If txtTGrid.Text.Trim <> "" Then


                str_err = calltransaction_time(errorMessage)
                If str_err = "0" Then
                    resettime()
                    gvTime.Columns(1).Visible = True
                    BindTIMETABLE_SET()
                    GRIDBIND_Time()
                    ltGridError.Text = "Record Saved Successfully"

                Else
                    ltGridError.Text = errorMessage
                End If
            Else
                ltGridError.Text = "Timegrid cannot be left empty"
            End If
        End If

        plReopen.Visible = True

    End Sub
    Protected Sub lbtnEditGRP_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim TS_TOT_WEEK_DAY As String = String.Empty
            Dim TS_START_WEEK As String = String.Empty
            Dim TS_ID As String = String.Empty
            Dim TS_MULTI_TIMEGRID As String = String.Empty

            hfTS_ID.Value = TryCast(sender.FindControl("lblTS_ID"), Label).Text
            txtTGrid.Text = TryCast(sender.FindControl("lblTS_MULTI_TIMEGRID"), Label).Text

            btnAddSaveGrid.Visible = False
            btnUpdateSaveGrid.Visible = True
            btnCancelSaveGrid.Visible = True
            gvTime.Columns(1).Visible = False

            plReopen.Visible = True
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnUpdateSaveGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSaveGrid.Click
        If Page.IsValid = True Then
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            If txtTGrid.Text.Trim <> "" Then

                str_err = calltransaction_time(errorMessage)
                If str_err = "0" Then
                    resettime()
                    gvTime.Columns(1).Visible = True
                    BindTIMETABLE_SET()
                    GRIDBIND_Time()
                    ltGridError.Text = "Record Saved Successfully"

                Else
                    ltGridError.Text = errorMessage
                End If
            Else
                ltGridError.Text = "Timegrid cannot be left empty"
            End If
        End If


        plReopen.Visible = True
    End Sub

    Function calltransaction_time(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean

        Dim GRD_IDs As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim str As String = String.Empty


        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetPhoenix_ASCConnection
            'Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Phoenix_ASC").ConnectionString)
            'conn.Open()

            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                bEdit = False

                Dim PARAM(3) As SqlParameter

                PARAM(0) = New SqlParameter("@TS_BSU_ID", BSU_ID)
                PARAM(1) = New SqlParameter("@TS_MULTI_TIMEGRID", txtTGrid.Text.Trim)
                PARAM(2) = New SqlParameter("@TS_ID", hfTS_ID.Value)
                PARAM(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(3).Direction = ParameterDirection.ReturnValue


                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVETIMETABLE_SETTING", PARAM)

                status = CInt(PARAM(3).Value)


                If status <> 0 Then
                    calltransaction_time = "1"
                    errorMessage = "Error Occured While Saving."
                    Return "1"
                End If


                calltransaction_time = "0"


            Catch ex As Exception
                calltransaction_time = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction_time <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using

    End Function
    Protected Sub Imageclose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imageclose.Click
        plReopen.Visible = False
    End Sub
    Protected Sub ddlUploadFor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUploadFor.SelectedIndexChanged
        bindTimetableGrid()
    End Sub
End Class
