﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Forcepasswordchange
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "CheckPasswordStrength", "CheckPasswordStrength('" + txtNewpassword.Text + "');", True)
            If hdn_chk.Value <> "1" Then
                Exit Sub
            End If
            If txtCurrentPassword.Text = txtNewpassword.Text Then
                lblError.Text = "Current Password and New Password are Same!!!Please Re Enter the Passwords"
                txtCurrentPassword.Focus()
                Exit Sub
            End If
            If validate_pwd() > 0 Then
                lblError.Text = "Passwords must not contain the username's consecutive 3 characters "

                Exit Sub
            End If

            If Not UtilityObj.PasswordVerify(txtConfPassword.Text, 2, 0, 2) Then
                lblError.Text = UtilityObj.getErrorMessage("781")
                Exit Sub
            End If
            If pwd_HISTORY() = 0 Then
                lblError.Text = "Password needs to be 8 characters or more and avoid last three passwords used "

                Exit Sub
            End If
            Try
                Dim status As Integer
                Dim transaction As SqlTransaction
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        'call the class to insert user password
                        status = UtilityObj.VerifyandUpdatePassword(Encr_decrData.Encrypt(txtCurrentPassword.Text), _
                        Encr_decrData.Encrypt(txtNewpassword.Text), Session("sUsr_name"), transaction)
                        If status = 0 Then
                            status = UtilityObj.operOnAudiTable("Change Password", Session("sUsr_name"), "Password Updated", Page.User.Identity.Name.ToString, Me.Page)
                        End If
                        If status = 0 Then
                            lblError.Text = String.Format("{0}, your password is updated successfully", Session("sUsr_name").ToUpper)
                        Else
                            lblError.Text = UtilityObj.getErrorMessage(status)
                        End If
                        transaction.Commit()


                        status = VerifyandUpdatePassword(Encr_decrData.Encrypt(txtNewpassword.Text), Session("sUsr_name"))
                        Dim vGLGUPDPWD As New com.ChangePWDWebService
                        vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                        Dim respon As String = vGLGUPDPWD.ChangePassword(Session("sUsr_name"), txtNewpassword.Text, txtNewpassword.Text)
                        Response.Redirect("~/login.aspx")
                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message)
                        transaction.Rollback()
                        lblError.Text = "Error while updating your password"
                    End Try
                End Using
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Change password")
                lblError.Text = UtilityObj.getErrorMessage("1000")
            End Try
        End If
    End Sub

    Sub get_message()

        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
       
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "get_password_chang_text", pParms)

            While DATAREADER.Read

                lblMessage.Text = Convert.ToString(DATAREADER("Message"))
            End While
        End Using
    End Sub
    Public Function pwd_HISTORY() As Integer
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
        pParms(2) = New SqlClient.SqlParameter("@PWD", txtNewpassword.Text)

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "SAVE_PASSWORD_LOG", pParms)

            While DATAREADER.Read
                i = Convert.ToString(DATAREADER("STATUS_VALUE"))
            End While
        End Using
        Return i
    End Function
    Public Function validate_pwd() As Integer
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@username", Session("sUsr_name"))
        pParms(1) = New SqlClient.SqlParameter("@pwd", txtNewpassword.Text)

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "get_password_strength", pParms)

            While DATAREADER.Read
                i = Convert.ToString(DATAREADER("STATUS_VALUE"))
            End While
        End Using
        Return i
    End Function
    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, _
                    ByVal username As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value
        End Using
    End Function
    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtCurrentPassword.Text = ""
        txtConfPassword.Text = ""
        txtNewpassword.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/login.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        get_message()
    End Sub
End Class
