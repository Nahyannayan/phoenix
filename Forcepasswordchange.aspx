﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Forcepasswordchange.aspx.vb" 
Title="::::GEMS OASIS:::: Online Student Administration System::::"
Inherits="Forcepasswordchange" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    
   <script language="javascript" type="text/javascript">
       self.moveTo(0, 0); self.resizeTo(screen.availWidth, screen.availHeight);

       function CheckPasswordStrength(password) {



           //Regular Expressions.
           var regex = new Array();
           regex.push("[A-Z]"); //Uppercase Alphabet.
           regex.push("[a-z]"); //Lowercase Alphabet.
           regex.push("[0-9]"); //Digit.
           regex.push("[$@$!%*#?&]"); //Special Character.

           var passed = 0;

           //Validate for each Regular Expression.
           for (var i = 0; i < regex.length; i++) {
               if (new RegExp(regex[i]).test(password)) {
                   passed++;
               }
           }

           //Validate for length of Password.

           if (passed > 2 && password.length >= 8) {

               document.getElementById("<%=lblError.ClientID%>").innerHTML = "";
            document.getElementById("<%=hdn_chk.ClientID%>").value = "1";

        }

        else {
            document.getElementById("<%=lblError.ClientID%>").innerHTML = "Passwords must meet the following minimum requirements:<ul><li>Not contain the user's account name or parts of the user's full name that exceed two consecutive characters</li><li>Be at least eight characters in length</li></ul>Contain characters from three of the following four categories:<ul><li>English uppercase characters (A through Z)</li><li>English lowercase characters (a through z)</li><li>Base 10 digits (0 through 9)</li><li>Non-alphabetic characters (for example, !, $, #, %)</li></ul>";
            document.getElementById("<%=hdn_chk.ClientID%>").value = "0";
        }



    }

    </script>
     <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon">
    <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" defaultfocus="txtCurrentPassword">
        <asp:HiddenField id="hdn_chk" runat="server"/>
    <table style="border-collapse: collapse;" align="center">
            <tr>
                <td style="width: 763px">
                    <img src="images/loginImage/head.jpg" /></td>
            </tr>
            <tr>
                <td style="width: 763px; height: 23px;">
                </td>
            </tr>
            <tr>
                <td style="width: 763px; background-image: url(images/loginImage/background1.jpg);
                    background-repeat: repeat; height: 20%;">
                             <table style="border-collapse:collapse;width: 100%; height: 157px" cellpadding="0" cellspacing="0">
                        <tr> 
                            <td style=" background-image: url(Images/loginImage/background-MAIN.jpg);font-weight: bold; font-size: 16px; font-family: Arial,Helvetica, sans-serif;  background-repeat: no-repeat; height: 46px;" align="center" valign="middle">
                                </td>
                        </tr>
                        <tr style="font-size: 12pt; font-family: Times New Roman">
                            <td colspan="2" style="background-image: url(Images/loginImage/background-MAIN.jpg); background-repeat: no-repeat; height: 204px; background-position: center center;" align="center">
                                           <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="Server">
                                            </ajaxToolkit:ToolkitScriptManager>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
    cellspacing="0">
    <tr>
    <td class="matters" align="left">
    <asp:Label ID="lblMessage" runat="server"  ></asp:Label><br><br>
    </td>
    </tr>
    <tr>
        <td align="left" valign="bottom">
            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Text="Passwords must meet the following minimum requirements:<ul><li>Not contain the user's account name or parts of the user's full name that exceed two consecutive characters</li><li>Be at least eight characters in length</li></ul>Contain characters from three of the following four categories:<ul><li>English uppercase characters (A through Z)</li><li>English lowercase characters (a through z)</li><li>Base 10 digits (0 through 9)</li><li>Non-alphabetic characters (for example, !, $, #, %)</li></ul>"></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error" />
            <ajaxToolkit:PasswordStrength ID="PasswordStrength1" runat="server" DisplayPosition="RightSide"
                HelpStatusLabelID="TextBox1_HelpLabel" PreferredPasswordLength="8"
                PrefixText="&nbsp;Strength:"   StrengthIndicatorType="Text"
                StrengthStyles="red;blue;grey;yellow;green" TargetControlID="txtNewpassword"
                TextStrengthDescriptions="Very Poor&nbsp;;Weak&nbsp;;Average&nbsp;;Strong;Excellent">
            </ajaxToolkit:PasswordStrength>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table align="center" border="0" cellpadding="5" cellspacing="0" class="BlueTable">
                <tr class="subheader_img">
                    <td align="left" colspan="3" style="height: 19px" valign="middle">
                        Change your Password
                    </td>
                </tr>
                <tr>
                    <td align="left" class="matters">
                        Current Password</td>
                    <td class="matters">
                        :</td>
                    <td align="left" class="matters" style="width: 324px">
                        <asp:TextBox ID="txtCurrentPassword" runat="server" CssClass="inputbox" MaxLength="100"
                            TextMode="Password" Width="161px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtCurrentPassword"
                            CssClass="error" Display="Dynamic" ErrorMessage="CurrentPassword can not be left empty"
                            ForeColor="" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="matters">
                        New Password</td>
                    <td class="matters">
                        :
                       


                        </td>
                    <td align="left" class="matters" style="width: 324px">
                        <asp:TextBox ID="txtNewpassword" runat="server" CssClass="inputbox" MaxLength="100"
                             TextMode="Password" Width="161px" onkeyup="CheckPasswordStrength(this.value)"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewpassword"
                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter your new password"
                            ForeColor="">*</asp:RequiredFieldValidator><br />
                        <asp:Label ID="TextBox1_HelpLabel" runat="server" CssClass="error_password"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" class="matters">
                        Confirm New Password</td>
                    <td class="matters">
                        :</td>
                    <td align="left" class="matter" style="width: 324px">
                        <asp:TextBox ID="txtConfPassword" runat="server" CssClass="inputbox" TextMode="Password"
                            Width="162px"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewpassword"
                            ControlToValidate="txtConfPassword" CssClass="error" Display="Dynamic" ErrorMessage="Password do not match"
                            ForeColor="">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfPassword"
                            CssClass="error" Display="Dynamic" ErrorMessage="Confirm password can not be left empty"
                            ForeColor="">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td align="center" class="matters" colspan="3">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                        <asp:Button ID="btnClear" runat="server" CausesValidation="False" CssClass="button"
                            Text="Clear" />
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                            Text="Cancel" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
         </td>
                        </tr>
                    </table>
 </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td style="width: 763px; background-image: url(images/loginImage/background1.jpg);
                    background-repeat: repeat; height: 126px; text-align: center;">
                    <img src="images/loginImage/allschools4.jpg" width="757" align="middle" />
                </td>
            </tr>
            <tr style="font-size: 12pt; font-family: Times New Roman">
                <td align="center" style="width: 763px; height: 15px; background-image: url(images/loginImage/background1.jpg);
                    background-repeat: repeat;">
                    <span style="letter-spacing: 3px; font-family: Arial Rounded MT Bold; font-size: 14px;
                        color: #0066cc;">GEMS - IT</span></td>
            </tr>
            <tr>
                <td style="background-image: url(images/loginImage/footerSep.gif); width: 763px;
                    height: 10px">
                </td>
            </tr>
        </table>
  
    </form>
</body>
</html>
