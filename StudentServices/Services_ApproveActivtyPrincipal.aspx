﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services_ApproveActivtyPrincipal.aspx.vb" Inherits="StudentServices_Services_ApproveActivtyPrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <style>
        .itemforgrid {
            width: 700px !important;
            word-wrap: break-word !important;
            word-break: normal !important;
            padding-left: 10px !important;
            display: block !important;
        }

        .gradeshowS_CAL {
            background-color: #fff !important;
            position: absolute;
            /*font-family: Verdana !important;*/
            font-weight: bold !important;
            /*font-size: 10px !important;*/
            left: 10px !important;
            top: 60px !important;
            opacity: 1;
        }

            .gradeshowS_CAL.fade {
                opacity: 0.5;
            }


        img {
            width: auto !important;
        }

        /*.textareaS_CAL {
            width: 100%;
            height: 100px;
            padding: 5px 5px 5px 5px;
            box-sizing: border-box;
            border: 1px solid #1B80B6 !important;
            border-radius: 4px;
            font-family: Verdana !important;
            background-color: white;
            font-family: Verdana !important;
            font-size: small !important;
        }*/

        .lblShowS_CAL {
            background-color: lightblue;
            border: 2px solid #e7e7e7;
            color: white;
            padding: 10px 22px;
            text-align: center;
            border-radius: 4px;
            text-decoration: none;
            display: inline-block;
            /*font-size: 12px;*/
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            cursor: pointer;
        }

            .lblShowS_CAL:hover {
                background-color: #e7e7e7;
            }



        .clsBodyS_CAL {
            border: double 1px;
            padding: 10px;
        }
    </style>
</head>

<body class="bg-dark bg-white m-2">

    <form id="formPrincipalApprovalAct" runat="server">

        <div class="card mb-3">

            <div class="card-header letter-space">
                <i class="fa fa-users mr-3"></i>
                <asp:Label ID="lblheading" runat="server"></asp:Label>
            </div>

            <div class="card-body">
                <div class="table-responsive m-auto">

                    <div>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </p>
                    </div>
                    <br />
                    <div class="title-bg-lite">
                        <asp:Label ID="lblEventDetails" Text="Event Details" runat="server" />
                    </div>
                    <table id="dataTable" width="100%">
                        <tbody>

                            <tr>
                                <td width="20%"><b><span class="field-label">Activity name</span></b></td>

                                <td width="30%">
                                    <asp:Label ID="lblEventName" runat="server" required="required" CssClass="field-value" /></td>

                                <td width="20%"><b><span class="field-label">Academic year</span></b></td>
                                <td width="30%">
                                    <asp:Label ID="lblACDYear" AutoPostBack="true" runat="server" CssClass="field-value"></asp:Label>
                                    <asp:HiddenField ID="hidacdid" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><b><span class="field-label">Description </span></b></td>
                                <td width="30%">
                                    <asp:Label aria-multiline="true" ID="lblEventDesc" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                                <td width="20%"><b><span class="field-label">Activity type</span> </b></td>
                                <td width="30%">
                                    <asp:Label ID="lblActivtyTypes" runat="server" CssClass="field-value"></asp:Label></td>
                            </tr>
                            <tr>
                                <td width="20%"><span class="field-label">Sub Group</span> </td>
                                <td width="30%">
                                    <asp:Label ID="lblSubType" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                                <td width="20%"><span class="field-label" > Gender </span></td>
                                <td width="30%">
                                    <asp:RadioButtonList ID="rdGender" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Both" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Male" Value="1" Selected="False"></asp:ListItem>
                                        <asp:ListItem Text="Female" Value="2" Selected="False"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr id="rowtrip" runat="server" visible="false">
                                <td width="20%"><span class="field-label">Trip Type </span> </td>
                                <td width="30%">
                                    <asp:Label ID="lblTripName" runat="server" CssClass="field-value" Text=""></asp:Label>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr>

                                <td width="20%" runat="server" id="tdterms2" nowrap="true">
                                    <b>
                                        <asp:Label ID="lblslctgrade" runat="server" CssClass="field-label" />
                                    </b></td>
                                <td width="30%">
                                    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600" EnablePageMethods="true">
                                    </ajaxToolkit:ToolkitScriptManager>
                                    <asp:Label ID="lblShowGrades" runat="server" Visible="false" onmouseover="this.style.cursor='hand'" Text="Mouse hover for <br />selected grades and sections" CssClass="lblShowS_CAL"
                                        onmouseout="this.style.cursor='default'"></asp:Label>
                                    <ajaxToolkit:HoverMenuExtender ID="pceGradeDetail" runat="server" PopupControlID="pnlGrades"
                                        PopupPosition="Top" TargetControlID="lblShowGrades" OffsetX="-350">
                                    </ajaxToolkit:HoverMenuExtender>
                                    <asp:Panel ID="pnlGrades" runat="server" BackColor="White">
                                        <div class="gradeshowS_CAL">
                                            <asp:GridView ID="gvGrades" runat="server" EmptyDataText="No Data Found" AutoGenerateColumns="false" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:BoundField DataField="GRADE" HeaderText="GRADE">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SECTIONS" HeaderText="SECTIONS" ItemStyle-CssClass="itemforgrid">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td width="20%"><span class="field-label">Maximum Limit (Count of students)</span></td>
                                <td width="30%" runat="server">
                                    <asp:TextBox ID="txtCount" runat="server" Text="0" OnTextChanged="txtcount_TextChanged"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" align="left"><span class="field-label">Do you want to Auto Enroll for kids </span></td>
                                <td width="30%" align="left">
                                    <asp:CheckBox ID="chkAutoEnroll" runat="server" />
                                </td>
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <div class="title-bg-lite">
                        Date details
                    </div>
                    <table id="dataTable2" width="100%">
                        <tbody>
                            <tr>
                                <td width="20%"><b><span class="field-label">Activity request start date </span></b></td>

                                <td width="30%">
                                    <asp:Label ID="lblApplSt" runat="server" CssClass="field-value" placeholder="DD/MMM/YYYY" TabIndex="2" ClientIDMode="Static" onfocus="click_me();false;" onClick="click_me();false;" required="required"></asp:Label>

                                </td>
                                <td width="20%"><b><span class="field-label">End date </span></b></td>

                                <td width="30%">
                                    <asp:Label ID="lblApplEnd" runat="server" TabIndex="2" CssClass="field-value" placeholder="DD/MMM/YYYY" ClientIDMode="Static" onClick="click_me();false;" required="required"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><b><span class="field-label">Activity start date</span> </b></td>

                                <td width="30%">
                                    <asp:Label ID="lblEventSt" runat="server" TabIndex="2" CssClass="field-value" placeholder="DD/MMM/YYYY" ClientIDMode="Static" onClick="click_me();false;" required="required"></asp:Label>

                                </td>
                                <td width="20%"><b><span class="field-label">End date </span></b></td>

                                <td width="30%">
                                    <asp:Label ID="lblEventEnd" runat="server" TabIndex="2" CssClass="field-value" placeholder="DD/MMM/YYYY" required="required"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><span class="field-label">Do activity have multiple schedules </span></td>
                                <td colspan="3">
                                    <asp:RadioButtonList ID="rdSchdl" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="1" Selected="False"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="0" Selected="true"></asp:ListItem>
                                    </asp:RadioButtonList>                                   
                                    <asp:GridView ID="gvSchdl" runat="server" CssClass="table table-bordered table-row" EmptyDataText="None." AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="Weeks" ItemStyle-Width="20%" HeaderText="Weeks" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                            <asp:BoundField DataField="TimeFrom" ItemStyle-Width="20%" HeaderText="Time From" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                            <asp:BoundField DataField="TimeTo" ItemStyle-Width="20%" HeaderText="Time To" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                            <asp:BoundField DataField="Description" ItemStyle-Width="20%" HeaderText="Description" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                            <%--<asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnSchdDelete" runat="server" OnCommand="btnSchdDelete_Command" CommandArgument='<%# Bind("Weeks")%>' Text="Delete" CssClass="button" CausesValidation="false" data-validate-target="none" UseSubmitBehavior="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" align="left">
                                    <span class="field-label">Restrict activity for same schedule</span>
                                    <%--  <asp:DropDownList ID="ddlNotAllow" runat="server" >
                                                                <asp:ListItem Text="ALLOW MULTPLE ACTIVTITY ON THE SAME DAY" Selected="True" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text ="DO NOT ALLOW MULTIPLE ACTIVITIES ON THE SAME DAY" Value="1"></asp:ListItem>
                                                            </asp:DropDownList>--%>
                                </td>
                                <td width="30%" align="left">
                                    <asp:RadioButtonList ID="chkNotAllow" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="0" Selected="true"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <div class="title-bg-lite">
                        Event payment collection details
                    </div>
                    <table id="dataTable3" width="100%">
                        <tbody>

                            <tr>
                                <td width="20%" runat="server" nowrap="true"><b><span class="field-label">Amount</span> </b></td>
                                <td width="30%">
                                    <asp:Label ID="lblAmount" runat="server" required="required" AutoPostBack="true" CssClass="field-value" />


                                </td>
                                <td width="20%"><b><span class="field-label">Approval required</span> </b></td>
                                <td width="30%">
                                    <asp:Label ID="lblapprovalreq" runat="server" CssClass="field-value"></asp:Label>
                                    <%--<asp:RadioButtonList ID="radioButtonList" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem ID="isEnabledTrue">Yes</asp:ListItem>
                            <asp:ListItem ID="isEnabledFalse" Selected="True">No</asp:ListItem>
                        </asp:RadioButtonList>--%>
                                </td>
                            </tr>
                            <tr id="rowFinance" runat="server" visible="false">
                                <td width="20%"><b><span class="field-label">Collection type</span> </b></td>
                                <td width="30%">
                                    <asp:Label ID="lbllistcoll" runat="server" CssClass="field-value" />
                                    <br />
                                    <asp:Label ID="lbllistcolltype" runat="server" CssClass="field-value" />
                                </td>
                                <td width="20%"><b><span class="field-label">Revenue date</span> </b></td>
                                <td width="30%">
                                    <asp:Label ID="lblRevnDt" runat="server" CssClass="field-value" placeholder="DD/MMM/YYYY" ClientIDMode="Static" TabIndex="2"></asp:Label>
                                </td>
                            </tr>
                            <tr id="rowPaymentMode" runat="server" visible="false">
                                <td width="20%"><b><span class="field-label">Mode of payements allowed </span></b></td>
                                <td width="30%">
                                    <%-- <asp:CheckBoxList runat="server" RepeatDirection="Horizontal" ID="chklistPaymentMode">
                            <asp:ListItem Selected="True" Text="Pay Online"></asp:ListItem>
                            <asp:ListItem Selected="false" Text="Pay at School Counter"></asp:ListItem>
                        </asp:CheckBoxList>--%>
                                    <asp:Label ID="lblpay_mode" runat="server" CssClass="field-value"></asp:Label>
                                </td>
                                <td width="20%"><span class="field-label">Allow multiple enroll</span></td>
                                <td width="30%">
                                    <asp:RadioButtonList ID="rdoAllowMultiple" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem ID="MultiEnrollTrue">Yes</asp:ListItem>
                                        <asp:ListItem ID="MultiEnrollFalse" Selected="True">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />

                    <div class="title-bg-lite">
                        Parent email notification template on successful enrollment
                    </div>
                    <table id="dataTable4" width="100%">
                        <tbody>
                            <tr id="rowEmailTemplate" runat="server">
                                <td>
                                    <asp:Label ID="lblmail" runat="server" CssClass="field-value" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />

                    <div class="title-bg-lite">
                        Terms and conditions applicable for parent on successful enrollment
                    </div>
                    <table id="dataTable6" width="100%">
                        <tbody>
                            <tr id="Tr1" runat="server">
                                <td>
                                    <asp:Label ID="lblterms" runat="server" CssClass="field-value" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />

                    <div class="title-bg-lite" id="divupload" runat="server" visible="false">
                        Activity Uploads
                    </div>
                    <table id="dataTable7" width="100%">
                        <tr id="rowViewfiles" runat="server">
                            <td>
                                <asp:Repeater ID="repdocview" runat="server" OnItemCreated="repdocview_ItemCreated">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%-- <a id="docref" href='<%# Bind("DOC_URL")%>' runat="server" target="_blank" >--%>
                                                <asp:LinkButton ID="lbSName" runat="server" Text='<%# Bind("DOC_NAME")%>' OnClick="lbSName_Click"></asp:LinkButton>
                                                <%-- </a>--%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <br />

                    <div class="title-bg-lite">
                        Comments section
                    </div>
                    <table id="dataTable5" width="100%">
                        <tbody>

                            <tr>
                                <td width="40%"><b>
                                    <asp:Label ID="lblinitiatorcomment" CssClass="field-label" runat="server" Text="Cordinator comment on event details<br/>" /></b></td>
                                <td>
                                    <asp:Label ID="txtComments1" CssClass="field-value" runat="server"></asp:Label>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr id="rowFinanceCmmnts" runat="server" visible="false">

                                <td width="40%"><b>
                                    <asp:Label ID="lblfinancecomment" CssClass="field-label" runat="server" Text="Finance comment<br/>" /></b></td>
                                <td>
                                    <asp:Label ID="txtComments2" CssClass="field-value" runat="server"></asp:Label>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr id="rowPrincipal" runat="server" visible="false">
                                <td width="40%"><b>
                                    <asp:Label ID="lblhoscomment" CssClass="field-label" runat="server" Text="HOS comment" /></b></td>
                                <td>
                                    <textarea id="txtComments3" runat="server" placeholder="Please enter your comments here.."></textarea>
                                </td>
                                <td colspan="2"></td>
                            </tr>

                            <tr id="rowaccept" runat="server">
                                <td colspan="4">
                                    <asp:CheckBox ID="chkaccpt" runat="server" Checked="false" />
                                    <asp:Label ID="lblaccept" runat="server" Text="I have verified the activity details and approving the request" CssClass="field-label"></asp:Label>
                                </td>
                            </tr>
                            <tr id="rowbutton" runat="server">
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Approve" OnClick="btnSave_Click" OnClientClick="return Confirm();" CausesValidation="true" UseSubmitBehavior="true" />
                                    <asp:Button ID="btnReject" CssClass="button" runat="server" Text="Reject" OnClick="btnReject_Click" CausesValidation="false" data-validate-target="none" OnClientClick="return Confirm();" />
                                    <asp:Button ID="btnRevert" CssClass="button" runat="server" Text="Revert" OnClick="btnRevert_Click" CausesValidation="false" data-validate-target="none" OnClientClick="return Confirm();" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />

                    <div align="center">
                        <asp:Label ID="lblInitiatorLogDt" runat="server"></asp:Label><br />
                        <asp:Label ID="lblFinancLogDt" runat="server"></asp:Label><br />
                        <asp:Label ID="lblApprvrLogDt" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
<script type="text/javascript" language="javascript">

    function Confirm() {
        return confirm('Are you sure you want to approve?');
    }


</script>
</html>
