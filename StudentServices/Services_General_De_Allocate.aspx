﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="Services_General_De_Allocate.aspx.vb" Inherits="StudentServices_Services_General_De_Allocate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameParticipant").fancybox({
                type: 'iframe',
                maxWidth: 200,
                maxHeight: 600,
                fitToView: false,
                width: '48%',
                height: '58%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                'showCloseButton': false,
                'hideOnOverlayClick': false,
                'hideOnContentClick': false,
                helpers: {
                    overlay: { closeClick: false } // prevents closing when clicking OUTSIDE fancybox 
                }
            });
        });


        //        function pageLoad() {

        //            $('[id$=chkHeader]').click(function () {
        //                $("[id$='chkChild']").attr('checked', this.checked);
        //            });

        //            $('#<%=btnDeAllocate.ClientID%>').click(function () {
        //                var chkboxrowcount = $("#<%=gvReq.ClientID%> input[id*='chkChild']:checkbox:checked").size();
        //                if (chkboxrowcount == 0) {
        //                    alert("please select at least a record");
        //                    return false;
        //                }
        //                return true;
        //            });

        //        }

        function deAllocate_Student(ssvId) {
            var sFeatures, url;
            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


            //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
            url = "Services_Gen_DeAllocate_F.aspx?ssv=" + ssvId;

            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            if (result != '' && result != undefined) {

                var existingID = $("#<%=hdnSelected.ClientID %>").val();

                if (existingID == '') {
                    $("#<%=txtDeAllocate.ClientID %>").val(result);
                    $("#<%=hdnSelected.ClientID %>").val(result);
                }
                else {
                    $("#<%=txtDeAllocate.ClientID %>").val(existingID + ',' + result);
                    $("#<%=hdnSelected.ClientID %>").val(existingID + ',' + result);
                }


            }
            return true;
        }


        function ShowRPTSETUP_S(id) {

            var sFeatures;
            sFeatures = "dialogWidth: 950px; ";
            sFeatures += "dialogHeight: 750px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;

            var bsuid = document.getElementById('<%=ddlBusinessUnit.ClientID %>').value
            url = '../StudentServices/StudPro_detailsNew.aspx?id=' + id + '&bsuID=' + bsuid + '';
            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }

        }

        function LoadServices(bReload) {
            if (bReload == 1)
                $("#<%=btnSearch.ClientID%>")[0].click();;
        }

    </script>
    <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">DISCONTINUE SERVICE
            </td>
        </tr>
    </table>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    Font-Size="12px"></asp:Label>
            </td>
        </tr>
        <tr style="font-size: 8pt; color: #800000" valign="bottom">
            <td align="center" class="matters" valign="middle">&nbsp;
            </td>
        </tr>
        <tr>
            <td class="matters" valign="bottom">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    class="BlueTable_simple" width="75%">
                    <tr>
                        <td class="subheader_img" colspan="6">
                            <asp:Literal ID="ltLabel" runat="server" Text="Student Search"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters">Academic year
                        </td>
                        <td align="center" style="width: 2px;" class="matters">:
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="height: 21px" class="matters">Business Unit
                        </td>
                        <td align="center" style="width: 2px; height: 21px" class="matters">:
                        </td>
                        <td align="left" style="height: 21px" class="matters">
                            <asp:DropDownList ID="ddlBusinessUnit" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>

                        <td align="left" style="height: 21px" class="matters">Student No
                        </td>
                        <td align="center" style="width: 2px; height: 21px" class="matters">:
                        </td>
                        <td align="left" style="height: 21px" class="matters" colspan="4">
                            <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 21px;" class="matters">Activity
                        </td>
                        <td align="center" style="width: 2px; height: 21px" class="matters">:
                        </td>
                        <td align="left" style="height: 21px" class="matters">
                            <asp:DropDownList ID="ddlCategory" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="height: 21px" class="matters">Status
                        </td>
                        <td align="center" style="width: 2px; height: 21px" class="matters">:
                        </td>
                        <td align="left" style="height: 21px" class="matters">
                            <asp:DropDownList ID="ddlStatus" runat="server">
                                <asp:ListItem Value="0" Selected="True">ACTIVE SERVICES</asp:ListItem>
                                <asp:ListItem Value="1">DISCONTINUATION PENDING</asp:ListItem>
                                <asp:ListItem Value="2">DISCONTINUATION APPROVED</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" style="height: 21px; width: 140px;" class="matters">Current Status
                        </td>
                        <td align="center" style="width: 2px; height: 21px" class="matters">:
                        </td>
                        <td align="left" style="height: 21px" class="matters" colspan="4">Requested &nbsp;
                            <asp:LinkButton ID="lnkReq" runat="server">
                                <asp:Label ID="lblReq" runat="server" ForeColor="Blue" Font-Bold="true" Font-Size="11px"
                                    Text="0"></asp:Label>
                            </asp:LinkButton>
                            &nbsp;&nbsp;&nbsp; Approved &nbsp;
                            <asp:LinkButton ID="lnkApproved" runat="server">
                                <asp:Label ID="lblAppr" runat="server" ForeColor="Green" Font-Bold="true" Font-Size="11px"
                                    Text="0"></asp:Label>
                            </asp:LinkButton>
                            &nbsp;&nbsp;&nbsp; Rejected &nbsp;
                            <asp:LinkButton ID="lnkRejected" runat="server">
                                <asp:Label ID="lblRej" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="11px"
                                    Text="0"></asp:Label>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lnkShowAll" runat="server" CssClass="lnk">&nbsp;SHOW ALL</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 30px; padding: 2px;">
                <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="SEARCH" Width="110px" /><asp:Button
                    ID="btnSave" runat="server" CssClass="button" Text="Add/Save" Width="110px" Visible="false" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Reset" Width="110px"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td class="matters" align="center">
                <table style="width: 60%; border: 0px solid #000; float: none margin-left: 5px;"
                    cellspacing="2" cellpadding="2" id="tblRequestedList" runat="server" visible="false">
                    <tr class="subheader_img">
                        <td align="left" colspan="3" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                <asp:Label ID="lblInfo" runat="server"></asp:Label></span></font>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td class="matters" colspan="2">ACTIVITY : <font color="blue">PENDING</font>,<font color="green"> APPROVED</font>,<font
                            color="RED"> DEALLOCATED/REJECTED</font>
                        </td>
                        <td class="matters">APPROVED :
                            <asp:Label ID="lblApprovedCount" runat="server" Font-Bold="true"></asp:Label>
                            AVAILABLE :
                            <asp:Label ID="lblAvailable" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3" class="matters">
                            <asp:GridView ID="gvReq" runat="server" AutoGenerateColumns="False" CssClass="gridstyle"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" DataKeyNames="SSV_ID" AllowPaging="True" OnRowDataBound="gvReq_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select" Visible="false">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkHeader" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkChild" runat="server" />
                                            <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSSV_ID" runat="server" Text='<%# Bind("SSV_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDeAllocated" runat="server" Text='<%# Bind("Isenable") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">Student No
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtSTU_NO" runat="server" Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchSTU_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchSTU_NO_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">Student Name
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtSNAME" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchSNAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchSNAME_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>--%>
                                            <asp:LinkButton ID="lbtnSname" runat="server" Text='<%# Bind("SNAME") %>' Visible="true" OnClientClick=<%# string.Format("javascript:ShowRPTSETUP_S('{0}');return false;",Eval("STU_ID"))%>> </asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gender" Visible="false">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">Gender
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtGender" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchGender" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchGender_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGender" runat="server" Text='<%# Bind("Gender") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Activities" DataField="SVC_DESCRIPTION" HtmlEncode="false">
                                        <ItemStyle HorizontalAlign="Center" Wrap="true" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SVC_DATE" HeaderText="ServiceDate" />
                                    <asp:TemplateField HeaderText=" Action  ">
                                        <ItemTemplate>
                                            <a id="framePartcpnt" class="frameParticipant" style="display: none;" href="Services_Gen_DeAllocate_F.aspx?ssv=<%#Eval("SSV_ID")%>">Discontinue</a>
                                            <asp:HyperLink ID="lnkDeAllocate" runat="server" Text="De-Allocate" CssClass="frameParticipant"></asp:HyperLink>
                                            <asp:LinkButton ID="lbtnAdd" OnClick="lbtnADeallocateClick" OnClientClick='<%#Eval("SSV_ID","javascript:deAllocate_Student({0});")%>'
                                                runat="server" Text="DISCONTINUE" Visible="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters">
                <asp:Button ID="btnDeAllocate" Visible="false" ValidationGroup="vgApprove" Text="DISCONTINUE"
                    runat="server" CssClass="button" />
                <asp:TextBox ID="txtDeAllocate" runat="server" Width="330px" AutoPostBack="True"
                    OnTextChanged="txtDeAllocate_TextChanged" Visible="false"></asp:TextBox>&nbsp;
                <asp:HiddenField ID="hdnSelected" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 10px" valign="bottom"></td>
        </tr>
    </table>
    <asp:HiddenField ID="hfGender" runat="server" Value="3" />
    <asp:HiddenField ID="hfTerms" runat="server" Value="0" />
</asp:Content>
