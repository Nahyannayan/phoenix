﻿<%@ Page Language="VB" AutoEventWireup="true" EnableEventValidation="true" MasterPageFile="~/mainMasterPage.master" CodeFile="Services_CreateActivityList.aspx.vb" Inherits="StudentServices_Services_CreateActivityList" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
  <%-- <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />--%>
    <%--<link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <link href="../cssfiles/bootstrap.css" rel="stylesheet" />--%>
    <style>
        .itemforgrid {
            width: 700px;
            word-wrap: break-word !important;
            word-break: normal !important;
            padding-left: 100px;
            display: block;
        }

        .gradeshowS_CAL {
            background-color: #fff !important;
            position: absolute;
            /*font-family: Verdana !important;
    font-weight: bold !important;
    font-size: 10px !important;*/
            left: -100px !important;
            top: 60px !important;
            opacity: 1;
        }

            .gradeshowS_CAL.fade {
                opacity: 0.5;
            }


        img {
            width: auto !important;
        }


        .divheadingS_CAL {
            color: black;
            text-align: center;
            font-weight: bold;
            padding-left: 40px;
        }


        /*table.table-striped.tableextrastyleS_CAL {
     width:95% !important;   
   font-size :small !important;
    
}*/
        .toggle-msg {
            position: absolute;
            width: 20%;
            background-color: rgb(237,237,237);
            color: #333;
            padding: 2px;
            z-index: 1000;
            border-radius: 8px;
            box-shadow: rgba(0, 0, 0, 0.3) 0px 5px 9px;
            border-color: #efefef;
        }

        .toggle-padding {
            padding: 6px 6px;
        }

        .columnstyleS_CAL {
            width: 15% !important;
        }

        .aligncenterS_CAL {
            text-align: center;
        }

        /*.lnkCopyClassS_CAL {
            float: right;
        }

            .lnkCopyClassS_CAL:hover {
                float: right;
                /*-webkit-transform: scale(1.1);
     -ms-transform: scale(1.1);
     transform: scale(1.1);
            }*/

        .lblShowS_CAL {
            background-color: lightblue;
            border: 2px solid #e7e7e7;
            color: white;
            padding: 10px 22px;
            text-align: center;
            border-radius: 4px;
            text-decoration: none;
            display: inline-block;
            /*font-size: 12px;*/
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            cursor: pointer;
        }

            .lblShowS_CAL:hover {
                background-color: #e7e7e7;
            }

        .panel-cover-act {
            padding: 6px 6px;
            border: 1px solid #cccccc;
            border-radius: 8px;
        }

            .panel-cover-act:hover {
                padding: 6px 6px;
                border: 1px solid #e7e7e7;
                border-radius: 8px;
                background-color: #fcfdff;
            }

        .btn-accordion {
            padding: 5px 80% 5px 20px;
            color: #333;
            font-weight: bold;
        }
    
            .RadComboBox_Default .rcbReadOnly {
                background-image: none !important;
                background-color: transparent !important;
            }

            .RadComboBox_Default .rcbDisabled {
                background-color: rgba(0,0,0,0.01) !important;
            }

                .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                    background-color: transparent !important;
                    border-radius: 0px !important;
                    border: 0px !important;
                    padding: initial !important;
                    box-shadow: none !important;
                }

            .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
                display: inline;
                float: left;
            }

            .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
                border: 0 !important;
            }

            .RadComboBox_Default .rcbInner {
                padding: 10px;
                border-color: #dee2da !important;
                border-radius: 6px !important;
                box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
                width: 80%;
            }

            .RadComboBox_Default .rcbInput {
                font-family: 'Nunito', sans-serif !important;
            }

            .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
                border: 0 !important;
                box-shadow: none;
            }

            .RadComboBox_Default .rcbActionButton {
                border: 0px;
                background-image: none !important;
                height: 100% !important;
                color: transparent !important;
                background-color: transparent !important;
            }

            .darkPanlAlumini {
                width: 100%;
                height: 100%;
                position: fixed;
                left: 0%;
                top: 0%;
                background: rgba(0,0,0,0.2) !important;
                /*display: none;*/
                display: block;
            }

            .inner_darkPanlAlumini {
                left: 10%;
                top: 10%;
                position: fixed;
                width: 100%;
            }
            .ui-widget-content {
                min-height: 300px !important;
                overflow-y: scroll !important;
            }
        .RadEditor {
        min-height: 0px !important;
        }
        </style>
    <%--<style>
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 100%;
            background-image: none !important;
            background-color: #ffffff /*!important*/;
        }

        .RadComboBox_Default .rcbDisabled .rcbInput {
            background-color: transparent !important;
            border: none !important;
            box-shadow: none;
            padding: 0px;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
            font-style: normal;
        }

        .RadComboBox_Default .rcbReadOnly .rcbInput {
            color: #333;
            font-style: normal;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default {
            color: #333;
            font: normal 12px/16px "Segoe UI",Arial,Helvetica,sans-serif;
            width: 80% !important;
        }
    </style>--%>
    <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_emp" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i>
            <asp:Label Text='Create Activity' ID="lblheading" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div id="divHeading" class="divheadingS_CAL">
                    <div>
                        <asp:HiddenField ID="hidlatestrefid" runat="server" />
                    </div>
                </div>

                <table id="dataTabl" width="100%">
                    <tbody>
                        <tr>
                            <td align="left">
                                <div>
                                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <%--<asp:LinkButton ID="btnAddGroup" runat="server" Text="Click here to add groups" CssClass="panel-cover-act" Visible="false"></asp:LinkButton>--%>
                                <asp:LinkButton ID="btnCopy" runat="server" Text="Click here to copy previous activity" CssClass="panel-cover-act" Visible="false"></asp:LinkButton>
                                <asp:TextBox ID="hidID" runat="server" AutoPostBack="true" Style="display: none;" />
                                <div id="dialog" class="DisplayNoneO_AR">
                                </div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<tr>
                            <td colspan="4" class="title-bg">Activity details
                            </td>
                        </tr>--%>
                                <div class="accordion" id="accordionExample">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseOne"
                                                    aria-expanded="true" aria-controls="collapseOne">
                                                    Activity Details
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                            data-parent="#accordionExample">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Activity name </span><span style="color: red">*</span></td>
                                                        <td width="30%">
                                                            <asp:TextBox ID="txtEventName" runat="server" CssClass="evTextNameStyleS_CAL" required="required" placeholder="Enter name of activity here..." autocomplete="off" />
                                                        </td>
                                                        <td width="20%"><span class="field-label">Description</span><span style="color: red">*</span></td>
                                                        <td width="30%">
                                                            <textarea aria-multiline="true" id="txtEventDesc" runat="server" required="required" class="textareaS_CAL" placeholder="Activity description, maximum 2000 charecters..."></textarea>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Activity type</span><span style="color: red">*</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlActivtyTypes" runat="server" OnSelectedIndexChanged="ddlActivtyTypes_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </td>
                                                        <td><span class="field-label">Activity sub type</span>
                                                            <span style="font-size: xx-small !important;">
                                                                <asp:LinkButton ID="lnkMoreInfo" runat="server" Text="More Info"></asp:LinkButton></span>
                                                            <div id="divMore" runat="server" style="display: none;" class="table table-striped table-bordered toggle-msg">
                                                                <div class="margin-bottom0 toggle-padding">
                                                                    Activity sub type is linked with activity type . Please create activity sub type under 'Students->Master->Create Activity Group'
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSubGrp" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubGrp_SelectedIndexChanged">
                                                            </asp:DropDownList>

                                                        </td>
                                                    </tr>
                                                    <tr id="rowtrips" runat="server" visible="false">
                                                        <td align="left" width="20%"><span class="field-label">Trip Type</span><span style="color: red">*</span></td>
                                                        <td align="left" width="30%">
                                                            <%--<asp:DropDownlist ID="ddlTripsList" runat="server" ></asp:DropDownlist>--%>
                                                            <telerik:RadComboBox ID="ddlTripsList" runat="server" Filter="Contains" RenderMode="Lightweight" Width="400" Height="400px" Visible="false"></telerik:RadComboBox>
                                                             <asp:TextBox ID="txtSubActivityName" runat="server" onfocus="InitStudentAutoComplete();" required="required" onclick="InitStudentAutoComplete();"></asp:TextBox>
                                        <asp:ImageButton ID="btnEmp_Name" runat="server" ImageAlign="Top" CausesValidation="false" ImageUrl="../Images/forum_search.gif"
                                            OnClientClick="getTripID('AST');return false;" />
                                                        </td>
                                                        <td align="left" width="20%"></td>
                                                        <td align="left" width="30%"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Gender</span> </td>
                                                        <td width="30%">
                                                            <asp:RadioButtonList ID="rdGender" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Both" Value="0" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Male" Value="1" Selected="False"></asp:ListItem>
                                                                <asp:ListItem Text="Female" Value="2" Selected="False"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td><span class="field-label">Allow multiple enroll</span>
                                                            <span style="font-size: xx-small !important;">
                                                                <asp:LinkButton ID="lnkMoreInfo1" runat="server" Text="More Info"></asp:LinkButton></span>
                                                            <div id="divMore1" style="display: none;" runat="server" class="table table-striped table-bordered toggle-msg">
                                                                <div class="margin-bottom0 toggle-padding">
                                                                    Please select yes if a student can enroll more than one activity at the same time
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="rdoAllowMultiple" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdoAllowMultiple_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem ID="MultiEnrollTrue">Yes</asp:ListItem>
                                                                <asp:ListItem ID="MultiEnrollFalse" Selected="True">No</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:TextBox ID="txtallowcount" runat="server" placeholder="Allowable limit" Visible="false" autocomplete="off"></asp:TextBox><br />
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, Custom"
                                                                ValidChars="." TargetControlID="txtallowcount" />
                                                            <i>
                                                                <asp:Label ID="lblnote" runat="server" Text="*contains maximum count student can reuqest" Font-Size="X-Small" Visible="false"></asp:Label></i>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td width="20%"><span class="field-label">Maximum Limit (Count of students)</span><span style="color: red">*</span></td>
                                                        <td width="30%" runat="server">
                                                            <asp:TextBox ID="txtCount" runat="server" Text="0" OnTextChanged="txtcount_TextChanged" required="required" autocomplete="off"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                                                ValidChars="." TargetControlID="txtCount" />
                                                        </td>
                                                        <td width="20%"><span class="field-label">Approval required for online activity registration</span>
                                                            <span style="font-size: xx-small !important;">
                                                                <asp:LinkButton ID="lnkMoreInfo2" runat="server" Text="More Info"></asp:LinkButton></span>
                                                            <div id="divMore2" style="display: none;" class="table table-striped table-bordered toggle-msg" runat="server">
                                                                <div class="margin-bottom0 toggle-padding">
                                                                    Please select if the activity needs one more level of approval once student requested to activity
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td width="30%">
                                                            <asp:RadioButtonList ID="radioButtonList" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem ID="isEnabledTrue" Value="0" Text="Yes"></asp:ListItem>
                                                                <asp:ListItem ID="isEnabledFalse" Value="1" Text="No"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion" id="accordionExample2">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseTwo"
                                                    aria-expanded="true" aria-controls="collapseTwo">
                                                    Academic Details
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne"
                                            data-parent="#accordionExample2">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Academic year</span></td>
                                                        <td width="30%">
                                                            <asp:DropDownList ID="ddlACDYear" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlACDYear_SelectedIndexChanged"></asp:DropDownList>
                                                        </td>
                                                        <td width="20%">
                                                            <asp:Label ID="lblslctgrade" runat="server" Text="Select grade(s)" CssClass="field-label" /><span style="color: red">*</span></td>
                                                        <td runat="server" id="tdterms2" nowrap="true" width="30%">

                                                            <%--<asp:LinkButton ID="lnkBtnSelectGrade" runat="server" OnClientClick="return false;" Text="Click here to select grades & sections" CssClass ="gradeClassS_CAL"></asp:LinkButton>
                            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="lnkBtnSelectGrade"
                                PopupControlID="pnlGrades2" CommitProperty="value" Position="Top" CommitScript=""/>
                            <asp:Panel ID="pnlGrades2" runat="server" BackColor="#dee2e6" Visible="false" >--%>
                                                            <div class="checkbox-list" runat="server" id="pnlGrades2">
                                                                <asp:TreeView ID="trGrades" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked()"
                                                                    runat="server">
                                                                </asp:TreeView>
                                                            </div>
                                                            <%-- </asp:Panel>--%>


                                                            <asp:Label ID="lblShowGrades" runat="server" Visible="false" onmouseover="this.style.cursor='hand'" Text="Mouse hover for <br />selected grades and sections" CssClass="lblShowS_CAL"
                                                                onmouseout="this.style.cursor='default'"></asp:Label>
                                                            <ajaxToolkit:HoverMenuExtender ID="pceGradeDetail" runat="server" PopupControlID="pnlGrades"
                                                                PopupPosition="Top" TargetControlID="lblShowGrades" OffsetX="-350">
                                                            </ajaxToolkit:HoverMenuExtender>
                                                            <asp:Panel ID="pnlGrades" runat="server" BackColor="White">
                                                                <div class="gradeshowS_CAL">
                                                                    <asp:GridView ID="gvGrades" runat="server" EmptyDataText="No Data Found" AutoGenerateColumns="false" CssClass="table table-bordered table-row">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="GRADE" HeaderText="GRADE">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SECTIONS" HeaderText="SECTIONS" ItemStyle-CssClass="itemforgrid">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </asp:Panel>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td width="20%" align="left"><span class="field-label">Automatically enroll selected students </span></td>
                                                        <td width="30%" align="left">
                                                            <asp:CheckBox ID="chkAutoEnroll" runat="server" OnCheckedChanged="chkAutoEnroll_CheckedChanged" AutoPostBack="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--<tr>
                        <td colspan="4" class="title-bg">Academic details
                        </td>
                    </tr>--%>

                                <%--                        <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>--%>
                                <%-- <tr>
                            <td colspan="4" class="title-bg">Date details
                            </td>
                        </tr>--%>
                                <div class="accordion" id="accordionExample3">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingThree">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseThree"
                                                    aria-expanded="true" aria-controls="collapseThree">
                                                    Date Details
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseThree" class="collapse show" aria-labelledby="headingOne"
                                            data-parent="#accordionExample3">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Activity request start date</span><span style="color: red">*</span></td>
                                                        <td width="30%">
                                                            <asp:TextBox ID="txtApplSt" runat="server" placeholder="DD/MMM/YYYY" TabIndex="2" ClientIDMode="Static" onfocus="click_me();false;" onClick="click_me();false;" required="required" autocomplete="off"></asp:TextBox>

                                                        </td>
                                                        <td width="20%"><span class="field-label">End date</span><span style="color: red">*</span></td>
                                                        <td width="30%">

                                                            <asp:TextBox ID="txtApplEnd" runat="server" TabIndex="2" placeholder="DD/MMM/YYYY" ClientIDMode="Static" onClick="click_me();false;" required="required" autocomplete="off"></asp:TextBox>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td width="20%"><span class="field-label">Event start date</span><span style="color: red">*</span></td>
                                                        <td width="30%">

                                                            <asp:TextBox ID="txtEventSt" runat="server" TabIndex="2" placeholder="DD/MMM/YYYY" ClientIDMode="Static" onClick="click_me();false;" required="required" autocomplete="off"></asp:TextBox>

                                                        </td>
                                                        <td width="20%"><span class="field-label">End date</span><span style="color: red">*</span></td>
                                                        <td width="30%">

                                                            <asp:TextBox ID="txtEventEnd" runat="server" TabIndex="2" placeholder="DD/MMM/YYYY" required="required" autocomplete="off"></asp:TextBox>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Allow multiple schedules </span></td>
                                                        <td colspan="3">
                                                            <asp:RadioButtonList ID="rdSchdl" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdSchdl_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Text="Yes" Value="1" Selected="False"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0" Selected="true"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" align="middle">
                                                            <table class="table table-bordered table-row" id="tblScdl" runat="server" width="100%" visible="false" align="middle">
                                                                <tr>
                                                                    <th width="30%" align="middle">Days</th>
                                                                    <th width="10%" align="middle">Time From </th>
                                                                    <th width="10%" align="middle">Time To</th>
                                                                    <th width="15%" align="middle">Description
                                                                    </th>
                                                                    <th width="10%" align="middle">Count</th>
                                                                    <th width="5%" align="middle"></th>
                                                                </tr>
                                                                <tr>
                                                                    <td width="30%" align="middle">
                                                                        <asp:CheckBoxList ID="chkWeekly" runat="server" RepeatDirection="Horizontal">
                                                                            <asp:ListItem>Sun</asp:ListItem>
                                                                            <asp:ListItem>Mon</asp:ListItem>
                                                                            <asp:ListItem>Tue</asp:ListItem>
                                                                            <asp:ListItem>Wed</asp:ListItem>
                                                                            <asp:ListItem>Thur</asp:ListItem>
                                                                            <asp:ListItem>Fri</asp:ListItem>
                                                                            <asp:ListItem>Sat</asp:ListItem>
                                                                        </asp:CheckBoxList>
                                                                    </td>
                                                                    <td width="10%" align="middle">
                                                                        <%-- <telerik:RadTimePicker ID="radWeekTimeFrom" runat="server"></telerik:RadTimePicker>--%>
                                                                        <asp:TextBox ID="rdFrm" runat="server" autocomplete="off"></asp:TextBox>
                                                                    </td>
                                                                    <td width="10%" align="middle">
                                                                        <%--<telerik:RadTimePicker ID="radWeekTimeTo" runat="server"></telerik:RadTimePicker>--%>
                                                                        <asp:TextBox ID="rdTo" runat="server" autocomplete="off"></asp:TextBox>
                                                                    </td>
                                                                    <td width="15%" align="middle">
                                                                        <asp:TextBox ID="txtSchlDescr" runat="server" Enabled="true" autocomplete="off"></asp:TextBox>
                                                                    </td>
                                                                    <td width="10%" align="middle">
                                                                        <asp:TextBox ID="txtSchlCount" runat="server" Enabled="true" autocomplete="off"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom"
                                                                            ValidChars="." TargetControlID="txtSchlCount" />
                                                                    </td>
                                                                    <td width="5%" align="middle">
                                                                        <asp:Button ID="btnschdl" runat="server" OnClick="btnSchdl_Click" Text="Add" CssClass="button" CausesValidation="false" data-validate-target="none" UseSubmitBehavior="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvSchdl" runat="server" CssClass="table table-bordered table-row" EmptyDataText="None." AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Weeks" ItemStyle-Width="20%" HeaderText="Weeks" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                                                    <asp:BoundField DataField="TimeFrom" ItemStyle-Width="20%" HeaderText="Time From" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                                                    <asp:BoundField DataField="TimeTo" ItemStyle-Width="20%" HeaderText="Time To" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                                                    <asp:BoundField DataField="Description" ItemStyle-Width="20%" HeaderText="Description" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                                                    <asp:BoundField DataField="Count" ItemStyle-Width="20%" HeaderText="Count" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" />
                                                                    <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center">
                                                                        <ItemTemplate>
                                                                            <asp:Button ID="btnSchdDelete" runat="server" OnCommand="btnSchdDelete_Command" CommandArgument='<%# Bind("Weeks")%>' Text="Delete" CssClass="button" CausesValidation="false" data-validate-target="none" UseSubmitBehavior="false" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" align="left">
                                                            <span class="field-label">Do not allow if student is enrolled in another activity on same schedule</span>
                                                            <%--  <asp:DropDownList ID="ddlNotAllow" runat="server" >
                                                                <asp:ListItem Text="ALLOW MULTPLE ACTIVTITY ON THE SAME DAY" Selected="True" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text ="DO NOT ALLOW MULTIPLE ACTIVITIES ON THE SAME DAY" Value="1"></asp:ListItem>
                                                            </asp:DropDownList>--%>
                                                        </td>
                                                        <td width="30%" align="left">
                                                            <asp:RadioButtonList ID="chkNotAllow" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0" Selected="true"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%--  <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>--%>
                                <%--<tr>
                            <td colspan="4" class="title-bg">Event amount details
                            </td>
                        </tr>--%>
                                <div class="accordion" id="accordionExample4">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingFour">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseFour"
                                                    aria-expanded="true" aria-controls="collapseFour">
                                                    Amount Details
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseFour" class="collapse show" aria-labelledby="headingFour"
                                            data-parent="#accordionExample4">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="17%"><span class="field-label">Amount</span></td>
                                                        <td width="10%" runat="server" nowrap="true">
                                                            <asp:TextBox ID="txtAmount" runat="server" onfocus="this.select();" CausesValidation="true" required="required" AutoPostBack="true" OnTextChanged="txtAmount_TextChanged" Style="text-align: right" autocomplete="off" />
                                                            <label id="lblAED" runat="server"></label>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                                                ValidChars="." TargetControlID="txtAmount" />
                                                            <asp:HiddenField ID="hidAmount" runat="server" Value="0.00" />
                                                        </td>
                                                        <td width="20%"></td>
                                                        <td width="30%"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <tr>
                                    <td colspan="4">
                                        <br />
                                    </td--%>
                                <%--  </tr>--%>

                                <div class="accordion" id="rowFinanceheading" runat="server" visible="false">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingFive">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseFive"
                                                    aria-expanded="true" aria-controls="collapseFive">
                                                    Activity fee payment details
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseFive" class="collapse show" aria-labelledby="headingFive"
                                            data-parent="#rowFinanceheading">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr id="rowFinance" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td width="20%"><span class="field-label">Add collection type</span><span style="color: red">*</span></td>
                                                                    <td colspan="3">
                                                                        <asp:RadioButtonList ID="radlistFee" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="radlistFee_SelectindexChanged" AutoPostBack="true">
                                                                            <asp:ListItem Value="Fee Collection">Fee collection</asp:ListItem>
                                                                            <asp:ListItem Value="Other Collection">Other collection</asp:ListItem>
                                                                            <asp:ListItem Value="Registration Only">Register without fee collection</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <asp:Label ID="lblFeeType" runat="server" Text="Fee Type : " Visible="false" CssClass="field-label" />
                                                                    </td>
                                                                    <td width="30%">
                                                                        <asp:DropDownList ID="ddlFeetype" runat="server" Visible="false" OnSelectedIndexChanged="ddlFeetype_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                                    </td>
                                                                    <td width="20%">
                                                                        <asp:Label ID="lblrevenuedt" runat="server" Text="Revenue date*" Visible="false" CssClass="field-label"></asp:Label></td>
                                                                    <td id="colRevendt" runat="server">
                                                                        <asp:TextBox ID="txtRevnDt" runat="server" placeholder="DD/MMM/YYYY" ClientIDMode="Static" onClick="click_me();false;" TabIndex="2" autocomplete="off"></asp:TextBox>
                                                                        <br />
                                                                        <asp:CheckBox ID="chkrevnueprd" runat="server" OnCheckedChanged="chkrevnueprd_CheckedChanged" Text="Equally split across event period" AutoPostBack="true" />
                                                                    </td>
                                                                    <asp:HiddenField ID="hidFeeVatCode" runat="server" />
                                                                </tr>
                                                                <tr runat="server" visible="false">
                                                                    <td width="20%">
                                                                        <asp:Label ID="lblVatDescr" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td colspan="4">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="rowPaymentMode" runat="server" visible="false">
                                                        <td width="20%"><span class="field-label">Mode of payements allowed</span><span style="color: red">*</span></td>
                                                        <td colspan="3">

                                                            <asp:CheckBoxList runat="server" RepeatDirection="Horizontal" ID="chklistPaymentMode">
                                                                <asp:ListItem Selected="false" Text="Pay online"></asp:ListItem>
                                                                <asp:ListItem Selected="false" Text="Pay at school counter"></asp:ListItem>
                                                            </asp:CheckBoxList>

                                                        </td>
                                                    </tr>
                                                    <tr id="rowShowSplitAmount" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <table width="100%">
                                                                <tr width="100%">
                                                                    <td width="10%"><span class="field-label">Amount</span>  </td>
                                                                    <td width="15%">
                                                                        <asp:TextBox ID="txtAmountExlTax" runat="server" Text="" Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                    </td>
                                                                    <td width="10%"><span class="field-label">TAX Code</span>  </td>
                                                                    <td width="15%">
                                                                        <asp:TextBox ID="txtVatCode" runat="server" Text="" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                    <td width="10%"><span class="field-label">Tax </span></td>
                                                                    <td width="15%">
                                                                        <asp:TextBox ID="txttax" runat="server" Text="" Enabled="false" Style="text-align: right"></asp:TextBox></td>
                                                                    <td width="10%"><span class="field-label">Net Amount </span></td>
                                                                    <td width="15%">
                                                                        <asp:TextBox ID="txtNetamnt" runat="server" Text="" Enabled="false" Style="text-align: right"></asp:TextBox></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion" id="accordionExample6">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingsix">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapsesix"
                                                    aria-expanded="true" aria-controls="collapsesix">
                                                    Email notification 
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapsesix" class="collapse show" aria-labelledby="headingsix"
                                            data-parent="#accordionExample6">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Parent notification on successful enrollment </span></td>
                                                        <td width="30%">
                                                            <asp:RadioButtonList ID="radioDoEmail" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="radioDoEmail_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Text="No" Value="0" Selected="true"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr id="rowEmailTemplate" runat="server">
                                                        <td colspan="1" width="20%"></td>
                                                        <td colspan="3">
                                                            <telerik:RadEditor ID="txtEmailTemplate" runat="server" RenderMode="Lightweight"
                                                                StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr id="rowMailView" runat="server">
                                                        <td width="20%"></td>
                                                        <td colspan="3">
                                                            <asp:Label ID="lblmail" runat="server" CssClass="field-value"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <tr id="rowFinanceheading" runat="server" visible="false">
                                    <td colspan="4" class="title-bg">Activity fee payment details
                                    </td>
                                </tr>--%>

                                <%-- <tr>
                                    <td colspan="4">
                                        <br />
                                    </td>
                                </tr>--%>
                                <%--  <tr>
                        <td colspan="4" class="title-bg">Email notification details
                        </td>
                    </tr>--%>
                                <%-- <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>--%>
                                <div class="accordion" id="accordionExample7">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingseven">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseseven"
                                                    aria-expanded="true" aria-controls="collapseseven">
                                                    Terms and condition details
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseseven" class="collapse show" aria-labelledby="headingseven"
                                            data-parent="#accordionExample7">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%"><span class="field-label">Terms and conditions applicable for parent </span>
                                                        </td>
                                                        <td width="30%">
                                                            <asp:RadioButtonList ID="radoDoTerms" runat="server" OnSelectedIndexChanged="radoDoTerms_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="No" Value="0" Selected="true"></asp:ListItem>
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr id="rowtermstemplate" runat="server">
                                                        <td colspan="1" width="20%">
                                                            <br />
                                                            <br />
                                                        </td>
                                                        <td colspan="3">
                                                            <telerik:RadEditor ID="txtTermsandCond" runat="server" RenderMode="Lightweight"
                                                                StripFormattingOptions="MSWordNoFonts, Css, Font, Span"
                                                                StripFormattingOnPaste="MSWordNoFonts, Css, Font, Span" ToolsFile="~/Fees/RadEditorXML/RadEditorFormat.xml">
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr id="rowTermsView" runat="server">
                                                        <td width="20%"></td>
                                                        <td colspan="3">
                                                            <asp:Label ID="lblterms" runat="server" CssClass="field-value"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <tr>
                        <td colspan="4" class="title-bg">Terms and condition details
                        </td>
                    </tr>--%>


                                <%--  <tr>
                        <td colspan="4">
                            <br />
                        </td>
                    </tr>--%>
                                <%-- <tr>
                    <td colspan="4" class="title-bg">Comments from authors
                    </td>
                </tr>--%>
                                <div class="accordion" id="accordionExample8">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingeight">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapseeight"
                                                    aria-expanded="true" aria-controls="collapseeight">
                                                    Comments from authors
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseeight" class="collapse show" aria-labelledby="headingeight"
                                            data-parent="#accordionExample8">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="lblinicomment" runat="server" Text="Activity details" CssClass="field-label" /><span style="color: red">*</span></td>
                                                        <td colspan="2">
                                                            <textarea id="txtComments1" placeholder="Please enter your comments here.." runat="server" class="textareaS_CAL"></textarea>
                                                        </td>
                                                        <td width="30%"></td>
                                                    </tr>
                                                    <tr id="rowFinanceCmmnts" runat="server" visible="false">
                                                        <td width="20%">
                                                            <asp:Label ID="lblfinancecommnt" runat="server" Text="Finance comment" CssClass="field-label" /><span style="color: red">*</span></td>
                                                        <td colspan="2">
                                                            <textarea id="txtComments2" runat="server" placeholder="Please enter your comments here.." class="textareaS_CAL"></textarea>
                                                        </td>
                                                        <td width="30%"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- <tr id="rowPrincipal" runat="server" visible="false">
                    <td>
                        <asp:Label ID="lblhoscomment" runat="server" Text="HOS comment" CssClass="field-label" /></td>
                    <td colspan="2">
                        <textarea id="txtComments3" runat="server" placeholder="Please enter your comments here.." class="textareaS_CAL"></textarea>
                    </td>
                    <td></td>
                </tr>--%>

                                <%--  <tr>
                    <td colspan="4">
                        <br />
                    </td>
                </tr>--%>

                                <div class="accordion" id="rowdocumentheading" runat="server" visible="false">
                                    <div class="card z-depth-0 bordered">
                                        <div class="card-header p-0" id="headingnine">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link btn-accordion" type="button" data-toggle="collapse" data-target="#collapsnine"
                                                    aria-expanded="true" aria-controls="collapsenine">
                                                    Activity Uploads
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapsenine" class="collapse show" aria-labelledby="headingnine"
                                            data-parent="#rowdocumentheading">
                                            <div class="card-body">
                                                <table width="100%">
                                                    <tr id="rowUploadoption" runat="server" visible="false">
                                                        <td><span class="field-label">Documents related </span></td>
                                                        <td colspan="3">
                                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Repeater ID="repdoc" runat="server">
                                                                                    <ItemTemplate>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="lblfile" runat="server" Text='<%# Bind("DOC_NAME")%>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hffile" runat="server" Value='<%# Bind("DOC_ID")%>' />
                                                                                                    <asp:FileUpload ID="Fileupload" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnfilesave" align="center" runat="server" OnClick="btnfilesave_Click" Text="Save File" CausesValidation="false" data-validate-target="none" CssClass="button" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnfilesave" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <br />
                                                            <asp:Label ID="lbl" runat="server" />
                                                        </td>

                                                    </tr>
                                                    <tr id="rowViewfiles" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <asp:Repeater ID="repdocview" runat="server" OnItemCreated="repdocview_ItemCreated">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <%-- <a id="docref" href='<%# Bind("DOC_URL")%>' runat="server" target="_blank" >--%>
                                                                            <asp:HiddenField ID="hidpath" runat="server" Value='<%# Bind("DOC_URL")%>' />
                                                                            <asp:LinkButton ID="lbSName" runat="server" Text='<%# Bind("DOC_NAME")%>' OnClick="lbSName_Click"></asp:LinkButton>
                                                                            <%-- </a>--%>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <tr id="rowdocumentheading" runat="server" visible="false">
                    <td colspan="4" class="title-bg">Activity uploads
                    </td>
                </tr>--%>

                                <%-- <tr>
                    <td colspan="4">
                        <br />
                    </td>
                </tr>--%>
                            </td>
                        </tr>
                        <tr id="rowaccept" runat="server" visible="false">
                            <td>
                                <asp:CheckBox ID="chkaccpt" runat="server" Checked="false" />
                                <asp:Label ID="lblaccept" runat="server" Text="I have verified the activity details and approving the request"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="aligncenterS_CAL">
                                <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Submit" OnClick="btnSave_Click" OnClientClick="return btnSaveConfirm();" CausesValidation="true" UseSubmitBehavior="true" />
                                <asp:Button ID="btnDelete" CssClass="button" runat="server" OnClientClick="return Confirm();" OnClick="btnDelete_Click" Text="Delete" />
                                <asp:Button ID="btnAdd" CssClass="button" runat="server" OnClick="btnAdd_Click" OnClientClick="return Confirm();" Text="Add New" />
                                <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Close" OnClick="btnCancel_Click" OnClientClick="if ( !confirm('Are you sure you want to close ? ')) return false;" CausesValidation="false" data-validate-target="none" UseSubmitBehavior="false" />
                                <asp:Button ID="btnReject" CssClass="button" runat="server" Text="Reject" OnClick="btnReject_Click" CausesValidation="false" data-validate-target="none" OnClientClick="return Confirm();" />
                                <asp:Button ID="btnRevert" CssClass="button" runat="server" Text="Revert" OnClick="btnRevert_Click" CausesValidation="false" data-validate-target="none" OnClientClick="return Confirm();" />
                                <asp:Button ID="btnUpdate" CssClass="button" runat="server" Text="Update" OnClick="btnUpdate_Click" Visible="false" CausesValidation="false" data-validate-target="none" OnClientClick="return Confirm();" />
                                <asp:Button ID="btnExtend" CssClass="button" runat="server" Text="Extend Dates" Visible="false" CausesValidation="false" data-validate-target="none" OnClick="btnExtend_Click" />
                                <asp:Button ID="btnPrint" CssClass="button" runat="server" Text="Print" Visible="false" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInitiatorLogDt" runat="server"></asp:Label><br />
                    <asp:Label ID="lblFinancLogDt" runat="server"></asp:Label><br />
                    <asp:Label ID="lblApprvrLogDt" runat="server"></asp:Label>
                </div>

                <asp:Panel ID="pnlAddGroup" runat="server" CssClass="">
                </asp:Panel>

                <asp:HiddenField ID="hf_mode" runat="server" />
                <asp:HiddenField ID="h_SubActivity_ID" runat="server" />
            </div>
        </div>
    </div>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="popup" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <script type="text/javascript" language="javascript">

        function InitStudentAutoComplete() {
           
            var ActivityId = document.getElementById("<%=ddlActivtyTypes.ClientID%>").value;
          
            $("#<%=txtSubActivityName.ClientID%>").autocomplete({                         
                source: function (request, response) {
                   
                    $.ajax({
                        url: "/phoenixbeta/StudentServices/Services_CreateActivityList.aspx/GetTrips",
                        data: "{ 'Id': '" + ActivityId + "','prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {                            
                            response($.map(data.d, function (item) { 
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]                                   
                                }                               
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=h_SubActivity_ID.ClientID%>").val(i.item.val);
                    $("#<%=txtSubActivityName.ClientID%>").val(i.item.label);                  
                },
                minLength: 1
            });
        }
        function getTripID(mode) {
            var url;
            document.getElementById("<%=hf_mode.ClientID%>").value = mode
            var ActivityId=document.getElementById("<%=ddlActivtyTypes.ClientID%>").value
            if (mode == 'AST') {
                url = "../Accounts/accShowEmpDetail.aspx?id=AST&EMPNO="+ActivityId

            }            
            var oWnd = radopen(url, "pop_emp");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (document.getElementById("<%=hf_mode.ClientID%>").value == "AST") {
                    document.getElementById("<%=txtSubActivityName.ClientID %>").value = NameandCode[0];
                    document.getElementById("<%=h_SubActivity_ID.ClientID %>").value = NameandCode[1];
                }
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        $('table').dataTable({ searching: false, paging: false });

        function setSelectedID(userText) {

            if (userText == null) {
                $('#<%= hidID.ClientID%>').val("0");
                var hv = $('#<%= hidID.ClientID%>').val();

                MyConfirmMethod(hv);
            }
            else {
                //   $('#<%= hidID.ClientID%>').val(userTextrepdocview);
                $('#<%= hidID.ClientID%>').val(userText);
                var hv = $('#<%= hidID.ClientID%>').val();

                MyConfirmMethod(hv);
            }

        }

        //function showDocument(filename, contenttype) {

        //    result = radopen("/PHOENIXBETA/Students/IFrameNew.aspx?filename=" + filename + "&contenttype=" + contenttype, "popup");
        //    return false;
        //}


        function MyConfirmMethod(hv) {

            if (confirm('Are your sure?')) {

                $.ajax({
                    type: "POST",
                    url: "/phoenixbeta/StudentServices/Services_CreateActivityList.aspx/a",
                    data: '{ Id: " ' + hv + ' "}',
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {

                        //useresult(results.d);           
                        var a = results.d.split('||');

                        $('#<%= txtEventName.ClientID%>').val(a[0]);
                        $('#<%= txtEventDesc.ClientID%>').val(a[1]);
                        $("#<%= ddlActivtyTypes.ClientID%>").val(a[2]).attr("selected", "selected");
                        $('#<%= txtAmount.ClientID%>').val(a[3]);




                        if (String(a[4]) == "Yes") {

                            $('#<%=radioButtonList.ClientID%>').find("input[value='0']").prop("checked", true);
                        }
                        else {

                            $('#<%=radioButtonList.ClientID%>').find("input[value='1']").prop("checked", true);
                        }

                        if (a[7] == 'B') {
                            $('#<%=rdGender.ClientID%>').find("input[value='0']").prop("checked", true);
                        } else if (a[7] == 'M') {
                            $('#<%=rdGender.ClientID%>').find("input[value='1']").prop("checked", true);
                        } else if (a[7] == 'F') {
                            $('#<%=rdGender.ClientID%>').find("input[value='2']").prop("checked", true);
                        }
                        $('#<%= txtCount.ClientID%>').val(a[8]);

                        if (a[9] == "True") {

                            $('#<%=rdoAllowMultiple.ClientID%>').find("input[value='Yes']").prop("checked", true);
                        } else {
                            $('#<%=rdoAllowMultiple.ClientID%>').find("input[value='No']").prop("checked", true);
                        }

                        // nt working                                                                          
                        var editor = $find("<%=txtEmailTemplate.ClientID%>");

                        editor.set_html(a[5]);
                        $telerik.findEditor('#<%=txtEmailTemplate.ClientID%>').set_html(a[5]);


                        __doPostBack("<%=ddlActivtyTypes.ClientID%>", "SelectedIndexChanged");

                        $("#<%= ddlSubGrp.ClientID%>").val(a[6]).attr("selected", "selected");
                    }
                });

                return true;
            }
            else {
                return false;
            }
        }


        function ShowWindowWithClose(gotourl, pageTitle, w, h) {

            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();
                    if (hfPostBack == "Y")
                        window.location.reload(true);
                },
                afterClose: function () {
                    //window.location.reload();

                }
            });
            return false;
        }


        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $('#<%= txtApplSt.ClientID%>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2050',
                    dateFormat: "dd/M/yy"
                });

                $('#<%= txtApplEnd.ClientID%>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2050',
                    dateFormat: "dd/M/yy"
                });

                $('#<%= txtEventSt.ClientID%>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2050',
                    dateFormat: "dd/M/yy"
                });

                $('#<%= txtEventEnd.ClientID%>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2050',
                    dateFormat: "dd/M/yy"
                });

                $('#<%= txtRevnDt.ClientID%>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1950:2050',
                    dateFormat: "dd/M/yy"
                });

            }

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitStudentAutoComplete();
            // TripsAutoComplete();
        });

        function InitializeRequest(sender, args) {
        }
        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
          
            InitStudentAutoComplete();
            //CheckForPrint();
        }

        function TripsAutoComplete() {
            <%-- $("#<%=.ClientID%>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "../StudentServices/Services_CreateActivityList.aspx/GetTrips",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    },
                    select: function (e, i) {
                        alert(i.item.val);
                        alert(i.item.label);
                        $("#<%=h_Company_ID.ClientID%>").val(i.item.val);
                    },
                    minLength: 1
                });--%>
        }


        function pageLoad() {
            $('#<%= txtApplSt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtApplEnd.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtEventSt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtEventEnd.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtRevnDt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

        }

        function click_me() {
            $('#<%= txtApplSt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtApplEnd.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtEventSt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtEventEnd.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

            $('#<%= txtRevnDt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });

        }

        function TPick(txt) {

            $("#" + txt).timepicker(
                {
                    showPeriod: true,
                    onHourShow: OnHourShowCallback,
                    onMinuteShow: OnMinuteShowCallback
                });

        }

        function OnHourShowCallback(hour) {
            if ((hour > 20) || (hour < 6)) {
                return false; // not valid
            }
            return true; // valid
        }
        function OnMinuteShowCallback(hour, minute) {
            if ((hour == 20) && (minute >= 30)) { return false; } // not valid
            if ((hour == 6) && (minute < 30)) { return false; }   // not valid
            return true;  // valid
        }


        function Confirm() {
            return confirm('Are you sure you want to continue?');
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function btnSaveConfirm() {
            var btnName = document.getElementById("<%=btnSave.ClientID%>").value;
            if (btnName == 'Submit') {
                var EventName = document.getElementById("<%=txtEventName.ClientID%>").value;
                var EventDesc = document.getElementById("<%=txtEventDesc.ClientID()%>").value
                var applstdt = document.getElementById("<%=txtApplSt.ClientID%>").value;
                var applenddt = document.getElementById("<%=txtApplEnd.ClientID%>").value;
                var evstdt = document.getElementById("<%=txtEventSt.ClientID%>").value;
                var evenddt = document.getElementById("<%=txtEventEnd.ClientID%>").value;
                var amnt = document.getElementById("<%=txtAmount.ClientID%>").value;
                if (!EventName == '') {
                    if (!EventDesc == '') {
                        if (!applstdt == '') {
                            if (!applenddt == '') {
                                if (!evstdt == '') {
                                    if (!evenddt == '') {
                                        if (!amnt == '') {
                                            return confirm('Are you sure want to submit');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                return confirm('Are you sure want to approve?');
            }
        }



        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }

        function fnMouseOver(div) {
            div.style.display = "block";
        }

        function fnMouseOut(div) {
            div.style.display = "none";
        }

    </script>

</asp:Content>
