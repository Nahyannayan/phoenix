﻿''''''' PAGE CREATED BY ATHIRA KM 
''''''' ON 01/04/2019 
''''''' CHANGES FOR - ADDING MASTER TYPE, SUBTYPE AND GROUPS
''''''' MENU --  STUDENT -> MASTER -> ADD ACTIVITY TYPES

Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Web.Script.Services
Imports Telerik.Web.UI

Partial Class StudentServices_Services_CreateActivityGroups
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try

                ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))

                ViewState("datamode") = "none"
                'If Request.QueryString("ACD_ID") <> "" Then
                '    ViewState("ACD_ID") = Encr_decrData.Decrypt(Request.QueryString("ACD_ID").Replace(" ", "+"))
                'Else
                '    ViewState("ACD_ID") = ""
                'End If
                ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))

                clearall()
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Page_Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
    End Sub

    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Protected Sub mnuMaster_MenuItemClick(sender As Object, e As MenuEventArgs)
        Try
            Dim Iindex As Integer = Int32.Parse(e.Item.Value)
            Dim i As Integer
            clearall()
            ShowMessage("", False)
            'Make the selected menu item reflect the correct imageurl
            For i = 0 To mnuMaster.Items.Count - 1
                Select Case i
                    Case 0
                        mvMaster.ActiveViewIndex = Iindex
                    Case 1
                        mvMaster.ActiveViewIndex = Iindex
                    Case 2
                        mvMaster.ActiveViewIndex = Iindex
                End Select
            Next
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From mnuMaster_MenuItemClick: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub clearall()
        btnSave.Text = "Create"
        txtGroupDescr.Text = ""
        txtGroupName.Text = ""
        txtSubGroup.Text = ""
        txtSubGroupDescr.Text = ""
        ddlGrp.ClearSelection()
        ' txtGrpLimit.Text = ""
        ' txtSubGrpLimit.Text = ""
        ' fillGroups()
        ' ddlActivtyTypes.ClearSelection()
        fillActivityTypes()
        bindGroup()
        bindSubgroup()
        bindgradeddl()
        bindActddl()
        bindGridAct()
    End Sub

    Sub bindGridAct()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim params(1) As SqlParameter
        params(0) = Mainclass.CreateSqlParameter("@BSUID", Session("sbsuid"), SqlDbType.VarChar)
        params(1) = Mainclass.CreateSqlParameter("@ACDID", Session("Current_ACD_ID"), SqlDbType.BigInt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ACTIVITY_GROUPS_ON_SEARCH]", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                gvActDetails.DataSource = ds
                gvActDetails.DataBind()
                ViewState("Dataset_Grp") = ds.Tables(0)
            End If
        End If
    End Sub
    Sub bindActddl()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim params(3) As SqlParameter
        params(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
        params(1) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID"), SqlDbType.BigInt)
        params(2) = Mainclass.CreateSqlParameter("@GRADE", ddlGrade.SelectedValue, SqlDbType.VarChar)
        params(3) = Mainclass.CreateSqlParameter("@SECTION", ddlSection.SelectedValue, SqlDbType.VarChar)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ACTIVITIES_ON_SEARCH]", params)
        If (ds.Tables.Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlEvent.DataSource = ds
                ddlEvent.DataValueField = "ALD_ID"
                ddlEvent.DataTextField = "ALD_EVENT_NAME"
                ddlEvent.DataBind()
                For Each rowACD As DataRow In ds.Tables(0).Rows
                    If rowACD("APD_COUNT") > 0 Then
                        ddlEvent.Items.FindItemByValue(rowACD("ALD_ID")).Enabled = False                        
                    End If
                Next
            End If
        End If
    End Sub

    Sub bindgradeddl()
        '[OASIS].[GET_ALL_GRADES_FROM_ACDID]
        ddlGrade.ClearSelection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim params(1) As SqlParameter
        params(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
        params(1) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID"), SqlDbType.BigInt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_GRADES_FROM_ACDID]", params)
        If (ds.Tables.Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlGrade.DataSource = ds
                ddlGrade.DataValueField = "GRM_GRD_ID"
                ddlGrade.DataTextField = "GRM_GRD_ID"
                ddlGrade.DataBind()
            End If
        End If
        ddlGrade.Items.Insert(0, New ListItem("ALL", 0))
        ddlGrade.Items.FindByText("ALL").Selected = True
        bindsectionddl()
    End Sub

    Sub bindsectionddl()
        '[OASIS].[GET_ALL_SECTION_FROM_ACDID]
        ddlSection.ClearSelection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim params(2) As SqlParameter
        params(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
        params(1) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID"), SqlDbType.BigInt)
        params(2) = Mainclass.CreateSqlParameter("@GRADE", ddlGrade.SelectedValue, SqlDbType.VarChar)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_SECTION_FROM_GRADE]", params)
        If (ds.Tables.Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataBind()
            End If
        End If
        ddlSection.Items.Insert(0, New ListItem("ALL", 0))
        ddlSection.Items.FindByText("ALL").Selected = True

        bindActddl()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        strans = objConn.BeginTransaction
        Try
            If (btnSave.Text = "Update") Then
                If (mvMaster.ActiveViewIndex = 0) Then
                    Dim pParams(3) As SqlParameter
                    Dim spName As String = "[OASIS].[ADD_EDIT_GROUPS_ACTIVITY]"
                    pParams(0) = Mainclass.CreateSqlParameter("@OPTION", "E", SqlDbType.Char)
                    If (hdn_group_id.Value = 0) Then
                        Message = getErrorMessage("4000")
                        ShowMessage(Message, True)
                        Exit Sub
                    Else
                        pParams(1) = Mainclass.CreateSqlParameter("@ACT_TYPE_ID", hdn_group_id.Value, SqlDbType.Int)
                    End If
                    pParams(2) = Mainclass.CreateSqlParameter("@GROUPNAME", txtGroupName.Text, SqlDbType.VarChar)
                    pParams(3) = Mainclass.CreateSqlParameter("@GROUPDESCR", txtGroupDescr.Text, SqlDbType.VarChar)
                    '  pParams(4) = Mainclass.CreateSqlParameter("@GROUPICON", txtImage.Text, SqlDbType.VarChar)
                    '  pParams(5) = Mainclass.CreateSqlParameter("@MAX_LIMIT", Convert.ToInt64(txtGrpLimit.Text), SqlDbType.BigInt)
                    SqlHelper.ExecuteNonQuery(strans, spName, pParams)
                    lblError.Text = "Group Updated Succesfully"
                ElseIf (mvMaster.ActiveViewIndex = 1) Then
                    If ddlGrp.SelectedValue = 0 Then
                        ShowMessage("Please select any activity type", True)
                    Else
                        Dim spName As String = "[OASIS].[ADD_EDIT_SUB_GROUPS_ACTIVITY]"
                        Dim params(6) As SqlParameter
                        params(0) = Mainclass.CreateSqlParameter("@OPTION", "E", SqlDbType.Char)
                        If (hdn_subgroup_id.Value = 0) Then
                            Message = getErrorMessage("4000")
                            ShowMessage(Message, True)
                            Exit Sub
                        Else
                            params(1) = Mainclass.CreateSqlParameter("@ASD_ID", hdn_subgroup_id.Value, SqlDbType.Int)
                        End If
                        params(2) = Mainclass.CreateSqlParameter("@ACT_TYPE_ID", ddlGrp.SelectedValue, SqlDbType.BigInt)
                        params(3) = Mainclass.CreateSqlParameter("@SUBGROUPNAME", txtSubGroup.Text, SqlDbType.VarChar)
                        params(4) = Mainclass.CreateSqlParameter("@SUBGROUPDESCR", txtSubGroupDescr.Text, SqlDbType.VarChar)
                        params(5) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        params(6) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID"), SqlDbType.BigInt)
                        '  params(7) = Mainclass.CreateSqlParameter("@MAX_LIMIT", Convert.ToInt64(txtSubGrpLimit.Text), SqlDbType.BigInt)
                        SqlHelper.ExecuteNonQuery(strans, spName, params)
                        lblError.Text = "Sub Group Updated Succesfully"
                    End If
                ElseIf (mvMaster.ActiveViewIndex = 2) Then
                    If ddlEvent.CheckedItems.Count < Convert.ToInt16(txtMaxLimit.Text) Then
                        lblError.Text = "Selected activity count is less than maximum count"
                        Exit Sub
                    ElseIf Convert.ToInt16(txtMaxLimit.Text) = 0 Then
                        lblError.Text = "Maximum count must be higher than 0"
                        Exit Sub
                    Else
                        Dim Ald_ids As String = ""
                        For Each item In ddlEvent.CheckedItems
                            If Ald_ids = "" Then
                                Ald_ids = item.Value
                            Else
                                Ald_ids += "," + item.Value
                            End If
                        Next
                        '  [OASIS].[ADD_EDIT_GROUPS]
                        Dim params_act(7) As SqlParameter
                        params_act(0) = Mainclass.CreateSqlParameter("@ALD_IDS", Ald_ids, SqlDbType.Char)
                        params_act(1) = Mainclass.CreateSqlParameter("@MAXLIMIT", Convert.ToInt16(txtMaxLimit.Text), SqlDbType.Int)
                        params_act(2) = Mainclass.CreateSqlParameter("@GRP_NAME", txtGrpName.Text, SqlDbType.VarChar)
                        params_act(3) = Mainclass.CreateSqlParameter("@GRP_DESCR", txtGrpDescr.Text, SqlDbType.VarChar)
                        params_act(4) = Mainclass.CreateSqlParameter("@VIEWID", hdn_actgrps.Value, SqlDbType.BigInt)
                        params_act(5) = Mainclass.CreateSqlParameter("@TYPE", "E", SqlDbType.Char)
                        params_act(6) = Mainclass.CreateSqlParameter("@BSUID", Session("Sbsuid"), SqlDbType.VarChar)
                        params_act(7) = Mainclass.CreateSqlParameter("@LOGUSER", ViewState("EmployeeID"), SqlDbType.VarChar)
                        SqlHelper.ExecuteNonQuery(strans, "[OASIS].[ADD_EDIT_GROUPS]", params_act)
                        lblError.Text = "Activity Group Updated Succesfully"
                    End If
                End If
            ElseIf (btnSave.Text = "Create") Then
                If (mvMaster.ActiveViewIndex = 0) Then
                    Dim pParams(3) As SqlParameter
                    Dim spName As String = "[OASIS].[ADD_EDIT_GROUPS_ACTIVITY]"
                    pParams(0) = Mainclass.CreateSqlParameter("@OPTION", "A", SqlDbType.Char)
                    pParams(1) = Mainclass.CreateSqlParameter("@ACT_TYPE_ID", 0, SqlDbType.Int)
                    pParams(2) = Mainclass.CreateSqlParameter("@GROUPNAME", txtGroupName.Text, SqlDbType.VarChar)
                    pParams(3) = Mainclass.CreateSqlParameter("@GROUPDESCR", txtGroupDescr.Text, SqlDbType.VarChar)
                    '  pParams(4) = Mainclass.CreateSqlParameter("@GROUPICON", txtImage.Text, SqlDbType.VarChar)
                    '   pParams(5) = Mainclass.CreateSqlParameter("@MAX_LIMIT", Convert.ToInt64(txtGrpLimit.Text), SqlDbType.BigInt)
                    Dim count As Integer = SqlHelper.ExecuteScalar(strans, spName, pParams)
                    If count = 0 Then
                        ShowMessage("Group Updated Succesfully", False)
                    Else
                        ShowMessage("There is already activity type exists with this name", True)
                    End If
                ElseIf (mvMaster.ActiveViewIndex = 1) Then
                    If ddlGrp.SelectedValue = 0 Then
                        ShowMessage("Please select any activity type", True)
                    Else
                        Dim spName As String = "[OASIS].[ADD_EDIT_SUB_GROUPS_ACTIVITY]"
                        Dim params(6) As SqlParameter
                        params(0) = Mainclass.CreateSqlParameter("@OPTION", "A", SqlDbType.Char)
                        params(1) = Mainclass.CreateSqlParameter("@ASD_ID", 0, SqlDbType.Int)
                        params(2) = Mainclass.CreateSqlParameter("@ACT_TYPE_ID", ddlGrp.SelectedValue, SqlDbType.BigInt)
                        params(3) = Mainclass.CreateSqlParameter("@SUBGROUPNAME", txtSubGroup.Text, SqlDbType.VarChar)
                        params(4) = Mainclass.CreateSqlParameter("@SUBGROUPDESCR", txtSubGroupDescr.Text, SqlDbType.VarChar)
                        params(5) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                        params(6) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID"), SqlDbType.BigInt)
                        ' params(7) = Mainclass.CreateSqlParameter("@MAX_LIMIT", Convert.ToInt64(txtSubGrpLimit.Text), SqlDbType.BigInt)
                        Dim count As Integer = SqlHelper.ExecuteScalar(strans, spName, params)
                        If count = 0 Then
                            ShowMessage("Sub Group Created Succesfully", False)
                        Else
                            ShowMessage("There is already sub group exists with this name", True)
                        End If
                        'SqlHelper.ExecuteNonQuery(strans, spName, params)
                        '  lblError.Text = "Sub Group Created Succesfully"
                    End If
                ElseIf (mvMaster.ActiveViewIndex = 2) Then
                    If ddlEvent.CheckedItems.Count < Convert.ToInt16(txtMaxLimit.Text) Then
                        lblError.Text = "Selected activity count is less than maximum count"
                        Exit Sub
                    ElseIf Convert.ToInt16(txtMaxLimit.Text) = 0 Then
                        lblError.Text = "Maximum count must be higher than 0"
                        Exit Sub
                    Else
                        Dim Ald_ids As String = ""
                        For Each item In ddlEvent.CheckedItems
                            If Ald_ids = "" Then
                                Ald_ids = item.Value
                            Else
                                Ald_ids += "," + item.Value
                            End If
                        Next

                        '  [OASIS].[ADD_EDIT_GROUPS]
                        Dim params_act(7) As SqlParameter
                        params_act(0) = Mainclass.CreateSqlParameter("@ALD_IDS", Ald_ids, SqlDbType.Char)
                        params_act(1) = Mainclass.CreateSqlParameter("@MAXLIMIT", Convert.ToInt16(txtMaxLimit.Text), SqlDbType.Int)
                        params_act(2) = Mainclass.CreateSqlParameter("@GRP_NAME", txtGrpName.Text, SqlDbType.VarChar)
                        params_act(3) = Mainclass.CreateSqlParameter("@GRP_DESCR", txtGrpDescr.Text, SqlDbType.VarChar)
                        params_act(4) = Mainclass.CreateSqlParameter("@VIEWID", 0, SqlDbType.BigInt)
                        params_act(5) = Mainclass.CreateSqlParameter("@TYPE", "I", SqlDbType.Char)
                        params_act(6) = Mainclass.CreateSqlParameter("@BSUID", Session("Sbsuid"), SqlDbType.VarChar)
                        params_act(7) = Mainclass.CreateSqlParameter("@LOGUSER", ViewState("EmployeeID"), SqlDbType.VarChar)
                        SqlHelper.ExecuteNonQuery(strans, "[OASIS].[ADD_EDIT_GROUPS]", params_act)
                        lblError.Text = "Activity Group Created Succesfully"
                    End If
                End If
            End If
            strans.Commit()
            ShowMessage(lblError.Text, False)
            clearall()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Private Sub fillActivityTypes()
        'FILLING ACTIVITY TYPES TO ACTIVITY DROP DOWN LIST
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_ACTIVITY_TYPES]")
            If (dsActivityTypes.Tables(0).Rows.Count > 0) Then
                ddlGrp.DataSource = dsActivityTypes.Tables(0)
                ddlGrp.DataBind()
                ddlGrp.DataTextField = "ACTIVITY_NAME"
                ddlGrp.DataValueField = "ACTIVITY_ID"
                ddlGrp.DataBind()
            End If
            ddlGrp.Items.Insert(0, New ListItem("SELECT", 0))
            ddlGrp.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillActivityTypes: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Sub bindGroup()
        Try
            '[OASIS].[GET_ALL_GROUP_DETAILS_TOBIND]
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim params(1) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            params(1) = Mainclass.CreateSqlParameter("@OPTION", "G", SqlDbType.Char)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_GROUP_DETAILS_TO_BIND]", params)
            If ds.Tables.Count > 0 Then
                ViewState("Dataset_Group") = ds.Tables(0)
            End If
            grdGroup.DataSource = ds
            grdGroup.DataBind()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From bindgroup " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Sub bindSubgroup()
        Try
            '[OASIS].[GET_ALL_GROUP_DETAILS_TOBIND]
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim params(1) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            params(1) = Mainclass.CreateSqlParameter("@OPTION", "S", SqlDbType.Char)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_GROUP_DETAILS_TO_BIND]", params)
            ViewState("Dataset_SubGroup") = ds.Tables(0)
            grdSubGroup.DataSource = ds
            grdSubGroup.DataBind()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From bindSubgroup " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub grdGroup_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            grdGroup.PageIndex = e.NewPageIndex
            bindGroup()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From grdGroup_PageIndexChanging " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub

    Protected Sub grdSubGroup_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            grdSubGroup.PageIndex = e.NewPageIndex
            bindSubgroup()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From grdSubGroup_PageIndexChanging " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub gvActDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvActDetails.PageIndex = e.NewPageIndex
        bindGridAct()
    End Sub

    Protected Sub btnEdit_S_Command(sender As Object, e As CommandEventArgs)
        Try
            btnSave.Text = "Update"
            Dim dt As DataTable = ViewState("Dataset_SubGroup")
            If (dt.Rows.Count > 0) Then
                Dim row() As DataRow = dt.Select("ID =" & e.CommandArgument)
                If row.Length > 0 Then
                    hdn_subgroup_id.Value = row(0)("ID")
                    txtSubGroup.Text = row(0)("NAME")
                    txtSubGroupDescr.Text = row(0)("DESCR")
                    ddlGrp.ClearSelection()
                    ddlGrp.Items.FindByText(row(0)("TYPE")).Selected = True
                    '  txtSubGrpLimit.Text = row(0)("MAX")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnEdit_S_Command " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub btnDelete_S_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim strans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim spName As String = "[OASIS].[ADD_EDIT_SUB_GROUPS_ACTIVITY]"
            Dim params(6) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@OPTION", "D", SqlDbType.Char)
            params(1) = Mainclass.CreateSqlParameter("@ASD_ID", e.CommandArgument, SqlDbType.Int)
            params(2) = Mainclass.CreateSqlParameter("@ACT_TYPE_ID", ddlGrp.SelectedValue, SqlDbType.BigInt)
            params(3) = Mainclass.CreateSqlParameter("@SUBGROUPNAME", txtSubGroup.Text, SqlDbType.VarChar)
            params(4) = Mainclass.CreateSqlParameter("@SUBGROUPDESCR", txtSubGroupDescr.Text, SqlDbType.VarChar)
            params(5) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            params(6) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID"), SqlDbType.BigInt)
            ' params(7) = Mainclass.CreateSqlParameter("@MAX_LIMIT", 0, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, spName, params)
            strans.Commit()
            lblError.Text = "Sub Group got deleted"
            clearall()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnDelete_S_Command " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub btnEdit_Command(sender As Object, e As CommandEventArgs)
        Try
            btnSave.Text = "Update"
            Dim dt As DataTable = ViewState("Dataset_Group")
            If (dt.Rows.Count > 0) Then
                Dim row() As DataRow = dt.Select("ID =" & e.CommandArgument)
                If row.Length > 0 Then
                    hdn_group_id.Value = row(0)("ID")
                    txtGroupName.Text = row(0)("NAME")
                    txtGroupDescr.Text = row(0)("DESCR")
                    '  txtGrpLimit.Text = row(0)("MAX")
                    '  txtImage.Text = row(0)("ICON")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnEdit_Command " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub btnDelete_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim strans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim pParams(3) As SqlParameter
            Dim spName As String = "[OASIS].[ADD_EDIT_GROUPS_ACTIVITY]"
            pParams(0) = Mainclass.CreateSqlParameter("@OPTION", "D", SqlDbType.Char)
            pParams(1) = Mainclass.CreateSqlParameter("@ACT_TYPE_ID", e.CommandArgument, SqlDbType.Int)
            pParams(2) = Mainclass.CreateSqlParameter("@GROUPNAME", txtGroupName.Text, SqlDbType.VarChar)
            pParams(3) = Mainclass.CreateSqlParameter("@GROUPDESCR", txtGroupDescr.Text, SqlDbType.VarChar)
            'pParams(4) = Mainclass.CreateSqlParameter("@GROUPICON", txtImage.Text, SqlDbType.VarChar)
            '  pParams(5) = Mainclass.CreateSqlParameter("@MAX_LIMIT", 0, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, spName, pParams)
            strans.Commit()
            lblError.Text = "Group got deleted."
            clearall()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnDelete_Command " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)
        bindsectionddl()
    End Sub

    Protected Sub lnkEditact_Command(sender As Object, e As CommandEventArgs)
        btnSave.Text = "Update"
        Dim dt As DataTable = ViewState("Dataset_Grp")
        If (dt.Rows.Count > 0) Then
            Dim row() As DataRow = dt.Select("ID =" & e.CommandArgument)
            If row.Length > 0 Then
                hdn_actgrps.Value = row(0)("ID")
                txtGrpName.Text = row(0)("NAME")
                txtGrpDescr.Text = row(0)("DESCRIPTION")
                txtMaxLimit.Text = row(0)("MAX")
                Dim aldids As String = row(0)("ALD_IDS")
                Dim ALD_ID() As String = aldids.Split(",")
                '2,13,14
                For Each item As RadComboBoxItem In ddlEvent.Items
                    For Each id As String In ALD_ID
                        If item.Value = id Then
                            item.Checked = True
                        End If
                    Next
                Next
            End If
        End If
    End Sub

    Protected Sub lnkDeleteAct_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            '[OASIS].[ADD_EDIT_GROUPS]
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim params_act(7) As SqlParameter
            params_act(0) = Mainclass.CreateSqlParameter("@ALD_IDS", "", SqlDbType.Char)
            params_act(1) = Mainclass.CreateSqlParameter("@MAXLIMIT", 0, SqlDbType.Int)
            params_act(2) = Mainclass.CreateSqlParameter("@GRP_NAME", txtGrpName.Text, SqlDbType.VarChar)
            params_act(3) = Mainclass.CreateSqlParameter("@GRP_DESCR", txtGrpDescr.Text, SqlDbType.VarChar)
            params_act(4) = Mainclass.CreateSqlParameter("@VIEWID", e.CommandArgument, SqlDbType.BigInt)
            params_act(5) = Mainclass.CreateSqlParameter("@TYPE", "D", SqlDbType.Char)
            params_act(6) = Mainclass.CreateSqlParameter("@BSUID", Session("Sbsuid"), SqlDbType.VarChar)
            params_act(7) = Mainclass.CreateSqlParameter("@LOGUSER", ViewState("EmployeeID"), SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[ADD_EDIT_GROUPS]", params_act)
            strans.Commit()
            lblError.Text = "Group got deleted."
            clearall()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From lnkDeleteAct_Command " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(sender As Object, e As EventArgs)
        bindActddl()
    End Sub

   
End Class
