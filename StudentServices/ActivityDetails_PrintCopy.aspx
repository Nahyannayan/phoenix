﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ActivityDetails_PrintCopy.aspx.vb" Inherits="StudentServices_ActivityDetails_PrintCopy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        .img {
            display: block;
        }

        .ReceiptCaption {
            FONT-WEIGHT: bold;
            FONT-SIZE: 10pt;
            COLOR: #000095;
            FONT-FAMILY: Verdana;
            HEIGHT: 19px;
        }

        .matters_print {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #000095;
        }

        .matters_heading {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 13px;
            font-weight: bold;
            color: #000095;
        }

        .matters_normal {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
        }

        .matters_grid {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
            TEXT-INDENT: 20px;
        }

        .matters_small {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .Printbg {
            vertical-align: middle;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .PrintSource {
            vertical-align: bottom;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
            width: 50px;
        }

        .columnwidth {
            max-width: 15% !important;
            width: 25% !important;
        }

        .Firstcolumnwidth {
            max-width: 30% !important;
            width: 30% !important;
        }
    </style>

    
    <link href ="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />

    <title>Print Activity Details </title>
    <script language="javascript" type="text/javascript">
        function PrintReceiptExport() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
            try {
                if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                    var ieversion = new Number(RegExp.$1) // capture x.x portion and store as a number
                    if (ieversion >= 8)
                        window.close();
                }
            }
            catch (ex) { }
        }
        function PrintReceipt() {
            if ('<%= Request.QueryString("isexport") %>' == '0')
                 PrintReceiptExport();
         }
         function OnEscape() {
             if ((event.keyCode == 27) || (event.keyCode == 99) || (event.keyCode == 120))
                 window.close();
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="sm" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <table width="100%">
                <tr valign="top" id="tr_Print">
                    <td align="right" colspan="4">
                        <asp:UpdatePanel ID="UP1" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblMessage" Text="" Font-Size="Small" ForeColor="red" runat="server"></asp:Label>
                                <img src="../Images/Misc/print.gif" onclick="PrintReceiptExport();" alt="Print" style="cursor: hand" />
                                <asp:HiddenField ID="hf_FCLID" runat="server" />
                                <asp:HiddenField ID="hf_RecNo" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                
                <tr>
                   <td colspan="4">
                       <asp:PlaceHolder ID = "PlaceHolder1" runat="server" />
                   </td>
                </tr>
            </table>
        </div>                
    </form>
</body>
</html>
