﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class StudentServices_Services_ApproveActivtyPrincipal
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = FindControl("ScriptManager1")
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Else
                        ViewState("ViewId") = ""
                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)                  
                    ' Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
                    ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))
                    '  lblAED.InnerText = Session("BSU_CURRENCY")
                    GetApprovalDetlsForPrncpl(Convert.ToInt32(ViewState("ViewId")))
                    fillGrades()
                    If ViewState("MainMnu_code") = "S103301" Then
                        If (ViewState("datamode") = "approve") Then
                            lblheading.Text = "Activity Request For Approval"                           
                        ElseIf (ViewState("datamode") = "view") Then
                            lblheading.Text = "View Activity Request"                            
                            NoButtons()
                            GetActivityViewForApprvl(Convert.ToInt32(ViewState("ViewId")))
                        End If
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If (txtComments3.InnerText Is Nothing Or txtComments3.InnerText = "") Then
            Message = getErrorMessage("4022")
            ShowMessage(Message, True)
        Else
            UPD_ACTIVITY_ON_PRINCIPAL_APPRVL()
        End If
    End Sub
    Private Sub fillGrades()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_GRADE_DETAILS_TOSHOW]", Param)
            Dim dt As DataTable = DS.Tables(0)

            For Each mrow In dt.Rows
                Dim flag As Boolean = Checkallgradesselected(mrow("SECTIONS"), mrow("GRADE"))
                If (flag = 0) Then
                    mrow("SECTIONS") = "ALL SELECTED"
                Else

                End If
            Next
            gvGrades.DataSource = dt
            gvGrades.DataBind()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillGrades: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Function Checkallgradesselected(ByVal ROESECTIONS As String, ByVal GRADE As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(2) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", hidacdid.Value, SqlDbType.BigInt)
        Param(2) = Mainclass.CreateSqlParameter("@GRADE_ID", GRADE, SqlDbType.VarChar)
        Dim Sections As String = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[CHECK_ALL_GRADES_INSCHOOL]", Param)
        If (Sections = ROESECTIONS) Then
            Return 0
        Else
            Return 1
        End If
    End Function
    Private Sub UPD_ACTIVITY_ON_PRINCIPAL_APPRVL()
        Dim strans As SqlTransaction = Nothing
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        Try
            If (txtComments3.InnerText Is Nothing Or txtComments3.InnerText = "") Then
                ' lblError.Text = " Please Enter yout comments"
                Message = getErrorMessage("4022")
                ShowMessage(Message, True)
            ElseIf chkaccpt.Checked = False Then
                Message = getErrorMessage("4041")
                ShowMessage(Message, True)
            Else
                Dim Params(2) As SqlParameter
                Dim objConn As New SqlConnection(st_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                Params(0) = Mainclass.CreateSqlParameter("@COMMENTS", txtComments3.InnerText, SqlDbType.VarChar)
                Params(1) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
                Params(2) = Mainclass.CreateSqlParameter("@VIEWD", ViewState("ViewId"), SqlDbType.BigInt)               
                SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UPDATE_ACTIVITY_WITH_PRNCPL_APPRVL]", Params)
                strans.Commit()
                CreateNotificationEntry()
                'lblError.Text = "Congratulations ! Successfully Completed  the Activity "
                Message = getErrorMessage("4023")
                ShowMessage(Message, False)
                NoButtons()
            End If
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From UPD_ACTIVITY_ON_PRINCIPAL_APPRVL: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub CreateNotificationEntry()
        Dim Today As DateTime = DateTime.Today
        Dim NotificationDateTime As DateTime
        Dim strans As SqlTransaction = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParams(6) As SqlParameter
        Dim Params(5) As SqlParameter
        Dim qry1 = "[DBO].[INSERT_NOTIFICATIONLIST_ONCREATION]"
        Dim qry2 = "[DBO].[INSERT_NOTIFICATIONGRADES_ONCREATION]"
        Dim GET_SELECTED_SECTIONS As String = ""
        Dim GET_SELECTED_GRADES As String = ""
        Dim GET_SELECTED_SEC_DESCR As String = ""
        Try
            Dim ActivityContent As String = "Dear Parent, New activity" + """" + lblEventName.Text + """" + "is added to your student group, to check more details please click Activities tile on the home page, Thanks. "
            NotificationDateTime = DateTime.Today
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", hidacdid.Value, SqlDbType.Int)
            pParams(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            pParams(2) = Mainclass.CreateSqlParameter("@NOTIFICATION_CONTENT", ActivityContent, SqlDbType.VarChar)
            pParams(3) = Mainclass.CreateSqlParameter("@NOTIFICATION_DATETIME", NotificationDateTime, SqlDbType.DateTime)
            pParams(4) = Mainclass.CreateSqlParameter("@CREATOR", ViewState("EmployeeID").ToString, SqlDbType.VarChar)
            pParams(5) = Mainclass.CreateSqlParameter("@CATEGORY", 1, SqlDbType.BigInt)
            Latest_refid = SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry1, pParams)           
            Dim Paramss(0) As SqlParameter
            Paramss(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_SELECTED_ACTIVITY_ GRADES_TO_NOTIFICATION]", Paramss)
            For Each row In DS.Tables(0).Rows
                Params(0) = Mainclass.CreateSqlParameter("@REF_ID", Latest_refid, SqlDbType.Int)
                Params(1) = Mainclass.CreateSqlParameter("@GRD_ID", row("AGD_GRADE_ID"), SqlDbType.VarChar)
                Params(2) = Mainclass.CreateSqlParameter("@SCT_ID", row("AGD_SCT_ID"), SqlDbType.VarChar)
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
            Next
            strans.Commit()                      
        Catch ex As Exception
            strans.Rollback()
        End Try          
    End Sub
    Private Sub NoButtons()        
        rowbutton.Visible = False
        rowaccept.Visible = False
    End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then

            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
    Sub ShowSchedules(ByVal dt As DataTable)
        gvSchdl.DataSource = dt
        gvSchdl.DataBind()        
    End Sub

    Protected Sub GetActivityDocumentOnView()

        Dim query As String = "[OASIS].[GET_ACTIVITY_DOCUMENTS_TOVIEW]"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
        Dim dsDOCS As DataSet = SqlHelper.ExecuteDataset(str_conn, query, Param)
        If (dsDOCS.Tables.Count > 0) Then
            If dsDOCS.Tables(0).Rows.Count > 0 Then
                divupload.Visible = True
                repdocview.DataSource = dsDOCS.Tables(0)
                repdocview.DataBind()
            End If            
        End If
    End Sub
    Private Function PopulateData_toLoadtoPrincplPage(ByVal ViewId As Integer) As DataSet
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(0) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewId, SqlDbType.BigInt)
        ds = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ACTIVITY_DETAILS_ONVIEW]", Param)
        Return ds
    End Function
    Private Sub GetApprovalDetlsForPrncpl(ByVal viewid As Integer)
        Try
            Dim EmployeeName As String = ""
            rowPrincipal.Visible = True
            Dim ds As DataSet = PopulateData_toLoadtoPrincplPage(viewid)
            If ds.Tables.Count > 0 Then
                If Not ds.Tables(0) Is Nothing Then
                    Dim dt As DataTable = ds.Tables(0)
                    If Not dt Is Nothing And dt.Rows.Count > 0 Then
                        If Not dt Is Nothing Then
                            rowFinance.Visible = True
                            rowPaymentMode.Visible = True
                            rowFinanceCmmnts.Visible = True
                            lblShowGrades.Visible = True
                            lblslctgrade.Text = "Selected Grades"
                            If Not dt.Rows(0)("ALD_EVENT_NAME") Is Nothing Or Not dt.Rows(0)("ALD_EVENT_NAME") = "" Then
                                lblEventName.Text = dt.Rows(0)("ALD_EVENT_NAME")
                            Else
                                lblEventName.Text = "Event Name Is Not Available"
                            End If

                            If Not dt.Rows(0)("ALD_EVENT_DESCR") Is Nothing Or Not dt.Rows(0)("ALD_EVENT_DESCR") = "" Then
                                lblEventDesc.Text = dt.Rows(0)("ALD_EVENT_DESCR")
                            Else
                                lblEventDesc.Text = "Event Description Is Not Available"
                            End If

                            If Not dt.Rows(0)("EVENT_TYPE") Is Nothing Or Not dt.Rows(0)("EVENT_TYPE") = "" Then

                                lblActivtyTypes.Text = (dt.Rows(0)("EVENT_TYPE"))
                            Else
                                lblActivtyTypes.Text = "Event Type Is Not Available"
                            End If

                            If (Not dt.Rows(0)("ALD_ATM_ID") Is Nothing) Then
                                If (dt.Rows(0)("ATM_NAME") <> "") Then
                                    rowtrip.Visible = True
                                    lblTripName.Text = dt.Rows(0)("ATM_NAME")
                                End If
                            End If
                            If Not dt.Rows(0)("ACY_DESCR") Is Nothing Or Not dt.Rows(0)("ACY_DESCR") = "" Then
                                lblACDYear.Text = dt.Rows(0)("ACY_DESCR")
                            Else
                                lblACDYear.Text = "2016-2017"
                            End If
                            If Not dt.Rows(0)("ALD_ACDID") Is Nothing Or Not dt.Rows(0)("ALD_ACDID") = 0 Then
                                hidacdid.Value = dt.Rows(0)("ALD_ACDID")
                            Else
                                hidacdid.Value = 0
                            End If
                            If dt.Rows(0)("ALD_ISEMAIL") And Not dt.Rows(0)("ALD_EMAIL_TEMPLATE").ToString Is Nothing Or Not dt.Rows(0)("ALD_EMAIL_TEMPLATE").ToString = "" Then
                                lblmail.Text = Server.HtmlDecode(dt.Rows(0)("ALD_EMAIL_TEMPLATE").ToString)
                            Else
                                lblmail.Text = "No Emails Has Been Set"
                            End If

                            If dt.Rows(0)("ALD_ISTERMS") And Not dt.Rows(0)("ALD_TERMS_CONDITIONS") Is Nothing Or Not dt.Rows(0)("ALD_TERMS_CONDITIONS") = "" Then
                                lblterms.Text = dt.Rows(0)("ALD_TERMS_CONDITIONS").ToString
                            Else
                                lblterms.Text = "No Terms and Conditions Has Been Set"
                            End If

                            If Not dt.Rows(0)("ALD_APPLREQ_ST_DT") = "#1/1/1900#" Then
                                lblApplSt.Text = Format(dt.Rows(0)("ALD_APPLREQ_ST_DT"), "dd/MMM/yyyy")
                            Else
                                lblApplSt.Text = "Application Request Start Date Is Not Available"
                            End If

                            If Not dt.Rows(0)("ALD_APPLREQ_END_DT") = "#1/1/1900#" Then
                                lblApplEnd.Text = Format(dt.Rows(0)("ALD_APPLREQ_END_DT"), "dd/MMM/yyyy")
                            Else
                                lblApplEnd.Text = "Application Request End Date Is Not Available"
                            End If

                            If Not dt.Rows(0)("ALD_EVENT_ST_DT") = "#1/1/1900#" Then
                                lblEventSt.Text = Format(dt.Rows(0)("ALD_EVENT_ST_DT"), "dd/MMM/yyyy")
                            Else
                                lblEventSt.Text = "Event Start Date Is Not Available"
                            End If

                            If Not dt.Rows(0)("ALD_EVENT_END_DT") = "#1/1/1900#" Then
                                lblEventEnd.Text = Format(dt.Rows(0)("ALD_EVENT_END_DT"), "dd/MMM/yyyy")
                            Else
                                lblEventEnd.Text = "Event end date is not available"
                            End If

                            lblAmount.Text = Convert.ToString(dt.Rows(0)("ALD_EVENT_AMT")) + " " + "AED"

                            lblapprovalreq.Text = dt.Rows(0)("ALD_APPRVLREQ_FLAG")

                            If Not dt.Rows(0)("ALD_ACT_CREATOR_LOGDT") = "#1/1/1900#" Then
                                lblInitiatorLogDt.Text = "Activity request logged on : " + Format(dt.Rows(0)("ALD_ACT_CREATOR_LOGDT"), "dd/MMM/yyyy")
                            Else
                                lblInitiatorLogDt.Text = ""
                            End If

                            If Not dt.Rows(0)("ALD_ACT_CREATOR") Is Nothing Or Not dt.Rows(0)("ALD_ACT_CREATOR") = "" Then
                                EmployeeName = EOS_MainClass.GetEmployeeNameFromID(dt.Rows(0)("ALD_ACT_CREATOR"))
                            Else
                                EmployeeName = ""
                            End If
                            If Not dt.Rows(0)("ALD_ACT_CREATOR_CMMNTS") Is Nothing Or Not dt.Rows(0)("ALD_ACT_CREATOR_CMMNTS") = "" Then
                                lblinitiatorcomment.Text = lblinitiatorcomment.Text + "( " + EmployeeName + " )"
                                txtComments1.Text = dt.Rows(0)("ALD_ACT_CREATOR_CMMNTS")
                            Else
                                txtComments1.Text = ""
                            End If
                            If (dt.Rows(0)("ALD_FEE_REG_MODE")) = "FC" Then
                                lbllistcoll.Text = "Fee Collection"
                                If dt.Rows(0)("FEE_DESCR") Is Nothing Then
                                    lbllistcolltype.Text = ""
                                Else
                                    lbllistcolltype.Text = dt.Rows(0)("FEE_DESCR")
                                End If
                            ElseIf (dt.Rows(0)("ALD_FEE_REG_MODE")) = "OC" Then
                                lbllistcoll.Text = "Other Collection"

                                If dt.Rows(0)("ACT_DESCR") Is Nothing Then
                                    lbllistcolltype.Text = ""
                                Else
                                    lbllistcolltype.Text = dt.Rows(0)("ACT_DESCR")
                                End If
                            Else
                                lbllistcoll.Text = "Registration Only"
                                lbllistcolltype.Text = ""
                            End If

                            If dt.Rows(0)("ALD_REVENUE_DT") = "#1/1/1900#" Then
                                lblRevnDt.Text = "NA"
                            Else
                                lblRevnDt.Text = Format(dt.Rows(0)("ALD_REVENUE_DT"), "dd/MMM/yyyy")
                            End If
                            lblpay_mode.Text = ""
                            Dim flag As Integer = 0
                            If Not dt.Rows(0)("ALD_PAY_ONLINE") Is Nothing Then
                                If dt.Rows(0)("ALD_PAY_ONLINE") = "1" Then
                                    lblpay_mode.Text = "Payment via online"
                                    flag = 1
                                Else
                                    flag = 0
                                End If
                            End If
                            If Not dt.Rows(0)("ALD_PAY_COUNTER") Is Nothing Then
                                If dt.Rows(0)("ALD_PAY_COUNTER") = "1" Then
                                    If Not (lblpay_mode.Text = "") Then
                                        lblpay_mode.Text = lblpay_mode.Text + " , Payment via school counter"
                                    Else
                                        lblpay_mode.Text = lblpay_mode.Text + "Payment via school counter"
                                    End If
                                    flag = 1
                                Else
                                    flag = 0
                                End If
                            End If
                            If (flag = 0) Then
                                lblpay_mode.Text = "NA"
                            End If
                            If Not dt.Rows(0)("ALD_ACT_FINANCE_LOGDT") = "#1/1/1900#" Then
                                lblFinancLogDt.Text = " Finance department approved on : " + Format(dt.Rows(0)("ALD_ACT_FINANCE_LOGDT"), "dd/MMM/yyyy")
                            Else
                                lblFinancLogDt.Text = ""
                            End If

                            If Not dt.Rows(0)("ALD_ACT_FINANCE") Is Nothing Or Not dt.Rows(0)("ALD_ACT_FINANCE") = "" Then
                                EmployeeName = EOS_MainClass.GetEmployeeNameFromID(dt.Rows(0)("ALD_ACT_FINANCE"))
                            Else
                                EmployeeName = ""
                            End If

                            If Not dt.Rows(0)("ALD_ACT_FINANCE_CMMNTS") Is Nothing Or Not dt.Rows(0)("ALD_ACT_FINANCE_CMMNTS") = "" Then
                                lblfinancecomment.Text = lblfinancecomment.Text + "( " + EmployeeName + " )"
                                txtComments2.Text = dt.Rows(0)("ALD_ACT_FINANCE_CMMNTS")
                            Else
                                txtComments2.Text = ""
                            End If

                            'NEWLY ADDED
                            If (Not dt.Rows(0)("ALD_ALLOW_MULTIPLE") Is Nothing) And (dt.Rows(0)("ALD_ALLOW_MULTIPLE")) = 0 Then
                                rdoAllowMultiple.SelectedValue = "No"
                            Else
                                rdoAllowMultiple.SelectedValue = "Yes"
                            End If
                            rdoAllowMultiple.Enabled = False

                            If (Not dt.Rows(0)("ALD_MAX_COUNT") Is Nothing) Then
                                txtCount.Text = dt.Rows(0)("ALD_MAX_COUNT").ToString
                            End If
                            txtCount.Enabled = False

                            If (Not dt.Rows(0)("ASD_SUBGRP_NAME") Is Nothing) Then
                                lblSubType.Text = dt.Rows(0)("ASD_SUBGRP_NAME")
                            Else
                                lblSubType.Text = ""
                            End If

                            If (Not dt.Rows(0)("ALD_GENDER") Is Nothing) Then
                                rdGender.ClearSelection()
                                If (dt.Rows(0)("ALD_GENDER") = "M") Then
                                    rdGender.SelectedValue = 1
                                ElseIf (dt.Rows(0)("ALD_GENDER") = "F") Then
                                    rdGender.SelectedValue = 2
                                Else
                                    rdGender.SelectedValue = 0
                                End If
                            End If
                            rdGender.Enabled = False

                            If (Not dt.Rows(0)("ALD_IS_SCHEDULE") Is Nothing) Then
                                rdSchdl.ClearSelection()
                                If (dt.Rows(0)("ALD_IS_SCHEDULE")) Then
                                    rdSchdl.SelectedValue = 1
                                Else
                                    rdSchdl.SelectedValue = 0
                                End If
                            Else
                            End If
                            rdSchdl.Enabled = False

                            If (Not dt.Rows(0)("ALD_BNOT_ALLWDAY") Is Nothing) Then
                                If dt.Rows(0)("ALD_BNOT_ALLWDAY") Then
                                    chkNotAllow.SelectedValue = 1
                                Else
                                    chkNotAllow.SelectedValue = 0
                                End If
                            End If
                            chkNotAllow.Enabled = False

                            If (Not dt.Rows(0)("ALD_AUTO_ENROLL") Is Nothing) Then
                                If dt.Rows(0)("ALD_AUTO_ENROLL") Then
                                    chkAutoEnroll.Checked = True
                                Else
                                    chkAutoEnroll.Checked = False
                                End If
                            End If
                            chkAutoEnroll.Enabled = False

                        End If
                    End If
                End If
                If Not ds.Tables(1) Is Nothing Then
                    If (ds.Tables(1).Rows.Count > 0) Then
                        Dim DT2 = ds.Tables(1)
                        ShowSchedules(DT2)
                    End If
                End If
                GetActivityDocumentOnView()
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetApprovalDetlsForPrncpl: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub GetActivityViewForApprvl(ByVal ViewId As Integer)
        Dim ApproverName As String = String.Empty
        Try           
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ACTIVITY_DETAILS_ONVIEW]", Param)
            Dim dt As DataTable = DS.Tables(0)           
            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                If Not dt.Rows(0)("ALD_ACT_PRINPL_LOGDT") = "#1/1/1900#" Then
                    lblApprvrLogDt.Text = "Principal approved on :" + Format(dt.Rows(0)("ALD_ACT_PRINPL_LOGDT"), "dd/MMM/yyyy")
                Else
                    lblApprvrLogDt.Text = ""
                End If
                If Not dt.Rows(0)("ALD_ACT_PRINPL") Is Nothing Or Not dt.Rows(0)("ALD_ACT_PRINPL") = "" Then
                    ApproverName = EOS_MainClass.GetEmployeeNameFromID(dt.Rows(0)("ALD_ACT_PRINPL"))
                Else
                    ApproverName = ""
                End If
                If Not dt.Rows(0)("ALD_ACT_PRINPL_CMMNTS") Is Nothing Or Not dt.Rows(0)("ALD_ACT_PRINPL_CMMNTS") = "" Then
                    lblhoscomment.Text = lblhoscomment.Text + "( " + ApproverName + " )"
                    txtComments3.InnerText = dt.Rows(0)("ALD_ACT_PRINPL_CMMNTS")
                Else
                    txtComments3.InnerText = ""
                End If
                rowPrincipal.Visible = True
                txtComments3.Disabled = True
                txtComments3.Visible = True
                rowbutton.Visible = False
                ShowMessage("", True)
            Else

            End If            
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetActivityViewForApprvl: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub btnReject_Click(sender As Object, e As EventArgs)
        Try
            
            If ViewState("MainMnu_code") = "S103301" Then
                If txtComments3.InnerText Is Nothing Or txtComments3.InnerText = "" Then
                    Message = getErrorMessage("4022")
                    ShowMessage(Message, True)
                Else
                    Insert_Reject_Details()
                End If
            Else

            End If

        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnReject_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Private Sub Insert_Reject_Details()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim Param(3) As SqlParameter          
            If ViewState("MainMnu_code") = "S103301" Then
                Param(0) = Mainclass.CreateSqlParameter("@COMMENTS", txtComments3.InnerText, SqlDbType.VarChar)
                Param(1) = Mainclass.CreateSqlParameter("@LEVEL", "L2", SqlDbType.VarChar)
            Else
            End If
            Param(2) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[REJECT_OASIS_ACTIVITY]", Param)
            strans.Commit()
            ' lblError.Text = "OO.. The Activity Got REJECTED"
            Message = getErrorMessage("4024")
            ShowMessage(Message, True)
            NoButtons()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Insert_Reject_Details: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub btnRevert_Click(sender As Object, e As EventArgs)
        Try
            Revert_Item_To_Initiator()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnRevert_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub Revert_Item_To_Initiator()
        Try
            Dim flag As Boolean = False
            
            If ViewState("MainMnu_code") = "S103301" Then
                If (txtComments3.InnerText Is Nothing Or txtComments3.InnerText = "") Then
                    Message = getErrorMessage("4022")
                    ShowMessage(Message, True)
                    flag = True
                Else
                    flag = False
                End If
            Else
            End If

            If (flag = False) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim strans As SqlTransaction = Nothing
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                Dim Param(3) As SqlParameter               
                If ViewState("MainMnu_code") = "S103301" Then
                    Param(0) = Mainclass.CreateSqlParameter("@COMMENTS", txtComments3.InnerText, SqlDbType.VarChar)
                    Param(1) = Mainclass.CreateSqlParameter("@OPTION", "L3", SqlDbType.VarChar)
                End If
                Param(2) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
                Param(3) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
                SqlHelper.ExecuteNonQuery(strans, "[OASIS].[REVERT_OASIS_ACTIVITY]", Param)
                strans.Commit()
                NoButtons()
                'Revert Message Is : Activity Has Been Successfully Reverted Back To Initiator
                Message = getErrorMessage("4035")
                ShowMessage(Message, False)
            End If
        Catch EX As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Revert_Item_To_Initiator: " + EX.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub lbSName_Click(sender As Object, e As EventArgs)
        Try
            Dim lnkbutton1 As LinkButton = sender
            Dim filename As String = lnkbutton1.Text
            Dim query As String = "[OASIS].[GET_ACTIVITY_UPLOAD_PATH]"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@FILENAME", filename, SqlDbType.VarChar)
            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("ActivityDocumentspath2").ConnectionString
            Dim filepath As String = SqlHelper.ExecuteScalar(str_conn, query, Param)
            filepath = ConFigPath + filepath
            Response.Clear()
            Response.ContentType = "application/pdf"
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & filename)
            Response.BinaryWrite(System.IO.File.ReadAllBytes(filepath))
            Response.End()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From lbSName_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
       
    End Sub

    Protected Sub repdocview_ItemCreated(sender As Object, e As RepeaterItemEventArgs)
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = FindControl("ScriptManager1")
            Dim btn As LinkButton = e.Item.FindControl("lbSName")
            If Not btn Is Nothing Then
                smScriptManager.RegisterPostBackControl(btn)
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From repdocview_ItemCreated: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try        
    End Sub
    Protected Sub txtcount_TextChanged(sender As Object, e As EventArgs)

    End Sub

End Class
