﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeECAAdjustmentApproval.aspx.vb" Inherits="StudentServices_FeeECAAdjustmentApproval" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />


    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        UpdateSum();
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            document.getElementById('<%= h_print.ClientID %>').value = '';

            //result = window.showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
            var url = '<%= request.url %>'
            return ShowWindowWithClose('../Reports/ASPX Report/RptViewerModal.aspx', 'search', '55%', '85%')
            return false;
            //location.href=url.replace(/FeeAdjustment_App/,'FEEAdjustmentREQ_APPView')      
        }
    }
    );

    function UpdateSum() {
        var sum = 0.0;
        var dsum = 0.0;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].name.search(/txtApprAmt/) > 0) {
                dsum = parseFloat(document.forms[0].elements[i].value);
                if (isNaN(dsum))
                    document.forms[0].elements[i].value = 0;
                document.forms[0].elements[i].value = parseFloat(document.forms[0].elements[i].value).toFixed(3);
                sum += parseFloat(document.forms[0].elements[i].value);
            }
        }

        document.getElementById('<%=txtTotalAmount.ClientID %>').value = sum.toFixed(3);
    }
    function CheckAmount(e) {
        var amt;
        amt = parseFloat(e.value)
        if (isNaN(amt))
            amt = 0;
        e.value = amt.toFixed(3);
        UpdateSum()
        return true;
    }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee ECA Adjustments Approval"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" EnableViewState="False" SkinID="LabelError" />--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="MAINERROR" CssClass="error" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="REJECTERROR" CssClass="error" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Approval Date</span> </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAppDate" runat="server"></asp:TextBox>
                                        <asp:Image ID="ImgDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAppDate"
                                            ErrorMessage="Approval Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"><span class="field-label">Requested Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtReqDate" runat="server" AutoPostBack="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Requested DocNo</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblReqDocno" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Requested Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblAcademicYear" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student Details</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblStudNo" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trJoinDet">
                                    <td align="left" width="20%"><span class="field-label">Joined Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblJoinAcademicYear" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Joined Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblJDate" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="Tr1">
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="20%"><span class="field-label">Joined Grade</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblJGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Reason</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAdjReason" runat="server" Enabled="False">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Last Attend Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblLastatDate" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr id="tr_StudName_To" runat="server" visible="false">
                                    <td align="left" width="20%"><span class="field-label">Transferred to</span></td>
                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblStudName_To" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr valign="top">
                                    <td align="left" width="20%"><span class="field-label">Request Remarks</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" colspan="2">
                                        <asp:LinkButton ID="lnkMoreFee" runat="server" OnClientClick="return false;">(View Pending Request(s))</asp:LinkButton></td>
                                </tr>
                                <tr valign="top">
                                    <td align="left" colspan="4">Click
               <asp:LinkButton ID="lnkClickHere" runat="server" OnClientClick="return false;">Here</asp:LinkButton>
                                        to see the Fee paid for the Academic Year
               <asp:Label ID="lblAcdYr" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details For Approval...</td>
                                </tr>
                                <tr id="tr_Deatails" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" Width="100%"
                                            CssClass="table table-bordered table-row" CellPadding="4">
                                            <Columns>
                                                <asp:TemplateField HeaderText="FeeId" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fee Type">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkFeeType" runat="server" OnClientClick="return false;" Text='<%# bind("FEE_TYPE") %>'></asp:LinkButton>
                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" DynamicContextKey='<%# Eval("FEE_ID") %>'
                                                            DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                                            Position="Left" TargetControlID="lnkFeeType">
                                                        </ajaxToolkit:PopupControlExtender>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" Width="135px"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FEE_DESCR_TO" HeaderText="Adj. To" />
                                                <asp:TemplateField HeaderText="Charged Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# bind("CHARGEDAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Paid Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# bind("PAIDAMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Adjustment Period">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# bind("FEE_AMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Amount">
                                                    <ItemTemplate>
                                                        <asp:TextBox Style="text-align: right" ID="txtApprAmt" runat="server" Text='<%# Bind("FEE_APPR_AMOUNT") %>' AutoPostBack="true" onblur="return CheckAmount(this);" ValidationGroup="MAINERROR" Width="110px" OnTextChanged="txtApprAmt_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="MAINERROR" ErrorMessage="Approved Amount Required" ControlToValidate="txtApprAmt">*</asp:RequiredFieldValidator>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tax Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaxCode" runat="server" Text='<%# Bind("FAD_TAX_CODE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tax Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTaxAmount" runat="server" Text='<%# Bind("FAD_TAX_AMOUNT", "{0:F}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Net Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNetAmount" runat="server" Text='<%# Bind("FAD_NET_AMOUNT", "{0:F}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="3"><span class="field-label">Total</span></td>
                                    <td align="right">
                                        <asp:TextBox ID="txtTotalAmount" runat="server" Style="text-align: right" onfocus="this.blur();"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Approval Remarks</span></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtApprRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Approval Remarks required"
                                            ValidationGroup="MAINERROR" ControlToValidate="txtApprRemarks">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtApprRemarks"
                                            ErrorMessage="Rejection Remarks required" ValidationGroup="REJECTERROR">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:CheckBox ID="ChkPrint" runat="server" Text="Print After Approve" Checked="true" CssClass="field-label" />
                                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" ValidationGroup="MAINERROR" />
                                        <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" OnClientClick="return confirm('Are you sure');" ValidationGroup="REJECTERROR" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server" CssClass="panel-cover">
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover">
                </asp:Panel>
                <asp:Panel ID="pnlPendigPayments" runat="server">
                    <asp:GridView ID="gvPendingHistory" runat="server" SkinID="GridViewNormal">
                    </asp:GridView>
                </asp:Panel>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtAppDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtAppDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:PopupControlExtender ID="PUPCECURACDYR" runat="server" DynamicControlID="Panel2"
                    DynamicServiceMethod="GetDynamicContentCurrentAcademicYear" PopupControlID="Panel2"
                    Position="Left" TargetControlID="lnkClickHere">
                </ajaxToolkit:PopupControlExtender>
                <ajaxToolkit:PopupControlExtender ID="ppeHistory" runat="server" DynamicControlID="Panel2"
                    DynamicServiceMethod="GetDynamicContentCurrentAcademicYear" PopupControlID="pnlPendigPayments"
                    Position="Bottom" TargetControlID="lnkMoreFee">
                </ajaxToolkit:PopupControlExtender>

                <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>


    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>
</asp:Content>

