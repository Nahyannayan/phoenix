﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services_ActivitySettings.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Accounts_Services_Activity" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Services - Activity  Settings
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div align="left">
                    <asp:Label ID="lblError" runat="server" CssClass="error"> </asp:Label>
                </div>
                <br />

                <ajaxToolkit:TabContainer runat="server" ID="tab1">
                    <ajaxToolkit:TabPanel runat="server" ID="panel1" HeaderText="Add/Edit Activity Type">
                        <ContentTemplate>
                            <asp:GridView ID="grdact_types" runat="server" OnRowDeleting="grdact_types_RowDeleting" AutoGenerateColumns="false" EmptyDataText="Sorry No Activity Types To Display" CssClass="table table-bordered table-row" OnRowDataBound="grdact_types_RowDataBound"
                                OnRowCommand="grdact_types_RowCommand" EnableViewState="true" OnRowEditing ="grdact_types_RowEditing">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActID" Text='<%# Bind("ID")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACTIVITY TYPE">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtActName" Text='<%# Bind("NAME")%>' runat="server" Enabled="false"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TYPE DESCRIPTION">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtActDescr" Text='<%# Bind("DESCR")%>' runat="server" Enabled="false"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PARENTPORTAL ICON PATH WRT ACTIVITY TYPE">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtActIconPath" Text='<%# Bind("PATH")%>' runat="server" Enabled="false"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField Text="Edit" runat="server" CommandName="Edit" />
                                    <asp:ButtonField Text="Delete" runat="server" CommandName="Delete" />
                                    <%--<asp:TemplateField>
                                        <ItemTemplate>
                                            <tr style="background-color:whitesmoke !important;" >
                                                <td colspan="7" align="right" >
                                                    <asp:GridView ID="gvOrders" OnRowEditing ="gvOrders_RowEditing" OnRowDeleting="gvOrders_RowDeleting" runat="server" OnRowDataBound ="gvOrders_RowDataBound" AutoGenerateColumns="false" CssClass="table table-bordered table-row" OnRowCommand="gvOrders_RowCommand" >
                                                        <Columns>
                                                             <asp:TemplateField HeaderText="ACTIVITY SUB TYPE" ItemStyle-Width ="30%"  Visible="false" >
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrdatmid" Text='<%# Bind("ATM_ID")%>' runat="server" Visible="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ACTIVITY SUB TYPE" ItemStyle-Width ="30%" >
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrdactname" Text='<%# Bind("ATM_NAME")%>' runat="server" Enabled="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="DESCRIPTION" ItemStyle-Width ="30%">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtgrdactdescr" Text='<%# Bind("ATM_DESCR")%>' runat="server" Enabled="false"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SUB GROUP(Fee Head)" ItemStyle-Width ="30%">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hidfeecode" runat="server" Value='<%# Bind("ATM_FEE_CODE")%>' />
                                                                    <asp:DropDownList ID="ddlgrdfeetype" runat="server"  ></asp:DropDownList> 
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="COUNTRY" ItemStyle-Width ="30%">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hidCountrycode" runat="server" Value='<%# Bind("ATM_COUNTRY_CODE")%>' />
                                                                    <asp:DropDownList ID="ddlgrdCountry" runat="server"  ></asp:DropDownList> 
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField Text="Edit" runat="server" CommandName="Edit" ItemStyle-Width="5%"/>
                                                            <asp:ButtonField Text="Delete" runat="server" CommandName="Delete" ItemStyle-Width="5%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>                                
                            </asp:GridView>
                            <div class="accordion" id="accordionExample2">
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                                                Add New Activity Type
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample2">
                                        <div class="card-body">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <span class="field-label">Activity Type Name</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAct_Name" Text="" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="field-label">Activity Type Description </span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAct_Descr" Text="" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <span class="field-label">Activity  Icon Path for Parent portal</span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAct_Path" Text="" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSave" Text="Add" runat="server" CssClass="button" OnClick="btnSave_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion" id="accordionExample3" style="display:none !important;">
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Add New Sub Activity Type
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample3">
                                        <div class="card-body">
                                            <table>
                                                <tr>
                                                    <td width="20%">
                                                        <span class="field-label">Sub Type Name</span>
                                                    </td>
                                                    <td width="30%">
                                                       <asp:TextBox ID="txtSubName" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td width="20%">
                                                        <span class="field-label">Sub Type Description </span>
                                                    </td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txtSubDecsr" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <span class="field-label">Associated Activity Type</span>
                                                    </td>
                                                    <td width="30%">
                                                        <asp:DropDownList ID="ddlActiTypes" runat="server" ></asp:DropDownList> 
                                                    </td>
                                                    <td width="20%">
                                                        <span class="field-label">
                                                            Associated Fee Code
                                                        </span>
                                                    </td>
                                                    <td width="30%">
                                                        <asp:DropDownList ID="ddlFeeCode" runat="server"></asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <span class="field-label">Associated Country</span>
                                                    </td>
                                                    <td width="30%">
                                                        <asp:DropDownList ID="ddlCountry" runat="server" ></asp:DropDownList> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <asp:Button ID="btnSaveSub" runat="server" OnClick="btnSaveSub_Click" CssClass="button" Text ="Add" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>


                </ajaxToolkit:TabContainer>

            </div>
        </div>
    </div>
</asp:Content>
