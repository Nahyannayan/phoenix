﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FeeECA_Gen_CollectionView.aspx.vb" Inherits="StudentServices_FeeECA_Gen_CollectionView" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--   <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>--%>
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
      function CheckForPrint() {
          if (document.getElementById('<%= h_print.ClientID %>').value != '') {
              <%--showModelessDialog('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");--%>
              if (isIE()) {
                  window.showModalDialog('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                  document.getElementById('<%= h_print.ClientID %>').value = '';
              } else {
                  Popup('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
                  document.getElementById('<%= h_print.ClientID %>').value = '';
              }
          }
      }
  );
      function CheckForPrint() {
          if (document.getElementById('<%= h_print.ClientID %>').value != '') {
              <%--showModelessDialog('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");--%>
              //Popup('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value);         
              if (isIE()) {
                  window.showModalDialog('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                  document.getElementById('<%= h_print.ClientID %>').value = '';
              } else {
                  Popup('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
                  document.getElementById('<%= h_print.ClientID %>').value = '';
              }

          }
      }

      function isIE() {
          ua = navigator.userAgent;
          /* MSIE used to detect old browsers and Trident used to newer ones*/
          var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
          //alert(is_ie);
          return is_ie;
      }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="top">
                        <td valign="top" align="left" colspan="2">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr valign="top">
                        <td align="left" valign="middle" class="matters" width="20%"><span class="field-label">Select Business Unit</span></td>
                        <td align="left" class="matters" valign="middle" width="30%">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" TabIndex="5" SkinID="DropDownListNormal" onchange="if(this.selectedIndex == 0)return false;">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlTopFilter" runat="server" SkinID="DropDownListNormal" AutoPostBack="True" Visible="false">
                                <asp:ListItem Value="10">Top 10</asp:ListItem>
                                <asp:ListItem Value="100">Top 100</asp:ListItem>
                                <asp:ListItem>All</asp:ListItem>
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="15" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Docno">
                                        <HeaderTemplate>
                                            Receipt#<br />
                                            <asp:TextBox ID="txtReceipt" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchReceipt" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchReceipt_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date<br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchDate" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchDate_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FCL_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student No<br />
                                            <asp:TextBox ID="txtSTU_NO" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSTU_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchSTU_NO_Click"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtSNAME" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchSNAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchSNAME_Click"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderTemplate>
                                            Amount
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Description
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("FCL_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print Receipt">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbPrint" OnClick="lbPrint_Click" runat="server">Print</asp:LinkButton>
                                            <br />
                                            <asp:LinkButton ID="lblViewReceipt" runat="server" OnClick="lblViewReceipt_Click">ViewReceipt</asp:LinkButton>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_Export" runat="server" />
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
