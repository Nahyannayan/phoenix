﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Imports Telerik.Web.UI

Partial Class StudentServices_Services_EnrollActivity_ForAdmin
    Inherits System.Web.UI.Page
    Dim Message As String = Nothing
    Dim Count As Integer = 0
    Dim smScriptManager As New ScriptManager
    Dim Encr_decrData As New Encryption64
    Dim Latest_refid As Integer = 0
    Public Property DataResult() As DataTable
        Get
            Return Session("DataActivityResult")
        End Get
        Set(ByVal value As DataTable)
            Session("DataActivityResult") = value
        End Set
    End Property

    Private Property DiscountBreakUpXML() As String
        Get
            Return ViewState("DiscountBreakUpXML")
        End Get
        Set(ByVal value As String)
            ViewState("DiscountBreakUpXML") = value
        End Set
    End Property
    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value

        End Set
    End Property
    Private Property Max_Limit_ForRptr() As Integer
        Get
            Return ViewState("Max_Limit_ForRptr")
        End Get
        Set(ByVal value As Integer)
            ViewState("Max_Limit_ForRptr") = value

        End Set
    End Property
    Private Property B_DayNotallow() As Integer
        Get
            Return ViewState("B_DayNotallow")
        End Get
        Set(ByVal value As Integer)
            ViewState("B_DayNotallow") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        smScriptManager = Master.FindControl("ScriptManager1")
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    BindBusinessunits()
                    InitializePage()
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
    End Sub
    Sub InitializePage()
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
    End Sub
    Private Sub BindBusinessunits()
        ddlBusinessunit.Items.Clear()
        ddlBusinessunit.DataSource = clsFeeCollection.GetBusinessUnitsForCollection()
        ddlBusinessunit.DataBind()
        If ddlBusinessunit.Items.Count > 0 Then
            ddlBusinessunit.Items.FindItemByValue(Session("sBsuId")).Selected = True
        End If
    End Sub
    Protected Sub uscStudentPicker_StudentCleared(sender As Object, e As EventArgs) Handles uscStudentPicker.StudentCleared
        rptrGroup.DataSource = Nothing
        rptrGroup.DataBind()
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
    End Sub
    Protected Sub uscStudentPicker_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentNoChanged
        Try
            Dim Student_id As Long = uscStudentPicker.STU_ID
            B_DayNotallow = 0
            Max_Limit_ForRptr = 0
            bPaymentConfirmMsgShown = False
            LoadRepeaterGroup(Student_id)
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Load: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub LoadRepeaterGroup(ByVal Student_id As Long)
        DataResult = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As System.Data.DataSet
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@TYPE", "G")
        param(1) = New SqlClient.SqlParameter("@STUID", Student_id)
        param(2) = New SqlClient.SqlParameter("@GROUPID", 0)

        ds = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", param)
        If ds.Tables(0).Rows.Count = 0 Then
            'lblError.Text = "Sorry! No Activities To Display"
            Message = UtilityObj.getErrorMessage("4007")
            ShowMessage(Message, True)
        Else
            'Dim dt As DataTable = ds.Tables(0)
            ' Session("datatable_Activity") = dt
            rptrGroup.DataSource = ds.Tables(0)
            rptrGroup.DataBind()
        End If
    End Sub
    Protected Sub rptrGroup_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.DataItem Is Nothing Then
                Return
            Else
                Max_Limit_ForRptr = 0
                Dim hidAPG_ID As HiddenField = DirectCast(e.Item.FindControl("hidAPG_ID"), HiddenField)
                Dim Repeater1 As Repeater = DirectCast(e.Item.FindControl("Repeater1"), Repeater)
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim lbllimitFulltxt As Label = DirectCast(e.Item.FindControl("lbllimitFulltxt"), Label)
                Dim hidMaxlimit As HiddenField = DirectCast(e.Item.FindControl("hidMaxlimit"), HiddenField)
                Dim lblMaximumLimit As Label = DirectCast(e.Item.FindControl("lblMaximumLimit"), Label)
                Dim divRepeater As HtmlGenericControl = DirectCast(e.Item.FindControl("divRepeater"), HtmlGenericControl)



                If Not hidMaxlimit Is Nothing And hidMaxlimit.Value > 0 Then
                    lblMaximumLimit.Visible = True
                    lblMaximumLimit.Text = "( Maximum allowed :  " + hidMaxlimit.Value + " )"
                    'If hidMaxlimit.Value = 1 Then
                    'Else
                    '    lblMaximumLimit.Visible = True
                    '    lblMaximumLimit.Text = "This is a group activity. Maximum number of activity can be chosen per this group is:  " + hidMaxlimit.Value
                    'End If                   
                End If

                If hidAPG_ID.Value = 0 Then
                    divRepeater.Style.Add("Background-color", "white")
                    divRepeater.Style.Add("border", "none")
                End If
                'get max count of group 
                Dim Params(2) As SqlClient.SqlParameter
                Params(0) = New SqlClient.SqlParameter("@TYPE", "M")
                Params(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
                Params(2) = New SqlClient.SqlParameter("@GROUPID", hidAPG_ID.Value)
                Dim max_limit As Integer = SqlHelper.ExecuteScalar(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", Params)
                Max_Limit_ForRptr = max_limit
                If max_limit = 0 Then
                    lbllimitFulltxt.Text = "Activity Selection Limit Reached"
                Else
                    lbllimitFulltxt.Text = "&nbsp;"
                End If
                Dim ds_A As System.Data.DataSet
                Dim param(2) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@TYPE", "A")
                param(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
                param(2) = New SqlClient.SqlParameter("@GROUPID", hidAPG_ID.Value)
                ds_A = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", param)

                Dim Dt_Result As DataTable = ds_A.Tables(0)
                '   Dim d_Col As DataColumn = New DataColumn              
                If DataResult Is Nothing Then
                    DataResult = Dt_Result
                Else
                    DataResult.Merge(Dt_Result)
                End If

                Repeater1.DataSource = ds_A
                Repeater1.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Sub Repeater1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.DataItem Is Nothing Then
                Return

            Else
                B_DayNotallow = 0


                Dim count As Integer = 0
                Dim ViewID As Integer = Convert.ToInt32(DirectCast(e.Item.FindControl("lblViewId"), Label).Text.ToString())
                'Get specific row from datatable
                Dim Dt_table As DataTable = DataResult
                Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)


                'controls loading
                Dim lblamt As Label = DirectCast(e.Item.FindControl("lblamt"), Label)
                Dim lblApprvl As Label = DirectCast(e.Item.FindControl("lblApprvl"), Label)
                Dim lnkbtnapply As LinkButton = DirectCast(e.Item.FindControl("lnkbtnapply"), LinkButton)
                Dim lnkbtnPay As LinkButton = DirectCast(e.Item.FindControl("lnkbtnPay"), LinkButton)
                Dim lnkbtnAdjst As LinkButton = DirectCast(e.Item.FindControl("lnkbtnAdjst"), LinkButton)
                Dim lnkbtnUnReg As LinkButton = DirectCast(e.Item.FindControl("lnkbtnUnReg"), LinkButton)
                Dim lnkbtnRefund As LinkButton = DirectCast(e.Item.FindControl("lnkbtnRefund"), LinkButton)
                Dim lnkbtnCnclRecpt As LinkButton = DirectCast(e.Item.FindControl("lnkbtnCnclRecpt"), LinkButton)
                Dim divcount As HtmlGenericControl = DirectCast(e.Item.FindControl("divcount"), HtmlGenericControl)
                Dim ddlcount As DropDownList = DirectCast(e.Item.FindControl("ddlcount"), DropDownList)
                Dim lnkTermCond As LinkButton = CType(e.Item.FindControl("lnkTermCond"), LinkButton)
                Dim divgrad As HtmlGenericControl = DirectCast(e.Item.FindControl("divgrad"), HtmlGenericControl)
                Dim chkTerm As CheckBox = DirectCast(e.Item.FindControl("chkTerm"), CheckBox)
                Dim divTermMessage As HtmlGenericControl = DirectCast(e.Item.FindControl("divTermMessage"), HtmlGenericControl)
                Dim labelStatus As Label = DirectCast(e.Item.FindControl("lblStatus"), Label)

                Dim lnkbtnReverseAndUnReg As LinkButton = DirectCast(e.Item.FindControl("lnkbtnReverseAndUnReg"), LinkButton) 'Added by vikranth on 21st Jan 2020

                ' Dim hidAPG_ID As HiddenField = DirectCast(sender.parent.FindControl("hidAPG_ID"), HiddenField)
                ' Dim label As String = DirectCast(e.Item.FindControl("lblViewId"), Label).Text               
                ' Dim hidStatus As HiddenField = DirectCast(e.Item.FindControl("hidStatus"), HiddenField)
                ' Dim hidPayOnlineFlag As HiddenField = DirectCast(e.Item.FindControl("hidALD_PAY_ONLINE"), HiddenField)
                ' Dim hidCollMode As HiddenField = DirectCast(e.Item.FindControl("hidCollMode"), HiddenField)
                ' Dim hidAmnt As HiddenField = DirectCast(e.Item.FindControl("hidAmnt"), HiddenField)

                ' Dim hidAvaiableSeat As HiddenField = DirectCast(e.Item.FindControl("hidAvaiableSeat"), HiddenField)
                ' Dim hidpaymentStatus As HiddenField = DirectCast(e.Item.FindControl("hidpaymentStatus"), HiddenField)                
                ' Dim hidallowmultiple As HiddenField = CType(e.Item.FindControl("hidallowmultiple"), HiddenField)
                '  Dim hidAllowMultipleActDay As HiddenField = DirectCast(e.Item.FindControl("hidAllowMultipleActDay"), HiddenField)
                '  Dim hdn_count As HiddenField = DirectCast(e.Item.FindControl("hdn_count"), HiddenField)

                ' Dim hidMulMaxCount As HiddenField = DirectCast(e.Item.FindControl("hidMulMaxCount"), HiddenField)
                ' Dim hidtotalamount As HiddenField = CType(e.Item.FindControl("hidtotalamount"), HiddenField)
                ' Dim hidcount As HiddenField = CType(e.Item.FindControl("hidcount"), HiddenField)
                ' Dim hidschedule As HiddenField = CType(e.Item.FindControl("hidschedule"), HiddenField)
                ' Dim hidAPD_ID As HiddenField = CType(e.Item.FindControl("hidAPD_ID"), HiddenField)



                If dRow(0)("ALD_ALLOW_MULTIPLE") Then
                    divcount.Visible = True
                    ddlcount.Items.Clear()
                    Dim mulmaxcount As Integer = 0
                    If (Not dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT") Is Nothing) And (Not dRow(0)("ALD_AVAILABLE_SEAT") Is Nothing) Then
                        If (Convert.ToInt32(dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT")) > Convert.ToInt32(dRow(0)("ALD_AVAILABLE_SEAT"))) Then
                            mulmaxcount = Convert.ToInt32(dRow(0)("ALD_AVAILABLE_SEAT"))
                        Else
                            mulmaxcount = Convert.ToInt32(dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT"))
                        End If
                    End If
                    If mulmaxcount > 0 Then
                        For i As Integer = 1 To mulmaxcount
                            ddlcount.Items.Add(New ListItem(i.ToString(), i.ToString()))
                        Next i
                    Else
                        ddlcount.Items.Add(New ListItem(0, 0))
                    End If

                Else
                    divcount.Visible = False
                End If
                'lnkbtnapply.Attributes.Add("OnClick", "return ShowWindowWithClose('/Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%');")
                count = CheckEnrolledStatus(ViewID)
                'SHOW IF SCHEDULES ARE THERE 
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim divSchdl As HtmlGenericControl = DirectCast(e.Item.FindControl("divSchdl"), HtmlGenericControl)
                Dim rdoSchdlDetails As RadioButtonList = DirectCast(e.Item.FindControl("rdoSchdlDetails"), RadioButtonList)
                Dim ribbonNotPaid As HtmlGenericControl = DirectCast(e.Item.FindControl("ribbonNotPaid"), HtmlGenericControl)
                If divSchdl.Visible = True Then
                    Dim paramS(2) As SqlClient.SqlParameter
                    paramS(0) = New SqlClient.SqlParameter("@TYPE", "S")
                    paramS(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
                    paramS(2) = New SqlClient.SqlParameter("@GROUPID", ViewID)
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", paramS)
                    rdoSchdlDetails.DataSource = ds.Tables(0)
                    rdoSchdlDetails.DataValueField = "ASH_ID"
                    rdoSchdlDetails.DataTextField = "ASH_WEEKS"
                    rdoSchdlDetails.DataBind()
                    rdoSchdlDetails.SelectedIndex = 0

                    For Each item As ListItem In rdoSchdlDetails.Items
                        If item.Text.Contains("Available Seat: 0") Then
                            item.Enabled = False
                        End If
                    Next

                End If
                If divSchdl.Visible = True Then
                    If (rdoSchdlDetails.Items.Count > 0) Then
                        B_DayNotallow = valiatesameday(ViewID, rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0), rdoSchdlDetails.SelectedItem.Value)
                    End If
                End If

                If (count > 0 And (dRow(0)("APD_PAYMENT_STATUS") = "Pending" Or dRow(0)("APD_PAYMENT_STATUS") = "Receipt Cancelled" Or dRow(0)("APD_PAYMENT_STATUS") = "Refund Processed")) Then
                    If divSchdl.Visible = True Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            If Not rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")) Is Nothing Then
                                rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Selected = True
                                rdoSchdlDetails.Enabled = False
                            End If
                        End If
                    End If

                    If (dRow(0)("ALD_ALLOW_MULTIPLE")) Then
                        lblamt.Text = dRow(0)("APD_TOTAL_AMOUNT") & " " & Session("BSU_CURRENCY")
                        ddlcount.SelectedValue = dRow(0)("APD_COUNT")
                        ddlcount.Enabled = False
                    End If

                    If (dRow(0)("ALD_AVAILABLE_SEAT") <= 0) Then
                        lnkbtnapply.Visible = False
                        labelStatus.Text = "Full Booked"
                        ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    Else
                        'dRow(0)("ALD_AUTO_ENROLL")
                        ' lnkbtnapply.Text = "View"
                        lnkbtnapply.Visible = False
                        If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "S10899" Or ViewState("MainMnu_code") = "F108916")) Then
                            If dRow(0)("ALD_AUTO_ENROLL") Then
                                lnkbtnUnReg.Visible = False
                            Else
                                lnkbtnUnReg.Visible = True
                            End If
                        End If
                        If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "F108917" Or ViewState("MainMnu_code") = "F108916")) Then
                            If (lblApprvl.Text = "No") Then
                                If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing And Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) And (dRow(0)("ALD_PAY_COUNTER")) Then
                                    lnkbtnPay.Visible = True
                                    lnkbtnAdjst.Visible = True
                                    lnkbtnPay.Attributes.Add("OnClick", "return ShowWindowWithClose('ActivityPayDetails.aspx?ALD_ID=" & Encr_decrData.Encrypt(ViewID) & "&COLMODE=" & Encr_decrData.Encrypt(dRow(0)("ALD_FEE_REG_MODE")) & "&AMT=" & Encr_decrData.Encrypt(dRow(0)("APD_TOTAL_AMOUNT")) & "&TYPE=" & Encr_decrData.Encrypt(uscStudentPicker.STU_TYPE) & "&APD_ID=" & Encr_decrData.Encrypt(dRow(0)("APD_ID")) & "&STU_ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "&BSU_ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_BSU_ID) & "', 'Activity Details', '90%', '85%', '" & lnkbtnPay.Text & "');")
                                Else
                                    lnkbtnPay.Visible = False
                                    lnkbtnAdjst.Visible = False
                                End If
                                labelStatus.Text = "Pending"
                                ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                            Else
                                If Not dRow(0)("APD_STATUS") Is Nothing And dRow(0)("APD_STATUS") = "A" Then
                                    If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing And Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) And (dRow(0)("ALD_PAY_COUNTER")) Then
                                        lnkbtnPay.Visible = True
                                        lnkbtnAdjst.Visible = True
                                        lnkbtnPay.Attributes.Add("OnClick", "return ShowWindowWithClose('ActivityPayDetails.aspx?ALD_ID=" & Encr_decrData.Encrypt(ViewID) & "&COLMODE=" & Encr_decrData.Encrypt(dRow(0)("ALD_FEE_REG_MODE")) & "&AMT=" & Encr_decrData.Encrypt(dRow(0)("APD_TOTAL_AMOUNT")) & "&TYPE=" & Encr_decrData.Encrypt(uscStudentPicker.STU_TYPE) & "&APD_ID=" & Encr_decrData.Encrypt(dRow(0)("APD_ID")) & "&STU_ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "&BSU_ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_BSU_ID) & "', 'Activity Details', '90%', '85%', '" & lnkbtnPay.Text & "');")
                                    Else
                                        lnkbtnPay.Visible = False
                                        lnkbtnAdjst.Visible = False
                                    End If
                                    labelStatus.Text = "Pending"
                                    ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                                Else
                                    lnkbtnPay.Visible = False
                                    lnkbtnAdjst.Visible = False
                                    labelStatus.Text = "Need Approval"
                                    ribbonNotPaid.Attributes.Add("class", "ribbonNotPaid")
                                End If
                            End If
                        ElseIf (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "S10899")) Then
                            lnkbtnPay.Visible = False
                            lnkbtnAdjst.Visible = False
                            labelStatus.Text = "Pending"
                            ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                        End If
                        Dim btnname As String = lnkbtnapply.Text
                        ' lnkbtnapply.Attributes.Add("OnClick", "return ShowWindowWithClose('../Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%', '" & btnname & "');")

                    End If
                ElseIf (dRow(0)("ALD_AUTO_ENROLL") And count = 0) Then
                    If (dRow(0)("ALD_AVAILABLE_SEAT") <= 0) Then
                        lnkbtnapply.Visible = False
                        lnkbtnUnReg.Visible = False
                        ' labelName.Text = labelName.Text
                        labelStatus.Text = "Full Booked"
                        ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    Else
                        lnkbtnapply_Click(lnkbtnapply, Nothing)
                        Dim Dt_table2 As DataTable = DataResult
                        dRow = Dt_table2.Select("ALD_ID = " & ViewID)

                        lnkbtnapply.Visible = False
                        If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "S10899" Or ViewState("MainMnu_code") = "F108916")) Then
                            lnkbtnUnReg.Visible = False
                        End If
                        If (lblApprvl.Text = "No") Then
                            If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "F108917" Or ViewState("MainMnu_code") = "F108916")) Then
                                If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing And Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) And (dRow(0)("ALD_PAY_COUNTER")) Then
                                    lnkbtnPay.Visible = True
                                    lnkbtnAdjst.Visible = True
                                    lnkbtnPay.Attributes.Add("OnClick", "return ShowWindowWithClose('ActivityPayDetails.aspx?ALD_ID=" & Encr_decrData.Encrypt(ViewID) & "&COLMODE=" & Encr_decrData.Encrypt(dRow(0)("ALD_FEE_REG_MODE")) & "&AMT=" & Encr_decrData.Encrypt(dRow(0)("APD_TOTAL_AMOUNT")) & "&TYPE=" & Encr_decrData.Encrypt(uscStudentPicker.STU_TYPE) & "&APD_ID=" & Encr_decrData.Encrypt(dRow(0)("APD_ID")) & "&STU_ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_ID) & "&BSU_ID=" & Encr_decrData.Encrypt(uscStudentPicker.STU_BSU_ID) & "', 'Activity Details', '90%', '85%', 'AutoEnroll');")
                                Else
                                    lnkbtnPay.Visible = False
                                    lnkbtnAdjst.Visible = False
                                End If
                            ElseIf (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "S10899")) Then
                                lnkbtnPay.Visible = False
                                lnkbtnAdjst.Visible = False
                            End If
                            ' labelName.Text = labelName.Text
                            labelStatus.Text = "Pending"
                            ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                        End If
                        Dim btnname As String = lnkbtnapply.Text
                    End If
                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Completed") Then
                    lnkbtnapply.Visible = False
                    lnkbtnPay.Visible = False
                    lnkbtnAdjst.Visible = False
                    If divSchdl.Visible = True Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            If Not rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")) Is Nothing Then
                                rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Selected = True
                                rdoSchdlDetails.Enabled = False
                            End If
                        End If
                    End If

                    If (dRow(0)("ALD_ALLOW_MULTIPLE")) Then
                        lblamt.Text = dRow(0)("APD_TOTAL_AMOUNT") & " " & Session("BSU_CURRENCY")
                        ddlcount.SelectedValue = dRow(0)("APD_COUNT")
                        ddlcount.Enabled = False
                    End If

                    labelStatus.Text = "Enrolled"
                    'Added by vikranth on 12th Jan 2020
                    'lnkbtnRefund.Visible = True
                    If GetDoubleVal(dRow(0)("APD_TOTAL_AMOUNT")) = 0 Then
                        lnkbtnUnReg.Visible = True
                        lnkbtnRefund.Visible = False
                        lnkbtnReverseAndUnReg.Visible = False 'added by vikranth on 21st Jan 2020
                    Else
                        lnkbtnUnReg.Visible = False
                        If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing AndAlso dRow(0)("ALD_FEE_REG_MODE") = "FC" Then 'added by vikranth on 21st Jan 2020
                            lnkbtnRefund.Visible = True
                            lnkbtnReverseAndUnReg.Visible = True 'added by vikranth on 21st Jan 2020
                        Else lnkbtnRefund.Visible = False 'added by vikranth on 21st Jan 2020
                            lnkbtnReverseAndUnReg.Visible = False 'added by vikranth on 21st Jan 2020
                        End If

                    End If
                    'End by vikranth

                    If Not dRow(0)("PAYMENT_DATE") Is Nothing AndAlso dRow(0)("PAYMENT_DATE").ToString <> "" Then
                        If Today.Date = CDate(dRow(0)("PAYMENT_DATE")) Then
                            lnkbtnCnclRecpt.Visible = True
                        Else
                            lnkbtnCnclRecpt.Visible = False
                        End If
                    End If

                    ribbonNotPaid.Attributes.Add("class", "ribbonActive")
                    '  lblDone.Style.Item("display") = "block"       
                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Refund Processing") Then 'Refund Processed
                    lnkbtnapply.Visible = False
                    lnkbtnPay.Visible = False
                    lnkbtnAdjst.Visible = False
                    labelStatus.Text = "Refund raised"
                    ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    'ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Refund Processed") Then
                    '    lnkbtnapply.Visible = False
                    '    lnkbtnPay.Visible = False
                    '    lnkbtnAdjst.Visible = False
                    '    labelStatus.Text = "Refunded"
                    '    ribbonNotPaid.Attributes.Add("class", "ribbonActive")
                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Receipt Cancellation") Then
                    lnkbtnapply.Visible = False
                    lnkbtnPay.Visible = False
                    lnkbtnAdjst.Visible = False
                    labelStatus.Text = "Cancel Receipt"
                    ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    'ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Receipt Cancelled") Then
                    '    lnkbtnapply.Visible = False
                    '    lnkbtnPay.Visible = False
                    '    lnkbtnAdjst.Visible = False
                    '    labelStatus.Text = "Receipt Cancelled"
                    '    ribbonNotPaid.Attributes.Add("class", "ribbonActive")
                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "INITIATED") Then
                    If dRow(0)("ALD_IS_SCHEDULE") Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            If Not rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")) Is Nothing Then
                                rdoSchdlDetails.SelectedValue = rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Value
                                rdoSchdlDetails.Enabled = False
                            End If
                        End If
                    End If
                    lnkbtnAdjst.Visible = False
                    Dim TIMENOW As DateTime = DateTime.Now
                    Dim PAY_TIME As DateTime = Convert.ToDateTime(dRow(0)("APD_PAY_TIME"))
                    Dim TIMEDIFF = (TIMENOW - PAY_TIME).TotalMinutes
                    If (TIMEDIFF < 15) Then
                        lnkbtnPay.Text = "Processing.."
                        labelStatus.Text = "Processing.."
                        ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                        lnkbtnapply.Attributes.Add("OnClick", "AlertInitError(); return false;")
                        lnkbtnapply.Text = "Processing.."
                        '  lnkbtnapply.Visible = False
                    Else
                        dRow(0)("APD_PAYMENT_STATUS") = "Pending"
                        Dim objConn As New SqlConnection(str_conn)
                        objConn.Open()
                        Dim stTrans As SqlTransaction = objConn.BeginTransaction
                        Try
                            Dim Param(1) As SqlParameter
                            ' Param(0) = New SqlClient.SqlParameter("@APD_ID", hidAPD_ID.Value)
                            Param(0) = New SqlClient.SqlParameter("@APD_ID", dRow(0)("APD_ID"))
                            Param(1) = New SqlClient.SqlParameter("@TYPE", "U")
                            SqlHelper.ExecuteNonQuery(str_conn, "[OASIS].[ACTIVITY_PAYMENT_INITIATION]", Param)
                            stTrans.Commit()
                        Catch ex As Exception
                            stTrans.Rollback()
                        Finally
                            If objConn.State = ConnectionState.Open Then
                                objConn.Close()
                            End If
                        End Try
                        LoadRepeaterGroup(uscStudentPicker.STU_ID)
                    End If
                    ' 'NEW CHANGES FOR PAYMENT FIXES - END 
                ElseIf dRow(0)("ALD_AVAILABLE_SEAT") <= 0 Then

                    lnkbtnapply.Visible = False
                    labelStatus.Text = "Full Booked"
                    ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                Else

                    If dRow(0)("ALD_ISTERMS") Then
                        lnkTermCond.Visible = True
                        chkTerm.Visible = True
                    End If

                    If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "S10899" Or ViewState("MainMnu_code") = "F108916")) Then
                        lnkbtnapply.Text = "Enroll"
                        Dim btnname As String = lnkbtnapply.Text
                        If (Max_Limit_ForRptr = 0) Then
                            If dRow(0)("ALD_ISTERMS") Then
                                lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
                                lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
                                chkTerm.Enabled = False
                            End If
                            lnkbtnapply.Attributes.Add("OnClick", "AlertLimitReached(); return false;")
                            lnkbtnapply.Enabled = False
                        ElseIf (B_DayNotallow = 1) Then
                            If dRow(0)("ALD_ISTERMS") Then
                                lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
                                lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
                                chkTerm.Enabled = False
                            End If
                            lnkbtnapply.Attributes.Add("OnClick", "AlertMultiple(); return false;")
                            lnkbtnapply.Enabled = False
                        Else
                            If dRow(0)("ALD_ISTERMS") Then
                                lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
                                lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
                                If chkTerm.Checked Then
                                    lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
                                Else
                                    lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
                                End If
                            Else
                                lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
                            End If

                            ' lnkbtnapply.Attributes.Add("OnClick", "return ShowWindowWithClose('../Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%', '" & btnname & "');")
                        End If
                    Else
                        If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "F108917")) Then
                            lnkbtnapply.Visible = False
                            labelStatus.Text = "Not Enrolled"
                            ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                            lnkTermCond.Visible = False
                            chkTerm.Visible = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Repeater1_ItemDataBound: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub




    Protected Function valiatesameday(ByVal Label As Integer, ByVal selecteditemtext As String, ByVal selecteditem As Int64) As Integer
        'Select multiple activty happening on same day, if there and if do not allow flag is there then should not allow activity to enroll. flag is getting data for the same. 
        ' [OASIS].[VALIDATE_ACT_NOTTOALLOW_SAMEDAY]()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim paramSs(3) As SqlClient.SqlParameter
        paramSs(0) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
        paramSs(1) = New SqlClient.SqlParameter("@VIEWID", Label)
        paramSs(2) = New SqlClient.SqlParameter("@WEEK", selecteditemtext)
        paramSs(3) = New SqlClient.SqlParameter("@SCH_ID", selecteditem)
        Dim ds_notallow As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[VALIDATE_ACT_NOTTOALLOW_SAMEDAY]", paramSs)
        If ds_notallow.Tables.Count > 0 Then
            If ds_notallow.Tables(0).Rows.Count > 0 Then
                B_DayNotallow = 1
            Else
                B_DayNotallow = 0
            End If
        End If
        Return B_DayNotallow
    End Function
    Protected Function CheckEnrolledStatus(ByVal ViewId As Integer) As Integer
        Dim rowcount As Integer = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Param(0) = New SqlClient.SqlParameter("@VIEWID", ViewId)
        Param(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
        rowcount = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[GET_ACT_REQ_DETAILS_PARENTTABLE]", Param)
        Return rowcount
    End Function


    Public Shared Function FEECOLLECTION_H_ONLINE_Validation(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
        pParms(0).Value = FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.FEECOLLECTION_H_ONLINE_Validation", pParms)
        FEECOLLECTION_H_ONLINE_Validation = pParms(1).Value
    End Function
    Public Shared Function FeecollectionTypeSetting(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction, ByVal APD_ID As Integer) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@MERCH_TXN_REF", SqlDbType.Int)
        pParms(0).Value = FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@ACT_APD_ID", SqlDbType.Int)
        pParms(1).Value = APD_ID
        pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[SET_ACTIVITY_COLLECTIONTYPE]", pParms)
        FeecollectionTypeSetting = pParms(2).Value
    End Function
    'Protected Sub btnSkip_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.testpopup.Style.Item("display") = "none"
    '    ' Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    '    bPaymentConfirmMsgShown = False
    'End Sub

    'Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Response.Redirect("../Fees/feePaymentRedirectNew.aspx")
    'End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                'lblError.CssClass = "alert alert-warning"
            Else
                ' lblError.CssClass = "alert alert-warning"
            End If
        Else
            ' lblError.CssClass = "alert alert-warning"
        End If
        lblError.Text = Message
    End Sub

    Protected Sub lnkbtnUnReg_Command(sender As Object, e As CommandEventArgs)
        'Dim ViewId As Integer = Convert.ToInt32(e.CommandArgument)
        'Dim Dt_table As DataTable = DataResult
        'Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewId)
        'Dim labelStatus As Label = DirectCast(sender.parent.FindControl("lblStatus"), Label)
        'Dim lnkbtnapply As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnapply"), LinkButton)
        'Dim lnkbtnPay As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnPay"), LinkButton)
        'Dim lnkbtnUnReg As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnUnReg"), LinkButton)
        'Dim lnkTermCond As LinkButton = CType(sender.parent.FindControl("lnkTermCond"), LinkButton)
        'Dim chkTerm As CheckBox = DirectCast(sender.parent.FindControl("chkTerm"), CheckBox)
        'Dim divgrad As HtmlGenericControl = DirectCast(sender.parent.FindControl("divgrad"), HtmlGenericControl)
        'Dim divTermMessage As HtmlGenericControl = DirectCast(sender.parent.FindControl("divTermMessage"), HtmlGenericControl)
        'Dim ribbonNotPaid As HtmlGenericControl = DirectCast(sender.parent.FindControl("ribbonNotPaid"), HtmlGenericControl)

        'If dRow(0)("ALD_AUTO_ENROLL") Then
        '    If dRow(0)("ALD_ISTERMS") Then
        '        lnkTermCond.Visible = True
        '        chkTerm.Visible = True
        '        lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
        '        lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
        '        lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
        '    Else
        '        lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
        '    End If
        '    ribbonNotPaid.Attributes.Add("class", "")
        '    labelStatus.Text = ""
        '    lnkbtnUnReg.Visible = False
        '    lnkbtnPay.Visible = False
        '    If (ViewState("MainMnu_code") <> "" And (ViewState("MainMnu_code") = "S10899" Or ViewState("MainMnu_code") = "F108916")) Then
        '        lnkbtnapply.Visible = True
        '    End If           

        '    Exit Sub
        'End If
        'Dim param(1) As SqlClient.SqlParameter
        'Try
        '    Dim objConn As New SqlConnection(str_conn)
        '    objConn.Open()
        '    strans = objConn.BeginTransaction
        '    param(0) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID)
        '    param(1) = New SqlClient.SqlParameter("@VIEWID", e.CommandArgument)
        '    SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UNREGISSTER_ACTIVITY]", param)
        '    strans.Commit()
        '    LoadRepeaterGroup(uscStudentPicker.STU_ID)
        'Catch ex As Exception
        '    strans.Rollback()
        '    lblError.Text = UtilityObj.getErrorMessage(1000)
        '    UtilityObj.Errorlog(ex.Message)
        'End Try
        '[OASIS].[UNREGISSTER_ACTIVITY]

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim param(4) As SqlClient.SqlParameter

            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            param(0).Value = uscStudentPicker.STU_ID
            param(1) = New SqlClient.SqlParameter("@APD_ID", SqlDbType.BigInt)
            param(1).Value = e.CommandArgument
            param(2) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
            param(2).Value = Session("sUsr_name")
            param(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[UNREGISTER_ACTIVITY]", param)
            If param(3).Value = 0 Then
                strans.Commit()
            Else
                strans.Rollback()
            End If
            LoadRepeaterGroup(uscStudentPicker.STU_ID)
        Catch ex As Exception
            strans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(1000)
            UtilityObj.Errorlog("From lnkbtnUnReg_Command:" + ex.Message, "ACTIVITY SERVICES")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub rdoSchdlDetails_SelectedIndexChanged(sender As Object, e As EventArgs)
        ' Dim label As String = DirectCast(sender.parent.FindControl("lblViewId"), Label).Text

        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)


        Dim rdoSchdlDetails As RadioButtonList = DirectCast(sender.parent.FindControl("rdoSchdlDetails"), RadioButtonList)
        Dim lnkbtnapply As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnapply"), LinkButton)
        Dim lnkTermCond As LinkButton = CType(sender.parent.FindControl("lnkTermCond"), LinkButton)
        Dim divgrad As HtmlGenericControl = DirectCast(sender.parent.FindControl("divgrad"), HtmlGenericControl)
        Dim chkTerm As CheckBox = DirectCast(sender.parent.FindControl("chkTerm"), CheckBox)
        Dim divTermMessage As HtmlGenericControl = DirectCast(sender.parent.FindControl("divTermMessage"), HtmlGenericControl)

        B_DayNotallow = valiatesameday(ViewID, rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0), rdoSchdlDetails.SelectedItem.Value)
        If (B_DayNotallow = 1) Then
            lnkbtnapply.Enabled = False
            lnkbtnapply.Attributes.Add("OnClick", "AlertMultiple(); return false;")
            If dRow(0)("ALD_ISTERMS") Then
                chkTerm.Enabled = False
            End If
        Else
            lnkbtnapply.Enabled = True
            If dRow(0)("ALD_ISTERMS") Then
                'chkTerm.Enabled = True
                'lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
                If chkTerm.Checked Then
                    divTermMessage.Style.Add("display", "none")
                    lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
                Else
                    lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
                End If
            Else
                lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
            End If
            ' lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
        End If

        'Repeater1_ItemDataBound(Nothing, Nothing)
    End Sub
    Protected Sub ddlcount_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
        'Get specific row from datatable 
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)

        Dim ddlcount As DropDownList = DirectCast(sender.parent.findcontrol("ddlcount"), DropDownList)
        Dim lblamt As Label = DirectCast(sender.parent.findcontrol("lblamt"), Label)
        '   Dim hidAmnt As HiddenField = DirectCast(sender.parent.findcontrol("hidAmnt"), HiddenField)
        Dim hidTotalAmt As HiddenField = DirectCast(sender.parent.findcontrol("hidTotalAmt"), HiddenField)
        If (ddlcount.SelectedValue <> 0) Then
            Dim count As Integer = Convert.ToInt16(ddlcount.SelectedValue)
            Dim total_amount_topay As Double = count * dRow(0)("ALD_EVENT_AMT")
            lblamt.Text = String.Format("{0:#.00}", total_amount_topay) + " " + Session("BSU_CURRENCY")
            hidTotalAmt.Value = String.Format("{0:#.00}", total_amount_topay)
        Else
            Dim amount As Double = dRow(0)("ALD_EVENT_AMT")
            lblamt.Text = String.Format("{0:#.00}", amount) + " " + Session("BSU_CURRENCY")
            hidTotalAmt.Value = String.Format("{0:#.00}", amount)
        End If
        'End If
    End Sub

    Protected Sub lnkbtnapply_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim param(8) As SqlClient.SqlParameter
        Dim ReturnVal As String = Nothing
        Dim Comments As String = ""
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
            Dim Dt_table As DataTable = DataResult
            Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)

            Dim divcount As HtmlGenericControl = DirectCast(sender.parent.findcontrol("divcount"), HtmlGenericControl)
            Dim ddlcount As DropDownList = DirectCast(sender.parent.findcontrol("ddlcount"), DropDownList)
            Dim hidTotalAmt As HiddenField = DirectCast(sender.parent.findcontrol("hidTotalAmt"), HiddenField)
            '  Dim hidAmnt As HiddenField = DirectCast(sender.parent.findcontrol("hidAmnt"), HiddenField)
            '  Dim label As String = DirectCast(sender.parent.FindControl("lblViewId"), Label).Text
            Dim rdoSchdlDetails As RadioButtonList = DirectCast(sender.parent.FindControl("rdoSchdlDetails"), RadioButtonList)
            Dim divSchdl As HtmlGenericControl = DirectCast(sender.parent.FindControl("divSchdl"), HtmlGenericControl)
            '  Dim hidCollMode As HiddenField = DirectCast(sender.parent.findcontrol("hidCollMode"), HiddenField)
            '  Dim hidALD_PAY_ONLINE As HiddenField = DirectCast(sender.parent.findcontrol("hidALD_PAY_ONLINE"), HiddenField)
            '  Dim hidmailid As HiddenField = DirectCast(sender.parent.findcontrol("hidmailid"), HiddenField)
            '  Dim txtEmailTemplate As TextBox = CType(sender.parent.FindControl("txtEmailTemplate"), TextBox)

            Dim hidAPG_ID As HiddenField = DirectCast(sender.parent.parent.parent.parent.FindControl("hidAPG_ID"), HiddenField)


            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@VIEWID", ViewID)
            param(1) = New SqlClient.SqlParameter("@STU_ID", uscStudentPicker.STU_ID)
            param(2) = New SqlClient.SqlParameter("@STU_NAME", uscStudentPicker.STU_NAME)
            If divcount.Visible = True Then
                param(3) = New SqlClient.SqlParameter("@STU_COUNT", Convert.ToInt64(ddlcount.SelectedValue))
            Else
                param(3) = New SqlClient.SqlParameter("@STU_COUNT", 1)
            End If
            If Not hidTotalAmt.Value Is Nothing And hidTotalAmt.Value.ToString.Trim <> "" Then
                param(4) = New SqlClient.SqlParameter("@TOTAL_AMOUNT", Convert.ToDecimal(hidTotalAmt.Value))
            Else
                param(4) = New SqlClient.SqlParameter("@TOTAL_AMOUNT", Convert.ToDecimal(dRow(0)("ALD_EVENT_AMT")))
            End If
            If divSchdl.Visible = True AndAlso rdoSchdlDetails.Items.Count > 0 Then
                param(5) = New SqlClient.SqlParameter("@SCHEDULE", rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0))
                param(6) = New SqlClient.SqlParameter("@SCH_ID", rdoSchdlDetails.SelectedItem.Value)
            Else
                param(5) = New SqlClient.SqlParameter("@SCHEDULE", "")
                param(6) = New SqlClient.SqlParameter("@SCH_ID", Nothing)
            End If

            If hidAPG_ID.Value = 0 Or hidAPG_ID.Value = "0" Then
                param(7) = New SqlClient.SqlParameter("@GRP_ID", Nothing)
            Else
                param(7) = New SqlClient.SqlParameter("@GRP_ID", hidAPG_ID.Value)
            End If
            param(8) = New SqlClient.SqlParameter("@APD_ID", SqlDbType.VarChar, 50)
            param(8).Direction = ParameterDirection.Output

            'ReturnVal = SqlHelper.ExecuteScalar(strans, "[OASIS].[UPDATE_ACTIVITY_DETAILS_TO_PARENTPORTAL]", param)
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[UPDATE_ACTIVITY_DETAILS_TO_PARENTPORTAL]", param)

            If Not param(8).Value Is Nothing AndAlso param(8).Value.ToString.Trim <> "" Then
                strans.Commit()
            Else
                strans.Rollback()
            End If

            If ReturnVal = "No" Then
                If dRow(0)("ALD_ISEMAIL") Then
                    SendNotificationEmail(dRow(0)("ALD_EMAIL_TEMPLATE"), dRow(0)("STS_FEMAIL"))
                End If
            End If
            LoadRepeaterGroup(uscStudentPicker.STU_ID)
        Catch ex As Exception
            strans.Rollback()
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From lnkbtnapply_Click:" + ex.Message, "ACTIVITY SERVICES")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Public Shared Function StringReplace(ByVal Source As String, ByVal FromReplaceStr As String, ByVal ToReplaceStr As String) As String
        Try
            Dim StartVal As String, OrigStr As String
            StartVal = Source.ToUpper.IndexOf(FromReplaceStr.ToUpper)
            OrigStr = Source.Substring(StartVal, FromReplaceStr.Length)
            Source = Source.Replace(OrigStr, ToReplaceStr)
        Catch ex As Exception
        Finally
            StringReplace = Source
        End Try

    End Function
    Protected Sub SendNotificationEmail(ByVal emailtemplate As String, ByVal mailid As String)

        Dim tomail As String = String.Empty
        Dim sendAs As String = String.Empty
        Dim SendpWD As String = String.Empty
        Dim sendPort As String = String.Empty
        Dim FromEmail As String = String.Empty
        Dim SendHost As String = String.Empty
        Dim subject As String = String.Empty
        Dim mailbody As String = String.Empty
        Try
            tomail = mailid.ToString()
            If Not tomail Is Nothing Then
                If Mainclass.isEmail(tomail) Then
                    subject = "Mail Notification : Your Activity Request for *" + ViewState("ActivtyName") + "* Has been APPROVED"
                    mailbody = emailtemplate.ToString()
                    'Inserting into table [COM_EMAIL_SCHEDULE] for sending the email 
                    InsertintoMailTable(subject, mailbody, tomail)
                Else
                    'Check on this
                    ' Dim ErrorMessage As String = "Error: Email Id Is Not Valid"
                    Message = UtilityObj.getErrorMessage("4005")
                    UtilityObj.Errorlog(Message, "ACTIVITY SERVICES")
                End If
            End If
        Catch EX As Exception
            'failure sending email                    
            UtilityObj.Errorlog("From SendNotificationEmail" + EX.Message, "ACTIVITY SERVICES")
        Finally

        End Try
    End Sub
    Protected Sub InsertintoMailTable(ByVal subject As String, ByVal message As String, ByVal tomailid As String)
        Try
            Dim str_conn = ConnectionManger.GetOASISConnectionString
            Dim query As String = "[dbo].[InsertIntoEmailSendSchedule] '" & ddlBusinessunit.SelectedValue & "','ActivityRequestStatusMail','SYSTEM','" & tomailid & "','" & subject & "','" & message & "'"
            Mainclass.getDataValue(query, "OASISConnectionString")
        Catch ex As Exception
            message = UtilityObj.getErrorMessage("4000")
            ShowMessage(message, True)
            UtilityObj.Errorlog("From InsertIntoMailTable: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub hidStatus_ValueChanged(sender As Object, e As EventArgs)
        hidStatus.Value = "false"
        LoadRepeaterGroup(uscStudentPicker.STU_ID)
    End Sub
    Protected Sub chkTerm_CheckedChanged(sender As Object, e As EventArgs)

        'Dim label As String = DirectCast(sender.parent.FindControl("lblViewId"), Label).Text
        'Dim rdoSchdlDetails As RadioButtonList = DirectCast(sender.parent.FindControl("rdoSchdlDetails"), RadioButtonList)
        ' Dim divSchdl As HtmlGenericControl = DirectCast(sender.parent.FindControl("divSchdl"), HtmlGenericControl)
        'Dim lnkTermCond As LinkButton = CType(sender.parent.FindControl("lnkTermCond"), LinkButton)
        'Dim divgrad As HtmlGenericControl = DirectCast(sender.parent.FindControl("divgrad"), HtmlGenericControl)

        Dim lnkbtnapply As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnapply"), LinkButton)
        Dim chkTerm As CheckBox = sender
        Dim divTermMessage As HtmlGenericControl = DirectCast(sender.parent.FindControl("divTermMessage"), HtmlGenericControl)

        If chkTerm.Checked Then
            divTermMessage.Style.Add("display", "none")
            lnkbtnapply.Attributes.Add("OnClick", "return ConfirmEnroll();")
        Else
            lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
        End If
    End Sub



    Protected Sub lnkbtnRefund_Command(sender As Object, e As CommandEventArgs)
        Dim retval As String = "1000"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_NEW_FRH_ID As String = ""
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim txtBankCharge As String = String.Empty
        Dim AMOUNT As Double = 0, BSU_ID As String = uscStudentPicker.STU_BSU_ID
        Dim FRD_NARRATION As String = String.Empty
        Dim fee_id As String = String.Empty
        Dim stTrans As SqlTransaction
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        stTrans = objConn.BeginTransaction
        Try

            Dim ViewId As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Dt_table As DataTable = DataResult
            Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewId)
            Dim lnkbtnRefund As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnRefund"), LinkButton)
            STR_BankOrCash = "C"
            h_Adjustment.Value = 0
            STR_ACCOUNT = "24301001"
            txtBankCharge = "0"
            AMOUNT = dRow(0)("APD_TOTAL_AMOUNT")


            retval = FeeRefund.F_SaveFEE_REFUND_H(0, BSU_ID, 0, uscStudentPicker.STU_TYPE,
                uscStudentPicker.STU_ID, h_Adjustment.Value, Format(Date.Now, "dd/MMM/yyyy"), Format(Date.Now, "dd/MMM/yyyy"), False, str_NEW_FRH_ID, False,
             "Refund Requested for activity - " + dRow(0)("ALD_EVENT_NAME").ToString + " (" + dRow(0)("EVENT_DATE").ToString + ")", "", STR_BankOrCash, STR_ACCOUNT, "",
             AMOUNT, txtBankCharge, stTrans, dRow(0)("APD_ID"), "ACTIVITY", 0)
            If retval = "0" Then
                fee_id = dRow(0)("ALD_FEETYPE_ID")
                FRD_NARRATION = "Activity Payment Refund"
                retval = FeeRefund.F_SaveFEE_REFUND_D(0, str_NEW_FRH_ID, AMOUNT,
                                     fee_id, FRD_NARRATION, uscStudentPicker.STU_ID, stTrans)

            End If
            If retval = "0" Then
                stTrans.Commit()
                LoadRepeaterGroup(uscStudentPicker.STU_ID)
                ShowMessage("Fee refund request sent for approval", False)
            Else
                stTrans.Rollback()
                ShowMessage(getErrorMessage(retval), True)
                lnkbtnRefund.Visible = True
            End If

        Catch ex As Exception
            stTrans.Rollback()
            ShowMessage(ex.Message, True)
            UtilityObj.Errorlog("From lnkbtnRefund_Command:" + ex.Message, "ACTIVITY SERVICES")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub

    Protected Sub lnkbtnCnclRecpt_Command(sender As Object, e As CommandEventArgs)
        Dim retval As String = "1000"
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        conn.Open()
        Try
            Dim ViewId As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Dt_table As DataTable = DataResult
            Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewId)
            Dim lnkbtnCnclRecpt As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnCnclRecpt"), LinkButton)

            'trans = conn.BeginTransaction("DeleteReceipt_TRANS")

            Dim cmd As New SqlCommand("FEES.DeleteReceipt_OASIS_Approval", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            sqlpAUD_BSU_ID.Value = ddlBusinessunit.SelectedValue
            cmd.Parameters.Add(sqlpAUD_BSU_ID)

            Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlpFCL_RECNO.Value = dRow(0)("APD_RECEIPT_NO")
            cmd.Parameters.Add(sqlpFCL_RECNO)


            Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            sqlpAUD_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpAUD_USER)

            Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            sqlpAUD_REMARKS.Value = "Activity Fee Receipt Cancellation for activity - " + dRow(0)("ALD_EVENT_NAME") + " (" + dRow(0)("EVENT_DATE") + ")"
            cmd.Parameters.Add(sqlpAUD_REMARKS)

            Dim sqlpAUD_TYPE As New SqlParameter("@FRCA_TYPE", SqlDbType.VarChar, 10)
            sqlpAUD_TYPE.Value = "FEE"
            cmd.Parameters.Add(sqlpAUD_TYPE)

            Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            sqlpFromDT.Value = Today.Date
            cmd.Parameters.Add(sqlpFromDT)

            Dim sqlpAPDID As New SqlParameter("@APD_ID", SqlDbType.BigInt)
            sqlpAPDID.Value = dRow(0)("APD_ID")
            cmd.Parameters.Add(sqlpAPDID)

            Dim sqlpSOURCE As New SqlParameter("@SOURCE", SqlDbType.VarChar)
            sqlpSOURCE.Value = "ACTIVITY"
            cmd.Parameters.Add(sqlpSOURCE)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                ShowMessage(UtilityObj.getErrorMessage(iReturnvalue), True)
                lnkbtnCnclRecpt.Visible = True
            Else
                LoadRepeaterGroup(uscStudentPicker.STU_ID)
                ShowMessage("Fee receipt cancellation sent for approval", False)
            End If
        Catch ex As Exception
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub
    Protected Sub lnkbtnAdjst_Click(sender As Object, e As EventArgs)
        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim param(2) As SqlClient.SqlParameter
        Dim ReturnVal As String = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try

            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@ALD_ID", SqlDbType.BigInt)
            param(0).Value = ViewID
            param(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            param(1).Value = uscStudentPicker.STU_ID
            param(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "FEES.SAVE_ACTIVITY_ADJUSTMENTS", param)

            If Not IsDBNull(param(2).Value) Then
                If param(2).Value = 0 Then
                    strans.Commit()
                    'lblError.Text = "Activity Adjusted Successfully"
                    '  ShowMessage(lblError.Text, False)
                    LoadRepeaterGroup(uscStudentPicker.STU_ID)
                Else
                    strans.Rollback()
                    ShowMessage(getErrorMessage(param(2).Value), True)
                End If
            Else
                strans.Rollback()
                ShowMessage(getErrorMessage(1000), True)
            End If

        Catch ex As Exception
            strans.Rollback()
            ShowMessage(getErrorMessage(1000), True)
            UtilityObj.Errorlog("From lnkbtnAdjst_Click:" + ex.Message, "ACTIVITY SERVICES")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    'Added by vikranth on 21st Jan 2020
    Protected Sub lnkbtnReverseAndUnReg_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            Dim ViewId As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Dt_table As DataTable = DataResult
            Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewId)
            Dim param(5) As SqlClient.SqlParameter

            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            param(0).Value = uscStudentPicker.STU_ID
            param(1) = New SqlClient.SqlParameter("@APD_ID", SqlDbType.BigInt)
            param(1).Value = dRow(0)("APD_ID")
            param(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            param(2).Value = uscStudentPicker.STU_BSU_ID
            param(3) = New SqlClient.SqlParameter("@FRH_Narr", SqlDbType.VarChar)
            param(3).Value = dRow(0)("ALD_EVENT_NAME") + " (" + dRow(0)("EVENT_DATE") + ")"
            param(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[ACTIVITY_REVERSE_UNREGISTER]", param)
            If param(4).Value = 0 Then
                strans.Commit()
            Else
                strans.Rollback()
            End If
            LoadRepeaterGroup(uscStudentPicker.STU_ID)
        Catch ex As Exception
            strans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(1000)
            UtilityObj.Errorlog("From lnkbtnReverseAndUnReg_Command:" + ex.Message, "ACTIVITY SERVICES")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        uscStudentPicker.ClearDetails()
        uscStudentPicker.STU_BSU_ID = ddlBusinessunit.SelectedValue
        B_DayNotallow = 0
        Max_Limit_ForRptr = 0
        bPaymentConfirmMsgShown = False
    End Sub
End Class
