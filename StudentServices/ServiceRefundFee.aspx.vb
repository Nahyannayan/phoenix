﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_ServiceRefundFee
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtChqNo.Attributes.Add("readonly", "readonly")
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            InitialiseCompnents()
            bindBusinessUnits()
            'ddlBusinessunit.DataBind()


            FillACD()
            'Session("FeeCollection") = FeeCollection.CreateFeeCollection
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "F733038" _
                And ViewState("MainMnu_code") <> "F733039") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Select Case ViewState("MainMnu_code").ToString
                    Case "F733038"
                        lblHead.Text = "Service Refund Request"
                        tbl_Approval.Visible = False
                        If Request.QueryString("view_id") <> "" Then
                            ViewState("datamode") = "view"
                            SET_VIEWDATA(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            tbl_Allocation.Visible = False
                            tbl_Approval.Visible = True
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Case "F733039"
                        lblHead.Text = "Service Refund Request Approval"
                        btnApprove.Visible = True
                        btnReject.Visible = True
                        imgStudent.Enabled = False
                        rbEnrollment.Enabled = False
                        rbEnquiry.Enabled = False
                        txtBankCharge.Attributes.Add("readonly", "readonly")
                        txtFrom.Attributes.Add("readonly", "readonly")
                        'imgFrom.Enabled = False
                        tbl_Allocation.Visible = False
                        tbl_Approval.Visible = True
                        If Request.QueryString("view_id") <> "" Then
                            SET_VIEWDATA(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")))
                            SetTCReference()
                            tbl_Allocation.Visible = False
                            tbl_Approval.Visible = True
                        End If
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        btnSave.Visible = False
                End Select
            End If
        End If
    End Sub
    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()
        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter

        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddlBusinessUnit.DataSource = ds.Tables(0)
            ddlBusinessUnit.DataTextField = "BSU_NAME"
            ddlBusinessUnit.DataValueField = "BSU_ID"
            ddlBusinessUnit.DataBind()
            ddlBusinessUnit.SelectedIndex = -1
            ddlBusinessUnit.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlBusinessunit_SelectedIndexChanged(ddlBusinessUnit, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clear_All()
        Set_Previous_Acd()
        'PopulateMonths()
        'Gridbind_Feedetails()
    End Sub
    Sub FillACD()
        'If Not ddlBusinessunit.SelectedItem.Value = 0 Then
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sbsuId"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        Set_Previous_Acd()
        'End If
    End Sub
    Sub Set_Previous_Acd()
        h_PREV_ACD.Value = TransportRefund.GetPreviousACD(ddlAcademicYear.SelectedItem.Value, ddlBusinessunit.SelectedItem.Value, _
        ConnectionManger.GetOASISTRANSPORTConnectionString)
    End Sub
    Sub set_bankaccount()
        Dim str_bankact_name As String = UtilityObj.GetDataFromSQL("SELECT BSU.BSU_PAYMENTBANK_ACT_ID+'|'+ACT.ACT_NAME " _
        & " FROM VW_OSO_BUSINESSUNIT_M AS BSU INNER JOIN" _
        & " VW_OSA_ACCOUNTS_M AS ACT ON BSU.BSU_PAYMENTBANK_ACT_ID = ACT.ACT_ID" _
        & " WHERE (BSU.BSU_ID = '" & Session("sBsuid") & "')", ConnectionManger.GetOASISFINConnectionString)
        If str_bankact_name <> "--" Then
            txtBankCode.Text = str_bankact_name.Split("|")(0)
            txtBankDescr.Text = str_bankact_name.Split("|")(1)
        End If
    End Sub

    Sub SET_VIEWDATA(ByVal p_FRD_FRH_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim STR_SQL As String = "SELECT FRH.FRH_BSU_ID,FRH.FRH_STU_BSU_ID, FRH.FRH_ACD_ID, FRH.FRH_STU_TYPE, FRH.FRH_STU_ID, " _
        & " FRH.FRH_DATE, FRH.FRH_NARRATION, FRH.FRH_ACT_ID, FRH.FRH_PAIDTO, STU.STU_NO, " _
        & " STU.STU_NAME, FRH.FRH_TOTAL, FRH.FRH_FAR_ID, ACT.ACT_NAME, FRH.FRH_CHQDT, " _
        & " FRH.FRH_SCT_ID, FRH.FRH_BANK_CASH, FAR.FAR_REMARKS, FRH.FRH_VHH_DOCNO, " _
        & " isnull(FRH.FRH_bPosted,0) FRH_bPosted,ISNULL(FRH_BANKCHARGE,0) FRH_BANKCHARGE FROM FEES.FEE_REFUND_H AS FRH " _
        & " INNER JOIN VW_OSO_STUDENT_ENQUIRY AS STU ON FRH.FRH_STU_ID = STU.STU_ID " _
        & " INNER JOIN OASIS_FEES.dbo.vw_OSF_ACCOUNTS_M AS ACT ON FRH.FRH_ACT_ID = ACT.ACT_ID " _
        & " LEFT OUTER JOIN FEES.FEEADJREQUEST_H AS FAR ON FRH.FRH_FAR_ID = FAR.FAR_ID WHERE FRH.FRH_ID='" & p_FRD_FRH_ID & "'"
        Dim DS1 As New DataSet
        DS1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)
        Me.ddlBusinessunit.SelectedValue = DS1.Tables(0).Rows(0)("FRH_STU_BSU_ID")
        'ddlBusinessunit_SelectedIndexChanged(Me, New EventArgs)
        FillACD()
        rbBank.Enabled = False
        rbCash.Enabled = False
        H_STU_TYPE.Value = DS1.Tables(0).Rows(0)("FRH_STU_TYPE")
        h_Student_no.Value = DS1.Tables(0).Rows(0)("FRH_STU_ID")
        ViewState("FRH_VHH_DOCNO") = DS1.Tables(0).Rows(0)("FRH_VHH_DOCNO")
        ViewState("FRH_BANK_CASH") = DS1.Tables(0).Rows(0)("FRH_BANK_CASH")
        txtBankCharge.Text = Format(DS1.Tables(0).Rows(0)("FRH_BANKCHARGE"), "0.00")

        If Not ViewState("FRH_VHH_DOCNO") Is Nothing And DS1.Tables(0).Rows(0)("FRH_bPosted") = True Then
            btnPrint1.Visible = True
            chkPrintChq.Visible = True
        End If
        If DS1.Tables(0).Rows(0)("FRH_BANK_CASH") = "C" Then
            chkPrintChq.Visible = False
            rbBank.Checked = False
            rbCash.Checked = True
            txtCashAcc.Text = DS1.Tables(0).Rows(0)("FRH_ACT_ID")
            txtCashDescr.Text = DS1.Tables(0).Rows(0)("ACT_NAME")
        Else
            rbCash.Checked = False
            rbBank.Checked = True
            txtBankCode.Text = DS1.Tables(0).Rows(0)("FRH_ACT_ID")
            txtBankDescr.Text = DS1.Tables(0).Rows(0)("ACT_NAME")
        End If
        Set_CashorBank()
        set_chequeno_controls()
        txPaidto.Text = DS1.Tables(0).Rows(0)("FRH_PAIDTO")
        txtAmount.Text = Format(DS1.Tables(0).Rows(0)("FRH_TOTAL"), "0.00")
        txtFrom.Text = Format(DS1.Tables(0).Rows(0)("FRH_DATE"), "dd/MMM/yyyy")
        txtChqdt.Text = Format(DS1.Tables(0).Rows(0)("FRH_CHQDT"), "dd/MMM/yyyy")
        txtStudentname.Text = DS1.Tables(0).Rows(0)("STU_NAME")
        txtStdNo.Text = DS1.Tables(0).Rows(0)("STU_NO")
        txtRemarks.Text = DS1.Tables(0).Rows(0)("FRH_NARRATION")
        Dim strSTU_TYP As String = DS1.Tables(0).Rows(0)("FRH_STU_TYPE")
        If strSTU_TYP = "E" Then
            rbEnquiry.Checked = True
        Else
            rbEnrollment.Checked = True
        End If
        txtAdjustment.Text = DS1.Tables(0).Rows(0)("FAR_REMARKS").ToString
        h_Adjustment.Value = DS1.Tables(0).Rows(0)("FRH_FAR_ID").ToString

        STR_SQL = "SELECT FRD_ID,  FEES.FEES_M.FEE_DESCR AS Fee, FEES.FEE_REFUND_D.FRD_AMOUNT AS Amount" _
            & " FROM  FEES.FEE_REFUND_D INNER JOIN " _
            & " FEES.FEES_M ON FEES.FEE_REFUND_D.FRD_FEE_ID = FEES.FEES_M.FEE_ID " _
            & " WHERE   FEES.FEE_REFUND_D.FRD_FRH_ID='" & p_FRD_FRH_ID & "'"
        Dim DS2 As New DataSet
        DS2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STR_SQL)
        gvApproval.DataSource = DS2.Tables(0)
        gvApproval.DataBind()

        If ViewState("MainMnu_code").ToString = "F733039" Then
            ddlBusinessunit.Enabled = False
            ddlAcademicYear.Enabled = False
        End If


    End Sub

    Sub InitialiseCompnents()
        H_STU_TYPE.Value = "S"
        txtBankCharge.Text = "0"
        gvRefund.Attributes.Add("bordercolor", "#1b80b6")
        gvNetRefund.Attributes.Add("bordercolor", "#1b80b6")
        Set_CashorBank()
        set_chequeno_controls()
        txtAmount.Attributes.Add("readonly", "readonly")
        txtCashAcc.Attributes.Add("readonly", "readonly")
        txtCashDescr.Attributes.Add("readonly", "readonly")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdt.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtBankCode.Attributes.Add("readonly", "readonly")
        txtBankDescr.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtChqNo.Attributes.Add("readonly", "readonly")
        'txtAdjustment.Attributes.Add("readonly", "readonly")
        txtChqBook.Attributes.Add("readonly", "readonly")
        txtProvCode.Attributes.Add("readonly", "readonly")
        txtProvDescr.Attributes.Add("readonly", "readonly")
        'txPaidto.Attributes.Add("readonly", "readonly")
        set_bankaccount()
    End Sub

    Sub clear_All()
        txtBankCharge.Text = "0"
        h_Adjustment.Value = ""
        h_Student_no.Value = ""
        H_STU_TYPE.Value = ""
        txtCashAcc.Text = ""
        txtCashDescr.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtChqdt.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtBankCode.Text = ""
        txtBankDescr.Text = ""
        txtChqBook.Text = ""
        txtChqNo.Text = ""
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtProvCode.Text = ""
        txtProvDescr.Text = ""
        ClearStudentData()
        set_bankaccount()
        txtRemarks.Text = ""
        txtAdjustment.Text = ""
        lblAdjType.Text = ""
        txPaidto.Text = ""
        chkFee.Checked = False
        chkLab.Checked = False
        chkLibrary.Checked = False
        chkRegistrar.Checked = False
        setStudntData()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        'Set_GridTotal()

    End Sub

   

    Sub SettleFee()

        If IsDate(txtFrom.Text) And h_Student_no.Value <> "" Then
            Dim retval As Integer
            Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                

                retval = FeeCollectionService.F_SettleFee_Service(Session("sBsuid"), txtFrom.Text, _
                                h_Student_no.Value, ddlBusinessunit.SelectedValue, stTrans)


                If retval = "0" Then
                    stTrans.Commit()
                Else
                    stTrans.Rollback()
                    '  lblError.Text = getErrorMessage(retval)
                    usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                '   lblError.Text = getErrorMessage(1000)
                usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
                stTrans.Rollback()
                Errorlog(ex.Message)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        End If
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        h_Student_no.Value = ""
        rbBank.Enabled = True
        rbCash.Enabled = True
        ViewState("datamode") = "add"

        clear_All()

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        setStudntData()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
        SetTCReference()
    End Sub

    Private Sub SetTCReference()
        If h_Student_no.Value <> "" Then
            Dim STR_SQL As String = "SELECT TCM.TCM_bLabAppr, TCM.TCM_bFeeAppr, TCM.TCM_bLibAppr,TCM.TCM_ID, " & _
            " TCM_bRegAppr,'Refund'AS EVENT, TCM.TCM_TCSO, TCM.TCM_LASTATTDATE, TCM.TCM_REASON " & _
            " FROM VW_OSO_STUDENT_M INNER JOIN TCM_M TCM ON TCM_BSU_ID =STU_BSU_ID " & _
            " AND TCM_ACD_ID = STU_ACD_ID AND TCM_STU_ID = STU_ID  " & _
            " WHERE STU_bActive=1 AND isnull(TCM_ISSUEDATE,'1/1/1900') = '1/1/1900' " & _
            " AND TCM_bCANCELLED = 0 AND TCM_bRegAppr = 1 AND ISNULL(TCM_bCANCELLED,0) = 0 " & _
            " AND TCM_BSU_ID = '" & Session("sBsuid") & "' AND STU_ID = " & h_Student_no.Value
            Dim dr As SqlDataReader
            Dim bDataExists As Boolean = False
            dr = SqlHelper.ExecuteReader(ConnectionManger.GetOASISConnectionString, CommandType.Text, STR_SQL)
            While (dr.Read())
                bDataExists = True
                chkFee.Checked = dr("TCM_bFeeAppr")
                chkLab.Checked = dr("TCM_bLabAppr")
                chkLibrary.Checked = dr("TCM_bLibAppr")
                chkRegistrar.Checked = dr("TCM_bRegAppr")
                lblAdjType.Text = "Last Att. Date : " & Format(dr("TCM_LASTATTDATE"), OASISConstants.DateFormat)
                txtAdjustment.Text = dr("TCM_REASON")
                h_Adjustment.Value = dr("TCM_ID")
                Exit While
            End While
            If Not bDataExists Then
                chkFee.Checked = False
                chkLab.Checked = False
                chkLibrary.Checked = False
                chkRegistrar.Checked = False
                lblAdjType.Text = ""
                txtAdjustment.Text = ""
                h_Adjustment.Value = ""
            End If
        End If
    End Sub

    Sub setStudntData()
        If rbEnquiry.Checked Then
            H_STU_TYPE.Value = "E"
        Else
            H_STU_TYPE.Value = "S"
        End If
        'StudentDetails()

        SettleFee()
    End Sub

    Sub StudentDetails()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                       CommandType.Text, " FEES.GetStudentDetailsForConcession '" & h_Student_no.Value & "'")
        gvStudentDetails.DataSource = ds.Tables(0)
        gvStudentDetails.DataBind()
    End Sub

    Sub Set_GridTotal()
        Dim dRefundAmount As Decimal = 0
        Dim dNetRefundAmount As Decimal
        Dim dtTotalApprvAmt As Double = 0
        Dim txtAmt As TextBox
        For i As Integer = 0 To gvRefund.Rows.Count - 2
            Dim gvRefundRow As GridViewRow = gvRefund.Rows(i)
            txtAmt = CType(gvRefundRow.FindControl("txtApprAmt"), TextBox)
            If txtAmt Is Nothing OrElse txtAmt.Text = "" Then Continue For
            dtTotalApprvAmt += CDbl(txtAmt.Text)
        Next
        If gvRefund.Rows.Count > 1 And gvNetRefund.Rows.Count > 1 Then
            'dRefundAmount = Convert.ToDecimal(gvRefund.Rows(gvRefund.Rows.Count - 1).Cells(1).Text)
            dNetRefundAmount = Convert.ToDecimal(gvNetRefund.Rows(gvNetRefund.Rows.Count - 1).Cells(1).Text)
            txtAmt = CType(gvRefund.Rows(gvRefund.Rows.Count - 1).FindControl("txtApprAmt"), TextBox)
            If Not txtAmt Is Nothing Then
                txtAmt.Text = dtTotalApprvAmt
                txtAmt.Attributes.Add("readonly", "readonly")
            End If
            If dtTotalApprvAmt <= dNetRefundAmount * -1 AndAlso dtTotalApprvAmt > 0 Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
        Else
            btnSave.Enabled = False
        End If
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData()
        ' Me.lblStudent.Text = ""
        ViewState("gvSelectedStudents") = Nothing
        ViewState("gvRefund") = Nothing
        ViewState("gvNetRefund") = Nothing
        ViewState("gvRefundSummary") = Nothing
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        setStudntData()

        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Try
            Dim str_data, str_sql As String
            txtStdNo.Text = txtStdNo.Text.Trim
            Dim iStdnolength As Integer = txtStdNo.Text.Length
            If rbEnrollment.Checked Then
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = Session("sBsuid") & txtStdNo.Text
                End If
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
                 & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & ddlBusinessunit.SelectedItem.Value & "') AND  STU_NO='" & txtStdNo.Text & "'"
                str_data = GetDataFromSQL(str_sql, ConnectionManger.GetOASIS_SERVICESConnectionString)
            Else
                str_data = GetDataFromSQL("SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                & " WHERE     (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & txtStdNo.Text & "'", _
                ConnectionManger.GetOASIS_SERVICESConnectionString)
            End If
            If str_data <> "--" Then
                h_Student_no.Value = str_data.Split("|")(0)
                txtStdNo.Text = str_data.Split("|")(1)
                txtStudentname.Text = str_data.Split("|")(2)
                setStudntData()
                SetTCReference()
            Else
                h_Student_no.Value = ""
                txtStudentname.Text = ""
                lblNoStudent.Text = "Student # Entered is not valid  !!!"
            End If
        Catch ex As Exception
        End Try
    End Sub
   

    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged
        Set_CashorBank()
    End Sub

    Sub Set_CashorBank()
        If rbCash.Checked Then
            tr_Bank.Visible = False
            tr_Cheque.Visible = False
            tr_Cash.Visible = True
            tr_Provision.Visible = False
            tr_ChqType.Visible = False
        Else
            tr_Bank.Visible = True
            If btnApprove.Visible Then
                tr_Cheque.Visible = True
                tr_Provision.Visible = True
                tr_ChqType.Visible = True
            End If
            FillLotNo()
            tr_Cash.Visible = False
        End If
    End Sub

    Private Sub FillLotNo()
        If txtBankCode.Text <> "" Then
            Dim str_sql As String = " SELECT TOP 1 ISNULL(CHQBOOK_M.CHB_LOTNO,'') AS LOT_NO, " & _
            " CHB_ID, ISNULL(MIN(ISNULL(CHD_NO,'')),'') AS CHD_NO FROM CHQBOOK_M INNER JOIN " & _
            " CHQBOOK_D ON CHQBOOK_M.CHB_ID = CHQBOOK_D.CHD_CHB_ID WHERE " & _
            " (CHQBOOK_D.CHD_ALLOTED = 0  ) AND (CHQBOOK_M.CHB_ACT_ID = '" & txtBankCode.Text & "')" & _
            " AND CHQBOOK_M.CHB_BSU_ID = '" & Session("sBSUID") & "'"

            str_sql += "GROUP BY CHB_LOTNO, CHB_ID "
            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, str_sql)
            While (dr.Read())
                hCheqBook.Value = dr("CHB_ID")
                txtChqNo.Text = dr("CHD_NO")
                If dr("LOT_NO") <> 0 Then
                    txtChqBook.Text = dr("LOT_NO")
                Else
                    txtChqBook.Text = ""
                End If
            End While
        End If
    End Sub

    Protected Sub rbBank_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbBank.CheckedChanged
        Set_CashorBank()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim str_error As String = ""
        If rbBank.Checked Then
            If (txtChqNo.Text = "" Or hCheqBook.Value = "") And rbCheque.Checked Then
                str_error = str_error & "Please Select Cheque"
            End If
        End If

        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim STR_DOCTYPE As String = String.Empty
        Dim STR_DOCNO As String = String.Empty
        If rbBank.Checked Then
            STR_DOCTYPE = "BP"
            STR_BankOrCash = "B"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
            If rbOthers.Checked And txtrefChequeno.Text.Trim = "" Then
                str_error = str_error & "Invalid Ref. No. <br />"
            End If
        Else
            STR_DOCTYPE = "CP"
            STR_BankOrCash = "C"
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Invalid cash account<br />"
            Else
                STR_ACCOUNT = txtCashAcc.Text
            End If
            'If AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtFrom.Text.Trim) < CDbl(txtAmount.Text) Then
            '    lblError.Text = "There is not Enough Balance in the Account"
            '    Exit Sub
            'End If
        End If

        If str_error <> "" Then
            '  lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            If h_Adjustment.Value = "" Then
                h_Adjustment.Value = 0
            End If
            Dim retval As String = "1000"
            AccountFunctions.GetNextDocId(STR_DOCTYPE, Session("sBsuid"), CType(txtFrom.Text, Date).Month, CType(txtFrom.Text, Date).Year)
            ViewState("doctype") = STR_DOCTYPE
            STR_DOCNO = "" 'current acd of student selected in sp so here scd is pasased as 0
            retval = FeeCollectionService.F_SaveService_REFUND_H(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), Session("sBsuid"), ddlBusinessunit.SelectedValue, 0, H_STU_TYPE.Value, _
                        h_Student_no.Value, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, 0, False, _
            txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, txPaidto.Text, txtAmount.Text, txtBankCharge.Text, stTrans)




            Dim dTotalAmount As Decimal = 0

            If retval = "0" Then
                For Each gvr As GridViewRow In gvApproval.Rows
                    Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtApprAmt"), TextBox)
                    Dim lblID As Label = CType(gvr.FindControl("lblID"), Label)
                    Dim dAmount As Decimal = Convert.ToDecimal(gvr.Cells(1).Text)
                    Dim aprAmt As Decimal = Convert.ToDecimal(txtAmountToPay.Text)
                    If dAmount < aprAmt Or aprAmt < 0 Then
                        retval = 940 'Approved Amount should not be greater than Actual
                        Exit For
                    End If
                    If Not lblID Is Nothing Then
                        If dAmount > 0 Then
                            dTotalAmount = dTotalAmount + aprAmt
                            retval = FeeCollectionService.F_SaveService_REFUND_D(lblID.Text, Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), aprAmt, _
                            lblID.Text, gvr.Cells(0).Text, stTrans)
                            'dDiffAmount = dDiffAmount + dAmount - aprAmt
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
            End If
            If retval = 0 Then
                If dTotalAmount <> txtAmount.Text Then
                    txtAmount.Text = dTotalAmount
                End If
                retval = F_GenerateRefundVoucher(objConn, txtFrom.Text, "Service Fee Refund for : " & txtStdNo.Text & " " & txtRemarks.Text, txtAmount.Text, STR_DOCTYPE, _
                       STR_DOCNO, stTrans)
            End If

            If ViewState("datamode") <> "edit" And retval = 0 Then
                retval = FeeCollectionService.PostServiceFeeRefund(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), _
                STR_DOCNO, txtFrom.Text, stTrans)
            End If
            If retval = "0" Then
                stTrans.Commit()
                ViewState("FRH_VHH_DOCNO") = STR_DOCNO
                ViewState("FRH_BANK_CASH") = STR_BankOrCash
                If STR_BankOrCash = "B" Then
                    chkPrintChq.Visible = True
                End If
                btnPrint1.Visible = True
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, STR_DOCNO, _
            "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                '   lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                '  lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            '   lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub txtApprAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal()
    End Sub

    Protected Sub txtApprAmt_Approve_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Set_GridTotal_Approve()
    End Sub

    Sub Set_GridTotal_Approve()
        Dim dRefundAmount As Decimal = 0
        Dim txtAmt As TextBox
        For i As Integer = 0 To gvApproval.Rows.Count - 1
            Dim gvApprovalRow As GridViewRow = gvApproval.Rows(i)
            txtAmt = CType(gvApprovalRow.FindControl("txtApprAmt"), TextBox)
            If txtAmt Is Nothing OrElse txtAmt.Text = "" Then Continue For
            dRefundAmount += CDbl(txtAmt.Text)
        Next
        txtAmount.Text = Format(dRefundAmount, "0.00")
    End Sub

    Protected Sub btnPrint1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint1.Click
        If Not ViewState("FRH_VHH_DOCNO") Is Nothing Then
            If ViewState("FRH_BANK_CASH") = "C" Then
                Session("ReportSource") = VoucherReports.CashPaymentVoucher(Session("sBSUID"), Session("F_YEAR"), Session("SUB_ID"), "CP", ViewState("FRH_VHH_DOCNO"), True)
                Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
            Else
                If chkPrintChq.Checked Then
                    Session("ReportSource") = AccountsReports.ChquePrint(ViewState("FRH_VHH_DOCNO"), "", Session("sBSUID"), Session("F_YEAR"), "BP")
                    If Session("ReportSource").Equals(Nothing) Then
                        ' lblError.Text = "Cheque Format not supported for printing"
                        usrMessageBar.ShowNotification("Cheque Format not supported for printing", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                    Response.Redirect("../Accounts/accChqPrint.aspx?ChequePrint=BP", True)
                Else
                    Session("ReportSource") = VoucherReports.BankPaymentVoucher(Session("sBSUID"), Session("F_YEAR"), Session("SUB_ID"), "BP", ViewState("FRH_VHH_DOCNO"), False, True)
                    Response.Redirect("../Reports/ASPX Report/rptviewer.aspx")
                End If
            End If
        End If

    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim str_error As String = ""
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        Dim STR_DOCNO As String = String.Empty
        If rbBank.Checked Then
            STR_BankOrCash = "B"
        Else
            STR_BankOrCash = "C"
        End If
        If h_Adjustment.Value = "" Then
            h_Adjustment.Value = 0
        End If

        If txtBankCode.Text = "" Then
            str_error = str_error & "Invalid bank<br />"
        Else
            STR_ACCOUNT = txtBankCode.Text
        End If
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            retval = TransportRefund.F_SaveTRANSPORT_REFUND_H(Encr_decrData.Decrypt(Request.QueryString("view_id").Replace(" ", "+")), Session("sBsuid"), ddlBusinessunit.SelectedValue, 0, H_STU_TYPE.Value, _
               h_Student_no.Value, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, 0, True, _
            txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, txPaidto.Text, txtAmount.Text, txtBankCharge.Text, stTrans)



            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, STR_DOCNO, _
            "Edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                '  lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                'lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Function F_GenerateRefundVoucher(ByVal objConn As SqlConnection, ByVal p_Date As Date, ByVal p_Narration As String, _
    ByVal p_Amount As String, ByVal p_Doctype As String, ByRef STR_DOCNO As String, ByVal stTrans As SqlTransaction) As String
        Dim SqlCmd As New SqlCommand("[F_GenerateServiceRefundVoucher]", objConn, stTrans)
        SqlCmd.CommandType = CommandType.StoredProcedure

        SqlCmd.Parameters.AddWithValue("@VHH_SUB_ID", Session("SUB_ID"))
        SqlCmd.Parameters.AddWithValue("@VHH_BSU_ID", Session("sBsuid"))
        SqlCmd.Parameters.AddWithValue("@VHH_STU_BSU_ID", Me.ddlBusinessunit.SelectedValue)
        SqlCmd.Parameters.AddWithValue("@VHH_FYEAR", Session("F_YEAR"))
        SqlCmd.Parameters.AddWithValue("@VHH_DOCTYPE", p_Doctype)

        If rbCash.Checked Then
            SqlCmd.Parameters.AddWithValue("@VHD_CHQID", System.DBNull.Value)
            SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", "1/jan/1900")
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtCashAcc.Text)
            SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", "")
        Else
            SqlCmd.Parameters.AddWithValue("@VHH_ACT_ID", txtBankCode.Text)
            If rbCheque.Checked = True Then
                SqlCmd.Parameters.AddWithValue("@VHD_CHQID", Convert.ToInt32(hCheqBook.Value))
                SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", Trim(txtChqdt.Text))
                SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", txtChqNo.Text)
            Else
                SqlCmd.Parameters.AddWithValue("@VHD_CHQID", 0)
                SqlCmd.Parameters.AddWithValue("@VHD_CHQDT", txtFrom.Text)
                SqlCmd.Parameters.AddWithValue("@VHD_CHQNO", txtrefChequeno.Text)
            End If
        End If

        SqlCmd.Parameters.AddWithValue("@VHH_CUR_ID", Session("BSU_CURRENCY"))
        SqlCmd.Parameters.AddWithValue("@VHH_NARRATION", p_Narration)
        SqlCmd.Parameters.AddWithValue("@AMOUNT", Convert.ToDecimal(txtAmount.Text))
        SqlCmd.Parameters.AddWithValue("@VHH_RECEIVEDBY", txPaidto.Text)

        SqlCmd.Parameters.AddWithValue("@VHH_PROV_ACT_ID", txtProvCode.Text)
        SqlCmd.Parameters.AddWithValue("@STU_ID", h_Student_no.Value)
        SqlCmd.Parameters.AddWithValue("@STU_NAME", txtStudentname.Text)
        SqlCmd.Parameters.AddWithValue("@VHH_DOCDT", p_Date)
        SqlCmd.Parameters.AddWithValue("@VHH_LOCK", Session("sUsr_name"))
        SqlCmd.Parameters.AddWithValue("@VHH_BBearer", ChkBearer.Checked)
        SqlCmd.Parameters.Add("@VHH_DOCNO", SqlDbType.VarChar, 20)
        SqlCmd.Parameters("@VHH_DOCNO").Direction = ParameterDirection.Output
        SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
        SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue


        SqlCmd.ExecuteNonQuery()
        If SqlCmd.Parameters("@ReturnValue").Value = 0 Then
            STR_DOCNO = CStr(SqlCmd.Parameters("@VHH_DOCNO").Value)
        End If
        Return SqlCmd.Parameters("@ReturnValue").Value
    End Function

    Protected Sub rbCheque_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCheque.CheckedChanged
        set_chequeno_controls()
    End Sub

    Protected Sub rbOthers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOthers.CheckedChanged
        set_chequeno_controls()
    End Sub

    Sub set_chequeno_controls()
        If rbOthers.Checked = True Then
            txtrefChequeno.Enabled = True
            txtChqBook.Text = ""
            txtChqNo.Text = ""
        Else
            txtrefChequeno.Enabled = False
            txtrefChequeno.Text = ""
        End If
    End Sub

    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        'FillACD()
        ClearStudentData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            '  lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim str_error As String = ""
        If Not IsDate(txtFrom.Text) Then
            str_error = str_error & "Invalid from date <br />"
        End If
        If h_Student_no.Value = "" Then
            str_error = str_error & "Please select student <br />"
        End If
        If h_Adjustment.Value = "" Then
            h_Adjustment.Value = 0
        End If
        If txtBankCode.Text = "" Then
            str_error = str_error & "Please select bank <br />"
        End If

        Dim txtAmt As New TextBox
        Dim dRefundAmount As Decimal = 0
        If gvRefund.Rows.Count > 1 And gvNetRefund.Rows.Count > 1 Then
            txtAmt = CType(gvRefund.Rows(gvRefund.Rows.Count - 1).FindControl("txtApprAmt"), TextBox)
            dRefundAmount = Convert.ToDecimal(txtAmt.Text)
            If dRefundAmount <= 0 Then
                str_error = str_error & "Invalid selection <br />"
            End If
        Else
            str_error = str_error & "Invalid selection <br />"
        End If

        If Not IsDate(txtChqdt.Text) Then
            str_error = str_error & "Invalid cheque date<br />"
        End If

        If Not IsNumeric(txtBankCharge.Text) Then
            txtBankCharge.Text = "0"
        End If

        If txtRemarks.Text = "" Then
            str_error = str_error & "Please enter remarks<br />"
        End If
        Dim STR_BankOrCash As String = String.Empty
        Dim STR_ACCOUNT As String = String.Empty
        If rbBank.Checked Then
            STR_BankOrCash = "B"
            If txtBankCode.Text = "" Then
                str_error = str_error & "Invalid bank<br />"
            Else
                STR_ACCOUNT = txtBankCode.Text
            End If
        Else
            STR_BankOrCash = "C"
            If txtCashAcc.Text = "" Then
                str_error = str_error & "Invalid cash account<br />"
            Else
                STR_ACCOUNT = txtCashAcc.Text
            End If
            'If AccountFunctions.CheckAccountBalance(txtCashAcc.Text.Trim, Session("sBsuid"), txtFrom.Text.Trim) < dRefundAmount Then
            '    str_error = str_error & "There is not Enough Balance in the Account"
            'End If
        End If

        If str_error <> "" Then
            'lblError.Text = str_error
            usrMessageBar.ShowNotification(str_error, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(txtFrom.Text)


        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        objConn.Open()

        Dim str_NEW_FRH_ID As String = ""
        Dim dDiffAmount As Decimal = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            retval = FeeCollectionService.F_SaveService_REFUND_H(0, Session("sBsuid"), ddlBusinessunit.SelectedValue, 0, H_STU_TYPE.Value, _
                h_Student_no.Value, h_Adjustment.Value, txtFrom.Text, txtChqdt.Text, False, str_NEW_FRH_ID, False, _
             txtRemarks.Text, "", STR_BankOrCash, STR_ACCOUNT, "", dRefundAmount, txtBankCharge.Text, stTrans)
            If retval = "0" Then
                For Each gvr As GridViewRow In gvRefund.Rows
                    Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtApprAmt"), TextBox)
                    Dim lblID As Label = CType(gvr.FindControl("lblID"), Label)
                    Dim dAmount As Decimal = Convert.ToDecimal(gvr.Cells(1).Text)
                    Dim aprAmt As Decimal = Convert.ToDecimal(txtAmountToPay.Text)
                    If dAmount < aprAmt Or aprAmt < 0 Then
                        retval = 940 'Approved Amount should not be greater than Actual
                        Exit For
                    End If
                    If Not lblID Is Nothing Then
                        If dAmount > 0 Then
                            If CInt(lblID.Text) < 1000 Then 'ID=1000 means total column
                                retval = FeeCollectionService.F_SaveSERVICE_REFUND_D(0, str_NEW_FRH_ID, aprAmt, _
                                lblID.Text, gvr.Cells(0).Text, stTrans)
                                dDiffAmount = dDiffAmount + dAmount - aprAmt
                            End If
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
            End If
            If dDiffAmount < CDec(txtBankCharge.Text) Or CDec(txtBankCharge.Text) < 0 Then
                retval = 941 'Please check bank charge
            End If
            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, str_NEW_FRH_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, "")
                '  lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Success)
                clear_All()
            Else
                stTrans.Rollback()
                '  lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            '  lblError.Text = getErrorMessage(1000)
            usrMessageBar.ShowNotification(getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Protected Sub gvRefund_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRefund.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(0).Text.ToUpper = "TOTAL" Then
                DirectCast(e.Row.FindControl("txtApprAmt"), TextBox).Enabled = False
            Else
                DirectCast(e.Row.FindControl("txtApprAmt"), TextBox).Enabled = True
            End If
        End If
    End Sub
    Protected Sub txtStudentname_TextChanged(sender As Object, e As EventArgs)
        setStudntData()
        gvRefund.DataBind()
        gvNetRefund.DataBind()
        Set_GridTotal()
        SetTCReference()
    End Sub
    
End Class
