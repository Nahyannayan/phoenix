﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="HifzTrackerAddEditForm.aspx.vb" Inherits="HifzTracker_AddEditHifzTrackerForm" %>

<%@ Import Namespace="System.Linq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title id="popupTitle" runat="server">Add Hif'z Data</title>
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
    <style>
        .error {
            font-family: Raleway,sans-serif;
            font-size: 7pt;
            font-weight: normal;
        }

        table td input.form-field, table td select.form-field {
            min-width: 65% !important;
            width: 70% !important;
        }

            table td input.form-field.numeric {
                min-width: 10% !important;
                width: 15% !important;
            }

        #divVerses {
            width: 34%;
            max-height: 91px;
        }
    </style>
    <!-- Bootstrap core JavaScript-->
    <script language="javascript" type="text/javascript" src="../vendor/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script language="javascript" type="text/javascript">


        function fancyClose() {
            window.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function CloseAddEditHifzTrackerFormModal(result) {
            setTimeout(function () {
                GetRadWindow().close(result); // Close the window 
            }, 0);
        }
    </script>
    <script language="javascript" type="text/javascript" src="/PHOENIXBETA/Scripts/AddEditHifzTrackerForm.js?1=2"></script>
</head>
<body onload="listen_window();">
    <form id="frmHifzTracker" runat="server">
        <asp:HiddenField ID="hdnTrackerId" runat="server" />
        <asp:HiddenField ID="hdnEmployeeId" runat="server" />
        <div class="card mb-3">
            <div class="card-body">
                <div id="divError" runat="server" class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> A problem has been occurred while submitting your data.
                </div>
                <div class="table-responsive">
                    <table id="tblHifzTracker" cellspacing="2" cellpadding="2" width="100%">
                        <tr id="trLabelError">
                            <td align="left" class="matters" valign="bottom" colspan="2">
                                <div id="lblError" runat="server">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120">Academic Year
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" CssClass="form-field" AutoComplete="off" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAcademicYear"
                                    Display="Dynamic" ErrorMessage="Academic Year required" ValidationGroup="SaveHifzValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120">Grade
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlGrade" runat="server" CssClass="form-field" AutoComplete="off">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlGrade"
                                    Display="Dynamic" ErrorMessage="Grade required" ValidationGroup="SaveHifzValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120">Name of Surah
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="form-field" AutoComplete="off" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtTitle"
                                    Display="Dynamic" ErrorMessage="Title required" ValidationGroup="SaveHifzValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120">URL
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtURL" runat="server" CssClass="form-field" AutoComplete="off" MaxLength="500"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtURL"
                                    Display="Dynamic" ErrorMessage="URL required" ValidationGroup="SaveHifzValidation" CssClass="error"
                                    ForeColor=""></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120">No. of Verses
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFromVerses" CssClass="form-field numeric" runat="server" Width="5%" AutoComplete="off" MaxLength="3"></asp:TextBox>
                                to
                                <asp:TextBox ID="txtToVerses" CssClass="form-field numeric" runat="server" Width="5%" AutoComplete="off" MaxLength="3"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdnNoOfVerses" />
                                <asp:ImageButton ID="imgAddVerses" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/edit_add.png"
                                    ToolTip="Add New" OnClientClick="addVerses();return false;" runat="server" />
                                <span id="lblVersesErrorMsg" runat="server" class="error" style="font-family: Raleway, sans-serif; font-size: 7pt; font-weight: normal; display: inline;"></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120"></td>
                            <td align="left">
                                <div id="divVerses" class="table-responsive">
                                    <table id="tbl-Verses" class="table table-bordered" cellspacing="2" cellpadding="2" width="100%">
                                        <asp:Literal ID="tblVersesHtml" runat="server" EnableViewState="false"></asp:Literal>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="120">Available Online
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkStatus" CssClass="form-field" runat="server"></asp:CheckBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <input type="button" class="button pull-left" id="btnCancel" title="Cancel" value="Cancel" onclick="fancyClose()" />
                <asp:Button ID="btnSaveHifzData" Text="Save" runat="server" CssClass="button" Style="float: right;" CausesValidation="true" ValidationGroup="SaveHifzValidation" OnClick="btnSaveHifzData_Click" />
            </div>
        </div>
    </form>
</body>
</html>
