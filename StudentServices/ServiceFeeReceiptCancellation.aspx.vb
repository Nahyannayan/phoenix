﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_ServiceFeeReceiptCancellation
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            trstudDet.Visible = False
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            txtDate.Text = Format(Now.Date, OASISConstants.DateFormat)
            ViewState("ID") = 1
            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "F733036" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            filldDDLBSU()
        End If
    End Sub
    Public Sub filldDDLBSU()
        ddBusinessunit.Items.Clear()
        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter

        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)
            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSU_ID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.Insert(0, New ListItem("Please Select", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try

    End Sub

    Function ValidateRecieptNo(ByVal recNo As String, ByVal vBSUID As String) As Boolean
        Dim CommandText As String = "select count(*) from [FEES].[FEECOLLECTION_H] where FCL_RECNO='" & _
        recNo & "' and FCL_STU_BSU_ID = '" & vBSUID & "'"
        Dim vCount As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, CommandText)
        If vCount > 0 Then
            Return True
        Else
            Return False
        End If
        Return False
    End Function

    'Function UpdateStudentDetails(ByVal recNo As String, ByVal vBSUID As String) As Boolean
    '    trstudDet.Visible = False
    '    Dim CommandText As String
    '    If rbEnrollment.Checked Then
    '        CommandText = "SELECT     VW_OSO_STUDENT_M.STU_NAME, FEES.FEECOLLECTION_H.FCL_AMOUNT " & _
    '        " FROM FEES.FEECOLLECTION_H LEFT OUTER JOIN  VW_OSO_STUDENT_M ON " & _
    '        " FEES.FEECOLLECTION_H.FCL_STU_ID = VW_OSO_STUDENT_M.STU_ID where FCL_RECNO='" & _
    '        recNo & "' and FCL_STU_BSU_ID = '" & vBSUID & "'"
    '    Else
    '        CommandText = "SELECT     FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, FEES.FEECOLLECTION_H.FCL_AMOUNT " & _
    '          " FROM FEES.FEECOLLECTION_H LEFT OUTER JOIN  FEES.vw_OSO_ENQUIRY_COMP ON " & _
    '          " FEES.FEECOLLECTION_H.FCL_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID where FCL_RECNO='" & _
    '          recNo & "' and FCL_STU_BSU_ID = '" & vBSUID & "'"
    '    End If

    '    Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, CommandText)
    '    While (drReader.Read())
    '        lblStudentName.Text = drReader("STU_NAME")
    '        lblAmount.Text = Format(drReader("FCL_AMOUNT"), "0.00")
    '        trstudDet.Visible = True
    '    End While

    'End Function

    Private Function ChechErrors() As Boolean
        Try
            If CDate(txtDate.Text) > Date.Now Then
                'lblError.Text = "Future Date is not allowed"
                usrMessageBar.ShowNotification("Future Date is not allowed", UserControls_usrMessageBar.WarningType.Danger)
                Return False
            End If
        Catch
            'lblError.Text = "Date is not valid"
            usrMessageBar.ShowNotification("Date is not valid", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End Try
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not ChechErrors() Then Return


        If Not ValidateRecieptNo(txtRecieptNo.Text, ddBusinessunit.SelectedValue) Then
            'lblError.Text = "Receipt No is not Valid..."
            usrMessageBar.ShowNotification("Receipt No is not Valid...", UserControls_usrMessageBar.WarningType.Danger)
            Return
        End If
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        Dim trans As SqlTransaction
        Try
            conn.Open()
            trans = conn.BeginTransaction("DeleteServiceFeeReceiptCancel")

            Dim cmd As New SqlCommand("FEES.DeleteReceipt_OASIS", conn, trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpAUD_BSU_ID As New SqlParameter("@AUD_BSU_ID", SqlDbType.VarChar, 20)
            sqlpAUD_BSU_ID.Value = ddBusinessunit.SelectedItem.Value
            cmd.Parameters.Add(sqlpAUD_BSU_ID)

            Dim sqlpProviderAUD_BSU_ID As New SqlParameter("@AUD_PROVIDER_BSU_ID", SqlDbType.VarChar, 20)
            sqlpProviderAUD_BSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpProviderAUD_BSU_ID)

            Dim sqlpFCL_RECNO As New SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            sqlpFCL_RECNO.Value = txtRecieptNo.Text
            cmd.Parameters.Add(sqlpFCL_RECNO)


            Dim sqlpAUD_USER As New SqlParameter("@AUD_USER", SqlDbType.VarChar, 50)
            sqlpAUD_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpAUD_USER)

            Dim sqlpAUD_REMARKS As New SqlParameter("@AUD_REMARKS", SqlDbType.VarChar, 100)
            sqlpAUD_REMARKS.Value = txtRemarks.Text
            cmd.Parameters.Add(sqlpAUD_REMARKS)

            Dim sqlpFromDT As New SqlParameter("@DT", SqlDbType.DateTime)
            sqlpFromDT.Value = CDate(txtDate.Text)
            cmd.Parameters.Add(sqlpFromDT)

            Dim retSValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.VarChar)
            retSValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retSValParam)

            cmd.ExecuteNonQuery()
            Dim iReturnvalue As Integer = retSValParam.Value
            If iReturnvalue <> 0 Then
                trans.Rollback()
                'lblError.Text = "Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue)
                usrMessageBar.ShowNotification("Error occured while deleting : " & UtilityObj.getErrorMessage(iReturnvalue), UserControls_usrMessageBar.WarningType.Danger)
            Else
                trans.Commit()
                'lblError.Text = "Receipt deleted Successfully.."
                usrMessageBar.ShowNotification("Receipt deleted Successfully..", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
            End If
        Catch ex As Exception
            'lblError.Text = "Error occured while deleting  " & ex.Message
            usrMessageBar.ShowNotification("Error occured while deleting  " & ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            trans.Rollback()
        Finally
            conn.Close()
        End Try

    End Sub

    Private Sub ClearAll()
        lblAmount.Text = ""
        lblStudentName.Text = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtRecieptNo.Text = ""
        txtRemarks.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Protected Sub txtRecieptNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRecieptNo.TextChanged
        'UpdateStudentDetails(txtRecieptNo.Text, ddBusinessunit.SelectedValue)
    End Sub
    Protected Sub imgDocno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'UpdateStudentDetails(txtRecieptNo.Text, ddBusinessunit.SelectedValue)
    End Sub
    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        txtRecieptNo.Text = ""
    End Sub
End Class

