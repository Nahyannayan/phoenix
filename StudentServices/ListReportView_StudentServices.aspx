﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ListReportView_StudentServices.aspx.vb" Inherits="StudentServices_ListReportView_StudentServices" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
  
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>--%>
    <style>
        input {
         vertical-align :middle !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ajaxToolkit_CalendarExtenderClientShowing(e) {
            if (!e.get_selectedDate() || !e.get_element().value)
                e._selectedDate = (new Date()).getDateOnly();
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkAccept')
                            {
                                document.forms[0].elements[i].checked = chk_state;
                            }                            
                        }
                    }
                }
            }
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Create Student Activities
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" 
                    height="0">
                    <tr id="ACD_Row" runat="server">
                        <td width="20%"><span class="field-label">Academic Year</span>  </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlACDYear" Width="30%" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlACDYear_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr valign="top">
                        <td valign="bottom"  align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                        <td></td>
                        <td align="right">
                            <table style="margin-left: 0; >
                                <tr>
                                    <td width="0" height="0px">
                                        <asp:CheckBox ID="chkSelection1" runat="server" AutoPostBack="True" Visible="False" CssClass="field-label" />
                                    </td>
                                    <td width="0" height="0px">
                                        <asp:CheckBox ID="chkSelection2" runat="server" AutoPostBack="True" CssClass="field-label"  />
                                    </td>
                                    <td height="0px">
                                    </td>
                                    <td width="0" height="0px">
                                        <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" CssClass="field-label" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False"  CssClass="field-label" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" CssClass="field-label"  />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad4" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" CssClass="field-label"  />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad5" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" CssClass="field-label"  />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad6" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" CssClass="field-label"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td visible="false" runat="server" id="dateCol" align="right">
                            <asp:Label ID="lblDate" runat="server" Text="Expiry As on Date:" CssClass="field-label"></asp:Label>
                            <asp:TextBox ID="txtDate" runat="server" CssClass="inputbox" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" Width="16px"></asp:ImageButton>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDate" PopupButtonID="lnkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <th align="right" valign="middle" width="70%">
                            <asp:Label ID="lblTitle" runat="server" Style="display:none;"></asp:Label>
                            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True" DataSourceID="namesXML" Visible="false"
                                DataTextField="FilterText" DataValueField="FilterValue" SkinID="DropDownListNormal">
                            </asp:DropDownList><asp:XmlDataSource ID="namesXML" runat="server" DataFile="~/XMLData/XMLTopFilter.xml"
                                XPath="FilterElements/FiletrList"></asp:XmlDataSource>
                        </th>
                        <th align="right" valign="middle" style="text-align: right" width="30%">
                            <asp:LinkButton ID="btnPrint" runat="server" Visible="False">Print</asp:LinkButton>
                            &nbsp;
                <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink>
                        </th>
                    </tr>
                    <tr>
                        <td align="center" valign="top" width="100%" colspan="2">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="table table-bordered table-row" AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col1"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol1" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col2"></asp:Label><br />
                                                        <asp:TextBox ID="txtCol2" runat="server" Width="70%"></asp:TextBox>
                                                         <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col3"></asp:Label>
                                                        <br />
                                                         <asp:TextBox ID="txtCol3" runat="server" Width="70%"></asp:TextBox>
                                                         <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col4"></asp:Label>
                                                        <br />
                                                         <asp:TextBox ID="txtCol4" runat="server" Width="70%"></asp:TextBox>
                                                         <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                          <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col5"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol5" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                         <asp:Label ID="lblHeaderCol6" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col6"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol6" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col7" SortExpression="Col7">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol7" runat="server" Text='<%# Bind("Col7") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                          <asp:Label ID="lblHeaderCol7" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col7"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol7" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol8" runat="server" Text='<%# Bind("Col8") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <asp:Label ID="lblHeaderCol8" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col8"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol8" runat="server" Width="70%"></asp:TextBox>
                                                         <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col9" SortExpression="Col9">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol9" runat="server" Text='<%# Bind("Col9") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <asp:Label ID="lblHeaderCol9" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col9"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol9" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col10" SortExpression="Col10">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol10" runat="server" Text='<%# Bind("Col10") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol10" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col10"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol10" runat="server" Width="70%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="hlVwAndApprv" runat="server" Text="View And Approve"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:CheckBox ID="chkAccept" runat="server" Visible="False" />
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                            Text="Select All" Visible="False" />
                                    </td>
                                    <td align="center" width="60%">
                                        <input type="button" id="ListButton1" class="button" style="color: White; background-color: #1B80B6;  font-weight: bold;" runat="server" onclick="this.disabled = true;" onserverclick="ListButton1_Click" />
                                        <asp:Button ID="ListButton2" runat="server" CssClass="button" CausesValidation="False"
                                            Width="142px" OnClick="ListButton2_Click" Visible="True"></asp:Button>
                                    </td>
                                    <td width="20%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>

                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />

            </div>
        </div>
    </div>
    
    <script type="text/javascript" language="javascript">

        function ShowWindowWithClose(gotourl, pageTitle, w, h) {

            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                },
                afterClose: function () {
                    window.location.reload();

                }
            });
            return false;
        }
    </script>
</asp:Content>

