﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeReceiptECA.aspx.vb" Inherits="StudentServices_FeeReceiptECA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Receipt</title>
    <base target="_self" />
    <style type="text/css">
        img {
            display: block;
        }

        .ReceiptCaption {
            FONT-WEIGHT: bold;
            FONT-SIZE: 10pt;
            COLOR: #000000;
            FONT-FAMILY: Verdana;
            HEIGHT: 19px;
        }

        .matters_print {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #000000;
        }

        .matters_heading {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 13px;
            font-weight: bold;
            color: #000000;
        }

        .matters_normal {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000000;
        }

        .matters_grid {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000000;
            TEXT-INDENT: 20px;
        }

        .matters_small {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000000;
        }

        .Printbg {
            vertical-align: middle;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000000;
        }

        .PrintSource {
            vertical-align: bottom;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000000;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PrintReceiptExport() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
            try {
                if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                    var ieversion = new Number(RegExp.$1) // capture x.x portion and store as a number
                    if (ieversion >= 8)
                        window.close();
                }
            }
            catch (ex) { }
        }
        function PrintReceipt() {
            if ('<%= Request.QueryString("isexport") %>' == '0')
                PrintReceiptExport();
        }
        function OnEscape() {
            if ((event.keyCode == 27) || (event.keyCode == 99) || (event.keyCode == 120))
                window.close();
        }
    </script>
</head>
<body onload="PrintReceipt();" onkeypress="OnEscape()" onkeydown="OnEscape()">
    <form id="form1" runat="server">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
            <tr valign="top" id="tr_Print">
                <td align="right">
                    <img src="../Images/Misc/print.gif" alt="../Images/Misc/print.gif" onclick="return PrintReceiptExport();" style="cursor: hand" /></td>
            </tr>
            <tr valign="top">
                <td colspan="2" align="center" style="border-bottom: #000000 2pt solid">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" style="width:auto;" rowspan="5">
                                <asp:Image ID="imgLogo" runat="server" />
                            </td>
                            <td class="matters_heading" align="center">
                                <asp:Label ID="lblProvider" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trAddress" runat="server" visible="false">
                            <td class="matters_print" align="center">
                                <asp:Label ID="lblAddress" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="matters_print" align="center">
                                <asp:Label ID="lblHeader1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters_print" align="center">
                                <asp:Label ID="lblHeader2" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="matters_print" align="center">
                                <asp:Label ID="lblHeader3" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td align="center" colspan="1" style="height: 90px"></td>
            </tr>
            <tr>
                <td height="20px">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <table align="center" border="0" cellspacing="0" cellpadding="2">
                        <tr>
                            <td align="center" class="ReceiptCaption">Receipt
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table cellpadding="3" width="100%" border="0"
                                    style="border: 1px solid #000000;">
                                    <tr>
                                        <td align="left" class="matters_normal" width="110">Receipt No</td>
                                        <td align="center" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_print">
                                            <asp:Label ID="lblRecno" runat="server"></asp:Label>
                                        </td>
                                        <td align="right" class="matters_normal" width="50px">&nbsp; &nbsp;Date</td>
                                        <td align="left" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_normal" width="70">
                                            <asp:Label ID="lblDate" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr id="trSchool" runat="server">
                                        <td align="left" class="matters_normal" width="110">School</td>
                                        <td align="center" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_print" colspan="4">
                                            <asp:Label ID="lblStuShoolName" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters_normal">Student ID</td>
                                        <td align="center" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_print">
                                            <asp:Label ID="lblStudentNo" runat="server"></asp:Label></td>
                                        <td align="left" class="matters_normal" style="display: none">Grade</td>
                                        <td align="left" class="matters_print" style="display: none">:</td>
                                        <td align="left" class="matters_normal">
                                            <asp:Label ID="lblGrade" runat="server"></asp:Label></td>
                                    </tr>

                                    <tr>
                                        <td align="left" class="matters_normal">Name</td>
                                        <td align="center" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_print" colspan="4">
                                            <asp:Label ID="lblStudentName" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr style="display: none">
                                        <td align="left" class="matters_normal">Area</td>
                                        <td align="center" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_print">
                                            <asp:Label ID="lblArea" runat="server"></asp:Label></td>
                                        <td align="right" class="matters_normal">Bus No</td>
                                        <td align="left" class="matters_print" style="width: 1px;">:</td>
                                        <td align="left" class="matters_normal">
                                            <asp:Label ID="lblBusno" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="ReceiptCaption">
                            <td style="height: 19px" align="left">Fee Details</td>
                        </tr>
                        <tr>
                            <td align="center" class="matters_grid" valign="top">
                                <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" BorderColor="#000000" BorderWidth="1pt" CellPadding="3" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Received ECA Fee">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FCS_AMOUNT" HeaderText="Amount" DataFormatString="{0:###,###,###,##0.00}">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="matters_print">
                                <asp:Label ID="lblBalance" runat="server"></asp:Label>&nbsp;</td>
                        </tr>
                        <tr class="ReceiptCaption">
                            <td align="left" class="matters_print">Payment Details</td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal">
                                <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False"
                                    CellPadding="4" BorderWidth="1pt" Width="600px" ShowFooter="True">
                                    <Columns>
                                        <asp:BoundField DataField="CLT_DESCR" HeaderText="Payment Mode">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CRI_DESCR" HeaderText="Bank/Credit Card"></asp:BoundField>
                                        <asp:BoundField DataField="FCD_REFNO" HeaderText="Cheque No./Auth Code" />
                                        <asp:BoundField DataField="FCD_DATE" HeaderText="Cheque Date" />
                                        <asp:BoundField DataField="FCD_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                            HeaderText="Amount">
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;"
                                class="matters_normal">
                                <asp:Label ID="lblNarration" runat="server" Visible="True"></asp:Label>
                                <br />
                                <asp:Label ID="lblMessageGMS" runat="server" CssClass="matters_print"></asp:Label><br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;"
                                class="matters_normal">
                                <asp:Label ID="lblProvidername" runat="server"></asp:Label><br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;"
                                class="matters_normal">------------------------------</td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 30px; height: 20px"
                                class="matters_normal">Cashier/Accountant</td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px; height: 25px"
                                class="matters_normal">Payment by Cheque(s) are subject to realization.</td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px; height: 25px"
                                class="matters_normal">Cheques to be drawn in favour of &#39;<asp:Label ID="lblSchoolNameCheque" runat="server"></asp:Label>
                                &nbsp;&#39; </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="height: 25px; padding: 9px"
                                class="matters_normal">Please retain this receipt for refund if any.<br />
                                <br />
                                In case of any fee refunds for payments made through Credit card(s),  applicable bank charges will be deducted while processing such claims</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" style="text-indent: 10px; height: 25px;"
                                class="matters_normal">
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <asp:Label ID="lblProvidernameFavour"
                                    runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" style="text-indent: 10px; height: 25px"
                                class="matters_normal">We value your patronage</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="Printbg" valign="middle">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                        <tr>
                            <td align="center" class="PrintSource" valign="top">&nbsp;</td>
                            <td valign="top" align="right">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" valign="bottom" class="matters_normal">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-top: #003A63 1px solid; width: 100%; height: 100%">
                        <tr>
                            <td align="left" class="PrintSource" valign="top">
                                <asp:Label ID="lbluserloggedin" runat="server" CssClass="matters_small"></asp:Label></td>
                            <td valign="top" align="right" class="PrintSource">
                                <asp:Label ID="lblPrintTime" runat="server" CssClass="matters_small"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="h_print" runat="server" />
    </form>
</body>
</html>
