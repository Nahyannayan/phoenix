﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Imports System.Windows.Forms.MessageBox
Partial Class StudentServices_ListReportView_StudentServices
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Latest_refid As Integer = 0
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property NoViewAndApproveAllowed() As Boolean
        Get
            Return ViewState("NoViewAndApproveAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAndApproveAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Private Property AllRecordsIDs() As String
        Get
            Return ViewState("AllRecordsIDs")
        End Get
        Set(ByVal value As String)
            ViewState("AllRecordsIDs") = value
        End Set
    End Property
    Private Property FilterText1() As String
        Get
            Return chkSelection1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection1.Text = value
                chkSelection1.Visible = True
            Else
                chkSelection1.Visible = False
                chkSelection1.Text = ""
            End If
        End Set
    End Property
    Private Property FilterText2() As String
        Get
            Return chkSelection2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection2.Text = value
                chkSelection2.Visible = True
            Else
                chkSelection2.Visible = False
                chkSelection2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox1Text() As String
        Get
            Return rad1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad1.Text = value
                rad1.Visible = True
            Else
                rad1.Visible = False
                rad1.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBoxAcceptText() As String
        Get
            Return chkAccept.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkAccept.Text = value
                chkAccept.Visible = True
            Else
                chkAccept.Visible = False
                chkAccept.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox2Text() As String
        Get
            Return rad2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad2.Text = value
                rad2.Visible = True
            Else
                rad2.Visible = False
                rad2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox3Text() As String
        Get
            Return rad3.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad3.Text = value
                rad3.Visible = True
            Else
                rad3.Visible = False
                rad3.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox4Text() As String
        Get
            Return rad4.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad4.Text = value
                rad4.Visible = True
            Else
                rad4.Visible = False
                rad4.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox5Text() As String
        Get
            Return rad5.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad5.Text = value
                rad5.Visible = True
            Else
                rad5.Visible = False
                rad5.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox6Text() As String
        Get
            Return rad6.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad6.Text = value
                rad6.Visible = True
            Else
                rad6.Visible = False
                rad6.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText1() As String
        Get
            Return ListButton1.Value
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton1.Value = value
                ListButton1.Visible = True
            Else
                ListButton1.Visible = False
                ListButton1.Value = ""
            End If
        End Set
    End Property   
    Private Property ButtonText2() As String
        Get
            Return ListButton2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton2.Text = value
                ListButton2.Visible = True
            Else
                ListButton2.Visible = False
                ListButton2.Text = ""
            End If
        End Set
    End Property
    Private Property ACD_DropdownList As Boolean
        Get
            Return ViewState("ACD_Year")
        End Get
        Set(value As Boolean)
            ViewState("ACD_Year") = value
        End Set
    End Property
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Try
    '        If Session("sModule") = "SS" Then
    '            Me.MasterPageFile = "../mainMasterPageSS.master"
    '        Else
    '            Me.MasterPageFile = "../mainMasterPage.master"
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog("From Page_PreInit: " + ex.Message)
    '    End Try
    'End Sub
    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"

                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    lblError.Text = msgText
                End If
                ShowGridCheckBox = False
                NoAddingAllowed = False
                NoViewAllowed = False
                HideExportButton = False
                IsStoredProcedure = False
                ButtonText1 = ""
                ButtonText2 = ""
                rad1.Checked = True
                CheckBox1Text = ""
                CheckBox2Text = ""
                CheckBox3Text = ""
                CheckBox4Text = ""
                CheckBox5Text = ""
                CheckBox6Text = ""
                FilterText1 = ""
                FilterText2 = ""
                lblError.Text = ""
                fillTranAcdY()
                BuildListView()
                gridbind(False)
                btnExport.Visible = Not HideExportButton
                If NoAddingAllowed Then
                    hlAddNew.Visible = False
                Else
                    hlAddNew.Visible = True
                    Dim url As String
                    url = EditPagePath & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = url
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("From Load: " + ex.Message + "LIST-STUDENT SERVICES")
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
                UtilityObj.Errorlog("From setID: " + ex.Message)
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
                UtilityObj.Errorlog("From getid: " + ex.Message)
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
            UtilityObj.Errorlog("From getColumnFilterString: " + ex.Message)
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog("From SetHeaderFilderString: " + ex.Message + "LIST-STUDENT SERVICES")
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean, Optional ByVal ForceResetColumns As Boolean = False)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            FilterTextboxString = ""
            ACD_Row.Visible = ACD_DropdownList
            If gvDetails.Rows.Count > 0 And Not ForceResetColumns Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If
            If ddlFilter.Visible Then
                If ddlFilter.SelectedItem Is Nothing Then
                    SQLSelect = SQLSelect.Replace("~", " top 10 ")
                ElseIf ddlFilter.SelectedItem.Value = "All" Then
                    SQLSelect = SQLSelect.Replace("~", " top 99999999 ")
                Else
                    SQLSelect = SQLSelect.Replace("~", " top " & ddlFilter.SelectedItem.Value)
                End If
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                Session("GridData") = GridData
                str_Filter = " 1 = 1 " & str_Filter
                GridData.Select(str_Filter)
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridData.Copy
                mySelectDT.Rows.Clear()
                For Each mRow In GridData.Select(str_Filter)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If

            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy

            Dim dataView As New DataView(bindDT)
            If bindDT.Columns.Contains("REFID") Then
                dataView.Sort = "REFID DESC"
            Else

            End If
            bindDT = dataView.ToTable()

            Try
                Dim mRow As DataRow
                AllRecordsIDs = ""
                If myData.Columns.Count > 0 Then
                    For Each mRow In myData.Rows
                        AllRecordsIDs &= IIf(AllRecordsIDs <> "", "|", "") & mRow(0).ToString
                    Next
                End If
            Catch ex As Exception
                UtilityObj.Errorlog("From gridbind: " + ex.Message)
            End Try
            If myData.Columns.Count > 0 Then
                myData.Columns.RemoveAt(0)
            End If
            ViewState("GridTable") = GridData
            Session("myData") = myData
            SetExportFileLink()
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 10 Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If
            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(11).Visible = Not NoAddingAllowed
            gvDetails.Columns(1).Visible = False
            gvDetails.Columns(11).Visible = Not NoViewAllowed
            gvDetails.Columns(12).Visible = Not NoViewAndApproveAllowed

          

            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = getErrorMessage("4011")
                ButtonText2 = ""
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog("From gridbind: " + ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblItemCol1"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            Dim hlVwAndApprv As New LinkButton
            hlVwAndApprv = TryCast(e.Row.FindControl("hlVwAndApprv"), LinkButton)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            If hlVwAndApprv IsNot Nothing Then
                Select Case MainMenuCode
                    Case "S103301"
                        hlVwAndApprv.Attributes.Add("OnClick", "return ShowWindowWithClose('../StudentServices/Services_ApproveActivtyPrincipal.aspx?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "', 'Old Activity List', '80%', '80%');")
                        If (Encr_decrData.Decrypt(ViewState("datamode")) = "view") Then
                            hlVwAndApprv.Text = "View"
                        ElseIf (Encr_decrData.Decrypt(ViewState("datamode")) = "approve") Then
                            hlVwAndApprv.Text = "View And Approve"
                        End If
                End Select

            End If

            Try
                Dim FloatColIndexArr(), i, ControlName As String
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("####0.00")
                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception
                UtilityObj.Errorlog("From RowDataBound: " + ex.Message)
            End Try
        Catch ex As Exception
            UtilityObj.Errorlog("From RowDataBound: " + ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        RememberOldValues()
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
        SetChk(Me.Page)
        RePopulateValues()

    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt(lblTitle.Text)
        Catch ex As Exception
            UtilityObj.Errorlog("From SetExportFileLink: " + ex.Message)
        End Try
    End Sub
    Protected Sub rad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad1.CheckedChanged, rad2.CheckedChanged, rad3.CheckedChanged, rad4.CheckedChanged, rad5.CheckedChanged, rad6.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From rad_CheckedChanged: " + ex.Message)
        End Try
    End Sub
    Protected Sub chkSelection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelection1.CheckedChanged, chkSelection2.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From chkSelection_CheckedChanged: " + ex.Message)
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From btnPrint_Click: " + ex.Message)
        End Try
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim MySelectedIDS() As String
            Dim itmIdList As New ArrayList()
            If chkSelectAll.Checked Then
                MySelectedIDS = AllRecordsIDs.Split("|")
                If MySelectedIDS.Length > 0 Then
                    Dim MySelectedID As String
                    For Each MySelectedID In MySelectedIDS
                        If Not itmIdList.Contains(MySelectedID) Then
                            itmIdList.Add(MySelectedID)
                        End If
                    Next
                End If
            End If
            ViewState("checked_items") = itmIdList
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From chkSelectAll_CheckedChanged: " + ex.Message)
        End Try

    End Sub

    Protected Sub ListButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkControl As New HtmlInputCheckBox
        Dim chkControl1 As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""

        Dim lblID1 As Label
        Dim IDs1 As String = ""

        Dim SelectedIds As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl1 = grow.FindControl("chkControl")
            If chkControl1 IsNot Nothing Then
                If chkControl1.Checked Then
                    lblID1 = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs1 &= IIf(IDs <> "", "|", "") & lblID1.Text
                End If
            End If
        Next

        If IDs1 <> "" Then
            RememberOldValues()
        End If


        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If

        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    If Not ("|" & SelectedIds.ToString & "|").ToString.Contains("|" & lblID.Text.ToString & "|") Then
                        SelectedIds &= IIf(SelectedIds <> "", "|", "") & lblID.Text
                    End If
                End If
            End If
        Next
        If IDs = "" And SelectedIds = "" Then
            lblError.Text = getErrorMessage("4010")
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "F300172"
                'PrintBarcode(IDs)

        End Select
    End Sub
    Sub fillTranAcdY()
        Try
            Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
            ddlACDYear.DataSource = dtACD
            ddlACDYear.DataTextField = "ACY_DESCR"
            ddlACDYear.DataValueField = "ACD_ID"
            ddlACDYear.DataBind()
            For Each rowACD As DataRow In dtACD.Rows
                If rowACD("ACD_CURRENT") Then
                    ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog("From fillTranAcdY :" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub ddlACDYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            'Message = getErrorMessage("4000")
            'ShowMessage(Message, True)
            UtilityObj.Errorlog("From ddlACDYear_SelectedIndexChanged: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub ListButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
            End If
        Next
        Dim SelectedIds As String = ""
        RememberOldValues()
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If
        If IDs = "" Then
            lblError.Text = getErrorMessage("4010")
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "S103301"

                'IDs &= IIf(IDs <> "", "|", "") & SelectedIds
                If chkAccept.Checked Then
                    Dim EmployeeID As String = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    Dim ApprovedMessage As String = getErrorMessage("4009")
                    Dim Params(2) As SqlParameter
                    Dim strans As SqlTransaction = Nothing
                    Dim st_conn = ConnectionManger.GetOASISConnectionString
                    Dim objConn As New SqlConnection(st_conn)
                    Try
                        objConn.Open()
                        strans = objConn.BeginTransaction
                        Params(0) = Mainclass.CreateSqlParameter("@COMMENTS", ApprovedMessage, SqlDbType.VarChar)
                        Params(1) = Mainclass.CreateSqlParameter("@USER", EmployeeID, SqlDbType.VarChar)
                        Params(2) = Mainclass.CreateSqlParameter("@VIEWDs", IDs, SqlDbType.VarChar)
                        SqlHelper.ExecuteNonQuery(strans, "[dbo].[UPDATE_CHKALL_ACTIVITY_WITH_PRNCPL_APPRVL]", Params)
                        strans.Commit()
                        Session("ButtonSave") = True
                        BuildListView()
                        CreateNotificationEntry(IDs)
                        gridbind(False)
                    Catch EX As Exception
                        strans.Rollback()
                        UtilityObj.Errorlog("From ListButton2_Click-S103301: " + EX.Message)
                    End Try
                    'Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
                Else
                    lblError.Text = getErrorMessage("4041")
                End If

            Case "S104301"
                Try
                    'IDs &= IIf(IDs <> "", "|", "") & SelectedIds
                    Dim ApprovedMessage As String = getErrorMessage("4009")
                    Dim Params(2) As SqlParameter
                    Dim strans As SqlTransaction = Nothing
                    Dim st_conn = ConnectionManger.GetOASISConnectionString
                    Dim objConn As New SqlConnection(st_conn)
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    Params(0) = Mainclass.CreateSqlParameter("@COMMENTS", ApprovedMessage, SqlDbType.VarChar)
                    Params(1) = Mainclass.CreateSqlParameter("@STATUS", "A", SqlDbType.VarChar)
                    Params(2) = Mainclass.CreateSqlParameter("@VIEWDs", IDs, SqlDbType.VarChar)
                    SqlHelper.ExecuteNonQuery(strans, "[dbo].[UPDATE_CHKALL_DETAILS_PENDING_ACT_REQUEST_ON_APPROVAL]", Params)
                    strans.Commit()
                    Session("ButtonSave") = True
                    Try
                        Dim IdsArray() As String = (IDs.Split("|"))
                        For Each uniId In IdsArray
                            SendNotificationEmail("A", Convert.ToInt64(uniId))
                        Next
                    Catch ex As Exception
                        UtilityObj.Errorlog("From ListButton2_Click-S104301: " + ex.Message)
                    End Try
                    BuildListView()
                    gridbind(False)
                    'Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
                Catch EX As Exception
                    UtilityObj.Errorlog("From ListButton2_Click-S104301: " + EX.Message)
                End Try
        End Select
    End Sub
    Protected Sub CreateNotificationEntry(ByVal IDs As String)
        Dim Today As DateTime = DateTime.Today
        Dim NotificationDateTime As DateTime
        Dim strans As SqlTransaction = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParams(5) As SqlParameter
        Dim Params(5) As SqlParameter
        Dim qry1 = "[DBO].[INSERT_NOTIFICATIONLIST_ONCREATION]"
        Dim qry2 = "[DBO].[INSERT_NOTIFICATIONGRADES_ONCREATION]"
        Dim GET_SELECTED_SECTIONS As String = ""
        Dim GET_SELECTED_GRADES As String = ""
        Dim GET_SELECTED_SEC_DESCR As String = ""
        Dim dt As DataTable = Session("GridData")
        Try
            Dim IDArray = IDs.Split("|")
            For Each id As String In IDArray
                Dim EmployeeID As String = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                Dim foundRows() As Data.DataRow = dt.Select("REFID = " & id & "")
                Dim EventName As String = foundRows(0)("EVENT_NAME")
                Dim ActivityContent As String = "Dear Parent, New activity" + """" + EventName + """" + "is added to your student group, to check more details please click Activities tile on the home page, Thanks. "
                NotificationDateTime = DateTime.Today
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.Int)
                pParams(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
                pParams(2) = Mainclass.CreateSqlParameter("@NOTIFICATION_CONTENT", ActivityContent, SqlDbType.VarChar)
                pParams(3) = Mainclass.CreateSqlParameter("@NOTIFICATION_DATETIME", NotificationDateTime, SqlDbType.DateTime)
                pParams(4) = Mainclass.CreateSqlParameter("@CREATOR", EmployeeID, SqlDbType.VarChar)
                Latest_refid = SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry1, pParams)
                Dim Paramss(0) As SqlParameter
                Paramss(0) = Mainclass.CreateSqlParameter("@VIEWID", id, SqlDbType.BigInt)
                Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_SELECTED_ACTIVITY_ GRADES_TO_NOTIFICATION]", Paramss)
                For Each row In DS.Tables(0).Rows
                    Params(0) = Mainclass.CreateSqlParameter("@REF_ID", Latest_refid, SqlDbType.Int)
                    Params(1) = Mainclass.CreateSqlParameter("@GRD_ID", row("AGD_GRADE_ID"), SqlDbType.VarChar)
                    Params(2) = Mainclass.CreateSqlParameter("@SCT_ID", row("AGD_SCT_ID"), SqlDbType.VarChar)
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
                Next
                strans.Commit()
            Next
        Catch ex As Exception
            strans.Rollback()
        End Try
    End Sub
    Protected Sub SendNotificationEmail(ByVal Status As String, ByVal ViewId As Integer)
        Dim tomail As String = String.Empty
        Dim EventName As String = String.Empty
        Dim subject As String = String.Empty
        Dim mailbody As String = String.Empty

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@REF_ID", ViewId, SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[VIEW_DETAILS_PENDING_ACT_REQUEST_ON_APPROVAL]", Param)
            Dim dt As DataTable = DS.Tables(0)
            If Not dt.Rows(0)("STS_FEMAIL") Is Nothing Or Not dt.Rows(0)("STS_FEMAIL") = "" Then
                tomail = dt.Rows(0)("STS_FEMAIL")
            Else
                tomail = ""
            End If
            If Not dt.Rows(0)("ALD_EMAIL_TEMPLATE") Is Nothing Or Not dt.Rows(0)("ALD_EMAIL_TEMPLATE") = "" Then
                mailbody = dt.Rows(0)("ALD_EMAIL_TEMPLATE")
            Else
                mailbody = ""
            End If
            If Not dt.Rows(0)("EVENT_NAME") Is Nothing Or Not dt.Rows(0)("EVENT_NAME") = "" Then
                EventName = dt.Rows(0)("EVENT_NAME")

            Else
                EventName = ""

            End If
            If Mainclass.isEmail(tomail) Then
                If (Status = "A") Then
                    subject = "Mail Notification : Your Activity Request for *" + EventName + "* Has been APPROVED"
                End If
                'Inserting into table [COM_EMAIL_SCHEDULE] for sending the email 
                InsertintoMailTable(subject, mailbody, tomail)
            Else
                'Dim ErrorMessage As String = "Error: Email Id Is Not Valid"
                Dim Message As String = getErrorMessage("4005")
                UtilityObj.Errorlog(Message, "OASIS ACTIVITY SERVICES")
            End If
        Catch EX As Exception
            UtilityObj.Errorlog("From Sendnotificationemail: " + EX.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub InsertintoMailTable(ByVal subject As String, ByVal message As String, ByVal tomailid As String)
        Try
            Dim str_conn = "OASISConnectionString"
            Dim query As String = "[dbo].[InsertIntoEmailSendSchedule] '" & Session("sBsuid") & "','ActivityRequestStatusMail','SYSTEM','" & tomailid & "','" & subject & "','" & message & "'"
            Mainclass.getDataValue(query, str_conn)
        Catch ex As Exception
            UtilityObj.Errorlog("From InsertintoMailTable: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Sub BuildListView()
        Try
            Dim strConn, StrSortCol As String
            Dim StrSQL As New StringBuilder
            strConn = "" : StrSortCol = ""
            StrSQL.Append("")
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case MainMenuCode
                Case "S108915"
                    NoViewAndApproveAllowed = True
                    ACD_DropdownList = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    EditPagePath = "../StudentServices/Services_CreateActivityList.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = ""
                    HeaderTitle = "Create Student Activities"
                    IsStoredProcedure = True
                    StrSQL.Append("[dbo].[GET_PENDING_ACT_FORCREATOR]")
                    ReDim SPParam(3)
                    Dim EmployeeID As String = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USERID", EmployeeID, SqlDbType.VarChar)
                    If rad1.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    ElseIf rad2.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    ElseIf rad3.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    End If
                    SPParam(3) = Mainclass.CreateSqlParameter("@ACDYear", ddlACDYear.SelectedValue, SqlDbType.BigInt)
                Case "S102301"
                    NoViewAndApproveAllowed = True
                    ACD_DropdownList = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    EditPagePath = "../StudentServices/Services_CreateActivityList.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = ""
                    HeaderTitle = "Approve Activities (Finance)"
                    IsStoredProcedure = True
                    NoAddingAllowed = True
                    StrSQL.Append("[dbo].[GET_PENDING_ACT_FORFINANCE]")
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("approve")
                    ElseIf rad2.Checked Then
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    ElseIf rad3.Checked Then
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    End If
                    SPParam(2) = Mainclass.CreateSqlParameter("@ACDYear", ddlACDYear.SelectedValue, SqlDbType.BigInt)
                Case "S103301"
                    ACD_DropdownList = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBoxAcceptText = "I have verified the activity details and approving the request"
                    EditPagePath = "../StudentServices/Services_ApproveActivtyPrincipal.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = ""
                    HeaderTitle = "Approve Activities (Principal)"
                    IsStoredProcedure = True
                    NoAddingAllowed = True
                    StrSQL.Append("[dbo].[GET_PENDING_ACT_FOR_APPRVL]")
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        NoViewAndApproveAllowed = False
                        NoViewAllowed = True
                        ShowGridCheckBox = True
                        ButtonText2 = "Approve"
                        If (Session("ButtonSave") = True) Then
                            lblError.Text = getErrorMessage("4008")
                            Session("ButtonSave") = False
                        Else
                            lblError.Text = ""
                        End If
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("approve")
                    ElseIf rad2.Checked Then
                        NoViewAndApproveAllowed = False
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText2 = ""
                        lblError.Text = ""
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    ElseIf rad3.Checked Then
                        NoViewAndApproveAllowed = False
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText2 = ""
                        lblError.Text = ""
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    End If
                    SPParam(2) = Mainclass.CreateSqlParameter("@ACDYear", ddlACDYear.SelectedValue, SqlDbType.BigInt)
                Case "S104301"
                    ACD_DropdownList = True
                    NoViewAndApproveAllowed = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    EditPagePath = "../StudentServices/Services_ApproveActivityOnRequest.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = ""
                    HeaderTitle = "Approve Student Activity Requests"
                    IsStoredProcedure = True
                    NoAddingAllowed = True
                    StrSQL.Append("[dbo].[GET_PENDING_ACT_ON_REQUESTAPPROVAL]")
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        ShowGridCheckBox = True
                        ButtonText2 = "Approve"
                        If (Session("ButtonSave") = True) Then
                            lblError.Text = getErrorMessage("4008")
                            Session("ButtonSave") = False
                        Else
                            lblError.Text = ""
                        End If
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("approve")
                    ElseIf rad2.Checked Then
                        ShowGridCheckBox = False
                        ButtonText2 = ""
                        lblError.Text = ""
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    ElseIf rad3.Checked Then
                        ShowGridCheckBox = False
                        ButtonText2 = ""
                        lblError.Text = ""
                        SPParam(1) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                        ViewState("datamode") = Encr_decrData.Encrypt("view")
                    End If
                    SPParam(2) = Mainclass.CreateSqlParameter("@ACDYear", ddlACDYear.SelectedValue, SqlDbType.BigInt)
            End Select
            SQLSelect = StrSQL.ToString
            ConnectionString = strConn
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog("From buildListView: " + ex.Message)
        End Try
    End Sub
    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From ddlFilter_SelectedIndexChanged: " + ex.Message)
        End Try
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From txtDate_TextChanged: " + ex.Message)
        End Try
    End Sub
    Private Sub RememberOldValues()
        Dim itmIdList As New ArrayList()
        Dim bindDt As DataTable
        bindDt = gvDetails.DataSource
        For Each row As GridViewRow In gvDetails.Rows
            Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text

            Dim result As Boolean = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox).Checked
            If ViewState("checked_items") IsNot Nothing Then
                itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
            End If
            If result Then
                If Not itmIdList.Contains(index) Then
                    itmIdList.Add(index)
                End If
            Else
                itmIdList.Remove(index)
            End If
        Next
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk IsNot Nothing Then

                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub RePopulateValues()
        Dim itmIdList As ArrayList = DirectCast(ViewState("checked_items"), ArrayList)
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            For Each row As GridViewRow In gvDetails.Rows
                Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text
                If itmIdList.Contains(index) Then
                    Dim myCheckBox As HtmlInputCheckBox = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub


End Class

