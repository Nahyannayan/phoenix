﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services_CreateActivitySubTypes.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="StudentServices_Services_CreateActivitySubTypes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .modal-dialog {
            max-width: 1150px !important;
        }

        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>
    <asp:UpdatePanel runat="server" ID="UP1">
        <ContentTemplate>
            <div class="card mb-3">
                <div class="card-header letter-space">
                    <i class="fa fa-users mr-3"></i>
                    Activity Sub Types
                </div>
                <div class="card-body">
                    <div class="table-responsive m-auto">

                        <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%"
                            height="0">
                            <tr valign="top">
                                <td valign="bottom" align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td valign="bottom" align="left">
                                    <%--<a href="#" data-toggle="modal" data-target="#Add_ACTIVITYSUBTYPE">Add New</a>--%>
                                    <asp:LinkButton ID="btnModal" ToolTip="Add New Sub Activity" Text="Add New" runat="server" OnClick="btnModal_Click" />

                                </td>
                            </tr>
                        </table>
                        <a id='top'></a>
                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top" width="100%">
                                    <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%" OnRowDataBound="gvDetails_RowDataBound" OnSelectedIndexChanged="gvDetails_SelectedIndexChanged"
                                        CssClass="table table-bordered table-row" AllowPaging="True" PageSize="10" EmptyDataText="No records Found" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvDetails_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="SUB TYPE NAME" SortExpression="Col1">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgrdactname" runat="server" Text='<%# Bind("ATM_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblATMid" Text='<%# Bind("ATM_ID")%>' runat="server" Visible="false"></asp:Label>
                                                    <asp:HiddenField ID="hdnALD_ATM_ID" Value='<%# Bind("ALD_ATM_ID")%>' runat="server" />
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                        Text="ACTIVITY SUB TYPE"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtCol1" runat="server" Width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                        OnClick="btnSearch_Click" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SUB TYPE DESCR" SortExpression="Col2" ItemStyle-Width="25%" Visible="false">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgrdactdescr" runat="server" Text='<%# Bind("ATM_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                        Text="DESCRIPTION"></asp:Label><br />
                                                    <asp:TextBox ID="txtCol2" runat="server" Width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                        OnClick="btnSearch_Click" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FEE CODE" SortExpression="Col3">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidFeecode" runat="server" Value='<%# Bind("ATM_FEE_CODE")%>' />
                                                    <asp:Label ID="lblgrdfeetype" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                        Text="SUB GROUP(Fee Head)"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtCol3" runat="server" Width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                        OnClick="btnSearch_Click" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ACTIVITY NAME" SortExpression="Col4">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidActivitycode" runat="server" Value='<%# Bind("AM_ACTIVITY_ID")%>' />
                                                    <asp:Label ID="lblgrdActivityName" runat="server" Text='<%# Bind("AM_ACTIVITY_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                        Text="ACTIVITY TYPE"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtCol4" runat="server" Width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                        OnClick="btnSearch_Click" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="COUNTRY NAME" SortExpression="Col5">
                                                <EditItemTemplate>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidCountrycode" runat="server" Value='<%# Bind("ATM_COUNTRY_CODE")%>' />
                                                    <asp:Label ID="lblgrdCountry" runat="server" Text='<%# Bind("CTY_NATIONALITY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                        Text="COUNTRY"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtCol5" runat="server" Width="70%"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                        OnClick="btnSearch_Click" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="hlVwAndApprv" runat="server" Text="View And Approve"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="" ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="hlEdit" runat="server" ToolTip="Update Sub Activity" OnClick="hlEdit_Click">Edit</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton OnClientClick="return confirmbtn();" ID="hlDelete" runat="server" ToolTip="Delete Sub Activity" OnClick="hlDelete_Click">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                            </asp:TemplateField>

                                            <%-- <asp:ButtonField Text="Edit" runat="server" CommandName="Edit" ItemStyle-Width="5%" />
                                                <asp:ButtonField Text="Delete" runat="server" CommandName="Delete" ItemStyle-Width="5%" />--%>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <div class="accordion" id="accordionExample3" runat="server" visible="false">
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Add New Sub Activity Type
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample3">
                                    <div class="card-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input id="hdnfATM_ID" runat="server" type="hidden" value="=" />
                    </div>
                </div>
            </div>
            <asp:Panel ID="divSubActivity" runat="server" CssClass="darkPanlAlumini" Visible="false">
                <div class="panel-cover inner_darkPanlAlumini">

                    <%--<div id="Add_ACTIVITYSUBTYPE" class="modal custom-modal fade" role="dialog" runat="server">
        <div class="modal-dialog modal-dialog-centered" role="document">--%>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" runat="server" id="PanelTitle">Add New Activity Sub Type</h5>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X" OnClick="btClose_Click"></asp:Button>

                            <%-- <button type="button" runat="server" class="close" data-dismiss="modal" aria-label="Close" id="btnModalClose">
                        <span aria-hidden="true">&times;</span>
                    </button>--%>
                        </div>

                        <div class="modal-body">
                            <span id="ErrorPnOAdd" class=""></span>
                            <table>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label">Activity Sub Type</span><span style="color: red">*</span>
                                    </td>
                                    <td width="30%">
                                        <asp:TextBox ID="txtSubName" runat="server"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label">Description </span><span style="color: red">*</span>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtSubDecsr" TextMode="MultiLine" Rows="3" Width="92%" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label">Associated Activity Type</span><span style="color: red">*</span>
                                    </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlActiTypes" runat="server"></asp:DropDownList>
                                    </td>
                                    <td width="20%">
                                        <span class="field-label">Associated Fee Code
                                        </span><span style="color: red">*</span>
                                    </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlFeeCode" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label">Country</span><span style="color: red">*</span>
                                    </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlCountry" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSaveSub" runat="server" CssClass="button" Text="Add" OnClick="btnSaveSub_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <%--</div>
    </div>--%>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSaveSub" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">



          function confirmbtn() {
                return confirm("Are you sure, you want delete the record!");

        }

        $(document).ready(function () {
         <%--   function btnSave() {
                 alert("");
                    var EventName = document.getElementById("<%=txtSubName.ClientID%>").value;
                var EventDesc = document.getElementById("<%=txtSubDecsr.ClientID()%>").value
                
                        if (!EventDesc == '') {
                            //if (!applstdt == '') {
                            
                               $("#btnModalClose").click()
                            return true
                            //}
                        }else return false
                    }else return false
            }--%>

            function ConfirmDelete(objMsg) {
                if (confirm(objMsg)) {
                    alert("execute code.");
                    return true;
                }
                else
                    return false;
            }

          

        })
    </script>
</asp:Content>
