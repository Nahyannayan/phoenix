﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web
Imports System.Threading
Imports System.Net.Mail
Imports System.Data.Sql
Partial Class StudentServices_Services_ApproveActivityOnRequest
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        ' smScriptManager.RegisterPostBackControl(btnImpTrRate)
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    Page.Title = OASISConstants.Gemstitle                   
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Else
                        ViewState("ViewId") = ""
                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                    'disable the control based on the rights 
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
                    ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))
                    If Not (Session("BSU_CURRENCY")) Is Nothing Then
                        Dim Currenncy As String = Session("BSU_CURRENCY")
                    End If
                    If (ViewState("datamode") = "approve") Then
                        lblapprvllogdt.Visible = False
                        GetPageLoad()
                    ElseIf (ViewState("datamode") = "view") Then
                        lblheading.Text = "View Activity Request"
                        GetViewPageLoad()
                    End If
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
    End Sub

#Region "Common_Methodes"
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
#End Region
#Region "Approval Page"
    Protected Sub GetPageLoad()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@REF_ID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[VIEW_DETAILS_PENDING_ACT_REQUEST_ON_APPROVAL]", Param)
            Dim dt As DataTable = DS.Tables(0)
            setpage(dt)
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetPageLoad: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Protected Sub setpage(ByVal dt As DataTable)
        Try
            If Not dt.Rows(0)("STUDENT_ID") Is Nothing Or Not dt.Rows(0)("STUDENT_ID") = 0 Then
                lblId.Text = dt.Rows(0)("STUDENT_ID")
            Else
                lblId.Text = ""
            End If
            If Not dt.Rows(0)("STUDENT_NAME") Is Nothing Or Not dt.Rows(0)("STUDENT_NAME") = "" Then
                lblName.Text = dt.Rows(0)("STUDENT_NAME")
            Else
                lblName.Text = ""
            End If
            If Not dt.Rows(0)("ACADEMIC_YEAR") Is Nothing Or Not dt.Rows(0)("ACADEMIC_YEAR") = "" Then
                lblacdyr.Text = dt.Rows(0)("ACADEMIC_YEAR")
            Else
                lblacdyr.Text = ""
            End If
            If Not dt.Rows(0)("APD_TOTAL_AMOUNT") Is Nothing Then
                lblAmnt.Text = Convert.ToString(dt.Rows(0)("APD_TOTAL_AMOUNT")) + " " + Session("BSU_CURRENCY")
            Else
                lblAmnt.Text = "0" + " " + Session("BSU_CURRENCY")
            End If
            If Not dt.Rows(0)("STU_COMMENTS") Is Nothing Or Not dt.Rows(0)("STU_COMMENTS") = "" Then
                lblComments.Text = dt.Rows(0)("STU_COMMENTS")
            Else
                lblComments.Text = ""
            End If
            If Not dt.Rows(0)("EVENT_DESCRIPTION") Is Nothing Or Not dt.Rows(0)("EVENT_DESCRIPTION") = "" Then
                lblEventDescr.Text = dt.Rows(0)("EVENT_DESCRIPTION")
            Else
                lblEventDescr.Text = ""
            End If
            If Not dt.Rows(0)("EVENT_DATE") = "#1/1/1900#" Then
                lblEventDt.Text = dt.Rows(0)("EVENT_DATE")
            Else
                lblEventDt.Text = ""
            End If
            If Not dt.Rows(0)("EVENT_NAME") Is Nothing Or Not dt.Rows(0)("EVENT_NAME") = "" Then
                lblEventNm.Text = dt.Rows(0)("EVENT_NAME")
                ViewState("ActivtyName") = dt.Rows(0)("EVENT_NAME")
            Else
                lblEventNm.Text = ""
                ViewState("ActivtyName") = ""
            End If
            If Not dt.Rows(0)("APD_LOG_DT") = "#1/1/1900#" Then
                lblLogDt.Text = dt.Rows(0)("APD_LOG_DT")
            Else
                lblLogDt.Text = ""
            End If
            If Not dt.Rows(0)("STS_FEMAIL") Is Nothing Or Not dt.Rows(0)("STS_FEMAIL") = "" Then
                ViewState("ParentMailId") = dt.Rows(0)("STS_FEMAIL")
            Else
                ViewState("ParentMailId") = ""
            End If
            If Not dt.Rows(0)("ALD_EMAIL_TEMPLATE") Is Nothing Or Not dt.Rows(0)("ALD_EMAIL_TEMPLATE") = "" Then
                ViewState("HTML_Message") = dt.Rows(0)("ALD_EMAIL_TEMPLATE")
            Else
                ViewState("HTML_Message") = ""
            End If
            If Not dt.Rows(0)("APD_COUNT") Is Nothing Or Not dt.Rows(0)("APD_COUNT") = 0 Then
                lblCount.Text = dt.Rows(0)("APD_COUNT")
            Else
                lblCount.Text = "1"
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From setpage:" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    'Public Function getparentcontact(ByVal viewid As Integer) As String
    '    Dim tomailid As String = String.Empty
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim Param(0) As SqlParameter
    '        Param(0) = Mainclass.CreateSqlParameter("@VIEWID", viewid, SqlDbType.BigInt)
    '        tomailid = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[GET_PARENT_MAILID]", Param)
    '        Return tomailid
    '    Catch ex As Exception
    '        Return tomailid            
    '    End Try
    'End Function
    Protected Sub SendNotificationEmail(ByVal Status As String)
        Dim tomail As String = String.Empty
        Dim sendAs As String = String.Empty
        Dim SendpWD As String = String.Empty
        Dim sendPort As String = String.Empty
        Dim FromEmail As String = String.Empty
        Dim SendHost As String = String.Empty
        Dim subject As String = String.Empty
        Dim mailbody As String = String.Empty

        Try
            tomail = ViewState("ParentMailId")
            If Mainclass.isEmail(tomail) Then               
                If (Status = "A") Then
                    subject = "Mail Notification : Your Activity Request for *" + ViewState("ActivtyName") + "* Has been APPROVED"
                    mailbody = ViewState("HTML_Message")
                End If
                'Inserting into table [COM_EMAIL_SCHEDULE] for sending the email 
                InsertintoMailTable(subject, mailbody, tomail)
            Else
                'Dim ErrorMessage As String = "Error: Email Id Is Not Valid"
                Message = getErrorMessage("4005")              
                UtilityObj.Errorlog(Message, "OASIS ACTIVITY SERVICES")
            End If
        Catch EX As Exception
            UtilityObj.Errorlog("From Sendnotificationemail: " + EX.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub InsertintoMailTable(ByVal subject As String, ByVal message As String, ByVal tomailid As String)
        Try
            Dim str_conn = "OASISConnectionString"
            Dim query As String = "[dbo].[InsertIntoEmailSendSchedule] '" & Session("sBsuid") & "','ActivityRequestStatusMail','SYSTEM','" & tomailid & "','" & subject & "','" & message & "'"
            Mainclass.getDataValue(query, str_conn)
        Catch ex As Exception
            UtilityObj.Errorlog("From InsertintoMailTable: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region
#Region "View Approved/Rejected"
    Protected Sub GetViewPageLoad()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@REF_ID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[VIEW_DETAILS_PENDING_ACT_REQUEST_ON_APPROVAL]", Param)
            Dim dt As DataTable = DS.Tables(0)
            setpage(dt)
            If Not dt.Rows(0)("APPRVL_CMMNTS") Is Nothing Or Not dt.Rows(0)("APPRVL_CMMNTS") = "" Then
                txtApprvlCmmnts.InnerText = dt.Rows(0)("APPRVL_CMMNTS")
            Else
                txtApprvlCmmnts.InnerText = ""
            End If
            If Not dt.Rows(0)("APPRVL_LOGDT") = "#1/1/1900#" Then
                lblapprvllogdt.Text = dt.Rows(0)("APPRVL_LOGDT")
            End If
            lblapprvllogdt.Visible = True
            lblAprvLogg.Visible = True
            btnApprove.Visible = False
            btnReject.Visible = False
            lblapprvcmmnt.Text = "Approver Comments"
            txtApprvlCmmnts.Disabled = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetViewPageLoad: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region
#Region "Control Methodes"
    
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(2) As SqlParameter
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Param(0) = Mainclass.CreateSqlParameter("@REF_ID", ViewState("ViewId"), SqlDbType.BigInt)
            Param(1) = Mainclass.CreateSqlParameter("@STATUS", "A", SqlDbType.VarChar)
            If txtApprvlCmmnts.InnerText Is Nothing Then
                Param(2) = Mainclass.CreateSqlParameter("@COMMENTS", "", SqlDbType.VarChar)
            Else
                Param(2) = Mainclass.CreateSqlParameter("@COMMENTS", txtApprvlCmmnts.InnerText, SqlDbType.VarChar)
            End If
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UPDATE_DETAILS_PENDING_ACT_REQUEST_ON_APPROVAL]", Param)
            btnApprove.Visible = False
            btnReject.Visible = False
            'lblError.Text = "Activity Has Got Approved"
            Message = getErrorMessage("4025")
            ShowMessage(Message, False)
            strans.Commit()
            Dim status As String = "A"
            SendNotificationEmail(status)
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnApprove_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub btnReject_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(2) As SqlParameter
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Param(0) = Mainclass.CreateSqlParameter("@REF_ID", ViewState("ViewId"), SqlDbType.BigInt)
            Param(1) = Mainclass.CreateSqlParameter("@STATUS", "R", SqlDbType.VarChar)
            If txtApprvlCmmnts.InnerText Is Nothing Then
                Param(2) = Mainclass.CreateSqlParameter("@COMMENTS", "", SqlDbType.VarChar)
            Else
                Param(2) = Mainclass.CreateSqlParameter("@COMMENTS", txtApprvlCmmnts.InnerText, SqlDbType.VarChar)
            End If
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UPDATE_DETAILS_PENDING_ACT_REQUEST_ON_APPROVAL]", Param)
            btnApprove.Visible = False
            btnReject.Visible = False
            'lblError.Text = "Activity Has Got Rejected"
            Message = getErrorMessage("4026")
            ShowMessage(Message, True)
            strans.Commit()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnReject_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub btnClose_Click(sender As Object, e As EventArgs)
        Try
            Dim URL As String = Nothing
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            URL = String.Format("~/StudentServices/ListReportView_StudentServices.aspx?MainMnu_code={0}", ViewState("MainMnu_code"))
            Response.Redirect(URL)
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnClose_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

   
#End Region


    
End Class
