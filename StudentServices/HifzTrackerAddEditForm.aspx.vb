﻿Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Data
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Public Class HifzTracker_AddEditHifzTrackerForm
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Not Page.IsPostBack Then
            Dim trackerId = Convert.ToString(Request.QueryString("Id"))

            If Not String.IsNullOrEmpty(trackerId) Then
                InitAddEditHifzTrackerForm(trackerId)
            Else
                BindControls()
                hdnTrackerId.Value = "0"
                hdnEmployeeId.Value = Convert.ToString(HttpContext.Current.Session("EmployeeId"))
            End If
            divError.Visible = False
        End If
    End Sub

    Protected Sub btnSaveHifzData_Click(ByVal sender As Object, ByVal e As EventArgs)
        If Page.IsValid AndAlso Not String.IsNullOrEmpty(hdnNoOfVerses.Value) Then
            Try
                divError.Visible = False
                Dim model = GetFormData(Request.Form)
                Dim result = UpdateHifzTrackerData(model)

                If result Then
                    ScriptManager.RegisterStartupScript(Me, [GetType](), "close", "CloseAddEditHifzTrackerFormModal(true);", True)
                Else
                    divError.Visible = True
                    LoadVersesGrid(hdnNoOfVerses.Value)
                End If

            Catch ex As Exception
                divError.Visible = True
                LoadVersesGrid(hdnNoOfVerses.Value)
                UtilityObj.Errorlog(ex.Message)
            End Try

        Else
            If Not String.IsNullOrEmpty(hdnNoOfVerses.Value) Then
                lblVersesErrorMsg.InnerText = ""
            Else
                lblVersesErrorMsg.InnerText = "No. of verses required"
            End If
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindGrade(ddlAcademicYear.SelectedItem.Value)
    End Sub

    Private Sub BindControls()
        BindAcademicYear()
        BindGrade()
    End Sub

    Private Sub BindAcademicYear()

        ddlAcademicYear.Items.Clear()
        Try
            Dim parameters As SqlParameter() = New SqlParameter(4) {}
            parameters(0) = New SqlParameter("@BSU_ID", GetCurrentSchoolId())
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetCurrentAcademicYearList", parameters)
            ddlAcademicYear.DataSource = ds
            ddlAcademicYear.DataTextField = "AcademicYearDescription"
            ddlAcademicYear.DataValueField = "AcademicYearId"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindGrade(Optional ByVal academicYearId As String = "")

        ddlGrade.Items.Clear()
        Try
            Dim ds As DataSet

            If Not String.IsNullOrEmpty(academicYearId) Then
                Dim str_query As String = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " & " ,grm_grd_id AS GRM_GRD_ID,grd_displayorder,STM_ID FROM grade_bsu_m,grade_m,stream_m WHERE" & " grade_bsu_m.grm_grd_id=grade_m.grd_id  " & " and grade_bsu_m.grm_stm_id=stream_m.stm_id " & " and grm_acd_id=" & academicYearId & " order by grd_displayorder"
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.Text, str_query)
                ddlGrade.DataSource = ds
                ddlGrade.DataTextField = "grm_display"
                ddlGrade.DataValueField = "grm_grd_id"
                ddlGrade.DataBind()
            End If

            ddlGrade.Items.Insert(0, New ListItem("Select", ""))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub InitAddEditHifzTrackerForm(ByVal trackerId As String)
        Try
            Dim formData = GetHifzTrackerFormData(trackerId)
            Dim rowData = formData.Rows(0)
            hdnTrackerId.Value = Convert.ToString(rowData("HifzTrackerId"))
            hdnEmployeeId.Value = Convert.ToString(rowData("EmployeeId"))
            ddlAcademicYear.Text = Convert.ToString(rowData("AcademicYearId"))
            ddlGrade.Text = Convert.ToString(rowData("GradeId"))
            txtTitle.Text = Convert.ToString(rowData("Title"))
            txtURL.Text = Convert.ToString(rowData("URL"))
            hdnNoOfVerses.Value = Convert.ToString(rowData("NoOfVerses"))
            chkStatus.Checked = If(Convert.ToBoolean(rowData("Status")), True, False)
            BindAcademicYear()
            BindGrade(ddlAcademicYear.Text)
            LoadVersesGrid(hdnNoOfVerses.Value)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub LoadVersesGrid(ByVal verses As String)
        Dim versesList = verses.Split("|"c).ToList()
        Dim allRowsHtml = ""

        For Each verse In versesList
            Dim deleteButton = "<input type='image' class='btnDeleteVerses' title='Delete' src='../Images/ButtonImages/delete.png' style='height:16px;width:16px;border-width:0px;'>"
            Dim row = "<tr data-itemid='" & verse & "'>" & "<td class='col-md-8'>" + verse & "</td>" & "<td class='col-md-4'>" & deleteButton & "</td>" & "</tr>"
            allRowsHtml += row
        Next
        tblVersesHtml.Text = allRowsHtml
    End Sub

    Private Function GetHifzTrackerFormData(ByVal trackerId As String) As DataTable
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@HifzTrackerId", trackerId)
        Dim ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetHifzTrackerFormData", parameters)
        Return ds.Tables(0)
    End Function

    Private Function GetFormData(ByVal formData As NameValueCollection) As HifzTrackerView
        Dim model As HifzTrackerView = New HifzTrackerView()
        model.TrackerId = Convert.ToInt32(formData("hdnTrackerId"))
        model.GradeId = Convert.ToString(formData("ddlGrade"))
        model.AcademicYearId = Convert.ToInt32(formData("ddlAcademicYear"))
        model.Title = Convert.ToString(formData("txtTitle"))
        model.URL = Convert.ToString(formData("txtURL"))
        model.EmployeeId = Convert.ToInt32(formData("hdnEmployeeId"))
        model.NoOfVerses = Convert.ToString(formData("hdnNoOfVerses"))
        model.TransMode = If(model.TrackerId > 0, "U"c, "I"c)
        model.Status = If(formData("chkStatus") IsNot Nothing AndAlso formData("chkStatus").ToString() = "on", True, False)
        Return model
    End Function

    Private Function UpdateHifzTrackerData(ByVal model As HifzTrackerView) As Boolean
        Dim parameters As SqlParameter() = New SqlParameter(15) {}
        parameters(0) = New SqlParameter("@BSU_ID", GetCurrentSchoolId())
        parameters(1) = New SqlParameter("@HifzTrackerId", model.TrackerId)
        parameters(2) = New SqlParameter("@EmployeeId", model.EmployeeId)
        parameters(3) = New SqlParameter("@AcademicYearId", model.AcademicYearId)
        parameters(4) = New SqlParameter("@Title", model.Title)
        parameters(5) = New SqlParameter("@URL", model.URL)
        parameters(6) = New SqlParameter("@NoOfVerses", model.NoOfVerses)
        parameters(7) = New SqlParameter("@GradeId", model.GradeId)
        parameters(8) = New SqlParameter("@Status", model.Status)
        parameters(9) = New SqlParameter("@TransMode", model.TransMode)
        parameters(10) = New SqlParameter("@output", SqlDbType.Int)
        parameters(10).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.UpdateHifzTrackerData", parameters)
        Return CInt(parameters(10).Value) > 0
    End Function

    Private Function GetCurrentSchoolId() As String
        Return Convert.ToString(HttpContext.Current.Session("sBsuid"))
    End Function
End Class

Public Class HifzTrackerView
    Public Property TrackerId As Integer
    Public Property GradeId As String
    Public Property AcademicYearId As Integer
    Public Property Title As String
    Public Property URL As String
    Public Property EmployeeId As Integer
    Public Property NoOfVerses As String
    Public Property TransMode As Char
    Public Property Status As Boolean

End Class
