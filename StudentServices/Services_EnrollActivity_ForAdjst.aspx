﻿<%@ Page Language="VB" AutoEventWireup="true" EnableEventValidation="true" MasterPageFile="~/mainMasterPage.master" CodeFile="Services_EnrollActivity_ForAdjst.aspx.vb" Inherits="StudentServices_Services_EnrollActivity_ForAdjst" %>
<%@ Register Src="~/UserControls/uscStudentPicker.ascx" TagPrefix="uc1" TagName="uscStudentPicker" %>
<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <link href="../cssfiles/Ribbon.css" rel="stylesheet" />
    <%--<script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />--%>

    <style>
           input[type="radio"] {
            vertical-align: top !important
        }

        .container-fluid {
            padding: 0 !important;
        }

        .container {
            max-width: 95% !important;
            min-width: 95% !important;
        }

        .pull-right{
            float :inherit  !important;
        }

        .toggle-msg {
            position: absolute;
            width: 60%;
            background-color: #ffffff;
            color: #333;
            padding: 2px;
            z-index: 1000;
            border-radius: 8px;
            box-shadow: rgba(0, 0, 0, 0.3) 0px 5px 9px;
            border-color: #efefef;
        }
    </style>
    <script >
        function fnMouseOver(div) {            
            div.style.display = "block";
        }
        function fnMouseOut(div) {           
            div.style.display = "none";
        }
        function ConfirmTerms(div) {
            div.style.display = "block";
        }

    </script>
    <script>

        function ShowWindowWithClose(gotourl, pageTitle, w, h, btnname) {
              
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                //closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                },
                afterClose: function () {
                    //window.location.reload();
                    document.getElementById('<%= hidStatus.ClientID%>').value = "true" 
                    __doPostBack('<%= hidStatus.ClientID%>', 'OnValueChanged');
                }
            });
            return false;
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function Showdata(mode) {
            var url;
            var NameandCode;
            var STU_ID = document.getElementById('<%= h_Student_no.ClientID %>').value;

            var ACD_ID = '<%= uscStudentPicker.STU_ACD_ID %>';
            if (mode == 1) {
                url = "../common/PaymentHistory.aspx?id=RECEIPTHISTORY&stuid=" + STU_ID;
            }
            else if (mode == 2)
                url = "../common/PopupShowData.aspx?id=CHARGEHISTORY&stuid=" + STU_ID;
            else if (mode == 3)
                url = "../common/PopupShowData.aspx?id=SIBBLINGS&stuid=" + STU_ID;
            else if (mode == 4)
                url = "../common/PopupShowData.aspx?id=CONCESSION_FEES&stuid=" + STU_ID + "&acd=" + ACD_ID;
            else if (mode == 5)
                url = "../common/PopupShowData.aspx?id=ADJUSTMENT&stuid=" + STU_ID;
            else if (mode == 6)
                url = "../common/PopupShowData.aspx?id=CHQBOUNCE&stuid=" + STU_ID;
            else if (mode == 7)
                url = "../common/PopupShowData.aspx?id=ADVPAY&stuid=" + STU_ID;
            else if (mode == 8)
                url = "../common/PopupShowData.aspx?id=AGING&stuid=" + STU_ID;
            else if (mode == 9)
                url = "FeeCalculateFee.aspx";
            Popup(url);

        }


        function ConfirmEnroll() {

            return confirm('Do  you want to enroll for this activiy?');
        }

        function ConfirmUnregister() {
            return confirm('Please confirm to unsubscribe from this activity?');
        }

        function AlertMultiple() {
            alert("Can't enroll, Activity has been requested on the same day! ");
        }

        function AlertLimitReached() {
            alert("Can't enroll, Activity selection limit has been reached! ");
        }
       
    </script>
    <style>

 .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft  {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <style>
        label {
            font-weight: normal !important;
        }

        .divbodyO_AR {
            /*font-size:12px;*/
        }

        .divnoteO_AR {
            text-align: center;
        }

        .linkbtnPositionO_AR {
            float: right;
            color: blue;
        }

        .OverFlowO_AR {
            overflow: auto;
        }

        .LabelEventNameO_AR {
            /*position: absolute;*/
            color: #39A8C5;
            font-weight: bold;
            font-size: large;
            padding-top: 15px;
        }

        .DisplayNoneO_AR {
            display: none;
        }

        .hrStyleO_AR {
            border: 1px solid lightblue;
        }

        .lblShowO_AR {
            background-color: lightBlue;
            border: 2px solid #e7e7e7;
            color: white;
            float: right;
            padding: 10px 22px;
            text-align: center;
            border-radius: 6px;
            text-decoration: none;
            display: inline-block;
            /*font-size: 12px;*/
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            cursor: pointer;
        }

        /*.lblShowO_AR:hover {background-color: #e7e7e7; color:black;}*/
        .lblShow1O_AR {
            border: 1px solid #e7e7e7;
            padding: 5px;
            border-radius: 6px;
            margin: 10px 2px;
            background-color: #f9f9f9;
        }

        .lblShow1O_AR-inner {
            background-color: #ffffff;
            border: 1px solid #e7e7e7;
            padding: 10px;
            border-radius: 8px;
            margin: 0 2px 2px 2px;
        }

        .LabelEventStatus {
            color: #39A8C5;
            font-style: italic;
        }

        .badge {
            background-color: orange !important;
            /*transform: rotate(45deg) !important;*/
            /*-webkit-transform: rotate(45deg) !important;*/
            padding: 8px 17px !important;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        }


        .sched-txt {
            font-size: 16px;
            font-weight: bold;
        }

        .act-desc-title {
            font-size: 14px;
            font-weight: bold;
        }

        .act-title {
            font-size: 2rem;
            font-weight: bold;
        }

        .act-error {
            /*color: #2b88a0;*/
            font-style: italic;
            /*background-color: #f9f9e1;*/
            display: inline;
            padding: 2px 24px;
            margin-right: 4px;
            /*font-weight: 600;*/
        }

        .textcntO_AD {
            width: 40%;
            border-radius: 8px;
        }

        
      

        /*.lblShow1O_AR:hover {background-color: lightblue; color:white !important;}*/
    </style>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
         <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="620px">
            </telerik:RadWindow>
        </Windows>
     </telerik:RadWindowManager>
  

      
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Enroll In Activity
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <asp:HiddenField ID="hidStatus" runat="server" Value="false" OnValueChanged="hidStatus_ValueChanged" />
                <asp:HiddenField ID="hidamt" runat="server" />
                <asp:hiddenField ID="hidTotalAmt" runat="server" />
                <asp:HiddenField ID="hidavailseat" runat="server" />
                <asp:hiddenField ID="hidallowmultiple" runat="server" />
                <asp:HiddenField ID="h_Student_no" runat="server" />
                <asp:label ID="lblError" runat="server" ></asp:label>
                <table width="100%">                   
                    <tr>
                        <td align="left"><span class="field-label">Select Student</span></td>
                        <td colspan="3">
                            <uc1:uscStudentPicker runat="server" ID="uscStudentPicker" />
                        </td>
                    </tr>
                     <tr>
                        <td colspan="4">
                            <asp:Panel ID="pnlActivity" runat="server">
                                <div class="content margin-top30 margin-bottom60">
                                    <div class="container">
                                        <div class="row">
                                            <asp:HiddenField ID="hidclientidbtn" Value="" runat="server" />
                                            <!-- Posts Block -->
                                            <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="my-account">

                                                    <div id="divbody" class="divbodyO_AR">
                                                        <div id="divNote" runat="server" class="divnoteO_AR">
                                                          <%--  <img src="/Images/activity1.png" alt="No Image" />--%>
                                                            <br />
                                                            <%--<asp:LinkButton ID="btnEnrolledLst" runat="server" Visible="true" Text="Click Here to see enrolled activities & status" CssClass="btn btn-info rightEnd pull-right" />--%>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                        <div class="mainheading" style="display: none;">
                                                            <div class="left">Enroll In Activity/Activities</div>
                                                            <div class="right">
                                                                <%--<asp:Label ID="lbChildName" runat="server" CssClass="lblChildNameCss"></asp:Label>--%>
                                                            </div>
                                                            <br />
                                                        </div>
                                                        <div class="OverFlowO_AR" id="divActivity" runat="server">
                                                            <div class="divnoteO_AR">
                                                                <asp:Label ID="Label1" runat="server" Text="" />
                                                            </div>
                                                            <asp:Repeater ID="rptrGroup" OnItemDataBound="rptrGroup_ItemDataBound" runat="server">
                                                                <ItemTemplate>
                                                                    <%--  <asp:RadioButton ID="radSelectAct" runat="server" GroupName="SelectControl" />
                    <asp:CheckBox ID="chkSelectAct" runat="server"  />--%>
                                                                    <asp:HiddenField ID="hidAPG_ID" runat="server" Value='<%# Eval("APG_ID")%>' />
                                                                    <div class="lblShow1O_AR" id="divRepeater" runat="server">
                                                                        <div class="container-fluid">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                    <div class="act-title">
                                                                                        <asp:Label ID="lblHeaderrptr" runat="server" Text='<%# Eval("APG_NAME")%>'></asp:Label>
                                                                                        <asp:Label ID="lblMaximumLimit" runat="server" Text="" Visible="false" Style="float: right; font-size: small !important;"></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                                                                    <div id="divLimitFullText" style="color: burlywood; font-style: italic;">
                                                                                        <asp:Label ID="lbllimitFulltxt" runat="server" Text="" />
                                                                                        <asp:HiddenField ID="hidMaxlimit" runat="server" Value='<%# Eval("APG_MAXLIMIT")%>' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <asp:Repeater ID="Repeater1" OnItemDataBound="Repeater1_ItemDataBound" runat="server">
                                                                            <ItemTemplate>
                                                                                <div runat="server" id="ribbonNotPaid">
                                                                                    <asp:Label ID="lblStatus" runat="server" Text="" CssClass="LabelEventStatus"></asp:Label>
                                                                                </div>
                                                                             <%--   <div runat="server" class="ribbonActive">
                                                                                    <asp:Label ID="lblDone" runat="server" Text="ENROLLED" Style="display: none;"></asp:Label>
                                                                                </div>--%>
                                                                                <div class="lblShow1O_AR-inner" pagesize="2">

                                                                                    <%--<asp:RadioButton ID="radSelectAct" runat="server" GroupName="SelectControl" />
                                    <asp:CheckBox ID="chkSelectAct" runat="server" />--%>
                                                                                    <asp:Label ID="lblViewId" runat="server" Visible="FALSE" Text='<%# Eval("ALD_ID")%>' />
                                                                                    <img src='<%# Eval("AM_ACTIVITY_ICON_PATH")%>' width="50px" style="display: none;" />
                                                                                    <asp:Label ID="lblEventName" CssClass="LabelEventNameO_AR" runat="server" Text='<%# Eval("ALD_EVENT_NAME")%>'></asp:Label>
                                                                                    <%--<table align="right" id="tblSuccess" runat="server">
                                                            <tr>
                                                                <td>                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                                                    <div class="row">
                                                                                        <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom20">
                                                                                            <asp:Label ID="lblDescr" runat="server" Text='<%# Eval("ALD_EVENT_DESCR")%>' />
                                                                                        </div>
                                                                                    </div>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="40%">
                                                                                                <div class="container-fluid margin-bottom10 margin-top10">
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-6 col-md-2 col-sm-6"><span class="act-desc-title">Amount :</span></div>
                                                                                                        <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                                            <asp:Label ID="lblamt" runat="server" Text='<%# String.Concat(Eval("ALD_EVENT_AMT"), " ", "AED")%>'> </asp:Label>
                                                                                                            <span style="display: none;">
                                                                                                                <asp:Label ID="lblApprvl" runat="server" Text='<%# Eval("ALD_APPRVLREQ_FLAG")%>' value='<%# Eval("ALD_APPRVLREQ_FLAG")%>'></asp:Label></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-6 col-md-2 col-sm-6"><span class="act-desc-title">Event on :</span></div>
                                                                                                        <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                                            <asp:Label ID="lblLogDt" runat="server" Text='<%# Eval("EVENT_DATE")%>' value='<%# Eval("EVENT_DATE")%>' />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-6 col-md-2 col-sm-6"><span class="act-desc-title">Requesting Period :</span></div>
                                                                                                        <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("REQ_DATE")%>' value='<%# Eval("EVENT_DATE")%>' />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-6 col-md-2 col-sm-6">
                                                                                                            <span class="act-desc-title">Available Seats :</span>
                                                                                                        </div>
                                                                                                        <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                                            <asp:Label ID="lblAvailble" runat="server" Text='<%# Eval("ALD_AVAILABLE_SEAT")%>' value='<%# Eval("ALD_AVAILABLE_SEAT")%>'></asp:Label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <asp:HiddenField ID="hdn_count" runat="server" Value='<%# Eval("ALD_ALLOW_MULTIPLE")%>' />
                                                                                                    <div class="row" id="divcount" runat="server" visible="false">
                                                                                                        <div class="col-lg-6 col-md-2 col-sm-6">
                                                                                                            <span class="act-desc-title">Count :</span>

                                                                                                        </div>
                                                                                                        <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                                            <asp:DropDownList runat="server" ID="ddlcount"  AutoPostBack="true" CssClass="textcntO_AD"></asp:DropDownList>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td width="60%">
                                                                                                <div id="divSchdl" runat="server" visible='<%# Eval("ALD_IS_SCHEDULE")%>'>
                                                                                                    <div class="container-fluid margin-bottom10 margin-top10">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                                                <div class="sched-txt">Schedules :</div>
                                                                                                                <asp:RadioButtonList ID="rdoSchdlDetails" runat="server"  AutoPostBack="true"></asp:RadioButtonList>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <asp:CheckBox ID="chkTerm" runat="server" Text="I Accept"  AutoPostBack="true" Visible="false"></asp:CheckBox>
                                                                                                <asp:LinkButton ID="lnkTermCond" runat="server" Text="Terms and conditions." Visible="false"></asp:LinkButton>
                                                                                                <div style="display: none" id="divTermMessage" runat="server">Please accept terms and conditions</div>
                                                                                                <div runat="server" id="divgrad" style="display: none" class="table table-striped table-bordered toggle-msg">
                                                                                                    <div class="alert-success alert m-0">
                                                                                                    <asp:Literal ID="ltrlTerm" runat="server" Text='<%# Eval("ALD_TERMS_CONDITIONS")%>'></asp:Literal>
                                                                                                        </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <div class="container-fluid margin-bottom10 margin-top10">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                                <%--<asp:LinkButton ID="lnkbtnUnReg" runat="server" Text="UnSubscribe" CssClass="btn btn-info submit pull-right bottom-pad" Visible="false" OnCommand="lnkbtnUnReg_Command" CommandArgument='<%# Eval("ALD_ID")%>' OnClientClick="return ConfirmUnregister();"></asp:LinkButton>&nbsp;&nbsp;--%>
                                                                                                <%--<asp:LinkButton ID="lnkbtnPay" runat="server" Text="Pay" CssClass="btn btn-info submit pull-right bottom-pad" Visible="false" />&nbsp;&nbsp;--%>
                                                                                                <%--OnCommand="lnkbtnPay_Command" CommandArgument='<%# Eval("ALD_EVENT_NAME")%>'--%>
                                                                                                <asp:LinkButton ID="lnkbtnAdjst" runat="server" Text="Registration Complete Via Adjustment" CssClass="btn btn-info submit pull-right bottom-pad" OnClick="lnkbtnAdjst_Click" />&nbsp;&nbsp;

                                                                                                <%--<asp:HiddenField ID="hidStatus" runat="server" Value='<%# Eval("APD_STATUS")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidCollMode" runat="server" Value='<%# Eval("ALD_FEE_REG_MODE")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidFeetypeId" runat="server" Value='<%# Eval("ALD_FEETYPE_ID")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidAmnt" runat="server" Value='<%# Eval("ALD_EVENT_AMT")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidAPD_ID" runat="server" Value='<%# Eval("APD_ID")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidALD_PAY_ONLINE" runat="server" Value='<%# Eval("ALD_PAY_ONLINE")%>' />--%>
                                                                                                <%-- new fields--%>
                                                                                                <%--<asp:HiddenField ID="hidAvaiableSeat" runat="server" Value='<%# Eval("ALD_AVAILABLE_SEAT")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidtotalamount" runat="server" Value='<%# Eval("APD_TOTAL_AMOUNT")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidcount" runat="server" Value='<%# Eval("APD_COUNT")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidallowmultiple" runat="server" Value='<%# Eval("ALD_ALLOW_MULTIPLE")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="maxcount" runat="server" Value='<%# Eval("ALD_MAX_COUNT")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidpaymentStatus" runat="server" Value='<%# Eval("APD_PAYMENT_STATUS")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidAllowMultipleActDay" runat="server" Value='<%# Eval("ALD_BNOT_ALLWDAY")%>' />--%>
                                                                                                <asp:HiddenField ID="hidTotalAmt" runat="server" />
                                                                                                <%--<asp:HiddenField ID="hidMulMaxCount" runat="server" Value='<%# Eval("ALD_ALLOW_MULTIPLE_MAXCOUNT")%>' />--%>
                                                                                                <%--<asp:HiddenField ID="hidmailid" runat="server" Value='<%# Eval("STS_FEMAIL")%>' />--%>
                                                                                                <%--<asp:TextBox ID="txtEmailTemplate" runat="server" Text='<%# Eval("ALD_EMAIL_TEMPLATE")%>' Visible="false"></asp:TextBox>--%>
                                                                                                <%--<asp:HiddenField ID="hidschedule" runat="server" Value='<%# Eval("APD_SCHEDULE_ID")%>' />--%>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                 <%--   <div class=" alert-success alert" runat="server" visible="false" id="divgrad">
                                                                                        <asp:Label ID="lblMsgHead1" runat="server" Text="Please note the next steps :" Visible="false"></asp:Label><br />
                                                                                        <asp:Label ID="lblMessage" runat="server" Visible="false" />
                                                                                    </div>--%>
                                                                                    <%-- <hr runat="server" class ="hrStyleO_AR"/> --%>
                                                                                </div>

                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>                                                                         
            </div>
        </div>
    </div>  


</asp:Content>
