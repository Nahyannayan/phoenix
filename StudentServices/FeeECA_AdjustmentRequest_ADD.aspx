﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeECA_AdjustmentRequest_ADD.aspx.vb" Inherits="StudentServices_FeeECA_AdjustmentRequest_ADD" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <%--  <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />--%>

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <link href="../cssfiles/Popup.css?1=2" rel="stylesheet" />
    <script type="text/javascript">

        //        Sys.Applicon.add_load(
        //function CheckForPrint() {
        //    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        //        document.getElementById('<%= h_print.ClientID %>').value = '';
        //        showModalDialog('../Reports/ASPX Report/RptViewerModal.aspx', '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
        //    }
        //}
        //    );

        function FillDetailRemarks() {
            var HeaderRemarks;
            HeaderRemarks = document.getElementById('<%=txtHeaderRemarks.ClientID %>').value;
            if (HeaderRemarks != '')
                document.getElementById('<%=txtDetRemarks.ClientID %>').value = HeaderRemarks;
            return false;
        }


        function GetStudentListName() {
            var sFeatures, url;


            sFeatures = "dialogWidth: 600px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            var bsuId = $("#<%=ddlBusinessunit.ClientID%>").val();

            //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
            url = "FeeECA_Gen_Add_Students.aspx?ID=" + bsuId
            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_fee");
            <%--if (result == '' || result == undefined) {

                return false;
            }
            if (result != '' && result != undefined) {
                NameandCode = result.split('||');
                $("#<%=h_StuID.ClientID %>").val(NameandCode[0]);
                $("#<%=txtStdNo.ClientID %>").val(NameandCode[2]);
                $("#<%=txtStudentname.ClientID %>").val(NameandCode[1]);

            }--%>
            //return true;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
              
                $("#<%=h_StuID.ClientID %>").val(NameandCode[0]);
                $("#<%=txtStdNo.ClientID %>").val(NameandCode[2]);
                $("#<%=txtStudentname.ClientID %>").val(NameandCode[1]);
                <%--__doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');--%>
            }
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ECA Fee Adjustment ADD"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%--<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
                    <tr>
                        <td colspan="4" align="left"></td>
                    </tr>
                </table>--%>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>  </td>
                        <td colspan="3" align="left">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" TabIndex="5">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton
                                ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date  required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtDate">*</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDate"
                                ErrorMessage="Date Required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Reason</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAdjReason" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAdjReason_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%">
                            <asp:RadioButton ID="radStud" AutoPostBack="true" runat="server" GroupName="ENQ_STUD"
                                Text="Student" Checked="True" CssClass="field-label" />
                            <asp:RadioButton ID="radEnq" AutoPostBack="true" runat="server" GroupName="ENQ_STUD"
                                Text="Enquiry" CssClass="field-label" />
                        </td>
                        <td align="left" width="30%">
                            <asp:LinkButton ID="lnkRefundable" OnClientClick="return false;" runat="server" Visible="false">View Refundable Fees</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Student</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" OnTextChanged="txtStdNo_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetStudentListName();return false;" />
                        </td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>
                        </td>
                        <td align="left" width="30%">
                            <asp:Label ID="labGrade" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtHeaderRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Header remarks required"
                                ValidationGroup="MAINERROR" ControlToValidate="txtHeaderRemarks">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg-lite" colspan="4">Fee Adjustments Details...</td>
                    </tr>

                    <tr runat="server">
                        <td align="center" colspan="4">
                            <table style="width: 100% !important;">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Fee Type</span></td>
                                    <td align="left" width="20%" id="tcTAXCode1" runat="server"><span class="field-label">Tax Code</span></td>
                                    <td align="left" width="20%"><span class="field-label">Amount</span></td>
                                    <td align="left" width="20%" id="tcTAXAmt1" runat="server"><span class="field-label">Tax Amount</span></td>
                                    <td align="left" width="20%" id="tcNETAmt1" runat="server"><span class="field-label">Net Amount</span></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Label ID="lblBalance" runat="server" Text="Balance Amount " CssClass="field-label"></asp:Label>
                                        <asp:Label ID="lblBalanceAmt" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                    <td align="left" width="20%" runat="server" id="tcTAXCode2">
                                        <asp:DropDownList ID="ddlTaxCode" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <asp:TextBox ID="txtDetAmount" runat="server" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator22" runat="server" ErrorMessage="Amount required" ValidationGroup="SUBERROR"
                                            ControlToValidate="txtDetAmount">*</asp:RequiredFieldValidator>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                            ValidChars=".-" TargetControlID="txtDetAmount" />
                                        <br />
                                        <asp:Label ID="lbDescription" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" width="20%" runat="server" id="tcTAXAmt2">
                                        <asp:Label ID="lblTAX_AMOUNT" runat="server" Text="0.00"></asp:Label></td>
                                    <td align="left" width="20%" runat="server" id="tcNETAmt2">
                                        <asp:Label ID="lblNET_AMOUNT" runat="server" Text="0.00"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trTaxAlert">
                                    <td colspan="5" align="left">
                                        <asp:Label ID="lblalert" CssClass="field-label" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="tr_ToStudent1" runat="server" visible="false">
                        <td align="left" colspan="5">
                            <asp:RadioButton ID="radStudCR" runat="server" AutoPostBack="true" Checked="True"
                                GroupName="CR_STUD_TYPE" Text="Student" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="radEnqCR" runat="server" AutoPostBack="true" GroupName="CR_STUD_TYPE"
                                Text="Enquiry" CssClass="field-label"></asp:RadioButton></td>
                    </tr>
                    <tr id="tr_ToStudent2" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Student(CR)</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStudNameCR" runat="server"> </asp:TextBox>
                            <asp:ImageButton ID="ImgCRStud" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetStudent(0);" OnClick="ImgCRStud_Click"></asp:ImageButton><asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator5" runat="server" co="" ControlToValidate="txtStudNameCR"
                                    ErrorMessage="Student Name required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%">
                            <asp:Label ID="lblGradeCR" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr id="tr_ToStudent3" runat="server" visible="false">
                        <td align="left" width="20%"><span class="field-label">Fee Type(CR)</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlFEETYPECR" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Amount</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDetAmount_HH" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDetAmount"
                                ErrorMessage="Amount required" ValidationGroup="SUBERROR">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr valign="top">
                        <td align="left" width="20%"><span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="txtDetRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Detailed Remarks required"
                                ValidationGroup="SUBERROR" ControlToValidate="txtDetRemarks">*</asp:RequiredFieldValidator>
                            <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_GRD_ID_CR" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_STUD_ID" runat="server" />
                            <asp:HiddenField ID="hf_GRD_ID" runat="server" />
                            <asp:HiddenField ID="h_Amount" runat="server" />
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_print" runat="server" />
                            <asp:HiddenField ID="h_CanEdit" runat="server" />
                        </td>
                    </tr>
                    <tr id="tr_Addbutton" runat="server">
                        <td align="center" colspan="4">
                            <asp:Button ID="btnDetAdd" runat="server" CssClass="button" Text="Add" ValidationGroup="SUBERROR" />
                            <asp:Button ID="btnSubCancel" runat="server" CssClass="button" Text="Cancel" />
                        </td>
                    </tr>
                    <tr id="tr_Deatails" runat="server">
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" CssClass="table table-bordered table-row" CellPadding="4">
                                <Columns>
                                    <asp:TemplateField HeaderText="FeeId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fee Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# bind("FEE_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%# bind("DURATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAD_TAX_CODE" HeaderText="Tax Code" />
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# bind("FEE_AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FAD_TAX_AMOUNT" HeaderText="Tax Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:F}" HtmlEncode="false" />
                                    <asp:BoundField DataField="FAD_NET_AMOUNT" HeaderText="Net Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:F}" HtmlEncode="false" />
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# bind("FEE_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" runat="server">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trPrint" runat="server" visible="false">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkPrint" runat="server" Text="Print After Save" Checked="True" CssClass="field-label"></asp:CheckBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" /></td>
                    </tr>
                </table>
                <%-- <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <asp:HiddenField ID="h_StuID" runat="server" />
                <asp:HiddenField ID="h_STUD_ID_CR" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
                    Position="left" TargetControlID="lnkRefundable">
                </ajaxToolkit:PopupControlExtender>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:GridView ID="gvFeePaidHistory" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="FEE_DESCR" HeaderText="FEE Type" />
                            <asp:BoundField DataField="DOCDATE" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False"
                                HeaderText="Date" />
                            <asp:BoundField DataField="PAID_AMT" HeaderText="Paid Amount" />
                            <asp:BoundField DataField="CHARGED_AMT" HeaderText="Charged Amt" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
