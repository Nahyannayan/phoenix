﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class StudentServices_Services_Gen_DeAllocate_F
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("ssv") IsNot Nothing And Request.QueryString("ssv") <> "" Then
                Dim id As Integer = Convert.ToInt32(Request.QueryString("ssv").ToString())
                ViewState("SSV_ID") = id
                BindRequestDetails(id)
                btnRequest.Enabled = True
                hf_bSuccess.Value = 0
            End If
        End If

        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub btnRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRequest.Click
        Dim SSV_ID As String = ViewState("SSV_ID")
        Dim errormsg As String = String.Empty
        If txtDate.Value = "" OrElse Not IsDate(txtDate.Value) Then
            SHOW_MESSAGE("Please input the date of discontinuation")
            Exit Sub
        End If
        If txtRemarks.Value.Trim = "" Then
            SHOW_MESSAGE("Please input the narration")
            Exit Sub
        End If
        If callTransaction(errormsg) <> 0 Then
            SHOW_MESSAGE(errormsg, True)
            btnRequest.Enabled = True
        Else
            SHOW_MESSAGE("Discontinue request saved successfully with discontinue date as " & txtDate.Value, False)
            btnRequest.Enabled = False
        End If
    End Sub

    Sub BindRequestDetails(ByVal Id As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@SSV_ID", Id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.GET_General_SVC_Details ", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)
                If Dt.Rows.Count > 0 Then
                    lblName.Text = Dt.Rows(0)("SNAME").ToString()
                    lblActivity.Text = Dt.Rows(0)("SVC_DESCRIPTION").ToString()
                End If
            End If
        Catch ex As Exception
            SHOW_MESSAGE("Unable to get Discontinuation data!")
        End Try
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer
        Dim tran As SqlTransaction
        Dim SSV_ID As String = ViewState("SSV_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection
            tran = CONN.BeginTransaction("SampleTransaction")
            Try
                Dim param(7) As SqlParameter
                param(0) = New SqlParameter("@SSV_ID", SSV_ID)
                param(1) = New SqlParameter("@UserId", Session("sUsr_id"))
                param(2) = New SqlParameter("@EMP_ID", Convert.ToInt32(Session("EmployeeId")))
                param(3) = New SqlParameter("@DisContinueFrom", Convert.ToDateTime(txtDate.Value))
                param(4) = New SqlParameter("@Remarks", txtRemarks.Value)
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "ST.DISCONTINUE_SINGLE_REQUEST", param)
                Dim ReturnFlag As Integer = param(5).Value

                If ReturnFlag = 0 Then
                    ViewState("SVB_ID") = "0"
                    ViewState("datamode") = "none"
                    callTransaction = "0"
                    hf_bSuccess.Value = 1
                Else
                    callTransaction = ReturnFlag
                    errormsg = UtilityObj.getErrorMessage(ReturnFlag)
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved. !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try
        End Using
    End Function

    Sub SHOW_MESSAGE(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If bError Then
            lblError.CssClass = "diverrorPopUp"
        Else
            lblError.CssClass = "divvalidPopUp"
        End If
        lblError.Text = Message
    End Sub
End Class
