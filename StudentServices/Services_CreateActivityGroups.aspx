﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="Services_CreateActivityGroups.aspx.vb" Inherits="StudentServices_Services_CreateActivityGroups" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
      .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i>
            <asp:Label Text='Create Activity Types/ Create Sub Activity types / Create Groups' ID="lblheading" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Menu ID="mnuMaster" Orientation="Horizontal" runat="server" OnMenuItemClick="mnuMaster_MenuItemClick" CssClass="menu_a">
                                <Items >
                                  <%--  <asp:MenuItem Text="Create Activity Master Type" Value="0" Selected="true" ></asp:MenuItem>--%>
                                    <asp:MenuItem Text="Create Activty Sub Type" Value="1" Selected="true"></asp:MenuItem>
                                    <asp:MenuItem Text="Create Activty Groups for Parent portal" Value="2"></asp:MenuItem>
                                </Items>
                                <StaticMenuItemStyle CssClass="menuItem" />
                                <StaticSelectedStyle CssClass="selectedItem" />
                                <StaticHoverStyle CssClass="hoverItem" />
                            </asp:Menu>
                        </td>
                    </tr>
                    <tr>
                        <td class="panel-cover" width="100%">
                            <asp:MultiView ID="mvMaster" ActiveViewIndex="1" runat="server">
                                <asp:View ID="View1" runat="server" >
                                    <table width="100%" id="tblActType" runat="server">
                                        <tr>
                                            <td colspan="4" class="title-bg">Activity Type Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%"><span class="field-label">Activity Type Name </span>

                                            </td>
                                            <td width="30%">
                                                <asp:TextBox ID="txtGroupName" runat="server" required="required" placeholder="Enter name of group here..." />
                                                <asp:HiddenField ID="hdn_group_id" runat="server" Value="0" />
                                            </td>
                                            <td width="20%"><span class="field-label">Activity Type Description</span></td>
                                            <td width="30%">
                                                <asp:TextBox ID="txtGroupDescr" runat="server" required="required" placeholder="Enter descr of group here..." />
                                            </td>
                                        </tr>
                                        <%-- <tr>
                                            <td width="20%"><span class="field-label">Activity Type Image </span></td>
                                            <td>
                                                <asp:TextBox ID="txtImage" runat="server" required="required" placeholder="Enter activity icon path ..."></asp:TextBox>
                                            </td>
                                            <td width="20%" align="left">
                                                <span an class="field-label">Max Limit of Sub Groups Chosen Per Type </span>
                                            </td>
                                            <td width="30%" align="left">
                                                <asp:TextBox ID="txtGrpLimit" runat="server" placeholder="Maximum limit of groups can be chosen by student.. " required="required" />
                                                <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterType="Numbers, Custom"
                                                    ValidChars="." TargetControlID="txtGrpLimit" />
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td colspan="4">
                                                <asp:GridView runat="server" ID="grdGroup" CssClass="table table-bordered table-row" AllowPaging="true" OnPageIndexChanging="grdGroup_PageIndexChanging"
                                                    EmptyDataText="No rows to show" AutoGenerateColumns="false" PageSize="5">
                                                    <Columns>
                                                        <asp:BoundField DataField="ID" HeaderText="Activity Type Id" />
                                                        <asp:BoundField DataField="NAME" HeaderText="Actvity Type Name" />
                                                        <asp:BoundField DataField="DESCR" HeaderText="Actvity Type Description" />
                                                        <%-- <asp:BoundField DataField="ICON" HeaderText="Associated Activity Icon" />
                                                        <asp:BoundField DataField="MAX" HeaderText="Maximum Limit" />--%>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEdit" runat="server" OnCommand="btnEdit_Command" Text="Edit" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDelete" runat="server" OnCommand="btnDelete_Command" Text="Delete" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" class="title-bg">Sub Activity Type Details
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%" align="left"><span class="field-label">Sub Group Name </span></td>
                                            <td width="30%" align="left">
                                                <asp:TextBox ID="txtSubGroup" runat="server" required="required" placeholder="Enter name of sub group here..."></asp:TextBox>
                                                <asp:HiddenField ID="hdn_subgroup_id" runat="server" Value="0" />
                                            </td>
                                            <td width="20%" align="left"><span class="field-label">Sub Group Description </span></td>
                                            <td width="30%" align="left">
                                                <asp:TextBox ID="txtSubGroupDescr" runat="server" required="required" placeholder="Enter descr of sub group here..." />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%" align="left"><span class="field-label">Select Associated Activity Type </span></td>
                                            <td width="30%" align="left">
                                                <asp:DropDownList ID="ddlGrp" runat="server" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                           <%-- <td width="20%" align="left">
                                                <span class="field-label">Max Limit of Activities Chosen Per Sub Group </span>
                                            </td>
                                            <td width="30%" align="left">
                                                <asp:TextBox ID="txtSubGrpLimit" runat="server" placeholder="Maximum limit of sub groups can be chosen by student.. " required="required" />
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                                    ValidChars="." TargetControlID="txtSubGrpLimit" />--%>
                                            <%--</td>--%>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:GridView runat="server" ID="grdSubGroup" CssClass="table table-bordered table-row" AllowPaging="true" OnPageIndexChanging="grdSubGroup_PageIndexChanging"
                                                    EmptyDataText="No rows to show" AutoGenerateColumns="false" PageSize="5">
                                                    <Columns>
                                                        <asp:BoundField DataField="ID" HeaderText="Sub Group ID " />
                                                        <asp:BoundField DataField="NAME" HeaderText="Sub Group Name" />
                                                        <asp:BoundField DataField="DESCR" HeaderText="Sub Group Description" />
                                                        <asp:BoundField DataField="TYPE" HeaderText="Associated Group" />
                                                       <%-- <asp:BoundField DataField="MAX" HeaderText="Maximum Limit" />--%>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEdit_S" runat="server" OnCommand="btnEdit_S_Command" Text="Edit" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDelete_S" runat="server" OnCommand="btnDelete_S_Command" Text="Delete" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View3" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="20%">
                                                <span class="field-label">Group Name</span>
                                            </td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtGrpName" runat="server"  placeholder ="Group name .."></asp:TextBox>
                                            </td>
                                            <td align="left" width="20%">
                                                <span class="field-label">Group Description</span>
                                            </td>
                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtGrpDescr" runat="server"  placeholder ="Group description .."></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlGrade" runat="server" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Select Section</span></td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlSection" runat="server" OnSelectedIndexChanged ="ddlSection_SelectedIndexChanged" AutoPostBack="true"  ></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="field-label">Select Event</span></td>
                                            <td>
                                                <telerik:RadComboBox ID="ddlEvent" runat="server" CheckBoxes="true" RenderMode="Lightweight" Width="500px" ></telerik:RadComboBox>      
                                                <asp:HiddenField ID="hdn_actgrps" Value="" runat="server" />                                     
                                            </td>
                                            <td><span class="field-label">Select Maximum Limit of this group </span></td>
                                            <td>
                                                <asp:TextBox ID="txtMaxLimit" runat="server" required="reqired" placeholder="Maximum limit of actvites can be chosen by student.. "></asp:TextBox>
                                                 <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom"
                                                    ValidChars="." TargetControlID="txtMaxLimit" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:GridView ID="gvActDetails" runat="server"  CssClass="table table-bordered table-row" AutoGenerateColumns ="false" OnPageIndexChanging="gvActDetails_PageIndexChanging" >
                                                    <Columns>                                                       
                                                        <asp:BoundField DataField="NAME" HeaderText="Event Name" /> 
                                                        <asp:BoundField DataField="DESCRIPTION" HeaderText ="Group Description" />
                                                        <asp:boundfield DataField ="MAX" HeaderText ="Maximum limit" />
                                                        <asp:BoundField DataField ="EVENTS"  HeaderText ="Events coming under group" />                                                  
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkEditact" runat="server" OnCommand ="lnkEditact_Command" CommandArgument='<%# Bind("ID")%>' Text="Edit" ></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDeleteAct" runat="server" OnCommand="lnkDeleteAct_Command" CommandArgument ='<%# Bind("ID")%>'  Text =" Delete"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click" Text="Create" />
                            <br />
                            <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
