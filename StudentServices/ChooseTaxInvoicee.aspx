﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChooseTaxInvoicee.aspx.vb" Inherits="StudentServices_ChooseTaxInvoicee" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../cssfiles/SiteStyle.css" rel="stylesheet" />
    <style type="text/css">
        .InfoLabel {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            background-position: 5px center;
            background: #feffb3 url(../images/Common/PageBody/info_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
            text-align: justify;
        }

        .errLabel {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            background-position: 5px center;
            background: #ffc7c7 url(../images/Common/PageBody/error_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }

        .validLabel {
            display: block;
            border: 1px solid #ccc;
            text-align: left;
            font-family: segoe ui, verdana, arial, sans-serif;
            font-size: 11px;
            font-weight: normal;
            color: #2e2e2e;
            letter-spacing: 1px;
            font-weight: 600;
            background-position: 5px center;
            background: #d1ff83 url(../images/Common/PageBody/success_s.png) no-repeat 2px 3px;
            padding-left: 22px;
            padding-top: 3pt;
            padding-bottom: 3pt;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <table style="margin-top: 30%; margin-left: 10%; margin-right: 10%; width: 80%; border: solid 2px red;">
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lblMessage1" CssClass="InfoLabel" runat="server"></asp:Label>
                    <asp:HiddenField ID="h_print" runat="server" />
                    <asp:HiddenField ID="hfINVTYPE" runat="server" />
                    <asp:HiddenField ID="hfFSHID" runat="server" />
                    <asp:HiddenField ID="F_COMP_TAX_REGISTRATION_NO" runat="server" />
                    <asp:HiddenField ID="M_COMP_TAX_REGISTRATION_NO" runat="server" />
                    <asp:HiddenField ID="COMP_TAX_REGISTRATION_NO" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">Student Id</td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblStuNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Student Name</td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblStuName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Invoice No</td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblInvNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Invoice Generated For
                </td>
                <td style="width: 1px !important;">:</td>
                <td>
                    <asp:RadioButtonList ID="rblInvoicee" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="F">Father</asp:ListItem>
                        <asp:ListItem Value="M">Mother</asp:ListItem>
                        <asp:ListItem Value="FC">Father&#39;s Company</asp:ListItem>
                        <asp:ListItem Value="MC">Mother&#39;s Company</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>Address</td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Tax Registration No</td>
                <td>:</td>
                <td>
                    <asp:Label ID="lblTRNo" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lblMessage2" CssClass="InfoLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lblError" runat="server"></asp:Label></td>
            </tr>
            <tr id="trAnimation1" runat="server" visible="false">
                <td colspan="3" style="text-align: center"><asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /></td>
                </tr>
            <tr id="trAnimation2" runat="server" visible="false">
                <td colspan="3" style="text-align: center">&nbsp;</td>
            </tr>
                <tr>
                    <td colspan="3" style="text-align: center">
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" OnClientClick="return confirm('Are you sure you want to continue?');" />
                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" OnClientClick="parent.fnParent();" />
                    </td>
            </tr>
        </table>
    </form>
</body>
</html>
