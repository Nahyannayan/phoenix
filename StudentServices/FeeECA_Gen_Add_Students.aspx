﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeeECA_Gen_Add_Students.aspx.vb" Inherits="StudentServices_FeeECA_Gen_Add_Students" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
<base target="_self" />
    <title></title>
      <base target="_self" />
        <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <%-- <style>
  .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
    }
    .GridPager a
    {
        background-color: #f5f5f5; 
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }

  </style>--%>
        <script>
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
    </script>
</head>
<body  class="matter" onload="listen_window();" >
    <form id="form1" runat="server">
  <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvStudentsList" CssClass="table table-row table-bordered " runat="server" AutoGenerateColumns="False"  Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="20" SkinID="GridViewView">
                            <Columns>
                                <asp:TemplateField> 
                                    <HeaderTemplate>
                                        Student#<br />
                                                            <asp:TextBox ID="txtStu_NO" runat="server"  ></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchSTU_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchSTU_NO_Click"></asp:ImageButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <EditItemTemplate> 
                                    </EditItemTemplate>
                                    <HeaderTemplate>
                                        Student Name<br />
                                                            <asp:TextBox ID="txtSTU_NAME" runat="server"  ></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchSTU_NAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchSTU_NAME_Click"></asp:ImageButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        &nbsp;<asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("STU_NAME") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Grade<br />
                                                            <asp:TextBox ID="txtSTU_GRD" runat="server"  ></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchSTU_GRD" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchSTU_GRD_Click"></asp:ImageButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField >
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Parent Name<br />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Parent Mobile">
                                    <HeaderTemplate>
                                        
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("PARENT_MOBILE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                              
                            </Columns>
                             <PagerStyle HorizontalAlign = "Right" CssClass = "GridPager" />
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                           <asp:HiddenField ID="hdnSelected" runat="server" />
                        
                    </td>
                </tr>                
               
            </table>
    </form>
</body>
</html>
