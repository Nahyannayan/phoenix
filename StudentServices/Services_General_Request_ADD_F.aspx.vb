﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class StudentServices_Services_General_Request_ADD_F
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("StuNo") = ""
            ViewState("grdID") = ""
            ViewState("bsu_ID") = ""
            ViewState("acdID") = ""
            If Request.QueryString("bsuId") IsNot Nothing And Request.QueryString("bsuId") <> "" Then
                Dim Bsuid As String = Request.QueryString("bsuId").ToString()
                ViewState("bsu_ID") = Bsuid
            End If
            If Request.QueryString("grdID") IsNot Nothing And Request.QueryString("grdID") <> "" Then
                Dim grdID As String = Request.QueryString("grdID").ToString()

                If grdID <> "null" Then
                    ViewState("grdID") = grdID
                Else
                    ViewState("grdID") = "0"
                End If

            End If

            If Request.QueryString("acdID") IsNot Nothing And Request.QueryString("acdID") <> "" Then
                Dim acdID As String = Request.QueryString("acdID").ToString()
                ViewState("acdID") = acdID
            End If

            If Request.QueryString("IsRequested") IsNot Nothing And Request.QueryString("IsRequested") <> "" Then
                Dim IsRequested As String = Request.QueryString("IsRequested").ToString()
                ViewState("IsRequested") = IsRequested
            End If

            If Request.QueryString("StuNo") IsNot Nothing And Request.QueryString("StuNo") <> "" Then
                Dim StuNo As String = Request.QueryString("StuNo").ToString()

                If StuNo <> "You have selected multiple students" Then
                    ViewState("StuNo") = StuNo
                End If

            End If
            gridbind()
            Session("liStudList") = New ArrayList
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReq.PageIndexChanging
        gvReq.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchServiceName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchGender_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim STU_IDs As String = String.Empty

        For Each gvrow As GridViewRow In gvReq.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            If chk IsNot Nothing And chk.Checked Then
                STU_IDs += gvReq.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

            End If
        Next

        ''checking if seat is available

        STU_IDs = STU_IDs.Trim(",".ToCharArray())
        hdnSelected.Value = STU_IDs
        Session("liStudList") = STU_IDs

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('hdnSelected').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.ReturnValue ='" & hdnSelected.Value & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & hdnSelected.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub
  

    Private Sub gridbind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim ds As DataSet
            Dim txtSearch As New TextBox
            Dim str_query As String = String.Empty
            Dim STU_NO As String = String.Empty
            Dim str_STU_NO As String = String.Empty
            Dim STUD_NO_FILTER = String.Empty
            Dim SNAME As String = String.Empty
            Dim str_SNAME As String = String.Empty

            Dim GENDER As String = String.Empty
            Dim str_GENDER As String = String.Empty


            Dim STU_SERVICES As String = String.Empty
            Dim str_STU_SERVICES As String = String.Empty
            Dim REQ_SERVICES As String = String.Empty

            Dim GRD_SCT As String = String.Empty
            Dim str_GRD_SCT As String = String.Empty
            Dim FILTER_COND As String = String.Empty

            Dim PARAM(6) As SqlParameter


            If gvReq.Rows.Count > 0 Then


                txtSearch = gvReq.HeaderRow.FindControl("txtSTU_NO")

                If txtSearch.Text.Trim <> "" Then
                    STU_NO = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NO = txtSearch.Text.Trim
                End If


                txtSearch = gvReq.HeaderRow.FindControl("txtSNAME")

                If txtSearch.Text.Trim <> "" Then
                    SNAME = " AND SNAME like '%" & txtSearch.Text.Trim & "%'"
                    str_SNAME = txtSearch.Text.Trim
                End If



                txtSearch = gvReq.HeaderRow.FindControl("txtGender")

                If txtSearch.Text.Trim <> "" Then
                    GENDER = " AND GENDER like '" & txtSearch.Text.Trim & "%'"
                    str_GENDER = txtSearch.Text.Trim
                End If
                'txtSearch = gvReq.HeaderRow.FindControl("txtServiceName")

                'If txtSearch.Text.Trim <> "" Then
                '    STU_SERVICES = " AND STU_SERVICES like '%" & txtSearch.Text.Trim & "%'"
                '    str_STU_SERVICES = txtSearch.Text.Trim
                'End If

                If ViewState("lnkStatus") = "P" Then
                    REQ_SERVICES = " AND SSR_APPR_FLAG = 'P' "
                    ViewState("lnkStatus") = ""
                End If

                If ViewState("lnkStatus") = "A" Then
                    REQ_SERVICES = " AND SSR_APPR_FLAG = 'A' "
                    ViewState("lnkStatus") = ""
                End If

                If ViewState("lnkStatus") = "R" Then
                    REQ_SERVICES = " AND SSR_APPR_FLAG = 'R' "
                    ViewState("lnkStatus") = ""
                End If
            End If


            If ViewState("StuNo").Trim <> "" Then
                STUD_NO_FILTER = " AND replace(STU_NO,' ','') like '%" & ViewState("StuNo").Trim.Replace(" ", "") & "%'"
            End If

            FILTER_COND = STU_NO + SNAME + GENDER + STU_SERVICES + GRD_SCT + REQ_SERVICES + STUD_NO_FILTER
            PARAM(0) = New SqlParameter("@ACD_ID", ViewState("acdID"))
            PARAM(1) = New SqlParameter("@GRD_ID", ViewState("grdID"))
            PARAM(2) = New SqlParameter("@BSU_ID", ViewState("bsu_ID"))
            PARAM(3) = New SqlParameter("@FILTER_COND", FILTER_COND)
            PARAM(4) = New SqlParameter("@IsRequested", ViewState("IsRequested"))
            'PARAM(5) = New SqlParameter("@SVG_ID", ddlCategory.SelectedValue)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.GETSTUD_GRIDBIND_FORGENERAL_SVC", PARAM)

            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                gvReq.DataSource = ds.Tables(0)
                gvReq.DataBind()

                'btnSave.Enabled = True
                'btnCancel.Enabled = True
            Else
                Dim ds2 As DataSet = New DataSet()

                ds2.Tables(0).Rows.Add(ds2.Tables(0).NewRow())
                gvReq.DataSource = ds2.Tables(0)
                Try
                    gvReq.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvReq.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvReq.Rows(0).Cells.Clear()
                gvReq.Rows(0).Cells.Add(New TableCell)
                gvReq.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReq.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReq.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If



            txtSearch = gvReq.HeaderRow.FindControl("txtSTU_NO")
            txtSearch.Text = str_STU_NO

            txtSearch = gvReq.HeaderRow.FindControl("txtSNAME")
            txtSearch.Text = str_SNAME
            txtSearch = gvReq.HeaderRow.FindControl("txtServiceName")
            txtSearch.Text = str_STU_SERVICES
            txtSearch = gvReq.HeaderRow.FindControl("txtGender")
            txtSearch.Text = str_GENDER
            txtSearch = gvReq.HeaderRow.FindControl("txtGRD_SCT")
            txtSearch.Text = str_GRD_SCT


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
