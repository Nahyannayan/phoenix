﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports Telerik
Partial Class StudentServices_ActivityDetails_PrintCopy
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            Dim refid As Integer = 0

            If Request.QueryString("refid") <> "" Then
                refid = Encr_decrData.Decrypt(Request.QueryString("refid").Replace(" ", "+"))
            Else
                refid = 0
            End If

            'Populating a DataTable from database.
            Dim ds As DataSet = Me.GetData(refid)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'Building an HTML string.
                    Dim html As New StringBuilder()
                    Dim dt As DataTable = ds.Tables(0)
                    'Table start.
                    html.Append("<table width='100%' class='table table-bordered table-row'>")

                    'Building the Header row.
                    html.Append("<tr>")
                    For Each column As DataColumn In dt.Columns
                        html.Append("<th colspan='2'>")
                        html.Append(column.ColumnName)
                        html.Append("</th>")
                    Next
                    html.Append("</tr>")

                    'Building the Data rows.
                    For Each row As DataRow In dt.Rows
                        html.Append("<tr>")
                        For Each column As DataColumn In dt.Columns
                            html.Append("<td colspan='2'>")
                            html.Append(row(column.ColumnName))
                            html.Append("</td>")
                        Next
                        html.Append("</tr>")
                    Next

                    'Table end.
                    html.Append("</table>")

                    'Append the HTML string to Placeholder.
                    PlaceHolder1.Controls.Add(New Literal() With { _
                       .Text = html.ToString() _
                     })
                End If
            End If
           
        End If

    End Sub

    Private Function GetData(ByVal refid As Int64) As DataSet
        Dim constr As String = ConnectionManger.GetOASISConnectionString     
        Dim ds As DataSet
        Dim param(1) As SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@ALD_ID", refid, SqlDbType.BigInt)
        ds = SqlHelper.ExecuteDataset(constr, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_PRINT_DETAILS]", param)
        Return ds
    End Function
End Class
