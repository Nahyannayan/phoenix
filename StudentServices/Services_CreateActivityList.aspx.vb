﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.UI.WebControls
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Imports Telerik.Web.UI

Partial Class StudentServices_Services_CreateActivityList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        ' smScriptManager.RegisterPostBackControl(btnfilesave)
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    ' Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Else
                        ViewState("ViewId") = ""
                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)

                    'COMMON METHODE FOR ALL PAGES

                    ' rdFrm.Attributes.Add("onClick", "TPick('" & rdFrm.ClientID & "')")
                    '  rdTo.Attributes.Add("onClick", "TPick('" & rdTo.ClientID & "')")
                    ' rdFrm.Attributes.Add("onMouseDown", "TPick('" & rdFrm.ClientID & "')")
                    ' rdTo.Attributes.Add("onMouseDown", "TPick('" & rdTo.ClientID & "')")

                    lnkMoreInfo.Attributes.Add("onmouseover", "fnMouseOver(" & divMore.ClientID & "); return false;")
                    lnkMoreInfo.Attributes.Add("onmouseout", "fnMouseOut(" & divMore.ClientID & "); return false;")
                    lnkMoreInfo1.Attributes.Add("onmouseover", "fnMouseOver(" & divMore1.ClientID & "); return false;")
                    lnkMoreInfo1.Attributes.Add("onmouseout", "fnMouseOut(" & divMore1.ClientID & "); return false;")
                    lnkMoreInfo2.Attributes.Add("onmouseover", "fnMouseOver(" & divMore2.ClientID & "); return false;")
                    lnkMoreInfo2.Attributes.Add("onmouseout", "fnMouseOut(" & divMore2.ClientID & "); return false;")

                    fillTranAcdY()
                    fillActivityTypes()
                    fillFeeTypes()
                    'fillOtherTypes()
                    fillSubGroups()


                    '  btnAddGroup.Attributes.Add("OnClick", "return ShowWindowWithClose('Services_CreateActivityGroups.aspx?ACD_ID=" & Encr_decrData.Encrypt(ddlACDYear.SelectedValue) & "', 'Create Groups', '80%', '90%');")
                    'CONTROL BUTTON COPY FROM PREVIOUS ACTIVITES - INITIALISING
                    btnCopy.Attributes.Add("OnClick", "return ShowWindowWithClose('OldActivityList.aspx?ACD_ID=" & Encr_decrData.Encrypt(ddlACDYear.SelectedValue) & "', 'Old Activity List', '30%', '50%');")
                    'CALLING ACCESSRIGHT TO SET PAGE BUTTONS 
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
                    'GETTING EMPLOYEEID AND EMPLOYEE PASSPORT NAME
                    ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))
                    lblAED.InnerText = Session("BSU_CURRENCY") + " (inclusive of VAT)"
                    'IF THE MENU CODE IS FOR CREATE ACTIVITY - INITIATOR
                    If ViewState("MainMnu_code") = "S108915" Then
                        If (ViewState("datamode") = "add") Then
                            InitiatorActivityLogginScreen()
                            PopulateGrade()
                            radioButtonList.Items("1").Selected = True
                        ElseIf (ViewState("datamode") = "view") Then
                            GetActivityViewForinitiator(ViewState("ViewId"))
                        End If
                        'IF THE MENU IS FOR THE FINANCE DEPARTMENT
                    ElseIf ViewState("MainMnu_code") = "S102301" Then
                        If (ViewState("datamode") = "approve") Then
                            lblheading.Text = "Activity Request For Approval"
                            GetApprovalDetlsForFinance()
                        ElseIf (ViewState("datamode") = "view") Then

                            lblheading.Text = "View Activity Request"
                            GetActivityViewForFinance(ViewState("ViewId"))
                        End If
                        'ELSE REDIRECT TO HOME PAGE
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        Else
           
        End If
        smScriptManager.RegisterPostBackControl(btnSave)
    End Sub

#Region "Common_Methodes"
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
    Sub ClearSchedule()
        If (tblScdl.Visible) Then
            chkWeekly.ClearSelection()
            rdFrm.Text = ""
            rdTo.Text = ""
            txtSchlDescr.Text = ""
            txtSchlCount.Text = ""
        End If       
    End Sub
   

    Private Sub CLEARALL()
        'TO CLEAR ALL FIELDS
        Try
            txtComments1.InnerText = ""
            txtEventDesc.InnerText = ""
            txtEventName.Text = ""
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
            btnDelete.Enabled = True
            ShowMessage("", False)
            txtAmount.Text = "0.00"
            fillActivityTypes()
            fillSubGroups()
            ClearSchedule()
            gvSchdl.DataSource = ""
            gvSchdl.DataBind()
            gvSchdl.Visible = False
            rdSchdl.SelectedValue = 0
            chkNotAllow.SelectedValue = 0
            If Not ViewState("Schdl_Dt") Is Nothing Then
                Dim dt As DataTable = ViewState("Schdl_Dt")
                If dt.Rows.Count > 0 Then
                    dt.Clear()
                End If
                ViewState("Schdl_Dt") = dt
            End If          
            tblScdl.Visible = False          
            chkAutoEnroll.Checked = False
            txtApplEnd.Text = Nothing
            txtApplSt.Text = Nothing
            txtEventSt.Text = Nothing
            txtEventEnd.Text = Nothing
            txtCount.Text = 0
            lblInitiatorLogDt.Text = ""
            lblFinancLogDt.Text = ""
            ' txtEmailTemplate.Content = ""
            PopulateGrade()
            chkAutoEnroll_CheckedChanged(Nothing, Nothing)

            rdoAllowMultiple.SelectedValue = "No"
            rdoAllowMultiple_SelectedIndexChanged(Nothing, Nothing)
            ' txtallowcount.Visible = False
            radoDoTerms.SelectedValue = 0
            radoDoTerms_SelectedIndexChanged(Nothing, Nothing)
            radioDoEmail.SelectedValue = 0
            radioDoEmail_SelectedIndexChanged(Nothing, Nothing)

        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Clearall: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
   
    Private Sub fillSubGroups()
        Try
            ddlSubGrp.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim params(2) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@GRP_ID", ddlActivtyTypes.SelectedValue, SqlDbType.BigInt)
            params(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("Sbsuid"), SqlDbType.VarChar)
            Dim dsGroups As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_ALL_SUBGROUPS_FOR_ACTIVITY]", params)
            If dsGroups.Tables(0).Rows.Count > 0 Then
                ddlSubGrp.DataSource = dsGroups
                ddlSubGrp.DataTextField = "SUBGROUP_NAME"
                ddlSubGrp.DataValueField = "SUBGROUP_ID"
                ddlSubGrp.DataBind()
            End If
            ddlSubGrp.Items.Insert(0, New ListItem("SELECT", 0))
            ddlSubGrp.ClearSelection()
            ddlSubGrp.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillSubGroups: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Sub fillActivityTypes()
        'FILLING ACTIVITY TYPES TO ACTIVITY DROP DOWN LIST
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_ALL_ACTIVITY_TYPES]")
            ddlActivtyTypes.DataSource = dsActivityTypes.Tables(0)
            ddlActivtyTypes.DataBind()
            ddlActivtyTypes.DataTextField = "ACTIVITY_NAME"
            ddlActivtyTypes.DataValueField = "ACTIVITY_ID"
            ddlActivtyTypes.DataBind()
            ddlActivtyTypes.Items.Insert(0, New ListItem("SELECT", 0))
            ddlActivtyTypes.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillActivityTypes: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Private Sub fillFeeTypes()
        'METHODE TO FILL FEE TYPES
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim Param(1) As SqlParameter
            'Param(0) = Mainclass.CreateSqlParameter("@CheckOption", 1, SqlDbType.Int)
            Dim Param(2) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@CheckOption", 3, SqlDbType.Int)
            Param(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("Sbsuid"), SqlDbType.VarChar)
            Dim dsFeeTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_REGISTRATION_FEE_DETAILS]", Param)
            ddlFeetype.DataSource = dsFeeTypes.Tables(0)
            ddlFeetype.DataTextField = "DESCR"
            ddlFeetype.DataValueField = "ID"
            ddlFeetype.DataBind()
            ddlFeetype.Items.Insert(0, New ListItem("SELECT", 0))
            ddlFeetype.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From FillFeetypes: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Private Sub fillGrades()
        'FILL GRADES IN GRIDVIEW - IN VIEW AND FINANCE PAGE
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_GRADE_DETAILS_TOSHOW]", Param)
            Dim dt As DataTable = DS.Tables(0)
            For Each mrow In dt.Rows
                Dim flag As Boolean = Checkallgradesselected(mrow("SECTIONS"), mrow("GRADE"))
                If (flag = 0) Then
                    mrow("SECTIONS") = "ALL SELECTED"
                Else

                End If
            Next
            gvGrades.DataSource = dt
            gvGrades.DataBind()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillGrades: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Function Checkallgradesselected(ByVal ROESECTIONS As String, ByVal GRADE As String) As Boolean
        'METHODE TO CHECK WHETHER ALL GRADES SELECTED OR NOT
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(2) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.BigInt)
        Param(2) = Mainclass.CreateSqlParameter("@GRADE_ID", GRADE, SqlDbType.VarChar)
        Dim Sections As String = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[CHECK_ALL_GRADES_INSCHOOL]", Param)
        If Not IsDBNull(Sections) Then
            If (Sections = ROESECTIONS) Then
                Return 0
            Else
                Return 1
            End If
        Else
            Return 1
        End If
    End Function

    Protected Sub fillTripsDropdownlist()
        '
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(2) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@REF_ID", ddlActivtyTypes.SelectedValue, SqlDbType.BigInt)
            Param(1) = Mainclass.CreateSqlParameter("@CNTY_ID", Session("BSU_COUNTRY_ID"), SqlDbType.BigInt)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OASIS.GET_SUBACT_TYPES_DETAILS", Param)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    rowtrips.Visible = True
                    ddlTripsList.DataSource = ds
                    ddlTripsList.DataTextField = "ATM_NAME"
                    ddlTripsList.DataValueField = "ATM_ID"
                    ddlTripsList.DataBind()
                    ddlTripsList.AllowCustomText = True
                    ddlTripsList.MarkFirstMatch = True
                    txtSubActivityName.Focus()
                Else
                    rowtrips.Visible = False
                End If
            End If

        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillTripsDropdownlist :" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Sub fillTranAcdY()
        'FILL ACADEMIC YEAR IN DROP DOWN LIST
        Try
            Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
            ddlACDYear.DataSource = dtACD
            ddlACDYear.DataTextField = "ACY_DESCR"
            ddlACDYear.DataValueField = "ACD_ID"
            ddlACDYear.DataBind()
            For Each rowACD As DataRow In dtACD.Rows
                If rowACD("ACD_CURRENT") Then
                    ddlACDYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillTranAcdY :" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region

#Region "Control_Methodes"
    Protected Sub gvTrRateImport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            e.Row.Cells(0).ForeColor = Drawing.Color.Orange
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub ddlSubGrp_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub
    Protected Sub txtcount_TextChanged(sender As Object, e As EventArgs)

    End Sub
    Protected Sub btnSchdl_Click(sender As Object, e As EventArgs)
        Try

            Dim dt As DataTable = New DataTable
            Dim DT_EXIST As DataTable
            If ViewState("Schdl_Dt") Is Nothing Then
                DT_EXIST = New DataTable
            Else
                DT_EXIST = ViewState("Schdl_Dt")
            End If

            dt.Columns.Add("Weeks")
            dt.Columns.Add("TimeFrom")
            dt.Columns.Add("TimeTo")
            dt.Columns.Add("Description")
            dt.Columns.Add("Count")
            Dim WeekDetails As String = ""

            For i As Integer = 0 To chkWeekly.Items.Count - 1
                If chkWeekly.Items(i).Selected Then
                    If WeekDetails = "" Then
                        WeekDetails = chkWeekly.Items(i).Text
                    Else
                        WeekDetails = WeekDetails + ", " + chkWeekly.Items(i).Text
                    End If
                End If
            Next

            If (WeekDetails = "") Then
                lblError.Text = "Week details in schedule creation should not be empty"
                Exit Sub
            ElseIf txtSchlCount.Text = "" Then
                lblError.Text = "Count should not be empty while creating schedule"
                Exit Sub
            End If

            dt.Rows.Add(WeekDetails, rdFrm.Text, rdTo.Text, txtSchlDescr.Text, txtSchlCount.Text)


            If DT_EXIST.Rows.Count = 0 Then
                gvSchdl.DataSource = dt
                gvSchdl.DataBind()
                ViewState("Schdl_Dt") = dt
                ClearSchedule()
            Else
                Dim dt_p As DataTable = ViewState("Schdl_Dt")
                Dim flag As Boolean = False
                For Each row In dt_p.Rows
                    If row("Weeks") = WeekDetails Then
                        flag = True
                        Exit For
                    Else
                    End If
                Next
                If (flag = True) Then
                    ShowMessage("Same schedule has added already", True)
                    Exit Sub
                Else
                    dt.Merge(ViewState("Schdl_Dt"))
                End If
                gvSchdl.DataSource = dt
                gvSchdl.DataBind()
                ViewState("Schdl_Dt") = dt
                ClearSchedule()

            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub chkrevnueprd_CheckedChanged(sender As Object, e As EventArgs)
        If (chkrevnueprd.Checked) Then
            txtRevnDt.Enabled = False
            txtRevnDt.Text = ""
        Else
            txtRevnDt.Enabled = True
        End If
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Try
            CLEARALL()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnAdd_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub ddlActivtyTypes_SelectedIndexChanged(sender As Object, e As EventArgs)
        rowViewfiles.Visible = False
        fillSubGroups()
        txtSubActivityName.Text = ""
        onloaddocumentupload(Session("sBsuid"), ddlActivtyTypes.SelectedValue)
        ' If ddlActivtyTypes.SelectedValue = 11 Or ddlActivtyTypes.SelectedValue = 12 Or ddlActivtyTypes.SelectedValue = 13 Then
        fillTripsDropdownlist()
        ' Else
        '' rowtrips.Visible = False
        ' End If
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim strans As SqlTransaction = Nothing
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        Try
            Dim objConn As New SqlConnection(st_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim Param(0) As SqlParameter
            If ViewState("MainMnu_code") = "S108915" Then
                If (ViewState("datamode") = "add") Then
                    Param(0) = Mainclass.CreateSqlParameter("@viewid", Convert.ToInt32(hidlatestrefid.Value), SqlDbType.Int)
                ElseIf (ViewState("datamode") = "view") Then
                    Param(0) = Mainclass.CreateSqlParameter("@viewid", ViewState("ViewId"), SqlDbType.Int)
                End If
            End If
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[DELETE_OASIS_ACTIVITY]", Param)
            strans.Commit()

            'visibility hidden for the below buttons after delete
            DeleteAndUpdateNone()

            ' 'Showing error message "OO.. The Activity Got Deleted"
            Message = getErrorMessage("4028")
            ShowMessage(Message, True)
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnDelete_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub radioDoEmail_SelectedIndexChanged(sender As Object, e As EventArgs)
        If radioDoEmail.SelectedValue = 1 Then
            rowEmailTemplate.Visible = True
        Else
            rowEmailTemplate.Visible = False
        End If
    End Sub
    Protected Sub radoDoTerms_SelectedIndexChanged(sender As Object, e As EventArgs)
        If radoDoTerms.SelectedValue = 1 Then
            rowtermstemplate.Visible = True
        Else
            rowtermstemplate.Visible = False
        End If
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs)
        Try
            'enabling the disabled controls in the page
            txtEventName.Enabled = True
            txtEventDesc.Disabled = False
            ddlActivtyTypes.Enabled = True
            txtSubActivityName.Enabled = True
            ddlACDYear.Enabled = True
            txtApplSt.Enabled = True
            txtApplEnd.Enabled = True
            txtEventSt.Enabled = True
            txtEventEnd.Enabled = True
            txtAmount.Enabled = True
            radioButtonList.Enabled = True
            txtComments1.Disabled = True
            btnUpdate.Visible = False
            txtComments1.Disabled = False
            rdoAllowMultiple.Enabled = True
            rdGender.Enabled = True
            ddlSubGrp.Enabled = True
            gvSchdl.Columns(5).Visible = True
            rdSchdl.Enabled = True
            If rdSchdl.SelectedValue = 0 Then
                tblScdl.Visible = False
                chkNotAllow.Enabled = False
            Else
                tblScdl.Visible = True
                chkNotAllow.Enabled = True
            End If
            '  rowAllowMultiple.Visible = True
            txtCount.Enabled = True
            If rdoAllowMultiple.SelectedValue = "Yes" Then
                txtallowcount.Enabled = True
            End If

            'enabling email template for editing
            If (radioDoEmail.SelectedValue) Then
                rowEmailTemplate.Visible = True
            Else
                rowEmailTemplate.Visible = False
            End If
            radioDoEmail.Enabled = True
            If radoDoTerms.SelectedValue Then
                rowtermstemplate.Visible = True
            Else
                rowtermstemplate.Visible = False
            End If
            radoDoTerms.Enabled = True

            rowMailView.Visible = False
            rowTermsView.Visible = False
            chkAutoEnroll.Enabled = True
            'enabling submit button for updating the page
            btnSave.Visible = True
            btnSave.Text = "Submit"
            If chkAutoEnroll.Checked Then
                chkAutoEnroll_CheckedChanged(Nothing, Nothing)
            End If
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnUpdate_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub btnReject_Click(sender As Object, e As EventArgs)
        Try
            If ViewState("MainMnu_code") = "S102301" Then
                If txtComments2.InnerText Is Nothing Or txtComments2.InnerText = "" Then
                    Message = getErrorMessage("4022")
                    ShowMessage(Message, True)
                Else
                    Insert_Reject_Details()
                End If
            End If

        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnReject_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Protected Sub btnRevert_Click(sender As Object, e As EventArgs)
        Try
            Revert_Item_To_Initiator()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnRevert_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
    Protected Sub rdSchdl_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (rdSchdl.SelectedValue = 1) Then
            tblScdl.Visible = True
            gvSchdl.Visible = True
            ' rowAllowMultiple.Visible = True
            chkNotAllow.Enabled = True
        Else
            tblScdl.Visible = False
            gvSchdl.Visible = False
            ' rowAllowMultiple.Visible = False
            chkNotAllow.Enabled = False
        End If
    End Sub

    Protected Sub btnSchdDelete_Command(sender As Object, e As CommandEventArgs)
        Dim dt_schl As DataTable = ViewState("Schdl_Dt")
        For Each row In dt_schl.Rows
            If (row("Weeks") = e.CommandArgument) Then
                dt_schl.Rows.Remove(row)
                gvSchdl.DataSource = dt_schl
                gvSchdl.DataBind()
                ViewState("Schdl_Dt") = dt_schl
                Exit Sub
            End If
        Next
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        Try
            Dim URL As String = Nothing
            If ViewState("MainMnu_code") = "S108915" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                URL = String.Format("~/StudentServices/ListReportView_StudentServices.aspx?MainMnu_code={0}", ViewState("MainMnu_code"))
                Response.Redirect(URL)
            ElseIf ViewState("MainMnu_code") = "S102301" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                URL = String.Format("~/StudentServices/ListReportView_StudentServices.aspx?MainMnu_code={0}", ViewState("MainMnu_code"))
                Response.Redirect(URL)
            ElseIf ViewState("MainMnu_code") = "S103301" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                URL = String.Format("~/StudentServices/ListReportView_StudentServices.aspx?MainMnu_code={0}", ViewState("MainMnu_code"))
                Response.Redirect(URL)
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
        Catch ex As Exception
            Dim a As String = ex.Message
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnCancel_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub ddlACDYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            PopulateGrade()
            txtApplEnd.Text = Nothing
            txtApplSt.Text = Nothing
            txtEventSt.Text = Nothing
            txtEventEnd.Text = Nothing
            btnCopy.Attributes.Add("OnClick", "return ShowWindowWithClose('OldActivityList.aspx?ACD_ID=" & Encr_decrData.Encrypt(ddlACDYear.SelectedValue) & "', 'Old Activity List', '30%', '50%');")
            fillActivityTypes()
            ddlActivtyTypes_SelectedIndexChanged(Nothing, Nothing)
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From ddlACDYear_SelectedIndexChanged: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub showrevdtdetails()
        lblrevenuedt.Visible = True
        'colRevendt.Visible = True
        txtRevnDt.Visible = True
        chkrevnueprd.Visible = True
        rowPaymentMode.Visible = True
    End Sub

    Protected Sub radlistFee_SelectindexChanged(sender As Object, e As EventArgs)
        Try
            If (radlistFee.SelectedItem.Text = "Fee collection") Then
                If (Convert.ToDouble(txtAmount.Text) > 0) Then
                    showrevdtdetails()
                    lblFeeType.Visible = True
                    ' ddlFeetype.Visible = True
                    '  hideothercolldetails()
                    '  ddlTAXCode.Visible = False
                Else
                    Message = "Amount should be greater than 0"
                    ShowMessage(Message, True)
                End If
            ElseIf (radlistFee.SelectedItem.Text = "Other collection") Then
                If (Convert.ToDouble(txtAmount.Text) > 0) Then
                    rowPaymentMode.Visible = True
                    'chklistPaymentMode.Items(0).Enabled = False
                    lblFeeType.Visible = True
                    ' ddlFeetype.Visible = False
                    ' lblOtheract.Visible = True
                    ' ddlOtherColl.Visible = True
                    lblrevenuedt.Visible = False
                    ' colRevendt.Visible = False
                    txtRevnDt.Visible = False
                    chkrevnueprd.Visible = False
                Else
                    Message = "Amount should be greater than 0"
                    ShowMessage(Message, True)
                End If
            Else
                'radlistFee.Enabled = False 'Commented by vikranth on 22nd Dec 2019
                txtAmount.Text = "0.00"
                lblrevenuedt.Visible = False
                '   colRevendt.Visible = False
                txtRevnDt.Visible = False
                chkrevnueprd.Visible = False
                rowPaymentMode.Visible = False
                chklistPaymentMode.Items(0).Selected = False
                chklistPaymentMode.Items(1).Selected = False
                lblFeeType.Visible = False
                ddlFeetype.Visible = False
                rowShowSplitAmount.Visible = False
                lblVatDescr.Text = ""
                'hideothercolldetails()
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From radlistFee_SelectindexChanged: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Function getTaxDetails() As DataSet
        Dim bIncludingVAT As Integer = 1 'IIf(DirectCast(Session("BSU_bVATEnabled"), Boolean) = True, 1, 0)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT a.*,b.TAX_DESCR FROM TAX.GetTAXCodeAndAmount_EXT('FEES','" & Session("sBsuId") & "','" & "FEE" & "','" & ddlFeetype.SelectedValue & "','" & System.DateTime.Now & "'," & GetDoubleVal(txtAmount.Text) & ",''," & bIncludingVAT & ") a LEFT JOIN oasis.tax.TAX_CODES_M b on  a.TAX_CODE =b.tax_code") 'Left join table added by vikranth on 22nd Dec 2019, suggested by Shakeel
        Return ds
    End Function
    Protected Sub ddlFeetype_SelectedIndexChanged(sender As Object, e As EventArgs)

        If ddlFeetype.SelectedValue = 0 Then
            rowShowSplitAmount.Visible = False
            lblVatDescr.Text = ""
        Else
            rowShowSplitAmount.Visible = True
            Dim DS As DataSet = getTaxDetails()
            If DS.Tables.Count > 0 Then
                txtAmountExlTax.Text = DS.Tables(0).Rows(0)("INV_AMOUNT")
                txttax.Text = DS.Tables(0).Rows(0)("TAX_AMOUNT")
                txtNetamnt.Text = DS.Tables(0).Rows(0)("NET_AMOUNT")
                lblVatDescr.Text = " (VAT Code: " + DS.Tables(0).Rows(0)("TAX_CODE") + " )"
                hidFeeVatCode.Value = DS.Tables(0).Rows(0)("TAX_CODE")
                txtVatCode.Text = DS.Tables(0).Rows(0)("TAX_DESCR") 'Added by vikranth on 22nd dec 2019
            End If
            ' select * from [TAX].[GetTAXCodeAndAmount_EXT] ('FEES','125017','FEE', 5, GETDATE(),100,'',1)

        End If
    End Sub
    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs)
        If ViewState("MainMnu_code") = "S102301" Then
            If (GetDoubleVal(txtAmount.Text) > 0) Then
                radlistFee.Items.FindByText("Fee collection").Selected = True
                radlistFee.Items.FindByText("Register without fee collection").Selected = False
                radlistFee.Items.FindByText("Other collection").Selected = False
                showrevdtdetails()
                radlistFee.Enabled = True
                ddlFeetype.Visible = True
                lblFeeType.Visible = True
                ddlFeetype_SelectedIndexChanged(Nothing, Nothing)
            Else
                radlistFee.Items.FindByText("Register without fee collection").Selected = True
                rowShowSplitAmount.Visible = False
                lblVatDescr.Text = ""
                radlistFee.Enabled = False
                ddlFeetype.Visible = False
                ' ddlOtherColl.Visible = False
                lblrevenuedt.Visible = False
                'colRevendt.Visible = False
                txtRevnDt.Visible = False
                chkrevnueprd.Visible = False
                lblFeeType.Visible = False
                ' hideothercolldetails()
            End If
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Try
            Dim ApplStDt As Date
            Dim ApplEndDt As Date
            Dim EventStartDt As Date
            Dim EventEndDt As Date

            If ViewState("MainMnu_code") = "S108915" Then
                If (gvSchdl.Rows.Count > 0) Then
                    Dim count As Integer = 0, maxcount As Integer = Convert.ToInt64(txtCount.Text)
                    For Each row In gvSchdl.Rows
                        count = count + Convert.ToInt64(row.Cells(4).Text)
                    Next
                    If count <> maxcount Then
                        lblError.Text = "Maximum student count is not tallying with count listed in schedule and max count for activity"
                        Exit Sub
                    End If
                End If

                If ((txtEventName.Text = "" Or txtEventName.Text Is Nothing) Or
                    (txtEventDesc.InnerText Is Nothing Or txtEventDesc.InnerText = "") Or
                    (txtApplSt.Text Is Nothing Or txtApplSt.Text = "") Or
                    (txtApplEnd.Text Is Nothing Or txtApplEnd.Text = "") Or
                    (txtEventSt.Text Is Nothing Or txtEventEnd.Text = "") Or
                    (txtAmount.Text Is Nothing Or txtAmount.Text = "") Or
                    (txtComments1.InnerText Is Nothing Or txtComments1.InnerText = "") Or
                    (rowtrips.Visible = True AndAlso h_SubActivity_ID.Value = "")) Then
                    ' lblError.Text = "Please fill all mandatory fields"
                    Message = getErrorMessage("4029")
                    ShowMessage(Message, True)
                ElseIf Not (txtEmailTemplate.Content.ToString.IndexOf("'") < 0) Then
                    Message = "Email should not contain special charecter '"
                    ShowMessage(Message, True)
                Else
                    Dim Refid As Integer = 0
                    Try
                        ApplStDt = DateTime.Parse(txtApplSt.Text.Trim())
                        ApplEndDt = DateTime.Parse(txtApplEnd.Text.Trim())
                        EventStartDt = DateTime.Parse(txtEventSt.Text.Trim())
                        EventEndDt = DateTime.Parse(txtEventEnd.Text.Trim())
                    Catch ex As Exception
                        ApplStDt = "#1/1/1900#"
                        ApplEndDt = "#1/1/1900#"
                        EventStartDt = "#1/1/1900#"
                        EventEndDt = "#1/1/1900#"
                    End Try
                    If (ApplEndDt = "#1/1/1900#" Or ApplStDt = "#1/1/1900#" Or EventEndDt = "#1/1/1900#" Or EventStartDt = "#1/1/1900#") Then
                        Message = getErrorMessage("4030")
                        ShowMessage(Message, True)
                    Else
                        If ddlActivtyTypes.SelectedItem.Text = "SELECT" Then
                            ' Showing Error Message : Please select Activity Type
                            Message = getErrorMessage("4034")
                            ShowMessage(Message, True)
                            'ElseIf (ApplEndDt < ApplStDt) Or (ApplStDt < Today) Then
                        ElseIf (ApplEndDt < ApplStDt) Then
                            'Showing Error Message : Application End Date Is Less Than Application Start Date "
                            Message = getErrorMessage("4012")
                            ShowMessage(Message, True)
                            ' ElseIf (EventEndDt < EventStartDt) Or (EventStartDt < Today) Then
                        ElseIf (EventEndDt < EventStartDt) Then
                            ' Showing Error Message : Event End Date Is Less Than Event Start Date "
                            Message = getErrorMessage("4013")
                            ShowMessage(Message, True)
                        ElseIf (EventStartDt < ApplStDt) Then
                            'Showing Error Message : Event Start Date Should Not Be Less Than 'Application Request Submission Start Date' "
                            Message = getErrorMessage("4014")
                            ShowMessage(Message, True)
                        ElseIf (Not IsNumeric(txtCount.Text)) Or (GetDoubleVal(txtCount.Text) = 0) Then
                            Message = "Please enter valid count of maximum allowed students for this activity."
                            ShowMessage(Message, True)
                            'ElseIf (rdoAllowMultiple.SelectedValue = "Yes") Then
                            '  Message = "Allowable count should be less than maximum count limit."
                            '   ShowMessage(Message, True)
                            'ElseIf (ddlGrp.SelectedValue = 0) Then
                            '   Message = "Please select group."
                            '    ShowMessage(Message, True)
                            'ElseIf (ddlSubGrp.SelectedValue = 0) Then
                            '    Message = "Please select sub group."
                            '    ShowMessage(Message, True)
                        ElseIf (rdSchdl.SelectedValue = 1 And gvSchdl.Rows.Count = 0) Then
                            Message = "Please insert the activity schedules ."
                            ShowMessage(Message, True)
                        Else
                            If ViewState("ViewId") Is Nothing Or ViewState("ViewId") = "" Then
                                If trGrades.CheckedNodes.Count = 0 And ViewState("MainMnu_code") = "S108915" Then
                                    'Showing Error Message : Please Select Applicable Grades"
                                    Message = getErrorMessage("4015")
                                    ShowMessage(Message, True)
                                Else
                                    INSERT_INTO_ACTIVITY_DETAILS("I")
                                End If
                            Else
                                INSERT_INTO_ACTIVITY_DETAILS("U")
                            End If
                        End If
                    End If
                End If
            ElseIf ViewState("MainMnu_code") = "S102301" Then
                UPD_ACTIVITY_ON_FINANCE_APPRVL()
            Else
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSave_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub rdoAllowMultiple_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (rdoAllowMultiple.SelectedValue = "Yes") Then
            txtallowcount.Visible = True
            lblnote.Visible = True
        Else
            txtallowcount.Visible = False
            lblnote.Visible = False
        End If
    End Sub
#End Region

#Region "Initiator"
    Protected Sub InitiatorActivityLogginScreen()

        'display panel for treeview (grades)
        pnlGrades2.Visible = True

        'makeing visibility none to those controls required for finance section        

        rowPaymentMode.Visible = False
        rowMailView.Visible = False
        rowTermsView.Visible = False
        rowEmailTemplate.Visible = False
        rowtermstemplate.Visible = False

        'buttons visibility arranging
        btnReject.Visible = False
        btnRevert.Visible = False
        btnCopy.Visible = True
        ' btnAddGroup.Visible = True
        btnUpdate.Visible = False


        'default values setting
        txtAmount.Text = "0.00"
        lblFinancLogDt.Text = ""
        lblInitiatorLogDt.Text = ""
        txtEmailTemplate.Content = "<p style=""text-align: left;"">Dear Parent,<br /><br />Activity request has been approved. Please check Parent portal. <br /><br />Thanks GEMS</p>"
        txtTermsandCond.Content = "<p style=""Text-align: left;"">Dear Parent ,<br />By accepting you agree to below Terms of service <br />1.  .... <br />2.  .... <br />Thanks GEMS</p>"

    End Sub
    Private Sub INSERT_INTO_ACTIVITY_DETAILS(ByVal action As String)
        Dim strans As SqlTransaction = Nothing
        Try
            Dim pParams(27) As SqlParameter
            Dim GET_SELECTED_SECTIONS As String = ""
            Dim GET_SELECTED_GRADES As String = ""
            Dim GET_SELECTED_SEC_DESCR As String = ""
            Dim Params(3) As SqlParameter
            Dim Params_s(5) As SqlParameter
            Dim qry2 As String = "[OASIS].[INSERT_GRADES_ONCREATION]"
            Dim qry3 As String = "[OASIS].[INSERT_SCHEDULES_ONCREATION]"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim Amount As Double = txtAmount.Text
            Amount = String.Format("{0:#.00}", Amount)
            Dim qry As String = "[OASIS].[INSERTUPDATE_ACTIVITY_ONCREATION]"
            pParams(0) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.Int)
            pParams(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            pParams(2) = Mainclass.CreateSqlParameter("@EVENT_NAME", txtEventName.Text.ToString, SqlDbType.VarChar)
            pParams(3) = Mainclass.CreateSqlParameter("@EVENT_DESCR", txtEventDesc.Value.ToString, SqlDbType.VarChar)
            pParams(4) = Mainclass.CreateSqlParameter("@APP_REQ_ST_DT", txtApplSt.Text, SqlDbType.Date)
            pParams(5) = Mainclass.CreateSqlParameter("@APP_REQ_END_DT", txtApplEnd.Text, SqlDbType.Date)
            pParams(6) = Mainclass.CreateSqlParameter("@EVENT_ST_DT", txtEventSt.Text, SqlDbType.Date)
            pParams(7) = Mainclass.CreateSqlParameter("@EVENT_END_DT", txtEventEnd.Text, SqlDbType.Date)
            pParams(8) = Mainclass.CreateSqlParameter("@EVENT_AMOUNT", Amount, SqlDbType.Decimal)
            pParams(10) = Mainclass.CreateSqlParameter("@CREATOR_CMMNTS", txtComments1.Value, SqlDbType.VarChar)
            pParams(11) = Mainclass.CreateSqlParameter("@LOGGED_USER", ViewState("EmployeeID").ToString, SqlDbType.VarChar)
            pParams(12) = Mainclass.CreateSqlParameter("@APPRVL_FLAG", radioButtonList.SelectedItem.Text, SqlDbType.VarChar)
            pParams(13) = Mainclass.CreateSqlParameter("@ACT_TYPE", ddlActivtyTypes.SelectedValue, SqlDbType.BigInt)
            pParams(14) = Mainclass.CreateSqlParameter("@ACTION", action, SqlDbType.VarChar)
            pParams(15) = Mainclass.CreateSqlParameter("@VIEWID", 0, SqlDbType.BigInt)

            If (rdoAllowMultiple.SelectedValue = "Yes") Then
                If txtallowcount.Text = "" Then
                    Message = "Please enter maximum allowable count 'Multi Enroll' option."
                    ShowMessage(Message, True)
                    Exit Sub
                Else
                    If (Convert.ToInt16(txtallowcount.Text) > Convert.ToInt16(txtCount.Text)) Then
                        Message = "Allowable count should be less than maximum count limit."
                        ShowMessage(Message, True)
                        Exit Sub

                    Else
                        pParams(17) = Mainclass.CreateSqlParameter("@ALLOWMULTIPLE", 1, SqlDbType.Bit)
                        pParams(19) = Mainclass.CreateSqlParameter("@ALLOWMULTIPLECOUNT", Convert.ToInt16(txtallowcount.Text), SqlDbType.BigInt)
                    End If
                End If
            Else
                pParams(17) = Mainclass.CreateSqlParameter("@ALLOWMULTIPLE", 0, SqlDbType.Bit)
                pParams(19) = Mainclass.CreateSqlParameter("@ALLOWMULTIPLECOUNT", Nothing, SqlDbType.BigInt)
            End If
            pParams(18) = Mainclass.CreateSqlParameter("@MAXCOUNT", Convert.ToInt64(txtCount.Text), SqlDbType.BigInt)
            If (ddlSubGrp.SelectedValue = 0) Then
                pParams(20) = Mainclass.CreateSqlParameter("@SUBGROUP_ID", Nothing, SqlDbType.BigInt)
            Else
                pParams(20) = Mainclass.CreateSqlParameter("@SUBGROUP_ID", ddlSubGrp.SelectedValue, SqlDbType.BigInt)
            End If
            If (rdSchdl.SelectedValue) Then
                pParams(21) = Mainclass.CreateSqlParameter("@IS_SCHDL", True, SqlDbType.Bit)
            Else
                pParams(21) = Mainclass.CreateSqlParameter("@IS_SCHDL", False, SqlDbType.Bit)
            End If
            Dim Gender As Char = "B"
            If Not rdGender.SelectedValue Is Nothing Then
                If rdGender.SelectedValue = 1 Then
                    Gender = "M"
                ElseIf rdGender.SelectedValue = 2 Then
                    Gender = "F"
                Else
                    Gender = "B"
                End If
            End If
            pParams(22) = Mainclass.CreateSqlParameter("@GENDER", Gender, SqlDbType.Char)
            Dim bDDLNOTALLOW As Boolean
            If chkNotAllow.SelectedValue = 1 Then
                bDDLNOTALLOW = True
            Else
                bDDLNOTALLOW = False
            End If
            pParams(23) = Mainclass.CreateSqlParameter("@bNOTALLOWDAY", bDDLNOTALLOW, SqlDbType.Bit)
            If (chkAutoEnroll.Checked) Then
                pParams(24) = Mainclass.CreateSqlParameter("@bAutoEnroll", True, SqlDbType.Bit)
            Else
                pParams(24) = Mainclass.CreateSqlParameter("@bAutoEnroll", False, SqlDbType.Bit)
            End If
            If radioDoEmail.SelectedValue Then
                pParams(25) = Mainclass.CreateSqlParameter("@bDoEmail", True, SqlDbType.Bit)
                pParams(9) = Mainclass.CreateSqlParameter("@EMAIL_TEMPLATE", txtEmailTemplate.Content.ToString, SqlDbType.VarChar)
            Else
                pParams(25) = Mainclass.CreateSqlParameter("@bDoEmail", False, SqlDbType.Bit)
                pParams(9) = Mainclass.CreateSqlParameter("@EMAIL_TEMPLATE", Nothing, SqlDbType.VarChar)
            End If
            If radoDoTerms.SelectedValue Then
                pParams(26) = Mainclass.CreateSqlParameter("@bDoTerms", True, SqlDbType.Bit)
                pParams(16) = Mainclass.CreateSqlParameter("@TERMSCONDITIONS", txtTermsandCond.Content.ToString, SqlDbType.VarChar)
            Else
                pParams(26) = Mainclass.CreateSqlParameter("@bDoTerms", False, SqlDbType.Bit)
                pParams(16) = Mainclass.CreateSqlParameter("@TERMSCONDITIONS", Nothing, SqlDbType.VarChar)
            End If
            If rowtrips.Visible Then
                'pParams(27) = Mainclass.CreateSqlParameter("@Subactid", ddlTripsList.SelectedValue, SqlDbType.BigInt)
                pParams(27) = Mainclass.CreateSqlParameter("@Subactid", h_SubActivity_ID.Value, SqlDbType.BigInt) ' Added by vikranth 26th Dec 2019
            Else
                pParams(27) = Mainclass.CreateSqlParameter("@Subactid", Nothing, SqlDbType.BigInt)
            End If
            If (action = "I") Then
                Latest_refid = SqlHelper.ExecuteScalar(strans, CommandType.StoredProcedure, qry, pParams)
                hidlatestrefid.Value = Latest_refid
                'insert grade details to table
                For Each node As TreeNode In trGrades.CheckedNodes
                    If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                        Continue For
                    End If
                    If node.Value <> "ALL" Then
                        GET_SELECTED_SEC_DESCR = node.Text
                        GET_SELECTED_SECTIONS = node.Value
                        GET_SELECTED_GRADES = node.Parent.Value
                        'Dim A As String = node
                        Params(0) = Mainclass.CreateSqlParameter("@REF_ID", Latest_refid, SqlDbType.Int)
                        Params(1) = Mainclass.CreateSqlParameter("@GRD_ID", GET_SELECTED_GRADES, SqlDbType.VarChar)
                        Params(2) = Mainclass.CreateSqlParameter("@SCT_ID", GET_SELECTED_SECTIONS, SqlDbType.VarChar)
                        SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry2, Params)
                    End If
                Next
                If (rdSchdl.SelectedValue) Then
                    If gvSchdl.Rows.Count > 0 Then
                        For Each row In gvSchdl.Rows
                            Params_s(0) = Mainclass.CreateSqlParameter("@REF_ID", Latest_refid, SqlDbType.Int)
                            Params_s(1) = Mainclass.CreateSqlParameter("@WEEKS", row.Cells(0).Text, SqlDbType.VarChar)
                            Params_s(2) = Mainclass.CreateSqlParameter("@TIMEFROM", row.Cells(1).Text, SqlDbType.VarChar)
                            Params_s(3) = Mainclass.CreateSqlParameter("@TIMETO", row.Cells(2).Text, SqlDbType.VarChar)
                            Params_s(4) = Mainclass.CreateSqlParameter("@DESCR", row.Cells(3).Text, SqlDbType.VarChar)
                            Params_s(5) = Mainclass.CreateSqlParameter("@COUNT", row.Cells(4).Text, SqlDbType.VarChar)
                            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry3, Params_s)
                        Next
                    End If
                End If

                'insert documents to server and insert details to table
                Dim returnvalue As Integer = callSavefile(Session("sbsuid"), Latest_refid, strans)
                If returnvalue = 0 Then
                    strans.Commit()
                    'lblError.Text = "SuccessFully Created New Activity"
                    Message = getErrorMessage("4016")
                    ShowMessage(Message, False)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "view")
                    btnPrint.Attributes.Add("OnClick", "return ShowWindowWithClose('ActivityDetails_PrintCopy.aspx?refid=" + Encr_decrData.Encrypt(Latest_refid) + "', 'Old Activity List', '75%', '75%');")
                    NoButtons()
                    ' SentNotification_ToFinance_ForActivity_Approval(Latest_refid)
                Else
                    strans.Rollback()
                    Message = getErrorMessage("4000")
                    ShowMessage(Message, True)
                    UtilityObj.Errorlog("From INSERT_INTO_ACTIVITY_DETAILS: " & Message, "OASIS ACTIVITY SERVICES")
                End If
            ElseIf (action = "U") Then
                pParams(15) = Mainclass.CreateSqlParameter("@VIEWID", Convert.ToInt32(ViewState("ViewId")), SqlDbType.BigInt)
                Latest_refid = Convert.ToInt32(ViewState("ViewId"))
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                If (rdSchdl.SelectedValue) Then
                    If gvSchdl.Rows.Count > 0 Then
                        For Each row In gvSchdl.Rows
                            Params_s(0) = Mainclass.CreateSqlParameter("@REF_ID", Latest_refid, SqlDbType.Int)
                            Params_s(1) = Mainclass.CreateSqlParameter("@WEEKS", row.Cells(0).Text, SqlDbType.VarChar)
                            Params_s(2) = Mainclass.CreateSqlParameter("@TIMEFROM", row.Cells(1).Text, SqlDbType.VarChar)
                            Params_s(3) = Mainclass.CreateSqlParameter("@TIMETO", row.Cells(2).Text, SqlDbType.VarChar)
                            Params_s(4) = Mainclass.CreateSqlParameter("@DESCR", row.Cells(3).Text, SqlDbType.VarChar)
                            Params_s(5) = Mainclass.CreateSqlParameter("@COUNT", row.Cells(4).Text, SqlDbType.VarChar)
                            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry3, Params_s)
                        Next
                    End If
                End If
                Dim returnvalue As Integer = callSavefile(Session("sbsuid"), ViewState("ViewId"), strans)
                If returnvalue = 0 Then
                    strans.Commit()
                    'lblError.Text = "SuccessFully Created New Activity"
                    Message = getErrorMessage("4036")
                    ShowMessage(Message, False)
                    NoButtons()
                    DeleteAndUpdateNone()
                    ' SentNotification_ToFinance_ForActivity_Approval(ViewState("ViewId"))
                Else
                    strans.Rollback()
                    Message = getErrorMessage("4000")
                    ShowMessage(Message, True)
                End If
            ElseIf (action = "D") Then
                pParams(15) = Mainclass.CreateSqlParameter("@VIEWID", Convert.ToInt32(ViewState("ViewId")), SqlDbType.BigInt)
                Latest_refid = Convert.ToInt32(ViewState("ViewId"))
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, pParams)
                strans.Commit()

            End If

        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From INSERT_INTO_ACTIVITY_DETAILS: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Function PopulateGradeandSections(ByVal ACD_ID As String, ByVal BSU_ID As String) As DataTable
        Try
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@ACD_ID", ACD_ID, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ALL_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                Return dsData.Tables(0)
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGradeandSections" + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Function
    Private Sub PopulateGrade()
        Try
            Dim dtTable As DataTable = PopulateGradeandSections(ddlACDYear.SelectedValue, Session("sbsuid"))
            ' PROCESS Filter
            Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "GRM_GRD_ID", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("Select All", "ALL")
            Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
            Dim drTRM_DESCRIPTION As DataRowView
            While (ienumTRM_DESCRIPTION.MoveNext())
                'Processes List
                drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("GRM_GRD_ID") Then
                        contains = True
                    End If
                End While
                Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("GRM_GRD_ID"), drTRM_DESCRIPTION("GRM_GRD_ID"))
                If contains Then
                    Continue While
                End If
                Dim strAMS_GRD As String = "GRM_GRD_ID = '" &
                drTRM_DESCRIPTION("GRM_GRD_ID") & "'"
                Dim dvAMS_GRD As New DataView(dtTable, strAMS_GRD, "GRM_GRD_ID", DataViewRowState.OriginalRows)
                Dim ienumAMS_GRD As IEnumerator = dvAMS_GRD.GetEnumerator
                While (ienumAMS_GRD.MoveNext())
                    Dim drGRD_DESCR As DataRowView = ienumAMS_GRD.Current
                    Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("SCT_DESCR"), (drGRD_DESCR("SCT_ID")))
                    trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeGRD_DESCR)
                End While
                trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
            End While
            trGrades.Nodes.Clear()
            trGrades.Nodes.Add(trSelectAll)
            trGrades.DataBind()
            trGrades.CollapseAll()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PopulateGrade: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub GetActivityViewForinitiator(ByVal viewid As Integer)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_DETAILS_ONVIEW]", Param)
            If DS.Tables.Count > 0 Then
                Dim DT As DataTable = DS.Tables(0)
                ShowControlsForBoth(DT)
                fillGrades()
                If (Not DS.Tables(1) Is Nothing) Then
                    If DS.Tables(1).Rows.Count > 0 Then
                        Dim DT2 = DS.Tables(1)
                        ShowSchedules(DT2)
                    End If
                End If
                NoButtons()
                If Not (DT.Rows(0)("ALD_ACT_LEVEL")) Is Nothing Then
                    If DT.Rows(0)("ALD_ACT_LEVEL") = "L1" Or DT.Rows(0)("ALD_ACT_LEVEL") = "RL2" Or DT.Rows(0)("ALD_ACT_LEVEL") = "RL3" Then
                        btnDelete.Visible = True
                        btnUpdate.Visible = True
                    ElseIf DT.Rows(0)("ALD_ACT_APPROVAL_STATUS") = "P" Or DT.Rows(0)("ALD_ACT_APPROVAL_STATUS") = "A" Then
                        btnExtend.Visible = True
                    End If
                    btnPrint.Visible = True
                    btnPrint.Attributes.Add("OnClick", "return ShowWindowWithClose('ActivityDetails_PrintCopy.aspx?refid=" + Encr_decrData.Encrypt(ViewState("ViewId")) + "', 'Old Activity List', '75%', '75%');")

                End If
                txtAmount.Enabled = False

            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetActivityViewForinitiator: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region

#Region "Financer"
    Sub ShowSchedules(ByVal dt As DataTable)
        gvSchdl.DataSource = dt
        gvSchdl.DataBind()
        ViewState("Schdl_Dt") = dt
        gvSchdl.Columns(5).Visible = False
    End Sub

    Private Sub UPD_ACTIVITY_ON_FINANCE_APPRVL()
        Dim strans As SqlTransaction = Nothing
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        Dim RevDt As Date = Nothing
        Try
            If (txtComments2.InnerText Is Nothing Or txtComments2.InnerText = "") Then
                'Showing Error Message As "Please fill comments"
                Message = getErrorMessage("4017")
                ShowMessage(Message, True)
            ElseIf (radlistFee.SelectedItem.Text = "Fee collection" And ddlFeetype.SelectedItem.Text = "SELECT") Then
                'Showing Error Message As "Please fill Fee Type"
                Message = getErrorMessage("4018")
                ShowMessage(Message, True)
            ElseIf (radlistFee.SelectedItem.Text = "Fee collection" And ((txtRevnDt.Text = "" Or txtRevnDt Is Nothing) And (chkrevnueprd.Checked = False))) Then
                'Please fill revenue date details
                Message = getErrorMessage("4039")
                ShowMessage(Message, True)
            ElseIf (radlistFee.SelectedItem.Text = "Other collection" And ddlFeetype.SelectedItem.Text = "SELECT") Then
                'Showing Error Message As "Please select Other Collection Type and revenue details"
                Message = getErrorMessage("4019")
                ShowMessage(Message, True)
            ElseIf (radlistFee.SelectedItem.Text = "Other collection" And ((txtRevnDt.Text = "" Or txtRevnDt Is Nothing) And (chkrevnueprd.Checked = False))) Then
                'Please fill revenue date details
                Message = getErrorMessage("4039")
                ShowMessage(Message, True)
            ElseIf Not chklistPaymentMode.Items(0).Selected And Not chklistPaymentMode.Items(1).Selected And (radlistFee.SelectedItem.Text = "Fee collection" Or radlistFee.SelectedItem.Text = "Other collection") Then
                Message = getErrorMessage("4038")
                ShowMessage(Message, True)
            ElseIf (radlistFee.SelectedItem.Text = "Fee collection" Or radlistFee.SelectedItem.Text = "Other collection") And GetDoubleVal(txtAmount.Text) = 0 Then
                Message = getErrorMessage("4040")
                ShowMessage(Message, True)
            ElseIf (chkaccpt.Checked = False) Then
                Message = getErrorMessage("4041")
                ShowMessage(Message, True)
            Else
                Try
                    If txtRevnDt.Text = "" Then
                        RevDt = Nothing
                    Else
                        RevDt = DateTime.Parse(txtRevnDt.Text.Trim())
                    End If

                Catch ex As Exception
                    RevDt = "#1/1/1900#"
                End Try
                Dim FeeCollection As String = "FC"
                Dim OtherCollection As String = "OC"
                Dim RegistrationOnly As String = "RO"
                Dim Params(13) As SqlParameter
                Dim objConn As New SqlConnection(st_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                If (radlistFee.SelectedItem.Text = "Fee collection") Then
                    Params(0) = Mainclass.CreateSqlParameter("@COLL_MODE", FeeCollection, SqlDbType.VarChar)
                    Params(1) = Mainclass.CreateSqlParameter("@FEETYPE", ddlFeetype.SelectedValue, SqlDbType.BigInt)
                    Params(2) = Mainclass.CreateSqlParameter("@OTHERTYPE", Nothing, SqlDbType.BigInt)
                    Params(10) = Mainclass.CreateSqlParameter("@EVT_ID", Nothing, SqlDbType.VarChar)
                    Params(11) = Mainclass.CreateSqlParameter("@TAXCODE", Nothing, SqlDbType.VarChar)
                    If (chkrevnueprd.Checked = True) Then
                        Params(3) = Mainclass.CreateSqlParameter("@REVENUDT", Nothing, SqlDbType.VarChar)
                        Params(12) = Mainclass.CreateSqlParameter("@REVRECSPAN", 1, SqlDbType.Bit)
                    Else
                        If (RevDt >= Today) And (Not RevDt = "#1/1/1900#") Then
                            Params(3) = Mainclass.CreateSqlParameter("@REVENUDT", txtRevnDt.Text, SqlDbType.VarChar)
                            Params(12) = Mainclass.CreateSqlParameter("@REVRECSPAN", 0, SqlDbType.Bit)
                        Else
                            'lShowing error message as "Revision Date is an Expired one. Please try to submit a valid date"
                            Message = getErrorMessage("4021")
                            ShowMessage(Message, True)
                            Exit Sub
                        End If
                    End If
                ElseIf (radlistFee.SelectedItem.Text = "Other collection") Then

                    'If ddlEvents.SelectedItem.Text Is Nothing Or ddlEvents.SelectedItem.Text = "-SELECT-" Then
                    '    Message = getErrorMessage("4037")
                    '    ShowMessage(Message, True)
                    '    Exit Sub
                    'Else
                    'If ddlTAXCode.SelectedItem.Text Is Nothing Or ddlTAXCode.SelectedItem.Text = "SELECT" Then
                    '    Message = getErrorMessage("4037")
                    '    ShowMessage(Message, True)
                    '    Exit Sub
                    'Else
                    Params(0) = Mainclass.CreateSqlParameter("@COLL_MODE", OtherCollection, SqlDbType.VarChar)
                    Params(1) = Mainclass.CreateSqlParameter("@FEETYPE", ddlFeetype.SelectedValue, SqlDbType.BigInt)
                    Params(2) = Mainclass.CreateSqlParameter("@OTHERTYPE", Nothing, SqlDbType.VarChar)
                    Params(10) = Mainclass.CreateSqlParameter("@EVT_ID", Nothing, SqlDbType.VarChar)
                    Params(11) = Mainclass.CreateSqlParameter("@TAXCODE", hidFeeVatCode.Value, SqlDbType.VarChar)
                    Params(3) = Mainclass.CreateSqlParameter("@REVENUDT", Nothing, SqlDbType.VarChar)
                    Params(12) = Mainclass.CreateSqlParameter("@REVRECSPAN", 0, SqlDbType.Bit)
                    'If chkrevnueprd.Checked Then
                    '    Params(3) = Mainclass.CreateSqlParameter("@REVENUDT", Nothing, SqlDbType.VarChar)
                    '    Params(12) = Mainclass.CreateSqlParameter("@REVRECSPAN", 1, SqlDbType.Bit)
                    'Else
                    '    If (RevDt >= Today) And (Not RevDt = "#1/1/1900#") Then
                    '        Params(3) = Mainclass.CreateSqlParameter("@REVENUDT", txtRevnDt.Text, SqlDbType.VarChar)
                    '        Params(12) = Mainclass.CreateSqlParameter("@REVRECSPAN", 0, SqlDbType.Bit)
                    '    Else
                    '        'lShowing error message as "Revision Date is an Expired one. Please try to submit a valid date"
                    '        Message = getErrorMessage("4021")
                    '        ShowMessage(Message, True)
                    '        Exit Sub
                    '    End If
                    'End If
                    ' End If
                    '   End If
                ElseIf (radlistFee.SelectedItem.Text = "Register without fee collection") Then
                    Params(0) = Mainclass.CreateSqlParameter("@COLL_MODE", RegistrationOnly, SqlDbType.VarChar)
                    Params(1) = Mainclass.CreateSqlParameter("@FEETYPE", Nothing, SqlDbType.Int)
                    Params(2) = Mainclass.CreateSqlParameter("@OTHERTYPE", Nothing, SqlDbType.Int)
                    Params(10) = Mainclass.CreateSqlParameter("@EVT_ID", Nothing, SqlDbType.VarChar)
                    Params(11) = Mainclass.CreateSqlParameter("@TAXCODE", hidFeeVatCode.Value, SqlDbType.VarChar)
                    Params(3) = Mainclass.CreateSqlParameter("@REVENUDT", Nothing, SqlDbType.VarChar)
                    Params(12) = Mainclass.CreateSqlParameter("@REVRECSPAN", 0, SqlDbType.Bit)
                End If

                Params(4) = Mainclass.CreateSqlParameter("@COMMENTS", txtComments2.InnerText, SqlDbType.VarChar)
                Params(5) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
                Params(6) = Mainclass.CreateSqlParameter("@VIEWD", ViewState("ViewId"), SqlDbType.BigInt)
                Params(7) = Mainclass.CreateSqlParameter("@FEEAMOUNT", Convert.ToDecimal(txtAmount.Text), SqlDbType.Decimal)
                If chklistPaymentMode.Items(0).Selected Then
                    Params(8) = Mainclass.CreateSqlParameter("@PAY_ONLINE", 1, SqlDbType.Bit)
                Else
                    Params(8) = Mainclass.CreateSqlParameter("@PAY_ONLINE", 0, SqlDbType.Bit)
                End If
                If chklistPaymentMode.Items(1).Selected Then
                    Params(9) = Mainclass.CreateSqlParameter("@PAY_COUNTER", 1, SqlDbType.Bit)
                Else
                    Params(9) = Mainclass.CreateSqlParameter("@PAY_COUNTER", 0, SqlDbType.Bit)
                End If
                Params(13) = Mainclass.CreateSqlParameter("@VIEWD_Encrypt", Encr_decrData.Encrypt(ViewState("ViewId")), SqlDbType.VarChar) 'Added by vikranth on 2nd Jan 2020
                SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UPDATE_ACTIVITY_ONFINANCE_APPRVL]", Params)
                strans.Commit()
                ' Showing Success Message as "Updated the Activity With Fee Details Successfully"
                Message = getErrorMessage("4020")
                ShowMessage(Message, False)
                NoButtons()
                '  SentNotification_ToPrincipal_ForActivity_Approval(Convert.ToInt32(ViewState("ViewId")))
            End If
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From UPD_ACTIVITY_ON_FINANCE_APPRVL: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub


    Private Function PopulateData_ToLoadFinance(ByRef ViewId As Integer) As DataSet
        Dim ds As DataSet = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewId, SqlDbType.BigInt)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_DETAILS_ONVIEW]", Param)
        Return ds
    End Function
    Private Sub GetApprovalDetlsForFinance()
        Try
            rowaccept.Visible = True

            ShowableControlstoboth()
            Dim ds As DataSet = PopulateData_ToLoadFinance(ViewState("ViewId"))
            If (ds.Tables.Count > 0) Then
                'hidAmount.Value = GetDoubleVal(txtAmount.Text)
                Dim dt As DataTable = ds.Tables(0)
                ShowControlsForBoth(dt)
                fillGrades()
                If dt.Rows(0)("ALD_EVENT_END_DT") = "#1/1/1900#" Then
                    txtRevnDt.Text = ""
                Else
                    txtRevnDt.Text = Format(dt.Rows(0)("ALD_EVENT_END_DT"), "dd/MMM/yyyy")
                End If
                If GetDoubleVal(txtAmount.Text) > 0 Then
                    radlistFee.Enabled = True
                    rowPaymentMode.Visible = True
                    lblrevenuedt.Visible = True
                    txtRevnDt.Visible = True
                    chkrevnueprd.Visible = True
                    ' colRevendt.Visible = True
                Else
                    radlistFee.Enabled = False
                    rowPaymentMode.Visible = False
                    lblrevenuedt.Visible = False
                    txtRevnDt.Visible = False
                    chkrevnueprd.Visible = False
                    ' colRevendt.Visible = False
                End If

                If (Not ds.Tables(1) Is Nothing) Then
                    If ds.Tables(1).Rows.Count > 0 Then
                        Dim DT2 = ds.Tables(1)
                        ShowSchedules(DT2)
                    End If
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetApprovalDetlsForFinance " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub GetActivityViewForFinance(ByVal ViewId As Integer)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_DETAILS_ONVIEW]", Param)
            Dim dt As DataTable = DS.Tables(0)
            ShowControlsForBoth(dt)
            ShowControls_OnView(dt)
            fillGrades()
            txtAmountExlTax.Enabled = False
            txttax.Enabled = False
            txtNetamnt.Enabled = False
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetActivityViewForFinance: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Sub Insert_Reject_Details()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim Param(3) As SqlParameter
            If ViewState("MainMnu_code") = "S102301" Then
                Param(0) = Mainclass.CreateSqlParameter("@COMMENTS", txtComments2.InnerText, SqlDbType.VarChar)
                Param(1) = Mainclass.CreateSqlParameter("@LEVEL", "L1", SqlDbType.VarChar)
            End If
            Param(2) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[REJECT_OASIS_ACTIVITY]", Param)
            strans.Commit()
            ' lblError.Text = "OO.. The Activity Got REJECTED"
            Message = getErrorMessage("4024")
            ShowMessage(Message, True)
            NoButtons()
        Catch ex As Exception
            strans.Rollback()
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Insert_Reject_Details: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Sub NoButtons()
        'REMOVING VISIBILITY OF BELOW BUTTONS
        btnReject.Visible = False
        btnRevert.Visible = False
        btnSave.Visible = False
    End Sub
    Private Sub ShowControlsForBoth(ByVal dt As DataTable)
        'LOADING CONTROLS
        'lnkBtnSelectGrade.Visible = False
        lblShowGrades.Visible = True
        pnlGrades2.Visible = False

        'LOADING DOCUMENTS
        rowViewfiles.Visible = True
        ' rowdocumentheading.Visible = True
        GetActivityDocumentOnView()

        'POPULATING VALUES
        Dim EmployeeName As String = String.Empty
        Try
            If Not dt Is Nothing Then
                lblslctgrade.Text = "Selected grades"
                If Not dt.Rows(0)("ALD_EVENT_NAME") Is Nothing Or Not dt.Rows(0)("ALD_EVENT_NAME") = "" Then
                    txtEventName.Text = dt.Rows(0)("ALD_EVENT_NAME")
                Else
                    txtEventName.Text = ""
                End If
                txtEventName.Enabled = False
                If Not dt.Rows(0)("ALD_EVENT_DESCR") Is Nothing Or Not dt.Rows(0)("ALD_EVENT_DESCR") = "" Then
                    txtEventDesc.Value = dt.Rows(0)("ALD_EVENT_DESCR")
                Else
                    txtEventDesc.Value = ""
                End If
                txtEventDesc.Disabled = True
                If Not dt.Rows(0)("EVENT_TYPE") Is Nothing Or Not dt.Rows(0)("EVENT_TYPE") = "" Then
                    ddlActivtyTypes.Items.FindByText("SELECT").Selected = False
                    ddlActivtyTypes.Items.FindByText(dt.Rows(0)("EVENT_TYPE")).Selected = True
                    fillSubGroups()
                Else
                    ddlActivtyTypes.SelectedItem.Text = "SELECT"
                End If
                ddlActivtyTypes.Enabled = False

                If Not dt.Rows(0)("ALD_ACDID") Is Nothing Or Not (dt.Rows(0)("ALD_ACDID")).ToString = "" Then
                    ddlACDYear.ClearSelection()
                    ddlACDYear.Items.FindByValue(dt.Rows(0)("ALD_ACDID")).Selected = True

                Else

                End If
                ddlACDYear.Enabled = False

                If Not dt.Rows(0)("ALD_APPLREQ_ST_DT") = "#1/1/1900#" Then
                    txtApplSt.Text = Format(dt.Rows(0)("ALD_APPLREQ_ST_DT"), "dd/MMM/yyyy")
                Else
                    txtApplSt.Text = ""
                End If
                txtApplSt.Enabled = False

                If Not dt.Rows(0)("ALD_APPLREQ_END_DT") = "#1/1/1900#" Then
                    txtApplEnd.Text = Format(dt.Rows(0)("ALD_APPLREQ_END_DT"), "dd/MMM/yyyy")
                Else
                    txtApplEnd.Text = ""
                End If
                txtApplEnd.Enabled = False

                If Not dt.Rows(0)("ALD_EVENT_ST_DT") = "#1/1/1900#" Then
                    txtEventSt.Text = Format(dt.Rows(0)("ALD_EVENT_ST_DT"), "dd/MMM/yyyy")
                Else
                    txtEventSt.Text = ""
                End If
                txtEventSt.Enabled = False

                If Not dt.Rows(0)("ALD_EVENT_END_DT") = "#1/1/1900#" Then
                    txtEventEnd.Text = Format(dt.Rows(0)("ALD_EVENT_END_DT"), "dd/MMM/yyyy")
                Else
                    txtEventEnd.Text = ""
                End If

                txtEventEnd.Enabled = False
                txtAmount.Text = dt.Rows(0)("ALD_EVENT_AMT")
                If ViewState("MainMnu_code") = "S102301" Then
                    rowFinance.Visible = True
                    rowFinanceheading.Visible = True
                    If (dt.Rows(0)("ALD_EVENT_AMT") > 0) Then
                        lblrevenuedt.Visible = True
                        ' colRevendt.Visible = True
                        txtRevnDt.Visible = True
                        chkrevnueprd.Visible = True
                        radlistFee.Items.FindByText("Fee collection").Selected = True
                        lblFeeType.Visible = True
                        ddlFeetype.Visible = True
                        rowPaymentMode.Visible = True
                    Else
                        lblrevenuedt.Visible = False
                        ' colRevendt.Visible = False
                        txtRevnDt.Visible = False
                        chkrevnueprd.Visible = False
                        rowPaymentMode.Visible = False
                        rowShowSplitAmount.Visible = False
                        lblVatDescr.Text = ""
                        radlistFee.Items.FindByText("Register without fee collection").Selected = True
                    End If
                Else

                End If

                If (dt.Rows(0)("ALD_APPRVLREQ_FLAG") <> "" And Not dt.Rows(0)("ALD_APPRVLREQ_FLAG") Is Nothing) Then
                    If dt.Rows(0)("ALD_APPRVLREQ_FLAG") = "Yes" Then
                        radioButtonList.Items("0").Selected = True
                        radioButtonList.Enabled = False
                    ElseIf dt.Rows(0)("ALD_APPRVLREQ_FLAG") = "No" Then
                        radioButtonList.Items("1").Selected = True
                        radioButtonList.Enabled = False
                    Else
                        radioButtonList.Items("1").Selected = True
                        radioButtonList.Enabled = False
                    End If
                End If

                '   radioButtonList.SelectedValue = dt.Rows(0)("ALD_APPRVLREQ_FLAG")
                '   radioButtonList.Enabled = False

                If Not dt.Rows(0)("ALD_ACT_CREATOR_LOGDT") = "#1/1/1900#" Then
                    lblInitiatorLogDt.Text = "Activity request logged on : " + Format(dt.Rows(0)("ALD_ACT_CREATOR_LOGDT"), "dd/MMM/yyyy")
                Else
                    lblInitiatorLogDt.Text = ""
                End If
                If Not dt.Rows(0)("ALD_ACT_CREATOR") Is Nothing Or Not dt.Rows(0)("ALD_ACT_CREATOR") = "" Then
                    EmployeeName = EOS_MainClass.GetEmployeeNameFromID(dt.Rows(0)("ALD_ACT_CREATOR"))
                Else
                    EmployeeName = ""
                End If
                If Not dt.Rows(0)("ALD_ACT_CREATOR_CMMNTS") Is Nothing Or Not dt.Rows(0)("ALD_ACT_CREATOR_CMMNTS") = "" Then
                    lblinicomment.Text = lblinicomment.Text + "(" + EmployeeName + ")"
                    txtComments1.InnerText = dt.Rows(0)("ALD_ACT_CREATOR_CMMNTS")
                Else
                    txtComments1.InnerText = ""
                End If
                txtComments1.Disabled = True

                radioDoEmail.ClearSelection()
                If dt.Rows(0)("ALD_ISEMAIL") And dt.Rows(0)("ALD_EMAIL_TEMPLATE") Is Nothing Or Not dt.Rows(0)("ALD_EMAIL_TEMPLATE") = "" Then
                    lblmail.Text = dt.Rows(0)("ALD_EMAIL_TEMPLATE").ToString()
                    txtEmailTemplate.Content = dt.Rows(0)("ALD_EMAIL_TEMPLATE").ToString
                    radioDoEmail.Items.FindByValue(1).Selected = True
                Else
                    rowMailView.Visible = False
                    lblmail.Text = ""
                    txtEmailTemplate.Content = ""
                    radioDoEmail.Items.FindByValue(0).Selected = True
                End If
                radioDoEmail.Enabled = False

                radoDoTerms.ClearSelection()
                If dt.Rows(0)("ALD_ISTERMS") And Not dt.Rows(0)("ALD_TERMS_CONDITIONS") Is Nothing Or Not dt.Rows(0)("ALD_TERMS_CONDITIONS") = "" Then
                    lblterms.Text = dt.Rows(0)("ALD_TERMS_CONDITIONS").ToString
                    txtTermsandCond.Content = dt.Rows(0)("ALD_TERMS_CONDITIONS").ToString
                    radoDoTerms.Items.FindByValue(1).Selected = True
                Else
                    rowTermsView.Visible = False
                    lblterms.Text = ""
                    txtTermsandCond.Content = ""
                    radoDoTerms.Items.FindByValue(0).Selected = True
                End If
                radoDoTerms.Enabled = False

                rowEmailTemplate.Visible = False
                rowtermstemplate.Visible = False


                'If (Not dt.Rows(0)("AGD_ID") Is Nothing) Then
                '    ddlGrp.ClearSelection()
                '    ddlGrp.Items.FindByValue(dt.Rows(0)("AGD_ID")).Selected = True
                '    ddlGrp_SelectedIndexChanged(Nothing, Nothing)
                'Else
                'End If
                'ddlGrp.Enabled = False
                ddlSubGrp.ClearSelection()
                If dt.Rows(0)("ASD_SUBGRP_NAME") Is Nothing Or dt.Rows(0)("ASD_SUBGRP_NAME") = "" Then
                    ddlSubGrp.Items.FindByText("SELECT").Selected = True
                Else
                    ddlSubGrp.Items.FindByText(dt.Rows(0)("ASD_SUBGRP_NAME")).Selected = True
                End If
                ddlSubGrp.Enabled = False

                If (Not dt.Rows(0)("ALD_IS_SCHEDULE") Is Nothing) Then
                    rdSchdl.ClearSelection()
                    If (dt.Rows(0)("ALD_IS_SCHEDULE")) Then
                        rdSchdl.SelectedValue = 1
                    Else
                        rdSchdl.SelectedValue = 0
                    End If
                Else
                End If
                rdSchdl.Enabled = False

                If (Not dt.Rows(0)("ALD_GENDER") Is Nothing) Then
                    rdGender.ClearSelection()
                    If (dt.Rows(0)("ALD_GENDER") = "M") Then
                        rdGender.SelectedValue = 1
                    ElseIf (dt.Rows(0)("ALD_GENDER") = "F") Then
                        rdGender.SelectedValue = 2
                    Else
                        rdGender.SelectedValue = 0
                    End If
                End If
                rdGender.Enabled = False

                'NEWLY ADDED
                If (Not dt.Rows(0)("ALD_ALLOW_MULTIPLE") Is Nothing) And (dt.Rows(0)("ALD_ALLOW_MULTIPLE")) = 0 Then
                    rdoAllowMultiple.SelectedValue = "No"
                Else
                    rdoAllowMultiple.SelectedValue = "Yes"
                    txtallowcount.Visible = True
                    lblnote.Visible = True
                    If (Not dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT") Is Nothing) And (dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT")) <> 0 Then
                        txtallowcount.Text = dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT")
                    Else
                        txtallowcount.Text = 0
                    End If
                    txtallowcount.Enabled = False
                End If
                rdoAllowMultiple.Enabled = False

                If (Not dt.Rows(0)("ALD_MAX_COUNT") Is Nothing) Then
                    txtCount.Text = dt.Rows(0)("ALD_MAX_COUNT").ToString
                End If
                txtCount.Enabled = False

                If (Not dt.Rows(0)("ALD_BNOT_ALLWDAY") Is Nothing) Then
                    If dt.Rows(0)("ALD_BNOT_ALLWDAY") Then
                        chkNotAllow.SelectedValue = 1
                    Else
                        chkNotAllow.SelectedValue = 0
                    End If
                End If
                chkNotAllow.Enabled = False

                If (Not dt.Rows(0)("ALD_AUTO_ENROLL") Is Nothing) Then
                    If dt.Rows(0)("ALD_AUTO_ENROLL") Then
                        chkAutoEnroll.Checked = True
                    Else
                        chkAutoEnroll.Checked = False
                    End If
                End If
                chkAutoEnroll.Enabled = False

                If (Not dt.Rows(0)("ALD_ATM_ID") Is Nothing) Then
                    fillTripsDropdownlist()
                    If rowtrips.Visible Then
                        ddlTripsList.FindItemByValue(dt.Rows(0)("ALD_ATM_ID")).Selected = True
                        ddlFeetype.ClearSelection()
                        ddlTripsList.Enabled = False
                        txtSubActivityName.Text = dt.Rows(0)("ATM_NAME") ' Added by vikranth 26th Dec 2019
                        txtSubActivityName.Enabled = False
                        btnEmp_Name.Visible = False
                        h_SubActivity_ID.Value = dt.Rows(0)("ALD_ATM_ID")
                        ddlFeetype.Items.FindByValue(dt.Rows(0)("ATM_FEE_CODE")).Selected = True
                        ddlFeetype.Enabled = False
                        rowShowSplitAmount.Visible = True
                        Dim DS As DataSet = getTaxDetails()
                        If DS.Tables.Count > 0 Then
                            txtAmountExlTax.Text = DS.Tables(0).Rows(0)("INV_AMOUNT")
                            txttax.Text = DS.Tables(0).Rows(0)("TAX_AMOUNT")
                            txtNetamnt.Text = DS.Tables(0).Rows(0)("NET_AMOUNT")
                            lblVatDescr.Text = " Tax Code: " + DS.Tables(0).Rows(0)("TAX_CODE") 'changed from vat code to tax code by vikranth on 22nd Dec 2019
                            hidFeeVatCode.Value = DS.Tables(0).Rows(0)("TAX_CODE")
                            txtVatCode.Text = DS.Tables(0).Rows(0)("TAX_DESCR") 'Added by vikranth on 22nd dec 2019
                        End If
                    End If
                End If

            Else

            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From ShowControlsForBoth: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub DeleteAndUpdateNone()
        btnDelete.Visible = False
        btnUpdate.Visible = False
    End Sub
    Private Sub ShowableControlstoboth()
        Try
            ' lblslctgrade.Text = "Selected Grades"    
            rowFinanceCmmnts.Visible = True
            btnSave.Text = "Approve"
            btnAdd.Visible = False
            DeleteAndUpdateNone()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From ShowableControlstoboth: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub ShowControls_OnView(ByVal dt As DataTable)
        Dim EmployeeName As String = String.Empty
        Try

            If (dt.Rows(0)("ALD_FEE_REG_MODE")) = "FC" Then
                ' radlistFee.SelectedValue = "Fee collection"
                radlistFee.Items.FindByText("Fee collection").Selected = True
                ddlFeetype.Visible = True
                If dt.Rows(0)("FEE_DESCR") Is Nothing Then
                    ddlFeetype.SelectedItem.Text = ""
                Else
                    ddlFeetype.Items.FindByText("SELECT").Selected = False
                    ddlFeetype.Items.FindByText(dt.Rows(0)("FEE_DESCR")).Selected = True
                End If
                ddlFeetype.Enabled = False
                ' ddlOtherColl.Visible = False
            ElseIf (dt.Rows(0)("ALD_FEE_REG_MODE")) = "OC" Then
                ' radlistFee.SelectedValue = "Other collection"
                radlistFee.Items.FindByText("Other collection").Selected = True
                ' ddlOtherColl.Visible = True
                If dt.Rows(0)("ACT_DESCR") Is Nothing Then
                    '  ddlOtherColl.SelectedItem.Text = ""
                Else
                    '   lblOtheract.Visible = True
                    '  ddlOtherColl.Items.FindByText("SELECT").Selected = False
                    ' ddlOtherColl.Items.FindByText(dt.Rows(0)("ACT_DESCR")).Selected = True
                End If
                ' ddlOtherColl.Enabled = False
                ' ddlFeetype.Visible = False
            Else
                ' radlistFee.SelectedValue = "Registration Only"
                radlistFee.Items.FindByText("Register without fee collection").Selected = True
                rowShowSplitAmount.Visible = False
                lblVatDescr.Text = ""
                ' ddlOtherColl.Visible = False
                ddlFeetype.Visible = False
            End If
            radlistFee.Enabled = False
            If dt.Rows(0)("ALD_REVENUE_DT") = "#1/1/1900#" Then
                txtRevnDt.Text = ""
                chkrevnueprd.Checked = True

            Else
                txtRevnDt.Text = Format(dt.Rows(0)("ALD_REVENUE_DT"), "dd/MMM/yyyy")
            End If
            chkrevnueprd.Enabled = False
            txtRevnDt.Enabled = False

            If Not dt.Rows(0)("ALD_PAY_ONLINE") Is Nothing Then
                If dt.Rows(0)("ALD_PAY_ONLINE") = "1" Then
                    rowPaymentMode.Visible = True
                    chklistPaymentMode.Items(0).Selected = True
                Else
                    chklistPaymentMode.Items(0).Selected = False
                End If
            End If

            If Not dt.Rows(0)("ALD_PAY_COUNTER") Is Nothing Then
                If dt.Rows(0)("ALD_PAY_COUNTER") = "1" Then
                    rowPaymentMode.Visible = True
                    chklistPaymentMode.Items(1).Selected = True
                Else
                    chklistPaymentMode.Items(1).Selected = False
                End If
            End If
            chklistPaymentMode.Enabled = False
            If Not dt.Rows(0)("ALD_ACT_FINANCE_LOGDT") = "#1/1/1900#" Then
                lblFinancLogDt.Text = " Finance department approved on : " + Format(dt.Rows(0)("ALD_ACT_FINANCE_LOGDT"), "dd/MMM/yyyy")
            Else
                lblFinancLogDt.Text = ""
            End If

            If Not dt.Rows(0)("ALD_ACT_FINANCE") Is Nothing Or Not dt.Rows(0)("ALD_ACT_FINANCE") = "" Then
                EmployeeName = EOS_MainClass.GetEmployeeNameFromID(dt.Rows(0)("ALD_ACT_FINANCE"))
            Else
                EmployeeName = ""
            End If
            rowFinanceCmmnts.Visible = True
            If Not dt.Rows(0)("ALD_ACT_FINANCE_CMMNTS") Is Nothing Or Not dt.Rows(0)("ALD_ACT_FINANCE_CMMNTS") = "" Then
                lblfinancecommnt.Text = lblfinancecommnt.Text + "(" + EmployeeName + ")"
                txtComments2.InnerText = dt.Rows(0)("ALD_ACT_FINANCE_CMMNTS")
            Else
                txtComments2.InnerText = ""
            End If
            txtComments2.Disabled = True
            NoButtons()
            txtAmount.Enabled = False
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From ShowControls_OnView: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub Revert_Item_To_Initiator()
        Try
            Dim flag As Boolean = False
            If ViewState("MainMnu_code") = "S102301" Then
                If (txtComments2.InnerText Is Nothing Or txtComments2.InnerText = "") Then
                    Message = getErrorMessage("4022")
                    ShowMessage(Message, True)
                    flag = True
                Else
                    flag = False
                End If
            End If
            If (flag = False) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim strans As SqlTransaction = Nothing
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                Dim Param(3) As SqlParameter
                If ViewState("MainMnu_code") = "S102301" Then
                    Param(0) = Mainclass.CreateSqlParameter("@COMMENTS", txtComments2.InnerText, SqlDbType.VarChar)
                    Param(1) = Mainclass.CreateSqlParameter("@OPTION", "L2", SqlDbType.VarChar)
                End If
                Param(2) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
                Param(3) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
                SqlHelper.ExecuteNonQuery(strans, "[OASIS].[REVERT_OASIS_ACTIVITY]", Param)
                strans.Commit()
                NoButtons()
                'Revert Message Is : Activity Has Been Successfully Reverted Back To Initiator
                Message = getErrorMessage("4035")
                ShowMessage(Message, False)
            End If
        Catch EX As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Revert_Item_To_Initiator: " + EX.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

#End Region

#Region "fileupload"
    Function ContainsSpecialChars(s As String) As Boolean
        Return s.IndexOfAny("[~`!@#$%^&*()-+=|{}':;,<>/?]".ToCharArray) <> -1
    End Function
    Private Function GetExtension(ByVal FileName As String) As String
        Dim Extension As String
        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)
        Return Extension
    End Function
    Public Function CurrentDate() As String
        'Dim lastDate As String = Day(DateTime.Now).ToString() & "/" & MonthName(Month(DateTime.Now), True) & "/" & Year(DateTime.Now).ToString()
        'lastDate = Convert.ToDateTime(lastDate).ToString("ddMMMyyyy")
        'Return lastDate
        Dim lastdate As String = DateTime.Now.Ticks.ToString
        Return lastdate
    End Function
    Protected Sub onloaddocumentupload(ByVal bsuid As String, ByVal activitytype As Integer)
        Try
            Dim query As String = "[OASIS].[GET_ACTIVITY_DOCUMENTS_TOFETCH]"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(3) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSUID", bsuid, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@Eventtype", activitytype, SqlDbType.Int)
            Param(2) = Mainclass.CreateSqlParameter("@ACDID", ddlACDYear.SelectedValue, SqlDbType.Int)
            Dim dsDOCS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, query, Param)
            Dim Count = dsDOCS.Tables(0).Rows.Count
            If Count > 0 Then
                repdoc.DataSource = dsDOCS.Tables(0)
                repdoc.DataBind()
                Session("tablefilecount") = dsDOCS.Tables(0).Rows.Count
                rowUploadoption.Visible = True
                rowdocumentheading.Visible = True
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From onloaddocumentupload: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Function callSavefile(ByVal bsuid As String, ByVal activityid As Integer, ByRef strans As SqlTransaction) As Integer
        callSavefile = 1000
        Try
            Dim status As Integer = 0
            Dim ACTIVITY_ID As String = activityid
            Dim STU_BSU_ID As String = bsuid
            Dim PHOTO_PATH As String = String.Empty
            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("ActivityDocumentspath2").ConnectionString
            Dim path As String
            If ViewState("ACTIVITYFILEPATH") <> "" Then

                If Not Directory.Exists(ConFigPath & bsuid & "\" & activityid & "\") Then
                    Directory.CreateDirectory(ConFigPath & bsuid & "\" & activityid & "\")
                    path = ConFigPath & bsuid & "\" & activityid & "\"
                Else
                    Dim dold As New DirectoryInfo(ConFigPath & bsuid & "\" & activityid & "\")
                    path = ConFigPath & bsuid & "\" & activityid & "\"
                    Dim fiold() As System.IO.FileInfo
                    fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fiold.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fiold
                            f.Delete()
                        Next
                    End If
                End If

                Dim d As New DirectoryInfo(ViewState("ACTIVITYFILEPATH"))

                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fi.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fi
                        f.MoveTo(path & f.Name)
                        Dim Str_fullpath As String = bsuid & "\" & activityid & "\" & "" & f.Name
                        Dim filename() = f.Name.Split("_")
                        If path <> "" Then
                            Dim param(6) As SqlClient.SqlParameter
                            param(0) = New SqlClient.SqlParameter("@RefId", activityid)
                            param(1) = New SqlClient.SqlParameter("@ActType", ddlActivtyTypes.SelectedValue)
                            param(2) = New SqlClient.SqlParameter("@DocTypeId", filename(0).ToString)
                            param(3) = New SqlClient.SqlParameter("@DocName", f.Name.ToString)
                            param(4) = New SqlClient.SqlParameter("@Fileurl", Str_fullpath)
                            param(5) = New SqlClient.SqlParameter("@User", ViewState("EmployeeID"))
                            param(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                            param(6).Direction = ParameterDirection.ReturnValue
                            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[INSERT_ACTIVITY_DOCUMENT_DETAILS]", param)
                            status = param(6).Value
                            callSavefile = status
                        Else
                            callSavefile = 1000
                            strans.Rollback()
                            Exit Function
                        End If
                    Next
                End If
                d.Delete()
                ViewState("ACTIVITYFILEPATH") = ""
            End If
            callSavefile = status
        Catch ex As Exception
            UtilityObj.Errorlog("From callSavefile: " + ex.Message, "OASIS ACTIVITY SERVICES")
            strans.Rollback()
            callSavefile = 1000
        End Try
    End Function

    Protected Sub btnfilesave_Click(sender As Object, e As EventArgs)
        Try
            Dim str_error As String = ""
            Dim obj As Object = sender.parent
            Dim BSUID = Session("sbsuid")
            Dim ActivityName = txtEventName.Text
            Dim str_file_name As String = ""
            Dim errCount As Integer = 0
            Dim fileCount As Integer = 0
            Dim returnvalue As Boolean = False
            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("ActivityDocumentspath1").ConnectionString
            Dim Tempath As String = ConFigPath & BSUID & "\" & ActivityName & "\"

            'to remove exising files in temp path
            Dim dold As New DirectoryInfo(Tempath)
            Dim fiold() As System.IO.FileInfo
            If dold.Exists Then
                fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fiold.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fiold
                        f.Delete()
                    Next
                End If
            End If
            'end of deletion of exising files


            For Each rptr As RepeaterItem In repdoc.Items
                Dim lblfile As Label = CType(rptr.FindControl("lblfile"), Label)
                Dim Fileupload As FileUpload = CType(rptr.FindControl("Fileupload"), FileUpload)
                Dim hffile As HiddenField = CType(rptr.FindControl("hffile"), HiddenField)
                If Fileupload.HasFile Then
                    Try
                        fileCount = fileCount + 1
                        Dim strPostedFileName As String = Fileupload.FileName 'get the file name
                        Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                        If strPostedFileName <> "" And ConFigPath <> "" Then
                            Dim intDocFileLength As Integer = Fileupload.FileContent.Length ' get the file size
                            Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                            Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
                            Dim FileNameLength As Integer = strPostedFileName.Length
                            Dim UploadfileName As String = hffile.Value & "_" & CurrentDate() & strExtn
                            Dim s As String = strPostedFileName
                            Dim result() As String
                            result = s.Split(".")
                            Dim fileext = GetExtension(strExtn)
                            'Checking file extension
                            If (strPostedFileName <> String.Empty) And fileext <> "" Then
                                If Not (fileext.ToUpper = "PDF") Then 'Or fileext.ToUpper = "PNG"

                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "Upload PDF Files Only!!"
                                    Else
                                        str_error = str_error & "Upload PDF Files Only!!"
                                    End If
                                    returnvalue = False
                                    '  Exit Sub
                                    'Checking file extension length
                                ElseIf fileext.Length = 0 Then
                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "file with out extension not allowed...!!"
                                    Else
                                        str_error = str_error & "file with out extension not allowed...!!"
                                    End If
                                    returnvalue = False
                                    '   Exit Sub
                                    'Checking Special Characters in file name

                                ElseIf ContainsSplChar = True Then

                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "File Name with special characters are not allowed..!!"
                                    Else
                                        str_error = str_error & "File Name with special characters are not allowed..!!"
                                    End If

                                    returnvalue = False
                                    ' Exit Sub
                                    'Checking FileName length

                                ElseIf FileNameLength = 0 Or FileNameLength > 1500 Then '255
                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "Error with file Name Length..!!"
                                    Else
                                        str_error = str_error & "Error with file Name Length..!!"
                                    End If

                                    returnvalue = False
                                    ' Exit Sub

                                ElseIf result.Length > 2 Then
                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "Invalid file Name..!!"
                                    Else
                                        str_error = str_error & "Invalid file Name..!!"
                                    End If

                                    returnvalue = False
                                    'Exit Sub

                                ElseIf Fileupload.FileContent.Length > 5000000 Then

                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "The Size of file is greater than 5MB"
                                    Else
                                        str_error = str_error & "The Size of file is greater than 5MB"
                                    End If

                                    returnvalue = False
                                    '' Exit Sub

                                ElseIf Not (strExtn = ".pdf") Then 'exten type

                                    If str_error <> "" Then
                                        str_error = str_error & "<br /><br />"
                                        str_error = str_error & "File type is different than allowed!"
                                    Else
                                        str_error = str_error & "File type is different than allowed!"
                                    End If

                                    returnvalue = False
                                    'Exit Sub

                                Else
                                    str_error = ""

                                    If Not Directory.Exists(Tempath) Then
                                        Directory.CreateDirectory(Tempath)
                                    End If
                                    If errCount = 0 Then
                                        Fileupload.SaveAs(Tempath & UploadfileName)
                                    End If
                                    ViewState("ACTIVITYFILEPATH") = ConFigPath & BSUID & "\" & ActivityName
                                End If
                            End If
                        End If

                    Catch ex As Exception
                        ' returnvalue = False
                        '  Return returnvalue
                        ShowMessage(ex.Message, True)
                        Exit Sub
                    End Try
                End If
                str_file_name = str_file_name & "-" & Fileupload.FileName
            Next

            If str_error <> "" Then
                ShowMessage(str_error, True)
                ' returnvalue = False
                ' Return returnvalue
                Exit Sub
            ElseIf Session("tablefilecount") <> fileCount Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please select all files for upload!!!"
                Else
                    str_error = str_error & "Please select all files for upload!!!"
                End If
                '  returnvalue = False
                ' Return returnvalue
                ShowMessage(str_error, True)
                Exit Sub
            Else
                If Not (errCount = 0 And Session("tablefilecount") = fileCount) Then
                    ' Dim status As Integer = callSave(BSUID, ActivityID, strans)
                    ' If status = 1000 Then
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "Please upload file!"
                    Else
                        str_error = str_error & "Please upload file!"
                    End If
                    ShowMessage(str_error, True)
                    'returnvalue = False
                Else
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "File saved successfully!"
                    Else
                        str_error = str_error & "File saved successfully!"
                        ShowMessage(str_error, True)
                        rowUploadoption.Visible = False
                        rowdocumentheading.Visible = False
                    End If
                    '   returnvalue = True
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnfilesave_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub



    Protected Sub GetActivityDocumentOnView()
        Try
            Dim query As String = "[OASIS].[GET_ACTIVITY_DOCUMENTS_TOVIEW]"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@VIEWID", ViewState("ViewId"), SqlDbType.BigInt)
            Dim dsDOCS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, query, Param)
            If (dsDOCS.Tables.Count > 0) Then
                If (dsDOCS.Tables(0).Rows.Count > 0) Then
                    rowdocumentheading.Visible = True
                    repdocview.DataSource = dsDOCS.Tables(0)
                    repdocview.DataBind()
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From GetActivityDocumentOnView: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub repdocview_ItemCreated(sender As Object, e As RepeaterItemEventArgs)
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")
            Dim btn As LinkButton = e.Item.FindControl("lbSName")
            If Not btn Is Nothing Then
                smScriptManager.RegisterPostBackControl(btn)
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From repdocview_ItemCreated: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    'Protected Sub repdocview_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
    '    Dim btn As LinkButton = DirectCast(e.Item.FindControl("lbSName"), LinkButton)
    '    Dim hidpath As HiddenField = DirectCast(e.Item.FindControl("hidpath"), HiddenField)
    '    Dim strExtn As String = System.IO.Path.GetExtension(hidpath.Value).ToLower
    '    hidpath.Value = hidpath.Value.Replace("\", "\\")
    '    btn.Attributes.Add("OnClick", "javascript: return showDocument('" & hidpath.Value & "','" & strExtn & "')")
    'End Sub
    Protected Sub lbSName_Click(sender As Object, e As EventArgs)
        Try
            Dim lnkbutton1 As LinkButton = sender
            Dim filename As String = lnkbutton1.Text
            Dim query As String = "[OASIS].[GET_ACTIVITY_UPLOAD_PATH]"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@FILENAME", filename, SqlDbType.VarChar)
            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("ActivityDocumentspath2").ConnectionString
            Dim filepath As String = SqlHelper.ExecuteScalar(str_conn, query, Param)
            filepath = ConFigPath + filepath
            Response.Clear()
            Response.ContentType = "application/pdf"
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & filename)
            Response.BinaryWrite(System.IO.File.ReadAllBytes(filepath))
            Response.End()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From lbSName_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try

    End Sub
#End Region

#Region "SHARED FUNCTION"
    <System.Web.Services.WebMethod()>
    Public Shared Function a(ByVal Id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@VIEWID", Convert.ToInt32(Id), SqlDbType.BigInt)
        Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_DETAILS_ONVIEW]", Param)
        Dim Dt As DataTable = DS.Tables(0)
        Dim results As String = ""
        If Dt.Rows.Count > 0 Then
            results = Dt.Rows(0)("ALD_EVENT_NAME").ToString + "||" + Dt.Rows(0)("ALD_EVENT_DESCR").ToString + "||" + Dt.Rows(0)("ALD_EVENT_TYPE").ToString + "||" + Dt.Rows(0)("ALD_EVENT_AMT").ToString + "||" + Dt.Rows(0)("ALD_APPRVLREQ_FLAG").ToString + "||" + IIf(Dt.Rows(0)("ALD_EMAIL_TEMPLATE").Equals(DBNull.Value), "", Dt.Rows(0)("ALD_EMAIL_TEMPLATE").ToString + "||" + Dt.Rows(0)("ALD_SUB_GROUP_ID").ToString + "||" + Dt.Rows(0)("ALD_GENDER").ToString + "||" + Dt.Rows(0)("ALD_MAX_COUNT").ToString + "||" + Dt.Rows(0)("ALD_ALLOW_MULTIPLE").ToString)
        End If
        Return results
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetTrips(ByVal Id As String, ByVal prefix As String) As String()
        Dim trips As New List(Of String)()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(3) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@REF_ID", Id, SqlDbType.BigInt)
        Param(1) = Mainclass.CreateSqlParameter("@CNTY_ID", HttpContext.Current.Session("BSU_COUNTRY_ID"), SqlDbType.BigInt)
        Param(2) = Mainclass.CreateSqlParameter("@Prefix", prefix, SqlDbType.VarChar)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OASIS.GET_SUBACT_TYPES_DETAILS", Param)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                trips.Add(String.Format("{0}|{1}", row("ATM_NAME"), row("ATM_ID")))
            Next
        Else
            trips.Add(String.Format("{0}|{1}", "SELECT", 0))
        End If
        Return trips.ToArray()
    End Function

#End Region


    Protected Sub chkActivityTypes_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Protected Sub chkAutoEnroll_CheckedChanged(sender As Object, e As EventArgs)
        ClearSchedule()
        chkNotAllow.Enabled = False
        radioButtonList.SelectedValue = 1
        gvSchdl.DataSource = ""
        gvSchdl.DataBind()
        gvSchdl.Visible = False
        rdSchdl.SelectedValue = 0
        chkNotAllow.SelectedValue = 0

        If (chkAutoEnroll.Checked) Then
            rdSchdl.Enabled = False
            radioButtonList.Enabled = False
            If Not ViewState("Schdl_Dt") Is Nothing Then
                Dim dt As DataTable = ViewState("Schdl_Dt")
                If dt.Rows.Count > 0 Then
                    dt.Clear()
                End If
                ViewState("Schdl_Dt") = dt
            End If
            tblScdl.Visible = False
        Else
            rdSchdl.Enabled = True
            radioButtonList.Enabled = True
        End If

    End Sub
#Region "Extend Dates"
    Protected Sub btnExtend_Click(sender As Object, e As EventArgs)
        If (btnExtend.Text = "Extend Dates") Then
            enable_dates()
            btnExtend.Text = "Update Dates"
        ElseIf btnExtend.Text = "Update Dates" Then
            INSERT_INTO_ACTIVITY_DETAILS("D")
            btnExtend.Visible = False
        End If
    End Sub
    Sub enable_dates()
        txtApplSt.Enabled = True
        txtApplEnd.Enabled = True
        txtEventSt.Enabled = True
        txtEventEnd.Enabled = True
    End Sub
#End Region


#Region "SentEmail"
    Protected Sub SentNotification_ToPrincipal_ForActivity_Approval(ByVal ViewId As Integer)
        Dim Subject = "Notification : Activity creation request is pending in your queue"
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        Dim Params(2) As SqlParameter
        Params(0) = Mainclass.CreateSqlParameter("@VIEWID", Encr_decrData.Encrypt(ViewId), SqlDbType.VarChar)
        Params(1) = Mainclass.CreateSqlParameter("@OPTION", "P", SqlDbType.Char)
        Dim dts As DataSet = SqlHelper.ExecuteDataset(st_conn, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_EMAIL_TEMPLATE]", Params)
        Dim EmailMessage As String = ""
        If Not dts Is Nothing Then
            If Not dts.Tables(0) Is Nothing Then
                If Not dts.Tables(0).Rows(0)("MESSAGE") Is Nothing Then
                    EmailMessage = dts.Tables(0).Rows(0)("MESSAGE")
                End If
            End If
        End If
        SentToEmailID("P", Subject, EmailMessage)
    End Sub

    Protected Sub SentNotification_ToFinance_ForActivity_Approval(ByVal ViewId As Integer)
        Dim Subject = "Notification : Activity creation request is pending in your queue"
        '  Dim Email_Message As String = "Hi," + Environment.NewLine + "New Activity Request On Queue" + Environment.NewLine + "Please Login To OASIS and follow the Path   Student Management -> Approval -> Approve Activities (Finance) For Approving The Request"
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        Dim Params(2) As SqlParameter
        Params(0) = Mainclass.CreateSqlParameter("@VIEWID", Encr_decrData.Encrypt(ViewId), SqlDbType.VarChar)
        Params(1) = Mainclass.CreateSqlParameter("@OPTION", "F", SqlDbType.Char)
        Dim dts As DataSet = SqlHelper.ExecuteDataset(st_conn, CommandType.StoredProcedure, "[OASIS].[GET_ACTIVITY_EMAIL_TEMPLATE]", Params)
        Dim EmailMessage As String = ""
        If Not dts Is Nothing Then
            If Not dts.Tables(0) Is Nothing Then
                If Not dts.Tables(0).Rows(0)("MESSAGE") Is Nothing Then
                    EmailMessage = dts.Tables(0).Rows(0)("MESSAGE")
                End If
            End If
        End If
        SentToEmailID("F", Subject, EmailMessage)
    End Sub
    Protected Function SentToEmailID(ByVal Options As Char, ByVal Subject As String, ByVal Message As String) As String
        'SENDING MAIL TO PRINCIPAL AND FINANCE DEPT
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Dim Mailid As String = ""
        Param(0) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
        Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OASIS].[GET_SCHL_FINANC_PRINCIPAL_MAIL_DETAILS]", Param)
        If Not DS Is Nothing And Not DS.Tables(0) Is Nothing And DS.Tables(0).Rows.Count > 0 Then
            Dim DT As DataTable = DS.Tables(0)
            If Options = "P" Then
                For Each ROW In DT.Rows
                    If (ROW("DES_DESCR").ToString = "PRINCIPAL") Then
                        Mailid = ROW("EMD_EMAIL")
                        If Mainclass.isEmail(Mailid) Then
                            InsertintoMailTable(Subject, Message, Mailid)
                        End If
                    End If
                Next
            ElseIf Options = "F" Then
                For Each ROW In DT.Rows
                    If ROW("DES_DESCR").ToString = "Accounts Officer" And Not ROW("EMD_EMAIL") Is Nothing And Not ROW("EMD_EMAIL").ToString = "" Then
                        Mailid = ROW("EMD_EMAIL")
                        If Mainclass.isEmail(Mailid) Then
                            InsertintoMailTable(Subject, Message, Mailid)
                        End If
                    End If
                Next
            End If
        End If
        Return Mailid
    End Function
    Protected Sub InsertintoMailTable(ByVal subject As String, ByVal message As String, ByVal tomailid As String)
        Try
            Dim str_conn = "OASISConnectionString"
            Dim query As String = "[dbo].[InsertIntoEmailSendSchedule] '" & Session("sBsuid") & "','ActivityApprovalRequestMail','SYSTEM','" & tomailid & "','" & subject & "','" & message & "'"
            Mainclass.getDataValue(query, str_conn)
        Catch ex As Exception
            UtilityObj.Errorlog("From InsertintoMailTable: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region

#Region "REMOVING CHANGES"

    'Protected Sub ddlEvents_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    'CALLING LOAD TAX CODES AFTER SELECTING EVENT ID FROM EVENT DROP DOWN LIST
    '    LOAD_TAX_CODES(ddlEvents.SelectedValue)
    'End Sub
    'Protected Sub ddlOtherColl_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    'CALLING EVENT ID AFTER SELECTING OTHER COLLECTION ID FROM DROP DOWN LIST
    '    FillEvents()
    'End Sub

    'Sub LOAD_TAX_CODES(ByVal ACT_ID As String)
    '    'FILL TAX CODES IN DDLS - OTHER COLLECTION
    '    Try

    '        Dim TAX_CODE As String = "", TAX_AMOUNT As Double = 0, NET_AMOUNT As Double = 0, AMOUNT_EX_TAX As Double = 0
    '        Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(con, "[FEES].[GET_TAXCODES]")
    '        ddlTAXCode.DataSource = ds
    '        ddlTAXCode.DataTextField = "TAX_DESCR"
    '        ddlTAXCode.DataValueField = "TAX_CODE"
    '        ddlTAXCode.DataBind()
    '        ddlTAXCode.Items.Insert(0, New ListItem("SELECT", 0))
    '        'ddlTAXCode.Items.FindByText("SELECT").Selected = True
    '        ddlTAXCode.Visible = True
    '        lblTaxcode.Visible = True

    '        Dim ds2 As DataSet = getTaxDetails(ACT_ID)

    '        If Not ds2 Is Nothing AndAlso ds2.Tables(0).Rows.Count > 0 Then
    '            TAX_CODE = ds2.Tables(0).Rows(0)("TAX_CODE").ToString
    '            TAX_AMOUNT = Format(Convert.ToDouble(ds2.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
    '            NET_AMOUNT = Format(Convert.ToDouble(ds2.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
    '            AMOUNT_EX_TAX = Format(Convert.ToDouble(ds2.Tables(0).Rows(0)("INV_AMOUNT").ToString), "#,##0.00")
    '        End If
    '        ddlTAXCode.Items.FindByValue(TAX_CODE).Selected = True

    '    Catch EX As Exception
    '        Message = getErrorMessage("4000")
    '        ShowMessage(Message, True)
    '        UtilityObj.Errorlog("From LOAD_TAX_CODES: " + EX.Message, "OASIS ACTIVITY SERVICES")
    '    End Try
    'End Sub


    'Private Sub FillEvents()
    '    'FILL EVENT ID - OTHER COLLECTION
    '    Try
    '        Dim dtEvents As DataTable = FeeCommon.Get_Events_and_Activities(Session("sBsuid"), 0, ddlOtherColl.SelectedValue.ToString, "")
    '        ddlEvents.DataSource = dtEvents
    '        ddlEvents.DataTextField = "EVT_DESCR"
    '        ddlEvents.DataValueField = "EVT_ID"
    '        ddlEvents.DataBind()
    '        ddlEvents.Visible = True
    '        lblOtherevt.Visible = True
    '    Catch EX As Exception
    '        Message = getErrorMessage("4000")
    '        ShowMessage(Message, True)
    '        UtilityObj.Errorlog("From FillEvents: " + EX.Message, "OASIS ACTIVITY SERVICES")
    '    End Try
    'End Sub


    'Private Sub fillOtherTypes()
    '    'METHODE TO FILL OTHER COLLECTION TYPES IN DDL
    '    Try

    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim Param(0) As SqlParameter
    '        Param(0) = Mainclass.CreateSqlParameter("@CheckOption", 2, SqlDbType.Int)
    '        Dim dsFeeTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_REGISTRATION_FEE_DETAILS]", Param)
    '        ddlOtherColl.DataSource = dsFeeTypes.Tables(0)
    '        ddlOtherColl.DataTextField = "ACT_NAME"
    '        ddlOtherColl.DataValueField = "ACT_ID"
    '        ddlOtherColl.DataBind()
    '        ddlOtherColl.Items.Insert(0, New ListItem("SELECT", 0))
    '        ddlOtherColl.Items.FindByText("SELECT").Selected = True
    '    Catch ex As Exception
    '        Message = getErrorMessage("4000")
    '        ShowMessage(Message, True)
    '        UtilityObj.Errorlog("From fillOthertypes: " + ex.Message, "OASIS ACTIVITY SERVICES")
    '    End Try
    'End Sub
    'Protected Sub hideothercolldetails()
    '    lblOtheract.Visible = False
    '    lblOtherevt.Visible = False
    '    lblTaxcode.Visible = False
    '    ddlOtherColl.Visible = False
    '    If (Not ddlEvents Is Nothing) Then
    '        ddlEvents.Visible = False
    '    End If
    '    If (Not ddlTAXCode Is Nothing) Then
    '        ddlTAXCode.Visible = False
    '    End If
    'End Sub
    'Private Sub fillGroups()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim params(1) As SqlParameter
    '        params(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.Char)
    '        params(1) = Mainclass.CreateSqlParameter("@ACD_ID", ddlACDYear.SelectedValue, SqlDbType.Int)
    '        Dim dsGroups As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_GROUPS_FORACTIVITY]", params)
    '        If dsGroups.Tables(0).Rows.Count > 0 Then
    '            ddlGrp.DataSource = dsGroups
    '            ddlGrp.DataBind()
    '            ddlGrp.DataTextField = "GROUP_NAME"
    '            ddlGrp.DataValueField = "GROUP_ID"
    '            ddlGrp.DataBind()
    '        End If
    '        ddlGrp.Items.Insert(0, New ListItem("SELECT", 0))
    '        ddlGrp.ClearSelection()
    '        ddlGrp.Items.FindByText("SELECT").Selected = True
    '    Catch ex As Exception
    '        Message = getErrorMessage("4000")
    '        ShowMessage(Message, True)
    '        UtilityObj.Errorlog("From fillGroups: " + ex.Message, "OASIS ACTIVITY SERVICES")
    '    End Try

    'End Sub
    'Protected Sub lbSName_Click(sender As Object, e As EventArgs)
    '    Try
    '        Dim lnkbutton1 As LinkButton = sender
    '        Dim filename As String = lnkbutton1.Text
    '        Dim query As String = "[OASIS].[GET_ACTIVITY_UPLOAD_PATH]"
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim Param(0) As SqlParameter
    '        Param(0) = Mainclass.CreateSqlParameter("@FILENAME", filename, SqlDbType.VarChar)
    '        Dim filepath As String = SqlHelper.ExecuteScalar(str_conn, query, Param)
    '        'Response.Clear()
    '        'Response.ContentType = "application/pdf"
    '        'Response.AppendHeader("Content-Disposition", "attachment; filename=" & filename)
    '        'Response.BinaryWrite(System.IO.File.ReadAllBytes(filepath))
    '        'Response.End()
    '    Catch ex As Exception
    '        Message = getErrorMessage("4000")
    '        ShowMessage(Message, True)
    '        UtilityObj.Errorlog("From lbSName_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
    '    End Try
    'End Sub
#End Region



End Class