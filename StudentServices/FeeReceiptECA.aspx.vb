﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_FeeReceiptECA
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10
                gvPayments.Attributes.Add("bordercolor", "#000000")
                gvFeeDetails.Attributes.Add("bordercolor", "#000000")
                Select Case Request.QueryString("type")
                    Case "REC"
                        If Request.QueryString("id") <> "" Then
                            PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("user").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("stu_bsu_id").Replace(" ", "+")))
                        End If
                End Select
                If Session("sbsuid") = "900520" Then
                    Me.trAddress.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub PrintReceipt(ByVal p_BSU_ID As String, ByVal p_Receiptno As String, _
       ByVal USER_NAME As String, ByVal p_Stu_Bsu_id As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim str_query As String = ""
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@FCL_RECNO", p_Receiptno)
        param(1) = New SqlClient.SqlParameter("@pBsuID", p_BSU_ID)

        Try
            Dim dsReceipt As DataSet
            dsReceipt = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GetFeeRecieptByID", param)



            If Not dsReceipt Is Nothing Or dsReceipt.Tables(0).Rows.Count > 0 Then
                imgLogo.ImageUrl = "~/Common/GetLogo.aspx?BSU_ID=" & p_BSU_ID
                ''  lblAddress.Text = dsReceipt.Tables(0).Rows(0)("BSU_ADDRESS")
                lblHeader1.Text = dsReceipt.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
                lblHeader2.Text = dsReceipt.Tables(0).Rows(0)("BSU_HEADER2")
                lblHeader3.Text = dsReceipt.Tables(0).Rows(0)("BSU_HEADER3")
                lblProvider.Text = dsReceipt.Tables(0).Rows(0)("BSU_NAME")
                lblProvidername.Text = lblProvider.Text
                lblSchoolNameCheque.Text = dsReceipt.Tables(0).Rows(0)("CHEQUE_SCHOOLNAME") 'lblProvidername.Text
                lblStuShoolName.Text = dsReceipt.Tables(0).Rows(0)("STUDENT_BSU")
                lblDate.Text = Format(dsReceipt.Tables(0).Rows(0)("FCL_DATE"), "dd/MMM/yyyy")
                lblRecno.Text = dsReceipt.Tables(0).Rows(0)("FCL_RECNO")
                lblStudentNo.Text = dsReceipt.Tables(0).Rows(0)("STU_NO")
                lblStudentName.Text = dsReceipt.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
                lblNarration.Text = dsReceipt.Tables(0).Rows(0)("FCL_NARRATION")
                Dim formatstring As String = Session("BSU_DataFormatString")
                If p_BSU_ID = "500605" Then 'Academy Plus
                    trSchool.Visible = False
                Else
                    trSchool.Visible = True
                End If

                If Convert.ToString(formatstring) <> "" Then
                    formatstring = formatstring.Replace("0.", "###,###,###,##0.")
                    If IsNumeric(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso dsReceipt.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
                        If dsReceipt.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
                            lblBalance.Text = "Due : " & dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE"), formatstring)
                        Else
                            lblBalance.Text = "Advance : " & dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(dsReceipt.Tables(0).Rows(0)("FCL_BALANCE") * -1, formatstring)
                        End If
                    End If
                End If
                
                Dim mcSpell As New Mainclass
                lbluserloggedin.Text = "Printed by " & USER_NAME & " (Time: " + Now.ToString("dd/MMM/yyyy hh:mm:ss tt") & ")"
                Dim LogDatetime As String = Format(dsReceipt.Tables(0).Rows(0)("FCL_LOGDATE"), "dd/MMM/yyyy hh:mm:ss tt")
                lblPrintTime.Text = "Ref. " & dsReceipt.Tables(0).Rows(0)("FCL_EMP_ID") & " - " & LogDatetime & " - " & dsReceipt.Tables(0).Rows(0)("FCL_COUNTERSERIES").ToString

                'lblAmount.Text = "Amount in words " + mcSpell.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
                Dim str_paymnts As String = " exec fees.GetReceiptPrint_ECA @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & p_BSU_ID & "'"

                Dim ds1 As New DataSet
                SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
                gvFeeDetails.DataSource = ds1.Tables(0)
                gvFeeDetails.DataBind()
                gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
                Dim HeaderText As String = ""
                If p_BSU_ID = "500605" Then
                    HeaderText = "Course"
                Else
                    HeaderText = "Received ECA Fee"
                End If
                gvFeeDetails.HeaderRow.Cells(0).Text = ""
                gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True

                gvPayments.DataSource = ds1.Tables(1)
                gvPayments.DataBind()

                Dim columnCount As Integer = gvPayments.FooterRow.Cells.Count
                gvPayments.FooterRow.Cells.Clear()
                gvPayments.FooterRow.Cells.Add(New TableCell)
                gvPayments.FooterRow.Cells(0).ColumnSpan = columnCount
                gvPayments.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Left
                gvPayments.FooterRow.Cells(0).Text = "Amount in words : " + mcSpell.SpellNumberWithDenomination(dsReceipt.Tables(0).Rows(0)("FCL_AMOUNT"), dsReceipt.Tables(0).Rows(0)("BSU_CURRENCY"), 0)
                gvPayments.FooterRow.Cells(0).Font.Bold = True
                Dim str_Total As String = gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(4).Text
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Clear()
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).ColumnSpan = columnCount - 1
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).Text = "Total"
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(0).HorizontalAlign = HorizontalAlign.Right

                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells.Add(New TableCell)
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).Text = str_Total
                gvPayments.Rows(gvPayments.Rows.Count - 1).Cells(1).HorizontalAlign = HorizontalAlign.Right
                lblMessageGMS.Text = F_GetECAFeeReceiptNarration(dsReceipt.Tables(0).Rows(0)("FCL_ACD_ID").ToString)
                If lblMessageGMS.Text = "" Then
                    lblMessageGMS.Visible = False
                Else
                    lblMessageGMS.Visible = True
                End If
            End If
        Catch ex As Exception

        End Try
       
    End Sub
    Public Shared Function F_GetECAFeeReceiptNarration(ByVal p_ACD_ID As String) As String
        Dim pParms(0) As SqlClient.SqlParameter
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(0).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(str_conn, _
          CommandType.StoredProcedure, "FEES.F_GetECAFeeReceiptNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function
    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #000000 0pt solid; border-right: #000000 1pt dotted; border-top: #000000 1pt dotted; border-bottom: #000000 1pt dotted;"
        Next
        If e.Row.Cells(0).Text.Trim().Contains("Total") Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToUpper.Trim.StartsWith("TRANSPORT") Then
            If lblNarration.Text.Replace("&nbsp;", "").Trim <> "" Then
                e.Row.Cells(0).Text = lblNarration.Text
            End If
        End If
    End Sub

End Class
