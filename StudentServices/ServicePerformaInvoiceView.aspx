<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ServicePerformaInvoiceView.aspx.vb" Inherits="Fees_ServicePerformaInvoiceView"
    Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
   function CheckForPrint() {
       if (document.getElementById('<%= h_print.ClientID %>').value != '') {
           document.getElementById('<%= h_print.ClientID %>').value = '';

           if (document.getElementById('<%= h_Export.ClientID %>').value == "Yes")
           {
               var result = radopen('../Reports/ASPX Report/RptViewerModalview.aspx', 'pop_up');
           }
           else
               var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
       }

       document.getElementById('<%= h_Export.ClientID %>').value = '';
   }
    );


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="5" cellspacing="0"
                    width="100%">
                    <tr valign="top">
                        <td valign="top" align="left" colspan="2">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <%-- <tr class="subheader_BlueTableView" valign="top">
            <th align="left" colspan="2" valign="middle" >
                <asp:Label ID="lblHead" runat="server"></asp:Label></th>
        </tr>--%>
                    <tr valign="top">
                        <td align="left" valign="middle" width="20%"><span class="field-label">Select Business Unit</span> </td>
                        <td align="left" valign="middle" width="30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True"
                                TabIndex="5">
                            </asp:DropDownList></td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="3">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                                AllowPaging="True" PageSize="25" DataKeyNames="FPH_COMP_ID">
                                <Columns>
                                    <asp:TemplateField HeaderText="Docno">
                                        <HeaderTemplate>
                                            Invoice
                                <br />
                                            <asp:TextBox ID="txtReceiptno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnReceiptSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("FPH_INOICENO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                <br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FPH_DT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade
                                <br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGradeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student ID
                                <br />
                                            <asp:TextBox ID="txtStuno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunoSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name
                                <br />
                                            <asp:TextBox ID="txtStuname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnstunameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("AMOUNT")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                            Description
                                <br />
                                            <asp:TextBox ID="txtDesc" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDescSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("FPH_NARRATION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print Invoice">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbVoucher" OnClick="lbVoucher_Click" runat="server">Print</asp:LinkButton>
                                            <br />
                                            <asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">ViewInvoice</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FPH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFPH_ID" runat="server" Text='<%# Bind("FPH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <%--<asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="SERVICES_BSU_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
            <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>
                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_Export" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
