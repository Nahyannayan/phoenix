﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ServiceProformaInvoice.aspx.vb" Inherits="StudentServices_ServiceProformaInvoice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <style>
       table td {
            vertical-align: top !important;
        }

    </style>
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
                    function CheckForPrint() {
                        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                            document.getElementById('<%= h_print.ClientID %>').value = '';
                            var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
                        }
                    }
                    );

        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }

        function CheckAlready() {
            if (document.getElementById('<%= h_setAlert.ClientID %>').value == '1')
                return confirm('Invoice already exists!!! Do you want to Continue ?')
            else
                return true
        }
        

        function getStudent() {
            var url;

            var NameandCode;
            var result;
            var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
            var COMP_ID = document.getElementById('<%=h_Company_ID.ClientID %>').value;
            var STUD_TYP = document.getElementById('<%=rbEnquiry.ClientID %>').checked;
            var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var BSUID = document.getElementById('<%=ddBusinessunit.ClientID %>').value;
            var chkCompanyFilter = document.getElementById('<%=chkCompanyFilter.ClientID %>').checked;

            var url;

            if (GroupSel == true) {
                if (chkCompanyFilter == true) {
                    url = "../Fees/ShowStudentMulti.aspx?TYPE=SERVICE_COMP&COMP_ID=" + COMP_ID + "&MULTI_SEL=" + GroupSel + "&ACD_ID=" + ACD_ID + "&BSU=" + BSUID;
                }
                else {
                    url = "../Fees/ShowStudentMulti.aspx?TYPE=SERVICE_COMP&MULTI_SEL=" + GroupSel + "&BSU=" + BSUID;
                }
            }
            else
                if (chkCompanyFilter == true) {
                    url = "../Fees/ShowStudent.aspx?type=SERVICE_COMP&COMP_ID=" + COMP_ID + "&BSU=" + BSUID;
                }
                else {
                    url = "../Fees/ShowStudent.aspx?type=SERVICE_COMP&BSU=" + BSUID;
                }
                 result = radopen(url, "pop_up2");

            <%--if (result == '' || result == undefined) {
                            return false;
                        }

                        if (GroupSel == true) {
                            document.getElementById('<%= txtStdNo.ClientID %>').value = "";
                            document.getElementById('<%=txtStudentname.ClientID %>').value = 'Multiple Students selected';
                            document.getElementById('<%=h_Student_no.ClientID %>').value = result;
                            document.getElementById('<%= txtStdNo.ClientID %>').value = "1234";
                        }
                        else {
                            NameandCode = result.split('||');
                            document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
                            document.getElementById('<%= txtStudentname.ClientID %>').value = NameandCode[1];
                            document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                        }
                        return true;--%>
        }

        function OnClientClose2(oWnd, args) {

            var GroupSel = document.getElementById('<%=chkGroupInvoice.ClientID %>').checked;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                if (GroupSel == true) {
                    document.getElementById('<%= txtStdNo.ClientID %>').value = "";
                    document.getElementById('<%=txtStudentname.ClientID %>').value = 'Multiple Students selected';
                    document.getElementById('<%=h_Student_no.ClientID %>').value = arg.NameandCode;
                    document.getElementById('<%= txtStdNo.ClientID %>').value = "";
                }
                else {                  
                    document.getElementById('<%= h_Student_no.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= txtStudentname.ClientID %>').value = NameandCode[1];
                    document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
                }
                __doPostBack('<%= h_Student_no.ClientID%>', 'ValueChanged');
            }
        }


        function getLOCATION() {
            var chk;
            chk = 'notchecked';
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    if (document.forms[0].elements[i].checked == true)
                        chk = 'checked';

                }
            }

            if (chk == 'notchecked') {
                alert('Please select the term')
                return false;
            }
            var url;

            var NameandCode;
            var result;

            url = "../common/PopupFormIDhidden.aspx?iD=LOCATION&bsu=" + document.getElementById('<%= ddBusinessunit.ClientID %>').value + "&MULTISELECT=FALSE";
            result = radopen(url, "pop_up3");
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            <%-- NameandCode = result.split('___');
                        document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];--%>
            //    return true;
        }

         function OnClientClose3(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                 document.getElementById('<%= H_Location.ClientID %>').value = NameandCode[0];                
                __doPostBack('<%= H_Location.ClientID%>', 'TextChanged');
            }
        }


        function GetCompany() {

            var NameandCode;
            var result;
            var url = "../Common/popupForm.aspx?ID=COMPANY&multiSelect=false";
            result = radopen(url, "pop_up4")
            <%--if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];
                return true;
            }
            else {
                return false;
            }--%>
        }

         function OnClientClose4(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById('<%=h_Company_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtCompanyDescr.ClientID %>').value = NameandCode[1];               
                __doPostBack('<%= h_Company_ID.ClientID%>', 'TextChanged');
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_up4" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose4" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Service Performa Invoice
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
                            <%--<asp:Label ID="lblError" runat="server" SkinID="Error" EnableViewState="False" CssClass="error"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <%--<tr class="subheader_img">
            <td colspan="4" align="left">Service Performa Invoice</td>
        </tr>--%>
                    <tr>
                        <td align="left" widh="20%"><span class="field-label">Business Unit</span> </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddBusinessunit" runat="server" AutoPostBack="True" TabIndex="5">
                            </asp:DropDownList>
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink" PopDelay="25" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Company </span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCompanyDescr" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgCompany" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="GetCompany(); return false;" />
                            <asp:LinkButton ID="lnkClearCompany" runat="server"
                                TabIndex="15">Clear </asp:LinkButton>
                            <asp:CheckBox ID="chkCompanyFilter" runat="server" Text="Company Filter" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">CAcademic Year </span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10">
                            </asp:DropDownList></td>
                        <td align="left" width="20%">
                            <span class="field-label">CTerm/Months </span></td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rbTermly" runat="server" Text="Termly" Checked="True"
                                GroupName="trm" AutoPostBack="True" />
                            <asp:RadioButton ID="rbMonthly" runat="server" Text="Monthly" GroupName="trm" AutoPostBack="True" />
                            <asp:Panel ID="pnlLink" runat="server">
                                <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                                    TabIndex="15">Select Month/Term </asp:LinkButton>
                            </asp:Panel>

                            <asp:Panel ID="PanelTree" runat="server" CssClass="Visibility_none" BackColor="#8dc24c"
                                Style="display: none">
                                <div style="border: 1px outset white; width: 300px; text-align: left;">
                                    <asp:TreeView ID="trMonths" ShowCheckBoxes="all" onclick="client_OnTreeNodeChecked();"
                                        runat="server">
                                    </asp:TreeView>
                                    <div style="text-align: center;">
                                        <asp:Button ID="btnNarrInsert" runat="server" CssClass="button_small" Text="Fill Narration" />

                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" class="title-bg">Services </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Select Student</span></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkGroupInvoice" runat="server" Text="Group Invoice" />
                            <asp:RadioButton ID="rbEnrollment" runat="server" Checked="True" GroupName="mode"
                                Text="Student#" AutoPostBack="True" />
                            <asp:RadioButton ID="rbEnquiry" runat="server" GroupName="mode" Text="Enquiry#" AutoPostBack="True"
                                Enabled="False" />
                        </td>
                        <td align="left" width="20%">
                            <span class="field-label">Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server" TabIndex="15"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select Student</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" Width="92%"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return false;" />                           
                        </td>
                        <td>
                             <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False">Add</asp:LinkButton>
                            <asp:Label ID="lblNoStudent" runat="server" EnableViewState="False" CssClass="error"
                                ForeColor="Red"></asp:Label><br />
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">
                    <tr>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                    <tr id="TrGrid2" runat="server">
                        <td align="left" style="width: 20%">
                            <asp:Panel runat="server" ScrollBars="Auto">
                                <asp:GridView ID="gvServices" runat="server" AutoGenerateColumns="False" DataKeyNames="FEE_ID" EnableModelValidation="True" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Service" />
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox runat="server" ID="chkSelectAll" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvStudentDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="STU_NO" HeaderText="Student No" />
                                    <asp:BoundField DataField="STU_NAME" HeaderText="Student Name" />
                                    <asp:TemplateField HeaderText="Amount" Visible="True">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAmount" runat="server" Text='<%# Bind("AMOUNT") %>'>Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%" cellpadding="5">
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Remarks</span>                            
                        </td>
                        <td width="30%" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                TabIndex="100"></asp:TextBox>
                        </td>
                        <td ></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                            <asp:Button ID="btnSave" runat="server" OnClientClick="return CheckAlready()" CssClass="button"
                                Text="Save" TabIndex="105" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="H_Location" runat="server" />
                <asp:HiddenField ID="h_Student_no" runat="server" OnValueChanged="h_Student_no_ValueChanged" />
                <asp:HiddenField ID="h_Edit_STU_ID" runat="server" />
                <asp:HiddenField ID="h_Company_ID" runat="server" />
                <asp:ObjectDataSource ID="odsSERVICES_BSU_M" runat="server" SelectMethod="SERVICES_BSU_M"
                    TypeName="FeeCommon" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="USR_ID" SessionField="sUsr_name" Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="h_setAlert" runat="server" />
                <script src="../Scripts/jquery-1.8.3.min.js" type="text/javascript"></script>
                <script type="text/javascript">
                    $("[id$=chkSelectAll]").live("click", function () {
                        var chkHeader = $(this);
                        var grid = $(this).closest("table");
                        $("[id$='chkSelect']", grid).each(function () {
                            if (chkHeader.is(":checked")) {
                                $(this).attr("checked", "checked");
                                $("td", $(this).closest("tr")).addClass("selected");
                            } else {
                                $(this).removeAttr("checked");
                                $("td", $(this).closest("tr")).removeClass("selected");
                            }
                        });
                    });
                    $("[id$='chkSelect']").live("click", function () {
                        var grid = $(this).closest("table");
                        var chkHeader = $("[id$=chkSelectAll]", grid);
                        if (!$(this).is(":checked")) {
                            $("td", $(this).closest("tr")).removeClass("selected");
                            chkHeader.removeAttr("checked");
                        } else {
                            $("td", $(this).closest("tr")).addClass("selected");
                            if ($("[id$='chkSelect']", grid).length == $("[id$='chkSelect']:checked", grid).length) {
                                chkHeader.attr("checked", "checked");
                            }
                        }
                    });

                </script>
            </div>
        </div>
    </div>
</asp:Content>

