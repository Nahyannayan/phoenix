﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_FeeECA_AdjustmentRequest
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                hlAddNew.NavigateUrl = "FeeECA_AdjustmentRequest_ADD.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ECA_ADJUSTMENT_COLLECTION AndAlso ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ECA_ADJUSTMENT_APPROVAL Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_ECA_ADJUSTMENT_COLLECTION
                            hlAddNew.NavigateUrl = "FeeECA_AdjustmentRequest_ADD.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                            lblHeader.Text = "Fee ECA Adjustment Request"
                            hlAddNew.Visible = True
                        Case OASISConstants.MNU_FEE_ECA_ADJUSTMENT_APPROVAL
                            hlAddNew.Visible = False
                            lblHeader.Text = "Fee ECA Adjustment Approval"
                            hlAddNew.Visible = False
                            'Case OASISConstants.MNU_FEE_ECA_ADJUSTMENT_COLLECTION
                            '    hlAddNew.NavigateUrl = "FeeAdjustment_REQNew.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                            '    lblHeader.Text = "Fee Adjustment Request"
                            '    hlAddNew.Visible = True
                    End Select

                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar2.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFEEAdjustments.PageIndexChanging
        gvFEEAdjustments.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFEEAdjustments.RowDataBound
        Try
            Dim lblFAH_ID As New Label
            lblFAH_ID = TryCast(e.Row.FindControl("lblFAH_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblFAH_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                Select Case ViewState("MainMnu_code").ToString
            
                    Case OASISConstants.MNU_FEE_ECA_ADJUSTMENT_APPROVAL
                        hlEdit.NavigateUrl = "FeeECAAdjustmentApproval.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                    Case OASISConstants.MNU_FEE_ECA_ADJUSTMENT_COLLECTION
                        hlEdit.NavigateUrl = "FeeECA_AdjustmentRequest_ADD.aspx?FAH_ID=" & Encr_decrData.Encrypt(lblFAH_ID.Text) & _
                        "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End Select
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnReceiptSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        gridbind()
    End Sub

  
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub radStud_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStud.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnq.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radRequested_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radRequested.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radApproved.CheckedChanged
        GridBind()
    End Sub

    Protected Sub radReject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReject.CheckedChanged
        GridBind()
    End Sub

    Sub GridBind()
        Try
            Dim ds As New DataSet
            Dim str_Filter As String = String.Empty
            Dim str_Sql As String = String.Empty
            Dim lstrCondn1 As String = String.Empty
            Dim lstrCondn2 As String = String.Empty
            Dim lstrCondn3 As String = String.Empty
            Dim lstrCondn4 As String = String.Empty
            Dim lstrCondn5 As String = String.Empty
            Dim lstrCondn6 As String = String.Empty
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            Dim str_STU_NAME As String = String.Empty
            Dim STU_NAME As String = String.Empty

            Dim str_STU_NO As String = String.Empty
            Dim STU_NO As String = String.Empty
            Dim str_FAR_DATE As String = String.Empty
            Dim FAR_DATE As String = String.Empty
            Dim str_FAR_DOCNO As String = String.Empty
            Dim FAR_DOCNO As String = String.Empty
            Dim str_FAH_REMARKS As String = String.Empty
            Dim FAH_REMARKS As String = String.Empty

            Dim str_ARM_DESCR As String = String.Empty
            Dim ARM_DESCR As String = String.Empty

          
            If gvFEEAdjustments.Rows.Count > 0 Then

                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")

                If txtSearch.Text.Trim <> "" Then
                    STU_NAME = " AND replace(STU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NAME = txtSearch.Text.Trim
                End If

                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudno")

                If txtSearch.Text.Trim <> "" Then
                    STU_NO = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtDate")

                If txtSearch.Text.Trim <> "" Then
                    FAR_DATE = " AND replace(FAR_DATE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FAR_DATE = txtSearch.Text.Trim
                End If

                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtAcademicYear")

                If txtSearch.Text.Trim <> "" Then
                    FAR_DOCNO = " AND replace(FAR_DOCNO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FAR_DOCNO = txtSearch.Text.Trim
                End If


                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")

                If txtSearch.Text.Trim <> "" Then
                    FAH_REMARKS = " AND replace(FAH_REMARKS,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FAH_REMARKS = txtSearch.Text.Trim
                End If


                txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtReason")

                If txtSearch.Text.Trim <> "" Then
                    ARM_DESCR = " AND replace(ARM_DESCR,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_ARM_DESCR = txtSearch.Text.Trim
                End If

             
            End If
            str_Filter = ARM_DESCR + FAH_REMARKS + FAR_DOCNO + FAR_DATE + STU_NO + STU_NAME
            Dim str_cond As String = String.Empty
            Dim vStatus As String = String.Empty
            If radApproved.Checked Then
                vStatus = "A"
            ElseIf radReject.Checked Then
                vStatus = "R"
            ElseIf radRequested.Checked Then
                vStatus = "N"
            End If
            'FEES.FEEADJREQUEST_H.FAR_REMARKS FAH_REMARKS,
            Dim selFilter As String = " FAR_APPRSTATUS = '" & vStatus & "' AND  ISNULL(FAR_bDeleted,0)=0" _
            & " AND FAR_BSU_ID ='" & Session("sBSUID") & "' "

            Dim strGroupBy As String = " GROUP BY FAR_ID, FAR_REMARKS, FAR_DATE, STU_NAME, ACY_DESCR, FAR_BSU_ID,FAR_DOCNO, ARM_DESCR,STU_NO, " & _
            " FAR_EVENT_REF_ID, FAR_EVENT_SOURCE "
            If radStud.Checked Then
                str_Sql = " SELECT * FROM (" & _
                " SELECT FEES.FEEADJREQUEST_H.FAR_ID FAH_ID,  " & _
                " (FEES.FEEADJREQUEST_H.FAR_REMARKS +  " & _
                " (CASE WHEN FEES.FEEADJREQUEST_H.FAR_EVENT_SOURCE = 'TCSO' THEN  '.. Last Attend Date :' + " & _
                " ( SELECT replace(convert(char,TCM_LASTATTDATE,106),' ','/')TCM_LASTATTDATE " & _
                " FROM OASIS.DBO.TCM_M WHERE TCM_ID  = FEES.FEEADJREQUEST_H.FAR_EVENT_REF_ID )  ELSE '' END )) AS FAH_REMARKS, " & _
                " FEES.FEEADJREQUEST_H.FAR_DATE FAH_DATE, VW_OSO_STUDENT_M.STU_NAME, " & _
                " VW_OSO_STUDENT_M.ACY_DESCR, FEES.FEEADJREQUEST_H.FAR_BSU_ID FAH_BSU_ID,VW_OSO_STUDENT_M.STU_NO, " & _
                " SUM(FEES.FEEADJREQUEST_S.FRS_AMOUNT) AMOUNT ,FEES.FEEADJREQUEST_H.FAR_DOCNO, ARM.ARM_DESCR" & _
                " FROM FEES.FEEADJREQUEST_H INNER JOIN VW_OSO_STUDENT_M  " & _
                " ON FEES.FEEADJREQUEST_H.FAR_STU_ID = VW_OSO_STUDENT_M.STU_ID " & _
                " INNER JOIN FEES.FEEADJREQUEST_S ON FAR_ID = FRS_FAR_ID INNER JOIN" & _
                " FEES.ADJ_REASON_M AS ARM ON FEES.FEEADJREQUEST_H.FAR_EVENT = ARM.ARM_ID" & _
                " WHERE isnull(FAR_STU_TYP,'S') = 'S' AND " & selFilter & strGroupBy & _
                ")A WHERE 1=1 " & str_Filter
            ElseIf radEnq.Checked Then
                str_Sql = " SELECT * FROM (" & _
                " SELECT FEES.FEEADJREQUEST_H.FAR_ID FAH_ID,  " & _
                " (FEES.FEEADJREQUEST_H.FAR_REMARKS +  " & _
                " (CASE WHEN FEES.FEEADJREQUEST_H.FAR_EVENT_SOURCE = 'TCSO' THEN  '.. Last Attend Date :' + " & _
                " ( SELECT replace(convert(char,TCM_LASTATTDATE,106),' ','/')TCM_LASTATTDATE " & _
                " FROM OASIS.DBO.TCM_M WHERE TCM_ID  = FEES.FEEADJREQUEST_H.FAR_EVENT_REF_ID )  ELSE '' END )) AS FAH_REMARKS, " & _
                " FEES.FEEADJREQUEST_H.FAR_DATE FAH_DATE, FEES.vw_OSO_ENQUIRY_COMP.STU_NAME, " & _
                " FEES.vw_OSO_ENQUIRY_COMP.ACY_DESCR, FEES.FEEADJREQUEST_H.FAR_BSU_ID FAH_BSU_ID, FEES.vw_OSO_ENQUIRY_COMP.STU_NO," & _
                " SUM(FEES.FEEADJREQUEST_S.FRS_AMOUNT) AMOUNT,FEES.FEEADJREQUEST_H.FAR_DOCNO,ARM.ARM_DESCR " & _
                " FROM FEES.FEEADJREQUEST_H INNER JOIN FEES.vw_OSO_ENQUIRY_COMP  " & _
                " ON FEES.FEEADJREQUEST_H.FAR_STU_ID = FEES.vw_OSO_ENQUIRY_COMP.STU_ID " & _
                " INNER JOIN FEES.FEEADJREQUEST_S ON FAR_ID = FRS_FAR_ID INNER JOIN" & _
                " FEES.ADJ_REASON_M AS ARM ON FEES.FEEADJREQUEST_H.FAR_EVENT = ARM.ARM_ID" & _
                " WHERE FAR_STU_TYP = 'E' AND " & selFilter & strGroupBy & _
                ")A WHERE 1=1 " & str_Filter
            End If
            Dim str_orderby As String = " ORDER BY FAR_DOCNO DESC, FAH_DATE DESC, STU_NAME "

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.Text, str_Sql & str_orderby)
            gvFEEAdjustments.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvFEEAdjustments.DataBind()
                Dim columnCount As Integer = gvFEEAdjustments.Rows(0).Cells.Count

                gvFEEAdjustments.Rows(0).Cells.Clear()
                gvFEEAdjustments.Rows(0).Cells.Add(New TableCell)
                gvFEEAdjustments.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFEEAdjustments.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFEEAdjustments.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvFEEAdjustments.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudname")
            txtSearch.Text = str_STU_NAME

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtstudno")
            txtSearch.Text = str_STU_NO

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtAcademicYear")
            txtSearch.Text = str_FAR_DOCNO

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = str_FAH_REMARKS

            txtSearch = gvFEEAdjustments.HeaderRow.FindControl("txtReason")
            txtSearch.Text = str_ARM_DESCR

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
