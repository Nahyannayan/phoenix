﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class StudentServices_StudPro_detailsNew
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)

        Try
            mvMaster.ActiveViewIndex = Int32.Parse(e.Item.Value)


            Dim i As Integer = Int32.Parse(e.Item.Value)
            mnuChild.Items(0).ImageUrl = "~/Images/buttons/beh_A.jpg"
            mnuChild.Items(1).ImageUrl = "~/Images/buttons/Med_A.jpg"
            mnuChild.Items(2).ImageUrl = "~/Images/buttons/Tran_A.jpg"
            mnuChild.Items(3).ImageUrl = "~/Images/buttons/Lib_A.jpg"
            mnuChild.Items(4).ImageUrl = "~/Images/buttons/CommH_A.jpg"
            mnuChild.Items(5).ImageUrl = "~/Images/buttons/Oth_A.jpg"

            mnuMaster.Items(0).ImageUrl = "~/Images/buttons/Main_A.jpg"
            mnuMaster.Items(1).ImageUrl = "~/Images/buttons/Cont_A.jpg"
            mnuMaster.Items(2).ImageUrl = "~/Images/buttons/sib_A.jpg"
            mnuMaster.Items(3).ImageUrl = "~/Images/buttons/fee_A.jpg"
            mnuMaster.Items(4).ImageUrl = "~/Images/buttons/Att_A.jpg"
            mnuMaster.Items(5).ImageUrl = "~/Images/buttons/Curr_A.jpg"
            mnuMaster.Items(6).ImageUrl = "~/Images/buttons/Tim_A.jpg"

            Select Case i
                Case 0
                    mnuMaster.Items(0).ImageUrl = "~/Images/buttons/Main_B.jpg"

                Case 1
                    mnuMaster.Items(1).ImageUrl = "~/Images/buttons/Cont_B.jpg"

                Case 2
                    mnuMaster.Items(2).ImageUrl = "~/Images/buttons/sib_B.jpg"
                Case 3
                    mnuMaster.Items(3).ImageUrl = "~/Images/buttons/fee_B.jpg"
                Case 4
                    mnuMaster.Items(4).ImageUrl = "~/Images/buttons/Att_B.jpg"

                Case 5
                    mnuMaster.Items(5).ImageUrl = "~/Images/buttons/Curr_B.jpg"
                Case 6
                    mnuMaster.Items(6).ImageUrl = "~/Images/buttons/Tim_B.jpg"

            End Select
            ' menuShow()
            'tabDis()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuMaster_MenuItemClick")
        End Try
    End Sub

    Protected Sub mnuChild_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)
        Try
            mvMaster.ActiveViewIndex = Int32.Parse(e.Item.Value)

            Dim i As Integer = Int32.Parse(e.Item.Value) - 7

            mnuMaster.Items(0).ImageUrl = "~/Images/buttons/Main_A.jpg"
            mnuMaster.Items(1).ImageUrl = "~/Images/buttons/Cont_A.jpg"
            mnuMaster.Items(2).ImageUrl = "~/Images/buttons/sib_A.jpg"
            mnuMaster.Items(3).ImageUrl = "~/Images/buttons/fee_A.jpg"
            mnuMaster.Items(4).ImageUrl = "~/Images/buttons/Att_A.jpg"
            mnuMaster.Items(5).ImageUrl = "~/Images/buttons/Curr_A.jpg"
            mnuMaster.Items(6).ImageUrl = "~/Images/buttons/Tim_A.jpg"



            mnuChild.Items(0).ImageUrl = "~/Images/buttons/beh_A.jpg"
            mnuChild.Items(1).ImageUrl = "~/Images/buttons/Med_A.jpg"
            mnuChild.Items(2).ImageUrl = "~/Images/buttons/Tran_A.jpg"
            mnuChild.Items(3).ImageUrl = "~/Images/buttons/Lib_A.jpg"
            mnuChild.Items(4).ImageUrl = "~/Images/buttons/CommH_A.jpg"
            mnuChild.Items(5).ImageUrl = "~/Images/buttons/Oth_A.jpg"

            Select Case i
                Case 0
                    mnuChild.Items(0).ImageUrl = "~/Images/buttons/beh_B.jpg"

                Case 1
                    mnuChild.Items(1).ImageUrl = "~/Images/buttons/Med_B.jpg"

                Case 2
                    mnuChild.Items(2).ImageUrl = "~/Images/buttons/Tran_B.jpg"
                Case 3
                    mnuChild.Items(3).ImageUrl = "~/Images/buttons/Lib_B.jpg"
                Case 4
                    mnuChild.Items(4).ImageUrl = "~/Images/buttons/CommH_B.jpg"

                Case 5
                    mnuChild.Items(5).ImageUrl = "~/Images/buttons/Oth_B.jpg"

            End Select
            'menuShow()
            'tabDis()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuMaster_MenuItemClick")
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Request.QueryString("bsuid").ToString
                Dim USR_NAME As String = Session("sUsr_name")


                mvMaster.ActiveViewIndex = 0
                'mnuMaster.Items(0).ImageUrl = "~/Images/buttons/Main_B.jpg"
                BindBsuDetails()


                'If (UCase(ViewState("stu_status")) = "TC" Or UCase(ViewState("stu_status")) = "SO") Then

                Dim conn As SqlConnection = ConnectionManger.GetOASISConnection

                Dim PARAM(1) As SqlParameter
                PARAM(0) = New SqlParameter("@USR_NAME", Session("sUsr_name"))
                Using reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "STU.GETSTU_PROFILE_TAB_RIGHTS_TCSO", PARAM)
                    If reader.HasRows = True Then
                        Session("tabsuperuser") = True
                        Session("tabDis") = 0
                    Else
                        'mnuMaster.Items(2).Enabled = False
                        'mnuMaster.Items(3).Enabled = False
                        'mnuMaster.Items(4).Enabled = False
                        'mnuMaster.Items(5).Enabled = False
                        'mnuMaster.Items(6).Enabled = False
                        'mnuChild.Items(0).Enabled = False
                        'mnuChild.Items(1).Enabled = False
                        'mnuChild.Items(2).Enabled = False
                        'mnuChild.Items(3).Enabled = False
                        'mnuChild.Items(4).Enabled = False
                        'mnuChild.Items(5).Enabled = False
                        Session("tabDis") = 1
                        'tabDis()
                        Session("tabsuperuser") = False
                    End If

                End Using
                'End If

                Dim MNu_code As String = Session("MainMnu_code_pro")
                Dim RoleID As String = String.Empty
                Using reader_Rol_ID As SqlDataReader = AccessRoleUser.GetRoleID_user(Session("sUsr_name"))
                    If reader_Rol_ID.HasRows = True Then
                        While reader_Rol_ID.Read()
                            RoleID = Convert.ToString(reader_Rol_ID("USR_ROL_ID"))
                        End While
                    End If
                End Using
                Dim T_code As String = String.Empty
                Dim T_right As String = String.Empty

                Dim ht_tab As New Hashtable()
                Using readertab_Access As SqlDataReader = AccessRoleUser.GetTabRights(RoleID, Session("sBsuid"), MNu_code)
                    If readertab_Access.HasRows = True Then
                        While readertab_Access.Read()
                            T_code = CInt(Convert.ToString(readertab_Access("TAB_CODE"))) - 20
                            T_right = Convert.ToString(readertab_Access("TAR_RIGHT"))
                            ht_tab.Add(T_code, T_right)
                        End While
                    End If
                End Using

                If BSU_RIGHTS() = 1 Then
                    Session("BSU_TAB_SUPER") = True
                    Session("tabsuperuser") = True
                    Session("tabDis") = 0
                Else
                    Session("BSU_TAB_SUPER") = False
                End If
                Session("tab_Right_User") = ht_tab

                'Call menuShow()

                Session("DB_Stu_ID") = Request.QueryString("id")
                StudHeader(Session("DB_Stu_ID"), CurBsUnit)

                ' End If
                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Public Sub BindBsuDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select BSU_NAME,BSU_ADDRESS,BSU_POBOX,BSU_CITY,BSU_TEL,BSU_FAX,BSU_EMAIL,BSU_MOE_LOGO,BSU_URL from BUSINESSUNIT_M where BSU_ID='" & Session("sBsuid") & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        BsuName.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
        bsuAddress.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_ADDRESS").ToString()
        bsupostbox.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_POBOX").ToString()
        bsucity.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_CITY").ToString()
        bsutelephone.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_TEL").ToString()
        bsufax.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_FAX").ToString()
        bsuemail.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_EMAIL").ToString()
        bsuwebsite.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_URL").ToString()
        imglogo.ImageUrl = ds.Tables(0).Rows(0).Item("BSU_MOE_LOGO").ToString()
    End Sub
    Sub tabDis()
        If Session("sBusper") = False Then

            If Session("tabDis") = 1 Then
                mnuMaster.Items(2).ImageUrl = "~/Images/buttons/sib_L.jpg"
                mnuMaster.Items(2).Enabled = False
                mnuMaster.Items(2).ToolTip = "Tab is locked/disabled!!!"
                mnuMaster.Items(3).ImageUrl = "~/Images/buttons/fee_L.jpg"
                mnuMaster.Items(3).Enabled = False
                mnuMaster.Items(3).ToolTip = "Tab is locked/disabled!!!"
                mnuMaster.Items(4).ImageUrl = "~/Images/buttons/Att_L.jpg"
                mnuMaster.Items(4).Enabled = False
                mnuMaster.Items(4).ToolTip = "Tab is locked/disabled!!!"
                mnuMaster.Items(5).ImageUrl = "~/Images/buttons/Curr_L.jpg"
                mnuMaster.Items(5).Enabled = False
                mnuMaster.Items(5).ToolTip = "Tab is locked/disabled!!!"
                mnuMaster.Items(6).ImageUrl = "~/Images/buttons/Tim_L.jpg"
                mnuMaster.Items(6).Enabled = False
                mnuMaster.Items(6).ToolTip = "Tab is locked/disabled!!!"
                mnuChild.Items(0).ImageUrl = "~/Images/buttons/beh_L.jpg"
                mnuChild.Items(0).Enabled = False
                mnuChild.Items(0).ToolTip = "Tab is locked/disabled!!!"
                mnuChild.Items(1).ImageUrl = "~/Images/buttons/Med_L.jpg"
                mnuChild.Items(1).Enabled = False
                mnuChild.Items(1).ToolTip = "Tab is locked/disabled!!!"
                mnuChild.Items(2).ImageUrl = "~/Images/buttons/Tran_L.jpg"
                mnuChild.Items(2).Enabled = False
                mnuChild.Items(2).ToolTip = "Tab is locked/disabled!!!"
                mnuChild.Items(3).ImageUrl = "~/Images/buttons/Lib_L.jpg"
                mnuChild.Items(3).Enabled = False
                mnuChild.Items(3).ToolTip = "Tab is locked/disabled!!!"
                mnuChild.Items(4).ImageUrl = "../Images/buttons/CommH_L.jpg"
                mnuChild.Items(4).Enabled = False
                mnuChild.Items(4).ToolTip = "Tab is locked/disabled!!!"
                mnuChild.Items(5).ImageUrl = "~/Images/buttons/Oth_L.jpg"
                mnuChild.Items(5).Enabled = False
                mnuChild.Items(5).ToolTip = "Tab is locked/disabled!!!"
            End If
        End If
    End Sub
    

    Sub menuShow()

        If (Session("BSU_TAB_SUPER") = False) Then
            If Session("sBusper") = False Then
                Dim i As Integer
                Dim FirstFlag As Boolean = True
                '0 to 6 is mnuMaster
                For i = 0 To mnuMaster.Items.Count - 1
                    Dim str1 As String = i
                    If Session("tab_Right_User").ContainsKey(str1) Then
                        If FirstFlag And (Session("tab_Right_User").Item(str1) = "1" Or Session("tab_Right_User").Item(str1) = "2") Then
                            'mvMaster.ActiveViewIndex = i
                            FirstFlag = False
                        End If
                        If Session("tab_Right_User").Item(str1) = "1" Then
                            mnuMaster.Items(i).Selectable = True
                            'SetPanelEnabled(i, False)
                        ElseIf Session("tab_Right_User").Item(str1) = "2" Then
                            mnuMaster.Items(i).Selectable = True
                            'SetPanelEnabled(i, True)
                        End If
                    Else
                        Select Case i
                            Case 0
                                mnuMaster.Items(0).ImageUrl = "~/Images/buttons/Main_L.jpg"
                                mnuMaster.Items(0).Enabled = False
                                mnuMaster.Items(0).ToolTip = "Tab is locked/disabled!!!"
                            Case 1
                                mnuMaster.Items(1).ImageUrl = "~/Images/buttons/Cont_L.jpg"
                                mnuMaster.Items(1).Enabled = False
                                mnuMaster.Items(1).ToolTip = "Tab is locked/disabled!!!"
                            Case 2
                                mnuMaster.Items(2).ImageUrl = "~/Images/buttons/sib_L.jpg"
                                mnuMaster.Items(2).Enabled = False
                                mnuMaster.Items(2).ToolTip = "Tab is locked/disabled!!!"
                            Case 3
                                mnuMaster.Items(3).ImageUrl = "~/Images/buttons/fee_L.jpg"
                                mnuMaster.Items(3).Enabled = False
                                mnuMaster.Items(3).ToolTip = "Tab is locked/disabled!!!"
                            Case 4
                                mnuMaster.Items(4).ImageUrl = "~/Images/buttons/Att_L.jpg"
                                mnuMaster.Items(4).Enabled = False
                                mnuMaster.Items(4).ToolTip = "Tab is locked/disabled!!!"
                            Case 5
                                mnuMaster.Items(5).ImageUrl = "~/Images/buttons/Curr_L.jpg"
                                mnuMaster.Items(5).Enabled = False
                                mnuMaster.Items(5).ToolTip = "Tab is locked/disabled!!!"
                            Case 6
                                mnuMaster.Items(6).ImageUrl = "~/Images/buttons/Tim_L.jpg"
                                mnuMaster.Items(6).Enabled = False
                                mnuMaster.Items(6).ToolTip = "Tab is locked/disabled!!!"

                        End Select

                        'mnuMaster.Items(i).ImageUrl = ""
                    End If
                Next
                '7 to 12 is mnuChild
                For i = 0 To mnuChild.Items.Count - 1
                    Dim str1 As String = i + 7
                    If Session("tab_Right_User").ContainsKey(str1) Then
                        If FirstFlag And (Session("tab_Right_User").Item(str1) = "1" Or Session("tab_Right_User").Item(str1) = "2") Then
                            'mvMaster.ActiveViewIndex = i
                            FirstFlag = False
                        End If
                        If Session("tab_Right_User").Item(str1) = "1" Then
                            mnuChild.Items(i).Selectable = True
                            'SetPanelEnabled(i, False)
                        ElseIf Session("tab_Right_User").Item(str1) = "2" Then
                            mnuChild.Items(i).Selectable = True
                            'SetPanelEnabled(i, True)
                        End If
                    Else
                        Select Case i
                            Case 0
                                mnuChild.Items(0).ImageUrl = "~/Images/buttons/beh_L.jpg"
                                mnuChild.Items(0).Enabled = False
                                mnuChild.Items(0).ToolTip = "Tab is locked/disabled!!!"

                            Case 1
                                mnuChild.Items(1).ImageUrl = "~/Images/buttons/Med_L.jpg"
                                mnuChild.Items(1).Enabled = False
                                mnuChild.Items(1).ToolTip = "Tab is locked/disabled!!!"


                            Case 2
                                mnuChild.Items(2).ImageUrl = "~/Images/buttons/Tran_L.jpg"
                                mnuChild.Items(2).Enabled = False
                                mnuChild.Items(2).ToolTip = "Tab is locked/disabled!!!"

                            Case 3
                                mnuChild.Items(3).ImageUrl = "~/Images/buttons/Lib_L.jpg"
                                mnuChild.Items(3).Enabled = False
                                mnuChild.Items(3).ToolTip = "Tab is locked/disabled!!!"

                            Case 4
                                mnuChild.Items(4).ImageUrl = "../Images/buttons/CommH_L.jpg"
                                mnuChild.Items(4).Enabled = False
                                mnuChild.Items(4).ToolTip = "Tab is locked/disabled!!!"


                            Case 5
                                mnuChild.Items(5).ImageUrl = "~/Images/buttons/Oth_L.jpg"
                                mnuChild.Items(5).Enabled = False
                                mnuChild.Items(5).ToolTip = "Tab is locked/disabled!!!"


                        End Select
                        'mnuChild.Items(i).ImageUrl = ""
                    End If
                Next


            End If

        End If
    End Sub



    Function BSU_RIGHTS() As Integer

        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlString As String = ""
        sqlString = " select count(bsu_id) from(select bsu_id from businessunit_m where bsu_clm_id in(19,13,14,10,18,2,22,4,6) " & _
" and bus_bsg_id<>4 and bsu_id<>'125017'  and bsu_bShow=1)A WHERE BSU_ID='" & Session("sBsuid") & "'"
        Dim STATUS As Integer = CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlString))
        Return STATUS
    End Function
    Sub StudHeader(ByVal Stu_ID As String, ByVal bsuid As String)

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", Stu_ID)
        'param(1) = New SqlParameter("@STU_BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_BSU_ID", bsuid)


        Try


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[GETSTU_PROFILE_HEADER_DETAILS]", param)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ltStudName.Text = Convert.ToString(readerStudent_Detail("Sname"))
                        ltStudId.Text = Convert.ToString(readerStudent_Detail("stu_no"))
                        ltCLM.Text = Convert.ToString(readerStudent_Detail("clm"))
                        ltGrd.Text = Convert.ToString(readerStudent_Detail("grm_display"))
                        ltSct.Text = Convert.ToString(readerStudent_Detail("sct_descr"))
                        ltShf.Text = Convert.ToString(readerStudent_Detail("shf"))
                        ltStm.Text = Convert.ToString(readerStudent_Detail("stm"))
                        ltStatus.Text = Convert.ToString(readerStudent_Detail("stu_currstatus"))
                        ltFather.Text = Convert.ToString(readerStudent_Detail("fname"))
                        ltMother.Text = Convert.ToString(readerStudent_Detail("mname"))
                        ltParUserName.Text = Convert.ToString(readerStudent_Detail("ParUserName"))
                        ltStuUserName.Text = Convert.ToString(readerStudent_Detail("StudUserName"))
                        ltminlist.Text = Convert.ToString(readerStudent_Detail("STU_MINLIST"))
                        ltmintype.Text = Convert.ToString(readerStudent_Detail("STU_MINLISTTYPE"))
                        ltHouse.Text = Convert.ToString(readerStudent_Detail("HOUSE"))

                        If IsDate(readerStudent_Detail("STU_LastAttDate")) = True Then
                            ltLDA.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_LastAttDate"))))
                        End If


                        If IsDate(readerStudent_Detail("STU_LEAVEDATE")) = True Then
                            ltLD.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_LEAVEDATE"))))
                        End If


                        If Trim(ltCLM.Text) = "" Then
                            trCurr.Visible = False
                        Else
                            trCurr.Visible = True
                        End If

                        If Trim(ltShf.Text) = "" Then
                            trshf.Visible = False
                        Else
                            trshf.Visible = True
                        End If
                        If Trim(ltStm.Text) = "" Then
                            trstm.Visible = False
                        Else
                            trstm.Visible = True
                        End If

                        Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                        Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                        Dim strImagePath As String = String.Empty
                        If strPath <> "" Then
                            strImagePath = connPath & strPath
                            imgEmpImage.ImageUrl = strImagePath
                        End If

                    End While

                End If

            End Using
        Catch ex As Exception

        End Try
    End Sub

End Class
