﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization

Partial Class StudentServices_OldActivityList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load    
        ' Dim MainMnu_code As String
        ViewState("datamode") = "none"
        ' If Request.QueryString("MainMnu_code") <> "" Then
        'MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ' ViewState("MainMnu_code") = MainMnu_code
        ' Else
        ' MainMnu_code = ""
        ' End If
        If Request.QueryString("ACD_ID") <> "" Then
            ViewState("ACD_ID") = Encr_decrData.Decrypt(Request.QueryString("ACD_ID").Replace(" ", "+"))
        Else
            ViewState("ACD_ID") = ""
        End If
        ' If Request.QueryString("viewid") <> "" Then
        'ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
        ' Else
        '   ViewState("ViewId") = ""
        ' End If
        '  ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
        ' ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))
        ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
        Dim dsData As DataSet = Nothing
        Dim Param(2) As SqlParameter
        Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@USER", ViewState("EmployeeID"), SqlDbType.VarChar)
        Param(2) = Mainclass.CreateSqlParameter("@ACD_ID", Convert.ToInt32(ViewState("ACD_ID")), SqlDbType.BigInt)
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "[OASIS].[GET_ALL_ACTIVITIES_FROM_DB_FOR_USER]"
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            radioOldActList.DataSource = dsData
            radioOldActList.DataTextField = "EVENT_NAME"
            radioOldActList.DataValueField = "ID"
            radioOldActList.DataBind()
        End Using
    End Sub
End Class
