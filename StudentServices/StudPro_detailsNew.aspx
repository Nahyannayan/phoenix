﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="StudPro_detailsNew.aspx.vb" Inherits="StudentServices_StudPro_detailsNew" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>:::: GEMS :::: Student Profile ::::</title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <%--   <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />    
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" /--%>>


      <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

    <link href="../cssfiles/StudDashBoard.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap header files ends here -->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta content="MShtml 6.00.2900.3268" name="GENERATOR" />


    <script>

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

    </script>

    <script type="text/javascript">


        //          window.setTimeout('setpath()',700);
        //      function setpath()
        //        {
        //          var objFrame=document.getElementById("ifMainDetail"); 
        //                   
        //          objFrame.scr="../Tabpages/stuContactdetails.aspx" 
        // alert( objFrame.scr)
        //          var objFrame=document.getElementById("ifContDetail"); 
        //          var objFrame=document.getElementById("ifSibDetail"); 
        //          var objFrame=document.getElementById("ifFeeDetail"); 
        //          var objFrame=document.getElementById("ifAttDetail"); 
        //          var objFrame=document.getElementById("ifCurrDetail"); 
        //          var objFrame=document.getElementById("ifBehDetail"); 
        //          var objFrame=document.getElementById("ifTranDetail"); 
        //          var objFrame=document.getElementById("ifLibDetail"); 
        //          var objFrame=document.getElementById("ifCommHDetail"); 
        //          var objFrame=document.getElementById("ifOthDetail"); 
        //          
        //          
        // 
        //        }
        //    

        function printTable() {
            var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            disp_setting += "scrollbars=yes,width=1280, height=1000, left=2, top=2";
            var content_vlue = document.getElementById('tblStudProfile').innerHTML;

            var docprint = window.open("", "", disp_setting);
            docprint.document.open();
            docprint.document.write('<html><head>');
            //docprint.document.write('<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />');
            docprint.document.write('</head><body onLoad="self.print();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');
            docprint.document.write(content_vlue);
            docprint.document.write('</center></body></html>');
            docprint.document.close();
            docprint.focus();
        }
        function printAll() {

            var sFeatures;
            sFeatures = "dialogWidth: 950px; ";
            sFeatures += "dialogHeight: 750px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/Tabpages/StudPro_details_printAll.aspx?id=0';
            //alert(status) 

            result = window.open(url);
            if (result == '' || result == undefined) {
                return false;
            }

        }

    </script>
</head>
<body leftmargin="0" topmargin="0">
    <form id="Form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="padding: 1px; margin-bottom: 1px; margin-top: 1px;">
                    <asp:LinkButton ID="lbPrintTab" runat="server" Text="Print Selected Tab" OnClientClick="javascript: window.print();return false;"></asp:LinkButton>
                    <asp:LinkButton ID="lbPrintAll" runat="server" Text="Print All" OnClientClick="javascript: printAll();return false;">
                    </asp:LinkButton>
                </div>
                <table id="tblStudProfile" width="100%">
                    <tr>
                        <td align="center" valign="middle">
                            <table cellspacing="0" cellpadding="0" align="center" border="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td valign="top" align="left">
                                            <asp:Image ID="imglogo" runat="server" Height="80" />
                                        </td>
                                        <td align="left" width="100%">
                                            <table width="100%" border="0" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td align="center"><span id="BsuName" runat="server"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <span id="bsuAddress" runat="server"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center"><span id="bsupostbox" runat="server"></span>, <span id="bsucity" runat="server"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center"><span id="bsutelephone" runat="server"></span>, <span id="bsufax" runat="server"></span>
                                                            ,<span id="bsuemail" runat="server"></span> : <span id="bsuwebsite" runat="server"></span></td>
                                                    </tr>
                                                    <%--<tr>
                                                        <td style="background-color: #ffffff; text-align: center"></td>
                                                    </tr>--%>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" height="10">
                                            <img
                                                src="../Images/colourbar.jpg" width="100%" /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>

                        <td>

                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" style="text-align: left">
                                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                            EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
                                        <%-- <span style="color:red"></span>--%>


                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <table align="left" width="100%">
                                            <tr class="title-bg">
                                                <td align="left" valign="middle"><span class="field-label">Student Profile</span></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <table id="Table1" align="left" style="width: 100%" runat="server">
                                                        <tr>
                                                            <td align="left"><span class="field-label">Name</span></td>
                                                            <td colspan="4" align="left">
                                                                <asp:Literal ID="ltStudName" runat="server"></asp:Literal></td>
                                                            <td align="center" colspan="3" rowspan="11">
                                                                <asp:Image ID="imgEmpImage" runat="server" ImageUrl="~/Images/Photos/no_image.gif" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Student ID</span> </td>
                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltStudId" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr runat="server" id="trCurr">
                                                            <td align="left"><span class="field-label">Curriculum
                                                            </span></td>
                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltCLM" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Grade</span></td>
                                                            <td colspan="0" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltGrd" runat="server"></asp:Literal></font></td>
                                                            <td align="left"><span class="field-label">Section</span></td>
                                                            <td style="text-align: left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltSct" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">House</span></td>

                                                            <td align="left" colspan="4"><font color="black">
                                                                <asp:Literal ID="ltHouse" runat="server"></asp:Literal></font>
                                                            </td>

                                                        </tr>
                                                        <tr runat="server" id="trshf">
                                                            <td align="left"><span class="field-label">Shift</span></td>

                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltShf" runat="server"></asp:Literal></font><br />
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="trstm">
                                                            <td align="left"><span class="field-label">Stream</span></td>

                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltStm" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Current Status</span></td>

                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltStatus" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Father&#39;s Name</span></td>

                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltFather" runat="server"></asp:Literal></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Mother&#39;s Name</span></td>

                                                            <td colspan="4" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltMother" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Parent User Name</span></td>

                                                            <td align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltParUserName" runat="server"></asp:Literal></font></td>

                                                            <td align="left"><span class="field-label">Student User Name</span></td>

                                                            <td style="text-align: left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltStuUserName" runat="server"></asp:Literal></font></td>

                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">Ministry List</span></td>

                                                            <td colspan="0" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltminlist" runat="server"></asp:Literal></font></td>
                                                            <td align="left"><span class="field-label">Ministry Type</span></td>

                                                            <td style="text-align: left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltmintype" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left"><span class="field-label">TC Last Att Date</span></td>

                                                            <td colspan="0" align="left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltLDA" runat="server"></asp:Literal></font></td>
                                                            <td align="left"><span class="field-label">TC Leave Date</span></td>

                                                            <td style="text-align: left">
                                                                <font color="black">
                                                                    <asp:Literal ID="ltLD" runat="server"></asp:Literal></font></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%-- <tr>
                                    <td  align="right">

                                        <!--<a href="javascript:window.print()" ><img   src="../Images/Misc/print.gif" style="width: 30px; border-top-style: none; border-right-style: none; border-left-style: none;  height: 30px; border-bottom-style: none; font-weight: bold; " alt="Print this page"></a>    -->
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="center">
                                        <div id="topDiv">
                                            <asp:Menu ID="mnuMaster" runat="server" OnMenuItemClick="mnuMaster_MenuItemClick" CssClass="menu_a"
                                                Orientation="Horizontal">
                                                <Items>
                                                    <%-- <asp:MenuItem ImageUrl="../Images/buttons/Main_A.jpg" Selected="True" Text="" Value="0"></asp:MenuItem>--%>
                                                    <asp:MenuItem Text="Contact Info" Value="1" Selected="true"></asp:MenuItem>
                                                    <%--<asp:MenuItem ImageUrl="../Images/buttons/sib_A.jpg" Text="" Value="2"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/fee_A.jpg" Text="" Value="3" ></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/att_A.jpg" Text="" Value="4"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/curr_A.jpg" Text="" Value="5"></asp:MenuItem>
                          <asp:MenuItem ImageUrl="../Images/buttons/Tim_A.jpg" Text="" Value="6"></asp:MenuItem>     --%>
                                                </Items>
                                                <%--<StaticMenuItemStyle Font-Strikeout="False" />
                                                <StaticSelectedStyle />
                                                <DynamicSelectedStyle />--%>
                                                <StaticMenuItemStyle CssClass="menuItem" />
                                                <StaticSelectedStyle CssClass="selectedItem" />

                                            </asp:Menu>
                                        </div>
                                        <div id="bottomDiv">

                                            <asp:Menu ID="mnuChild" runat="server"
                                                Orientation="Horizontal" OnMenuItemClick="mnuChild_MenuItemClick">
                                                <Items>
                                                    <%--<asp:MenuItem ImageUrl="../Images/buttons/beh_A.jpg"  Text="" Value="7"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/Med_A.jpg" Text="" Value="8"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/Tran_A.jpg" Text="" Value="9"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/Lib_A.jpg" Text="" Value="10"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/CommH_A.jpg" Text="" Value="11"></asp:MenuItem>
                        <asp:MenuItem ImageUrl="../Images/buttons/Oth_A.jpg" Text="" Value="12"></asp:MenuItem>--%>
                                                </Items>
                                                <StaticMenuItemStyle Font-Strikeout="False" />
                                                <StaticSelectedStyle />
                                                <DynamicSelectedStyle />
                                            </asp:Menu>
                                        </div>
                                        <asp:MultiView ID="mvMaster" runat="server" ActiveViewIndex="0">

                                            <%--<asp:View ID="vwMain" runat="server">
                       <div align="center" style="margin-top:0px;">
                       <iframe id="ifMainDetail"  height="700" src="../Students/Tabpages/STU_Maindetails.aspx"    scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                       </div>
                    </asp:View>--%>
                                            <asp:View ID="vwCont" runat="server">
                                                <div align="center" style="margin-top: 0px;">
                                                    <iframe id="ifContDetail" width="100%" height="700" src="../Students/Tabpages/stuContactdetails.aspx" scrolling="auto" marginwidth="0px" frameborder="0"></iframe>
                                                </div>
                                            </asp:View>
                                            <%-- <asp:View ID="vwSib" runat="server">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifSibDetail" height="700" src="../Students/Tabpages/StuSiblingDetails.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                   </div>
                    </asp:View>
                    <asp:View ID="vwFee" runat="server" EnableTheming="True">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifFeeDetail" height="700" src="../Students/Tabpages/STU_TAB_FEE.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                    </div>
                    </asp:View>
                     <asp:View ID="vwAtt" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                       <iframe id="ifAttDetail" height="700" src="../Students/Tabpages/StuAttDetail.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                   
                    </asp:View>
                     <asp:View ID="vwCurr" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifCurrDetail" height="700" src="../Students/Tabpages/STU_TAB_curr.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                    <asp:View ID="vwTime" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifTimeDetail" height="700" src="../Students/Tabpages/STU_TAB_Time.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                    </div>
                    </asp:View>
                     <asp:View ID="vwBeh" runat="server" EnableTheming="True">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifBehDetail" height="700" src="../Students/Tabpages/STU_TAB_PASTORAL.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                   </div>
                    </asp:View>
                     <asp:View ID="vwMed" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifMedDetail" height="700" src="../Students/Tabpages/STU_TAB_MED.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwTran" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifTranDetail" height="700" src="../Students/Tabpages/STU_TAB_TPT.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwLib" runat="server" EnableTheming="True">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifLibDetail" height="700" src="../Students/Tabpages/STU_TAB_LIB.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwCommH" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifCommHDetail" height="700" src="../Students/Tabpages/STU_TAB_COM.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwOth" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifOthDetail" height="700" src="../Students/Tabpages/STU_TAB_OTH.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>--%>
                                        </asp:MultiView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>


