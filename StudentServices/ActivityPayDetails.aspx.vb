﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports Mainclass
Imports UtilityObj
Partial Class StudentServices_ActivityPayDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("ALD_ID") <> "" Then
            ViewState("ALD_ID") = Encr_decrData.Decrypt(Request.QueryString("ALD_ID").Replace(" ", "+"))
        Else
            ViewState("ALD_ID") = 0
        End If
        'If Request.QueryString("AMT") <> "" Then
        '    ViewState("Amount") = Encr_decrData.Decrypt(Request.QueryString("AMT").Replace(" ", "+"))
        '    lblAmountPay.Text = FeeCollection.GetDoubleVal(ViewState("Amount"))
        'Else
        '    ViewState("Amount") = ""
        ' End If
        If Request.QueryString("TYPE") <> "" Then
            ViewState("S_TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
        Else
            ViewState("S_TYPE") = ""
        End If
        If Request.QueryString("APD_ID") <> "" Then
            ViewState("APD_ID") = Encr_decrData.Decrypt(Request.QueryString("APD_ID").Replace(" ", "+"))
        Else
            ViewState("APD_ID") = ""
        End If
        If Request.QueryString("STU_ID") <> "" Then
            ViewState("STU_ID") = Encr_decrData.Decrypt(Request.QueryString("STU_ID").Replace(" ", "+"))
        Else
            ViewState("STU_ID") = ""
        End If
        If (Request.QueryString("COLMODE")) <> "" Then
            ViewState("COLMODE") = Encr_decrData.Decrypt(Request.QueryString("COLMODE").Replace(" ", "+"))
        Else
            ViewState("COLMODE") = ""
        End If
        If (Request.QueryString("BSU_ID")) <> "" Then
            ViewState("BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("BSU_ID").Replace(" ", "+"))
        Else
            ViewState("BSU_ID") = ""
        End If
        Dim ds As DataSet
        ds = loaddloaddetails()
        If ds.Tables(0).Rows.Count > 0 Then
            lblEventName.Text = ds.Tables(0).Rows(0)("ALD_EVENT_NAME")
            lblEventOn.Text = ds.Tables(0).Rows(0)("EVENT_DATE")
            lblStudNam.Text = ds.Tables(0).Rows(0)("STU_NAME")
            lblStudNum.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblgrdtxt.Text = ds.Tables(0).Rows(0)("STU_GRD_ID")
            lblscttxt.Text = ds.Tables(0).Rows(0)("SCT_DESCR")
            lblAmountPay.Text = ds.Tables(0).Rows(0)("ALD_EVENT_AMT")
            ViewState("Amount") = ds.Tables(0).Rows(0)("ALD_EVENT_AMT")
        End If

        If (ViewState("COLMODE") = "FC") Then
            tblFeeCollection.Visible = True
        ElseIf (ViewState("COLMODE") = "OC") Then
            tblOtherCollection.Visible = True
            BindCollectionType()
            tr_Cash.Visible = False
            tr_Cheque1.Visible = False
            tr_Cheque2.Visible = False
            tr_CreditCard.Visible = False
            SET_PAYMENT_MODE_AMOUNT()
        End If
        'hfTaxable.Value = IIf(DirectCast(Session("BSU_bGSTEnabled"), Boolean) = True, "1", "0")
        'Session("FeeCollection") = FeeCollectionOther.CreateFeeCollectionOther()
        ShowMessage("", False)
    End Sub
    Protected Function loaddloaddetails() As DataSet
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim paramS(1) As SqlClient.SqlParameter
        Try
            paramS(0) = New SqlClient.SqlParameter("@VIEWID", ViewState("ALD_ID"))
            paramS(1) = New SqlClient.SqlParameter("@STUID", ViewState("STU_ID"))
            ds = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENTPORTAL", paramS)
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds
    End Function
    Sub BindCollectionType()
        'If ddlPaymentMode.Items.Count = 0 Then
        '    ddlPaymentMode.Items.Add(New ListItem("Cash", COLLECTIONTYPE.CASH))
        '    ddlPaymentMode.Items.Add(New ListItem("Cheque", COLLECTIONTYPE.CHEQUES))
        '    ddlPaymentMode.Items.Add(New ListItem("Credit Card", COLLECTIONTYPE.CREDIT_CARD))
        'End If
        If rblPaymentModes.Items.Count = 0 Then
            rblPaymentModes.Items.Add(New ListItem("CASH", COLLECTIONTYPE.CASH))
            rblPaymentModes.Items.Add(New ListItem("CHEQUE", COLLECTIONTYPE.CHEQUES))
            rblPaymentModes.Items.Add(New ListItem("CREDIT CARD", COLLECTIONTYPE.CREDIT_CARD))
            rblPaymentModes.Items(0).Selected = True
        End If
    End Sub
    Private Sub SET_PAYMENT_MODE_AMOUNT()
        If rblPaymentModes.SelectedValue = COLLECTIONTYPE.CASH Then
            tr_Cash.Visible = True
            txtothCashTotal.Text = ViewState("Amount")
        ElseIf rblPaymentModes.SelectedValue = COLLECTIONTYPE.CHEQUES Then
            tr_Cheque1.Visible = True
            tr_Cheque2.Visible = True
            txtChequeTotal.Text = ViewState("Amount")
        ElseIf rblPaymentModes.SelectedValue = COLLECTIONTYPE.CREDIT_CARD Then
            tr_CreditCard.Visible = True
            txtothCCTotal.Text = ViewState("Amount")
            txtChargeTotal.Text = Format(Convert.ToDouble(txtCrCharge.Text) + Convert.ToDouble(ViewState("Amount")), "#,##0.00")
        End If
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function GetDynamicContentnew() As String
        Try
            Dim b As New StringBuilder()
            Dim itemDiv As HtmlControls.HtmlGenericControl
            itemDiv = HttpContext.Current.Session("divDetail")
            Dim sw As New StringWriter()
            Dim pnl As Panel
            pnl = itemDiv.FindControl("pnlDiscntDtl")
            'BindAssetDetails(pnl, contextKey, False)
            Dim w As New HtmlTextWriter(sw)
            itemDiv.RenderControl(w)
            Dim s As String = sw.GetStringBuilder().ToString()
            b.Append("<table style='background-color:#f3f3f3; ")
            b.Append("width:1000px; ' >")
            b.Append("<tr><td >")
            b.Append(s)
            b.Append("</td></tr>")
            b.Append("</table>")
            Return b.ToString
        Catch ex As Exception

        End Try
        GetDynamicContentnew = ""
    End Function
    Protected Function calulatepageamount() As Double
        Dim Total_page_amount As Double = 0.0
        Dim ExchRate = 1
        ' Dim ExchRate As Decimal = IIf(ForeignCurrency = True, FeeCollection.GetDoubleVal(lblExgRate.Text), 1.0)
        If FeeCollection.GetDoubleVal(txtCrCTotal1.Text) > 0 Then
            Total_page_amount = Total_page_amount + (FeeCollection.GetDoubleVal(txtCrCTotal1.Text) + FeeCollection.GetDoubleVal(txtCrCardP1.Text)) * ExchRate
        End If
        If FeeCollection.GetDoubleVal(txtCrCTotal2.Text) > 0 Then 'credit card2 here
            Total_page_amount = Total_page_amount + (FeeCollection.GetDoubleVal(txtCrCTotal2.Text) + FeeCollection.GetDoubleVal(txtCrCardP2.Text)) * ExchRate
        End If
        If FeeCollection.GetDoubleVal(txtCrCTotal3.Text) > 0 Then 'credit card3 here
            Total_page_amount = Total_page_amount + (FeeCollection.GetDoubleVal(txtCrCTotal3.Text) + FeeCollection.GetDoubleVal(txtCrCardP3.Text)) * ExchRate
        End If
        If FeeCollection.GetDoubleVal(txtCashTotal.Text) > 0 Then 'cash  here
            Total_page_amount = Total_page_amount + FeeCollection.GetDoubleVal(txtCashTotal.Text)
        End If
        If FeeCollection.GetDoubleVal(txtChequeTotal1.Text) > 0 Then 'cheque 1 here
            Total_page_amount = Total_page_amount + FeeCollection.GetDoubleVal(txtChequeTotal1.Text) * ExchRate
        End If
        If FeeCollection.GetDoubleVal(txtChequeTotal2.Text) > 0 Then 'cheque 2 here
            Total_page_amount = Total_page_amount + FeeCollection.GetDoubleVal(txtChequeTotal2.Text) * ExchRate
        End If
        If FeeCollection.GetDoubleVal(txtChequeTotal3.Text) > 0 Then 'cheque 2 here
            Total_page_amount = Total_page_amount + FeeCollection.GetDoubleVal(txtChequeTotal3.Text) * ExchRate
        End If
        Return Total_page_amount
    End Function
    Protected Function GetErrors(ByVal totalamount As Double) As String
        Dim str_error As String = ""
        Dim total_amount_page As Double = calulatepageamount()
        If (total_amount_page <> totalamount) Then
            str_error = str_error & "Mismatches in amount to pay <br />"
        End If
        If Not IsNumeric(txtCCTotal.Text) Then
            str_error = str_error & "Invalid Credit Card amount <br />"
        Else
            If FeeCollection.GetDoubleVal(txtCCTotal.Text) > 0 And txtCreditno.Text = "" Then
                str_error = str_error & "Invalid Authorisation Code <br />"

            End If
        End If
        If Not IsNumeric(txtCashTotal.Text) Then
            str_error = str_error & "Invalid Cash amount <br />"

        End If
        If Not IsNumeric(txtChequeTotal1.Text) Then
            str_error = str_error & "Invalid Cheque amount 1  <br />"

        Else
            If FeeCollection.GetDoubleVal(txtChequeTotal1.Text) > 0 Then
                If Not IsDate(txtChqDate1.Text) Then
                    str_error = str_error & "Invalid Cheque date 1  <br />"

                End If
                If txtChqno1.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 1  <br />"

                End If
                If h_Bank1.Value = "" Or txtBank1.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 1  <br />"

                End If
                If txtBank1.Text = "Bank Transfer" AndAlso hfBankAct1.Value = "" Then
                    str_error = str_error & "Please select a Bank Account for Bank Transfers  <br />"

                End If
                If txtBank1.Text = "Bank Transfer" AndAlso IsDate(txtChqDate1.Text) AndAlso CDate(txtChqDate1.Text) > CDate(DateTime.Now.ToShortDateString) Then
                    str_error = str_error & "Post dated cheques are not allowed for Bank Transfers<br />"

                End If
                'If h_Emirate1.Value = "" Or txtEmirate1.Text = "" Then
                '    str_error = str_error & "Invalid emirate for Cheque # 1  <br />"
                'End If
            End If
        End If
        If Not IsNumeric(txtChequeTotal2.Text) Then
            str_error = str_error & "Invalid Cheque amount 2  <br />"
        Else
            If FeeCollection.GetDoubleVal(txtChequeTotal2.Text) > 0 Then
                If Not IsDate(txtChqDate2.Text) Then
                    str_error = str_error & "Invalid Cheque date 2  <br />"
                End If
                If txtChqno2.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 2  <br />"
                End If
                If h_Bank2.Value = "" Or txtBank2.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 2  <br />"
                End If
                If txtBank2.Text = "Bank Transfer" AndAlso hfBankAct2.Value = "" Then
                    str_error = str_error & "Please select a Bank Account  <br />"
                End If
                If txtBank2.Text = "Bank Transfer" AndAlso IsDate(txtChqDate2.Text) AndAlso CDate(txtChqDate2.Text) > CDate(DateTime.Now.ToShortDateString) Then
                    str_error = str_error & "Post dated cheques are not allowed for Bank Transfers<br />"
                End If
                'If h_Emirate2.Value = "" Or txtEmirate2.Text = "" Then
                '    str_error = str_error & "Invalid emirate for Cheque # 2  <br />"
                'End If
            End If
        End If
        If Not IsNumeric(txtChequeTotal3.Text) Then
            str_error = str_error & "Invalid Cheque amount 3  <br />"
        Else
            If FeeCollection.GetDoubleVal(txtChequeTotal3.Text) > 0 Then
                If Not IsDate(txtChqDate3.Text) Then
                    str_error = str_error & "Invalid Cheque date 3  <br />"
                End If
                If txtChqno3.Text = "" Then
                    str_error = str_error & "Invalid Cheque no # 3  <br />"
                End If
                If h_Bank3.Value = "" Or txtBank3.Text = "" Then
                    str_error = str_error & "Invalid Bank for Cheque # 3  <br />"
                End If
                If txtBank3.Text = "Bank Transfer" AndAlso hfBankAct3.Value = "" Then
                    str_error = str_error & "Please select a Bank Account  <br />"
                End If
                If txtBank3.Text = "Bank Transfer" AndAlso IsDate(txtChqDate3.Text) AndAlso CDate(txtChqDate3.Text) > CDate(DateTime.Now.ToShortDateString) Then
                    str_error = str_error & "Post dated cheques are not allowed for Bank Transfers<br />"
                End If
                'If h_Emirate3.Value = "" Or txtEmirate3.Text = "" Then
                '    str_error = str_error & "Invalid emirate for Cheque # 3  <br />"
                'End If
            End If
        End If
        Return str_error
    End Function

    Private Function GetTAXAmount(ByVal FEE_ID As Int32, ByVal ACD_ID As Int32, ByVal STU_ID As Int32, ByVal AMOUNT As Double) As Double
        GetTAXAmount = 0
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '​
        objConn.Open()
        Dim dsSummary As New DataSet
        Dim sqlParam(9) As SqlParameter
        Dim retval As String = ""
        Dim TAX As Double = 0
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim ddate As Date = Today.Date
            sqlParam(0) = New SqlParameter("@ReturnValue", SqlDbType.VarChar)
            sqlParam(0).Direction = ParameterDirection.ReturnValue

            sqlParam(1) = New SqlParameter("@STU_ID", SqlDbType.Int)
            sqlParam(1).Value = STU_ID
            sqlParam(2) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlParam(2).Value = ViewState("BSU_ID")
            sqlParam(3) = New SqlParameter("@ACD_ID", SqlDbType.Int)
            sqlParam(3).Value = ACD_ID
            sqlParam(4) = New SqlParameter("@FEE_ID", SqlDbType.Int)
            sqlParam(4).Value = FEE_ID
            sqlParam(5) = New SqlParameter("@DATE", SqlDbType.DateTime)
            sqlParam(5).Value = ddate
            sqlParam(6) = New SqlParameter("@AMOUNT", SqlDbType.Decimal)
            sqlParam(6).Value = AMOUNT
            sqlParam(7) = New SqlParameter("@TAX", SqlDbType.Float)
            sqlParam(7).Direction = ParameterDirection.Output
            sqlParam(8) = New SqlParameter("@STU_TYPE", SqlDbType.VarChar)
            sqlParam(8).Value = ViewState("S_TYPE") 'IIf(rbEnrollment.Checked, "S", "E")​

            Mainclass.ExecuteParamQRY(objConn, stTrans, "FEES.GET_TAX_FEECOLLECTION", sqlParam)
            retval = sqlParam(0).Value
            GetTAXAmount = sqlParam(7).Value

        Catch ex As Exception
            GetTAXAmount = 0
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Function

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Dim ds As DataSet
        ds = loaddloaddetails()
        Dim total = FeeCollection.GetDoubleVal(ViewState("Amount"))
        Dim str_error As String
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString) '
        objConn.Open()
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim ACD_ID As Integer
        Dim STU_NAME As String
        Dim STU_NO As String
        Dim EVENT_NAME As String = "", FCL_NARRATION As String = ""
        Dim REG_MODE As String = ""
        Dim FEE_Type_Id As String = ""
        Dim oth_account_code As String = ""
        Dim oth_event_code As String = ""
        Dim oth_tax_code As String = ""
        Dim retval As String = "1000"
        Dim retvaloth As Integer = 1000
        Dim ExchRate = 1
        ' Dim ExchRate As Decimal = IIf(ForeignCurrency = True, FeeCollection.GetDoubleVal(lblExgRate.Text), 1.0)
        ' Dim Currency As String = IIf(ForeignCurrency = True, ddCurrency.SelectedItem.Text, Session("BSU_CURRENCY"))
        Dim Currency As String = Session("BSU_CURRENCY")
        Dim STR_TYPE As String = "S"
        Dim ddate As DateTime = Date.Now.ToString("yyyy-MM-dd")
        Dim APD_ID As String = ""
        Try

            If (ds.Tables.Count > 0) Then
                If ds.Tables(0).Rows.Count > 0 Then

                    ACD_ID = ds.Tables(0).Rows(0)("ALD_ACDID")
                    STU_NAME = ds.Tables(0).Rows(0)("STU_NAME")
                    EVENT_NAME = ds.Tables(0).Rows(0)("ALD_EVENT_NAME")
                    STU_NO = ds.Tables(0).Rows(0)("STU_NO")
                    REG_MODE = ds.Tables(0).Rows(0)("ALD_FEE_REG_MODE")
                    If (REG_MODE = "FC") Then
                        FEE_Type_Id = ds.Tables(0).Rows(0)("ALD_FEETYPE_ID")
                    ElseIf (REG_MODE = "OC") Then
                        oth_account_code = ds.Tables(0).Rows(0)("ALD_ACT_ID")
                        oth_event_code = ds.Tables(0).Rows(0)("ALD_OTH_EVT_ID")
                        oth_tax_code = ds.Tables(0).Rows(0)("ALD_OTH_TAX_CODE")
                        FEE_Type_Id = ds.Tables(0).Rows(0)("ALD_FEETYPE_ID") 'Added by vikranth on 21st Jan 2020
                    End If

                    APD_ID = ds.Tables(0).Rows(0)("APD_ID")
                    If (REG_MODE <> "") And (REG_MODE = "FC") Then

                        str_error = GetErrors(total)
                        If (str_error <> "") Then
                            ' lblError.Text = str_error
                            ShowMessage(str_error, True)
                            Exit Sub
                        End If

                        Dim dblCash As Decimal = FeeCollection.GetDoubleVal(txtCashTotal.Text)
                        Dim dblTAX_FEE As Double = 0.0
                        'If IsNumeric(lblTAX.Text) AndAlso FeeCollection.GetDoubleVal(lblTAX.Text) <> 0 Then
                        '    dblTAX_FEE = FeeCollection.GetDoubleVal(lblTAX.Text)
                        'End If
                        Dim FeeAutoChg As Boolean = False
                        Dim CurrencyAmount As Decimal = FeeCollection.GetDoubleVal(ViewState("Amount"))
                        'If ForeignCurrency Then
                        '    Dim txtAmountToPayFC As TextBox = CType(gvr.FindControl("txtAmountToPayFC"), TextBox)
                        '    CurrencyAmount = FeeCollection.GetDoubleVal(txtAmountToPayFC.Text)
                        'Else
                        '    CurrencyAmount = FeeCollection.GetDoubleVal(txtAmountToPay.Text)
                        'End If
                        '----1st 
                        FCL_NARRATION = Left("Activty fee collection - " & EVENT_NAME, 500)
                        retval = FeeCollection.F_SaveFEECOLLECTION_H_Normal(0, "Counter", ddate,
                               "", ACD_ID, ViewState("STU_ID"), total, Session("sUsr_name"),
                               False, str_new_FCL_ID, ViewState("BSU_ID"), FCL_NARRATION, "CR", str_NEW_FCL_RECNO, 0,
                                ViewState("BSU_ID"), STR_TYPE, 0, Currency, ExchRate, stTrans, 0, Session("sBsuId"))

                        ' student_id, acd_id, total, fee_type, fee_type_id 
                        '----2nd
                        If retval = "0" Then
                            '  Dim TaxAmount As Double = GetTAXAmount(FEE_Type_Id, ACD_ID, ViewState("STU_ID"), total)
                            retval = FeeCollection.F_SaveFEECOLLSUB_Normal(0, str_new_FCL_ID, FEE_Type_Id,
                                          total, -1, total, 0, CurrencyAmount, stTrans, FeeAutoChg, dblTAX_FEE, "ACT_PAY" & APD_ID, "ACT_PAY")
                        End If


                        '----3rd
                        If retval = "0" Then
                            Dim BaseCurrTotal As Decimal
                            If FeeCollection.GetDoubleVal(txtCrCTotal1.Text) > 0 And retval = "0" Then 'credit card1 here
                                BaseCurrTotal = (FeeCollection.GetDoubleVal(txtCrCTotal1.Text) + FeeCollection.GetDoubleVal(txtCrCardP1.Text)) * ExchRate

                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD,
                                BaseCurrTotal, FeeCollection.GetDoubleVal(txtCrCTotal1.Text) + FeeCollection.GetDoubleVal(txtCrCardP1.Text), txtCreditno.Text, ddate, 0, "", ddCreditcard.SelectedItem.Value,
                                "", stTrans, "", FeeCollection.GetDoubleVal(Me.txtCrCardP1.Text))
                            End If
                            If FeeCollection.GetDoubleVal(txtCrCTotal2.Text) > 0 And retval = "0" Then 'credit card2 here
                                BaseCurrTotal = (FeeCollection.GetDoubleVal(txtCrCTotal2.Text) + FeeCollection.GetDoubleVal(txtCrCardP2.Text)) * ExchRate

                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD,
                                BaseCurrTotal, FeeCollection.GetDoubleVal(txtCrCTotal2.Text) + FeeCollection.GetDoubleVal(txtCrCardP2.Text), txtCreditno2.Text, ddate, 0, "", ddCreditcard2.SelectedItem.Value,
                                "", stTrans, "", FeeCollection.GetDoubleVal(Me.txtCrCardP2.Text))
                            End If
                            If FeeCollection.GetDoubleVal(txtCrCTotal3.Text) > 0 And retval = "0" Then 'credit card3 here
                                BaseCurrTotal = (FeeCollection.GetDoubleVal(txtCrCTotal3.Text) + FeeCollection.GetDoubleVal(txtCrCardP3.Text)) * ExchRate

                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD,
                                BaseCurrTotal, FeeCollection.GetDoubleVal(txtCrCTotal3.Text) + FeeCollection.GetDoubleVal(txtCrCardP3.Text), txtCreditno3.Text, ddate, 0, "", ddCreditcard3.SelectedItem.Value,
                                "", stTrans, "", FeeCollection.GetDoubleVal(Me.txtCrCardP3.Text))
                            End If

                            If FeeCollection.GetDoubleVal(txtCashTotal.Text) > 0 And retval = "0" Then 'cash  here
                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID, COLLECTIONTYPE.CASH,
                               dblCash, dblCash, "", ddate, 0, "", "", "", stTrans)
                            End If


                            If FeeCollection.GetDoubleVal(txtChequeTotal1.Text) > 0 And retval = "0" Then 'cheque 1 here
                                BaseCurrTotal = FeeCollection.GetDoubleVal(txtChequeTotal1.Text) * ExchRate

                                If h_Bank1.Value.ToUpper <> "65" OrElse txtBank1.Text.ToUpper <> "BANK TRANSFER" Then
                                    hfBankAct1.Value = ""
                                End If

                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID,
                                COLLECTIONTYPE.CHEQUES, BaseCurrTotal, FeeCollection.GetDoubleVal(txtChequeTotal1.Text), txtChqno1.Text,
                                txtChqDate1.Text, 0, "", h_Bank1.Value, ddlEmirate1.SelectedItem.Value,
                                stTrans, h_Chequeid.Value, 0, hfBankAct1.Value)
                            End If

                            If FeeCollection.GetDoubleVal(txtChequeTotal2.Text) > 0 And retval = "0" Then 'cheque 2 here
                                BaseCurrTotal = FeeCollection.GetDoubleVal(txtChequeTotal2.Text) * ExchRate

                                If h_Bank2.Value.ToUpper <> "65" OrElse txtBank2.Text.ToUpper <> "BANK TRANSFER" Then
                                    hfBankAct2.Value = ""
                                End If

                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID,
                                COLLECTIONTYPE.CHEQUES, BaseCurrTotal, FeeCollection.GetDoubleVal(txtChequeTotal2.Text), txtChqno2.Text,
                                txtChqDate2.Text, 0, "", h_Bank2.Value, ddlEmirate2.SelectedItem.Value,
                                stTrans, "", 0, hfBankAct2.Value)
                            End If
                            If FeeCollection.GetDoubleVal(txtChequeTotal3.Text) > 0 And retval = "0" Then 'cheque 3 here
                                BaseCurrTotal = FeeCollection.GetDoubleVal(txtChequeTotal3.Text) * ExchRate

                                If h_Bank3.Value.ToUpper <> "65" OrElse txtBank3.Text.ToUpper <> "BANK TRANSFER" Then
                                    hfBankAct3.Value = ""
                                End If

                                retval = FeeCollection.F_SaveFEECOLLSUB_D_Normal(0, str_new_FCL_ID,
                                COLLECTIONTYPE.CHEQUES, BaseCurrTotal, FeeCollection.GetDoubleVal(txtChequeTotal3.Text), txtChqno3.Text,
                                txtChqDate3.Text, 0, "", h_Bank3.Value, ddlEmirate3.SelectedItem.Value,
                                stTrans, "", 0, hfBankAct3.Value)
                            End If


                            '-----4th 

                            If retval = "0" Then
                                retval = FeeCollection.F_SaveFEECOLLALLOCATION_D_Normal(ViewState("BSU_ID"),
                                str_new_FCL_ID, stTrans)
                            End If

                            '-----5th
                            '--- UPDATING ACTIVITY DETAILS INTO PARENT PORTAL TABLE.
                        End If
                        If retval = "0" Then
                            UpdateActivityTableAfterPayment_Success(stTrans, str_new_FCL_ID, "FC")
                            stTrans.Commit()
                            btnSave.Visible = False
                            ' btnCancel.Visible = False
                            lnkReceipt.Visible = True
                            '  lblError.Text = "Payment Successful"
                            ShowMessage("Payment Successful", False)
                            Dim PopForm As String = ""
                            If hfTaxable.Value = "1" Then
                                PopForm = "../fees/FeeReceipt_TAX.aspx"
                            Else
                                PopForm = "../fees/FeeReceipt.aspx"
                            End If
                            lnkReceipt.Attributes.Add("onClick", "return ShowWindowWithClose('" & PopForm & "?type=" & "REC" & "&id=" & Encr_decrData.Encrypt(str_NEW_FCL_RECNO.Trim) & "&bsu_id=" & Encr_decrData.Encrypt(ViewState("BSU_ID")) & "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & "&isenq=" & Encr_decrData.Encrypt(False) & "&iscolln=" & Encr_decrData.Encrypt(True) & "&isexport=1""', '', '75%', '75%');")
                        Else
                            ShowMessage(getErrorMessage(retval), False)
                            stTrans.Rollback()
                        End If

                        '----6th

                    ElseIf (REG_MODE <> "") And (REG_MODE = "OC") Then

                        If rblPaymentModes.SelectedValue = COLLECTIONTYPE.CREDIT_CARD Then
                            If (txtothCreditno.Text = "") Then
                                ' lblError.Text = "Invalid auth code!"
                                ShowMessage("Invalid auth code!", True)
                                Exit Sub
                            End If
                        ElseIf rblPaymentModes.SelectedValue = COLLECTIONTYPE.CHEQUES Then
                            If txtChqno.Text = "" Then
                                '   lblError.Text = "Invalid cheque number!"
                                ShowMessage("Invalid cheque number!", True)
                                Exit Sub
                            ElseIf ddlEmirate.SelectedValue = "-" Then
                                'lblError.Text = "Invalid emirate selected!"
                                ShowMessage("Invalid emirate selected!", True)
                                Exit Sub
                            End If
                        End If

                        retvaloth = FeeCommon.CheckFeeclose(ViewState("BSU_ID"), ddate, stTrans)
                        Dim dblActualAmount As Decimal
                        Dim FOC_EVT_ID As String = "", FOC_CFA_ID As String = "", FOC_REF_VDR_CODE As String = "", FOC_CUSTOMER_ACT_ID As String = ""
                        Dim FOC_CUSTOMER_ADDRESS As String = "", FOC_CUSTOMER_TAX_REG_NO As String = "", FOC_TAX_CODE As String = "", STAFF_EMP_ID As Integer = 0
                        FOC_EVT_ID = oth_event_code
                        FOC_TAX_CODE = oth_tax_code
                        FOC_CUSTOMER_ACT_ID = "242C0005"
                        FOC_CFA_ID = "OTHRPT"
                        If retvaloth = 0 Then
                            STAFF_EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                            Dim STU_ID As Long = 0, FCD_AMount As Double = 0, FOD_AMOUNT As Double = 0, NARRATION As String = ""
                            If FeeCollection.GetDoubleVal(ViewState("STU_ID")) = 0 OrElse FeeCollection.GetDoubleVal(ViewState("Amount")) = 0 Then
                                retvaloth = "112"
                                Exit Sub
                            Else

                                Dim str_NEW_FOC_RECNO As String = ""
                                Dim str_new_FOC_ID As Int64 = 0
                                dblActualAmount = CDbl(ViewState("Amount"))
                                STU_ID = ViewState("STU_ID")
                                NARRATION = Left("Activty Collection - Activity" & EVENT_NAME & " [" & STU_NO & " - " & STU_NAME & "]", 500)


                                Dim taxamount As Double = CALCULATE_TAX(dblActualAmount, oth_tax_code)

                                Dim AmountToPay As Double = dblActualAmount - taxamount

                                retvaloth = FeeCollectionOther.F_SaveFEEOTHCOLLECTION_H(0, "", ddate,
                                ViewState("BSU_ID"), AmountToPay, Session("sUsr_name"), rblPaymentModes.SelectedValue,
                                False, NARRATION, "CR", False, 8, oth_account_code,
                                str_NEW_FOC_RECNO, str_new_FOC_ID, stTrans, objConn, Currency, 1, FOC_EVT_ID, FOC_CFA_ID,
                                FOC_REF_VDR_CODE, FOC_CUSTOMER_ACT_ID, "", FOC_CUSTOMER_ADDRESS, FOC_TAX_CODE, STU_ID, STAFF_EMP_ID, 0, "", "", 1, "", FEE_Type_Id, 0, "ACT_PAY", Session("sBsuId")) 'Replace 0 with FEE_Type_Id by vikranth on 21st Jan 2020 as told by Shakeel
                                FCD_AMount = dblActualAmount
                                FOD_AMOUNT = dblActualAmount
                                If retvaloth = 0 Then
                                    Dim REF_NO As String = ""
                                    If rblPaymentModes.SelectedValue = COLLECTIONTYPE.CREDIT_CARD Then
                                        REF_NO = txtothCreditno.Text
                                    ElseIf rblPaymentModes.SelectedValue = COLLECTIONTYPE.CHEQUES Then
                                        REF_NO = txtChqno.Text
                                    End If
                                    retvaloth = FeeCollectionOther.F_SaveFEEOTHCOLLSUB_D(0, str_new_FOC_ID, ddCreditcard.SelectedValue, ddlEmirate.SelectedValue, FOD_AMOUNT, REF_NO,
                                                                                  txtChqDate.Text, 0, "", False, stTrans, objConn, FCD_AMount, 0,
                                                                                  h_Bank.Value, oth_tax_code, taxamount, total)
                                End If


                                If retvaloth = 0 Then
                                    UpdateActivityTableAfterPayment_Success(stTrans, str_new_FOC_ID, "OC")
                                    stTrans.Commit()
                                    btnSave.Visible = False
                                    ' btnCancel.Visible = False
                                    lnkReceipt.Visible = True
                                    ' lblError.Text = "Payment Successfull"
                                    ShowMessage("Payment Successful", False)
                                    h_print.Value = "print"
                                    PrintReceipt(str_NEW_FOC_RECNO)
                                    Session("str_NEW_FOC_RECNO") = str_NEW_FOC_RECNO
                                    '    Dim PopForm As String = "../StudentServices/FeeReceipt.aspx"
                                    '    lnkReceipt.Attributes.Add("onClick", "return ShowWindowWithClose('" & PopForm & "?type=" & "OC" & "&id=" & Encr_decrData.Encrypt(str_NEW_FOC_RECNO.Trim) & "', '', '75%', '75%');")
                                Else
                                    ShowMessage(getErrorMessage(retvaloth), False)
                                    stTrans.Rollback()
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ' ShowMessage(ex.Message, False)
            stTrans.Rollback()
        End Try
    End Sub

    Protected Sub lnkReceipt_Click(sender As Object, e As EventArgs)
        h_print.Value = "print"
        PrintReceipt(Session("str_NEW_FOC_RECNO").Trim)
    End Sub
    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        'Dim FC As Boolean = False
        Dim strFilter As String
        strFilter = "  FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & ViewState("BSU_ID") & "' "
        Dim FCurr As Boolean = IIf(SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, "select CASE WHEN  ltrim(rtrim(foc_cur_id))=ltrim(rtrim(BASECURRENCY)) THEN 1 ELSE 0 END FROM FEES.VW_OSO_FEES_RECEIPT_OTHER WHERE " + strFilter) = 1, False, True)

        'If ddCurrency.SelectedItem.Text <> Session("BSU_CURRENCY") And pnlCurrency.Visible = True Then
        '    FC = True
        'End If
        If Session("BSU_IsOnDAX") = 1 Then
            Session("ReportSource") = FeeCollectionOther.PrintReceipt_DAX(p_Receiptno, Session("sUsr_name"), ViewState("BSU_ID"),
      ConnectionManger.GetOASIS_FEESConnectionString, FCurr)
        Else
            Session("ReportSource") = FeeCollectionOther.PrintReceipt(p_Receiptno, Session("sUsr_name"), ViewState("BSU_ID"),
       ConnectionManger.GetOASIS_FEESConnectionString)
        End If

    End Sub
    Private Function CALCULATE_TAX(ByVal Amount As Double, ByVal taxcode As String) As Double

        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount_Ext(" & Amount & ",'" & taxcode & "'," & 1 & ")")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim netamount As Double = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
                CALCULATE_TAX = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
            Else
                CALCULATE_TAX = 0.0
            End If
        Else
            CALCULATE_TAX = 0.0
        End If

    End Function
    Protected Sub UpdateActivityTableAfterPayment_Success(ByRef strans2 As SqlTransaction, ByVal REF_ID As Integer, ByVal REG_MODE As String)
        Dim ReturnVal As String = Nothing
        Dim act_param(3) As SqlClient.SqlParameter
        Try
            act_param(0) = New SqlClient.SqlParameter("@REF_ID", REF_ID)
            act_param(1) = New SqlClient.SqlParameter("@STUD_ID", Convert.ToInt32(ViewState("STU_ID").ToString))
            act_param(2) = New SqlClient.SqlParameter("@ACT_APD_ID", Convert.ToInt32(ViewState("APD_ID").ToString))
            act_param(3) = New SqlClient.SqlParameter("@MODE", "COUNTER-" + REG_MODE)
            SqlHelper.ExecuteNonQuery(strans2, "OASIS.OASIS.UPDATE_ACTIVITY_AFTER_PAYMENT", act_param)
        Catch ex As Exception
            strans2.Rollback()
            UtilityObj.Errorlog("From UpdateActivityTableAfterPayment_Success: " + ex.Message, "PHOENIX")
        End Try
    End Sub

    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        lblError.CssClass = ""
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "alert alert-danger"
            Else
                lblError.CssClass = "alert alert-success"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

End Class
