﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class StudentServices_Services_Gen_De_Allocate_Approval_F
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("ssd") IsNot Nothing And Request.QueryString("ssd") <> "" Then
                Dim id As Integer = Convert.ToInt32(Request.QueryString("ssd").ToString())
                ViewState("SSD_ID") = id
                BindRequestDetails(id)
                btnDeAllocate.Enabled = True
                hf_bSuccess.Value = 0
            End If
        End If
    End Sub

    Protected Sub btnDeAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeAllocate.Click
        Dim errormsg As String = String.Empty
        If txtDate.Value = "" OrElse Not IsDate(txtDate.Value) Then
            SHOW_MESSAGE("Please input the date of discontinuation")
            Exit Sub
        End If
        If txtRemarks.Value.Trim = "" Then
            SHOW_MESSAGE("Please input the narration")
            Exit Sub
        End If
        If callTransaction(errormsg) <> 0 Then
            SHOW_MESSAGE(errormsg)
            btnDeAllocate.Enabled = True
        Else
            SHOW_MESSAGE("Request has been approved successfully with discontinuation date as " & txtDate.Value, False)
            btnDeAllocate.Enabled = False
        End If
    End Sub

    Sub BindRequestDetails(ByVal Id As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim selectedbsu As String = ""
            Dim param(2) As SqlClient.SqlParameter
            Dim Dt As New DataTable
            Dim ds As DataSet

            param(0) = New SqlClient.SqlParameter("@SSD_ID", Id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.GET_General_SVC_REQUEST_Details ", param)

            If ds.Tables.Count > 0 Then
                Dt = ds.Tables(0)
                If Dt.Rows.Count > 0 Then
                    Dim DiscDt As DateTime = Date.Now
                    If IsDate(Dt.Rows(0)("SSD_SERVICE_DIS_DT").ToString()) Then
                        DiscDt = DirectCast(Dt.Rows(0)("SSD_SERVICE_DIS_DT"), DateTime)
                    End If
                    lblName.Text = Dt.Rows(0)("SNAME").ToString()
                    lblActivity.Text = Dt.Rows(0)("SVC_DESCRIPTION").ToString()
                    txtDate.Value = Format(DiscDt, OASISConstants.DataBaseDateFormat)
                    'lblAllocationDt.Text =
                    txtRemarks.Value = Dt.Rows(0)("SSD_REMARKS").ToString()
                End If
            End If
        Catch ex As Exception
            'lblError.InnerText = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer
        Dim tran As SqlTransaction
        Dim SSD_ID As String = ViewState("SSD_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try
                Dim param(5) As SqlParameter
                Dim ReturnFlag As Integer

                param(0) = New SqlParameter("@SSD_ID", SSD_ID)
                param(1) = New SqlParameter("@UserId", Session("sUsr_id").ToString)

                param(3) = New SqlParameter("@EmpId", Convert.ToInt32(Session("EmployeeId").ToString))
                param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(2).Direction = ParameterDirection.ReturnValue
                param(4) = New SqlParameter("@DIS_DT", Me.txtDate.Value)
                param(5) = New SqlParameter("@Remarks", Me.txtRemarks.Value)
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "ST.APPROVE_DISCONTINUE_SINGLE_REQUEST", param)
                ReturnFlag = param(2).Value

                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SVB_ID") = "0"
                    ViewState("datamode") = "none"
                    hf_bSuccess.Value = 1
                    callTransaction = "0"
                    'resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = "Record saved successfully"
                    tran.Commit()
                End If
            End Try
        End Using
    End Function

    Sub SHOW_MESSAGE(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If bError Then
            lblError.CssClass = "diverrorPopUp"
        Else
            lblError.CssClass = "divvalidPopUp"
        End If
        lblError.Text = Message
    End Sub
End Class
