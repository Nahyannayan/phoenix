﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Imports Telerik.Web.UI

Partial Class StudentServices_Services_EnrollActivity_ForAdjst
    Inherits System.Web.UI.Page
    Dim Message As String = Nothing
    Dim Count As Integer = 0
    Dim smScriptManager As New ScriptManager
    Dim Encr_decrData As New Encryption64
    Dim Latest_refid As Integer = 0
    Public Property DataResult() As DataTable
        Get
            Return Session("DataActivityResult")
        End Get
        Set(ByVal value As DataTable)
            Session("DataActivityResult") = value
        End Set
    End Property

    Private Property DiscountBreakUpXML() As String
        Get
            Return ViewState("DiscountBreakUpXML")
        End Get
        Set(ByVal value As String)
            ViewState("DiscountBreakUpXML") = value
        End Set
    End Property
    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value

        End Set
    End Property
    Private Property Max_Limit_ForRptr() As Integer
        Get
            Return ViewState("Max_Limit_ForRptr")
        End Get
        Set(ByVal value As Integer)
            ViewState("Max_Limit_ForRptr") = value

        End Set
    End Property
    Private Property B_DayNotallow() As Integer
        Get
            Return ViewState("B_DayNotallow")
        End Get
        Set(ByVal value As Integer)
            ViewState("B_DayNotallow") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        smScriptManager = Master.FindControl("ScriptManager1")
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    ' Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If

                    'MAIN LOADING FUNCTIONS
                    'fillTranAcdY()
                    'END
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
        'If (ViewState("Paid")) Then
        '    uscStudentPicker_StudentNoChanged(Nothing, Nothing)
        'End If

    End Sub
    Protected Sub uscStudentPicker_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uscStudentPicker.StudentNoChanged
        Try
            Dim Student_id As Long = uscStudentPicker.STU_ID           
            bPaymentConfirmMsgShown = False
            LoadRepeaterGroup(Student_id)
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Load: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub LoadRepeaterGroup(ByVal Student_id As Long)
        DataResult = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As System.Data.DataSet
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@TYPE", "G")
        param(1) = New SqlClient.SqlParameter("@STUID", Student_id)
        param(2) = New SqlClient.SqlParameter("@GROUPID", 0)

        ds = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", param)
        If ds.Tables(0).Rows.Count = 0 Then
            'lblError.Text = "Sorry! No Activities To Display"
            Message = UtilityObj.getErrorMessage("4007")
            ShowMessage(Message, True)
        Else
            Dim dt As DataTable = ds.Tables(0)
            ' Session("datatable_Activity") = dt
            rptrGroup.DataSource = ds.Tables(0)
            rptrGroup.DataBind()
        End If
    End Sub
    Protected Sub rptrGroup_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.DataItem Is Nothing Then
                Return
            Else
                Max_Limit_ForRptr = 0
                Dim hidAPG_ID As HiddenField = DirectCast(e.Item.FindControl("hidAPG_ID"), HiddenField)
                Dim Repeater1 As Repeater = DirectCast(e.Item.FindControl("Repeater1"), Repeater)
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim lbllimitFulltxt As Label = DirectCast(e.Item.FindControl("lbllimitFulltxt"), Label)
                Dim hidMaxlimit As HiddenField = DirectCast(e.Item.FindControl("hidMaxlimit"), HiddenField)
                Dim lblMaximumLimit As Label = DirectCast(e.Item.FindControl("lblMaximumLimit"), Label)
                Dim divRepeater As HtmlGenericControl = DirectCast(e.Item.FindControl("divRepeater"), HtmlGenericControl)



                If Not hidMaxlimit Is Nothing And hidMaxlimit.Value > 0 Then
                    lblMaximumLimit.Visible = True
                    lblMaximumLimit.Text = "( Maximum allowed :  " + hidMaxlimit.Value + " )"
                    'If hidMaxlimit.Value = 1 Then
                    'Else
                    '    lblMaximumLimit.Visible = True
                    '    lblMaximumLimit.Text = "This is a group activity. Maximum number of activity can be chosen per this group is:  " + hidMaxlimit.Value
                    'End If                   
                End If

                If hidAPG_ID.Value = 0 Then
                    divRepeater.Style.Add("Background-color", "white")
                    divRepeater.Style.Add("border", "none")
                End If
                'get max count of group 
                Dim Params(2) As SqlClient.SqlParameter
                Params(0) = New SqlClient.SqlParameter("@TYPE", "M")
                Params(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
                Params(2) = New SqlClient.SqlParameter("@GROUPID", hidAPG_ID.Value)
                Dim max_limit As Integer = SqlHelper.ExecuteScalar(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", Params)
                Max_Limit_ForRptr = max_limit
                If max_limit = 0 Then
                    lbllimitFulltxt.Text = "Activity Selection Limit Reached"
                Else
                    lbllimitFulltxt.Text = "&nbsp;"
                End If
                Dim ds_A As System.Data.DataSet
                Dim param(2) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@TYPE", "A")
                param(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
                param(2) = New SqlClient.SqlParameter("@GROUPID", hidAPG_ID.Value)
                ds_A = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", param)

                Dim Dt_Result As DataTable = ds_A.Tables(0)
                '   Dim d_Col As DataColumn = New DataColumn              
                If DataResult Is Nothing Then
                    DataResult = Dt_Result
                Else
                    DataResult.Merge(Dt_Result)
                End If

                Repeater1.DataSource = ds_A
                Repeater1.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Sub Repeater1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.DataItem Is Nothing Then
                Return
            Else

                Dim count As Integer = 0
                Dim ViewID As Integer = Convert.ToInt32(DirectCast(e.Item.FindControl("lblViewId"), Label).Text.ToString())
                'Get specific row from datatable
                Dim Dt_table As DataTable = DataResult
                Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)


                'controls loading
                Dim lblamt As Label = DirectCast(e.Item.FindControl("lblamt"), Label)
                Dim lnkbtnAdjst As LinkButton = DirectCast(e.Item.FindControl("lnkbtnAdjst"), LinkButton)                
                Dim divcount As HtmlGenericControl = DirectCast(e.Item.FindControl("divcount"), HtmlGenericControl)
                Dim ddlcount As DropDownList = DirectCast(e.Item.FindControl("ddlcount"), DropDownList)
                Dim labelStatus As Label = DirectCast(e.Item.FindControl("lblStatus"), Label)



                If dRow(0)("ALD_ALLOW_MULTIPLE") Then
                    divcount.Visible = True
                    ddlcount.Items.Clear()
                    Dim mulmaxcount As Integer = 0
                    If (Not dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT") Is Nothing) And (Not dRow(0)("ALD_AVAILABLE_SEAT") Is Nothing) Then
                        If (Convert.ToInt32(dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT")) > Convert.ToInt32(dRow(0)("ALD_AVAILABLE_SEAT"))) Then
                            mulmaxcount = Convert.ToInt32(dRow(0)("ALD_AVAILABLE_SEAT"))
                        Else
                            mulmaxcount = Convert.ToInt32(dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT"))
                        End If
                    End If
                    If mulmaxcount > 0 Then
                        For i As Integer = 1 To mulmaxcount
                            ddlcount.Items.Add(New ListItem(i.ToString(), i.ToString()))
                        Next i
                    Else
                        ddlcount.Items.Add(New ListItem(0, 0))
                    End If

                Else
                    divcount.Visible = False
                End If
                'lnkbtnAdjst.Attributes.Add("OnClick", "return ShowWindowWithClose('/Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%');")
                count = CheckEnrolledStatus(ViewID)
                'SHOW IF SCHEDULES ARE THERE 
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim divSchdl As HtmlGenericControl = DirectCast(e.Item.FindControl("divSchdl"), HtmlGenericControl)
                Dim rdoSchdlDetails As RadioButtonList = DirectCast(e.Item.FindControl("rdoSchdlDetails"), RadioButtonList)
                Dim ribbonNotPaid As HtmlGenericControl = DirectCast(e.Item.FindControl("ribbonNotPaid"), HtmlGenericControl)
                If divSchdl.Visible = True Then
                    Dim paramS(2) As SqlClient.SqlParameter
                    paramS(0) = New SqlClient.SqlParameter("@TYPE", "S")
                    paramS(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
                    paramS(2) = New SqlClient.SqlParameter("@GROUPID", ViewID)
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", paramS)
                    rdoSchdlDetails.DataSource = ds.Tables(0)
                    rdoSchdlDetails.DataValueField = "ASH_ID"
                    rdoSchdlDetails.DataTextField = "ASH_WEEKS"
                    rdoSchdlDetails.DataBind()
                    rdoSchdlDetails.SelectedIndex = 0

                    For Each item As ListItem In rdoSchdlDetails.Items
                        If item.Text.Contains("Available Seat: 0") Then
                            item.Enabled = False
                        End If
                    Next

                End If


                If (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Pending") Then
                    If divSchdl.Visible = True Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Selected = True
                            rdoSchdlDetails.Enabled = False
                        End If
                    End If

                    If (dRow(0)("ALD_ALLOW_MULTIPLE")) Then
                        lblamt.Text = dRow(0)("APD_TOTAL_AMOUNT") & " " & Session("BSU_CURRENCY")
                        ddlcount.SelectedValue = dRow(0)("APD_COUNT")
                        ddlcount.Enabled = False
                    End If

                    If (dRow(0)("ALD_AVAILABLE_SEAT") <= 0) Then
                        lnkbtnAdjst.Visible = False
                        labelStatus.Text = "Full Booked"
                        ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    Else
                       

                        labelStatus.Text = "Pending"
                        ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                       
                    End If
                ElseIf (dRow(0)("ALD_AUTO_ENROLL") And count = 0) Then

                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Completed") Then
                    lnkbtnAdjst.Visible = False
                    If divSchdl.Visible = True Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Selected = True
                            rdoSchdlDetails.Enabled = False
                        End If
                    End If

                    If (dRow(0)("ALD_ALLOW_MULTIPLE")) Then
                        lblamt.Text = dRow(0)("APD_TOTAL_AMOUNT") & " " & Session("BSU_CURRENCY")
                        ddlcount.SelectedValue = dRow(0)("APD_COUNT")
                        ddlcount.Enabled = False
                    End If

                    labelStatus.Text = "Enrolled"
                    ribbonNotPaid.Attributes.Add("class", "ribbonActive")
                    '  lblDone.Style.Item("display") = "block"            

                ElseIf dRow(0)("ALD_AVAILABLE_SEAT") <= 0 Then

                    lnkbtnAdjst.Visible = False
                    labelStatus.Text = "Full Booked"
                    ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                Else
                    lnkbtnAdjst.Visible = False

                End If
            End If
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Repeater1_ItemDataBound: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub


    Protected Function CheckEnrolledStatus(ByVal ViewId As Integer) As Integer
        Dim rowcount As Integer = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Param(0) = New SqlClient.SqlParameter("@VIEWID", ViewId)
        Param(1) = New SqlClient.SqlParameter("@STUID", uscStudentPicker.STU_ID)
        rowcount = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[GET_ACT_REQ_DETAILS_PARENTTABLE]", Param)
        Return rowcount
    End Function


    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                'lblError.CssClass = "alert alert-warning"
            Else
                ' lblError.CssClass = "alert alert-warning"
            End If
        Else
            ' lblError.CssClass = "alert alert-warning"
        End If
        lblError.Text = Message
    End Sub

    Public Shared Function StringReplace(ByVal Source As String, ByVal FromReplaceStr As String, ByVal ToReplaceStr As String) As String
        Try
            Dim StartVal As String, OrigStr As String
            StartVal = Source.ToUpper.IndexOf(FromReplaceStr.ToUpper)
            OrigStr = Source.Substring(StartVal, FromReplaceStr.Length)
            Source = Source.Replace(OrigStr, ToReplaceStr)
        Catch ex As Exception
        Finally
            StringReplace = Source
        End Try

    End Function
    
   
    Protected Sub hidStatus_ValueChanged(sender As Object, e As EventArgs)
        hidStatus.Value = "false"
        LoadRepeaterGroup(uscStudentPicker.STU_ID)
    End Sub
   

    Protected Sub lnkbtnAdjst_Click(sender As Object, e As EventArgs)
        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim param(2) As SqlClient.SqlParameter
        Dim ReturnVal As String = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@ALD_ID", SqlDbType.BigInt)
            param(0).Value = ViewID
            param(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            param(1).Value = uscStudentPicker.STU_ID
            param(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "FEES.SAVE_ACTIVITY_ADJUSTMENTS", param)

            If Not IsDBNull(param(2).Value) Then
                If param(2).Value = 0 Then
                    strans.Commit()
                    'lblError.Text = "Activity Adjusted Successfully"
                    '  ShowMessage(lblError.Text, False)
                    LoadRepeaterGroup(uscStudentPicker.STU_ID)
                Else
                    strans.Rollback()
                    ShowMessage(getErrorMessage(param(2).Value), True)
                End If
            Else
                strans.Rollback()
                ShowMessage(getErrorMessage(1000), True)
            End If

        Catch ex As Exception
            strans.Rollback()
            ShowMessage(getErrorMessage(1000), True)
        End Try

    End Sub
End Class
