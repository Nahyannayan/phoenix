Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_ServicePerformaInvoiceView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "ServiceProformaInvoice.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "F300163" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "F300163"
                            lblHead.Text = "Service Performa Invoice"
                            ViewState("trantype") = "A"
                    End Select
                    BindBusinessUnit()
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                '  lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub
    Sub BindBusinessUnit()
        ddBusinessunit.Items.Clear()
        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter
        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSU_ID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.Insert(0, New ListItem("Please Select", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FPH_INOICENO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_INOICENO", lstrCondn1)

                '   -- 2  txtDate
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_DT", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_DISPLAY", lstrCondn3)

                '   -- 4   txtStuno
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NO", lstrCondn4)

                '   -- 5  txtStuname
                larrSearchOpr = h_selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn5)



                '   -- 7  txtDesc
                larrSearchOpr = h_selected_menu_7.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
                lstrCondn7 = txtSearch.Text
                If (lstrCondn7 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "FPH_NARRATION", lstrCondn7)
            End If
            str_Sql = " SELECT  FPH_ID,ISNULL(A.FPH_COMP_ID,0)FPH_COMP_ID,FPH_INOICENO,FPH_DT ,B.GRD_DISPLAY ,C.STU_NO,C.NAME ," & _
                    " FPH_NARRATION,SUM(ISNULL(D.FPD_AMOUNT, 0)) AMOUNT " & _
                    " FROM    FEES.FEE_PERFORMAINVOICE_H AS A " & _
                    " LEFT JOIN OASIS.dbo.GRADE_M B ON A.FPD_GRD_ID = B.GRD_ID " & _
                    " INNER JOIN dbo.STUDENTS C ON A.FPD_STU_ID = C.STU_ID " & _
                    " LEFT JOIN FEES.FEE_PERFORMAINVOICE_D D ON A.FPH_ID = D.FPD_FPH_ID " _
                     & " where  FPH_BSU_ID='" & Session("sBSUID") & "'" _
                    & " AND FPH_STU_BSU_ID='" & ddBusinessunit.SelectedItem.Value & "'" & str_Filter _
            & " GROUP BY FPH_ID ,ISNULL(A.FPH_COMP_ID,0),FPH_INOICENO ,FPH_DT ,B.GRD_DISPLAY ,C.STU_NO ,C.NAME ,FPH_NARRATION"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If

            txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            txtSearch.Text = lstrCondn4

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            txtSearch.Text = lstrCondn5



            txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            txtSearch.Text = lstrCondn7

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "ServiceProformaInvoice.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub lbVoucher_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFPH_ID As Label = sender.Parent.parent.findcontrol("lblFPH_ID")
            Dim lblFPH_INVNO As Label = sender.Parent.parent.findcontrol("lblReceipt")
            Dim gvr As GridViewRow = TryCast(sender.Parent.parent, GridViewRow)
            Dim COMP_ID As Integer = gvJournal.DataKeys(gvr.RowIndex)(0)
            If COMP_ID > 0 Then
                Session("ReportSource") = ServicePerformaInvoice.PrintReceiptComp(lblFPH_INVNO.Text, Session("sBsuid"), Me.ddBusinessunit.SelectedValue, _
                                            Session("sUsr_name"))
                h_print.Value = "print"
                h_Export.Value = "No"
            Else
                Session("ReportSource") = ServicePerformaInvoice.PrintReceipt(lblFPH_ID.Text, Session("sBsuid"), _
                            Session("sUsr_name"), ddBusinessunit.SelectedItem.Value)
                h_print.Value = "print"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub lbExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblFPH_ID As Label = sender.Parent.parent.findcontrol("lblFPH_ID")
            Dim lblFPH_INVNO As Label = sender.Parent.parent.findcontrol("lblReceipt")
            'PrintReceipt(lblFPH_ID.Text)
            Dim gvr As GridViewRow = TryCast(sender.Parent.parent, GridViewRow)
            'gvr.RowIndex
            Dim COMP_ID As Integer = gvJournal.DataKeys(gvr.RowIndex)(0)
            If COMP_ID > 0 Then
                Session("ReportSource") = ServicePerformaInvoice.PrintReceiptComp(lblFPH_INVNO.Text, Session("sBsuid"), Me.ddBusinessunit.SelectedValue, _
                                            Session("sUsr_name"))
                h_print.Value = "print"
                h_Export.Value = "No"
            Else
                Session("ReportSource") = ServicePerformaInvoice.PrintReceipt(lblFPH_ID.Text, Session("sBsuid"), _
                            Session("sUsr_name"), ddBusinessunit.SelectedItem.Value)
                h_print.Value = "print"
                h_Export.Value = "Yes"
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
