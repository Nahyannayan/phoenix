﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_ServiceProformaInvoice
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            ViewState("datamode") = "add"
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Or ViewState("MainMnu_code") <> "F300163" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ClientScript.RegisterStartupScript(Me.GetType(), _
                     "script", "<script language='javascript'>  CheckForPrint(); </script>")
            
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            InitialiseCompnents()
            'Session("FeeTransport") = FeeTranspotInvoice.CreateFeeTransportPI
            Session("dataBind") = Nothing
            Session("DetailedINV") = Nothing
            BindBusinessUnit()
            FillACD()
            ddlAcademicYear_SelectedIndexChanged(sender, e)
            'ViewState("ACD_ID") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ACD_ID FROM dbo.ACADEMICYEAR_D WHERE ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CURRENT=1")
            PopulateMonths()
        End If
    End Sub
    Sub BindBusinessUnit()
        ddBusinessunit.Items.Clear()
        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter
        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSU_ID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.Insert(0, New ListItem("Please Select", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        Clear_All()
        ' ddlAcademicYear_SelectedIndexChanged(sender, e)
    End Sub
    Sub InitialiseCompnents()
        gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBsuid"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Private Sub PopulateMonths()

        Dim dtTable As DataTable = ServicePerformaInvoice.PopulateMonthsInAcademicYearID(ddlAcademicYear.SelectedValue)
        ' PROCESS Filter
        Dim dvTRM_DESCRIPTION As New DataView(dtTable, "", "TRM_DESCRIPTION", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumTRM_DESCRIPTION As IEnumerator = dvTRM_DESCRIPTION.GetEnumerator
        Dim drTRM_DESCRIPTION As DataRowView
        While (ienumTRM_DESCRIPTION.MoveNext())
            'Processes List
            drTRM_DESCRIPTION = ienumTRM_DESCRIPTION.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drTRM_DESCRIPTION("TRM_DESCRIPTION") Then
                    contains = True
                End If
            End While
            Dim trNodeTRM_DESCRIPTION As New TreeNode(drTRM_DESCRIPTION("TRM_DESCRIPTION"), drTRM_DESCRIPTION("TRM_ID"))
            If contains Then
                Continue While
            End If
            'If trSelectAll.ChildNodes.Contains(trNodeTRM_DESCRIPTION) Then
            '    Continue While
            'End If
            Dim strAMS_MONTH As String = "TRM_DESCRIPTION = '" & _
            drTRM_DESCRIPTION("TRM_DESCRIPTION") & "'"
            Dim dvAMS_MONTH As New DataView(dtTable, strAMS_MONTH, "AMS_ID", DataViewRowState.OriginalRows)
            Dim ienumAMS_MONTH As IEnumerator = dvAMS_MONTH.GetEnumerator
            While (ienumAMS_MONTH.MoveNext())
                Dim drMONTH_DESCR As DataRowView = ienumAMS_MONTH.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drMONTH_DESCR("MONTH_DESCR"), drMONTH_DESCR("AMS_ID"))
                trNodeTRM_DESCRIPTION.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeTRM_DESCRIPTION)
        End While
        trMonths.Nodes.Clear()
        trMonths.Nodes.Add(trSelectAll)
        trMonths.DataBind()


    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateServices()
        PopulateMonths()
    End Sub
    Private Sub PopulateServices()

        Dim qry As String = "SELECT FEE_ID,FEE_DESCR FROM dbo.SERVICES_BSU_M A LEFT JOIN " & _
            "FEES.FEES_M B ON A.SVB_SVC_ID=B.FEE_SVC_ID WHERE ISNULL(FEE_SVC_ID,0)<>0 " & _
            "AND SVB_BSU_ID='" & Session("sBsuid") & "' AND SVB_ACD_ID='" & ddlAcademicYear.SelectedValue & "' ORDER BY FEE_DESCR"

        Dim dsServices As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, qry)
        If dsServices.Tables(0).Rows.Count > 0 Then
            Me.gvServices.DataSource = dsServices.Tables(0)
            Me.gvServices.DataBind()
            SelectAllServices()
        End If

    End Sub
    Sub SelectAllServices()
        For Each gvr As GridViewRow In gvServices.Rows
            Dim chb As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            chb.Checked = True
        Next
    End Sub
    Sub Clear_All()
        txtRemarks.Text = ""
        H_Location.Value = ""
        FillACD()
        PopulateServices()
        'PopulateMonths()
        txtFrom.Text = Format(Date.Now, "dd/MMM/yyyy")
        ClearStudentData()
        gvStudentDetails.DataSource = Nothing
        gvStudentDetails.DataBind()
        Session("dataBind") = Nothing
        Session("DetailedINV") = Nothing
    End Sub

    Function CheckForErrors() As String
        Dim str_error As String = ""

        If h_Student_no.Value = "" Then
            str_error = str_error & "Please select the student<br />"
        End If

        If H_Location.Value = "" Then
            str_error = str_error & "Please select location<br />"
        End If

        If txtRemarks.Text = "" Then
            str_error = str_error & "Please enter remarks <br />"
        End If
        Return str_error
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Not Master.IsSessionMatchesForSave() Then
            ' lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If gvStudentDetails.Rows.Count = 0 Then
            ' lblError.Text = "Please enter students..!"
            usrMessageBar.ShowNotification("Please enter students..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If txtRemarks.Text = "" Then
            ' lblError.Text = "Please enter remarks..!"
            usrMessageBar.ShowNotification("Please enter remarks..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim retval As String = "1000"
        Dim new_fph_id As String = ""
        Dim STU_TYPE As String = "S"
        If rbEnquiry.Checked Then
            STU_TYPE = "E"
        End If
        Dim sch_id As String
        Dim str_monthorTerm As String
        If rbTermly.Checked Then
            str_monthorTerm = GetTermsSelected()
            sch_id = 2
        Else
            str_monthorTerm = GetMonthsSelected()
            sch_id = 0
        End If
        Dim InvoiceNo As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim lblStudID As New Label
            Dim SblId As String = "0"
            For Each gvrH As GridViewRow In gvStudentDetails.Rows
                lblStudID = TryCast(gvrH.FindControl("lblStudID"), Label)
                If Not lblStudID Is Nothing Then
                    If h_Company_ID.Value = "" Then
                        InvoiceNo = ""
                    End If
                    retval = ServicePerformaInvoice.F_SAVEFEE_PERFORMAINVOICE_H(0, Session("sBsuid"), ddBusinessunit.SelectedItem.Value, _
                                    InvoiceNo, txtFrom.Text, ddlAcademicYear.SelectedValue, lblStudID.Text, "", STU_TYPE, new_fph_id, False, sch_id, _
                                     h_Company_ID.Value, 0, SblId, txtRemarks.Text, objConn, stTrans)
                    If retval <> "0" Then
                        Exit For
                    End If
                    Dim ds As New DataSet
                    If Session("DetailedINV") IsNot Nothing Then
                        Dim mRow As DataRow
                        For Each mRow In CType(Session("DetailedINV"), DataTable).Select("STU_ID='" & lblStudID.Text & "'", "")
                            retval = ServicePerformaInvoice.F_SAVEFEE_PERFORMAINVOICE_D(0, new_fph_id, mRow("FEE_ID"), mRow("AMOUNT").ToString(), _
                                                                                 mRow("REF_ID").ToString(), False, mRow("DESCRIPTN").ToString, objConn, stTrans)
                            If retval <> "0" Then
                                Exit For
                            End If
                        Next
                    End If
                    retval = ServicePerformaInvoice.SaveReferenceDetails(0, new_fph_id, objConn, stTrans, txtFrom.Text, False)
                    If retval <> "0" Then
                        Exit For
                    End If
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                End If
            Next
            If retval = "0" Then
                stTrans.Commit()
                '  lblError.Text = getErrorMessage("0")
                usrMessageBar.ShowNotification(getErrorMessage("0"), UserControls_usrMessageBar.WarningType.Danger)
                If Not chkGroupInvoice.Checked Then
                    h_print.Value = "print"
                    Session("ReportSource") = ServicePerformaInvoice.PrintReceipt(new_fph_id, Session("sBsuid"), _
                    Session("sUsr_name"), ddBusinessunit.SelectedItem.Value)
                ElseIf h_Company_ID.Value <> "" Then
                    h_print.Value = "print"
                    Session("ReportSource") = ServicePerformaInvoice.PrintReceiptComp(InvoiceNo, Session("sBsuid"), _
                    ddBusinessunit.SelectedItem.Value, Session("sUsr_name"))
                End If

                Clear_All()
            Else
                stTrans.Rollback()
                '  lblError.Text = getErrorMessage(retval)
                usrMessageBar.ShowNotification(getErrorMessage(retval), UserControls_usrMessageBar.WarningType.Danger)
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            'lblError.Text = getErrorMessage("1000")
            usrMessageBar.ShowNotification(getErrorMessage("1000"), UserControls_usrMessageBar.WarningType.Danger)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub setEditdata()
        txtFrom.Attributes.Remove("readonly")
        imgFrom.Enabled = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Clear_All()
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        SetStudentCount()
    End Sub

    Sub SetStudentCount()
        Try
            h_Student_no.Value = h_Student_no.Value.Replace("||", ",")

            If chkGroupInvoice.Checked = False Then

            End If

            If h_Student_no.Value <> "" Then
                Dim str_Sql As String = "select count(*) from  FEES.FEE_PERFORMAINVOICE_H where " & _
                "FPD_STU_ID in ( " & h_Student_no.Value & " ) AND FPH_ACD_ID = " & ddlAcademicYear.SelectedValue
                Dim count As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, str_Sql)
                If count > 0 Then
                    h_setAlert.Value = "1"
                Else
                    h_setAlert.Value = "0"
                End If
            End If
        Catch
        End Try
    End Sub

    Protected Sub txtAmountToPay_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtAmount As New TextBox
        txtAmount = sender
        'txtAmount.Attributes.Add("onFocus", "this.select();")
        '  txtAmount.Attributes.Add("readonly", "readonly")
        txtAmount.Style.Add("text-align", "right")
    End Sub

    Protected Sub rbEnrollment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnrollment.CheckedChanged
        ClearStudentData()
    End Sub

    Protected Sub rbEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnquiry.CheckedChanged
        ClearStudentData()
    End Sub

    Sub ClearStudentData()
        h_Student_no.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
    End Sub

  
    Private Function GetselectedServices() As String
        GetselectedServices = ""
        For Each gvr As GridViewRow In gvServices.Rows
            Dim chb As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            Dim FEEID As Integer = Me.gvServices.DataKeys(gvr.RowIndex)(0)
            GetselectedServices = GetselectedServices & IIf(GetselectedServices = "", "", "|") & IIf(chb.Checked = True, FEEID, "")
        Next
    End Function
    Sub Addto_Grid(ByVal GroupYN As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim ds As DataSet, ds_Detail As DataSet
        Dim sch_id As Integer
        Dim str_monthorTerm As String, FEEIDs As String
        If rbTermly.Checked Then
            str_monthorTerm = GetTermsSelected()
            sch_id = 2
        Else
            str_monthorTerm = GetMonthsSelected()
            sch_id = 0
        End If
        If GetselectedServices() = "" Then
            ' lblError.Text = "Please select any service..!"
            usrMessageBar.ShowNotification("Please select any service..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            FEEIDs = GetselectedServices()
        End If
        If str_monthorTerm.Equals("") Then
            ' lblError.Text = "Please Select Term..!"
            usrMessageBar.ShowNotification("Please Select Term..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not chkGroupInvoice.Checked Then
            If Session("dataBind") IsNot Nothing Then
                CType(Session("dataBind"), DataTable).Rows.Clear()
            End If
            If Session("DetailedINV") IsNot Nothing Then
                CType(Session("DetailedINV"), DataTable).Rows.Clear()
            End If
        End If
        ds = ServicePerformaInvoice.GETFEEPAYABLEFORPERFORMA_GROUP(ddlAcademicYear.SelectedValue, "", Session("sBsuid"), _
                  str_monthorTerm, h_Student_no.Value.Replace(",", "|"), FEEIDs, sch_id, False, Val(h_Company_ID.Value), str_conn)
        ds_Detail = ServicePerformaInvoice.GETFEEPAYABLEFORPERFORMA_GROUP(ddlAcademicYear.SelectedValue, "", Session("sBsuid"), _
                 str_monthorTerm, h_Student_no.Value.Replace(",", "|"), FEEIDs, sch_id, True, Val(h_Company_ID.Value), str_conn)
        Dim mRow, iRow As DataRow
        If Session("dataBind") IsNot Nothing Then
            For Each mRow In ds.Tables(0).Rows
                For Each iRow In CType(Session("dataBind"), DataTable).Select("STU_ID='" & mRow("STU_ID") & "'")
                    iRow.Delete()
                Next
                CType(Session("dataBind"), DataTable).ImportRow(mRow)
                If Session("DetailedINV") IsNot Nothing Then
                    For Each iRow In CType(Session("DetailedINV"), DataTable).Select("STU_ID='" & mRow("STU_ID") & "'")
                        iRow.Delete()
                    Next
                End If
            Next
        Else
            Session("dataBind") = ds.Tables(0)
        End If
        If Session("DetailedINV") IsNot Nothing Then
            For Each iRow In ds_Detail.Tables(0).Rows
                CType(Session("DetailedINV"), DataTable).ImportRow(iRow)
            Next
        Else
            If ds_Detail.Tables.Count > 0 Then
                Session("DetailedINV") = ds_Detail.Tables(0)
            End If
        End If

        gvStudentDetails.DataSource = Session("dataBind")
        gvStudentDetails.DataBind()
        'Session("dataBind") = ds.Tables(0)
        If txtStdNo.Text = "" Or txtStudentname.Text = "" Then
            '  lblError.Text = "Select student!!!"
            usrMessageBar.ShowNotification("Select student!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Exit Sub
        If chkGroupInvoice.Checked Then
            Addto_Grid(True)
        Else
            Try
                Dim str_data, str_sql As String

                txtStdNo.Text = txtStdNo.Text.Trim
                Dim iStdnolength As Integer = txtStdNo.Text.Length
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = ddBusinessunit.SelectedItem.Value & txtStdNo.Text
                End If
                If rbEnrollment.Checked Then
                    str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
                     & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & ddBusinessunit.SelectedItem.Value & "') AND  STU_NO='" & txtStdNo.Text & "'"
                    str_data = GetDataFromSQL(str_sql, WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString)
                Else
                    str_data = GetDataFromSQL("SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                    & " WHERE     (STU_BSU_ID = '" & ddBusinessunit.SelectedItem.Value & "') AND  STU_NO='" & txtStdNo.Text & "'", _
                    WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString)
                End If
                If str_data <> "--" Then
                    h_Student_no.Value = str_data.Split("|")(0)
                    txtStdNo.Text = str_data.Split("|")(1)
                    txtStudentname.Text = str_data.Split("|")(2)
                    If Not FeeCommon.GetStudentWithLocation_ForPerforma(h_Student_no.Value, txtStdNo.Text, txtStudentname.Text, _
                    ddBusinessunit.SelectedItem.Value, True, H_Location.Value, "", txtFrom.Text, ddlAcademicYear.SelectedValue) Then
                        lblNoStudent.Text = "Invalid Student #/Area Not Found!!!"
                    End If
                    If H_Location.Value <> "" Then
                        If txtRemarks.Text.Trim = "" Then
                            If rbMonthly.Checked Then
                                txtRemarks.Text = FillMonthsSelected()
                            Else
                                txtRemarks.Text = FillTermsSelected()
                            End If
                        End If
                        Addto_Grid(False)
                    End If
                Else
                    h_Student_no.Value = ""
                    txtStudentname.Text = ""
                    lblNoStudent.Text = "Student # Entered is not valid  !!!"
                End If
            Catch
            End Try
            SetStudentCount()
        End If
    End Sub

    Private Function FillMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If str_Selected = "" Then
                    str_Selected = "For the month(s) " & node.Text
                Else
                    str_Selected = str_Selected & ", " & node.Text
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function FillTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    If node.Value <> "ALL" Then
                        If str_Selected = "" Then
                            str_Selected = "For the Term(s) " & node.Text
                        Else
                            str_Selected = str_Selected & ", " & node.Text
                        End If
                    End If
                Else
                    Continue For
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetTermsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    If node.Value <> "ALL" Then
                        If str_Selected = "" Then
                            str_Selected = node.Value & "|"
                        Else
                            str_Selected = str_Selected & node.Value & "|"
                        End If
                    End If
                Else
                    Continue For
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Private Function GetMonthsSelected() As String
        Try
            Dim str_Selected As String = ""
            For Each node As TreeNode In trMonths.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If str_Selected = "" Then
                    str_Selected = node.Value & "|"
                Else
                    str_Selected = str_Selected & node.Value & "|"
                End If
            Next
            Return str_Selected
        Catch
            Return ""
        End Try
    End Function

    Protected Sub btnNarrInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNarrInsert.Click
        If rbMonthly.Checked Then
            txtRemarks.Text = FillMonthsSelected()
        Else
            txtRemarks.Text = FillTermsSelected()
        End If
        Addto_Grid(False)
    End Sub

    Protected Sub rbTermly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTermly.CheckedChanged
        'gvFeeTransport.DataBind()
    End Sub

    Protected Sub rbMonthly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbMonthly.CheckedChanged
        'gvFeeTransport.DataBind()
    End Sub

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        If chkGroupInvoice.Checked Then
            Addto_Grid(True)
        Else
            Addto_Grid(False)
        End If
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStudID As New Label
        Dim iRow As DataRow
        lblStudID = TryCast(sender.parent.FindControl("lblStudID"), Label)
        If Not lblStudID Is Nothing Then
            Dim dv As New DataView(Session("dataBind"))
            dv.RowFilter = " STU_ID<>" & CInt(lblStudID.Text) & ""
            Session("dataBind") = dv.ToTable()
            gvStudentDetails.DataSource = dv
            gvStudentDetails.DataBind()
            h_Student_no.Value = h_Student_no.Value.Replace(lblStudID.Text, "0")

            If Session("DetailedINV") IsNot Nothing Then
                For Each iRow In CType(Session("DetailedINV"), DataTable).Select("STU_ID='" & CInt(lblStudID.Text) & "'")
                    iRow.Delete()
                Next
            End If
        End If
    End Sub

    Protected Sub lnkClearCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClearCompany.Click
        h_Company_ID.Value = ""
        txtCompanyDescr.Text = ""
        chkCompanyFilter.Checked = False
    End Sub


    Protected Sub h_Student_no_ValueChanged(sender As Object, e As EventArgs)
        SetStudentCount()
    End Sub
End Class
