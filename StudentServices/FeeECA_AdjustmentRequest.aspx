﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeECA_AdjustmentRequest.aspx.vb" Inherits="StudentServices_FeeECA_AdjustmentRequest" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fee Adjustments..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                        </td>

                    </tr>
                </table>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" colspan="2">
                            <asp:RadioButton ID="radStud" runat="server" AutoPostBack="True" Checked="True" Text="Student"
                                GroupName="STU_TYPE" />
                            <asp:RadioButton ID="radEnq" runat="server" Text="Enquiry" AutoPostBack="True" GroupName="STU_TYPE" />
                        </td>
                        <td colspan="2" align="right">
                            <asp:RadioButton ID="radRequested" runat="server" AutoPostBack="True" Checked="True"
                                Text="Requested" GroupName="STU_APROVE" CausesValidation="True" />
                            <asp:RadioButton ID="radApproved" runat="server" AutoPostBack="True" Text="Approved"
                                GroupName="STU_APROVE" />
                            <asp:RadioButton ID="radReject" runat="server" Text="Reject" AutoPostBack="True"
                                GroupName="STU_APROVE" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFEEAdjustments" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found"
                                Width="100%" AllowPaging="True" PageSize="30" CssClass="table table-bordered table-row">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="FCH_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFAH_ID" runat="server" Text='<%# Bind("FAH_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student ID
                                                       <br />
                                            <asp:TextBox ID="txtstudno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudnoSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            Name     
                                            <br />
                                            <asp:TextBox ID="txtstudname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStudnameSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_NAME" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderTemplate>
                                            Date
                                                        <br />
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>

                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("FAH_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doc No">
                                        <HeaderTemplate>
                                            Doc No            
                                            <br />
                                            <asp:TextBox ID="txtAcademicYear" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAcademicYearSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACD_YEAR" runat="server" Text='<%# Bind("FAR_DOCNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderTemplate>
                                            Remarks
                                                       <br />
                                            <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("FAH_REMARKS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ARM_DESCR">
                                        <HeaderTemplate>
                                            Reason 
                                            <br />
                                            <asp:TextBox ID="txtReason" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnReasonSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReason" runat="server" Text='<%# Bind("ARM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <PagerStyle HorizontalAlign="Right" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="h_Export" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
