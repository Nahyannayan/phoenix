﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class StudentServices_FeeECA_Gen_Add_Students
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("ID") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("ID").ToString
                ViewState("MainID") = MainID
                bindStudentsGrid()
            End If
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub

    Protected Sub gvStudentsList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentsList.PageIndexChanging
        gvStudentsList.PageIndex = e.NewPageIndex
        bindStudentsGrid()
    End Sub

    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindStudentsGrid()
    End Sub
    Protected Sub btnSearchSTU_NAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindStudentsGrid()
    End Sub

    Protected Sub btnSearchSTU_GRD_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindStudentsGrid()
    End Sub

    Private Sub bindStudentsGrid()

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim param(3) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        Dim STU_NO As String = String.Empty
        Dim str_STU_NO As String = String.Empty
        Dim STU_NAME As String = String.Empty
        Dim str_STU_NAME As String = String.Empty
        Dim STU_GRD As String = String.Empty
        Dim str_STU_GRD As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty

        param(0) = New SqlClient.SqlParameter("@STU_BSU_ID", ViewState("MainID"))

        Try
            If gvStudentsList.Rows.Count > 0 Then

                txtSearch = gvStudentsList.HeaderRow.FindControl("txtStu_NO")

                If txtSearch.Text.Trim <> "" Then
                    STU_NO = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvStudentsList.HeaderRow.FindControl("txtStu_NAME")

                If txtSearch.Text.Trim <> "" Then
                    STU_NAME = " AND replace(STU_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NAME = txtSearch.Text.Trim
                End If

                txtSearch = gvStudentsList.HeaderRow.FindControl("txtStu_GRD")

                If txtSearch.Text.Trim <> "" Then
                    STU_GRD = " AND replace(GRD_DISPLAY,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_GRD = txtSearch.Text.Trim
                End If
            End If
            FILTER_COND = STU_NO + STU_NAME

            param(1) = New SqlParameter("@FILTERCONDITION", FILTER_COND)
            param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuId"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SM.Get_ECA_GEN_STUDENT_LIST", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudentsList.DataSource = ds
                gvStudentsList.DataBind()
                Dim columnCount As Integer = gvStudentsList.Rows(0).Cells.Count
                gvStudentsList.Rows(0).Cells.Clear()
                gvStudentsList.Rows(0).Cells.Add(New TableCell)
                gvStudentsList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudentsList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudentsList.Rows(0).Cells(0).Text = "Currently there is no Student Exists"
            Else
                gvStudentsList.DataSource = ds
                gvStudentsList.DataBind()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSTU_ID As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        lblSTU_ID = sender.Parent.FindControl("lblSTU_ID")
        Dim lblCode As New Label
        lblCode = sender.Parent.FindControl("lblCode")
        If (Not lblSTU_ID Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblSTU_ID.Text & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblCode.Text & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            'h_SelectedId.Value = "Close"
        End If
    End Sub
End Class
