﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="FeeECA_Gen_Collection_Add.aspx.vb" Inherits="StudentServices_FeeECA_Gen_Collection_Add" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">
        Sys.Application.add_load(
    function CheckForPrint() {
        ShowHideBankAccount();
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            <%--showModelessDialog('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");--%>
            Popup('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
            document.getElementById('<%= h_print.ClientID %>').value = '';
        }
    }
    );
    function CheckForPrint() {
        ShowHideBankAccount();
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
            <%--showModelessDialog('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");--%>
            Popup('FeeReceiptECA.aspx' + document.getElementById('<%= h_print.ClientID %>').value);
            document.getElementById('<%= h_print.ClientID %>').value = '';
        }
    }

    <%--function GetStudentListName() {
        var sFeatures, url;


        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 600px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;

        var bsuId = $("#<%=ddlBusinessunit.ClientID%>").val();

        //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
        url = "FeeECA_Gen_Add_Students.aspx?ID=" + bsuId
        result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined) {

            return false;
        }
        if (result != '' && result != undefined) {
            NameandCode = result.split('||');
            $("#<%=h_StuID.ClientID %>").val(NameandCode[0]);
            $("#<%=txtStdNo.ClientID %>").val(NameandCode[2]);
            $("#<%=txtStudentname.ClientID %>").val(NameandCode[1]);

        }
        return true;
    }--%>
        function CheckNumber(ctrl) {
            var temp;
            temp = parseFloat(ctrl.value);
            if (isNaN(temp))
                ctrl.value = 0;
            else
                ctrl.value = temp.toFixed(2);
        }


        function UpdateSum() {
            var txtCCTotal, txtBankTotal, txtCashTotal,
    txtTotal, txtReturn, txtReceivedTotal, txtBalance;

            txtCCTotal = parseFloat($("#<%=txtCCTotal.ClientID %>").val());
            if (isNaN(txtCCTotal))
                txtCCTotal = 0;

            txtBankTotal = parseFloat($("#<%=txtBankTotal.ClientID %>").val());
            if (isNaN(txtBankTotal))
                txtBankTotal = 0;
            txtCashTotal = parseFloat($("#<%=txtCashTotal.ClientID %>").val());
            if (isNaN(txtCashTotal))
                txtCashTotal = 0;
            txtTotal = parseFloat($("#<%=txtTotal.ClientID %>").val());
            if (isNaN(txtTotal))
                txtTotal = 0;
            txtReceivedTotal = txtCCTotal + txtBankTotal + txtCashTotal;
            txtBankTotal = txtBankTotal;
            txtBalance = 0;
            if (txtReceivedTotal > 0 && txtTotal > txtReceivedTotal) {
                txtBalance = txtTotal - txtReceivedTotal;
            }
         
            //if (txtCashTotal > 0) {//|| txtBankTotal > 0 || txtCCTotal > 0
            if (txtReceivedTotal > txtTotal) {
                txtBalance = txtReceivedTotal - txtTotal;               
                //if (txtBalance > txtCashTotal)
                //    txtBalance = 0;
            }
            else {
               
                txtBalance = (parseFloat(txtTotal)) - parseFloat(txtReceivedTotal);
            }
            //}

            if (txtReceivedTotal > 0) {
                var temp1;
                temp1 = parseFloat(txtReceivedTotal);
                if (isNaN(temp1))
                    txtReceivedTotal = 0;
                else
                    txtReceivedTotal = temp1.toFixed(2);

                $("#<%=txtReceivedTotal.ClientID %>").val(txtReceivedTotal);
            <%--$("#<%=txtReceivedTotal.ClientID %>").val(Math.round(txtReceivedTotal));--%>
            }
            if (txtCCTotal > 0) {
                var temp2;
                temp2 = parseFloat(txtCCTotal);
                if (isNaN(temp2))
                    txtCCTotal = 0;
                else
                    txtCCTotal = temp2.toFixed(2);

                $("#<%=txtCCTotal.ClientID %>").val(txtCCTotal);
               <%-- $("#<%=txtCCTotal.ClientID %>").val(Math.round(txtCCTotal));--%>
            }
            if (txtBankTotal) {
                var temp3;
                temp3 = parseFloat(txtBankTotal);
                if (isNaN(temp3))
                    txtBankTotal = 0;
                else
                    txtBankTotal = temp3.toFixed(2);

                $("#<%=txtBankTotal.ClientID %>").val(txtBankTotal);
            <%--$("#<%=txtBankTotal.ClientID %>").val(Math.round(txtBankTotal));--%>
            }

            if (txtCashTotal) {
                var temp4;
                temp4 = parseFloat(txtCashTotal);
                if (isNaN(temp4))
                    txtCashTotal = 0;
                else
                    txtCashTotal = temp4.toFixed(2);

                $("#<%=txtCashTotal.ClientID %>").val(txtCashTotal);
            <%--$("#<%=txtCashTotal.ClientID %>").val(Math.round(txtCashTotal));--%>
            }
            if (txtBalance > 0) {
                var temp5;
                temp5 = parseFloat(txtBalance);
                if (isNaN(temp5))
                    txtBalance = 0;
                else
                    txtBalance = temp5.toFixed(2);

                $("#<%=txtBalance.ClientID %>").val(txtBalance);
                <%--$("#<%=txtBalance.ClientID %>").val(Math.round(txtBalance));--%>
            }



        }

        function HideAll() {
            $("#<%=pnlCheque.ClientID %>").hide();
            $("#<%=pnlCreditCard.ClientID %>").hide();

            return false;
        }

        <%--function getBankOrEmirate(mode, ctrl) {
            var sFeatures, url;
            sFeatures = "dialogWidth: 600px; dialogHeight: 500px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            if (mode == 1) {
                if (ctrl == 1) {
                    document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                    document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
                    if (NameandCode[1] == "Bank Transfer") {
                        $("#tdhBA").show(250);
                        $("#tdBA1").show(250);
                    }
                    else {
                        $("#tdhBA").hide(250);
                        $("#tdBA1").hide(250);
                    }
                }

            }
            return false;
        }--%>
        function ShowHideBankAccount() {
            if (document.getElementById('<%= txtBank.ClientID%>').value != 'Bank Transfer') {
                $("#tdhBA").hide();
                $("#tdBA1").hide();
            }
            else {
                $("#tdhBA").show();
                $("#tdBA1").show();
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>

    <script>
        function GetStudentListName() {
            var url;

            var bsuId = $("#<%=ddlBusinessunit.ClientID%>").val();

            //url = 'ShowStudent.aspx?TYPE='+TYPE+'&VAL=' + selType + '&ACD_ID=' + selACD_ID 
            url = "FeeECA_Gen_Add_Students.aspx?ID=" + bsuId
            var oWnd = radopen(url, "pop_student");
        }


        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                $("#<%=h_StuID.ClientID %>").val(NameandCode[0]);
                $("#<%=txtStdNo.ClientID %>").val(NameandCode[2]);
                $("#<%=txtStudentname.ClientID %>").val(NameandCode[1]);
                __doPostBack('<%=txtStdNo.ClientID%>', 'TextChanged');
            }
        }
        function getBankOrEmirate(mode, ctrl) {
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
            var url;
            if (mode == 1)
                url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
            else
                url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
            var oWnd = radopen(url, "pop_getBankOrEmirate");
        }
        function OnClientClose2(oWnd, args) {
            var mode = document.getElementById('<%= hf_mode.ClientID%>').value;
            var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');

                if (mode == 1) {
                    if (ctrl == 1) {
                        document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                        document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
                        if (NameandCode[1] == "Bank Transfer") {
                            $("#tdhBA").show(250);
                            $("#tdBA1").show(250);
                        }
                        else {
                            $("#tdhBA").hide(250);
                            $("#tdBA1").hide(250);
                        }
                    }

                }
            }
        }
        function getBank(mode) {
            document.getElementById('<%= hf_mode.ClientID%>').value = mode
            var txtBankCode;
            if (mode == 1) txtBankCode = document.getElementById('<%=txtBankAct1.ClientID%>').value;

            var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "pop_getBank");


        }

        function OnClientClose3(oWnd, args) {
            var mode = document.getElementById('<%= hf_mode.ClientID%>').value;
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');

                if (mode == 1) {
                    document.getElementById('<%=hfBankAct1.ClientID%>').value = lstrVal[0];
                    document.getElementById('<%=txtBankAct1.ClientID%>').value = lstrVal[0];
                }
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Services Fee Collection
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblError" runat="server" SkinID="LabelError" EnableViewState="False"></asp:Label>
                            <asp:HiddenField ID="h_StuID" runat="server" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlBusinessunit" runat="server" AutoPostBack="True" TabIndex="5">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" TabIndex="10"
                                SkinID="DropDownListNormal" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left"><span class="field-label">Date</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtFrom" runat="server" TabIndex="15" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="20" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Select Student Type</span> </td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="rbEnrollment" runat="server" Checked="True" GroupName="mode" CssClass="field-label"
                                Text="Student#" AutoPostBack="True" />
                            <asp:RadioButton ID="rbEnquiry" runat="server" GroupName="mode" Text="Enquiry#" AutoPostBack="True" CssClass="field-label"
                                Enabled="False" />
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server" HoverCssClass="popupHover"
                                PopDelay="25" PopupControlID="PanelTree" PopupPosition="Center" TargetControlID="pnlLink">
                            </ajaxToolkit:HoverMenuExtender>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Student Code</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" OnTextChanged="txtStdNo_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetStudentListName();return false;" />
                        </td>
                        <td><span class="field-label">Student Name</span></td>
                        <td>
                            <asp:TextBox ID="txtStudentname" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImgMore" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Misc/moreinfo.gif"
                                OnClientClick="return false;" Visible="false" />
                            <asp:Panel ID="pnlMonthterm" runat="server">
                                <asp:Label ID="lblNoStudent" runat="server" EnableViewState="False" CssClass="error"
                                    ForeColor="Red"></asp:Label>
                                <asp:RadioButton ID="rbMonthly" runat="server" AutoPostBack="True" GroupName="trm"
                                    Text="Monthly" Visible="false" />
                                <asp:RadioButton ID="rbTermly" runat="server" AutoPostBack="True" Checked="True"
                                    GroupName="trm" Text="Termly" Visible="false" />
                                <asp:Panel ID="pnlLink" runat="server" Visible="False">
                                    <asp:LinkButton ID="lnkBtnSelectmonth" runat="server" OnClientClick="return false;"
                                        TabIndex="15">Select Month/Term</asp:LinkButton>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr class="title-bg">
                        <td colspan="4" align="left">Fee Details</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                CssClass="table table-row table-bordered" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C.P">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ChkPost" runat="server" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OPENING" DataFormatString="{0:0.00}" HeaderText="Opening">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MONTHLY_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Charge">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONC_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Concession">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ADJUSTMENT" DataFormatString="{0:0.00}" HeaderText="Adjustment">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PAID_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Paid">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLOSING" DataFormatString="{0:0.00}" HeaderText="Due">
                                        <ItemStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Paying Now">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmountToPay" AutoCompleteType="Disabled" runat="server" AutoPostBack="True"
                                                onblur="return CheckAmount(this);" onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged"
                                                Style="text-align: right" TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtAmountToPay" runat="server" FilterType="Numbers, Custom"
                                                ValidChars="." TargetControlID="txtAmountToPay" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FSH_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="tr_AddFee" runat="server" visible="false">
                        <td align="left"><span class="field-label">Fee</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlFeeType" runat="server" AutoPostBack="True" SkinID="DropDownListNormal">
                            </asp:DropDownList><br />
                            <asp:CheckBox ID="ChkChgPost" runat="server" Text="Charge & Post" Checked="True" CssClass="field-label"></asp:CheckBox></td>
                        <td align="left"><span class="field-label">Amount</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtAmountAdd" runat="server" onblur="CheckNumber(this)" AutoCompleteType="Disabled"
                                TabIndex="45"></asp:TextBox>
                            <asp:Button ID="btnAddDetails" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="50" /></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="left" colspan="1" width="13%"><span class="field-label">Credit card</span></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtCCTotal" onFocus="this.select();" Style="text-align: right" runat="server" autocomplete="off" Text="0.00"
                                AutoCompleteType="Disabled" onblur="CheckNumber(this);UpdateSum();" TabIndex="66"></asp:TextBox>
                            <ajaxToolkit:PopupControlExtender ID="pceCreditCard" runat="server" PopupControlID="pnlCreditCard"
                                Position="Bottom" TargetControlID="txtCCTotal">
                            </ajaxToolkit:PopupControlExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtCCTotal" runat="server" FilterType="Numbers, Custom"
                                ValidChars="." TargetControlID="txtCCTotal" />
                        </td>
                        <td width="13%" align="left"><span class="field-label">Cheque</span></td>
                        <td align="left" width="20%">
                            <asp:TextBox ID="txtBankTotal" Style="text-align: right" runat="server" AutoCompleteType="Disabled" autocomplete="off" Text="0.00"
                                onblur="CheckNumber(this);UpdateSum();" onfocus="this.select();" TabIndex="70"></asp:TextBox>
                            <ajaxToolkit:PopupControlExtender ID="pceCheque" runat="server" PopupControlID="pnlCheque"
                                Position="Bottom" TargetControlID="txtBankTotal">
                            </ajaxToolkit:PopupControlExtender>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtBankTotal" runat="server" FilterType="Numbers, Custom"
                                ValidChars="." TargetControlID="txtBankTotal" />
                        </td>
                        <td align="left" rowspan="1" width="13%"><span class="field-label">Cash</span></td>
                        <td align="left" rowspan="1" width="20%">
                            <asp:TextBox ID="txtCashTotal" onFocus="this.select();" Style="text-align: right" autocomplete="off" Text="0.00"
                                runat="server" AutoCompleteType="Disabled" onblur="CheckNumber(this);UpdateSum();"
                                TabIndex="60"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtCashTotal" runat="server" FilterType="Numbers, Custom"
                                ValidChars="." TargetControlID="txtCashTotal" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Total Fee</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtTotal" Style="text-align: right" runat="server" Enabled="false" TabIndex="210" onblur="CheckNumber(this);" Text="0.00"></asp:TextBox></td>
                        <td align="left"><span class="field-label">Total Received</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtReceivedTotal" Style="text-align: right" Enabled="false" runat="server" TabIndex="215" onblur="CheckNumber(this);" Text="0.00"></asp:TextBox>
                        </td>
                        <td align="left"><span class="field-label">Balance</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" Enabled="false" TabIndex="220" onblur="CheckNumber(this);" Text="0.00"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1"><span class="field-label">Narration</span></td>
                        <td align="left" colspan="5">
                            <asp:TextBox ID="txtRemarks" runat="server" TabIndex="100" TextMode="MultiLine"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" Visible="false" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" TabIndex="155" Text="Save"
                                OnClientClick="UpdateSum();" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" TabIndex="158" />
                            <asp:CheckBox ID="chkRemember" runat="server" Text="Remember Payment Info" TabIndex="165"
                                Visible="false" />
                            <asp:Button ID="btnNext" runat="server" CausesValidation="False" CssClass="button"
                                Text="Next" TabIndex="160" Visible="false" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
                    Position="Bottom" TargetControlID="ImgMore">
                </ajaxToolkit:PopupControlExtender>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                    PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:Panel ID="pnlCreditCard" runat="server" Style="display: none" CssClass="panel-cover">
                    <table width="100%">
                        <tr class="title-bg">
                            <td align="left" colspan="2">Credit Card          
                     
                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                        OnClientClick="return HideAll();" Style="text-align: right" TabIndex="54" /></td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1">Card Type</td>
                            <td align="left">
                                <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                    DataValueField="CRR_ID" TabIndex="67" SkinID="DropDownListNormal">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" rowspan="1">Authorisation Code</td>
                            <td align="left">
                                <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="68"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left">Machine # </td>
                            <td align="left" colspan="1">
                                <asp:DropDownList ID="ddMachineList" runat="server" DataSourceID="sqlMachineList"
                                    DataTextField="CCM_DESCR" DataValueField="CCM_ID" TabIndex="69">
                                </asp:DropDownList><asp:SqlDataSource ID="sqlMachineList" runat="server" ConnectionString="<%$ ConnectionStrings:OASIS_SERVICESConnectionString %>"
                                    SelectCommand="FEES.F_GETCREDITCARDFORUSER" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter SessionField="sBSUID" Type="String" Name="vCCM_BSU_ID"></asp:SessionParameter>
                                        <asp:ControlParameter PropertyName="SelectedValue" Type="String" Name="vCCM_ALLOC_BSU_ID"
                                            ControlID="ddlBusinessunit"></asp:ControlParameter>
                                        <asp:SessionParameter SessionField="sUsr_name" Type="String" Name="vCCM_USR_ID"></asp:SessionParameter>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                        SelectCommand="exec GetCreditCardListForCollection 'TRANSPORT'"></asp:SqlDataSource>
                </asp:Panel>
                <asp:Panel ID="pnlCheque" runat="server" Style="display: none; left: 600px!important" CssClass="panel-cover">
                    <table width="100%">
                        <tr class="title-bg">
                            <td align="left">Bank</td>
                            <td align="left" id="tdhBA">Bank Account
                            </td>
                            <td align="left">Emirate</td>
                            <td align="left">Cheque No</td>
                            <td align="left">Cheque Date</td>
                            <td align="left" style="display: none">Total</td>
                            <td align="center" style="display: none">
                                <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/close_red.gif"
                                    OnClientClick="return HideAll();" TabIndex="157" /></td>
                        </tr>
                        <tr class="gridheader_pop_orange">
                            <td align="left">
                                <asp:TextBox ID="txtBank" runat="server" AutoPostBack="True" SkinID="TextBoxCollection"
                                    TabIndex="71"></asp:TextBox><asp:ImageButton ID="ImageButton6" runat="server"
                                        ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif" OnClientClick="getBankOrEmirate(1,1); return false"
                                        TabIndex="72" /></td>
                            <td id="tdBA1" align="left">
                                <asp:TextBox ID="txtBankAct1" runat="server" onfocus="this.blur();" TabIndex="73"
                                    SkinID="TextBoxCollection"></asp:TextBox>
                                <asp:ImageButton ID="imgBankAct1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                    OnClientClick="getBank(1);return false" TabIndex="74" /><asp:HiddenField ID="hfBankAct1" runat="server" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlEmirateMain" runat="server" DataSourceID="SqlEmirate_m"
                                    DataTextField="EMR_DESCR" DataValueField="EMR_CODE" SkinID="DropDownListNormal"
                                    TabIndex="75">
                                </asp:DropDownList></td>
                            <td align="left">
                                <asp:TextBox ID="txtChequeNo" runat="server" AutoCompleteType="Disabled" SkinID="TextBoxCollection"
                                    TabIndex="76"></asp:TextBox></td>
                            <td align="left">
                                <asp:TextBox ID="txtChqdate" runat="server" SkinID="TextBoxCollection" TabIndex="77"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"
                                    TabIndex="78" /></td>
                            <td align="center" style="display: none" colspan="2">
                                <asp:LinkButton ID="LinkButton10" runat="server">Apply</asp:LinkButton></td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td colspan="7" align="left">
                                <asp:GridView ID="gvChequeDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                    Width="100%" OnRowDataBound="GridView1_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Bank">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDBank" runat="server" AutoPostBack="True" SkinID="TextBoxCollection"
                                                    TabIndex="71" OnPreRender="txtDBank_PreRender"></asp:TextBox><asp:ImageButton
                                                        ID="imgBank1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                                        TabIndex="75" />
                                                <asp:HiddenField ID="h_Bank" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Emirate">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlEmirate" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                                    DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="78">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cheque#">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChqno" runat="server" AutoCompleteType="Disabled" SkinID="TextBoxCollection"
                                                    TabIndex="80"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChqDate" Text='<%# Bind("DATE") %>' runat="server" SkinID="TextBoxCollection"
                                                    TabIndex="87"></asp:TextBox><asp:ImageButton ID="imgChequedate"
                                                        runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="88" />
                                                <ajaxToolkit:CalendarExtender ID="calCheque" runat="server" CssClass="MyCalendar"
                                                    Enabled="True" Format="dd/MMM/yyyy" PopupButtonID="imgChequedate" PopupPosition="TopLeft"
                                                    TargetControlID="txtChqDate">
                                                </ajaxToolkit:CalendarExtender>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtChequeTotal" Text='<%# AccountFunctions.Round(Container.DataItem("Amount")) %>'
                                                    runat="server" AutoCompleteType="Disabled" onblur="UpdateSum();" onfocus="this.select();"
                                                    SkinID="TextBoxCollection" Style="text-align: right" TabIndex="85"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblBankInvalid" runat="server" EnableViewState="False" SkinID="LabelError"></asp:Label>
                    <asp:HiddenField ID="h_Bank2" runat="server" />
                    <asp:HiddenField ID="h_Bank3" runat="server" />
                    <asp:HiddenField ID="h_Bank1" runat="server" />

                    <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                        SelectCommand="SELECT [EMR_CODE], [EMR_DESCR] FROM [EMIRATE_M]"></asp:SqlDataSource>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="MyCalendar" runat="server"
                        PopupButtonID="ImageButton5" TargetControlID="txtChqdate" Format="dd/MMM/yyyy">
                    </ajaxToolkit:CalendarExtender>
                </asp:Panel>
                <asp:Panel ID="PanelTree" runat="server" CssClass="panel-cover"
                    Style="display: none">
                    <div>
                        <asp:TreeView ID="trMonths" runat="server" onclick="client_OnTreeNodeChecked();"
                            ShowCheckBoxes="all">
                        </asp:TreeView>
                        <div style="text-align: center">
                            <asp:Button ID="btnNarrInsert" runat="server" CssClass="button_small" Text="Fill" />

                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h_BankId" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />
                <asp:HiddenField ID="hf_mode" runat="server" />
                <asp:HiddenField ID="hf_ctrl" runat="server" />
            </div>
        </div>

        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>
