﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="HifzTrackerManage.aspx.vb" Inherits="HifzTracker_ManageHifzTracker" MasterPageFile="~/mainMasterPage.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
      
    </style>
    <script type="text/javascript">
        function loadViewStudentHifzTracker(studentId, transactionId) {            
            var oWnd = radopen("HifzTrackerViewStudent.aspx?transactionId=" + transactionId, "pop_ViewStudentHifzTracker");
        }

        function refreshGrid() {
           <%-- var masterTable = $find("<%=gv_ManageHifzTracker.ClientID%>").get_masterTableView();
            masterTable.rebind();--%>
        }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_ViewStudentHifzTracker" runat="server" Behaviors="Close,Move" OnClientClose="refreshGrid"
                Width="1000px" Height="450px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3 form-horizontal">
        <div class="card-header letter-space">
            <i class="fa fa-file mr-3"></i>Manage Hif'z Tracker
        </div>
     
        <div class="card-body">
              <div class="mb-2">
           <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
       </div>
            <div class="form-group">
                <div class="card">
                    <div class="card-body">
                        <table width="100%">
                            <tr>
                                <td align="left" width="10%">
                                    <span class="field-label">Academic Year</span>
                                </td>
                                <td align="left" width="20%">
                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" CssClass="form-field" AutoComplete="off" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="5%">
                                    <span class="field-label">Grade</span>
                                </td>
                                <td align="left" width="20%">
                                    <asp:DropDownList ID="ddlGrade" runat="server" CssClass="form-field" AutoComplete="off" AutoPostBack="true" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                 <td align="left" width="5%">
                                    <span class="field-label">Section</span>
                                </td>
                                <td align="left" width="15">
                                    <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-field" AutoComplete="off" >
                                    </asp:DropDownList>
                                </td>
                                <td align="left" width="5%">
                                    <span class="field-label">Title</span>
                                </td>
                                <td align="left" width="15%">
                                    <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-field" AutoComplete="off">
                                    </asp:DropDownList>
                                </td>
                                <td class="matters" style="height: 11px" valign="bottom" width="5%">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" OnClick="btnSearch_Click"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div id="divManageHifzTrackerGrid" class="form-group">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <telerik:RadGrid ID="gv_ManageHifzTracker" runat="server" AllowPaging="True"
                                OnNeedDataSource="gv_ManageHifzTracker_NeedDataSource"
                                PageSize="10" AllowSorting="True" CssClass="table table-bordered  table-row" MasterTableView-AllowPaging="true"
                                AutoGenerateColumns="False" CellSpacing="0" GridLine="">
                                <ClientSettings AllowDragToGroup="True">
                                </ClientSettings>
                                <MasterTableView ShowHeader="true" AutoGenerateColumns="False" AllowPaging="true" HierarchyLoadMode="ServerBind">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="StudentName" AllowFiltering="false"
                                            HeaderText="Student Name"
                                            UniqueName="column0">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="StudentUniqueNo" AllowFiltering="false"
                                            HeaderText="Student No."
                                            UniqueName="column0">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="GradeName" AllowFiltering="false"
                                            HeaderText="Grade"
                                            UniqueName="Grade">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SectionName" AllowFiltering="false"
                                            HeaderText="Section"
                                            UniqueName="Section">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AcademicYear" AllowFiltering="false"
                                            HeaderText="Academic Year"
                                            UniqueName="column0">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Title"
                                            HeaderText="Title"
                                            UniqueName="column2">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EnrollmentDate"
                                            HeaderText="Applied On"
                                            UniqueName="column3">
                                        </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="IS_APPROVED"
                                            HeaderText="Is Approved"
                                            UniqueName="column4">
                                        </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="IsFileAvailable"
                                            HeaderText="Is File Available"
                                            UniqueName="column5">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Actions" UniqueName="TemplateColumn" Groupable="False"
                                            AllowFiltering="False">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnView" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/approve.png"
                                                    ToolTip="View Hif'z Tracker" OnClientClick='<%# "loadViewStudentHifzTracker(" + Eval("StudentId").ToString() + "," + Eval("HifzTransactionId").ToString() + ");return false;"%>' />
                                                &nbsp; | &nbsp;
                                    <asp:ImageButton ID="btnDownload" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/save.png"
                                        ToolTip="Download all verses" OnClientClick='<%# "loadViewStudentHifzTracker(" + Eval("StudentId").ToString() + "," + Eval("HifzTransactionId").ToString() + ");return false;"%>'  />
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" Font-Size="12px" />
                                            <ItemStyle />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                <ItemStyle Font-Bold="true" HorizontalAlign="Left" />
                                <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                                <FilterMenu>
                                </FilterMenu>
                                <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                            </telerik:RadGrid>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>





