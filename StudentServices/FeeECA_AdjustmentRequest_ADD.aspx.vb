﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_FeeECA_AdjustmentRequest_ADD
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" And USR_NAME <> "" Then
                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Dim CurBsUnit As String = Session("sBsuid")
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                bindBusinessUnits()
                FillACD()
                bindAdjustmentReason()
                PopulateFeeType()
                txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
                btnEdit.Visible = False
                btnAdd.Visible = False
                btnPrint.Visible = False
                txtHeaderRemarks.Attributes.Add("OnBlur", "FillDetailRemarks()")
                Session("sFEE_ADJUSTMENT") = Nothing

                If ViewState("datamode") = "view" Then
                    Dim FAH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAH_ID").Replace(" ", "+"))
                    Dim vFEE_ADJ As ECAFeeAdjustmentREQ = ECAFeeAdjustmentREQ.GetFeeAdjustments(FAH_ID)

                    txtDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                    txtHeaderRemarks.Text = vFEE_ADJ.FAH_REMARKS
                    radEnq.Checked = vFEE_ADJ.IsEnquiry
                    h_StuID.Value = vFEE_ADJ.FAH_STU_ID
                    'UsrSelStudent1.IsStudent = IIf(vFEE_ADJ.vSTU_TYP = "E", False, True)

                    'UsrSelStudent1.STUDENT_GRADE_ID = vFEE_ADJ.FAH_GRD_ID

                    bindBusinessUnits()

                    ddlBusinessunit.SelectedValue = vFEE_ADJ.STU_BSU_ID
                    'ddlBusinessunit.Items.FindByValue(vFEE_ADJ.STU_BSU_ID).Selected = True
                    BindAcademicYear(Session("sBsuid"))
                    ddlAcademicYear.SelectedValue = vFEE_ADJ.FAH_ACD_ID
                    SetStudentDetails(vFEE_ADJ.FAH_STU_ID)
                    labGrade.Text = "Grade : " & vFEE_ADJ.GRM_DISPLAY
                    ViewState("viewid") = vFEE_ADJ.FAH_ID
                    h_CanEdit.Value = vFEE_ADJ.CanEdit
                    btnEdit.Visible = True
                    PopulateFeeType()
                    If Not ddlAdjReason.Items.FindByValue(vFEE_ADJ.FAR_EVENT) Is Nothing Then
                        ddlAdjReason.SelectedIndex = -1
                        ddlAdjReason.Items.FindByValue(vFEE_ADJ.FAR_EVENT).Selected = True
                    End If
                    SetControls_Head2Head()
                    If vFEE_ADJ.APPR_STATUS <> "N" Then
                        btnEdit.Enabled = False
                    End If
                    Session("sFEE_ADJUSTMENT") = vFEE_ADJ
                    If vFEE_ADJ.FAR_EVENT = 4 Then
                        h_STUD_ID_CR.Value = vFEE_ADJ.FAR_TO_STU_ID
                        h_GRD_ID_CR.Value = vFEE_ADJ.FAR_TO_GRD_ID
                        lblGradeCR.Text = "Grade : " & vFEE_ADJ.GRM_TO_DISPLAY
                        If vFEE_ADJ.FAR_TO_STU_TYP = "S" Then
                            txtStudNameCR.Text = FeeCommon.GetStudentName(vFEE_ADJ.FAR_TO_STU_ID, False)
                            radEnqCR.Checked = False
                            radStudCR.Checked = True
                        Else
                            txtStudNameCR.Text = FeeCommon.GetStudentName(vFEE_ADJ.FAR_TO_STU_ID, True)
                            radEnqCR.Checked = True
                            radStudCR.Checked = False
                        End If
                        FillSubDetailsForHead2Head(vFEE_ADJ)
                    Else
                        GridBindAdjustments()
                    End If
                    DissableAllControls(True)
                End If
                'UsrSelStudent1.SelectedDate = CDate(txtDate.Text)
                lblalert.Text = UtilityObj.getErrorMessage("642")
                SET_VAT_DROPDOWN_RIGHT()
                LOAD_TAX_CODES()
                DISABLE_TAX_FIELDS()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub SET_VAT_DROPDOWN_RIGHT()
        Dim bHasRight As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CASE WHEN COUNT(USER_NAME)>0 THEN 1 ELSE 0 END AS bEXIST FROM OASIS.TAX.VW_VAT_ENABLE_USERS WHERE USER_NAME='" & Session("sUsr_name").ToString & "' AND USER_SOURCE='FEES'"))
        If Not bHasRight Then
            ddlTaxCode.Enabled = False
        End If
    End Sub
    Sub LOAD_TAX_CODES()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT TAX_ID,TAX_CODE,TAX_DESCR FROM TAX.VW_TAX_CODES WHERE ISNULL(TAX_bDeleted,0)=0 ORDER BY TAX_ID")
        ddlTaxCode.DataSource = ds
        ddlTaxCode.DataTextField = "TAX_DESCR"
        ddlTaxCode.DataValueField = "TAX_CODE"
        ddlTaxCode.DataBind()
    End Sub
    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        Dim bShow As Boolean = False
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        tcTAXCode1.Visible = bShow
        tcTAXCode2.Visible = bShow
        tcTAXAmt1.Visible = bShow
        tcTAXAmt2.Visible = bShow
        tcNETAmt1.Visible = bShow
        tcNETAmt2.Visible = bShow
        trTAXalert.Visible = bShow
    End Sub
    Private Sub GET_TAX_DETAIL(ByVal FEEID As Integer, ByVal Amount As Double, ByVal txDate As String)
        lblTAX_AMOUNT.Text = "0.00"
        lblNET_AMOUNT.Text = "0.00"
        If ddlAdjReason.SelectedValue = 11 Or ddlAdjReason.SelectedValue = 13 Then
            ddlTaxCode.SelectedValue = "NA"
        Else
            If IsNumeric(Amount) AndAlso IsDate(txDate) AndAlso FEEID > 0 Then
                'SELECT * FROM OASIS.TAX.GetTAXCodeAndAmount('FEES','500600','FEE','1','30/OCT/2017',500,'')
                Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXCodeAndAmount('FEES','" & Session("sBsuId") & "','FEE','" & FEEID & "','" & txDate & "'," & Amount & ",'')")
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    ddlTaxCode.SelectedValue = ds.Tables(0).Rows(0)("TAX_CODE").ToString
                    lblTAX_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                    lblNET_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
                End If
            End If
        End If

    End Sub
    Private Sub CALCULATE_TAX(ByVal Amount As Double)
        lblTAX_AMOUNT.Text = "0.00"
        lblNET_AMOUNT.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & ddlTaxCode.SelectedValue & "')")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTAX_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                lblNET_AMOUNT.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Private Sub DISABLE_TAX_CALCULATION(ByVal bEnable As Boolean)
        ddlTaxCode.Enabled = bEnable
        ddlTaxCode.SelectedValue = "NA"
        lblTAX_AMOUNT.Text = "0.00"
        lblNET_AMOUNT.Text = "0.00"
    End Sub
    Private Sub PopulateFeeTypeHH(ByVal vGRD_ID As String, ByVal ddlFeeType As DropDownList)
        ddlFeeType.DataSource = ECAFeeAdjustmentREQ.PopulateFeeMaster(h_BSUID.Value, ddlAcademicYear.SelectedValue, vGRD_ID)
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
    End Sub

    Public Sub FillSubDetailsForHead2Head(ByVal vFEE_ADJ As ECAFeeAdjustmentREQ)
        h_GRD_ID_CR.Value = vFEE_ADJ.FAR_TO_GRD_ID
        PopulateFeeTypeHH(h_GRD_ID_CR.Value, ddlFEETYPECR)
        Dim htDetails As Hashtable = vFEE_ADJ.FEE_ADJ_DET
        If htDetails Is Nothing Then
            Exit Sub
        End If
        Dim vFEE_DET As FEEADJUSTMENT_S
        Dim ienum As IDictionaryEnumerator = htDetails.GetEnumerator
        While (ienum.MoveNext())
            If ienum.Value Is Nothing Then Continue While
            vFEE_DET = ienum.Value
            If vFEE_DET.bDelete Then Continue While
            ddlFEETYPECR.SelectedIndex = -1
            ddlFEETYPECR.Items.FindByValue(vFEE_DET.FRS_TO_FEE_ID).Selected = True
            ddlFeeType.SelectedIndex = -1
            ddlFeeType.Items.FindByValue(vFEE_DET.FAD_FEE_ID).Selected = True
            txtDetAmount_HH.Text = vFEE_DET.FAD_AMOUNT
            txtDetRemarks.Text = vFEE_DET.FAD_REMARKS
            Exit While
        End While
    End Sub
    Protected Sub ImgCRStud_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgCRStud.Click

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH
            usrMessageBar2.ShowNotification(OASISConstants.ERRORMSG_SESSIONDOESNOTMATCH, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim vFEE_ADJ As New ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        If ddlAdjReason.SelectedItem.Value <> "4" And ddlAdjReason.SelectedItem.Value <> "13" Then
            If vFEE_ADJ.FEE_ADJ_DET Is Nothing OrElse _
            vFEE_ADJ.FEE_ADJ_DET.Count <= 0 Then
                'lblError.Text = "Please Add atleast one detail..."
                usrMessageBar2.ShowNotification("Please Add atleast one detail...", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If
        End If
        If ddlAdjReason.SelectedItem.Value = "4" Then
            If Me.ddlFeeType.SelectedItem.Value = "145" Or Me.ddlFeeType.SelectedItem.Value = "146" Then
                If Me.ddlAdjReason.SelectedItem.Value <> "11" And Me.ddlAdjReason.SelectedItem.Value <> "13" Then
                    'lblError.Text = "Revenue Adjustment cannot be passed for the selected fee type"
                    usrMessageBar2.ShowNotification("Revenue Adjustment cannot be passed for the selected fee type", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If

            If ddlFEETYPECR.SelectedItem IsNot Nothing Then
                If Me.ddlFEETYPECR.SelectedItem.Value = "145" Or Me.ddlFEETYPECR.SelectedItem.Value = "146" Then
                    If Me.ddlAdjReason.SelectedItem.Value <> "11" And Me.ddlAdjReason.SelectedItem.Value <> "13" Then
                        'lblError.Text = "Revenue Adjustment cannot be passed for the selected fee type"
                        usrMessageBar2.ShowNotification("Revenue Adjustment cannot be passed for the selected fee type", UserControls_usrMessageBar.WarningType.Danger)
                        Exit Sub
                    End If
                End If
            End If
        End If

        Dim conn As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection
        Dim trans As SqlTransaction
        Dim retVal As Integer = 1000
        Dim vNewFAR_ID As String = String.Empty
        Dim bEdit As Boolean = False
        trans = conn.BeginTransaction("sFEE_ADJUSTMENT")
        If ViewState("datamode") = "edit" Then
            bEdit = True
        End If
        Select Case ddlAdjReason.SelectedValue
            Case 4
                retVal = SaveFeeAdjustment_HeadToHead(conn, trans, vNewFAR_ID, bEdit)
            Case 13
                retVal = SaveFeeAdjustment_HeadToHead(conn, trans, vNewFAR_ID, bEdit)
            Case Else
                retVal = SaveFeeAdjustments_Others(conn, trans, vNewFAR_ID, bEdit)
        End Select
        If retVal <> 0 Then
            trans.Rollback()
            'If lblError.Text = "" Then
            '    lblError.Text = UtilityObj.getErrorMessage(retVal)
            'End If
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
        Else
            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") <> "edit" Then
                str_KEY = "EDIT"
            End If
            If chkPrint.Checked Then
                PrintReceiptAdjustmentRequest(vNewFAR_ID)
            End If
            trans.Commit()
            'lblError.Text = UtilityObj.getErrorMessage(retVal)
            usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            ClearDetails()
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, "", str_KEY, Page.User.Identity.Name.ToString, Me.Page)
        End If
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateFeeType()
        'If txtStdNo.Text <> "" AndAlso h_StuID.Value = "" Then
        '    h_StuID.Value = ECA_Fee_Management.GetECAStudentID(txtStdNo.Text, ddlBusinessunit.SelectedValue, False)
        'End If

        'If h_StuID.Value <> "" Then
        '    Dim STUD_ID As Integer = h_StuID.Value.Split("||")(0)
        '    SetStudentDetails(STUD_ID)
        '    ClearEnteredDatas()
        '    PopulateFeeType()
        '    GridBindRefundableDetails()
        'End If
    End Sub

    Public Sub SetStudentDetails(ByVal STU_ID As String)
        Dim str_sql As String = String.Empty

        str_sql = "SELECT STU_ID,STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, " _
        & " isnull(STU_CURRSTATUS,'EN') STU_CURRSTATUS, STU_ACD_ID FROM VW_OSO_STUDENT_M" _
        & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & ddlBusinessunit.SelectedValue & "') AND  STU_ID='" & STU_ID & "'"

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, Data.CommandType.Text, str_sql)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            h_STUD_ID.Value = ds.Tables(0).Rows(0)("STU_ID")
            h_STUD_ID_CR.Value = ds.Tables(0).Rows(0)("STU_ID")
            txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
            'h_GRD_ID_CR.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
            'h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
            'h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
            ''h.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")

        End If
    End Sub


    'Protected Sub UsrSelStudent1_StudentNoChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UsrSelStudent1.StudentNoChanged
    '    h_STUD_ID.Value = UsrSelStudent1.STUDENT_ID
    '    labGrade.Text = "Grade : " & UsrSelStudent1.GRM_DISPLAY
    '    If h_STUD_ID.Value <> "" Then
    '        Dim STUD_ID As Integer = h_STUD_ID.Value.Split("||")(0)
    '        ClearEnteredDatas()
    '        PopulateFeeType()
    '        GridBindRefundableDetails()
    '    End If
    'End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        If h_CanEdit.Value = "False" Then
            'lblError.Text = "Auto Entry Can Not Modify..!"
            usrMessageBar2.ShowNotification("Auto Entry Can Not Modify..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        DissableAllControls(False)
        DissableHeaderPart(True)
        Dim vFEE_ADJ As New ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            'vFEE_ADJ.bEdit = True
        End If
        ddlFeeType_SelectedIndexChanged(ddlFeeType, e)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged

    End Sub

    Protected Sub ddlFeeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFeeType.SelectedIndexChanged
        If ddlAdjReason.SelectedValue = 11 Or ddlAdjReason.SelectedValue = 13 Then ' if Non-Revenue Adjustment/Non-Revenue Transfer Between Heads then TAX no need to be considered
            DISABLE_TAX_CALCULATION(False)
        Else
            txtDetAmount.Text = IIf(IsNumeric(txtDetAmount.Text), txtDetAmount.Text, 0)
            GET_TAX_DETAIL(ddlFeeType.SelectedValue, txtDetAmount.Text, txtDate.Text)
        End If
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            ClearDetails()
            lnkRefundable.Visible = False
            Panel1.Visible = False
            Select Case ddlAdjReason.SelectedValue
                Case "2"
                    lnkRefundable.Visible = True
                    Panel1.Visible = True
                    radStud.Checked = True
                    radEnq.Checked = False
                    radEnq.Enabled = False
                    If h_STUD_ID.Value <> "" Then
                        GridBindRefundableDetails()
                    End If
                Case Else
                    radEnq.Enabled = True
            End Select
            'UsrSelStudent1.ACD_ID = ddlAcademicYear.SelectedValue
            'UsrSelStudent1.IsWithDrawal = IIf(ddlAdjReason.SelectedValue = "2", True, False)
        Catch
        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            Dim vFEE_DET As ECAFEEADJUSTMENT_S = vFEE_ADJ.FEE_ADJ_DET(CInt(lblFEE_ID.Text))
            If Not vFEE_DET Is Nothing Then
                ddlFeeType.SelectedIndex = -1
                ddlFeeType.Items.FindByValue(vFEE_DET.FAD_FEE_ID).Selected = True
                txtDetRemarks.Text = vFEE_DET.FAD_REMARKS

                If Not vFEE_DET.FRS_Description.Equals("0") Then
                    lbDescription.Text = vFEE_DET.FRS_Description
                End If
                'radAdjType.Items.FindByValue(vFEE_DET.AdjustmentType).Selected = True
                'lblTextEntered.Text = radAdjType.SelectedItem.Text
                ddlFeeType_SelectedIndexChanged(ddlFeeType, e)
                If vFEE_DET.AdjustmentType = 0 Then

                    txtDetAmount.Text = vFEE_DET.FAD_AMOUNT
                Else
                    txtDetAmount.Text = vFEE_DET.DurationCount
                End If
                ViewState("Eid") = CInt(lblFEE_ID.Text)
                btnDetAdd.Text = "Save"
            End If
        End If

    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vFEE_ADJ As New ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
        End If
        Dim lblFEE_ID As New Label
        lblFEE_ID = TryCast(sender.parent.FindControl("lblFEE_ID"), Label)
        If Not lblFEE_ID Is Nothing Then
            vFEE_ADJ.FEE_ADJ_DET.Remove(CInt(lblFEE_ID.Text))
        End If
        Session("sFEE_ADJUSTMENT") = vFEE_ADJ
        GridBindAdjustments()
    End Sub

    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubCancel.Click
        ClearSubDetails()
    End Sub

    Protected Sub btnDetAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetAdd.Click
        If IsNumeric(txtDetAmount.Text) AndAlso CDbl(txtDetAmount.Text) <> 0 Then
            Dim vFEE_ADJ As New ECAFeeAdjustmentREQ
            If CDec(txtDetAmount.Text) < 0 And ddlAdjReason.SelectedItem.Value <> "6" And ddlAdjReason.SelectedItem.Value <> "11" Then
                'lblError.Text = "Amount must be positive!!!"
                usrMessageBar2.ShowNotification("Amount must be positive!!!", UserControls_usrMessageBar.WarningType.Danger)
                Exit Sub
            End If

            'Start - Validation for Fee Type and Reason - Added by Hashmi - 7th Feb 2013
            If Me.ddlFeeType.SelectedItem.Value = "145" Or Me.ddlFeeType.SelectedItem.Value = "146" Then
                If Me.ddlAdjReason.SelectedItem.Value <> "11" And Me.ddlAdjReason.SelectedItem.Value <> "13" Then
                    'lblError.Text = "Revenue Adjustment cannot be passed for the selected fee type"
                    usrMessageBar2.ShowNotification("Revenue Adjustment cannot be passed for the selected fee type", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If

            'End

            hf_GRD_ID.Value = vFEE_ADJ.FAH_GRD_ID
            If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
                vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            End If
            vFEE_ADJ.FEE_ADJ_DET = AddDetails(vFEE_ADJ.FEE_ADJ_DET)
            Session("sFEE_ADJUSTMENT") = vFEE_ADJ
            GridBindAdjustments()
        Else
            'lblError.Text = "Invalid Amount!!!"
            usrMessageBar2.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearDetails()
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Sub ClearEnteredDatas()
        ClearSubDetails()
        Session("sFEE_ADJUSTMENT") = Nothing
        GridBindAdjustments()
    End Sub

    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()



        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter


        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddlBusinessunit.DataSource = ds.Tables(0)
            ddlBusinessunit.DataTextField = "BSU_NAME"
            ddlBusinessunit.DataValueField = "BSU_ID"
            ddlBusinessunit.DataBind()
            ddlBusinessunit.SelectedIndex = -1
            ddlBusinessunit.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlBusinessunit_SelectedIndexChanged(ddlBusinessunit, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub

    Private Sub PopulateFeeType()
        Dim httabFPM_ID As New Hashtable
        Dim stID As Integer = 0
        If h_StuID.Value <> "" Then
            stID = Convert.ToInt32(h_StuID.Value)
        End If

        ddlFeeType.DataSource = ECAFeeAdjustmentREQ.PopulateFeeMaster(Session("sbsuid"), ddlAcademicYear.SelectedValue, stID, h_GRD_ID_CR.Value, ddlAdjReason.SelectedItem.Value, httabFPM_ID)

        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
        If (Not ddlFeeType.DataSource Is Nothing) AndAlso _
        ddlFeeType.Items.Count > 0 Then
            ddlFeeType_SelectedIndexChanged(Nothing, Nothing)
        End If
        Session("FPM_IDs") = httabFPM_ID
    End Sub

    Protected Sub PrintReceiptAdjustmentRequest(ByVal vFAR_ID As Integer)
        Dim IsStudent As Boolean = True
        If radEnq.Checked Then
            IsStudent = False
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETFEEADJUSTMENTREQUESTFORVOUCHER]")
        cmd.CommandType = CommandType.StoredProcedure
        Dim sqlpFAR_ID As New SqlParameter("@FAR_ID", SqlDbType.Int)
        sqlpFAR_ID.Value = vFAR_ID
        cmd.Parameters.Add(sqlpFAR_ID)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.Int)
        sqlpSTU_TYPE.Value = IsStudent
        cmd.Parameters.Add(sqlpSTU_TYPE)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("RPT_CAPTION") = "FEE ADJUSTMENT REQUEST"
        params("UserName") = Session("sUsr_name")
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If ddlAdjReason.SelectedItem.Value = "4" Or ddlAdjReason.SelectedItem.Value = "13" Then
            repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentRequestVoucher_inter.rpt"
        Else
            repSource.ResourceName = "../../fees/Reports/RPT/rptFEEAdjustmentRequestVoucher.rpt"
        End If

        Session("ReportSource") = repSource
        h_print.Value = "print"
    End Sub

    Private Sub DissableHeaderPart(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        imgDate.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        'UsrSelStudent1.IsReadOnly = dissble
        ddlAdjReason.Enabled = Not dissble
    End Sub

    Private Function AddDetails(ByVal htFEE_DET As Hashtable) As Hashtable
        Dim vFEE_DET As New ECAFEEADJUSTMENT_S
        Dim eId As Integer = -1
        If btnDetAdd.Text = "Save" Then
            vFEE_DET = htFEE_DET(ViewState("Eid"))
            htFEE_DET.Remove(ViewState("Eid"))
            eId = ViewState("Eid")
            'If ViewState("datamode") = "edit" Then
            '    vFEE_DET.bEdit = True
            'End If
        End If
        If ddlFeeType.SelectedValue = "" Then
            'lblError.Text = "Invalid Fee type..! "
            usrMessageBar2.ShowNotification("Invalid Fee type..! ", UserControls_usrMessageBar.WarningType.Danger)
            Return Nothing
        End If

        Dim selFeeTyp As Integer = ddlFeeType.SelectedValue
        If Not htFEE_DET(CInt(selFeeTyp)) Is Nothing Then
            'lblError.Text = "The Fee type is repeating..."
            usrMessageBar2.ShowNotification("The Fee type is repeating...", UserControls_usrMessageBar.WarningType.Danger)
            Return htFEE_DET
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        ElseIf ECAFeeAdjustmentREQ.IsRepeated(vFEE_DET.FAD_FAH_ID, h_StuID.Value, _
        ddlAcademicYear.SelectedValue, h_GRD_ID_CR.Value, Session("sBsuid")) Then

            'lblError.Text = "There is already approval pending for " & ddlFeeType.SelectedItem.Text
            usrMessageBar2.ShowNotification("There is already approval pending for " & ddlFeeType.SelectedItem.Text, UserControls_usrMessageBar.WarningType.Danger)
            Return htFEE_DET
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
        Else
            vFEE_DET.FAD_AMOUNT = CDbl(txtDetAmount.Text)
        End If
        If vFEE_DET.FAD_AMOUNT = 0 Then
            If eId <> -1 Then htFEE_DET(eId) = vFEE_DET
            Return htFEE_DET
        End If
        Dim TaxAmt As Double = 0, NetAmt As Double = 0
        If tcTAXAmt1.Visible AndAlso IsNumeric(lblTAX_AMOUNT.Text) Then
            TaxAmt = CDbl(lblTAX_AMOUNT.Text)
        End If
        If tcNETAmt1.Visible AndAlso IsNumeric(lblNET_AMOUNT.Text) Then
            NetAmt = CDbl(lblNET_AMOUNT.Text)
        End If
        vFEE_DET.FEE_TYPE = ddlFeeType.SelectedItem.Text
        vFEE_DET.FAD_FEE_ID = selFeeTyp
        vFEE_DET.FRS_Description = "0"
        vFEE_DET.FAD_TAX_CODE = ddlTaxCode.SelectedValue
        vFEE_DET.FAD_TAX_AMOUNT = TaxAmt
        vFEE_DET.FAD_NET_AMOUNT = NetAmt
        vFEE_DET.FAD_REMARKS = txtDetRemarks.Text
        htFEE_DET(vFEE_DET.FAD_FEE_ID) = vFEE_DET
        ClearSubDetails()
        'End If
        Return htFEE_DET
    End Function

    Private Sub DissableAllControls(ByVal dissble As Boolean)
        txtDate.ReadOnly = dissble
        txtHeaderRemarks.ReadOnly = dissble
        txtDetRemarks.ReadOnly = dissble
        txtDetAmount.ReadOnly = dissble
        ddlFeeType.Enabled = Not dissble
        ddlAcademicYear.Enabled = Not dissble
        ddlAdjReason.Enabled = Not dissble
        btnDetAdd.Enabled = Not dissble
        imgDate.Enabled = Not dissble
        CalendarExtender1.Enabled = Not dissble
        calendarButtonExtender.Enabled = Not dissble
        gvFeeDetails.Columns(8).Visible = Not dissble
        'UsrSelStudent1.IsReadOnly = dissble
    End Sub

    Private Function SaveFeeAdjustment_HeadToHead(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, _
    ByRef vNewFAR_ID As String, ByVal bEdit As Boolean) As String
        If h_StuID.Value = h_StuID.Value AndAlso _
        h_GRD_ID_CR.Value = h_GRD_ID_CR.Value AndAlso _
        ddlFEETYPECR.SelectedValue = ddlFeeType.SelectedValue Then
            Return 919 'Fee Transfering to the same head is not allowed..
        End If
        If Not IsNumeric(txtDetAmount_HH.Text) Then
            Return 922 'Invalid Amount!!!
        End If
        'If Math.Abs(CDbl(txtDetAmount_HH.Text)) > Math.Abs(CDbl(lblBalanceAmt.Text)) Then
        '    Return 920 'Transfering amount is greater than Balance amount
        'End If

        'If CDbl(lblBalanceAmt.Text) >= 0 Then
        '    Return 922 'Invalid Amount!!!
        'End If

        If CDbl(txtDetAmount_HH.Text) < 0 Then
            Return 922 'Invalid Amount!!!
        End If

        Dim vFEE_ADJ As New ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            'vFEE_ADJ.bEdit = True
        End If
        vFEE_ADJ.FAH_ACD_ID = ddlAcademicYear.SelectedValue
        vFEE_ADJ.FAH_BSU_ID = Session("sBSUID")
        vFEE_ADJ.FAH_STU_ID = h_STUD_ID.Value
        'vFEE_ADJ.FAH_GRD_ID = UsrSelStudent1.STUDENT_GRADE_ID
        vFEE_ADJ.FAR_TO_STU_ID = h_STUD_ID_CR.Value
        vFEE_ADJ.FAR_TO_GRD_ID = h_GRD_ID_CR.Value
        vFEE_ADJ.FAH_REMARKS = txtDetRemarks.Text
        vFEE_ADJ.FAR_EVENT = ddlAdjReason.SelectedItem.Value
        vFEE_ADJ.CanEdit = True
        'vFEE_ADJ.ADJUSTMENTTYPE = 1
        vFEE_ADJ.IsEnquiry = radEnq.Checked
        If radEnqCR.Checked Then
            vFEE_ADJ.FAR_TO_STU_TYP = "E"
        ElseIf radStudCR.Checked Then
            vFEE_ADJ.FAR_TO_STU_TYP = "S"
        End If
        vFEE_ADJ.FAH_DATE = CDate(txtDate.Text)

        Dim vFEE_DET As New ECAFEEADJUSTMENT_S
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            Dim ienum As IDictionaryEnumerator = vFEE_ADJ.FEE_ADJ_DET.GetEnumerator
            While (ienum.MoveNext())
                vFEE_DET = ienum.Value
                'vFEE_DET.bEdit = True
                Exit While
            End While
        End If
        Dim eId As Integer = -1
        vFEE_DET.AdjustmentType = 1
        vFEE_DET.FAD_AMOUNT = CDbl(txtDetAmount_HH.Text)
        vFEE_DET.FEE_TYPE = ddlFeeType.SelectedItem.Text
        vFEE_DET.FAD_FEE_ID = ddlFeeType.SelectedValue
        vFEE_DET.TO_FEE_TYPE = ddlFEETYPECR.SelectedItem.Text
        vFEE_DET.FRS_TO_FEE_ID = ddlFEETYPECR.SelectedValue
        vFEE_DET.FAD_REMARKS = txtDetRemarks.Text
        If ddlAdjReason.SelectedValue = "11" Or ddlAdjReason.SelectedValue = "13" Then 'if non revenue or non revenue between heads adjustments
            vFEE_DET.FAD_TAX_CODE = "NA"
            vFEE_DET.FAD_TAX_AMOUNT = 0
            vFEE_DET.FAD_NET_AMOUNT = vFEE_DET.FAD_AMOUNT
        Else
            vFEE_DET.FAD_TAX_CODE = ddlTaxCode.SelectedValue
            vFEE_DET.FAD_TAX_AMOUNT = lblTAX_AMOUNT.Text
            vFEE_DET.FAD_NET_AMOUNT = lblNET_AMOUNT.Text
        End If
        Dim httab As New Hashtable
        httab(vFEE_DET.FAD_FEE_ID) = vFEE_DET
        vFEE_ADJ.FEE_ADJ_DET = httab
        vFEE_ADJ.FAR_USER = Session("sUsr_name")
        Return ECAFeeAdjustmentREQ.F_SAVEFEEADJREQUEST_H(vFEE_ADJ, vNewFAR_ID, conn, bEdit, trans)
    End Function

    Private Function SaveFeeAdjustments_Others(ByVal conn As SqlConnection, ByVal trans As SqlTransaction, _
    ByRef vNewFAR_ID As String, ByVal bEdit As Boolean) As String
        Dim vFEE_ADJ_REQ As ECAFeeAdjustmentREQ
        Dim retVal As Integer = -1
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ_REQ = Session("sFEE_ADJUSTMENT")
            vFEE_ADJ_REQ.FAH_ACD_ID = ddlAcademicYear.SelectedValue
            vFEE_ADJ_REQ.IsEnquiry = radEnq.Checked
            vFEE_ADJ_REQ.FAH_BSU_ID = Session("sBSUID")
            vFEE_ADJ_REQ.FAH_STU_ID = h_StuID.Value
            vFEE_ADJ_REQ.FAH_GRD_ID = h_GRD_ID_CR.Value
            vFEE_ADJ_REQ.FAH_DATE = CDate(txtDate.Text)
            vFEE_ADJ_REQ.FAH_REMARKS = txtHeaderRemarks.Text
            vFEE_ADJ_REQ.FAR_EVENT = ddlAdjReason.SelectedItem.Value
            vFEE_ADJ_REQ.CanEdit = True
            vFEE_ADJ_REQ.FAR_USER = Session("sUsr_name")
            retVal = ECAFeeAdjustmentREQ.F_SAVEFEEADJREQUEST_H(vFEE_ADJ_REQ, vNewFAR_ID, _
            conn, bEdit, trans)
        End If
        Return retVal
    End Function

    Private Sub GridBindRefundableDetails()
        Dim stuTYPE As String = "S"
        If radEnq.Checked Then
            stuTYPE = "E"
        End If

        Dim str_sql As String = "SELECT SUM(CASE DRCR WHEN 'DR' THEN AMOUNT ELSE 0 END) PAID_AMT , " & _
        " SUM(CASE DRCR WHEN 'CR' THEN AMOUNT ELSE 0 END) CHARGED_AMT ," & _
        " DOCDATE, FEE_DESCR FROM FEES.[VW_AlLSTUDENTSDATA]  AS fn_STUDENTLEDGERALL_1 " & _
        " WHERE STU_ID = " & h_StuID.Value & " AND STU_TYPE = '" & stuTYPE & "' " & _
        " GROUP BY DOCDATE, FEE_DESCR, STU_ID"
        gvFeePaidHistory.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, str_sql)
        gvFeePaidHistory.DataBind()
    End Sub

    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            gvFeeDetails.DataSource = ECAFeeAdjustmentREQ.GetSubDetailsAsDataTable(vFEE_ADJ)
            gvFeeDetails.DataBind()
        Else
            gvFeeDetails.DataSource = Nothing
            gvFeeDetails.DataBind()
        End If
    End Sub

    Protected Sub ddlAdjReason_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAdjReason.SelectedIndexChanged
        If ViewState("datamode") <> "edit" Then
            ClearDetails()
        End If
        If ddlAdjReason.SelectedValue = 11 Or ddlAdjReason.SelectedValue = 13 Then ' if Non-Revenue Adjustment/Non-Revenue Transfer Between Heads then TAX no need to be considered
            DISABLE_TAX_CALCULATION(False)
        End If
        ''  SetControls_Head2Head()
    End Sub

    Private Sub ClearDetails()
        h_BSUID.Value = Session("sBSUID")
        h_STUD_ID.Value = ""
        txtDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        txtHeaderRemarks.Text = ""
        ddlFeeType.ClearSelection()
        Session("sFEE_ADJUSTMENT") = Nothing
        ClearSubDetails()
        GridBindAdjustments()
        'UsrSelStudent1.ClearDetails()
        ddlFeeType.DataSource = Nothing
        ddlFeeType.DataBind()
        h_CanEdit.Value = ""
        labGrade.Text = ""
    End Sub

    Sub FillACD()
        Dim dtACD As DataTable = ECA_Fee_Management.GetECABSUAcademicYear(Session("sBsuid"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
        '' Set_Previous_Acd()
    End Sub

    Sub BindAcademicYear(ByVal bsuId As String)
        Dim sql_query As String = " EXEC FEES.GetBSUAcademicYear '" & bsuId & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            ddlAcademicYear.DataSource = dsData
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()

            If Not Session("Current_ACD_ID") Is Nothing Then
                If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                    ddlAcademicYear.ClearSelection()
                    ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            End If
        Else

        End If

    End Sub

    Sub bindAdjustmentReason()
        ddlAdjReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.Get_Adjustment_Reason")



        ddlAdjReason.DataSource = ds
        ddlAdjReason.DataTextField = "ARM_DESCR"
        ddlAdjReason.DataValueField = "ARM_ID"
        ddlAdjReason.DataBind()

        If (ds.Tables(0).Rows.Count > 0) Then
            ddlAdjReason.Items.FindByText("Adjustment").Selected = True
            'ddlAdjReason.Enabled = False
        End If
        'ddlAdjReason.Items.Insert(0, New ListItem("-Select Reason-", "0"))

    End Sub


    Private Sub ClearSubDetails()
        'radAdjType.SelectedIndex = 0
        txtDetAmount.Text = "0.00"
        txtDetRemarks.Text = ""
        ddlFeeType.SelectedIndex = -1
        btnDetAdd.Text = "Add"
        lblBalanceAmt.Text = "0.00"
        txtDetAmount_HH.Text = "0.00"
        txtStudNameCR.Text = ""
        lblGradeCR.Text = ""
        h_STUD_ID_CR.Value = ""
        lbDescription.Text = ""
        ddlFeeType_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Sub SetControls_Head2Head()
        If ddlAdjReason.SelectedItem.Value = "4" Or ddlAdjReason.SelectedItem.Value = "13" Then 'Transfer Between Heads
            lblBalance.Visible = True
            txtDetAmount.Enabled = False
            tr_ToStudent1.Visible = True
            tr_ToStudent2.Visible = True
            tr_ToStudent3.Visible = True
            tr_Addbutton.Visible = True
            gvFeeDetails.Visible = False
            tr_Addbutton.Visible = False
            'tr_Deduction.Visible = False
        Else
            lblBalance.Visible = False
            txtDetAmount.Enabled = True
            tr_ToStudent1.Visible = False
            tr_ToStudent2.Visible = False
            tr_ToStudent3.Visible = False
            tr_Addbutton.Visible = False
            gvFeeDetails.Visible = True
            tr_Addbutton.Visible = True
            'tr_Deduction.Visible = True
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        PopulateFeeType()
    End Sub

 
    Protected Sub ddlTaxCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTaxCode.SelectedIndexChanged
        txtDetAmount.Text = IIf(IsNumeric(txtDetAmount.Text), txtDetAmount.Text, 0)
        CALCULATE_TAX(txtDetAmount.Text)
    End Sub

    Protected Sub txtDetAmount_TextChanged(sender As Object, e As EventArgs) Handles txtDetAmount.TextChanged
        txtDetAmount.Text = IIf(IsNumeric(txtDetAmount.Text), txtDetAmount.Text, 0)
        CALCULATE_TAX(txtDetAmount.Text)
    End Sub
End Class
