﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="HifzTrackerViewStudent.aspx.vb" Inherits="HifzTracker_ViewStudentHifzTracker" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title id="popupTitle" runat="server">View Student Hif'z Tracker</title>
    <link type="text/css" href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
    <style>
        .error {
            font-family: Raleway,sans-serif;
            font-size: 7pt;
            font-weight: normal;
        }

        .pull-right {
            float: right;
        }

        .hide-field {
            display: none;
        }

        .button.field-disabled {
            cursor: not-allowed;
        }        

        div.RadGrid {          
                min-height: 225px;
        }
    </style>
    <!-- Bootstrap core JavaScript-->
    <script language="javascript" type="text/javascript" src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


</head>
<body onload="listen_window();">

    <form id="frmStudentHifzTracker" runat="server">
        <asp:HiddenField ID="hdnHifzTransactionId" runat="server" />
        <asp:HiddenField ID="hdnStudentId" runat="server" />
        <asp:ScriptManager runat="server" ID="sm1"></asp:ScriptManager>
        <div class="card mb-3">
            <div class="card-body">
                <div id="divSuccess" runat="server" class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong> Your data has been updated successfully.
                </div>
                <div id="divError" runat="server" class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> A problem has been occurred while submitting your data.
                </div>
                <div id="divViewHifzTransaction" class="table-responsive">

                    <telerik:RadGrid ID="tbl_StudentHifzTracker" runat="server" AllowPaging="True"
                        OnNeedDataSource="tbl_StudentHifzTracker_NeedDataSource"
                        PageSize="5" AllowSorting="True" CssClass="table table-bordered  table-row"
                        AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False" GridLines="None">
                     

                        <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="HifzTransactionId,HifzRelationId" AllowPaging="true" HierarchyLoadMode="ServerBind">
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="" UniqueName="TemplateColumn0" Groupable="False"
                                    AllowFiltering="False">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkVerse" CssClass="check-verse" runat="server" Value='<%#Eval("HifzRelationId")%>' Checked='<%# (Convert.ToBoolean(Eval("IsReviewed")))%>' Enabled='<%# (Convert.ToBoolean(Eval("IsEnabled")))%>' />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" Font-Size="12px" />
                                    <ItemStyle />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CompletedVerses" AllowFiltering="false"
                                    HeaderText="Verses"
                                    UniqueName="column0">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Completed" AllowFiltering="false"
                                    HeaderText="Completed"
                                    UniqueName="Grade">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Reviewed"
                                    HeaderText="Reviewed"
                                    UniqueName="column2">
                                </telerik:GridBoundColumn>

                                 <telerik:GridTemplateColumn HeaderText="Comment" UniqueName="comment" Groupable="False"
                                    AllowFiltering="False">
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="2" Width="90%" Text='<%#Eval("comment")%>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" Font-Size="12px" />
                                    <ItemStyle />
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn HeaderText="Actions" UniqueName="TemplateColumn1" Groupable="False"
                                    AllowFiltering="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnSave" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/save.png"
                                            ToolTip="Download Audio File" OnClick="btnSave_Click" CommandArgument='<%#Eval("UploadedFileName")%>' />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" Font-Size="12px" />
                                    <ItemStyle />
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                        <ItemStyle Font-Bold="true" HorizontalAlign="Left" />
                        <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                        <FilterMenu>
                        </FilterMenu>
                        <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                    </telerik:RadGrid>
                </div>
            </div>
            <div class="card-footer">
                <input type="button" class="button pull-left" id="btnCancel" title="Cancel" value="Close" onclick="fancyClose();" />
                <asp:Button ID="btnApprove" Text="Approve" runat="server" CssClass="button pull-right" OnClick="btnApprove_Click" />
                <asp:Button ID="btnEnableCerificate" Text="Enable Cerificate" runat="server" CssClass="button pull-right" OnClick="btnEnableCerificate_Click" />
            </div>
        </div>
    </form>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $(document).on('change', '.check-verse', function () {
                $('#<%= btnApprove.ClientID()%>').toggle(isApprovalEnabled());
            });

            $('#<%= btnApprove.ClientID()%>').toggle(isApprovalEnabled());
        });


        function fancyClose() {
            window.close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function CloseAddEditHifzTrackerFormModal() {
            setTimeout(function () {
                GetRadWindow().close(); // Close the window 
            }, 0);
        }

        function isApprovalEnabled() {
            var result = false;
            $('#<%=tbl_StudentHifzTracker.ClientID%>').find('.check-verse').each(function (idx, item) {
                var verseCheckBox = $(this).find('input');
                if (verseCheckBox.is(':checked') && !verseCheckBox.prop('disabled')) result = true;
                console.log($(this))
            });
            return result
        }
    </script>
</body>
</html>
