﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Imports System.Windows.Forms.MessageBox
Partial Class StudentServices_Services_CreateActivitySubTypes
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property

    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                divSubActivity.Visible = False
                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    lblError.Text = msgText
                End If
                gridbind()
                Clear()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("From Load: " + ex.Message + "Create ACTIVITY Sub Types")
        End Try
    End Sub

    Protected Sub Clear()
        Try
            txtSubDecsr.Text = ""
            txtSubName.Text = ""
            fillActivityTypes()
            fillFeeTypes()
            GetCountryDtls()
            PanelTitle.InnerText = "Add New Activity Sub Type"
            btnSaveSub.Text = "Add"
            ddlActiTypes.Enabled = True
            hdnfATM_ID.Value = 0
            divSubActivity.Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog("From Clear : " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub

    Protected Sub fillActivityTypes()
        'FILLING ACTIVITY TYPES TO ACTIVITY DROP DOWN LIST
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_ACTIVITY_TYPES]")
            ddlActiTypes.DataSource = dsActivityTypes.Tables(0)
            ddlActiTypes.DataBind()
            ddlActiTypes.DataTextField = "ACTIVITY_NAME"
            ddlActiTypes.DataValueField = "ACTIVITY_ID"
            ddlActiTypes.DataBind()
            ddlActiTypes.Items.Insert(0, New ListItem("SELECT", 0))
            ddlActiTypes.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog("From fillActivityTypes: " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub
    Private Sub fillFeeTypes()
        'METHODE TO FILL FEE TYPES
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(1) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@CheckOption", 1, SqlDbType.Int)
            Dim dsFeeTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_REGISTRATION_FEE_DETAILS]", Param)
            ddlFeeCode.DataSource = dsFeeTypes.Tables(0)
            ddlFeeCode.DataTextField = "DESCR"
            ddlFeeCode.DataValueField = "ID"
            ddlFeeCode.DataBind()
            ddlFeeCode.Items.Insert(0, New ListItem("SELECT", 0))
            ddlFeeCode.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog("From FillFeetypes: " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub

    Public Sub GetCountryDtls()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            str_Sql = "Select ID, C_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as C_Name FROM COUNTRY_M)a where a.ID<>'' and C_Name <> '-' and a.ID not in (0,5,252)"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlCountry.DataSource = ds.Tables(0)
            ddlCountry.DataTextField = "C_Name"
            ddlCountry.DataValueField = "ID"
            ddlCountry.DataBind()
            ddlCountry.Items.Insert(0, New ListItem("SELECT", 0))
            ddlCountry.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog("From GetCountryDtls: " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub

    Private Sub gridbind()
        'METHODE TO FILL FEE TYPES
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As DataSet
            ViewState("txtCol1") = ""
            ViewState("txtCol2") = ""
            ViewState("txtCol3") = ""
            ViewState("txtCol4") = ""
            ViewState("txtCol5") = ""
            If gvDetails.Rows.Count > 0 Then
                Dim header As GridViewRow = gvDetails.HeaderRow
                Dim txtCol1 As TextBox = TryCast(header.FindControl("txtCol1"), TextBox)
                Dim txtCol2 As TextBox = TryCast(header.FindControl("txtCol2"), TextBox)
                Dim txtCol3 As TextBox = TryCast(header.FindControl("txtCol3"), TextBox)
                Dim txtCol4 As TextBox = TryCast(header.FindControl("txtCol4"), TextBox)
                Dim txtCol5 As TextBox = TryCast(header.FindControl("txtCol5"), TextBox)
                ViewState("txtCol1") = txtCol1.Text
                ViewState("txtCol2") = txtCol2.Text
                ViewState("txtCol3") = txtCol3.Text
                ViewState("txtCol4") = txtCol4.Text
                ViewState("txtCol5") = txtCol5.Text
                Dim Param(4) As SqlParameter
                Param(0) = Mainclass.CreateSqlParameter("@SubtypeName", txtCol1.Text, SqlDbType.VarChar)
                Param(1) = Mainclass.CreateSqlParameter("@SubtypeDescr", txtCol2.Text, SqlDbType.VarChar)
                Param(2) = Mainclass.CreateSqlParameter("@FeeDescr", txtCol3.Text, SqlDbType.VarChar)
                Param(3) = Mainclass.CreateSqlParameter("@ActivityType", txtCol4.Text, SqlDbType.VarChar)
                Param(4) = Mainclass.CreateSqlParameter("@Country", txtCol5.Text, SqlDbType.VarChar)
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "OASIS.GET_SUB_ACTVITY_TYPES", Param)
            Else ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "OASIS.GET_SUB_ACTVITY_TYPES")
            End If
            If ds.Tables.Count > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                gvDetails.DataSource = ds.Tables(0)
                gvDetails.DataBind()
                'If gvDetails.Rows.Count > 0 Then
                Dim header As GridViewRow = gvDetails.HeaderRow
                Dim txtCol1 As TextBox = TryCast(header.FindControl("txtCol1"), TextBox)
                Dim txtCol2 As TextBox = TryCast(header.FindControl("txtCol2"), TextBox)
                Dim txtCol3 As TextBox = TryCast(header.FindControl("txtCol3"), TextBox)
                Dim txtCol4 As TextBox = TryCast(header.FindControl("txtCol4"), TextBox)
                Dim txtCol5 As TextBox = TryCast(header.FindControl("txtCol5"), TextBox)
                txtCol1.Text = ViewState("txtCol1")
                txtCol2.Text = ViewState("txtCol2")
                txtCol3.Text = ViewState("txtCol3")
                txtCol4.Text = ViewState("txtCol4")
                txtCol5.Text = ViewState("txtCol5")
                'End If
                'End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("From gridbind: " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSaveSub_Click(sender As Object, e As EventArgs)
        'OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES
        If txtSubName.Text.Trim() = "" Then
            lblError.Text = "Please enter all mandatory fileds."
            Exit Sub
        ElseIf txtSubDecsr.Text.Trim() = "" Then
            lblError.Text = "Please enter all mandatory fileds."
            Exit Sub
        ElseIf ddlActiTypes.SelectedValue = 0 Then
            lblError.Text = "Please enter all mandatory fileds."
            Exit Sub
        ElseIf ddlFeeCode.SelectedValue = 0 Then
            lblError.Text = "Please enter all mandatory fileds."
            Exit Sub
        ElseIf ddlCountry.SelectedValue = 0 Then
            lblError.Text = "Please enter all mandatory fileds."
            Exit Sub
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim strans As SqlTransaction = Nothing
            Try
                objConn.Open()
                strans = objConn.BeginTransaction
                Dim pParams(8) As SqlParameter
                pParams(0) = New SqlClient.SqlParameter("@ATM_NAME", txtSubName.Text)
                pParams(1) = New SqlClient.SqlParameter("@ATM_DESCR", txtSubDecsr.Text)
                pParams(2) = New SqlClient.SqlParameter("@ATM_FEE_CODE", ddlFeeCode.SelectedValue)
                pParams(3) = New SqlClient.SqlParameter("@ATM_REF_ID", ddlActiTypes.SelectedValue)
                pParams(4) = New SqlClient.SqlParameter("@ATM_ID", IIf(hdnfATM_ID.Value = 0, 0, hdnfATM_ID.Value))
                pParams(5) = New SqlClient.SqlParameter("@USER", EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name")))
                pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", ddlCountry.SelectedValue)
                pParams(7) = New SqlClient.SqlParameter("@TYPE", IIf(hdnfATM_ID.Value = 0, "I", "U"))
                pParams(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParams(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES", pParams)
                If (pParams(8).Value = 0) Then
                    strans.Commit()
                    gridbind()
                    lblError.Text = IIf(hdnfATM_ID.Value = 0, "Record Added Successfully", "Record Updated Successfully")
                    Clear()
                Else
                    strans.Rollback()
                    lblError.Text = "Error Occured during processing"
                End If

            Catch ex As Exception
                strans.Rollback()
                lblError.Text = ex.Message
                UtilityObj.Errorlog("From btnSaveSub: " + ex.Message, "Create ACTIVITY Sub Types")
            End Try
        End If
    End Sub
    Protected Sub gvDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Dim hfALD_ATM_ID As HiddenField
        hfALD_ATM_ID = TryCast(e.Row.FindControl("hdnALD_ATM_ID"), HiddenField)
        Dim hlEdit As New LinkButton
        hlEdit = TryCast(e.Row.FindControl("hlEdit"), LinkButton)
        Dim hlDelete As New LinkButton
        hlDelete = TryCast(e.Row.FindControl("hlDelete"), LinkButton)
        If hfALD_ATM_ID IsNot Nothing Then
            If hfALD_ATM_ID.Value <> "" Then
                hlEdit.Visible = False
                hlDelete.Visible = False
            End If
        End If
    End Sub
    Protected Sub gvDetails_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim Row As GridViewRow = gvDetails.SelectedRow
        Dim hfALD_ATM_ID As Label
        hfALD_ATM_ID = TryCast(Row.FindControl("lblATMid"), Label)
    End Sub
    Protected Sub hlEdit_Click(sender As Object, e As EventArgs)
        Try
            Dim RowId As Integer = (CType((CType(sender, LinkButton)).Parent.Parent, GridViewRow)).RowIndex
            Dim selectedRow As GridViewRow = DirectCast(gvDetails.Rows(RowId), GridViewRow)
            Dim hfALD_ATM_ID As Label
            hfALD_ATM_ID = TryCast(selectedRow.FindControl("lblATMid"), Label)
            Dim lblgrdactname As Label
            lblgrdactname = TryCast(selectedRow.FindControl("lblgrdactname"), Label)
            Dim lblgrdactdescr As Label
            lblgrdactdescr = TryCast(selectedRow.FindControl("lblgrdactdescr"), Label)
            Dim hidFeecode As HiddenField
            hidFeecode = TryCast(selectedRow.FindControl("hidFeecode"), HiddenField)
            Dim hidActivitycode As HiddenField
            hidActivitycode = TryCast(selectedRow.FindControl("hidActivitycode"), HiddenField)
            Dim hidCountrycode As HiddenField
            hidCountrycode = TryCast(selectedRow.FindControl("hidCountrycode"), HiddenField)
            divSubActivity.Visible = True
            txtSubName.Text = lblgrdactname.Text
            txtSubDecsr.Text = lblgrdactdescr.Text

            ddlFeeCode.SelectedValue = IIf(hidFeecode.Value = "", 0, hidFeecode.Value)
            ddlActiTypes.SelectedValue = IIf(hidActivitycode.Value = "", 0, hidActivitycode.Value)
            ddlActiTypes.Enabled = False
            ddlCountry.SelectedValue = IIf(hidCountrycode.Value = "", 0, hidCountrycode.Value)
            'ddlFeeCode.Items.FindByText(lblgrdfeetype.Text).Selected = True
            'ddlActiTypes.Items.FindByText(lblgrdActivityName.Text).Selected = True
            'ddlCountry.Items.FindByText(lblgrdCountry.Text).Selected = True
            'btnModal_Click(sender, e)
            'lbl_ReceiptNo = selectedRow.FindControl("lbl_ReceiptNo")
            'lbl_StudentType = selectedRow.FindControl("lbl_StudentType")
            PanelTitle.InnerText = "Update Activity Sub Type"
            btnSaveSub.Text = "Update"
            hdnfATM_ID.Value = hfALD_ATM_ID.Text
        Catch ex As Exception
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From btnSaveSub: " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub
    Protected Sub hlDelete_Click(sender As Object, e As EventArgs)
        Dim strans As SqlTransaction = Nothing
        Try

            Dim RowId As Integer = (CType((CType(sender, LinkButton)).Parent.Parent, GridViewRow)).RowIndex
            Dim selectedRow As GridViewRow = DirectCast(gvDetails.Rows(RowId), GridViewRow)
            Dim hfALD_ATM_ID As Label
            hfALD_ATM_ID = TryCast(selectedRow.FindControl("lblATMid"), Label)
            Dim lblgrdactname As Label
            lblgrdactname = TryCast(selectedRow.FindControl("lblgrdactname"), Label)
            Dim lblgrdactdescr As Label
            lblgrdactdescr = TryCast(selectedRow.FindControl("lblgrdactdescr"), Label)
            Dim hidFeecode As HiddenField
            hidFeecode = TryCast(selectedRow.FindControl("hidFeecode"), HiddenField)
            Dim hidActivitycode As HiddenField
            hidActivitycode = TryCast(selectedRow.FindControl("hidActivitycode"), HiddenField)
            Dim hidCountrycode As HiddenField
            hidCountrycode = TryCast(selectedRow.FindControl("hidCountrycode"), HiddenField)
            'divSubActivity.Visible = True
            txtSubName.Text = lblgrdactname.Text
            txtSubDecsr.Text = lblgrdactdescr.Text
            hdnfATM_ID.Value = hfALD_ATM_ID.Text
            ddlFeeCode.SelectedValue = IIf(hidFeecode.Value = "", 0, hidFeecode.Value)
            ddlActiTypes.SelectedValue = IIf(hidActivitycode.Value = "", 0, hidActivitycode.Value)
            ddlActiTypes.Enabled = False
            ddlCountry.SelectedValue = IIf(hidCountrycode.Value = "", 0, hidCountrycode.Value)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)


            objConn.Open()
            strans = objConn.BeginTransaction
            Dim pParams(8) As SqlParameter
            pParams(0) = New SqlClient.SqlParameter("@ATM_NAME", txtSubName.Text)
            pParams(1) = New SqlClient.SqlParameter("@ATM_DESCR", txtSubDecsr.Text)
            pParams(2) = New SqlClient.SqlParameter("@ATM_FEE_CODE", ddlFeeCode.SelectedValue)
            pParams(3) = New SqlClient.SqlParameter("@ATM_REF_ID", ddlActiTypes.SelectedValue)
            pParams(4) = New SqlClient.SqlParameter("@ATM_ID", hdnfATM_ID.Value)
            pParams(5) = New SqlClient.SqlParameter("@USER", EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name")))
            pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", ddlCountry.SelectedValue)
            pParams(7) = New SqlClient.SqlParameter("@TYPE", "D")
            pParams(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParams(8).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES", pParams)
            If (pParams(8).Value = 0) Then
                strans.Commit()
                gridbind()
                Clear()
                lblError.Text = "Record Deleted Successfully"
            Else
                strans.Rollback()
                lblError.Text = "Error Occured during processing"
            End If

        Catch ex As Exception
            strans.Rollback()
            lblError.Text = ex.Message
            UtilityObj.Errorlog("From btnSaveSub: " + ex.Message, "Create ACTIVITY Sub Types")
        End Try
    End Sub
    Protected Sub btnModal_Click(sender As Object, e As EventArgs)
        ddlActiTypes.Enabled = True
        divSubActivity.Visible = True
    End Sub

    Private Sub fillFeeTypes(ByRef ddl As DropDownList)
        'METHODE TO FILL FEE TYPES
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@CheckOption", 1, SqlDbType.Int)
            Dim dsFeeTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_REGISTRATION_FEE_DETAILS]", Param)
            ddl.DataSource = dsFeeTypes.Tables(0)
            ddl.DataTextField = "DESCR"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("SELECT", 0))
            ddl.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog("From FillFeetypes: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub btClose_Click(sender As Object, e As EventArgs)
        divSubActivity.Visible = False
        Clear()
    End Sub
    Protected Sub gvDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvDetails.PageIndex = e.NewPageIndex
        gvDetails.DataBind()
        gridbind()
    End Sub
End Class
