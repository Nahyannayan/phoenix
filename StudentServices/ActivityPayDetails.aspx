﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ActivityPayDetails.aspx.vb" Inherits="StudentServices_ActivityPayDetails" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
     <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> --%>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <%--  <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>--%>

    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>




    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../cssfiles/Accordian.css" rel="stylesheet" />

    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->

    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet">
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>


    <%--    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />--%>
</head>
<body>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_getBankOrEmirate" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="420px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getBank" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="420px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getCheque" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="420px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <form id="form1" runat="server">
        <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
        <ajaxToolkit:ToolkitScriptManager ID="scriptmanager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
        <div style="padding: 20px;">
            <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
            <uc2:usrMessageBar ID="usrMessageBar" runat="server" />
        </div>

        <div>
            <table width="98%" align="center">
                <tr class="title-bg">
                    <td align="left" colspan="4">Student  Details                            
                    </td>
                </tr>
                <tr>
                    <td width="20%"><span class="field-label">Student Name </span></td>
                    <td width="30%">
                        <asp:Label ID="lblStudNam" runat="server" Text="" CssClass="field-value"></asp:Label>
                    </td>
                    <td width="20%"><span class="field-label">Student Number </span></td>
                    <td width="30%">
                        <asp:Label ID="lblStudNum" runat="server" Text="" CssClass="field-value"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="20%"><span class="field-label">Grade </span></td>
                    <td width="30%">
                        <asp:Label ID="lblgrdtxt" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                    <td width="20%"><span class="field-label">Section </span></td>
                    <td width="30%">
                        <asp:Label ID="lblscttxt" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                </tr>
            </table>

            <table width="98%" align="center">
                <tr class="title-bg">
                    <td align="left" colspan="4">Activity  Details                            
                    </td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Event Name</span> </td>
                    <td align="left" width="30%">
                        <asp:Label ID="lblEventName" runat="server" Text="" CssClass="field-value"></asp:Label>
                    </td>
                    <td align="left" width="20%"><span class="field-label">Event On</span></td>
                    <td align="left" width="30%">
                        <asp:Label ID="lblEventOn" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                </tr>
            </table>
            <br />
            <table width="98%" align="center" id="tblFeeCollection" runat="server" visible="false">
                <tr class="title-bg">
                    <td align="left" colspan="4">Payment Details
                            <asp:TextBox ID="txtTotal" runat="server" Visible="false" Text="0.00" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <table style="width: 100%;">
                            <%-- <tr>
                                <td>
                                    
                                </td>
                            </tr>--%>
                            <tr id="rowcash" runat="server">
                                <td align="left" width="20%"><span class="field-label">Cash</span></td>
                                <td width="30%">
                                    <%--CASH--%>
                                    <asp:TextBox ID="txtCashTotal" runat="server" AutoCompleteType="Disabled" autocomplete="off" onFocus="this.select();"
                                        onBlur="UpdateSum('0','0','0','0');" Style="text-align: right" TabIndex="45" Text="0.00"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtCashTotal" runat="server" FilterType="Numbers, Custom"
                                        ValidChars="." TargetControlID="txtCashTotal" />
                                </td>
                                <td colspan="2" width="50%"></td>
                            </tr>
                            <tr id="rowcard" runat="server">
                                <%-- CARD--%>
                                <%-- <td>                                       
                                        <table style="width: 100%;">
                                            <tr>--%>
                                <td align="left" width="20%"><span class="field-label">Credit Card</span></td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtCCTotal" Style="text-align: right" runat="server" onFocus="this.select(); " autocomplete="off"
                                        onBlur="UpdateSum('0','0','0','0');" TabIndex="50" AutoPostBack="false" Text="0.00"></asp:TextBox>
                                    <ajaxToolkit:PopupControlExtender ID="pceCreditCard" runat="server" PopupControlID="pnlCreditCard"
                                        Position="Bottom" TargetControlID="txtCCTotal">
                                    </ajaxToolkit:PopupControlExtender>
                                </td>
                                <td align="left" width="20%">
                                    <table width="100%">
                                        <tr>
                                            <td><span class="field-label">Txn.Charge </span></td>
                                            <td>
                                                <asp:TextBox ID="txtCrCardCharges" runat="server" Text="0.00" Style="text-align: right; width: 60px;"
                                                    TabIndex="200" Enabled="False"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" width="30%"></td>
                            </tr>
                            <tr id="rowcheque" runat="server">
                                <td align="left" width="20%"><span class="field-label">Cheque</span></td>
                                <td width="30%">
                                    <%-- -CHEQUE--%>
                                    <asp:TextBox ID="txtBankTotal" Style="text-align: right" runat="server" onFocus="this.select();"
                                        onBlur="UpdateSum('0','0','0','0');" AutoCompleteType="Disabled" autocomplete="off" TabIndex="165" Text="0.00"></asp:TextBox>
                                    <ajaxToolkit:PopupControlExtender ID="pceCheque" runat="server" PopupControlID="pnlCheque"
                                        Position="Bottom" TargetControlID="txtBankTotal">
                                    </ajaxToolkit:PopupControlExtender>
                                </td>
                                <td colspan="2" width="50%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Total Paying</span></td>
                                <td align="left" width="30%">
                                    <asp:Label ID="txtReceivedTotal" runat="server" Text="0.00"></asp:Label>
                                </td>
                                <td colspan="2" width="50%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Total To Pay</span></td>
                                <td align="left" width="30%">
                                    <asp:Label ID="lblAmountPay" runat="server" Text="0.00"></asp:Label>
                                </td>
                                <td colspan="2" width="50%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlCheque" Style="display: none" runat="server" CssClass="panel-cover" Width="50%">
                <asp:Label ID="lblBankInvalid" runat="server" EnableViewState="False" SkinID="LabelError" Height="0px"></asp:Label>

                <table border="1" cellpadding="0" cellspacing="0" class="BlueTable_Colln"
                    bgcolor="#ffffff">
                    <tr class="title-bg">
                        <th align="left">Bank
                        </th>
                        <th align="left" id="tdhBA">Bank Account
                        </th>
                        <th align="left">Emirate
                        </th>
                        <th align="left">Cheque #<asp:LinkButton ID="lnkClear" OnClientClick="return ClearCheque();" runat="server">(Clear)</asp:LinkButton>
                        </th>
                        <th align="left">Amount
                        </th>
                        <th align="left">Cheque Date
                        </th>
                        <th align="left">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="return HideAll(0);" TabIndex="89" /></th>
                    </tr>
                    <tr id="tr_chq1" class="table-popup">
                        <td align="left">

                            <asp:TextBox ID="txtBank1" runat="server" AutoPostBack="True" TabIndex="70"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgBank1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getBankOrEmirate(1,1); return false" TabIndex="75" />
                        </td>
                        <td id="tdBA1" align="left">
                            <asp:TextBox ID="txtBankAct1" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgBankAct1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getBank(1); return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct1" runat="server" />
                        </td>
                        <td align="center">
                            <asp:DropDownList ID="ddlEmirate1" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="78">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqno1" runat="server" TabIndex="80" AutoCompleteType="Disabled" autocomplete="off"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgCheque" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getCheque() ; return false;" TabIndex="75" />
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChequeTotal1" onBlur="UpdateSum('1','0','0','0');" onFocus="this.select();"
                                runat="server" Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqDate1" runat="server" TabIndex="87" Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgChequedate1" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="88" />
                        </td>
                        <td align="left">
                            <asp:LinkButton ID="LinkButton9" runat="server" OnClientClick="Hidebank('tr_chq2',2);return false;">Add</asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="tr_chq2" style="display: none" class="table-popup">
                        <td align="left">
                            <asp:TextBox ID="txtBank2" runat="server" AutoPostBack="True" TabIndex="90"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgBank2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="  getBankOrEmirate(1,2);return false" TabIndex="95" />
                        </td>
                        <td id="tdBA2" align="left">

                            <asp:TextBox ID="txtBankAct2" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgBankAct2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="  getBank(2); return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct2" runat="server" />
                        </td>
                        <td align="center">
                            <asp:DropDownList ID="ddlEmirate2" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="100">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqno2" runat="server" TabIndex="105" AutoCompleteType="Disabled " autocomplete="off"
                                Style="width: 80% !important"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChequeTotal2" onBlur="UpdateSum('2','0','0','0');" onFocus="this.select();"
                                runat="server" Style="text-align: right" TabIndex="110" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqDate2" runat="server" TabIndex="115" Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgChequedate2" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="120" />
                        </td>
                        <td align="left">
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="return HideBankRow('tr_chq2',2);" TabIndex="121" />
                            <asp:LinkButton ID="LinkButton10" runat="server" OnClientClick="Hidebank('tr_chq3',3);return false;">Add</asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="tr_chq3" style="display: none" class="table-popup">
                        <td align="left">
                            <asp:TextBox ID="txtBank3" runat="server" AutoPostBack="True" TabIndex="125"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgBank3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="getBankOrEmirate(1,3); return false" TabIndex="130" />
                        </td>
                        <td id="tdBA3" align="left">

                            <asp:TextBox ID="txtBankAct3" runat="server" onfocus="this.blur();" AutoPostBack="True" TabIndex="70"
                                Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgBankAct3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick="  getBank(3);return false;" TabIndex="75" /><asp:HiddenField ID="hfBankAct3" runat="server" />


                        </td>
                        <td align="center">
                            <asp:DropDownList ID="ddlEmirate3" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                                DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="135">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqno3" runat="server" TabIndex="140" AutoCompleteType="Disabled" autocomplete="off"
                                Style="width: 80% !important"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChequeTotal3" onBlur="UpdateSum('3','0','0','0');" onFocus="this.select();"
                                Style="text-align: right" runat="server" TabIndex="145" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqDate3" runat="server" TabIndex="150" Style="width: 80% !important"></asp:TextBox>
                            <asp:ImageButton ID="imgChequedate3" runat="server" ImageUrl="~/Images/calendar.gif"
                                TabIndex="156" />
                        </td>
                        <td align="left">
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="return HideBankRow('tr_chq3',3);" TabIndex="157" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Bank1" runat="server" />
                <asp:HiddenField ID="h_Bank2" runat="server" />
                <asp:HiddenField ID="h_Bank3" runat="server" />
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                    SelectCommand="SELECT     EMR_CODE, EMR_DESCR, EMR_ENQ_GROUP, EMR_CTY_ID&#13;&#10;FROM  EMIRATE_M&#13;&#10;WHERE     (EMR_CTY_ID IN&#13;&#10;                          (SELECT     BSU_COUNTRY_ID&#13;&#10;                            FROM          BUSINESSUNIT_M&#13;&#10;                            WHERE      (BSU_ID =@BSU_ID)))">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
                <ajaxToolkit:CalendarExtender ID="calCheque1" runat="server"
                    PopupButtonID="imgChequedate1" TargetControlID="txtChqDate1" Format="dd/MMM/yyyy"
                    Enabled="True" PopupPosition="TopLeft">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calCheque2" runat="server"
                    PopupButtonID="imgChequedate2" TargetControlID="txtChqDate2" Format="dd/MMM/yyyy"
                    Enabled="True" PopupPosition="TopLeft">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calCheque3" runat="server"
                    PopupButtonID="imgChequedate3" TargetControlID="txtChqDate3" Format="dd/MMM/yyyy"
                    Enabled="True" PopupPosition="TopLeft">
                </ajaxToolkit:CalendarExtender>
            </asp:Panel>
            <asp:Panel ID="pnlCreditCard" Style="display: none" runat="server" CssClass="panel-cover" Width="50%">
                <table border="1" align="center"
                    bgcolor="#ffffff">
                    <tr class="title-bg">
                        <th align="left" colspan="1" class="matters_Colln">Credit Card Type
                        </th>
                        <th align="left" colspan="1" class="matters_Colln">Authorisation code
                        </th>
                        <th align="left" colspan="1" class="matters_Colln">Amount
                        </th>
                        <th align="left" colspan="1" class="matters_Colln">Charge
                        </th>
                        <th align="left" colspan="1" class="matters_Colln">Total
                        </th>
                        <th align="center" colspan="1">
                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="return HideAll(0);" TabIndex="54" Style="text-align: right" />
                        </th>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:DropDownList ID="ddCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                DataValueField="CRR_ID" TabIndex="55" Style="width: 80% !important">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCreditno" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                TabIndex="56" Style="width: 80% !important"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCTotal1" runat="server" AutoCompleteType="Disabled" autocomplete="off" onfocus="this.select();" onBlur="UpdateSum('1','0','0','0');"
                                Style="text-align: right; width: 80% !important" TabIndex="57"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCardP1" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                TabIndex="58" Enabled="False"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCardCharge1" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                TabIndex="59" Enabled="False"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:LinkButton ID="LinkButton11" runat="server" TabIndex="60" OnClientClick="HideCard('trCrCard2',2);return false;">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="return HideAll(1);" TabIndex="61" />
                            <asp:HiddenField ID="hfCard1" runat="server" />
                        </td>
                    </tr>
                    <tr id="trCrCard2" style="display: none">
                        <td align="left" colspan="1">
                            <asp:DropDownList ID="ddCreditcard2" runat="server" DataSourceID="SqlCreditCard"
                                DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal"
                                TabIndex="62" Style="width: 80% !important">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCreditno2" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                TabIndex="63" Style="width: 80% !important"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCTotal2" runat="server" AutoCompleteType="Disabled" autocomplete="off" onfocus="this.select();" onBlur="UpdateSum('2','0','0','0');"
                                Style="text-align: right; width: 80% !important" TabIndex="64"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCardP2" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                TabIndex="65" Enabled="False"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCardCharge2" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                TabIndex="66" Enabled="False"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt; width: auto">
                            <asp:LinkButton ID="LinkButton12" runat="server" TabIndex="67" OnClientClick="HideCard('trCrCard3',3);return false;">Add</asp:LinkButton>
                            <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="HideCardRow('trCrCard2',1);return false;" TabIndex="68" />
                            <asp:HiddenField ID="hfCard2" runat="server" />
                        </td>
                    </tr>
                    <tr id="trCrCard3" style="display: none">
                        <td align="left">
                            <asp:DropDownList ID="ddCreditcard3" runat="server" DataSourceID="SqlCreditCard"
                                DataTextField="CRI_DESCR" DataValueField="CRR_ID" SkinID="DropDownListNormal"
                                TabIndex="69" Width="80%">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCreditno3" runat="server" AutoCompleteType="Disabled" autocomplete="off"
                                TabIndex="70" Width="80%"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCTotal3" runat="server" AutoCompleteType="Disabled" autocomplete="off" onfocus="this.select();" onBlur="UpdateSum('3','0','0','0');"
                                Style="text-align: right; width: 80% !important" TabIndex="71"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCardP3" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                TabIndex="72" Enabled="False"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:TextBox ID="txtCrCardCharge3" runat="server" Text="0.00" AutoCompleteType="Disabled" autocomplete="off"
                                onfocus="this.select();" Style="text-align: right; width: 80% !important"
                                TabIndex="73" Enabled="False"></asp:TextBox>
                        </td>
                        <td align="left" style="font-size: 12pt">
                            <asp:ImageButton ID="ImageButton6" runat="server" TabIndex="74" ImageUrl="~/Images/close_red.gif"
                                OnClientClick="HideCardRow('trCrCard3',1);return false;" />
                            <asp:HiddenField ID="hfCard3" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                    SelectCommand="GetCreditCardListForCollection" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="CollectFrom" DefaultValue="FEES" Type="String" />
                        <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hfCobrand" runat="server" />
            </asp:Panel>
        </div>
        <div>
            <table id="tblOtherCollection" runat="server" width="98%" visible="false" align="center">
                <tr class="title-bg">
                    <td align="left" colspan="6">Payment Details
                    </td>
                </tr>
                <tr runat="server">
                    <td align="center" colspan="6">
                        <asp:RadioButtonList ID="rblPaymentModes" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr id="tr_Cash" runat="server">
                    <td align="left" width="20%"><span class="field-label">Cash</span>
                    </td>
                    <td align="left" width="20%">
                        <asp:TextBox ID="txtothCashTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.blur();"
                            onBlur="UpdateSumoth();" Style="text-align: right" TabIndex="45"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtothCashTotal"
                            FilterType="Numbers,Custom" ValidChars="." />
                    </td>
                    <%--  <td align="left" rowspan="1"  colspan="4" width="60%">
                            <asp:Button ID="btnAddCash" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="46" />
                        </td>--%>
                </tr>
                <tr id="tr_CreditCard" runat="server">
                    <td colspan="6">

                        <table>
                            <tr class="title-bg">
                                <td align="left" class="matters_Colln">Card Type
                                </td>
                                <td align="left" class="matters_Colln">Auth Code
                                </td>
                                <td align="left" class="matters_Colln">Amount
                                </td>
                                <td align="left" class="matters_Colln">Charge
                                </td>
                                <td align="left" class="matters_Colln">Total
                                </td>
                                <td align="left" class="matters_Colln"></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:DropDownList ID="ddothCreditcard" runat="server" DataSourceID="SqlCreditCard" DataTextField="CRI_DESCR"
                                        DataValueField="CRR_ID" SkinID="DropDownListNormal" TabIndex="55" onChange="UpdateSumoth();">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtothCreditno" runat="server" AutoCompleteType="Disabled" TabIndex="56"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtothCCTotal" runat="server" AutoCompleteType="Disabled" onFocus="this.blur();"
                                        onBlur="UpdateSumoth();" Style="text-align: right" TabIndex="57"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                        TargetControlID="txtCCTotal" FilterType="Numbers,Custom" ValidChars="." />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCrCharge" runat="server" AutoCompleteType="Disabled" Text="0.00"
                                        Style="text-align: right" TabIndex="58" Enabled="false"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtChargeTotal" runat="server" AutoCompleteType="Disabled" Text="0.00"
                                        Style="text-align: right" TabIndex="59" Enabled="false"></asp:TextBox>
                                </td>
                                <%-- <td>
                                <asp:Button ID="btnAddCreditcard" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                    TabIndex="60" />
                            </td>--%>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr id="tr_Cheque1" runat="server">
                    <td align="left" width="15%"><span class="field-label">Cheque Amount</span>
                    </td>
                    <td align="left" width="15%">
                        <asp:TextBox ID="txtChequeTotal" onBlur="UpdateSumoth();" onFocus="this.blur();" runat="server"
                            Style="text-align: right" TabIndex="85" AutoCompleteType="Disabled"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                            TargetControlID="txtChequeTotal" FilterType="Numbers,Custom" ValidChars="." />
                    </td>
                    <td align="left" width="30%" colspan="2">
                        <table style="border: none 0;" width="100%">
                            <tr>
                                <td align="left"><span class="field-label">Bank</span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtBank" runat="server" AutoPostBack="True" TabIndex="86"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton5" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                        OnClientClick="getBankOrEmirate(1,4); return false;" TabIndex="87" />
                                    <asp:HiddenField ID="hf_BNK_TYPE" runat="server" />
                                </td>

                            </tr>
                            <tr>
                                <td align="left" id="tdhBAoth"><span class="field-label">Account</span>
                                </td>
                                <td id="tdBA1oth">
                                    <asp:TextBox ID="txtothBankAct1" runat="server" onfocus="this.blur();" TabIndex="88"
                                        SkinID="TextBoxCollection"></asp:TextBox>
                                    <asp:ImageButton ID="imgothBankAct1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/forum_search.gif"
                                        OnClientClick="getBankAct(1);return false;" TabIndex="75" /><asp:HiddenField ID="HiddenField1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td align="left" width="15%"><span class="field-label">Emirate</span>
                    </td>
                    <td align="left" width="15%">
                        <asp:DropDownList ID="ddlEmirate" runat="server" DataSourceID="SqlEmirate_m" DataTextField="EMR_DESCR"
                            DataValueField="EMR_CODE" SkinID="DropDownListNormal" TabIndex="89">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="tr_Cheque2" runat="server">
                    <td align="left"><span class="field-label">Chq. No.</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtChqno" runat="server" TabIndex="90" AutoCompleteType="Disabled">
                        </asp:TextBox>
                    </td>
                    <td align="left" colspan="2"><span class="field-label">Date</span> &nbsp;&nbsp;
                            <asp:TextBox ID="txtChqDate" runat="server" TabIndex="91"></asp:TextBox>
                        <asp:ImageButton ID="imgChequedate" runat="server" ImageUrl="~/Images/calendar.gif"
                            TabIndex="92" />
                        <ajaxToolkit:CalendarExtender ID="calCheque" CssClass="MyCalendar" runat="server"
                            PopupButtonID="imgChequedate" TargetControlID="txtChqDate" Format="dd/MMM/yyyy"
                            Enabled="True" PopupPosition="TopLeft">
                        </ajaxToolkit:CalendarExtender>
                    </td>
                    <%-- <td align="left" colspan="2">
                            <asp:Button ID="btnAddCheque" runat="server" CssClass="button" Text="Add" CausesValidation="False"
                                TabIndex="93" OnClick="btnAddCheque_Click" />
                        </td>--%>
                </tr>
            </table>
        </div>
        <div>
            <table width="100%" align="center">
                <tr>
                    <td align="center">
                        <%--<asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" Visible="false" />--%>
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" TabIndex="165" OnClick="btnSave_Click" />
                        <%--<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False"  />--%>
                        <asp:Label ID="lblToken" runat="server"></asp:Label>
                        <asp:Button ID="lnkReceipt" runat="server" Visible="false" Text="Print Receipt" CssClass="button" OnClick="lnkReceipt_Click"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>


        <div>


            <asp:SqlDataSource ID="SqlEmirate_m" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
                SelectCommand="SELECT [EMR_CODE], [EMR_DESCR] FROM [EMIRATE_M]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlCreditCard" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                SelectCommand="GetCreditCardListForCollection" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:Parameter Name="CollectFrom" DefaultValue="FEES" Type="String" />
                    <asp:SessionParameter SessionField="sBsuid" Name="BSU_ID"></asp:SessionParameter>
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:MainDB %>"
                SelectCommand="SELECT DISTINCT [COL_ID], [COL_DESCR], [COL_ACT_ID] FROM [COLLECTION_M] WITH(NOLOCK) WHERE ([COL_ID] IN (SELECT DISTINCT OFM_COL_ID FROM OASIS_FEES.DAX.OTH_FEECOLLECTION_MAPPING WITH(NOLOCK))) AND ([COL_ID] <> 1)"></asp:SqlDataSource>
            <asp:HiddenField ID="h_print" runat="server" />
            <asp:HiddenField ID="h_hideBank" runat="server" />
            <asp:HiddenField ID="h_CrCard" runat="server" />
            <asp:HiddenField ID="h_Chequeid" runat="server" />
            <asp:HiddenField ID="hf_mode" runat="server" />
            <asp:HiddenField ID="hf_ctrl" runat="server" />
            <asp:HiddenField ID="h_Bank" runat="server" />

        </div>



        <script>
            function getRoundOff() {
                var roundOff = '<%=Session("BSU_ROUNDOFF") %>';
                var amt;
                amt = parseFloat(roundOff)
                if (isNaN(amt))
                    amt = 2;
                return amt;
            }
            function CheckNumber(ctrl) {
                var temp;
                temp = parseFloat(ctrl.value);
                if (isNaN(temp))
                    ctrl.value = 0;
                else
                    ctrl.value = temp.toFixed(2);
            }
            function CheckAmount(e) {
                var amt;
                amt = parseFloat(e.value)
                if (isNaN(amt))
                    amt = 0;
                e.value = amt.toFixed(getRoundOff());
                return true;
            }
            function UpdateSum(ddl, txt, txtrate, txtSum) {

                var txtCCTotal, txtChequeTotal1, txtChequeTotal2, txtChequeTotal3, txtCashTotal,
                   TotalPayingNow, txtReturn, TotalReceived, Balance, BankTotal, txtCrCTotal1,
                   txtCrCTotal2, txtCrCTotal3, txtTAX, VoucherAmount1, VoucherTotal, RewardsTotal;

                txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
                if (isNaN(txtCCTotal))
                    txtCCTotal = 0;

                txtChequeTotal1 = parseFloat(document.getElementById('<%=txtChequeTotal1.ClientID %>').value);
                if (isNaN(txtChequeTotal1))
                    txtChequeTotal1 = 0;
                txtChequeTotal2 = parseFloat(document.getElementById('<%=txtChequeTotal2.ClientID %>').value);
                if (isNaN(txtChequeTotal2))
                    txtChequeTotal2 = 0;
                txtChequeTotal3 = parseFloat(document.getElementById('<%=txtChequeTotal3.ClientID %>').value);
                if (isNaN(txtChequeTotal3))
                    txtChequeTotal3 = 0;
                BankTotal = txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3;
                txtCrCTotal1 = parseFloat(document.getElementById('<%=txtCrCTotal1.ClientID %>').value);
                if (isNaN(txtCrCTotal1))
                    txtCrCTotal1 = 0;
                txtCrCTotal2 = parseFloat(document.getElementById('<%=txtCrCTotal2.ClientID %>').value);
                 if (isNaN(txtCrCTotal2))
                     txtCrCTotal2 = 0;
                 txtCrCTotal3 = parseFloat(document.getElementById('<%=txtCrCTotal3.ClientID %>').value);
            if (isNaN(txtCrCTotal3))
                txtCrCTotal3 = 0;
            txtCCTotal = txtCrCTotal1 + txtCrCTotal2 + txtCrCTotal3;
            if (isNaN(txtCCTotal))
                txtCCTotal = 0;
            txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
            if (isNaN(txtCashTotal))
                txtCashTotal = 0;
            <%--    TotalPayingNow = parseFloat(document.getElementById('<%=txtTotal.ClientID %>').value);--%>
                TotalPayingNow = parseFloat($("#<%=txtTotal.ClientID%>").text().replace(/,/g, ''));
                if (isNaN(TotalPayingNow))
                    TotalPayingNow = 0;


                TotalReceived = txtCCTotal + txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3 + txtCashTotal;

                Balance = 0;
                if (txtCashTotal > 0 || VoucherTotal > 0 || RewardsTotal > 0) {
                    if (TotalReceived > (TotalPayingNow)) {
                        Balance = TotalReceived - TotalPayingNow;
                        if (Balance > txtCashTotal)
                            Balance = 0;
                    }
                    else {
                        Balance = (parseFloat(TotalPayingNow)) - parseFloat(TotalReceived);
                    }
                }
            <%-- document.getElementById('<%=txtReceivedTotal.ClientID %>').value = TotalReceived.toFixed(getRoundOff());--%>
                $("#<%=txtReceivedTotal.ClientID%>").text(TotalReceived.toFixed(getRoundOff()));
                document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());
                document.getElementById('<%=txtBankTotal.ClientID %>').value = BankTotal.toFixed(getRoundOff());
                document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
                document.getElementById('<%=txtChequeTotal1.ClientID %>').value = txtChequeTotal1.toFixed(getRoundOff());
                document.getElementById('<%=txtChequeTotal2.ClientID %>').value = txtChequeTotal2.toFixed(getRoundOff());
                document.getElementById('<%=txtChequeTotal3.ClientID %>').value = txtChequeTotal3.toFixed(getRoundOff());
           <%-- document.getElementById('<%=txtBalance.ClientID %>').innerHTML = Balance.toFixed(getRoundOff());--%>

                if (txtCCTotal >= 0) {
                    CheckRate(ddl, txt, txtrate, txtSum);
                    txtCrCardCharges = parseFloat(document.getElementById('<%=txtCrCardCharges.ClientID %>').value);
                    if (isNaN(txtCrCardCharges))
                        txtCrCardCharges = 0.00;
                    TotalReceived = txtCCTotal + txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3 + txtCashTotal + txtCrCardCharges;
                    BankTotal = txtChequeTotal1 + txtChequeTotal2 + txtChequeTotal3;

                    if (txtCashTotal > 0) {
                        Balance = 0;
                        if (TotalReceived > (TotalPayingNow)) {
                            Balance = TotalReceived - TotalPayingNow - txtCrCardCharges;
                            if (Balance > txtCashTotal)
                                Balance = 0;
                        }
                        else {
                            Balance = (parseFloat(TotalPayingNow)) - parseFloat(TotalReceived);
                        }
                    }
                <%--  document.getElementById('<%=txtReceivedTotal.ClientID %>').value = TotalReceived.toFixed(getRoundOff());--%>
                    $("#<%=txtReceivedTotal.ClientID%>").text(TotalReceived.toFixed(getRoundOff()));
                }
           <%-- document.getElementById('<%=txtBalance.ClientID %>').innerHTML = Balance.toFixed(getRoundOff());--%>
            }

            function CheckRate(ddl, txt, txtrate, txtSum) {

                var temp = new Array();
                if (txt != "0") {
                    var txtCr = parseFloat(document.getElementById(txt).value);
                    if (isNaN(txtCr))
                        txtCr = 0.00;
                }
                else
                    txtCr = 0.00;
                var CrCardCharge = parseFloat(document.getElementById('<%=txtCrCardCharges.ClientID %>').value);
                if (isNaN(CrCardCharge))
                    CrCardCharge = 0.00;
                temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
                var ddlist = document.getElementById(ddl);

                if (ddl != "0" && ddlist != null) {
                    for (var i = 0; i < temp.length; i++) {
                        if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                            document.getElementById(txtrate).value = Math.ceil((txtCr * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff()));
                            document.getElementById(txtSum).value = txtCr + Math.ceil((txtCr * (temp[i].split("=")[1] / 100.00)));
                        }
                    }
                }

                var txtCr1 = parseFloat(document.getElementById('<%=txtCrCTotal1.ClientID %>').value);
                if (isNaN(txtCr1))
                    txtCr1 = 0.00;
                var txtCr2 = parseFloat(document.getElementById('<%=txtCrCTotal2.ClientID %>').value);
                if (isNaN(txtCr2))
                    txtCr2 = 0.00;
                var txtCr3 = parseFloat(document.getElementById('<%=txtCrCTotal3.ClientID %>').value);
                if (isNaN(txtCr3))
                    txtCr3 = 0.00;
                var CardP1 = parseFloat(document.getElementById('<%=txtCrCardP1.ClientID %>').value);
                if (isNaN(CardP1))
                    CardP1 = 0.00;
                var CardP2 = parseFloat(document.getElementById('<%=txtCrCardP2.ClientID %>').value);
                 if (isNaN(CardP2))
                     CardP2 = 0.00;
                 var CardP3 = parseFloat(document.getElementById('<%=txtCrCardP3.ClientID %>').value);
                 if (isNaN(CardP3))
                     CardP3 = 0.00;

                 CrCardCharge = (CardP1) + (CardP2) + (CardP3);
                 document.getElementById('<%=txtCrCardCharge1.ClientID %>').value = (txtCr1 + CardP1).toFixed(getRoundOff());
                 document.getElementById('<%=txtCrCardCharge2.ClientID %>').value = (txtCr2 + CardP2).toFixed(getRoundOff());
                document.getElementById('<%=txtCrCardCharge3.ClientID %>').value = (txtCr3 + CardP3).toFixed(getRoundOff());
                document.getElementById('<%=txtCrCardCharges.ClientID %>').value = CrCardCharge.toFixed(getRoundOff());

                //}return true;

            }

            function Hidebank(id, index) {
                $("#" + id).show(250);
                $('#<%=h_hideBank.ClientID %>').val(index);
                return false;
            }
            function HideBankRow(id, index) {
                $("#" + id).hide(250);
                $('#<%=h_hideBank.ClientID%>').val(index);
                return false;
            }

            function HideCard(id, index) {
                document.getElementById(id).style.display = 'table-row';
                document.getElementById('<%=h_CrCard.ClientID %>').value = index;
                return false;
            }
            function HideCardRow(id, index) {
                if (id == "trCrCard2") {
                    document.getElementById('<%=txtCrCTotal2.ClientID %>').value = 0.00;
                    document.getElementById('<%=txtCrCardP2.ClientID %>').value = 0.00;
                    document.getElementById('<%=txtCrCardCharge2.ClientID %>').value = 0.00;
                }
                else if (id == "trCrCard3") {
                    document.getElementById('<%=txtCrCTotal3.ClientID%>').value = 0.00;
                    document.getElementById('<%=txtCrCardP3.ClientID%>').value = 0.00;
                    document.getElementById('<%=txtCrCardCharge3.ClientID%>').value = 0.00;
                }
            $("#" + id).hide(250);
            $('#<%=h_CrCard.ClientID%>').val(index);
             return false;
         }

         function HideAll(index) {
             if (index == 0) {
                 document.getElementById('<%= pnlCheque.ClientID %>').style.display = 'none';
                 document.getElementById('<%= pnlCreditCard.ClientID %>').style.display = 'none';
                 return false;
             }
             else if (index == 1) {
                 document.getElementById('<%= txtCrCTotal1.ClientID %>').value = 0.00;
                 document.getElementById('<%= txtCrCardP1.ClientID %>').value = 0.00;
                 document.getElementById('<%= txtCrCardCharge1.ClientID %>').value = 0.00;
                 return false;
             }
     }
     function ClearCheque() {
         document.getElementById('<%= txtChqno1.ClientID %>').readOnly = '';
         document.getElementById('<%= h_Chequeid.ClientID %>').value = '';
         document.getElementById('<%= txtChqno1.ClientID %>').value = '';
         return false;
     }
     function autoSizeWithCalendar(oWindow) {
         var iframe = oWindow.get_contentFrame();
         var body = iframe.contentWindow.document.body;

         var height = body.scrollHeight;
         var width = body.scrollWidth;

         var iframeBounds = $telerik.getBounds(iframe);
         var heightDelta = height - iframeBounds.height;
         var widthDelta = width - iframeBounds.width;

         if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
         if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
         oWindow.center();
     }

     function getCheque() {
         var oWnd = radopen("../common/PopupFormThreeIDHidden.aspx?iD=FEE_CHEQUE&MULTISELECT=FALSE", "pop_getCheque");

     }
     function OnClientClose3(oWnd, args) {

         //get the transferred arguments
         var arg = args.get_argument();
         if (arg) {

             NameandCode = arg.NameandCode.split('||');

             document.getElementById('<%= txtChqno1.ClientID %>').readOnly = true;
             document.getElementById('<%= h_Chequeid.ClientID %>').value = NameandCode[0];
             document.getElementById('<%= txtChqno1.ClientID %>').value = NameandCode[1];
         }
     }
     function getBankOrEmirate(mode, ctrl) {
         var url;
         document.getElementById('<%= hf_mode.ClientID%>').value = mode
         document.getElementById('<%= hf_ctrl.ClientID%>').value = ctrl
         if (mode == 1)
             url = "../common/PopupFormIDhidden.aspx?iD=BANK&MULTISELECT=FALSE";
         else
             url = "../common/PopupFormIDhidden.aspx?iD=EMIRATE&MULTISELECT=FALSE";
         var oWnd = radopen(url, "pop_getBankOrEmirate");

     }

     function OnClientClose1(oWnd, args) {

         var mode = document.getElementById('<%= hf_mode.ClientID%>').value
             var ctrl = document.getElementById('<%= hf_ctrl.ClientID%>').value
             //get the transferred arguments
             var arg = args.get_argument();
             if (arg) {

                 NameandCode = arg.NameCode.split('||');

                 if (mode == 1) {
                     if (ctrl == 4) {
                         document.getElementById('<%= h_Bank.ClientID%>').value = NameandCode[0];
                         document.getElementById('<%= txtBank.ClientID %>').value = NameandCode[1];
                         if (NameandCode[1] == "Bank Transfer") {
                             $("#tdhBAoth").show(250);
                             $("#tdBA1oth").show(250);
                         }
                         else {
                             $("#tdhBAoth").hide(250);
                             $("#tdBA1oth").hide(250);
                         }
                     }
                     if (ctrl == 1) {
                         document.getElementById('<%= h_Bank1.ClientID %>').value = NameandCode[0];
                         document.getElementById('<%= txtBank1.ClientID %>').value = NameandCode[1];
                         if (NameandCode[1] == "Bank Transfer") {
                             $("#tdhBA").show(250);
                             $("#tdBA1").show(250);
                         }
                         else {
                             $("#tdhBA").hide(250);
                             $("#tdBA1").hide(250);
                         }
                     }
                     else if (ctrl == 2) {
                         document.getElementById('<%= h_Bank2.ClientID %>').value = NameandCode[0];
                             document.getElementById('<%= txtBank2.ClientID %>').value = NameandCode[1];
                             if (NameandCode[1] == "Bank Transfer")
                                 $("#tdBA2").show(250);
                             else
                                 $("#tdBA2").hide(250);
                         }
                         else if (ctrl == 3) {
                             document.getElementById('<%= h_Bank3.ClientID %>').value = NameandCode[0];
                             document.getElementById('<%= txtBank3.ClientID %>').value = NameandCode[1];
                             if (NameandCode[1] == "Bank Transfer")
                                 $("#tdBA3").show(250);
                             else
                                 $("#tdBA3").hide(250);
                         }
             }
         }
     }

     function CalculateRate(CardAmount) {
         var temp = new Array();
         temp = document.getElementById('<%=hfCobrand.ClientID %>').value.split("|");
        var ddlist = document.getElementById('<%=ddCreditcard.ClientID %>');

        if (temp.length > 0 && ddlist != null) {
            for (var i = 0; i < temp.length; i++) {
                if (temp[i].split("=")[0] == ddlist.options[ddlist.selectedIndex].value) {
                    document.getElementById('<%=txtCrCharge.ClientID %>').value = Math.ceil(CardAmount * temp[i].split("=")[1] / 100.00).toFixed(getRoundOff());
                    //document.getElementById('<%=txtReceivedTotal.ClientID %>').value = CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                    document.getElementById('<%=txtChargeTotal.ClientID %>').value = CardAmount + Math.ceil(CardAmount * (temp[i].split("=")[1] / 100.00));
                }
            }
        }
    }


    function getBank(mode) {
        var url;
        document.getElementById('<%= hf_mode.ClientID%>').value = mode

        var txtBankCode;
        if (mode == 1)
            txtBankCode = document.getElementById('<%=txtBankAct1.ClientID%>').value;
        var oWnd = radopen("../ACCOUNTS/PopUp.aspx?ShowType=BANK&codeorname=" + txtBankCode, "pop_getBank");


    }

    function OnClientClose2(oWnd, args) {

        var mode = document.getElementById('<%= hf_mode.ClientID%>').value

        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {

            NameandCode = arg.NameandCode.split('||');

            if (mode == 1) {
                document.getElementById('<%=hfBankAct1.ClientID%>').value = NameandCode[0];
                        document.getElementById('<%=txtBankAct1.ClientID%>').value = NameandCode[0];
                    }
                    else if (mode == 2) {
                        document.getElementById('<%=hfBankAct2.ClientID%>').value = NameandCode[0];
                        document.getElementById('<%=txtBankAct2.ClientID%>').value = NameandCode[0];
                    }
                    else if (mode == 3) {
                        document.getElementById('<%=hfBankAct3.ClientID%>').value = NameandCode[0];
                 document.getElementById('<%=txtBankAct3.ClientID%>').value = NameandCode[0];
             }
 }
}

function UpdateSumoth() {

    var txtCCTotal, txtChequeTotal, txtCashTotal,
           txtTotal, txtReturn, lblTotalNETAmount, txtBalance, txtBankTotal;
    try {
        txtCCTotal = parseFloat(document.getElementById('<%=txtCCTotal.ClientID %>').value);
         }
         catch (e) { }
         if (isNaN(txtCCTotal))
             txtCCTotal = 0;
         try {
             txtChequeTotal = parseFloat(document.getElementById('<%=txtChequeTotal.ClientID %>').value);
        }
        catch (e) { }

        if (isNaN(txtChequeTotal))
            txtChequeTotal = 0;
        try {
            txtCashTotal = parseFloat(document.getElementById('<%=txtCashTotal.ClientID %>').value);
        }
        catch (e) { }

        if (isNaN(txtCashTotal))
            txtCashTotal = 0;

        if (isNaN(txtTotal))
            txtTotal = 0;
        lblTotalNETAmount = txtCCTotal + txtChequeTotal + txtCashTotal;
        txtBankTotal = txtChequeTotal;
        txtBalance = 0;
        if (txtCashTotal > 0)
            if (lblTotalNETAmount > txtTotal) {
                txtBalance = lblTotalNETAmount - txtTotal;
                if (txtBalance > txtCashTotal)
                    txtBalance = 0;
            }
         //document.getElementById('<%=txtReceivedTotal.ClientID %>').value = txtReceivedTotal.toFixed(getRoundOff());
         try {
             document.getElementById('<%=txtCCTotal.ClientID %>').value = txtCCTotal.toFixed(getRoundOff());
        } catch (e) { }
        try {
            document.getElementById('<%=txtChequeTotal.ClientID %>').value = txtBankTotal.toFixed(getRoundOff());
        } catch (e) { }
        try {
            document.getElementById('<%=txtCashTotal.ClientID %>').value = txtCashTotal.toFixed(getRoundOff());
        } catch (e) { }


        if (txtCCTotal >= 0) {
            CalculateRate(txtCCTotal);
        }
    }

    function ShowWindowWithClose(gotourl, pageTitle, w, h) {

        $.fancybox({
            type: 'iframe',
            //maxWidth: 300,
            href: gotourl,
            //maxHeight: 600,
            fitToView: true,
            padding: 6,
            width: w,
            height: h,
            autoSize: false,
            openEffect: 'none',
            showLoading: true,
            closeClick: true,
            closeEffect: 'fade',
            'closeBtn': true,
            afterLoad: function () {
                this.title = '';//ShowTitle(pageTitle);
            },
            helpers: {
                overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                title: { type: 'inside' }
            },
            onComplete: function () {
                $("#fancybox-wrap").css({ 'top': '90px' });
            },
            onCleanup: function () {
                var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                if (hfPostBack == "Y")
                    window.location.reload(true);
            },
            afterClose: function () {
                //window.location.reload();

            }
        });
        return false;
    }

        </script>



        <script type="text/javascript" language="javascript">
            Sys.Application.add_load(
    function CheckForPrint() {
        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
              document.getElementById('<%= h_print.ClientID %>').value = '';
              Popup('../Reports/ASPX Report/RptViewerModal.aspx ');
          }
      }
    );

      function Popup(url) {
          //$.fancybox({
          //    'width': '80%',
          //    'height': '100%',
          //    'autoScale': true,
          //    'transitionIn': 'fade',
          //    'transitionOut': 'fade',
          //    'type': 'iframe',
          //    'href': url
          //});

          ShowWindowWithClose(url, '', '75%', '75%');
      };
        </script>
    </form>
</body>
</html>
