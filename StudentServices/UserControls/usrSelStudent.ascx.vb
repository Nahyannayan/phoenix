Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class UserControls_usrSelStudent
    Inherits System.Web.UI.UserControl
    Public Event StudentNoChanged As EventHandler

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Try
            

            Dim ds As DataSet = getTable()
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                h_Student_id.Value = ds.Tables(0).Rows(0)("STU_ID")
                txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
                txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
                h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
                h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
                h_ACD_ID.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")

                If IsStudent Then
                    h_StuStatus.Value = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
                    h_TCdate.Value = ds.Tables(0).Rows(0)("STU_CANCELDATE")
                    h_LDAdate.Value = ds.Tables(0).Rows(0)("STU_LASTATTDATE")
                    h_StuDOJ.Value = ds.Tables(0).Rows(0)("STU_DOJ")
                    H_STU_PAY_SCH_ID.Value = ds.Tables(0).Rows(0)("STU_PAY_SCH_ID")
                End If
            Else
                h_Student_id.Value = ""
                txtStudentname.Text = ""
                Dim lblError As Label = Parent.FindControl("lblError")
                If Not lblError Is Nothing Then
                    lblError.Text = "Student # Entered is not valid  !!!"
                End If
            End If
            RaiseEvent StudentNoChanged(Me, e)
        Catch ex As Exception
            Errorlog(ex.Message, "Student User Control")
        End Try
    End Sub
    Function getTable() As DataSet
        Dim dt As DataTable = Nothing
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSuid"))
        pParms(1) = New SqlClient.SqlParameter("@STU_NO", txtStdNo.Text.Trim)
        pParms(2) = New SqlClient.SqlParameter("@STUtype", IsStudent)
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.StoredProcedure, "FEES.GET_STUDENT", pParms)
        Return _table
    End Function

    Public ReadOnly Property STUDENT_NO() As String
        Get
            Return txtStdNo.Text
        End Get
    End Property

    Public Property SelectedDate() As Date
        Get
            Return h_SelDate.Value
        End Get
        Set(ByVal value As Date)
            h_SelDate.Value = value
        End Set
    End Property

    Public Property IsStudent() As Boolean
        Get
            Return h_isStudent.Value
        End Get
        Set(ByVal value As Boolean)
            If value Then
                h_isStudent.Value = "1"
            Else
                h_isStudent.Value = "0"
            End If
        End Set
    End Property

    Public Property IsWithDrawal() As Boolean
        Get
            Return h_REASON.Value
        End Get
        Set(ByVal value As Boolean)
            If value Then
                h_REASON.Value = 2
            Else
                h_REASON.Value = 1
            End If
        End Set
    End Property

    Public Property ACD_ID() As String
        Get
            Return h_ACD_ID.Value
        End Get
        Set(ByVal value As String)
            h_ACD_ID.Value = value
        End Set
    End Property

    Public Property STU_PAY_SCH_ID() As String
        Get
            Return H_STU_PAY_SCH_ID.Value
        End Get
        Set(ByVal value As String)
            H_STU_PAY_SCH_ID.Value = value
        End Set
    End Property


    Public Property GRM_DISPLAY() As String
        Get
            Return h_GrmDisplay.Value
        End Get
        Set(ByVal value As String)
            h_GrmDisplay.Value = value
        End Set
    End Property

    Public Property STU_STATUS() As String
        Get
            Return h_StuStatus.Value
        End Get
        Set(ByVal value As String)
            h_StuStatus.Value = value
        End Set
    End Property

    Public ReadOnly Property STUDENT_NAME() As String
        Get
            Return txtStudentname.Text
        End Get
    End Property

    Public ReadOnly Property STUDENT_ID() As String
        Get
            Return h_Student_id.Value
        End Get
    End Property

    Public Property STUDENT_GRADE_ID() As String
        Get
            Return h_GRD_ID.Value
        End Get
        Set(ByVal value As String)
            h_GRD_ID.Value = value
        End Set
    End Property

    Public WriteOnly Property IsReadOnly() As Boolean
        Set(ByVal value As Boolean)
            txtStdNo.ReadOnly = value
            imgStudent.Enabled = Not value
        End Set
    End Property
    Public Property TCDATE() As String
        Get
            Return h_TCdate.Value
        End Get
        Set(ByVal value As String)
            h_TCdate.Value = value
        End Set
    End Property

    Public Property LDADATE() As String
        Get
            Return h_LDAdate.Value
        End Get
        Set(ByVal value As String)
            h_LDAdate.Value = value
        End Set
    End Property
    Public Property STUDOJ() As String
        Get
            Return h_StuDOJ.Value
        End Get
        Set(ByVal value As String)
            h_StuDOJ.Value = value
        End Set
    End Property
    Public Sub ClearDetails()
        h_GRD_ID.Value = ""
        h_Student_id.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        h_GrmDisplay.Value = ""
        h_StuStatus.Value = "EN"
        h_LDAdate.Value = ""
        h_TCdate.Value = ""
        h_StuDOJ.Value = ""
    End Sub

    Public Sub ChangeColor(ByVal ChangeY As Boolean)
        If ChangeY = True Then
            txtStudentname.ForeColor = Drawing.Color.Red
            txtStdNo.ForeColor = Drawing.Color.Red
        Else
            txtStudentname.ForeColor = Drawing.Color.Black
            txtStdNo.ForeColor = Drawing.Color.Black
        End If
    End Sub

    Public Sub SetStudentDetails(ByVal STU_ID As String)
        Dim str_sql As String = String.Empty
        If IsStudent Then
            str_sql = "SELECT STU_ID,STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, " _
            & " isnull(STU_CURRSTATUS,'EN') STU_CURRSTATUS, STU_ACD_ID FROM VW_OSO_STUDENT_M" _
            & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_ID='" & STU_ID & "'"
        Else
            str_sql = "SELECT STU_ID, STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, STU_ACD_ID FROM FEES.vw_OSO_ENQUIRY_COMP" _
             & " WHERE (STU_BSU_ID = '" & Session("sBsuid") & "') AND STU_ID='" & STU_ID & "'"
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, Data.CommandType.Text, str_sql)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            h_Student_id.Value = ds.Tables(0).Rows(0)("STU_ID")
            txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
            h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
            h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
            h_ACD_ID.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")
            If IsStudent Then
                h_StuStatus.Value = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ImgLike.Attributes.Add("onclick", "return ImgInline()")
        If Not IsPostBack Then
            txtStudentname.Attributes.Add("onkeyup", "return SearchName('" & txtStudentname.ClientID & "')")
            txtStudentname.Attributes.Add("onclick", "return getList()")
            txtStudentname.Attributes.Add("Onkeypress", "return selectItem()")
            h_businessUnit.Value = CType(Me.Parent.FindControl("ddlBusinessUnit"), DropDownList).SelectedValue.ToString()
            IsWithDrawal = False
            IsStudent = True
        End If
        If h_Like.Value = "S" Then
            ImgLike.ImageUrl = "../Images/operations/startswith.gif"
        End If
    End Sub

    Public Sub SetStudentDetailsByNO(ByVal STU_NO As String)
        Dim str_data As String = String.Empty
        Dim str_sql As String = String.Empty
        If IsStudent Then
            str_sql = "SELECT  STU_ID,STU_NO, STU_NAME,GRD_DISPLAY, STU_GRD_ID , " _
            & " isnull(STU_CURRSTATUS,'EN') STU_CURRSTATUS, STU_ACD_ID FROM VW_OSO_STUDENT_M" _
            & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & STU_NO & "'"
        Else
            str_sql = "SELECT STU_ID, STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, STU_ACD_ID FROM FEES.vw_OSO_ENQUIRY_COMP" _
             & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & Session("sBsuid") & "') AND  STU_NO='" & STU_NO & "'"
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, Data.CommandType.Text, str_sql)
        If Not ds Is Nothing And ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                h_Student_id.Value = ds.Tables(0).Rows(0)("STU_ID")
                txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
                txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
                h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
                h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
                h_ACD_ID.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")
                If IsStudent Then
                    h_StuStatus.Value = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
                End If
            End If
        End If
    End Sub

End Class
