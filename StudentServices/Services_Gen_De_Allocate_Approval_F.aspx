﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services_Gen_De_Allocate_Approval_F.aspx.vb" Inherits="StudentServices_Services_Gen_De_Allocate_Approval_F" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <%--<link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <%-- <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>--%>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

    <link media="screen" href="../cssfiles/Popup.css" type="text/css" rel="Stylesheet" />

    <!-- Bootstrap header files ends here -->

    <script type="text/javascript" language="javascript">
        $(function () {

            $(".datepicker").datepicker({
                dateFormat: 'dd/M/yy',
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true
            });
            //$("#txtDate").datepicker('setDate', new Date());
        }
    );

        function fancyClose() {
            if ($("#<%=hf_bSuccess.ClientID%>").val() == 1) {
                parent.$.fancybox.close();
                parent.LoadServices(1);
            }
            else {
                parent.$.fancybox.close();
            }
            //parent.location.reload();
        }
        function DisableButton() {
            document.getElementById("<%=btnDeAllocate.ClientID%>").disabled = true;
        }
        window.onbeforeunload = DisableButton;
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblError" runat="server"></asp:Label>
        <asp:HiddenField ID="hf_bSuccess" runat="server" Value="0" />
        <center>
            <table style="width: 100%">
                <tr>
                    <td align="left" class="title-bg-lite">
                        <asp:Literal ID="ltHeader" runat="server" Text="Discontinue Approval"></asp:Literal>
                        <asp:HiddenField ID="hdnSelected" runat="server" />
                    </td>
                </tr>
            </table>
            <table width="100%" id="tblCategory">
                <tr>
                    <td align="left" width="30%"><span class="field-label">Student </span>
                    </td>
                    <td align="left" width="70%">
                        <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td align="left" width="30%"><span class="field-label">Activity </span>
                    </td>
                    <td align="left" width="70%">
                        <asp:Label ID="lblActivity" runat="server" CssClass="field-value"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="30%"><span class="field-label">Discontinue date</span>
                    </td>
                    <td align="left" width="70%">
                        <input type="text" id="txtDate" class="field-value" runat="server" />
                        <%--<asp:TextBox ID="txtDate" runat="server" Width="90px"></asp:TextBox> <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                            PopupButtonID="imgCalendar" TargetControlID="txtDate">
                        </ajaxToolkit:CalendarExtender>
                        &nbsp;<asp:ImageButton
                            ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="Attendance Date required" ForeColor=""
                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        <asp:Label ID="lblAllocationDt" runat="server" Visible="False"></asp:Label>--%>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="30%"><span class="field-label">Remarks</span>
                    </td>
                    <td align="left" width="70%">
                        <textarea id="txtRemarks" runat="server" cols="24" rows="6" readonly="readonly" class="field-value"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnDeAllocate"
                            Text="APPROVE" runat="server" CssClass="button" />
                        <input type="button" class="button" id="btnCancel1" title="Close" value="Close"
                            onclick="fancyClose()" />
                        <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
