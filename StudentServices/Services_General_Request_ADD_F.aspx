﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services_General_Request_ADD_F.aspx.vb" Inherits="StudentServices_Services_General_Request_ADD_F" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title></title>
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('[id$=chkHeader]').click(function () {
                $("[id$='chkChild']").attr('checked', this.checked);
            });
        });
    </script>
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">
        <div>
            <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Add " />
                        <asp:HiddenField ID="hdnSelected" runat="server" />
                    </td>
                </tr>
                <%-- <tr>
                    <td>&nbsp;</td>
                </tr>--%>
                <tr>
                    <td align="center">
                        <table id="tblData" width="100%" cellpadding="2" cellspacing="2">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="gvReq" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20" DataKeyNames="STU_ID" AllowPaging="True" EnableModelValidation="True" OnPageIndexChanging="gvReq_PageIndexChanging" BorderStyle="Solid">
                                        <RowStyle CssClass="griditem" Height="20px" Width="100%" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkHeader" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkChild" runat="server" />
                                                    <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Student No">
                                                <HeaderTemplate>
                                                    Student No
                                                    <br />
                                                    <asp:TextBox ID="txtSTU_NO" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearchSTU_NO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        ImageAlign="Top" OnClick="btnSearchSTU_NO_Click"></asp:ImageButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Student Name">
                                                <HeaderTemplate>
                                                    Student Name
                                                    <br />
                                                    <asp:TextBox ID="txtSNAME" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearchSNAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        ImageAlign="Top" OnClick="btnSearchSNAME_Click"></asp:ImageButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>
                                                    <asp:LinkButton ID="lbtnSname" runat="server" Text='<%# Bind("SNAME") %>' Visible="false"></asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                            </asp:TemplateField>
                                            <%--  <asp:TemplateField HeaderText="GRD_SCT">
                            <HeaderTemplate>
                                <table style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <td colspan="2" class="gridheader_text" align="center">
                                                Grade & Section
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="gridheader_text" align="left">
                                                <asp:TextBox ID="txtGRD_SCT" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td style="width: 18px" valign="middle">
                                                <asp:ImageButton ID="btnSearchGRD_SCT" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                    ImageAlign="Top" OnClick="btnSearchGRD_SCT_Click"></asp:ImageButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblGRD_SCT" runat="server" Text='<%# Bind("GRD_SCT") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                        </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Gender">
                                                <HeaderTemplate>
                                                    Gender<br />
                                                    <asp:TextBox ID="txtGender" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="btnSearchGender" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        ImageAlign="Top" OnClick="btnSearchGender_Click"></asp:ImageButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGender" runat="server" Text='<%# Bind("Gender") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="center" Wrap="False"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnAdd" runat="server" Text="ADD"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle BackColor="Aqua" />
                                        <HeaderStyle CssClass="gridheader_pop" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
