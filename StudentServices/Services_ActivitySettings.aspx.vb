﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Accounts_Services_Activity
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MainMnu_code As String
        Try
            If Not IsPostBack Then
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    ' Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    GetActivitySettings()
                    fillActivityTypes()
                    fillFeeTypes(ddlFeeCode)
                    GetCountryDtls(ddlCountry) 'Added by vikranth on 23rd Dec 2019
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GetActivitySettings()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ACTIVITY_TYPES_FORSETTINGS]")
            If dsActivityTypes.Tables.Count > 0 Then
                grdact_types.DataSource = dsActivityTypes.Tables(0)
                grdact_types.DataBind()
            End If
        
        Catch ex As Exception

        End Try        
    End Sub




    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub

    Protected Sub grdact_types_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim strans As SqlTransaction = Nothing
        Try
            If Not e.CommandArgument = "" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = DirectCast(grdact_types.Rows(index), GridViewRow)
                Dim lblActID As Label = row.FindControl("lblActID")
                Dim txtActName As TextBox = row.FindControl("txtActName")
                Dim txtActDescr As TextBox = row.FindControl("txtActDescr")
                Dim txtActIconPath As TextBox = row.FindControl("txtActIconPath")
                If e.CommandName = "Edit" Then                   
                    Dim btnEdit As LinkButton = row.Cells(4).Controls(0)
                    If btnEdit.Text = "Edit" Then
                        btnEdit.Text = "Save"
                        txtActName.Enabled = True
                        txtActDescr.Enabled = True
                        txtActIconPath.Enabled = True
                    ElseIf btnEdit.Text = "Save" Then
                        objConn.Open()
                        strans = objConn.BeginTransaction
                        Dim pParams(5) As SqlParameter
                        pParams(0) = Mainclass.CreateSqlParameter("@OPTION", "U", SqlDbType.Char)
                        pParams(1) = Mainclass.CreateSqlParameter("@ID", Convert.ToInt64(lblActID.Text), SqlDbType.BigInt)
                        pParams(2) = Mainclass.CreateSqlParameter("@NAME", txtActName.Text, SqlDbType.VarChar)
                        pParams(3) = Mainclass.CreateSqlParameter("@DESCR", txtActDescr.Text, SqlDbType.VarChar)
                        pParams(4) = Mainclass.CreateSqlParameter("@PATH", txtActIconPath.Text, SqlDbType.VarChar)
                        SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[INSERT_UPDATE_ACTIVITY_TYPES]", pParams)
                        strans.Commit()
                        GetActivitySettings()
                        ShowMessage("Successfully Updated", False)
                    End If
                ElseIf e.CommandName = "Delete" Then
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    Dim pParams(5) As SqlParameter
                    pParams(0) = Mainclass.CreateSqlParameter("@OPTION", "D", SqlDbType.Char)
                    pParams(1) = Mainclass.CreateSqlParameter("@ID", Convert.ToInt64(lblActID.Text), SqlDbType.BigInt)
                    pParams(2) = Mainclass.CreateSqlParameter("@NAME", txtActName.Text, SqlDbType.VarChar)
                    pParams(3) = Mainclass.CreateSqlParameter("@DESCR", txtActDescr.Text, SqlDbType.VarChar)
                    pParams(4) = Mainclass.CreateSqlParameter("@PATH", txtActIconPath.Text, SqlDbType.VarChar)
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[INSERT_UPDATE_ACTIVITY_TYPES]", pParams)
                    strans.Commit()
                    GetActivitySettings()
                    ShowMessage("Successfully Deleted", False)
                End If
            End If

        Catch ex As Exception
            strans.Rollback()
        End Try

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim strans As SqlTransaction = Nothing
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim pParams(5) As SqlParameter
            pParams(0) = Mainclass.CreateSqlParameter("@OPTION", "I", SqlDbType.Char)
            pParams(1) = Mainclass.CreateSqlParameter("@ID", 0, SqlDbType.BigInt)
            pParams(2) = Mainclass.CreateSqlParameter("@NAME", txtAct_Name.Text, SqlDbType.VarChar)
            pParams(3) = Mainclass.CreateSqlParameter("@DESCR", txtAct_Descr.Text, SqlDbType.VarChar)
            pParams(4) = Mainclass.CreateSqlParameter("@PATH", txtAct_Path.Text, SqlDbType.VarChar)
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[INSERT_UPDATE_ACTIVITY_TYPES]", pParams)
            strans.Commit()
            GetActivitySettings()
            ShowMessage("Successfully Inserted New Activity Type", False)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdact_types_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            'Commented below code by vikranth on 30th Dec 2019 for inner grid
            'Dim gvOrders As GridView = TryCast(e.Row.FindControl("gvOrders"), GridView)
            'Dim lblActID As Label = TryCast(e.Row.FindControl("lblActID"), Label)

            ''bind fee types 

            'Dim conn As String = ConnectionManger.GetOASISConnectionString
            'Dim param(2) As SqlParameter
            'param(0) = Mainclass.CreateSqlParameter("@REF_ID", lblActID.Text, SqlDbType.BigInt)
            'param(1) = Mainclass.CreateSqlParameter("@CNTY_ID", Session("BSU_COUNTRY_ID"), SqlDbType.BigInt)
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "OASIS.GET_SUBACT_TYPES_DETAILS", param)
            'If ds.Tables.Count > 0 Then
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        gvOrders.DataSource = ds.Tables(0)
            '        gvOrders.DataBind()
            '    End If
            'End If
        End If
        'OASIS.GET_TRIPS_DETAILS

    End Sub
    Protected Sub fillActivityTypes()
        'FILLING ACTIVITY TYPES TO ACTIVITY DROP DOWN LIST
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsActivityTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_ALL_ACTIVITY_TYPES]")
            ddlActiTypes.DataSource = dsActivityTypes.Tables(0)
            ddlActiTypes.DataBind()
            ddlActiTypes.DataTextField = "ACTIVITY_NAME"
            ddlActiTypes.DataValueField = "ACTIVITY_ID"
            ddlActiTypes.DataBind()
            ddlActiTypes.Items.Insert(0, New ListItem("SELECT", 0))
            ddlActiTypes.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From fillActivityTypes - ActivitySettings: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Private Sub fillFeeTypes(ByRef ddl As DropDownList)
        'METHODE TO FILL FEE TYPES
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@CheckOption", 1, SqlDbType.Int)
            Dim dsFeeTypes As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_REGISTRATION_FEE_DETAILS]", Param)
            ddl.DataSource = dsFeeTypes.Tables(0)
            ddl.DataTextField = "DESCR"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("SELECT", 0))
            ddl.Items.FindByText("SELECT").Selected = True
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From FillFeetypes: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    'Added by vikranth on 23rd Dec 2019
    Public Sub GetCountryDtls(ByRef ddl As DropDownList)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            str_Sql = "Select ID, C_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as C_Name FROM COUNTRY_M)a where a.ID<>'' and C_Name <> '-' and a.ID not in (0,5,252)"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddl.DataSource = ds.Tables(0)
            ddl.DataTextField = "C_Name"
            ddl.DataValueField = "ID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("SELECT", 0))
            ddl.Items.FindByValue(Session("BSU_COUNTRY_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog("From GetCountryDtls: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub gvOrders_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        'OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim parent As GridViewRow = DirectCast(sender.parent.parent, GridViewRow)
        Dim lblActID As Label = parent.FindControl("lblActID")
        Dim objConn As New SqlConnection(str_conn)
        Dim strans As SqlTransaction = Nothing
        Try
            If Not e.CommandArgument = "" Then
                Dim index2 As Integer = Convert.ToInt32(e.CommandArgument)
                Dim gvOrders As GridView = DirectCast(sender, GridView)
                Dim row As GridViewRow = DirectCast(gvOrders.Rows(index2), GridViewRow)
                Dim txtgrdatmid As TextBox = row.FindControl("txtgrdatmid")
                Dim txtgrdactname As TextBox = row.FindControl("txtgrdactname")
                Dim txtgrdactdescr As TextBox = row.FindControl("txtgrdactdescr")
                Dim hidfeecode As HiddenField = row.FindControl("hidfeecode")
                Dim ddlgrdfeetype As DropDownList = row.FindControl("ddlgrdfeetype")
                Dim ddlgrdCountry As DropDownList = row.FindControl("ddlgrdCountry") 'Added  by vikranth on 23rd Dec 2019

                If e.CommandName = "Edit" Then
                    Dim btnEdit1 As LinkButton = row.Cells(5).Controls(0)
                    If btnEdit1.Text = "Edit" Then
                        btnEdit1.Text = "Save"
                        txtgrdactname.Enabled = True
                        txtgrdactdescr.Enabled = True
                        ddlgrdfeetype.Enabled = True
                        ddlgrdCountry.Enabled = True 'Added  by vikranth on 23rd Dec 2019
                    ElseIf btnEdit1.Text = "Save" Then
                        objConn.Open()
                        strans = objConn.BeginTransaction
                        Dim pParams(8) As SqlParameter
                        pParams(0) = New SqlClient.SqlParameter("@ATM_NAME", txtgrdactname.Text)
                        pParams(1) = New SqlClient.SqlParameter("@ATM_DESCR", txtgrdactdescr.Text)
                        pParams(2) = New SqlClient.SqlParameter("@ATM_FEE_CODE", ddlgrdfeetype.SelectedValue)
                        pParams(3) = New SqlClient.SqlParameter("@ATM_REF_ID", lblActID.Text)
                        pParams(4) = New SqlClient.SqlParameter("@ATM_ID", CInt(txtgrdatmid.Text))
                        pParams(5) = New SqlClient.SqlParameter("@USER", EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name")))
                        'pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", Session("BSU_COUNTRY_ID")) 'Commented by vikranth on 23rd Dec 2019
                        pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", ddlgrdCountry.SelectedValue) 'Added by vikranth on 23rd dec 2019
                        pParams(7) = New SqlClient.SqlParameter("@TYPE", "U")
                        pParams(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                        pParams(8).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES", pParams)
                        If (pParams(8).Value = 0) Then
                            strans.Commit()
                            GetActivitySettings()
                            lblError.Text = "Record Updated Successfully"
                        Else
                            strans.Rollback()
                            lblError.Text = "Error Occured during processing"
                        End If
                    End If

                ElseIf e.CommandName = "Delete" Then
                    objConn.Open()
                    strans = objConn.BeginTransaction
                    Dim pParams(8) As SqlParameter
                    pParams(0) = New SqlClient.SqlParameter("@ATM_NAME", txtgrdactname.Text)
                    pParams(1) = New SqlClient.SqlParameter("@ATM_DESCR", txtgrdactdescr.Text)
                    pParams(2) = New SqlClient.SqlParameter("@ATM_FEE_CODE", ddlgrdfeetype.SelectedValue)
                    pParams(3) = New SqlClient.SqlParameter("@ATM_REF_ID", lblActID.Text)
                    pParams(4) = New SqlClient.SqlParameter("@ATM_ID", CInt(txtgrdatmid.Text))
                    pParams(5) = New SqlClient.SqlParameter("@USER", EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name")))
                    'pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", Session("BSU_COUNTRY_ID")) 'Commented by vikranth on 23rd Dec 2019
                    pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", ddlgrdCountry.SelectedValue) 'Added by vikranth on 23rd dec 2019
                    pParams(7) = New SqlClient.SqlParameter("@TYPE", "D")
                    pParams(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                    pParams(8).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES", pParams)
                    If (pParams(8).Value = 0) Then
                        strans.Commit()
                        GetActivitySettings()
                        lblError.Text = "Record Deleted Successfully"
                    Else
                        strans.Rollback()
                        lblError.Text = "Error Occured during processing"
                    End If
                End If
            End If
        Catch ex As Exception
            strans.Rollback()
        End Try

    End Sub


    Protected Sub btnSaveSub_Click(sender As Object, e As EventArgs)
        'OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES

        If ddlActiTypes.SelectedValue = 0 Then
            lblError.Text = ""
            Exit Sub
        ElseIf ddlFeeCode.SelectedValue = 0 Then
            lblError.Text = ""
            Exit Sub
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Dim strans As SqlTransaction = Nothing
            Try
                objConn.Open()
                strans = objConn.BeginTransaction
                Dim pParams(8) As SqlParameter
                pParams(0) = New SqlClient.SqlParameter("@ATM_NAME", txtSubName.Text)
                pParams(1) = New SqlClient.SqlParameter("@ATM_DESCR", txtSubDecsr.Text)
                pParams(2) = New SqlClient.SqlParameter("@ATM_FEE_CODE", ddlFeeCode.SelectedValue)
                pParams(3) = New SqlClient.SqlParameter("@ATM_REF_ID", ddlActiTypes.SelectedValue)
                pParams(4) = New SqlClient.SqlParameter("@ATM_ID", 0)
                pParams(5) = New SqlClient.SqlParameter("@USER", EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name")))
                'pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", Session("BSU_COUNTRY_ID")) 'Commented by vikranth on 23rd Dec 2019
                pParams(6) = New SqlClient.SqlParameter("@ATM_CNTY_CODE", ddlCountry.SelectedValue) 'Added by vikranth on 23rd dec 2019
                pParams(7) = New SqlClient.SqlParameter("@TYPE", "I")
                pParams(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                pParams(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "OASIS.INSERT_UPDATE_DELETE_SUB_ACT_TYPES", pParams)
                If (pParams(8).Value = 0) Then
                    strans.Commit()
                    GetActivitySettings()
                    lblError.Text = "Record Added Successfully"
                Else
                    strans.Rollback()
                    lblError.Text = "Error Occured during processing"
                End If

            Catch ex As Exception
                strans.rollback()
                lblError.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Sub gvOrders_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlgrdfeetype As DropDownList = TryCast(e.Row.FindControl("ddlgrdfeetype"), DropDownList)
            Dim hidfeecode As HiddenField = TryCast(e.Row.FindControl("hidfeecode"), HiddenField)
            Dim ddlgrdCountry As DropDownList = TryCast(e.Row.FindControl("ddlgrdCountry"), DropDownList) 'Added by vikranth on 23rd Dec 2019
            Dim hidCountrycode As HiddenField = TryCast(e.Row.FindControl("hidCountrycode"), HiddenField) 'Added by vikranth on 23rd Dec 2019
            fillFeeTypes(ddlgrdfeetype)
            GetCountryDtls(ddlgrdCountry) 'Added by vikranth on 23rd Dec 2019
            ddlgrdfeetype.ClearSelection()
            ddlgrdfeetype.Items.FindByValue(hidfeecode.Value).Selected = True
            ddlgrdfeetype.Enabled = False
            ddlgrdCountry.Enabled = False  'Added by vikranth on 23rd Dec 2019
        End If      
    End Sub

    Protected Sub grdact_types_RowEditing(sender As Object, e As GridViewEditEventArgs)
    End Sub

    Protected Sub gvOrders_RowEditing(sender As Object, e As GridViewEditEventArgs)
    End Sub

    Protected Sub gvOrders_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
    End Sub

    Protected Sub grdact_types_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
    End Sub
End Class

