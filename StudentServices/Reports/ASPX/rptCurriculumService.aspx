﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCurriculumService.aspx.vb" Inherits="Students_Reports_ASPX_rptCurriculumService" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/StudentServices/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Service Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" cellpadding="2" cellspacing="0"
                                width="100%">


                                <tr id="trBSUnit" runat="server">
                                    <td align="left" valign="top" width="20%"><span class="field-label">Business Unit</span></td>
                                    <td align="left" valign="top" width="30%">
                                          <div class="checkbox-list">
                                        <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" /></div>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                              


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Service</span></td>
                                    <td align="left" width="30%"
                                         >
                                        <div class="checkbox-list">
                                        <asp:TreeView ID="trvServiceTypes" runat="server"
                                            onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                                        </asp:TreeView></div>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Status</span></td>
                                    <td align="left" width="30%">
                                        <asp:RadioButtonList ID="rbStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">Active</asp:ListItem>
                                            <asp:ListItem>Discontinued</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>

                             

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Service Date Between</span></td>

                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:TextBox ID="txtFromdate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtFromdate_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtFromdate" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtFromdate" PopupButtonID="imgFromDate"
                                            Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                        &nbsp; And
                            <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtTodate_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtTodate" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgtoDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtTodate" PopupButtonID="imgtoDate"
                                            Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

