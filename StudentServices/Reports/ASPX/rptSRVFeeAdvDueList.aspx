﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSRVFeeAdvDueList.aspx.vb" Inherits="StudentServices_Reports_ASPX_rptSRVFeeAdvDueList" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="~/StudentServices/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                            <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list-full">
                                <uc1:usrBSUnits ID="usrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Fee Type</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list-full">
                                <asp:TreeView ID="trvFeeTypes" runat="server"
                                    onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                                </asp:TreeView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Ason Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="rbGroupBSU" runat="server" Checked="True" GroupName="GB" Text="Groupby BSU" CssClass="field-label" />
                            <asp:RadioButton ID="rbGroupSRV" runat="server" GroupName="GB" Text="Groupby Service" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_STUD_ID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>


