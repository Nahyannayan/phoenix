﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class StudentServices_Reports_ASPX_rptECAFeeStudentsLedger
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            BindBusinessUnit()
            '' ddBusinessunit.Items.FindByValue(Session("sBsuid")).Selected = True
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            gvStudentDetails.Attributes.Add("bordercolor", "#1b80b6")
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case OASISConstants.MNU_ECA_FEE_STUDENT_LEDGER
                    lblReportCaption.Text = "GMA Student Ledger"
                    h_Single.Value = "0"
                Case "F703011"
                    lblReportCaption.Text = "Student Fee Receipts Summary"
                    h_Single.Value = "1"
                    TdF.Style("display") = "none"
                    TdF2.Style("display") = "none"
            End Select
            'txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            'txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
            '' SetAcademicYearDate()
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            ' lblError.Text = "No Records with specified condition"
            usrMessageBar.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            '   lblError.Text = ""
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnStudentAudit)
    End Sub

    Sub BindBusinessUnit()
        ddBusinessunit.Items.Clear()



        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter


        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddBusinessunit.DataSource = ds.Tables(0)
            ddBusinessunit.DataTextField = "BSU_NAME"
            ddBusinessunit.DataValueField = "BSU_ID"
            ddBusinessunit.DataBind()
            ddBusinessunit.SelectedIndex = -1
            ddBusinessunit.Items.Insert(0, New ListItem("Please Select", "0"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub

    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If h_STUD_ID.Value = "" Then
            '   lblError.Text = "Please Select Student(s)"
            usrMessageBar.ShowNotification("Please Select Student(s)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If IsFutureDate_Ledger(txtFromDate.Text, txtToDate.Text) Then
            ' lblError.Text = "Invalid Date !!!"
            usrMessageBar.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_ECA_FEE_STUDENT_LEDGER
                F_rptStudentLedger()
            Case "F703011"
                ReceiptSummary()
        End Select
    End Sub

    Public Function IsFutureDate_Ledger(ByVal FromDate As String, Optional ByVal ToDate As String = "") As Boolean
        Dim str_allowfuturedate As Boolean = UtilityObj.GetDataFromSQL("SELECT isnull(USR_bITSupport,0) FROM USERS_M WHERE (USR_NAME = '" & Session("sUsr_name") & "')", ConnectionManger.GetOASISConnectionString)
        If ToDate = "" Then
            If Not IsDate(FromDate) Then
                Return True
            End If
            If Not HttpContext.Current.Session("sBusper") Then
                If CDate(FromDate) > Now.Date Then
                    Return True
                End If
            End If
        Else
            If Not IsDate(FromDate) Or Not IsDate(ToDate) Then
                Return True
            End If
            If CDate(FromDate) > CDate(ToDate) Then
                Return True
            End If
            If Not HttpContext.Current.Session("sBusper") And Not str_allowfuturedate Then
                If CDate(FromDate) > Now.Date Or CDate(ToDate) > Now.Date Then
                    Return True
                End If
            End If
        End If
        Return False
    End Function

    Private Sub F_rptStudentLedger()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("FEES.F_rptStudentLedger")
        Dim cmdSub As New SqlCommand("[FEES].[F_rptStudentLedgerSummary]")
        Dim cmdSub2 As New SqlCommand("[FEES].[F_rptStudentPDCData]")
        cmd.CommandType = CommandType.StoredProcedure
        cmdSub.CommandType = CommandType.StoredProcedure
        cmdSub2.CommandType = CommandType.StoredProcedure

        'ALTER procedure rptStudentLedger
        ' @BSU_ID VARCHAR(20)='125016',
        '@STU_IDS VARCHAR(2000)='',
        '@STU_TYPE VARCHAR(5)='S',
        '@FromDT DATETIME ='1-JUN-2008',
        '@ToDT DATETIME='1-JUN-2008'

        Dim sqlpSTU_IDS As New SqlParameter("@STU_IDS", SqlDbType.Xml)
        sqlpSTU_IDS.Value = UtilityObj.GenerateXML(h_STUD_ID.Value.Replace(",", "||"), XMLType.STUDENT)
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)
        cmdSub2.Parameters.AddWithValue("@STU_IDS", sqlpSTU_IDS.Value)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub.Parameters.AddWithValue("@BSU_ID", ddBusinessunit.SelectedItem.Value)
        cmdSub2.Parameters.AddWithValue("@BSU_ID", ddBusinessunit.SelectedItem.Value)


        Dim sqlpPROVIDER_BSU_ID As New SqlParameter("@PROVIDER_BSU_ID", SqlDbType.VarChar, 20)
        sqlpPROVIDER_BSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpPROVIDER_BSU_ID)
        cmdSub.Parameters.AddWithValue("@PROVIDER_BSU_ID", Session("sBsuid"))
        cmdSub2.Parameters.AddWithValue("@PROVIDER_BSU_ID", Session("sBsuid"))



        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        If radEnquiry.Checked Then
            sqlpSTU_TYPE.Value = "E"
        Else
            sqlpSTU_TYPE.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYPE)
        cmdSub.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)
        cmdSub2.Parameters.AddWithValue("@STU_TYPE", sqlpSTU_TYPE.Value)


        Dim sqlpFromDT As New SqlParameter("@FromDT", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub.Parameters.AddWithValue("@FromDT", CDate(txtFromDate.Text))
        cmdSub2.Parameters.AddWithValue("@FromDT", CDate(txtFromDate.Text))

        Dim sqlpTODT As New SqlParameter("@ToDT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)
        cmdSub.Parameters.AddWithValue("@ToDT", CDate(txtToDate.Text))
        cmdSub2.Parameters.AddWithValue("@ToDT", CDate(txtToDate.Text))

        GetBSU_RoundOff(ddBusinessunit.SelectedValue)
        'cmdSub.Parameters.AddWithValue("RoundOffVal", ViewState("RoundOffVal"))
        'cmdSub2.Parameters.AddWithValue("RoundOffVal", ViewState("RoundOffVal"))
        'cmdSub.Parameters.AddWithValue("userName", Session("sUsr_name"))

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub.Connection = New SqlConnection(str_conn)
        cmdSub2.Connection = New SqlConnection(str_conn)
        Dim repSourceSubRep(2) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub
        repSourceSubRep(1) = New MyReportClass
        repSourceSubRep(1).Command = cmdSub2
        Dim repSource As New MyReportClass

        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDate") = txtFromDate.Text
        params("ToDate") = txtToDate.Text
        params("RoundOffVal") = ViewState("RoundOffVal")
        If radEnquiry.Checked Then
            params("STU_TYPE") = "E"
        Else
            params("STU_TYPE") = "S"
        End If

       

        repSource.SubReport = repSourceSubRep

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = Session("sBsuid") 'ddBusinessunit.SelectedItem.Value
        'repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFeeStudentLedger.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection2()
    End Sub

    Private Sub ReceiptSummary()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("[FEES].[StudentFeeDetails]")
        Dim cmdSub As New SqlCommand("[FEES].[StudentFeeDetails_Receiptdetails]")

        cmd.CommandType = CommandType.StoredProcedure
        cmdSub.CommandType = CommandType.StoredProcedure

        Dim sqlpSTU_IDS As New SqlParameter("@STU_ID", SqlDbType.Int)
        sqlpSTU_IDS.Value = h_STUD_ID.Value.Replace("||", "")
        cmd.Parameters.Add(sqlpSTU_IDS)
        cmdSub.Parameters.AddWithValue("@STU_ID", h_STUD_ID.Value.Replace("||", ""))

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = ddBusinessunit.SelectedItem.Value
        cmd.Parameters.Add(sqlpBSU_ID)
        cmdSub.Parameters.AddWithValue("@BSU_ID", ddBusinessunit.SelectedItem.Value)

        Dim sqlpSTU_TYPE As New SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        If radEnquiry.Checked Then
            sqlpSTU_TYPE.Value = "E"
        Else
            sqlpSTU_TYPE.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYPE)

        Dim sqlpFromDT As New SqlParameter("@DOCDT", SqlDbType.VarChar)
        sqlpFromDT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpFromDT)
        cmdSub.Parameters.AddWithValue("@DOCDT", txtToDate.Text)

        cmd.Connection = New SqlConnection(str_conn)
        cmdSub.Connection = New SqlConnection(str_conn)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        repSourceSubRep(0).Command = cmdSub

        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("ReportHead") = "Student Fee Receipts Summary As On : " & txtToDate.Text
        params("BsuName") = ddBusinessunit.SelectedItem.Text.ToUpper()
        'params("ToDate") = txtToDate.Text
        'If radEnquiry.Checked Then
        '    params("STU_TYPE") = "E"
        'Else
        '    params("STU_TYPE") = "S"
        'End If

        repSource.SubReport = repSourceSubRep
        repSource.Parameter = params
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        repSource.HeaderBSUID = ddBusinessunit.SelectedItem.Value
        'repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../OASIS_SERVICES/REPORTS/RPT/StudentFeeReceiptSummaryMain.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection2()
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim lblBSUID As New Label
        'lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        'If Not lblBSUID Is Nothing Then
        '    h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
        '    If Not FillBSUNames(h_BSUID.Value) Then
        '        h_BSUID.Value = lblBSUID.Text
        '    End If
        '    grdBSU.PageIndex = grdBSU.PageIndex
        '    FillBSUNames(h_BSUID.Value)
        'End If
    End Sub

    Protected Sub lblAddNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblAddNewStudent.Click
        AddStudent()
    End Sub

    Private Sub AddStudent()
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If MainMnu_code = "F703011" And gvStudentDetails.Rows.Count > 0 Then

        Else
            If txtStudName.Text <> "" Then
                Dim str_stuid As String = ECA_Fee_Management.GetECAStudentID(txtStudName.Text, ddBusinessunit.SelectedItem.Value, radEnquiry.Checked)
                If str_stuid <> "" Then
                    If h_STUD_ID.Value <> "" Then
                        If h_STUD_ID.Value.EndsWith(",") Then
                            h_STUD_ID.Value = h_STUD_ID.Value & str_stuid
                        Else
                            h_STUD_ID.Value = h_STUD_ID.Value & "," & str_stuid
                        End If
                    Else
                        h_STUD_ID.Value = str_stuid
                    End If

                    txtStudName.Text = ""
                    FillSTUNames(h_STUD_ID.Value)
                End If

            End If
        End If

    End Sub

    Private Function FillSTUNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split(",")
        Dim condition As String = String.Empty
        Dim str_Sql As String
        Dim ds As DataSet

        If EMPIDs <> "" Then
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                '  i += 1
            Next
        End If


        If EMPIDs <> "" Then
            If radEnquiry.Checked Then
                str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP WHERE STU_ID IN (" + EMPIDs + ")"
            Else
                str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + EMPIDs + ")"
            End If
        Else
            If radEnquiry.Checked Then
                str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP WHERE STU_ID IN (" + condition + ")"
            Else
                str_Sql = " SELECT STU_NO , STU_NAME FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID IN (" + condition + ")"
            End If
        End If

        If condition <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, str_Sql)
            gvStudentDetails.DataSource = ds
            gvStudentDetails.DataBind()

            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
        End If


        Return True
    End Function

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub gvStudentDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentDetails.PageIndexChanging
        gvStudentDetails.PageIndex = e.NewPageIndex
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub ddBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddBusinessunit.SelectedIndexChanged
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
        SetAcademicYearDate()
    End Sub

    Protected Sub txtStudName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStudName.TextChanged
        AddStudent()
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub radEnquiry_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_STUD_ID.Value = ""
        FillSTUNames(h_STUD_ID.Value)
    End Sub

    Protected Sub btnStudentAudit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStudentAudit.Click
        If h_STUD_ID.Value = "" Then
            '   lblError.Text = "Please Select Student(s)"
            usrMessageBar.ShowNotification("Please Select Student(s)", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        CallStudentAuditReport()
    End Sub

    Sub CallStudentAuditReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim str_query As String = "SELECT STU_NAME,GRM_DISPLAY,SCT_DESCR,STU_CURRSTATUS,STU_NO FROM FEES.VW_OSO_STUDENT_DETAILS WHERE STU_ID in (" + h_STUD_ID.Value + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim param As New Hashtable

        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        With ds.Tables(0).Rows(0)
            param.Add("studName", .Item(0))
            param.Add("Grade", .Item(1))
            param.Add("Section", .Item(2))
            param.Add("StuNo", .Item(4))
            param.Add("status", .Item(3))
        End With
        param.Add("stu_id", h_STUD_ID.Value.Replace("|", ""))
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis"
            .reportPath = Server.MapPath("../../../Students/Reports/RPT/rptstudAudit.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        '   Response.Write("<script>window.open('../../../Reports/ASPX Report/rptReportViewer_princi.aspx');</script>")

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub SetAcademicYearDate()
        Dim DTFROM As String = String.Empty
        Dim DTTO As String = String.Empty
        FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, FeeCommon.GetCurrentAcademicYear(ddBusinessunit.SelectedValue), ddBusinessunit.SelectedItem.Value)
        txtFromDate.Text = DTFROM
        'txtToDate.Text = DTTO
        If txtFromDate.Text = "" Then
            txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
        End If
        txtToDate.Text = Format(Now.Date, OASISConstants.DateFormat)
    End Sub

    
End Class
