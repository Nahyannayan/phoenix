﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptECATRNFEECollectionBankWise.aspx.vb" Inherits="StudentServices_Reports_ASPX_rptECATRNFEECollectionBankWise" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Src="~/StudentServices/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetFEECounter() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            if (document.getElementById('<%=h_bTransport.ClientID %>').value == 1) {
                type = 'FEE_TRN_USER';
            }
            else {
                type = 'FEE_ECA_RPT_USER';
            }
            //result = window.showModalDialog("../../../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "", sFeatures)
            var oWnd = radopen("../../../Common/PopupForm.aspx?multiSelect=false&ID=" + type, "pop_fee");
            <%--if (result != "" && result != "undefined") {
                NameandCode = result.split('___');
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
            }
            return false;--%>
        }
        function CheckOnPostback() {
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'none') {

            }
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments           
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                //alert(arg.NameandCode);
                document.getElementById('<%=hfFEECounter.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtFeeCounter.ClientID %>').value = NameandCode[1];
                <%--__doPostBack('<%= txtFeeCounter.ClientID%>', 'TextChanged');--%>
            }
        }
    </script>
     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_fee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%-- <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">As Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trBSUnit" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="2">
                            <div class="checkbox-list-full">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr id="TrUsrName" runat="server">
                        <td align="left" width="20%"><span class="field-label">User Name</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFeeCounter" runat="server">
                            </asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClientClick=" GetFEECounter(); return false;"></asp:ImageButton>&nbsp;</td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr id="trChkPDC" runat="server" visible="false">
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="rbDetailed" runat="server" Checked="True" GroupName="pdc" Text="Detailed" CssClass="field-label" />
                            <asp:RadioButton ID="rbSummary" runat="server" GroupName="pdc" Text="Summary" CssClass="field-label" />
                            <asp:RadioButton ID="rbBank" runat="server" GroupName="pdc" Text="Bank Details" CssClass="field-label" /></td>
                    </tr>
                    <tr runat="server" id="trChkConciliation">
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkSummary" runat="server" Text="Summary" CssClass="field-label"></asp:CheckBox>
                            <asp:CheckBox ID="chkPDCOnly" runat="server" Text="PDC Only" Visible="false" CssClass="field-label" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:CheckBox ID="chkFC" runat="server"
                                Text="Show Amount in Foreign Currency" CssClass="field-label" />
                            <asp:CheckBox ID="CheckEmployee" runat="server" Text="View All Users" Visible="false" CssClass="field-label" />
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                &nbsp;&nbsp;
    <input id="hfBSU" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="h_bTransport" runat="server" type="hidden" />
                <input id="hfFEECounter" runat="server" type="hidden" />

            </div>
        </div>
    </div>
</asp:Content>


