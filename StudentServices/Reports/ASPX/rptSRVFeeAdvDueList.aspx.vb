﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class StudentServices_Reports_ASPX_rptSRVFeeAdvDueList
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Me.usrBSUnits1.MenuCode = MainMnu_code
            BindFeeTypes()
            SelectAllTreeView(trvFeeTypes.Nodes, True)
            For i As Integer = 0 To trvFeeTypes.Nodes.Count - 1
                trvFeeTypes.Nodes(i).CollapseAll()
            Next
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            Select Case MainMnu_code
                Case "F720101"
                    lblReportCaption.Text = "Service Fee Due Details"
                Case "F720101"
                    lblReportCaption.Text = "Service Fee Advance Details"
            End Select
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            Dim DTFROM As String = String.Empty
            Dim DTTO As String = String.Empty
            FeeCommon.AcademicYearStartEndDate(DTFROM, DTTO, Session("Current_ACD_ID"), Session("sBsuid"))
            txtFromDate.Text = DTFROM
            If txtFromDate.Text = "" Then
                txtFromDate.Text = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            End If
        End If


        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If Not IsDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        GenerateServiceFeeAdvDueList(MainMnu_code)
    End Sub

    Private Sub GenerateServiceFeeAdvDueList(ByVal MenuCode As String)
        Dim cmd As New SqlCommand("SM.GET_SVC_ADVANCE_DUE_LIST")
        Dim caption As String = ""
        cmd.CommandType = CommandType.StoredProcedure
        UpdateFeesSelected()

        Dim sqlParam(4) As SqlClient.SqlParameter

        sqlParam(0) = New SqlParameter("@STU_BSU_IDs", SqlDbType.VarChar)
        sqlParam(0).Value = usrBSUnits1.GetSelectedNode().Replace("||", "|")
        cmd.Parameters.Add(sqlParam(0))

        sqlParam(1) = New SqlParameter("@BSU_ID", SqlDbType.VarChar)
        sqlParam(1).Value = Session("sBsuid")
        cmd.Parameters.Add(sqlParam(1))

        sqlParam(2) = New SqlParameter("@MODE", SqlDbType.VarChar)
        If MenuCode = "F720101" Then
            sqlParam(2).Value = "DUE"
            caption = "SERVICE FEE DUE LIST"
        ElseIf MenuCode = "F720100" Then
            sqlParam(2).Value = "ADV"
            caption = "SERVICE FEE ADVANCE LIST"
        End If
        cmd.Parameters.Add(sqlParam(2))

        sqlParam(3) = New SqlParameter("@ASONDT", SqlDbType.VarChar)
        sqlParam(3).Value = txtFromDate.Text
        cmd.Parameters.Add(sqlParam(3))

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)

        Dim repSource As New MyReportClass
        Dim params As New Hashtable

        params("userName") = Session("sUsr_name")
        params("FROMDT") = txtFromDate.Text
        params("RPT_CAPTION") = caption

        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        If rbGroupBSU.Checked Then
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptSVCFeeAdvance_BSU.rpt"
        ElseIf rbGroupSRV.Checked Then
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptSVCFeeAdvance_SRV.rpt"
        End If


        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx?isExport=true", True)
            ReportLoadSelectionExport()
        Else
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        End If
    End Sub

    Protected Sub lnkExporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExporttoexcel.Click
        ViewState("isExport") = True
        btnGenerateReport_Click(sender, e)
    End Sub

    Public Sub BindFeeTypes()
        trvFeeTypes.Nodes.Clear()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty ' SSC_ID hard coded as 4(Others)
        str_Sql = " SELECT DISTINCT ISNULL(SSC_ID,4) SSC_ID,LTRIM(RTRIM(ISNULL(SC.SSC_DESC,'Others'))) SSC_DESC " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID ORDER BY SSC_ID,SSC_DESC"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("SSC_DESC").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("SSC_ID").ToString()
                trvFeeTypes.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In trvFeeTypes.Nodes
            BindChildNodes(node)
        Next
        trvFeeTypes.ExpandAll()
    End Sub
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim childnodeValue = ParentNode.Value
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR " & _
                  "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
                  "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID WHERE ISNULL(SSC_ID,4)='" & childnodeValue & "' " & _
                  "ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("FEE_DESCR").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("FEE_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If
        'For Each node As TreeNode In ParentNode.ChildNodes
        '    BindChildNodes(node)
        'Next
    End Sub

    Private Sub UpdateFeesSelected()
        Try
            Dim FEE_IDs As String = ""
            For Each node As TreeNode In trvFeeTypes.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If FEE_IDs.Trim() = "" Then
                    FEE_IDs = node.Value
                Else
                    FEE_IDs = FEE_IDs + "|" + node.Value
                End If
            Next
            ViewState("FEE_IDs") = FEE_IDs
        Catch
            ViewState("FEE_IDs") = ""
        End Try
    End Sub
    Private Sub SelectAllTreeView(ByVal nodes As TreeNodeCollection, ByVal selAll As Boolean)
        For Each node As TreeNode In nodes
            node.Checked = selAll
            If node.ChildNodes.Count > 0 Then
                SelectAllTreeView(node.ChildNodes, selAll)
            End If
        Next
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
    Sub ReportLoadSelectionExport()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx?isExport=true','_blank');", True)
        End If
    End Sub
End Class