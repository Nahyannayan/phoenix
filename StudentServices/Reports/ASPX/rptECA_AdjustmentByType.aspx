<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptECA_AdjustmentByType.aspx.vb" Inherits="rptECA_AdjustmentByType" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
        ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
    <br />
    <table align="center" class="BlueTable" cellpadding="4" cellspacing="0" style="width: 65%;">
        <tr class="subheader_img">
            <td align="left" colspan="4" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
        </tr>
        <tr runat="server">
            <td align="left" class="matters">
                Business Unit</td>
            <td align="right" class="matters" style="text-align: left" colspan="3">
                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters">
                From
                Date</td>
            <td align="right" class="matters" style="text-align: left" id="tdToDate1" runat="server">
                <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" Width="123px"></asp:TextBox>
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>&nbsp;
            </td>
            <td align="left" class="matters" style="text-align: left" id="TdToDate2" runat="server">
                To Date
            </td>
            <td id="tdToDate3" runat="server">
                <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox" Width="123px">
                </asp:TextBox>
                <asp:ImageButton ID="ImgTo" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                        runat="server" ControlToValidate="txtTodate" Display="Dynamic" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTodate"
                    Display="Dynamic" ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g. 21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" visible="false" id="TrFeeSource">
            <td align="left" class="matters">
                <asp:Label ID="lblCombotext" runat="server" Text="Fee Source"></asp:Label>
            </td>
            <td align="right" class="matters" colspan="3" style="text-align: left">
                <asp:DropDownList ID="ddlFeeSource" runat="server">
                    <asp:ListItem Selected="True">FEE MONTHLY CHARGE</asp:ListItem>
                    <asp:ListItem>FEE Adjustment</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlSmsfilter" runat="server" Visible="False">
                    <asp:ListItem>All</asp:ListItem>
                    <asp:ListItem>Success</asp:ListItem>
                    <asp:ListItem>Failed</asp:ListItem>
                    <asp:ListItem>Pending</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="4" style="text-align: right">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                    ValidationGroup="MAINERROR" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalTodate" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="ImgTo" TargetControlID="txtTodate">
    </ajaxToolkit:CalendarExtender>
    <input id="hfFEECounter" runat="server" type="hidden" />
</asp:Content>
