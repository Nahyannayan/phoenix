﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptCurriculumService
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")
            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200455" And ViewState("MainMnu_code") <> "S200999") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                bindAcademic_Year()
                BindActivityTypes()
                DisplayFromDateToDate(ddlAcademicYear.SelectedItem.Text)
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        End If
    End Sub
    Public Sub BindActivityTypes()
        'trvServiceTypes.Nodes.Clear()
        'Dim ds As DataSet
        'Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        'Dim str_Sql As String = String.Empty ' SSC_ID hard coded as 4(Others)
        'Dim PARAM(5) As SqlParameter
        'PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        'PARAM(1) = New SqlParameter("@sBSU_ID", Session("sBsuid"))
        'PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.[GET_CS_SVC_BY_BSU]", PARAM)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    Dim i = 0
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        Dim ParentNode As New TreeNode
        '        ParentNode.Text = ds.Tables(0).Rows(i).Item("SVC_DESCRIPTION").ToString()
        '        ParentNode.Value = ds.Tables(0).Rows(i).Item("SVC_ID").ToString()
        '        trvServiceTypes.Nodes.Add(ParentNode)
        '    Next
        'End If
        'trvServiceTypes.ExpandAll()

        'added on 05122013

        trvServiceTypes.Nodes.Clear()
        PopulateRootLevel()
        trvServiceTypes.DataBind()
        trvServiceTypes.CollapseAll()
    End Sub
    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ServicesConnectionString").ConnectionString
        Dim str_Sql As String
        trvServiceTypes.Nodes.Clear()
        str_Sql = "select 0 as SVC_ID,'All' as SVC_DESCRIPTION,count(*) childnodecount from  (SELECT  DISTINCT SVC_ID , SSC_DESC + ' - ' + SVC_DESCRIPTION AS SVC_DESCRIPTION,SVC_BSU_ID  FROM SERVICES_CATEGORY SSC  INNER JOIN SERVICES_SYS_M SVC WITH ( NOLOCK ) ON SSC_ID = SVC.SVC_SSC_ID INNER JOIN SERVICES_BSU_M WITH ( NOLOCK ) ON SVB_BSU_ID = SVC_BSU_ID)a "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), trvServiceTypes.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("SVC_DESCRIPTION").ToString()
            tn.Value = dr("SVC_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub
    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        trvServiceTypes.DataBind()
        trvServiceTypes.CollapseAll()
    End Sub

    'Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

    '    Dim childnodeValue = ParentNode.Value
    '    Dim ds As DataSet
    '    Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
    '    Dim str_Sql As String = String.Empty
    '    str_Sql = " SELECT DISTINCT ISNULL(FEE_ID, 0) FEE_ID,LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) FEE_DESCR " & _
    '              "FROM FEES.FEES_M FM LEFT JOIN OASIS.dbo.SERVICES_SYS_M SYM ON FM.FEE_SVC_ID=SYM.SVC_ID " & _
    '              "LEFT JOIN OASIS.dbo.SERVICES_CATEGORY SC ON SYM.SVC_SSC_ID=SC.SSC_ID WHERE ISNULL(SSC_ID,4)='" & childnodeValue & "' " & _
    '              "ORDER BY ISNULL(FEE_ID, 0),LTRIM(RTRIM(ISNULL(FEE_DESCR,''))) "
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        Dim i = 0
    '        For i = 0 To ds.Tables(0).Rows.Count - 1
    '            Dim ChildNode As New TreeNode
    '            ChildNode.Text = ds.Tables(0).Rows(i).Item("FEE_DESCR").ToString()
    '            ChildNode.Value = ds.Tables(0).Rows(i).Item("FEE_ID").ToString()
    '            ParentNode.ChildNodes.Add(ChildNode)

    '        Next
    '    End If
    'End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            'Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
            Using YEAR_DESCRreader As SqlDataReader = GetYear_DESCR(Session("sBsuid"))

                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Public Shared Function GetYear_DESCR(ByVal Bsu_id As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetYear_DESCR As String = ""

        sqlGetYear_DESCR = "Select  ACY_ID,ACD_ID,Y_DESCR,ACD_CURRENT from(SELECT DISTINCT ACADEMICYEAR_D.ACD_ACY_ID AS ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, " & _
                     " ACADEMICYEAR_D.ACD_CLM_ID AS CLM_ID, ACADEMICYEAR_D.ACD_ID as ACD_ID,ACD_CURRENT FROM ACADEMICYEAR_D INNER JOIN " & _
                     " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where  a.BSU_ID='" & Bsu_id & "' order by a.Y_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetYear_DESCR, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then

            CallReport()
        End If
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        UpdateServiceSelected()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)

        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        If h_BSUID.Value = "" Then
            lblError.Text = "Please select at least one business unit."
            Exit Sub
        End If
        param.Add("@sBSU_ID", h_BSUID.Value)

        If txtFromdate.Text = "" Then
            param.Add("@FROMDATE", DBNull.Value)
        Else
            param.Add("@FROMDATE", txtFromdate.Text)
        End If
        If txtTodate.Text = "" Then
            param.Add("@TODATE", DBNull.Value)
        Else
            param.Add("@TODATE", txtTodate.Text)
        End If
        Dim service As String = ""
        service = ViewState("SERVICE_IDs")

        If service = "" Then
            param.Add("@SVC_ID", DBNull.Value)
        Else
            param.Add("@SVC_ID", service)
        End If

        param.Add("@STATUS", rbStatus.SelectedValue)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_SERVICES"
            .reportParameters = param

            If ViewState("MainMnu_code") = "S200999" Then
                .reportPath = Server.MapPath("../RPT/rptstudentCurriculumServicesListInfo.rpt")
            Else
                .reportPath = Server.MapPath("../RPT/rptstudentCurriculumServicesList.rpt")
            End If


        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Sub UpdateServiceSelected()
        Try
            Dim SERVICE_IDs As String = ""
            For Each node As TreeNode In trvServiceTypes.CheckedNodes
                If (Not node.ChildNodes Is Nothing AndAlso node.ChildNodes.Count > 0) Then
                    Continue For
                End If
                If SERVICE_IDs.Trim() = "" Then
                    SERVICE_IDs = node.Value
                Else
                    SERVICE_IDs = SERVICE_IDs + "|" + node.Value
                End If
            Next
            ViewState("SERVICE_IDs") = SERVICE_IDs
        Catch
            ViewState("SERVICE_IDs") = ""
        End Try
    End Sub
    Public Sub bindAcademic_Year()
        Try
            Dim CONN As String = WebConfigurationManager.ConnectionStrings("OASIS_ServiceSConnectionString").ConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@INFO_TYPE", "ACD")
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@CLM_ID", Session("CLM"))
            Using YEAR_DESCRreader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "SM.GETACADEMIC_DATABIND", PARAM)
                ddlAcademicYear.Items.Clear()
                If YEAR_DESCRreader.HasRows = True Then
                    ddlAcademicYear.DataSource = YEAR_DESCRreader
                    ddlAcademicYear.DataTextField = "ACY_DESCR"
                    ddlAcademicYear.DataValueField = "ACD_ID"
                    ddlAcademicYear.DataBind()
                End If
            End Using

            If Not Session("Current_ACD_ID") Is Nothing Then
                If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                    ddlAcademicYear.ClearSelection()
                    ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function GetSERVICES_Details(Optional ByVal SVCNOt As Integer = 0) As DataTable
        Dim sql_query As String = "SELECT SVB_ID,SVC_ID, SVC_DESCRIPTION FROM SERVICES_BSU_M " & _
                                  " INNER JOIN SERVICES_SYS_M ON SVB_SVC_ID=SVC_ID WHERE " & _
                                  " SVB_ACD_ID=" & ddlAcademicYear.SelectedValue & " AND SVB_BSU_ID='" & Session("sBsuId") & "'" & _
                                  " AND SVB_bAvailable='True'"

        If SVCNOt >= 1 Then
            sql_query += " AND SVC_ID<>" & SVCNOt.ToString
        End If
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Protected Sub trvServiceTypes_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trvServiceTypes.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub
    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ServiceSConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        PARAM(1) = New SqlParameter("@sBSU_ID", Session("sBsuid"))
        PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.[GET_CS_SVC_BY_BSU_WITH_CHECKALL]", PARAM)
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        DisplayFromDateToDate(ddlAcademicYear.SelectedItem.Text)
    End Sub
    Private Sub DisplayFromDateToDate(ByVal AcaYear As String)
        Dim myReader As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        myReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, "select ACD_STARTDT,ACD_ENDDT FROM ACADEMICYEAR_D WHERE ACD_ID=" & ddlAcademicYear.SelectedItem.Value)
        If myReader.HasRows Then
            myReader.Read()
            txtFromdate.Text = Format(myReader("ACD_STARTDT"), "dd/MMM/yyyy")
            txtTodate.Text = Format(myReader("ACD_ENDDT"), "dd/MMM/yyyy")
        End If

        

    End Sub
End Class


