<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptECAConsolidatedReports.aspx.vb" Inherits="rptECAConsolidatedReports" title="Untitled Page" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc2" %>

<%@ Register Src="../../../UserControls/usrTransportBSUnits.ascx" TagName="usrTransportBSUnits"
    TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">            
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
        ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
    <br />
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" >
        <tr class ="subheader_img">
            <td align="left" colspan="4" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></td>
        </tr>
        <tr runat="server" visible="false" >
            <td align="left" class="matters" colspan="4">
                <asp:RadioButton ID="radStud" runat="server" Checked="True" GroupName="STUD_ENQ"
                    Text="Student" />
                <asp:RadioButton ID="radEnq" runat="server" GroupName="STUD_ENQ" Text="Enquiry" /></td>
        </tr>
        <tr runat="server" id ="trAsonDate">
            <td align="left" class="matters" >
                As On Date</td>
            <td align="left" class="matters" colspan="3" >
                <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="inputbox" Width="122px">
                </asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                    ImageUrl="~/Images/calendar.gif" OnClientClick="return false" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAsOnDate"
                    ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAsOnDate"
                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr runat="server" id ="trPeriod">
            <td align="left" class="matters" >
                From Date</td>
            <td align="left" class="matters" >
                <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox" Width="112px"></asp:TextBox>
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" >
                To Date</td>
            <td align="left" class="matters" >
                <asp:TextBox id="txtToDate" runat="server" CssClass="inputbox" Width="122px">
                </asp:TextBox>
                <asp:ImageButton id="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false">
                </asp:ImageButton>
                <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="To Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator id="revToDate" runat="server" ControlToValidate="txtToDate"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" >
                Business Unit</td>
            <td align="left" class="matters" colspan="3" >
                <uc2:usrBSUnits ID="UsrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr id="trExclude" runat="server">
            <td align="left" class="matters" colspan="4" >
                <asp:CheckBox id="chkExclude" runat="server" Text="PDC Exclude">
                </asp:CheckBox></td>
        </tr>
        <tr id ="trSortby" runat="server" >
            <td align="left" class="matters" >
                Sort By</td>
            <td align="left" class="matters" colspan="3" >
                &nbsp;<asp:RadioButton id="radGradeWise" runat="server" Checked="True" GroupName="SORT"
                    Text="Grade Wise"></asp:RadioButton>
                <asp:RadioButton id="radBuswise" runat="server" GroupName="SORT" Text="Bus wise">
                </asp:RadioButton></td>
        </tr>
        <tr id="trSummary" runat="server" >
            <td align="left" class="matters" colspan="4" >
                &nbsp;<asp:RadioButton ID="radDetailed" runat="server" Checked="True" Text="Detailed" ValidationGroup="SUMMARY" GroupName="SUMMARY" />
                <asp:RadioButton ID="radGradeSummary" runat="server" Text="Gradewise Summary" GroupName="SUMMARY" />
                <asp:RadioButton ID="radSummary" runat="server" Text="Summary" ValidationGroup="SUMMARY" GroupName="SUMMARY" /></td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="4" style=" text-align:right">
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <ajaxtoolkit:calendarextender id="calFromDate1" runat="server" format="dd/MMM/yyyy"
        popupbuttonid="imgFromDate" targetcontrolid="txtFromDate">
    </ajaxtoolkit:calendarextender><ajaxtoolkit:calendarextender id="calFromDate2" runat="server" format="dd/MMM/yyyy" targetcontrolid="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate" PopupButtonID="imgToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender><ajaxtoolkit:calendarextender id="calAsOnDate1" runat="server" format="dd/MMM/yyyy"
        popupbuttonid="ImageButton1" targetcontrolid="txtAsOnDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxtoolkit:calendarextender id="calAsOnDate2" runat="server" format="dd/MMM/yyyy" targetcontrolid="txtAsOnDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

