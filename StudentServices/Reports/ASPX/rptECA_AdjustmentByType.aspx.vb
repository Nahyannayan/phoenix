Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports GemBox.Spreadsheet

Partial Class rptECA_AdjustmentByType
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle

            txtFromDate.Text = Format(DateTime.Now.AddMonths(-1), OASISConstants.DateFormat)
            txtTodate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            UsrBSUnits1.MenuCode = ViewState("MainMnu_code")

            Select Case ViewState("MainMnu_code").ToString
                Case "F720104"
                    lblReportCaption.Text = "Service Fee Adjustment by Type"
                    TrFeeSource.Visible = True
                    lblCombotext.Text = "Adjustment Type"
                    'If IsPostBack = False Then
                    getAdjReason()
                    'End If
            End Select
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case ViewState("MainMnu_code").ToString
            Case "F720104"
                FeeAdjustmentbyType()
        End Select
    End Sub

    Private Sub FeeAdjustmentbyType()
        Dim cmd As New SqlCommand("[FEES].[ADJUSTMENT_REASON]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpFromDT As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        sqlpFromDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODATE ", SqlDbType.DateTime)
        sqlpToDT.Value = CDate(txtTodate.Text)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpBSU_ID As New SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.VarChar)
        sqlpSTU_BSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpARM_ID As New SqlParameter("@ARM_ID", SqlDbType.Int)
        sqlpARM_ID.Value = ddlFeeSource.SelectedItem.Value
        cmd.Parameters.Add(sqlpARM_ID)

        cmd.Connection = New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BsuName") = Session("BSU_Name")
        params("RptHead") = "Service Fee Adjustment By Type "
        params("DateHead") = "For the period " & txtFromDate.Text & " To " & txtTodate.Text
        repSource.Parameter = params
        repSource.Command = cmd

        repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_ADJUSTMENTBY_TYPE.rpt"
        Session("ReportSource") = repSource
        Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
    End Sub

    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Sub getAdjReason()
        ddlFeeSource.Items.Clear()
        ddlFeeSource.DataSource = FeeCommon.getADJREASON(True)
        ddlFeeSource.DataTextField = "ARM_DESCR"
        ddlFeeSource.DataValueField = "ARM_ID"
        ddlFeeSource.DataBind()
    End Sub

End Class
