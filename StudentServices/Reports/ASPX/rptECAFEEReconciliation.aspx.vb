Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptECAFEEReconciliation
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        Session("RPTToDate") = txtToDate.Text
        Select Case MainMnu_code
            Case "F720102"
                GenerateFEEReconciliation()
        End Select
    End Sub

    Private Sub GenerateFEEReconciliation()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATIONSTUDENTREPORT]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuId")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar)
        sqlpSTUBSU_ID.Value = Me.UsrBSUnits1.GetSelectedNode()
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        Dim sqlpACD_ID As New SqlParameter("@ACD_ID", SqlDbType.Int)
        sqlpACD_ID.Value = 0
        cmd.Parameters.Add(sqlpACD_ID)

        Dim sqlpGRD_ID As New SqlParameter("@GRD_ID", SqlDbType.VarChar, 10)
        sqlpGRD_ID.Value = "-1"
        cmd.Parameters.Add(sqlpGRD_ID)

        Dim sqlpSTU_TYP As New SqlParameter("@STU_TYP", SqlDbType.VarChar, 2)
        If radEnq.Checked Then
            sqlpSTU_TYP.Value = "E"
        ElseIf radStud.Checked Then
            sqlpSTU_TYP.Value = "S"
        End If
        cmd.Parameters.Add(sqlpSTU_TYP)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpExcludePDC As New SqlParameter("@BExcludePDc", SqlDbType.Bit)
        If chkExclude.Checked = True Then
            sqlpExcludePDC.Value = 1
        Else
            sqlpExcludePDC.Value = 0
        End If
        cmd.Parameters.Add(sqlpExcludePDC)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("BSU_NAME") = ""
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim monthDesc As String = CDate(txtFromDate.Text).ToString("MMMM").ToUpper
        If chkExclude.Checked = False Then
            params("RPT_CAPTION") = " FEE RECONCILIATION WITH PDC"
        Else
            params("RPT_CAPTION") = " FEE RECONCILIATION WITHOUT PDC"
        End If
        If chkGradeSummary.Checked Then
            If chkExclude.Checked = False Then
                'repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_NORM_FEERECONCILIATIONGRADEWISE.rpt"
                repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_FEERECONCILGRADEWISE.rpt"
            Else
                repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_FEERECONCILGRADEWISE_EXCLUDE_PDC.rpt"
            End If
        Else
            If chkExclude.Checked = False Then
                repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_FEERECONCILSTUDENT.rpt"
            Else
                repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_FEERECONCILSTUDENT_EXCLUDE_PDC.rpt"
            End If
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            Select Case MainMnu_code
                Case "F720102"
                    'trSummary.Visible = False
                    chkGradeSummary.Text = "Gradewise Summary"
                    lblReportCaption.Text = "Service Fee Reconciliation"
            End Select
            Me.UsrBSUnits1.MenuCode = MainMnu_code
            'BindBusinessUnit()
            'BindAcademicYear(ddlBSUnit.SelectedValue)
            'FillACD()
            'BindGrade()
            Me.trACY.Visible = False
            Me.trGRD.Visible = False
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            'If bNoData Then
            '    txtFromDate.Text = Session("RPTFromDate")
            '    txtToDate.Text = Session("RPTToDate")
            'Else
            '    txtFromDate.Text = Format(DateTime.Now.AddDays(-DateTime.Now.Day + 1), OASISConstants.DateFormat)
            txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
            'End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If
    End Sub

    Private Sub BindGrade()
        Try
            Dim conn_str As String = ConnectionManger.GetOASISConnectionString
            Dim sql_query As String = "SELECT GRM_DISPLAY, GRM_GRD_ID FROM GRADE_BSU_M " & _
            " INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID WHERE GRM_ACD_ID = " & _
            ddlAcademicYear.SelectedValue & " AND GRM_BSU_ID ='0' "
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, sql_query)
            If (dsData IsNot Nothing) AndAlso (dsData.Tables(0).Rows.Count > 0) Then
                Dim dr As DataRow = dsData.Tables(0).NewRow
                dr(0) = "ALL"
                dr(1) = "-1"
                dsData.Tables(0).Rows.Add(dr)
            End If
            ddlGrade.DataSource = dsData
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRM_GRD_ID"
            ddlGrade.DataBind()
            If ddlGrade.Items.Count > 0 Then
                ddlGrade.Items.FindByValue("-1").Selected = True
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
