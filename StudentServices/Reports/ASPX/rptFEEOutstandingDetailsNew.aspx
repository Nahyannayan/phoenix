﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFEEOutstandingDetailsNew.aspx.vb" Inherits="StudentServices_rptFEEOutstandingDetailsNew" %>

<%@ Register Src="~/StudentServices/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" width="100%">



                    <tr>
                        <td align="left" width="20%"><span class="field-label">As On</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="3">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>

                    <tr runat="server" id="trRdbutton">
                        <td align="left" width="20%"></td>
                        <td align="left" colspan="3">
                            <asp:RadioButton ID="radDetailed" runat="server" Checked="True" GroupName="SUMMARY"
                                Text="Detailed" ValidationGroup="SUMMARY" CssClass="field-label" ></asp:RadioButton>
                            <asp:RadioButton ID="radGradeSummary" runat="server" GroupName="SUMMARY"
                                    Text="Gradewise Summary" CssClass="field-label" ></asp:RadioButton>
                            <asp:RadioButton ID="rbParent" runat="server"
                                        GroupName="SUMMARY" Text="Parentwise" ValidationGroup="SUMMARY" CssClass="field-label" ></asp:RadioButton>
                            <asp:RadioButton ID="radSummary" runat="server" GroupName="SUMMARY" Text="Summary"
                                ValidationGroup="SUMMARY" CssClass="field-label"  /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

