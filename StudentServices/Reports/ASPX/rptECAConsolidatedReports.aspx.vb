Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml

Partial Class rptECAConsolidatedReports
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Session("RPTFromDate") = txtFromDate.Text
        If UsrBSUnits1.GetSelectedNode() = String.Empty Then
            lblError.Text = "***Please select atleast one business Unit***"
            Exit Sub
        End If
        Select Case MainMnu_code
            Case "F720103"
                GenerateFeeReconciliationReport()
        End Select
    End Sub

    Private Sub GenerateFeeReconciliationReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("[FEES].[F_GETFEERECONCILIATIONCONSOLIDATED]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBSUID")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTUBSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.VarChar, 300)
        sqlpSTUBSU_ID.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTUBSU_ID)

        Dim sqlpFRMDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFRMDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpFRMDT)

        Dim sqlpTODT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpTODT.Value = CDate(txtToDate.Text)
        cmd.Parameters.Add(sqlpTODT)

        Dim sqlpExclude As New SqlParameter("@BExcludePDc", SqlDbType.Bit)
        If chkExclude.Checked = True Then
            sqlpExclude.Value = 1
        Else
            sqlpExclude.Value = 0
        End If
        cmd.Parameters.Add(sqlpExclude)


        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("FromDT") = txtFromDate.Text
        params("TODT") = txtToDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        Dim monthDesc As String = CDate(txtFromDate.Text).ToString("MMMM").ToUpper
        If chkExclude.Checked = False Then
            params("RPT_CAPTION") = "SERVICE FEE RECONCILIATION CONSOLIDATED REPORT WITH PDC"
        Else
            params("RPT_CAPTION") = "SERVICE FEE RECONCILIATION CONSOLIDATED REPORT WITHOUT PDC"
        End If
        'repSource.ResourceName = "../../FEES/REPORTS/RPT/rptFEE_TRANSPORT_FEERECONCILIATION_CONSOLIDATED.rpt"
        If chkExclude.Checked = False Then
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_FEERECONCIL_CONSOLIDATED.rpt"
        Else
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptECA_FEERECONCIL_CONSOLIDATED_EXCLUDE_PDC.rpt"
        End If
        Session("ReportSource") = repSource
        Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")

            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If

            trPeriod.Visible = True
            trAsonDate.Visible = False
            trSummary.Visible = False
            trExclude.Visible = False
            Select Case MainMnu_code
                Case "F720103"
                    trAsonDate.Visible = False
                    trPeriod.Visible = True
                    trSummary.Visible = False
                    trSortby.Visible = False
                    trExclude.Visible = True
                    lblReportCaption.Text = "Service Fee Reconciliation Report"
            End Select
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
                txtToDate.Text = Session("RPTToDate")
                txtAsOnDate.Text = Session("RPTToDate")
            Else
                txtToDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
                txtFromDate.Text = Format(DateTime.Now.AddMonths(-3), OASISConstants.DateFormat)
                txtAsOnDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
    End Sub

End Class
