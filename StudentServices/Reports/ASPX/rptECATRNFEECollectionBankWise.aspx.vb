﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class StudentServices_Reports_ASPX_rptECATRNFEECollectionBankWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim MainMnu_code As String
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case OASISConstants.MNU_FEE_TRANS_COLL_PDC_SCH
                    lblReportCaption.Text = "Transport Fee PDC Schedule"
                    chkSummary.Checked = True
                    h_bTransport.Value = 1
                Case OASISConstants.MNU_ECA_FEE_COLLECTION_DETAILS
                    lblReportCaption.Text = "Collection Summary Report"
                    chkSummary.Visible = False
                    trChkConciliation.Visible = False
                    h_bTransport.Value = 0
                    txtFeeCounter.Text = Session("sUsr_name")
                    hfFEECounter.Value = Session("sUsr_name")
                    CheckEmployee.Visible = True
                Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH
                    lblReportCaption.Text = "Fee PDC Schedule"
                    chkSummary.Checked = True
                    trChkConciliation.Visible = False
                    trChkPDC.Visible = True
                    h_bTransport.Value = 0
                Case OASISConstants.MNU_FEE_NORM_DAILY_CHQ_COLLECTION
                    lblReportCaption.Text = "Daily Cheque Collection"
                    chkSummary.Visible = True
                    chkSummary.Text = "Deposit Slip"
                    chkPDCOnly.Visible = True
                    chkPDCOnly.Text = "Cheque Wise Report"
                    trChkConciliation.Visible = True
                    h_bTransport.Value = 0
                Case OASISConstants.MNU_TRAN_CHEQUSTATUS
                    lblReportCaption.Text = "Cheque Status"
                    chkSummary.Text = "Pdc"
                    TrUsrName.Visible = False
                    h_bTransport.Value = 0
                Case OASISConstants.MNU_TRAN_CHEQUHAND
                    lblReportCaption.Text = "Cheque In Hand"
                    chkSummary.Visible = False
                    TrUsrName.Visible = False
                    chkPDCOnly.Visible = True
                    h_bTransport.Value = 0
            End Select
            FillBSUNames(True)
            txtFeeCounter.Attributes.Add("ReadOnly", "ReadOnly")
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
            Else
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        If IsPostBack Then
            FillBSUNames()
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select isnull(BSU_bFeesMulticurrency,0) from BUSINESSUNIT_M where BSU_ID='" & Session("sBSUID") & "'"
        Dim MultiCurrency As Boolean = Convert.ToBoolean(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        If MultiCurrency = True Then
            Me.chkFC.Visible = True
        Else
            Me.chkFC.Visible = False
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Session("RPTFromDate") = txtFromDate.Text
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        UsrBSUnits1.MenuCode = MainMnu_code
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        If h_BSUID.Value = "" Then
            'lblError.Text = "Please select at least one business unit."
            usrMessageBar2.ShowNotification("Please select at least one business unit.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case OASISConstants.MNU_ECA_FEE_COLLECTION_DETAILS
                GenerateFeeCollectionSummary()

        End Select

    End Sub

    

    Private Sub GenerateFeeCollectionSummary()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand
        If Me.chkFC.Checked = True Then
            cmd.CommandText = "[FEES].[F_GET_FEECOLLECTION_FC]"
        Else
            cmd.CommandText = "[FEES].[F_GET_FEECOLLECTION]"
        End If

        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpPROVIDER_BSU_ID As New SqlParameter("@PROVIDER_BSU_ID", SqlDbType.VarChar, 20)
        sqlpPROVIDER_BSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpPROVIDER_BSU_ID)

        Dim sqlpBSU_ID As New SqlParameter("@BSUIDs", SqlDbType.Xml)
        sqlpBSU_ID.Value = UtilityObj.GenerateXML(h_BSUID.Value, XMLType.BSUName)
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpUserName As New SqlParameter("@Usr_Name", SqlDbType.VarChar, 70)
        sqlpUserName.Value = hfFEECounter.Value
        If CheckEmployee.Checked Then
            sqlpUserName.Value = ""
        End If
        cmd.Parameters.Add(sqlpUserName)

        Dim sqlpDT As New SqlParameter("@DT", SqlDbType.DateTime)
        sqlpDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpDT)

        Dim repSourceSubRep(1) As MyReportClass
        repSourceSubRep(0) = New MyReportClass
        Dim cmdSubOtherTypes As SqlCommand = cmd.Clone()
        If Me.chkFC.Checked = True Then
            cmdSubOtherTypes.CommandText = "[FEES].[F_GET_FEECOLL_TOTAL_FC]"
        Else
            cmdSubOtherTypes.CommandText = "[FEES].[F_GET_FEECOLL_TOTAL]"
        End If
        '"Select * from  FEES.vw_OSO_FEE_RECEIPTOTHERS "
        cmdSubOtherTypes.Connection = New SqlConnection(str_conn)
        repSourceSubRep(0).Command = cmdSubOtherTypes

        'Select * from  FEES.vw_OSO_FEE_RECEIPTOTHERS
        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("DATE") = txtFromDate.Text

        Dim RoundOffVal As Integer = Session("BSU_ROUNDOFF")
        Dim separator As String() = New String() {"||"}
        Dim myValarr() As String = h_BSUID.Value.Split(separator, StringSplitOptions.RemoveEmptyEntries)
        If myValarr.Length > 1 Then
            'If sqlpBSU_ID.Value <> "" Then
            RoundOffVal = 2
        Else

            GetBSU_RoundOff(myValarr(0))
            RoundOffVal = ViewState("RoundOffVal")

        End If
        params("RoundOffVal") = RoundOffVal
        'params("FromDT") = txtFromDate.Text
        'params("ToDT") = txtToDate.Text
        repSource.Parameter = params
        repSource.SubReport = repSourceSubRep
        'repSource.IncludeBSUImage = True
        repSource.Command = cmd
        If Me.chkFC.Checked = True Then
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFEEReceiptSummarywithFC_ECA.rpt"
        Else
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFEEReceiptSummary_ECA.rpt"
        End If

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Function FillBSUNames(Optional ByVal bGetAll As Boolean = False) As Boolean
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case OASISConstants.MNU_ECA_FEE_COLLECTION_DETAILS
                Return FillBSUNamesECA()
                'Case OASISConstants.MNU_FEE_NORM_COLL_PDC_SCH, _
                ' OASISConstants.MNU_FEE_COLLECTION_DETAILS, _
                ' OASISConstants.MNU_FEE_NORM_DAILY_CHQ_COLLECTION
                '    If bGetAll Then
                '        h_BSUID.Value = Session("sBSUID")
                '    End If
                '    Return FillBSUNames(h_BSUID.Value)
        End Select
    End Function

    Private Function FillBSUNamesECA() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim conn As New SqlConnection(str_conn)
        Dim ds As New DataSet

        Try
            Dim cmd As New SqlCommand
            cmd.CommandText = "SM.Get_GMA_BusinessUnit"
            cmd.Parameters.AddWithValue("@userId", Session("sUsr_id"))
            cmd.Parameters.AddWithValue("@providerBsuId", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@menuCode", ViewState("MainMnu_code"))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
            Dim dv As New DataView(ds.Tables(0))
            'dv.RowFilter = condition
            'If bgetAll Then
            h_BSUID.Value = ""
            For i As Integer = 0 To dv.Table.Rows.Count - 1
                h_BSUID.Value += dv.Item(i)("BSU_ID") & "||"
            Next
            'End If
            Return True
        Catch
            Return False
        Finally
            conn.Close()
        End Try

    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
