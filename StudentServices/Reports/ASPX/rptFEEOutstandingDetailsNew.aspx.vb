﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class StudentServices_rptFEEOutstandingDetailsNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim MainMnu_code As String
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            lblReportCaption.Text = "Fee Outstanding Details - New"


            Page.Title = OASISConstants.Gemstitle
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                txtFromDate.Text = Session("RPTFromDate")
            Else
                txtFromDate.Text = Format(DateTime.Now, OASISConstants.DateFormat)
            End If
        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            lblError.Text = "No Records with specified condition"
        Else
            lblError.Text = ""
        End If
    End Sub

    

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If UtilityObj.IsFutureDate(txtFromDate.Text) Then
            lblError.Text = "Invalid Date !!!"
            Exit Sub
        End If
        Session("RPTFromDate") = txtFromDate.Text
       
        F_FEEOUTSTANDINGSTUDENTWISE_SERVICES()

    End Sub
    Private Sub F_FEEOUTSTANDINGSTUDENTWISE_SERVICES()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand("FEES.F_FEEOUTSTANDINGSTUDENTWISE_FEES")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpSTU_BSU_IDs As New SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 300)
        sqlpSTU_BSU_IDs.Value = UtilityObj.GetBSUnitWithSeperator(UsrBSUnits1.GetSelectedNode(), OASISConstants.SEPARATOR_PIPE)
        cmd.Parameters.Add(sqlpSTU_BSU_IDs)

        


        Dim sqlpAsOnDT As New SqlParameter("@Dt", SqlDbType.DateTime)
        sqlpAsOnDT.Value = CDate(txtFromDate.Text)
        cmd.Parameters.Add(sqlpAsOnDT)

        Dim sqlpTyp As New SqlParameter("@Typ", SqlDbType.TinyInt)

        If radDetailed.Checked Or rbParent.Checked Then
            sqlpTyp.Value = 0
        ElseIf radGradeSummary.Checked Then
            sqlpTyp.Value = 1
        ElseIf radSummary.Checked Then
            sqlpTyp.Value = 2
        End If
        cmd.Parameters.Add(sqlpTyp)

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("fromDate") = txtFromDate.Text
        repSource.Parameter = params
        repSource.Command = cmd
        'repSource.IncludeBSUImage = True
        If radDetailed.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING STUDENTWISE DETAILS"
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFEE_FEEOUTSTANDING_SERVICE.rpt"
        ElseIf Me.rbParent.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING PARENT WISE DETAILS"
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFEE_FEEOS_ParentWise_SERVICE.rpt"
        ElseIf radGradeSummary.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING GRADE WISE DETAILS"
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFEE_FEEOUTSTANDING_GRADEWISE_SUMMARY_SERVICE.rpt"
        ElseIf radSummary.Checked Then
            params("RPT_CAPTION") = "FEE OUTSTANDING SUMMARY" ' AS ON : " & txtFromDate.Text.ToString()
            repSource.ResourceName = "../../StudentServices/REPORTS/RPT/rptFEE_FEEOUTSTANDING_SUMMARY_SERVICE.rpt"
        End If
        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
