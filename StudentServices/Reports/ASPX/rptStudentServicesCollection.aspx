<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentServicesCollection.aspx.vb" Inherits="StudentServices_Reports_ASPX_rptStudentServicesCollection" %>

<%@ Register Src="~/StudentServices/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../../Scripts/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            $(function () {
                $(".datepicker").datepicker({
                    dateFormat: 'dd/M/yy',
                    changeMonth: true,
                    changeYear: true,
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            }
      );
        }
        function CheckOnPostback() {
            if (document.getElementById('<%=hfBSU.ClientID %>').value == 'none') {
            }
            return false;
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%--<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar ID="usrMessageBar2" runat="server"></uc2:usrMessageBar>
                <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
                <br />
                <table align="center" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date </span></td>
                        <td align="left" width="30%">
                            <input type="text" id="calFromDT" class="datepicker" runat="server" />

                        </td>
                        <td align="left" width="20%"><span class="field-label">To Date </span></td>
                        <td align="left" width="30%">
                            <input type="text" id="calToDT" class="datepicker" runat="server" />

                        </td>
                    </tr>

                           
                    <tr id="trBSUnit" runat="server">
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" colspan="2">
                            <div class="checkbox-list-full">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:CheckBox ID="chkSummary" runat="server" Text="Summary" CssClass="field-label" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Group by</span></td>

                        <td align="left" width="30%">
                            <asp:RadioButtonList ID="rblGroupby" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="ST"><span class="field-label">Student</span></asp:ListItem>
                                <asp:ListItem Value="SV"><span class="field-label">Service</span></asp:ListItem>
                                <asp:ListItem Value="BU"><span class="field-label">BusinessUnit</span></asp:ListItem>
                                <asp:ListItem Value="PM"><span class="field-label">PaymentMode</span></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <input id="hfBSU" runat="server" type="hidden" />
                <input id="h_BSUID" runat="server" type="hidden" />
                <input id="h_bTransport" runat="server" type="hidden" />
                <input id="hfFEECounter" runat="server" type="hidden" />
            </div>
        </div>
    </div>
</asp:Content>


