﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Partial Class StudentServices_Reports_ASPX_rptStudentServicesCollection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ClientScript.RegisterStartupScript(Me.GetType(), _
        "script", "<script language='javascript'>  CheckOnPostback(); </script>")
        If Not Page.IsPostBack Then
            hfBSU.Value = "display"
        End If
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Dim MainMnu_code As String
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            Page.Title = OASISConstants.Gemstitle
            Select Case MainMnu_code
                Case "F720099"
                    lblReportCaption.Text = "Services Fee Collection"
            End Select
            FillBSUNames(True)
            Dim bNoData As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If bNoData Then
                calFromDT.Value = Session("RPTFromDate")
                calToDT.Value = Session("RPTToDate")
            Else
                calFromDT.Value = Format(DateTime.Now.AddMonths(-1), OASISConstants.DateFormat)
                calToDT.Value = Format(DateTime.Now, OASISConstants.DateFormat)
            End If

        End If

        Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
        If nodata Then
            'lblError.Text = "No Records with specified condition"
            usrMessageBar2.ShowNotification("No Records with specified condition", UserControls_usrMessageBar.WarningType.Danger)
        Else
            'lblError.Text = ""
        End If

        If h_BSUID.Value <> Nothing And h_BSUID.Value <> "" And h_BSUID.Value <> "undefined" Then
            h_BSUID.Value = h_BSUID.Value.Split("___")(0)
        End If
        If IsPostBack Then
            FillBSUNames()
        End If

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Session("RPTFromDate") = calFromDT.Value
        Session("RPTToDate") = calToDT.Value
        If UtilityObj.IsFutureDate(calFromDT.Value) Then
            'lblError.Text = "Invalid Date !!!"
            usrMessageBar2.ShowNotification("Invalid Date !!!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        UsrBSUnits1.MenuCode = MainMnu_code
        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        If h_BSUID.Value = "" Then
            'lblError.Text = "Please select at least one business unit."
            usrMessageBar2.ShowNotification("Please select at least one business unit.", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Select Case MainMnu_code
            Case "F720099"
                GenerateServiceFeeCollectionSummary()
        End Select

    End Sub



    Private Sub GenerateServiceFeeCollectionSummary()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim cmd As New SqlCommand

        cmd.CommandText = "FEES.GET_SERVICE_FEECOLLECTION"
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = Session("sBsuid")
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpSTU_BSU_ID As New SqlParameter("@STU_BSU_IDs", SqlDbType.VarChar)
        sqlpSTU_BSU_ID.Value = (h_BSUID.Value).Replace("||", "|")
        cmd.Parameters.Add(sqlpSTU_BSU_ID)

        Dim sqlpFromDT As New SqlParameter("@FROMDT", SqlDbType.DateTime)
        sqlpFromDT.Value = (calFromDT.Value)
        cmd.Parameters.Add(sqlpFromDT)

        Dim sqlpToDT As New SqlParameter("@TODT", SqlDbType.DateTime)
        sqlpToDT.Value = (calToDT.Value)
        cmd.Parameters.Add(sqlpToDT)

        Dim sqlpGroupBy As New SqlParameter("@GroupBy", SqlDbType.VarChar, 2)
        sqlpGroupBy.Value = "ST"
        cmd.Parameters.Add(sqlpGroupBy)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        repSource.Command = cmd

        params("BSU_NAME") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM dbo.BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBsuid") & "'")
        params("userName") = Session("sUsr_name")
        params("Heading") = "Service Fee Collection "
        params("FromDt") = calFromDT.Value
        params("ToDt") = calToDT.Value

        Dim RepFile As String = ""
        Dim IfSummary As String = IIf(chkSummary.Checked, "_Summary", "")

        If rblGroupby.SelectedValue = "ST" Then

            RepFile = "../../StudentServices/REPORTS/RPT/rptServiceFeeCollection" & IfSummary & ".rpt"
        ElseIf rblGroupby.SelectedValue = "BU" Then
            params("Heading") += " - Group By Business Unit"
            RepFile = "../../StudentServices/REPORTS/RPT/rptServiceFeeCollection_BSU" & IfSummary & ".rpt"
        ElseIf rblGroupby.SelectedValue = "SV" Then
            params("Heading") += " - Group By Services"
            RepFile = "../../StudentServices/REPORTS/RPT/rptServiceFeeCollection_Service" & IfSummary & ".rpt"
        ElseIf rblGroupby.SelectedValue = "PM" Then
            params("Heading") += " - Group By Payment Mode"
            RepFile = "../../StudentServices/REPORTS/RPT/rptServiceFeeCollection_PayMode" & IfSummary & ".rpt"
        End If
        repSource.ResourceName = RepFile
        repSource.Parameter = params
        repSource.IncludeBSUImage = False

        Session("ReportSource") = repSource
        'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection()
    End Sub

    Public Sub GetBSU_RoundOff(ByVal BSU_ID As String)
        'Author(--Swapna)
        'Date   --18/Aug/2011
        'Purpose--Get selected BSU's round off
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim query As String = ""

        query = "SELECT  BSU_ROUNDOFF FROM BUSINESSUNIT_M WHERE BSU_ID = '" & BSU_ID & "'"

        Dim command As SqlCommand = New SqlCommand(query, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

        While reader.Read
            ViewState("RoundOffVal") = reader.Item("BSU_ROUNDOFF")
        End While

    End Sub

    Private Function FillBSUNames(Optional ByVal bGetAll As Boolean = False) As Boolean
        Dim MainMnu_code As String
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            Case "F720099"
                Return FillBSUNamesECA()
            Case Else
                Return False
        End Select
    End Function

    Private Function FillBSUNamesECA() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim conn As New SqlConnection(str_conn)
        Dim ds As New DataSet

        Try
            Dim cmd As New SqlCommand
            cmd.CommandText = "SM.Get_GMA_BusinessUnit"
            cmd.Parameters.AddWithValue("@userId", Session("sUsr_id"))
            cmd.Parameters.AddWithValue("@providerBsuId", Session("sBsuid"))
            cmd.Parameters.AddWithValue("@menuCode", ViewState("MainMnu_code"))
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
                Return False
            End If
            Dim dv As New DataView(ds.Tables(0))
            'dv.RowFilter = condition
            'If bgetAll Then
            h_BSUID.Value = ""
            For i As Integer = 0 To dv.Table.Rows.Count - 1
                h_BSUID.Value += dv.Item(i)("BSU_ID") & "||"
            Next
            'End If
            Return True
        Catch
            Return False
        Finally
            conn.Close()
        End Try

    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
