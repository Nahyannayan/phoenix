﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptFEESrvTxnDetailsServices.aspx.vb" Inherits="StudentServices_Reports_ASPX_rptFEESrvTxnDetailsServices" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/StudentServices/UserControls/usrBSUnits.ascx" tagname="usrBSUnits" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server"> 
<script language="javascript" type="text/javascript">      
     function GetStudent()
      {     
        var sFeatures;
        var sFeatures;
        sFeatures="dialogWidth: 875px; ";
        sFeatures+="dialogHeight: 600px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result; 
        var STUD_TYP = document.getElementById('<%=radEnquiry.ClientID %>').checked;
        var url; 
            if(STUD_TYP ==true)
            {
                url = "../../ShowStudentMulti.aspx?TYPE=ENQUIRY&MULTI_SEL=true&bsu=" + document.getElementById('<%= Session("sBsuid") %>').value;
                 result = window.showModalDialog(url,"", sFeatures);
            }
            else
            {
                url = "../../ShowStudentMulti.aspx?TYPE=STUD_BSU&MULTI_SEL=true&bsu=" + document.getElementById('<%= Session("sBsuid") %>').value;
                 result = window.showModalDialog(url,"", sFeatures);
            } 
            if(result != '' && result != undefined)
            {        
                document.getElementById('<%=txtStudName.ClientID %>').value='Multiple Students selected'; 
                document.getElementById('<%=h_STUD_ID.ClientID %>').value=result;
            }
            return true; 
      }   
    </script>
 <table align="center" width="70%">
     <tr>
         <td align="left">
         <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
         <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />
         </td>
     </tr>
 </table>  
   <table align="center" cellpadding="5" cellspacing="0" width="65%" style="border-right: #1b80b6 1pt solid; border-top: #1b80b6 1pt solid; border-left: #1b80b6 1pt solid; border-bottom: #1b80b6 1pt solid" >
        <tr class ="subheader_BlueTableView">
            <th align="left" colspan="6" style="height: 19px">
                <asp:Label ID="lblReportCaption" runat="server"></asp:Label></th>
        </tr>
        <tr>
            <td align="left" class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid">
                Business<br />
                Unit</td>
            <td class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid;">
                :</td>
            <td align="left" class="matters" colspan="4" 
                style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right-width: 1pt; border-right-color: #1b80b6">
                <uc1:usrBSUnits ID="usrBSUnits1" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid">
                Fee Type</td>
            <td class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid;">
                :</td>
            <td align="left" class="matters" colspan="4" 
                style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right-width: 1pt; border-right-color: #1b80b6">
                <asp:TreeView ID="trvFeeTypes" runat="server" 
                    onclick="client_OnTreeNodeChecked();" ShowCheckBoxes="all">
                </asp:TreeView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid">
                From Date</td>
            <td class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid;">
                :</td>
            <td align="left" class="matters" 
                style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right: #1b80b6 1pt solid;">
                <asp:TextBox ID="txtFromDate" runat="server" Width="112px"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false;" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                        runat="server" ControlToValidate="txtFromDate" ErrorMessage="From Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                            ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
            <td align="left" class="matters" colspan="1" style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right: #1b80b6 1pt solid;">
                To Date</td>
            <td align="left" class="matters" colspan="1" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid">
                :</td>
            <td align="left" class="matters" colspan="1" style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right-width: 1pt; border-right-color: #1b80b6">
                <asp:TextBox ID="txtToDate" runat="server" Width="106px"></asp:TextBox>&nbsp;<asp:ImageButton
                    ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return false" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                        runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                        ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="revToDate" runat="server" ControlToValidate="txtToDate" Display="Dynamic"
                            EnableViewState="False" ErrorMessage="Enter the To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                            ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid">
                <asp:CheckBox ID="chkDetail" runat="server" Text="Detailed" />
            </td>
            <td class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid;">
                &nbsp;</td>
            <td align="left" class="matters" 
                
                style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right: #1b80b6 1pt solid;" 
                colspan="4">
                <asp:RadioButtonList ID="rblActiv" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Active</asp:ListItem>
                    <asp:ListItem Value="2">Discontinued</asp:ListItem>
                    <asp:ListItem Value="3" Selected="True">All</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid">
                Student</td>
            <td class="matters" style="border-top-width: 1pt; border-right: #1b80b6 1pt solid; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid;">
                :</td>
            <td align="left" colspan="4" class="matters" 
                style="border-top-width: 1pt; border-left-width: 1pt; border-left-color: #1b80b6; border-top-color: #1b80b6; border-bottom: #1b80b6 1pt solid; border-right-width: 1pt; border-right-color: #1b80b6">
                <asp:RadioButton id="radStudent" runat="server" Checked="True" GroupName="STUD_ENQ"
                    Text="Student" AutoPostBack="True" OnCheckedChanged="radStudent_CheckedChanged">
                </asp:RadioButton>
                <asp:RadioButton id="radEnquiry" runat="server" GroupName="STUD_ENQ" Text="Enquiry" AutoPostBack="True" OnCheckedChanged="radEnquiry_CheckedChanged">
                </asp:RadioButton>
                <br />
                <asp:TextBox ID="txtStudName" runat="server" Width="61%" AutoPostBack="True" OnTextChanged="txtStudName_TextChanged"></asp:TextBox>
                <asp:LinkButton ID="lblAddNewStudent" runat="server" CausesValidation="False" OnClick="lblAddNewStudent_Click">Add</asp:LinkButton>
                <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="GetStudent();" /><br />
                <br />
                <asp:GridView ID="gvStudentDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    SkinID="GridViewPickDetails" Width="96%">
                    <Columns>
<asp:TemplateField HeaderText="Student ID"><ItemTemplate>
                                <asp:Label ID="lblStudID" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                            
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><ItemTemplate>
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# bind("STU_NAME") %>'></asp:Label>
                            
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField Visible="False" HeaderText="Delete">
<ItemStyle HorizontalAlign="Center"></ItemStyle>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" runat="server" >Delete</asp:LinkButton>
                            
</ItemTemplate>
</asp:TemplateField>
</Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="6" style=" text-align:right">
                <asp:LinkButton ID="lnkExporttoexcel" runat="server" OnClick="lnkExporttoexcel_Click">Export To Excel</asp:LinkButton>
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="MAINERROR" />
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" /></td>
        </tr>
    </table>
    <asp:HiddenField ID="h_STUD_ID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtFromDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgToDate" TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtToDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>


