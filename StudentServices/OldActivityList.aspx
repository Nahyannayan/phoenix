﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OldActivityList.aspx.vb" Inherits="StudentServices_OldActivityList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.js"></script>
    <script  type="text/javascript" src="../Scripts/jquery-1.10.2.min.js" ></script>
    <script  type="text/javascript" src="../Scripts/jquery-ui-1.10.2.min.js"  ></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script> 
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script> 
    <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
</head>
<body class="bg-white">

    <form id="formOldActivityList" runat="server">

        <style>
            .DisplayNoneO_AR {
                padding: 40px;
            }
        </style>
        <div>
            <i><asp:Label runat="server" Text="Please Select One Activity From Below List And Close The Window" CssClass="DisplayNoneO_AR" Visible="true"></asp:Label></i></div>
        <br />
        <div id="dialog" class="DisplayNoneO_AR" >

            <asp:RadioButtonList ID="radioOldActList" runat="server" cssclass="table table-striped"></asp:RadioButtonList>

        </div>
    </form>
    

    <script type="text/javascript" language="javascript">

        $(document).ready(function () {            
            $('#<%= radioOldActList.ClientID%>').click(function () {                                                              
                parent.setSelectedID($('#<%= radioOldActList.ClientID%>').find('input[type=radio]:checked').val());
                parent.jQuery.fancybox.close();
            });
        });

    </script>
</body>
</html>


