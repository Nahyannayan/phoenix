﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="Services_ApproveActivityOnRequest.aspx.vb" Inherits="StudentServices_Services_ApproveActivityOnRequest" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <link href="../cssfiles/bootstrap.css" rel="stylesheet" />
    


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="lblheading" runat="server" class="h5" Text="Pending Activity Request For Approval " />
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="dataTable" width="100%">
                    <tbody>
                        <tr>
                            <td colspan="4" align="left">
                                <asp:Label ID="lblError" runat="server" />
                            </td>
                        </tr>
                        <tr id="rowStuId">
                            <td width="20%"><span class="field-label">Student Id</span></td>
                            <td width="30%">
                                <asp:Label ID="lblId" runat="server" CssClass="field-value" /></td>
                            <td  width="20%"><span class="field-label">Student Name </span></td>
                            <td width="30%">
                                <asp:Label ID="lblName" runat="server" CssClass="field-value" /></td>

                        </tr>
                        <%-- <tr id="rowStuName">
                 
                
            </tr>--%>
                        <tr id="rowacdyr">
                            <td  width="20%"><span class="field-label">Academic Year</span></td>
                            <td width="30%">
                                <asp:Label ID="lblacdyr" runat="server" CssClass="field-value" /></td>
                            <td width="20%"><span class="field-label">Event Name</span></td>
                            <td width="30%">
                                <asp:Label ID="lblEventNm" runat="server" CssClass="field-value" /></td>
                        </tr>
                        <%-- <tr id="rowEventName">
               
                
            </tr>--%>
                        <tr id="rowEventdescr">
                            <td width="20%"><span class="field-label">Event Description</span> </td>
                            <td width="30%">
                                <asp:Label ID="lblEventDescr" runat="server" CssClass="field-value" /></td>
                            <td width="20%"><span class="field-label">Event Date</span> </td>
                            <td width="30%">
                                <asp:Label ID="lblEventDt" runat="server" CssClass="field-value" /></td>
                        </tr>
                        <%--  <tr id="rowEventDate">
               
               
            </tr>--%>

                        <tr id="rowEventAmt">
                            <td width="20%"><span class="field-label">Event Amount</span></td>
                            <td width="30%">
                                <asp:Label ID="lblAmnt" runat="server" CssClass="field-value" /></td>

                        </tr>
                        <tr id="rowcount" >
                            <td width="20%"><span class="field-label">Count</span></td>
                            <td width="30%">
                                 <asp:Label ID="lblCount" runat="server" CssClass="field-value" /></td>
                        </tr>
                        <%-- <tr id="rowStucomments">
               
                
            </tr>--%>
                        <tr id="rowApprvlcomments">
                            <td width="20%"><span class="field-label">Comments From  Requester</span></td>
                            <td width="30%">
                                <asp:Label ID="lblComments" runat="server" CssClass="field-value" /></td>
                            <td width="20%">
                                <asp:Label ID="lblapprvcmmnt" runat="server" Text="Please Enter Your Comments" CssClass="field-label" />
                            </td>
                            <td width="30%">
                                <textarea id="txtApprvlCmmnts" runat="server" class="textareaS_AAOR" />
                            </td>
                        </tr>
                        <tr>

                            <td colspan="4" align="center">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" OnClientClick="return Confirm()" CssClass="button" />
                                <asp:Button ID="btnReject" runat="server" Text="Reject" OnClick="btnReject_Click" OnClientClick="return Confirm()" CssClass="button" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" OnClientClick="return Confirm()" CssClass="button" />
                            </td>

                        </tr>

                    </tbody>
                </table>
                <div id="divHeading" align="center">
                    <p>
                        Requester Logged on
                        <asp:Label ID="lblLogDt" runat="server" />
                        <br />
                        <asp:Label ID="lblAprvLogg" runat="server" Text="Approver Logged On" Visible="false" />
                        <asp:Label ID="lblapprvllogdt" runat="server" Visible="false" />
                    </p>

                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript" lang="javascript">
        function Confirm() {
            return confirm('Are you sure you want to continue?');
        }
    </script>
</asp:Content>
