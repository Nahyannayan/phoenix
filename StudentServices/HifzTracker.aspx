﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="HifzTracker.aspx.vb" Inherits="HifzTracker_HifzTracker" MasterPageFile="~/mainMasterPage.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .error {
            font-family: Raleway,sans-serif;
            font-size: 7pt;
            font-weight: normal;
        }

        .verses-popover {
            position: relative;
        }

        .verse-popover-id {
            position: relative;
            display: inline-block;
            width: 50px;
        }
        .hqh-tracker {
            overflow: inherit;
        }
            .verses-popover .popover {
                transform: none !important;
                left: 30px !important;
                /*left: -85% !important;*/
                width: 186px;
            }

                .verses-popover .popover .list-group-item {
                    display: inline-block;
                    border: 0;
                    border-right: solid 1px #ccc;
                    border-radius: 0;
                    width: 46px;
                    border-bottom: solid 1px #ccc;
                    margin-bottom: 0;
                    background: transparent;
                    text-align: center;
                }

                    .verses-popover .popover .list-group-item:nth-child(4n + 4) {
                        border-right: none;
                    }

                .verses-popover .popover .arrow {
                    top: 2px !important;
                }

                .verses-popover .popover .popover-body {
                    padding: 0px;
                }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            enableVersesPopover();
        });

        function loadAddEditHifzTrackerForm(trackerId) {
            if (trackerId == undefined)
                trackerId = ''
            var oWnd = radopen("HifzTrackerAddEditForm.aspx?Id=" + trackerId, "pop_AddEditHifzTrackerForm");
        }

        function refreshGrid(radWindow, args) {
            var result = args.get_argument();
            var masterTable = $find("<%=gv_HifzTracker.ClientID%>").get_masterTableView();
            masterTable.rebind();
            setTimeout(function () {
                enableVersesPopover();
            }, 200);

        }


        function enableVersesPopover() {
            $('.verse-popover-id').each(function () {
                var targetObj = $(this);
                var versesData = $(this).closest('tr').find('.verses-data').val().split('|');

                //Create the content for popover
                var contentHtml = '<div class="border-bottom">';
                $.each(versesData, function (idx, item) {
                    contentHtml += '<div class="list-group-item" >' + item + '</div>';
                });
                contentHtml += '</div>';

                var options = {
                    trigger: 'manual', html: true, container: targetObj,
                    content: function () {
                        return contentHtml;
                    },
                    placement: 'right'
                };
                targetObj.popover(options)
                    .on("mouseenter", function () {
                        var _this = this;
                        $(this).popover("show");
                        $(this).siblings(".popover").on("mouseleave", function () {
                            $(_this).popover('hide');
                        });
                    }).on("mouseleave", function () {
                        var _this = this;
                        $(_this).popover('hide');
                    });
            });
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_AddEditHifzTrackerForm" runat="server" Behaviors="Close,Move" OnClientClose="refreshGrid"
                Width="710px" Height="575px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3 form-horizontal">
        <div class="card-header letter-space">
            <i class="fa fa-file mr-3"></i>The Holy Quran Hif'z Tracker
            <div class="pull-right">
                <asp:ImageButton ID="imgAddNew" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/edit_add.png"
                    ToolTip="Add New" OnClientClick="loadAddEditHifzTrackerForm();return false;" />
            </div>
        </div>
        
        <div class="card-body">
             <div class="mb-2">
           <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
       </div>
            <div class="table-responsive hqh-tracker">
                <telerik:RadGrid ID="gv_HifzTracker" runat="server" AllowPaging="True"
                    OnNeedDataSource="gv_HifzTracker_NeedDataSource"
                    PageSize="25" AllowSorting="True" CssClass="table table-bordered  table-row"
                    AutoGenerateColumns="False" CellSpacing="0" EnableTheming="False" GridLines="None">
                    <ClientSettings AllowDragToGroup="True">
                    </ClientSettings>
                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" AllowPaging="true" HierarchyLoadMode="ServerBind">
                        <Columns>
                            <telerik:GridBoundColumn DataField="AcademicYear" AllowFiltering="false"
                                HeaderText="Academic Year"
                                UniqueName="column0">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="GradeName" AllowFiltering="false"
                                HeaderText="Grade"
                                UniqueName="Grade">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Title"
                                HeaderText="Title"
                                UniqueName="column2">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="URL"
                                HeaderText="URL"
                                UniqueName="column3">
                            </telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn DataField="VersesCount"
                                HeaderText="No Of Verses" ItemStyle-CssClass="verses-popover"
                                UniqueName="column4">
                                  <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridTemplateColumn HeaderText="No Of Verses" ItemStyle-CssClass="verses-popover" UniqueName="column4">
                                <ItemTemplate>
                                    <span class="verse-popover-id"><%# Eval("VersesCount") %></span>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="EmployeeName" AllowFiltering="false"
                                HeaderText="Created By"
                                UniqueName="column6">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Actions" UniqueName="TemplateColumn" Groupable="False"
                                AllowFiltering="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/edit.png"
                                        ToolTip="Edit" OnClientClick='<%# "loadAddEditHifzTrackerForm("+ Eval("HifzTrackerId").ToString() + ");return false;" %>' />
                                    &nbsp; | &nbsp;
                                    <asp:ImageButton ID="btnDelete" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/ButtonImages/delete.png"
                                        ToolTip="Delete" CommandArgument='<%#Eval("HifzTrackerId")%>' OnClientClick="return confirm('Are you sure you want to delete this record?');" OnClick="btnDelete_Click" />
                                    <input type="hidden" value='<%#Eval("NoOfVerses")%>' class="verses-data" />
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" Font-Size="12px" />
                                <ItemStyle />
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                    <ItemStyle Font-Bold="true" HorizontalAlign="Left" />
                    <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                    <FilterMenu>
                    </FilterMenu>
                    <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                </telerik:RadGrid>
            </div>
        </div>
    </div>
</asp:Content>





