﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class StudentServices_FeeECA_Gen_CollectionView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                hlAddNew.NavigateUrl = "FeeECA_Gen_Collection_Add.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ECA_FEE_COLLECTION And ViewState("MainMnu_code") <> "ER01011") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case OASISConstants.MNU_FEE_ECA_FEE_COLLECTION
                            lblHead.Text = "ECA Fee Collection"
                    End Select
                    bindBusinessUnits()
                    ''  gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()



        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter


        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddlBusinessUnit.DataSource = ds.Tables(0)
            ddlBusinessUnit.DataTextField = "BSU_NAME"
            ddlBusinessUnit.DataValueField = "BSU_ID"
            ddlBusinessUnit.DataBind()
            ddlBusinessUnit.SelectedIndex = -1
            ddlBusinessUnit.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlBusinessunit_SelectedIndexChanged(ddlBusinessUnit, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub
    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        gridbind()
    End Sub
    Protected Sub btnSearchReceipt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        gridbind()
    End Sub
    Protected Sub lbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) & _
            "&isexport=0"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub lblViewReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")
            h_print.Value = "?type=REC&id=" + Encr_decrData.Encrypt(lblReceipt.Text) & _
            "&bsu_id=" & Encr_decrData.Encrypt(Session("sbsuid")) & _
            "&user=" & Encr_decrData.Encrypt(Session("sUsr_name")) & _
            "&stu_bsu_id=" & Encr_decrData.Encrypt(ddlBusinessunit.SelectedItem.Value) & _
            "&isexport=1"
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        gridbind()
    End Sub
    Protected Sub btnSearchDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        gridbind()
    End Sub


    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessunit.SelectedIndexChanged
        Try
            gridbind()
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString

            Dim ds As New DataSet
            Dim txtSearch As New TextBox
            Dim FILTER_COND As String = String.Empty
            Dim str_query As String = String.Empty
            Dim STU_NO As String = String.Empty
            Dim str_STU_NO As String = String.Empty
            Dim SNAME As String = String.Empty
            Dim str_SNAME As String = String.Empty

            Dim Receipt As String = String.Empty
            Dim str_Receipt As String = String.Empty

            Dim sDate As String = String.Empty
            Dim str_Date As String = String.Empty

            If gvJournal.Rows.Count > 0 Then

                txtSearch = gvJournal.HeaderRow.FindControl("txtReceipt")

                If txtSearch.Text.Trim <> "" Then
                    SNAME = " AND FCL_RECNO like '%" & txtSearch.Text.Trim & "%'"
                    str_SNAME = txtSearch.Text.Trim
                End If

                txtSearch = gvJournal.HeaderRow.FindControl("txtDate")

                If txtSearch.Text.Trim <> "" Then
                    sDate = " AND convert(varchar(12),FCL_DATE,110) like '%" & Convert.ToString(Convert.ToDateTime((txtSearch.Text.Trim)).ToString("MM-dd-yyyy")) & "%'"
                    str_Date = txtSearch.Text.Trim
                End If

                txtSearch = gvJournal.HeaderRow.FindControl("txtSTU_NO")

                If txtSearch.Text.Trim <> "" Then
                    STU_NO = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NO = txtSearch.Text.Trim
                End If

                txtSearch = gvJournal.HeaderRow.FindControl("txtSNAME")

                If txtSearch.Text.Trim <> "" Then
                    SNAME = " AND STU_NAME like '%" & txtSearch.Text.Trim & "%'"
                    str_SNAME = txtSearch.Text.Trim
                End If


            End If
            Dim str_topFilter As String = ""
            FILTER_COND = STU_NO + SNAME + sDate + Receipt

            If ddlTopFilter.SelectedItem.Value <> "All" Then
                str_topFilter = " top " & ddlTopFilter.SelectedItem.Value
            End If
            Dim param(5) As SqlClient.SqlParameter
            'str_Sql = "SELECT " & str_topFilter & " * FROM FEES.VW_OSO_FEES_RECEIPT where  FCL_BSU_ID='" & Session("sBSUID") & "'" _
            '& " AND isnull(FCL_bDELETED,0)=0 AND FCL_STU_BSU_ID='" & ddlBusinessunit.SelectedItem.Value & "'" & str_Filter _
            '& " ORDER BY FCL_DATE DESC ,FCL_RECNO DESC,STU_NAME"

            param(0) = New SqlClient.SqlParameter("@TopFilter", str_topFilter)
            param(1) = New SqlClient.SqlParameter("@FCL_BSU_ID", Session("sBsuid"))
            param(2) = New SqlClient.SqlParameter("@STU_BSU_ID", ddlBusinessunit.SelectedValue)
            param(3) = New SqlClient.SqlParameter("@FILTERCONDITION", FILTER_COND)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.GET_ECA_FEES_DETAILS", param)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            'txtSearch = gvJournal.HeaderRow.FindControl("txtReceiptno")
            'txtSearch.Text = lstrCondn1
            'txtSearch = gvJournal.HeaderRow.FindControl("txtDate")
            'txtSearch.Text = lstrCondn2
            'txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            'txtSearch.Text = lstrCondn3
            'txtSearch = gvJournal.HeaderRow.FindControl("txtStuno")
            'txtSearch.Text = lstrCondn4
            'txtSearch = gvJournal.HeaderRow.FindControl("txtStuname")
            'txtSearch.Text = lstrCondn5
            'txtSearch = gvJournal.HeaderRow.FindControl("txtAmount")
            'txtSearch.Text = lstrCondn6
            'txtSearch = gvJournal.HeaderRow.FindControl("txtDesc")
            'txtSearch.Text = lstrCondn7
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
End Class
