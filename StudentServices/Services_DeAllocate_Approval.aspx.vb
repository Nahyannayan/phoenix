﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class StudentServices_Services_DeAllocate_Approval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S108921") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights

                    bindAcademic_Year()
                    bindBusinessUnits()



                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If

    End Sub

    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub btnSearchServiceName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub
    Protected Sub ddlBusinessunit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBusinessUnit.SelectedIndexChanged
        Try
            Dim BSU_ID As String
            BSU_ID = ddlBusinessUnit.SelectedValue
            bindCategory()
            ''  callGrade_byBSU()
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            bindCategory()
        Catch ex As Exception
            '' lblError.InnerHtml = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        gvReq.Visible = True

        btnDeAllocate.Visible = False
  
        gridbind()

        If ddlStatus.SelectedValue = "2" Or ddlStatus.SelectedValue = "3" Then
            btnDeAllocate.Visible = False
        End If
        'BindApprovedStudents_Details()
    End Sub

    Protected Sub btnSearchGender_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        gridbind()
    End Sub

    Protected Sub gvReq_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim decodedText As String = HttpUtility.HtmlDecode(e.Row.Cells(4).Text)
                e.Row.Cells(4).Text = decodedText

                If ddlStatus.SelectedValue = "2" Or ddlStatus.SelectedValue = "3" Then 'Approved/Rejected
                    e.Row.Cells(0).Visible = False
                    gvReq.HeaderRow.Cells(0).Visible = False

                    e.Row.Cells(5).Visible = False
                    gvReq.HeaderRow.Cells(5).Visible = False
                End If

            End If
        Catch ex As Exception

        End Try



    End Sub

    Protected Sub btnDeAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeAllocate.Click

        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            lblError.Text = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
            gridbind()

        End If
    End Sub

    Protected Sub gvReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReq.PageIndexChanging
        gvReq.PageIndex = e.NewPageIndex
        
        gridbind()
    End Sub

    Sub bindBusinessUnits()
        ddlBusinessunit.Items.Clear()



        Dim ds As New DataSet
        Dim PARAM(5) As SqlParameter


        Try
            PARAM(0) = New SqlParameter("@providerBsuId", Session("sbsuId"))
            PARAM(1) = New SqlParameter("@userId", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@menuCode", ViewState("MainMnu_code"))
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
            CommandType.StoredProcedure, "SM.Get_GMA_BusinessUnit", PARAM)

            ddlBusinessUnit.DataSource = ds.Tables(0)
            ddlBusinessUnit.DataTextField = "BSU_NAME"
            ddlBusinessUnit.DataValueField = "BSU_ID"
            ddlBusinessUnit.DataBind()
            ddlBusinessUnit.SelectedIndex = -1
            ddlBusinessUnit.Items.Insert(0, New ListItem("All", "0"))
            ddlBusinessunit_SelectedIndexChanged(ddlBusinessUnit, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind pdc")
        End Try
    End Sub

    Private Sub bindAcademic_Year()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@INFO_TYPE", "ACD")
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@CLM_ID", Session("CLM"))


            Using YEAR_DESCRreader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "SM.GETACADEMIC_DATABIND", PARAM)
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then

                    ddlAcademicYear.DataSource = YEAR_DESCRreader
                    ddlAcademicYear.DataTextField = "ACY_DESCR"
                    ddlAcademicYear.DataValueField = "ACD_ID"
                    ddlAcademicYear.DataBind()


                End If
            End Using

            If Not Session("Current_ACD_ID") Is Nothing Then
                If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
                    ddlAcademicYear.ClearSelection()
                    ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            End If

            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub callGrade_byBSU()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If


            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@INFO_TYPE", "BSU_CURRENT_GRADE")
            PARAM(1) = New SqlParameter("@BSU_ID", ddlBusinessUnit.SelectedValue)


            'Using Grade_ACDReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "SM.GETACADEMIC_DATABIND", PARAM)
            '    ddlGrade.Items.Clear()
            '    'di = New ListItem("ALL", "0")
            '    'ddlGrade.Items.Add(di)
            '    If Grade_ACDReader.HasRows = True Then
            '        ddlGrade.DataSource = Grade_ACDReader
            '        ddlGrade.DataTextField = "GRD_DISPLAY"
            '        ddlGrade.DataValueField = "GRD_ID"
            '        ddlGrade.DataBind()

            '        'While Grade_ACDReader.Read
            '        '    di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
            '        '    ddlGrade.Items.Add(di)
            '        'End While

            '    End If
            'End Using
            '' ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub gridbind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim ds As DataSet
            Dim txtSearch As New TextBox
            Dim str_query As String = String.Empty
            Dim STU_NO As String = String.Empty
            Dim str_STU_NO As String = String.Empty
            Dim STUD_NO_FILTER = String.Empty
            Dim SNAME As String = String.Empty
            Dim str_SNAME As String = String.Empty

            Dim GENDER As String = String.Empty
            Dim str_GENDER As String = String.Empty


            Dim STU_SERVICES As String = String.Empty
            Dim str_STU_SERVICES As String = String.Empty
            Dim REQ_SERVICES As String = String.Empty

            Dim GRD_SCT As String = String.Empty
            Dim str_GRD_SCT As String = String.Empty
            Dim FILTER_COND As String = String.Empty

            Dim PARAM(7) As SqlParameter


            If gvReq.Rows.Count > 0 Then


                txtSearch = gvReq.HeaderRow.FindControl("txtSTU_NO")

                If txtSearch.Text.Trim <> "" Then
                    STU_NO = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_STU_NO = txtSearch.Text.Trim
                End If


                txtSearch = gvReq.HeaderRow.FindControl("txtSNAME")

                If txtSearch.Text.Trim <> "" Then
                    SNAME = " AND ( STU_FIRSTNAME like '%" & txtSearch.Text.Trim & "%' OR STU_LASTNAME like '%" & txtSearch.Text.Trim & "%')"
                    str_SNAME = txtSearch.Text.Trim
                End If



                txtSearch = gvReq.HeaderRow.FindControl("txtGender")

                If txtSearch.Text.Trim <> "" Then
                    GENDER = " AND GENDER like '%" & txtSearch.Text.Trim & "%'"
                    str_GENDER = txtSearch.Text.Trim
                End If
                'txtSearch = gvReq.HeaderRow.FindControl("txtServiceName")

                'If txtSearch.Text.Trim <> "" Then
                '    STU_SERVICES = " AND STU_SERVICES like '%" & txtSearch.Text.Trim & "%'"
                '    str_STU_SERVICES = txtSearch.Text.Trim
                'End If
                If ViewState("lnkStatus") = "P" Then 'Pending
                    REQ_SERVICES = " AND SSR_APPR_FLAG = 'P' "
                    ViewState("lnkStatus") = ""
                End If

                If ViewState("lnkStatus") = "A" Then
                    REQ_SERVICES = " AND SSR_APPR_FLAG = 'A' "
                    ViewState("lnkStatus") = ""
                End If

                If ViewState("lnkStatus") = "R" Then 'Rejected
                    REQ_SERVICES = " AND SSR_APPR_FLAG = 'R' "
                    ViewState("lnkStatus") = ""
                End If

            End If



            If txtStuNo.Text.Trim <> "" Then
                STUD_NO_FILTER = " AND replace(STU_NO,' ','') like '%" & txtStuNo.Text.Trim.Replace(" ", "") & "%'"
            End If

            Dim Status As String = "P"
            If ddlStatus.SelectedValue = "1" Then
                Status = "P"
            ElseIf ddlStatus.SelectedValue = "3" Then
                Status = "R"
            Else
                Status = "A"
            End If


            FILTER_COND = STU_NO + SNAME + GENDER + STU_SERVICES + GRD_SCT + REQ_SERVICES + STUD_NO_FILTER
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@GRD_ID", "")
            PARAM(2) = New SqlParameter("@BSU_ID", ddlBusinessUnit.SelectedValue)
            PARAM(3) = New SqlParameter("@FILTER_COND", FILTER_COND)
            'PARAM(4) = New SqlParameter("@GENDER", hfGender.Value)
            PARAM(4) = New SqlParameter("@SVC_ID", ddlCategory.SelectedValue)
            PARAM(5) = New SqlParameter("@SVC_BSU_ID", Session("sBsuid"))
            PARAM(6) = New SqlParameter("@Status", Status)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ST.GET_STUDENT_GEN_DISONTINUE_REQUEST", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvReq.DataSource = ds.Tables(0)
                gvReq.DataBind()
                tblRequestedList.Visible = True
                btnSave.Enabled = True
                btnCancel.Enabled = True
                btnDeAllocate.Visible = True
            Else
                btnSave.Enabled = False
                btnCancel.Enabled = False
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                '' ds.Tables(0).Rows(0)(6) = True
                ''ds.Tables(0).Rows(0)("ENABLE_CHECKBOX") = False
                ''ds.Tables(0).Rows(0)("SHOW_SERVICE") = False
                ''ds.Tables(0).Rows(0)("SHOW_SERVICE_TOGGLE") = False
                ''ds.Tables(0).Rows(0)("STU_SERVICES") = ""

                gvReq.DataSource = ds.Tables(0)
                Try
                    gvReq.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvReq.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvReq.Rows(0).Cells.Clear()
                gvReq.Rows(0).Cells.Add(New TableCell)
                gvReq.Rows(0).Cells(0).ColumnSpan = columnCount
                gvReq.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvReq.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            lblInfo.Text = IIf(Status = "P", "REQUESTED FOR DISCONTINUE", IIf(Status = "A", "DISCONTINUATION APPROVED", IIf(Status = "R", "DISCONTINUATION REJECTED", "")))


            'txtSearch = gvReq.HeaderRow.FindControl("txtSTU_NO")
            'txtSearch.Text = str_STU_NO

            'txtSearch = gvReq.HeaderRow.FindControl("txtSNAME")
            'txtSearch.Text = str_SNAME
            ''txtSearch = gvReq.HeaderRow.FindControl("txtServiceName")
            ''txtSearch.Text = str_STU_SERVICES
            'txtSearch = gvReq.HeaderRow.FindControl("txtGender")
            'txtSearch.Text = str_GENDER
            ''txtSearch = gvReq.HeaderRow.FindControl("txtGRD_SCT")
            ''txtSearch.Text = str_GRD_SCT


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindCategory()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            'If ddlGrade.SelectedIndex = -1 Then
            '    GRD_ID = ""
            'Else
            '    GRD_ID = ddlGrade.SelectedItem.Value
            'End If
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@sBSU_ID", ddlBusinessUnit.SelectedValue)
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Using CategoryReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "ST.GET_GMA_SVC_BY_BSU", PARAM)
                ddlCategory.Items.Clear()


                If CategoryReader.HasRows = True Then

                    ddlCategory.DataSource = CategoryReader
                    ddlCategory.DataTextField = "SVC_DESCRIPTION"
                    ddlCategory.DataValueField = "SVC_ID"
                    ddlCategory.DataBind()
                    ddlCategory.Items.Insert(0, New ListItem("-All-", "0"))
                Else
                    ddlCategory.Items.Insert(0, New ListItem("-No Activity Exists-", "0"))

                End If
            End Using
            '' ddlCategory_SelectedIndexChanged(ddlCategory, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer

        Dim tran As SqlTransaction

        Dim SSD_IDs As String = String.Empty


        Using CONN As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim param(3) As SqlParameter

                Dim ReturnFlag As Integer

                For Each gvrow As GridViewRow In gvReq.Rows
                    Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
                    If chk IsNot Nothing And chk.Checked Then
                        SSD_IDs += gvReq.DataKeys(gvrow.RowIndex).Value.ToString() + ","c

                    End If
                Next

                param(0) = New SqlParameter("@SSD_IDs", SSD_IDs)
                param(1) = New SqlParameter("@UserId", Session("sUsr_id").ToString)

                param(3) = New SqlParameter("@EmpId", Convert.ToInt32(Session("EmployeeId").ToString))
                param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "ST.APPROVE_DEALLOCATION_REQUEST ", param)
                ReturnFlag = param(2).Value


                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("SVB_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    'resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function
End Class
