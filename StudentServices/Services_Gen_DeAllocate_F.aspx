﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Services_Gen_DeAllocate_F.aspx.vb"
    Inherits="StudentServices_Services_Gen_DeAllocate_F" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link media="screen" href="../cssfiles/Popup.css" type="text/css" rel="Stylesheet" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/M/yy',
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true
            });
            //$("#txtDate").datepicker('setDate', new Date());
        }
    );
        function fancyClose() {
            if ($("#<%=hf_bSuccess.ClientID%>").val() == 1) {
                parent.$.fancybox.close();
                parent.LoadServices(1);
            }
            else {
                parent.$.fancybox.close();
            }
        }
        function DisableButton() {
            document.getElementById("<%=btnRequest.ClientID%>").disabled = true;
        }
        window.onbeforeunload = DisableButton;
    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">
        <asp:Label ID="lblError" runat="server"></asp:Label>
        <asp:HiddenField ID="hf_bSuccess" runat="server" Value="0" />
        <center>
            <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                <tr class="title">
                    <td style="height: 25px" align="left">
                        <asp:Literal ID="ltHeader" runat="server" Text="Discontinue Request"></asp:Literal>
                        <asp:HiddenField ID="hdnSelected" runat="server" />
                    </td>
                </tr>
            </table>
            <table width="80%" cellpadding="5" border="1" cellspacing="2" class="BlueTablePopup"
                id="tblCategory">
                <tr>
                    <td class="matters">Student :
                    </td>
                    <td class="matters">
                        <asp:Label ID="lblName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="matters">Activity :
                    </td>
                    <td class="matters">
                        <asp:Label ID="lblActivity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="matters">Discontinue date
                    </td>
                    <td class="matters">
                        <input type="text" id="txtDate" class="datepicker" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="matters">Remarks
                    </td>
                    <td>
                        <textarea id="txtRemarks" runat="server" cols="24" rows="6"></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="matters" colspan="2" align="center">
                        <asp:Button ID="btnRequest" runat="server" Text="DISCONTINUE" CssClass="button" />
                        <input type="button" class="button" id="btnCancel1" title="Close" value="Close"
                            onclick="fancyClose()" />
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
