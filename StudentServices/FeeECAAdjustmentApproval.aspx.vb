﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data
Partial Class StudentServices_FeeECAAdjustmentApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                If USR_NAME = "" Or ViewState("MainMnu_code") <> OASISConstants.MNU_FEE_ECA_ADJUSTMENT_APPROVAL Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Dim CurBsUnit As String = Session("sBsuid")
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                gvFeeDetails.Attributes.Add("bordercolor", "#1b80b6")
                gvPendingHistory.Attributes.Add("bordercolor", "#1b80b6")
                Session("sFEE_ADJUSTMENT") = Nothing
                bindAdjustmentReason()
                If ViewState("datamode") = "view" Then
                    Dim FAH_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("FAH_ID").Replace(" ", "+"))
                    Dim vFEE_ADJ As ECAFeeAdjustmentREQ = ECAFeeAdjustmentREQ.GetFeeAdjustments(FAH_ID)

                    Session("sTotalFeePaid") = ECAFeeAdjustmentREQ.GetTotalFeePaid(vFEE_ADJ.FAH_STU_ID)
                    Session("sFEEPaidCurAcdYr") = ECAFeeAdjustmentREQ.GetFeePaidForCurrentAcademicYear(vFEE_ADJ.FAH_STU_ID, vFEE_ADJ.FAH_ACD_ID)

                    txtReqDate.Text = Format(vFEE_ADJ.FAH_DATE, OASISConstants.DateFormat)
                    txtAppDate.Text = Format(Now.Date, OASISConstants.DateFormat)
                    txtHeaderRemarks.Text = vFEE_ADJ.FAH_REMARKS
                    txtApprRemarks.Text = txtHeaderRemarks.Text
                    lblStudName.Text = vFEE_ADJ.FAH_STU_NAME
                    lblStudNo.Text = vFEE_ADJ.FAH_STU_NO
                    lblReqDocno.Text = vFEE_ADJ.FAR_DOCNO
                    'h_STUD_ID.Value = vFEE_ADJ.FAH_STU_ID
                    lblStudName_To.Text = vFEE_ADJ.FAR_TO_STU_NAME
                    lblLastatDate.Text = vFEE_ADJ.LASTATTDATE
                    If lblLastatDate.Text <> "" Then
                        txtApprRemarks.Text = txtHeaderRemarks.Text & " ( LDA : " & lblLastatDate.Text & " ) "
                    End If
                    If vFEE_ADJ.FAR_TO_STU_NAME <> "" Then
                        tr_StudName_To.Visible = True

                        gvFeeDetails.Columns(2).Visible = True
                    Else
                        tr_StudName_To.Visible = False

                        gvFeeDetails.Columns(2).Visible = False
                    End If
                    lblAcademicYear.Text = vFEE_ADJ.vFAR_ACD_DISPLAY
                    lblAcdYr.Text = vFEE_ADJ.vFAR_ACD_DISPLAY
                    If vFEE_ADJ.vJOIN_DATE = CDate("1/1/1900") Then
                        trJoinDet.Visible = False
                    Else
                        lblJDate.Text = Format(vFEE_ADJ.vJOIN_DATE, OASISConstants.DateFormat)
                        lblJGrade.Text = vFEE_ADJ.vJOIN_GRD_ID
                        lblJoinAcademicYear.Text = vFEE_ADJ.vJOIN_ACD_DISPLAY
                    End If

                    lblGrade.Text = vFEE_ADJ.GRM_DISPLAY

                    ddlAdjReason.DataBind()
                    If Not ddlAdjReason.Items.FindByValue(vFEE_ADJ.FAR_EVENT) Is Nothing Then
                        ddlAdjReason.SelectedIndex = -1
                        ddlAdjReason.Items.FindByValue(vFEE_ADJ.FAR_EVENT).Selected = True
                    End If
                    'txtAppDate.Text = Format(Date.Now, OASISConstants.DateFormat)
                    If vFEE_ADJ.APPR_STATUS <> "N" Then
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                        ChkPrint.Enabled = False
                    End If
                    Dim str_pending_req As String = "SELECT CASE FCH_DRCR WHEN 'CR' THEN 'Conc' ELSE 'Conc Cancel' END AS Descr, " _
                    & " AMOUNT as Amount, REPLACE(CONVERT(VARCHAR(11), FCH_DT, 113), ' ', '/') AS Date, FCH_REMARKS AS Narration, " _
                    & " FCM_DESCR 'Conc Descr' FROM FEES.vw_OSO_FEES_GETFEECONCESSIONTRANS " _
                    & " WHERE (FCH_BSU_ID = '" & Session("sBsuid") & "')  AND (FCH_STU_ID = '" & vFEE_ADJ.FAH_STU_ID & "') AND (ISNULL(FCH_bPosted, 0) = 0)" _
                    & " order by FCH_DT desc"
                    gvPendingHistory.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, str_pending_req)
                    gvPendingHistory.DataBind()
                    Session("sFEE_ADJUSTMENT") = vFEE_ADJ
                    GridBindAdjustments()
                End If
                DISABLE_TAX_FIELDS()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAdjustmentReason()
        ddlAdjReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.Get_Adjustment_Reason")
        ddlAdjReason.DataSource = ds
        ddlAdjReason.DataTextField = "ARM_DESCR"
        ddlAdjReason.DataValueField = "ARM_ID"
        ddlAdjReason.DataBind()
        'ddlAdjReason.Items.Insert(0, New ListItem("-Select Reason-", "0"))
    End Sub
    Private Sub GridBindAdjustments()
        Dim vFEE_ADJ As ECAFeeAdjustmentREQ
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ = Session("sFEE_ADJUSTMENT")
            gvFeeDetails.DataSource = ECAFeeAdjustmentREQ.GetSubDetailsAsDataTable(vFEE_ADJ)
            gvFeeDetails.DataBind()
        Else
            gvFeeDetails.DataSource = Nothing
            gvFeeDetails.DataBind()
        End If
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()
        If HttpContext.Current.Session("sTotalFeePaid") Is Nothing Then
            Return String.Empty
        End If
        Dim dt As DataTable = HttpContext.Current.Session("sTotalFeePaid")
        Dim STUD_ID As Integer = contextKey
        If dt Is Nothing OrElse dt.Rows.Count < 0 Then
            Return sTemp.ToString()
        End If
        Dim dv As New DataView(dt)
        Dim dgvRow As DataRowView
        dv.RowFilter = " FSH_FEE_ID = " & contextKey
        Dim i As Integer
        While (i < dv.Count)
            dgvRow = dv.Item(i)
            If i = 0 Then
                sTemp.Append("<table >")
                sTemp.Append("<tr>")
                sTemp.Append("<td colspan=8><b>FEE Details For " & dgvRow("FEE_DESCR") & " </b></td>")
                sTemp.Append("</tr>")
                sTemp.Append("<tr>")
                sTemp.Append("<td><b>DATE</b></td>")
                sTemp.Append("<td><b>ACD. YEAR</b></td>")
                sTemp.Append("<td><b>GRADE</b></td>")
                sTemp.Append("<td><b>SOURCE</b></td>")
                sTemp.Append("<td><b>DR/CR</b></td>")
                sTemp.Append("<td><b>CHARG. AMT</b></td>")
                sTemp.Append("<td><b>PAID AMT</b></td>")
                sTemp.Append("<td><b>ACT DATE</b></td>")
                sTemp.Append("</tr>")
            End If
            sTemp.Append("<tr>")
            sTemp.Append("<td>" & Format(dgvRow("FSH_DATE"), OASISConstants.DateFormat) & "</td>")
            sTemp.Append("<td>" & dgvRow("ACY_DESCR") & "</td>")
            sTemp.Append("<td>" & dgvRow("GRD_DISPLAY") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_SOURCE") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_DRCR") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_AMOUNT") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_SETTLEAMT") & "</td>")
            sTemp.Append("<td>" & Format(dgvRow("FSH_ACTDATE"), OASISConstants.DateFormat) & "</td>")
            sTemp.Append("</tr>")
            i += 1
        End While
        sTemp.Append("</table>")

        Return sTemp.ToString()
    End Function

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContentCurrentAcademicYear() As String
        Dim sTemp As New StringBuilder()
        If HttpContext.Current.Session("sFEEPaidCurAcdYr") Is Nothing Then
            Return String.Empty
        End If
        Dim dt As DataTable = HttpContext.Current.Session("sFEEPaidCurAcdYr")
        sTemp.Append("<table >")
        sTemp.Append("<tr>")
        sTemp.Append("<td colspan=3><b>FEE Paid details For the Academic Year</b></td>")
        sTemp.Append("</tr>")
        sTemp.Append("<tr>")
        sTemp.Append("<td><b>FEE DESCR</b></td>")
        sTemp.Append("<td><b>CHARG. AMT</b></td>")
        sTemp.Append("<td><b>PAID AMT</b></td>")
        sTemp.Append("</tr>")
        If dt Is Nothing OrElse dt.Rows.Count < 0 Then
            sTemp.Append("</table>")
            Return sTemp.ToString()
        End If
        Dim dv As New DataView(dt)
        Dim dgvRow As DataRowView
        Dim i As Integer
        While (i < dv.Count)
            dgvRow = dv.Item(i)
            sTemp.Append("<tr>")
            sTemp.Append("<td>" & dgvRow("FEE_DESCR") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_AMOUNT") & "</td>")
            sTemp.Append("<td>" & dgvRow("FSH_SETTLEAMT") & "</td>")
            sTemp.Append("</tr>")
            i += 1
        End While
        sTemp.Append("</table>")
        Return sTemp.ToString()
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Private Function IsValidApprovalDate() As Boolean
        If CDate(txtAppDate.Text) < CDate(txtReqDate.Text) Then
            'lblError.Text = "Approval Date should not be less than Requested Date"
            usrMessageBar2.ShowNotification("Approval Date should not be less than Requested Date", UserControls_usrMessageBar.WarningType.Danger)
            Return False
        End If
        Return True
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = "Sessions not matching...."
            usrMessageBar2.ShowNotification("Sessions not matching....", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Convert.ToDecimal(txtTotalAmount.Text) = 0 Then
            'lblError.Text = "Invalid Total Amount...."
            usrMessageBar2.ShowNotification("Invalid Total Amount....", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        If Not IsValidApprovalDate() Then
            Exit Sub
        End If
        Dim vFEE_ADJ_APPR As ECAFeeAdjustmentREQ
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection
        Dim trans As SqlTransaction
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            trans = conn.BeginTransaction("sFEE_ADJUSTMENTAPPROVAL")
            Try
                vFEE_ADJ_APPR = SaveApprovedAmounts(Session("sFEE_ADJUSTMENT"))
                If vFEE_ADJ_APPR Is Nothing Then
                    trans.Rollback()
                    Exit Sub
                End If
                vFEE_ADJ_APPR.APPR_DATE = CDate(txtAppDate.Text)
                vFEE_ADJ_APPR.APPROVAL_REMARKS = txtApprRemarks.Text
                vFEE_ADJ_APPR.FAR_USER = Session("sUsr_name")
                Dim NEW_FAH_ID As String = String.Empty
                Dim retVal As Integer = ECAFeeAdjustmentREQ.F_SAVEFEEADJAPPROVAL(vFEE_ADJ_APPR, True, NEW_FAH_ID, conn, trans)
                If retVal = 0 Then
                    trans.Commit()
                    'lblError.Text = "The Adjustment Approved..."
                    usrMessageBar2.ShowNotification("The Adjustment Approved...", UserControls_usrMessageBar.WarningType.Success)
                    ViewState("datamode") = "none"
                    ClearAll()
                    Dim isHaedtoHead As Boolean = False
                    If ddlAdjReason.SelectedItem.Value = "4" Then
                        isHaedtoHead = True
                    End If
                    If (ChkPrint.Checked) Then
                        PrintReceipt(NEW_FAH_ID)
                    End If

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_KEY As String = "FEE ADJUSTMENT APPROVAL"
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NEW_FAH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                Else
                    trans.Rollback()
                    'lblError.Text = UtilityObj.getErrorMessage(retVal)
                    usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
                End If
            Catch ex As Exception
                trans.Rollback()
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = UtilityObj.getErrorMessage(1000)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(1000), UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If Not Master.IsSessionMatchesForSave() Then
            'lblError.Text = "Sessions not matching...."
            usrMessageBar2.ShowNotification("Sessions not matching....", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If
        Dim vFEE_ADJ_APPR As ECAFeeAdjustmentREQ
        Dim conn As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection
        Dim trans As SqlTransaction
        If Not Session("sFEE_ADJUSTMENT") Is Nothing Then
            vFEE_ADJ_APPR = Session("sFEE_ADJUSTMENT")
            vFEE_ADJ_APPR.FAR_USER = Session("sUsr_name")
            vFEE_ADJ_APPR.APPR_DATE = CDate(txtAppDate.Text)
            vFEE_ADJ_APPR.APPROVAL_REMARKS = txtApprRemarks.Text
            trans = conn.BeginTransaction("sFEE_ADJUSTMENTREJECT")
            Dim NEW_FAH_ID As String = String.Empty
            Dim retVal As Integer = ECAFeeAdjustmentREQ.F_SAVEFEEADJAPPROVAL(vFEE_ADJ_APPR, False, NEW_FAH_ID, conn, trans)
            If retVal > 0 Then
                trans.Rollback()
                'lblError.Text = UtilityObj.getErrorMessage(retVal)
                usrMessageBar2.ShowNotification(UtilityObj.getErrorMessage(retVal), UserControls_usrMessageBar.WarningType.Danger)
            Else
                'trans.Rollback()
                trans.Commit()
                'lblError.Text = "The Adjustment Rejected..."
                usrMessageBar2.ShowNotification("The Adjustment Rejected...", UserControls_usrMessageBar.WarningType.Success)
                ClearAll()
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Dim str_KEY As String = "FEE ADJUSTMENT REJECT"
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, NEW_FAH_ID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
            End If
        End If
    End Sub

    Private Function SaveApprovedAmounts(ByVal vFEE_ADJ_APPR As ECAFeeAdjustmentREQ) As ECAFeeAdjustmentREQ
        If vFEE_ADJ_APPR Is Nothing Then Return Nothing
        Dim txtApprAmount As TextBox
        Dim vSUB_DET As ECAFEEADJUSTMENT_S
        Dim lblFEE_ID As Label
        Dim vFeeID As Integer
        Try
            If gvFeeDetails.Rows.Count > 0 Then
                For Each drGVRow As GridViewRow In gvFeeDetails.Rows
                    txtApprAmount = drGVRow.FindControl("txtApprAmt")
                    Dim lblTaxCode As Label = drGVRow.FindControl("lblTaxCode")
                    Dim lblTaxAmount As Label = drGVRow.FindControl("lblTaxAmount")
                    Dim lblNetAmount As Label = drGVRow.FindControl("lblNetAmount")
                    lblFEE_ID = drGVRow.FindControl("lblFEE_ID")
                    If (Not txtApprAmount Is Nothing) AndAlso (Not lblFEE_ID Is Nothing) Then
                        vFeeID = lblFEE_ID.Text
                        vSUB_DET = vFEE_ADJ_APPR.FEE_ADJ_DET(vFeeID)
                        If vSUB_DET.FAD_AMOUNT >= 0 Then
                            If CDec(txtApprAmount.Text) < 0 Then
                                'lblError.Text = "Invalid Amount!!!"
                                usrMessageBar2.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                                Return Nothing
                            End If
                            If CDec(txtApprAmount.Text) > vSUB_DET.FAD_AMOUNT Then
                                'lblError.Text = "Approved Amount should not be more than Requested.."
                                usrMessageBar2.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                                Return Nothing
                            End If
                        Else
                            If CDec(txtApprAmount.Text) > 0 Then
                                'lblError.Text = "Invalid Amount!!!"
                                usrMessageBar2.ShowNotification("Invalid Amount!!!", UserControls_usrMessageBar.WarningType.Danger)
                                Return Nothing
                            End If
                            If CDec(txtApprAmount.Text) < vSUB_DET.FAD_AMOUNT Then
                                'lblError.Text = "Approved Amount should not be more than Requested.."
                                usrMessageBar2.ShowNotification("Approved Amount should not be more than Requested..", UserControls_usrMessageBar.WarningType.Danger)
                                Return Nothing
                            End If
                        End If
                        vSUB_DET.APPROVEDAMOUNT = CDec(txtApprAmount.Text)
                        vSUB_DET.FAD_TAX_CODE = lblTaxCode.Text
                        vSUB_DET.FAD_TAX_AMOUNT = lblTaxAmount.Text
                        vSUB_DET.FAD_NET_AMOUNT = lblNetAmount.Text
                    End If
                Next
            End If
        Catch ex As Exception
            Return Nothing
        End Try
        Return vFEE_ADJ_APPR
    End Function


    Private Sub ClearAll()
        txtReqDate.Text = ""
        txtHeaderRemarks.Text = ""
        lblStudName.Text = ""
        lblAcademicYear.Text = ""
        lblJDate.Text = ""
        lblJGrade.Text = ""
        lblJoinAcademicYear.Text = ""
        lblGrade.Text = ""
        txtAppDate.Text = ""
        txtApprRemarks.Text = ""
        lblReqDocno.Text = ""
        gvFeeDetails.DataSource = Nothing
        gvFeeDetails.DataBind()
    End Sub

    Protected Sub PrintReceipt(ByVal FAH_ID As Integer)
        Try
            Dim isHaedtoHead As Boolean = False
            If ddlAdjReason.SelectedItem.Value = "4" Then
                isHaedtoHead = True
            End If
            Session("ReportSource") = ECAFEEADJUSTMENT.PrintFEEAdjustmentVoucher(FAH_ID, Session("sUsr_name"), isHaedtoHead)
            h_print.Value = "print"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub txtApprAmt_TextChanged(sender As Object, e As EventArgs)
        Dim gvr As GridViewRow = sender.parent.parent
        Dim txtApprAmt As TextBox = sender
        If Not txtApprAmt Is Nothing AndAlso Not gvr Is Nothing Then
            Dim lblTaxCode As New Label, lblTaxAmount As New Label, lblNetAmount As New Label
            lblTaxCode = gvr.FindControl("lblTaxCode")
            lblTaxAmount = gvr.FindControl("lblTaxAmount")
            lblNetAmount = gvr.FindControl("lblNetAmount")
            If Not lblNetAmount Is Nothing And Not lblTaxAmount Is Nothing And Not lblTaxCode Is Nothing Then
                CALCULATE_TAX(lblTaxCode.Text, txtApprAmt.Text, lblTaxAmount, lblNetAmount)
            End If
        End If
    End Sub
    Private Sub CALCULATE_TAX(ByVal TaxCode As String, ByVal Amount As Double, ByRef lblTaxAmt As Label, ByRef lblNetAmt As Label)
        lblTaxAmt.Text = "0.00"
        lblNetAmt.Text = "0.00"
        If IsNumeric(Amount) Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM TAX.GetTAXAmount(" & Amount & ",'" & TaxCode & "')")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTaxAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString), "#,##0.00")
                lblNetAmt.Text = Format(Convert.ToDouble(ds.Tables(0).Rows(0)("NET_AMOUNT").ToString), "#,##0.00")
            End If
        End If
    End Sub
    Private Sub DISABLE_TAX_FIELDS() 'hide tax related fields if the unit is not TAX enabled
        Dim bShow As Boolean = False
        If Not Session("BSU_bVATEnabled") Is Nothing AndAlso IsNumeric(Session("BSU_bVATEnabled")) Then
            bShow = Convert.ToBoolean(Session("BSU_bVATEnabled"))
        End If
        gvFeeDetails.Columns(9).Visible = bShow
        gvFeeDetails.Columns(10).Visible = bShow
        gvFeeDetails.Columns(11).Visible = bShow
       
    End Sub
End Class
