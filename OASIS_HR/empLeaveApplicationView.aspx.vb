Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
'Version        Date             Author          Change
'1.1            20-Feb-2011      Swapna          To filter view screen based on logged in user's id
'2.0            08-Aug-2019      Vikranth        Added 2 more parameters in gridbind() method while exec GetLeaveDetails procedure
Partial Class Payroll_empLeaveApplicationView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Session("sModule") = "SS" Then
            Me.MasterPageFile = "../mainMasterPageSS.master"
        Else
            Me.MasterPageFile = "../mainMasterPage.master"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empLeaveApplication.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
            Page.Title = OASISConstants.Gemstitle
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "P130015" And ViewState("MainMnu_code") <> "U000022") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            'str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "'"     'V1.1 comment
            'Last two parameters (ALL,1) added by vikranth on 8th Aug 2019
            str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "','" & Session("sUsr_name") & "','All',1"    'V1.1
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            gvJournal.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "empLeaveApplication.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            Dim lblAPPRSTATUS As New Label
            lblAPPRSTATUS = TryCast(e.Row.FindControl("lblAPPRSTATUS"), Label)
            Dim hlPrint As New LinkButton
            hlPrint = TryCast(e.Row.FindControl("hlPrint"), LinkButton)
            If hlPrint IsNot Nothing And lblAPPRSTATUS IsNot Nothing Then
                If lblAPPRSTATUS.Text = "A" Then
                    hlPrint.Enabled = True
                Else
                    hlPrint.Enabled = False
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim printId As String = ""
            Dim gvCell As DataControlFieldCell
            gvCell = sender.parent
            If gvCell Is Nothing Then Exit Sub
            Dim gvRow As GridViewRow
            gvRow = gvCell.Parent
            If gvRow Is Nothing Then Exit Sub
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(gvRow.FindControl("lblELA_ID"), Label)
            If lblELA_ID Is Nothing Then Exit Sub
            printId = lblELA_ID.Text
            printId = Replace(printId, "A", "")
            Dim params As New Hashtable
            params.Add("@IMG_BSU_ID", Session("sbsuid"))
            params.Add("@IMG_TYPE", "LOGO")
            params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
            params.Add("@ELA_ID", CInt(printId))
            params.Add("@DocType", "LEAVE")
            ' params("VoucherName") = labHead.Text.ToUpper().ToString()   'V1.1
            params("reportHeading") = "LEAVE APPLICATION FORM"
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "Oasis"
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../Payroll/Reports/Rpt/rptLeaveAppForm.rpt")
                'End If
            End With
            Session("rptClass") = rptClass
            If Session("sModule") = "SS" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewerSS.aspx")
            Else
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request cannot be processed"
        End Try
    End Sub
End Class
