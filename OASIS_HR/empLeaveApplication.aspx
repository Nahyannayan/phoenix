<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empLeaveApplication.aspx.vb" Inherits="Payroll_empLeaveApplication"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_Edit() {

            if (confirm("You are about to edit a forwarded/approved leave application. Editing requires all the approvers to reapprove the changes. Do you want to proceed ?") == true)
                return true;
            else
                return false;

        }

        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            <%-- result = window.showModalDialog("../Accounts/accShowEmpDetail.aspx?id=EL", "", sFeatures)
            if (result != '' && result != undefined) {
                NameandCode = result.split('___');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
            //Comment and added below line by vikranth on 1st Aug 2019
            //var url = "../Accounts/accShowEmpDetail.aspx?id=EL";
            var url = "../Accounts/accShowEmpDetail.aspx?id=ELEAVE";
            var oWnd = radopen(url, "pop_getempname");
        }
        function OnClientClose1(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_Emp_No.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpNo.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpNo.ClientID%>', 'TextChanged');
            }
        }
        function showhide_leavetype(id) {
            if (document.getElementById(id).className + '' == 'display_none') {
                document.getElementById(id).className = '';
            }
            else {
                document.getElementById(id).className = 'display_none';
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       <Windows>
            <telerik:RadWindow ID="popup1" runat="server" Behaviors="Close,Move" 
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_getempname" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user-plus mr-3"></i> Leave Application
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table width="100%" align="center">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" EnableViewState="False"
                    CssClass="error"></asp:Label>
                
            </td>
        </tr>
        <tr>
            <td colspan="4" align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="SingleParagraph"
                    CssClass="error" ForeColor="" HeaderText="Enter following fields:-" ValidationGroup="Datas" />
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle">Fields Markedwith(<span
                class="text-danger">*</span>)are mandatory
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    <table width="100%" align="center">
        
        <tr>
            <td align="left" width="20%"><span class="field-label">Select employee</span><span class="text-danger">*</span>
            </td>
            <td align="left" width="30%">
                <asp:TextBox ID="txtEmpNo" runat="server" ></asp:TextBox>&nbsp;<asp:ImageButton
                    ID="imgEmployee" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="GetEMPName(); return false;"
                    CausesValidation="False" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmpNo"
                    CssClass="error" ErrorMessage="Employee Name:" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator><br />
                <asp:LinkButton ID="lnkEmpLeaves" runat="server" CausesValidation="False">Leave Status</asp:LinkButton>
                <asp:LinkButton ID="hdlnkEmpLeaves" runat="server" Visible="false"></asp:LinkButton>
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="lnkEmpLeaves"
                    DynamicControlID="Panel2" PopupControlID="Panel2" DynamicServiceMethod="GetDynamicContent"
                    CommitProperty="value" Position="bottom" CommitScript="">
                </ajaxToolkit:PopupControlExtender>
            </td>
            <td align="left" width="20%"><span class="field-label">Leave Type</span></td>
            <td align="left" width="30%">
                <asp:DropDownList ID="ddMonthstatusPeriodically" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                        <span class="field-label">Joining Date</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtLRejoindt" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
        </tr>
        <tr>
            <td align="left" class="title-bg" colspan="4">Select a Period
            </td>
        </tr>        
        <tr>
                    <td align="left">
                        <span class="field-label">From</span><span class="text-danger">*</span>
                    </td>
                    <td align="left" >
                        <asp:TextBox ID="txtFrom" runat="server" AutoPostBack="True" OnTextChanged="txtFrom_TextChanged"></asp:TextBox><asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                <asp:RequiredFieldValidator runat="server" ID="rfvFromDate" ControlToValidate="txtFrom"
                    ErrorMessage=" From Date:" ValidationGroup="Datas" Display="None">*</asp:RequiredFieldValidator>
                    </td>
                    <td align="left">
                        <span class="field-label">To</span> <span class="text-danger">*</span>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                <asp:RequiredFieldValidator runat="server" ID="rfvToDate" ControlToValidate="txtTo"
                    ErrorMessage="To Date:" ValidationGroup="Datas">*</asp:RequiredFieldValidator>&nbsp;
                &nbsp;
                        <br />
                <asp:LinkButton ID="lnkPlanner" runat="server" CausesValidation="False">Leave Planner</asp:LinkButton>
                    </td>
                    
               
        </tr>
        <tr>
            <td align="left"><span class="field-label">Details</span><span class="text-danger">*</span>
            </td>
            <td align="left">
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                    TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtRemarks"
                    ErrorMessage="Details:" Display="None" ValidationGroup="Datas">*</asp:RequiredFieldValidator>
              
            </td>
        
            <td align="left"><span class="field-label">Work will be handed to</span><span class="text-danger">*</span>
            </td>
            <td align="left">
                <asp:TextBox ID="txtHandover" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                    TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtHandover"
                    ErrorMessage="Work handover details:" Display="None" ValidationGroup="Datas">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="trUplaod" runat="server">
            <td align="left" width="20%">
                <%--<asp:ImageButton ID="imgAmended" runat="server" BorderColor="#CC9900"
                    BorderStyle="Solid" ImageUrl="~/Images/animated-news.gif"
                     />--%>
                <span class="field-label">Medical Documents</span></td>
            <td colspan="3" align="left" valign="top" nowrap="nowrap">
                <asp:FileUpload ID="filupload" runat="server" Width="364px" />
                &nbsp;&nbsp;&nbsp;
               
                <asp:Label ID="lblView" runat="server" Text="Click to view uploaded document :"
                    Display="None" Visible="False" CssClass="matters_Colln"></asp:Label>
                &nbsp;
                
                      <asp:HyperLink ID="imgDoc" runat="server" ImageUrl="../Images/ViewDoc.png" Visible="false" ToolTip="Click To View Document">View</asp:HyperLink>
                &nbsp;&nbsp;
                <asp:ImageButton ID="imgDelete" runat="server" ToolTip="Click To Delete Document" ImageUrl="../Images/Oasis_Hr/Images/cross.png" Visible="false" />

            </td>
        </tr>
        <tr>
            <td colspan="4" class="title-bg" align="left">Contact Details During Leave
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Contact Address</span>
                <span class="text-danger">*</span>
            </td>
            <td align="left">
                <asp:TextBox ID="txtAddress" runat="server" CssClass="inputbox_multi" SkinID="MultiText"
                    TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAddress"
                    ErrorMessage="Contact Address:" Display="None" ValidationGroup="Datas">*</asp:RequiredFieldValidator>
            </td>
       
            <td align="left"><span class="field-label">Phone</span>
            </td>
            <td align="left">
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left"><span class="field-label">Mobile</span><span class="text-danger">*</span>
            </td>
            <td align="left">
                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMobile"
                    ErrorMessage="Mobile Number:" ValidationGroup="Datas">*</asp:RequiredFieldValidator>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trPObox" runat="server" visible="false">
            <td align="left"><span class="field-label">Po Box</span><span class="text-danger">*</span>
            </td>
            <td align="left">
                <asp:TextBox ID="txtPobox" runat="server">0</asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:CheckBox ID="chkForward" runat="server" CssClass="field-label" Text="Forward For Approval" Checked="True" />&nbsp;
                <asp:CheckBox runat="server" CssClass="field-label" ID="chkSaveAnyway" Text="Continue anyway" Visible="false"></asp:CheckBox>
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" CausesValidation="False" />
                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="Datas" />
                <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" CausesValidation="False" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                <asp:Button ID="btnPrintLeave" runat="server" CssClass="button" Text="Print"
                    Visible="False" />
                <asp:Button ID="btnCancelLeave" runat="server" CssClass="button"
                    Text="Cancel Leave Application" CausesValidation="False" Visible="False" />
            </td>

        </tr>
        <tr>
            <td id="tdMessage" runat="server" style="visibility: hidden;" align="left" colspan="4">*
        Accurate leave balance including any 2012 carryover leave days will be updated
            and displayed from 15th Jan 2013.
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
        SelectCommand="SELECT [ELT_ID], [ELT_DESCR] FROM [EMPLEAVETYPE_M] WHERE ELT_bLEAVE=1"></asp:SqlDataSource>
    <asp:HiddenField ID="h_Emp_No" runat="server" />
    <asp:HiddenField ID="h_ELA_ID" runat="server" />
    <asp:HiddenField ID="h_DOC_PCD_ID" runat="server" />
    <asp:HiddenField ID="h_ID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
        PopupButtonID="imgFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtFrom" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="txtTo" TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
    <div id="divDetail" runat="server" width="100%">
        <asp:Panel ID="Panel2" runat="server" CssClass="panel-cover">
            <asp:Repeater ID="rptGridViewLeave" runat="server">
                <HeaderTemplate>
                    <table width="100%">
                        <tr class="title-bg">
                            <th>Leave Type
                            </th>
                            <th>Eligible
                            </th>
                            <th>Availed
                            </th>
                            <th>Available
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#Container.DataItem("Leave Type")%>
                        </td>
                        <td>
                            <%#Container.DataItem("Eligible")%>
                        </td>
                        <td>
                            <%#Container.DataItem("Taken")%>
                        </td>
                        <td>
                            <%#Container.DataItem("Balance")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
    </div>

                </div>
            </div>
        </div>

</asp:Content>
