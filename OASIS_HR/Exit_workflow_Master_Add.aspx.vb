﻿
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Exit_workflow_Master_Add
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

      
        If Not Page.IsPostBack Then
            ViewState("EXT_WF_USR_ID") = "0"
            If Request.QueryString("ID") IsNot Nothing Then

                If Request.QueryString("ID").Length > 1 Then

                    Dim Encr_decrData As New Encryption64
                    ViewState("EXT_WF_USR_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                        btnAddPDC.Text = "UPDATE"
                End If
                'bindTrainersGrid()
            End If
            LoadDepartment()

            bindEmployeeGrid()
            If Convert.ToInt64(ViewState("EXT_WF_USR_ID")) > 0 Then
                ddlDepartment.Enabled = False
            End If

        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
        End Try
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        bindEmployeeGrid()
    End Sub

    Private Sub bindEmployeeGrid()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlClient.SqlParameter
        Dim ds As DataSet
        Dim DT As DataTable
        Dim str_query As String = ""



        Dim str_FullName As String = String.Empty
        Dim str_BSU_Name As String = String.Empty
        Dim FullName As String = String.Empty
        Dim BSU_Name As String = String.Empty
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty

        Try
            If gvPDC.Rows.Count > 0 Then

                txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NAME")

                If txtSearch.Text.Trim <> "" Then
                    FullName = " AND replace(EMP_NAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_FullName = txtSearch.Text.Trim
                End If


            End If
            FILTER_COND = FullName + BSU_Name

            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@FILTERCONDITION", FILTER_COND)
            param(2) = New SqlParameter("@DEPT_ID", ddlDepartment.SelectedValue)
            param(3) = New SqlParameter("@EXT_WF_USR_ID", ViewState("EXT_WF_USR_ID"))


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_EMPLOYEE_LIST]", param)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPDC.DataSource = ds
                gvPDC.DataBind()
                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "Currently there is no Employee Exists"
            Else
                gvPDC.DataSource = ds
                gvPDC.DataBind()
            End If
            txtSearch = gvPDC.HeaderRow.FindControl("txtEMP_NAME")
            txtSearch.Text = str_FullName
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindEmployeeGrid")
        End Try

    End Sub
     
    Protected Sub btnSearchEMP_NAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        bindEmployeeGrid()
    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click
        Dim errormsg As String = String.Empty
        For Each gvrow As GridViewRow In gvPDC.Rows
            Dim chk As CheckBox = DirectCast(gvrow.FindControl("chkChild"), CheckBox)
            Dim emp_id As Label = DirectCast(gvrow.FindControl("lblId"), Label)
            Dim dpt_id As Label = DirectCast(gvrow.FindControl("lblDPT_ID"), Label)
            Dim EXT_WF_USR_ID As Label = DirectCast(gvrow.FindControl("lblEXT_WF_USR_ID"), Label)
            Dim chkEXT_WF_ACTIVE As CheckBox = DirectCast(gvrow.FindControl("chkEXT_WF_ACTIVE"), CheckBox)
            Dim chkIncludeInWorkFlow As CheckBox = DirectCast(gvrow.FindControl("chkIncludeInWorkFlow"), CheckBox)
            Dim chkEXT_WF_USR_IS_HR As CheckBox = DirectCast(gvrow.FindControl("chkEXT_WF_USR_IS_HR"), CheckBox)
            ' 
            If chk IsNot Nothing And chk.Checked Or Convert.ToInt64(EXT_WF_USR_ID.Text) > 0 Then

                If callTransaction(errormsg, emp_id.Text, dpt_id.Text, Convert.ToInt64(EXT_WF_USR_ID.Text), chkEXT_WF_ACTIVE.Checked, chkIncludeInWorkFlow.Checked, chkEXT_WF_USR_IS_HR.Checked) <> 0 Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
                Else
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:green;padding:5pt;background-color:white;'>Data Saved Successfully.</div>"

                End If

            End If


        Next

        ''checking if seat is available

        'Ids = Ids.Trim(",".ToCharArray())
        'hdnSelected.Value = Ids
        'Session("liEmpList") = Ids
        'hdnSelectedTrainersSelection.Value = Ids
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "scriptexitWflow", "setEmpToParent();", True)


    End Sub


    Private Function callTransaction(ByRef errormsg As String, ByVal EXT_WF_EMP_ID As Integer, ByVal EXT_WF_DPT_ID As Integer, ByVal EXT_WF_USR_ID As Integer, ByVal chkEXT_WF_ACTIVE As Boolean, ByVal chkIncludeInWorkFlow As Boolean, ByVal EXT_WF_USR_IS_HR As Boolean) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try
                Dim param(10) As SqlParameter
                Dim RegionLoc As String = ""
                Dim LocStatus = ""
                param(0) = New SqlParameter("@EXT_WF_USR_EMP_ID", EXT_WF_EMP_ID)
                param(1) = New SqlParameter("@EXT_WF_USR_DPT_ID", EXT_WF_DPT_ID)
                param(2) = New SqlParameter("@EXT_WF_USR_BSU_ID", Session("sBsuid"))
                param(3) = New SqlParameter("@EXT_WF_USR_CREATED_BY", Session("sUsr_name"))
                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue
                param(5) = New SqlParameter("@EXT_WF_USR_ID", EXT_WF_USR_ID)
                param(6) = New SqlParameter("@EXT_WF_USR_ACTIVE", chkEXT_WF_ACTIVE)
                param(8) = New SqlParameter("@EXT_WF_USR_INCLUDE_IN_WORKFLOW", chkIncludeInWorkFlow)
                param(9) = New SqlParameter("@EXT_WF_USR_IS_HR", EXT_WF_USR_IS_HR)
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SAVE_EXIT_WORKFLOW_USERS]", param)
                Dim ReturnFlag As Integer = param(4).Value


                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("datamode") = "none"
                    callTransaction = "0"
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Sub LoadDepartment()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            'Dim param(2) As SqlClient.SqlParameter
            'param(0) = New SqlClient.SqlParameter("@EMP_ID", 0)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_DEPARTMENT_LIST]")

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                ddlDepartment.DataTextField = "DPT_DESCR"
                ddlDepartment.DataValueField = "DPT_ID"
                ddlDepartment.DataSource = DT
                ddlDepartment.DataBind()
                ' Insert the first item.
                ddlDepartment.Items.Insert(0, New ListItem("- Select a Department -", "0"))

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "LoadDepartment")
        End Try

    End Sub
    
    Protected Sub ddlDepartment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDepartment.SelectedIndexChanged
        bindEmployeeGrid()
    End Sub
End Class


 