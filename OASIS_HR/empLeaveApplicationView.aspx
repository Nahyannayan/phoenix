<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="empLeaveApplicationView.aspx.vb" Inherits="Payroll_empLeaveApplicationView"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">  
 
 function ShowLeaveDetail(id)
       {    
            var sFeatures;
            sFeatures="dialogWidth: 700px; ";
            sFeatures+="dialogHeight: 450px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            //result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)
         
     //return false;
            var url = "../Payroll/ShowEmpLeave.aspx?ela_id="+id;
            var oWnd = radopen(url, "pop_showleavedetail");
        }
   


 function autoSizeWithCalendar(oWindow) {
     var iframe = oWindow.get_contentFrame();
     var body = iframe.contentWindow.document.body;

     var height = body.scrollHeight;
     var width = body.scrollWidth;

     var iframeBounds = $telerik.getBounds(iframe);
     var heightDelta = height - iframeBounds.height;
     var widthDelta = width - iframeBounds.width;

     if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
     if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
     oWindow.center();
 }

    </script>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="MAINERROR" />

    <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
       
        <Windows>
            <telerik:RadWindow ID="pop_showleavedetail" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>




     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user-plus mr-3"></i>  Leave Application
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table align="center" width="100%" >
        <tr valign="top">
            <td valign="top" align="left">
                <asp:HyperLink id="hlAddNew" runat="server">Add New</asp:HyperLink>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
            
        </tr>
    </table>
    <a id='top'></a>
    <table width="100%" align="center" >
        
        <tr>
            <td align="center" width="100%">
              <asp:GridView ID="gvJournal" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" DataKeyNames="EMPNO"
                    SkinID="GridViewView" Width="100%" AllowPaging="True" PageSize="30" >
                    <Columns>
                        <asp:BoundField DataField="EMP_NAME" HeaderText="Name" ReadOnly="True" 
                            SortExpression="EMP_NAME">
                        </asp:BoundField>
                        <asp:BoundField DataField="EMPNO" HeaderText="Staff ID" ReadOnly="True" 
                            SortExpression="EMPNO">
                        </asp:BoundField>
                        <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR">
                        </asp:BoundField>
                        <asp:BoundField DataField="LEAVEDAYS" HeaderText="Leave Days" ReadOnly="True" SortExpression="LEAVEDAYS">
                        </asp:BoundField>
                        <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date From"
                            HtmlEncode="False" SortExpression="ELA_DTFROM"></asp:BoundField>
                        <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date To"
                            HtmlEncode="False" SortExpression="ELA_DTTO"></asp:BoundField>
                        <asp:BoundField DataField="ELA_APPRSTATUS" HeaderText="Status" ReadOnly="True" SortExpression="ELA_APPRSTATUS">
                        </asp:BoundField>
                        <asp:BoundField DataField="ELA_APPRDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Approval Date"
                            HtmlEncode="False" SortExpression="ELA_APPRDATE"></asp:BoundField>
                        <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="<%# &quot;ShowLeaveDetail('&quot; & Container.DataItem(&quot;ELA_ID&quot;) & &quot;');return false;&quot; %>"
                                    Text='<%# Bind("ELA_REMARKS") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Print">
                            <ItemTemplate>
                                <asp:LinkButton ID="hlPrint" runat="server" Text="Print" OnClick="btnPrint_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="APPRSTATUS" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAPPRSTATUS" runat="server" Text='<%# Bind("APPRSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>

                </div>
            </div>
         </div>
</asp:Content>
