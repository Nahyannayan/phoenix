Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class HRVisa_Appr
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then



            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H675005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("VR_ID") = "0"

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    ViewState("VR_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))


                    BINDREQ_ID()
                    bindReq_APPRV()



                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub BINDREQ_ID()

        Dim NAT As String = String.Empty
        Dim QLF As String = String.Empty
        Dim STR_CONN As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim PARAM(2) As SqlClient.SqlParameter
        PARAM(0) = New SqlParameter("@VR_ID", ViewState("VR_ID"))

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.StoredProcedure, "[VISA].[GETVISA_REQ_MAIN]", PARAM)

            If DATAREADER.HasRows = True Then

                While DATAREADER.Read

                    ltCat.Text = "<font color='black'>" & Convert.ToString(DATAREADER("CATEGORY_DES")) & "</font>"
                    ltSchool.Text = "<font color='black'>" & Convert.ToString(DATAREADER("BSU_NAME")) & "</font>"
                    ltGender.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_GENDER")) & "</font>"
                    ltStatus.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_STATUS")) & "</font>"
                    ltPurpose.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_PURPOSE")) & "</font>"
                    ltAppName.Text = "<font color='black'>" & Convert.ToString(DATAREADER("APPL_NAME")) & "</font>"
                    ltPassNo.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_PASSPORTNO")) & "</font>"
                    ltTitle.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_TITLE")) & "</font>"
                    ltVisaType.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VM_TYPE")) & "</font>"
                    ltStartDt.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_DOC_DT")) & "</font>"
                    ltQlf.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_EDU_QLF")) & "</font>"
                    ltAddr.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_HOME_ADDR")) & "</font>"
                    ltFather.Text = "<font color='black'>" & Convert.ToString(DATAREADER("FATHER_NAME")) & "</font>"
                    ltMother.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MOTHER_NAME")) & "</font>"
                    ltProc_fee.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_PROC_FEE")) & "</font>"
                    ltComments.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_COMMENTS")) & "</font>"
                    ltNational.Text = "<font color='black'>" & Convert.ToString(DATAREADER("App_nat")) & "</font>"
                    ltDOB.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_DOB")) & "</font>"
                    ltBudget.Text = "<font color='black'>" & Convert.ToString(DATAREADER("bBUDGETTED")) & "</font><div>" & Convert.ToString(DATAREADER("VR_BUDGETTED_DESCR")) & "</div>"
                    If Convert.ToBoolean(DATAREADER("bHISTORY")) = True Then
                        lbHistory.Visible = True

                        bindReq_History()
                    Else
                        lbHistory.Visible = False
                    End If


                End While
            End If

        End Using
    End Sub
    Sub clear_all()


    End Sub

    Private Sub bindReq_APPRV()
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim ds As New DataSet

        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@VR_ID", ViewState("VR_ID"))
        param(1) = New SqlParameter("@USR_ID", Session("sUsr_id"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "VISA.GETVISA_REQ_APPR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvAppr.DataSource = ds
            gvAppr.DataBind()
        End If



    End Sub

    Private Sub bindReq_History()
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@VR_ID", ViewState("VR_ID"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[VISA].[GEVISA_REQ_HISTORY]", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvSchool.DataSource = ds
            gvSchool.DataBind()
        End If



    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            ViewState("VR_ID") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                clear_all()


                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvAppr_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAppr.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblProcess As LinkButton = DirectCast(e.Row.FindControl("lblPROC_STATUS"), LinkButton)
            Dim lblLEVEL_ID As Label = DirectCast(e.Row.FindControl("lblLEVEL_ID"), Label)

            If lblProcess.Enabled = True Then
                lblProcess.Attributes.Add("onclick", "javascript:Apprvdata('" & ViewState("VR_ID") & "','" & lblLEVEL_ID.Text & "','" & Session("sUsr_id") & "'); return true;")
            End If
        End If

    End Sub
End Class
