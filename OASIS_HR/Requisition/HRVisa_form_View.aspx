<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="HRVisa_form_View.aspx.vb"  Inherits="HRVisa_form_view" Theme="General" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 
<%@ Register Src="~/OASIS_HR/Requisition/Control/PROG_BAR.ascx" TagName="Progressbar" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


   <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" width="50%">
                <asp:Literal id="ltHeader" runat="server" Text="Visa Requisation"></asp:Literal></td>
        </tr>
    </table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
            <tr valign="top" > 
                <td align="left">
                    &nbsp;<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>&nbsp;</td>
            </tr>
            <tr valign="top" > 
                <td align="left">
                <asp:LinkButton id="lbAddNew" runat="server" Font-Bold="True" onclick="lbAddNew_Click">Add New</asp:LinkButton></td>
            </tr>
        </table>  <a id='top'></a>
    <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" width="98%">
            <tr class="subheader_img" height="19px" valign="top" >
                <td align="left" colspan="2" valign="middle">
                  Business Unit  <asp:DropDownList ID="ddlBSU_Add" runat="server" style="margin-left: 0px" 
                                    AutoPostBack="True"  >
                                </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="center" class="matters" colspan="2" valign="top" > 
           <asp:GridView ID="gvManageUsers" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" DataKeyNames="VR_ID" PageSize="30" 
                                        SkinID="GridViewView" Width="100%"  BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <HeaderTemplate>
                                                    <table >
                                                        <tr>
                                                            <td align="center" >
                                                                <asp:Label ID="lblsRNOH" runat="server" CssClass="gridheader_text" Text="Sr.No"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CATEGORY_DES" SortExpression="CATEGORY_DES">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <table>
                                                        <tr>
                                                            <td align="center"><asp:Label ID="lblCategoryH" runat="server" CssClass="gridheader_text" 
                                                                    Text="Category" Width="100px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr><td>                                                                            
                                                                            <asp:TextBox ID="txtCategory_des" runat="server" Width="135px"></asp:TextBox>
                                                                        </td>
                                                                        <td  valign="middle">
                                                                            <asp:ImageButton ID="btnSearchCategory_Des" runat="server" ImageAlign="Top" 
                                                                                ImageUrl="~/Images/forum_search.gif" onclick="btnSearchCategory_Des_Click" 
                                                                                />
                                                                           </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCategory_Des" runat="server" 
                                                        Text='<%# Bind("Category_Des") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Title/Position">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <table >
                                                        <tr>
                                                            <td align="center" >
                                                                <asp:Label ID="lblVR_TITLEH" runat="server" CssClass="gridheader_text" 
                                                                    Text="Title/Position" Width="100px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtVR_TITLE" runat="server" Width="170px"></asp:TextBox>
                                                                        </td>
                                                                        <td valign="middle">
                                                                            <asp:ImageButton ID="btnSearchVR_TITLE" runat="server" ImageAlign="Top" 
                                                                                ImageUrl="~/Images/forum_search.gif" onclick="btnSearchVR_TITLE_Click"  />
                                                                            </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVR_TITLE" runat="server" Text='<%# Bind("VR_TITLE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Doc. Date">
                                                <HeaderTemplate>
                                                    <table >
                                                        <tr>
                                                            <td align="center" >
                                                                <asp:Label ID="lblVR_DOC_DTH" 
                                                                    runat="server" CssClass="gridheader_text" 
                                                                    Text="Doc. Date" Width="100px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" >
                                                                    <tr>
                                                                        <td >
                                                                            <asp:TextBox ID="txtVR_DOC_DT" runat="server" Width="80px"></asp:TextBox>
                                                                        </td>
                                                                        <td valign="middle">
                                                                            <asp:ImageButton ID="btnSearchVR_DOC_DT" runat="server" ImageAlign="Top" 
                                                                                ImageUrl="~/Images/forum_search.gif" onclick="btnSearchVR_DOC_DT_Click" 
                                                                                />
                                                                            </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVR_DOC_DT" runat="server" Text='<%# Bind("VR_DOC_DT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Budgeted">
                                                <HeaderTemplate>
                                                    <table >
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblBUDGETEDH" runat="server" CssClass="gridheader_text" 
                                                                    Text="Budgeted" Width="100px"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtBUDGETTED" runat="server" Width="88px"></asp:TextBox>
                                                                        </td>
                                                                        <td  valign="middle">
                                                                            <asp:ImageButton ID="btnSearchBUDGETED" runat="server" ImageAlign="Top" 
                                                                                ImageUrl="~/Images/forum_search.gif" onclick="btnSearchBUDGETED_Click" 
                                                                                 />
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBUDGETTED" runat="server" 
                                                        Text='<%# bind("BUDGETTED") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Applicant Name">
                                                <HeaderTemplate>
                                                    <table>
                                                        <tr>
                                                            <td align="center" >
                                                                <asp:Label ID="lblAPPl_NAMEH" runat="server" CssClass="gridheader_text" 
                                                                    Text="Applicant Name"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAPPl_NAME" runat="server" Text='<%# bind("APPl_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Current Status">
                                                <HeaderTemplate>
                                                    <table>
                                                        <tr>
                                                            <td align="center" >
                                                                <asp:Label ID="lblVR_PROG_STATUSH" runat="server" CssClass="gridheader_text" 
                                                                    Text="Current Status"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVR_PROG_STATUS" runat="server" 
                                                        Text='<%# bind("VR_PROG_STATUS") %>'></asp:Label>
                                                          <br />
                                                    <uc1:Progressbar ID="Progressbar1" runat="server" BGColor="white" Blocks="20" 
                                                        BorderColor="#1b80b6" BorderSize="1" Cellpadding="1" CellSpacing="0" 
                                                        FillColor="Green" Height="8" Value='<%# bind("PER_VALUE") %>' Width="130" Visible='<%# bind("bPRG_SHOW") %>'
                                                        Current_STATUS='<%# bind("VR_PROG_STATUS") %>' />
                                                        
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reopen Requisation">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbReopen" runat="server" Enabled='<%# bind("flag") %>' 
                                                        onclick="lbReopen_Click">Reopen</asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblReopenReqH" runat="server" CssClass="gridheader_text" 
                                                        Text="Reopen Requisation" Width="93px"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnView" runat="server" onclick="lbtnView_Click">View</asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblviewH" runat="server" CssClass="gridheader_text" Text="View" 
                                                        Width="73px"></asp:Label>
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VR_ID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVR_ID" runat="server" 
                                                        Text='<%# BIND("VR_ID") %>'></asp:Label>
                                                          <asp:Label ID="lblVR_DOC_ID" runat="server" 
                                                        Text='<%# BIND("VR_DOC_ID") %>'></asp:Label> 
                                                </ItemTemplate>
                                               
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
            
            
            </td>
            </tr>
            </table>
            <div id="plReopen" runat="server" style="display: block; overflow: visible; border-color: #1b80b6;
                    border-style: solid; border-width: 1px; width: 600px; background-color:White;">
                    <div class="msg_header" style="width: 600px; margin-top: 1px; vertical-align: middle;
                        background-color: White;">
                        <div class="msg" style="margin-top: 0px; vertical-align: middle; text-align: left;">
                            <span style="clear: left; display: inline; float: left; visibility: visible;">Reopen Requisation</span> <span style="clear: right; display: inline; float: right; visibility: visible;
                                    margin-top: -4px; vertical-align: top;">
                            
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../../Images/closeme.png"
                                        Style="margin-top: -1px; vertical-align: top;" /></span>
                        </div>
                    </div>
                     <asp:Panel ID="Panel1" runat="server" Width="600px" Height="320px" BackColor="White"
                        BorderColor="#1b80b6" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Both">
                        <div>
                        
                       <table  BorderColor="#1b80b6" border="1"
                    CellPadding="6" CellSpacing="2" class="BlueTableView"   style="width: 100%">
                   
                    
                    <tr class="matters">
                        <td  align="left" style="height:20px">Business unit </td>
                        <td >:</td>
                        <td align="left" colspan="4">  
                            <asp:Literal ID="ltBSU" runat="server"></asp:Literal>
                          
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td  align="left" style="height:20px">Category</td>
                        <td>:</td>
                        <td  align="left" colspan="4"> 
                           <asp:Literal ID="ltCategory" runat="server"></asp:Literal>
                          
                        </td>  

                       
                    </tr>
                    <tr  class="matters">
                        <td align="left" style="height:20px">Title/Position</td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                           
                          <asp:Literal ID="ltTitle" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td align="left">Remark<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                           
                          <asp:TextBox ID="txtRemark" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="450px" ></asp:TextBox>
                        </td>
                    </tr>
                    </table>
                    <div align="center" style ="padding-top:12px">
                    <asp:Button ID="btnSaveReopen" runat="server" Text="Save" CssClass="button" 
                            width="90px" Height="27px" ValidationGroup="reopen"  ></asp:Button>
                    </div>
                    <div align="left" style ="padding-top:12px">
                    <asp:label ID="ltReopen_Error" runat="server"  ></asp:label>
                    </div>
                    </div>
                        </asp:Panel></div>
            
  <asp:Button ID="btnShowReopen" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mdlReopen" runat="server" TargetControlID="btnShowReopen"
                    PopupControlID="plReopen"  BackgroundCssClass="modalBackground"
                    DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:HiddenField ID="hfVR_ID" runat="server" />   
          <asp:HiddenField ID="hfDOC_ID" runat="server" />   
</asp:Content>