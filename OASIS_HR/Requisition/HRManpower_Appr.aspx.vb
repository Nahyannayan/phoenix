Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class HRManpower_Appr
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then



            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H680005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("MPR_ID") = "0"
                  
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                   
                    ViewState("MPR_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))


                    BINDREQ_ID()
                    bindReq_APPRV()



                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub BINDREQ_ID()

        Dim NAT As String = String.Empty
        Dim QLF As String = String.Empty
        Dim STR_CONN As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim PARAM(2) As SqlClient.SqlParameter
        PARAM(0) = New SqlParameter("@MPR_ID", ViewState("MPR_ID"))

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.StoredProcedure, "HR.[GETMANPOWER_M_MAIN]", PARAM)

            If DATAREADER.HasRows = True Then

                While DATAREADER.Read

                    ltCat.Text = "<font color='black'>" & Convert.ToString(DATAREADER("CATEGORY_DES")) & "</font>"
                    ltSchool.Text = "<font color='black'>" & Convert.ToString(DATAREADER("BSU_NAME")) & "</font>"
                    ltGender.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_GENDER")) & "</font>"
                    ltStatus.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_STATUS")) & "</font>"
                    ltType.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_JOB_TYPE")) & "</font>"
                    ltDur.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_JOB_TYPE_DESCR")) & "</font>"
                    ltBug.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_BUDGETED")) & "</font>"
                    ltTitle.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_TITLE")) & "</font>"
                    ltContr.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_CONTRACT_TYPE")) & "</font>"
                    ltStartDt.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_STARTDT")) & "</font>"
                    ltNo_Vac.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_NO_VAC")) & "</font>"
                    ltRes_Vac.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_VAC_DESCR")) & "</font>"

                    ltPos.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_RECRUITED_FOR")) & "</font>"
                    ltProp.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_BENEFITS")) & "</font>"
                    ltKey.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_KEY_RESP")) & "</font>"
                    ltMin_Edu.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_MIN_EDU_QLF")) & "</font>"
                    ltNat.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_NAT")) & "</font>"
                    ltMin_exp.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_MIN_EXP")) & "</font>"
                    ltAdd_info.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_ADD_INFO")) & "</font>"
                    ltComm.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_COMMENTS")) & "</font>"

           
                    If Convert.ToBoolean(DATAREADER("MPR_bHISTORY")) = True Then
                        lbHistory.Visible = True

                        bindReq_History()
                    Else
                        lbHistory.Visible = False
                    End If


                End While
            End If

        End Using
    End Sub
    Sub clear_all()
      

    End Sub

    Private Sub bindReq_APPRV()
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim ds As New DataSet

        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@MPR_ID", ViewState("MPR_ID"))
        param(1) = New SqlParameter("@USR_ID", Session("sUsr_id"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.GETMANPOWER_REQ_APPR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvAppr.DataSource = ds
            gvAppr.DataBind()
        End If



    End Sub

    Private Sub bindReq_History()
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@MPH_MPR_ID", ViewState("MPR_ID"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.GETMANPOWER_REQ_HISTORY", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvSchool.DataSource = ds
            gvSchool.DataBind()
        End If



    End Sub



    
    Function saveReq_form(ByVal bEDIT As Boolean, ByVal errorMessage As String) As String

        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty
        Dim NAT As New StringBuilder
        Dim QLF As New StringBuilder

        Using conn As SqlConnection = ConnectionManger.GetOASIS_HRConnection

            Try

                transaction = conn.BeginTransaction("SampleTransaction")
                Dim pParms(25) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MPR_ID", ViewState("MPR_ID"))
                pParms(1) = New SqlClient.SqlParameter("@MPR_BSU_ID", "")
                pParms(2) = New SqlClient.SqlParameter("@MPR_CAT_ID", "")
                pParms(3) = New SqlClient.SqlParameter("@MPR_STARTDT", "")
                pParms(4) = New SqlClient.SqlParameter("@MPR_TITLE", "")
                pParms(5) = New SqlClient.SqlParameter("@MPR_GENDER", "")
                pParms(6) = New SqlClient.SqlParameter("@MPR_STATUS", "")
                pParms(7) = New SqlClient.SqlParameter("@MPR_NO_VAC", "")
                pParms(8) = New SqlClient.SqlParameter("@MPR_VAC_DESCR", "")
                pParms(9) = New SqlClient.SqlParameter("@MPR_JOB_TYPE", "")
                pParms(10) = New SqlClient.SqlParameter("@MPR_JOB_TYPE_DESCR", "")
                pParms(11) = New SqlClient.SqlParameter("@MPR_CONTRACT_TYPE", "")
                pParms(12) = New SqlClient.SqlParameter("@MPR_BUDGETED", "")
                pParms(13) = New SqlClient.SqlParameter("@MPR_RECRUITED_FOR", "")
                pParms(14) = New SqlClient.SqlParameter("@MPR_BENEFITS", "")
                pParms(15) = New SqlClient.SqlParameter("@MPR_KEY_RESP", "")
                pParms(16) = New SqlClient.SqlParameter("@MPR_MIN_EXP", "")
                pParms(17) = New SqlClient.SqlParameter("@MPR_ADD_INFO", "")
                pParms(18) = New SqlClient.SqlParameter("@MPR_COMMENTS", "")
                pParms(19) = New SqlClient.SqlParameter("@MPR_MIN_EDU_QLF", QLF.ToString)
                pParms(20) = New SqlClient.SqlParameter("@MPR_NAT", NAT.ToString)
                pParms(21) = New SqlClient.SqlParameter("@MPR_ADD_USR", Session("sUsr_id"))
                pParms(22) = New SqlClient.SqlParameter("@bEDIT", bEDIT)
                pParms(23) = New SqlClient.SqlParameter("@OUT_MPR_ID", SqlDbType.BigInt)
                pParms(23).Direction = ParameterDirection.Output
                pParms(24) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(24).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "HR.SAVEMANPOWER_REQ_M", pParms)
                Status = pParms(24).Value



                If Status <> 0 Then
                    saveReq_form = "1"
                    Return "1"
                Else
                    ViewState("MPR_ID") = pParms(23).Value.ToString
                End If


                saveReq_form = "0"
                ViewState("datamode") = "edit"


                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Catch ex As Exception
                saveReq_form = "1"
                errorMessage = "Error Occured While Saving."
                UtilityObj.Errorlog(ex.Message)
            Finally
                If saveReq_form <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

 

   
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            ViewState("MPR_ID") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                clear_all()


                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvAppr_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAppr.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblProcess As LinkButton = DirectCast(e.Row.FindControl("lblPROC_STATUS"), LinkButton)
            Dim lblLEVEL_ID As Label = DirectCast(e.Row.FindControl("lblLEVEL_ID"), Label)

            If lblProcess.Enabled = True Then
                lblProcess.Attributes.Add("onclick", "javascript:Apprvdata('" & ViewState("MPR_ID") & "','" & lblLEVEL_ID.Text & "','" & Session("sUsr_id") & "'); return true;")
            End If
        End If

    End Sub
End Class
