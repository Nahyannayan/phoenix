Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class HRApprovalInfo
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
              
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> "H000670") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                End If
                BindBusinessUnits()
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

   

    Private Sub BindBusinessUnits()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "BSU")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_MPR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlBSU_Add.DataSource = ds
            ddlBSU_Add.DataTextField = "DESCR"
            ddlBSU_Add.DataValueField = "ID"
            ddlBSU_Add.DataBind()
        End If


        If Not ddlBSU_Add.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_Add.ClearSelection()
            ddlBSU_Add.Items.FindByValue(Session("sBsuid")).Selected = True

        End If

       
    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
            Dim str_Sql As String = String.Empty
            Dim BSU_ID As String = ddlBSU_Add.SelectedValue
            Dim STR_FILTER_CATEGORY_DES As String = String.Empty
            Dim STR_FILTER_MPR_TITLE As String = String.Empty
            Dim STR_FILTER_MPR_STARTDT As String = String.Empty
            Dim str_filter_MPR_BUDGETED As String = String.Empty
            str_Sql = "select * from( select ROW_NUMBER() OVER (order by CATEGORY_DES,MPR_STARTDT DESC) AS R1," & _
"  MPR_ID,MPR_TITLE,CATEGORY_DES,REPLACE(convert(varchar(12),MPR_STARTDT,106),' ','/') as MPR_STARTDT," & _
" case when isnull(MPR_BUDGETED,'')='Yes' then 'Budgeted' else 'Unbudgeted' end as MPR_BUDGETED,MPR_NO_VAC,CASE WHEN ISNULL(MPR_PROG_STATUS,'')='' THEN 'NEW ENTRY' else MPR_PROG_STATUS end as MPR_PROG_STATUS ," & _
" case when ISNULL(MPR_PROG_STATUS,'') ='REJECTED'  then 1 else 0 end as flag,CASE WHEN ISNULL(MPR_PROG_STATUS,'')='' THEN 0 ELSE 1 END AS bPRG_SHOW,HR.GETREQ_PER(MPR_PROG_STATUS,MPR_BSU_ID,MPR_CAT_ID,MPR_ID) AS PER_VALUE FROM HR.MANPOWER_REQ_M WITH(NOLOCK) INNER JOIN DBO.CATEGORY WITH(NOLOCK) " & _
" ON CATEGORY_ID=MPR_CAT_ID  WHERE MPR_BSU_ID='" & BSU_ID & "')A WHERE MPR_ID<>'' "


            Dim ds As New DataSet
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox

            Dim STR_TXTCATEGORY_DES As String = String.Empty
            Dim STR_TXTMPR_STARTDT As String = String.Empty
            Dim STR_TXTMPR_TITLE As String = String.Empty
            Dim str_txtMPR_BUDGETED As String = String.Empty

            If gvManageUsers.Rows.Count > 0 Then

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtCATEGORY_DES")
                STR_TXTCATEGORY_DES = txtSearch.Text
                STR_FILTER_CATEGORY_DES = " AND CATEGORY_DES LIKE '%" & STR_TXTCATEGORY_DES & "%'"

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtMPR_STARTDT")
                STR_TXTMPR_STARTDT = txtSearch.Text
                STR_FILTER_MPR_STARTDT = " AND MPR_STARTDT LIKE '%" & STR_TXTMPR_STARTDT & "%'"

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtMPR_TITLE")
                STR_TXTMPR_TITLE = txtSearch.Text
                STR_FILTER_MPR_TITLE = " AND MPR_TITLE LIKE '%" & STR_TXTMPR_TITLE & "%'"

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtMPR_BUDGETED")
                str_txtMPR_BUDGETED = txtSearch.Text
                str_filter_MPR_BUDGETED = " AND MPR_BUDGETED LIKE '%" & str_txtMPR_BUDGETED & "%'"

            End If

           
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & STR_FILTER_CATEGORY_DES & STR_FILTER_MPR_STARTDT & STR_FILTER_MPR_TITLE & str_filter_MPR_BUDGETED & "  ORDER BY CATEGORY_DES,MPR_STARTDT DESC ")
            gvManageUsers.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)(8) = True
                ds.Tables(0).Rows(0)(9) = False
                ds.Tables(0).Rows(0)(10) = False
                gvManageUsers.DataBind()
                Dim columnCount As Integer = gvManageUsers.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.



                gvManageUsers.Rows(0).Cells.Clear()
                gvManageUsers.Rows(0).Cells.Add(New TableCell)
                gvManageUsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUsers.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUsers.DataBind()
            End If
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtCATEGORY_DES")
            txtSearch.Text = STR_TXTCATEGORY_DES
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtMPR_STARTDT")
            txtSearch.Text = STR_TXTMPR_STARTDT
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtMPR_TITLE")
            txtSearch.Text = STR_TXTMPR_TITLE
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtMPR_BUDGETED")
            txtSearch.Text = str_txtMPR_BUDGETED
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

   
    Protected Sub gvManageUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUsers.PageIndexChanging
        gvManageUsers.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ddlBSU_Add_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU_Add.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub btnSearchCategory_Des_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchMPR_TITLE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

  
    Protected Sub btnSearchMPR_BUDGETED_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchMPR_STARTDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbtnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblMPR_ID As New Label
            Dim url As String
            Dim viewid As String
            lblMPR_ID = TryCast(sender.FindControl("lblMPR_ID"), Label)
            viewid = lblMPR_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\OASIS_HR\Requisition\HRManpower_form.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\OASIS_HR\Requisition\HRManpower_form.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

   

    Sub bind_reopen(ByVal MPR_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim str_query As String = String.Empty
        str_query = "SELECT BSU_NAME, CATEGORY_DES,  MPR_TITLE  FROM HR.MANPOWER_REQ_M WITH(NOLOCK) INNER JOIN " & _
" DBO.CATEGORY WITH(NOLOCK) ON CATEGORY_ID=MPR_CAT_ID " & _
" INNER JOIN OASIS.DBO.BUSINESSUNIT_M WITH(NOLOCK) ON BSU_ID=MPR_BSU_ID WHERE MPR_ID='" & MPR_ID & "'"

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    ltBSU.Text = "<font color='black'>" & Convert.ToString(DATAREADER("BSU_NAME")) & "</font>"
                    ltCategory.Text = "<font color='black'>" & Convert.ToString(DATAREADER("CATEGORY_DES")) & "</font>"
                    ltTitle.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_TITLE")) & "</font>"
                    txtRemark.Text = ""
                End While
            End If

        End Using


    End Sub
    Protected Sub lbReopen_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblMPR_ID As Label = DirectCast(sender.findcontrol("lblMPR_ID"), Label)
        hfMPR_ID.Value = lblMPR_ID.Text
        bind_reopen(lblMPR_ID.Text)
        txtRemark.Text = ""
        Me.mdlReopen.Show()
    End Sub

    Protected Sub btnSaveReopen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReopen.Click
        If txtRemark.Text.Replace(",", "") <> "" Then
            Dim errorMessage As String = String.Empty

            Dim str_err As String = saveReq_Reopen(errorMessage)

            If str_err = "0" Then
                ltReopen_Error.Text = " <font color='maroon'>  Record saved successfully</font>"
                Me.mdlReopen.Show()
                btnSaveReopen.Visible = False
            Else
                ltReopen_Error.Text = "<font color='maroon'>Record could not be Updated</font>"
                Me.mdlReopen.Show()
                btnSaveReopen.Visible = False
            End If
        Else
            txtRemark.Text = txtRemark.Text.Replace(",", "")
            ltReopen_Error.Text = "<font color='maroon'>Remarks required !!!</font>"
            Me.mdlReopen.Show()
        End If


    End Sub

    Function saveReq_Reopen(ByVal errorMessage As String) As String

        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty

        Using conn As SqlConnection = ConnectionManger.GetOASIS_HRConnection

            Try

                transaction = conn.BeginTransaction("SampleTransaction")
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MPR_ID", hfMPR_ID.Value)
                pParms(1) = New SqlClient.SqlParameter("@MRO_USR_ID", Session("sUsr_id"))
                pParms(2) = New SqlClient.SqlParameter("@MRO_REMARKS", txtRemark.Text.Replace(",", ""))
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "HR.SAVEMANPOWER_REQ_REOPEN", pParms)
                Status = pParms(3).Value

                If Status <> 0 Then
                    saveReq_Reopen = "1"
                    Return "1"
                End If


                saveReq_Reopen = "0"




            Catch ex As Exception
                saveReq_Reopen = "1"
                errorMessage = "Error Occured While Saving."
                UtilityObj.Errorlog(ex.Message)
            Finally
                If saveReq_Reopen <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
End Class
