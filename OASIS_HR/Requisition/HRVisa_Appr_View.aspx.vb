Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class HRVisa_Appr_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> "H675005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                End If
                BindBusinessUnits()
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub



    Private Sub BindBusinessUnits()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "BSU")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_VR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlBSU_Add.DataSource = ds
            ddlBSU_Add.DataTextField = "DESCR"
            ddlBSU_Add.DataValueField = "ID"
            ddlBSU_Add.DataBind()
        End If


        If Not ddlBSU_Add.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_Add.ClearSelection()
            ddlBSU_Add.Items.FindByValue(Session("sBsuid")).Selected = True

        End If


    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
            Dim str_Sql As String = String.Empty
            Dim BSU_ID As String = ddlBSU_Add.SelectedValue
            Dim STR_FILTER_CATEGORY_DES As String = String.Empty
            Dim STR_FILTER_VR_TITLE As String = String.Empty
            Dim STR_FILTER_VR_DOC_DT As String = String.Empty
            Dim str_filter_BUDGETED As String = String.Empty
            str_Sql = " SELECT * FROM( SELECT ROW_NUMBER() OVER ( order by CATEGORY_DES,VR_DOC_DT DESC) AS R1," & _
" VR_DOC_ID,VR_ID ,VR_TITLE,CATEGORY_DES,REPLACE(convert(varchar(12),VR_DOC_DT,106),' ','/') as VR_DOC_DT," & _
"CASE WHEN VR_bBUDGETTED=1 THEN  'Budgeted' else 'Unbudgeted' end  as BUDGETTED,VR_bBUDGETTED,VR_BUDGETTED_DESCR,VR_APP_FIRST_NAME+' '+isnull(VR_APP_MID_NAME,'')+' '+ " & _
" isnull(VR_APP_LAST_NAME,'') as APPl_NAME," & _
" CASE WHEN ISNULL( VR_PROG_STATUS,'')='' THEN 'NEW ENTRY' else  VR_PROG_STATUS end as  VR_PROG_STATUS , " & _
 " case when ISNULL( VR_PROG_STATUS,'') ='REJECTED'  then 1 else 0 end as flag," & _
 " CASE WHEN ISNULL( VR_PROG_STATUS,'')='' THEN 0 ELSE 1 END AS bPRG_SHOW," & _
 " [VISA].[GETVISAREQ_PER]( VR_PROG_STATUS,VR_BSU_ID,VR_CAT_ID,VR_ID,VR_DOC_ID) AS PER_VALUE " & _
  " FROM VISA.VISA_REQ_M WITH(NOLOCK) INNER JOIN DBO.CATEGORY WITH(NOLOCK) " & _
 " ON CATEGORY_ID=VR_CAT_ID WHERE VR_BSU_ID='" & ddlBSU_Add.SelectedValue & "')A  where VR_ID<>''"

            Dim ds As New DataSet
            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox

            Dim STR_TXTCATEGORY_DES As String = String.Empty
            Dim STR_TXTVR_DOC_DT As String = String.Empty
            Dim STR_TXTVR_TITLE As String = String.Empty
            Dim str_txtBUDGETED As String = String.Empty

            If gvManageUsers.Rows.Count > 0 Then

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtCATEGORY_DES")
                STR_TXTCATEGORY_DES = txtSearch.Text
                STR_FILTER_CATEGORY_DES = " AND CATEGORY_DES LIKE '%" & STR_TXTCATEGORY_DES & "%'"

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtVR_DOC_DT")
                STR_TXTVR_DOC_DT = txtSearch.Text
                STR_FILTER_VR_DOC_DT = " AND VR_DOC_DT LIKE '%" & STR_TXTVR_DOC_DT & "%'"

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtVR_TITLE")
                STR_TXTVR_TITLE = txtSearch.Text
                STR_FILTER_VR_TITLE = " AND VR_TITLE LIKE '%" & STR_TXTVR_TITLE & "%'"

                txtSearch = gvManageUsers.HeaderRow.FindControl("txtBUDGETTED")
                str_txtBUDGETED = txtSearch.Text
                str_filter_BUDGETED = " AND BUDGETTED LIKE '%" & str_txtBUDGETED & "%'"

            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & STR_FILTER_CATEGORY_DES & STR_FILTER_VR_DOC_DT & STR_FILTER_VR_TITLE & str_filter_BUDGETED & "  ORDER BY CATEGORY_DES,VR_DOC_DT DESC ")
            gvManageUsers.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)(11) = False
                ds.Tables(0).Rows(0)(12) = False
                ds.Tables(0).Rows(0)(13) = False
                gvManageUsers.DataBind()
                Dim columnCount As Integer = gvManageUsers.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.



                gvManageUsers.Rows(0).Cells.Clear()
                gvManageUsers.Rows(0).Cells.Add(New TableCell)
                gvManageUsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUsers.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUsers.DataBind()
            End If
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtCATEGORY_DES")
            txtSearch.Text = STR_TXTCATEGORY_DES
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtVR_DOC_DT")
            txtSearch.Text = STR_TXTVR_DOC_DT
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtVR_TITLE")
            txtSearch.Text = STR_TXTVR_TITLE
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtBUDGETTED")
            txtSearch.Text = str_txtBUDGETED
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvManageUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUsers.PageIndexChanging
        gvManageUsers.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ddlBSU_Add_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSU_Add.SelectedIndexChanged
        gridbind()
    End Sub

    Protected Sub btnSearchCategory_Des_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchVR_TITLE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchVR_DOC_DT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchBUDGETED_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub lbtnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblVR_ID As New Label
            Dim url As String
            Dim viewid As String
            lblVR_ID = TryCast(sender.FindControl("lblVR_ID"), Label)
            viewid = lblVR_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "edit"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\OASIS_HR\Requisition\HRVisa_Appr.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Sub bind_reopen(ByVal VR_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim str_query As String = String.Empty
        str_query = "SELECT BSU_NAME, CATEGORY_DES,  VR_TITLE  FROM VISA.VISA_REQ_M WITH(NOLOCK) INNER JOIN " & _
" DBO.CATEGORY WITH(NOLOCK) ON CATEGORY_ID=VR_CAT_ID " & _
" INNER JOIN OASIS.DBO.BUSINESSUNIT_M WITH(NOLOCK) ON BSU_ID=VR_BSU_ID WHERE VRR_ID='" & VR_ID & "'"

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    ltBSU.Text = "<font color='black'>" & Convert.ToString(DATAREADER("BSU_NAME")) & "</font>"
                    ltCategory.Text = "<font color='black'>" & Convert.ToString(DATAREADER("CATEGORY_DES")) & "</font>"
                    ltTitle.Text = "<font color='black'>" & Convert.ToString(DATAREADER("VR_TITLE")) & "</font>"
                    txtRemark.Text = ""
                End While
            End If

        End Using


    End Sub
    Protected Sub lbReopen_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblVR_ID As Label = DirectCast(sender.findcontrol("lblVR_ID"), Label)
        Dim lblVR_DOC_ID As Label = DirectCast(sender.findcontrol("lblVR_DOC_ID"), Label)
        hfVR_ID.Value = lblVR_ID.Text
        hfDOC_ID.Value = lblVR_DOC_ID.Text
        bind_reopen(lblVR_ID.Text)
        txtRemark.Text = ""
        Me.mdlReopen.Show()
    End Sub

    Protected Sub btnSaveReopen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReopen.Click
        If txtRemark.Text.Replace(",", "") <> "" Then
            Dim errorMessage As String = String.Empty

            Dim str_err As String = saveReq_Reopen(errorMessage)

            If str_err = "0" Then
                ltReopen_Error.Text = " <font color='maroon'>  Record saved successfully</font>"
                Me.mdlReopen.Show()
                btnSaveReopen.Visible = False
            Else
                ltReopen_Error.Text = "<font color='maroon'>Record could not be Updated</font>"
                Me.mdlReopen.Show()
                btnSaveReopen.Visible = False
            End If
        Else
            txtRemark.Text = txtRemark.Text.Replace(",", "")
            ltReopen_Error.Text = "<font color='maroon'>Remarks required !!!</font>"
            Me.mdlReopen.Show()
        End If


    End Sub

    Function saveReq_Reopen(ByVal errorMessage As String) As String

        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty

        Using conn As SqlConnection = ConnectionManger.GetOASIS_HRConnection

            Try

                transaction = conn.BeginTransaction("SampleTransaction")
                Dim pParms(65) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RRO_REF_ID", hfVR_ID.Value)
                pParms(1) = New SqlClient.SqlParameter("@RRO_USR_ID", Session("sUsr_id"))
                pParms(2) = New SqlClient.SqlParameter("@RRO_REMARKS", txtRemark.Text.Replace(",", ""))
                pParms(3) = New SqlClient.SqlParameter("@RRO_APM_DOC_TYPE", hfDOC_ID.Value)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[VISA].[SAVEREQUISITION_REOPEN]", pParms)
                Status = pParms(4).Value

                If Status <> 0 Then
                    saveReq_Reopen = "1"
                    Return "1"
                End If


                saveReq_Reopen = "0"




            Catch ex As Exception
                saveReq_Reopen = "1"
                errorMessage = "Error Occured While Saving."
                UtilityObj.Errorlog(ex.Message)
            Finally
                If saveReq_Reopen <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

End Class
