﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class OASIS_HR_Requisition_HRModal_ProcessAppr
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("MPR_ID") = Request.QueryString("MPR_ID")
            'get the menucode to confirm the user is accessing the valid page
            ViewState("LEV_ID") = Request.QueryString("LEV")

            ViewState("USR_ID") = Request.QueryString("UID")
            bind_Appr_info()
        End If

    End Sub
    Private Sub bind_Appr_info()
        Dim STR_CONN As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim PARAM(5) As SqlClient.SqlParameter
        PARAM(0) = New SqlParameter("@MPR_ID", ViewState("MPR_ID"))
        PARAM(1) = New SqlParameter("@USR_ID", ViewState("USR_ID"))
        PARAM(2) = New SqlParameter("@LEV_ID", ViewState("LEV_ID"))

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.StoredProcedure, "HR.GETAPPR_INFO", PARAM)

            If DATAREADER.HasRows = True Then

                While DATAREADER.Read

                    ltCat.Text = "<font color='black'>" & Convert.ToString(DATAREADER("CATEGORY_DES")) & "</font>"
                    ltTitle.Text = "<font color='black'>" & Convert.ToString(DATAREADER("MPR_TITLE")) & "</font>"
                    ltAppr.Text = "<font color='black'>" & Convert.ToString(DATAREADER("LEVEL_DESCR")) & "</font>"
                    ltProc_by.Text = "<font color='black'>" & Convert.ToString(DATAREADER("USR_NAME")) & "</font>"
                End While
            End If
        End Using


    End Sub

    Protected Sub btnAppr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppr.Click
        If txtRemarks.Text.Replace(",", "") = "" Then
            divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Remarks cannot be left empty</div>"
            txtRemarks.Text = txtRemarks.Text.Replace(",", "")
        Else
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty


            str_err = calltransaction("APPROVED", errorMessage)
            If str_err = "0" Then

                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Record Updated  Successfully</div>"

                btnRej.Enabled = False
                btnAppr.Enabled = False

                txtRemarks.Enabled = False

            Else
                txtRemarks.Text = ""
                txtRemarks.Text = txtRemarks.Text.Replace(",", "")

                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>" & errorMessage & "</div>"
            End If
        End If
    End Sub

    Protected Sub btnRej_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRej.Click

        If txtRemarks.Text.Replace(",", "") = "" Then
            divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Remarks cannot be left empty</div>"
            txtRemarks.Text = txtRemarks.Text.Replace(",", "")
        Else
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty


            str_err = calltransaction("REJECTED", errorMessage)
            If str_err = "0" Then

                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Record Updated  Successfully</div>"

                btnRej.enabled = False
                btnAppr.enabled = False
             
                txtRemarks.enabled = False
             
            Else
                txtRemarks.Text = ""
                txtRemarks.Text = txtRemarks.Text.Replace(",", "")
               
                divError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>" & errorMessage & "</div>"
            End If
        End If


    End Sub
    Function calltransaction(ByVal flag As String, ByRef errorMessage As String) As Integer
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_HRConnection()
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MPR_ID", ViewState("MPR_ID"))
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", ViewState("USR_ID"))
                pParms(2) = New SqlClient.SqlParameter("@LEV_ID", ViewState("LEV_ID"))
                pParms(3) = New SqlClient.SqlParameter("@STATUS", flag) 'flag--approved or rejected
                pParms(4) = New SqlClient.SqlParameter("@REMARKS", txtRemarks.Text)
                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[HR].[SaveMANPOWER_REQ_APPR]", pParms)
                status = pParms(5).Value


                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If


                calltransaction = "0"
            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
End Class
