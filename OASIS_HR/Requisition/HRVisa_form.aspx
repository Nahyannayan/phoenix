<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="HRVisa_form.aspx.vb" Inherits="Requisition_HRVisa_form" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
 
 
 
 </script>


    
    <table  align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error"
                    EnableViewState="False" ForeColor="" ValidationGroup="groupM1" /></td>
        </tr> <tr><td class="matters" style="height: 13px" colspan="6" align="center">Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory      
        </td></tr>  
               <tr>
               
            <td class="matters" align="center">
               
                <table  BorderColor="#1b80b6" border="1"
                    CellPadding="4" CellSpacing="0" class="BlueTableView"   style="width: 750px">
                    
                    <tr Class="subheader_img">
                        <td   colspan="6"  >
                                 <span style="clear: left;display: inline; float: left; visibility: visible;">
                                                Visa Requisation Form</span>
                                                  <span style="clear: right;display: inline; float: right; visibility: visible">
                                                    <asp:LinkButton ID="lbHistory" runat="server" Font-Size="12px" Font-Bold="True" Font-Italic="True">View History</asp:LinkButton>
                                                   </span>
                        
                        </td>
                    </tr>
                    
                    <tr class="matters">
                        <td  align="left">School<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td >:</td>
                        <td align="left" colspan="4">  
                            <asp:DropDownList ID="ddlBSU_ID" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td  align="left">Category<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:DropDownList ID="ddlCAT_ID" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator id="rfvCAT_ID" 
                                    runat="server" 
                                    ErrorMessage="Please make category  selection"
                                    ControlToValidate="ddlCAT_ID" 
                                    InitialValue="0" CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                        </td>
                        <td align="left" >Document Date<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left">
                       <asp:TextBox ID="txtDoc_dt" runat="server" Width="110px"></asp:TextBox>
<ajaxToolkit:CalendarExtender ID="SchFrom_dt_CalendarExtender" runat="server" 
                                                                                                    
                                Format="dd/MMM/yyyy" PopupButtonID="imgStart_dt" 
                                                                                                    
                                TargetControlID="txtDoc_dt"></ajaxToolkit:CalendarExtender>
                                                                                                &nbsp;<asp:ImageButton 
                                ID="imgStart_dt" runat="server" 
                                                                                                    
                                ImageUrl="~/Images/calendar.gif" />
                                <asp:RequiredFieldValidator id="RequiredFieldValidator1" 
                                    runat="server" 
                                    ErrorMessage="Start date required"
                                    ControlToValidate="txtDoc_dt"
                                     CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                                                                <br />
                                                                                                <span style="font-size: 7pt" >(dd/mmm/yyyy)</span>
                                                                                            </td>
                    </tr>
                    <tr  class="matters">
                        <td  align="left">Visa Type<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td  align="left" colspan="4"> 
                            <asp:DropDownList ID="ddlVisa_type" runat="server">
                            </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                              <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server"
    TargetControlID="lbtnVisaInfo"
    PopupControlID="visaId"
    Position="left" OffsetX="-10"  />
<asp:LinkButton ID="lbtnVisaInfo" runat="server" Text="View Details">
</asp:LinkButton><div id="visaId" runat="server" style="Height:100px;width:400px; background-color:White;border:solid 1px #CEE3FF;"><asp:Literal ID="ltVisaInfo" runat="server"></asp:Literal></div>
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td align="left">Full name of applicant as per passport<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                             <asp:TextBox ID="txtFname" runat="server" MaxLength="100" Width="143px"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfvFname_Stud" runat="server" ControlToValidate="txtFname"
                                                                                                    Display="Dynamic" ErrorMessage="First name required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                                                                <asp:TextBox ID="txtMname" runat="server" MaxLength="100" Width="143px" ValidationGroup="App_Info"></asp:TextBox>&nbsp;
                                                                                                <asp:TextBox ID="txtLname" runat="server" MaxLength="100" Width="142px"></asp:TextBox>
                                                                                                <br />
                                                                                                  &nbsp; &nbsp; &nbsp;First Name  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Last Name
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td align="left">Purpose of visit</td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                            <asp:TextBox ID="txtPurpose" runat="server" Width="445px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td align="left">Position applied for<br />
                            (Prospective staff)<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                            <asp:TextBox ID="txtPos_Title" runat="server" Width="445px"></asp:TextBox>
                            <asp:RequiredFieldValidator id="RequiredFieldValidator2" 
                                    runat="server" 
                                    ErrorMessage="Position applied for description required"
                                    ControlToValidate="txtPos_Title"
                                     CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Nationality<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td  align="left" colspan="4"> 
                            <asp:DropDownList ID="ddlNat" runat="server">
                            </asp:DropDownList>
                             <asp:RequiredFieldValidator id="RequiredFieldValidator3" 
                                    runat="server" 
                                    ErrorMessage="Please select nationality"
                                    ControlToValidate="ddlNat" 
                                    InitialValue="0" CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Passport number</td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:TextBox ID="txtPassportNo" runat="server" Width="220px"></asp:TextBox>
                        </td>
                        <td align="left" >Date of birth<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left">
                                 <asp:TextBox ID="txtDob" runat="server" Width="110px"></asp:TextBox>
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                                                                    
                                Format="dd/MMM/yyyy" PopupButtonID="imgDOB" 
                                                                                                    
                                TargetControlID="txtDob"></ajaxToolkit:CalendarExtender>
                    &nbsp;<asp:ImageButton 
                                ID="imgDOB" runat="server" 
                                                                                                    
                                ImageUrl="~/Images/calendar.gif" /> <asp:RequiredFieldValidator id="RequiredFieldValidator4" 
                                    runat="server" 
                                    ErrorMessage="Date of birth required"
                                    ControlToValidate="txtDob"
                                     CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                <br />
                                                                                                <span style="font-size: 7pt" >(dd/mmm/yyyy)</span></td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Male/Female</td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:DropDownList ID="ddlGender" runat="server">
                                <asp:ListItem>Female</asp:ListItem>
                                <asp:ListItem>Male</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" >Marital status</td>
                        <td>:</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlStatus" runat="server">
                                <asp:ListItem>Single</asp:ListItem>
                                <asp:ListItem>Married</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Father&#39;s name</td>
                        <td>:</td>
                        <td  align="left" colspan="4"> 
                             <asp:TextBox ID="txtFather_First_name" runat="server" MaxLength="100" Width="143px"></asp:TextBox>
                                                                                               
                                                                                                <asp:TextBox ID="txtFather_Mid_name" runat="server" MaxLength="100" Width="143px" ValidationGroup="App_Info"></asp:TextBox>&nbsp;
                                                                                                <asp:TextBox ID="txtFather_Last_name" runat="server" MaxLength="100" Width="142px"></asp:TextBox>
                                                                                                <br />
                                                                                                &nbsp; &nbsp; &nbsp;First Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Last Name
                        </td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Mother&#39;s name</td>
                        <td>:</td>
                        <td  align="left" colspan="4"> 
                            <asp:TextBox ID="txtMother_First_name" runat="server" MaxLength="100" Width="143px"></asp:TextBox>
                                                                                                                                          <asp:TextBox ID="txtMother_Mid_name" runat="server" MaxLength="100" Width="143px" ValidationGroup="App_Info"></asp:TextBox>&nbsp;
                                                                                                <asp:TextBox ID="txtMother_Last_name" runat="server" MaxLength="100" Width="142px"></asp:TextBox>
                                                                                                <br />
                                                                                                &nbsp; &nbsp; &nbsp;First Name  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Last Name
                        </td>
                    </tr>
                 
                    <tr class="matters">
                        <td align="left">Qualifications</td>
                        <td>:</td>
                        <td  align="left" > 
                             <asp:ListBox ID="lstQlf" runat="server" SelectionMode="Multiple" 
                                Width="220px" Height="50px" >
                       </asp:ListBox>
                        </td>
                        <td  align="left">Address in home country</td>
                        <td>:</td>
                        <td  align="left" > 
                            <asp:TextBox ID="txtAddress" runat="server" 
                           Rows="4" SkinID="MultiText" TextMode="MultiLine" Height="70px" 
                           Width="220px"></asp:TextBox></td>
                        
                    </tr>
                    <tr class="matters">
                         <td align="left" colspan="6">
                            <div style="padding:2px;">Processing fee to be charged to</div>
                            <asp:TextBox ID="txtProc_fee" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox></td>
                    </tr>
                   
                    <tr class="matters">
                        <td align="left" colspan="6">
                            <div style="padding:2px;">Is position budgetted and approved &nbsp; <asp:CheckBox id="chkbBug" runat="server" Text="Yes" /> </div><div>&nbsp;If not , explain : </div>
                            <asp:TextBox ID="txtPos_budget" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox></td>
                    </tr>
                   
                    <tr class="matters">
                        <td align="left" colspan="6">
                             <div style="padding:2px;">Further Comments</div>
                            <asp:TextBox ID="txtComments" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr class="matters">
            <td class="matters" style="height: 17px; width: 793px;" valign="bottom">
            </td>
        </tr>
        <tr class="matters">
            <td class="matters" valign="bottom" 
                align="center">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" 
                    CssClass="button" Text="Add" Width="90px" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" 
                    CssClass="button" Text="Edit" Width="90px" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                    ValidationGroup="groupM1" Width="90px" /><asp:Button ID="btnCancel" 
                    runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" Width="90px" />
                </td>
        </tr>
      
    </table>
    <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
    TargetControlID="lbHistory"
    PopupControlID="plHistory"
    Position="left" OffsetX="-510" OffsetY="16" />
      <asp:Panel id="plHistory" runat="server" Height="100px" width="600px">
                                   <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                                        DataKeyNames="r1" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                                        <RowStyle CssClass="griditem" Height="25px" />
                                        <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                    
                                                    
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Level">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLEVEL_DESCR" runat="server" Text='<%# Bind("LEVEL_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Authorized By ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUSR_DISPLAY_NAME" runat="server" 
                                                        Text='<%# Bind("USR_DISPLAY_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMPH_DATE" runat="server" Text='<%# BIND("MPH_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# BIND("MPH_STATUS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# BIND("MPH_REMARKS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle BackColor="Khaki" />
                                        <HeaderStyle CssClass="subheader_img" Height="25px" />
                                        <FooterStyle BackColor="#E4F0F7" Height="25px" Font-Names="Verdana" 
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                    </asp:Panel>
          


</asp:Content>

