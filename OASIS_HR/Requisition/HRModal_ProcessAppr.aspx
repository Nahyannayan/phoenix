﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HRModal_ProcessAppr.aspx.vb" Inherits="OASIS_HR_Requisition_HRModal_ProcessAppr" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
<title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .style1
            {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 10px;
                font-weight: bold;
                color: #1B80B6;
                width: 115px;
            }
            .style2
          
         {
             font-family: Verdana, Arial, Helvetica, sans-serif;
             font-size: 11px;
             width: 610px;
             height: 26px;
             text-align: left;
             vertical-align: middle;
             background-image: url(../../Images/bgblue.gif);
             background-repeat: repeat-x;
             margin-right:-1px;
         }
          .style3
         {
             font-family: Verdana, Arial, Helvetica, sans-serif;
             vertical-align: middle;
             text-align: left;
             padding-top: 5px;
             padding-left: 6px;
             color: #FFFFFF;
             font-size: 12px;
             font-weight: bold;
         }
        </style>
</head>
<body  >
    <form id="form1" runat="server">
      <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
                    </ajaxToolkit:ToolkitScriptManager>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
    
    <div style="text-align:center;margin-top:-1px;">
    <table style="margin-top:-1px;">
    <tr>
    <td align="center">
       
               <div  style="border-color: #1b80b6; border-style:solid;border-width:1px;padding:0px;text-align:left;">
         <div class="style2" >
          <div class="style3" align="left">
              Process Approval</div>
           </div>
                           
                                                                <table align="center"   border="1" bordercolor="#1b80b6"  cellpadding="6" cellspacing="0"
                            style="border-collapse: collapse;width: 100%;">
                            <tr>
    <td align="left" class="style1" >
     Category</td>
    <td class="matters" style="width: 1px;">:
    </td>
    <td align="left" class="matters">
      
        <asp:Literal ID="ltCat" runat="server"></asp:Literal>
                                </td>
    </tr>
    <tr >
    <td align="left" class="style1" >
        Title/Position</td>
    <td class="matters" style="width: 1px;">:
    </td>
    <td align="left" class="matters">
        <asp:Literal ID="ltTitle" runat="server"></asp:Literal>
        </td>
    </tr>
     <tr >
    <td align="left" class="style1">
        Approval Level</td>
    <td class="matters" style="width: 1px;">:
    </td>
    <td align="left" class="matters">
      
        <asp:Literal ID="ltAppr" runat="server"></asp:Literal>
         </td>
    </tr>
                                                                    <tr>
                                                                        <td align="left" class="style1">
                                                                            To be processed by</td>
                                                                        <td class="matters" style="width: 1px;">
                                                                            :</td>
                                                                        <td align="left" class="matters">
                                                                            <asp:Literal ID="ltProc_by" runat="server"></asp:Literal>
                                                                        </td>
                                                                    </tr>
                                                                    <tr >
                                                                        <td align="left" class="style1" >
                                                                            Remarks</td>
                                                                        <td class="matters" style="width: 1px;">
                                                                            :</td>
                                                                        <td align="left" class="matters">
                                                                            <asp:TextBox ID="txtRemarks" runat="server" Height="16px" SkinID="MultiText" 
                                                                                TextMode="MultiLine" Width="352px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                                ControlToValidate="txtRemarks" CssClass="error" Display="Dynamic" 
                                                                                ErrorMessage="Required" ForeColor="" SetFocusOnError="True" 
                                                                                ValidationGroup="REQ"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
    </table>
    
    

                             
                                
                                </div> 
                 </td>
               </tr><tr><td align="center">
               <div id="divError" runat="server" align="center"></div>
               <br />
                          
                               <asp:Button ID="btnAppr" runat="server" Text="Approve" CssClass="button" 
                                        Height="28px" Width="90px" ValidationGroup="REQ" /><asp:Button ID="btnRej" runat="server" CssClass="button" Text="Reject" 
                   ValidationGroup="REQ" Width="90px" Height="28px" />
               <asp:Button ID="btnCancelAuth_add" runat="server" Text="Close" CssClass="button" 
                                        Height="28px" Width="90px" 
                   OnClientClick="javascript:window.close();return false;"/>
     
    
    </td>
    </tr>
    </table>
    <asp:HiddenField ID="h_BSU_ID" runat="server" Value="0" /> 
    </div>
                                
                    
     </ContentTemplate>
                        </asp:UpdatePanel>
    </form>
</body>
</html>
