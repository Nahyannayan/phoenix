<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="HRManpower_form.aspx.vb" Inherits="Requisition_HRManpower_form" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
 
 
 
 </script>


    
    <table  align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error"
                    EnableViewState="False" ForeColor="" ValidationGroup="groupM1" /></td>
        </tr> <tr><td class="matters" style="height: 13px" colspan="6" align="center">Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory      
        </td></tr>  
               <tr>
               
            <td class="matters" align="center">
               
                <table  BorderColor="#1b80b6" border="1"
                    CellPadding="4" CellSpacing="0" class="BlueTableView"   style="width: 750px">
                    
                    <tr Class="subheader_img">
                        <td   colspan="6"  >
                                 <span style="clear: left;display: inline; float: left; visibility: visible;">
                                                Manpower Requisation Form</span>
                                                  <span style="clear: right;display: inline; float: right; visibility: visible">
                                                    <asp:LinkButton ID="lbHistory" runat="server" Font-Size="12px" Font-Bold="True" Font-Italic="True">View History</asp:LinkButton>
                                                   </span></span>
                        
                        </td>
                    </tr>
                    
                    <tr class="matters">
                        <td  align="left">School<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td >:</td>
                        <td align="left" colspan="4">  
                            <asp:DropDownList ID="ddlBSU_ID" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td  align="left">Category<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:DropDownList ID="ddlCAT_ID" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator id="rfvCAT_ID" 
                                    runat="server" 
                                    ErrorMessage="Please make category  selection"
                                    ControlToValidate="ddlCAT_ID" 
                                    InitialValue="0" CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                        </td>
                        <td align="left" >Start Date<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left">
                                                                                                <asp:TextBox ID="txtStart_dt" runat="server" Width="110px"></asp:TextBox>
                                                                                                <ajaxToolkit:CalendarExtender ID="SchFrom_dt_CalendarExtender" runat="server" 
                                                                                                    
                                Format="dd/MMM/yyyy" PopupButtonID="imgStart_dt" 
                                                                                                    
                                TargetControlID="txtStart_dt"></ajaxToolkit:CalendarExtender>
                                                                                                &nbsp;<asp:ImageButton 
                                ID="imgStart_dt" runat="server" 
                                                                                                    
                                ImageUrl="~/Images/calendar.gif" />
                                <asp:RequiredFieldValidator id="RequiredFieldValidator1" 
                                    runat="server" 
                                    ErrorMessage="Start date required"
                                    ControlToValidate="txtStart_dt"
                                     CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                                                                <br />
                                                                                                <span style="font-size: 7pt" >(dd/mmm/yyyy)</span>
                                                                                            </td>
                    </tr>
                    <tr  class="matters">
                        <td align="left">Title/Position<span style="font-size: 8pt; color: #800000">*</span></td>
                        <td>:</td>
                        <td align="left" colspan="4"> 
                            <asp:TextBox ID="txtTitle" runat="server" Width="575px"></asp:TextBox>
                            <asp:RequiredFieldValidator id="RequiredFieldValidator2" 
                                    runat="server" 
                                    ErrorMessage="Title/Position description required"
                                    ControlToValidate="txtTitle"
                                     CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Male/Female</td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:DropDownList ID="ddlGender" runat="server">
                                <asp:ListItem>Any</asp:ListItem>
                                <asp:ListItem>Female</asp:ListItem>
                                <asp:ListItem>Male</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" >Status(single/married)</td>
                        <td>:</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlStatus" runat="server">
                                <asp:ListItem>Any</asp:ListItem>
                                <asp:ListItem>Single</asp:ListItem>
                                <asp:ListItem>Married</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td align="left">No. of vacancies</td>
                        <td>:</td>
                        <td  align="left"> 
                            <asp:TextBox ID="txtVac_No" runat="server" Width="76px"></asp:TextBox>
                        </td>
                        <td align="left" >Vacancy result of</td>
                        <td>:</td>
                        <td align="left">
                            <asp:TextBox ID="txtVac_Result" runat="server" CssClass="inputbox_multi" 
                                Height="35px" SkinID="MultiText" TextMode="MultiLine" Width="230px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters" >
                        <td  align="left">Permanent/temporary</td>
                        <td>:</td>
                        <td align="left"> 
                            <asp:DropDownList ID="ddlPer_Tem" runat="server">
                                <asp:ListItem Value="PERM">Permanent</asp:ListItem>
                                <asp:ListItem Value="TEMP">Temporary</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" >if temp,then duration</td>
                        <td>:</td>
                        <td align="left">
                            <asp:TextBox ID="txtTemp_dur" runat="server" Width="230px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td align="left">Local/overseas contract</td>
                        <td>:</td>
                        <td align="left"> 
                            <asp:DropDownList ID="ddlLocal" runat="server">
                                <asp:ListItem Value="Local">Local Contract</asp:ListItem>
                                <asp:ListItem Value="Overseas">Overseas  Contract</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" >Budgeted</td>
                        <td>:</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlBudget" runat="server">
                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator id="rfvBudget" 
                                    runat="server" 
                                    ErrorMessage="Select budgeted or unbudgeted"
                                    ControlToValidate="ddlBudget" 
                                    InitialValue="0" CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr class="subheader_img">
                                   
                    <td   colspan="6">  Description<font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        </span></font></td>
                    
                    
                    </tr>
                    <tr class="matters">
                   <td align="left"  colspan="6" >
                       <div style="padding:2px;">Why is this position begin recruited for?(extra 
                       staff,resignation,termination,etc.)</div><asp:TextBox ID="txtRecr_For" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox></td>
                    </tr>
                    <tr  class="matters">
                    <td  align="left" colspan="6"> <div style="padding:2px;">Proposed grade / salary, 
                        allowances, benefits etc.</div><asp:TextBox ID="txtBenefits" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox></td>
                    </tr>
                    
                    <tr class="matters">
                    <td align="left" colspan="6"> <div style="padding:2px;">Key responsibilities for the 
                        post (attach a job description)</div><asp:TextBox ID="txtKey" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox></td>
                    </tr>
                    
                    <tr class="matters">
                    
                      <td align="left">Minimum educational qualifications required<div style="padding:2px; font-size: 8pt; color: #800000; font-family:Verdana MS Sans Serif;">Hold Ctr key for multi select</div></td>
                      
                        <td>:</td>
                        <td align="left"> 
                             <asp:ListBox ID="lstQlf" runat="server" SelectionMode="Multiple" Width="218px">
                       </asp:ListBox>
                        </td>
                        <td align="left" >Nationality<br />(if appropriate)<div style="padding:2px;font-size: 8pt; color: #800000; font-family:Verdana MS Sans Serif;">Hold Ctr key for multi select</div></td>
                        <td>:</td>
                        <td align="left">
                           <asp:ListBox ID="lstBoxNat" runat="server" SelectionMode="Multiple" Width="229px">
                       </asp:ListBox>
                        </td>
                    
                    
                       
                    </tr>
                    
                    <tr class="matters">
                      <td align="left" colspan="6"> <div style="padding:2px;">Minimum experience required</div><asp:TextBox ID="txtMin_exp" 
                              runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox></td>
                    </tr>
                    <tr class="matters">
                    <td align="left" colspan="6"> <div style="padding:2px;">Additional information(include any travel needs)</div>
                        <asp:TextBox ID="txtAdd_info" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"
                        ></asp:TextBox>
                        </td>
                    </tr>
                   
                    <tr class="matters">
                        <td align="left" colspan="6">
                             <div style="padding:2px;">Further Comments</div>
                            <asp:TextBox ID="txtComments" runat="server" 
                           Rows="2" SkinID="MultiText" TextMode="MultiLine" Height="20px" 
                           Width="733px"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr class="matters">
            <td class="matters" style="height: 17px; width: 793px;" valign="bottom">
            </td>
        </tr>
        <tr class="matters">
            <td class="matters" valign="bottom" 
                align="center">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" 
                    CssClass="button" Text="Add" Width="90px" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" 
                    CssClass="button" Text="Edit" Width="90px" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                    ValidationGroup="groupM1" Width="90px" /><asp:Button ID="btnCancel" 
                    runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" Width="90px" />
                </td>
        </tr>
      
    </table>
    <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
    TargetControlID="lbHistory"
    PopupControlID="plHistory"
    Position="left" OffsetX="-510" OffsetY="16" />
      <asp:Panel id="plHistory" runat="server" Height="100px" width="600px">
                                   <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                                        DataKeyNames="r1" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                                        <RowStyle CssClass="griditem" Height="25px" />
                                        <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                    
                                                    
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Level">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLEVEL_DESCR" runat="server" Text='<%# Bind("LEVEL_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Authorized By ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUSR_DISPLAY_NAME" runat="server" 
                                                        Text='<%# Bind("USR_DISPLAY_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMPH_DATE" runat="server" Text='<%# BIND("MPH_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# BIND("MPH_STATUS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# BIND("MPH_REMARKS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle BackColor="Khaki" />
                                        <HeaderStyle CssClass="subheader_img" Height="25px" />
                                        <FooterStyle BackColor="#E4F0F7" Height="25px" Font-Names="Verdana" 
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                    </asp:Panel>
          


</asp:Content>

