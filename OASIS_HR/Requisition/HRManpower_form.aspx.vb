Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Requisition_HRManpower_form
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

          

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000670") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("MPR_ID") = "0"
                    bindBSU()
                    bindCategory()
                    bindNatonal()
                    bindQlf()
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    clear_all()
                    If ViewState("datamode") = "edit" Then
                        ViewState("MPR_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        ddlBSU_ID.Enabled = False
                        txtStart_dt.Enabled = False
                        imgStart_dt.Visible = False

                        BINDREQ_ID()

                    ElseIf ViewState("datamode") = "add" Then
                        lbHistory.Visible = False
                        ddlBSU_ID.Enabled = True
                        txtStart_dt.Enabled = True
                        imgStart_dt.Visible = True
                        txtStart_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    End If

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub BINDREQ_ID()

        Dim NAT As String = String.Empty
        Dim QLF As String = String.Empty
        Dim STR_CONN As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim STR_QUERY As String = "SELECT     MPR_CAT_ID, MPR_BSU_ID, MPR_TITLE, MPR_GENDER, MPR_NO_VAC, MPR_VAC_DESCR, MPR_JOB_TYPE, MPR_JOB_TYPE_DESCR, MPR_STATUS, " & _
                     " MPR_CONTRACT_TYPE, MPR_BUDGETED,REPLACE(CONVERT(VARCHAR(12), MPR_STARTDT,106),' ','/') AS MPR_STARTDT, MPR_RECRUITED_FOR, MPR_BENEFITS, MPR_KEY_RESP, MPR_MIN_EDU_QLF, MPR_MIN_EXP, " & _
    " MPR_NAT, MPR_ADD_INFO, MPR_COMMENTS, MPR_PROG_STATUS,isnull(MPR_bHISTORY,0) as MPR_bHISTORY  FROM  HR.MANPOWER_REQ_M WHERE MPR_ID='" & ViewState("MPR_ID") & "'"



        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, STR_QUERY)

            If DATAREADER.HasRows = True Then

                While DATAREADER.Read
                    If Not ddlCAT_ID.Items.FindByValue(Convert.ToString(DATAREADER("MPR_CAT_ID"))) Is Nothing Then
                        ddlCAT_ID.ClearSelection()
                        ddlCAT_ID.Items.FindByValue(Convert.ToString(DATAREADER("MPR_CAT_ID"))).Selected = True
                    End If

                    If Not ddlGender.Items.FindByValue(Convert.ToString(DATAREADER("MPR_GENDER"))) Is Nothing Then
                        ddlGender.ClearSelection()
                        ddlGender.Items.FindByValue(Convert.ToString(DATAREADER("MPR_GENDER"))).Selected = True
                    End If

                    If Not ddlStatus.Items.FindByValue(Convert.ToString(DATAREADER("MPR_STATUS"))) Is Nothing Then
                        ddlStatus.ClearSelection()
                        ddlStatus.Items.FindByValue(Convert.ToString(DATAREADER("MPR_STATUS"))).Selected = True
                    End If
                    If Not ddlPer_Tem.Items.FindByValue(Convert.ToString(DATAREADER("MPR_JOB_TYPE"))) Is Nothing Then
                        ddlPer_Tem.ClearSelection()
                        ddlPer_Tem.Items.FindByValue(Convert.ToString(DATAREADER("MPR_JOB_TYPE"))).Selected = True
                    End If
                    If Not ddlLocal.Items.FindByValue(Convert.ToString(DATAREADER("MPR_CONTRACT_TYPE"))) Is Nothing Then
                        ddlLocal.ClearSelection()
                        ddlLocal.Items.FindByValue(Convert.ToString(DATAREADER("MPR_CONTRACT_TYPE"))).Selected = True
                    End If
                    If Not ddlBudget.Items.FindByValue(Convert.ToString(DATAREADER("MPR_BUDGETED"))) Is Nothing Then
                        ddlBudget.ClearSelection()
                        ddlBudget.Items.FindByValue(Convert.ToString(DATAREADER("MPR_BUDGETED"))).Selected = True
                    End If
                    txtStart_dt.Text = Convert.ToString(DATAREADER("MPR_STARTDT"))
                    txtTitle.Text = Convert.ToString(DATAREADER("MPR_TITLE"))
                    txtVac_No.Text = Convert.ToString(DATAREADER("MPR_NO_VAC"))
                    txtVac_Result.Text = Convert.ToString(DATAREADER("MPR_VAC_DESCR"))
                    txtTemp_dur.Text = Convert.ToString(DATAREADER("MPR_JOB_TYPE_DESCR"))
                    txtRecr_For.Text = Convert.ToString(DATAREADER("MPR_RECRUITED_FOR"))
                    txtBenefits.Text = Convert.ToString(DATAREADER("MPR_BENEFITS"))
                    txtKey.Text = Convert.ToString(DATAREADER("MPR_KEY_RESP"))
                    txtMin_exp.Text = Convert.ToString(DATAREADER("MPR_MIN_EXP"))
                    txtAdd_info.Text = Convert.ToString(DATAREADER("MPR_ADD_INFO"))
                    txtComments.Text = Convert.ToString(DATAREADER("MPR_COMMENTS"))

                    lstQlf.ClearSelection()
                    QLF = "|" & Convert.ToString(DATAREADER("MPR_MIN_EDU_QLF"))
                    For Each item As ListItem In lstQlf.Items
                        If QLF.Contains("|" & item.Value & "|") = True Then
                            item.Selected = True

                        End If
                    Next

                    lstBoxNat.ClearSelection()
                    NAT = "|" & Convert.ToString(DATAREADER("MPR_NAT"))
                    For Each item As ListItem In lstBoxNat.Items
                        If NAT.Contains("|" & item.Value & "|") = True Then
                            item.Selected = True

                        End If
                    Next

                    If Convert.ToString(DATAREADER("MPR_PROG_STATUS")) = "" Or Convert.ToString(DATAREADER("MPR_PROG_STATUS")) = "Reopened" Then
                        btnSave.Enabled = True

                    Else
                        btnSave.Enabled = False

                    End If


                    If Convert.ToBoolean(DATAREADER("MPR_bHISTORY")) = True Then
                        lbHistory.Visible = True

                        bindReq_History()
                    Else
                        lbHistory.Visible = False
                    End If



                End While
            End If

        End Using
    End Sub
    Sub clear_all()
        If Not ddlBSU_ID.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_ID.ClearSelection()
            ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
        If Not ddlCAT_ID.Items.FindByValue("0") Is Nothing Then
            ddlCAT_ID.ClearSelection()
            ddlCAT_ID.Items.FindByValue("0").Selected = True
        End If
        If Not ddlGender.Items.FindByValue("Any") Is Nothing Then
            ddlGender.ClearSelection()
            ddlGender.Items.FindByValue("Any").Selected = True
        End If
        If Not ddlStatus.Items.FindByValue("Any") Is Nothing Then
            ddlStatus.ClearSelection()
            ddlStatus.Items.FindByValue("Any").Selected = True
        End If

        If Not ddlPer_Tem.Items.FindByValue("PERM") Is Nothing Then
            ddlPer_Tem.ClearSelection()
            ddlPer_Tem.Items.FindByValue("PERM").Selected = True
        End If

        If Not ddlLocal.Items.FindByValue("Local") Is Nothing Then
            ddlLocal.ClearSelection()
            ddlLocal.Items.FindByValue("Local").Selected = True
        End If

        If Not ddlBudget.Items.FindByValue("0") Is Nothing Then
            ddlBudget.ClearSelection()
            ddlBudget.Items.FindByValue("0").Selected = True
        End If
        lstQlf.ClearSelection()

        For Each item As ListItem In lstQlf.Items
            If item.Value = "0" Then
                item.Selected = True
            End If
        Next
        lstBoxNat.ClearSelection()
        For Each item As ListItem In lstBoxNat.Items
            If item.Value = "0" Then
                item.Selected = True
            End If
        Next



        txtStart_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTitle.Text = ""
        txtVac_No.Text = ""
        txtVac_Result.Text = ""
        txtTemp_dur.Text = ""
        txtRecr_For.Text = ""
        txtBenefits.Text = ""
        txtKey.Text = ""
        txtMin_exp.Text = ""
        txtAdd_info.Text = ""
        txtComments.Text = ""

    End Sub

    Private Sub bindReq_History()
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@MPH_MPR_ID", ViewState("MPR_ID"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.GETMANPOWER_REQ_HISTORY", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvSchool.DataSource = ds
            gvSchool.DataBind()
        End If



    End Sub

    Private Sub bindBSU()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "BSU")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_MPR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlBSU_ID.DataSource = ds
            ddlBSU_ID.DataTextField = "DESCR"
            ddlBSU_ID.DataValueField = "ID"
            ddlBSU_ID.DataBind()
        End If


        If Not ddlBSU_ID.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_ID.ClearSelection()
            ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True

        End If


    End Sub

    Private Sub bindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "CAT")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_MPR", param)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlCAT_ID.DataSource = ds
            ddlCAT_ID.DataTextField = "DESCR"
            ddlCAT_ID.DataValueField = "ID"
            ddlCAT_ID.DataBind()
        End If

    End Sub
    Private Sub bindNatonal()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet
        Dim ls As New ListItem
        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "NAT")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_MPR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            lstBoxNat.DataSource = ds
            lstBoxNat.DataTextField = "DESCR"
            lstBoxNat.DataValueField = "ID"
            lstBoxNat.DataBind()
        End If
        lstBoxNat.ClearSelection()
        For Each item As ListItem In lstBoxNat.Items
            If item.Value = "0" Then
                item.Selected = True
            End If
        Next
    End Sub
    Private Sub bindQlf()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "QLF")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_MPR", param)
        lstQlf.Items.Add(New ListItem("Any", "0"))
        If ds.Tables(0).Rows.Count > 0 Then
            lstQlf.DataSource = ds
            lstQlf.DataTextField = "DESCR"
            lstQlf.DataValueField = "ID"
            lstQlf.DataBind()
        End If
        lstQlf.ClearSelection()
        For Each item As ListItem In lstQlf.Items
            If item.Value = "0" Then
                item.Selected = True
            End If
        Next

    End Sub
    Function saveReq_form(ByVal bEDIT As Boolean, ByVal errorMessage As String) As String

        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty
        Dim NAT As New StringBuilder
        Dim QLF As New StringBuilder

        Using conn As SqlConnection = ConnectionManger.GetOASIS_HRConnection

            Try

                For Each ITEM As ListItem In lstBoxNat.Items
                    If ITEM.Selected = True Then
                        NAT.Append(ITEM.Value + "|")
                    End If
                Next

                For Each ITEM As ListItem In lstQlf.Items
                    If ITEM.Selected = True Then
                        QLF.Append(ITEM.Value + "|")
                    End If
                Next

                transaction = conn.BeginTransaction("SampleTransaction")
                Dim pParms(25) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MPR_ID", ViewState("MPR_ID"))
                pParms(1) = New SqlClient.SqlParameter("@MPR_BSU_ID", ddlBSU_ID.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@MPR_CAT_ID", ddlCAT_ID.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@MPR_STARTDT", txtStart_dt.Text.Trim)
                pParms(4) = New SqlClient.SqlParameter("@MPR_TITLE", txtTitle.Text)
                pParms(5) = New SqlClient.SqlParameter("@MPR_GENDER", ddlGender.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@MPR_STATUS", ddlStatus.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@MPR_NO_VAC", txtVac_No.Text.Trim)
                pParms(8) = New SqlClient.SqlParameter("@MPR_VAC_DESCR", txtVac_Result.Text)
                pParms(9) = New SqlClient.SqlParameter("@MPR_JOB_TYPE", ddlPer_Tem.SelectedValue)
                pParms(10) = New SqlClient.SqlParameter("@MPR_JOB_TYPE_DESCR", txtTemp_dur.Text.Trim)
                pParms(11) = New SqlClient.SqlParameter("@MPR_CONTRACT_TYPE", ddlLocal.SelectedValue)
                pParms(12) = New SqlClient.SqlParameter("@MPR_BUDGETED", ddlBudget.SelectedValue)
                pParms(13) = New SqlClient.SqlParameter("@MPR_RECRUITED_FOR", txtRecr_For.Text.Trim)
                pParms(14) = New SqlClient.SqlParameter("@MPR_BENEFITS", txtBenefits.Text.Trim)
                pParms(15) = New SqlClient.SqlParameter("@MPR_KEY_RESP", txtKey.Text.Trim)
                pParms(16) = New SqlClient.SqlParameter("@MPR_MIN_EXP", txtMin_exp.Text.Trim)
                pParms(17) = New SqlClient.SqlParameter("@MPR_ADD_INFO", txtAdd_info.Text.Trim)
                pParms(18) = New SqlClient.SqlParameter("@MPR_COMMENTS", txtComments.Text.Trim)
                pParms(19) = New SqlClient.SqlParameter("@MPR_MIN_EDU_QLF", QLF.ToString)
                pParms(20) = New SqlClient.SqlParameter("@MPR_NAT", NAT.ToString)
                pParms(21) = New SqlClient.SqlParameter("@MPR_ADD_USR", Session("sUsr_id"))
                pParms(22) = New SqlClient.SqlParameter("@bEDIT", bEDIT)
                pParms(23) = New SqlClient.SqlParameter("@OUT_MPR_ID", SqlDbType.BigInt)
                pParms(23).Direction = ParameterDirection.Output
                pParms(24) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(24).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "HR.SAVEMANPOWER_REQ_M", pParms)
                Status = pParms(24).Value



                If Status <> 0 Then
                    saveReq_form = "1"
                    Return "1"
                Else
                    ViewState("MPR_ID") = pParms(23).Value.ToString
                End If


                saveReq_form = "0"
                ViewState("datamode") = "edit"


                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Catch ex As Exception
                saveReq_form = "1"
                errorMessage = "Error Occured While Saving."
                UtilityObj.Errorlog(ex.Message)
            Finally
                If saveReq_form <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim errorMessage As String = String.Empty
        Dim bEDIT As Boolean
        If ViewState("datamode") = "add" Then
            bEDIT = False

        Else
            bEDIT = True
        End If

        Dim str_err As String = saveReq_form(bEDIT, errorMessage)

        If str_err = "0" Then
            lblError.Text = "Record saved successfully"


        Else
            lblError.Text = "Record could not be Updated"
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_all()

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ViewState("MPR_ID") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                clear_all()


                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
   
    
End Class
