Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Requisition_HRVisa_form
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000675") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("VR_ID") = "0"
                    bindBSU()
                    bindCategory()
                    bindNatonal()
                    bindQlf()
                    bindVISA_type()
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    clear_all()
                    If ViewState("datamode") = "edit" Then
                        ViewState("VR_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        ddlBSU_ID.Enabled = False
                        txtDoc_dt.Enabled = False
                        imgStart_dt.Visible = False

                        BINDREQ_ID()

                    ElseIf ViewState("datamode") = "add" Then
                        ddlBSU_ID.Enabled = True
                        txtDoc_dt.Enabled = True
                        imgStart_dt.Visible = True
                        txtDoc_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        lbHistory.Visible = False
                    End If

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Sub BINDREQ_ID()
        Dim NAT As String = String.Empty
        Dim QLF As String = String.Empty
        Dim STR_CONN As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim STR_QUERY As String = " SELECT REPLACE(CONVERT(VARCHAR(12),[VR_DOC_DT],106),' ','/') AS [VR_DOC_DT],[VR_VM_ID]" & _
" ,[VR_APP_FIRST_NAME],[VR_APP_MID_NAME],[VR_APP_LAST_NAME],[VR_PASSPORTNO],REPLACE(CONVERT(VARCHAR(12),[VR_DOB],106),' ','/') AS [VR_DOB]" & _
      " ,[VR_GENDER] ,[VR_STATUS],[VR_FATHER_FIRST_NAME],[VR_FATHER_MID_NAME] ,[VR_FATHER_LAST_NAME]" & _
      " ,[VR_MOTHER_FIRST_NAME],[VR_MOTHER_MID_NAME],[VR_MOTHER_LAST_NAME],[VR_HOME_ADDR],[VR_NAT_ID] " & _
      " ,[VR_BSU_ID],[VR_CAT_ID],[VR_TITLE],[VR_MPR_ID], [VR_EDU_QLF],[VR_PROC_FEE] ,[VR_bBUDGETTED]" & _
      "  ,[VR_BUDGETTED_DESCR],[VR_COMMENTS],[VR_PROG_STATUS], isnull([VR_bHISTORY],0)  as VR_bHISTORY,[VR_PURPOSE] FROM [OASIS_HR].[VISA].[VISA_REQ_M] WITH(NOLOCK)   where [VR_ID]='" & ViewState("VR_ID") & "'"
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(STR_CONN, CommandType.Text, STR_QUERY)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    txtPurpose.Text = Convert.ToString(DATAREADER("VR_PURPOSE"))

                    txtDoc_dt.Text = Convert.ToString(DATAREADER("VR_DOC_DT"))
                    txtPos_Title.Text = Convert.ToString(DATAREADER("VR_TITLE"))
                    txtProc_fee.Text = Convert.ToString(DATAREADER("VR_PROC_FEE"))
                    txtPos_budget.Text = Convert.ToString(DATAREADER("VR_BUDGETTED_DESCR"))
                    txtComments.Text = Convert.ToString(DATAREADER("VR_COMMENTS"))
                    chkbBug.Checked = Convert.ToBoolean(DATAREADER("VR_bBUDGETTED"))
                    If Not ddlBSU_ID.Items.FindByValue(Convert.ToString(DATAREADER("VR_BSU_ID"))) Is Nothing Then
                        ddlBSU_ID.ClearSelection()
                        ddlBSU_ID.Items.FindByValue(Convert.ToString(DATAREADER("VR_BSU_ID"))).Selected = True
                    End If

                    If Not ddlNat.Items.FindByValue(Convert.ToString(DATAREADER("VR_NAT_ID"))) Is Nothing Then
                        ddlNat.ClearSelection()
                        ddlNat.Items.FindByValue(Convert.ToString(DATAREADER("VR_NAT_ID"))).Selected = True
                    End If

                    If Not ddlVisa_type.Items.FindByValue(Convert.ToString(DATAREADER("VR_VM_ID"))) Is Nothing Then
                        ddlVisa_type.ClearSelection()
                        ddlVisa_type.Items.FindByValue(Convert.ToString(DATAREADER("VR_VM_ID"))).Selected = True
                    End If

                    If Not ddlCAT_ID.Items.FindByValue(Convert.ToString(DATAREADER("VR_CAT_ID"))) Is Nothing Then
                        ddlCAT_ID.ClearSelection()
                        ddlCAT_ID.Items.FindByValue(Convert.ToString(DATAREADER("VR_CAT_ID"))).Selected = True
                    End If

                    If Not ddlGender.Items.FindByValue(Convert.ToString(DATAREADER("VR_GENDER"))) Is Nothing Then
                        ddlGender.ClearSelection()
                        ddlGender.Items.FindByValue(Convert.ToString(DATAREADER("VR_GENDER"))).Selected = True
                    End If

                    If Not ddlStatus.Items.FindByValue(Convert.ToString(DATAREADER("VR_STATUS"))) Is Nothing Then
                        ddlStatus.ClearSelection()
                        ddlStatus.Items.FindByValue(Convert.ToString(DATAREADER("VR_STATUS"))).Selected = True
                    End If
                    txtFname.Text = Convert.ToString(DATAREADER("VR_APP_FIRST_NAME"))
                    txtMname.Text = Convert.ToString(DATAREADER("VR_APP_MID_NAME"))
                    txtLname.Text = Convert.ToString(DATAREADER("VR_APP_LAST_NAME"))
                    txtDob.Text = Convert.ToString(DATAREADER("VR_DOB"))
                    txtPassportNo.Text = Convert.ToString(DATAREADER("VR_PASSPORTNO"))
                    txtFather_First_name.Text = Convert.ToString(DATAREADER("VR_FATHER_FIRST_NAME"))
                    txtFather_Mid_name.Text = Convert.ToString(DATAREADER("VR_FATHER_MID_NAME"))
                    txtFather_Last_name.Text = Convert.ToString(DATAREADER("VR_FATHER_LAST_NAME"))
                    txtMother_First_name.Text = Convert.ToString(DATAREADER("VR_MOTHER_FIRST_NAME"))
                    txtMother_Mid_name.Text = Convert.ToString(DATAREADER("VR_MOTHER_MID_NAME"))
                    txtMother_Last_name.Text = Convert.ToString(DATAREADER("VR_MOTHER_LAST_NAME"))
                    txtAddress.Text = Convert.ToString(DATAREADER("VR_HOME_ADDR"))
                    lstQlf.ClearSelection()
                    QLF = "|" & Convert.ToString(DATAREADER("VR_EDU_QLF"))
                    For Each item As ListItem In lstQlf.Items
                        If QLF.Contains("|" & item.Value & "|") = True Then
                            item.Selected = True

                        End If
                    Next

                    If Convert.ToString(DATAREADER("VR_PROG_STATUS")) = "" Or Convert.ToString(DATAREADER("VR_PROG_STATUS")) = "Reopened" Then
                        btnSave.Enabled = True

                    Else
                        btnSave.Enabled = False

                    End If


                    If Convert.ToBoolean(DATAREADER("VR_bHISTORY")) = True Then
                        lbHistory.Visible = True

                        bindReq_History()
                    Else
                        lbHistory.Visible = False
                    End If





                End While
            End If

        End Using
    End Sub
    Sub clear_all()
        If Not ddlBSU_ID.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_ID.ClearSelection()
            ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True
        End If

        ddlCAT_ID.ClearSelection()
        ddlCAT_ID.SelectedIndex = 0

        ddlGender.ClearSelection()
        ddlGender.SelectedIndex = 0
        ddlStatus.ClearSelection()
        ddlStatus.SelectedIndex = 0
        lstQlf.ClearSelection()
        For Each item As ListItem In lstQlf.Items
            If item.Value = "0" Then
                item.Selected = True
            End If
        Next
        txtDoc_dt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtPos_Title.Text = ""

        txtComments.Text = ""
    End Sub
    Private Sub bindReq_History()
        Dim str_conn As String = ConnectionManger.GetOASIS_HRConnectionString
        Dim ds As New DataSet

        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@RH_REF_ID", ViewState("VR_ID"))
        param(1) = New SqlParameter("@RH_APM_DOC_TYPE", "VR")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[VISA].[GETREQUISITION_HISTORY]", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvSchool.DataSource = ds
            gvSchool.DataBind()
        End If
    End Sub
    Private Sub bindBSU()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "BSU")

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_VR", param)

            ddlBSU_ID.DataSource = datareader
            ddlBSU_ID.DataTextField = "DESCR"
            ddlBSU_ID.DataValueField = "ID"
            ddlBSU_ID.DataBind()
        End Using


        If Not ddlBSU_ID.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBSU_ID.ClearSelection()
            ddlBSU_ID.Items.FindByValue(Session("sBsuid")).Selected = True

        End If
    End Sub
    Private Sub bindVISA_type()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet
        Dim str As String = String.Empty
        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "VISA")
        str = "<table cellpadding='6' cellspacing='4'><tr><td colspan='2' class='matters'>Mininum processing time:</td><td colspan='2' class='matters'>Maximum permitted stay:</td></tr>"
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_VR", param)
            While datareader.Read

                ddlVisa_type.Items.Add(New ListItem(Convert.ToString(datareader("VM_TYPE")), Convert.ToString(datareader("VM_ID"))))
                str += "<tr><td class='matters'>" & Convert.ToString(datareader("VM_TYPE")) & "</td><td class='matters'>" & Convert.ToString(datareader("VM_MIN_PROC_TIME")) & "</td>" & _
                "<td class='matters'>" & Convert.ToString(datareader("VM_TYPE")) & "</td><td class='matters'>" & Convert.ToString(datareader("VM_MAX_PER_STAY")) & "</td></tr>"

            End While

        End Using
        ltVisaInfo.Text = str + "</table>"


    End Sub
    Private Sub bindCategory()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "CAT")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_VR", param)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlCAT_ID.DataSource = ds
            ddlCAT_ID.DataTextField = "DESCR"
            ddlCAT_ID.DataValueField = "ID"
            ddlCAT_ID.DataBind()
        End If

    End Sub
    Private Sub bindNatonal()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet
        Dim ls As New ListItem
        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "NAT")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_VR", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlNat.DataSource = ds
            ddlNat.DataTextField = "DESCR"
            ddlNat.DataValueField = "ID"
            ddlNat.DataBind()
        End If

        If Not ddlNat.Items.FindByValue("0") Is Nothing Then
            ddlNat.ClearSelection()
            ddlNat.Items.FindByValue("0").Selected = True
        End If

    End Sub
    Private Sub bindQlf()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet

        Dim param(1) As SqlClient.SqlParameter
        param(0) = New SqlParameter("@Type", "QLF")

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "HR.COMMON_SP_VR", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lstQlf.DataSource = ds
            lstQlf.DataTextField = "DESCR"
            lstQlf.DataValueField = "ID"
            lstQlf.DataBind()
        End If
        lstQlf.ClearSelection()
        For Each item As ListItem In lstQlf.Items
            If item.Value = "0" Then
                item.Selected = True
            End If
        Next

    End Sub
    Function saveReq_form(ByVal bEDIT As Boolean, ByVal errorMessage As String) As String

        Dim transaction As SqlTransaction
        Dim Status As String = String.Empty
        Dim NAT As New StringBuilder
        Dim QLF As New StringBuilder

        Using conn As SqlConnection = ConnectionManger.GetOASIS_HRConnection

            Try



                For Each ITEM As ListItem In lstQlf.Items
                    If ITEM.Selected = True Then
                        QLF.Append(ITEM.Value + "|")
                    End If
                Next

                transaction = conn.BeginTransaction("SampleTransaction")
                Dim pParms(35) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@VR_ID", ViewState("VR_ID"))
                pParms(1) = New SqlClient.SqlParameter("@VR_DOC_DT", txtDoc_dt.Text.Trim)
                pParms(2) = New SqlClient.SqlParameter("@VR_VM_ID", ddlVisa_type.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@VR_APP_FIRST_NAME", txtFname.Text.Trim)
                pParms(4) = New SqlClient.SqlParameter("@VR_APP_MID_NAME", txtMname.Text)
                pParms(5) = New SqlClient.SqlParameter("@VR_APP_LAST_NAME", txtLname.Text)
                pParms(6) = New SqlClient.SqlParameter("@VR_PASSPORTNO", txtPassportNo.Text.Trim)
                pParms(7) = New SqlClient.SqlParameter("@VR_DOB", txtDob.Text.Trim)
                pParms(8) = New SqlClient.SqlParameter("@VR_GENDER", ddlGender.SelectedValue)
                pParms(9) = New SqlClient.SqlParameter("@VR_STATUS", ddlStatus.SelectedValue)
                pParms(10) = New SqlClient.SqlParameter("@VR_FATHER_FIRST_NAME", txtFather_First_name.Text.Trim)
                pParms(11) = New SqlClient.SqlParameter("@VR_FATHER_MID_NAME", txtFather_Mid_name.Text.Trim)
                pParms(12) = New SqlClient.SqlParameter("@VR_FATHER_LAST_NAME", txtFather_Last_name.Text.Trim)
                pParms(13) = New SqlClient.SqlParameter("@VR_MOTHER_FIRST_NAME", txtMother_First_name.Text.Trim)
                pParms(14) = New SqlClient.SqlParameter("@VR_MOTHER_MID_NAME", txtMother_Mid_name.Text.Trim)
                pParms(15) = New SqlClient.SqlParameter("@VR_MOTHER_LAST_NAME", txtMother_Last_name.Text.Trim)
                pParms(16) = New SqlClient.SqlParameter("@VR_HOME_ADDR", txtAddress.Text.Trim)
                pParms(17) = New SqlClient.SqlParameter("@VR_NAT_ID", ddlNat.SelectedValue)
                pParms(18) = New SqlClient.SqlParameter("@VR_BSU_ID", ddlBSU_ID.SelectedValue)
                pParms(19) = New SqlClient.SqlParameter("@VR_CAT_ID", ddlCAT_ID.SelectedValue)
                pParms(20) = New SqlClient.SqlParameter("@VR_TITLE", txtPos_Title.Text.Trim)
                pParms(21) = New SqlClient.SqlParameter("@VR_EDU_QLF", QLF.ToString)
                pParms(22) = New SqlClient.SqlParameter("@VR_PROC_FEE", txtProc_fee.Text.Trim)
                pParms(23) = New SqlClient.SqlParameter("@VR_bBUDGETTED", chkbBug.Checked)
                pParms(24) = New SqlClient.SqlParameter("@VR_BUDGETTED_DESCR", txtPos_budget.Text.Trim)
                pParms(25) = New SqlClient.SqlParameter("@VR_COMMENTS", txtComments.Text.Trim)
                pParms(26) = New SqlClient.SqlParameter("@VR_USR_ID", Session("sUsr_id"))
                pParms(27) = New SqlClient.SqlParameter("@VR_PURPOSE", txtPurpose.Text)

                pParms(28) = New SqlClient.SqlParameter("@bEDIT", bEDIT)
                pParms(29) = New SqlClient.SqlParameter("@OUT_VR_ID", SqlDbType.BigInt)
                pParms(29).Direction = ParameterDirection.Output
                pParms(30) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(30).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "VISA.SAVEVISA_REQ_M", pParms)
                Status = pParms(30).Value



                If Status <> 0 Then
                    saveReq_form = "1"
                    Return "1"
                Else
                    ViewState("VR_ID") = pParms(29).Value.ToString
                End If


                saveReq_form = "0"
                ViewState("datamode") = "edit"


                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Catch ex As Exception
                saveReq_form = "1"
                errorMessage = "Error Occured While Saving."
                UtilityObj.Errorlog(ex.Message)
            Finally
                If saveReq_form <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim errorMessage As String = String.Empty
        Dim bEDIT As Boolean
        If ViewState("datamode") = "add" Then
            bEDIT = False

        Else
            bEDIT = True
        End If

        Dim str_err As String = saveReq_form(bEDIT, errorMessage)

        If str_err = "0" Then
            lblError.Text = "Record saved successfully"


        Else
            lblError.Text = "Record could not be Updated"
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clear_all()

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ViewState("VR_ID") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                clear_all()


                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


End Class
