<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="HRManpower_Appr.aspx.vb" Inherits="HRManpower_Appr" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">

    function Apprvdata(MPR_ID, LEV_ID, USR_ID) {
       
    
            var sFeatures, url;
            sFeatures = "dialogWidth:620px; ";
            sFeatures += "dialogHeight: 350px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            url = "HRModal_ProcessAppr.aspx?MPR_ID=" + MPR_ID + " &LEV=" + LEV_ID + "&UID=" + USR_ID;
            result = window.showModalDialog(url, "", sFeatures);

            location.reload();
        return true;
    } 
</script>


    
    <table  align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error"
                    EnableViewState="False" ForeColor="" ValidationGroup="groupM1" /></td>
        </tr>  
               <tr>
               
            <td class="matters" align="center">
          
               
                <table  BorderColor="#1b80b6" border="1"
                    CellPadding="5" CellSpacing="0" class="BlueTableView"   style="width: 810px">
                    
                    <tr Class="subheader_img">
                        <td   colspan="6"  >
                                 <span style="clear: left;display: inline; float: left; visibility: visible;">
                                                Manpower Requisation Approval</span>
                                                  <span style="clear: right;display: inline; float: right; visibility: visible">
                                                   <asp:LinkButton ID="lbHistory" runat="server" Font-Size="12px" Font-Bold="True" Font-Italic="True">View History</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton ID="lbDescr" runat="server" Font-Size="12px" Font-Bold="True" Font-Italic="True">View Description</asp:LinkButton></span></td>
                    </tr>
                    
                    <tr class="matters" >
                        <td  align="left"  >School</td>
                        <td style="height:17px;">:</td>
                        <td align="left" colspan="4">  
                            <asp:Literal ID="ltSchool" runat="server" ></asp:Literal>
                        </td>
                    </tr>
                    <tr  class="matters">
                        <td  align="left">Category</td>
                        <td style="height:17px;">:</td>
                        <td  align="left"> 
                            <asp:Literal ID="ltCat" runat="server"></asp:Literal>

                        </td>
                        <td align="left" >Start Date</td>
                        <td style="height:17px;">:</td>
                        <td align="left">
                                                                                                <asp:Literal ID="ltStartDt" runat="server"></asp:Literal>
                                                                                                
                                                                                                
                                                                                            </td>
                    </tr>
                    <tr  class="matters">
                        <td align="left">Title/Position</td>
                        <td style="height:17px;">:</td>
                        <td align="left" colspan="4"> 
                            <asp:Literal ID="ltTitle" runat="server"></asp:Literal>
                           
                        </td>
                    </tr>
                    <tr class="matters">
                        <td  align="left">Male/Female</td>
                        <td style="height:17px;">:</td>
                        <td  align="left"> 
                            <asp:Literal ID="ltGender" runat="server"></asp:Literal>
                        </td>
                        <td align="left" >Status(single/married)</td>
                        <td>:</td>
                        <td align="left">
                            <asp:Literal ID="ltStatus" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td align="left">No. of vacancies</td>
                        <td style="height:17px;">:</td>
                        <td  align="left"> 
                            <asp:Literal ID="ltNo_Vac" runat="server"></asp:Literal>
                        </td>
                        <td align="left" >Vacancy result of</td>
                        <td>:</td>
                        <td align="left">
                            <asp:Literal ID="ltRes_Vac" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="matters" >
                        <td  align="left">Permanent/temporary</td>
                        <td style="height:17px;">:</td>
                        <td align="left"> 
                            <asp:Literal ID="ltType" runat="server"></asp:Literal>
                        </td>
                        <td align="left" >if temp,then duration</td>
                        <td style="height:17px;">:</td>
                        <td align="left">
                            <asp:Literal ID="ltDur" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td align="left">Local/overseas contract</td>
                        <td style="height:17px;">:</td>
                        <td align="left"> 
                            <asp:Literal ID="ltContr" runat="server"></asp:Literal> </td>
                        <td align="left" >Budgeted</td>
                        <td>:</td>
                        <td align="left">
                            <asp:Literal ID="ltBug" runat="server"></asp:Literal> </td>
                    </tr>
                    <tr class="subheader_img">
                                   
                    <td   colspan="6">Approvals<font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                        </span></font></td>
                    
                    
                    </tr>
                    <tr class="matters">
                   <td align="left"  colspan="6" >
                        <asp:GridView ID="gvAppr" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                                        DataKeyNames="ID" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" 
                            Width="100%" BorderStyle="Solid" BorderWidth="1px" CellPadding="1">
                                        <RowStyle CssClass="griditem" Height="25px" />
                                        <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    
                                                    
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Level">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLEVEL_DESCR" runat="server" Text='<%# Bind("LEVEL_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Authorized By ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAUTH_USR" runat="server" 
                                                        Text='<%# Bind("AUTH_USR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Processed By">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnProc_By" runat="server" Text='<%# bind("PROC_BY") %>'></asp:LinkButton>
                                                    <div>
                                  <div ID="panel1" runat="server" style="background-color:white; border:solid 1px #1b80b6;color:#1b80b6;width:265px;height:100px;overflow: auto;" visible='<%# BIND("bREMARKS") %>' >
                                     <div class="msgHeadOutAdd" >
          <div class="msgInner2">
        Remarks
                    </div>
           </div><div>
                                      <asp:label id="lbRemark" runat="server" Text='<%# BIND("PROC_REMARKS") %>' 
                                          Font-Names="Verdana" Font-Size="12px" ForeColor="Black" ></asp:label></div></div></div>
<ajaxToolkit:PopupControlExtender id="PopupControlExtender1" runat="server" OffsetX="-50"   PopupControlID="Panel1" Position="Bottom"  TargetControlID="lbtnProc_By" Enabled='<%# BIND("bREMARKS") %>'>
                            </ajaxToolkit:PopupControlExtender> 
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Process Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPROC_DATE" runat="server" Text='<%# BIND("PROC_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="lblPROC_STATUS" runat="server" 
                                                         Text='<%# BIND("PROC_STATUS") %>' Enabled='<%# BIND("FLAG") %>' 
                                                         ToolTip='<%# BIND("FLAG") %>' ></asp:LinkButton>
                                                     <asp:Label ID="lblLEVEL_ID" runat="server" Text='<%# bind("LEVEL_ID") %>' 
                                                         Visible="False"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle BackColor="Khaki" />
                                        <HeaderStyle CssClass="subheader_img" Height="25px" />
                                        <FooterStyle BackColor="#E4F0F7" Height="25px" Font-Names="Verdana" 
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView></td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr class="matters">
            <td class="matters" style="height: 17px; width: 793px;" valign="bottom">
            </td>
        </tr>
        <tr class="matters">
            <td class="matters" valign="bottom" 
                align="center">
                <asp:Button ID="btnBack" 
                    runat="server" CausesValidation="False" CssClass="button"
                    Text="Back" Width="90px" />
                </td>
        </tr>
      
    </table>
 
        <ajaxToolkit:PopupControlExtender ID="popExDescr" runat="server"
    TargetControlID="lbDescr"
    PopupControlID="plDescr"
    Position="left" OffsetX="-480" OffsetY="16" />
      <asp:Panel id="plDescr" runat="server" Height="100px" width="600px" BackColor="White">
      <div style="background-color:White;" width="100%">
      <table BorderColor="#1b80b6" border="1"
                    CellPadding="5" CellSpacing="0" class="BlueTableView" width="100%">
      <tr>
      <td class="matters">
       <div style="padding:2px;">Why is this position begin recruited for?(extra 
                       staff,resignation,termination,etc.)</div>
          &nbsp;<asp:Literal ID="ltPos" runat="server"></asp:Literal></td>
      </tr>
        <tr style="background-color:#CEE3FF;">
      <td class="matters">
      <div style="padding:2px;">Proposed grade / salary, 
                        allowances, benefits etc.</div>
         &nbsp;<asp:Literal ID="ltProp" runat="server"></asp:Literal></td>
      </tr>
         <tr>
      <td class="matters">
     <div style="padding:2px;">Key responsibilities for the 
                        post (attach a job description)</div>
              &nbsp;<asp:Literal ID="ltKey" runat="server"></asp:Literal></td>
      </tr>
       <tr style="background-color:#CEE3FF;">
      <td class="matters">
     <div style="padding:2px;">Minimum educational qualifications required</div>
     &nbsp;<asp:Literal ID="ltMin_Edu" runat="server"></asp:Literal></td>
      </tr>
       <tr>
      <td class="matters">
     <div style="padding:2px;">Nationality (if appropriate)</div>
       &nbsp;<asp:Literal ID="ltNat" runat="server"></asp:Literal></td>
      </tr>
        <tr style="background-color:#CEE3FF;">
      <td class="matters">
     <div style="padding:2px;">Minimum experience required</div>
     &nbsp;<asp:Literal ID="ltMin_exp" runat="server"></asp:Literal></td>
      </tr>
         <tr>
      <td class="matters">
    <div style="padding:2px;">Additional information(include any travel needs)</div>
    &nbsp;<asp:Literal ID="ltAdd_info" runat="server"></asp:Literal></td>
      </tr>
        <tr style="background-color:#CEE3FF;">
      <td class="matters">
    <div style="padding:2px;">Further Comments</div>
    &nbsp;<asp:Literal ID="ltComm" runat="server"></asp:Literal></td>
      </tr>
      </table></div>
      </asp:Panel>
    <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
    TargetControlID="lbHistory"
    PopupControlID="plHistory"
    Position="left" OffsetX="-510" OffsetY="16" />
      <asp:Panel id="plHistory" runat="server" Height="100px" width="600px">
                                   <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                                        DataKeyNames="r1" EmptyDataText="No record available !!!" EnableModelValidation="True"
                                        Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                                        <RowStyle CssClass="griditem" Height="25px" />
                                        <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                    
                                                    
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Level">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLEVEL_DESCR" runat="server" Text='<%# Bind("LEVEL_DESCR") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Authorized By ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUSR_DISPLAY_NAME" runat="server" 
                                                        Text='<%# Bind("USR_DISPLAY_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMPH_DATE" runat="server" Text='<%# BIND("MPH_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# BIND("MPH_STATUS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# BIND("MPH_REMARKS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle BackColor="Khaki" />
                                        <HeaderStyle CssClass="subheader_img" Height="25px" />
                                        <FooterStyle BackColor="#E4F0F7" Height="25px" Font-Names="Verdana" 
                                            Font-Size="8pt" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                    </asp:Panel>
          


</asp:Content>

