﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Exit_workflow_View.aspx.vb" Inherits="Exit_workflow_View" %>

<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameAddPDC").fancybox({
                type: 'iframe',
                fitToView: false,
                width: '85%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {  
                    parent.location.reload(true);
                }
            });
        });
 

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>EXIT WORKFLOW VIEW
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" width="100%">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom">
                            <div id="lblError" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                      <tr id="trAdd">
                        <td align="left">    
                            <a id="frameAdd" class="frameAddPDC" href="Exit_workflow_Add.aspx?Id=0">Initiate</a> 
                        </td>
                    </tr>
                    <tr id="trGridv" runat="server">
                        <td align="center">

                            <asp:GridView ID="gvEmpList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="10" Width="100%" OnPageIndexChanging="gvEmpList_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="EXT_WF_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXT_WF_ID" runat="server" Text='<%# Bind("EXT_WF_ID")%>'></asp:Label>
                                             <asp:Label ID="lblStaff_LST_ID" runat="server" Text='<%# Bind("Staff_LST_ID")%>'></asp:Label>
                                            <asp:Label ID="lblIs_HR" runat="server" Text='<%# Bind("Is_HR")%>'></asp:Label>
                                            
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Employee Number
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtEMPNO" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchEMPNO" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchEMPNO"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNO" runat="server" Text='<%# Bind("EMPNO")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Employee Name
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtEMP_NAME" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchEMP_NAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchEMP_NAME"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMP_NAME" runat="server" Text='<%# Bind("EMP_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Department
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtDPT_DESCR" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchDPT_DESCR" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchDPT_DESCR"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDPT_DESCR" runat="server" Text='<%# Bind("DPT_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Designation
                                            </span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDES_DESCR" runat="server" Text='<%# Bind("DES_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Workflow started
                                            </span>
                                        </HeaderTemplate>
                                        <ItemTemplate><div align="center">
                                            <asp:Label ID="lblIsInDraft" runat="server" Text='<%# Bind("IsInDraft")%>'></asp:Label></div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Workflow completed
                                            </span>
                                        </HeaderTemplate>
                                        <ItemTemplate><div align="center">
                                            <asp:Label ID="lblEXT_IS_COMPLETED" runat="server" Text='<%# Bind("EXT_IS_COMPLETED")%>'></asp:Label></div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypEdit" runat="server" CssClass="frameAddPDC">Edit</asp:HyperLink>
                                            <%--<asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="Edit" CssClass="frameAddPDC"
                                                OnClick="lbtnEdit_Click"></asp:LinkButton> --%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypView" runat="server" Target="_blank">View</asp:HyperLink> 
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="#" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypOpen" runat="server" Target="_blank">Open</asp:HyperLink> 
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                           
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

