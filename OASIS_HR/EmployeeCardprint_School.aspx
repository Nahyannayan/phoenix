<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EmployeeCardprint_School.aspx.vb" Inherits="Students_Studcardprint" Title="Untitled Page" %>

<%@ Register Src="usercontrols/EmployeeCardPrint_School.ascx" TagName="EmployeeCardPrint" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Print Employee ID Card(SCHOOL)
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <uc1:EmployeeCardPrint ID="EmployeeCardPrint1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

