Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class EmployeeCardPrint_Safecor
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        If Not IsPostBack Then
            lblmessage.Text = ""
            Dim studClass As New studClass
            BindBussinessUnit()

            ddlBSUnit.ClearSelection()
            ddlBSUnit.Items.FindByValue(Session("sBsuid")).Selected = True


            BindStudentView()

            FUUploadStuPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
            If Session("sBsuid") = "888889" Then
                Session("printIDType") = "SAFECOR"
            Else
                Session("printIDType") = "GEMS_SCHOOL"
            End If
        End If
        If ViewState("popup") = 1 Then
            ModalpopupUpload.Show()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))
    End Sub
    Public Sub BindBussinessUnit()
        Dim strQuery = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE ISNULL(BSU_bSHOW, 0) = 1 "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        ddlBSUnit.DataSource = ds
        ddlBSUnit.DataTextField = "BSU_NAME"
        ddlBSUnit.DataValueField = "BSU_ID"
        ddlBSUnit.DataBind()
    End Sub
    Public Function GetData() As DataSet


        Dim strQuery = "SELECT * FROM(SELECT EMP_ID, EMPNO,  " & _
        " ISNULL(EMP_DISPLAYNAME,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,''))) EMP_NAME " & _
        ", CASE WHEN ISNULL(EMD_PHOTO,'') = '' THEN 'False' ELSE 'True' END PHOTOAVALIABLE " & _
        ", EMPDESIGNATION_M.DES_DESCR, EMPCATEGORY_M.ECT_DESCR, DEPARTMENT_M.DPT_DESCR,EMP_DES_ID FROM EMPLOYEE_M  " & _
        " INNER JOIN EMPLOYEE_D ON EMPLOYEE_D.EMD_EMP_ID = EMPLOYEE_M.EMP_ID " & _
        "INNER JOIN EMPCATEGORY_M ON EMPLOYEE_M.EMP_ECT_ID = EMPCATEGORY_M.ECT_ID  " & _
        "INNER JOIN EMPDESIGNATION_M ON EMPLOYEE_M.EMP_DES_ID = EMPDESIGNATION_M.DES_ID  " & _
        "INNER JOIN DEPARTMENT_M ON EMPLOYEE_M.EMP_DPT_ID = DEPARTMENT_M.DPT_ID WHERE EMP_BSU_ID = '" & ddlBSUnit.SelectedItem.Value & "' " & _
        " AND EMP_STATUS = 1) A WHERE 1 =1  "



        If ddlBSUnit.SelectedItem.Value <> "888889" Then
            strQuery += " AND EMP_DES_ID IN (SELECT DES_ID From CUST_SHOWDES)"
        End If


        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String
        Dim txtBusno As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()
            If txtnumber.Trim() <> "" Then
                strQuery &= " and EMPNO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and EMP_NAME like '%" & txtname & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and DES_DESCR like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and DPT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If
            'If txtBusno.Trim() <> "" Then
            '    strQuery &= " and STU_BUS_NO like '%" & txtBusno.Replace(" ", "") & "%' "
            'End If
        End If

        strQuery &= " order by PHOTOAVALIABLE DESC, EMPNO,EMP_NAME,DES_DESCR,ECT_DESCR "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        Dim total = 0
        Dim puploaded = 0
        Dim pnotuploaded = 0
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim PHOTOAVALIABLE = ds.Tables(0).Rows(i).Item("PHOTOAVALIABLE")
            total = total + 1
            If PHOTOAVALIABLE = "True" Then
                puploaded = puploaded + 1
            Else
                pnotuploaded = pnotuploaded + 1
            End If
        Next
        lblmessage.Text = "Total : " & total & " , Uploaded : " & puploaded & " , Not Uploaded : " & pnotuploaded
        Return ds
    End Function

    Public Sub BindStudentView()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String

        If GrdStudents.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

        End If

        Dim ds As DataSet

        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMPNO")
            dt.Columns.Add("EMP_NAME")
            dt.Columns.Add("DES_DESCR")
            dt.Columns.Add("PHOTOAVALIABLE")
            dt.Columns.Add("DPT_DESCR")
            Dim dr As DataRow = dt.NewRow()
            dr("EMP_ID") = ""
            dr("EMPNO") = ""
            dr("PHOTOAVALIABLE") = "false"
            dr("EMP_NAME") = ""
            dr("DPT_DESCR") = ""
            dt.Rows.Add(dr)
            GrdStudents.DataSource = dt
            GrdStudents.DataBind()
            'DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = False
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = False
        Else
            GrdStudents.DataSource = ds
            GrdStudents.DataBind()
            GrdStudents.Columns(5).Visible = False
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))
        End If

        If GrdStudents.Rows.Count > 0 Then
            DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If
    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

        'If e.CommandName = "Deleting" Then
        '    Dim Record_id = e.CommandArgument
        '    Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        '    Dim transaction As SqlTransaction
        '    connection.Open()
        '    transaction = connection.BeginTransaction()
        '    Try
        '        '' Check if item taken for this membership
        '        Dim sqlquery = " SELECT * FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
        '                       " INNER JOIN dbo.LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
        '                       " INNER JOIN dbo.LIBRARY_TRANSACTIONS C ON C.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
        '                       " WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND A.RECORD_ID='" & Record_id & "' " & _
        '                       " AND C.USER_ID=(SELECT USER_ID FROM dbo.LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' AND USER_TYPE='STUDENT') "

        '        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sqlquery)

        '        If ds.Tables(0).Rows.Count > 0 Then
        '            lblMessage.Text = "Library Memebership is in use. <br> " & ds.Tables(0).Rows.Count & " Item(s) has been taken by this user using this membership. Record cannot be deleted.Return the item(s) and proceed with deletion."
        '        Else
        '            Dim query = "DELETE LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' "
        '            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
        '            transaction.Commit()
        '            lblMessage.Text = "User Membership Deleted Successfully."
        '        End If


        '    Catch ex As Exception
        '        lblMessage.Text = "Error while transaction. Error :" & ex.Message
        '        transaction.Rollback()

        '    Finally
        '        connection.Close()

        '    End Try

        '    MO1.Show()

        '    BindStudentView()

        'End If

        If e.CommandName = "barcode" Then
            HiddenShowFlag.Value = 1
            Dim hash As New Hashtable
            Session("CardhashtableAll") = Nothing
            Session("Cardhashtable") = Nothing
            Session("PromoteGrade") = Nothing
            'If Session("Cardhashtable") Is Nothing Then
            'Else
            '    hash = Session("Cardhashtable")
            'End If
            For Each row As GridViewRow In GrdStudents.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("CheckBar1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("HiddensStuid"), HiddenField)
                Dim key = Hid.Value
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, Hid.Value)
                    End If
                Else
                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next

            If hash.Count = 0 Then
                HiddenShowFlag.Value = 0
            End If

            'If DirectCast(GrdStudents.FooterRow.FindControl("CheckIssueAll"), CheckBox).Checked Then

            '    hash.Clear()

            '    Session("CardhashtableAll") = "All"
            '    HiddenShowFlag.Value = 1
            'End If

            'If check_next_accid.Checked Then
            '    Session("PromoteGrade") = True
            'Else
            '    Session("PromoteGrade") = False
            'End If
            Session("Cardhashtable") = hash
        End If
    End Sub

    Protected Sub GrdStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStudents.PageIndexChanging
        GrdStudents.PageIndex = e.NewPageIndex
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuname_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchgrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchsection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchBusNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Private Sub UpLoadPhoto()

        If FUUploadStuPhoto.FileName <> "" Then
            lblmessage.Text = ""
            If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
                ''Throw New Exception
                lblmessage.Text = "Select Image Only"
                Exit Sub
            End If

            If FUUploadStuPhoto.HasFile Then

                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds As New DataSet
                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iImgSize As Integer = 151200 'ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

                    If FUUploadStuPhoto.PostedFile.ContentLength > iImgSize Then
                        lblmessage.Text = "Select Image Size Maximum 20KB"
                        Exit Sub
                    End If
                End If
            End If
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            Dim fs As New FileInfo(FUUploadStuPhoto.PostedFile.FileName)

            If Not Directory.Exists(str_img & "\" & Session("sBSUID") & "\EMPPHOTO\" & ViewState("STUID")) Then
                Directory.CreateDirectory(str_img & "\" & Session("sBSUID") & "\EMPPHOTO\" & ViewState("STUID"))
            Else
                'Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\EMPPHOTO\" & ViewState("STUID"))

                'Dim fi() As System.IO.FileInfo
                'fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                'If fi.Length > 0 Then '' If Having Attachments
                '    For Each f As System.IO.FileInfo In fi
                '        f.Delete()
                '    Next
                'End If
            End If

            Dim str_tempfilename As String = FUUploadStuPhoto.FileName
            Dim strFilepath As String = str_img & "\" & Session("sBSUID") & "\EMPPHOTO\" & ViewState("STUID") & "\" & "EMPPHOTO" & fs.Extension
            ''ImgHeightnWidth(strFilepath)
            FUUploadStuPhoto.PostedFile.SaveAs(strFilepath)
            Try
                If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
                    Throw New Exception
                End If
                imgstuImage.ImageUrl = str_imgvirtual & "/" & Session("sBSUID") & "/EMPPHOTO/" & ViewState("STUID") & "/" & "EMPPHOTO" & fs.Extension
                ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sBSUID") & "\EMPPHOTO\" & ViewState("STUID") & "\" & "EMPPHOTO" & fs.Extension
                ViewState("EMPPHOTOFILEPATH") = "\" & Session("sBSUID") & "\EMPPHOTO\" & ViewState("STUID") & "\" & "EMPPHOTO" & fs.Extension
            Catch ex As Exception
                'File.Delete(strFilepath)
                ' hfParent.Value = ""
                UtilityObj.Errorlog("No Image found")
                imgstuImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                imgstuImage.AlternateText = "No Image found"
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        lblmessage.Text = ""
        Try
            UpLoadPhoto()
            If FUUploadStuPhoto.HasFile Then

                If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
                    ''Throw New Exception
                    lblmessage.Text = "Select Image Only"
                    Exit Sub
                End If
                ''''
                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds As New DataSet
                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iImgSize As Integer = 151200 'ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

                    If FUUploadStuPhoto.PostedFile.ContentLength > iImgSize Then
                        lblmessage.Text = "Select Image Size Maximum 20KB"
                        Exit Sub
                    End If
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        status = UpdateEmployeePhotoInfo(transaction)

                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If
                        lblmessage.Text = "Photo Saved Successfully"
                        BindStudentView()
                    End Using
                End If
            Else
                lblmessage.Text = "Select Image"
                Exit Sub
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblmessage.Text = "Photo Upload Failed"
        End Try
    End Sub

    Function UpdateEmployeePhotoInfo(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STU_ID As String = ViewState("STUID")
            Dim STU_BSU_ID As String = Session("sBSUID")

            Dim PHOTO_PATH As String

            Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            imgstuImage.ImageUrl = str_imgvirtual & ViewState("EMPPHOTOFILEPATH")
            imgstuImage.AlternateText = "No Image found"
            PHOTO_PATH = ViewState("EMPPHOTOFILEPATH")
            'Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID"))
            'Dim fi() As System.IO.FileInfo
            'fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            'If fi.Length > 0 Then '' If Having Attachments
            '    For Each f As System.IO.FileInfo In fi
            '        f.Delete()
            '    Next
            'End If
            'Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
            'FUUploadStuPhoto.PostedFile.SaveAs(strFilepath)
            'File.Copy(ViewState("EMPPHOTOFILEPATHoldPath"), WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & ViewState("EMPPHOTOFILEPATH"), True)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim param(5) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EMP_ID", ViewState("STUID"))
            param(1) = New SqlClient.SqlParameter("@PHOTOPATH", PHOTO_PATH)
            param(2) = New SqlClient.SqlParameter("@DISPLAY_NAME", txtDisplaName.Text)
            param(3) = New SqlClient.SqlParameter("@DISPLAY_DESIGNATION", txtCardDesignaiton.Text)
            param(3) = New SqlClient.SqlParameter("@DISPLAY_DESIGNATION", txtCardDesignaiton.Text)
            param(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(4).Direction = ParameterDirection.ReturnValue


            status = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_EMPLOYEE_PHOTOPATH", param)
            status = param(4).Value
            Return status
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Protected Sub lnkbtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_STU_ID As HiddenField = TryCast(sender.Parent.FindControl("HiddensStuid"), HiddenField)
        Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
        Dim photopath As String = TryCast(sender.Parent.FindControl("HF_Photopath"), HiddenField).Value
        LoadInfo(HF_STU_ID.Value)
        If photopath <> "" Then
            imgstuImage.ImageUrl = str_img + photopath
        Else
            imgstuImage.ImageUrl = str_img + "\NOIMG\no_image.jpg"
        End If

        ViewState("STUID") = HF_STU_ID.Value
        ViewState("popup") = 1
        ModalpopupUpload.Show()
    End Sub

    Public Sub LoadInfo(ByVal EMP_ID As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_sql As String = "SELECT ISNULL(EMP_DISPLAYNAME,'') EMP_DISPLAYNAME, ISNULL(EMP_DISPLAY_DESIGNATION,'') EMP_DISPLAY_DESIGNATION FROM EMPLOYEE_M where emp_id = " & EMP_ID
        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
        While (drReader.Read())
            txtDisplaName.Text = drReader("EMP_DISPLAYNAME").ToString
            txtCardDesignaiton.Text = drReader("EMP_DISPLAY_DESIGNATION").ToString
            'Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            'imgstuImage.ImageUrl = str_imgvirtual & ViewState("EMPPHOTOFILEPATH")

            Exit Sub
        End While

    End Sub


    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        ViewState("popup") = 0
        ModalpopupUpload.Hide()
    End Sub


    Protected Sub ddlBSUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBSUnit.SelectedIndexChanged
        BindStudentView()
    End Sub
End Class
