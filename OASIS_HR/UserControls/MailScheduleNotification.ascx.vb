Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class UserControls_MailScheduleNotification
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select DATEDIFF(second,SCHEDULE_ON_TIME,GETDATE())SEC from COM_NOTIFICATIONS"

        Dim sec As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If sec > 5 Then
            OffAlert.Visible = True
        Else
            OffAlert.Visible = False
        End If

    End Sub
End Class
