<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApplicantProfile.ascx.vb" Inherits="OASIS_HR_UserControls_ApplicantProfile" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
 <link href="../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
<div align="center" class="matters"  >

    <table border="0" cellpadding="3" cellspacing="0" height="366" width="90%">
        <tr>
            <td height="163" width="78%">
                <h1 style="text-align: left">
                    <span lang="EN-US"><font face="Arial" size="5">HR Applicant Profile</font></span></h1>
            </td>
            <td bordercolor="#003366" bordercolordark="#003366" height="163" width="21%" align="right">
                <asp:Image ID="ImgApplicant" runat="server" Height="160px" Width="128px" /></td>
        </tr>
        <tr>
            <td bordercolor="#003366" colspan="2" height="24">
                <p align="right">
                    <font face="Arial">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 23px; height: 23px" >
                        <asp:ImageButton ID="ImagePrintCv" runat="server" ImageUrl="~/Images/print.gif" ToolTip="Print CV" />
                      
                        <ajaxToolkit:ConfirmButtonExtender ID="C1" ConfirmText="Do you wish to print Cv ?" TargetControlID="ImagePrintCv" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                                <td style="height: 23px" >
                                    <asp:ImageButton
                            ID="ImagePrint" runat="server" ImageUrl="~/Images/print.gif" OnClientClick="javascript:window.print(); return false;"
                            ToolTip="Print Page" />
                          
                            <ajaxToolkit:ConfirmButtonExtender ID="C2" ConfirmText="Do you wish to print Profile ?" TargetControlID="ImagePrint" runat="server"></ajaxToolkit:ConfirmButtonExtender>

                            </td>
                                <td style="height: 23px" >
                        <ajaxToolkit:Rating ID="CRatings" runat="server" CurrentRating='<%# Eval("RATINGS") %>'
                            EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar" MaxRating="5"
                            ReadOnly="True" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
                        </ajaxToolkit:Rating>
                                </td>
                            </tr>
                        </table>
                        &nbsp;&nbsp;
                        </font>
                </p>
            </td>
        </tr>
        <tr>
            <td bgcolor="#336699" bordercolor="#003366" colspan="2" height="35">
                <p align="center" >
                    <b><font color="#ffffff" face="Arial" size="4">
                        <asp:Label ID="lbltitle" runat="server"></asp:Label>
                        <asp:Label ID="lblname" runat="server"></asp:Label></font></b></p>
            </td>
        </tr>
        <tr>
            <td bordercolor="#003366" colspan="2" height="46">
                <p align="left">
                    <font face="Arial"><table width="100%">
                        <tr>
                            <td bgcolor="#336699">
                              <font color="#ffffff" face="Arial" style="font-size: 11pt"><b>  <asp:Label ID="lblNotes" runat="server" Text="Notes"></asp:Label></b></font></td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                                    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="100%">

                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="T5Notes" runat="server" Height="120px" SkinID="MultiText1" TextMode="MultiLine"
                                                                ValidationGroup="T5notes" Width="800px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="3">
                                                            <asp:Button ID="T5btnnotessave" runat="server" CssClass="button" Text="Save" ValidationGroup="T5notes" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="lblNotes" Collapsed="true"
                                    CollapsedSize="0" CollapsedText="Notes" ExpandControlID="lblNotes" ExpandedSize="180"
                                    ExpandedText="Hide" ScrollContents="false" TargetControlID="T5Panel1" TextLabelID="lblNotes">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </td>
                        </tr>
                        <tr>
                            <td id="T1" runat="server">
                            </td>
                        </tr>
                    </table>
                    </font></p>
            </td>
        </tr>
        <tr>
            <td bordercolor="#003366" colspan="2" valign="top">
                <div align="left">
                    <table border="1" bordercolor="#003366" cellspacing="0" width="100%">
                        <tr>
                            <td bgcolor="#336699" colspan="7">
                                <font color="#ffffff" face="Arial" style="font-size: 11pt"><b>Application Information</b></font></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2"  width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Application No</td>
                            <td colspan="2"  width="30%">
                                &nbsp;<asp:Label ID="lblApplicationNo" runat="server"></asp:Label></td>
                            <td width="15%" >
                                <font face="Arial" style="font-size: 11pt"></font>Date Applied</td>
                            <td colspan="2" width="25%" >
                                &nbsp;<asp:Label ID="lblentrydate" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Email Id</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblemail" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Mobile Number</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblmobile" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Category Applied for</td>
                            <td colspan="5" width="71%">
                                &nbsp;<asp:Label ID="lblcategories" runat="server"></asp:Label><%--<asp:GridView ID="GrdCategory" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                    EmptyDataText="Information not added" ShowHeader="False" Width="100%">
                                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Category Applied">
                                            <ItemTemplate>
                                                <center>
                                                    <%# Eval("CATEGORY_DES")%>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>--%>
                            </td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Schools Interested in working</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblschoolsinterested" runat="server"></asp:Label>
                            </td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Preferred Countries</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lbPrefferedCountries" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Available to Join From</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblavailabledate" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Introduced by</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblKnownFrom" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#336699" colspan="7" width="99%">
                                <b><font color="#ffffff" face="Arial" style="font-size: 11pt">Other Details</font></b></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Date of Birth</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lbldob" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Age</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblAge" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Nationality</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblcountryofresidence" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Gender</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblGender" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Marital Status</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblmaritalstatus" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Number of Children</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblnoofchildren" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td bgcolor="#336699" colspan="7" width="99%">
                                <b><font color="#ffffff" face="Arial" style="font-size: 11pt">Passport Details</font></b></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Name as in Passport</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblPassportName" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Passport Number</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblPassportNumber" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Issue Country</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblcountryissuepassport" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Issue Place</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblPassportIssuePlace" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Issue Date</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblPassportIssueDate" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Expiry Date</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblPassportExpiryDate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td bgcolor="#336699" colspan="7" width="99%">
                                <b><font color="#ffffff" face="Arial" style="font-size: 11pt">Visa Details</font></b></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Visa Number</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblVisaNumber" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Issue Date</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblVisaIssueDate" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Issue Place</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblVisaIssuePlace" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Expiry Date</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblVisaExpirtyDate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td bgcolor="#336699" colspan="7" width="99%">
                                <font color="#ffffff" face="Arial" style="font-size: 11pt"><b>Work Experience</b></font></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Qualified Teacher</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblqualified" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Year Qualified</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblqualifiedyear" runat="server"></asp:Label></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="2" width="27%">
                                <font face="Arial" style="font-size: 11pt"></font>Number of Years of Experience</td>
                            <td colspan="2" width="30%">
                                &nbsp;<asp:Label ID="lblexperience" runat="server"></asp:Label></td>
                            <td width="15%">
                                <font face="Arial" style="font-size: 11pt"></font>Subjects</td>
                            <td colspan="2" width="25%">
                                &nbsp;<asp:Label ID="lblSubjects" runat="server"></asp:Label>
                            </td>
                        </tr>

                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="7" rowspan="2">
                            <center>
                                <asp:GridView ID="GrdWorkExp" runat="server" AutoGenerateColumns="false" EmptyDataText="Information not provided."
                                    Width="100%">
                                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                        <span style="font-size: smaller">Name</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                           <center><%#Eval("COMPANY_SCHOOL_NAME")%></center>  
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                        <HeaderTemplate>
                                        <span style="font-size: smaller">Address</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                             <center><%#Eval("ADDRESS")%></center> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Po Box">
                                        <HeaderTemplate>
                                        <span style="font-size: smaller">Po Box</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                              <center><%#Eval("PO_BOX")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country">
                                         <HeaderTemplate>
                                        <span style="font-size: smaller">Country</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                               <center><%#Eval("Country")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="School">
                                         <HeaderTemplate>
                                        <span style="font-size: smaller">School</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                               <center><%#Eval("IS_SCHOOL")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="School Type">
                                         <HeaderTemplate>
                                        <span style="font-size: smaller">School Type</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                               <center><%#Eval("SCHOOL_TYPE")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Curriculum">
                                         <HeaderTemplate>
                                        <span style="font-size: smaller">Curriculum</span>
                                        </HeaderTemplate>
                                            <ItemTemplate>
                                               <center><%#Eval("CLM_INFO")%></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>
                                </center>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                        <tr style="page-break-before: always">
                            <td bgcolor="#336699" colspan="7" width="96%">
                                <font face="Arial"><b><font color="#ffffff" face="Arial" style="font-size: 11pt">References</font></b></font></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td colspan="7" rowspan="3">
                                <font face="Arial" size="2">
                                <center>
                                    <asp:GridView ID="Grdreference" runat="server" AutoGenerateColumns="false" EmptyDataText="Information not provided."
                                        Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                 <HeaderTemplate>
                                                <span style="font-size: smaller">Name</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("NAME")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company">
                                               <HeaderTemplate>
                                                <span style="font-size: smaller">Company</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("COMPANY_NAME")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job Title">
                                                <HeaderTemplate>
                                                <span style="font-size: smaller">Job Title</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("POSITION")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address">
                                                <HeaderTemplate>
                                                <span style="font-size: smaller">Address</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("ADDRESS")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Contact Number">
                                                <HeaderTemplate>
                                                <span style="font-size: smaller">Contact Number</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("TELEPHONE")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <HeaderTemplate>
                                                <span style="font-size: smaller">Email</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("EMAIL")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Years Known">
                                                <HeaderTemplate>
                                                <span style="font-size: smaller">Years Known</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("DAYS_WORKED")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Request">
                                                <HeaderTemplate>
                                                <span style="font-size: smaller">Request</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("REQUEST_REFERENCE")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                    </asp:GridView>
                                    </center>
                                </font>
                            </td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td bgcolor="#336699" colspan="7" width="97%">
                                <font face="Arial"><b><font color="#ffffff" face="Arial" style="font-size: 11pt">Education/
                                    Qualifications</font></b></font></td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                            <td align="left" colspan="7" rowspan="2">
                                <font face="Arial" size="2">
                                <center>
                                    <asp:GridView ID="GrdEducation" runat="server" AutoGenerateColumns="false" EmptyDataText="Information not provided."
                                        Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Qualification">
                                              <HeaderTemplate>
                                                <span style="font-size: smaller">Qualification</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("QUALIFICATION")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Institution">
                                              <HeaderTemplate>
                                                <span style="font-size: smaller">Institution</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("INSTITUTE")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Specialization">
                                             <HeaderTemplate>
                                                <span style="font-size: smaller">Specialization</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("SPECILIZATION")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Year">
                                             <HeaderTemplate>
                                                <span style="font-size: smaller">Year</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("YEAR")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Result">
                                               <HeaderTemplate>
                                                <span style="font-size: smaller">Result</span>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("RESULT")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                    </asp:GridView>
                                    </center>
                                </font>
                            </td>
                        </tr>
                        <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        </tr>
                    </table>
               
                <table border="1" bordercolor="#003366" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td bgcolor="#336699" colspan="4">
                            <font face="Arial"><b><font color="#ffffff" face="Arial" style="font-size: 11pt">Communication
                                Details</font></b></font></td>
                    </tr>
                    <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        <td width="27%">
                            <font face="Arial" style="font-size: 11pt"></font>Address</td>
                        <td width="30%">
                            &nbsp;<asp:Label ID="lbladdress" runat="server"></asp:Label></td>
                        <td width="14%">
                            <font face="Arial" style="font-size: 11pt"></font>PoB</td>
                        <td width="26%">
                            &nbsp;<asp:Label ID="lblpostalcode" runat="server"></asp:Label></td>
                    </tr>
                    <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        <td width="27%">
                            <font face="Arial" style="font-size: 11pt"></font>City/State</td>
                        <td width="30%">
                            &nbsp;<asp:Label ID="lbltown" runat="server"></asp:Label></td>
                        <td width="14%">
                            <font face="Arial" style="font-size: 11pt"></font>Country</td>
                        <td width="26%">
                            &nbsp;<asp:Label ID="lblCountry" runat="server"></asp:Label></td>
                    </tr>
                    <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        <td width="27%">
                            <font face="Arial" style="font-size: 11pt"></font>Home Telephone</td>
                        <td width="30%">
                            &nbsp;<asp:Label ID="lblhomephone" runat="server"></asp:Label></td>
                        <td width="14%">
                            <font face="Arial" style="font-size: 11pt"></font>Work Telephone</td>
                        <td width="26%">
                            &nbsp;<asp:Label ID="lblofficephone" runat="server"></asp:Label></td>
                    </tr>
                </table>
               </div>
            </td>
        </tr>
        <tr>
            <td bordercolor="#003366" colspan="2" valign="top">
                <table border="1" bordercolor="#003366" cellspacing="0" height="120" width="100%">
                    <tr>
                        <td bgcolor="#336699" height="24">
                            <font face="Arial"><b><font color="#ffffff" style="font-size: 11pt">Additional Information
                                provided by the candidate</font></b></font></td>
                    </tr>
                    <tr bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
                        <td>
                            &nbsp;<asp:Label ID="lbladditionalinfo" runat="server"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<asp:HiddenField ID="HiddenCvPath" runat="server" /><asp:HiddenField ID="HiddenApplicationNo" runat="server" />
