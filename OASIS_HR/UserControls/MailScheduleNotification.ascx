<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MailScheduleNotification.ascx.vb"
    Inherits="UserControls_MailScheduleNotification" %>
<div id="OffAlert" class="matters" runat="server" >
<center>
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Messaging Alert</td>
                        </tr>
                        <tr>
                            <td >

 <img src="../../Images/alertRed.gif" height="25px" alt="Mail Schedule Off" />&nbsp;<br />
    Messaging System is offline<br />
    Emails and SMS will not be delivered at this time.Please contact the administrator.
    <br />
    <br />
    You have the following Options<br />
    1. You may proceed or schedule with the messaging task, the message will be delivered
    when the system is online.<br />
    2. Please try again later.



 </td>
                    </tr>
               </table>
   </center>
</div>
