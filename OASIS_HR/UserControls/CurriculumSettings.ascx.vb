Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class OASIS_HR_UserControls_CurriculumSettings
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindControls()
        End If

    End Sub
    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CURRICULUM", pParms)
        If ds.Tables(0).Rows.Count > 0 Then
            GrdClm.DataSource = ds
            GrdClm.DataBind()
        Else
            BindEmpty(GrdClm)

        End If


    End Sub
    Public Sub BindEmpty(ByVal Grid As GridView)
        Dim dt As New DataTable
        dt.Columns.Add("CLM_ID")
        dt.Columns.Add("RECORD_ID")
        dt.Columns.Add("CURRICULUM")
        Dim dr As DataRow = dt.NewRow()
        dr.Item("CLM_ID") = ""
        dr.Item("RECORD_ID") = ""
        dr.Item("CURRICULUM") = ""
        dt.Rows.Add(dr)
        Grid.DataSource = dt
        Grid.DataBind()
        DirectCast(Grid.Rows(0).FindControl("LinkPEdit"), LinkButton).Visible = False
        DirectCast(Grid.Rows(0).FindControl("LinkDelete"), LinkButton).Visible = False
    End Sub
    Protected Sub btnsubjectsave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim val As String = DirectCast(GrdClm.FooterRow.FindControl("txtitem"), TextBox).Text.Trim()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CURRICULUM", val)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
        If val.Trim() <> "" Then
            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CURRICULUM", pParms)
        End If
        BindControls()
    End Sub

    Protected Sub GrdClm_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdClm.RowEditing
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim record_id As String = GrdClm.DataKeys(e.NewEditIndex).Value.ToString()
        Dim val = DirectCast(GrdClm.Rows(e.NewEditIndex).FindControl("txtvalue"), TextBox).Text.Trim()
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@CURRICULUM", val)
        pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", record_id)
        pParms(2) = New SqlClient.SqlParameter("@OPTION", 2)
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CURRICULUM", pParms)
        BindControls()
    End Sub

    Protected Sub GrdClm_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdClm.RowDeleting
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim record_id = GrdClm.DataKeys(e.RowIndex).Value.ToString()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@RECORD_ID", record_id)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 3)
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_CURRICULUM", pParms)
        BindControls()
    End Sub
End Class
