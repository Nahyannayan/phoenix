<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CR_Filter.ascx.vb" Inherits="OASIS_HR_UserControls_CR_Filter" %>

<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<div>
    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton>

    <asp:Panel ID="Panel1" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Search</td>
            </tr>
            <tr>
                <td align="left">
                    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="HT1" runat="server">
                            <ContentTemplate>
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="10%">
                                                <span class="field-label">Application No</span></td>

                                            <td align="left" width="20%">
                                                <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox></td>
                                            <td align="left" width="10%">
                                                <span class="field-label">First Name</span>
                                            </td>

                                            <td align="left" width="25%">
                                                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                                            <td align="left" width="10%">
                                                <span class="field-label">Last Name</span></td>

                                            <td align="left" width="25%">
                                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10%">
                                                <span class="field-label">Gender</span></td>

                                            <td align="left" width="20%">
                                                <asp:DropDownList ID="ddGender" runat="server">
                                                    <asp:ListItem Value="-1">Gender</asp:ListItem>
                                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td align="left" width="10%">
                                                <span class="field-label">Age</span></td>

                                            <td align="left" width="25%">
                                                <asp:DropDownList ID="ddAgeFrom" runat="server">
                                                </asp:DropDownList><asp:DropDownList ID="ddAgeTo" runat="server">
                                                </asp:DropDownList></td>
                                            <td align="left" width="10%">
                                                <span class="field-label">Marital Status</span>
                                            </td>

                                            <td align="left" width="25%">
                                                <asp:DropDownList ID="ddMaritalStatus" runat="server">
                                                    <asp:ListItem Value="-1">Marital Status</asp:ListItem>
                                                    <asp:ListItem Value="Married">Married</asp:ListItem>
                                                    <asp:ListItem Value="Single">Single</asp:ListItem>
                                                    <asp:ListItem Value="Partner">Partner</asp:ListItem>
                                                    <asp:ListItem Value="Widow">Widow</asp:ListItem>
                                                    <asp:ListItem Value="Widower">Widower</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10%">
                                                <span class="field-label">Postal Code</span></td>

                                            <td align="left" width="20%">
                                                <asp:TextBox ID="txtPostalCode" runat="server"></asp:TextBox></td>
                                            <td align="left" width="10%">
                                                <span class="field-label">Home Phone</span>
                                            </td>

                                            <td align="left" width="25%">
                                                <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox></td>
                                            <td align="left" width="10%">
                                                <span class="field-label">Mobile</span>
                                            </td>

                                            <td align="left" width="25%">
                                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10%"><span class="field-label">Email</span></td>

                                            <td align="left" width="20%">
                                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                                            <td align="left" width="10%"><span class="field-label">Available On</span></td>

                                            <td align="left" width="25%">
                                                <asp:DropDownList ID="ddAvailableMonth" runat="server">
                                                </asp:DropDownList><asp:DropDownList ID="ddAvailableYear" runat="server">
                                                </asp:DropDownList></td>
                                            <td align="left" width="10%"><span class="field-label">Nationality</span></td>

                                            <td align="left" width="25%">
                                                <asp:DropDownList ID="ddNationality" runat="server">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10%"><span class="field-label">Qualified</span>
                                            </td>

                                            <td align="left" width="20%">
                                                <asp:DropDownList ID="ddQualified" runat="server">
                                                    <asp:ListItem Selected="True" Value="-1">Qualified Teacher?</asp:ListItem>
                                                    <asp:ListItem Value="0">Yes</asp:ListItem>
                                                    <asp:ListItem Value="1">No</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td align="left" width="10%"><span class="field-label">Experience</span></td>

                                            <td align="left" width="25%">
                                                <asp:DropDownList ID="ddExpFrom" runat="server">
                                                </asp:DropDownList><asp:DropDownList ID="ddExpTo" runat="server">
                                                </asp:DropDownList></td>
                                            <td align="left" width="10%"><span class="field-label">Last Contact Date</span></td>

                                            <td align="left" width="25%">
                                                <asp:TextBox ID="txtLastContactDate" runat="server" Width="85px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="10%"><span class="field-label">Applied On</span></td>

                                            <td align="left" width="20%">
                                                <asp:TextBox ID="txtAppliedFrom" runat="server"></asp:TextBox>

                                            </td>
                                            <td align="left" width="10%"><span class="field-label">To</span></td>
                                            <td align="left" width="25%">
                                                <asp:TextBox ID="txtAppliedTo" runat="server"></asp:TextBox></td>
                                            <td align="left" width="10%"></td>
                                            <td align="left" width="25%"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6"></td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                        TargetControlID="txtLastContactDate">
                                    </ajaxToolkit:CalendarExtender>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                        TargetControlID="txtAppliedFrom">
                                    </ajaxToolkit:CalendarExtender>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                        TargetControlID="txtAppliedTo">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Basic Search
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT2" runat="server">
                            <ContentTemplate>
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Curriculum</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddCurriculum" runat="server">
                                                </asp:DropDownList></td>
                                            <td align="left" width="20%"><span class="field-label">Stage</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddApplicantStage" runat="server">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Subject</span></td>

                                            <td align="left" width="30%">
                                                <div class="checkbox-list">
                                                    <asp:Panel ID="P1" runat="server">
                                                        <asp:CheckBoxList ID="listSubjects" runat="server">
                                                        </asp:CheckBoxList>
                                                    </asp:Panel>
                                                </div>
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Applied For</span></td>

                                            <td align="left" width="30%">
                                                <div class="checkbox-list">
                                                    <asp:Panel ID="P2" runat="server">
                                                        <asp:CheckBoxList ID="listAppliedFor" runat="server">
                                                        </asp:CheckBoxList>
                                                    </asp:Panel>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                More Search Options
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                        Text="Search" /><asp:Button ID="btnReset" runat="server" CausesValidation="False"
                            CssClass="button" Text="Reset" /></td>
            </tr>
        </table>
        &nbsp;&nbsp;
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="False" CollapsedSize="0" CollapsedText="Advance Search" ExpandControlID="LinkAdvanceSearch"
        ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>
</div>
