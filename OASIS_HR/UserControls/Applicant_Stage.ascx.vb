Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports Oasis_Administrator
Partial Class OASIS_HR_UserControls_Applicant_Stage
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CheckMenuRights()
            BindSearchControls()
        End If

        If GrdStage.Rows.Count > 0 Then
            Dim pageindex = GrdStage.PageIndex
            GrdStage.PageIndex = pageindex
            Search()
        End If

    End Sub

    Public Sub BindSearchControls()

        txtApplicationNo.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtPostalCode.Text = ""
        txtHomePhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtLastContactDate.Text = ""
        txtAppliedFrom.Text = ""
        txtAppliedTo.Text = ""
        ddGender.SelectedIndex = 0
        ddMaritalStatus.SelectedIndex = 0
        ddQualified.SelectedIndex = 0


        ddExpFrom.Items.Clear()
        ddExpTo.Items.Clear()
        ddAgeFrom.Items.Clear()
        ddAgeTo.Items.Clear()

        Dim i = 0
        For i = 0 To 40
            ddExpFrom.Items.Add(i)
            ddExpTo.Items.Add(i)
        Next
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "--"
        ddExpFrom.Items.Insert(0, list1)
        ddExpTo.Items.Insert(0, list1)

        For i = 19 To 60
            ddAgeFrom.Items.Add(i)
            ddAgeTo.Items.Add(i)
        Next
        ddAgeFrom.Items.Insert(0, list1)
        ddAgeTo.Items.Insert(0, list1)

        ddNationality.DataSource = OasisAdministrator.Nationality()
        ddNationality.DataTextField = "CTY_NATIONALITY"
        ddNationality.DataValueField = "CTY_ID"
        ddNationality.DataBind()
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Select Country"
        ddNationality.Items.Insert(0, list2)

        listAppliedFor.DataSource = OasisAdministrator.GetCategory()
        listAppliedFor.DataTextField = "CATEGORY_DES"
        listAppliedFor.DataValueField = "CATEGORY_ID"
        listAppliedFor.DataBind()



        ddApplicantStage.DataSource = OasisAdministrator.Get_Stages()
        ddApplicantStage.DataTextField = "STAGE_DESCRIPTION"
        ddApplicantStage.DataValueField = "STAGE_ID"
        ddApplicantStage.DataBind()
        Dim list6 As New ListItem
        list6.Value = "-1"
        list6.Text = "Applicant Stage"
        ddApplicantStage.Items.Insert(0, list6)


        listSubjects.DataSource = OasisAdministrator.GetSubject()
        listSubjects.DataTextField = "SUBJECT"
        listSubjects.DataValueField = "SUBJECT_ID"
        listSubjects.DataBind()

        ddCurriculum.DataSource = OasisAdministrator.GetClm()
        ddCurriculum.DataTextField = "CURRICULUM"
        ddCurriculum.DataValueField = "CLM_ID"
        ddCurriculum.DataBind()
        Dim list4 As New ListItem
        list4.Value = "-1"
        list4.Text = "Select Curriculum"
        ddCurriculum.Items.Insert(0, list4)

        BindAvailabledate()

    End Sub

    Public Sub BindAvailabledate()
        ddAvailableMonth.Items.Clear()
        ddAvailableYear.Items.Clear()

        ''Month
        ddAvailableMonth.Items.Add("Jan")
        ddAvailableMonth.Items.Add("Feb")
        ddAvailableMonth.Items.Add("Mar")
        ddAvailableMonth.Items.Add("Apr")
        ddAvailableMonth.Items.Add("May")
        ddAvailableMonth.Items.Add("Jun")
        ddAvailableMonth.Items.Add("Jul")
        ddAvailableMonth.Items.Add("Aug")
        ddAvailableMonth.Items.Add("Sep")
        ddAvailableMonth.Items.Add("Oct")
        ddAvailableMonth.Items.Add("Nov")
        ddAvailableMonth.Items.Add("Dec")
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "Month"
        ddAvailableMonth.Items.Insert(0, list1)

        ''Year
        Dim i = Today.Year
        For i = Today.Year To Today.Year + 5
            ddAvailableYear.Items.Add(i)
        Next
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Year"
        ddAvailableYear.Items.Insert(0, list2)

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub

    Public Sub Evaluate(ByVal ds As DataSet)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim srt_query = ""
        srt_query = "SELECT STAGE_ID,STAGE_DESCRIPTION FROM STAGE_MASTER WHERE PARENT_STAGE='0' ORDER BY DISPLAY_ORDER "
        Dim dstage As DataSet
        dstage = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        Dim i = 0
        For i = 0 To dstage.Tables(0).Rows.Count - 1
            ds.Tables(0).Columns.Add(dstage.Tables(0).Rows(i).Item("STAGE_DESCRIPTION").ToString())
        Next

        GrdStage.DataSource = ds
        GrdStage.DataBind()

        Dim j = 0

        For Each row As GridViewRow In GrdStage.Rows

            For i = (ds.Tables(0).Columns.Count - dstage.Tables(0).Columns.Count) - 2 To ds.Tables(0).Columns.Count - 1
                Dim image As New ImageButton
                image.ID = "image" & GrdStage.HeaderRow.Cells(i).Text
                image.ImageUrl = "../../Images/Oasis_Hr/Images/cross.png"
                image.CommandName = GrdStage.HeaderRow.Cells(i).Text
                image.CommandArgument = row.Cells(0).Text & "," & j
                row.Cells(i).Controls.Add(image)

                Dim ParentStageHidden As New HiddenField
                ParentStageHidden.ID = "HiddenP" & GrdStage.HeaderRow.Cells(i).Text
                row.Cells(i).Controls.Add(ParentStageHidden)

                Dim SubStageHidden As New HiddenField
                SubStageHidden.ID = "HiddenS" & GrdStage.HeaderRow.Cells(i).Text
                row.Cells(i).Controls.Add(SubStageHidden)
                row.Cells(i).Style("text-align") = "center"
            Next
            j = j + 1

        Next
        For Each row As GridViewRow In GrdStage.Rows
            For i = (ds.Tables(0).Columns.Count - dstage.Tables(0).Columns.Count) - 2 To ds.Tables(0).Columns.Count - 1
                Dim stage As String = ""
                stage = GrdStage.HeaderRow.Cells(i).Text
                Dim ParentStage As String = ""
                str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
                srt_query = "SELECT STAGE_ID FROM STAGE_MASTER WHERE STAGE_DESCRIPTION='" & stage & "'"
                ParentStage = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, srt_query)
                DirectCast(row.FindControl("HiddenP" & GrdStage.HeaderRow.Cells(i).Text), HiddenField).Value = ParentStage


                srt_query = "SELECT TRAN_SUB_STAGE FROM TRANSACTIONS WHERE TRAN_PARENT_STAGE='" & ParentStage & "' AND APPLICATION_NO='" & row.Cells(0).Text & "'" & _
                            " AND STAGE_FLAG='0' AND TRANSACTION_ID= (SELECT MAX(TRANSACTION_ID) FROM  TRANSACTIONS WHERE TRAN_PARENT_STAGE='" & ParentStage & "' AND APPLICATION_NO='" & row.Cells(0).Text & "')"

                Dim sds As DataSet
                sds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)

                If sds.Tables(0).Rows.Count > 0 Then
                    DirectCast(row.FindControl("HiddenS" & GrdStage.HeaderRow.Cells(i).Text), HiddenField).Value = sds.Tables(0).Rows(0).Item("TRAN_SUB_STAGE").ToString()
                Else
                    DirectCast(row.FindControl("HiddenS" & GrdStage.HeaderRow.Cells(i).Text), HiddenField).Value = 0
                End If

                srt_query = "SELECT STAGE_IMAGE_URL FROM STAGE_MASTER WHERE PARENT_STAGE='" & ParentStage & "' AND STAGE_ID='" & DirectCast(row.FindControl("HiddenS" & GrdStage.HeaderRow.Cells(i).Text), HiddenField).Value & "'"

                Dim path = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, srt_query)
                If path = "" Then
                    path = "../../Images/Oasis_Hr/Images/cross.png"
                End If
                DirectCast(row.FindControl("image" & GrdStage.HeaderRow.Cells(i).Text), ImageButton).ImageUrl = path

                srt_query = "SELECT BLOCK_PREVIOUS FROM STAGE_MASTER WHERE PARENT_STAGE='" & ParentStage & "' AND STAGE_ID='" & DirectCast(row.FindControl("HiddenS" & GrdStage.HeaderRow.Cells(i).Text), HiddenField).Value & "'"

                Dim blockval = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, srt_query)

                If blockval = "1" Then
                    DirectCast(row.FindControl("image" & GrdStage.HeaderRow.Cells(i - 1).Text), ImageButton).Enabled = False
                ElseIf blockval = "2" Then
                    DirectCast(row.FindControl("image" & GrdStage.HeaderRow.Cells(i - 1).Text), ImageButton).Enabled = False
                    DirectCast(row.FindControl("image" & GrdStage.HeaderRow.Cells(i).Text), ImageButton).Enabled = False
                End If

            Next

        Next


    End Sub


    Protected Sub GrdStage_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStage.RowCommand

        If e.CommandName <> "Page" Then
            Dim EncDec As New Encryption64
            Response.Write(e.CommandArgument)
            Dim val As String = e.CommandArgument
            Dim split As String() = val.Split(",")
            Dim Application_No As String = split(0)
            Dim PStage As String = DirectCast(GrdStage.Rows(split(1)).FindControl("HiddenP" & e.CommandName), HiddenField).Value
            Dim Sstage As String = DirectCast(GrdStage.Rows(split(1)).FindControl("HiddenS" & e.CommandName), HiddenField).Value
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
            Response.Redirect("Stage_Remarks.aspx?Stage=" & EncDec.Encrypt(PStage) & "&Stage_Value=" & EncDec.Encrypt(Sstage) & "&Application_No=" & EncDec.Encrypt(Application_No) & mInfo)
        End If

    End Sub

    Protected Sub GrdStage_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

        GrdStage.PageIndex = e.NewPageIndex
        Search()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub

    Public Sub Search()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim str_query = " SELECT DISTINCT A.APPLICATION_NO as AppNo ,(ISNULL(A.FIRST_NAME,'') + ' ' + ISNULL(A.SUR_NAME,'')) Name  " & _
                        " FROM APPLICATION_MASTER A WHERE A.DELETED IS NULL AND A.ACTIVE='TRUE' "

        Dim condition As String = ""

        If txtApplicationNo.Text <> "" Then
            condition = condition & " AND A.APPLICATION_NO LIKE '%" & txtApplicationNo.Text.Trim() & "%'"
        End If

        If txtFirstName.Text <> "" Then
            condition = condition & " AND A.FIRST_NAME  LIKE '%" & txtFirstName.Text.Trim() & "%'"
        End If

        If txtLastName.Text <> "" Then
            condition = condition & " AND A.SUR_NAME LIKE '%" & txtLastName.Text.Trim() & "%'"
        End If

        If ddGender.SelectedIndex > 0 Then
            condition = condition & " AND A.GENDER LIKE '%" & ddGender.SelectedValue & "%'"
        End If

        If ddAgeFrom.SelectedIndex > 0 Then
            condition = condition & " AND DATEDIFF(yy,A.DATE_OF_BIRTH,GETDATE()) >=" & ddAgeFrom.SelectedValue & ""
        End If

        If ddAgeTo.SelectedIndex > 0 Then
            condition = condition & " AND DATEDIFF(yy,A.DATE_OF_BIRTH,GETDATE()) <=" & ddAgeTo.SelectedValue & ""
        End If

        If ddMaritalStatus.SelectedIndex > 0 Then
            condition = condition & " AND A.MARITAL_STATUS ='" & ddMaritalStatus.SelectedValue & "'"
        End If

        If txtPostalCode.Text <> "" Then
            condition = condition & " AND A.POSTAL_CODE LIKE '%" & txtPostalCode.Text.Trim() & "%'"
        End If

        If txtHomePhone.Text <> "" Then
            condition = condition & " AND A.HOME_PHONE LIKE '%" & txtHomePhone.Text.Trim() & "%'"
        End If

        If txtMobile.Text <> "" Then
            condition = condition & " AND A.MOBILE LIKE '%" & txtMobile.Text.Trim() & "%'"
        End If

        If txtEmail.Text <> "" Then
            condition = condition & " AND A.EMAIL LIKE '%" & txtEmail.Text.Trim() & "%'"
        End If

        If ddAvailableMonth.SelectedIndex > 0 Then
            condition = condition & " AND A.AVAILABLE_DATE LIKE '%" & ddAvailableMonth.SelectedValue & "%'"
        End If

        If ddAvailableYear.SelectedIndex > 0 Then
            condition = condition & " AND A.AVAILABLE_DATE LIKE '%" & ddAvailableYear.SelectedValue & "%'"
        End If

        If ddNationality.SelectedIndex > 0 Then
            condition = condition & " AND A.COUNTRY_OF_RESIDENCE =" & ddNationality.SelectedValue & ""
        End If

        If ddQualified.SelectedIndex > 0 Then
            condition = condition & " AND A.QUALIFIED_TEACHER =" & ddQualified.SelectedValue & ""
        End If

        If ddExpFrom.SelectedIndex > 0 Then
            condition = condition & " AND A.EXPERIENCE >=" & ddExpFrom.SelectedValue & ""
        End If

        If ddExpTo.SelectedIndex > 0 Then
            condition = condition & " AND A.EXPERIENCE <=" & ddExpTo.SelectedValue & ""
        End If

        If txtLastContactDate.Text <> "" Then
            condition = condition & " AND A.LAST_CONTACT_DATE LIKE '%" & txtLastContactDate.Text.Trim() & "%'"
        End If

        If txtAppliedFrom.Text <> "" Then
            condition = condition & " AND A.ENTRY_DATE >= '" & txtAppliedFrom.Text.Trim() & "'"
        End If

        If txtAppliedTo.Text <> "" Then
            condition = condition & " AND A.ENTRY_DATE <= '" & DateAdd(DateInterval.Day, 1, Convert.ToDateTime(txtAppliedTo.Text.Trim())) & "'"
        End If

        If ddApplicantStage.SelectedIndex > 0 Then
            condition = condition & " AND A.CURRENT_STAGE =" & ddApplicantStage.SelectedValue & ""
        End If

        If ddCurriculum.SelectedIndex > 0 Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  APPLICATION_NO FROM APPLICATION_WORK_EXPERIENCE WHERE CLM_INFO LIKE '%" & ddCurriculum.SelectedItem.Text.Trim() & "%')"
        End If


        Dim subjectsids As String = ""

        For Each item As ListItem In listSubjects.Items
            If item.Selected Then
                If subjectsids = "" Then
                    subjectsids = "'" & item.Value & "'"
                Else
                    subjectsids = subjectsids & ",'" & item.Value & "'"
                End If

            End If
        Next

        If subjectsids <> "" Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  DISTINCT APPLICATION_NO FROM APPLICATION_SUBJECT WHERE SUBJECT_ID IN (" & subjectsids & ") )"
        End If


        Dim Appliedfor As String = ""

        For Each item As ListItem In listAppliedFor.Items
            If item.Selected Then
                If Appliedfor = "" Then
                    Appliedfor = "'" & item.Value & "'"
                Else
                    Appliedfor = Appliedfor & ",'" & item.Value & "'"
                End If

            End If
        Next

        If Appliedfor <> "" Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  DISTINCT APPLICATION_NO FROM APPLICANTION_APPLIED_CATEGORY WHERE CATEGORY_ID IN (" & Appliedfor & ") )"
        End If


        If condition <> "" Then
            str_query = str_query & condition
        End If

        str_query = str_query & " ORDER BY A.APPLICATION_NO DESC "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Evaluate(ds)

    End Sub

End Class
