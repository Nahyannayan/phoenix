<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EmployeeCardPrint_GCO.ascx.vb"
    Inherits="Students_UserControls_StudIdcardprint_GCO" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<script type="text/javascript">
    function change_chk_stateg1(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;

            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar1") != -1) {
                //                           alert(document.forms[0].elements[i].disabled)                    
                if (document.forms[0].elements[i].disabled) {


                } else {
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                    //alert()
                }

            }
        }
    }
    function change_chk_stateNextacd(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chbxNextacd") != -1) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }
</script>
<div class="matters">
    <table width="100%">
        <tr align="left">
            <td width="20%">
                <span class="field-label">Business Unit  </span>
            </td>
            <td width="30%">
                <asp:DropDownList ID="ddlBSUnit" AutoPostBack="true" runat="server"></asp:DropDownList>
            </td>
            <td width="20%">
                <span class="field-label">Print Card Type</span>
            </td>
            <td width="30%">
                <asp:DropDownList ID="ddlCardType" AutoPostBack="true" runat="server">
                    
                </asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GrdStudents" AutoGenerateColumns="False" Width="100%" EmptyDataText="No record found for selected search" CssClass="table table-row table-bordered"
                    runat="server" ShowFooter="True" AllowPaging="True" PageSize="15">
                    <Columns>
                        <asp:TemplateField HeaderText="Barcode">
                            <HeaderTemplate>
                                Barcode
                                            <br />
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg1(this);" ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:CheckBox ID="CheckBar1" runat="server" Enabled='<%# Bind("PHOTOAVALIABLE") %>' />

                            </ItemTemplate>

                            <FooterTemplate>

                                <%-- <asp:CheckBox ID="CheckIssueAll" Text="<br>Issue All" Width="50px" runat="server" />--%>
                                <br />
                                <asp:Button ID="btnbarcode" runat="server" CssClass="button" CommandName="barcode" Text="Issue" />
                            </FooterTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Employee Number">
                            <HeaderTemplate>
                                Employee Number
                                            <br />
                                <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchStuno" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchStuno_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("emp_id")%>' runat="server" />
                                <%#Eval("EMPNO")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Employee Name">
                            <HeaderTemplate>
                                Employee Name
                                            <br />
                                <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchStuname" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchStuname_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("EMP_NAME")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation">
                            <HeaderTemplate>
                                Designation
                                            <br />
                                <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchgrade" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchgrade_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("DES_DESCR")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department">
                            <HeaderTemplate>
                                Department
                                            <br />
                                <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchsection" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchsection_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("DPT_DESCR")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Update Display">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpload" runat="server" OnClick="lnkbtnUpload_Click">Update</asp:LinkButton>
                                <asp:HiddenField ID="HF_Photopath" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
               <span class="field-label"> *In the absence of photograph, ID Card print option is inactive </span></td>
        </tr>
    </table>
    <asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenShowFlag" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="ModalpopupUpload" runat="server" BackgroundCssClass="modalBackground" DropShadow="True" PopupControlID="PanelTitleAdd" TargetControlID="lblmodalpopup">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Label ID="lblmodalpopup" runat="server" ForeColor="White"></asp:Label>
    <asp:Panel
        ID="PanelTitleAdd" runat="server" BackColor="LightYellow" CssClass="panel-cover"
        Style="display: none">
        <table width="100%">
            <tr class="matters">
                <td></td>
                <td style="width: 100px">
                     <asp:Label ID="lblmessage" runat="server"></asp:Label></td>
            </tr>
            <tr class="matters">
                <td>
                    <span class="field-label">Display Name</span></td>
                <td>
                    <asp:TextBox ID="txtDisplaName" runat="server" />&nbsp;</td>
            </tr>
            <tr class="matters">
                <td>
                    <span class="field-label">Display Designation</span></td>
                <td>
                    <asp:TextBox ID="txtCardDesignaiton" runat="server" />&nbsp;</td>
            </tr>
                        <tr class="matters">
                <td>
                    <span class="field-label">Card Number</span></td>
                <td>
                    <asp:TextBox ID="txtCard" runat="server"/></td>
            </tr>
            <tr class="matters">
                <td align="left" colspan="2">
                    <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="button" /><asp:Button
                        ID="btnClose" runat="server" CssClass="button" Text="Close" /></td>
            </tr>
        </table>
        <asp:HiddenField ID="HF_stu_id" runat="server" />
        <asp:HiddenField ID="h_EmpImagePath" runat="server" />
    </asp:Panel>

</div>
<script type="text/javascript">

    if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {
        document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0


        var sFeatures;
        sFeatures = "dialogWidth: 800px; ";
        sFeatures += "dialogHeight: 600px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";



        var result;
        //result = window.showModalDialog('Barcode/BarcodeStudentNumber.aspx?Type=STUDENT' ,"", sFeatures);
        result = window.open('../CardPrinter/NewApprovedCard/Barcode_empid.aspx?Type=EMPLOYEE')


    }


</script>
