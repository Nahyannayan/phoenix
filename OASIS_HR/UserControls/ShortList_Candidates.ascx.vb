Imports Oasis_Administrator
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
'ts
Partial Class OASIS_HR_UserControls_ShortList_Candidates
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim EncDec As New Encryption64
        If Not IsPostBack Then
            CheckMenuRights()
            BindCategory()
            BindSearchControls()
            If Request.QueryString("JobeCode") <> Nothing Then
                Dim Job_Code = EncDec.Decrypt(Request.QueryString("JobeCode").Replace(" ", "+"))
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
                Dim str_query = "SELECT CATEGORY_ID FROM JOBS WHERE JOB_CODE='" & Job_Code & "'"
                Dim category_id = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                ddSCategory.SelectedValue = category_id
                BindJobs()
                ddJobCode.SelectedValue = Job_Code
                BindGridVal()
            End If

        End If

    End Sub

    Public Sub BindSearchControls()

        txtApplicationNo.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtPostalCode.Text = ""
        txtHomePhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtLastContactDate.Text = ""
        txtAppliedFrom.Text = ""
        txtAppliedTo.Text = ""
        ddGender.SelectedIndex = 0
        ddMaritalStatus.SelectedIndex = 0
        ddQualified.SelectedIndex = 0



        ddExpFrom.Items.Clear()
        ddExpTo.Items.Clear()
        ddAgeFrom.Items.Clear()
        ddAgeTo.Items.Clear()

        Dim i = 0
        For i = 0 To 40
            ddExpFrom.Items.Add(i)
            ddExpTo.Items.Add(i)
        Next
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "--"
        ddExpFrom.Items.Insert(0, list1)
        ddExpTo.Items.Insert(0, list1)

        For i = 19 To 60
            ddAgeFrom.Items.Add(i)
            ddAgeTo.Items.Add(i)
        Next
        ddAgeFrom.Items.Insert(0, list1)
        ddAgeTo.Items.Insert(0, list1)

        ddNationality.DataSource = OasisAdministrator.Nationality()
        ddNationality.DataTextField = "CTY_NATIONALITY"
        ddNationality.DataValueField = "CTY_ID"
        ddNationality.DataBind()
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Select Country"
        ddNationality.Items.Insert(0, list2)

        listAppliedFor.DataSource = OasisAdministrator.GetCategory()
        listAppliedFor.DataTextField = "CATEGORY_DES"
        listAppliedFor.DataValueField = "CATEGORY_ID"
        listAppliedFor.DataBind()



        ddApplicantStage.DataSource = OasisAdministrator.Get_Stages()
        ddApplicantStage.DataTextField = "STAGE_DESCRIPTION"
        ddApplicantStage.DataValueField = "STAGE_ID"
        ddApplicantStage.DataBind()
        Dim list6 As New ListItem
        list6.Value = "-1"
        list6.Text = "Applicant Stage"
        ddApplicantStage.Items.Insert(0, list6)


        listSubjects.DataSource = OasisAdministrator.GetSubject()
        listSubjects.DataTextField = "SUBJECT"
        listSubjects.DataValueField = "SUBJECT_ID"
        listSubjects.DataBind()

        ddCurriculum.DataSource = OasisAdministrator.GetClm()
        ddCurriculum.DataTextField = "CURRICULUM"
        ddCurriculum.DataValueField = "CLM_ID"
        ddCurriculum.DataBind()
        Dim list4 As New ListItem
        list4.Value = "-1"
        list4.Text = "Select Curriculum"
        ddCurriculum.Items.Insert(0, list4)

        BindAvailabledate()

    End Sub

    Public Sub BindAvailabledate()
        ddAvailableMonth.Items.Clear()
        ddAvailableYear.Items.Clear()

        ''Month
        ddAvailableMonth.Items.Add("Jan")
        ddAvailableMonth.Items.Add("Feb")
        ddAvailableMonth.Items.Add("Mar")
        ddAvailableMonth.Items.Add("Apr")
        ddAvailableMonth.Items.Add("May")
        ddAvailableMonth.Items.Add("Jun")
        ddAvailableMonth.Items.Add("Jul")
        ddAvailableMonth.Items.Add("Aug")
        ddAvailableMonth.Items.Add("Sep")
        ddAvailableMonth.Items.Add("Oct")
        ddAvailableMonth.Items.Add("Nov")
        ddAvailableMonth.Items.Add("Dec")
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "Month"
        ddAvailableMonth.Items.Insert(0, list1)

        ''Year
        Dim i = Today.Year
        For i = Today.Year To Today.Year + 5
            ddAvailableYear.Items.Add(i)
        Next
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Year"
        ddAvailableYear.Items.Insert(0, list2)

    End Sub

    Public Sub AssignRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")

        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

        If GrdView.Rows.Count > 0 Then
            Dim btnshortlist As Button = DirectCast(GrdView.FooterRow.FindControl("btnshortlist"), Button)
            Dim directory As New System.Collections.Generic.Dictionary(Of String, Object)
            directory.Add("Save", btnshortlist)
            Call AccessRight3.setpage(directory, ViewState("menu_rights"), "Save")
        End If

        For Each row As GridViewRow In GrdView.Rows

            Dim linkdownlaod As LinkButton = DirectCast(row.FindControl("LinkShowCv"), LinkButton)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(linkdownlaod)

            ''Image path
            Dim PhotoActualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadPhotoPath").ToString()
            Dim photo = DirectCast(row.FindControl("HiddenPhotoPath"), HiddenField).Value
            Dim PhotoVirtualpath = Web.Configuration.WebConfigurationManager.AppSettings("UploadPhotoPathVirtual").ToString()
            Dim ApplicantImage1 As Image = DirectCast(row.FindControl("ApplicantImage1"), Image)
            If System.IO.File.Exists(PhotoActualPath & photo) Then
                ApplicantImage1.ImageUrl = PhotoVirtualpath & photo
            Else
                ApplicantImage1.ImageUrl = PhotoVirtualpath & "NoImage.bmp"

            End If

        Next

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000050" And ViewState("MainMnu_code") <> "H000060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub

    Public Sub BindJobCode(ByVal Category_ID As String)
        Dim ds As DataSet
        ds = OasisAdministrator.Get_Job_Code_Open_Jobs(Category_ID)
        If ds.Tables(0).Rows.Count > 0 Then

            ddJobCode.Visible = True
            ddJobCode.DataSource = ds
            ddJobCode.DataTextField = "JOB_DES"
            ddJobCode.DataValueField = "JOB_CODE"
            ddJobCode.DataBind()
            Dim list5 As New ListItem
            list5.Value = "-1"
            list5.Text = "Select Job Title"
            ddJobCode.Items.Insert(0, list5)
            lblMessage.Text = ""

        Else

            GrdView.DataSource = Nothing
            GrdView.DataBind()
            ddbsu.DataSource = Nothing
            ddbsu.DataBind()
            ddbsu.Visible = False
            ddJobCode.Controls.Clear()
            ddJobCode.Visible = False
            lblMessage.Text = "No jobs under this category"

        End If

    End Sub

    Public Sub BindCategory()
        Dim ds As DataSet = OasisAdministrator.GetCategory()
        If ds.Tables(0).Rows.Count > 0 Then
            ddSCategory.DataSource = ds
            ddSCategory.DataTextField = "CATEGORY_DES"
            ddSCategory.DataValueField = "CATEGORY_ID"
            ddSCategory.DataBind()
            Dim list As New ListItem
            list.Value = "-1"
            list.Text = "Select a Category"
            ddSCategory.Items.Insert(0, list)
            ddJobCode.Visible = False
        End If


    End Sub

    Protected Sub GrdView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdView.RowCommand
        Try
            If e.CommandName = "CV" Then
                Dim path = e.CommandArgument.ToString()
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadCVPath").ToString()
                path = cvVirtualPath + path

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)

                Response.Flush()

                Response.End()

                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()
            End If
            If e.CommandName = "Corres" Then
                Dim EncDec As New Encryption64
                Dim Application_No = EncDec.Encrypt(e.CommandArgument)
                Session("page") = "shorlist"
                Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
                Response.Redirect("ApplicantCorrespondence.aspx?Application_No=" & Application_No & mInfo)
            End If
            If e.CommandName = "history" Then
                Dim ds As DataSet
                ds = OasisAdministrator.Get_Applicant_Stage_Remarks(e.CommandArgument)

                GrdHistory.DataSource = ds
                GrdHistory.DataBind()


                MO1.Show()

            End If
        Catch ex As Exception
            lblMessage.Text = "Error :" & ex.Message
        End Try

    End Sub
    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.open('Applicant_Detail_Information.aspx?Application_No={0}', '','Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=no'); return false; ", pId)
    End Function

    Protected Sub btnshortlistsave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddbsu.Items.Count > 0 Then
            Dim Shorlisted_BSU_Id = ddbsu.SelectedValue
            Dim Flag = 0
            For Each row As GridViewRow In GrdView.Rows
                Dim chek As CheckBox = DirectCast(row.FindControl("Check1"), CheckBox)
                Dim Applicant_No As String = DirectCast(row.FindControl("HiddenApplication_No"), HiddenField).Value
                Dim Job_Code = ddJobCode.SelectedValue
                If chek.Checked Then
                    ''get the parent and sub id from table (ask prem) . hard coded
                    OasisAdministrator.Insert_Stage_Remarks(Applicant_No, 100, "Shortlisted")
                    OasisAdministrator.Update_Transactions(Applicant_No, 1, 100, Convert.ToInt32(Job_Code), Convert.ToInt32(Shorlisted_BSU_Id))
                    Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
                    Dim str_query = "UPDATE APPLICATION_MASTER SET STATUS='1' WHERE APPLICATION_NO='" & Applicant_No & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    Flag = 1
                End If
            Next

            Search()

            If Flag = 1 Then
                lblMessage.Text = "Selected applicant(s) shortlisted sucessfully."
            Else
                lblMessage.Text = "Please select applicants."
            End If

        Else
            lblMessage.Text = "Please select a Business Unit."
        End If

    End Sub


    Public Sub BindGridVal()
        If ddJobCode.SelectedIndex > 0 Then

            Dim ds As DataSet
            ds = OasisAdministrator.Get_Job_Details_Open_Jobs(ddJobCode.SelectedValue)

            If ds.Tables(0).Rows.Count > 0 Then

                ddbsu.DataSource = ds
                ddbsu.DataTextField = "BSU_NAME"
                ddbsu.DataValueField = "BSU"
                ddbsu.DataBind()
                ddbsu.Visible = True

            Else

                ddbsu.Visible = False
                GrdView.DataSource = Nothing
                GrdView.DataBind()
            End If
        Else
            GrdView.DataSource = Nothing
            GrdView.DataBind()
            ddbsu.Items.Clear()
            ddbsu.Visible = False

        End If
    End Sub

    Public Sub BindJobs()
        BindJobCode(ddSCategory.SelectedValue)
        ddbsu.Items.Clear()
        ddbsu.Visible = False
        GrdView.DataSource = Nothing
        GrdView.DataBind()
    End Sub

    Protected Sub GrdView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GrdView.PageIndex = e.NewPageIndex
        Search()
    End Sub

    Protected Sub ddJobCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddJobCode.SelectedIndex > 0 Then
            BindGridVal()
            GrdView.Controls.Clear()

        Else

            GrdView.Controls.Clear()
            ddbsu.Items.Clear()
            ddbsu.Visible = False

        End If

    End Sub

    Protected Sub ddSCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddSCategory.SelectedIndex > 0 Then
            BindJobs()
        Else
            ddJobCode.Visible = False
        End If

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub

    Public Sub Search()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim str_query = " SELECT DISTINCT A.APPLICATION_NO,A.FIRST_NAME,A.MIDDLE_NAME,(ISNULL(A.FIRST_NAME,'') + ' ' + ISNULL(A.SUR_NAME,'')) FULLNAME, " & _
                        " A.SUR_NAME,A.ADDRESS,A.TOWN,A.EMAIL,A.PHOTO_FILE_PATH,A.PHOTO_FILE_PATH,A.LAST_CONTACT_DATE,A.CV_FILE_PATH, " & _
                        " A.CV_FILE_NAME,A.POSTAL_CODE,A.HOME_PHONE,A.MOBILE,A.DATE_OF_BIRTH,A.AVAILABLE_DATE,A.OFFICE_PHONE,A.ADDITIONAL_INFORMATION,A.RATINGS, " & _
                        " (SELECT C.COUNTRY AS CRE FROM COUNTRY C WHERE A.COUNTRY_OF_RESIDENCE=C.COUNTRY_ID) AS COUNTRY_RES_NAME, " & _
                        " (SELECT CC.COUNTRY AS CIP FROM  COUNTRY CC WHERE A.COUNTRY_ISSUE_PASSPORT=CC.COUNTRY_ID) AS COUNTRY_PASS_ISS_NAME, " & _
                        " (SELECT BSU_NAME FROM OASIS.DBO.BUSINESSUNIT_M WHERE A.SHORTLISTED_BSU_ID=BSU_ID)AS BSU_NAME, " & _
                        " (CASE A.GENDER WHEN 'F' THEN 'Female' ELSE 'Male'  END) GENDER,EXPERIENCE, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(ADDITIONAL_INFORMATION,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, " & _
                        " (select CORRESPONDENCE + '<br>' + LAST_CONTACT_DATE + '<br>(' + (ISNULL(EM.EMP_FNAME,'') + ' ' + ISNULL(EM.EMP_LNAME,'')) + ')' FULLNAME   from  dbo.APPLICATION_CORRESPONDENCE " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M EM on  EM.EMP_ID=ENTRY_EMP_ID where id=(select max(id) from APPLICATION_CORRESPONDENCE where application_no=A.APPLICATION_NO))LASTCORRESS " & _
                        " FROM APPLICATION_MASTER A WHERE A.DELETED IS NULL AND A.ACTIVE='TRUE' AND A.STATUS='0'  "

        Dim condition As String = ""

        If txtApplicationNo.Text <> "" Then
            condition = condition & " AND A.APPLICATION_NO LIKE '%" & txtApplicationNo.Text.Trim() & "%'"
        End If

        If txtFirstName.Text <> "" Then
            condition = condition & " AND A.FIRST_NAME  LIKE '%" & txtFirstName.Text.Trim() & "%'"
        End If

        If txtLastName.Text <> "" Then
            condition = condition & " AND A.SUR_NAME LIKE '%" & txtLastName.Text.Trim() & "%'"
        End If

        If ddGender.SelectedIndex > 0 Then
            condition = condition & " AND A.GENDER LIKE '%" & ddGender.SelectedValue & "%'"
        End If

        If ddAgeFrom.SelectedIndex > 0 Then
            condition = condition & " AND DATEDIFF(yy,A.DATE_OF_BIRTH,GETDATE()) >=" & ddAgeFrom.SelectedValue & ""
        End If

        If ddAgeTo.SelectedIndex > 0 Then
            condition = condition & " AND DATEDIFF(yy,A.DATE_OF_BIRTH,GETDATE()) <=" & ddAgeTo.SelectedValue & ""
        End If

        If ddMaritalStatus.SelectedIndex > 0 Then
            condition = condition & " AND A.MARITAL_STATUS ='" & ddMaritalStatus.SelectedValue & "'"
        End If

        If txtPostalCode.Text <> "" Then
            condition = condition & " AND A.POSTAL_CODE LIKE '%" & txtPostalCode.Text.Trim() & "%'"
        End If

        If txtHomePhone.Text <> "" Then
            condition = condition & " AND A.HOME_PHONE LIKE '%" & txtHomePhone.Text.Trim() & "%'"
        End If

        If txtMobile.Text <> "" Then
            condition = condition & " AND A.MOBILE LIKE '%" & txtMobile.Text.Trim() & "%'"
        End If

        If txtEmail.Text <> "" Then
            condition = condition & " AND A.EMAIL LIKE '%" & txtEmail.Text.Trim() & "%'"
        End If

        If ddAvailableMonth.SelectedIndex > 0 Then
            condition = condition & " AND A.AVAILABLE_DATE LIKE '%" & ddAvailableMonth.SelectedValue & "%'"
        End If

        If ddAvailableYear.SelectedIndex > 0 Then
            condition = condition & " AND A.AVAILABLE_DATE LIKE '%" & ddAvailableYear.SelectedValue & "%'"
        End If

        If ddNationality.SelectedIndex > 0 Then
            condition = condition & " AND A.COUNTRY_OF_RESIDENCE =" & ddNationality.SelectedValue & ""
        End If

        If ddQualified.SelectedIndex > 0 Then
            condition = condition & " AND A.QUALIFIED_TEACHER =" & ddQualified.SelectedValue & ""
        End If

        If ddExpFrom.SelectedIndex > 0 Then
            condition = condition & " AND A.EXPERIENCE >=" & ddExpFrom.SelectedValue & ""
        End If

        If ddExpTo.SelectedIndex > 0 Then
            condition = condition & " AND A.EXPERIENCE <=" & ddExpTo.SelectedValue & ""
        End If

        If txtLastContactDate.Text <> "" Then
            condition = condition & " AND A.LAST_CONTACT_DATE LIKE '%" & txtLastContactDate.Text.Trim() & "%'"
        End If

        If txtAppliedFrom.Text <> "" Then
            condition = condition & " AND A.ENTRY_DATE >= '" & txtAppliedFrom.Text.Trim() & "'"
        End If

        If txtAppliedTo.Text <> "" Then
            condition = condition & " AND A.ENTRY_DATE <= '" & DateAdd(DateInterval.Day, 1, Convert.ToDateTime(txtAppliedTo.Text.Trim())) & "'"
        End If

        If ddApplicantStage.SelectedIndex > 0 Then
            condition = condition & " AND A.CURRENT_STAGE =" & ddApplicantStage.SelectedValue & ""
        End If

        If ddCurriculum.SelectedIndex > 0 Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  APPLICATION_NO FROM APPLICATION_WORK_EXPERIENCE WHERE CLM_INFO LIKE '%" & ddCurriculum.SelectedItem.Text.Trim() & "%')"
        End If


        Dim subjectsids As String = ""

        For Each item As ListItem In listSubjects.Items
            If item.Selected Then
                If subjectsids = "" Then
                    subjectsids = "'" & item.Value & "'"
                Else
                    subjectsids = subjectsids & ",'" & item.Value & "'"
                End If

            End If
        Next

        If subjectsids <> "" Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  DISTINCT APPLICATION_NO FROM APPLICATION_SUBJECT WHERE SUBJECT_ID IN (" & subjectsids & ") )"
        End If


        Dim Appliedfor As String = ""

        For Each item As ListItem In listAppliedFor.Items
            If item.Selected Then
                If Appliedfor = "" Then
                    Appliedfor = "'" & item.Value & "'"
                Else
                    Appliedfor = Appliedfor & ",'" & item.Value & "'"
                End If

            End If
        Next

        If Appliedfor <> "" Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  DISTINCT APPLICATION_NO FROM APPLICANTION_APPLIED_CATEGORY WHERE CATEGORY_ID IN (" & Appliedfor & ") )"
        End If


        If condition <> "" Then
            str_query = str_query & condition
        End If

        str_query = str_query & " ORDER BY A.APPLICATION_NO DESC "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GrdView.DataSource = ds
        GrdView.DataBind()
        lblMessage.Text = ""
        AssignRights()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        BindSearchControls()
    End Sub

End Class
