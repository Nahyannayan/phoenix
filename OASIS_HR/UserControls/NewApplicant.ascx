<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewApplicant.ascx.vb"
    Inherits="OASIS_HR_UserControls_NewApplicant" %>
<link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function change_chk_stateg(chkThis) {

        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckActive") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>

<div class="matters">
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>&nbsp;
    <br />
    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton>
    <asp:Panel ID="Panel1" runat="server">
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
            <tr>
                <td class="subheader_img">
                    Search
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;<ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="HT1" runat="server">
                            <HeaderTemplate>
                                Basic Search
                            </HeaderTemplate>
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                        <tr>
                                            <td>
                                                Application No
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                First Name
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Last Name
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Gender
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddGender" runat="server">
                                                    <asp:ListItem Value="-1">Gender</asp:ListItem>
                                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                Age
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddAgeFrom" runat="server">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddAgeTo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                Marital Status
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddMaritalStatus" runat="server">
                                                    <asp:ListItem Value="-1">Marital Status</asp:ListItem>
                                                    <asp:ListItem Value="Married">Married</asp:ListItem>
                                                    <asp:ListItem Value="Single">Single</asp:ListItem>
                                                    <asp:ListItem Value="Partner">Partner</asp:ListItem>
                                                    <asp:ListItem Value="Widow">Widow</asp:ListItem>
                                                    <asp:ListItem Value="Widower">Widower</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Postal Code
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPostalCode" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Home Phone
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Mobile
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Email
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                Available On
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddAvailableMonth" runat="server">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddAvailableYear" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                Nationality
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddNationality" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Qualified
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddQualified" runat="server">
                                                    <asp:ListItem Selected="True" Value="-1">Qualified Teacher?</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                Experience
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddExpFrom" runat="server">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddExpTo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Applied On
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtAppliedFrom" runat="server" Width="85px"></asp:TextBox>
                                                <asp:TextBox ID="txtAppliedTo" runat="server" Width="85px"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="9">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtAppliedFrom">
                                    </ajaxToolkit:CalendarExtender>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True"
                                        Format="dd/MMM/yyyy" TargetControlID="txtAppliedTo">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT2" runat="server">
                            <HeaderTemplate>
                                More Search Options
                            </HeaderTemplate>
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                        <tr>
                                            <td>
                                                Curriculum
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCurriculum" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Subject
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:Panel ID="P1" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="listSubjects" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                Applied For
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:Panel ID="P2" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="listAppliedFor" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                        Text="Search" Width="100px" /><asp:Button ID="btnReset" runat="server" CausesValidation="False"
                            CssClass="button" Text="Reset" Width="100px" />
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="False" CollapsedSize="0" CollapsedText="Advance Search" ExpandControlID="LinkAdvanceSearch"
        ExpandedSize="350" ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>
</div>
<br />
<div class="matters">
  
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="750">
        <tr>
            <td class="subheader_img">
                New Applicant
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GrdView" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    BorderWidth="0" EmptyDataText="<br><span style='color: red;font-weight:bold'>As of today there are no new applicants (or) Search query did not produce any results.</span><br><br><br>"
                    OnPageIndexChanging="GrdView_PageIndexChanging" PageSize="10" ShowFooter="True"
                    Width="100%">
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <Columns>
                        <asp:TemplateField HeaderText="App No">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            App&nbsp;No
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                               <center><%#Eval("APPLICATION_NO")%></center>
                                <asp:HiddenField ID="Hiddenappno" runat="server" Value='<%#Eval("APPLICATION_NO")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="Image">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Image
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenPhotoPath" Value='<%# Eval("PHOTO_FILE_PATH") %>' runat="server" />
                            <center><asp:Image ID="ApplicantImage1"  Width="80px" Height="89px" runat="server" /></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Name">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Name
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkView" runat="server"  OnClientClick="javascript:return false;" 
                                    Text='<%# Eval("FULLNAME") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Country">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Country
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("COUNTRY_RES_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Category
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:GridView ID="GrdCategory" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                    ShowHeader="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%#Eval("CATEGORY_DES")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Additional Information">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Additional&nbsp;Information
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="T5lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                                    <%#Eval("ADDITIONAL_INFORMATION")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T5lblview"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T5Panel1"
                                    TextLabelID="T5lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Detail View">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Detail&nbsp;View
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkShow" runat="server" OnClientClick='<%# GetNavigateUrl(Eval("APPLICATION_NO").ToString()) %>'
                                        Text="View"></asp:LinkButton></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ratings">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Ratings
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <ajaxToolkit:Rating ID="CRatings" runat="server" CurrentRating='<%# Eval("RATINGS") %>'
                                        EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar" MaxRating="5"
                                        StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
                                    </ajaxToolkit:Rating>
                                </center>
                            </ItemTemplate>
                            <ItemStyle Width="70px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Active
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <FooterTemplate>
                                <center>
                                    <asp:Button ID="BtnUpdate" runat="server" CssClass="button" OnClick="BtnUpdate_Click"
                                        Text="Update" /></center>
                            </FooterTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:CheckBox ID="CheckActive" runat="server" /></center>
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Delete
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkDelete" runat="server" CommandArgument='<%# Eval("APPLICATION_NO") %>'
                                        CommandName="Deleting" Text="Delete"></asp:LinkButton></center>
                                <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" ConfirmText="Delete this record?"
                                    TargetControlID="LinkDelete">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>

            </td>
        </tr>
    </table>
   
</div>
