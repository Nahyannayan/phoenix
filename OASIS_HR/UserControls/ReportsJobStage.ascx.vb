Imports System.Data
Imports Oasis_Administrator
Imports System.Text
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data

Partial Class OASIS_HR_UserControls_ReportsJobStage
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CheckMenuRights()
            BindSearchControls()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExportExcelSheet)
    End Sub
    Public Sub BindSearchControls()

        txtApplicationNo.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtPostalCode.Text = ""
        txtHomePhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtLastContactDate.Text = ""
        txtAppliedFrom.Text = ""
        txtAppliedTo.Text = ""

        ddExpFrom.Items.Clear()
        ddExpTo.Items.Clear()
        ddAgeFrom.Items.Clear()
        ddAgeTo.Items.Clear()

        Dim i = 0
        For i = 0 To 40
            ddExpFrom.Items.Add(i)
            ddExpTo.Items.Add(i)
        Next
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "--"
        ddExpFrom.Items.Insert(0, list1)
        ddExpTo.Items.Insert(0, list1)

        For i = 19 To 60
            ddAgeFrom.Items.Add(i)
            ddAgeTo.Items.Add(i)
        Next
        ddAgeFrom.Items.Insert(0, list1)
        ddAgeTo.Items.Insert(0, list1)

        ddNationality.DataSource = OasisAdministrator.Nationality()
        ddNationality.DataTextField = "CTY_NATIONALITY"
        ddNationality.DataValueField = "CTY_ID"
        ddNationality.DataBind()
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Select Country"
        ddNationality.Items.Insert(0, list2)

        listAppliedFor.DataSource = OasisAdministrator.GetCategory()
        listAppliedFor.DataTextField = "CATEGORY_DES"
        listAppliedFor.DataValueField = "CATEGORY_ID"
        listAppliedFor.DataBind()



        ddApplicantStage.DataSource = OasisAdministrator.Get_Stages()
        ddApplicantStage.DataTextField = "STAGE_DESCRIPTION"
        ddApplicantStage.DataValueField = "STAGE_ID"
        ddApplicantStage.DataBind()
        Dim list6 As New ListItem
        list6.Value = "-1"
        list6.Text = "Applicant Stage"
        ddApplicantStage.Items.Insert(0, list6)


        listSubjects.DataSource = OasisAdministrator.GetSubject()
        listSubjects.DataTextField = "SUBJECT"
        listSubjects.DataValueField = "SUBJECT_ID"
        listSubjects.DataBind()

        ddCurriculum.DataSource = OasisAdministrator.GetClm()
        ddCurriculum.DataTextField = "CURRICULUM"
        ddCurriculum.DataValueField = "CLM_ID"
        ddCurriculum.DataBind()
        Dim list4 As New ListItem
        list4.Value = "-1"
        list4.Text = "Select Curriculum"
        ddCurriculum.Items.Insert(0, list4)

        BindAvailabledate()

    End Sub

    Public Sub BindAvailabledate()
        ddAvailableMonth.Items.Clear()
        ddAvailableYear.Items.Clear()

        ''Month
        ddAvailableMonth.Items.Add("Jan")
        ddAvailableMonth.Items.Add("Feb")
        ddAvailableMonth.Items.Add("Mar")
        ddAvailableMonth.Items.Add("Apr")
        ddAvailableMonth.Items.Add("May")
        ddAvailableMonth.Items.Add("Jun")
        ddAvailableMonth.Items.Add("Jul")
        ddAvailableMonth.Items.Add("Aug")
        ddAvailableMonth.Items.Add("Sep")
        ddAvailableMonth.Items.Add("Oct")
        ddAvailableMonth.Items.Add("Nov")
        ddAvailableMonth.Items.Add("Dec")
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "Month"
        ddAvailableMonth.Items.Insert(0, list1)

        ''Year
        Dim i = Today.Year
        For i = Today.Year To Today.Year + 5
            ddAvailableYear.Items.Add(i)
        Next
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Year"
        ddAvailableYear.Items.Insert(0, list2)

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000080") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub

    Shared DelColumn As String


    Public Sub BindGrid(ByVal ds As DataSet)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                GrdReports.DataSource = ds
                GrdReports.DataBind()
                Dim c = 0
                For c = 0 To GrdReports.HeaderRow.Cells.Count - 1
                    Dim Headertext = GrdReports.HeaderRow.Cells(c).Text
                    For Each item As ListItem In CheckBoxListCheck.Items
                        If Headertext = item.Value And item.Selected = True Then
                            GrdReports.HeaderRow.Cells(c).Text = item.Text
                        End If
                    Next

                Next

                ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExportExcelSheet)
                btnExportExcelSheet.Visible = True
            Else
                btnExportExcelSheet.Visible = False
            End If


        Catch ex As Exception

        End Try

    End Sub
   
    Public Sub GridBindESheet(ByVal ds As DataSet)
        Dim dt As New DataTable
        dt.Rows.Clear()
        dt.Columns.Clear()
        For Each Item As ListItem In CheckBoxListCheck.Items
            If Item.Selected = True Then
                dt.Columns.Add(Item.Value)
            End If
        Next
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim dr As DataRow = dt.NewRow()
            For Each dc As DataColumn In dt.Columns
                If dc.ColumnName <> "SUBJECT" Then '' Special cases, selecting subjects 
                    dr.Item(dc.ColumnName) = ds.Tables(0).Rows(i).Item(dc.ColumnName).ToString()
                Else
                    Dim dsub As DataSet
                    dsub = OasisAdministrator.GetApplicantInfo(ds.Tables(0).Rows(i).Item("APPLICATION_NO").ToString(), 8)
                    Dim val As String = ""
                    Dim isu = 0
                    If dsub.Tables(0).Rows.Count > 0 Then
                        For isu = 0 To dsub.Tables(0).Rows.Count - 1
                            If isu = 0 Then
                                val = dsub.Tables(0).Rows(isu).Item("SUBJECT").ToString() & "-(" & dsub.Tables(0).Rows(isu).Item("STATUS").ToString() & ")"
                            Else
                                val = val & "," & dsub.Tables(0).Rows(isu).Item("SUBJECT").ToString() & "-(" & dsub.Tables(0).Rows(isu).Item("STATUS").ToString() & ")"
                            End If
                        Next
                    End If

                    dr.Item(dc.ColumnName) = val
                End If

            Next
            dt.Rows.Add(dr)
        Next
        ds.Tables.Clear()
        ds.Tables.Add(dt)
        BindGrid(ds)
    End Sub

    Protected Sub btnExportExcelSheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcelSheet.Click
        ExportGridToExcel(GrdReports, "Reports")
    End Sub

    Public Sub ExportGridToExcel(ByVal GrdView As GridView, ByVal fileName As String)
        Try
            Response.Clear()
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", fileName))
            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New StringWriter()
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            GrdView.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception

        End Try

    End Sub

    Public Sub Search()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim str_query = " SELECT DISTINCT A.APPLICATION_NO,A.FIRST_NAME,A.MIDDLE_NAME,(ISNULL(A.FIRST_NAME,'') + ' ' + ISNULL(A.SUR_NAME,'')) FULLNAME, " & _
                        " A.SUR_NAME,A.ADDRESS,A.TOWN,A.EMAIL,A.PHOTO_FILE_PATH,A.PHOTO_FILE_PATH,A.LAST_CONTACT_DATE,A.CV_FILE_PATH, " & _
                        " A.CV_FILE_NAME,A.POSTAL_CODE,A.HOME_PHONE,A.MOBILE,A.DATE_OF_BIRTH,A.AVAILABLE_DATE,A.OFFICE_PHONE,A.ADDITIONAL_INFORMATION,A.RATINGS, " & _
                        " (SELECT C.COUNTRY AS CRE FROM COUNTRY C WHERE A.COUNTRY_OF_RESIDENCE=C.COUNTRY_ID) AS COUNTRY_RES_NAME, " & _
                        " (SELECT CC.COUNTRY AS CIP FROM  COUNTRY CC WHERE A.COUNTRY_ISSUE_PASSPORT=CC.COUNTRY_ID) AS COUNTRY_PASS_ISS_NAME, " & _
                        " (SELECT BSU_NAME FROM OASIS.DBO.BUSINESSUNIT_M WHERE A.SHORTLISTED_BSU_ID=BSU_ID)AS BSU_NAME, " & _
                        " (CASE A.GENDER WHEN 'F' THEN 'Female' ELSE 'Male'  END) GENDER,EXPERIENCE, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(ADDITIONAL_INFORMATION,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, " & _
                        " (select CORRESPONDENCE + '<br>' + LAST_CONTACT_DATE + '<br>(' + (ISNULL(EM.EMP_FNAME,'') + ' ' + ISNULL(EM.EMP_LNAME,'')) + ')' FULLNAME   from  dbo.APPLICATION_CORRESPONDENCE " & _
                        " LEFT JOIN OASIS.dbo.EMPLOYEE_M EM on  EM.EMP_ID=ENTRY_EMP_ID where id=(select max(id) from APPLICATION_CORRESPONDENCE where application_no=A.APPLICATION_NO))LASTCORRESS " & _
                        " FROM APPLICATION_MASTER A WHERE A.DELETED IS NULL AND A.ACTIVE='TRUE' "

        Dim condition As String = ""

        If txtApplicationNo.Text <> "" Then
            condition = condition & " AND A.APPLICATION_NO LIKE '%" & txtApplicationNo.Text.Trim() & "%'"
        End If

        If txtFirstName.Text <> "" Then
            condition = condition & " AND A.FIRST_NAME  LIKE '%" & txtFirstName.Text.Trim() & "%'"
        End If

        If txtLastName.Text <> "" Then
            condition = condition & " AND A.SUR_NAME LIKE '%" & txtLastName.Text.Trim() & "%'"
        End If

        If ddGender.SelectedIndex > 0 Then
            condition = condition & " AND A.GENDER LIKE '%" & ddGender.SelectedValue & "%'"
        End If

        If ddAgeFrom.SelectedIndex > 0 Then
            condition = condition & " AND DATEDIFF(yy,A.DATE_OF_BIRTH,GETDATE()) >=" & ddAgeFrom.SelectedValue & ""
        End If

        If ddAgeTo.SelectedIndex > 0 Then
            condition = condition & " AND DATEDIFF(yy,A.DATE_OF_BIRTH,GETDATE()) <=" & ddAgeTo.SelectedValue & ""
        End If

        If ddMaritalStatus.SelectedIndex > 0 Then
            condition = condition & " AND A.MARITAL_STATUS ='" & ddMaritalStatus.SelectedValue & "'"
        End If

        If txtPostalCode.Text <> "" Then
            condition = condition & " AND A.POSTAL_CODE LIKE '%" & txtPostalCode.Text.Trim() & "%'"
        End If

        If txtHomePhone.Text <> "" Then
            condition = condition & " AND A.HOME_PHONE LIKE '%" & txtHomePhone.Text.Trim() & "%'"
        End If

        If txtMobile.Text <> "" Then
            condition = condition & " AND A.MOBILE LIKE '%" & txtMobile.Text.Trim() & "%'"
        End If

        If txtEmail.Text <> "" Then
            condition = condition & " AND A.EMAIL LIKE '%" & txtEmail.Text.Trim() & "%'"
        End If

        If ddAvailableMonth.SelectedIndex > 0 Then
            condition = condition & " AND A.AVAILABLE_DATE LIKE '%" & ddAvailableMonth.SelectedValue & "%'"
        End If

        If ddAvailableYear.SelectedIndex > 0 Then
            condition = condition & " AND A.AVAILABLE_DATE LIKE '%" & ddAvailableYear.SelectedValue & "%'"
        End If

        If ddNationality.SelectedIndex > 0 Then
            condition = condition & " AND A.COUNTRY_OF_RESIDENCE =" & ddNationality.SelectedValue & ""
        End If

        If ddQualified.SelectedIndex > 0 Then
            condition = condition & " AND A.QUALIFIED_TEACHER =" & ddQualified.SelectedValue & ""
        End If

        If ddExpFrom.SelectedIndex > 0 Then
            condition = condition & " AND A.EXPERIENCE >=" & ddExpFrom.SelectedValue & ""
        End If

        If ddExpTo.SelectedIndex > 0 Then
            condition = condition & " AND A.EXPERIENCE <=" & ddExpTo.SelectedValue & ""
        End If

        If txtLastContactDate.Text <> "" Then
            condition = condition & " AND A.LAST_CONTACT_DATE LIKE '%" & txtLastContactDate.Text.Trim() & "%'"
        End If

        If txtAppliedFrom.Text <> "" Then
            condition = condition & " AND A.ENTRY_DATE >= '" & txtAppliedFrom.Text.Trim() & "'"
        End If

        If txtAppliedTo.Text <> "" Then
            condition = condition & " AND A.ENTRY_DATE <= '" & DateAdd(DateInterval.Day, 1, Convert.ToDateTime(txtAppliedTo.Text.Trim())) & "'"
        End If

        If ddApplicantStage.SelectedIndex > 0 Then
            condition = condition & " AND A.CURRENT_STAGE =" & ddApplicantStage.SelectedValue & ""
        End If

        If ddCurriculum.SelectedIndex > 0 Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  APPLICATION_NO FROM APPLICATION_WORK_EXPERIENCE WHERE CLM_INFO LIKE '%" & ddCurriculum.SelectedItem.Text.Trim() & "%')"
        End If


        Dim subjectsids As String = ""

        For Each item As ListItem In listSubjects.Items
            If item.Selected Then
                If subjectsids = "" Then
                    subjectsids = "'" & item.Value & "'"
                Else
                    subjectsids = subjectsids & ",'" & item.Value & "'"
                End If

            End If
        Next

        If subjectsids <> "" Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  DISTINCT APPLICATION_NO FROM APPLICATION_SUBJECT WHERE SUBJECT_ID IN (" & subjectsids & ") )"
        End If


        Dim Appliedfor As String = ""

        For Each item As ListItem In listAppliedFor.Items
            If item.Selected Then
                If Appliedfor = "" Then
                    Appliedfor = "'" & item.Value & "'"
                Else
                    Appliedfor = Appliedfor & ",'" & item.Value & "'"
                End If

            End If
        Next

        If Appliedfor <> "" Then
            condition = condition & " AND A.APPLICATION_NO IN (SELECT  DISTINCT APPLICATION_NO FROM APPLICANTION_APPLIED_CATEGORY WHERE CATEGORY_ID IN (" & Appliedfor & ") )"
        End If


        If condition <> "" Then
            str_query = str_query & condition
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            btnExportExcelSheet.Visible = True
        Else
            btnExportExcelSheet.Visible = False
        End If
        GrdReports.Controls.Clear()
        GrdReports.DataSource = Nothing
        GrdReports.DataBind()
        GridBindESheet(ds)

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        BindSearchControls()
    End Sub

End Class
