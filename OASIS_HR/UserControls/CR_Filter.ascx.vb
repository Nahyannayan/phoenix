Imports Oasis_Administrator
Imports System.Data
Partial Class OASIS_HR_UserControls_CR_Filter
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindSearchControls()
                ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSearch)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindSearchControls()

        txtApplicationNo.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtPostalCode.Text = ""
        txtHomePhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtLastContactDate.Text = ""
        txtAppliedFrom.Text = ""
        txtAppliedTo.Text = ""

        ddExpFrom.Items.Clear()
        ddExpTo.Items.Clear()
        ddAgeFrom.Items.Clear()
        ddAgeTo.Items.Clear()

        Dim i = 0
        For i = 0 To 40
            ddExpFrom.Items.Add(i)
            ddExpTo.Items.Add(i)
        Next
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "--"
        ddExpFrom.Items.Insert(0, list1)
        ddExpTo.Items.Insert(0, list1)

        For i = 19 To 60
            ddAgeFrom.Items.Add(i)
            ddAgeTo.Items.Add(i)
        Next
        ddAgeFrom.Items.Insert(0, list1)
        ddAgeTo.Items.Insert(0, list1)

        ddNationality.DataSource = OasisAdministrator.Nationality()
        ddNationality.DataTextField = "CTY_NATIONALITY"
        ddNationality.DataValueField = "CTY_ID"
        ddNationality.DataBind()
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Select Country"
        ddNationality.Items.Insert(0, list2)

        listAppliedFor.DataSource = OasisAdministrator.GetCategory()
        listAppliedFor.DataTextField = "CATEGORY_DES"
        listAppliedFor.DataValueField = "CATEGORY_ID"
        listAppliedFor.DataBind()



        ddApplicantStage.DataSource = OasisAdministrator.Get_Stages()
        ddApplicantStage.DataTextField = "STAGE_DESCRIPTION"
        ddApplicantStage.DataValueField = "STAGE_ID"
        ddApplicantStage.DataBind()
        Dim list6 As New ListItem
        list6.Value = "-1"
        list6.Text = "Applicant Stage"
        ddApplicantStage.Items.Insert(0, list6)


        listSubjects.DataSource = OasisAdministrator.GetSubject()
        listSubjects.DataTextField = "SUBJECT"
        listSubjects.DataValueField = "SUBJECT_ID"
        listSubjects.DataBind()

        ddCurriculum.DataSource = OasisAdministrator.GetClm()
        ddCurriculum.DataTextField = "CURRICULUM"
        ddCurriculum.DataValueField = "CLM_ID"
        ddCurriculum.DataBind()
        Dim list4 As New ListItem
        list4.Value = "-1"
        list4.Text = "Select Curriculum"
        ddCurriculum.Items.Insert(0, list4)

        BindAvailabledate()

    End Sub

    Public Sub BindAvailabledate()
        ddAvailableMonth.Items.Clear()
        ddAvailableYear.Items.Clear()

        ''Month
        ddAvailableMonth.Items.Add("Jan")
        ddAvailableMonth.Items.Add("Feb")
        ddAvailableMonth.Items.Add("Mar")
        ddAvailableMonth.Items.Add("Apr")
        ddAvailableMonth.Items.Add("May")
        ddAvailableMonth.Items.Add("Jun")
        ddAvailableMonth.Items.Add("Jul")
        ddAvailableMonth.Items.Add("Aug")
        ddAvailableMonth.Items.Add("Sep")
        ddAvailableMonth.Items.Add("Oct")
        ddAvailableMonth.Items.Add("Nov")
        ddAvailableMonth.Items.Add("Dec")
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "Month"
        ddAvailableMonth.Items.Insert(0, list1)

        ''Year
        Dim i = Today.Year
        For i = Today.Year To Today.Year + 5
            ddAvailableYear.Items.Add(i)
        Next
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Year"
        ddAvailableYear.Items.Insert(0, list2)

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        SearchApplicant()
    End Sub
    Public Sub SearchApplicant()
        Dim param As New Hashtable

        If txtApplicationNo.Text <> "" Then
            param.Add("@APPLICATION_NO", txtApplicationNo.Text.Trim())
        Else
            param.Add("@APPLICATION_NO", DBNull.Value)
        End If

        If txtFirstName.Text <> "" Then
            param.Add("@FIRST_NAME", txtFirstName.Text.Trim())
        Else
            param.Add("@FIRST_NAME", DBNull.Value)
        End If

        If txtLastName.Text <> "" Then
            param.Add("@SUR_NAME", txtLastName.Text.Trim())
        Else
            param.Add("@SUR_NAME", DBNull.Value)
        End If

        If ddGender.SelectedIndex > 0 Then
            param.Add("@GENDER", ddGender.SelectedValue)
        Else
            param.Add("@GENDER", DBNull.Value)
        End If

        If ddAgeFrom.SelectedIndex > 0 Then
            param.Add("@AGE_FROM", ddAgeFrom.SelectedValue)
        Else
            param.Add("@AGE_FROM", DBNull.Value)
        End If

        If ddAgeTo.SelectedIndex > 0 Then
            param.Add("@AGE_TO", ddAgeTo.SelectedValue)
        Else
            param.Add("@AGE_TO", DBNull.Value)
        End If

        If ddMaritalStatus.SelectedIndex > 0 Then
            param.Add("@MARITAL_STATUS", ddMaritalStatus.SelectedValue)
        Else
            param.Add("@MARITAL_STATUS", DBNull.Value)
        End If

        If txtPostalCode.Text <> "" Then
            param.Add("@POSTAL_CODE", txtPostalCode.Text.Trim())
        Else
            param.Add("@POSTAL_CODE", DBNull.Value)
        End If

        If txtHomePhone.Text <> "" Then
            param.Add("@HOME_PHONE", txtHomePhone.Text.Trim())
        Else
            param.Add("@HOME_PHONE", DBNull.Value)
        End If

        If txtMobile.Text <> "" Then
            param.Add("@MOBILE", txtMobile.Text.Trim())
        Else
            param.Add("@MOBILE", DBNull.Value)
        End If

        If txtEmail.Text <> "" Then
            param.Add("@EMAIL_ID", txtEmail.Text.Trim())
        Else
            param.Add("@EMAIL_ID", DBNull.Value)
        End If


        Dim AvailableDate = ""

        If ddAvailableMonth.SelectedIndex > 0 Then
            AvailableDate = ddAvailableMonth.SelectedValue
        End If


        If ddAvailableYear.SelectedIndex > 0 Then
            AvailableDate = AvailableDate & "/" & ddAvailableYear.SelectedValue
        End If


        If AvailableDate <> "" Then
            param.Add("@AVAILABLE_DATE", AvailableDate)
        Else
            param.Add("@AVAILABLE_DATE", DBNull.Value)
        End If


        If ddNationality.SelectedIndex > 0 Then
            param.Add("@NATIONALITY", ddNationality.SelectedValue)
        Else
            param.Add("@NATIONALITY", DBNull.Value)
        End If

        If ddQualified.SelectedIndex > 0 Then
            param.Add("@QUALIFIED", ddQualified.SelectedValue)
        Else
            param.Add("@QUALIFIED", DBNull.Value)
        End If

        If ddExpFrom.SelectedIndex > 0 Then
            param.Add("@EXP_FROM", ddExpFrom.SelectedValue)
        Else
            param.Add("@EXP_FROM", DBNull.Value)
        End If

        If ddExpTo.SelectedIndex > 0 Then
            param.Add("@EXP_TO", ddExpTo.SelectedValue)
        Else
            param.Add("@EXP_TO", DBNull.Value)
        End If

        If txtLastContactDate.Text <> "" Then
            param.Add("@LAST_CONTACT_DATE", txtLastContactDate.Text.Trim())
        Else
            param.Add("@LAST_CONTACT_DATE", DBNull.Value)
        End If

        If txtAppliedFrom.Text <> "" Then
            param.Add("@APPLIED_FROM", txtAppliedFrom.Text.Trim())
        Else
            param.Add("@APPLIED_FROM", DBNull.Value)
        End If

        If txtAppliedTo.Text <> "" Then
            param.Add("@APPLIED_TO", txtAppliedTo.Text.Trim())
        Else
            param.Add("@APPLIED_TO", DBNull.Value)
        End If

        If ddApplicantStage.SelectedIndex > 0 Then
            param.Add("@APPLICANT_STAGE", ddApplicantStage.SelectedValue)
        Else
            param.Add("@APPLICANT_STAGE", DBNull.Value)
        End If

        If ddCurriculum.SelectedIndex > 0 Then
            param.Add("@CURRICULUM", ddCurriculum.SelectedItem.Text.Trim())
        Else
            param.Add("@CURRICULUM", DBNull.Value)
        End If


        Dim subjectsids As String = ""

        For Each item As ListItem In listSubjects.Items
            If item.Selected Then
                If subjectsids = "" Then
                    subjectsids = "'" & item.Value & "'"
                Else
                    subjectsids = subjectsids & ",'" & item.Value & "'"
                End If

            End If
        Next

        If subjectsids <> "" Then
            param.Add("@SUBJECT_ID", subjectsids)
        Else
            param.Add("@SUBJECT_ID", DBNull.Value)
        End If


        Dim Appliedfor As String = ""

        For Each item As ListItem In listAppliedFor.Items
            If item.Selected Then
                If Appliedfor = "" Then
                    Appliedfor = "'" & item.Value & "'"
                Else
                    Appliedfor = Appliedfor & ",'" & item.Value & "'"
                End If

            End If
        Next

        If Appliedfor <> "" Then
            param.Add("@APPLICANT_APPLIED_FOR", Appliedfor)
        Else
            param.Add("@APPLICANT_APPLIED_FOR", DBNull.Value)
        End If



        param.Add("@ReportHeader", "HR ONLINE APPLICATION MODULE REPORT")
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_HR"
            .reportParameters = param
            .reportPath = Server.MapPath("~/OASIS_HR/Reports/CR_Applicants.rpt")
        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click

        BindSearchControls()

    End Sub
End Class
