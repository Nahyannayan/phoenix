<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InterviewCityAdd.ascx.vb" Inherits="OASIS_HR_UserControls_InterviewCityAdd" %>

<div class="matters">

<table>
    <tr>
        <td align="left" width="20%">
        <span class="field-label">Information Source</span></td>
        <td align="left" width="30%">
<asp:DropDownList ID="ddSource" runat="server" AutoPostBack="True">
</asp:DropDownList></td>
    
        <td align="left" width="20%">
           <span class="field-label"> Cities</span></td>
        <td align="left" width="30%">
<asp:DropDownList ID="ddcities" runat="server">
</asp:DropDownList></td>
    </tr>
    <tr>       
        <td align="center" colspan="4">
        <asp:Button ID="btnadd" runat="server" CssClass="button" Text="Add" /></td>
    </tr>
   
</table>
</div>
<br />
<asp:GridView ID="GridInterviews" CssClass="table table-bordered table-row" EmptyDataText="Interview cities not added for selected source" AutoGenerateColumns="false" runat="server" Width="100%">
<Columns>
<asp:TemplateField HeaderText="Cities">
<ItemTemplate>
<%# Eval("CITY_DES") %>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Delete">
<ItemTemplate>
    <asp:LinkButton ID="LinkDelete" CommandName="Deleting" CommandArgument='<%# Eval("ID") %>'  runat="server">Delete</asp:LinkButton>
<ajaxToolkit:ConfirmButtonExtender ID="Cf1" ConfirmText="Are you sure?" TargetControlID="LinkDelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
</ItemTemplate>
</asp:TemplateField>

</Columns>
<HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                   <RowStyle CssClass="griditem" Wrap="False" />
                   <%--<SelectedRowStyle CssClass="Green" Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
                    <EmptyDataRowStyle Wrap="False" />
                   <EditRowStyle Wrap="False" />
</asp:GridView>

