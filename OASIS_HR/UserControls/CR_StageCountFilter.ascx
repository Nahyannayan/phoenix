<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CR_StageCountFilter.ascx.vb" Inherits="OASIS_HR_UserControls_CR_StageCountFilter" %>

<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->

<div align="center">
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Stage Count Filter</td>
        </tr>
        <tr>
            <td>
                <table align="center" width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Job</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddjob" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Job Category</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddjobcategory" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Shorlisted BSU</span></td>

                        <td colspan="3" align="left">
                            <asp:DropDownList ID="ddshortlistedbsu" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" class="title-bg-lite"><span class="field-label">Entry Date</span></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtfrom" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtto" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return false;" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnview" runat="server" CssClass="button" Text="View" /></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" PopupButtonID="ImageButton1"
        TargetControlID="txtfrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
        TargetControlID="txtto">
    </ajaxToolkit:CalendarExtender>
</div>
