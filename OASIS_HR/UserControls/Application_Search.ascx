<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Application_Search.ascx.vb" Inherits="OASIS_HR_UserControls_Application_Search" %>
<link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
     
<div class="matters">
    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton>

    <asp:Panel ID="Panel1" runat="server">
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
            <tr>
                <td class="subheader_img">
                    Search</td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;<ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="HT1" runat="server">
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                        <tr>
                                            <td>
                                                First Name</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="txtFirstName" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="txtApplicationNo" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Middle Name
                                            </td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtMidName" runat="server"></asp:TextBox></td>
                                            <td>
                                                Last Name</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                                        </tr>
                                        
                                        
                                         <tr>
                                            <td>
                                                Known As</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtKnownAs" runat="server" Visible="True"></asp:TextBox>
                                             </td>
                                            <td>
                                                Age
                                            </td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:DropDownList ID="ddAgeFrom" runat="server">
                                                </asp:DropDownList><asp:DropDownList ID="ddAgeTo" runat="server">
                                                </asp:DropDownList></td>
                                            <td>
                                                Place of Birth</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtPOB" runat="server"></asp:TextBox></td>
                                        </tr>
                                        
                                        
                                        
                                        <tr>
                                            <td>
                                                Gender</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:DropDownList ID="ddGender" runat="server">
                                                    <asp:ListItem Value="-1">Gender</asp:ListItem>
                                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>
                                                Religion</td>
                                            <td>
                                                :</td>
                                            <td>
                                              <asp:TextBox ID="txtReligion" runat="server"></asp:TextBox></td>
                                            <td>
                                                Nationality
                                            <td>
                                                :</td>
                                            <td>
                                             <asp:DropDownList ID="ddNationality" runat="server">
                                                </asp:DropDownList></td>
                                               
                                        </tr>
                                        
                                        
                                        
                                         <tr>
                                            <td>
                                                Marital Status</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:DropDownList ID="ddMaritalStatus" runat="server" Visible="True">
                                                    <asp:ListItem Value="-1">Marital Status</asp:ListItem>
                                                    <asp:ListItem Value="Married">Married</asp:ListItem>
                                                    <asp:ListItem Value="Single">Single</asp:ListItem>
                                                    <asp:ListItem Value="Partner">Partner</asp:ListItem>
                                                    <asp:ListItem Value="Widow">Widow</asp:ListItem>
                                                    <asp:ListItem Value="Widower">Widower</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>
                                                No of Children</td>
                                            <td>
                                                :</td>
                                            <td>
                                              <asp:TextBox ID="txtChildren" runat="server"></asp:TextBox></td>
                                           
                                        </tr>
                                        
                                        
                                         <tr>
                                            <td>
                                                Email</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                                            <td>
                                                Mobile</td>
                                            <td>
                                                :</td>
                                            <td>
                                               <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox></td>
                                            <td>
                                                Work Phone
                                            </td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtWorkPhone" runat="server"></asp:TextBox></td>
                                        </tr>   
                                        
                                        
                                        
                                        <tr>
                                            <td>
                                                Current Address</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox></td>
                                                <td>
                                                City/State</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
                                            <td>
                                                Country</td>
                                            <td>
                                                :</td>
                                            <td>
                                                
                                                <asp:DropDownList ID="ddCountryRes" runat="server">
                                                </asp:DropDownList>
                                                
                                                </td>                                         
                                           
                                        </tr>
                                        <tr>
                                            <td>
                                                Home Phone</td>
                                            <td>
                                                :</td>
                                            <td>
                                             <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox></td>
                                               
                                            <td>
                                                Postal Code</td>
                                            <td>
                                                :</td>
                                            <td>
                                            <asp:TextBox ID="txtPostalCode" runat="server"></asp:TextBox>
                                               </td>
                                           
                                               
                                        </tr>
                                        <tr>
                                           
                                            <td>
                                                <asp:DropDownList ID="ddQualified" runat="server" Visible="false">
                                                    <asp:ListItem Selected="True" Value="-1">Qualified Teacher?</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList></td>
                                           
                                            <td>
                                               </td>
                                            
                                           
                                            <td>
                                                <asp:TextBox ID="txtLastContactDate" runat="server" Width="85px" Visible="false"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                           
                                            <td colspan="2">
                                                <asp:TextBox ID="txtAppliedFrom" runat="server" Width="85px" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtAppliedTo" runat="server" Width="85px" Visible="false"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="9">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                        TargetControlID="txtLastContactDate">
                                    </ajaxToolkit:CalendarExtender>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                        TargetControlID="txtAppliedFrom">
                                    </ajaxToolkit:CalendarExtender>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                        TargetControlID="txtAppliedTo">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Personal Info
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        
                          <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                       <tr>
                                            <td>
                                                Passport No</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="TextBox1" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="TextBox2" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Passport Name
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
                                          
                                        </tr>
                                        
                                        
                                         <tr>
                                            <td>
                                                Passport Issue Country</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="TextBox4" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="TextBox5" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Passport Issue Date
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>
                                                
                                                <td>
                                                Passport Expiry Date
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox></td>
                                          
                                        </tr>
                                        
                                        
                                        
                                         <tr>
                                            <td>
                                                Visa No</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="TextBox8" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="TextBox9" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Visa Issue Country
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox></td>
                                          
                                        </tr>
                                        
                                        
                                         <tr>
                                            <td>
                                                Visa Issue Place</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="TextBox11" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="TextBox12" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Visa Issue Date
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox></td>
                                                
                                                <td>
                                                Visa Expiry Date
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox></td>
                                          
                                        </tr>
                                        
                                    </table>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Passport/Visa
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        
                         <ajaxToolkit:TabPanel ID="TabPanel5" runat="server">
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                       <tr>
                                            <td>
                                                Qualification</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="TextBox15" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="TextBox16" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Specialization
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox></td>
                                          
                                        </tr>
                                        
                                        
                                         <tr>
                                            <td>
                                                Awarding Body</td>
                                            <td>
                                                :</td>
                                            <td>
                                                 <asp:TextBox ID="TextBox18" runat="server" Visible="True"></asp:TextBox>
                                                <asp:TextBox ID="TextBox19" runat="server" Visible="False"></asp:TextBox>
                                             </td>
                                            <td>
                                                Year of Qualification
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox></td>
                                                
                                                <td>
                                                Registration Ref No
                                            </td>
                                            <td>
                                                :</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox></td>
                                          
                                        </tr>
                                        
                                        
                                        
                                         
                                    </table>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Qualification
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="HT2" runat="server">
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                        
                                           
                                                <asp:DropDownList ID="ddCurriculum" runat="server" Visible="False">
                                                </asp:DropDownList>
                                            
                                                <asp:DropDownList ID="ddApplicantStage" runat="server" Visible="False">
                                                </asp:DropDownList>
                                       
                                       
                                        <tr>
                                            <td>
                                                Interested In</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel2" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                Job Type</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel3" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList2" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            
                                              <td>
                                                Available From</td>
                                            <td>
                                                :</td>
                                            <td>
                                            <asp:DropDownList ID="ddAvailableMonth" runat="server" Visible="true">
                                                </asp:DropDownList><asp:DropDownList ID="ddAvailableYear" runat="server" Visible="true">
                                                </asp:DropDownList>
                                              </td>
                                            
                                            
                                            
                                            </tr>
                                            
                                            
                                            <tr>
                                            <td>
                                                Teaching Roles</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel10" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList9" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                Curriculum</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel11" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList10" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                             <td>
                                                Subject</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="P1" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="listSubjects" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            
                                            
                                            </tr>
                                            
                                            
                                             <tr>
                                            <td>
                                                Position Level</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel12" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList11" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                Position Sector</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel13" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList12" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            
                                            
                                            
                                            </tr>
                                            
                                       
                                           
                                          
                                                <asp:Panel ID="P2" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="listAppliedFor" runat="server" Visible="False">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            
                                    </table>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                School Preference
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                      <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                            <ContentTemplate>
                                <div class="matters">
                                    <table>
                                        
                                           
                                                <asp:DropDownList ID="DropDownList1" runat="server" Visible="False">
                                                </asp:DropDownList>
                                            
                                                <asp:DropDownList ID="DropDownList2" runat="server" Visible="False">
                                                </asp:DropDownList>
                                       
                                       
                                        <tr>
                                            <td>
                                                Years of Experience</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:DropDownList ID="ddExpFrom" runat="server" Visible="true">
                                                </asp:DropDownList><asp:DropDownList ID="ddExpTo" runat="server" Visible="true">
                                                </asp:DropDownList>
                                            </td>
                                           <td>
                                                Employer</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox></td>
                                            
                                            <td>
                                                Country</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox23" runat="server"></asp:TextBox></td>
                                            </tr>
                                            
                                            
                                            
                                             <tr>
                                            <td>
                                                Title</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox26" runat="server"></asp:TextBox></td>
                                           <td>
                                                Job Type</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox24" runat="server"></asp:TextBox></td>
                                            
                                            <td>
                                                Total Salary</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox25" runat="server"></asp:TextBox></td>
                                            </tr>
                                            
                                            
                                            <tr>
                                            <td>
                                                Teaching Roles</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel6" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList5" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                Curriculum</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel7" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList6" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                             <td>
                                                Subject</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel14" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList13" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            
                                            
                                            </tr>
                                            
                                            
                                             <tr>
                                            <td>
                                                Position Level</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel15" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList14" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                Position Sector</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:Panel ID="Panel16" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList15" runat="server">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            </td>
                                            
                                            
                                            
                                            </tr>
                                            
                                       
                                            <tr>
                                            <td>
                                                Reference Name</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox27" runat="server"></asp:TextBox></td>
                                           <td>
                                                Reference Position</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox28" runat="server"></asp:TextBox></td>
                                            
                                            <td>
                                                Reference Email</td>
                                            <td>
                                                :</td>
                                            <td>
                                                <asp:TextBox ID="TextBox29" runat="server"></asp:TextBox></td>
                                            </tr>
                                            
                                          
                                                <asp:Panel ID="Panel17" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                    ScrollBars="Auto" Width="200px">
                                                    <asp:CheckBoxList ID="CheckBoxList16" runat="server" Visible="False">
                                                    </asp:CheckBoxList>
                                                </asp:Panel>
                                            
                                    </table>
                                </div>
                            </ContentTemplate>
                            <HeaderTemplate>
                                Work Experience
                            </HeaderTemplate>
                        </ajaxToolkit:TabPanel>
                       
                       
                       
                    </ajaxToolkit:TabContainer>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                        Text="Search" Width="100px" /><asp:Button ID="btnReset" runat="server" CausesValidation="False"
                            CssClass="button" Text="Reset" Width="100px" /></td>
            </tr>
        </table>
        &nbsp;&nbsp;
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="False" CollapsedSize="0" CollapsedText="Advance Search" ExpandControlID="LinkAdvanceSearch"
        ExpandedSize="650" ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>
</div>
<div class="matters">

    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Applicant View</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GrdView" runat="server" PageSize="10" OnPageIndexChanging="GrdView_PageIndexChanging"
                    AllowPaging="True" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                    AutoGenerateColumns="False" Width="100%">
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False"></RowStyle>
                    <EmptyDataRowStyle Wrap="False"></EmptyDataRowStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="App No">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            App&nbsp;No
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("APPLICATION_NO")%>
                                <asp:HiddenField ID="Hiddenappno" Value='<%#Eval("APPLICATION_NO")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="Image">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Image
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenPhotoPath" Value='<%# Eval("PHOTO_FILE_PATH") %>' runat="server" />
                            <center><asp:Image ID="ApplicantImage1"  Width="80px" Height="89px" runat="server" /></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Name
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkView" Text='<%# Eval("FULLNAME") %>' OnClientClick="javascript:return false;" 
                                    runat="server"></asp:LinkButton>
                                    <br />
                                    <br />
                                     
                                    <center>
                                           <ajaxtoolkit:rating id="CRatings" runat="server" currentrating='<%# Eval("RATINGS") %>'
                                            maxrating="5" starcssclass="ratingStar" waitingstarcssclass="savedRatingStar"
                                            filledstarcssclass="filledRatingStar" ReadOnly="true" emptystarcssclass="emptyRatingStar" />
                                    </center>
                                     <br />
                                    <br />
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Country
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("COUNTRY_RES_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Contact Date">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Last&nbsp;Contact&nbsp;Date
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbllastcontactdate" runat="server" Text='<%#Eval("LAST_CONTACT_DATE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Detail View">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Detail&nbsp;View
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkShow" Text="View" OnClientClick='<%# GetNavigateUrl(Eval("APPLICATION_NO").ToString()) %>'
                                        runat="server"></asp:LinkButton></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Curriculum Vitae">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            CV
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkShowCv" CommandName="CV" CommandArgument='<%# Eval("CV_FILE_PATH") %>'
                                        Text='<%# Eval("CV_FILE_NAME") %>' runat="server"></asp:LinkButton></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Correspondence">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Correspondence
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkCorres" CommandName="Corres" CommandArgument='<%# Eval("APPLICATION_NO") %>'
                                        Text="Correspondence" runat="server"></asp:LinkButton></center>
                                <asp:Panel ID="Show" BorderColor="Black" BackColor="#ffff99" runat="server" Height="50px"
                                    Width="200px">
                                    <asp:Label ID="lblcoress" Text='<%#Eval("LASTCORRESS")%>' runat="server"></asp:Label>
                                </asp:Panel>
                                <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" TargetControlID="LinkCorres"
                                    PopupControlID="Show" PopupPosition="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stage History">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Stage&nbsp;History
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="T5lblview" Text="History" CommandName="history" CommandArgument='<%# Eval("APPLICATION_NO") %>'
                                        runat="server"></asp:LinkButton>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Edit
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                               <center><asp:HyperLink ID="lnkEdit" runat="server">Edit</asp:HyperLink></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <HeaderTemplate>
                                <table class="BlueTable" width="100%">
                                    <tr class="matterswhite">
                                        <td align="center" colspan="2">
                                            Delete
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkDelete" Text="Delete" CommandName="Deleting" CommandArgument='<%# Eval("APPLICATION_NO") %>'
                                        runat="server"></asp:LinkButton></center>
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" TargetControlID="LinkDelete"
                                    ConfirmText="Delete this record?" runat="server">
                                </ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                   <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
                <asp:Label ID="Label1" runat="server"></asp:Label><asp:Panel Style="display: none"
                    ID="PanelStatusupdate" runat="server" CssClass="modalPopup" BackColor="white"
                    Width="325">
                    <table bordercolor="#1b80b6" cellspacing="0" cellpadding="5" width="240" border="1">
                        <tbody>
                            <tr>
                                <td class="subheader_img">
                                    History</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GrdHistory" runat="server" AutoGenerateColumns="false" Width="50px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Stage">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Stage
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhisstage" runat="server" Text='<%#Eval("STAGE_DESCRIPTION") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Date
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhisdate" runat="server" Text='<%#Eval("ENTRY_DATE")%>' Width="100px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <HeaderTemplate>
                                                    <table class="BlueTable" width="100%">
                                                        <tr class="matterswhite">
                                                            <td align="center" colspan="2">
                                                                Remarks
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhisdate" runat="server" Text='<%#Eval("REMARKS")%>' Width="140px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                       <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                    </asp:GridView>
                                    <asp:Button ID="btncancel" runat="server" Text="OK" CssClass="button" Width="80px"></asp:Button>
                                    <asp:HiddenField ID="HiddenTasklistid" runat="server"></asp:HiddenField>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" TargetControlID="Label1"
                    RepositionMode="RepositionOnWindowResizeAndScroll" PopupControlID="PanelStatusupdate"
                    DropShadow="true" CancelControlID="btncancel" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
              
            </td>
        </tr>
    </table>

</div>