Imports Oasis_Administrator
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class OASIS_HR_UserControls_CR_StageCountFilter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindControls()
        End If
    End Sub

    Public Sub BindControls()
        BindJobs()
        BindCategory()
        BindBSU()
    End Sub
    Public Sub BindJobs()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim srt_query = "SELECT DISTINCT JOB_CODE,JOB_DES FROM JOBS WHERE JOBS.JOB_STATUS ='OPEN'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        ddjob.DataTextField = "JOB_DES"
        ddjob.DataValueField = "JOB_CODE"
        ddjob.DataSource = ds
        ddjob.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "-1"
        ddjob.Items.Insert(0, list)
    End Sub

    Public Sub BindCategory()
        ddjobcategory.DataSource = OasisAdministrator.GetCategory()
        ddjobcategory.DataTextField = "CATEGORY_DES"
        ddjobcategory.DataValueField = "CATEGORY_ID"
        ddjobcategory.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "ALL"
        ddjobcategory.Items.Insert(0, list)

    End Sub
    Public Sub BindBSU()
        ddshortlistedbsu.DataSource = OasisAdministrator.Get_BSU()
        ddshortlistedbsu.DataTextField = "BSU_NAME"
        ddshortlistedbsu.DataValueField = "BSU_ID"
        ddshortlistedbsu.DataBind()
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "ALL"
        list1.Selected = True
        ddshortlistedbsu.Items.Insert(0, list1)

    End Sub

    Protected Sub btnview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnview.Click
        Dim param As New Hashtable
        If ddjob.SelectedIndex > 0 Then
            param.Add("@JOB_CODE", ddjob.SelectedValue)
        Else
            param.Add("@JOB_CODE", DBNull.Value)
        End If

        If ddjobcategory.SelectedIndex > 0 Then
            param.Add("@JOB_CATEGORY_ID", ddjobcategory.SelectedValue)
        Else
            param.Add("@JOB_CATEGORY_ID", DBNull.Value)
        End If

        If ddshortlistedbsu.SelectedIndex > 0 Then
            param.Add("@SHORTLISTED_BSU_ID", ddshortlistedbsu.SelectedValue)
        Else
            param.Add("@SHORTLISTED_BSU_ID", DBNull.Value)
        End If

        If txtfrom.Text.Trim <> "" Then
            param.Add("@FROMDATE", txtfrom.Text.Trim())
        Else
            param.Add("@FROMDATE", DBNull.Value)
        End If
        If txtto.Text.Trim <> "" Then
            param.Add("@TODATE", txtto.Text.Trim())
        Else
            param.Add("@TODATE", DBNull.Value)
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_HR"
            .reportParameters = param
            .reportPath = Server.MapPath("~/OASIS_HR/Reports/CR_StageCounts.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
