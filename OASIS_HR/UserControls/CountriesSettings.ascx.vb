Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class OASIS_HR_UserControls_CountriesSettings
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindControls()
        End If
    End Sub

    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim strQuery As String = "Select * from Country order by COUNTRY "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        CheckListCountries.DataSource = ds
        CheckListCountries.DataValueField = "COUNTRY_ID"
        CheckListCountries.DataTextField = "COUNTRY"
        CheckListCountries.DataBind()
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim val = ds.Tables(0).Rows(i).Item("COUNTRY_ID").ToString()
            Dim active = ds.Tables(0).Rows(i).Item("ACTIVE")
            For Each item As ListItem In CheckListCountries.Items
                If item.Value = val Then
                    item.Selected = active
                End If
            Next
        Next


    End Sub

   

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim flag = 0
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim strQuery As String = ""
        For Each item As ListItem In CheckListCountries.Items
            If item.Selected Then
                strQuery = "Update COUNTRY set ACTIVE='True' where COUNTRY_ID='" & item.Value & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
            Else
                'strQuery = "Select * from  APPLICATION_COUNTRY_WILLING where COUNTRY_ID='" & item.Value & "'"
                'Dim ds As DataSet
                'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
                'If ds.Tables(0).Rows.Count = 0 Then
                strQuery = "Update COUNTRY set ACTIVE='False' where COUNTRY_ID='" & item.Value & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
                'Else
                '    flag = 1
                'End If

            End If
        Next
        If flag = 0 Then
            lblmessage.Text = "Updated"
        Else
            lblmessage.Text = "Some Countries cannot be removed. They are used by Applicants."
        End If

        BindControls()
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim strQuery As String = "Select * from Country where COUNTRY like '%" & txtSearch.Text.Trim() & "%' order by COUNTRY "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        CheckListCountries.DataSource = ds
        CheckListCountries.DataValueField = "COUNTRY_ID"
        CheckListCountries.DataTextField = "COUNTRY"
        CheckListCountries.DataBind()
        Dim i = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim val = ds.Tables(0).Rows(i).Item("COUNTRY_ID").ToString()
            Dim active = ds.Tables(0).Rows(i).Item("ACTIVE")
            For Each item As ListItem In CheckListCountries.Items
                If item.Value = val Then
                    item.Selected = active
                End If
            Next
        Next
        txtSearch.Text = ""
    End Sub
End Class
