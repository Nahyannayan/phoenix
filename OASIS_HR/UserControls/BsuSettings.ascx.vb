Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class OASIS_HR_UserControls_BsuSettings
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindControls()
        End If
    End Sub

    Public Sub BindControls()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strQuery As String = "Select BSU_ID,BSU_NAME from BUSINESSUNIT_M order by BSU_NAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        CheckListBsu.DataSource = ds
        CheckListBsu.DataValueField = "BSU_ID"
        CheckListBsu.DataTextField = "BSU_NAME"
        CheckListBsu.DataBind()

        Dim i = 0
        str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        For Each item As ListItem In CheckListBsu.Items
            strQuery = "select count(*) as bsuidcount from bsu where bsu_id='" & item.Value & "'"
            Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
            If val > 0 Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next


    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strQuery As String = "Select BSU_ID,BSU_NAME from BUSINESSUNIT_M where BSU_NAME like '%" & txtSearch.Text.Trim() & "%'  order by BSU_NAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        CheckListBsu.DataSource = ds
        CheckListBsu.DataValueField = "BSU_ID"
        CheckListBsu.DataTextField = "BSU_NAME"
        CheckListBsu.DataBind()

        Dim i = 0
        str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        For Each item As ListItem In CheckListBsu.Items
            strQuery = "select count(*) as bsuidcount from bsu where bsu_id='" & item.Value & "'"
            Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, strQuery)
            If val > 0 Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next
        txtSearch.Text = ""
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim flag = 0
        For Each item As ListItem In CheckListBsu.Items
            If item.Selected = True Then
                '' Insert 
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", item.Value)
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_BSU", pParms)
            Else
                ''Delete
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", item.Value)
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
                Dim val = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_UPDATE_BSU", pParms)
                If val = 1 Then
                    flag = 1
                End If
            End If
        Next
        If flag = 1 Then
            lblmessage.Text = "Some BSU cannot be removed. They are used by Applicants."
        Else
            lblmessage.Text = "Updated Successfully"
        End If
        BindControls()
    End Sub
End Class
