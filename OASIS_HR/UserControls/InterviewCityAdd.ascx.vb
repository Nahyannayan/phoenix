Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports Oasis_Administrator

Partial Class OASIS_HR_UserControls_InterviewCityAdd
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindSource()
            CityBind()
        End If
      
    End Sub
    Public Sub BindSource()
        Dim ds As DataSet
        ds = OasisAdministrator.GetSource()
        If ds.Tables(0).Rows.Count > 0 Then
            ddSource.DataSource = ds
            ddSource.DataTextField = "SOURCE_DES"
            ddSource.DataValueField = "SOURCE_ID"
            ddSource.DataBind()

            Dim list As New ListItem
            list.Text = "Select Source"
            list.Value = "-1"

            ddSource.Items.Insert(0, list)
        End If
    End Sub

    Public Sub CityBind()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim strQuery As String = "Select CITY_ID,CITY_DES from CITIES order by CITY_DES "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        If ds.Tables(0).Rows.Count > 0 Then
            ddcities.DataSource = ds
            ddcities.DataTextField = "CITY_DES"
            ddcities.DataValueField = "CITY_ID"
            ddcities.DataBind()
        End If

    End Sub

    Public Sub BindInterviewCities()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
        pParms(1) = New SqlClient.SqlParameter("@SOURCE_ID", Convert.ToInt32(ddSource.SelectedValue))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_DELETE_INTERVIEW_CITIES", pParms)
        GridInterviews.DataSource = ds
        GridInterviews.DataBind()

    End Sub
    Protected Sub ddSource_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddSource.SelectedIndexChanged
        BindInterviewCities()
    End Sub

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        If ddSource.SelectedIndex > 0 Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 2)
            pParms(1) = New SqlClient.SqlParameter("@SOURCE_ID", Convert.ToInt32(ddSource.SelectedValue))
            pParms(2) = New SqlClient.SqlParameter("@CITY_ID", Convert.ToInt32(ddcities.SelectedValue))
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_DELETE_INTERVIEW_CITIES", pParms)

        End If
        BindInterviewCities()
    End Sub

    Protected Sub GridInterviews_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridInterviews.RowCommand
        Dim Id As Integer = Convert.ToInt16(e.CommandArgument)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", 3)
        pParms(1) = New SqlClient.SqlParameter("@RECORD_ID", Id)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_DELETE_INTERVIEW_CITIES", pParms)
        BindInterviewCities()
    End Sub
End Class

