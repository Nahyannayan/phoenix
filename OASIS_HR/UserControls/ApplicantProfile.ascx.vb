Imports Oasis_Administrator
Imports System.Data
Imports System.Diagnostics
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class OASIS_HR_UserControls_ApplicantProfile
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                HiddenApplicationNo.Value = Request.QueryString("Application_No").Trim()
                BindControls(Request.QueryString("Application_No").Trim())
                BindNotes()
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
                Response.Cache.SetAllowResponseInBrowserHistory(False)
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub BindControls(ByVal Applicant_No As String)
        Dim ds As DataSet
        ds = OasisAdministrator.GetApplicantInfo(Applicant_No, 2)
        lbladditionalinfo.Text = ds.Tables(0).Rows(0).Item("ADDITIONAL_INFORMATION").ToString()
        lbladdress.Text = ds.Tables(0).Rows(0).Item("ADDRESS").ToString()
        lblavailabledate.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_DATE").ToString()
        lblcountryissuepassport.Text = ds.Tables(0).Rows(0).Item("COUNTRY_PASS_ISS_NAME").ToString()
        lblcountryofresidence.Text = ds.Tables(0).Rows(0).Item("NATIONALITY").ToString()
        lbldob.Text = ds.Tables(0).Rows(0).Item("DATE_OF_BIRTH").ToString()
        Dim val As String = ds.Tables(0).Rows(0).Item("EMAIL").ToString()
        lblemail.Text = "<a href='mailto:" & val & "'>" & val & "</a>"
        lblentrydate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ENTRY_DATE").ToString()).ToString("dd/MMM/yyyy")
        lblexperience.Text = ds.Tables(0).Rows(0).Item("EXPERIENCE").ToString()
        lblhomephone.Text = ds.Tables(0).Rows(0).Item("HOME_PHONE").ToString()
        lblmaritalstatus.Text = ds.Tables(0).Rows(0).Item("MARITAL_STATUS").ToString()
        lblmobile.Text = ds.Tables(0).Rows(0).Item("MOBILE").ToString()
        lblname.Text = ds.Tables(0).Rows(0).Item("FIRST_NAME").ToString() & " " & ds.Tables(0).Rows(0).Item("SUR_NAME").ToString()
        lblnoofchildren.Text = ds.Tables(0).Rows(0).Item("NO_OF_CHILDREN").ToString()
        lblofficephone.Text = ds.Tables(0).Rows(0).Item("OFFICE_PHONE").ToString()
        lblpostalcode.Text = ds.Tables(0).Rows(0).Item("POSTAL_CODE").ToString()
        lblqualified.Text = ds.Tables(0).Rows(0).Item("QUALIFIED_TEACHER").ToString()
        If lblqualified.Text = "True" Then
            lblqualified.Text = "Yes"
        Else
            lblqualified.Text = "No"
        End If

        lblqualifiedyear.Text = ds.Tables(0).Rows(0).Item("QUALIFIED_YEAR").ToString()
        'lblrelocate.Text = ds.Tables(0).Rows(0).Item("RE_ALLOCATION").ToString()
        'If lblrelocate.Text = "True" Then
        '    lblrelocate.Text = "Yes"
        'Else
        '    lblrelocate.Text = "No"
        'End If
        lbltown.Text = ds.Tables(0).Rows(0).Item("TOWN").ToString()

        Dim PhotoVirtualpath = Web.Configuration.WebConfigurationManager.AppSettings("UploadPhotoPathVirtual").ToString()

        ImgApplicant.ImageUrl = PhotoVirtualpath + ds.Tables(0).Rows(0).Item("PHOTO_FILE_PATH").ToString()

        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadCVPath").ToString()
        HiddenCvPath.Value = cvVirtualPath + ds.Tables(0).Rows(0).Item("CV_FILE_PATH").ToString()


        lbltitle.Text = ds.Tables(0).Rows(0).Item("TITLE").ToString()
        lblApplicationNo.Text = ds.Tables(0).Rows(0).Item("APPLICATION_NO").ToString()

        lblAge.Text = DateDiff(DateInterval.Year, Convert.ToDateTime(lbldob.Text), Today.Date, Microsoft.VisualBasic.FirstDayOfWeek.Monday)
        Dim Gender As String = ds.Tables(0).Rows(0).Item("GENDER").ToString()
        If Gender = "M" Then
            lblGender.Text = "Male"
        Else
            lblGender.Text = "Female"
        End If

        lblPassportName.Text = ds.Tables(0).Rows(0).Item("PASSPORT_NAME").ToString()
        lblPassportNumber.Text = ds.Tables(0).Rows(0).Item("PASSPORT_NUMBER").ToString()
        lblPassportIssuePlace.Text = ds.Tables(0).Rows(0).Item("PASSPORT_ISSUE_PLACE").ToString()
        lblPassportIssueDate.Text = ds.Tables(0).Rows(0).Item("PASSPORT_ISSUE_DATE").ToString()
        lblPassportExpiryDate.Text = ds.Tables(0).Rows(0).Item("PASSPORT_EXPIRY_DATE").ToString()

        lblVisaNumber.Text = ds.Tables(0).Rows(0).Item("VISA_NO").ToString()
        lblVisaIssuePlace.Text = ds.Tables(0).Rows(0).Item("VISA_ISSUE_PLACE").ToString()
        lblVisaIssueDate.Text = ds.Tables(0).Rows(0).Item("VISA_ISSUE_DATE").ToString()
        lblVisaExpirtyDate.Text = ds.Tables(0).Rows(0).Item("VISA_EXPIRY_DATE").ToString()
        lblCountry.Text = ds.Tables(0).Rows(0).Item("COUNTRY_RES_NAME").ToString()
        CRatings.CurrentRating = ds.Tables(0).Rows(0).Item("RATINGS").ToString()


        BindBsu(Applicant_No, ds)
        BindEducation(Applicant_No, ds)
        BindCountriesWilling(Applicant_No, ds)
        BindSource(Applicant_No, ds)
        BindSubject(Applicant_No, ds)
        BindCategory(Applicant_No, ds)
        BindReference(Applicant_No, ds)
        BindWorkExperience(Applicant_No)
    End Sub
    Public Sub BindWorkExperience(ByVal Application_No As String)
        GrdWorkExp.DataSource = OasisAdministrator.GetWorkExperienceInfo(Application_No)
        GrdWorkExp.DataBind()
    End Sub
    Public Sub BindBsu(ByVal Application_No As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantInfo(Application_No, 3)
        Dim val As String = ""
        Dim i = 0
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = 0 Then
                    val = ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString()
                Else
                    val = val & "," & ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString()
                End If
            Next
        End If
        lblschoolsinterested.Text = val
        'GrdBsu.DataSource = ds
        'GrdBsu.DataBind()
    End Sub
    Public Sub BindEducation(ByVal Application_NO As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantInfo(Application_NO, 5)
        GrdEducation.DataSource = ds
        GrdEducation.DataBind()
    End Sub
    Public Sub BindCountriesWilling(ByVal Application_NO As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantInfo(Application_NO, 4)
        Dim val As String = ""
        Dim i = 0
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = 0 Then
                    val = ds.Tables(0).Rows(i).Item("COUNTRY").ToString()
                Else
                    val = val & "," & ds.Tables(0).Rows(i).Item("COUNTRY").ToString()
                End If
            Next
        End If
        lbPrefferedCountries.Text = val
        'GrdCountriesWilling.DataSource = ds
        'GrdCountriesWilling.DataBind()
    End Sub
    Public Sub BindSource(ByVal Application_NO As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantInfo(Application_NO, 7)
        Dim val As String = ""
        Dim i = 0
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = 0 Then
                    val = ds.Tables(0).Rows(i).Item("SOURCE_DES").ToString()
                Else
                    val = val & "," & ds.Tables(0).Rows(i).Item("SOURCE_DES").ToString()
                End If
            Next
        End If
        lblKnownFrom.Text = val
        'GrdSource.DataSource = ds
        'GrdSource.DataBind()
    End Sub
    Public Sub BindSubject(ByVal Application_NO As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantInfo(Application_NO, 8)
        Dim val As String = ""
        Dim i = 0
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = 0 Then
                    val = ds.Tables(0).Rows(i).Item("SUBJECT").ToString() & "-(" & ds.Tables(0).Rows(i).Item("STATUS").ToString() & ")"
                Else
                    val = val & "," & ds.Tables(0).Rows(i).Item("SUBJECT").ToString() & "-(" & ds.Tables(0).Rows(i).Item("STATUS").ToString() & ")"
                End If
            Next
        End If
        lblSubjects.Text = val
        'GrdSubjects.DataSource = ds
        'GrdSubjects.DataBind()
    End Sub
    Public Sub BindCategory(ByVal Application_NO As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantCategory(Application_NO)
        Dim val As String = ""
        Dim i = 0
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i = 0 Then
                    val = ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString()
                Else
                    val = val & "," & ds.Tables(0).Rows(i).Item("CATEGORY_DES").ToString()
                End If
            Next
        End If
        lblcategories.Text = val
        'GrdCategory.DataSource = ds
        'GrdCategory.DataBind()
    End Sub
    Public Sub BindReference(ByVal Application_NO As String, ByVal ds As DataSet)
        ds = Nothing
        ds = OasisAdministrator.GetApplicantInfo(Application_NO, 6)
        Grdreference.DataSource = ds
        Grdreference.DataBind()
    End Sub
    Public Sub PrintCv()

        Try

            Dim path = HiddenCvPath.Value
            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()


        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ImagePrintCv_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagePrintCv.Click
        PrintCv()
    End Sub
    Public Sub BindNotes()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim str_query = "SELECT NOTES,ENTRY_DATE FROM APPLICANT_NOTES WHERE APPLICATION_NO='" & HiddenApplicationNo.Value & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            T5Notes.Text = ds.Tables(0).Rows(0).Item("NOTES").ToString()
            T1.InnerHtml = T5Notes.Text
        End If

    End Sub
    Protected Sub T5btnnotessave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles T5btnnotessave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@APPLICATION_NO", HiddenApplicationNo.Value)
        pParms(1) = New SqlClient.SqlParameter("@NOTES ", T5Notes.Text.Trim())
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_APPLICANT_NOTES", pParms)
        T5Notes.Text = ""
        BindNotes()
    End Sub
End Class
