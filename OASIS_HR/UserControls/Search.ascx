<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Search.ascx.vb" Inherits="UserControls_Search" %>

<%--     <asp:Panel ID="Panel1" CssClass="subheader_img" runat="server" Height="50px" Width="100%">
             <br />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></asp:Panel>--%>
            <script type="text/javascript" >
        function getDate(a)
        {
      
                var sFeatures;
                sFeatures="dialogWidth: 227px; ";
                sFeatures+="dialogHeight: 252px; ";
                sFeatures+="help: no; ";
                sFeatures+="resizable: no; ";
                sFeatures+="scroll: no; ";
                sFeatures+="status: no; ";
                sFeatures+="unadorned: no; ";
                var NameandCode;
                var result;
                var nofuture="../../Accounts/calendar.aspx";
                result = window.showModalDialog(nofuture,"", sFeatures)
                if (typeof result != 'undefined')
                {
                    if (a==1)
                    {
                        document.getElementById("<%=txtLastContactDate.ClientID %>").value=result;
                    }
                    if (a==2)
                    {
                        document.getElementById("<%=txtFromDate.ClientID %>").value=result;
                    }
                    if (a==3)
                    {
                        document.getElementById("<%=txtToDate.ClientID %>").value=result;
                    }
                    
                }
                
               return false;
        }
        
        </script>
        
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td align="left">
                    <br />
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                    <asp:LinkButton id="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton></td>
            </tr>
        </table>
        <br />
        <asp:Panel ID="Panel2"  BackColor="WhiteSmoke"   runat="server" Height="50px" Width="100%">
            <table border="1" bordercolor="#1b80b6" cellpadding="2" cellspacing="0" class="matters">
                <tr class="subheader_img" >
                    <td align="left"  colspan="4">
                        Search</td>
                </tr>
                <tr>
                    <td >
                    <asp:TextBox ID="txtSApplication_No" runat="server"></asp:TextBox></td>
                    <td >
                    <asp:TextBox ID="txtSFirstName" runat="server"></asp:TextBox></td>
                    <td >
                    <asp:TextBox ID="txtSMiddleName" runat="server"></asp:TextBox></td>
                    <td >
                    <asp:TextBox ID="txtSPostalCode" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td >
                    <asp:TextBox ID="txtSHomePhone" runat="server"></asp:TextBox></td>
                    <td >
                    <asp:TextBox ID="txtSMobile" runat="server"></asp:TextBox></td>
                    <td >
                    <asp:TextBox ID="txtSEmail" runat="server"></asp:TextBox></td>
                    <td >
                    <%--<asp:TextBox ID="txtSAvailableDate" runat="server"></asp:TextBox>--%>
                     <table border="0" cellpadding="0" cellspacing="0">
                      <tr><td colspan="2" align="center"> Available On</td> </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddMonth" runat="server">
                                                    </asp:DropDownList></td>
                                                <td>
                                                    <asp:DropDownList ID="ddYear" runat="server">
                                                    </asp:DropDownList>&nbsp;<%--<img id="img2" src="../Images/calendar.gif" OnClientClick="getDate('3');return false;" />--%></td>
                                            </tr>
                                        </table>
                    
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table border="1"  cellpadding="2" cellspacing="0" class="matters" width="100%">
                            <tr>
                                <td >
                    <asp:DropDownList ID="ddSTown" runat="server">
                    </asp:DropDownList></td>
                                <td >
                    <asp:DropDownList ID="ddSCountryOfResidence" runat="server">
                    </asp:DropDownList></td>
                                <td >
                    <asp:DropDownList ID="ddSQualifiedTeacher" runat="server">
                        <asp:ListItem Selected="True" Value="-1">Qualified Teacher?</asp:ListItem>
                        <asp:ListItem Value="0">Yes</asp:ListItem>
                        <asp:ListItem Value="1">No</asp:ListItem>
                    </asp:DropDownList></td>
                                <td >
                    <asp:DropDownList ID="ddSExperience" runat="server">
                    </asp:DropDownList></td>
                                <td >
                    <asp:DropDownList ID="ddSMaritalStatus" runat="server">
                    <asp:ListItem Value="-1">Marital Status</asp:ListItem>
                    <asp:ListItem Value="0">Married</asp:ListItem>
                    <asp:ListItem Value="1">Single</asp:ListItem>
                    <asp:ListItem Value="2">Partner</asp:ListItem>
                    <asp:ListItem Value="3">Widow</asp:ListItem>
                    <asp:ListItem Value="4">Widower</asp:ListItem>
                    </asp:DropDownList></td>
                            </tr>
                        </table>
                        </td>
                </tr>
                <tr>
                    <td colspan="4" align="left">
                        <table border="1"  cellpadding="2" cellspacing="0" class="matters">
                            <tr>
                                <td >
                    <asp:DropDownList ID="ddSCategory" runat="server">
                    </asp:DropDownList></td>
                                <td >
                        <asp:DropDownList ID="ddJobCode" runat="server">
                        </asp:DropDownList></td>
                                <td style="width: 85px" >
                        <asp:DropDownList ID="ddapplicantstage" runat="server">
                        </asp:DropDownList></td>
                                <td style="width: 85px">
                                    <asp:DropDownList ID="DDSSubject" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td >
                        <table border="0" cellpadding="0" cellspacing="0" class="matters" >
                            <tr>
                                <td colspan="2">
                                    Last Contacted Date</td>
                            </tr>
                            <tr>
                                <td >
                        <asp:TextBox ID="txtLastContactDate" runat="server"></asp:TextBox></td>
                                <td >
                                    &nbsp;<asp:ImageButton ID="ImageButton1"
                                runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;" /><%--<img id="img3" src="../Images/calendar.gif" />--%></td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="3">
                        <table border="0" cellpadding="0" cellspacing="0" class="matters">
                            <tr>
                                <td colspan="4" >
                        Applied Between</td>
                            </tr>
                            <tr>
                                <td >
                                    From&nbsp;
                                </td>
                                <td >
                        <table border="0" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td >
                        <asp:TextBox ID="txtFromDate" runat="server" ValidationGroup="Search"></asp:TextBox></td>
                                <td >
                                    &nbsp;<asp:ImageButton ID="ImageButton2"
                                runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;" /><%--<img id="img1" src="../Images/calendar.gif" />--%></td>
                            </tr>
                        </table>
                                </td>
                                <td >
                                    &nbsp;To &nbsp;</td>
                                <td >
                        <table border="0" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td >
                        <asp:TextBox ID="txtToDate" runat="server" ValidationGroup="Search"></asp:TextBox></td>
                                <td >
                                    &nbsp;<asp:ImageButton ID="ImageButton3"
                                runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;" /><%--<img id="img2" src="../Images/calendar.gif" />--%></td>
                            </tr>
                        </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" CausesValidation="False" Width="100px" /><asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="button" CausesValidation="False" Width="100px" /></td>
                </tr>
            </table>
        </asp:Panel>
    
        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1"  TargetControlID="Panel2" CollapsedSize="0" ExpandedSize="240"
    Collapsed="False" ExpandControlID="LinkAdvanceSearch" CollapseControlID="LinkAdvanceSearch"
    AutoCollapse="False" AutoExpand="False" ScrollContents="false"  TextLabelID="LinkAdvanceSearch" CollapsedText="Advance Search" ExpandedText="Hide Search" runat="server">
        </ajaxToolkit:CollapsiblePanelExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
    TargetControlID="txtFromDate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton3"
    TargetControlID="txtToDate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
    TargetControlID="txtLastContactDate">
</ajaxToolkit:CalendarExtender>
<table>
    <tr>
        <td style="width: 100px; height: 21px">
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                TargetControlID="txtSApplication_No" WatermarkCssClass="watermarked" WatermarkText="Application No">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                TargetControlID="txtSFirstName" WatermarkCssClass="watermarked" WatermarkText="First Name">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                TargetControlID="txtSMiddleName" WatermarkCssClass="watermarked" WatermarkText="Last Name">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server"
                TargetControlID="txtSPostalCode" WatermarkCssClass="watermarked" WatermarkText="Postal Code">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server"
                TargetControlID="txtSHomePhone" WatermarkCssClass="watermarked" WatermarkText="Home Phone">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server"
                TargetControlID="txtSMobile" WatermarkCssClass="watermarked" WatermarkText="Mobile">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server"
                TargetControlID="txtSEmail" WatermarkCssClass="watermarked" WatermarkText="Email">
            </ajaxToolkit:TextBoxWatermarkExtender>
            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server"
                TargetControlID="txtSAvailableDate"  WatermarkText="Available Date">
            </ajaxToolkit:TextBoxWatermarkExtender>--%>
            &nbsp; &nbsp;
        </td>
    </tr>
</table>
   