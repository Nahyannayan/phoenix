Imports Oasis_Administrator
Imports System.Data
Partial Class OASIS_HR_UserControls_ApplicantViewSearch
    Inherits System.Web.UI.UserControl

    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataTable)

    ' Event declaration 
    Public Event SearchFilter As OnButtonClick


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindSearchControls()
                ''ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSearch)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub BindSearchControls()
        Dim i = 0
        For i = 1 To 40
            ddSExperience.Items.Add(i)
        Next
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "Experience?"
        ddSExperience.Items.Insert(0, list1)


        ddSCountryOfResidence.DataSource = OasisAdministrator.Countries()
        ddSCountryOfResidence.DataTextField = "COUNTRY"
        ddSCountryOfResidence.DataValueField = "COUNTRY_ID"
        ddSCountryOfResidence.DataBind()
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Select Country"
        ddSCountryOfResidence.Items.Insert(0, list2)

        

        DDSSubject.DataSource = OasisAdministrator.GetSubject()
        DDSSubject.DataTextField = "SUBJECT"
        DDSSubject.DataValueField = "SUBJECT_ID"
        DDSSubject.DataBind()
        Dim list7 As New ListItem
        list7.Value = "-1"
        list7.Text = "Select Subject"
        DDSSubject.Items.Insert(0, list7)
        BindAvailabledate()

        DDSClm.DataSource = OasisAdministrator.GetClm()
        DDSClm.DataTextField = "CURRICULUM"
        DDSClm.DataValueField = "CLM_ID"
        DDSClm.DataBind()
        Dim list8 As New ListItem
        list8.Value = "-1"
        list8.Text = "Curriculum"
        DDSClm.Items.Insert(0, list8)

    End Sub
    Public Sub BindAvailabledate()
        ''Month
        ddMonth.Items.Add("Jan")
        ddMonth.Items.Add("Feb")
        ddMonth.Items.Add("Mar")
        ddMonth.Items.Add("Apr")
        ddMonth.Items.Add("May")
        ddMonth.Items.Add("Jun")
        ddMonth.Items.Add("Jul")
        ddMonth.Items.Add("Aug")
        ddMonth.Items.Add("Sep")
        ddMonth.Items.Add("Oct")
        ddMonth.Items.Add("Nov")
        ddMonth.Items.Add("Dec")
        Dim list1 As New ListItem
        list1.Value = "-1"
        list1.Text = "Month"
        ddMonth.Items.Insert(0, list1)

        ''Year
        Dim i = Today.Year
        For i = Today.Year To Today.Year + 5
            ddYear.Items.Add(i)
        Next
        Dim list2 As New ListItem
        list2.Value = "-1"
        list2.Text = "Year"
        ddYear.Items.Insert(0, list2)

    End Sub
    Public Sub search()
        Dim dt As New DataTable

        Try
            dt.Columns.Add("ApplicationNo")
            dt.Columns.Add("FirstName")
            dt.Columns.Add("MiddleName")
            dt.Columns.Add("Town")
            dt.Columns.Add("PostalCode")
            dt.Columns.Add("CountryOfResidence")
            dt.Columns.Add("HomePhone")
            dt.Columns.Add("Mobile")
            dt.Columns.Add("Email")
            dt.Columns.Add("AvailableDate")
            dt.Columns.Add("QualifiedTeacher")
            dt.Columns.Add("Qualify")
            dt.Columns.Add("Experience")
            dt.Columns.Add("Category_ID")
            dt.Columns.Add("MaritalStatus")
            dt.Columns.Add("Last_Contact_Date")
            dt.Columns.Add("From_Date")
            dt.Columns.Add("To_Date")
            dt.Columns.Add("Job_code")
            dt.Columns.Add("Applicant_Stage")
            dt.Columns.Add("sp")
            dt.Columns.Add("subject_id")
            dt.Columns.Add("Clm")
            dt.Columns.Add("Ratings")

            Dim QualifiedTeacher As String
            Dim AvailableDate As String = ""


            Dim dr As DataRow = dt.NewRow


            If txtSApplication_No.Text.Trim() <> "" Then
                dr.Item("ApplicationNo") = txtSApplication_No.Text.Trim()

            Else
                dr.Item("ApplicationNo") = Nothing
            End If

            If txtSFirstName.Text.Trim() <> "" Then
                dr.Item("FirstName") = txtSFirstName.Text.Trim()
            Else
                dr.Item("FirstName") = Nothing
            End If


            If txtSMiddleName.Text.Trim() <> "" Then
                dr.Item("MiddleName") = txtSMiddleName.Text.Trim()
            Else
                dr.Item("MiddleName") = Nothing
            End If



            dr.Item("Town") = Nothing


            If txtSPostalCode.Text.Trim() <> "" Then
                dr.Item("PostalCode") = txtSPostalCode.Text.Trim()
            Else
                dr.Item("PostalCode") = Nothing
            End If


            If ddSCountryOfResidence.SelectedValue <> "-1" Then
                dr.Item("CountryOfResidence") = ddSCountryOfResidence.SelectedValue
            Else
                dr.Item("CountryOfResidence") = Nothing
            End If


            If txtSHomePhone.Text.Trim() <> "" Then
                dr.Item("HomePhone") = txtSHomePhone.Text.Trim()
            Else
                dr.Item("HomePhone") = Nothing
            End If




            If txtSMobile.Text.Trim() <> "" Then
                dr.Item("Mobile") = txtSMobile.Text.Trim()
            Else
                dr.Item("Mobile") = Nothing
            End If



            If txtSEmail.Text.Trim() <> "" Then
                dr.Item("Email") = txtSEmail.Text.Trim()
            Else
                dr.Item("Email") = Nothing
            End If


            If ddMonth.SelectedValue <> "-1" Then

                AvailableDate = ddMonth.SelectedItem.Text
                dr.Item("AvailableDate") = ddMonth.SelectedItem.Text
            End If


            If ddYear.SelectedValue <> "-1" Then
                dr.Item("AvailableDate") = AvailableDate & "/" & ddYear.SelectedItem.Text
            End If

            If ddMonth.SelectedValue = "-1" And ddYear.SelectedValue = "-1" Then
                dr.Item("AvailableDate") = Nothing
            End If


            If ddSQualifiedTeacher.SelectedValue <> "-1" Then
                QualifiedTeacher = ddSQualifiedTeacher.SelectedItem.Text.Trim()
                If QualifiedTeacher = "Yes" Then
                    dr.Item("Qualify") = 1
                Else
                    dr.Item("Qualify") = 2
                End If
                dr.Item("QualifiedTeacher") = ddSQualifiedTeacher.SelectedItem.Text.Trim()
            Else
                dr.Item("QualifiedTeacher") = Nothing
            End If


            If ddSExperience.SelectedValue <> "-1" Then
                dr.Item("Experience") = ddSExperience.SelectedItem.Text.Trim()
            Else
                dr.Item("Experience") = Nothing
            End If

  
            dr.Item("Category_ID") = Nothing




            If ddSMaritalStatus.SelectedValue <> "-1" Then
                dr.Item("MaritalStatus") = ddSMaritalStatus.SelectedItem.Text.Trim()
            Else
                dr.Item("MaritalStatus") = Nothing
            End If


 
            dr.Item("Last_Contact_Date") = Nothing


            If txtFromDate.Text.Trim() <> "" Then
                dr.Item("From_Date") = Convert.ToDateTime(txtFromDate.Text.Trim()).ToString("MM/dd/yyyy")
            Else
                dr.Item("From_Date") = Nothing
            End If

            If txtToDate.Text.Trim() <> "" Then
                dr.Item("To_Date") = Convert.ToDateTime(txtToDate.Text.Trim()).ToString("MM/dd/yyyy")
            Else
                dr.Item("To_Date") = Nothing
            End If


  
            dr.Item("Job_code") = Nothing



            dr.Item("Applicant_Stage") = Nothing



            If DDSSubject.SelectedValue <> "-1" Then
                dr.Item("subject_id") = DDSSubject.SelectedValue
            Else
                dr.Item("subject_id") = Nothing
            End If


            If DDSClm.SelectedValue <> "-1" Then
                dr.Item("Clm") = DDSClm.SelectedItem.Text
            Else
                dr.Item("Clm") = Nothing
            End If

            If DDSRatings.SelectedValue <> "-1" Then
                dr.Item("Ratings") = DDSRatings.SelectedItem.Text
            Else
                dr.Item("Ratings") = Nothing
            End If

            dr.Item("sp") = Session("ViewSearch")

            'ds = OasisAdministrator.SearchApplicantView(ApplicationNo, FirstName, MiddleName, Nothing, Town, PostalCode, Convert.ToInt16(CountryOfResidence), HomePhone, Mobile, Email, AvailableDate, Qualify, Convert.ToInt32(Experience), Convert.ToInt32(Category_ID), MaritalStatus, Last_Contact_Date, From_Date, To_Date, Job_code, Applicant_Stage, sp, subject_id, Clm, Ratings)
            'RaiseEvent btnHandler(ds)
            dt.Rows.Add(dr)
            RaiseEvent SearchFilter(dt)

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        search()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtSApplication_No.Text = ""
        txtSFirstName.Text = ""
        ddMonth.SelectedValue = "-1"
        ddYear.SelectedValue = "-1"
        txtSEmail.Text = ""
        txtSHomePhone.Text = ""
        txtSMiddleName.Text = ""
        txtSMobile.Text = ""
        txtSPostalCode.Text = ""
        ddSCountryOfResidence.SelectedValue = "-1"
        ddSExperience.SelectedValue = "-1"
        ddSMaritalStatus.SelectedValue = "-1"
        ddSQualifiedTeacher.SelectedValue = "-1"
        txtFromDate.Text = ""
        txtToDate.Text = ""
        DDSSubject.SelectedValue = "-1"
        DDSClm.SelectedValue = "-1"
        DDSRatings.SelectedValue = "-1"
        ''search()
    End Sub
End Class
