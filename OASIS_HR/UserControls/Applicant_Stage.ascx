<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Applicant_Stage.ascx.vb" Inherits="OASIS_HR_UserControls_Applicant_Stage" %>
  <div class="matters">
        <span>
            <div class="matters">
                <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton>
                <asp:Panel ID="Panel1" runat="server">
                    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
                        <tr>
                            <td class="subheader_img">
                                Search</td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;<ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                                    <ajaxToolkit:TabPanel ID="HT1" runat="server">
                                        <HeaderTemplate>
                                            Basic Search
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <div class="matters">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Application No</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            First Name
                                                        </td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Last Name</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Gender</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddGender" runat="server">
                                                                <asp:ListItem Value="-1">Gender</asp:ListItem>
                                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Age</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddAgeFrom" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddAgeTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Marital Status
                                                        </td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddMaritalStatus" runat="server">
                                                                <asp:ListItem Value="-1">Marital Status</asp:ListItem>
                                                                <asp:ListItem Value="Married">Married</asp:ListItem>
                                                                <asp:ListItem Value="Single">Single</asp:ListItem>
                                                                <asp:ListItem Value="Partner">Partner</asp:ListItem>
                                                                <asp:ListItem Value="Widow">Widow</asp:ListItem>
                                                                <asp:ListItem Value="Widower">Widower</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Postal Code</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtPostalCode" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Home Phone
                                                        </td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Mobile
                                                        </td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            Available On</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddAvailableMonth" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddAvailableYear" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Nationality</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddNationality" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Qualified
                                                        </td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddQualified" runat="server">
                                                                <asp:ListItem Selected="True" Value="-1">Qualified Teacher?</asp:ListItem>
                                                                <asp:ListItem Value="0">Yes</asp:ListItem>
                                                                <asp:ListItem Value="1">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Experience</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddExpFrom" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddExpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Last Contact Date</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:TextBox ID="txtLastContactDate" runat="server" Width="85px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Applied On</td>
                                                        <td>
                                                            :</td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtAppliedFrom" runat="server" Width="85px"></asp:TextBox>
                                                            <asp:TextBox ID="txtAppliedTo" runat="server" Width="85px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="9">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                                    Format="dd/MMM/yyyy" TargetControlID="txtLastContactDate">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                                    Format="dd/MMM/yyyy" TargetControlID="txtAppliedFrom">
                                                </ajaxToolkit:CalendarExtender>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True"
                                                    Format="dd/MMM/yyyy" TargetControlID="txtAppliedTo">
                                                </ajaxToolkit:CalendarExtender>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="HT2" runat="server">
                                        <HeaderTemplate>
                                            More Search Options
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <div class="matters">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Curriculum</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddCurriculum" runat="server">
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            Stage</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddApplicantStage" runat="server">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Subject</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:Panel ID="P1" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                                ScrollBars="Auto" Width="200px">
                                                                <asp:CheckBoxList ID="listSubjects" runat="server">
                                                                </asp:CheckBoxList>
                                                            </asp:Panel>
                                                        </td>
                                                        <td>
                                                            Applied For</td>
                                                        <td>
                                                            :</td>
                                                        <td>
                                                            <asp:Panel ID="P2" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                                                                ScrollBars="Auto" Width="200px">
                                                                <asp:CheckBoxList ID="listAppliedFor" runat="server">
                                                                </asp:CheckBoxList>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Search" Width="100px" /><asp:Button ID="btnReset" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Reset" Width="100px" /></td>
                        </tr>
                    </table>
                    &nbsp;&nbsp;
                </asp:Panel>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                    AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
                    Collapsed="False" CollapsedSize="0" CollapsedText="Advance Search" ExpandControlID="LinkAdvanceSearch"
                    ExpandedSize="350" ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
                    TextLabelID="LinkAdvanceSearch">
                </ajaxToolkit:CollapsiblePanelExtender>
            </div>
            Please click on search to get the results.</span>
        <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
            <tr>
                <td class="subheader_img">
                    Applicant Stages</td>
            </tr>
            <tr>
                <td>
                    <center>
                        <asp:GridView ID="GrdStage" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                            Width="750px" runat="server" OnPageIndexChanging="GrdStage_PageIndexChanging"
                            PageSize="50" AllowPaging="True">
                            <RowStyle CssClass="griditem" Height="25px"  Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                            <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                        </asp:GridView></center>
                </td>
            </tr>
        </table>
        <br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
    </div>