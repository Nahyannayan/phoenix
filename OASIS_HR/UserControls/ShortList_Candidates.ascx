<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShortList_Candidates.ascx.vb" Inherits="OASIS_HR_UserControls_ShortList_Candidates" %>

<link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    function change_chk_stateg(chkThis) {

        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("Check1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>
<div class="matters">

    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
    <table width="100%">
        <tr>
            <td class="title-bg" colspan="3">Select Job for Shorlisting
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddSCategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddSCategory_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td>
                <asp:DropDownList ID="ddJobCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddJobCode_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td>
                <asp:DropDownList ID="ddbsu" runat="server" Visible="False">
                </asp:DropDownList></td>
        </tr>
    </table>

    <div class="matters">
        <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton>
        <asp:Panel ID="Panel1" runat="server">
            <table width="100%">
                <tr>
                    <td class="title-bg">Search</td>
                </tr>
                <tr>
                    <td align="left">
                        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
                            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                                <HeaderTemplate>
                                    Basic Search
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="matters">
                                        <table width="100%">
                                            <tr>
                                                <td width="20%"><span class="field-label">Application No</span></td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox>
                                                </td>
                                                <td width="20%"><span class="field-label">First Name</span>
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Last Name</span></td>
                                                <td>
                                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                                </td>

                                                <td><span class="field-label">Gender</span></td>

                                                <td>
                                                    <asp:DropDownList ID="ddGender" runat="server">
                                                        <asp:ListItem Value="-1">Gender</asp:ListItem>
                                                        <asp:ListItem Value="M">Male</asp:ListItem>
                                                        <asp:ListItem Value="F">Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Age</span></td>

                                                <td>
                                                    <asp:DropDownList ID="ddAgeFrom" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddAgeTo" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td><span class="field-label">Marital Status</span>
                                                </td>

                                                <td>
                                                    <asp:DropDownList ID="ddMaritalStatus" runat="server">
                                                        <asp:ListItem Value="-1">Marital Status</asp:ListItem>
                                                        <asp:ListItem Value="Married">Married</asp:ListItem>
                                                        <asp:ListItem Value="Single">Single</asp:ListItem>
                                                        <asp:ListItem Value="Partner">Partner</asp:ListItem>
                                                        <asp:ListItem Value="Widow">Widow</asp:ListItem>
                                                        <asp:ListItem Value="Widower">Widower</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Postal Code</span></td>

                                                <td>
                                                    <asp:TextBox ID="txtPostalCode" runat="server"></asp:TextBox>
                                                </td>
                                                <td><span class="field-label">Home Phone</span>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Mobile    </span>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox>
                                                </td>

                                                <td><span class="field-label">Email</span></td>

                                                <td>
                                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Available On</span></td>

                                                <td>
                                                    <asp:DropDownList ID="ddAvailableMonth" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddAvailableYear" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td><span class="field-label">Nationality</span></td>

                                                <td>
                                                    <asp:DropDownList ID="ddNationality" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Qualified</span>
                                                </td>

                                                <td>
                                                    <asp:DropDownList ID="ddQualified" runat="server">
                                                        <asp:ListItem Selected="True" Value="-1">Qualified Teacher?</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td><span class="field-label">Experience</span></td>

                                                <td>
                                                    <asp:DropDownList ID="ddExpFrom" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddExpTo" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Last Contact Date</span></td>

                                                <td>
                                                    <asp:TextBox ID="txtLastContactDate" runat="server"></asp:TextBox>
                                                </td>

                                                <td><span class="field-label">Applied On</span></td>

                                                <td>
                                                    <asp:TextBox ID="txtAppliedFrom" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtAppliedTo" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtLastContactDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtAppliedFrom">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True"
                                            Format="dd/MMM/yyyy" TargetControlID="txtAppliedTo">
                                        </ajaxToolkit:CalendarExtender>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                                <HeaderTemplate>
                                    More Search Options
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="matters">
                                        <table width="100%">
                                            <tr>
                                                <td width="20%"><span class="field-label">Curriculum</span></td>

                                                <td width="30%">
                                                    <asp:DropDownList ID="ddCurriculum" runat="server">
                                                    </asp:DropDownList></td>
                                                <td width="20%"><span class="field-label">Stage</span></td>

                                                <td width="30%">
                                                    <asp:DropDownList ID="ddApplicantStage" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td><span class="field-label">Subject</span></td>

                                                <td>
                                                    <asp:Panel ID="P1" runat="server"
                                                        ScrollBars="Auto">
                                                        <div class="checkbox-list">
                                                            <asp:CheckBoxList ID="listSubjects" runat="server">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                                <td><span class="field-label">Applied For</span></td>

                                                <td>
                                                    <asp:Panel ID="P2" runat="server" ScrollBars="Auto">
                                                        <div class="checkbox-list">
                                                            <asp:CheckBoxList ID="listAppliedFor" runat="server" >
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                            Text="Search" /><asp:Button ID="btnReset" runat="server" CausesValidation="False"
                                CssClass="button" Text="Reset" /></td>
                </tr>
            </table>
        </asp:Panel>
        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
            AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
            Collapsed="False" CollapsedSize="0" CollapsedText="Advance Search" ExpandControlID="LinkAdvanceSearch"
            ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
            TextLabelID="LinkAdvanceSearch">
        </ajaxToolkit:CollapsiblePanelExtender>
    </div>

    <div class="matters">

        <table width="100%">
            <tr>
                <td class="title-bg">Shortlist Applicant</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GrdView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                        EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                        OnPageIndexChanging="GrdView_PageIndexChanging" PageSize="10" ShowFooter="true"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <FooterTemplate>
                                    <asp:Button ID="btnshortlist" runat="server" CssClass="button" Font-Bold="True"
                                        OnClick="btnshortlistsave_Click" Text="ShortList" />
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure?"
                                        TargetControlID="btnshortlist">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <center>
                                <asp:CheckBox ID="Check1" runat="server" /></center>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                        ToolTip="Click here to select/deselect all rows" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="App No">
                                <HeaderTemplate>
                                    <table class="BlueTable" width="100%">
                                        <tr class="matterswhite">
                                            <td align="center" colspan="2">App&nbsp;No
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center><%#Eval("APPLICATION_NO")%></center>
                                    <asp:HiddenField ID="HiddenApplication_No" runat="server" Value='<%#Eval("APPLICATION_NO")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Image">
                                <HeaderTemplate>
                                    Image
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="HiddenPhotoPath" Value='<%# Eval("PHOTO_FILE_PATH") %>' runat="server" />
                                    <center><asp:Image ID="ApplicantImage1"   runat="server" /></center>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Name">
                                <HeaderTemplate>
                                    Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkView" runat="server" OnClientClick="javascript:return false;"
                                        Text='<%# Eval("FULLNAME") %>'></asp:LinkButton>
                                    <br />
                                    <br />

                                    <center>
                                           <ajaxtoolkit:rating id="CRatings" runat="server" currentrating='<%# Eval("RATINGS") %>'
                                            maxrating="5" starcssclass="ratingStar" waitingstarcssclass="savedRatingStar"
                                            filledstarcssclass="filledRatingStar" ReadOnly="true" emptystarcssclass="emptyRatingStar" />
                                    </center>
                                    <br />
                                    <br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country">
                                <HeaderTemplate>
                                    Country
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("COUNTRY_RES_NAME")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Contact Date">
                                <HeaderTemplate>
                                    Contact Date
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("LAST_CONTACT_DATE", "{0:dd/MMM/yyyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <HeaderTemplate>
                                    Email
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <a href='mailto:<%#Eval("EMAIL")%>'>
                                        <%#Eval("EMAIL")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Detail View">
                                <HeaderTemplate>
                                    View
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <asp:LinkButton ID="LinkShow" runat="server" CausesValidation="false" OnClientClick='<%# GetNavigateUrl(Eval("APPLICATION_NO").ToString()) %>'
                                    Text="View"></asp:LinkButton></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CV">
                                <HeaderTemplate>
                                    CV
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <asp:LinkButton ID="LinkShowCv" runat="server" CausesValidation="false" CommandArgument='<%# Eval("CV_FILE_PATH") %>'
                                    CommandName="CV" Text='<%# Eval("CV_FILE_NAME") %>'></asp:LinkButton></center>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Corres">
                                <HeaderTemplate>
                                    Corres
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <asp:LinkButton ID="LinkCorres" runat="server" CausesValidation="false" CommandArgument='<%# Eval("APPLICATION_NO") %>'
                                    CommandName="Corres" Text="Correspondence"></asp:LinkButton></center>
                                    <asp:Panel ID="Show" runat="server" BackColor="#ffff99" BorderColor="Black">
                                        <asp:Label ID="lblcoress" runat="server" Text='<%#Eval("LASTCORRESS")%>'></asp:Label>
                                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" PopupControlID="Show" PopupPosition="Left"
                                        TargetControlID="LinkCorres">
                                    </ajaxToolkit:HoverMenuExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stage History">
                                <HeaderTemplate>
                                    History
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                <asp:LinkButton ID="T5lblview" runat="server" CausesValidation="false" CommandArgument='<%# Eval("APPLICATION_NO") %>'
                                    CommandName="history" Text="History"></asp:LinkButton>
                            </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem" Wrap="False" />
                        <EmptyDataRowStyle Wrap="False" />
                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                        <EditRowStyle Wrap="False" />
                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    </asp:GridView>
                    <asp:Label ID="Label1" runat="server"></asp:Label><asp:Panel ID="PanelStatusupdate"
                        runat="server" BackColor="white" CssClass="modalPopup" Style="display: none">
                        <table width="100%">
                            <tr>
                                <td class="title-bg">History</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GrdHistory" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-row table-bordered">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Stage">
                                                <HeaderTemplate>
                                                    Stage
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhisstage" runat="server" Text='<%#Eval("STAGE_DESCRIPTION") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <HeaderTemplate>
                                                    Date
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhisdate" runat="server" Text='<%#Eval("ENTRY_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <HeaderTemplate>
                                                    Remarks
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblhisdate" runat="server" Text='<%#Eval("REMARKS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="griditem" Wrap="False" />
                                        <EmptyDataRowStyle Wrap="False" />
                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                        <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                        <EditRowStyle Wrap="False" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                    </asp:GridView>
                                    <asp:Button ID="btncancel" runat="server" CausesValidation="false" CssClass="button"
                                        Text="OK" />
                                    <asp:HiddenField ID="HiddenTasklistid" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btncancel" DropShadow="true" PopupControlID="PanelStatusupdate"
                        RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label1">
                    </ajaxToolkit:ModalPopupExtender>

                </td>
            </tr>
        </table>

    </div>
