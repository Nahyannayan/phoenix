﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Partial Class Exit_workflow_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                'Session("sBsuid") = "131001"
                'Session("EmployeeId") = "6181"
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                BindFlowDetails()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try


        End If

    End Sub
     

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim url As String

            'Dim lblEXT_WF_ID As New Label
            'lblEXT_WF_ID = TryCast(sender.FindControl("lblEXT_WF_ID"), Label)
            'ViewState("EXT_WF_ID") = lblEXT_WF_ID.Text
            'lblError.InnerHtml = ""

            'ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ''define the datamode to view if view is clicked
            'ViewState("datamode") = "edit"
            ''Encrypt the data that needs to be send through Query String

            'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            'Dim EXT_WF_ID As String = Encr_decrData.Encrypt(lblEXT_WF_ID.Text)
            'url = String.Format("~\oasis_hr\Exit_workflow_Master_Add.aspx?MainMnu_code={0}&datamode={1}&id={2}", ViewState("MainMnu_code"), ViewState("datamode"), EXT_WF_ID)
            'Response.Redirect(url)
        Catch ex As Exception
            lblError.InnerHtml = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, "lbtnEdit_Click")
        End Try
    End Sub

    Protected Sub gvEmpList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpList.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)

            Dim lblEXT_WF_ID As Label = e.Row.FindControl("lblEXT_WF_ID")
            Dim lblStaff_LST_ID As Label = e.Row.FindControl("lblStaff_LST_ID") 
            Dim lblIsInDraft As Label = e.Row.FindControl("lblIsInDraft")
            Dim lblIs_HR As Label = e.Row.FindControl("lblIs_HR")
            Dim lnkEdit As System.Web.UI.WebControls.HyperLink = CType(e.Row.FindControl("hypEdit"), System.Web.UI.WebControls.HyperLink)
            Dim hypView As System.Web.UI.WebControls.HyperLink = CType(e.Row.FindControl("hypView"), System.Web.UI.WebControls.HyperLink)
            Dim hypOpen As System.Web.UI.WebControls.HyperLink = CType(e.Row.FindControl("hypOpen"), System.Web.UI.WebControls.HyperLink)



            If lblEXT_WF_ID IsNot Nothing And lblEXT_WF_ID.Text.Length > 0 Then

                Dim EXT_WF_ID As String = Encr_decrData.Encrypt(lblEXT_WF_ID.Text)

                Dim url As String
                url = String.Format("~\oasis_hr\Exit_workflow_Add.aspx?IsInDraft=" + lblIsInDraft.Text.Trim + "&id=" + EXT_WF_ID)
                lnkEdit.NavigateUrl = url

                lblIs_HR.Text = IIf(lblIs_HR.Text = "", False, lblIs_HR.Text)
                If Convert.ToBoolean(lblIs_HR.Text) Then
                    hypView.NavigateUrl = String.Format("~\oasis_hr\Exit_workflow.aspx?EXT_WF_LST_ID=0&EXT_WF_ID=" + EXT_WF_ID)
                Else
                    hypView.Visible = False
                End If


            Else
                lnkEdit.NavigateUrl = ""
                lnkEdit.Visible = False
            End If

            If lblStaff_LST_ID.Text = "" Then
                lblStaff_LST_ID.Text = "0"
            End If

            lblIsInDraft.Text = IIf(lblIsInDraft.Text = "", "No", lblIsInDraft.Text)
            If Convert.ToInt64(lblStaff_LST_ID.Text) > 0 And lblIsInDraft.Text = "Yes" Then

                Dim LST_ID As String = Encr_decrData.Encrypt(lblStaff_LST_ID.Text)
                hypOpen.NavigateUrl = String.Format("~\oasis_hr\Exit_workflow.aspx?EXT_WF_LST_ID=" + LST_ID + "&EXT_WF_ID=0")
            Else
                hypOpen.Visible = False
            End If

        End If
    End Sub

    Protected Sub gvEmpList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvEmpList.PageIndex = e.NewPageIndex
        BindFlowDetails()
    End Sub

    Protected Sub btnSearchDPT_DESCR(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindFlowDetails()
    End Sub

    Protected Sub btnSearchEMP_NAME(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindFlowDetails()
    End Sub
    Protected Sub btnSearchEMPNO(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("ServicehashCheck") = Nothing
        BindFlowDetails()
    End Sub

    Sub BindFlowDetails()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param(5) As SqlClient.SqlParameter

        Dim str_CM_title As String = String.Empty
        Dim str_EMP_Name As String = String.Empty
        Dim str_DPT_DESCR As String = String.Empty
        Dim CM_title As String = String.Empty
        Dim EMP_Name As String = String.Empty
        Dim DPT_DESCR As String = String.Empty
        Dim Dt As New DataTable
        Dim txtSearch As New TextBox
        Dim FILTER_COND As String = String.Empty
        Try

            If gvEmpList.Rows.Count > 0 Then

                txtSearch = gvEmpList.HeaderRow.FindControl("txtEMP_NAME")

                If txtSearch.Text.Trim <> "" Then
                    CM_title = " AND EMP_NAME like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_CM_title = txtSearch.Text.Trim
                End If

                txtSearch = gvEmpList.HeaderRow.FindControl("txtDPT_DESCR")

                If txtSearch.Text.Trim <> "" Then
                    DPT_DESCR = " AND DPT_DESCR like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%'"
                    str_DPT_DESCR = txtSearch.Text.Trim
                End If


            End If
            FILTER_COND = CM_title + " " + DPT_DESCR
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@FILTERCONDITION", FILTER_COND)
            param(2) = New SqlParameter("@EMP_ID", Session("EmployeeId"))


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DETAILS_MAIN]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                Dt = ds.Tables(0)
                gvEmpList.DataSource = ds.Tables(0)
                gvEmpList.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvEmpList.DataSource = ds.Tables(0)
                Try
                    gvEmpList.DataBind()
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, " gvEmpList.DataBind()")
                End Try

                Dim columnCount As Integer = gvEmpList.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvEmpList.Rows(0).Cells.Clear()
                gvEmpList.Rows(0).Cells.Add(New TableCell)
                gvEmpList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpList.Rows(0).Cells(0).Text = "No records available !!!"
            End If

            txtSearch = gvEmpList.HeaderRow.FindControl("txtEMP_NAME")
            txtSearch.Text = str_CM_title

            txtSearch = gvEmpList.HeaderRow.FindControl("txtDPT_DESCR")
            txtSearch.Text = str_DPT_DESCR

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BindFlowDetails")
        End Try

    End Sub

End Class
