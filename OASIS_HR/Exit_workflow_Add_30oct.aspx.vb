﻿
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Exit_workflow_Add
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
        'scriptManager.RegisterAsyncPostBackControl(Me.gvEmployeeList)
        Try

      
        If Not Page.IsPostBack Then
            ViewState("EXT_WF_ID") = "0"
                ViewState("IsInDraft") = "No"
            If Request.QueryString("ID") IsNot Nothing Then

                If Request.QueryString("ID").Length > 1 Then
                    ViewState("EXT_WF_ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                End If
                If Request.QueryString("IsInDraft") IsNot Nothing Then
                    ViewState("IsInDraft") = Request.QueryString("IsInDraft")

                        If ViewState("IsInDraft") = "Yes" Then
                            btnAddPDC.Visible = False
                            btnSaveToDraft.Visible = False
                            ddlEmployee.Enabled = False
                            frameAddMoreEmployee.Visible = False
                            ddlMoreEmployee.Enabled = False
                        End If
                End If

            End If
            LoadEmployee()
            bindEmployeeGrid_More()
            'bindEmployeeGrid_More()
            If Convert.ToInt64(ViewState("EXT_WF_ID")) > 0 Then
                BindData()
            End If
            'tdEmp.Visible = False
        End If
        If hdnSelected.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
        End Try
    End Sub

    Protected Sub gvPDC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvPDC.PageIndex = e.NewPageIndex
        bindEmployeeGrid()
    End Sub
    Protected Sub OnRowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim DT As DataTable = Session("empDt")

        'Dim index As Integer = e.RowIndex
        'Dim row As GridViewRow = gvPDC.Rows(e.RowIndex - 1)

        Dim lblId As Label = gvPDC.Rows(e.RowIndex).FindControl("lblId")
        DT.Rows.RemoveAt(GetIndex(DT, lblId.Text))
        gvPDC.DataSource = DT
        gvPDC.DataBind()
        Session("empDt") = DT
    End Sub
    Private Function GetIndex(ByVal DT As DataTable, ByVal EMP_ID As String) As Integer

        Dim ndx As Int32 = -1
        Dim rows = DT.Select("EMP_ID =" + EMP_ID)
        If rows.Length > 0 Then
            ndx = DT.Rows.IndexOf(rows(0))
        End If
        Return ndx
    End Function

    Private Sub bindEmployeeGrid()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlClient.SqlParameter
        Dim ds As DataSet
        Try

            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_EXIT_WORKFLOW_MASTER]", param)

            'Session("dtWrokflow") = ds
            If ds.Tables(0).Rows.Count = 0 Or Convert.ToString(ViewState("empname")) = "" Then

                ds.Tables(0).Clear()
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvPDC.DataSource = ds
                gvPDC.DataBind()
                Dim columnCount As Integer = gvPDC.Rows(0).Cells.Count
                gvPDC.Rows(0).Cells.Clear()
                gvPDC.Rows(0).Cells.Add(New TableCell)
                gvPDC.Rows(0).Cells(0).ColumnSpan = columnCount
                gvPDC.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvPDC.Rows(0).Cells(0).Text = "Currently there is no Employee Exists"
            Else


                If Session("empDt") IsNot Nothing Then
                    gvPDC.DataSource = Session("empDt")
                    gvPDC.DataBind()
                Else
                    gvPDC.DataSource = ds
                    gvPDC.DataBind()
                    Session("empDt") = ds.Tables(0)
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindEmployeeGrid")
        End Try

    End Sub
    Private Sub bindEmployeeGrid_More()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_EMPLOYEE_LIST_MAIN]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                ddlMoreEmployee.DataTextField = "EMP_NAME_DDL"
                ddlMoreEmployee.DataValueField = "EMP_ID"
                ddlMoreEmployee.DataSource = DT
                ddlMoreEmployee.DataBind()
                ' Insert the first item.
                ddlMoreEmployee.Items.Insert(0, New ListItem("- Select Employee -", "0"))



            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bindEmployeeGrid_More")
        End Try

    End Sub


    Private Sub BindData()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DETAILS_BY_ID]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                ''comented by nahayn and addwed label
                '' ddlEmployee.SelectedValue = DT.Rows(0)("EMP_ID")
                ''ddlEmployee.Enabled = False
                ddlEmployee.Visible = False
                LblEmplyeename.Visible = True
                ViewState("empname") = DT.Rows(0)("EMP_NAME")
                LblEmplyeename.Text = DT.Rows(0)("EMP_NAME")
                tdMoreEmployee.Visible = True
                bindEmployeeGrid()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BindData")
        End Try
    End Sub

    Private Function callTransaction(ByRef errormsg As String, ByVal EXT_ISDRAFT As Boolean) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try
                Dim param(9) As SqlParameter
                Dim RegionLoc As String = ""
                Dim LocStatus = ""
                Dim EXT_WF_ID As Integer = ViewState("EXT_WF_ID")
                param(0) = New SqlParameter("@EXT_WF_ID", EXT_WF_ID)
                param(0).Direction = ParameterDirection.InputOutput

                param(1) = New SqlParameter("@EXT_EMP_ID", ddlEmployee.SelectedValue)
                param(2) = New SqlParameter("@EXT_BSU_ID", Session("sBsuid"))
                param(3) = New SqlParameter("@EXT_ISDRAFT", EXT_ISDRAFT)
                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue
                param(5) = New SqlParameter("@EXT_INITIATED_BY", Session("sUsr_name"))
                param(6) = New SqlParameter("@EXT_IS_COMPLETED", False)
                param(7) = New SqlParameter("@EXT_LAST_UPDATED_BY", Session("sUsr_name"))

                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SAVE_EXIT_WORKFLOW]", param)
                Dim ReturnFlag As Integer = param(4).Value

                If EXT_WF_ID = 0 Then
                    EXT_WF_ID = param(0).Value
                    ViewState("EXT_WF_ID") = param(0).Value
                End If


                If ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    'Remove the existing list

                    If Convert.ToInt64(ViewState("EXT_WF_ID")) > 0 Then
                        REMOVE_EXIT_WORKFLOW_LIST(tran, EXT_WF_ID)
                    End If

                    For Each gvrow As GridViewRow In gvPDC.Rows
                        Dim lblEXT_WF_ID As Label = DirectCast(gvrow.FindControl("lblEXT_WF_USR_ID"), Label)
                        Dim lblId As Label = DirectCast(gvrow.FindControl("lblId"), Label)
                        Dim lblEXT_NWF_ID As Label = DirectCast(gvrow.FindControl("lblEXT_NWF_ID"), Label)

                        Dim EXT_WF_LST_EXT_NWF_ID As Integer = 0
                        Dim EXT_WF_LST_EXT_WF_USR_ID As Integer = Convert.ToInt64(lblEXT_WF_ID.Text)

                        If (EXT_WF_LST_EXT_WF_USR_ID = 0) And Convert.ToInt64(lblEXT_NWF_ID.Text) = 0 Then
                            EXT_WF_LST_EXT_NWF_ID = SAVE_EXIT_WORKFLOW_EMP(tran, lblId.Text, gvrow)
                        Else
                            EXT_WF_LST_EXT_NWF_ID = Convert.ToInt64(lblEXT_NWF_ID.Text)
                        End If


                        Dim paramLST(5) As SqlParameter
                        paramLST(0) = New SqlParameter("@EXT_WF_LST_ID", 0)
                        paramLST(1) = New SqlParameter("@EXT_WF_LST_EXT_WF_ID", EXT_WF_ID)
                        paramLST(2) = New SqlParameter("@EXT_WF_LST_EXT_NWF_ID", EXT_WF_LST_EXT_NWF_ID)
                        paramLST(3) = New SqlParameter("@EXT_WF_LST_EXT_WF_USR_ID", EXT_WF_LST_EXT_WF_USR_ID)
                        paramLST(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        paramLST(4).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SAVE_EXIT_WORKFLOW_LIST_NEW]", paramLST)
                        Dim ReturnFlagLST As Integer = paramLST(4).Value
                        If ReturnFlagLST <> 0 Then
                            callTransaction = "1"
                            errormsg = "Error occured while processing info !!!"
                        End If

                    Next
                    callTransaction = "0"
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using

    End Function

    Private Function SAVE_EXIT_WORKFLOW_EMP(ByVal tran As SqlTransaction, ByVal EMP_ID As Integer, ByVal gvrow As GridViewRow) As Integer

        Dim lblEXT_WF_ID As Label = DirectCast(gvrow.FindControl("lblEXT_WF_ID"), Label)
        Dim EXT_NWF_EMP_ID As Label = DirectCast(gvrow.FindControl("lblId"), Label)
        Dim EXT_NWF_DPT_ID As Label = DirectCast(gvrow.FindControl("lblDPT_ID"), Label)
        Dim EXT_NWF_INCLUDE_IN_WORKFLOW As CheckBox = DirectCast(gvrow.FindControl("chkIncludeInWorkFlow"), CheckBox)
        Dim EXT_WF_IS_HR As CheckBox = DirectCast(gvrow.FindControl("chkEXT_WF_IS_HR"), CheckBox)



        Dim returnID As Integer = 0
        Dim param(9) As SqlParameter
        param(0) = New SqlParameter("@EXT_NWF_ID", 0)
        param(0).Direction = ParameterDirection.Output
        param(1) = New SqlParameter("@EXT_NWF_EMP_ID", EXT_NWF_EMP_ID.Text)
        param(2) = New SqlParameter("@EXT_NWF_DPT_ID", EXT_NWF_DPT_ID.Text)
        param(3) = New SqlParameter("@EXT_NWF_BSU_ID", Session("sBsuid"))
        param(4) = New SqlParameter("@EXT_NWF_CREATED_BY", Session("sUsr_name"))
        param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
        param(5).Direction = ParameterDirection.ReturnValue
        param(6) = New SqlParameter("@EXT_NWF_INCLUDE_IN_WORKFLOW", EXT_NWF_INCLUDE_IN_WORKFLOW.Checked)
        param(7) = New SqlParameter("@EXT_NWF_EMP_ID_FOR", ddlEmployee.SelectedValue)
        param(8) = New SqlParameter("@EXT_WF_IS_HR", EXT_WF_IS_HR.Checked)

        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SAVE_EXIT_WORKFLOW_EMP]", param)
        Dim ReturnFlag As Integer = param(5).Value
        returnID = param(0).Value

        Return returnID
    End Function

    Private Sub REMOVE_EXIT_WORKFLOW_LIST(ByVal tran As SqlTransaction, ByVal EXT_WF_ID As Integer)

        Dim param(9) As SqlParameter
        param(0) = New SqlParameter("@EXT_WF_ID", EXT_WF_ID)
        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[DELETE_EXIT_WORKFLOW_LIST]", param)

    End Sub


    Private Sub LoadEmployee()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_EMPLOYEE_LIST_MAIN]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                ddlEmployee.DataTextField = "EMP_NAME_DDL"
                ddlEmployee.DataValueField = "EMP_ID"
                ddlEmployee.DataSource = DT
                ddlEmployee.DataBind()
                ' Insert the first item.
                ddlEmployee.Items.Insert(0, New ListItem("- Select Employee -", "0"))

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "LoadEmployee")
        End Try

    End Sub

    Protected Sub ddlEmployee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEmployee.SelectedIndexChanged
        If (Convert.ToInt64(ddlEmployee.SelectedValue) > 0) Then
            tdMoreEmployee.Visible = True
            ViewState("empname") = ddlEmployee.SelectedItem.Text
        Else
            tdMoreEmployee.Visible = False
        End If
        bindEmployeeGrid()
    End Sub

    Protected Sub frameAddMoreEmployee_Click(sender As Object, e As EventArgs) Handles frameAddMoreEmployee.Click
        Try

            Dim DT As DataTable = Session("empDt") 
            Dim DPT_ID As Integer = 0
            Dim DPT_DESCR As String = ""
            Get_Depart_by_emp_id(ddlMoreEmployee.SelectedValue, DPT_ID, DPT_DESCR)

            Dim newRow As DataRow = DT.NewRow

            'Add some data to it
            newRow("EMP_ID") = ddlMoreEmployee.SelectedValue
            newRow("EXT_WF_USR_ID") = "0"
            newRow("EXT_NWF_ID") = "0"

            newRow("EMP_NAME") = ddlMoreEmployee.SelectedItem.Text.Substring(0, ddlMoreEmployee.SelectedItem.Text.IndexOf("("))
            newRow("DPT_DESCR") = DPT_DESCR
            newRow("DPT_ID") = DPT_ID
            newRow("EXT_WF_INCLUDE_IN_WORKFLOW") = "False"
            DT.Rows.Add(newRow)

            gvPDC.DataSource = DT
            gvPDC.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "frameAddMoreEmployee_Click")
        End Try
    End Sub

    Private Sub Get_Depart_by_emp_id(ByVal emp_id As Integer, ByRef DPT_ID As Integer, ByRef DPT_DESCR As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EMP_ID", emp_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_DEPARTMENT_BY_EMPID]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                DPT_ID = DT.Rows(0)("DPT_ID")
                DPT_DESCR = DT.Rows(0)("DPT_DESCR")
            Else
                DPT_ID = ""
                DPT_DESCR = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Get_Depart_by_emp_id")
        End Try

    End Sub

    Protected Sub btnSaveToDraft_Click(sender As Object, e As EventArgs) Handles btnSaveToDraft.Click
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg, True) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
        Else
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:green;padding:5pt;background-color:white;'>Data Saved Successfully.</div>"
        End If
    End Sub

    Protected Sub btnAddPDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPDC.Click
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg, False) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
        Else
            If SendEmailToAllDepartments(errormsg) <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
            Else
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:green;padding:5pt;background-color:white;'>Data Saved Successfully.</div>"

                btnAddPDC.Visible = False
                btnSaveToDraft.Visible = False
                ddlEmployee.Enabled = False
                frameAddMoreEmployee.Visible = False
                ddlMoreEmployee.Enabled = False 
            End If
        End If
    End Sub

    Private Function SendEmailToAllDepartments(ByRef errormsg As String) As Integer

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim DT As DataTable
        Dim param1(2) As SqlClient.SqlParameter
        param1(0) = New SqlClient.SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_EMPLOYEE_LIST_BY_EXT_WF_ID]", param1)

        If ds.Tables(0).Rows.Count > 0 Then
            DT = ds.Tables(0)
            Dim tran As SqlTransaction
            Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

                tran = CONN.BeginTransaction("SampleTransaction")
                Try
                    For Each row As DataRow In DT.Rows
                        Dim EXT_WF_LST_ID As Integer = row("EXT_WF_LST_ID")

                        Dim encr_EXT_WF_LST_ID As String = Encr_decrData.Encrypt(EXT_WF_LST_ID)
                        Dim param(4) As SqlParameter
                        param(0) = New SqlParameter("@EXT_WF_LST_ID", EXT_WF_LST_ID)
                        param(1) = New SqlParameter("@encr_EXT_WF_LST_ID", encr_EXT_WF_LST_ID)
                        param(2) = New SqlParameter("@USER_ID", Session("sUsr_id"))
                        param(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        param(3).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SEND_EMAIL_FOR_EXIT_WORKFLOW]", param)
                        Dim ReturnFlag As Integer = param(3).Value
                        If ReturnFlag <> 0 Then
                            SendEmailToAllDepartments = "1"
                            errormsg = "Error occured while processing info !!!"
                            Exit For
                        End If
                    Next
                    SendEmailToAllDepartments = 0
                Catch ex As Exception
                    SendEmailToAllDepartments = "1"
                    errormsg = ex.Message
                Finally
                    If SendEmailToAllDepartments = "-1" Then
                        errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                        UtilityObj.Errorlog(errormsg)
                        tran.Rollback()
                    ElseIf SendEmailToAllDepartments <> "0" Then
                        errormsg = "Error occured while saving !!!"
                        UtilityObj.Errorlog(errormsg)
                        tran.Rollback()
                    Else

                        errormsg = ""
                        tran.Commit()
                    End If
                End Try

            End Using
        End If

    End Function

 
    Protected Sub gvPDC_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPDC.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)

            Dim lblEXT_WF_USR_ID As Label = e.Row.FindControl("lblEXT_WF_USR_ID")
            Dim lblEXT_NWF_ID As Label = e.Row.FindControl("lblEXT_NWF_ID")

            lblEXT_WF_USR_ID.Text = IIf(lblEXT_WF_USR_ID.Text = "", "0", lblEXT_WF_USR_ID.Text)
            lblEXT_NWF_ID.Text = IIf(lblEXT_NWF_ID.Text = "", "0", lblEXT_NWF_ID.Text)


            If (Convert.ToInt64(lblEXT_WF_USR_ID.Text) > 0) Or Convert.ToInt64(lblEXT_NWF_ID.Text) > 0 Then
                Dim chkIncludeInWorkFlow As CheckBox = e.Row.FindControl("chkIncludeInWorkFlow")
                Dim chkEXT_WF_IS_HR As CheckBox = e.Row.FindControl("chkEXT_WF_IS_HR")
                chkIncludeInWorkFlow.Enabled = False
                chkEXT_WF_IS_HR.Enabled = False
            End If

            If ViewState("IsInDraft") = "Yes" Then
                e.Row.Cells(5).Enabled = False
            End If

        End If
    End Sub

End Class


 