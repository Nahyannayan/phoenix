Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Payroll_empLeaveApplication
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    'Version        Date            Author            Change
    '1.1            20-Feb-2011     Swapna          To add logged in username in EMPLEAVEAPP table
    '1.2            27-Jul-2011     Swapna          To display BSU join date instead of last rejoin date.
    '1.3            06-Feb-2012     Swapna          To check email sending error on forwarding saved applications
    '1.4            11/mar/2012     Swapna          added medical documents upload option/Shakeel's changes for leave editing
    '2.0            01/Aug/2019     Vikranth        Eleave functionality for scholl staff

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Page.Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnSave)
        If Page.IsPostBack = False Then
            trUplaod.Attributes.Add("style", "display:none")

            filupload.Attributes.Add("style", "display:none")
            ViewState("IdDocEnabled") = "0"
            Page.Title = OASISConstants.Gemstitle
            txtEmpNo.Attributes.Add("readonly", "readonly")

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            'txtFrom.Text = GetDiplayDate()
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'bindevents(CurBsUnit, CurUsr_id)

            HttpContext.Current.Session("divDetail") = divDetail

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "P130015" And ViewState("MainMnu_code") <> "U000022") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ViewState("PassElaID") = False

            If Request.QueryString("viewid") <> "" Then
                setViewData()
                'IsPlannerEnabled()
                setModifyHeader(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                ResetViewData()
                'GetLoggedInEmployee()
                'GetEmpLeaveCounts()
                ddMonthstatusPeriodically.SelectedIndex = 0

            End If

            Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();return false;", PopupControlExtender2.ClientID)
            Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();return false;", PopupControlExtender2.ClientID)

            hdlnkEmpLeaves.Attributes.Add("onclick", OnMouseOverScript)
            hdlnkEmpLeaves.Attributes.Add("onmouseout", OnMouseOutScript)

            lnkEmpLeaves.Attributes.Add("onclick", OnMouseOverScript)
            lnkEmpLeaves.Attributes.Add("onmouseout", OnMouseOutScript)
            'If ViewState("datamode") = "view" Then
            '    GetEmpLeaveCounts()
            'End If

        End If

    End Sub

    Public Sub GetLoggedInEmployee()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT " _
         & " EM.EMP_ID, ISNULL(EM.EMP_FNAME,'')+' '+" _
         & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
         & " AS EMP_NAME  FROM  EMPLOYEE_M EM INNER JOIN USERS_M" _
         & " ON EM.EMP_ID=USERS_M.USR_EMP_ID" _
         & " WHERE USERS_M.USR_NAME ='" & Session("sUsr_name") & "' and em.emp_bsu_id ='" & Session("sBsuid") & "'"
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then

            txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME")
            h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
            ImageButton3_Click(Nothing, Nothing)
        End If

    End Sub
    Private Sub IsPlannerEnabled()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter 'V1.1
            pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")

            pParms(1) = New SqlClient.SqlParameter("@ELTID", SqlDbType.VarChar, 10)
            pParms(1).Value = ddMonthstatusPeriodically.SelectedValue

            pParms(2) = New SqlClient.SqlParameter("@ECTID", SqlDbType.Int)
            pParms(2).Value = ViewState("BLS_ECT_ID")

            pParms(3) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
            pParms(3).Value = Convert.ToDateTime(txtFrom.Text)

            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "CheckPlannerEnabled", pParms)

            Dim count = pParms(4).Value
            If count > 0 Then
                ViewState("IsPlannerEnabled") = 1
            Else
                ViewState("IsPlannerEnabled") = 0
            End If

        Catch ex As Exception
            lblError.Text = "Error in fetching leaves."
        End Try
    End Sub

    Private Sub setModifyHeader(ByVal p_Modifyid As String, Optional ByVal IsELA As Boolean = False) 'setting header data on view/edit
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            'V1.2   'IsNull(EM.EMP_LASTREJOINDT, EM.EMP_JOINDT) changed to EMP_BSU_JOINDT
            str_Sql = "SELECT   ELA.ELA_ID, ELA.ID, ELA.ELA_EMP_ID," _
             & " ELA.ELA_ELT_ID,ELA.ELA_HANDOVERTXT, ELA.ELA_DTFROM, ELA.ELA_DTTO, " _
             & " ELA.ELA_REMARKS, ELA.ELA_ADDRESS, ELA.ELA_PHONE, " _
             & " ELA.ELA_MOBILE, ELA.ELA_POBOX,ELA.ELA_APPRSTATUS,isnull(EM.emp_fname,'')+'  '+isnull(EM.emp_mname,'')+'  '+isnull(EM.emp_lname,'') as EMP_NAME,IsNull(EM.EMP_JOINDT,EM.EMP_BSU_JOINDT) EMP_LASTREJOINDT,ELA_bFORWARD,IsNull(EM.EMP_ECT_ID,0) EMP_ECT_ID, CASE ELA_bFORWARD WHEN 0 THEN 'yes' else 'no' end as canedit,Isnull(FD.DOC_ID,0) DOC_ID ," _
             & " isnull(ELA_bReqCancellation,0) ReqCanc,case when isnull(ELA_APPRSTATUS,'')='C' then 1 else 0 end as IsCancelled   " _
             & " FROM VW_EmployeeLeaveApp AS ELA INNER JOIN" _
             & " EMPLOYEE_M AS EM ON ELA.ELA_EMP_ID = EM.EMP_ID left outer join OASIS_DOCS.FIN.FDOCUMENTS FD ON " _
             & "ELA.ELA_ID=FD.DOC_PCD_ID AND FD.DOC_TYPE='LEAVE' "
            ' & " WHERE ELA.ELA_ID='" & p_Modifyid & "' "
            If IsELA Then
                str_Sql &= " WHERE ELA.ELA_ID='" & p_Modifyid & "' "
            Else
                str_Sql &= " WHERE ELA.ID='" & p_Modifyid & "' "
            End If

            Dim ds As New DataSet
            ViewState("canedit") = "no"
            ViewState("ReqCanc") = "no"
            ViewState("IsCancelled") = "no"
            ViewState("IsForwarded") = "no"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                h_ID.Value = ds.Tables(0).Rows(0)("ID").ToString
                h_ELA_ID.Value = ds.Tables(0).Rows(0)("ELA_ID")
                h_DOC_PCD_ID.Value = ds.Tables(0).Rows(0)("DOC_ID") 'V1.4
                h_Emp_No.Value = ds.Tables(0).Rows(0)("ELA_EMP_ID")
                txtEmpNo.Text = ds.Tables(0).Rows(0)("EMP_NAME").ToString
                txtEmpNo_TextChanged(txtEmpNo, EventArgs.Empty)
                txtMobile.Text = ds.Tables(0).Rows(0)("ELA_MOBILE").ToString
                txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                txtRemarks.Text = ds.Tables(0).Rows(0)("ELA_REMARKS").ToString
                txtLRejoindt.Text = Format(CDate(ds.Tables(0).Rows(0)("EMP_LASTREJOINDT")), "dd/MMM/yyyy").ToString   'V1.1
                txtPobox.Text = ds.Tables(0).Rows(0)("ELA_POBOX").ToString
                txtAddress.Text = ds.Tables(0).Rows(0)("ELA_ADDRESS").ToString
                txtHandover.Text = ds.Tables(0).Rows(0)("ELA_HANDOVERTXT").ToString 'V1.1
                txtFrom.Text = Format(CDate(ds.Tables(0).Rows(0)("ELA_DTFROM")), "dd/MMM/yyyy")
                txtTo.Text = Format(CDate(ds.Tables(0).Rows(0)("ELA_DTTO")), "dd/MMM/yyyy")
                ViewState("canedit") = ds.Tables(0).Rows(0)("canedit")
                ViewState("BLS_ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID") 'V1.1
                ddMonthstatusPeriodically.SelectedValue = ds.Tables(0).Rows(0)("ELA_ELT_ID")
                ViewState("canedit") = ds.Tables(0).Rows(0)("canedit")
                'V1.4
                ViewState("ReqCanc") = IIf(ds.Tables(0).Rows(0)("ReqCanc"), "yes", "no")
                ViewState("IsCancelled") = IIf(ds.Tables(0).Rows(0)("IsCancelled"), "yes", "no")
                ViewState("IsForwarded") = IIf(ds.Tables(0).Rows(0)("ELA_bFORWARD"), "yes", "no")
                If (txtAddress.Text = "" And txtMobile.Text = "" And txtPhone.Text = "") Then
                    ImageButton3_Click(Nothing, Nothing)
                End If
                If ViewState("canedit") = "yes" Then
                    chkForward.Checked = False
                Else
                    chkForward.Checked = True
                End If
                If ViewState("IsForwarded") = "yes" Then
                    btnEdit.Attributes.Add("OnClick", "return confirm_Edit();")
                Else
                    btnEdit.Attributes.Remove("OnClick")
                End If
                If ds.Tables(0).Rows(0)("ELA_APPRSTATUS").ToString = "A" Then
                    btnPrintLeave.Visible = True
                Else

                    btnPrintLeave.Visible = False
                    If (ViewState("ReqCanc") = "yes" Or ViewState("IsCancelled") = "yes") Then
                        btnEdit.Enabled = False
                        btnCancelLeave.Enabled = False
                        btnDelete.Enabled = False
                        btnSave.Enabled = False
                    Else
                        btnEdit.Enabled = True
                        btnCancelLeave.Enabled = True
                        btnDelete.Enabled = True
                        btnSave.Enabled = True
                    End If

                    If ViewState("ReqCanc") = "yes" Then
                        lblError.Text = "Requested for cancellation of leave application."
                    End If

                    If ViewState("IsCancelled") = "yes" Then
                        lblError.Text = "Leave application cancelled."
                    End If
                End If
                getDocumentEnabledFlag(ds.Tables(0).Rows(0)("ELA_ELT_ID").ToString)
                If h_DOC_PCD_ID.Value <> "" And h_DOC_PCD_ID.Value <> "0" And ViewState("IdDocEnabled") = "1" Then
                    imgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(h_DOC_PCD_ID.Value)
                    trUplaod.Attributes.Remove("style")
                    'trUplaod.Attributes.Add("style", "display:inline")
                    trUplaod.Visible = True
                    'filupload.Visible = False
                    filupload.Attributes.Remove("style")
                    filupload.Attributes.Add("style", "display:none")
                    lblView.Visible = True
                    imgDoc.Visible = True
                ElseIf ViewState("IdDocEnabled") = "1" And h_DOC_PCD_ID.Value = "0" Then
                    trUplaod.Attributes.Remove("style")
                    'trUplaod.Attributes.Add("style", "display:inline")
                    trUplaod.Visible = True
                    ' filupload.Visible = True
                    filupload.Attributes.Remove("style")
                    filupload.Attributes.Add("style", "display:inline")
                    lblView.Visible = False
                    imgDoc.Visible = False
                Else
                    trUplaod.Attributes.Remove("style")
                    trUplaod.Attributes.Add("style", "display:none")
                    'trUplaod.Visible = False
                    ' filupload.Visible = False
                    'lblView.Visible = False
                    'imgDoc.Visible = False

                End If
            Else
                ViewState("canedit") = "no"
            End If

            GetEmpLeaveCounts()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub setViewData() 'setting controls on view/edit
        imgFrom.Enabled = False
        imgTo.Enabled = False
        imgEmployee.Enabled = False
        txtFrom.Attributes.Add("readonly", "readonly")
        txtTo.Attributes.Add("readonly", "readonly")
        txtPhone.Attributes.Add("readonly", "readonly")
        txtMobile.Attributes.Add("readonly", "readonly")
        txtRemarks.Attributes.Add("readonly", "readonly")
        txtPobox.Attributes.Add("readonly", "readonly")
        txtAddress.Attributes.Add("readonly", "readonly")
        txtHandover.Attributes.Add("readonly", "readonly")
        chkForward.Enabled = False
        ddMonthstatusPeriodically.Enabled = False
        CalendarExtender1.Enabled = False
        CalendarExtender2.Enabled = False
        CalendarExtender3.Enabled = False
        DocDate.Enabled = False
        btnCancelLeave.Visible = False
        chkForward.Attributes.Add("readonly", "readonly")
        'V1.4
    End Sub


    Private Sub ResetViewData() 'resetting controls on view/edit
        imgFrom.Enabled = True
        imgTo.Enabled = True
        imgEmployee.Enabled = True
        txtPhone.Attributes.Remove("readonly")
        txtFrom.Attributes.Remove("readonly")
        txtTo.Attributes.Remove("readonly")
        txtMobile.Attributes.Remove("readonly")
        txtRemarks.Attributes.Remove("readonly")
        txtPobox.Attributes.Remove("readonly")
        txtAddress.Attributes.Remove("readonly")
        txtHandover.Attributes.Remove("readonly")
        chkForward.Enabled = True
        ddMonthstatusPeriodically.Enabled = True
        CalendarExtender1.Enabled = True
        CalendarExtender2.Enabled = True
        CalendarExtender3.Enabled = True
        DocDate.Enabled = True
        If ViewState("IsForwarded") = "yes" Then
            btnCancelLeave.Visible = True
        Else
            btnCancelLeave.Visible = False
        End If
        chkForward.Attributes.Remove("readonly")
        'V1.4
    End Sub
    Sub bindevents(ByVal bsuid As String, ByVal usrname As String)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim ds As New DataSet
            'Commented and added belwo line by vikranth on 1st July 2019
            'Dim str_Sql As String
            'str_Sql = "exec GetBSULeaveTypes '" & bsuid & "','" & usrname & "'"
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "LeaveTypes"
            params(1) = New SqlClient.SqlParameter("@Usr_Name", SqlDbType.VarChar)
            params(1).Value = Session("sUsr_name")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
            'Vikranth end
            ddMonthstatusPeriodically.DataSource = ds.Tables(0)
            ddMonthstatusPeriodically.DataTextField = "ELT_DESCR"
            ddMonthstatusPeriodically.DataValueField = "ELT_ID"
            ddMonthstatusPeriodically.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    'V1.4


    Function get_Isholiday(ByVal dtDate As Date) As Boolean
        Dim isHoliday As Boolean = False
        ' SUNDAY, SATURDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
        Select Case dtDate.DayOfWeek
            Case DayOfWeek.Sunday
                If ViewState("strHoliday1") = "SUNDAY" Or ViewState("strHoliday2") = "SUNDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Monday
                If ViewState("strHoliday1") = "MONDAY" Or ViewState("strHoliday2") = "MONDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Tuesday
                If ViewState("strHoliday1") = "TUESDAY" Or ViewState("strHoliday2") = "TUESDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Wednesday
                If ViewState("strHoliday1") = "WEDNESDAY" Or ViewState("strHoliday2") = "WEDNESDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Thursday
                If ViewState("strHoliday1") = "THURSDAY" Or ViewState("strHoliday2") = "THURSDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Friday
                If ViewState("strHoliday1") = "FRIDAY" Or ViewState("strHoliday2") = "FRIDAY" Then
                    isHoliday = True
                End If
            Case DayOfWeek.Saturday
                If ViewState("strHoliday1") = "SATURDAY" Or ViewState("strHoliday2") = "SATURDAY" Then
                    isHoliday = True
                End If
        End Select
        Return isHoliday
    End Function

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Master.DisableScriptManager()
    'End Sub
    Public Shared Function SaveEMPLEAVEAPP(ByRef p_ELA_ID As String, ByVal p_ELA_BSU_ID As String, ByVal p_ELA_EMP_ID As String, _
      ByVal p_ELA_ELT_ID As String, ByVal p_ELA_DTFROM As Date, ByVal p_ELA_DTTO As Date, _
      ByVal p_ELA_REMARKS As String, ByVal p_ELA_APPRSTATUS As String, ByVal p_ELA_STATUS As Integer, _
      ByVal p_ELA_ADDRESS As String, ByVal p_ELA_PHONE As String, ByVal p_ELA_MOBILE As String, ByVal p_ELA_Pobox As String, _
      ByVal p_APS_bFORWARD As Boolean, ByVal p_Edit As Boolean, ByVal p_stTrans As SqlTransaction, ByVal p_user As String, ByVal p_handovertxt As String) As String
        Try
            Dim pParms(19) As SqlClient.SqlParameter 'V1.2 ,V1.3
            pParms(0) = Mainclass.CreateSqlParameter("@ELA_BSU_ID", p_ELA_BSU_ID, SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@ELA_EMP_ID", p_ELA_EMP_ID, SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@ELA_ELT_ID", p_ELA_ELT_ID, SqlDbType.VarChar)
            pParms(3) = Mainclass.CreateSqlParameter("@ELA_DTFROM", p_ELA_DTFROM, SqlDbType.DateTime)
            pParms(4) = Mainclass.CreateSqlParameter("@ELA_DTTO", p_ELA_DTTO, SqlDbType.DateTime)
            pParms(5) = Mainclass.CreateSqlParameter("@ELA_APPRSTATUS", p_ELA_APPRSTATUS, SqlDbType.VarChar)
            pParms(6) = Mainclass.CreateSqlParameter("@bEdit", p_Edit, SqlDbType.Bit)
            pParms(7) = Mainclass.CreateSqlParameter("@ELA_STATUS", p_ELA_STATUS, SqlDbType.Int)
            'pParms(8) = Mainclass.CreateSqlParameter("@return_value", "0", SqlDbType.Int, True)
            pParms(9) = Mainclass.CreateSqlParameter("@ELA_REMARKS", p_ELA_REMARKS, SqlDbType.VarChar)
            pParms(10) = Mainclass.CreateSqlParameter("@ELA_ID", p_ELA_ID, SqlDbType.Int, True)
            pParms(11) = Mainclass.CreateSqlParameter("@ELA_ADDRESS", p_ELA_ADDRESS, SqlDbType.VarChar)
            pParms(12) = Mainclass.CreateSqlParameter("@ELA_PHONE", p_ELA_PHONE, SqlDbType.VarChar)
            pParms(13) = Mainclass.CreateSqlParameter("@ELA_MOBILE", p_ELA_MOBILE, SqlDbType.VarChar)
            pParms(14) = Mainclass.CreateSqlParameter("@ELA_POBOX", p_ELA_Pobox, SqlDbType.VarChar)
            pParms(15) = Mainclass.CreateSqlParameter("@APS_bFORWARD", p_APS_bFORWARD, SqlDbType.Bit)

            pParms(16) = Mainclass.CreateSqlParameter("@UserName", p_user, SqlDbType.VarChar)
            pParms(17) = Mainclass.CreateSqlParameter("@Handovertxt", p_handovertxt, SqlDbType.VarChar)
            pParms(18) = Mainclass.CreateSqlParameter("@RetMsg", "", SqlDbType.VarChar, True, 100)
            pParms(19) = Mainclass.CreateSqlParameter("@RetVal", "", SqlDbType.VarChar, True, 100)
            Dim retval As String
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "SaveEMPLEAVEAPP_V2", pParms)
            p_ELA_ID = pParms(10).Value.ToString
            SaveEMPLEAVEAPP = pParms(19).Value.ToString & "||" & pParms(18).Value.ToString
        Catch ex As Exception
            Errorlog(ex.Message)
            Throw ex
        End Try
    End Function


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Page.Validate()
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim strfDate As String = txtFrom.Text.Trim
        Dim strtDate As String = txtTo.Text.Trim
        Dim str_err As String = DateFunctions.checkdates_canfuture(strfDate, strtDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        End If
        txtFrom.Text = strfDate
        txtTo.Text = strtDate
        If txtRemarks.Text.Length > 200 Then
            lblError.Text = "Details cannot be more than 200 characters."
            Exit Sub
        End If
        If ddMonthstatusPeriodically.SelectedIndex = 0 Then
            lblError.Text = "Please select a leave type."
            Exit Sub
        End If
        'If txtRemarks.Text.Trim = "" Then
        '    lblError.Text = "Details Cannot be empty"
        '    Exit Sub
        'End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim ela_id, str_mode As String
            Dim edit_bool As Boolean
            If ViewState("datamode") = "edit" Then
                edit_bool = True
                str_mode = "Edit"
                ela_id = h_ELA_ID.Value ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                edit_bool = False
                str_mode = "Insert"
                ela_id = 0
            End If
            Dim retval As String = "1000"
            Dim retMsg As String = ""
            Dim bIsLeaveExceeded As Boolean = False
            If ddMonthstatusPeriodically.SelectedItem.Value = "AL" Then
                bIsLeaveExceeded = IsLeaveExceeded()
            Else
                chkSaveAnyway.Visible = False
            End If

            If bIsLeaveExceeded Then
                retval = 2580
                lblError.Text = getErrorMessage(retval)
            Else
                retval = 0
            End If

            'If chkSaveAnyway.Visible = True And chkSaveAnyway.Checked = False Then
            '    retval = 1000
            'Else
            '    retval = 0
            'End If
            If retval = 0 Then
                retval = SaveEMPLEAVEAPP(ela_id, Session("sbsuid"), h_Emp_No.Value, ddMonthstatusPeriodically.SelectedItem.Value, _
                txtFrom.Text.Trim, txtTo.Text.Trim, txtRemarks.Text.Trim, "N", 0, txtAddress.Text.Trim, txtPhone.Text.Trim, _
                txtMobile.Text.Trim, "0", chkForward.Checked, edit_bool, stTrans, Session("sUsr_name").ToString, txtHandover.Text)   'V1.1
                Dim separator As String() = New String() {"||"}
                Dim myValarr() As String = retval.Split(separator, StringSplitOptions.None)
                retval = myValarr(0)
                If myValarr.Length > 1 Then
                    retMsg = myValarr(1)
                Else
                    retMsg = ""
                End If
                If retval = "0" Then
                    getDocumentEnabledFlag(ddMonthstatusPeriodically.SelectedValue)

                    If ViewState("IdDocEnabled") = "1" Then

                        If filupload.HasFile Then
                            If Not (SaveDocument(filupload, objConn, stTrans, ela_id)) Then
                                retval = 991
                            End If
                        End If


                    End If

                End If
                If retval = "0" Then
                    stTrans.Commit()

                    'V1.3 starts
                    Dim str_Sql As String
                    Dim ds As New DataSet
                    If chkForward.Checked = True Then
                        If ela_id = 0 And chkForward.Checked = True Then
                            str_Sql = "SELECT min(APS_ID) FROM APPROVAL_S where aps_doc_id in (select max(ela_id) from empleaveapp ) and aps_doctype='leave' "
                        Else
                            str_Sql = "SELECT min(APS_ID) FROM APPROVAL_S where aps_doc_id=" & ela_id & "and aps_doctype='leave' "
                        End If
                        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                        If ds.Tables(0).Rows.Count > 0 Then
                            ela_id = ds.Tables(0).Rows(0).Item(0).ToString
                        End If
                    End If

                    'V1.3 ends
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, _
                    h_Emp_No.Value & "-" & ddMonthstatusPeriodically.SelectedItem.Value & "-" & _
                    txtFrom.Text.Trim & "-" & txtTo.Text.Trim & "-" & txtRemarks.Text.Trim, _
                    str_mode, Page.User.Identity.Name.ToString, Me.Page)
                    If retMsg <> " " Then
                        lblError.Text = retMsg + " - " + getErrorMessage("0")
                        retval = 0
                    End If
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    lblError.Text = getErrorMessage("0")
                    ' chkSaveAnyway.Visible = False
                    'setViewData()
                    'setModifyHeader(ela_id, True)
                    'ViewState("datamode") = "view"
                    btnAdd_Click(btnAdd, EventArgs.Empty)
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    clear_All()
                    btnCancelLeave.Visible = False
                Else
                    stTrans.Rollback()
                    lblError.Text = getErrorMessage(retval) + IIf(retMsg <> "", "-", "") + retMsg
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Sub
    Protected Function IsLeaveExceeded() As Boolean

        Try
            If h_Emp_No.Value <> "" Then
                Dim pParms(7) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                pParms(0).Value = h_Emp_No.Value

                pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                pParms(1).Value = Today.Date

                pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
                pParms(2).Value = ddMonthstatusPeriodically.SelectedValue

                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = Session("sBsuid")

                pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
                pParms(4).Value = False
                pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
                pParms(5).Value = False

                pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                If ViewState("PassElaID") = True Then
                    pParms(6).Value = h_ELA_ID.Value
                Else
                    pParms(6).Value = DBNull.Value
                End If

                pParms(7) = New SqlClient.SqlParameter("@ELA_FROMDT", SqlDbType.DateTime)
                pParms(7).Value = txtFrom.Text.Trim



                Dim intLeaveBalnce, days As Integer
                Dim ds As New DataSet
                Dim dtgrid As New DataTable
                Dim adpt As New SqlDataAdapter
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddRange(pParms)
                    adpt.SelectCommand = cmd
                    adpt.Fill(ds)
                End Using
                If ds.Tables(0).Rows.Count > 0 Then
                    intLeaveBalnce = ds.Tables(0).Rows(0).Item(3)
                End If

                Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(txtFrom.Text) & "','" & CDate(txtTo.Text) & "','" & Session("sBsuid") & "'," & h_Emp_No.Value & ",'" & ddMonthstatusPeriodically.SelectedValue & "') as days"
                Dim ds2 As New DataSet
                ds2 = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
                ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
                days = ds2.Tables(0).Rows(0).Item("days")

                If (days > intLeaveBalnce) Then
                    IsLeaveExceeded = True
                    ' chkSaveAnyway.Visible = True
                    'lblError.Text = "Note: You have exceeded your leave balance for current year.Check ""Continue Anyway"" to save details or Cancel to re-apply"
                    'lblError.Text = getErrorMessage("1122")
                Else
                    IsLeaveExceeded = False
                End If


            End If

        Catch
        End Try

    End Function
    'V1.4
    Private Function SaveDocument(ByVal fuUpload As FileUpload, ByVal con As SqlConnection, ByVal tran As SqlTransaction, ByVal PCD_ID As String) As Boolean
        Try


            Dim filePath As String = fuUpload.PostedFile.FileName
            'Dim fuUpload As FileUpload
            'filePath = System.IO.Path.GetExtension(fuUpload.FileName)

            Dim filename As String = Path.GetFileName(filePath)

            Dim ext As String = Path.GetExtension(filename)

            Dim contenttype As String = String.Empty


            Select Case ext
                Case ".doc"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".docx"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".xls"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".xlsx"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".jpg"

                    contenttype = "image/jpg"
                    Exit Select

                Case ".jpeg"

                    contenttype = "image/jpeg"
                    Exit Select

                Case ".tif"

                    contenttype = "image/tiff"
                    Exit Select

                Case ".png"

                    contenttype = "image/png"
                    Exit Select

                Case ".gif"

                    contenttype = "image/gif"
                    Exit Select

                Case ".pdf"

                    contenttype = "application/pdf"
                    Exit Select



            End Select

            If contenttype <> String.Empty Then

                Dim fs As Stream = fuUpload.PostedFile.InputStream
                Dim br As New BinaryReader(fs)
                Dim bytes As Byte() = br.ReadBytes(fs.Length)


                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", "LEAVE")
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bytes)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", Session("sBsuid"))
                Dim bUploadfile As Boolean = InsertUploadedFile(cmd, con, tran)
                If bUploadfile Then
                    lblError.Text = "File Uploaded Successfully"
                    Return True
                Else
                    lblError.Text = UtilityObj.getErrorMessage("991")
                    Return False
                End If

            Else
                lblError.Text = "File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function
    Public Function InsertUploadedFile(ByVal cmd As SqlCommand, ByVal con As SqlConnection, ByVal tran As SqlTransaction) As Boolean

        'Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("conString").ConnectionString()
        'Dim strConnString As String = "Data Source=GEMSPROJECT;Initial Catalog=OASIS_DOCS;Persist Security Info=True;User ID=sa;Password=xf6mt"
        ' Dim con As New SqlConnection(strConnString)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        cmd.Transaction = tran

        Try
            'con.Open()
            cmd.ExecuteNonQuery()
            Return True

        Catch ex As Exception
            Response.Write(ex.Message)
            Return False

        Finally

            'con.Close()
            'con.Dispose()

        End Try

    End Function
    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmployee.Click
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim ds As New DataSet()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid").ToString

            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = h_Emp_No.Value

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getEmpLeaveAppDetails", pParms)
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                txtLRejoindt.Text = Format(ds.Tables(0).Rows(0)("EMP_LASTREJOINDT"), "dd/MMM/yyyy")
                txtPhone.Text = ds.Tables(0).Rows(0)("EMD_PERM_PHONE").ToString
                txtPobox.Text = ds.Tables(0).Rows(0)("EMD_PERM_POBOX").ToString
                txtAddress.Text = ds.Tables(0).Rows(0)("EMD_PERM_ADDRESS").ToString
                txtMobile.Text = ds.Tables(0).Rows(0)("EMD_PERM_MOBILE").ToString
                If txtLRejoindt.Text = "01/Jan/1900" Then
                    txtLRejoindt.Text = ""
                End If
                ViewState("BLS_ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID") 'V1.1
            Else
                txtLRejoindt.Text = ""
            End If
            'GetEmpLeaveCounts()
        Catch ex As Exception
            Errorlog(ex.Message)
            txtLRejoindt.Text = ""
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

        '    setViewData()
        '    clear_All()
        '    ViewState("datamode") = "view"
        '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        'Else
        Response.Redirect(ViewState("ReferrerUrl"))
        'End If
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ResetViewData()
        ViewState("datamode") = "add"
        clear_All()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ResetViewData()

    End Sub
    Sub clear_All()
        h_Emp_No.Value = ""
        h_ELA_ID.Value = ""
        txtEmpNo.Text = ""
        txtMobile.Text = ""
        txtPhone.Text = ""
        txtRemarks.Text = ""
        txtLRejoindt.Text = ""
        txtPobox.Text = ""
        txtAddress.Text = ""
        txtFrom.Text = GetDiplayDate()
        txtTo.Text = ""
        txtHandover.Text = ""
        chkSaveAnyway.Visible = False
        chkSaveAnyway.Checked = False
        GetLoggedInEmployee()
        ddMonthstatusPeriodically.Items.Clear()
        trUplaod.Attributes.Remove("style")
        trUplaod.Attributes.Add("style", "display:none")
        filupload.Attributes.Remove("style")
        filupload.Attributes.Add("style", "display:none")
        imgDoc.Visible = False
        imgDelete.Visible = False
        h_DOC_PCD_ID.Value = ""
        lblView.Visible = False
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If ViewState("canedit") = "yes" Or 1 = 1 Then
            ResetViewData()
            '  If ddMonthstatusPeriodically.SelectedValue = "ML" Then
            getDocumentEnabledFlag(ddMonthstatusPeriodically.SelectedValue)
            If ViewState("IdDocEnabled") = "1" Then
                If h_DOC_PCD_ID.Value <> "" Then
                    If ViewState("canedit") = "yes" Or imgDoc.Visible = True Then
                        If imgDoc.Visible = True Then
                            imgDelete.Visible = True
                        Else
                            imgDelete.Visible = False
                        End If

                    Else
                        imgDoc.Visible = False
                        imgDelete.Visible = False
                        ' filupload.Visible = True
                        filupload.Attributes.Remove("style")
                        filupload.Attributes.Add("style", "display:inline")
                    End If

                End If
            End If
            ViewState("PassElaID") = True
            ViewState("datamode") = "edit"
            GetEmpLeaveCounts()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        ElseIf ViewState("IsCancelled") = "yes" Then
            lblError.Text = "You can not edit the cancelled application ."
        Else
            ViewState("EditForwarded") = True

        End If
    End Sub

    Protected Sub SendEmailToNextApprover(ByVal APS_ID As String, Optional ByVal strStatus As String = "A")
        Dim Mailstatus As String = ""
        Try
            Dim strStatusDesc As String
            Dim getStrStatus As String
            getStrStatus = strStatus
            If strStatus = "A" Then
                strStatusDesc = "Approved"
            ElseIf strStatus = "C" Then
                strStatus = "C"
                getStrStatus = "A"
                strStatusDesc = "Cancelled"
            ElseIf strStatus = "M" Then
                strStatus = "M"
                getStrStatus = "A"
                strStatusDesc = "Modified"
            End If

            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection

            Dim ds As New DataSet()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = Session("sBsuid").ToString

            pParms(1) = New SqlClient.SqlParameter("@APS_ID", SqlDbType.Int)
            pParms(1).Value = Val(APS_ID)

            pParms(2) = New SqlClient.SqlParameter("@status", SqlDbType.VarChar, 2)
            pParms(2).Value = getStrStatus

            pParms(3) = New SqlClient.SqlParameter("@bFromLeave", SqlDbType.Bit)
            pParms(3).Value = True


            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getLeaveApproverMailDetails", pParms)
            'lblError.Text = ds.Tables(0).Rows(0).Item("EMD_EMAIL").ToString
            Dim ToEmailId As String = ""
            Dim ToEMPID As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                ToEmailId = ds.Tables(0).Rows(0).Item("ApproverMailID").ToString
                ToEMPID = ds.Tables(0).Rows(0).Item("ApproverEmpID").ToString
            End If
            If ToEmailId = "" Then
                ToEmailId = "swapna.tv@gemseducation.com"
            End If
            Dim fromemailid = "system@gemseducation.com"
            Dim sb As New StringBuilder
            Dim MainText As String = ""
            Dim Subject As String = ""
            If ds.Tables(0).Rows.Count > 0 Then
                Subject = "ONLINE LEAVE APPLICATION"
                If strStatus = "A" Then
                    MainText = "An application for leave submitted by " & ds.Tables(0).Rows(0).Item("EMPName").ToString() & _
                    " (" & ds.Tables(0).Rows(0).Item("DPT_DESCR").ToString() & ") is pending for approval at your desk "
                ElseIf strStatus = "C" Then
                    MainText = "An application for cancellation of leave application is submitted by " & ds.Tables(0).Rows(0).Item("EMPName").ToString() & _
                    " (" & ds.Tables(0).Rows(0).Item("DPT_DESCR").ToString() & ") is pending for approval at your desk "
                ElseIf strStatus = "M" Then
                    MainText = "An application for leave application is been modified by " & ds.Tables(0).Rows(0).Item("EMPName").ToString() & _
                    " (" & ds.Tables(0).Rows(0).Item("DPT_DESCR").ToString() & ") is pending for approval at your desk "

                End If
                sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear XXXXXXXXXXXXX </td></tr>")
                sb.Append("<tr><td ><br /><br /></td></tr>")
                sb.Append("<tr><td><b>SUB:<b> ONLINE LEAVE APPLICATION APPROVAL</td></tr>")
                sb.Append("<tr><td>" & MainText & "</td></tr>")
                Dim LoginURL As String
                LoginURL = WebConfigurationManager.AppSettings.Item("webvirtualURL") & "/login.aspx?ss=1"
                sb.Append("<tr><td>To view and approve the details, please click on the following link: <a href='" & LoginURL & "'><br />Employee Self Service</a></td></tr>")
                sb.Append("<tr><td ><br /><br />Thanks,</td></tr>")
                sb.Append("<tr><td><b>Team OASIS.</b> </td></tr>")
                sb.Append("<tr><td></td></tr>")
                sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
                sb.Append("<tr></tr><tr></tr>")

                sb.Append("</table>")

            End If
            Dim ds2 As New DataSet
            ds2 = GetCommunicationSettings()

            Dim username = ""
            Dim password = ""
            Dim port = ""
            Dim host = ""
            If ds2.Tables(0).Rows.Count > 0 Then
                username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            End If
            Try
                Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb.ToString.Replace("XXXXXXXXXXXXX", ds.Tables(0).Rows(0).Item("ApproverName")), username, password, host, port, 0, False)
            Catch ex As Exception
            Finally
                Mainclass.SaveEmailSendStatus(Session("sBsuid").ToString, "Leave", ToEMPID, ToEmailId, Subject, sb.ToString.Replace("XXXXXXXXXXXXX", ds.Tables(0).Rows(0).Item("ApproverName")), IIf(LCase(Mailstatus).StartsWith("error"), 0, 1), Mailstatus)
            End Try
            If ds.Tables(0).Rows.Count > 0 Then
                EOS_MainClass.SendMailToDelegates(ds.Tables(0).Rows(0).Item("ApproverEmpID"), "LA", Subject, sb)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Function GetCommunicationSettings() As DataSet
        Dim ds As DataSet
        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
        Return ds
    End Function

    Protected Sub lnkPlanner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPlanner.Click
        Try
            IsPlannerEnabled()
            If txtFrom.Text = "" Or txtTo.Text = "" Then
                lblError.Text = "Enter leave dates"
                Exit Sub
            End If
            If CDate(txtTo.Text) < CDate(txtFrom.Text) Then
                lblError.Text = "Invalid Dates"
                Exit Sub
            End If
            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(txtFrom.Text) & "','" & CDate(txtTo.Text) & "','" & Session("sBsuid") & "'," & h_Emp_No.Value & ",'" & ddMonthstatusPeriodically.SelectedValue & "') as days"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
            ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
            Dim days As String = ds.Tables(0).Rows(0).Item("days")
            Dim url As String = "../Common/PopupShowData.aspx?id=LEAVEPLANNER&StartDate=" + txtFrom.Text + "&Days=" + days + "&BSUID=" + Session("sBsuid") + "&IsPlannerEnabled=" + ViewState("IsPlannerEnabled").ToString
            'Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "LeavePlan", "<script language=javascript>window.open('" + url + "','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=785,height=420,left = 350,top = 200');</script>")
            'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "MyScript", "window.open('" + url + "','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=785,height=420,left = 350,top = 200')", True)
            'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "LeavePlan", "window.showModelessDialog('" + url + "','popup','dialogWidth:785px;dialogHeight:420px;status=0;resizable=1')", True)
            ' ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "LeavePlan", "javascript:radopen('" + url + "','popup1');", True)
            popup1.NavigateUrl = url
            Dim script As String = "function f(){$find(""" + popup1.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)

        Catch ex As Exception
            lblError.Text = "Error Occured"
        End Try
    End Sub

    'Protected Sub lnkEmpLeaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpLeaves.Click
    '    Try
    '        GetEmpLeaveCounts()
    '        'Dim url As String = "../Common/PopupShowData.aspx?id=EMPLEAVECOUNTS&EmpID=" + h_Emp_No.Value + "&Date=" + txtFrom.Text + "&ELTID=" + ddMonthstatusPeriodically.SelectedValue + "&BSUID=" + Session("sBsuid") + "&BDetailed=False"
    '        'Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "LeavePlan", "<script language=javascript>window.open('" + url + "','popup','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=785,height=400,left = 350,top = 200');</script>")
    '    Catch ex As Exception
    '        lblError.Text = "Error Occured"
    '    End Try
    'End Sub
    Public Function GetEmpLeaveCounts() As DataSet
        Try
            If h_Emp_No.Value <> "" Then
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                pParms(0).Value = h_Emp_No.Value

                pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                pParms(1).Value = Today.Date

                pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
                pParms(2).Value = ddMonthstatusPeriodically.SelectedValue

                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = Session("sBsuid")

                pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
                pParms(4).Value = False

                pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
                pParms(5).Value = False

                pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                If ViewState("PassElaID") = True Then
                    pParms(6).Value = h_ELA_ID.Value
                Else
                    pParms(6).Value = DBNull.Value
                End If


                Dim ds As New DataSet
                Dim dtgrid As New DataTable
                Dim adpt As New SqlDataAdapter
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddRange(pParms)
                    adpt.SelectCommand = cmd
                    adpt.Fill(ds)
                    For Each dt As DataTable In ds.Tables
                        If dt.Columns.Count > 2 Then
                            dtgrid = dt
                        End If
                    Next
                    rptGridViewLeave.DataSource = dtgrid
                    rptGridViewLeave.DataBind()
                    HttpContext.Current.Session("divDetail") = divDetail
                    Return ds
                    'GridViewShowDetails.Columns(2).Visible = False
                End Using
            Else
                If h_Emp_No.Value = "" Then
                    lblError.Text = "Select an employee."
                End If
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Try
            Dim b As New StringBuilder()
            Dim itemDiv As HtmlControls.HtmlGenericControl
            itemDiv = HttpContext.Current.Session("divDetail")
            Dim sw As New StringWriter()
            Dim w As New HtmlTextWriter(sw)
            itemDiv.RenderControl(w)
            Dim s As String = sw.GetStringBuilder().ToString()
            b.Append("<table style='background-color:#f3f3f3; ")
            b.Append("width:300px; ' >")
            b.Append("<tr><td >")
            b.Append(s)
            b.Append("</td></tr>")
            b.Append("</table>")
            Return b.ToString
        Catch ex As Exception

        End Try
    End Function
    Protected Sub lnkEmpLeaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpLeaves.Click
        GetEmpLeaveCounts()
        hdlnkEmpLeaves_Click(sender, e)
        'Dim OnMouseOverScript As String = String.Format("$find('{0}').showPopup();", PopupControlExtender2.ClientID)
        'Dim OnMouseOutScript As String = String.Format("$find('{0}').hidePopup();", PopupControlExtender2.ClientID)
        'lnkEmpLeaves.Attributes.Add("onclick", OnMouseOverScript)
        'lnkEmpLeaves.Attributes.Add("onmouseout", OnMouseOutScript)

    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintLeave.Click
        Try
            Dim printId As String = ""
            printId = Replace(h_ELA_ID.Value, "A", "")
            Dim params As New Hashtable
            params.Add("@IMG_BSU_ID", Session("sbsuid"))
            params.Add("@IMG_TYPE", "LOGO")
            params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
            params.Add("@ELA_ID", CInt(printId))
            params.Add("@DocType", "LEAVE")
            ' params("VoucherName") = labHead.Text.ToUpper().ToString()   'V1.1
            params("reportHeading") = "LEAVE APPLICATION FORM"
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "Oasis"
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../Payroll/Reports/Rpt/rptLeaveAppForm.rpt")
                'End If
            End With
            Session("rptClass") = rptClass
            If Session("sModule") = "SS" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewerSS.aspx")
            Else
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request cannot be processed"
        End Try
    End Sub

    Protected Sub hdlnkEmpLeaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdlnkEmpLeaves.Click
        'GetEmpLeaveCounts()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If ViewState("canedit") = "no" Then
            lblError.Text = "Forwarded applications cannot be deleted."
        Else

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = h_ELA_ID.Value ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

            pParms(1) = New SqlClient.SqlParameter("@retVal", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer = 0

            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim stTrans As SqlTransaction = conn.BeginTransaction


            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[DeleteEmpLeaveApp]", pParms)
            If pParms(1).Value = 0 Then
                lblError.Text = UtilityObj.getErrorMessage("0")
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, "Leave Application deleting", _
                 "Delete", Page.User.Identity.Name.ToString, Me.Page)
                clear_All()
            Else
                lblError.Text = UtilityObj.getErrorMessage("retval")
                stTrans.Rollback()
            End If
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Session("sModule") = "SS" Then
            Me.MasterPageFile = "../mainMasterPageSS.master"
        Else
            Me.MasterPageFile = "../mainMasterPage.master"
        End If

        Dim smScriptManager As New ScriptManager
        smScriptManager = Page.Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True


    End Sub

    Protected Sub txtFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(txtFrom.Text) Then
            If txtTo.Text = "" Then
                txtTo.Text = txtFrom.Text
            Else
                If Convert.ToDateTime(txtTo.Text) < Convert.ToDateTime(txtFrom.Text) Then
                    txtTo.Text = txtFrom.Text
                End If

            End If
        End If
        chkSaveAnyway.Visible = False
        chkSaveAnyway.Checked = False
        GetEmpLeaveCounts()
    End Sub
    Protected Sub ddMonthstatusPeriodically_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMonthstatusPeriodically.DataBound
        ddMonthstatusPeriodically.Items.Insert(0, "-- Select--")
        If Request.QueryString("viewid") = "" Then
            ddMonthstatusPeriodically.SelectedIndex = 0
        End If
    End Sub
    Private Sub ddMonthstatusPeriodically_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddMonthstatusPeriodically.SelectedIndexChanged
        GetEmpLeaveCounts()
        getDocumentEnabledFlag(ddMonthstatusPeriodically.SelectedValue)
        If ViewState("IdDocEnabled") = "1" Then
            trUplaod.Attributes.Remove("style")
            'trUplaod.Attributes.Add("style", "display:inline")
            trUplaod.Visible = True
            filupload.Attributes.Remove("style")
            filupload.Attributes.Add("style", "display:inline")
        Else
            trUplaod.Attributes.Remove("style")
            trUplaod.Attributes.Add("style", "display:none")
            trUplaod.Visible = False
            filupload.Attributes.Remove("style")
            filupload.Attributes.Add("style", "display:none")
        End If
    End Sub


    Protected Sub txtTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged
        chkSaveAnyway.Visible = False
        chkSaveAnyway.Checked = False
    End Sub
    Protected Sub getDocumentEnabledFlag(ByVal strLeaveType As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        If Not ddMonthstatusPeriodically.SelectedIndex = 0 Then
            Dim Sql_Query = "select Isnull(EAS_BDocCheck,0) EAS_BDocCheck from dbo.EMPATTENDANCE_STATUS where eas_elt_id= '" & strLeaveType & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows(0).Item("EAS_BDocCheck") = True Then
                ViewState("IdDocEnabled") = "1"
            Else
                ViewState("IdDocEnabled") = "0"
            End If
        Else
            ViewState("IdDocEnabled") = "0"
        End If
    End Sub

    Protected Sub btnCancelLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelLeave.Click
        If ViewState("canedit") = "yes" Then
            lblError.Text = "Only Forwarded applications can be Cancelled.Try Delete!!"
        Else

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = h_ELA_ID.Value ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

            pParms(1) = New SqlClient.SqlParameter("@retVal", SqlDbType.VarChar, 200)
            pParms(1).Value = ""

            pParms(1).Direction = ParameterDirection.ReturnValue
            Dim retval As Integer = 0

            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim stTrans As SqlTransaction = conn.BeginTransaction
            Try
                retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[CancelEmpLeaveApp]", pParms)
                If pParms(1).Value = 0 Then
                    'lblError.Text = getErrorMessage("0")
                    lblError.Text = "Request for Cancellation made successfully."
                    stTrans.Commit()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, "Leave Application Cancelling", _
                     "Cancel", Page.User.Identity.Name.ToString, Me.Page)
                    chkSaveAnyway.Visible = False

                    setViewData()
                    setModifyHeader(h_ID.Value)
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    clear_All()
                    btnCancelLeave.Visible = False
                    btnCancelLeave.Visible = False
                    'clear_All()
                Else
                    lblError.Text = UtilityObj.getErrorMessage("retval")
                    stTrans.Rollback()
                End If
            Catch ex As Exception
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage("1")
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub
    Protected Sub h_Emp_No_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles h_Emp_No.ValueChanged
        GetEmpLeaveCounts()
    End Sub
    Private Sub OpenFileFromDB(ByVal DocId As String)
        Try


            Dim conn As String = ""

            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("OpenDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)

            Dim myReader As SqlDataReader = cmd.ExecuteReader()

            If myReader.Read Then
                Response.ClearHeaders()
                Response.ContentType = myReader("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(myReader("DOC_DOCUMENT"), Byte())
                'Response.Buffer = True
                'Response.Charset = ""
                ' Response.Cache.SetCacheability(HttpCacheability.NoCache)
                'Response.AddHeader("Cache-control", "no-cache")
                Response.AddHeader("content-disposition", "attachment;filename=" & myReader("DOC_DOCNO").ToString())
                Response.Clear()
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.Close()
                'Response.End()

            End If
            myReader.Close()

        Catch ex As Exception
            lblError.Text = ex.Message
            lblError.Text = UtilityObj.getErrorMessage("1000")

        End Try

    End Sub


    Private Sub DeleteFileFromDB(ByVal DocId As String)
        Try


            Dim conn As String = ""

            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString

            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("DeleteDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)
            cmd.Parameters.Add("@retVal", SqlDbType.BigInt)
            cmd.Parameters("@retVal").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            objcon.Close()
            Dim ret As Integer = 0
            ret = cmd.Parameters("@retVal").Value
            If ret <> 0 Then
                lblError.Text = UtilityObj.getErrorMessage("1000")
            Else
                lblError.Text = "Document removed successfully."
                imgDelete.Visible = False
                imgDoc.Visible = False
                lblView.Visible = False
            End If


        Catch ex As Exception
            lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try

    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDelete.Click
        DeleteFileFromDB(h_DOC_PCD_ID.Value)
        'filupload.Visible = True
        filupload.Attributes.Remove("style")
        filupload.Attributes.Add("style", "inline")


    End Sub
    ' Added by vikranth on 1st Aug 2019
    Protected Sub txtEmpNo_TextChanged(sender As Object, e As EventArgs) Handles txtEmpNo.TextChanged
        Try
            If h_Emp_No.Value.ToString() <> "" Then
                Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim ds As DataSet
                Dim params(1) As SqlParameter
                params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
                params(0).Value = "LeaveTypes"
                params(1) = New SqlClient.SqlParameter("@Emp_Id", SqlDbType.VarChar)
                params(1).Value = h_Emp_No.Value
                ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Me.ddMonthstatusPeriodically.DataTextField = "ELT_DESCR"
                    Me.ddMonthstatusPeriodically.DataValueField = "ELT_ID"
                    Me.ddMonthstatusPeriodically.DataSource = ds.Tables(0)
                    Me.ddMonthstatusPeriodically.DataBind()
                End If
                Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds1 As New DataSet()
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(0).Value = Session("sBsuid").ToString

                pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                pParms(1).Value = h_Emp_No.Value

                ds1 = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "getEmpLeaveAppDetails", pParms)
                'Dim ds As New DataSet
                'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds1.Tables(0).Rows.Count > 0 Then
                    txtLRejoindt.Text = Format(ds1.Tables(0).Rows(0)("EMP_LASTREJOINDT"), "dd/MMM/yyyy")
                    txtPhone.Text = ds1.Tables(0).Rows(0)("EMD_PERM_PHONE").ToString
                    txtPobox.Text = ds1.Tables(0).Rows(0)("EMD_PERM_POBOX").ToString
                    txtAddress.Text = ds1.Tables(0).Rows(0)("EMD_PERM_ADDRESS").ToString
                    txtMobile.Text = ds1.Tables(0).Rows(0)("EMD_PERM_MOBILE").ToString
                    If txtLRejoindt.Text = "01/Jan/1900" Then
                        txtLRejoindt.Text = ""
                    End If
                    ViewState("BLS_ECT_ID") = ds1.Tables(0).Rows(0)("EMP_ECT_ID") 'V1.1
                Else
                    txtLRejoindt.Text = ""
                End If
            End If
        Catch ex As Exception
            Me.lblError.Text = "There was an issue loading leave types"
        End Try
    End Sub
End Class
