﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Exit_workflow.aspx.vb" Inherits="Exit_workflow" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no,shrink-to-fit=no" />
    <title>Exit Employee Workflow</title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js" type="text/javascript"></script>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <style>
        a label {
            cursor: pointer;
        }

        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }

        .borderLeft {
            border-left-style: solid;
            border-left-width: 1px;
            border-color: black;
        }

        .borderRight {
            border-right-style: solid;
            border-right-width: 1px;
            border-color: black;
        }

        .borderTop {
            border-top-style: solid;
            border-top-width: 1px;
            border-color: black;
        }

        .borderBottom {
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-color: black;
        }
    </style>

    <script type="text/javascript">
        function SaveData() {
            var allData = ""
            $("input[type=checkbox]").each(function (a) {
                allData = allData + "###" + this.id + "$$$" + this.checked;
            })
            $("textarea").each(function (a) {
                allData = allData + "###" + this.id + "$$$" + this.value;
            })
            $("file").each(function (a) {
                //alert(this);
                //allData = allData + "###" + this.id + "$$$" + this.value;
            })
            $("#hdnValues").val(allData);
            //var fl = document.getElementById('FileUpload1').files[0];

            //if (fl) {
            //    //document.getElementById('fileUploadHdn').files = document.getElementById('FileUpload1').files;               
                
            //}
            
        } 
        function downloadDoc(lstid) {
            $("#hdnLSTID").val(lstid)
            $("#btnDownload").click();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptmgr" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
        <asp:HiddenField ID="hdnValues" runat="server" />
        <asp:HiddenField ID="hdnLSTID" runat="server" />
       <%-- <asp:FileUpload ID="fileUploadHdn" runat="server" Style="display: none;" />--%>
        <asp:Button ID="btnDownload" Text="" runat="server" Style="display: none" />

        <div class="card mb-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="tblCategory" cellspacing="2" cellpadding="2" width="100%" runat="server">

                        <tr>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                            <td width="25%"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <table width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="72%" style="background-color: deepskyblue; /* border-radius: 10px; */
                                              border-top-left-radius: 8px; border-bottom-right-radius: 8px;"></td>
                                            <td align="right">
                                                <img src="https://school.gemsoasis.com/phoenixbeta/images/GEMS_LOGOHR.jpg" alt="" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center" style="font-size: 20px">
                                <b>GEMS Internal Clearance Form</b>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">*This form must be signed by each of the below Departments for any employee leaving the company.

                            </td>
                        </tr>
                        <tr id="trLabelError">
                            <td></td>
                            <td align="left" class="matters" valign="bottom" colspan="2">
                                <div id="lblError" runat="server">
                                </div>
                            </td>
                        </tr>
                         
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td width="50%"></td><td width="50%"></td>
                                    </tr>
                                    <tr>
                                        <td><b>
                                            <br />
                                            Employee Information</b></td>
                                    </tr>
                                    <tr>
                                        <td class="borderLeft borderTop">Employee Name:</td>
                                        <td class="borderTop borderRight">
                                            <asp:Label ID="lblEMP_NAME" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr> 
                                        <td class="borderLeft">Designation:</td>
                                        <td class="borderRight">
                                            <asp:Label ID="lblDesignation" runat="server" Text=""></asp:Label></td>
                                    </tr> 
                                    <tr> 
                                        <td class="borderLeft">Department:</td>
                                        <td class="borderRight">
                                            <asp:Label ID="lblEmpDepartment" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr> 
                                        <td class="borderLeft">D.O.J:</td>
                                        <td class="borderRight">
                                            <asp:Label ID="lblDOJ" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr> 
                                        <td class="borderLeft">Employment end date:</td>
                                        <td class="borderRight">
                                            <asp:Label ID="lblEOS" runat="server" Text=""></asp:Label></td>
                                    </tr> 
                                    <tr> 
                                        <td class="borderLeft">Last working day:</td>
                                        <td class="borderRight">
                                            <asp:Label ID="lblLWD" runat="server" Text=""></asp:Label></td>
                                    </tr> 
                                    <tr> 
                                        <td colspan="2" class="borderLeft borderRight borderBottom borderTop">
                                            <table width="100%">
                                                <tr>
                                                    <td width="25%">Forwarding phone:</td>
                                                    <td width="25%" class="borderRight">
                                                        <asp:Label ID="lblFPhone" runat="server" Text=""></asp:Label></td>
                                                    <td width="10%">Email:</td>
                                                    <td width="40%">
                                                        <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                            </table>

                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <asp:Table ID="tblDefault" runat="server" Visible="false" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell><br />
                                                <b>Department clearance:</b>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell CssClass="borderLeft borderRight borderTop">
                                            <asp:CheckBox ID="CheckBox999999_1" runat="server" Text="Company owned property returned" />
                                            <br />
                                            <asp:CheckBox ID="CheckBox999999_2" runat="server" Text="Direct reports moved to new Line Manager" />
                                            <br />
                                            <asp:CheckBox ID="CheckBox999999_3" runat="server" Text="Termination/Resignation letter signed by employee  " />
                                            <br />
                                            <asp:CheckBox ID="CheckBox999999_4" runat="server" Text="Handover completed" />
                                            <br />
                                            <asp:TextBox ID="TextBox999999_1" runat="server" placeholder="Comment" TextMode="MultiLine" Rows="4" Width="60%" ></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableFooterRow>
                                        <asp:TableCell CssClass="borderLeft borderRight borderBottom">
                                            Approver :
                                            <asp:Label ID="lblLineManager" runat="server" Text=""></asp:Label>
                                            <br />
                                            Date:<asp:Label ID="lblDefaultDate" runat="server" Text=""></asp:Label><br />
                                        </asp:TableCell>
                                    </asp:TableFooterRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <table width="100%" runat="server" id="tblDepartmentMain" visible="false">
                                    <tr>

                                        <td><b>
                                            <asp:Label ID="lblDepartment" runat="server" Text=""></asp:Label></b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td colspan="2" class="borderTop borderLeft borderRight">
                                            <asp:Table ID="tblDepartment" runat="server" width="100%">
                                            </asp:Table>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr class="borderLeft borderRight" id="trfileUpload1" runat="server" visible="false">
                                        <td colspan="2">
                                            <table width="100%">
                                                <tr>
                                                    <td> Upload Documents</td>
                                                    <td>
                                                        <asp:FileUpload ID="fileUpload1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                           
                                        </td>
                                       
                                    </tr>
                                    <tr>

                                        <td colspan="2" class="borderBottom borderLeft borderRight">
                                            <asp:Label ID="lblDepartmentName" runat="server" Text=""></asp:Label>
                                            <br />
                                            <asp:Label ID="lblDateTime" runat="server" Text=""></asp:Label>
                                        </td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <asp:Table ID="tblAllDepartments" runat="server" Width="100%">
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>

                        </tr>



                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" OnClientClick="return SaveData();" />
                                <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="button" OnClientClick="return SaveData();" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>




    </form>

</body>
</html>

