<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EmployeeCardprint_STS.aspx.vb" Inherits="Students_Studcardprint" Title="Untitled Page" %>

<%@ Register Src="usercontrols/EmployeeCardPrint_STS.ascx" TagName="EmployeeCardPrint_STS" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Print Employee ID Card(STS)
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <uc1:EmployeeCardPrint_STS ID="EmployeeCardPrint_STS1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

