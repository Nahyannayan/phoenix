﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Exit_workflow_Master_View.aspx.vb" Inherits="Exit_workflow_Master_View" %>

<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(".frameAddPDC").fancybox({
                type: 'iframe',
                fitToView: false,
                width: '85%',
                height: '95%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>EXIT WORKFLOW USER MASTER
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table border="0" width="100%">
                    <tr id="trLabelError">
                        <td align="left" class="matters" valign="bottom">
                            <div id="lblError" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                    <tr id="trAdd">
                        <td align="left">                                                        
                             <%--<asp:LinkButton ID="lbtnAdd" runat="server" CausesValidation="false" Text="Add New"
                                 OnClick="lbtnAdd_Click"></asp:LinkButton> --%>
                            <a id="frameAdd" class="frameAddPDC" href="Exit_workflow_Master_Add.aspx?Id=0">ADD NEW</a>
                           
                        </td>
                    </tr>
                    <tr id="trGridv" runat="server">
                        <td align="center">

                            <asp:GridView ID="gvEmpList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="10" Width="100%" OnPageIndexChanging="gvEmpList_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="EXT_WF_USR_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXT_WF_USR_ID" runat="server" Text='<%# Bind("EXT_WF_USR_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Department Name
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtDPT_DESCR" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchDPT_DESCR" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchDPT_DESCR"></asp:ImageButton> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDPT_DESCR" runat="server" Text='<%# Bind("DPT_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="field-label">Employee Name
                                            </span>
                                            <br />
                                            <asp:TextBox ID="txtEMP_NAME" runat="server" Width="160px"></asp:TextBox>

                                            <asp:ImageButton ID="btnSearchEMP_NAME" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                ImageAlign="Top" OnClick="btnSearchEMP_NAME"></asp:ImageButton> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMP_NAME" runat="server" Text='<%# Bind("EMP_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                          <span class="field-label">Is Active
                                            </span> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblisActive" runat="server" Text='<%# Bind("isActive")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderTemplate>
                                          <span class="field-label"> Included In Email Alert Only
                                            </span> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXT_WF_INCLUDE_IN_WORKFLOW" runat="server" Text='<%# Bind("EXT_WF_INCLUDE_IN_WORKFLOW")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                          <span class="field-label"> Is HR User
                                            </span> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXT_WF_USR_IS_HR" runat="server" Text='<%# Bind("EXT_WF_USR_IS_HR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypEdit" runat="server" CssClass="frameAddPDC">Edit</asp:HyperLink>
                                            <%--<asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Text="Edit" CssClass="frameAddPDC"
                                                OnClick="lbtnEdit_Click"></asp:LinkButton> --%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

