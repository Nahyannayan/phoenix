<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffRptStatitics.aspx.vb" Inherits="OASIS_HR_StaffReports_StaffRptStatitics" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Employee Statitics Report
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  width="20%"><span class="field-label">From</span>
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server" ValidationGroup="Search">
                            </asp:TextBox>

                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
                            TargetControlID="txtFromDate">
                        </ajaxToolkit:CalendarExtender>
                        </td>
                        
                        <td width="20%"><span class="field-label">To </span></td>
                        <td width="30%">
                            <asp:TextBox ID="txtToDate" runat="server" ValidationGroup="Search">
                            </asp:TextBox>
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton3"
                        TargetControlID="txtToDate">
                    </ajaxToolkit:CalendarExtender>
                        </td>

                    </tr>                    
                    <tr>
                        <td width="20%"><span class="field-label">Business Unit</span></td>
                        <td width="30%">
                            <asp:DropDownList ID="cmbBusUnits" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="cmbSurvey_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="4">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                                Text="Show Report" OnClick="btnSearch_Click" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

