﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
'-------------------------------------------------------------------------
''Purpose:To view employee history
'Version      Date          Done by         Purpose
' 1.1       13-Jan-2010     Swapna          Initial Release
'-------------------------------------------------------------------------
Partial Class StaffReports_empTransHistory
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not Page.IsPostBack Then

            ViewState("datamode") = "none"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            'ViewState("datamode") = Encr_decrData.Decrypt(ViewState("datamode"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'collect the url of the file to be redirected in view state
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000190") Then    'V1.1
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

            If ViewState("datamode") = "view" Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else
                BindTranypes()
            End If
            txtEmpName.Attributes.Add("ReadOnly", "ReadOnly")


        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub BindTranypes()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetAllEmpTranTypes")
        chkTransTypes.DataSource = ds.Tables(0)
        chkTransTypes.DataTextField = ds.Tables(0).Columns("TTP_DESCR").ToString
        chkTransTypes.DataValueField = ds.Tables(0).Columns("TTP_ID").ToString
        chkTransTypes.DataBind()
        chkTransTypes.Items.Insert(0, "Select All")

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenReport.Click
        CallReport()
    End Sub
    Protected Sub CallReport()
        ' Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim Ason_Date As String = ""
        Dim i As Integer = 0

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If
        Dim strOptions As String = ""
        'For Each chkItem As ListItem In chkTransTypes.Items
        '    If (chkItem.Selected) Then
        '        strOptions = strOptions + "," + chkItem.Value
        '    End If
        'Next
        For i = 1 To chkTransTypes.Items.Count - 1
            If (chkTransTypes.Items.Item(i).Selected) Then
                strOptions = strOptions + "," + chkTransTypes.Items.Item(i).Value
            End If
        Next

        If strOptions <> "" Then



            Dim param As New Hashtable
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@EmpID", h_Emp_ID.Value)
            param.Add("EmpName", txtEmpName.Text)
            param.Add("@TTPID", strOptions)
            param.Add("@DTFrom", CDate(txtDate.Text))
            param.Add("Group", ddlGroupOption.Text)



            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param
                If ViewState("MainMnu_code") = "H000190" Then
                    .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffHistory.rpt")

                End If

            End With
            Session("rptClass") = rptClass
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else
            lblError.Text = "Select a transaction."
        End If
    End Sub

    Protected Sub chkTransTypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTransTypes.SelectedIndexChanged
        'To check and uncheck all when "Select All" is clicked
        Dim result As String = Request.Form("__EVENTTARGET")
        Dim checkedBox() As String = result.Split("$")
        Dim index As Integer = Integer.Parse(checkedBox(checkedBox.Length - 1))

        If (index = 0) Then
            Dim value As Boolean = chkTransTypes.Items.Item(0).Selected
            For Each listItem As ListItem In chkTransTypes.Items
                listItem.Selected = value
            Next
        End If

    End Sub
End Class
