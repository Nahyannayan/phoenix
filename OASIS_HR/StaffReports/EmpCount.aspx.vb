Imports Oasis_Administrator
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
'Version            Date            Author          Change
'1.1                06-sep-2011     Swapna          Bug fix when no date selection available
'1.2                21-dec-2011     Swapna          New report to view employee leave balance details
'1.3                17-May-2012     Swapna          New report to view Airfare details
'1.4                29-May-2012     Swapna          New report to view Salary slips emailed.
'1.5                03-Jun-2012     Swapna          To add Final Settlement filter for salary reports
Partial Class Reports_ASPX_Report_EmpCount
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Shared bsuID As String
    Dim MainMnu_code As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ClientScript.RegisterStartupScript(Me.GetType(), _
            "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            If Not Page.IsPostBack Then
                hfEmpName.Value = "display"
                hfBSU.Value = "display"
                hfCategory.Value = "display"
                hfDepartment.Value = "display"
                hfDesignation.Value = "display"
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Page.Title = OASISConstants.Gemstitle
                UsrBSUnits1.MenuCode = MainMnu_code
                Select Case MainMnu_code
                    Case "P159005", "P159010", "P159015", "P159020", "P159025", "P159030", "P159035", "P159040", "P159045", "P159034", "P150023", "P130176" 'V1.3 , V1.4
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employees Salary Details"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        If MainMnu_code = "P130176" Then
                            imgBankSel.OnClientClick = "GetEMPNAME2(); return false;"
                        End If
                        trPrintA3.Visible = (MainMnu_code = "P159005")
                        trShowAllMonths.Visible = False
                    Case "P130078"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        imgBankSel.OnClientClick = "GetEMPNAME();"
                        lblrptCaption.Text = "Employees Leave Balance Details"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = True
                        trSelDepartment.Visible = True
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trAsOnDate.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        treeCat.Visible = False
                        tryear.Visible = True
                        trShowAllMonths.Visible = False
                        FillYear()
                    Case "P159062"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Salary Not Processed"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trBSUTree.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = False
                        trSelEMPName.Visible = False
                        trProcessingDate.Visible = False
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trPrintA3.Visible = (MainMnu_code = "P159005")
                        trShowAllMonths.Visible = False
                    Case "P450046"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Air Fare Details (Not Processed)"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trShowAllMonths.Visible = False
                    Case "H000172"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Air Fare Details "
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        trShowAllMonths.Visible = False

                    Case "P450048"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employee Air Fare Processed Details "
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = False
                        trSelDesignation.Visible = False
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = True
                        trShowAllMonths.Visible = False
                        trException.Visible = True

                    Case "H200005"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Insurance Export"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = False
                        trSelDepartment.Visible = False
                        trDesignation.Visible = True
                        trSelDesignation.Visible = True
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        trMonth_Year.Visible = False
                        trShowAllMonths.Visible = False
                        trException.Visible = False
                    Case Else '"P450051"
                        imgGetBSUName.OnClientClick = "GetBSUNameSingle();"
                        lblrptCaption.Text = "Employees Contact Details"
                        trBSUnit.Visible = False
                        trSelBSU.Visible = False
                        trCategory.Visible = False
                        trSelcategory.Visible = False
                        trDepartment.Visible = True
                        trSelDepartment.Visible = True
                        trDesignation.Visible = True
                        trSelDesignation.Visible = True
                        trEMPName.Visible = True
                        trSelEMPName.Visible = True
                        BindCategory()
                        chkEMPABC_A.Checked = True
                        chkEMPABC_B.Checked = True
                        chkEMPABC_C.Checked = True
                        'trEMPABCCAT.Visible = False
                        trSummary.Visible = False
                        trWPS.Visible = False
                        If MainMnu_code = "P130176" Then
                            imgBankSel.OnClientClick = "GetEMPNAME2(); return false;"
                        End If
                        trPrintA3.Visible = (MainMnu_code = "P159005")
                        trShowAllMonths.Visible = False
                End Select


                If MainMnu_code = "P130176" Then
                    imgBankSel.OnClientClick = "GetEMPNAME2(); return false;"
                End If
                trPrintA3.Visible = (MainMnu_code = "P159005")
                If MainMnu_code = "P159020" Then
                    trAsOnDate.Visible = True
                    txtAsOnDate.Text = Format(Now.Date, "dd/MMM/yyyy")
                End If

                If MainMnu_code = "P159005" Or MainMnu_code = "P159020" Then

                    trFinalSettlement.Visible = True 'V1.5
                End If
            End If
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            If h_BSUID.Value Is Nothing Or h_BSUID.Value = "" Or h_BSUID.Value = "undefined" Then
                h_BSUID.Value = Session("sBsuid")
            End If
            FillBSUNames(h_BSUID.Value)
            Select Case MainMnu_code
                Case "P152001", "P152002", "P152026", "P152025", "P150025", "P150040", "P150035", "P150026", "P130078", "P159034", "P150023", "P130176", "P450048", "H200005"
                    FillDeptNames(h_DEPTID.Value)
                    FillCATNames(h_CATID.Value)
                    FillDESGNames(h_DESGID.Value)
                    FillEmpNames(h_EMPID.Value)
                    StoreEMPFilter()
                Case "P159030"
                    trWPS.Visible = True
                Case "P159010"
                    trSummary.Visible = True
                Case "P450046"
                    FillEmpNames(h_EMPID.Value)
                Case Else
                    FillDeptNames(h_DEPTID.Value)
                    FillCATNames(h_CATID.Value)
                    FillDESGNames(h_DESGID.Value)
                    FillEmpNames(h_EMPID.Value)
                    StoreEMPFilter()
            End Select

            If Not IsPostBack Then
                If Not MainMnu_code = "P130078" Then
                    FillPayYearPayMonth()
                    If Request.UrlReferrer <> Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                End If
                If MainMnu_code = "P159034" Or MainMnu_code = "P150023" Or MainMnu_code = "P130176" Or MainMnu_code = "P450046" Or MainMnu_code = "H000172" Or MainMnu_code = "P450048" Or _
                MainMnu_code = "H200005" Then

                    trProcessingDate.Visible = False
                    trEMPABCCAT.Visible = True

                    trEMPName.Visible = True
                    trSelEMPName.Visible = True
                End If
            End If

            If Not IsPostBack Then
                If MainMnu_code = "P159005" Or MainMnu_code = "P159020" Then
                    Me.trCombined.Visible = True
                Else
                    Me.trCombined.Visible = False
                End If
                Me.optActive.Checked = True
                optActive_CheckedChanged(Nothing, Nothing)
                Me.BindNationality()

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub BindCategory()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        UsrTreeView1.DataSource = ds
        UsrTreeView1.DataTextField = "DESCR"
        UsrTreeView1.DataValueField = "ID"
        UsrTreeView1.DataBind()
    End Sub
    Private Sub FillYear() 'V1.2
        Try
            Dim strSql As String
            Dim EMP_ID As String
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            strSql = "SELECT DISTINCT YEAR(ELS_DTFROM) mYear FROM EMPLEAVESCHEDULE_D WHERE ELS_BSU_ID = '" & Session("sBsuid") & "' and ELS_DTFROM is not null order by YEAR(ELS_DTFROM) "
            fillDropdown(ddlYear, strSql, "mYear", "mYear", str_conn, True)
            If ddlYear.Items.Count > 0 Then
                ddlYear.SelectedIndex = ddlYear.Items.Count - 1
            End If

            'If Not Request.QueryString("SelValue") Is Nothing Then
            '    ddlYear.SelectedIndex = ddlYear.Items.Count - 1
            '    'If ddlYear.Items.FindByValue(Request.QueryString("SelValue").ToString) IsNot Nothing Then
            '    '    ddlYear.SelectedIndex = ddlYear.Items.Count - 1
            '    'End If
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub fillDropdown(ByVal drpObj As DropDownList, ByVal sqlQuery As String, ByVal txtFiled As String, ByVal valueFiled As String, ByVal connStr As String, ByVal addValue As Boolean)
        drpObj.DataSource = Mainclass.getDataTable(sqlQuery, connStr)
        drpObj.DataTextField = txtFiled
        drpObj.DataValueField = valueFiled
        drpObj.DataBind()
        'If addValue.Equals(True) Then
        '    drpObj.Items.Insert(0, " ")
        '    drpObj.Items(0).Value = "0"
        '    drpObj.SelectedValue = "0"
        'End If
    End Sub
    Private Sub FillPayYearPayMonth()

        Dim lst(12) As ListItem
        lst(0) = New ListItem("January", 1)
        lst(1) = New ListItem("February", 2)
        lst(2) = New ListItem("March", 3)
        lst(3) = New ListItem("April", 4)
        lst(4) = New ListItem("May", 5)
        lst(5) = New ListItem("June", 6)
        lst(6) = New ListItem("July", 7)
        lst(7) = New ListItem("August", 8)
        lst(8) = New ListItem("September", 9)
        lst(9) = New ListItem("October", 10)
        lst(10) = New ListItem("November", 11)
        lst(11) = New ListItem("December", 12)
        For i As Integer = 0 To 11
            ddlPayMonth.Items.Add(lst(i))
        Next

        Dim iyear As Integer = Session("BSU_PAYYEAR")
        'For i As Integer = iyear - 1 To iyear + 1
        '    ddlPayYear.Items.Add(i.ToString())
        'Next
        For i As Integer = 2010 To iyear + 1
            ddlPayYear.Items.Add(i.ToString())
        Next

        'ddlPayMonth.SelectedValue = Session("BSU_PAYYEAR")
        'ddlPayYear.SelectedValue = 0
        '''''''
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_ID," _
                & " BSU_FREEZEDT ,  BSU_PAYMONTH, BSU_PAYYEAR " _
                & " FROM  BUSINESSUNIT_M " _
                & " where BSU_ID='" & Session("sBsuid") & "'"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                'BSU_ID, BSU_FREEZEDT, BSU_PAYMONTH, BSU_PAYYEAR
                'txtPhone.Text = ds.Tables(0).Rows(0)("ELA_PHONE").ToString
                ddlPayMonth.SelectedIndex = -1
                ddlPayYear.SelectedIndex = -1
                ddlPayMonth.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYMONTH"))).Selected = True
                ddlPayYear.Items.FindByValue(CInt(ds.Tables(0).Rows(0)("BSU_PAYYEAR"))).Selected = True
                ViewState("freezdate") = ds.Tables(0).Rows(0)("BSU_FREEZEDT").ToString
            Else
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub StoreEMPFilter()
        Dim str_Filter As String = String.Empty
        If h_BSUID.Value <> "" Then
            str_Filter = " AND " & GetFilter("EMP_BSU_ID", h_BSUID.Value)
        End If
        If h_CATID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_ECT_ID", h_CATID.Value)
        End If
        If h_DEPTID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DPT_ID", h_DEPTID.Value)
        End If
        If h_DESGID.Value <> "" Then
            str_Filter += " AND " & GetFilter("EMP_DES_ID", h_DESGID.Value)
        End If
        Session("EMP_SEL_COND") = str_Filter
        '"WHERE EMP_ECT_ID IN ('1','2') AND EMP_DPT_ID IN ('2') AND EMP_DES_ID IN ('')"
    End Sub

    Private Function GetBSUName(ByVal BSUName As String) As String
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN('" + BSUName + "')"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return String.Empty
        End Try
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Select Case MainMnu_code
            'Case "P159005"
            '    GenerateSalarySummaryFormat1()
            'Case "P159010"
            '    GenerateSalarySummaryFormat2()
            'Case "P159015"
            '    GenerateSalarySummaryFormat3()
            'Case "P159020"
            '    GenerateSalarySummaryFormat4()
            'Case "P159025"
            '    GenerateSalarySummaryFormat5()
            'Case "P159030"
            '    GenerateSalarySummaryFormat6()
            'Case "P159035"
            '    GenerateSalarySummaryFormat7()
            'Case "P159040"
            '    'GenerateSalarySummaryFormat8()
            '    'GenerateSalarySummaryFormat9()
            '    GenerateSalarySummaryFormat11()
            'Case "P159045"
            '    GenerateSalarySummaryFormat13()
            '    'Case "P159045"
            '    '    GenerateSalarySummaryFormat9()
            '    'Case "P159045"
            '    '    GenerateSalarySummaryFormat11()
            'Case "P130078"
            '    GenerateLeaveBalanceDetails()
            'Case "P159034" 'v1.3
            '    GenerateAirfareForMonthDetails()
            'Case "P150023"
            '    GenerateSalarySlipMailedDetails()
            'Case "P159062"
            '    GenerateSalaryNotProcessedReport()
            'Case "P130176"
            '    GenerateHoldTillDateReport()
            'Case "P450046", "H000172"
            '    GenerateAirFareDetailsReport()
            'Case "P450048"
            '    GenerateAirFareProcessedDetailsReport()
            'Case "H200005"
            '    OpenInsuranceExportReport()
            Case 0
            Case Else
                'Me.GenerateEmpContactDetails()
                Me.GenerateEmpCountDetails()
        End Select

    End Sub
    Protected Sub OpenInsuranceExportReport()

        Session("InsuBSUIds") = UsrBSUnits1.GetSelectedNode("|")
        Session("InsuEmpIds") = h_EMPID.Value
        Session("InsuDesIds") = h_DESGID.Value
        Response.Redirect("~/Payroll/empInsuranceExportExcel.aspx?Catid=" & UsrTreeView1.GetSelectedNode("|") & "&ABCCat=" & GetABCCategory())
    End Sub

    Protected Sub GenerateHoldTillDateReport()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim cmd As New SqlCommand("rptHoldTillNowDetails", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@bsu_id", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@year", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)

            Dim sqlpMonth As New SqlParameter("@month", SqlDbType.VarChar, 5)
            sqlpMonth.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptHoldTillNowReport.rpt"

            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Protected Sub GenerateSalarySlipMailedDetails()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        Try

            Dim cmd As New SqlCommand("rptGetSalaryEmailsSentStatus", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpBSU_ID As New SqlParameter("@BSU_id", SqlDbType.VarChar, 5000)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)


            Dim sqlpEMP_ID As New SqlParameter("@EMP_ID", SqlDbType.VarChar, 5000)
            sqlpEMP_ID.Value = h_EMPID.Value
            cmd.Parameters.Add(sqlpEMP_ID)

            Dim sqlpYear As New SqlParameter("@year", SqlDbType.VarChar, 5)
            sqlpYear.Value = ddlPayYear.SelectedValue
            cmd.Parameters.Add(sqlpYear)


            Dim sqlpMonth As New SqlParameter("@month", SqlDbType.VarChar, 5)
            sqlpMonth.Value = ddlPayMonth.SelectedValue
            cmd.Parameters.Add(sqlpMonth)

            Dim sqlpCat As New SqlParameter("@Cat_ID", SqlDbType.VarChar, 5)
            sqlpCat.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCat)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            params("userName") = Session("sUsr_name")

            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text

            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryEmailsSendStatus.rpt"

            Session("ReportSource") = repSource
            'Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

   


    Private Sub GenerateEmpCountDetails()
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection

        'If Not Me.optActive.Checked Then
        '    If Me.txtFromDate.Text = Nothing Or Me.txtToDate.Text = Nothing Or Not IsDate(Me.txtFromDate.Text) Or Not IsDate(Me.txtToDate.Text) Then
        '        Me.lblError.Text = "Please enter valid a valid from date & to date."
        '        Exit Sub
        '    End If
        'End If

        Try

            Dim cmd As New SqlCommand("[getCategoryWiseEmpCounts]", objConn)
            cmd.CommandType = CommandType.StoredProcedure

            'If Not Me.optActive.Checked Then
            If (Not Me.txtFromDate.Text = Nothing And Not Me.txtToDate.Text = Nothing) And (IsDate(Me.txtFromDate.Text) And IsDate(Me.txtToDate.Text)) Then
                Dim sqlpFromDate As New SqlParameter("@From_Date", SqlDbType.DateTime)
                sqlpFromDate.Value = txtFromDate.Text
                cmd.Parameters.Add(sqlpFromDate)

                Dim sqlpToDate As New SqlParameter("@To_Date", SqlDbType.DateTime)
                sqlpToDate.Value = txtToDate.Text
                cmd.Parameters.Add(sqlpToDate)
            Else
                'Dim sqlpFromDate As New SqlParameter("@From_Date", SqlDbType.DateTime)
                'sqlpFromDate.Value = "1900-01-01"
                'cmd.Parameters.Add(sqlpFromDate)

                'Dim sqlpToDate As New SqlParameter("@To_Date", SqlDbType.DateTime)
                'sqlpToDate.Value = "2099-12-31"
                'cmd.Parameters.Add(sqlpToDate)
            End If

            Dim sqlpStatus As New SqlParameter("@Status", SqlDbType.VarChar)
            If Me.optJoined.Checked Then
                sqlpStatus.Value = "Joined"
            ElseIf Me.optResigned.Checked Then
                sqlpStatus.Value = "Resigned"
            ElseIf Me.optTerminated.Checked Then
                sqlpStatus.Value = "Terminated"
            ElseIf Me.optActive.Checked Then
                sqlpStatus.Value = "Active"
            End If
            cmd.Parameters.Add(sqlpStatus)

            Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar)
            sqlpBSU_ID.Value = UsrBSUnits1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpCAT_ID As New SqlParameter("@CAT_ID", SqlDbType.VarChar)
            sqlpCAT_ID.Value = UsrTreeView1.GetSelectedNode("|")
            cmd.Parameters.Add(sqlpCAT_ID)

            Dim sqlpDep_ID As New SqlParameter("@Dep_ID", SqlDbType.VarChar)
            'If Me.hfDepartment.Value = "display" Then Me.h_DEPTID.Value = 0
            'sqlpDep_ID.Value = Me.hfDepartment.Value.ToString 
            sqlpDep_ID.Value = Me.h_DEPTID.Value.ToString
            cmd.Parameters.Add(sqlpDep_ID)

            Dim sqlpDes_ID As New SqlParameter("@Des_ID", SqlDbType.VarChar)
            'If Me.hfDesignation.Value = "display" Then Me.h_DESGID.Value = 0
            'sqlpDes_ID.Value = Me.hfDesignation.Value.ToString 
            sqlpDes_ID.Value = Me.h_DESGID.Value.ToString
            cmd.Parameters.Add(sqlpDes_ID)

            Dim sqlpEmp_ID As New SqlParameter("@Emp_ID", SqlDbType.VarChar)
            If Me.h_EMPID.Value = "display" Then Me.h_EMPID.Value = 0
            sqlpEmp_ID.Value = Me.h_EMPID.Value
            cmd.Parameters.Add(sqlpEmp_ID)

            Dim sqlpABC_CAT As New SqlParameter("@ABC_CAT", SqlDbType.VarChar, 20)
            sqlpABC_CAT.Value = GetABCCategory()
            cmd.Parameters.Add(sqlpABC_CAT)

            Dim sqlpNationalityId As New SqlParameter("@Nationality_Id", SqlDbType.Int)
            sqlpNationalityId.Value = Me.ddNationality.SelectedValue  '0
            cmd.Parameters.Add(sqlpNationalityId)

            'If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Dim repSource As New MyReportClass
            Dim params As New Hashtable

            'If ddlProcessingDate.SelectedValue = "" Then V1.1
            ' ''If ddlProcessingDate.Items.Count = 0 Then
            ' ''    params("PROCESS_DATE") = ""
            ' ''Else
            ' ''    params("PROCESS_DATE") = IIf(trProcessingDate.Visible = True AndAlso Not chkIncludeJoiningDate.Checked, "(" + ddlProcessingDate.SelectedItem.Text + ")", "")
            ' ''End If
            'If Not Me.chkIncludeJoiningDate.Checked Then
            '    params("PROCESS_DATE") = ""
            'Else
            '    params("PROCESS_DATE") = "(" + txtFromDate.Text + " To " + txtToDate.Text + ")"
            'End If
            params("userName") = Session("sUsr_name")
            'params("PAYMONTH") = ddlPayMonth.SelectedItem.Text
            'params("PAYYEAR") = ddlPayYear.SelectedItem.Text
            'params("IsFinalSettlement") = chkFF.Checked

            If Me.optJoined.Checked Then
                params("ReportName") = "Joined"
            ElseIf Me.optResigned.Checked Then
                params("ReportName") = "Resigned"
            ElseIf Me.optTerminated.Checked Then
                params("ReportName") = "Terminated"
            ElseIf Me.optActive.Checked Then
                params("ReportName") = "Active"
            End If

            repSource.Parameter = params
            repSource.Command = cmd

            'Dim ds As New DataSet '= SqlHelper.ExecuteDataset(objConn, "getCategoryWiseEmpCounts", cmd.Parameters)
            'Dim adp As New SqlDataAdapter(cmd)
            'adp.Fill(ds)


            ' ''If chkPrintA3.Checked Then
            ' ''    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptEmpContactDetails.rpt"
            ' ''Else
            ' ''    repSource.ResourceName = "../../PAYROLL/REPORTS/RPT/rptSalaryDetails_Format1.rpt"
            ' ''End If
            repSource.ResourceName = "../../Oasis_HR/StaffReports/REPORTS/rptEmpCount.rpt"


            Session("ReportSource") = repSource
            'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
            ReportLoadSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally
            objConn.Close()
        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Sub BindNationality()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "Select * From (Select 0 as Cty_Id, '[Select]' as Cty_Nationality Union  SELECT CTY_ID, CTY_NATIONALITY FROM Country_M) T WHERE (T.Cty_Nationality IS NOT NULL AND T.Cty_Nationality <> '-') Order By T.Cty_Nationality "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.Text, str_Sql)
        ddNationality.DataSource = ds 'OasisAdministrator.Nationality()
        ddNationality.DataTextField = "CTY_NATIONALITY"
        ddNationality.DataValueField = "CTY_ID"
        ddNationality.DataBind()
    End Sub


    Public Function GetABCCategory() As String

        Dim strABC As String = ""

        If chkEMPABC_A.Checked Then strABC += "A|"
        If chkEMPABC_B.Checked Then strABC += "B|"
        If chkEMPABC_C.Checked Then strABC += "C|"

        Return strABC
    End Function

    Private Function GetFilter(ByVal fieldName As String, ByVal values As String, Optional ByVal addAND As Boolean = False) As String
        Dim condition As String = String.Empty
        If values <> "" Then
            Dim IDs As String() = values.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If i <> 0 Then
                    condition += ", "
                End If
                condition += "'" & IDs(i) & "'"
                i += 1
            Next
            If addAND Then
                fieldName = " AND " & fieldName
            End If
            condition = fieldName + " IN (" + condition + ") "
        End If
        Return condition
    End Function

    'Generates the XML for BSUnit
    Private Function GenerateXML(ByVal BSUIDs As String, ByVal type As XMLType) As String
        'If BSUIDs = String.Empty Or BSUIDs = "" Then
        '    Return String.Empty
        'End If
        Dim xmlDoc As New XmlDocument
        Dim BSUDetails As XmlElement
        Dim XMLEBSUID As XmlElement
        Dim XMLEBSUDetail As XmlElement
        Dim elements As String() = New String(3) {}
        Select Case type
            Case XMLType.BSUName
                elements(0) = "BSU_DETAILS"
                elements(1) = "BSU_DETAIL"
                elements(2) = "BSU_ID"
            Case XMLType.EMPName
                elements(0) = "EMP_DETAILS"
                elements(1) = "EMP_DETAIL"
                elements(2) = "EMP_ID"
            Case XMLType.AMOUNT
                elements(0) = "AMOUNT_DETAILS"
                elements(1) = "AMOUNTS"
                elements(2) = "AMOUNT"
        End Select
        Try
            BSUDetails = xmlDoc.CreateElement(elements(0))
            xmlDoc.AppendChild(BSUDetails)
            Dim IDs As String() = BSUIDs.Split("||")
            For i As Integer = 0 To IDs.Length - 1
                If IDs(i) <> "" Then
                    XMLEBSUDetail = xmlDoc.CreateElement(elements(1))
                    XMLEBSUID = xmlDoc.CreateElement(elements(2))
                    XMLEBSUID.InnerText = IDs(i)
                    XMLEBSUDetail.AppendChild(XMLEBSUID)
                    xmlDoc.DocumentElement.InsertBefore(XMLEBSUDetail, xmlDoc.DocumentElement.LastChild)
                End If
                i += 1
            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If ViewState("ReferrerUrl") <> "" Then
            Response.Redirect(ViewState("ReferrerUrl").ToString())
        Else
            Response.Redirect("../../Homepage.aspx")
        End If
    End Sub

    Private Function FillEmpNames(ByVal EMPIDs As String) As Boolean
        Dim IDs As String() = EMPIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "select EMPNO as ID, EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M WHERE EMP_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEMPName.DataSource = ds
        gvEMPName.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDeptNames(ByVal DEPTIDs As String) As Boolean
        Dim IDs As String() = DEPTIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M WHERE DPT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDept.DataSource = ds
        gvDept.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean

        Dim IDs As String() = CATIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M WHERE ECT_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillDESGNames(ByVal DESGIDs As String) As Boolean
        Dim IDs As String() = DESGIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M WHERE DES_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvDesg.DataSource = ds
        gvDesg.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddBSUID.Click
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub

    Protected Sub grdACTDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEMPName.PageIndexChanging
        gvEMPName.PageIndex = e.NewPageIndex
        FillBSUNames(h_EMPID.Value)
    End Sub

    Protected Sub gvDept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDept.PageIndexChanging
        gvDept.PageIndex = e.NewPageIndex
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub gvDesg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDesg.PageIndexChanging
        gvDesg.PageIndex = e.NewPageIndex
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnkbtngrdDeptDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDEPTID As New Label
        lblDEPTID = TryCast(sender.FindControl("lblDEPTID"), Label)
        If Not lblDEPTID Is Nothing Then
            h_DEPTID.Value = h_DEPTID.Value.Replace(lblDEPTID.Text, "").Replace("||||", "||")
            gvDept.PageIndex = gvDept.PageIndex
            FillDeptNames(h_DEPTID.Value)
        End If

    End Sub

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||||", "||")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdDESGDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblDESGID As New Label
        lblDESGID = TryCast(sender.FindControl("lblDESGID"), Label)
        If Not lblDESGID Is Nothing Then
            h_DESGID.Value = h_DESGID.Value.Replace(lblDESGID.Text, "").Replace("||||", "||")
            gvDesg.PageIndex = gvDesg.PageIndex
            FillDESGNames(h_DESGID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdEMPDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMPID As New Label
        lblEMPID = TryCast(sender.FindControl("lblEMPID"), Label)
        If Not lblEMPID Is Nothing Then
            h_EMPID.Value = h_EMPID.Value.Replace(lblEMPID.Text, "").Replace("||||", "||")
            gvEMPName.PageIndex = gvEMPName.PageIndex
            FillEmpNames(h_EMPID.Value)
        End If
    End Sub

    Protected Sub lnlbtnAddDEPTID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDEPTID.Click
        h_DEPTID.Value += "||" + txtDeptName.Text.Replace(",", "||")
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "||" + txtCatName.Text.Replace(",", "||")
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub lnlbtnAddDESGID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddDESGID.Click
        h_DESGID.Value += "||" + txtDesgName.Text.Replace(",", "||")
        FillDESGNames(h_DESGID.Value)
    End Sub

    Protected Sub lnlbtnAddEMPID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddEMPID.Click
        h_EMPID.Value += "||" + txtEMPNAME.Text.Replace(",", "||")
        FillEmpNames(h_EMPID.Value)
    End Sub

    Protected Sub ddlPayMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateProcessingDate()
    End Sub

    Protected Sub ddlPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateProcessingDate()
    End Sub

    Private Sub PopulateProcessingDate()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        str_Sql = "SELECT DISTINCT REPLACE(CONVERT(VARCHAR(11), ESD_LOGDT, 106), ' ', '/') AS ESD_DATE " & _
        "FROM EMPSALARYDATA_D where esd_month = '" & ddlPayMonth.SelectedValue & "' AND ESD_YEAR = '" & ddlPayYear.SelectedValue & _
        "' AND ESD_BSU_ID in ('" & UsrBSUnits1.GetSelectedNode("','") & "')"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing OrElse ds.Tables Is Nothing OrElse ds.Tables(0).Rows.Count <= 1 Then
            trProcessingDate.Visible = False
            Exit Sub
        End If
        trProcessingDate.Visible = True
        ddlProcessingDate.DataSource = ds
        ddlProcessingDate.DataTextField = "ESD_DATE"
        ddlProcessingDate.DataValueField = "ESD_DATE"
        ddlProcessingDate.DataBind()
    End Sub

    ' ''Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    ' ''    PopulateProcessingDate()
    ' ''    If MainMnu_code = "P159034" Or MainMnu_code = "P150023" Or MainMnu_code = "P159062" Or MainMnu_code = "P130176" Or MainMnu_code = "P450046" Or MainMnu_code = "H000172" Or MainMnu_code = "P450048" Or _
    ' ''    MainMnu_code = "H200005" Then 'V1.4
    ' ''        trProcessingDate.Visible = False
    ' ''    End If


    ' ''End Sub

    Protected Sub chkExcludeProcessDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProcessingDate.Enabled = Not chkIncludeJoiningDate.Checked
    End Sub

    Protected Sub optActive_CheckedChanged(sender As Object, e As System.EventArgs) Handles optActive.CheckedChanged
        If Me.optActive.Checked Then
            Me.trMonth_Year.Visible = False
            trAsOnDate.Visible = True
            txtAsOnDate.Text = Format(Now.Date, "dd/MMM/yyyy")
        Else
            Me.trMonth_Year.Visible = True
            trAsOnDate.Visible = False
        End If
    End Sub

    Protected Sub optJoined_CheckedChanged(sender As Object, e As System.EventArgs) Handles optJoined.CheckedChanged
        If Me.optJoined.Checked Then
            Me.trMonth_Year.Visible = True
        Else
            Me.trMonth_Year.Visible = False
        End If
    End Sub

    Protected Sub optResigned_CheckedChanged(sender As Object, e As System.EventArgs) Handles optResigned.CheckedChanged
        If Me.optResigned.Checked Then
            Me.trMonth_Year.Visible = True
        Else
            Me.trMonth_Year.Visible = False
        End If
    End Sub

    Protected Sub optTerminated_CheckedChanged(sender As Object, e As System.EventArgs) Handles optTerminated.CheckedChanged
        If Me.optTerminated.Checked Then
            Me.trMonth_Year.Visible = True
        Else
            Me.trMonth_Year.Visible = False
        End If
    End Sub

    Protected Sub txtBSUName_TextChanged(sender As Object, e As EventArgs)
        txtBSUName.Text = ""
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub txtDeptName_TextChanged(sender As Object, e As EventArgs)
        txtDeptName.Text = ""
        FillDeptNames(h_DEPTID.Value)
    End Sub

    Protected Sub txtCatName_TextChanged(sender As Object, e As EventArgs)
        txtCatName.Text = ""
        FillCATNames(h_CATID.Value)
    End Sub

    Protected Sub txtDesgName_TextChanged(sender As Object, e As EventArgs)
        txtDesgName.Text = ""
        FillDESGNames(h_DESGID.Value)
    End Sub
    Protected Sub txtEMPNAME_TextChanged(sender As Object, e As EventArgs)
        txtEMPNAME.Text = ""
        FillEmpNames(h_EMPID.Value)
    End Sub

End Class