﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports Telerik.Web.UI
Imports Telerik.Charting
Imports System.Drawing.Imaging

Partial Class Reports_ASPX_Report_StaffAgeAnalysis
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")
        'smScriptManager.EnablePartialRendering = False
      
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            UsrBSUnits1.MenuCode = MainMnu_code
            Page.Title = OASISConstants.Gemstitle
            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            UtilityObj.NoOpen(Me.Header)

            If Not IsPostBack Then
                If Request.UrlReferrer <> Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtFromDate.Text = String.Format("{0:dd/MMM/yyyy}", CDate(UtilityObj.GetDiplayDate()))
                'txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
                BindEmpNationality()
                BindEmpStatus()
                BindEmpVisaType()
                BindEmpDesignation()
                BindEmpStaffGroup()
                BindEmpDepartment()
                BindEmpLocalOverseas()
                BindYear()
                panel_chart.Visible = False
            End If
            Dim ScriptManager1 As ScriptManager = Master.FindControl("ScriptManager1")
            ScriptManager1.RegisterPostBackControl(btnExport)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetSelectedItems(ByVal RadCBox As RadComboBox) As String
        Dim Selected As String = String.Empty
        Dim Li As Telerik.Web.UI.RadComboBoxItem
        For Each Li In RadCBox.Items
            Dim chk As CheckBox = DirectCast(Li.FindControl("chkComboItem"), CheckBox)
            Dim hdf As HiddenField = DirectCast(Li.FindControl("hdnComboItem"), HiddenField)
            If chk.Checked = True Then
                Selected = IIf(Selected.Trim = "", hdf.Value, Selected + "|" + hdf.Value)
            End If
        Next
        Return Selected
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        h_BSUID.Value = UsrBSUnits1.GetSelectedNode()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim parm(15) As SqlClient.SqlParameter
        parm(0) = New SqlClient.SqlParameter("@OPTION", 1)
        parm(1) = New SqlClient.SqlParameter("@Date", Convert.ToDateTime(txtFromDate.Text))

        If (h_BSUID.Value <> "") Then
            parm(2) = New SqlClient.SqlParameter("@BSU_ID", h_BSUID.Value)
        End If

        Dim nationality As String = GetSelectedItems(cblEmpNationality).Trim()
        If (nationality <> "") Then
            parm(3) = New SqlClient.SqlParameter("@CTY_ID", GetSelectedItems(cblEmpNationality))
        End If

        Dim Designation As String = GetSelectedItems(cblEmpDesignation).Trim()
        If (Designation <> "") Then
            parm(4) = New SqlClient.SqlParameter("@DES_ID", Designation)
        End If

        Dim LocalOverseas As String = GetSelectedItems(cblEmpLocalOverseas).Trim()
        If (LocalOverseas <> "") Then
            parm(5) = New SqlClient.SqlParameter("@local_OverSeas", LocalOverseas)
        End If

        Dim VisaType As String = GetSelectedItems(cblEmpVisaType).Trim()
        If (VisaType <> "") Then
            parm(6) = New SqlClient.SqlParameter("@EVM_ID", VisaType)
        End If

        Dim EmpGroup As String = GetSelectedItems(cblEmpGroup).Trim()
        If (EmpGroup <> "") Then
            parm(7) = New SqlClient.SqlParameter("@ECT_ID", EmpGroup)
        End If

        Dim EmpStaus As String = GetSelectedItems(cblEmpStaus).Trim()
        If (EmpStaus <> "") Then
            parm(8) = New SqlClient.SqlParameter("@EST_ID", EmpStaus)
        End If

        Dim EmpDepartment As String = GetSelectedItems(cblEmpDepartment).Trim()
        If (EmpDepartment <> "") Then
            parm(9) = New SqlClient.SqlParameter("@DPT_ID", EmpDepartment)
        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Rpt_EmpAgeAnalysis", parm)

        Try



            If (ds.Tables.Count > 0) Then
                Dim myTable As DataTable
                myTable = ds.Tables(0)
                Dim CurrentAgeSum As Object
                CurrentAgeSum = myTable.Compute("Sum(CurrentAgeAvg)", "1=1")


                Dim PrevAgeSum As Object
                PrevAgeSum = myTable.Compute("Sum(PrevAgeAvg)", "1=1")

                Dim twoYearBackSum As Object
                twoYearBackSum = myTable.Compute("Sum(twoYearBackAgeAvg)", "1=1")

                Dim CurrentYearCount As Object
                CurrentYearCount = myTable.Compute("Sum(CurrentYearCount)", "1=1")

                Dim PrevYearCount As Object
                PrevYearCount = myTable.Compute("Sum(PrevYearCount)", "1=1")

                Dim twoYearBackCount As Object
                twoYearBackCount = myTable.Compute("Sum(twoYearBackCount)", "1=1")


                Dim CurrentAgeAverage As Decimal = CurrentAgeSum / CurrentYearCount
                Dim PrevYearAverage As Decimal = PrevAgeSum / PrevYearCount
                Dim twoYearBackAverage As Decimal = twoYearBackSum / twoYearBackCount
                lblaveragecuurent.Text = Math.Round(CurrentAgeAverage, 2)
                lblaverageprev.Text = Math.Round(PrevYearAverage, 2)
                lblaveragetwoyear.Text = Math.Round(twoYearBackAverage, 2)


            End If
        Catch ex As Exception

        End Try


        RadChart1.DataSource = ds

        RadChart1.Series.Clear()

        RadChart1.PlotArea.XAxis.AutoScale = False
        RadChart1.PlotArea.XAxis.MinValue = 20.0
        RadChart1.PlotArea.XAxis.MaxValue = 70.0
        RadChart1.PlotArea.XAxis.LabelStep = 5

        RadChart1.PlotArea.XAxis.AxisLabel.Visible = True
        RadChart1.PlotArea.YAxis.AxisLabel.Visible = True
        'RadChart1.PlotArea.XAxis.LayoutMode = Styles.ChartAxisLayoutMode.Inside
        RadChart1.PlotArea.XAxis.LayoutMode = Styles.ChartAxisLayoutMode.Normal


        Dim SelectedYears As String = GetSelectedItems(cblYears).Trim()
        If (SelectedYears.Trim() = "") Then
            SelectedYears = "0|1|2"
        End If
        If (SelectedYears.Contains("0")) Then
            Dim currentyear As New ChartSeries()
            currentyear.Type = ChartSeriesType.Line


            currentyear.Appearance.EmptyValue.Mode = Styles.EmtyValuesMode.Zero
            currentyear.Appearance.EmptyValue.Line.Color = Drawing.Color.Transparent


            'currentyear.Appearance.PointMark.Visible = True
            'currentyear.Appearance.PointMark.Dimensions.AutoSize = False
            'currentyear.Appearance.PointMark.Dimensions.Height = 5
            'currentyear.Appearance.PointMark.Border.Width = 1
            'currentyear.Appearance.PointMark.Border.Color = Drawing.Color.FromArgb(211, 185, 123)

            currentyear.Appearance.PointMark.Visible = True
            currentyear.Appearance.PointMark.Dimensions.AutoSize = False
            currentyear.Appearance.PointMark.Dimensions.Height = 5
            currentyear.Appearance.PointMark.Border.Width = 1
            currentyear.Appearance.PointMark.Border.Color = Drawing.Color.BurlyWood

            lblaveragecuurent.ForeColor = Drawing.Color.BurlyWood

            currentyear.Appearance.PointMark.FillStyle.MainColor = Drawing.Color.FromArgb(211, 185, 123)

            currentyear.Appearance.PointMark.FillStyle.FillType = Styles.FillType.Solid

            currentyear.Appearance.LabelAppearance.Visible = True

            currentyear.Name = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtFromDate.Text))
            'currentyear.LabelsAppearance.Visible = True
            'currentyear.LabelsAppearance.DataFormatString = "{1}"
            currentyear.ActiveRegionToolTip = "staff of age "
            currentyear.DataXColumn = "CurrentAge"
            currentyear.DataYColumn = "CurrentYearCount"
            ' currentyear.ActiveRegionToolTip = currentyear.. & " staff of age " & currentyear.DataYColumn
            RadChart1.Series.Add(currentyear)
        End If
        If (SelectedYears.Contains("1")) Then
            Dim oneyearback As New ChartSeries()
            oneyearback.Type = ChartSeriesType.Line
            oneyearback.Name = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtFromDate.Text).AddYears(-1))
            oneyearback.Appearance.LabelAppearance.Visible = True
            oneyearback.Appearance.EmptyValue.Mode = Styles.EmtyValuesMode.Zero
            oneyearback.Appearance.EmptyValue.Line.Color = Drawing.Color.Transparent
            oneyearback.Appearance.PointMark.Visible = True
            oneyearback.Appearance.PointMark.Dimensions.AutoSize = False
            oneyearback.Appearance.PointMark.Dimensions.Height = 5
            oneyearback.Appearance.PointMark.Border.Width = 1
            'oneyearback.Appearance.PointMark.Border.Color = Drawing.Color.FromArgb(149, 193, 204)
            'oneyearback.Appearance.PointMark.FillStyle.MainColor = Drawing.Color.FromArgb(149, 193, 204)
            oneyearback.Appearance.PointMark.Border.Color = Drawing.Color.LimeGreen
            oneyearback.Appearance.PointMark.FillStyle.MainColor = Drawing.Color.LimeGreen

            lblaverageprev.ForeColor = Drawing.Color.LimeGreen

            oneyearback.Appearance.PointMark.FillStyle.FillType = Styles.FillType.Solid

            'oneyearback.LabelsAppearance.Visible = True
            'oneyearback.LabelsAppearance.DataFormatString = "{1}"
            'oneyearback.TooltipsAppearance.DataFormatString = "{1} staff of age {0}"
            oneyearback.DataXColumn = "PrevAge"
            oneyearback.DataYColumn = "PrevYearCount"
            RadChart1.Series.Add(oneyearback)
        End If

        If (SelectedYears.Contains("2")) Then
            Dim Twoyearback As New ChartSeries()

            Twoyearback.Appearance.LabelAppearance.Visible = True
            'Twoyearback.Appearance.Lab = True
            Twoyearback.Type = ChartSeriesType.Line
            Twoyearback.Name = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(txtFromDate.Text).AddYears(-2))

            Twoyearback.Appearance.EmptyValue.Mode = Styles.EmtyValuesMode.Zero
            Twoyearback.Appearance.EmptyValue.Line.Color = Drawing.Color.Transparent
            Twoyearback.Appearance.PointMark.Visible = True
            Twoyearback.Appearance.PointMark.Dimensions.AutoSize = False
            Twoyearback.Appearance.PointMark.Dimensions.Height = 5
            Twoyearback.Appearance.PointMark.Border.Width = 1
            'Twoyearback.Appearance.PointMark.Border.Color = Drawing.Color.FromArgb(164, 194, 122)
            'Twoyearback.Appearance.PointMark.FillStyle.MainColor = Drawing.Color.FromArgb(164, 194, 122)
            Twoyearback.Appearance.PointMark.Border.Color = Drawing.Color.Red
            Twoyearback.Appearance.PointMark.FillStyle.MainColor = Drawing.Color.Red
            Twoyearback.Appearance.PointMark.FillStyle.FillType = Styles.FillType.Solid
            lblaveragetwoyear.ForeColor = Drawing.Color.Red
            'Twoyearback.LabelsAppearance.Visible = True
            'Twoyearback.LabelsAppearance.DataFormatString = "{1}"
            'Twoyearback.TooltipsAppearance.DataFormatString = "{1} staff of age {0}"
            Twoyearback.DataXColumn = "twoYearBackAge"
            Twoyearback.DataYColumn = "twoYearBackCount"
            RadChart1.Series.Add(Twoyearback)
        End If
        RadChart1.DataBind()
        'Dim ScatterLineSeries1 As ScatterLineSeries = DirectCast((RadChart1.PlotArea.Series(0)), ScatterLineSeries)

        panel_chart.Visible = True
        'RadChart1.DataSource = ds
        'RadChart1.DataBind()
        'RadChart1.Skin = "DeepRed"

    End Sub

#Region "BindControls"
    Private Sub BindEmpNationality()
        Dim qry As String = "select CTY_ID,CTY_NATIONALITY  from COUNTRY_M  Order By 2"
        Me.cblEmpNationality.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpNationality.DataBind()
    End Sub

    Private Sub BindEmpStatus()
        Dim qry As String = "select est_id,est_descr from EMPSTATUS_M Order By 2"
        Me.cblEmpStaus.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpStaus.DataBind()
    End Sub

    Private Sub BindEmpVisaType()
        Dim qry As String = "SELECT  EVM_DESCR , EVM_ID FROM  EMPVISATYPE_M where EVM_DESCR<>'Please Select' order by 1"
        Me.cblEmpVisaType.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpVisaType.DataBind()
    End Sub

    Private Sub BindEmpDesignation()
        Dim qry As String = "select des_id, des_descr from EMPDESIGNATION_M WHERE (DES_FLAG = 'SD')   Order By 2"
        Me.cblEmpDesignation.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpDesignation.DataBind()
    End Sub

    Private Sub BindEmpStaffGroup()
        Dim qry As String = "select ECT_ID,ECT_DESCR from EMPCATEGORY_M Order By 2"
        Me.cblEmpGroup.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpGroup.DataBind()
    End Sub

    Private Sub BindEmpDepartment()
        Dim qry As String = "select dpt_id,dpt_descr from DEPARTMENT_M Order By 2"
        Me.cblEmpDepartment.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpDepartment.DataBind()
    End Sub

    Private Sub BindEmpLocalOverseas()
        Dim qry As String = "select 'Local' deslocovs ,'False' locovs union select 'Overseas' , 'True' locovs "
        Me.cblEmpLocalOverseas.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblEmpLocalOverseas.DataBind()
    End Sub

    Private Sub BindYear()
        Dim qry As String = "select 'Current' Year_Text ,'0' Year_value union select 'Current - 1' Year_Text, '1' Year_value union select 'Current - 2' Year_Text, '2' Year_value order by Year_value"
        Me.cblYears.DataSource = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        Me.cblYears.DataBind()
    End Sub
#End Region

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        ClearSelectedItems(cblEmpNationality)
        ClearSelectedItems(cblEmpStaus)
        ClearSelectedItems(cblEmpVisaType)
        ClearSelectedItems(cblEmpDesignation)
        ClearSelectedItems(cblEmpGroup)
        ClearSelectedItems(cblEmpDepartment)
        ClearSelectedItems(cblEmpLocalOverseas)
        panel_chart.Visible = False

    End Sub

    Private Sub ClearSelectedItems(ByVal RadCBox As RadComboBox)
        Dim Selected As String = String.Empty
        Dim Li As Telerik.Web.UI.RadComboBoxItem
        For Each Li In RadCBox.Items
            Dim chk As CheckBox = DirectCast(Li.FindControl("chkComboItem"), CheckBox)
            chk.Checked = False
        Next
    End Sub


    Protected Sub RadChart1_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Charting.ChartItemDataBoundEventArgs) Handles RadChart1.ItemDataBound
        e.SeriesItem.ActiveRegion.Tooltip = e.SeriesItem.YValue & " staff of age " & e.SeriesItem.XValue
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim imageStream As MemoryStream = drawChart()

        Dim imageContent As [Byte]() = New [Byte](imageStream.Length - 1) {}
        imageStream.Position = 0
        imageStream.Read(imageContent, 0, Convert.ToInt32(imageStream.Length))
        imageStream.Close()


        'imageStream.Position = 0
        'imageStream.Read(imageContent, 0, Convert.ToInt32(imageStream.Length))
        'imageStream.Close()

        'Response.ContentType = "image/jpeg"
        'Response.BinaryWrite(imageContent)
        Dim Filename As String = "AgeAnalysis.jpg"
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "image/jpeg"
        Response.AddHeader("Content-Disposition", "attachment;filename=" + Filename)
        Response.BinaryWrite(imageContent)
        Response.Flush()
        HttpContext.Current.ApplicationInstance.CompleteRequest()

    End Sub

    Private Function drawChart() As MemoryStream
        Dim mStream As New MemoryStream()
        RadChart1.Save(mStream, ImageFormat.Jpeg)
        Return mStream
    End Function


End Class
