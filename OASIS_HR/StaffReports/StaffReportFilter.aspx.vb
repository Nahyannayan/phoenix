
Partial Class StaffReports_StaffReportFilter
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            Dim menucode As String = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            If menucode = "H000193" Then
                lblTopTitle.Text = "Empty Date List"
            ElseIf menucode = "H000194" Then
                lblTopTitle.Text = "Passport Borrow"
            ElseIf menucode = "H000195" Then
                lblTopTitle.Text = "Passport Pending Return"
            ElseIf menucode = "H000196" Then
                lblTopTitle.Text = "MOL Inspection List"
            ElseIf menucode = "H000197" Then
                lblTopTitle.Text = "Nationality Statistics"
            ElseIf menucode = "H000198" Then
                lblTopTitle.Text = "Qualification Category"
            ElseIf menucode = "H000199" Then
                lblTopTitle.Text = "Qualification Statistics"
            ElseIf menucode = "H000202" Then
                lblTopTitle.Text = "Staff Usernames Report"
            ElseIf menucode = "H000299" Then
                lblTopTitle.Text = "Staff List New"
            ElseIf menucode = "H000110" Then
                lblTopTitle.Text = "Health Card Expiry"
            ElseIf menucode = "H000102" Then
                lblTopTitle.Text = "Document Expiry (Working Unit)"
            ElseIf menucode = "H000191" Then
                lblTopTitle.Text = "Staff List"
            ElseIf menucode = "H000187" Then
                lblTopTitle.Text = "Expiry Report - Days Left"
            ElseIf menucode = "H000188" Then
                lblTopTitle.Text = "Super Annual"
            ElseIf menucode = "H000192" Then
                lblTopTitle.Text = "Document Expiry"
            End If

        End If
    End Sub

End Class
