<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffAgeAnalysis.aspx.vb" Inherits="Reports_ASPX_Report_StaffAgeAnalysis" %>

<%@ Register Src="../../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .RadComboBox .rcbInputCell {
            width: 100%;
            height: 31px !important;
            background-color: transparent !important;
            border-radius: 6px !important;
            background-image: none !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
        }

        .RadComboBox_Default {
            /* color: #333; */
            font: inherit;
            width: 80% !important;
        }

            .RadComboBox_Default .rcbFocused .rcbInput {
                color: black;
                padding: 20px;
            }

            .RadComboBox_Default .rcbEmptyMessage {
                padding: 20px;
            }

        .RadComboBox .rcbEmptyMessage {
            font-style: inherit !important;
        }

        .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image: none !important;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0px !important;
            border-width: 0;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblrptCaption" runat="server" Text="Staff Age Analysis"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" cellpadding="0" cellspacing="0" width="100%" runat="server" id="tblMain">
                    <%-- <tr  class="subheader_img">
            <td align="left" colspan="6" style="height: 19px" valign="middle">                 
                        
            </td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                        <td align="left" colspan="4">
                            <div class="checkbox-list">
                                <uc1:usrBSUnits ID="UsrBSUnits1" runat="server" />
                                <asp:HiddenField ID="h_BSUID" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Nationality</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpNationality" runat="server"
                                DataTextField="CTY_NATIONALITY" EmptyMessage="Nationality" Filter="Contains"
                                DataValueField="CTY_ID">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("CTY_NATIONALITY") %>'>
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("CTY_ID") %>' />
                                    </div>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Designation</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpDesignation" runat="server" Filter="Contains"
                                DataTextField="des_descr" EmptyMessage="Designation"
                                DataValueField="des_id">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("des_descr") %>' Width="100%">
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("des_id") %>' />
                                    </div>
                                </ItemTemplate>

                            </telerik:RadComboBox>
                        </td>




                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Local/Overseas</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpLocalOverseas" runat="server" Filter="Contains"
                                DataTextField="deslocovs"
                                DataValueField="locovs"
                                EmptyMessage="Local/Overseas">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("deslocovs") %>'>
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("locovs") %>' />
                                    </div>
                                </ItemTemplate>

                            </telerik:RadComboBox>
                        </td>

                        <td align="left" width="20%"><span class="field-label">Visa Type</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpVisaType" runat="server" Filter="Contains"
                                DataTextField="EVM_DESCR" EmptyMessage="Visa Type"
                                DataValueField="EVM_ID">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("EVM_DESCR") %>'>
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("EVM_ID") %>' />
                                    </div>
                                </ItemTemplate>

                            </telerik:RadComboBox>
                        </td>
                    </tr>

                    <tr>

                        <td align="left" width="20%"><span class="field-label">Staff Groups</span></td>
                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpGroup" runat="server" DataTextField="ECT_DESCR" EmptyMessage="Staff Groups"
                                DataValueField="ECT_ID" Filter="Contains">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("ECT_DESCR") %>'>
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("ECT_ID") %>' />
                                    </div>
                                </ItemTemplate>

                            </telerik:RadComboBox>
                        </td>

                        <td align="left" width="20%"><span class="field-label">Employee Status</span></td>
                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpStaus" runat="server" DataTextField="est_descr" EmptyMessage="Employee Status"
                                DataValueField="est_id" Filter="Contains">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("est_descr") %>'>
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("est_id") %>' />
                                    </div>
                                </ItemTemplate>

                            </telerik:RadComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">As on Date</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />&nbsp;
                                 
                        <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" CssClass="MyCalendar"
                            Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                        </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" TargetControlID="txtFromDate" PopupButtonID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                ErrorMessage="From Date required" ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revFromdate" runat="server" ControlToValidate="txtFromDate"
                                EnableViewState="False"
                                ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="dayBook">*</asp:RegularExpressionValidator>
                        </td>
                        <td align="left" width="20%"><span class="field-label">Department</span></td>
                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblEmpDepartment" runat="server" Filter="Contains"
                                DataTextField="dpt_descr" EmptyMessage="Department"
                                DataValueField="dpt_id">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("dpt_descr") %>' Style="width: 100%">
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("dpt_id") %>' />
                                    </div>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%"><span class="field-label">Years to compare</span></td>

                        <td align="left" width="30%">
                            <telerik:RadComboBox ID="cblYears" runat="server" Filter="Contains"
                                DataTextField="Year_Text" EmptyMessage="Years to compare"
                                DataValueField="Year_value">
                                <ItemTemplate>
                                    <div>
                                        <asp:CheckBox runat="server" ID="chkComboItem" />
                                        <asp:Label runat="server" ID="lblComboItem" AssociatedControlID="chkComboItem" Text='<%# Eval("Year_Text") %>' Style="width: 100%">
                                        </asp:Label>
                                        <asp:HiddenField ID="hdnComboItem" Visible="true" runat="server" Value='<%# Eval("Year_value") %>' />
                                    </div>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </td>
                        <td align="left" width="20%"></td>
                        <td align="left" width="30%"></td>

                    </tr>

                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="dayBook" />
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />

                            <br />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                        </td>
                    </tr>
                </table>

                <center>
    <asp:Panel ID="panel_chart" runat="server"> 
        <asp:ImageButton ID="rbPrintLeft" runat="server"  ImageUrl="~/images/PrintAck.png" Height="35px"  ToolTip="Print the report"  OnClientClick="return PrintReport();"/>
        <asp:ImageButton ID="btnExport" runat="server" Visible="false"  ImageUrl="~/images/VerArrow.png" Height="35px" ToolTip="Download the report"  />
   <%-- <asp:Button ID="rbPrintLeft1" runat="server" Text="Print" 
            OnClientClick="return PrintReport();" />--%> 
    <div  id="divToPrint" runat="server"   style="width: 800px; height: 650px;">
     <%--<telerik:RadButton runat="server" ID="rbpdfExport" Text="PDF Export" AutoPostBack="true"
               CssClass="PdfButton">
               <Icon PrimaryIconCssClass="Images/PDF.jpg"></Icon>
          </telerik:RadButton>--%>
      
        <%--   <telerik:RadButton runat="server" ID="rbPrintLeft" Text="Print this chart" AutoPostBack="false"

               OnClientClicked="printChartOnly" CssClass="PrintButton">

               <Icon PrimaryIconCssClass="rbPrint"></Icon>

          </telerik:RadButton>--%>
           <br />
           Staff Average Age : 
        <asp:Label ID="lblaveragecuurent" runat="server" Text=""></asp:Label>  &nbsp;&nbsp;
        <asp:Label ID="lblaverageprev" runat="server" Text=""></asp:Label> &nbsp;&nbsp;
        <asp:Label ID="lblaveragetwoyear" runat="server" Text=""></asp:Label> 
        
        <telerik:RadChart ID="RadChart1" runat="server" Height="600px" Skin="Colorful" 
            Width="800px" DefaultType="Line"  IntelligentLabelsEnabled="True"> 
            <Series>
            <telerik:ChartSeries DataYColumn="Temp" Name="Series 1" Type="Line">
                    <Appearance>
                         <FillStyle MainColor="255, 186, 74" secondcolor="255, 244, 227">
                         </FillStyle>
                         <PointMark Border-Color="DarkKhaki" Border-Width="2" Visible="True">
                             <border color="DarkKhaki" width="2" />
                         </PointMark>
                         <Border Color="DimGray" />
                    </Appearance>          </telerik:ChartSeries>     
            </Series>
            <PlotArea>            
            
                <EmptySeriesMessage Visible="false">
                    <Appearance Visible="false">
                    </Appearance>
                </EmptySeriesMessage>
                <XAxis>
                    <Appearance>
                        <MajorGridLines Color="DimGray" width="0"/>    
                        <MinorGridLines Color="DimGray" PenStyle="Dash" />                  
                    </Appearance>
                    <AxisLabel>
                        <TextBlock Text="Age">
                            <Appearance textproperties-font="Verdana, 9.75pt, style=Bold">
                            </Appearance>
                        </TextBlock>
                    </AxisLabel>
                </XAxis>
                <YAxis>
                    <Appearance >
                        <MajorGridLines Color="DimGray" />       
                        <MinorGridLines Color="lightgray" PenStyle="Dash" /> 
                    </Appearance>                    
                    <AxisLabel>
                        <TextBlock Text="Count">
                            <Appearance textproperties-font="Verdana, 9.75pt, style=Bold">
                            </Appearance>
                        </TextBlock>
                    </AxisLabel>
                </YAxis>
                <yaxis2>
                    <axislabel>
                        <textblock>
                            <appearance textproperties-font="Verdana, 9.75pt, style=Bold">
                            </appearance>
                        </textblock>
                    </axislabel>
                </yaxis2>
                <appearance corners="Round, Round, Round, Round, 6">
                    <fillstyle filltype="Solid" maincolor="White">
                    </fillstyle>
                    <border color="DimGray" />
                </appearance>
            </PlotArea>
            <ChartTitle>
                <Appearance corners="Round, Round, Round, Round, 6" 
                    dimensions-margins="4%, 10px, 14px, 0%" position-alignedposition="Top">
                    <FillStyle MainColor="224, 224, 224" gammacorrection="False">
                    </FillStyle>
                    <border color="DimGray" />
                </Appearance>
                <TextBlock Text="Staff Age Analysis">
                    <Appearance 
                        TextProperties-Font="Verdana, 11.25pt">
                    </Appearance>
                </TextBlock>
            </ChartTitle>
            <Legend>
                <Appearance>
                    <ItemTextAppearance TextProperties-Color="DimGray">
                    </ItemTextAppearance>
                    <Border color="DimGray" />
                </Appearance>
            </Legend>
        </telerik:RadChart>
     </div></asp:Panel>
          <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" Skin="Telerik"
          Width="200px" Animation="Slide" Position="TopCenter" EnableShadow="true" ToolTipZoneID="RadChart1"
          AutoTooltipify="true">
     </telerik:RadToolTipManager>
    </center>

                <%--<div id="chartPlaceholder">
       <telerik:RadChart ID="RadChart1" runat="server" DefaultType="StackedSplineArea" 
        Height="500px" Width="500px" >
         
        <Series >
            <telerik:ChartSeries Name="Series 1" DataXColumn="PrevAge" 
                DataYColumn="PrevYearCount" Type="StackedSplineArea" >
                <Appearance>
                    <FillStyle MainColor="213, 247, 255">
                    </FillStyle>
                </Appearance>
            </telerik:ChartSeries>
            <telerik:ChartSeries  Name="current year" Type="StackedSplineArea"    
                DataXColumn="CurrentAge" DataYColumn="CurrentYearCount" >
                <Appearance>
                    <FillStyle MainColor="218, 254, 122">
                    </FillStyle>
                </Appearance>
            </telerik:ChartSeries>
        </Series>
        <PlotArea>
        <XAxis MinValue="20"></XAxis> 
         <YAxis MinValue="20"></YAxis>
        </PlotArea>  
    </telerik:RadChart>
               </div>--%>

                <script type="text/javascript">
                    function PrintReport() {
                        var divToPrint = document.getElementById('<%=divToPrint.ClientID %>');
                        var popupWin = window.open('', '_blank', 'width=300,height=300');
                        popupWin.document.open();
                        popupWin.document.write('' + divToPrint.innerHTML + '');
                        popupWin.document.close();
                        popupWin.print();
                        return false;
                    }
                </script>

            </div>
        </div>
    </div>
</asp:Content>


<%--  <telerik:ChartSeries Name="Series 1" Type="Line" >
                         
                       </telerik:ChartSeries>
                       <telerik:ChartSeries Name="Series 2" Type="Line">
                                                           
                       </telerik:ChartSeries>--%>