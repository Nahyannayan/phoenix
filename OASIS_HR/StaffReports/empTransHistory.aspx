﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="empTransHistory.aspx.vb" Inherits="StaffReports_empTransHistory" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetEMPName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url = '../../Common/PopUpEmployeesList.aspx?id=EMPLOYEES_TOTRANSFER';
            result =radopen(url, "pop_up")

          <%--  if (result != '' && result != undefined) {
                NameandCode = result.split('___'); document.getElementById('<%=h_Emp_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpName.ClientID %>').value = NameandCode[0];
                //
                return true;
            }
            return false;--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



 function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();            
            if (arg) {
                NameandCode = arg.NameandCode .split('||');
                document.getElementById('<%=h_Emp_ID.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=txtEmpName.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtEmpName.ClientID%>', 'TextChanged');
            }
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Employee Visa Issuing Unit
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" width="100%"
                    cellpadding="5"
                    cellspacing="0">
                    <tr>
                        <td align="left">
                            <span class="error">All fields are mandatory </span>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <asp:RegularExpressionValidator
                                ID="revToDate" runat="server" ControlToValidate="txtDate" Display="Dynamic" EnableViewState="False"
                                ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                ValidationGroup="VALIDATE">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <%--  <tr style="font-size: 8pt; color: #000000" valign="bottom">
            <td align="center"  valign="middle" colspan="6"></td>
        </tr>--%>                   
                    <tr>
                        <td>
                            <table id="Table1" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">

                                <%-- <tr class="subheader_img" >
                        <td align="left" colspan="6" style="height: 16px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
             Employee Visa Issuing Unit</span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Employee Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtEmpName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgEmpSel" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetEMPName(); return false;" />
                                        <asp:RequiredFieldValidator ID="rqEmpName" runat="server"
                                            ControlToValidate="txtEmpName" ErrorMessage="Employee name required"
                                            ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator>

                                    </td>
                                     <td align="left" width="20%"><span class="field-label">Transaction Type</span></td>

                                    <td align="left" width="30%">
                                        <div class="checkbox-list">
                                        <asp:CheckBoxList ID="chkTransTypes" runat="server" BorderColor="MenuHighlight"
                                            BorderStyle="Solid" OnSelectedIndexChanged="chkTransTypes_SelectedIndexChanged"
                                            AutoPostBack="True" RepeatColumns="2">
                                        </asp:CheckBoxList></div>
                                    </td>
                                </tr>
                             <%--   <tr>

                                   
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"
                                            CssClass="listbox"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate" ErrorMessage="Date required"
                                            ValidationGroup="VALIDATE">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Group on</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGroupOption" runat="server">
                                            <asp:ListItem>Description</asp:ListItem>
                                            <asp:ListItem>Date</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="VALIDATE" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_Emp_ID" runat="server" />
                <asp:HiddenField ID="h_TTP_ID" runat="server" />
                <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy"
                    PopupButtonID="txtDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>
