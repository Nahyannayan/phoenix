<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StaffReportFilter.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="StaffReports_StaffReportFilter" %>

<%@ Register Src="UserControls/ReportFilter.ascx" TagName="ReportFilter" TagPrefix="uc1" %>


<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Label ID="lblTopTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>
                    <uc1:ReportFilter ID="ReportFilter1" runat="server" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>


