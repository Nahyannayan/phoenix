Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system

Partial Class OASIS_HR_StaffReports_StaffRptStatitics
    Inherits System.Web.UI.Page

    '-------------------------------------------------------------------------------------------------
    ' Page Name     : OASIS_HR_StaffReports_StaffRptStatitics.aspx
    ' Purpose       : Criteria screen for Staff Statitics Report
    ' Created By    : Sajesh Elias
    ' Created Date  : 25-11-08
    ' Description   : 
    '-------------------------------------------------------------------------------------------------
    ' Module level variable declarations
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String

    Protected Sub cmbSurvey_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Private Function RetriveBusnsUnits()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query = ""
        str_query = "SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M "
        Dim dsSurveyBUSUnits As DataSet
        dsSurveyBUSUnits = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cmbBusUnits.DataSource = dsSurveyBUSUnits.Tables(0)
        cmbBusUnits.DataTextField = "BSU_NAME"
        cmbBusUnits.DataValueField = "BSU_ID"
        cmbBusUnits.DataBind()
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'CheckMenuRights()
                RetriveBusnsUnits()
                cmbBusUnits.SelectedValue = Session("sBsuid")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString

        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        If Not IsDate(txtFromDate.Text) Or Not IsDate(txtToDate.Text) Then
            lblError.Text = "Invalid Date!!!"
            Exit Sub
        End If
        ' GenerateStaffStatitics()
        Dim fromdate As DateTime = Convert.ToDateTime(txtFromDate.Text.Trim())
        Dim todate As DateTime = Convert.ToDateTime(txtToDate.Text.Trim())
        Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
        Dim TDate As String = todate.ToString("yyyy-MM-dd")
        Dim param As New Hashtable
        ''Default Parameters 
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@From_Date", FDate)
        param.Add("@To_Date", TDate)
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RPTHEADER", "Report For the period:  " + FDate.ToString + "    To   " + TDate.ToString + "                                                                                         Academic Year :" + Session("F_YEAR").ToString)
        param.Add("USERNAME", Session("sUsr_name"))
        Dim rptClass As New rptClass '
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffStatitics.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
  
    Private Sub GenerateStaffStatitics()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim cmd As New SqlCommand("[dbo].[Employee_Statitics]")
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = cmbBusUnits.SelectedValue
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlpFROMDT As New SqlParameter("@From_Date", SqlDbType.DateTime)
        sqlpFROMDT.Value = txtFromDate.Text
        cmd.Parameters.Add(sqlpFROMDT)

        Dim sqlpTODT As New SqlParameter("@To_Date", SqlDbType.DateTime)
        sqlpTODT.Value = txtToDate.Text
        cmd.Parameters.Add(sqlpTODT)

        cmd.Connection = New SqlConnection(str_conn)
        Dim repSource As New MyReportClass
        Dim params As New Hashtable
        params("userName") = Session("sUsr_name")
        params("@From_Date") = txtFromDate.Text
        params("@To_Date") = txtToDate.Text

        'params("BSU_NAME") = cmbBusUnits.SelectedItem.Text
        params("RPT_CAPTION") = "For the period : " + txtFromDate.Text + " - " + txtToDate.Text + "          Accademaic Year : " + Session("F_Descr").ToString
        repSource.Parameter = params
        repSource.Command = cmd
        repSource.IncludeBSUImage = True
        repSource.ResourceName = "../../OASIS_HR/StaffReports/Reports/CR_StaffStatitics.rpt"
        Session("ReportSource") = repSource
        'Response.Redirect("../../Reports/ASPX Report/Rptviewer.aspx", True)
        ReportLoadSelection_2()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Sub ReportLoadSelection_2()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/RptviewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/RptviewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
