﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffExportFilter.aspx.vb" Inherits="OASIS_HR_StaffReports_StaffExportFilter" Title="Untitled Page" %>

<%@ Register Src="UserControls/ExportFilter.ascx" TagName="ExportFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTopTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div>

                    <uc1:ExportFilter ID="ExportFilter1" runat="server" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>

