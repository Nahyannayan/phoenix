﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
Imports System.Collections.Generic

Partial Class OASIS_HR_StaffReports_UserControls_ExportFilter
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            HiddenMenuCode.Value = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        End If
        BindFilterControls()

        Dim ScriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        ScriptManager1.RegisterPostBackControl(btnView)
    End Sub

   

    Public Sub BindFilterControls()
        ''Try
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select * from STF_REPORT_FILTER_TABLE where STF_FT_SHOW_FLAG='1' ORDER BY STF_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataStaffFilter.DataSource = ds
        DataStaffFilter.DataBind()

        For Each row As DataListItem In DataStaffFilter.Items
            Dim STF_FT_ID = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            Dim dc As DataSet
            Dim ListDynamic As New CheckBoxList
            ListDynamic.ID = "|" & STF_FT_ID & "|"
            'ListDynamic.SelectionMode = ListSelectionMode.Multiple
            'ListDynamic.Width = "200"
            'ListDynamic.Height = "200"
            'ListDynamic.CssClass = "matters"
            If ListDynamic.ID = "|5|" Then
                Dim hf As HiddenField = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField)

                Dim bsuper As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select usr_bsuper from users_m where USR_NAME='" & Session("sUsr_name") & "'")
                If bsuper = False Then
                    hf.Value = hf.Value + " where   bsu_id in(select USM_BSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                End If

            End If
            If ListDynamic.ID = "|11|" Then
                Dim hf As HiddenField = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField)
                Dim bsuper As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select usr_bsuper from users_m where USR_NAME='" & Session("sUsr_name") & "'")
                If bsuper = False Then
                    hf.Value = hf.Value + " where   bsu_id in(select USM_WBSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                End If

            End If

            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField).Value)
            ListDynamic.DataSource = dc
            ListDynamic.DataValueField = DirectCast(row.FindControl("HiddenSTF_FT_VALUE_FIELD"), HiddenField).Value.Trim()
            ListDynamic.DataTextField = DirectCast(row.FindControl("HiddenSTF_FT_TEXT_FIELD"), HiddenField).Value.Trim()
            ListDynamic.DataBind()
            'Dim list As New ListItem
            'list.Text = "All" '"<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            For Each clist As ListItem In ListDynamic.Items
                clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
            Next
            Dim list As New ListItem
            list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            list.Value = "-1"
            list.Attributes.Add("onclick", "javascript:change_chk_state(this);")

            'ListDynamic.Items.Insert(0, list)

            If ListDynamic.DataValueField = "est_id" Then
                'ListDynamic.SelectedValue = "1"
                ListDynamic.Items.FindByValue("1").Selected = True
                ListDynamic.Items.FindByValue("2").Selected = True

            Else
                list.Selected = True
                'Response.Write("<script language ='javascript' type ='text/javascript'> change_chk_state('" + list + "') </Script>")
            End If
            ListDynamic.Items.Insert(0, list)


            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(ListDynamic)

            If ListDynamic.DataValueField = "ESD_ID" And HiddenMenuCode.Value <> "H000192" And HiddenMenuCode.Value <> "H000187" Then
                PanelHolder.Controls.Remove(ListDynamic)
            End If
        Next

        'For Each row As DataListItem In DataStaffFilter.Items
        '    Dim ListDynamic As New CheckBoxList
        '    Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
        '    If STF_FT_ID <> 10 Then
        '        ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

        '        ListDynamic.SelectedValue = -1
        '    End If

        'Next

        ''Catch ex As Exception

        ''End Try
    End Sub

    Private Function ReportCriteriaFileds() As NameValueCollection
        Try
            Dim NvcCriteria As New NameValueCollection()
            NvcCriteria.Add("0", "")
            NvcCriteria.Add("1", "Designation  :")
            NvcCriteria.Add("2", "Department   :")
            NvcCriteria.Add("3", "Staff Groups : ")
            NvcCriteria.Add("4", "Staff status :")
            NvcCriteria.Add("5", "Issued Unit  :")
            NvcCriteria.Add("6", "Residence    :")
            NvcCriteria.Add("7", "Visa Type    :")
            NvcCriteria.Add("8", "Staff Type   :")
            NvcCriteria.Add("9", "ABC Group    :")
            NvcCriteria.Add("10", "Document type:")
            Return NvcCriteria
        Catch ex As Exception

        End Try
    End Function



    Shared directory As New Dictionary(Of String, String)

    Public Function Filter() As Dictionary(Of String, String)

        Dim NvcCriteriaList As NameValueCollection = ReportCriteriaFileds()
        Dim strCriteria As String
        Dim strFilterString As String

        For Each row As DataListItem In DataStaffFilter.Items
            Dim ListDynamic As New CheckBoxList
            Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

            If ListDynamic IsNot Nothing Then



                Dim FilterString As String = ""
                Dim allflag = 0


                For Each item As ListItem In ListDynamic.Items
                    If item.Value = "-1" And item.Selected = True Then
                        '' All
                        allflag = 1

                        For Each ALLItem As ListItem In ListDynamic.Items
                            If ALLItem.Value <> "-1" Then
                                If FilterString = "" Then
                                    FilterString = "" & ALLItem.Value & ""
                                    'strFilterString = "" & ALLItem.Text & ""
                                Else
                                    FilterString &= "," & ALLItem.Value & ""
                                    ' strFilterString &= "," & ALLItem.Text & ""
                                End If
                            End If

                        Next

                    ElseIf item.Selected = True And allflag = 0 Then
                        If FilterString = "" Then
                            FilterString = "" & item.Value & ""
                            strFilterString = "" & item.Text & ""
                        Else
                            FilterString &= "," & item.Value & ""
                            strFilterString &= "," & item.Text & ""
                        End If
                    End If
                Next

                If strFilterString <> "" Then
                    strCriteria &= NvcCriteriaList.Get(STF_FT_ID) & " " & strFilterString & " :: "
                    strFilterString = ""
                End If

                Dim STF_FT_PARAMETER_FIELD As String = DirectCast(row.FindControl("HiddenSTF_FT_PARAMETER_FIELD"), HiddenField).Value
                directory.Add(STF_FT_PARAMETER_FIELD, GenerateXML(FilterString))

            End If
        Next
        If strCriteria <> "" Then
            strCriteria = strCriteria.Substring(0, strCriteria.Length - 3)
        End If
        If strCriteria = "" Then
            ViewState("Criteria") = "Criteria :: All"
        Else
            ViewState("Criteria") = strCriteria
        End If


        Return directory

    End Function


    Private Function GenerateXML(ByVal ValueId As String) As String
        Dim xmlDoc As New XmlDocument
        Dim Details As XmlElement
        Dim XMLValueId As XmlAttribute
        Dim XMLDetail As XmlElement
        Try
            Details = xmlDoc.CreateElement("ROOT")
            xmlDoc.AppendChild(Details)
            Dim IDs As String() = ValueId.Split(",")
            For i As Integer = 0 To IDs.Length - 1
                XMLDetail = xmlDoc.CreateElement("Detail")
                XMLValueId = xmlDoc.CreateAttribute("ID")
                XMLValueId.InnerText = IDs(i)
                XMLDetail.Attributes.Append(XMLValueId)
                xmlDoc.DocumentElement.InsertBefore(XMLDetail, xmlDoc.DocumentElement.LastChild)

            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Sub StaffFilterParameters(ByVal param As Hashtable)

        param.Add("@EMP_DES_ID", directory.Item("EMP_DES_ID"))
        param.Add("@EMP_DPT_ID", directory.Item("EMP_DPT_ID"))
        param.Add("@EMP_ECT_ID", directory.Item("EMP_ECT_ID"))

        param.Add("@EMP_STATUS", directory.Item("EMP_STATUS"))
        param.Add("@EMP_VISA_BSU_ID", directory.Item("EMP_VISA_BSU_ID"))


        param.Add("@EMP_bOVERSEAS", directory.Item("EMP_bOVERSEAS"))

        param.Add("@EMP_VISATYPE", directory.Item("EMP_VISATYPE"))
        param.Add("@EMP_bTemp", directory.Item("EMP_bTemp"))
        param.Add("@EMP_ABC", directory.Item("EMP_ABC"))
        param.Add("@BSU_ID_EMP", directory.Item("BSU_ID"))
        

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        directory.Clear()

   


        ''Report(Parameters)
        Filter()

       

        ' StaffFilterParameters(param)
        ' param.Add("@FROMDATE", FDate)
        'param.Add("@TODATE", TDate)
       
        Dim connStr As String
        Dim DS As New DataSet
        Dim pParms(14) As SqlClient.SqlParameter

     
        connStr = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString



        '        @BSU_ID VARCHAR(50),
        '@FROMDATE VARCHAR(50),
        '@TODATE VARCHAR(50),
        '@EMP_DES_ID XML,
        '@EMP_DPT_ID XML,
        '@EMP_ECT_ID XML,
        '@EMP_STATUS XML,
        '@EMP_VISA_BSU_ID XML,
        '@EMP_bOVERSEAS XML,
        '@EMP_VISATYPE XML,
        '@EMP_bTemp XML,
        '@EMP_ABC  XML,


        '@BSU_ID_EMP XML

        Dim fromdate As DateTime = Convert.ToDateTime(txtfrom.Text.Trim())
        Dim todate As DateTime = Convert.ToDateTime(txtto.Text.Trim())
        Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
        Dim TDate As String = todate.ToString("yyyy-MM-dd")

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@FROMDATE", FDate)
        pParms(2) = New SqlClient.SqlParameter("@TODATE", TDate)
        pParms(3) = New SqlClient.SqlParameter("@EMP_DES_ID", directory.Item("EMP_DES_ID"))
        pParms(4) = New SqlClient.SqlParameter("@EMP_DPT_ID", directory.Item("EMP_DPT_ID"))
        pParms(5) = New SqlClient.SqlParameter("@EMP_ECT_ID", directory.Item("EMP_ECT_ID"))
        pParms(6) = New SqlClient.SqlParameter("@EMP_STATUS", directory.Item("EMP_STATUS"))
        pParms(7) = New SqlClient.SqlParameter("@EMP_VISA_BSU_ID", directory.Item("EMP_VISA_BSU_ID"))
        pParms(8) = New SqlClient.SqlParameter("@EMP_bOVERSEAS", directory.Item("EMP_bOVERSEAS"))
        pParms(9) = New SqlClient.SqlParameter("@EMP_VISATYPE", directory.Item("EMP_VISATYPE"))
        pParms(10) = New SqlClient.SqlParameter("@EMP_bTemp", directory.Item("EMP_bTemp"))
        pParms(11) = New SqlClient.SqlParameter("@EMP_ABC", directory.Item("EMP_ABC"))
        pParms(12) = New SqlClient.SqlParameter("@BSU_ID_EMP", directory.Item("BSU_ID"))



        DS = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "KHDA_STAFF_EXPORT", pParms)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        Dim ef As New GemBox.Spreadsheet.ExcelFile()
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(DS.Tables(0), "A1", True)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "Faculty_Data_Template.xls")
        ef.SaveXls(Response.OutputStream)





    End Sub
   
End Class

