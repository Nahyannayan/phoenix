﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ExportFilter.ascx.vb" Inherits="OASIS_HR_StaffReports_UserControls_ExportFilter" %>

<script language ="javascript" type ="text/javascript" >
    function change_chk_state(chkThis) {
        var ids = chkThis.id

        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                document.forms[0].elements[i].checked = state;

            }
        }
        //document.forms[0].submit()
        return false;
    }


    function uncheckall(chkThis) {
        var ids = chkThis.id
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
        var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }
        var uncheckflag = 0

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                if (currentid.indexOf(value) == -1) {
                    if (document.forms[0].elements[i].checked == false) {
                        uncheckflag = 1
                    }
                }
            }
        }

        if (uncheckflag == 1) {
            // uncheck parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }
        else {
            // Check parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = true;

                }
            }
            // document.forms[0].submit();
        }


        return false;
    }
</script>

<div class="matters">
<asp:HiddenField ID="HiddenMenuCode" runat="server" />
<br />
<asp:Panel ID="Panel1" runat="server">
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" class="matters"
        width="700">
        <tr>
            <td class="subheader_img">
                <asp:Label ID="LabelMessage" runat="server" Text="KHDA Export -Select Date Range"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            DOJ From Date</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtfrom" runat="server"></asp:TextBox></td>
                        <td style="width: 3px">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        <td>
                            To Date</td>
                        <td>
                            :</td>
                        <td>
                            <asp:TextBox ID="txtto" runat="server"></asp:TextBox></td>
                        <td style="width: 3px">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                    </tr>
                    
                    
                    
                </table>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
        TargetControlID="txtfrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
        TargetControlID="txtto">
    </ajaxToolkit:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfrom"
        Display="None" ErrorMessage="Please Enter From Date" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:RequiredFieldValidator
            ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtto" Display="None"
            ErrorMessage="Please Enter To Date" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:ValidationSummary
                ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
</asp:Panel>

  
            
 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                &nbsp;Filter Table</td>
                        </tr>
                        <tr>
                            <td >
<asp:DataList ID="DataStaffFilter" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
            ShowHeader="False">
            <ItemTemplate>
                <asp:HiddenField ID="HiddenSTF_FT_ID" runat="server" Value='<%# Eval("STF_FT_ID") %>' />
                <asp:HiddenField ID="HiddenSTF_FT_PARAMETER_FIELD" runat="server" Value='<%# Eval("STF_FT_PARAMETER_FIELD") %>' />
                <asp:HiddenField ID="HiddenSTF_FT_CONDITION" runat="server" Value='<%# Eval("STF_FT_CONDITION") %>' />
                <asp:HiddenField ID="HiddenSTF_FT_VALUE_FIELD" runat="server" Value='<%# Eval("STF_FT_VALUE_FIELD") %>' />
                <asp:HiddenField ID="HiddenSTF_FT_TEXT_FIELD" runat="server" Value='<%# Eval("STF_FT_TEXT_FIELD") %>' />
                <asp:HiddenField ID="HiddenSTF_FT_FIELDS" runat="server" Value='<%# Eval("STF_FT_FIELDS") %>' />
                <asp:HiddenField ID="HiddenSTF_FT_CASE" runat="server" Value='<%# Eval("STF_FT_CASE") %>' />
                <asp:HiddenField ID="HiddenSTS_TABLES" runat="server" Value='<%# Eval("STS_TABLES") %>' />
                <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0">
                    <tr class="subheader_img">
                        <td align="left" colspan="12" style="height: 16px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                <%#Eval("STF_FT_DES")%>
                            </span></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="PanelControl" runat="server" BorderColor="#1b80b6" BorderStyle="Solid"
                                Height="200px" ScrollBars="Auto" Width="200px" HorizontalAlign="Left">
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList>
        <asp:Button ID="btnView" runat="server" Text="View" CssClass="button" />
             
 </td>
                    </tr>
               </table>
               
</div>