<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReportFilter.ascx.vb" Inherits="StaffReports_UserControls_ReportFilter" %>
<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<!-- Bootstrap header files ends here -->


<script language="javascript" type="text/javascript">
    function change_chk_state(chkThis) {
        var ids = chkThis.id

        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                document.forms[0].elements[i].checked = state;

            }
        }
        //document.forms[0].submit()
        return false;
    }


    function uncheckall(chkThis) {
        var ids = chkThis.id
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
        var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }
        var uncheckflag = 0

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                if (currentid.indexOf(value) == -1) {
                    if (document.forms[0].elements[i].checked == false) {
                        uncheckflag = 1
                    }
                }
            }
        }

        if (uncheckflag == 1) {
            // uncheck parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }
        else {
            // Check parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = true;

                }
            }
            // document.forms[0].submit();
        }


        return false;
    }
</script>


<div>
    <asp:HiddenField ID="HiddenMenuCode" runat="server" />
    <br />
    <asp:Panel ID="Panel1" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" class="title-bg-lite">
                    <asp:Label ID="LabelMessage" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td align="left">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" width="20%">
                                <span class="field-label">From Date</span></td>

                            <td align="left" width="30%">
                                <asp:TextBox ID="txtfrom" runat="server" ValidationGroup ="PageValidation"></asp:TextBox>

                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                            <td align="left" width="20%">
                                <span class="field-label">To Date</span></td>

                            <td align="left" width="30%">
                                <asp:TextBox ID="txtto" runat="server" ValidationGroup="PageValidation"></asp:TextBox>

                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                        </tr>


                        <tr id="AgePanel" runat="Server">
                            <td align="left" width="20%"><span class="field-label">Show</span></td>

                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlAgePanel" runat="server">
                                    <asp:ListItem Value="Reaching">Age-Reaching 60</asp:ListItem>
                                    <asp:ListItem Value="Completed">Age-Completed 60</asp:ListItem>

                                </asp:DropDownList></td>
                            <td align="left" width="20%"></td>
                            <td align="left" width="30%"></td>
                        </tr>


                    </table>
                </td>
            </tr>
        </table>
        <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
            TargetControlID="txtfrom">
        </ajaxToolkit:CalendarExtender>
        <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image2"
            TargetControlID="txtto">
        </ajaxToolkit:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfrom"
            Display="None" ErrorMessage="Please Enter From Date" SetFocusOnError="True" ValidationGroup ="PageValidation">
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtto" Display="None"
                ErrorMessage="Please Enter To Date" SetFocusOnError="True" ValidationGroup ="PageValidation">
         </asp:RequiredFieldValidator>
        <asp:ValidationSummary
                    ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
    </asp:Panel>



    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" class="title-bg-lite">Filter Table</td>
        </tr>
        <tr>
            <td align="left" width="100%">
                <asp:DataList ID="DataStaffFilter" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                    ShowHeader="False" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="HiddenSTF_FT_ID" runat="server" Value='<%# Eval("STF_FT_ID") %>' />
                        <asp:HiddenField ID="HiddenSTF_FT_PARAMETER_FIELD" runat="server" Value='<%# Eval("STF_FT_PARAMETER_FIELD") %>' />
                        <asp:HiddenField ID="HiddenSTF_FT_CONDITION" runat="server" Value='<%# Eval("STF_FT_CONDITION") %>' />
                        <asp:HiddenField ID="HiddenSTF_FT_VALUE_FIELD" runat="server" Value='<%# Eval("STF_FT_VALUE_FIELD") %>' />
                        <asp:HiddenField ID="HiddenSTF_FT_TEXT_FIELD" runat="server" Value='<%# Eval("STF_FT_TEXT_FIELD") %>' />
                        <asp:HiddenField ID="HiddenSTF_FT_FIELDS" runat="server" Value='<%# Eval("STF_FT_FIELDS") %>' />
                        <asp:HiddenField ID="HiddenSTF_FT_CASE" runat="server" Value='<%# Eval("STF_FT_CASE") %>' />
                        <asp:HiddenField ID="HiddenSTS_TABLES" runat="server" Value='<%# Eval("STS_TABLES") %>' />
                        <table cellspacing="0" cellpadding="0" width="100%">

                            <tr>
                                <td class="title-bg">

                                    <%#Eval("STF_FT_DES")%>
                           
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="checkbox-list" style="width: 100%;">
                                        <asp:Panel ID="PanelControl" runat="server">
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                </asp:DataList>


            </td>
        </tr>

        <tr>
            <td align="center" width="100%">
                <asp:Button ID="btnView" runat="server" Text="View" CssClass="button" ValidationGroup ="PageValidation"/>
            </td>
        </tr>
    </table>

</div>
