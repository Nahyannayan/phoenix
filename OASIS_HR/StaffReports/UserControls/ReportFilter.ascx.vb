Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
Imports System.Collections.Generic
Partial Class StaffReports_UserControls_ReportFilter
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim Encr_decrData As New Encryption64
            HiddenMenuCode.Value = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            PanelHide()
        End If
        BindFilterControls()

    End Sub

    Public Sub PanelHide()
        If HiddenMenuCode.Value = "H000192" Or HiddenMenuCode.Value = "H000102" Or HiddenMenuCode.Value = "H000110" Or HiddenMenuCode.Value = "H000188" Or HiddenMenuCode.Value = "H000187" Then ''Document Expiry Report , Health Card Expiry
            Panel1.Visible = True
        Else
            Panel1.Visible = False
        End If
        AgePanel.Visible = False
        If HiddenMenuCode.Value = "H000192" Then
            LabelMessage.Text = "Document Expiry (Working Unit)"
        ElseIf HiddenMenuCode.Value = "H000193" Then
            LabelMessage.Text = "Empty Date List"
        ElseIf HiddenMenuCode.Value = "H000194" Then
            LabelMessage.Text = "Passport Borrow"
        ElseIf HiddenMenuCode.Value = "H000195" Then
            LabelMessage.Text = "Passport Pending Return"
        ElseIf HiddenMenuCode.Value = "H000196" Then
            LabelMessage.Text = "MOL Inspection List"
        ElseIf HiddenMenuCode.Value = "H000197" Then
            LabelMessage.Text = "Nationality Statistics"
        ElseIf HiddenMenuCode.Value = "H000198" Then
            LabelMessage.Text = "Qualification Category"
        ElseIf HiddenMenuCode.Value = "H000199" Then
            LabelMessage.Text = "Qualification Statistics"
        ElseIf HiddenMenuCode.Value = "H000102" Then
            LabelMessage.Text = "Document Expiry (Working Unit)"
        ElseIf HiddenMenuCode.Value = "H000110" Then
            LabelMessage.Text = "Health Card Expiry Report"
        ElseIf HiddenMenuCode.Value = "H000187" Then
            LabelMessage.Text = "Expiry Report -Days Left"
        ElseIf HiddenMenuCode.Value = "H000188" Then
            LabelMessage.Text = "Super Annual Report"
            AgePanel.Visible = True
        ElseIf HiddenMenuCode.Value = "H000202" Then
            LabelMessage.Text = "Staff Usernames Report"
        ElseIf HiddenMenuCode.Value = "H000299" Then
            LabelMessage.Text = "Staff List New"
        ElseIf HiddenMenuCode.Value = "H000191" Then
            LabelMessage.Text = "Staff List"
        End If

    End Sub

    Public Sub BindFilterControls()
        ''Try
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select * from STF_REPORT_FILTER_TABLE where STF_FT_SHOW_FLAG='1' ORDER BY STF_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataStaffFilter.DataSource = ds
        DataStaffFilter.DataBind()

        For Each row As DataListItem In DataStaffFilter.Items
            Dim STF_FT_ID = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            Dim dc As DataSet
            Dim ListDynamic As New CheckBoxList
            ListDynamic.ID = "|" & STF_FT_ID & "|"
            'ListDynamic.SelectionMode = ListSelectionMode.Multiple
            'ListDynamic.Width = "200"
            'ListDynamic.Height = "200"
            'ListDynamic.CssClass = "matters"
            If ListDynamic.ID = "|5|" Then
                Dim hf As HiddenField = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField)

                Dim bsuper As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select usr_bsuper from users_m where USR_NAME='" & Session("sUsr_name") & "'")
                If bsuper = False Then
                    If HiddenMenuCode.Value <> "H000102" And HiddenMenuCode.Value <> "H000103" Then
                        hf.Value = hf.Value + " where   bsu_id in(select USM_BSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                    Else
                        hf.Value = hf.Value + " where   bsu_id in(select USM_WBSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                    End If
                End If

            End If
            If ListDynamic.ID = "|11|" Then
                Dim hf As HiddenField = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField)
                Dim bsuper As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select usr_bsuper from users_m where USR_NAME='" & Session("sUsr_name") & "'")
                If bsuper = False Then
                    If HiddenMenuCode.Value <> "H000102" And HiddenMenuCode.Value <> "H000103" Then
                        hf.Value = hf.Value + " where   bsu_id in(select USM_WBSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                    Else
                        hf.Value = hf.Value + " where   bsu_id in(select USM_BSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                    End If

                End If

            End If

            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField).Value)
            ListDynamic.DataSource = dc
            ListDynamic.DataValueField = DirectCast(row.FindControl("HiddenSTF_FT_VALUE_FIELD"), HiddenField).Value.Trim()
            ListDynamic.DataTextField = DirectCast(row.FindControl("HiddenSTF_FT_TEXT_FIELD"), HiddenField).Value.Trim()
            ListDynamic.DataBind()
            'Dim list As New ListItem
            'list.Text = "All" '"<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            For Each clist As ListItem In ListDynamic.Items
                clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
            Next
            Dim list As New ListItem
            'list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            list.Text = "All"
            list.Value = "-1"
            list.Attributes.Add("onclick", "javascript:change_chk_state(this);")

            'ListDynamic.Items.Insert(0, list)

            If ListDynamic.DataValueField = "est_id" Then
                'ListDynamic.SelectedValue = "1"
                ListDynamic.Items.FindByValue("1").Selected = True
                ListDynamic.Items.FindByValue("2").Selected = True

            Else
                list.Selected = True
                'Response.Write("<script language ='javascript' type ='text/javascript'> change_chk_state('" + list + "') </Script>")
            End If
            ListDynamic.Items.Insert(0, list)


            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(ListDynamic)

            If ListDynamic.DataValueField = "ESD_ID" And HiddenMenuCode.Value <> "H000192" And HiddenMenuCode.Value <> "H000102" And HiddenMenuCode.Value <> "H000187" Then
                PanelHolder.Controls.Remove(ListDynamic)
            End If
        Next

        'For Each row As DataListItem In DataStaffFilter.Items
        '    Dim ListDynamic As New CheckBoxList
        '    Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
        '    If STF_FT_ID <> 10 Then
        '        ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

        '        ListDynamic.SelectedValue = -1
        '    End If

        'Next

        ''Catch ex As Exception

        ''End Try
    End Sub

    Private Function ReportCriteriaFileds() As NameValueCollection
        Try
            Dim NvcCriteria As New NameValueCollection()
            NvcCriteria.Add("0", "")
            NvcCriteria.Add("1", "Designation  :")
            NvcCriteria.Add("2", "Department   :")
            NvcCriteria.Add("3", "Staff Groups : ")
            NvcCriteria.Add("4", "Staff status :")
            NvcCriteria.Add("5", "Issued Unit  :")
            NvcCriteria.Add("6", "Residence    :")
            NvcCriteria.Add("7", "Visa Type    :")
            NvcCriteria.Add("8", "Staff Type   :")
            NvcCriteria.Add("9", "ABC Group    :")
            NvcCriteria.Add("10", "Document type:")
            Return NvcCriteria
        Catch ex As Exception

        End Try
    End Function



    Shared directory As New Dictionary(Of String, String)

    Public Function Filter() As Dictionary(Of String, String)

        Dim NvcCriteriaList As NameValueCollection = ReportCriteriaFileds()
        Dim strCriteria As String
        Dim strFilterString As String

        For Each row As DataListItem In DataStaffFilter.Items
            Dim ListDynamic As New CheckBoxList
            Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

            If ListDynamic IsNot Nothing Then



                Dim FilterString As String = ""
                Dim allflag = 0


                For Each item As ListItem In ListDynamic.Items
                    If item.Value = "-1" And item.Selected = True Then
                        '' All
                        allflag = 1

                        For Each ALLItem As ListItem In ListDynamic.Items
                            If ALLItem.Value <> "-1" Then
                                If FilterString = "" Then
                                    FilterString = "" & ALLItem.Value & ""
                                    'strFilterString = "" & ALLItem.Text & ""
                                Else
                                    FilterString &= "," & ALLItem.Value & ""
                                    ' strFilterString &= "," & ALLItem.Text & ""
                                End If
                            End If

                        Next

                    ElseIf item.Selected = True And allflag = 0 Then
                        If FilterString = "" Then
                            FilterString = "" & item.Value & ""
                            strFilterString = "" & item.Text & ""
                        Else
                            FilterString &= "," & item.Value & ""
                            strFilterString &= "," & item.Text & ""
                        End If
                    End If
                Next

                If strFilterString <> "" Then
                    strCriteria &= NvcCriteriaList.Get(STF_FT_ID) & " " & strFilterString & " :: "
                    strFilterString = ""
                End If

                Dim STF_FT_PARAMETER_FIELD As String = DirectCast(row.FindControl("HiddenSTF_FT_PARAMETER_FIELD"), HiddenField).Value
                directory.Add(STF_FT_PARAMETER_FIELD, GenerateXML(FilterString))

            End If
        Next
        If strCriteria <> "" Then
            strCriteria = strCriteria.Substring(0, strCriteria.Length - 3)
        End If
        If strCriteria = "" Then
            ViewState("Criteria") = "Criteria :: All"
        Else
            ViewState("Criteria") = strCriteria
        End If


        Return directory

    End Function


    Private Function GenerateXML(ByVal ValueId As String) As String
        Dim xmlDoc As New XmlDocument
        Dim Details As XmlElement
        Dim XMLValueId As XmlAttribute
        Dim XMLDetail As XmlElement
        Try
            Details = xmlDoc.CreateElement("ROOT")
            xmlDoc.AppendChild(Details)
            Dim IDs As String() = ValueId.Split(",")
            For i As Integer = 0 To IDs.Length - 1
                XMLDetail = xmlDoc.CreateElement("Detail")
                XMLValueId = xmlDoc.CreateAttribute("ID")
                XMLValueId.InnerText = IDs(i)
                XMLDetail.Attributes.Append(XMLValueId)
                xmlDoc.DocumentElement.InsertBefore(XMLDetail, xmlDoc.DocumentElement.LastChild)

            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Sub StaffFilterParameters(ByVal param As Hashtable)

        param.Add("@EMP_DES_ID", directory.Item("EMP_DES_ID"))
        param.Add("@EMP_DPT_ID", directory.Item("EMP_DPT_ID"))
        param.Add("@EMP_ECT_ID", directory.Item("EMP_ECT_ID"))

        param.Add("@EMP_STATUS", directory.Item("EMP_STATUS"))
        param.Add("@EMP_VISA_BSU_ID", directory.Item("EMP_VISA_BSU_ID"))


        param.Add("@EMP_bOVERSEAS", directory.Item("EMP_bOVERSEAS"))

        param.Add("@EMP_VISATYPE", directory.Item("EMP_VISATYPE"))
        param.Add("@EMP_bTemp", directory.Item("EMP_bTemp"))
        param.Add("@EMP_ABC", directory.Item("EMP_ABC"))
        param.Add("@BSU_ID_EMP", directory.Item("BSU_ID"))
        If HiddenMenuCode.Value = "H000192" Or HiddenMenuCode.Value = "H000102" Or HiddenMenuCode.Value = "H000187" Then
            param.Add("@ESD_ID", directory.Item("ESD_ID"))
            'ElseIf HiddenMenuCode.Value = "H000191" Or HiddenMenuCode.Value = "H000196" Or HiddenMenuCode.Value = "H000188" Or HiddenMenuCode.Value = "H000202" Or HiddenMenuCode.Value = "H000111" Or HiddenMenuCode.Value = "H000112" Then
            '    param.Add("@BSU_ID_EMP", directory.Item("BSU_ID"))

        End If

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        directory.Clear()

        Dim param As New Hashtable
        ''Default Parameters 
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@BSU_ID", Session("sbsuid"))


        ''Report(Parameters)
        Filter()

        If HiddenMenuCode.Value = "H000191" Then ''Staff Report 

            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF LIST REPORT"
        ElseIf HiddenMenuCode.Value = "H000103" Then ''Staff Report 

            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF LIST REPORT"

        ElseIf HiddenMenuCode.Value = "H000299" Then ''Staff Report 

            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF LIST REPORT"

        ElseIf HiddenMenuCode.Value = "H000192" Then ''Document Expiry Date Report

            Dim fromdate As DateTime = Convert.ToDateTime(txtfrom.Text.Trim())
            Dim todate As DateTime = Convert.ToDateTime(txtto.Text.Trim())
            Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
            Dim TDate As String = todate.ToString("yyyy-MM-dd")

            StaffFilterParameters(param)
            param.Add("@FROMDATE", FDate)
            param.Add("@TODATE", TDate)
            param.Add("@ReportHeader", "From :" & fromdate.ToString("dd-MMM-yyyy") & " :: Todate :" & todate.ToString("dd-MMM-yyyy") & " :: " & ViewState("Criteria")) 'DOCUMENT EXPIRY DATE REPORT
        ElseIf HiddenMenuCode.Value = "H000102" Then ''Document Expiry Date Report

            Dim fromdate As DateTime = Convert.ToDateTime(txtfrom.Text.Trim())
            Dim todate As DateTime = Convert.ToDateTime(txtto.Text.Trim())
            Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
            Dim TDate As String = todate.ToString("yyyy-MM-dd")

            StaffFilterParameters(param)
            param.Add("@FROMDATE", FDate)
            param.Add("@TODATE", TDate)
            param.Add("@ReportHeader", "From :" & fromdate.ToString("dd-MMM-yyyy") & " :: Todate :" & todate.ToString("dd-MMM-yyyy") & " :: " & ViewState("Criteria")) 'DOCUMENT EXPIRY DATE REPORT

        ElseIf HiddenMenuCode.Value = "H000187" Then ''Document Expiry Date Report

            Dim fromdate As DateTime = Convert.ToDateTime(txtfrom.Text.Trim())
            Dim todate As DateTime = Convert.ToDateTime(txtto.Text.Trim())
            Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
            Dim TDate As String = todate.ToString("yyyy-MM-dd")

            StaffFilterParameters(param)
            param.Add("@FROMDATE", FDate)
            param.Add("@TODATE", TDate)
            param.Add("@ReportHeader", "From :" & fromdate.ToString("dd-MMM-yyyy") & " :: Todate :" & todate.ToString("dd-MMM-yyyy") & " :: " & ViewState("Criteria")) 'DOCUMENT EXPIRY DATE REPORT

        ElseIf HiddenMenuCode.Value = "H000188" Then ''Super Annual Report

            Dim fromdate As DateTime = Convert.ToDateTime(txtfrom.Text.Trim())
            Dim todate As DateTime = Convert.ToDateTime(txtto.Text.Trim())
            Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
            Dim TDate As String = todate.ToString("yyyy-MM-dd")

            StaffFilterParameters(param)
            param.Add("@FROMDATE", FDate)
            param.Add("@TODATE", TDate)
            param.Add("@AgePanel", ddlAgePanel.SelectedItem.Value)
            param.Add("@ReportHeader", "From :" & fromdate.ToString("dd-MMM-yyyy") & " :: Todate :" & todate.ToString("dd-MMM-yyyy") & " :: " & ViewState("Criteria")) 'DOCUMENT EXPIRY DATE REPORT

        ElseIf HiddenMenuCode.Value = "H000193" Then ''Empty Date List Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) 'STAFF EMPTY DATE LIST REPORT

        ElseIf HiddenMenuCode.Value = "H000194" Then ''Passport Borrow Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF PASSPORT BORROW REPORT"
        ElseIf HiddenMenuCode.Value = "H000195" Then ''Passport Pending Return Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF PASSPORT PENDING RETURN REPORT"
        ElseIf HiddenMenuCode.Value = "H000196" Then ''MOL Inspection List Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF MOL INSPECTION LIST REPORT"
        ElseIf HiddenMenuCode.Value = "H000197" Then ''Nationality Statistics Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"NATIONALITY STATISTICS REPORT"
        ElseIf HiddenMenuCode.Value = "H000198" Then ''Qualification Category Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) 'QUALIFICATION CATEGORY REPORT
        ElseIf HiddenMenuCode.Value = "H000199" Then ''Qualification Statistics Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"QUALIFICATION STATISTICS REPORT"
        ElseIf HiddenMenuCode.Value = "H000110" Then ''Health Card Expiry Report
            Dim fromdate As DateTime = Convert.ToDateTime(txtfrom.Text.Trim())
            Dim todate As DateTime = Convert.ToDateTime(txtto.Text.Trim())
            Dim FDate As String = fromdate.ToString("yyyy-MM-dd")
            Dim TDate As String = todate.ToString("yyyy-MM-dd")
            StaffFilterParameters(param)
            param.Add("@FROMDATE", FDate)
            param.Add("@TODATE", TDate)
            param.Add("@ReportHeader", "From :" & fromdate.ToString("dd-MMM-yyyy") & " :: Todate :" & todate.ToString("dd-MMM-yyyy") & " :: " & ViewState("Criteria")) '"HEALTH CARD EXPIRY REPORT"
        ElseIf HiddenMenuCode.Value = "H000111" Then ''Staff List Format 1 Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) 'STAFF LIST FORMAT 1 REPORT
        ElseIf HiddenMenuCode.Value = "H000112" Then ''Staff List Format 2 Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) 'STAFF LIST FORMAT 2 REPORT
        ElseIf HiddenMenuCode.Value = "H000113" Then ''Staff Communication Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF COMMUNICATION REPORT"
        ElseIf HiddenMenuCode.Value = "H000202" Then ''Staff Communication Report
            StaffFilterParameters(param)
            param.Add("@ReportHeader", ViewState("Criteria")) '"STAFF COMMUNICATION REPORT"
        End If



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If HiddenMenuCode.Value = "H000191" Then ''Staff Report 
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffList.rpt")
            ElseIf HiddenMenuCode.Value = "H000103" Then ''Staff Report 
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffList.rpt")
            ElseIf HiddenMenuCode.Value = "H000299" Then ''Staff Report 
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffList_New.rpt")
            ElseIf HiddenMenuCode.Value = "H000192" Then ''Document Expiry Date Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_DocExpiry.rpt")
            ElseIf HiddenMenuCode.Value = "H000102" Then ''Document Expiry Date Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_DocExpiry.rpt")
            ElseIf HiddenMenuCode.Value = "H000187" Then ''Document Expiry Date Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_DocExpiry_DaysLeft.rpt")
            ElseIf HiddenMenuCode.Value = "H000193" Then ''Empty Date List Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_EmptyDateList.rpt")
            ElseIf HiddenMenuCode.Value = "H000194" Then ''Passport Borrow Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_PassportBorrow.rpt")
            ElseIf HiddenMenuCode.Value = "H000195" Then ''Passport Borrow Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_PassportPendingReturn.rpt")
            ElseIf HiddenMenuCode.Value = "H000196" Then ''MOL Inspection List Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_MOL_InspectionList_New.rpt")
            ElseIf HiddenMenuCode.Value = "H000197" Then ''Nationality Statistics Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_NationalityStatistics.rpt")
            ElseIf HiddenMenuCode.Value = "H000198" Then ''Qualification Category Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_QualificationCategory.rpt")
            ElseIf HiddenMenuCode.Value = "H000199" Then ''Qualification Statistics Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_QualificationStatistics.rpt")
            ElseIf HiddenMenuCode.Value = "H000110" Then ''Health Card Expiry Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_HealthCardExpiry.rpt")
            ElseIf HiddenMenuCode.Value = "H000111" Then ''Staff List Format 1 Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffListFormat1.rpt")
            ElseIf HiddenMenuCode.Value = "H000112" Then ''Staff List Format 2 Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffListFormat2.rpt")
            ElseIf HiddenMenuCode.Value = "H000113" Then ''Staff Communication Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_StaffCommunication.rpt")
            ElseIf HiddenMenuCode.Value = "H000188" Then ''Super Annual Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/CR_SuperAnnual.rpt")
            ElseIf HiddenMenuCode.Value = "H000202" Then ''Staff UserName Report
                .reportPath = Server.MapPath("~/OASIS_HR/StaffReports/Reports/StaffUserNameInfo.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    'Protected Sub DataStaffFilter_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataStaffFilter.Unload
    '    For Each row As DataListItem In DataStaffFilter.Items
    '        Dim ListDynamic As New CheckBoxList
    '        Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
    '        If STF_FT_ID <> 10 Then
    '            ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

    '            ListDynamic.SelectedValue = -1
    '        End If

    '    Next



    '    'If DataStaffFilter.Items.Count > 0 Then
    '    '    Dim chkl As CheckBoxList
    '    '    chkl = DirectCast(DataStaffFilter.FindControl("|5|"), CheckBoxList)
    '    '    chkl.SelectedIndex = 0
    '    'End If

    'End Sub

    'Protected Sub DataStaffFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataStaffFilter.SelectedIndexChanged
    '    For Each row As DataListItem In DataStaffFilter.Items
    '        Dim ListDynamic As New CheckBoxList
    '        Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
    '        ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

    '        If ListDynamic.DataValueField = "est_id" Then
    '            If ListDynamic.Items.FindByValue("1").Selected = True Then

    '                ListDynamic.Items.FindByValue("2").Selected = True
    '           end if
    '        End If

    '    Next


    'End Sub
End Class
