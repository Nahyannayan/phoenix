﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CORP_StaffAttendance.aspx.vb" Inherits="OASIS_HR_CORP_StaffAttendance" MasterPageFile="~/mainMasterPage.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .legend {
            height: 25px;
            width: 25px;
            display: inline-block;
        }

            .legend.annual {
                background-color: #33ccff;
            }

            .legend.emergency {
                background-color: #ff751a;
            }

            .legend.medical {
                background-color: #004d99;
            }

            .legend.maternity {
                background-color: #ff33cc;
            }

            .legend.holiday {
                background-color: #adb8b7;
            }

        .not-logged {
            font: bolder;
            color: red;
        }

        .validation-warning {
            color: red;
        }

        #divCorpStaffAttendanceReport td span {
            display: inline-block;
            float: left;
            line-height: 25px;
        }

            #divCorpStaffAttendanceReport td span.legend, .not-logged {
                margin-right: 8px;
            }

        .error {
            font-family: Raleway,sans-serif;
            font-size: 7pt;
            font-weight: normal;
        }

        .RadGrid_Default .rgHeader {
            background-color: #92D050;
            background: #92D050;
        }

        .RadGrid_Default .rgAltRow td, .RadGrid_Default .rgRow td, .RadGrid_Default .rgMasterTable td.rgExpandCol {
            border-color: #949494;
        }

        .RadGrid_Default .rgRow td, .RadGrid_Default .rgAltRow td {
            border-width: 0 1px 1px 0;
            font-weight:normal;
        }

        .RadGrid_Default .rgAltRow {
            background: none;
        }
        .RadGrid_Default .rgHeader {
            border-right: solid 1px #669435;
        }
        .RadGrid table.rgMasterTable tr .rgExpandCol  {
            border-right-width: 1px;
            border-left-width: 0;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function checkDate(sender, args) {
            var fromDate = new Date($('#<%= txtFrom.ClientID %>').val());
            if (sender._selectedDate < fromDate) {
                alert("You cannot select a day before 'From Date'!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(fromDate.format(sender._format))
                return;
            }
            console.log(sender._selectedDate);
        }

        function validateFromDate(sender, args) {
            var fromDate = sender._selectedDate;
            var toDate = new Date($('#<%= txtTo.ClientID%>').val());
            if (toDate != undefined && toDate != '') {
                if (fromDate > toDate) {
                    $('#<%= txtTo.ClientID%>').val(fromDate.format(sender._format))
                }
                console.log(sender._selectedDate);
            }
        }



        function getDateInMonthFirstFormat(dateInStringFormat) {
            var currentDate = dateInStringFormat;
            return currentDate.substring(3, 5) + "/" + currentDate.substring(0, 2) + "/" + currentDate.substring(6);
        }
    </script>
    <div class="card mb-3 form-horizontal">
        <div class="card-header letter-space">
            <i class="fa fa-file mr-3"></i>CORP Staff Attendance Report
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="card">
                    <div class="card-body">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <span class="field-label">From Date<font color="maroon">*</font></span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtFrom" runat="server" AutoComplete="off"></asp:TextBox>
                                    <asp:ImageButton ID="imgCal1" runat="server"
                                        ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="MM/dd/yyyy"
                                        PopupButtonID="imgCal1" TargetControlID="txtFrom" CssClass="MyCalendar" OnClientDateSelectionChanged="validateFromDate">
                                    </ajaxToolkit:CalendarExtender>

                                </td>
                                <td align="left">
                                    <span class="field-label">To Date<font color="maroon">*</font></span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtTo" runat="server" AutoComplete="off"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton2" runat="server"
                                        ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" OnClientDateSelectionChanged="checkDate"
                                        PopupButtonID="ImageButton2" TargetControlID="txtTo" CssClass="MyCalendar">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                                <%-- Employee dropdown functionality disabled as per the requirements --%>
                                <%-- <td align="left">
                                    <span class="field-label">Employee<font color="maroon">*</font></span>
                                </td>
                                <td align="left">
                                    <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbEmployee" Width="100%"
                                        MarkFirstMatch="true" OnClientItemsRequesting="Combo_OnClientItemsRequesting" AllowCustomText="true" Filter="Contains"
                                        HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                                        OnDataBound="cmbEmployee_DataBound" OnItemDataBound="cmbEmployee_ItemDataBound"
                                        EmptyMessage="Start typing to search..." Skin="Office2010Silver" >
                                        <HeaderTemplate>
                                            <ul>

                                                <li class="col2">Employee Name</li>
                                            </ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <ul>

                                                <li class="col2">
                                                    <%# DataBinder.Eval(Container.DataItem, "Name")%></li>
                                            </ul>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                                            items
                                        </FooterTemplate>
                                    </telerik:RadComboBox>
                                </td>--%>
                                <td class="matters" style="height: 11px" valign="bottom" colspan="6">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" OnClick="btnSearch_Click" CausesValidation="true" ValidationGroup="AttGroup1,AttGroup2,AttGroup3,DateValidation1,DateValidation2" />
                                    <asp:Button ID="btnDownloadPdf" runat="server" CssClass="button" Text="Pdf" OnClick="btnDownloadPdf_Click"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="left"></td>
                                <td align="left">
                                    <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                        Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup1" CssClass="error"
                                        ForeColor=""></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" CssClass="error"
                                        ControlToValidate="txtFrom" ErrorMessage="Enter a valid From Date"
                                        Operator="DataTypeCheck" Type="Date" ValidationGroup="DateValidation1" />
                                </td>
                                <td align="left"></td>
                                <td align="left">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTo"
                                        Display="Dynamic" ErrorMessage="To Date required" ValidationGroup="AttGroup2" CssClass="error"
                                        ForeColor=""></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator4" runat="server" CssClass="error"
                                        ControlToValidate="txtTo" ErrorMessage="Enter a valid To Date"
                                        Operator="DataTypeCheck" Type="Date" ValidationGroup="DateValidation2" />

                                </td>
                                <%-- Employee dropdown validation functionality disabled as per the requirements --%>
                                <%-- <td align="left"></td>
                                <td align="left">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cmbEmployee"
                                        Display="Dynamic" ErrorMessage="Employee required" ValidationGroup="AttGroup3" CssClass="error"
                                        ForeColor=""></asp:RequiredFieldValidator>
                                </td>--%>
                                <td></td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div id="divCorpStaffAttendanceReport" class="form-group">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <telerik:RadGrid ID="gv_CorpStaffAttendanceReport" runat="server" AllowPaging="True"
                                OnNeedDataSource="gv_CorpStaffAttendanceReport_NeedDataSource" OnDetailTableDataBind="gv_CorpStaffAttendanceReport_DetailTableDataBind"
                                PageSize="10" AllowSorting="True" CssClass="table table-bordered  table-row" MasterTableView-AllowPaging="true"
                                AutoGenerateColumns="False" CellSpacing="0" GridLine="">
                                <ClientSettings AllowDragToGroup="True">
                                </ClientSettings>
                                <MasterTableView ShowHeader="true" AutoGenerateColumns="False" AllowPaging="true" DataKeyNames="EmployeeId,StaffId,AttendanceDate" HierarchyLoadMode="ServerOnDemand">
                                    <DetailTables>
                                        <telerik:GridTableView BackColor="#92d050" DataKeyNames="EmployeeId,StaffId,AttendanceDate" Name="StaffDetails" AutoGenerateColumns="false" Width="100%" HierarchyLoadMode="ServerOnDemand" AllowPaging="true">
                                            <ParentTableRelation>
                                                <telerik:GridRelationFields DetailKeyField="StaffId" MasterKeyField="StaffId"></telerik:GridRelationFields>
                                                <telerik:GridRelationFields DetailKeyField="AttendanceDate" MasterKeyField="AttendanceDate"></telerik:GridRelationFields>
                                            </ParentTableRelation>
                                            <Columns>
                                                <telerik:GridBoundColumn HeaderText="In Time" DataField="InTime" UniqueName="column11">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="In Terminal" DataField="DepartmentDescription" UniqueName="column12">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Out Time" DataField="OutTime" UniqueName="column13">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Out Terminal" DataField="DepartmentDescription" UniqueName="column14">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="In Minutes" DataField="TotalMinutes" UniqueName="column15">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </telerik:GridTableView>
                                    </DetailTables>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="StaffName" AllowFiltering="false"
                                            HeaderText="Staff Name"
                                            UniqueName="column0">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AttendanceDate" AllowFiltering="false"
                                            HeaderText="Log Date"
                                            UniqueName="column1">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LoggedInTime"
                                            HeaderText="First In"
                                            UniqueName="column2">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LoggedOutTime"
                                            HeaderText="Last Out"
                                            UniqueName="column3">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TotalHours"
                                            HeaderText="Total Time"
                                            UniqueName="column4">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LeaveType"
                                            HeaderText="Leave Type"
                                            UniqueName="column5">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="StaffId" AllowFiltering="false"
                                            HeaderText="Staff Id"
                                            UniqueName="column6" Visible="false">
                                        </telerik:GridBoundColumn>

                                    </Columns>
                                </MasterTableView>
                                <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                <ItemStyle Font-Bold="true" HorizontalAlign="Left" />
                                <AlternatingItemStyle Font-Bold="true" HorizontalAlign="Left" />
                                <FilterMenu>
                                </FilterMenu>
                                <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                            </telerik:RadGrid>
                        </div>
                        <table width="80%" style="margin-left: 10%;">
                            <tr class="">
                                <td align="left"><span class="legend annual"></span><span>Annual Leave</span></td>
                                <td align="left"><span class="legend emergency"></span><span>Emergency Leave</span></td>
                                <td align="left"><span class="legend medical"></span><span>Medical Leave</span></td>
                                <td align="left"><span class="legend maternity"></span><span>Paternity/Maternity Leave</span></td>
                                <td align="left"><span class="legend holiday"></span><span>Holiday</span></td>
                                <td align="left"><span class="not-logged">NL</span><span>Not Logged</span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-body">
                        <p>
                            <small>Disclaimer:-
                                <br />
                                - The report is based on access / attendance records for 3 work locations only - SSC, Al Joud and McLaren offices<br />
                                - Time spent at the schools is not captured as part of the attendance logs<br />
                                - The daily work hours is calculated based on the sum total of time spent at individual offices. (excludes travel time and meetings attended in schools)<br />
                                - The report is based on the in and out entries logs captured at each work location. Attendance report may not accurately reflect the total time spent at work if the user does not properly log the in /out entries
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
            function Combo_OnClientItemsRequesting(sender, eventArgs) {
                var combo = sender;
                ComboText = combo.get_text();
                ComboInput = combo.get_element().getElementsByTagName('input')[0];
                ComboInput.focus = function () { this.value = ComboText };

                if (ComboText != '') {
                    window.setTimeout(TrapBlankCombo, 100);
                };
            }

            function TrapBlankCombo() {
                if (ComboInput) {
                    if (ComboInput.value == '') {
                        ComboInput.value = ComboText;
                    }
                    else {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                };
            }

        </script>
    </telerik:RadScriptBlock>
</asp:Content>




