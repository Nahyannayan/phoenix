﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="Emp_TuitionFeeConcessionApproval.aspx.vb" Inherits="OASIS_HR_Fees_Emp_TuitionFeeConcessionApproval" %>



<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        @media (min-width: 992px) {
            .modal-xl {
                max-width: 1200px;
            }
        }

        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }

       span.spancheck input[type=checkbox] {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            transform: scale(1.5);
            padding: 10px;
        }
    </style>
    <style type="text/css">
        /*-------Stamp css style for concession page goes here---------*/

        .stamp {
            transform: rotate(4deg);
            color: #555;
            font-size: 2rem;
            font-weight: 700;
            border: 0.25rem solid #555;
            display: inline-block;
            padding: 0.25rem 1rem;
            text-transform: uppercase;
            border-radius: 1rem;
            font-family: 'Courier';
            -webkit-mask-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/8399/grunge.png');
            -webkit-mask-size: 944px 604px;
            mix-blend-mode: multiply;
        }

        .is-nope {
            color: #D23;
            border: 0.5rem double #D23;
            transform: rotate(3deg);
            -webkit-mask-position: 2rem 3rem;
            font-size: 2rem;
        }

        .is-approved {
            color: #0A9928;
            border: 0.5rem solid #0A9928;
            -webkit-mask-position: 13rem 6rem;
            transform: rotate(-14deg);
            border-radius: 0;
        }

        .is-draft {
            color: #C4C4C4;
            border: 1rem double #C4C4C4;
            transform: rotate(-5deg);
            font-size: 6rem;
            font-family: "Open sans", Helvetica, Arial, sans-serif;
            border-radius: 0;
            padding: 0.5rem;
        }

        /*-------Stamp css style for concession page ends here---------*/
        .line-h30 {
            line-height: 30px;
        }

        .profile-image img {
            border-radius: 100%;
            width: 100px;
            overflow: hidden;
            border: 1px solid #efefef;
            box-shadow: 3px 5px 5px rgba(0,0,0,0.03);
        }

        .card {
            border-radius: 0px !important;
        }

        .card-header:first-child {
            border-radius: 0px !important;
        }


        /* The popup - Add Notes */
        /* Popup container - can be anything you want */
        .popup {
            position: absolute;
            top: -100px;
            background-color: rgb(153, 153, 153);
            border-radius: 6px;
            padding: 4px 6px;
            right: -10px;
            z-index: 1000;
        }



            /* The actual popup */
            .popup .popuptext {
                visibility: hidden;
                width: 160px;
                background-color: #555;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 8px 0;
                position: absolute;
                z-index: 1;
                bottom: 125%;
                left: 50%;
                margin-left: -80px;
            }

            .popup .show {
                visibility: visible;
            }
        /* The popup - Add Notes ends here*/
    </style>

    <script type="text/javascript">

        //$(function () {
        //    $('.chkTrt').on('click', function () {
        //        alert();
        //        $(' .chkTrt').not(this).prop('checked', false);
        //    });
        //});
        function chkboxuncheck(id) {
            //alert(id.value);
            var idval = id.value
            $('.' + idval).not(id).prop('checked', false);
        }

        function uploadComplete(val) {
            document.getElementById('<%=hfpostback.ClientID%>').value = val;
            $get("<%=btnUpload.ClientID%>").click();

        }
        function uploadError(sender) {
          <%-- $get("<%=lblMesg.ClientID%>").innerHTML = "File upload failed.";--%>
        }
        function NotesClick(id) {
            $('#' + id).addClass("show");
            $("#" + id).css("display", "block");
        }
        function closedivNotes(id) {
            $("#" + id).css("display", "none");
            $("#" + id).removeClass("show");
        }
        function getdocs(valueid, clientids) {
            //alert(valueid);            
            document.getElementById(clientids).value = valueid;
            $get("<%=btnDownload.ClientID%>").click();
        }

        $(document).ready(function () {
            $('[data-toggle="dwl_tooltip"]').tooltip();
        });
        function showConfirmDelModal() {
            $("#confirmDelModal").modal('show');
        }
        function deletedocs(hid, clienthids, valueid, clientids) {
            //alert(valueid);
            document.getElementById(clientids).value = valueid;
            document.getElementById(clienthids).value = hid;
            showConfirmDelModal()

        }
        function showConfirmDelete() {
            $get("<%=btnDelete.ClientID%>").click();
        }
        function cleardate(clientids) {
            document.getElementById(clientids).value = ""
        }
        $(document).ready(function () {
            $('[data-toggle="del_tooltip"]').tooltip();
        });
        function adddivNotes(id, idval, idval2, idnotes, idlblnotes) {

            //alert($('.' + idnotes).val());
            var notess = $('.' + idnotes).val();
            if (idval != '') {
                var Idval = idval
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/OASIS_HR/Fees/Emp_TuitionFeeConcessionApproval.aspx/AddNotes") %>',
                    data: '{SCR_ID: "' + idval + '", SCD_ID: "' + idval2 + '", Notes: "' + notess + '"  }',
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                        var a = results.d.split('||');
                        if (a[0] == '0') {
                            $("#" + id).css("display", "none");
                            $("#" + id).removeClass("show");
                            $("." + idlblnotes).text($('.' + idnotes).val());
                            //$('#LBLError2').text(a[1]);
                            //$('#divalert').attr("class", "alert alert-success");
                            //$('#divalert').css("display", "block");

                        }
                        else {

                            //$('#LBLError2').text(a[1]);
                            //$('#divalert').attr("class", "alert alert-danger");
                            //$('#divalert').css("display", "block");
                        }
                    }
                });
            }
        }
    </script>
    <script type="text/javascript">
        function showModal() {
            $("#confirmModal").modal('hide');
            $("#myModal").modal('show');
        }

        function hideModal() {
            $("#myModal").modal('hide');
            $('.modal-backdrop').remove();
            //$('#closebtn').click();                
        }

        function hideModal2() {
            $("#myModal").modal('hide');
            $('.modal-backdrop').remove();
            //$('#closebtn').click();    
            //location.reload(true);
            var url = "Emp_TuitionFeeConcessionApproval.aspx?MainMnu_code=4LcY%20vSkkfI=&datamode=4xCdg/cr4Xw=&viewid=" + '<%= Session("PHNX_FEE_CON_EMP_ID") %>';
            window.location.href = url

            $("#page-top").removeClass("modal-open");
            $("#page-top").css('padding-right', '0px');
        }
        function hideModal3() {
            $("#confirmDelModal").modal('hide');
            $('.modal-backdrop').remove();

            $("#page-top").removeClass("modal-open");
            $("#page-top").css('padding-right', '0px');
            //$('#closebtn').click();  
            var url = "Emp_TuitionFeeConcessionApproval.aspx?MainMnu_code=4LcY%20vSkkfI=&datamode=4xCdg/cr4Xw=&viewid=" + '<%= Session("PHNX_FEE_CON_EMP_ID") %>';
            window.location.href = url
        }

        function showFullModal_And(rowcount) {
            $('#<%= chkElgSelect1.ClientID%>').attr("disabled", "true");
            $('#<%= chkElgSelect2.ClientID%>').attr("disabled", "true");
            $('#<%= chkElgSelect3.ClientID%>').attr("disabled", "true");

            $('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect2.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect3.ClientID%>').removeAttr("checked");
            if (rowcount == 2) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "none");
            }
            else if (rowcount == 3) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "table-row");
            }
            //$("#elg_2").css("display", "table-row");
            //$("#elg_3").css("display", "table-row");
            $("#elg_rad").css("display", "table-row");
            $("#myModal").modal('show');
        }

        function showFullModal_Or1(rowcount) {
            //$('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled");
            $('#<%= chkElgSelect2.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect3.ClientID%>').removeAttr("checked");
            if (rowcount == 2) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "none");
            }
            else if (rowcount == 3) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "table-row");
            }
            $("#elg_rad").css("display", "table-row");
            $("#myModal").modal('show');
        }

        function showFullModal_Or2(rowcount) {
            //$('#<%= chkElgSelect2.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled");
            $('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect3.ClientID%>').removeAttr("checked");
            if (rowcount == 2) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "none");
            }
            else if (rowcount == 3) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "table-row");
            }
            //$("#elg_2").css("display", "table-row");
            //$("#elg_3").css("display", "table-row");
            $("#elg_rad").css("display", "table-row");
            $("#myModal").modal('show');
        }

        function showFullModal_Or3(rowcount) {
            //$('#<%= chkElgSelect3.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled");
            $('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
            $('#<%= chkElgSelect2.ClientID%>').removeAttr("checked");
            if (rowcount == 2) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "none");
            }
            else if (rowcount == 3) {
                $("#elg_2").css("display", "table-row");
                $("#elg_3").css("display", "table-row");
            }
            //$("#elg_2").css("display", "table-row");
            //$("#elg_3").css("display", "table-row");
            $("#elg_rad").css("display", "table-row");
            $("#myModal").modal('show');
        }

        function hideSetELG_Text() {
            $("#ELGIB_TXT").css('display', 'none');

            //$('#closebtn').click();
        }
        //$(function () {
        //    $("#closebtn").click(function () {
        //        hideModal();
        //    });
        //});
        function showConfirmModal() {
            $("#confirmModal").modal('show');
        }

        function hideConfirmModal() {
            $("#confirmModal").modal('hide');
            $('.modal-backdrop').remove();
            //$('#closebtn').click();            
        }

        function show_elg() {
            document.getElementById("hdn_elg_count").value = parseInt(document.getElementById("hdn_elg_count").value) + 1;
            //$("#hdn_elg_count").val = $("#hdn_elg_count").val + 1;
            if (document.getElementById("hdn_elg_count").value == 1) {
                $("#elg_2").css("display", "table-row");
                $("#chktdElgSelect1").css("display", "table-cell");
            }
            if (document.getElementById("hdn_elg_count").value == 2) {
                $("#elg_3").css("display", "table-row");
                $("#chktdElgSelect1").css("display", "table-cell");
            }

            if (document.getElementById("hdn_elg_count").value >= 1) {
                $("#elg_rad").css("display", "table-row");

                $('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
                $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled");
            }

            if ($('#<%=rad1.ClientID%>').prop("checked")) {

                $('#<%= chkElgSelect1.ClientID%>').attr("disabled", "true");
                $('#<%= chkElgSelect2.ClientID%>').attr("disabled", "true");
                $('#<%= chkElgSelect3.ClientID%>').attr("disabled", "true");

                $('#<%= chkElgSelect1.ClientID%>').prop('checked', false);
                $('#<%= chkElgSelect2.ClientID%>').prop('checked', false);
                $('#<%= chkElgSelect3.ClientID%>').prop('checked', false);
            }
            if ($('#<%= rad2.ClientID%>').prop("checked")) {

                $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled", "true");
                $('#<%= chkElgSelect2.ClientID%>').removeAttr("disabled", "true");
                $('#<%= chkElgSelect3.ClientID%>').removeAttr("disabled", "true");
                $('#<%= chkElgSelect1.ClientID%>').prop('checked', true);

            }

            $("#myModal").modal('show');
        }

        function remove_elg2() {
            $("#elg_2").css("display", "none");
            $('#<%= ddlStud2.ClientID%>').val("0");
            $('#<%= DDLBsu2.ClientID%>').val("0");
            $('#<%= txtConcPercentage2.ClientID%>').val("0");
            $('#<%= txtDate2.ClientID%>').val("");


            document.getElementById("hdn_elg_count").value = parseInt(document.getElementById("hdn_elg_count").value) - 1;

            if (document.getElementById("hdn_elg_count").value == 0) {
                $("#elg_rad").css("display", "none");
                $('#<%= chkElgSelect1.ClientID%>').attr("checked", "true");
                $('#<%= chkElgSelect1.ClientID%>').attr("disabled", "true");
            } else {
                $('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
                $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled");
            }



            $("#myModal").modal('show');


        }

        function remove_elg3() {
            $("#elg_3").css("display", "none");
            $('#<%= ddlStud3.ClientID%>').val("0");
            $('#<%= DDLBsu3.ClientID%>').val("0");
            $('#<%= txtConcPercentage3.ClientID%>').val("0");
            $('#<%= txtDate3.ClientID%>').val("");

            document.getElementById("hdn_elg_count").value = parseInt(document.getElementById("hdn_elg_count").value) - 1;

            if (document.getElementById("hdn_elg_count").value == 0) {
                $("#elg_rad").css("display", "none");
                $('#<%= chkElgSelect1.ClientID%>').attr("checked", "true");
                $('#<%= chkElgSelect1.ClientID%>').attr("disabled", "true");
            } else {
                $('#<%= chkElgSelect1.ClientID%>').removeAttr("checked");
                $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled");
            }


            $("#myModal").modal('show');
        }

        function rad1click() {
            $('#<%= chkElgSelect1.ClientID%>').attr("disabled", "true");
            $('#<%= chkElgSelect1.ClientID%>').prop('checked', false);

            $('#<%= chkElgSelect2.ClientID%>').attr("disabled", "true");
            $('#<%= chkElgSelect2.ClientID%>').prop('checked', false);

            $('#<%= chkElgSelect3.ClientID%>').attr("disabled", "true");
            $('#<%= chkElgSelect3.ClientID%>').prop('checked', false);
        }

        function rad2click() {
            $('#<%= chkElgSelect1.ClientID%>').removeAttr("disabled", "true");
            $('#<%= chkElgSelect1.ClientID%>').prop('checked', true);

            $('#<%= chkElgSelect2.ClientID%>').removeAttr("disabled", "true");
            //$('#<%= chkElgSelect2.ClientID%>').prop('checked', false);

            $('#<%= chkElgSelect3.ClientID%>').removeAttr("disabled", "true");
            //$('#<%= chkElgSelect3.ClientID%>').prop('checked', false);
        }

    </script>


    <div class="content container-fluid">
        <div class="row">
            <!-- Concession Error -->
            <div class="col-lg-12 col-md-12 col-12">
                <%--    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>--%>
                <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
            </div>
        </div>

        <div class="row">
            <!-- Concession Staff Profile starts here -->
            <div class="col-lg-4 col-md-4 col-12">
                <div class="profile-page" style="padding: 0px;">
                    <div class="row">

                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="card profile-header card-border">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">Employee Info
                                    </h5>
                                </div>

                                <div class="bg-inverse-dark card-body">
                                    <div class="row">

                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="profile-image float-md-center text-center mb-3">
                                                <asp:Image ID="imgEmpImage" runat="server" ImageUrl='' ToolTip='' Style="height: 100px;" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12 m-auto">

                                            <div class="row mb-3">
                                                <div class="col-lg-6 col-md-6 col-6">
                                                    <span class="font-weight-bold">Employee Name</span><br />
                                                    <span>
                                                        <asp:Label ID="LBL_EMP_NAME" runat="server"></asp:Label>
                                                    </span>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-6">
                                                    <span class="font-weight-bold">Employee ID</span>
                                                    <br />
                                                    <span>
                                                        <asp:Label ID="LBL_EMP_NO" runat="server"></asp:Label>
                                                        <asp:Label ID="LBL_EMP_ID" runat="server" Visible="false"></asp:Label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-lg-6 col-md-6 col-6">
                                                    <span class="font-weight-bold">Group Join Date</span><br />
                                                    <span>
                                                        <asp:Label ID="LBLGRPDOJ" runat="server"></asp:Label>
                                                    </span>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-6">
                                                    <span class="font-weight-bold">Date Of Join</span>
                                                    <br />
                                                    <span>
                                                        <asp:Label ID="LBL_EMP_DOJ" runat="server"></asp:Label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-lg-6 col-md-6 col-6">
                                                    <span class="font-weight-bold">Designation</span><br />
                                                    <span>
                                                        <asp:Label ID="LBL_EMP_DESIG" runat="server"></asp:Label>
                                                    </span>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-6">
                                                    <span class="font-weight-bold">Department</span>
                                                    <br />
                                                    <span>
                                                        <asp:Label ID="LBL_DPT" runat="server"></asp:Label>
                                                    </span>
                                                </div>
                                            </div>



                                            <div class="row mb-3">
                                                <%-- <div class="col-lg-12 col-md-12 col-6">
                                                    <span class="font-weight-bold">Eligibility for tuition fees concession for employee not set / verified                   
                     <asp:Button ID="btnEligibility" runat="server" CssClass="button" Text="Click here to Set Eligibility" />
                                                    </span>
                                                </div>--%>

                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <span class="font-weight-lite">

                                                        <asp:Label ID="LBL_ELIGIBILITY_TXT" runat="server"></asp:Label><br />
                                                        <span id="ELGIB_TXT" runat="server">Eligibility not set or verified for the employee</span>

                                                        <%--<a id="lnk_eligilibity" href="#" class="font-weight-bold" runat="server" onserverclick="btnEligibility_Click">Click here to Set Eligibility</a>--%>
                                                        <a id="A1" href="#" class="font-weight-bold" runat="server" onserverclick="btnEligibility_Click">Click here to Set Eligibility</a>
                                                        <asp:HiddenField ID="hdn_ELG_ID" runat="server" Value="0"></asp:HiddenField>
                                                        <input type="hidden" id="hdn_elg_count" value="0" />

                                                    </span>
                                                </div>
                                            </div>

                                        </div>

                                    </div>




                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Concession Staff Profile ends here -->
            <!-- Children Profile starts here -->
            <div class="col-lg-8 col-md-8 col-12">
                <div class="row justify-content-center">
                    <!-- Children Profile starts here -->

                    <asp:Repeater ID="repInfo" runat="server" OnItemDataBound="repeater_ItemDataBound">
                        <ItemTemplate>
                            <div class="col-lg-12 col-md-12 col-12 mb-2">
                                <div id="" class="card card-border" style='<%# String.Format("{0}", Eval("FREEZE_STUD"))%>'>
                                    <div class="card-header p-2" style='<%# String.Format("{0}", Eval("HEADER_COLOR"))%>'>
                                        <h5 class="card-title mb-0 line-h30">
                                            <asp:CheckBox ID="chkSelect" runat="server" Enabled='<%# Bind("ISEDITABLE")%>' CssClass="spancheck"></asp:CheckBox>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Student_Name")%>'></asp:Label>
                                            <%--<input class="btn btn-primary btn-round float-right btn-sm" style="margin-left: 10px;" data-toggle="modal" data-target="#Apply_concession" type="button" value="Apply Concession">--%>
                                            <a id='<%# String.Format("IMG{0}", Eval("STU_NO"))%>' class="btn btn-white btn-sm btn-rounded btnWithoutCursor pl-2 pr-2 float-right" href="#" data-toggle="dropdown" aria-expanded="false" style='<%# String.Format("{0}", Eval("STAMP_CLASS"))%>'>
                                                <i class="fa fa-dot-circle-o text-info"></i>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("STAMP_TEXT")%>'></asp:Label></a>
                                        </h5>
                                    </div>
                                    <div class="card-body pb-0 pt-1">
                                        <div class="row staff-grid-row">

                                            <div class="col-lg-3 col-md-3 col-12">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-12">

                                                        <div class="profile-image float-md-center text-center p-3">
                                                            <a data-toggle="modal" data-target="#View_Profile" class="avatar-lg">
                                                                <img src='<%# String.Format("../../Payroll/ImageHandler.ashx?ID={0}&TYPE=EDD", Eval("EDD_ID"))%>' alt="" style="height: 100px; width: 100px;"></a>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12 col-md-12 col-12 text-center">
                                                        <div class="row mb-1">

                                                            <div class="col-lg-12 col-md-12 col-12">
                                                                <span class="font-weight-bold">ID :</span>

                                                                <span>
                                                                    <asp:Label ID="lblStu_no" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                                                    <asp:HiddenField ID="hdnSCR_ID" runat="server" Value='<%# Bind("SCR_ID")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnSCD_ID" runat="server" Value='<%# Bind("SCD_ID")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnSTU_ID" runat="server" Value='<%# Bind("STU_ID")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnCONC_SCHOOL_BSU_ID" runat="server" Value='<%# Bind("CONC_SCHOOL_BSU_ID")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnCONC_ELIGIBLE_STARTDT" runat="server" Value='<%# Bind("CONC_ELIGIBLE_STARTDT")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnELG_ID" runat="server" Value='<%# Bind("ELG_ID")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnACD_ENDDT" runat="server" Value='<%# Bind("ACD_ENDDT")%>'></asp:HiddenField>
                                                                    <asp:HiddenField ID="hdnVERIFY_MISMATCH" runat="server" Value='<%# Bind("VERIFY_MISMATCH")%>'></asp:HiddenField>
                                                                </span>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12 col-12">
                                                                <span class="font-weight-bold">Grade :</span>
                                                                <span>
                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("[Grade/Section]")%>'></asp:Label></span>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12 col-12">
                                                                <span class="font-weight-bold">DOJ :</span>
                                                                <span>
                                                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("DOJ")%>'></asp:Label></span>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-12">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <%--<div class="row mb-3 text-center">
                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <span class="font-weight-bold">Student ID :</span><br />

                                                        <span>
                                                            <asp:Label ID="lblStu_no" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                                            <asp:HiddenField ID="hdnSCR_ID" runat="server" Value='<%# Bind("SCR_ID")%>'></asp:HiddenField>
                                                            <asp:HiddenField ID="hdnSCD_ID" runat="server" Value='<%# Bind("SCD_ID")%>'></asp:HiddenField>
                                                            <asp:HiddenField ID="hdnSTU_ID" runat="server" Value='<%# Bind("STU_ID")%>'></asp:HiddenField>
                                                        </span>
                                                    </div>
                                                </div>--%>
                                                </div>
                                            </div>



                                            <div class="col-lg-9 col-md-9 col-12 mt-3">
                                                <%--                                                <div class="row mb-3">
                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <span class="font-weight-bold">Name :</span>
                                                        <br />
                                                        <span>
                                                            <asp:Label ID="lbSName" runat="server" Text='<%# Bind("Student_Name")%>'></asp:Label>
                                                            
                                                        </span>
                                                    </div>
                                                </div>--%>


                                                <div class="row mb-2 border-bottom">
                                                    <div class="col-lg-6 col-md-6 col-12">
                                                        <span class="font-weight-bold">Father Name</span>
                                                        <br />
                                                        <span>
                                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("FATHER_NAME")%>' CssClass='<%# Bind("VERIFY_MISMATCH_STYLE")%>'></asp:Label></span>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6 col-12">
                                                        <span class="font-weight-bold">Mother Name</span>
                                                        <br />
                                                        <span>
                                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("MOTHER_NAME")%>' CssClass='<%# Bind("VERIFY_MISMATCH_STYLE")%>'></asp:Label></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-2 border-bottom" style="display: none;">
                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <span class="font-weight-bold">School</span>
                                                        <br />
                                                        <span>
                                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("BSU_NAME")%>'></asp:Label></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1" style="display: none;">

                                                    <div class="col-lg-4 col-md-4 col-12">
                                                        <span class="font-weight-bold">Annual Fee</span><br />

                                                        <span>
                                                            <asp:Label ID="lbl_actual_amt" runat="server" Text='<%# Bind("FEE_ACTUAL_AMT")%>'></asp:Label>

                                                        </span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-4 col-12">
                                                        <span class="font-weight-bold">Eligible Amount</span>
                                                        <br />
                                                        <span>
                                                            <asp:Label ID="lbl_eligible_amt" runat="server" Text='<%# Bind("ELIGIBLE_AMOUNT")%>'></asp:Label></span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-4 col-12">
                                                        <span class="font-weight-bold">Approved Amount</span>
                                                        <br />
                                                        <span>
                                                            <asp:TextBox ID="txt_approved_amt" runat="server" Width="70%" Text='<%# Bind("APPROVED_AMOUNT")%>' CssClass="text-right"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftetxt_approved_amt" runat="server" FilterType="Numbers, Custom"
                                                                ValidChars="." TargetControlID="txt_approved_amt" />
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1">

                                                    <div class="col-lg-4 col-md-4 col-12">
                                                        <span class="font-weight-bold">Student School</span><br />

                                                        <span>
                                                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("STU_SCHOOL_CODE")%>'></asp:Label>

                                                        </span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-4 col-12">
                                                        <span class="font-weight-bold">Eligible School</span>
                                                        <br />
                                                        <span>
                                                            <asp:Label ID="Label9" runat="server" Text='<%# Bind("ELIGIBLE_SCHOOL_CODE")%>'></asp:Label></span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-4 col-12">
                                                        <%-- <span class="font-weight-bold"></span>
                                                        <br />--%>
                                                        <span>
                                                            <a id='<%# String.Format("IMG2{0}", Eval("STU_NO"))%>' class="btn btn-white btn-sm btn-rounded btnWithoutCursor pl-2 pr-2 float-right" href="#" data-toggle="dropdown" aria-expanded="false" style='<%# String.Format("{0}", Eval("ELG_CHECK_TEXT_STYLE"))%>'>
                                                                <i class="fa fa-dot-circle-o text-info"></i>
                                                                <asp:Label ID="Label11" runat="server" Text='<%# Bind("ELG_CHECK_TEXT")%>'></asp:Label></a>

                                                            <%-- <asp:Label ID="Label10" runat="server" Text='<%# Bind("ELG_CHECK_TEXT")%>' style='<%# Bind("ELG_CHECK_TEXT_STYLE")%>'></asp:Label>--%>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1" style='<%# String.Format("{0}", Eval("FREEZEDIV7"))%>'>
                                                    <div class="col-lg-6 col-md-6 col-12">
                                                        <span class="font-weight-bold">
                                                            <%-- <asp:CheckBox ID="" class="chkTrt" CssClass="" runat="server" Text="Approve within eligibility" />--%>
                                                            <input type="checkbox" checked='<%#Eval("ISELIGIBILITY_CHECK")%>' id="CHK_APPLY_ELG" runat="server" class='<%# String.Format("chkTrt{0}", Eval("STU_NO"))%>' value='<%# String.Format("chkTrt{0}", Eval("STU_NO"))%>' onclick="chkboxuncheck(this)" />
                                                            Approve within eligibility
                                                        </span>

                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-12">
                                                        <span class="font-weight-bold">
                                                            <%--<asp:CheckBox ID="" class="chkTrt" CssClass="" runat="server" Text="Approve beyond eligibility" />--%>
                                                            <input type="checkbox" checked='<%#Eval("ISFIN_APPR_CHECK")%>' id="CHK_APPLY_FIN" runat="server" class='<%# String.Format("chkTrt{0}", Eval("STU_NO"))%>' value='<%# String.Format("chkTrt{0}", Eval("STU_NO"))%>' onclick="chkboxuncheck(this)" />
                                                            Approve beyond eligibility
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-2 pb-2 border-bottom " style='<%# String.Format("{0}", Eval("ELG_CHECK_VERIFICATION_STYLE"))%>'>
                                                    <div class="col-lg-6 col-md-6 col-12" style='<%# String.Format("{0}", Eval("FREEZEDIV"))%>'>
                                                        <span class="font-weight-bold">
                                                            <asp:FileUpload ID="TXT_UPLOAD_DOC" runat="server" ToolTip="Upload Documents" onchange="uploadComplete('uploadfiles')" />
                                                            <%-- <ajaxToolkit:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                                                                runat="server" ID="TXT_UPLOAD_DOC" UploaderStyle="Traditional"
                                                                CompleteBackColor="White" UploadingBackColor="#CCFFFF" ThrobberID="imgLoader"
                                                                OnUploadedComplete="FileUploadComplete" />
                                                            <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" />--%>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-12" style='<%# String.Format("{0}", Eval("FREEZEDIV"))%>'>
                                                        <span>
                                                            <span id="Label12" style='<%# String.Format("{0}", Eval("IS_NO_UPLOADS"))%>'>
                                                                <asp:HiddenField ID="hfFilePath" runat="server" Value='<%# String.Format("{0}", Eval("UPLOADED_DOC_PATH"))%>' />
                                                                <span class="text-dark"><%# String.Format("{0}", Eval("UPLOADED_DOC_NAME"))%>
                                                                    <i class="fa fa-download cursor-pointer text-center text-info pt-1 font-weight-bold" data-toggle="dwl_tooltip" title="download" onclick="getdocs('<%# String.Format("{0}", Eval("SCD_ID"))%>','<%= hfFilePathID.ClientID%>')"></i>
                                                                </span>
                                                                <i class="fa fa-trash cursor-pointer text-center text-dark float-left pt-1 mr-3 font-weight-bold" data-toggle="del_tooltip" title="delete" onclick="deletedocs('<%# String.Format("{0}", Eval("SCR_ID"))%>','<%= hfFilePathHID.ClientID%>','<%# String.Format("{0}", Eval("SCD_ID"))%>','<%= hfFilePathID.ClientID%>')"></i>
                                                            </span></span>
                                                    </div>
                                                </div>

                                                <div class="row mb-1">
                                                    <div class="col-lg-8 col-md-8 col-12">
                                                        <span class="font-weight-bold float-left pl-1">Notes :</span>
                                                        <asp:Label ID="Label10" class='<%# String.Format("lblNotes{0}", Eval("STU_NO"))%>' runat="server" Text='<%# Bind("[NOTES]")%>'></asp:Label>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-12 text-right" style='<%# String.Format("{0}", Eval("FREEZEDIV"))%>'>
                                                        <a id="111" href="#" onclick="NotesClick('<%# String.Format("divNotes{0}", Eval("STU_NO"))%>')">Add Notes
                                                                    
                                                        </a>
                                                        <div id='<%# String.Format("divNotes{0}", Eval("STU_NO"))%>' class="popup popuptext" style="display: none;">
                                                            <div>
                                                                <span class="font-weight-bold float-left pl-1">Notes</span>  <i class="fa fa-times text-default pl-4 float-right cursor-pointer " onclick="closedivNotes('<%# String.Format("divNotes{0}", Eval("STU_NO"))%>')"></i>
                                                                <span class=""><i class="fa fa-save text-default cursor-pointer btn btn-default bg-white float-right" onclick="adddivNotes('<%# String.Format("divNotes{0}", Eval("STU_NO"))%>','<%# String.Format("{0}", Eval("SCR_ID"))%>','<%# String.Format("{0}", Eval("SCD_ID"))%>','<%# String.Format("txtNotes{0}", Eval("STU_NO"))%>','<%# String.Format("lblNotes{0}", Eval("STU_NO"))%>')"></i></span>
                                                            </div>
                                                            <asp:TextBox ID="TXT_NOTES" class='<%# String.Format("txtNotes{0}", Eval("STU_NO"))%>' runat="server" TextMode="MultiLine" Rows="5" Columns="60" Text='<%# Bind("[NOTES]")%>' />

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-1" style='<%# String.Format("{0}", Eval("VERIFY_MISMATCH_CHK_STYLE"))%>'>
                                                    <asp:CheckBox ID="chkagree" Checked='<%# String.Format("{0}", Eval("ISAPPROVED"))%>' Style='<%# String.Format("{0}", Eval("FREEZEDIV"))%>' CssClass="font-weight-bold" runat="server" Text='<%# Bind("HR_VERIFICATION_TXT")%>' />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <!-- Children Profile ends here -->
                </div>

            </div>
            <!-- Children Profile ends here -->
        </div>

        <div class="row mb-3 mt-3">
            <div class="col-lg-12 col-md-12 col-12 text-center">
                <asp:Button ID="btn_approve" runat="server" Text="Approve" CssClass="button" />
                <asp:Button ID="btn_reject" runat="server" Text="Reject" CssClass="button" />
                <asp:Button ID="btnUpload" runat="server" Style="background-color: transparent !important; border: none;" Text="" CausesValidation="False" />
                <asp:Button ID="btnDownload" runat="server" Style="background-color: transparent !important; border: none;" Text="" CausesValidation="False" />
                <asp:Button ID="btnDelete" runat="server" Style="background-color: transparent !important; border: none;" Text="" CausesValidation="False" />
                <asp:HiddenField ID="hfFilePathID" runat="server" />
                <asp:HiddenField ID="hfFilePathHID" runat="server" />

                <asp:HiddenField ID="hfpostback" runat="server" />
            </div>
        </div>
        <%--  <div class="row mb-3">

            <div class="col-lg-12 col-md-12 col-12">
                <span id="LBLError" class=""></span>
            </div>
        </div>--%>
    </div>


    <%--    <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
        <div class="panel-cover inner_darkPanlAlumini">
            <div>
                <asp:Button ID="btClose" type="button" runat="server"
                    Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                    ForeColor="White" Text="X"></asp:Button>
                <div>
                    <div align="CENTER">
                        <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                    </div>

                    <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">
                        <tr>

                            <td align="left" width="20%"><span class="field-label">Count</span>
                            </td>
                            <td align="left" width="40%"><span class="field-label">School Name</span>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Concession Perc (%)</span>
                            </td>
                            <td align="left" width="20%"><span class="field-label">Till Date</span>
                            </td>

                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddlStud1" runat="server">
                                    <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="DDLBsu1" runat="server" Width="100%" DataTextField="BSU_NAME"
                                    DataValueField="BSU_ID">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtConcPercentage1" runat="server" Width="100%"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtConcPercentage1" runat="server" FilterType="Numbers, Custom"
                                    ValidChars="-" TargetControlID="txtConcPercentage1" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDate1" runat="server" Enabled="false"></asp:TextBox><asp:ImageButton
                                    ID="img1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" PopupButtonID="img1" TargetControlID="txtDate1">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddlStud2" runat="server">
                                    <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="DDLBsu2" runat="server" Width="100%" DataTextField="BSU_NAME"
                                    DataValueField="BSU_ID">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtConcPercentage2" runat="server" Width="100%"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtConcPercentage2" runat="server" FilterType="Numbers, Custom"
                                    ValidChars="-" TargetControlID="txtConcPercentage2" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDate2" runat="server" Enabled="false"></asp:TextBox><asp:ImageButton
                                    ID="img2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" PopupButtonID="img2" TargetControlID="txtDate2">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddlStud3" runat="server">
                                    <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="DDLBsu3" runat="server" Width="100%" DataTextField="BSU_NAME"
                                    DataValueField="BSU_ID">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtConcPercentage3" runat="server" Width="100%"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtConcPercentage3" runat="server" FilterType="Numbers, Custom"
                                    ValidChars="-" TargetControlID="txtConcPercentage3" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDate3" runat="server" Enabled="false"></asp:TextBox><asp:ImageButton
                                    ID="img3" runat="server" ImageUrl="~/Images/calendar.gif" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                    Format="dd/MMM/yyyy" PopupButtonID="img3" TargetControlID="txtDate3">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" />
                                <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                            </td>
                        </tr>

                    </table>
                </div>

            </div>
        </div>
    </asp:Panel>--%>

    <!-- Modal -->
    <div id="myModal" class="modal fade " role="dialog">
        <div class="modal-dialog modal-xl">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Set Eligibility</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div>
                        <div id="lblerr" runat="server" align="CENTER" style="display: none;">
                            <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </div>

                        <div id="elg_rad" style="display: none;">
                            <asp:RadioButton ID="rad1" onclick="rad1click()" runat="server" Checked="true" GroupName="Rad" AutoPostBack="False" Visible="True" Text="And" CssClass="field-label" />
                            <asp:RadioButton ID="rad2" onclick="rad2click()" runat="server" GroupName="Rad" AutoPostBack="False" Visible="True" Text="Or" CssClass="field-label" />
                        </div>

                        <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" width="5%"><span class="field-label"></span>
                                </td>
                                <td align="left" width="10%"><span class="field-label">No. of Child</span>
                                </td>
                                <td align="left" width="15%"><span class="field-label">Type</span>
                                </td>
                                <td align="left" width="35%"><span class="field-label">School </span>
                                </td>
                                <td align="left" width="10%"><span class="field-label">Conc (%)</span>
                                </td>
                                <td align="left" width="20%"><span class="field-label">Valid Till</span>
                                </td>
                                <td align="left" width="5%"><span class="field-label"></span>
                                </td>
                            </tr>
                            <tr id="elg_1">



                                <td id="chktdElgSelect1" align="left">
                                    <asp:RadioButton ID="chkElgSelect1" runat="server" GroupName="elgRad"></asp:RadioButton>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlStud1" runat="server">
                                        <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>

                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="DDLBsuTyp1" runat="server" AutoPostBack="true" Width="100%" DataTextField="BSG_DESCR" OnSelectedIndexChanged="Typ1_Changed"
                                        DataValueField="BSG_ID">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="DDLBsu1" runat="server" Width="100%" DataTextField="BSU_NAME"
                                        DataValueField="BSU_ID">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtConcPercentage1" runat="server" Width="100%" Text="0"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtConcPercentage1" runat="server" FilterType="Numbers, Custom"
                                        ValidChars="." TargetControlID="txtConcPercentage1" />
                                </td>
                                <td align="left">
                                    <span>
                                        <i class="fa fa-edit cursor-pointer text-center text-info pt-1 font-weight-bold" style="position: absolute; margin-top: 10px; right: 125px;" title="clear" onclick="cleardate('<%= txtDate1.ClientID %>')"></i>
                                        <asp:TextBox ID="txtDate1" runat="server" Enabled="false"></asp:TextBox><asp:ImageButton
                                            ID="img1" runat="server" ImageUrl="~/Images/calendar.gif" />

                                    </span>

                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                        Format="dd/MMM/yyyy" PopupButtonID="img1" TargetControlID="txtDate1">
                                    </ajaxToolkit:CalendarExtender>

                                </td>
                                <td align="left">
                                    <img id="btnPlus" onclick="show_elg()" src="../../../images/add_book.png" width="36px" />
                                </td>
                            </tr>
                            <tr id="elg_2" style="display: none;">


                                <td id="chktdElgSelect2" align="left">
                                    <asp:RadioButton ID="chkElgSelect2" runat="server" GroupName="elgRad"></asp:RadioButton>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlStud2" runat="server">
                                        <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>

                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="DDLBsuTyp2" runat="server" AutoPostBack="true" Width="100%" DataTextField="BSG_DESCR" OnSelectedIndexChanged="Typ2_Changed"
                                        DataValueField="BSG_ID">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="DDLBsu2" runat="server" Width="100%" DataTextField="BSU_NAME"
                                        DataValueField="BSU_ID">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtConcPercentage2" runat="server" Width="100%" Text="0"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtConcPercentage2" runat="server" FilterType="Numbers, Custom"
                                        ValidChars="." TargetControlID="txtConcPercentage2" />
                                </td>
                                <td align="left">
                                    <i class="fa fa-edit cursor-pointer text-center text-info pt-1 font-weight-bold" style="position: absolute; margin-top: 10px; right: 125px;" title="clear" onclick="cleardate('<%= txtDate2.ClientID %>')"></i>
                                    <asp:TextBox ID="txtDate2" runat="server" Enabled="false"></asp:TextBox><asp:ImageButton
                                        ID="img2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                        Format="dd/MMM/yyyy" PopupButtonID="img2" TargetControlID="txtDate2">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                                <td align="left">
                                    <img id="btnMinus1" onclick="remove_elg2()" src="../../../images/minus_book.png" width="36px" />
                                </td>
                            </tr>
                            <tr id="elg_3" style="display: none;">



                                <td id="chktdElgSelect3" align="left">
                                    <asp:RadioButton ID="chkElgSelect3" runat="server" GroupName="elgRad"></asp:RadioButton>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlStud3" runat="server">
                                        <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>

                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="DDLBsuTyp3" runat="server" Width="100%" AutoPostBack="true" DataTextField="BSG_DESCR" OnSelectedIndexChanged="Typ3_Changed"
                                        DataValueField="BSG_ID">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="DDLBsu3" runat="server" Width="100%" DataTextField="BSU_NAME"
                                        DataValueField="BSU_ID">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <i class="fa fa-edit cursor-pointer text-center text-info pt-1 font-weight-bold" style="position: absolute; margin-top: 10px; right: 125px;" title="clear" onclick="cleardate('<%= txtDate3.ClientID %>')"></i>
                                    <asp:TextBox ID="txtConcPercentage3" runat="server" Width="100%" Text="0"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtConcPercentage3" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtConcPercentage3" />
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtDate3" runat="server" Enabled="false"></asp:TextBox><asp:ImageButton
                                        ID="img3" runat="server" ImageUrl="~/Images/calendar.gif" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                        Format="dd/MMM/yyyy" PopupButtonID="img3" TargetControlID="txtDate3">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                                <td align="left">
                                    <img id="btnMinus2" onclick="remove_elg3()" src="../../../images/minus_book.png" width="36px" />
                                </td>
                            </tr>


                        </table>
                    </div>
                </div>



                <div class="modal-footer">
                    <div class="row" style="width: 100%;">
                        <div class="col-lg-6 col-md-6 col-12 text-left">
                            <asp:Label ID="Err_Label" runat="server" CssClass="error text-left"></asp:Label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 text-right">
                            <asp:Button ID="btnUpdate" Text="Update" CssClass="btn btn-primary" runat="server" OnClick="btnUpdate_Click" />
                            <button id="closebtn" type="button" class="btn btn-default" data-dismiss="modal" onclick="hideModal()">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="confirmModal" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-md vertical-align-center">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btn_close_modal_info"><span aria-hidden="true">&times;</span></button>
                        <div id="confirmContent" style="font-weight: normal;">Eligibility not set or verified for the employee, Do you want to set? </div>
                    </div>
                    <div class="modal-footer text-center" id="footer_modal">
                        <button type="button" class="btn btn-primary btn_yes_confirm" onclick="showModal();">Yes</button>
                        <button type="button" class="btn btn-primary btn_no_confirm" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmDelModal" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-md vertical-align-center">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btn_close_delmodal_info"><span aria-hidden="true">&times;</span></button>
                        <div id="confirmDelContent" style="font-weight: normal;">Are you sure to delete the upload document? </div>
                    </div>
                    <div class="modal-footer text-center" id="footer_delmodal">
                        <button type="button" class="btn btn-primary btn_yes_confirm" onclick="showConfirmDelete();">Yes</button>
                        <button type="button" class="btn btn-primary btn_no_confirm" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
