﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Drawing
Imports AjaxControlToolkit

Partial Class OASIS_HR_Fees_Emp_TuitionFeeConcessionApproval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDelete)
        If Page.IsPostBack = False Then

            Try
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                Dim Employee_ID As String = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))

                Session("PHNX_FEE_CON_EMP_ID") = Request.QueryString("viewid")

                Dim dt As DataTable = GET_EMPLOYEE_DETAILS(1, Session("sBsuid"), Employee_ID)
                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then
                        LBL_EMP_NAME.Text = dt.Rows(0)("EMPLOYEE_NAME")
                        LBL_DPT.Text = dt.Rows(0)("DPT_DESCR")
                        LBL_EMP_NO.Text = dt.Rows(0)("EMPNO")
                        LBL_EMP_DOJ.Text = dt.Rows(0)("JOIN_DATE")
                        LBL_EMP_DESIG.Text = dt.Rows(0)("DESIGNATION")
                        imgEmpImage.ImageUrl = dt.Rows(0)("IMG_URL")
                        LBLGRPDOJ.Text = dt.Rows(0)("GRP_JOIN_DATE")
                        LBL_EMP_ID.Text = dt.Rows(0)("EMP_ID")
                        LBL_ELIGIBILITY_TXT.Text = dt.Rows(0)("ELIGIBILITY_TXT")
                    End If
                End If

                Gridbind_StuDetails()

                'if employee has records show in criteria page - remove  "Eligibility not set or verified for the employee"
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
                pParms(0).Value = 1
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
                pParms(1).Value = Session("sBsuid")
                pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
                pParms(2).Value = Employee_ID

                Dim ds2 As New DataSet
                'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                'CommandType.Text, "SELECT '0' AS BSU_ID, '#---SELECT---#' AS BSU_NAME UNION  SELECT BSU_ID , BSU_NAME  FROM   OASIS..BUSINESSUNIT_M WHERE  BSU_SHORTNAME  NOT IN ('GVS','XXX','GEP','OOF','MHN','GDF','LLN','ADJ','GSA','PRS') AND ISNULL(BSU_bGEMSSCHOOL ,0)=1 AND BSU_NAME NOT LIKE 'DREAM%'  AND ISNULL(BSU_Bschool ,0)=1 ORDER BY BSU_NAME ASC")
                ds2 = GET_SCHOOL_TYPES(1, Session("sBsuid"))

                DDLBsuTyp1.DataSource = ds2
                DDLBsuTyp1.DataTextField = "BSG_DESCR"
                DDLBsuTyp1.DataValueField = "BSG_ID"
                DDLBsuTyp1.DataBind()

                DDLBsuTyp2.DataSource = ds2
                DDLBsuTyp2.DataTextField = "BSG_DESCR"
                DDLBsuTyp2.DataValueField = "BSG_ID"
                DDLBsuTyp2.DataBind()

                DDLBsuTyp3.DataSource = ds2
                DDLBsuTyp3.DataTextField = "BSG_DESCR"
                DDLBsuTyp3.DataValueField = "BSG_ID"
                DDLBsuTyp3.DataBind()



                Dim ds As New DataSet
                'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
                'CommandType.Text, "SELECT '0' AS BSU_ID, '#---SELECT---#' AS BSU_NAME UNION  SELECT BSU_ID , BSU_NAME  FROM   OASIS..BUSINESSUNIT_M WHERE  BSU_SHORTNAME  NOT IN ('GVS','XXX','GEP','OOF','MHN','GDF','LLN','ADJ','GSA','PRS') AND ISNULL(BSU_bGEMSSCHOOL ,0)=1 AND BSU_NAME NOT LIKE 'DREAM%'  AND ISNULL(BSU_Bschool ,0)=1 ORDER BY BSU_NAME ASC")
                ds = GET_SCHOOLS(1, Session("sBsuid"), 1, Employee_ID)

                DDLBsu1.DataSource = ds
                DDLBsu1.DataTextField = "BSU_NAME"
                DDLBsu1.DataValueField = "BSU_ID"
                DDLBsu1.DataBind()
                DDLBsu1.SelectedValue = CurBsUnit

                DDLBsu2.DataSource = ds
                DDLBsu2.DataTextField = "BSU_NAME"
                DDLBsu2.DataValueField = "BSU_ID"
                DDLBsu2.DataBind()
                DDLBsu2.SelectedValue = CurBsUnit

                DDLBsu3.DataSource = ds
                DDLBsu3.DataTextField = "BSU_NAME"
                DDLBsu3.DataValueField = "BSU_ID"
                DDLBsu3.DataBind()
                DDLBsu3.SelectedValue = CurBsUnit


                Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
                CommandType.StoredProcedure, "[OASIS].DBO.GET_ELIGIBILITY_LIST", pParms)
                If Not dsData.Tables(0) Is Nothing Then
                    If dsData.Tables(0).Rows.Count > 0 Then
                        If dsData.Tables(0).Rows(0)("ISELIGIBILITY_EXISTS") = "1" Then
                            ELGIB_TXT.Visible = False
                            btn_approve.Visible = True
                            btn_reject.Visible = True
                        Else
                            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showConfirmModal();", True)
                            btn_approve.Visible = False
                            btn_reject.Visible = False
                            LBL_ELIGIBILITY_TXT.Visible = False
                        End If
                    End If
                End If


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
            End Try

        Else

        End If
    End Sub
    Sub Gridbind_StuDetails()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))      
            Dim Employee_ID As String = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EMP_BSU_ID", Session("sBsuid"))
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Employee_ID)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[SP_CHECK_TFC_ELIGIBILTY_OF_EMP_CONCESSIONS]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                repInfo.Visible = True
            Else
                repInfo.Visible = False
            End If

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception
            repInfo.DataBind()

        End Try
    End Sub

    Protected Sub Typ1_Changed(sender As Object, e As EventArgs)
        Dim BsuTyp1 As Integer = Integer.Parse(DDLBsuTyp1.SelectedItem.Value)
        If BsuTyp1 > 0 Then
            Dim CurBsUnit As String = Session("sBsuid")
            Dim ds As New DataSet
            ds = GET_SCHOOLS(1, Session("sBsuid"), BsuTyp1, "")
            DDLBsu1.DataSource = ds
            DDLBsu1.DataTextField = "BSU_NAME"
            DDLBsu1.DataValueField = "BSU_ID"
            DDLBsu1.DataBind()
            'DDLBsu1.SelectedValue = CurBsUnit
            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
        End If
    End Sub

    Protected Sub Typ2_Changed(sender As Object, e As EventArgs)
        Dim BsuTyp2 As Integer = Integer.Parse(DDLBsuTyp2.SelectedItem.Value)
        If BsuTyp2 > 0 Then
            Dim CurBsUnit As String = Session("sBsuid")
            Dim ds As New DataSet
            ds = GET_SCHOOLS(1, Session("sBsuid"), BsuTyp2, "")
            DDLBsu2.DataSource = ds
            DDLBsu2.DataTextField = "BSU_NAME"
            DDLBsu2.DataValueField = "BSU_ID"
            DDLBsu2.DataBind()
            'DDLBsu1.SelectedValue = CurBsUnit
            'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)

            If rad1.Checked = True Then
                rad1.Checked = True
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_And(" + CStr(2) + ");", True)
            ElseIf rad2.Checked = True Then
                rad2.Checked = True
            End If

            If rad2.Checked = True Then
                'If dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 1 Then
                '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or1(" + CStr(rowcount) + ");", True)
                '    chkElgSelect1.Checked = True
                '    chkElgSelect2.Checked = False
                '    chkElgSelect3.Checked = False
                'ElseIf dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 2 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or2(" + CStr(2) + ");", True)
                chkElgSelect2.Checked = True
                chkElgSelect1.Checked = False
                chkElgSelect3.Checked = False
                'ElseIf dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 3 Then
                '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or3(" + CStr(rowcount) + ");", True)
                '    chkElgSelect3.Checked = True
                '    chkElgSelect2.Checked = False
                '    chkElgSelect1.Checked = False
                'End If
            End If


        End If
    End Sub

    Protected Sub Typ3_Changed(sender As Object, e As EventArgs)
        Dim BsuTyp3 As Integer = Integer.Parse(DDLBsuTyp3.SelectedItem.Value)
        If BsuTyp3 > 0 Then
            Dim CurBsUnit As String = Session("sBsuid")
            Dim ds As New DataSet
            ds = GET_SCHOOLS(1, Session("sBsuid"), BsuTyp3, "")
            DDLBsu3.DataSource = ds
            DDLBsu3.DataTextField = "BSU_NAME"
            DDLBsu3.DataValueField = "BSU_ID"
            DDLBsu3.DataBind()
            'DDLBsu1.SelectedValue = CurBsUnit
            'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)


            If rad1.Checked = True Then
                rad1.Checked = True
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_And(" + CStr(3) + ");", True)
            ElseIf rad2.Checked = True Then
                rad2.Checked = True
            End If

            If rad2.Checked = True Then
                'If dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 1 Then
                '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or1(" + CStr(rowcount) + ");", True)
                '    chkElgSelect1.Checked = True
                '    chkElgSelect2.Checked = False
                '    chkElgSelect3.Checked = False
                'ElseIf dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 2 Then
                'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or2(" + CStr(2) + ");", True)
                'chkElgSelect2.Checked = True
                'chkElgSelect1.Checked = False
                'chkElgSelect3.Checked = False
                'ElseIf dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 3 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or3(" + CStr(3) + ");", True)
                chkElgSelect3.Checked = True
                chkElgSelect2.Checked = False
                chkElgSelect1.Checked = False
                'End If
            End If

        End If
    End Sub
    Public Shared Function GET_SCHOOLS(ByVal OPTIONS As Integer, ByVal BSU_ID As String, ByVal BSU_BSG_ID As Integer, Optional ByVal EMP_ID As String = "") As DataSet
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_BSG_ID", SqlDbType.Int)
        pParms(2).Value = BSU_BSG_ID
        pParms(3) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
        pParms(3).Value = EMP_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[OASIS].DBO.GET_SCHOOLS", pParms)
        If Not dsData Is Nothing Then
            Return dsData
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GET_SCHOOL_TYPES(ByVal OPTIONS As Integer, ByVal BSU_ID As String) As DataSet
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = BSU_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[OASIS].DBO.GET_SCHOOL_TYPES", pParms)
        If Not dsData Is Nothing Then
            Return dsData
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GET_EMPLOYEE_DETAILS(ByVal OPTIONS As Integer, ByVal BSU_ID As String, Optional ByVal EMP_ID As String = "") As DataTable
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = EMP_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[OASIS].DBO.GET_ESS_EMPLOYEE_DETAILS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GET_ESS_CONCESSION_UPLOAD_DOCUMENT(ByVal OPTIONS As Integer, ByVal SCD_ID As String) As DataTable
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@SCD_ID", SqlDbType.Int)
        pParms(1).Value = SCD_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[OASIS_FEES].FEES.GET_ESS_CONCESSION_UPLOAD_DOCUMENT", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub btn_approve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_approve.Click
        Dim count As Integer = 0
        'check eligibility set or not
        'Dim Employee_ID As String = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
        'Dim pParms(3) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        'pParms(0).Value = 1
        'pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        'pParms(1).Value = Session("sBsuid")
        'pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
        'pParms(2).Value = Employee_ID


        'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
        '  CommandType.StoredProcedure, "[OASIS].DBO.GET_ELIGIBILITY_LIST", pParms)
        'If Not dsData.Tables(0) Is Nothing Then
        '    If dsData.Tables(0).Rows.Count > 0 Then
        '        If dsData.Tables(0).Rows(0)("ISELIGIBILITY_EXISTS") = "0" Then
        '            lblError.Text = "Please set eligibility Criteria!!"
        '            Exit Sub
        '        End If
        '    End If
        'End If



        For Each repitm As RepeaterItem In repInfo.Items
            Dim hdnSTU_ID As HiddenField = CType(repitm.FindControl("hdnSTU_ID"), HiddenField)
            Dim hdnSCR_ID As HiddenField = CType(repitm.FindControl("hdnSCR_ID"), HiddenField)
            Dim hdnSCD_ID As HiddenField = CType(repitm.FindControl("hdnSCD_ID"), HiddenField)
            Dim hdnVERIFY_MISMATCH As HiddenField = CType(repitm.FindControl("hdnVERIFY_MISMATCH"), HiddenField)
            Dim chkSelect As CheckBox = CType(repitm.FindControl("chkSelect"), CheckBox)
            Dim chkagree As CheckBox = CType(repitm.FindControl("chkagree"), CheckBox)
            Dim lbl_eligible_amt As Label = CType(repitm.FindControl("lbl_eligible_amt"), Label)
            Dim txt_approved_amt As TextBox = CType(repitm.FindControl("txt_approved_amt"), TextBox)
            Dim lbl_actual_amt As Label = CType(repitm.FindControl("lbl_actual_amt"), Label)
            Dim TXT_NOTES As TextBox = CType(repitm.FindControl("TXT_NOTES"), TextBox)
            Dim CHK_APPLY_ELG As System.Web.UI.HtmlControls.HtmlInputCheckBox = CType(repitm.FindControl("CHK_APPLY_ELG"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
            Dim CHK_APPLY_FIN As System.Web.UI.HtmlControls.HtmlInputCheckBox = CType(repitm.FindControl("CHK_APPLY_FIN"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
            Dim hfFilePath As HiddenField = CType(repitm.FindControl("hfFilePath"), HiddenField)
            Dim UploadFileName As String = hfFilePath.Value

            Dim hdnCONC_SCHOOL_BSU_ID As HiddenField = CType(repitm.FindControl("hdnCONC_SCHOOL_BSU_ID"), HiddenField)
            Dim hdnCONC_ELIGIBLE_STARTDT As HiddenField = CType(repitm.FindControl("hdnCONC_ELIGIBLE_STARTDT"), HiddenField)
            Dim hdnELG_ID As HiddenField = CType(repitm.FindControl("hdnELG_ID"), HiddenField)
            Dim hdnACD_ENDDT As HiddenField = CType(repitm.FindControl("hdnACD_ENDDT"), HiddenField)

            If CHK_APPLY_FIN.Checked Then

                Dim SCD_UPLOADED_DOC_PATH As String = ""
                Dim dt As DataTable = GET_ESS_CONCESSION_UPLOAD_DOCUMENT(1, hdnSCD_ID.Value)
                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then
                        SCD_UPLOADED_DOC_PATH = dt.Rows(0)("SCD_UPLOADED_DOC_PATH")

                        If SCD_UPLOADED_DOC_PATH = "" Then
                            usrMessageBar.ShowNotification("Please upload supporting documents", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    End If
                End If
                'If UploadFileName = "" Then
                '    usrMessageBar.ShowNotification("Please upload supporting documents", UserControls_usrMessageBar.WarningType.Danger)
                '    Exit Sub
                'End If

                If TXT_NOTES.Text = "" Then
                    usrMessageBar.ShowNotification("Please add notes", UserControls_usrMessageBar.WarningType.Danger)
                    Exit Sub
                End If
            End If

            'If UploadFileName <> "" Then
            '    Dim serverpath As String = WebConfigurationManager.AppSettings("ESSConcessionUploads").ToString
            '    Dim val As String = hdnSCD_ID.Value
            '    Directory.CreateDirectory(serverpath + val + "/Attachments")
            '    Dim filen As String()
            '    Dim FileName As String = TXT_UPLOAD_DOC.PostedFile.FileName
            '    FileName = FileName.Replace(" ", "_")
            '    filen = FileName.Split("\")
            '    FileName = filen(filen.Length - 1)
            '    TXT_UPLOAD_DOC.SaveAs(serverpath + val + "/Attachments/" + FileName)
            '    UploadFileName = serverpath + val + "/Attachments/" + FileName
            'End If


            Try
                Dim retval As String = ""
                If chkSelect.Checked Then

                    If hdnVERIFY_MISMATCH.Value = 0 Then
                        If Not chkagree.Checked Then

                            'lblError.Text = "Please check Agree checkbox "
                            usrMessageBar.ShowNotification("Please confirm and verify the staff and student details", UserControls_usrMessageBar.WarningType.Danger)
                            count = -1
                            Exit Sub
                        End If
                    End If

                    Dim elg_amt As Double, elg_appr_amt As Double, actual_amt As Double, fin_appr_status As String = "P"
                    elg_amt = lbl_eligible_amt.Text
                    elg_appr_amt = txt_approved_amt.Text
                    actual_amt = lbl_actual_amt.Text

                    If elg_appr_amt <= elg_amt Then
                        fin_appr_status = "P" 'AA- changed to P on 18-03-2020 by majo verified by shakeel
                    Else
                        fin_appr_status = "P"
                    End If

                    If CHK_APPLY_ELG.Checked Then
                        fin_appr_status = "P"
                    End If
                    If CHK_APPLY_FIN.Checked Then
                        fin_appr_status = "P"
                    End If

                    If elg_amt < actual_amt Then
                        If Not CHK_APPLY_ELG.Checked And Not CHK_APPLY_FIN.Checked Then
                            usrMessageBar.ShowNotification("Concession request for the selected student is beyond eligibility. Please select any one of the option mentioned to approve.", UserControls_usrMessageBar.WarningType.Danger)
                            Exit Sub
                        End If
                    Else
                        CHK_APPLY_ELG.Checked = True
                        CHK_APPLY_FIN.Checked = False
                    End If

                    If CHK_APPLY_FIN.Checked Then
                        elg_appr_amt = actual_amt
                    End If


                    If elg_appr_amt <= 0 Then
                        usrMessageBar.ShowNotification("Concession/Eligible amount is Zero or less than Zero.Not Applicable!!!", UserControls_usrMessageBar.WarningType.Success)
                        Exit Sub
                    End If

                    'If elg_appr_amt > actual_amt Then
                    '    usrMessageBar.ShowNotification("Concession amount is greater than actual fee amount.", UserControls_usrMessageBar.WarningType.Success)
                    '    Exit Sub
                    'End If
                    retval = EOS_EmployeeDependant.ApproveConcession(hdnSCR_ID.Value, hdnSCD_ID.Value, hdnSTU_ID.Value, LBL_EMP_ID.Text, HttpContext.Current.Session("sUsr_id"), "A", fin_appr_status, actual_amt, elg_amt, elg_appr_amt, TXT_NOTES.Text,
                                                                     UploadFileName, CHK_APPLY_ELG.Checked, CHK_APPLY_FIN.Checked, hdnCONC_SCHOOL_BSU_ID.Value,
                                                                     hdnCONC_ELIGIBLE_STARTDT.Value, hdnELG_ID.Value, hdnACD_ENDDT.Value)
                    If (retval = "0" Or retval = "") Then
                        'lblError.Text = "Successfully approved Fee Concession"
                        usrMessageBar.ShowNotification("Successfully approved Fee Concession", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        'lblError.Text = "Error occured!!"
                        usrMessageBar.ShowNotification("Error occured!!", UserControls_usrMessageBar.WarningType.Danger)

                    End If
                    count = count + 1

                End If

            Catch ex As Exception
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try


        Next

        Gridbind_StuDetails()
        If count = 0 Then
            'lblError.Text = "Please select student!!"
            usrMessageBar.ShowNotification("Please select at least one student!!", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub


    Protected Sub repeater_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        'Dim fo As AsyncFileUpload = CType(e.Item.FindControl("TXT_UPLOAD_DOC"), AsyncFileUpload)
        'Dim trigger As PostBackTrigger = New PostBackTrigger()
        '    trigger.ControlID = fo.ClientID
        '    Me.idpanel.Triggers.Add(trigger)
        '    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(fo)
    End Sub

    Protected Sub FileUploadComplete(ByVal sender As Object, ByVal e As EventArgs)

        For Each repitm As RepeaterItem In repInfo.Items
            Dim TXT_UPLOAD_DOC As FileUpload = CType(repitm.FindControl("TXT_UPLOAD_DOC"), FileUpload)
            Dim hdnSCD_ID As HiddenField = CType(repitm.FindControl("hdnSCD_ID"), HiddenField)
            Dim UploadFileName As String = System.IO.Path.GetFileName(TXT_UPLOAD_DOC.FileName)
            If (TXT_UPLOAD_DOC.HasFile) Then
                'Dim strFileType As String = System.IO.Path.GetExtension(TXT_UPLOAD_DOC.FileName).ToString().ToLower()
                'If strFileType <> ".xls" Then
                '    Exit Sub
                'End If
                'filename = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString().Replace(".", "") & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & filename
                'TXT_UPLOAD_DOC.SaveAs(filename)
                If UploadFileName <> "" Then
                    Dim serverpath As String = WebConfigurationManager.AppSettings("IRDocumentPath").ToString
                    Dim val As String = hdnSCD_ID.Value
                    Directory.CreateDirectory(serverpath + val + "/Attachments")
                    Dim filen As String()
                    Dim FileName As String = TXT_UPLOAD_DOC.PostedFile.FileName
                    FileName = FileName.Replace(" ", "_")
                    filen = FileName.Split("\")
                    FileName = filen(filen.Length - 1)
                    TXT_UPLOAD_DOC.SaveAs(serverpath + val + "/Attachments/" + FileName)
                    UploadFileName = serverpath + val + "/Attachments/" + FileName
                End If


            End If
        Next

    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        Dim SCR_ID As String = hfFilePathHID.Value
        Dim SCD_ID As String = hfFilePathID.Value

        Dim retval As String = ""
        retval = EOS_EmployeeDependant.UpdateUploadFilePath(SCR_ID, SCD_ID, "", "", HttpContext.Current.Session("sUsr_id"))
        Gridbind_StuDetails()
        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "hideModal3();", True)

        hfpostback.Value = "deletedone"
    End Sub
    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim SCD_ID As String = hfFilePathID.Value
        Dim SCD_UPLOADED_DOC_PATH As String = ""

        Dim dt As DataTable = GET_ESS_CONCESSION_UPLOAD_DOCUMENT(1, SCD_ID)
        Dim serverpath As String = WebConfigurationManager.AppSettings("IRDocumentPathVirtual").ToString

        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                SCD_UPLOADED_DOC_PATH = serverpath + "ESSApprovals/" + SCD_ID + "/" + dt.Rows(0)("SCD_UPLOADED_DOC_NAME")

                If SCD_UPLOADED_DOC_PATH <> "" Then
                    Try
                        Dim bytes() As Byte = File.ReadAllBytes(SCD_UPLOADED_DOC_PATH)
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Response.Clear()
                        Response.ClearHeaders()
                        Response.ContentType = "application/octect-stream"
                        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(SCD_UPLOADED_DOC_PATH))
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()
                    Catch ex As Exception
                        usrMessageBar.ShowNotification("Error in downloading the document!!", UserControls_usrMessageBar.WarningType.Danger)
                    End Try

                Else
                    usrMessageBar.ShowNotification("No approval document to download!!", UserControls_usrMessageBar.WarningType.Information)
                End If


            End If
        End If
    End Sub
    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If hfpostback.Value = "uploadfiles" Then

            For Each repitm As RepeaterItem In repInfo.Items
                Dim TXT_UPLOAD_DOC As FileUpload = CType(repitm.FindControl("TXT_UPLOAD_DOC"), FileUpload)
                Dim hdnSCR_ID As HiddenField = CType(repitm.FindControl("hdnSCR_ID"), HiddenField)
                Dim hdnSCD_ID As HiddenField = CType(repitm.FindControl("hdnSCD_ID"), HiddenField)

                Dim UploadFileName As String = System.IO.Path.GetFileName(TXT_UPLOAD_DOC.FileName)
                'UploadFileName = UploadFileName.Trim
                Dim UploadFilePath As String = ""
                If (TXT_UPLOAD_DOC.HasFile) Then
                    'Dim strFileType As String = System.IO.Path.GetExtension(TXT_UPLOAD_DOC.FileName).ToString().ToLower()
                    'If strFileType <> ".xls" Then
                    '    Exit Sub
                    'End If
                    'filename = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "\OnlineExcel\" & Session("sUsr_name").ToString().Replace(".", "") & Date.Now.ToString("dd-MMM-yyyy") & "-" & Date.Now.ToShortTimeString().Replace(":", "@") & filename
                    'TXT_UPLOAD_DOC.SaveAs(filename)
                    If UploadFileName <> "" Then
                        Dim serverpath As String = WebConfigurationManager.AppSettings("IRDocumentPath").ToString
                        Dim val As String = hdnSCD_ID.Value
                        Directory.CreateDirectory(serverpath + "ESSApprovals/" + val)
                        Dim filen As String()
                        Dim FileName As String = TXT_UPLOAD_DOC.PostedFile.FileName
                        FileName = FileName.Replace(" ", "_")
                        filen = FileName.Split("\")
                        FileName = filen(filen.Length - 1)
                        TXT_UPLOAD_DOC.SaveAs(serverpath + "ESSApprovals/" + val + "/" + FileName)
                        UploadFilePath = serverpath + "ESSApprovals/" + val + "/" + FileName

                        Dim retval As String = ""
                        retval = EOS_EmployeeDependant.UpdateUploadFilePath(hdnSCR_ID.Value, hdnSCD_ID.Value, UploadFilePath, FileName, HttpContext.Current.Session("sUsr_id"))


                    End If


                End If
            Next
            Gridbind_StuDetails()
            hfpostback.Value = "uploaddone"
        End If
    End Sub

    Protected Sub btn_reject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_reject.Click
        Dim count As Integer = 0
        For Each repitm As RepeaterItem In repInfo.Items

            Dim hdnSTU_ID As HiddenField = CType(repitm.FindControl("hdnSTU_ID"), HiddenField)
            Dim hdnSCR_ID As HiddenField = CType(repitm.FindControl("hdnSCR_ID"), HiddenField)
            Dim hdnSCD_ID As HiddenField = CType(repitm.FindControl("hdnSCD_ID"), HiddenField)
            Dim chkSelect As CheckBox = CType(repitm.FindControl("chkSelect"), CheckBox)
            Dim lbl_eligible_amt As Label = CType(repitm.FindControl("lbl_eligible_amt"), Label)
            Dim txt_approved_amt As TextBox = CType(repitm.FindControl("txt_approved_amt"), TextBox)
            Dim lbl_actual_amt As Label = CType(repitm.FindControl("lbl_actual_amt"), Label)
            Dim TXT_NOTES As TextBox = CType(repitm.FindControl("TXT_NOTES"), TextBox)
            Dim CHK_APPLY_ELG As System.Web.UI.HtmlControls.HtmlInputCheckBox = CType(repitm.FindControl("CHK_APPLY_ELG"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
            Dim CHK_APPLY_FIN As System.Web.UI.HtmlControls.HtmlInputCheckBox = CType(repitm.FindControl("CHK_APPLY_FIN"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
            Dim hfFilePath As HiddenField = CType(repitm.FindControl("hfFilePath"), HiddenField)
            Dim UploadFileName As String = hfFilePath.Value

            Dim hdnCONC_SCHOOL_BSU_ID As HiddenField = CType(repitm.FindControl("hdnCONC_SCHOOL_BSU_ID"), HiddenField)
            Dim hdnCONC_ELIGIBLE_STARTDT As HiddenField = CType(repitm.FindControl("hdnCONC_ELIGIBLE_STARTDT"), HiddenField)
            Dim hdnELG_ID As HiddenField = CType(repitm.FindControl("hdnELG_ID"), HiddenField)
            Dim hdnACD_ENDDT As HiddenField = CType(repitm.FindControl("hdnACD_ENDDT"), HiddenField)

            'If CHK_APPLY_FIN.Checked Then
            '    If UploadFileName = "" Then
            '        usrMessageBar.ShowNotification("Please upload supporting documents", UserControls_usrMessageBar.WarningType.Danger)
            '        Exit Sub
            '    End If

            '    If TXT_NOTES.Text = "" Then
            '        usrMessageBar.ShowNotification("Please add notes", UserControls_usrMessageBar.WarningType.Danger)
            '        Exit Sub
            '    End If
            'End If

            'If UploadFileName <> "" Then
            '    Dim serverpath As String = WebConfigurationManager.AppSettings("ESSConcessionUploads").ToString
            '    Dim val As String = hdnSCD_ID.Value
            '    Directory.CreateDirectory(serverpath + val + "/Attachments")
            '    Dim filen As String()
            '    Dim FileName As String = TXT_UPLOAD_DOC.PostedFile.FileName
            '    FileName = FileName.Replace(" ", "_")
            '    filen = FileName.Split("\")
            '    FileName = filen(filen.Length - 1)
            '    TXT_UPLOAD_DOC.SaveAs(serverpath + val + "/Attachments/" + FileName)
            '    UploadFileName = serverpath + val + "/Attachments/" + FileName
            'End If

            Try
                Dim retval As String = ""
                If chkSelect.Checked Then

                    Dim elg_amt As Double, elg_appr_amt As Double, actual_amt As Double, fin_appr_status As String = "P"
                    elg_amt = lbl_eligible_amt.Text
                    elg_appr_amt = txt_approved_amt.Text
                    actual_amt = lbl_actual_amt.Text

                    retval = EOS_EmployeeDependant.ApproveConcession(hdnSCR_ID.Value, hdnSCD_ID.Value, hdnSTU_ID.Value, LBL_EMP_ID.Text, HttpContext.Current.Session("sUsr_id"), "R", "P", actual_amt, elg_amt, elg_appr_amt, TXT_NOTES.Text, UploadFileName,
                                                                     CHK_APPLY_ELG.Checked, CHK_APPLY_FIN.Checked, hdnCONC_SCHOOL_BSU_ID.Value,
                                                                     hdnCONC_ELIGIBLE_STARTDT.Value, hdnELG_ID.Value, hdnACD_ENDDT.Value)
                    If (retval = "0" Or retval = "") Then
                        'lblError.Text = "Successfully approved Fee Concession"
                        usrMessageBar.ShowNotification("Rejected Fee Concession Request", UserControls_usrMessageBar.WarningType.Success)
                    Else
                        'lblError.Text = "Error occured!!"
                        usrMessageBar.ShowNotification("Error occured!!", UserControls_usrMessageBar.WarningType.Danger)
                    End If
                    count = count + 1
                End If

            Catch ex As Exception
                'lblError.Text = ex.Message
                usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Next
        If count = 0 Then
            'lblError.Text = "Please select student!!"
            usrMessageBar.ShowNotification("Please select at least one student!!", UserControls_usrMessageBar.WarningType.Danger)
        End If
    End Sub

    'Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
    '    'myModal.Visible = False
    'End Sub

    'Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
    '    'myModal.Visible = False
    'End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnUpdate.Click

        'If (IsNumeric(txtConcPercentage1.Text) = False) Or (IsNumeric(txtConcPercentage2.Text) = False) Or (IsNumeric(txtConcPercentage3.Text) = False) Then
        '    lblError.Text = "Percentage should be numeric"
        '    Exit Sub
        'Else
        '    Dim perc1 As Integer = IsNumeric(txtConcPercentage3.Text)
        '    If perc1 > 100 Then
        '        lblError.Text = "Percentage value cannot be greater than 100 "
        '        Exit Sub
        '    End If
        '    Dim perc2 As Integer = IsNumeric(txtConcPercentage3.Text)
        '    If perc2 > 100 Then
        '        lblError.Text = "Percentage value cannot be greater than 100 "
        '        Exit Sub
        '    End If
        '    Dim perc3 As Integer = IsNumeric(txtConcPercentage3.Text)
        '    If perc3 > 100 Then
        '        lblError.Text = "Percentage value cannot be greater than 100 "
        '        Exit Sub
        '    End If
        'End If

        If ddlStud1.SelectedValue >= 1 Or ddlStud2.SelectedValue >= 1 Or ddlStud3.SelectedValue >= 1 Then

            If ddlStud1.SelectedValue >= 1 Then

                If DDLBsu1.SelectedValue = "0" Then
                    Err_Label.Text = "Select School "
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                    Exit Sub
                End If

                If (IsNumeric(txtConcPercentage1.Text) = False) Then
                    Err_Label.Text = "Percentage should be numeric"
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                    Exit Sub
                End If

                'If txtDate1.Text = "" Then
                '    Err_Label.Text = "Select Date "
                '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                '    Exit Sub
                'End If
            Else
                'Err_Label.Text = "Select Count "
                'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                'Exit Sub
            End If

            If ddlStud2.SelectedValue >= 1 Then

                If DDLBsu2.SelectedValue = "0" Then
                    Err_Label.Text = "Select School "
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                    Exit Sub
                End If

                If (IsNumeric(txtConcPercentage2.Text) = False) Then
                    Err_Label.Text = "Percentage should be numeric"
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                    Exit Sub
                End If

                'If txtDate2.Text = "" Then
                '    Err_Label.Text = "Select Date "
                '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                '    Exit Sub
                'End If
            Else
                'Err_Label.Text = "Select Count "
                'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                'Exit Sub
            End If

            If ddlStud3.SelectedValue >= 1 Then

                If DDLBsu3.SelectedValue = "0" Then
                    Err_Label.Text = "Select School "
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                    Exit Sub
                End If

                If (IsNumeric(txtConcPercentage3.Text) = False) Then
                    Err_Label.Text = "Percentage should be numeric"
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                    Exit Sub
                End If

                'If txtDate3.Text = "" Then
                '    Err_Label.Text = "Select Date "
                '    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                '    Exit Sub
                'End If
            Else
                'Err_Label.Text = "Select Count "
                'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                'Exit Sub
            End If
        Else
            Err_Label.Text = "Select Count "
            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
            Exit Sub
        End If
        update_eligibility()

        'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "hideSetELG_Text();", True)
        'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "#myModal1", "$('body').removeClass('modal-open');$('.modal-backdrop').remove();", true);
        'Gridbind_StuDetails()

    End Sub

    Protected Sub update_eligibility()
        Dim Employee_ID As String = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
        Dim obj As EOS_EmployeeDependant.EligibilityCriteria = New EOS_EmployeeDependant.EligibilityCriteria
        obj.ELG_ID = hdn_ELG_ID.Value
        obj.ELG_ELIGIBLE_AndOr_OPTION = IIf(rad1.Checked, "And", "Or")
        obj.ELG_ELIGIBLE_CHILD_COUNT_1 = ddlStud1.SelectedValue
        obj.ELG_ELIGIBLE_CHILD_COUNT_2 = ddlStud2.SelectedValue
        obj.ELG_ELIGIBLE_CHILD_COUNT_3 = ddlStud3.SelectedValue
        obj.ELG_ELIGIBLE_PERCENTAGE_1 = IIf(txtConcPercentage1.Text = "", "0", txtConcPercentage1.Text)
        obj.ELG_ELIGIBLE_PERCENTAGE_2 = IIf(txtConcPercentage2.Text = "", "0", txtConcPercentage2.Text)
        obj.ELG_ELIGIBLE_PERCENTAGE_3 = IIf(txtConcPercentage3.Text = "", "0", txtConcPercentage3.Text)
        obj.ELG_ELIGIBLE_SCHOOL_TYP_ID_1 = DDLBsuTyp1.SelectedValue
        obj.ELG_ELIGIBLE_SCHOOL_TYP_ID_2 = DDLBsuTyp2.SelectedValue
        obj.ELG_ELIGIBLE_SCHOOL_TYP_ID_3 = DDLBsuTyp3.SelectedValue
        obj.ELG_ELIGIBLE_SCHOOL_ID_1 = DDLBsu1.SelectedValue
        obj.ELG_ELIGIBLE_SCHOOL_ID_2 = DDLBsu2.SelectedValue
        obj.ELG_ELIGIBLE_SCHOOL_ID_3 = DDLBsu3.SelectedValue
        obj.ELG_ELIGIBLE_VALID_TILL_1 = IIf(txtDate1.Text = "", "1/1/1900", txtDate1.Text)
        obj.ELG_ELIGIBLE_VALID_TILL_2 = IIf(txtDate2.Text = "", "1/1/1900", txtDate2.Text)
        obj.ELG_ELIGIBLE_VALID_TILL_3 = IIf(txtDate3.Text = "", "1/1/1900", txtDate3.Text)

        If chkElgSelect1.Checked Then
            obj.ELG_ACTIVE_OPTION = 1
        ElseIf chkElgSelect2.Checked Then
            obj.ELG_ACTIVE_OPTION = 2
        ElseIf chkElgSelect3.Checked Then
            obj.ELG_ACTIVE_OPTION = 3
        End If

        obj.USER = Session("sUsr_id")
        obj.ELG_bVerified = 1
        obj.ELG_BSU_ID = Session("sBsuid")
        obj.ELG_EMP_ID = Employee_ID
        Try
            Dim retval As String = ""

            retval = EOS_EmployeeDependant.UpdateEligibility(obj)

            If (retval = "0" Or retval = "") Then
                'lblError.Text = "Successfully updated eligibility"
                usrMessageBar.ShowNotification("Successfully updated eligibility", UserControls_usrMessageBar.WarningType.Success)
                ELGIB_TXT.Visible = False
                btn_approve.Visible = True
                btn_reject.Visible = True

                Dim dt As DataTable = GET_EMPLOYEE_DETAILS(1, Session("sBsuid"), Employee_ID)
                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then
                        LBL_EMP_NAME.Text = dt.Rows(0)("EMPLOYEE_NAME")
                        LBL_DPT.Text = dt.Rows(0)("DPT_DESCR")
                        LBL_EMP_NO.Text = dt.Rows(0)("EMPNO")
                        LBL_EMP_DOJ.Text = dt.Rows(0)("JOIN_DATE")
                        LBL_EMP_DESIG.Text = dt.Rows(0)("DESIGNATION")
                        imgEmpImage.ImageUrl = dt.Rows(0)("IMG_URL")
                        LBLGRPDOJ.Text = dt.Rows(0)("GRP_JOIN_DATE")
                        LBL_EMP_ID.Text = dt.Rows(0)("EMP_ID")
                        LBL_ELIGIBILITY_TXT.Text = dt.Rows(0)("ELIGIBILITY_TXT")
                    End If
                End If

                'ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "hideModal2(" + Request.QueryString("viewid") + ");", True)
                ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "hideModal2();", True)
            Else
                'lblError.Text = "Error occured!!"
                usrMessageBar.ShowNotification("Error occured!!", UserControls_usrMessageBar.WarningType.Danger)
            End If


        Catch ex As Exception
            'lblError.Text = ex.Message
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub



    Protected Sub btnEligibility_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnEligibility.Click
        Try
            'myModal.Visible = True
            ' data-toggle="modal" data-target="#myModal"

            Dim rowcount As Integer = 0


            'if employee has records show in criteria page
            Dim Employee_ID As String = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
            pParms(0).Value = 2
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
            pParms(1).Value = Session("sBsuid")
            pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
            pParms(2).Value = Employee_ID


            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
              CommandType.StoredProcedure, "[OASIS].DBO.GET_ELIGIBILITY_LIST", pParms)
            If Not dsData.Tables(0) Is Nothing Then
                If dsData.Tables(0).Rows.Count > 0 Then



                    ddlStud1.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_CHILD_COUNT_1")
                    ddlStud2.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_CHILD_COUNT_2")
                    ddlStud3.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_CHILD_COUNT_3")
                    DDLBsuTyp1.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_SCHOOL_TYP_ID_1")
                    DDLBsuTyp2.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_SCHOOL_TYP_ID_2")
                    DDLBsuTyp3.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_SCHOOL_TYP_ID_3")


                    Dim ds1 As New DataSet
                    ds1 = GET_SCHOOLS(1, Session("sBsuid"), DDLBsuTyp1.SelectedValue, Employee_ID)
                    DDLBsu1.DataSource = ds1
                    DDLBsu1.DataTextField = "BSU_NAME"
                    DDLBsu1.DataValueField = "BSU_ID"
                    DDLBsu1.DataBind()

                    Dim ds2 As New DataSet
                    ds2 = GET_SCHOOLS(1, Session("sBsuid"), DDLBsuTyp2.SelectedValue, Employee_ID)
                    DDLBsu2.DataSource = ds2
                    DDLBsu2.DataTextField = "BSU_NAME"
                    DDLBsu2.DataValueField = "BSU_ID"
                    DDLBsu2.DataBind()

                    Dim ds3 As New DataSet
                    ds3 = GET_SCHOOLS(1, Session("sBsuid"), DDLBsuTyp3.SelectedValue, Employee_ID)
                    DDLBsu3.DataSource = ds3
                    DDLBsu3.DataTextField = "BSU_NAME"
                    DDLBsu3.DataValueField = "BSU_ID"
                    DDLBsu3.DataBind()


                    Try
                        DDLBsu1.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_SCHOOL_ID_1")
                        DDLBsu2.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_SCHOOL_ID_2")
                        DDLBsu3.SelectedValue = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_SCHOOL_ID_3")
                    Catch ex As Exception

                    End Try



                    txtConcPercentage1.Text = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_PERCENTAGE_1")
                    txtConcPercentage2.Text = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_PERCENTAGE_2")
                    txtConcPercentage3.Text = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_PERCENTAGE_3")

                    If ddlStud1.SelectedValue > 0 Then
                        rowcount = 1
                    End If
                    If ddlStud2.SelectedValue > 0 Then
                        rowcount = 2
                    End If
                    If ddlStud3.SelectedValue > 0 Then
                        rowcount = 3
                    End If

                    hdn_ELG_ID.Value = dsData.Tables(0).Rows(0)("ELG_ID")
                    If dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_AndOr_OPTION") = "And" Then
                        rad1.Checked = True
                        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_And(" + CStr(rowcount) + ");", True)
                    ElseIf dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_AndOr_OPTION") = "Or" Then
                        rad2.Checked = True
                    End If

                    If dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_AndOr_OPTION") = "Or" Then
                        If dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 1 Then
                            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or1(" + CStr(rowcount) + ");", True)
                            chkElgSelect1.Checked = True
                            chkElgSelect2.Checked = False
                            chkElgSelect3.Checked = False
                        ElseIf dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 2 Then
                            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or2(" + CStr(rowcount) + ");", True)
                            chkElgSelect2.Checked = True
                            chkElgSelect1.Checked = False
                            chkElgSelect3.Checked = False
                        ElseIf dsData.Tables(0).Rows(0)("ELG_ACTIVE_OPTION") = 3 Then
                            ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showFullModal_Or3(" + CStr(rowcount) + ");", True)
                            chkElgSelect3.Checked = True
                            chkElgSelect2.Checked = False
                            chkElgSelect1.Checked = False
                        End If
                    End If

                    'txtDate1.Text = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_VALID_TILL_1")
                    'txtDate2.Text = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_VALID_TILL_2")
                    'txtDate3.Text = dsData.Tables(0).Rows(0)("ELG_ELIGIBLE_VALID_TILL_3")

                    txtDate1.Text = IIf(dsData.Tables(0).Rows(0)("ELG_DATE1") = "1/1/1900", "", dsData.Tables(0).Rows(0)("ELG_DATE1"))
                    txtDate2.Text = IIf(dsData.Tables(0).Rows(0)("ELG_DATE2") = "1/1/1900", "", dsData.Tables(0).Rows(0)("ELG_DATE2"))
                    txtDate3.Text = IIf(dsData.Tables(0).Rows(0)("ELG_DATE3") = "1/1/1900", "", dsData.Tables(0).Rows(0)("ELG_DATE3"))
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "Pop", "showModal();", True)
                End If
            End If


        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar.ShowNotification("Request could not be processed", UserControls_usrMessageBar.WarningType.Danger)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub



    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function AddNotes(ByVal SCR_ID As String, ByVal SCD_ID As String, ByVal Notes As String) As String
        Dim returnstring As String = ""
        Try
            Dim retval As String = ""
            retval = EOS_EmployeeDependant.UpdateNotes(1, SCR_ID, SCD_ID, Notes, HttpContext.Current.Session("sUsr_id"))
            If (retval = "0" Or retval = "") Then

                returnstring = "0" + "||" + "Notes Updated Successfully"
            Else
                returnstring = retval + "||" + "Error occured!!"

            End If
        Catch ex As Exception
            returnstring = "1000" + "||" + ex.Message

        End Try
        Return returnstring

    End Function
End Class
