﻿
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Collections.Generic

Partial Class Exit_workflow
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
        'scriptManager.RegisterAsyncPostBackControl(Me.gvEmployeeList)

        Try


            If Not Page.IsPostBack Then
                'ViewState("EXT_WF_ID") = "15"
                'ViewState("EXT_NWF_ID") = "614"
                'ViewState("EMP_ID") = "6220"

                ' ViewState("EXT_WF_LST_ID") = "157"

                If Request.QueryString("EXT_WF_LST_ID") IsNot Nothing Then
                    ViewState("EXT_WF_LST_ID") = 0
                    ViewState("EXT_WF_ID") = 0
                    If Request.QueryString("EXT_WF_LST_ID").Length > 1 Then

                        ViewState("EXT_WF_LST_ID") = Encr_decrData.Decrypt(Request.QueryString("EXT_WF_LST_ID").Replace(" ", "+"))
                    End If

                    If ViewState("EXT_WF_LST_ID") = "0" And Request.QueryString("EXT_WF_ID") IsNot Nothing Then
                        ViewState("EXT_WF_ID") = Encr_decrData.Decrypt(Request.QueryString("EXT_WF_ID").Replace(" ", "+"))
                        tblDepartmentMain.Visible = False
                    Else
                        tblDepartmentMain.Visible = True
                    End If
                End If
                BindData()

                If ViewState("EXT_WF_LST_ID") > 0 Then
                    If (ViewState("STAFF_DPT_ID") = ViewState("EMP_DPT_ID")) Then
                        tblDefault.Visible = True
                    End If
                    AddControls()
                Else
                    btnSave.Visible = False
                    btnSubmit.Visible = False
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
        End Try
    End Sub

    Private Sub BindData()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EXT_WF_LST_ID", ViewState("EXT_WF_LST_ID"))
            param(1) = New SqlClient.SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DETAILS_BY_LST_ID]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                lblEMP_NAME.Text = DT.Rows(0)("EMP_NAME")
                lblDesignation.Text = DT.Rows(0)("DES_DESCR")
                lblEmpDepartment.Text = DT.Rows(0)("DPT_DESCR")
                lblDOJ.Text = DT.Rows(0)("EMP_JOINDT")

                lblEOS.Text = DT.Rows(0)("EMP_RESGDT")
                lblLWD.Text = DT.Rows(0)("EMP_LASTATTDT")
                lblFPhone.Text = DT.Rows(0)("EMP_MOBILE")
                lblEmail.Text = DT.Rows(0)("EMP_EMAIL")
                ViewState("STAFF_EMP_ID") = DT.Rows(0)("STAFF_EMP_ID")
                ViewState("STAFF_BSU_ID") = DT.Rows(0)("STAFF_BSU_ID")


                If ViewState("EXT_WF_LST_ID") > 0 Then

                    lblDepartment.Text = DT.Rows(0)("STAFF_DEPT") + " clearance:"


                    ViewState("STAFF_DPT_ID") = DT.Rows(0)("STAFF_DPT_ID")
                    ViewState("EMP_DPT_ID") = DT.Rows(0)("EMP_DPT_ID")

                    ViewState("IsAlreadySubmit") = DT.Rows(0)("IsAlreadySubmit")

                    If DT.Rows(0)("Is_AlreadySubmitted_By_Other_DPT") > 0 Then
                        ViewState("IsAlreadySubmit") = 1
                        ViewState("EXT_WF_LST_ID") = DT.Rows(0)("LST_ID_SUBMITTED_BY_OTHER_DPT")
                    End If

                    If (ViewState("IsAlreadySubmit") > 0) And DT.Rows(0)("Is_AlreadySubmitted_By_Other_DPT") <= 0 Then

                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        lblDateTime.Text = DT.Rows(0)("SubmittedOn")
                        lblLineManager.Text = DT.Rows(0)("STAFF_NAME")
                        lblDefaultDate.Text = DT.Rows(0)("SubmittedOn")
                        lblDepartmentName.Text = DT.Rows(0)("STAFF_DEPT") + ":(" + DT.Rows(0)("STAFF_NAME") + ") "
                    ElseIf DT.Rows(0)("Is_AlreadySubmitted_By_Other_DPT") > 0 Then

                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        lblDateTime.Text = DT.Rows(0)("SubmittedByStaff_DATE")
                        lblLineManager.Text = DT.Rows(0)("SubmittedByStaff")
                        'SubmittedByStaff_DATE()
                        lblDefaultDate.Text = DT.Rows(0)("SubmittedByStaff_DATE")
                        lblDepartmentName.Text = DT.Rows(0)("STAFF_DEPT") + ":(" + DT.Rows(0)("SubmittedByStaff") + ") "
                    Else
                        lblLineManager.Text = DT.Rows(0)("STAFF_NAME")
                        lblDateTime.Text = DateTime.Now.ToString("dd-MMM-yyyy")
                        lblDefaultDate.Text = DateTime.Now.ToString("dd-MMM-yyyy")
                        lblDepartmentName.Text = DT.Rows(0)("STAFF_DEPT") + ":(" + DT.Rows(0)("STAFF_NAME") + ") "
                    End If
                Else
                    AddControlsForHR(DT)
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BindData")
        End Try

    End Sub
    Private Sub SetDefaultTable()
        'Set Default Table tblDefault
        tblDefault.Visible = True
        Dim ds As DataSet
        Dim DT As DataTable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DEFAULT_BY_WF_ID]", param)

        If ds.Tables(0).Rows.Count > 0 Then
            DT = ds.Tables(0)
            For Each rw As DataRow In DT.Rows

                If rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_1" Then
                    CheckBox999999_1.Checked = Convert.ToBoolean(rw("Control_Value"))
                    CheckBox999999_1.Enabled = False
                ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_2" Then
                    CheckBox999999_2.Checked = Convert.ToBoolean(rw("Control_Value"))
                    CheckBox999999_2.Enabled = False
                ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_3" Then
                    CheckBox999999_3.Checked = Convert.ToBoolean(rw("Control_Value"))
                    CheckBox999999_3.Enabled = False
                ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_4" Then
                    CheckBox999999_4.Checked = Convert.ToBoolean(rw("Control_Value"))
                    CheckBox999999_4.Enabled = False
                ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "TextBox999999_1" Then
                    TextBox999999_1.Text = Convert.ToString(rw("Control_Value"))
                    TextBox999999_1.Enabled = False

                End If
            Next

            lblLineManager.Text = Convert.ToString(DT.Rows(0)("STAFF_NAME"))
            lblDefaultDate.Text = Convert.ToString(DT.Rows(0)("SUBMITTED_ON"))
        Else
            Dim ds1 As DataSet
            Dim DT1 As DataTable
            Dim param1(2) As SqlClient.SqlParameter
            param1(0) = New SqlClient.SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))
            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_LINE_MANAGER_BY_WF_ID]", param1)
            If ds1.Tables(0).Rows.Count > 0 Then
                DT1 = ds1.Tables(0)
                lblLineManager.Text = Convert.ToString(DT1.Rows(0)("STAFF_NAME"))
                lblDefaultDate.Text = ""
            End If

        End If
    End Sub
    Private Sub AddControlsForHR(ByVal staffDT As DataTable)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            'Set default fields
            SetDefaultTable()
            tblAllDepartments.Visible = True

            Dim dictionary As New Dictionary(Of Integer, String)


            For Each row As DataRow In staffDT.Rows

                If dictionary.ContainsKey(row("STAFF_DPT_ID")) = False Then
                    dictionary.Add(row("STAFF_DPT_ID"), row("STAFF_DEPT"))

                    Dim lst_Id As Integer = 0
                    If (row("IsAlreadySubmit") > 0) Then
                        lst_Id = row("EXT_WF_LST_ID")
                    ElseIf (row("Is_AlreadySubmitted_By_Other_DPT") > 0) Then
                        lst_Id = row("LST_ID_SUBMITTED_BY_OTHER_DPT")
                    End If


                    Dim ds As DataSet
                    Dim DT As DataTable
                    Dim param(4) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@DEPT_ID", row("STAFF_DPT_ID"))
                    param(1) = New SqlClient.SqlParameter("@EXT_WF_LST_ID", lst_Id)
                    param(2) = New SqlClient.SqlParameter("@IsDefaultDPT", False)
                    param(3) = New SqlClient.SqlParameter("@EXT_WF_ID", ViewState("EXT_WF_ID"))

                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DEPTS_BY_DEPT_ID_HR]", param)

                    If ds.Tables(0).Rows.Count > 0 Then
                        DT = ds.Tables(0)
                        Dim tblRowH As New TableRow()
                        Dim tbleCellH As New TableCell()

                        For Each rw As DataRow In DT.Rows
                            If rw("DEPT_DSR").ToString.Trim.Length > 1 Then
                                tbleCellH.Text = rw("DEPT_DSR").ToString + " Clearance"
                                tbleCellH.BackColor = Drawing.Color.LightGray
                                tbleCellH.ColumnSpan = 2
                                Exit For
                            End If
                        Next
                        tblRowH.Cells.Add(tbleCellH)
                        tblAllDepartments.Rows.Add(tblRowH)

                        Dim cnt As Integer = 0
                        For Each rw As DataRow In DT.Rows


                            If rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_1" Or rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_2" Or
                               rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_3" Or rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_4" Or
                               rw("EXT_WF_DEPT_CONTROL_NAME") = "TextBox999999_1" Then
                            Else
                                Dim tblRow As New TableRow()
                                Dim tbleCell As New TableCell()
                                If rw("EXT_WF_DEPT_CONTROL_TYPE") = "CheckBox" Then
                                    Dim chk As New CheckBox()
                                    chk.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                                    chk.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                                    If rw("Control_Value") IsNot Nothing Then
                                        chk.Enabled = False
                                        chk.Checked = Convert.ToBoolean(rw("Control_Value"))
                                    End If
                                    If cnt = 0 Then
                                        tbleCell.CssClass = "borderTop borderLeft borderRight"
                                        cnt = 1
                                    Else

                                        tbleCell.CssClass = "borderLeft borderRight"
                                    End If
                                    tbleCell.ColumnSpan = 2
                                    tbleCell.Controls.Add(chk)


                                ElseIf rw("EXT_WF_DEPT_CONTROL_TYPE") = "TextBox" Then
                                    Dim tbleCellText As New TableCell()
                                    tbleCellText.Width = "200"
                                    tbleCellText.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")


                                    Dim txt As New TextBox()
                                    txt.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                                    txt.TextMode = TextBoxMode.MultiLine
                                    txt.Rows = "1"
                                    If rw("Control_Value") IsNot Nothing Then
                                        txt.Enabled = False
                                        txt.Text = Convert.ToString(rw("Control_Value"))
                                    End If
                                    If cnt = 0 Then
                                        tbleCellText.CssClass = "borderTop borderLeft"
                                        tbleCell.CssClass = "borderTop borderRight"
                                        cnt = 1
                                    Else

                                        tbleCellText.CssClass = "borderLeft"
                                        tbleCell.CssClass = "borderRight"
                                    End If

                                    tblRow.Cells.Add(tbleCellText)
                                    'tbleCell.ColumnSpan =2
                                    tbleCell.Controls.Add(txt)
                                ElseIf rw("EXT_WF_DEPT_CONTROL_TYPE") = "FileUpload" Then
                                    Dim tbleCellText As New TableCell()
                                    tbleCellText.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                                    tbleCellText.Width = "200"
                                    tblRow.Cells.Add(tbleCellText)

                                    If rw("filename") = "" Then
                                        'Dim fl As New FileUpload()
                                        'fl.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                                        tbleCell.Text = "No Document"
                                    Else

                                        Dim hyplink As New HyperLink()
                                        hyplink.ID = "hypDownload"
                                        hyplink.Text = rw("filename")
                                        hyplink.Attributes.Add("onclick", "return downloadDoc(" + Convert.ToString(rw("EXT_WF_LST_ID")) + ");")
                                        hyplink.Style.Add("cursor", "pointer")
                                        hyplink.Style.Add("color", "blue")
                                        hyplink.Style.Add("text-decoration", "underline")
                                        tbleCell.Controls.Add(hyplink)
                                    End If
                                    tbleCellText.CssClass = "borderLeft"
                                    tbleCell.CssClass = "borderRight"
                                End If

                                tblRow.Cells.Add(tbleCell)
                                tblAllDepartments.Rows.Add(tblRow)
                            End If
                            
                        Next
                       
                        For Each rw As DataRow In DT.Rows
                            If rw("SUBMITTED_BY_STAFF_NAME").ToString.Trim.Length > 1 Then
                                Dim tblRowF As New TableRow()
                                Dim tbleCellF As New TableCell()
                                Dim tblRowF1 As New TableRow()
                                Dim tbleCellF1 As New TableCell()
                                If rw("SUBMITTED_ON").ToString.Trim.Length = 0 Then
                                    tbleCellF.Text = "Staff Name : " + rw("SUBMITTED_BY_STAFF_NAME").ToString
                                    tbleCellF1.Text = "Date : " + rw("SUBMITTED_ON").ToString
                                Else
                                    tbleCellF.Text = "Submitted By: " + rw("SUBMITTED_BY_STAFF_NAME").ToString
                                    tbleCellF1.Text = "Submitted On : " + rw("SUBMITTED_ON").ToString
                                End If
                                tblRowF.CssClass = "borderLeft borderRight"
                                tblRowF1.CssClass = "borderBottom borderLeft borderRight"
                                tbleCellF.ColumnSpan = 2
                                tbleCellF1.ColumnSpan = 2

                                tblRowF.Cells.Add(tbleCellF)
                                tblAllDepartments.Rows.Add(tblRowF) 
                                tblRowF1.Cells.Add(tbleCellF1)
                                tblAllDepartments.Rows.Add(tblRowF1)

                                Dim tblRowEmpty As New TableRow()
                                Dim tbleCellEmpty As New TableCell()
                                tblRowEmpty.Cells.Add(tbleCellEmpty)
                                tblAllDepartments.Rows.Add(tblRowEmpty)
                                Exit For
                            End If
                        Next
                      
                    End If

                End If
            Next 
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AddControls")
        End Try


    End Sub
     
    Private Sub AddControls()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable
            Dim lst_Id As Integer = 0
            If (ViewState("IsAlreadySubmit") > 0) Then
                lst_Id = ViewState("EXT_WF_LST_ID")
            End If
            Dim IsDefaultDPT As Boolean = False
            If ViewState("STAFF_DPT_ID") = ViewState("EMP_DPT_ID") And ViewState("IsAlreadySubmit") = 0 Then
                IsDefaultDPT = True
            End If

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_ID", ViewState("STAFF_DPT_ID"))
            param(1) = New SqlClient.SqlParameter("@EXT_WF_LST_ID", ViewState("EXT_WF_LST_ID"))
            param(2) = New SqlClient.SqlParameter("@IsDefaultDPT", IsDefaultDPT)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DEPTS_BY_DEPT_ID]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                'Session("DT") = DT
                For Each rw As DataRow In DT.Rows
                    If rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_1" Or rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_2" Or
                            rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_3" Or rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_4" Or
                            rw("EXT_WF_DEPT_CONTROL_NAME") = "TextBox999999_1" Then

                        If rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_1" Then
                            CheckBox999999_1.Checked = Convert.ToBoolean(rw("Control_Value"))
                            If ViewState("IsAlreadySubmit") = 1 Then
                                CheckBox999999_1.Enabled = False
                            End If
                        ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_2" Then
                            CheckBox999999_2.Checked = Convert.ToBoolean(rw("Control_Value"))
                            If ViewState("IsAlreadySubmit") = 1 Then
                                CheckBox999999_2.Enabled = False
                            End If
                        ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_3" Then
                            CheckBox999999_3.Checked = Convert.ToBoolean(rw("Control_Value"))
                            If ViewState("IsAlreadySubmit") = 1 Then
                                CheckBox999999_3.Enabled = False
                            End If
                        ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "CheckBox999999_4" Then
                            CheckBox999999_4.Checked = Convert.ToBoolean(rw("Control_Value"))
                            If ViewState("IsAlreadySubmit") = 1 Then
                                CheckBox999999_4.Enabled = False
                            End If
                        ElseIf rw("EXT_WF_DEPT_CONTROL_NAME") = "TextBox999999_1" Then
                            TextBox999999_1.Text = Convert.ToString(rw("Control_Value"))
                            If ViewState("IsAlreadySubmit") = 1 Then
                                TextBox999999_1.Enabled = False
                            End If
                        End If



                    Else


                        Dim tblRow As New TableRow()
                        Dim tbleCell As New TableCell()
                        If rw("EXT_WF_DEPT_CONTROL_TYPE") = "CheckBox" Then

                            Dim chk As New CheckBox()
                            chk.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                            chk.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                            If ViewState("IsAlreadySubmit") = 1 Then
                                chk.Enabled = False
                                'chk.Checked = Convert.ToBoolean(rw("Control_Value"))
                            End If
                            chk.Checked = Convert.ToBoolean(rw("Control_Value"))
                            tbleCell.ColumnSpan = 2
                            tbleCell.Controls.Add(chk)


                        ElseIf rw("EXT_WF_DEPT_CONTROL_TYPE") = "TextBox" Then
                            Dim tbleCellText As New TableCell()
                            tbleCellText.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                            tbleCellText.Width = "200"
                            tblRow.Cells.Add(tbleCellText)


                            Dim txt As New TextBox()
                            txt.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                            txt.TextMode = TextBoxMode.MultiLine
                            txt.Rows = "1"
                            If ViewState("IsAlreadySubmit") = 1 Then
                                txt.Enabled = False
                                'txt.Text = Convert.ToString(rw("Control_Value"))
                            End If
                            txt.Text = Convert.ToString(rw("Control_Value"))
                            tbleCell.Controls.Add(txt)
                        ElseIf rw("EXT_WF_DEPT_CONTROL_TYPE") = "FileUpload" Then

                            If ViewState("IsAlreadySubmit") <> 1 And rw("filename") = "" Then
                                'Dim fl As New FileUpload()
                                'fl.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                                'tbleCell.Controls.Add(fl) 
                                trfileUpload1.Visible = True
                            Else
                                trfileUpload1.Visible = False
                                Dim tbleCellText As New TableCell()
                                tbleCellText.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                                tbleCellText.Width = "200"
                                tblRow.Cells.Add(tbleCellText)

                                Dim hyplink As New HyperLink()
                                hyplink.ID = "hypDownload"
                                hyplink.Text = rw("filename")
                                hyplink.Attributes.Add("onclick", "return downloadDoc(" + Convert.ToString(ViewState("EXT_WF_LST_ID")) + ");")
                                hyplink.Style.Add("cursor", "pointer")
                                hyplink.Style.Add("color", "blue")
                                hyplink.Style.Add("text-decoration", "underline")
                                tbleCell.Controls.Add(hyplink)
                            End If


                        End If
                        tblRow.Cells.Add(tbleCell)
                        tblDepartment.Rows.Add(tblRow)
                    End If
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AddControls")
        End Try


    End Sub

    Private Sub AddControlsDefault()

        Try

            tblDefault.Visible = True
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As DataSet
            Dim DT As DataTable

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@DEPT_ID", "999999")
            param(1) = New SqlClient.SqlParameter("@EXT_WF_LST_ID", 0)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DEPTS_BY_DEPT_ID]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                DT = ds.Tables(0)
                'Session("DT") = DT
                For Each rw As DataRow In DT.Rows
                    Dim tblRow As New TableRow()
                    Dim tbleCell As New TableCell()
                    If rw("EXT_WF_DEPT_CONTROL_TYPE") = "CheckBox" Then
                        Dim chk As New CheckBox()
                        chk.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                        chk.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                        If ViewState("IsAlreadySubmit") = 1 Then
                            chk.Enabled = False
                            chk.Checked = Convert.ToBoolean(rw("Control_Value"))
                        End If
                        tbleCell.Controls.Add(chk)
                    ElseIf rw("EXT_WF_DEPT_CONTROL_TYPE") = "TextBox" Then
                        Dim tbleCellText As New TableCell()
                        tbleCellText.Text = rw("EXT_WF_DEPT_CONTROL_TEXT")
                        tblRow.Cells.Add(tbleCellText)

                        Dim txt As New TextBox()
                        txt.ID = rw("EXT_WF_DEPT_CONTROL_NAME")
                        txt.TextMode = TextBoxMode.MultiLine
                        txt.Rows = "4"
                        If ViewState("IsAlreadySubmit") = 1 Then
                            txt.Enabled = False
                            txt.Text = Convert.ToString(rw("Control_Value"))
                        End If
                        tbleCell.Controls.Add(txt)
                    End If
                    tblRow.Cells.Add(tbleCell)
                    tblDefault.Rows.Add(tblRow)
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AddControls")
        End Try


    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click


        Dim allData As String = hdnValues.Value.Trim("###")

        Dim errormsg As String = String.Empty
        If callTransaction(errormsg, allData, False) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
        Else
            ' tblCategory.Visible = False
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:green;padding:5pt;background-color:white;'>Data Saved Successfully.</div>"
            'ViewState("IsAlreadySubmit") = 1
            'btnadd.Visible = False 
            AddControls()
        End If

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click


        Dim allData As String = hdnValues.Value.Trim("###")

        Dim errormsg As String = String.Empty
        If callTransaction(errormsg, allData, True) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:white;'>" & errormsg & "</div>"
        Else
            ' tblCategory.Visible = False
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:green;padding:5pt;background-color:white;'>Data Saved Successfully.</div>"
            ViewState("IsAlreadySubmit") = 1
            btnSubmit.Visible = False
            btnSave.Visible = False
            AddControls()
        End If

    End Sub


    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim DT As DataTable
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@DEPT_ID", ViewState("STAFF_DPT_ID"))
        param(1) = New SqlClient.SqlParameter("@EXT_WF_LST_ID", hdnLSTID.Value)
        param(2) = New SqlClient.SqlParameter("@IsDefaultDPT", False)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[HR].[GET_WORKFLOW_DEPTS_BY_DEPT_ID]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            DT = ds.Tables(0)
            For Each rw As DataRow In DT.Rows
                If rw("EXT_WF_DEPT_CONTROL_TYPE") = "FileUpload" Then
                    Dim bytes As Byte()

                    Dim fileName As String, contentType As String

                    bytes = DirectCast(rw("File_Contents"), Byte())
                    '  bytes = System.Text.Encoding.ASCII.GetBytes(rw("CONTROL_VALUE"))
                    contentType = rw("FileType")
                    fileName = rw("FileName")
                    If fileName <> "" Then


                        Response.Clear()
                        Response.Buffer = True
                        Response.Charset = ""
                        Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Response.ContentType = contentType
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName)
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()
                    End If
                    Exit For
                End If
            Next
        End If
        hdnLSTID.Value = ""
    End Sub



    Private Function callTransaction(ByRef errormsg As String, ByVal allData As String, ByVal IsSubmitted As Boolean) As Integer


        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim paramDel(3) As SqlParameter
                paramDel(0) = New SqlParameter("@EXT_WF_LST_ID", ViewState("EXT_WF_LST_ID"))
                paramDel(1) = New SqlParameter("@DEPT_ID", ViewState("STAFF_DPT_ID"))
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[DELETE_EXIT_WORKFLOW_DEPT]", paramDel)


                Dim data As String() = allData.Split(New Char() {"###"}, StringSplitOptions.RemoveEmptyEntries)
                For Each cntrl As String In data
                    If cntrl.Trim.Length > 0 Then

                        Dim str As String() = cntrl.Split(New Char() {"$$$"}, StringSplitOptions.RemoveEmptyEntries)
                        Dim _controlName As String = str(0)
                        Dim _controlValue As String = ""
                        Dim _controlType As String = ""
                        If _controlName.ToLower.Contains("checkbox") Then
                            _controlType = "CheckBox"
                            _controlValue = str(1)
                        Else
                            _controlType = "TextBox"
                            If str.Length > 1 Then
                                _controlValue = str(1)
                            End If
                        End If

                        If (ViewState("STAFF_DPT_ID") = ViewState("EMP_DPT_ID")) Or (
                            _controlName.ToLower.Contains("CheckBox999999_1") = False And
                            _controlName.ToLower.Contains("CheckBox999999_2") = False And
                            _controlName.ToLower.Contains("CheckBox999999_3") = False And
                            _controlName.ToLower.Contains("CheckBox999999_4") = False And
                             _controlName.ToLower.Contains("TextBox999999_1") = False
                            ) Then



                            Dim paramLST(13) As SqlParameter
                            paramLST(0) = New SqlParameter("@ID", 0)
                            paramLST(0).Direction = ParameterDirection.InputOutput
                            paramLST(1) = New SqlParameter("@EXT_WF_LST_ID", ViewState("EXT_WF_LST_ID"))
                            paramLST(2) = New SqlParameter("@CONTROL_NAME", _controlName)
                            paramLST(3) = New SqlParameter("@IP", GetCurrentIP())
                            paramLST(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                            paramLST(4).Direction = ParameterDirection.ReturnValue
                            paramLST(5) = New SqlParameter("@CONTROL_VALUE", _controlValue)
                            paramLST(6) = New SqlParameter("@CONTROL_TYPE", _controlType)
                            paramLST(7) = New SqlParameter("@STAFF_EMP_ID", ViewState("STAFF_EMP_ID"))
                            paramLST(8) = New SqlParameter("@STAFF_BSU_ID", ViewState("STAFF_BSU_ID"))
                            paramLST(9) = New SqlParameter("@FileName", "")
                            paramLST(10) = New SqlParameter("@FileType", "")
                            paramLST(11) = New SqlParameter("@File_Contents", New Byte())
                            paramLST(12) = New SqlParameter("@IsSubmitted", IsSubmitted)


                            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SAVE_EXIT_WORKFLOW_DEPT]", paramLST)
                            Dim ReturnFlagLST As Integer = paramLST(4).Value
                            If ReturnFlagLST <> 0 Then
                                callTransaction = "1"
                                errormsg = "Error occured while processing info !!!"
                            End If
                        End If
                    End If
                Next

                'Save file content

                If fileUpload1.HasFile Then
                    Dim paramLST(14) As SqlParameter
                    paramLST(0) = New SqlParameter("@ID", 0)
                    paramLST(0).Direction = ParameterDirection.InputOutput
                    paramLST(1) = New SqlParameter("@EXT_WF_LST_ID", ViewState("EXT_WF_LST_ID"))
                    paramLST(2) = New SqlParameter("@CONTROL_NAME", "FileUpload1")
                    paramLST(3) = New SqlParameter("@IP", GetCurrentIP())
                    paramLST(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                    paramLST(4).Direction = ParameterDirection.ReturnValue
                    paramLST(5) = New SqlParameter("@CONTROL_VALUE", "")
                    paramLST(6) = New SqlParameter("@CONTROL_TYPE", "FileUpload")
                    paramLST(7) = New SqlParameter("@STAFF_EMP_ID", ViewState("STAFF_EMP_ID"))
                    paramLST(8) = New SqlParameter("@STAFF_BSU_ID", ViewState("STAFF_BSU_ID"))
                    paramLST(9) = New SqlParameter("@FileName", fileUpload1.FileName)
                    paramLST(10) = New SqlParameter("@FileType", fileUpload1.PostedFile.ContentType)
                    Dim imageBytes(fileUpload1.PostedFile.InputStream.Length) As Byte
                    fileUpload1.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                    paramLST(11) = New SqlParameter("@File_Contents", imageBytes)
                    paramLST(12) = New SqlParameter("@IsSubmitted", IsSubmitted)
                    SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[HR].[SAVE_EXIT_WORKFLOW_DEPT]", paramLST)
                    Dim ReturnFlagLST As Integer = paramLST(4).Value
                    If ReturnFlagLST <> 0 Then
                        callTransaction = "1"
                        errormsg = "Error occured while processing info !!!"
                    End If

                End If

                callTransaction = "0"

            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()

                End If
            End Try

        End Using

    End Function
    Private Function GetCurrentIP() As String
        Dim ipList As String = Request.ServerVariables("HTTP_X_FORWARDED_FOR")

        If Not String.IsNullOrEmpty(ipList) Then
            Return ipList.Split(","c)(0)
        End If

        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
End Class


