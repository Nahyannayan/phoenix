﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Exit_workflow_Add.aspx.vb" Inherits="Exit_workflow_Add" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <title></title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

     <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }
    </style>

    <script type="text/javascript">



        $(document).ready(function () {
            $('[id$=chkHeader]').click(function () {
                $("[id$='chkChild']").attr('checked', this.checked);
            });
            $('[id$=chkHeaderEmp]').click(function () {
                $("[id$='chkChild']").attr('checked', this.checked);
            });

        });
        function fancyClose() {
            window.close();
            if (parent.$.fancybox) {

                setTimeout(function () { parent.$.fancybox.close(); }, 500);
                return false;
            }
        }


        function checkEmployeeSelection() {
            if ($("#ddlEmployee").val() == "0") {
                alert("Please select the employees");
                return false;
            }
            return true;
        }
        function AddRow()
        { 
            var rowNum = $("#gvPDC").length;
            var email ="em";
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + rowNum + "</td><td>" + email + "</td></tr>";
            $("#gvPDC").append(markup);

            return false;
        }
         
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptmgr" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
         
        <div class="card mb-3">
            <div class="card-header letter-space">
                <i class="fa fa-book mr-3"></i>INITIATE EXIT PROCESS
            </div>
            <div class="card-body">
                <div class="table-responsive">


                    <table id="tblCategory" cellspacing="2" cellpadding="2" width="100%">
                        <tr id="trLabelError">
                            <td align="left" class="matters" valign="bottom">
                                <div id="lblError" runat="server">
                                </div>
                            </td>
                        </tr>
                        <tr id="trAdd">
                            <td>
                                <div style="text-align: right; margin: 5px; font-weight: bold;">
                                </div>
                            </td>
                        </tr>
                        <tr id="trGridv" runat="server">
                            <td align="center">
                                <table width="100%">
                                    <tr class="subheader_img">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right">Employee Name
                                        </td>
                                        <td align="left" style="padding-left: 10px">
                                            <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="true"></asp:DropDownList>
                                          <strong>  <asp:Label ID="LblEmplyeename" runat="server" Visible="false"></asp:Label></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="btnSaveToDraft" Text="Save in Draft" runat="server" CssClass="button" OnClientClick="return checkEmployeeSelection();" />
                                            <asp:Button ID="btnAddPDC" Text="Submit & Send Email" runat="server" CssClass="button" OnClientClick="return checkEmployeeSelection();" />
                                            <input type="button" class="button" id="btnCancel1" title="CLOSE" value="CLOSE" onclick="fancyClose()" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdMoreEmployee"  visible="false" colspan="2" align="left" runat="server"> 
                                            <table width="70%">
                                                <tr>
                                                    <td width="35%">
                                                         Add More Employee in workflow :
                                                    </td>
                                                    <td width="40%">
                                                        <asp:DropDownList ID="ddlMoreEmployee" runat="server" Width="95%"></asp:DropDownList>  
                                                    </td>
                                                    <td width="25%">
                                                        <asp:Button ID="frameAddMoreEmployee" runat="server" class="button" Text="Add"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table> 
                                        </td>
                                    </tr>
                                
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="gvPDC" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                                PageSize="25" Width="100%" OnPageIndexChanging="gvPDC_PageIndexChanging" DataKeyNames="EMP_ID"
                                                OnRowDeleting="OnRowDeleting">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("EMP_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblEXT_WF_USR_ID" runat="server" Text='<%# Bind("EXT_WF_USR_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblEXT_NWF_ID" runat="server" Text='<%# Bind("EXT_NWF_ID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                           EMPLOYEE NAME
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFullName" runat="server" Text='<%# bind("EMP_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Department" ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDPT_DESCR" runat="server" Text='<%# Bind("DPT_DESCR")%>'></asp:Label>
                                                            <asp:Label ID="lblDPT_ID" runat="server" Text='<%# Bind("DPT_ID")%>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Include In Email Alert Only" ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIncludeInWorkFlow" runat="server" Checked='<%# IIf(IsDBNull(Eval("EXT_WF_INCLUDE_IN_WORKFLOW")),"False",Eval("EXT_WF_INCLUDE_IN_WORKFLOW")) %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Is HR User" ItemStyle-Width="20px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEXT_WF_IS_HR" runat="server" Checked='<%# IIf(IsDBNull(Eval("EXT_WF_IS_HR")),"False",Eval("EXT_WF_IS_HR")) %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Link" />


                                                </Columns>

                                                <RowStyle CssClass="griditem" Height="25px" />
                                                <SelectedRowStyle BackColor="Aqua" />
                                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                            <asp:HiddenField ID="hdnSelected" runat="server" />
                                        </td>
                                    </tr>


                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            </div>
             

         
       
    </form>
</body>
</html>

