Imports Oasis_Administrator
Imports System.Data
Imports Oasis_User
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Partial Class OASIS_HR_ErrorView_ErrorView
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            GridBind()
        End If

    End Sub


    Public Sub GridBind()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim sqlquery = "select * from ERROR_LOG order by id desc "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)
        GridView1.DataSource = ds
        GridView1.DataBind()

    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

End Class
