
Partial Class OasisMaster
    Inherits System.Web.UI.MasterPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            LabelHeader.Text = "GEMS Careers Online Application Form"
        End If

    End Sub
    Public Property HeaderText() As String
        Get
            Return LabelHeader.Text
        End Get
        Set(ByVal value As String)
            LabelHeader.Text = value
        End Set
    End Property
End Class

