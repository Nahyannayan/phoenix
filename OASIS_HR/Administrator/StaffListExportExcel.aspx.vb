Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports GemBox.Spreadsheet
Imports system
Imports System.Data.SqlTypes
Partial Class OASIS_HR_Administrator_StaffListExportExcel
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        BindFilterControlsMain()
        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                hfPartB.Value = "display"
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add" '"Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    chkCond1.Attributes.Add("name", chkCond1.ClientID)
                    chkCond1.Attributes.Add("onclick", "javascript:Cond1(event);")
                    ddlCond1.Attributes.Add("onChange", "Select_Cond()")
                    'calling pageright class to get the access rights
                    '   Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    ElseIf ViewState("datamode") = "add" Then

                    End If

                    Call bindFilter_Cond_C()
                    'txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    'txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    h_Selected_menu_1.Value = "LI__../../Images/operations/like.gif"

                    Session("PartC_CER") = CreateDataTable()
                    Session("PartC_CER").Rows.Clear()
                    ViewState("id") = 1
                    gridbind()
                    'imgCalendar1.Visible = False
                    'imgCalendar2.Visible = False
                    'txtCond2.Visible = False
                    'Dim str_search As String = String.Empty
                    'Dim str_Sid_search() As String
                    'str_Sid_search = h_Selected_menu_1.Value.Split("__")
                    'str_search = str_Sid_search(0)
                    ' Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                    ' "<script language=javascript>initialLoad_Cond();</script>")
                    h_BSUID.Value += "||" + Session("sBsuid").ToString.Replace(",", "||")
                    FillBSUNames(h_BSUID.Value)
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
        set_Menu_Img()
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTC", _
        "<script language=javascript>initialLoad_Cond();</script>")
        FillBSUNames(h_BSUID.Value)
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        Try
            If p_imgsrc <> "" Then
                mnu_1_img.Src = p_imgsrc
            End If
            Return mnu_1_img.ClientID
        Catch ex As Exception
            Return ""
        End Try

        Return ""
    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            'If Not tvGrade Is Nothing Then
            '    DisableAutoPostBack(tvGrade.Nodes)
            'End If
        End If


    End Sub
    Sub DisableAutoPostBack(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub ClearAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = False

            If tn.ChildNodes.Count > 0 Then
                ClearAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub SelectAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = True

            If tn.ChildNodes.Count > 0 Then
                SelectAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub


    Sub checkboxchecking(ByVal cbl As CheckBoxList, ByVal cb As CheckBox)
        Dim i As Integer = 0
        Dim cblcount As Integer = 0
        For i = 0 To cbl.Items.Count - 1
            If (cbl.Items(i).Selected = True) Then
                cblcount = cblcount + 1
            End If
        Next
        If (cblcount = cbl.Items.Count) Then
            cb.Checked = True
        Else
            cb.Checked = False
        End If
    End Sub
    Public Sub BindFilterControlsMain()
        ''Try
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select * from STF_REPORT_FILTER_TABLE where STF_FT_SHOW_FLAG='8' ORDER BY STF_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataStaffFilterMain.DataSource = ds
        DataStaffFilterMain.DataBind()

        Dim x As Integer = 0
        For Each row As DataListItem In DataStaffFilterMain.Items
            Dim STF_FT_ID = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            Dim dc As DataSet
            Dim ListDynamic As New CheckBoxList
            ListDynamic.ID = "|" & STF_FT_ID & "|"


            'ListDynamic.SelectionMode = ListSelectionMode.Multiple
            'ListDynamic.Width = "200"
            'ListDynamic.Height = "200"
            'ListDynamic.CssClass = "matters"
            If ListDynamic.ID = "|5|" Then
                Dim hf As HiddenField = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField)

                Dim bsuper As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select usr_bsuper from users_m where USR_NAME='" & Session("sUsr_name") & "'")
                If bsuper = False Then
                    hf.Value = hf.Value + " where   bsu_id in(select USM_BSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                End If

            End If
            If ListDynamic.ID = "|11|" Then
                Dim hf As HiddenField = DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField)
                'Dim bsuper As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select usr_bsuper from users_m where USR_NAME='" & Session("sUsr_name") & "'")
                '  If bsuper = False Then
                hf.Value = hf.Value + " where   bsu_id in(select USM_WBSU_ID from USERACCESS_MENU where USM_USR_NAME='" & Session("sUsr_name") & "' ) ORDER BY 2"
                'End If

            End If

            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, DirectCast(row.FindControl("HiddenSTF_FT_CONDITION"), HiddenField).Value)
            ListDynamic.DataSource = dc
            ListDynamic.DataValueField = DirectCast(row.FindControl("HiddenSTF_FT_VALUE_FIELD"), HiddenField).Value.Trim()
            ListDynamic.DataTextField = DirectCast(row.FindControl("HiddenSTF_FT_TEXT_FIELD"), HiddenField).Value.Trim()
            ListDynamic.DataBind()
            'Dim list As New ListItem
            'list.Text = "All" '"<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            For Each clist As ListItem In ListDynamic.Items
                'clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
                'ClientScript.RegisterArrayDeclaration(ChkId, String.Concat("'", clist.ClientID, "'"))
                clist.Attributes.Add("onclick", " return ChangeAllCheckBox(this," & dc.Tables(0).Rows.Count & " );")

            Next


            ''''ALL
            Dim list As New ListItem
            list.Text = "<span>All    </span>"
            List.Value = "-1"
            list.Attributes.Add("onclick", " return ChangeAllCheckBoxStates(this," & dc.Tables(0).Rows.Count & " );")

            'ListDynamic.Items.Insert(0, list) 

            If ListDynamic.DataValueField = "est_id" Then
                ' ListDynamic.SelectedValue = "1"
                ListDynamic.Items.FindByValue("1").Selected = True
                ListDynamic.Items.FindByValue("2").Selected = True
            Else
                ''''ALL       list.Selected = True
                'Response.Write("<script language ='javascript' type ='text/javascript'> change_chk_state('" + list + "') </Script>")
            End If
            ListDynamic.Items.Insert(0, list)


            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(ListDynamic)

            If ListDynamic.DataValueField = "ESD_ID" And HiddenMenuCode.Value <> "H000192" Then
                PanelHolder.Controls.Remove(ListDynamic)
            End If
        Next


        'For Each row As DataListItem In DataStaffFilter.Items
        '    Dim ListDynamic As New CheckBoxList
        '    Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
        '    If STF_FT_ID <> 10 Then
        '        ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

        '        ListDynamic.SelectedValue = -1
        '    End If

        'Next

        ''Catch ex As Exception

        ''End Try
        x = x + 1
    End Sub
    Sub bindFilter_Cond_C()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim C_TYPE As String = "EMP"

            Dim ds As New DataSet
            str_Sql = " select CCF_FIELD_NAME,CCF_FIELD_DISPLAY from com_cer_filter where CCF_TYPE='" & C_TYPE & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlCond1.Items.Clear()

            ddlCond1.DataSource = ds.Tables(0)
            ddlCond1.DataTextField = "CCF_FIELD_DISPLAY"
            ddlCond1.DataValueField = "CCF_FIELD_NAME"
            ddlCond1.DataBind()
            ddlCond1.Items.Add(New ListItem("-- Select --", "0"))
            ddlCond1.ClearSelection()
            ddlCond1.Items.FindByValue("0").Selected = True


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindPARTC_Grid()
        Dim str_search As String = String.Empty
        Dim str_cond1 As String = String.Empty

        Dim str_cond As String = String.Empty
        Dim str_Sid_search() As String
        Dim Temp_opr As String = String.Empty
        Dim Cond As String = String.Empty
        str_Sid_search = h_Selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        For Each item As ListItem In chkCond1.Items
            If (item.Selected) Then
                Temp_opr = item.Text
                If txtCond1.Text.Trim <> "" And ddlCond1.SelectedItem.Value <> "0" Then

                    If str_search = "LI" Then
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & "  LIKE '%" & txtCond1.Text & "%'"
                        Cond = "Any Where"

                    ElseIf str_search = "NLI" Then
                        Cond = "Not In"
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & " LIKE '%" & txtCond1.Text & "%'"
                    ElseIf str_search = "SW" Then
                        Cond = "Starts With"
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & " LIKE '" & txtCond1.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        Cond = "Not Start With"
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & " NOT LIKE '" & txtCond1.Text & "%'"
                    ElseIf str_search = "EW" Then
                        Cond = "Ends With"
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & " LIKE  '%" & txtCond1.Text & "'"
                    ElseIf str_search = "NEW" Then
                        Cond = "Not Ends With"
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & " NOT LIKE '%" & txtCond1.Text & "'"
                    ElseIf str_search = "BET" Then
                        Cond = "Between Dates"
                        str_cond1 = Temp_opr & "  " & ddlCond1.SelectedItem.Value & " BETWEEN '" & txtCond1.Text & "' AND '" & txtCond2.Text & "'"

                    End If
                    Dim checkDup As Boolean = False
                    For i As Integer = 0 To Session("PartC_CER").Rows.count - 1
                        If UCase(Session("PartC_CER").Rows(i)("STRCOND")) = UCase(str_cond1) Then
                            checkDup = True
                        End If
                    Next

                    If checkDup = False Then
                        Dim rDt As DataRow
                        rDt = Session("PartC_CER").NewRow
                        rDt("Id") = ViewState("id")
                        rDt("OPER") = Temp_opr
                        rDt("COLNAME") = ddlCond1.SelectedItem.Text
                        rDt("COND") = Cond
                        rDt("CONDVAL1") = txtCond1.Text
                        rDt("CONDVAL2") = txtCond2.Text
                        rDt("STRCOND") = str_cond1
                        '  If Session("PartC_CER").Rows.count = 0 Then

                        Session("PartC_CER").Rows.Add(rDt)
                        ViewState("id") = ViewState("id") + 1
                        gridbind()
                        lblError2.Text = ""
                    Else

                        lblError2.Text = "Duplicate Entry Not Allowed!!!"
                    End If

                    'lblError2.Text = ""

                    ' Else

                    ''Dim rDt As DataRow
                    ' ''        rDt = Session("dt").NewRow
                    'Session("PartC_CER").Rows.Add(rDt)
                    'ViewState("id") = ViewState("id") + 1
                    'gridbind()

                    '    Else
                    '    lblError2.Text = "Duplicate Entry Not Allowed!!!"
                    'End If

                    '        Next

                    ' End If
                Else

                    If ddlCond1.SelectedItem.Value = "0" Then
                        lblError2.Text = "Select the field name to be searched for!!!"
                    Else
                        lblError2.Text = "Enter the value to be searched for!!!"
                    End If

                End If

            End If
        Next

    End Sub
    Protected Sub btnPartCAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim CheckDate As Integer = InStr(1, UCase(ddlCond1.SelectedItem.Text), "DATE", CompareMethod.Text)
        For Each item As ListItem In chkCond1.Items
            If (item.Selected) Then
                i = 1
            End If
        Next
        If i = 0 Then
            lblError2.Text = "Select the AND/OR  operation before adding the filter condition"
        Else
            If CheckDate > 0 Then
                If serverDateValidate() = "0" Then
                    Call bindPARTC_Grid()

                End If
            Else
                Call bindPARTC_Grid()
            End If

        End If

    End Sub

    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtCond1.Text.Trim <> "" Then
            Dim strfDate As String = txtCond1.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtCond1.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtCond1.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date Required</div>"
        End If

        Dim str_search As String = String.Empty
        Dim str_Sid_search() As String
        str_Sid_search = h_Selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        If str_search = "BET" Then
            If txtCond2.Text.Trim <> "" Then

                Dim strfDate As String = txtCond2.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then

                    CommStr = CommStr & "<div>To Date format is Invalid</div>"
                Else
                    txtCond2.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtCond1.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtCond2.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtCond1.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime2 < dateTime1 Then

                                CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                            End If
                        Else

                            CommStr = CommStr & "<div>Invalid To Date</div>"
                        End If
                    End If

                End If
            Else
                CommStr = CommStr & "<div>To Date Required</div>"
            End If


        End If


        If CommStr <> "" Then
            lblError2.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        directory.Clear()
       
        ''Default Parameters 
        'param.Add("@IMG_BSU_ID", Session("sbsuid"))
        'param.Add("@IMG_TYPE", "LOGO")
        'param.Add("UserName", Session("sUsr_name"))
        'param.Add("@BSU_ID", Session("sbsuid"))
        ''Report(Parameters)
        Filter()

        If HiddenMenuCode.Value = "H000191" Then ''Staff Report 

        End If

        StaffFilterParameters()






    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim OPER As New DataColumn("OPER", System.Type.GetType("System.String"))
            Dim COLNAME As New DataColumn("COLNAME", System.Type.GetType("System.String"))
            Dim COND As New DataColumn("COND", System.Type.GetType("System.String"))
            Dim CONDVAL1 As New DataColumn("CONDVAL1", System.Type.GetType("System.String"))
            Dim CONDVAL2 As New DataColumn("CONDVAL2", System.Type.GetType("System.String"))
            Dim STRCOND As New DataColumn("STRCOND", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(OPER)
            dtDt.Columns.Add(COLNAME)
            dtDt.Columns.Add(COND)
            dtDt.Columns.Add(CONDVAL1)
            dtDt.Columns.Add(CONDVAL2)
            dtDt.Columns.Add(STRCOND)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError2.Text = "Record cannot be inserted"
            Return dtDt
        End Try
    End Function

    Protected Sub gvPartC_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gvPartC.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("PartC_CER").Rows.Count
            If Session("PartC_CER").rows(i)("Id") = COND_ID Then
                Session("PartC_CER").rows(i).delete()
            Else
                i = i + 1
            End If
        End While
        gridbind()
    End Sub
    Sub gridbind()
        Try
            gvPartC.DataSource = Session("PartC_CER")
            gvPartC.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub lnkbtngrdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblBSUID As New Label
        lblBSUID = TryCast(sender.FindControl("lblBSUID"), Label)
        If Not lblBSUID Is Nothing Then
            h_BSUID.Value = h_BSUID.Value.Replace(lblBSUID.Text, "").Replace("||||", "||")
            If Not FillBSUNames(h_BSUID.Value) Then
                h_BSUID.Value = lblBSUID.Text
            End If
            grdBSU.PageIndex = grdBSU.PageIndex
            FillBSUNames(h_BSUID.Value)
        End If
    End Sub
    Private Function ReportCriteriaFileds() As NameValueCollection
        Try
            Dim NvcCriteria As New NameValueCollection()
            NvcCriteria.Add("0", "")
            NvcCriteria.Add("1", "Designation  :")
            NvcCriteria.Add("2", "Department   :")
            NvcCriteria.Add("3", "Staff Groups : ")
            NvcCriteria.Add("4", "Staff status :")
            NvcCriteria.Add("5", "Issued Unit  :")
            NvcCriteria.Add("6", "Residence    :")
            NvcCriteria.Add("7", "Visa Type    :")
            NvcCriteria.Add("8", "Staff Type   :")
            NvcCriteria.Add("9", "ABC Group    :")
            NvcCriteria.Add("10", "Document type:")
            Return NvcCriteria
        Catch ex As Exception

        End Try
    End Function
    Shared directory As New Dictionary(Of String, String)

    Public Function Filter() As Dictionary(Of String, String)

        Dim NvcCriteriaList As NameValueCollection = ReportCriteriaFileds()
        Dim strCriteria As String
        Dim strFilterString As String

        For Each row As DataListItem In DataStaffFilterMain.Items
            Dim ListDynamic As New CheckBoxList
            Dim STF_FT_ID As String = DirectCast(row.FindControl("HiddenSTF_FT_ID"), HiddenField).Value
            ListDynamic = DirectCast(row.FindControl("|" & STF_FT_ID & "|"), CheckBoxList)

            If ListDynamic IsNot Nothing Then

                Dim FilterString As String = ""
                Dim allflag = 0

                For Each item As ListItem In ListDynamic.Items
                    If item.Value = "-1" And item.Selected = True Then
                        '' All
                        allflag = 1

                        For Each ALLItem As ListItem In ListDynamic.Items
                            If ALLItem.Value <> "-1" Then
                                If FilterString = "" Then
                                    FilterString = "" & ALLItem.Value & ""
                                    'strFilterString = "" & ALLItem.Text & ""
                                Else
                                    FilterString &= "," & ALLItem.Value & ""
                                    ' strFilterString &= "," & ALLItem.Text & ""
                                End If
                            End If

                        Next

                    ElseIf item.Selected = True And allflag = 0 Then
                        If FilterString = "" Then
                            FilterString = "" & item.Value & ""
                            strFilterString = "" & item.Text & ""
                        Else
                            FilterString &= "," & item.Value & ""
                            strFilterString &= "," & item.Text & ""
                        End If
                    End If
                Next

                If FilterString = "" Then
                    For Each ALLItem As ListItem In ListDynamic.Items
                        If ALLItem.Value <> "-1" Then
                            If FilterString = "" Then
                                FilterString = "" & ALLItem.Value & ""
                                'strFilterString = "" & ALLItem.Text & ""
                            Else
                                FilterString &= "," & ALLItem.Value & ""
                                ' strFilterString &= "," & ALLItem.Text & ""
                            End If
                        End If

                    Next
                End If

                If strFilterString <> "" Then
                    strCriteria &= NvcCriteriaList.Get(STF_FT_ID) & " " & strFilterString & " :: "
                    strFilterString = ""
                End If

                Dim STF_FT_PARAMETER_FIELD As String = DirectCast(row.FindControl("HiddenSTF_FT_PARAMETER_FIELD"), HiddenField).Value
                directory.Add(STF_FT_PARAMETER_FIELD, GenerateXML(FilterString))

            End If
        Next
        If strCriteria <> "" Then
            strCriteria = strCriteria.Substring(0, strCriteria.Length - 3)
        End If
        If strCriteria = "" Then
            ViewState("Criteria") = "Criteria :: All"
        Else
            ViewState("Criteria") = strCriteria
        End If


        Return directory

    End Function


    Private Function GenerateXML(ByVal ValueId As String) As String
        Dim xmlDoc As New XmlDocument
        Dim Details As XmlElement
        Dim XMLValueId As XmlAttribute
        Dim XMLDetail As XmlElement
        Try
            Details = xmlDoc.CreateElement("ROOT")
            xmlDoc.AppendChild(Details)
            Dim IDs As String() = ValueId.Split(",")
            For i As Integer = 0 To IDs.Length - 1
                XMLDetail = xmlDoc.CreateElement("Detail")
                XMLValueId = xmlDoc.CreateAttribute("ID")
                XMLValueId.InnerText = IDs(i)
                XMLDetail.Attributes.Append(XMLValueId)
                xmlDoc.DocumentElement.InsertBefore(XMLDetail, xmlDoc.DocumentElement.LastChild)

            Next
            Return xmlDoc.OuterXml
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Sub StaffFilterParameters()
        Try

            Dim param(30) As SqlClient.SqlParameter

            Dim bsu_ids As New StringBuilder
            For Each gr As GridViewRow In grdBSU.Rows
                Dim lblbsu As Label = DirectCast(gr.FindControl("lblBSUID"), Label)
                bsu_ids.Append(lblbsu.Text)
                bsu_ids.Append("|")

            Next

            param(0) = New SqlClient.SqlParameter("@BSU_ID", bsu_ids.ToString)
            param(1) = New SqlClient.SqlParameter("@EMP_DES_ID", directory.Item("EMP_DES_ID"))
            param(2) = New SqlClient.SqlParameter("@EMP_DPT_ID", directory.Item("EMP_DPT_ID"))
            param(3) = New SqlClient.SqlParameter("@EMP_ECT_ID", directory.Item("EMP_ECT_ID"))
            param(4) = New SqlClient.SqlParameter("@EMP_STATUS", directory.Item("EMP_STATUS"))
            param(5) = New SqlClient.SqlParameter("@EMP_VISA_BSU_ID", directory.Item("EMP_VISA_BSU_ID"))
            param(6) = New SqlClient.SqlParameter("@EMP_bOVERSEAS", directory.Item("EMP_bOVERSEAS"))
            param(7) = New SqlClient.SqlParameter("@EMP_VISATYPE", directory.Item("EMP_VISATYPE"))
            param(8) = New SqlClient.SqlParameter("@EMP_bTemp", directory.Item("EMP_bTemp"))
            param(9) = New SqlClient.SqlParameter("@EMP_ABC", directory.Item("EMP_ABC"))


            ''''''''''''''''''''''''''
            param(10) = New SqlClient.SqlParameter("@EMP_VISASTATUS", directory.Item("EMP_VISASTATUS"))
            param(11) = New SqlClient.SqlParameter("@EMP_RLG_ID", directory.Item("EMP_RLG_ID"))
            param(12) = New SqlClient.SqlParameter("@EMP_ACCOMODATION", directory.Item("EMP_ACCOMODATION"))
            '       param.Add("@EMP_bACTIVE", directory.Item("EMP_bOVERSEAS"))
            param(13) = New SqlClient.SqlParameter("@EMP_BANK", directory.Item("EMP_BANK"))
            param(14) = New SqlClient.SqlParameter("@EMP_MOE_DES_ID", directory.Item("EMP_MOE_DES_ID"))
            param(15) = New SqlClient.SqlParameter("@EMP_VISA_DES_ID", directory.Item("EMP_VISA_DES_ID"))
            param(16) = New SqlClient.SqlParameter("@EMP_MARITALSTATUS", directory.Item("EMP_MARITALSTATUS"))
            param(17) = New SqlClient.SqlParameter("@EMP_CTY_ID", directory.Item("EMP_CTY_ID"))
            param(18) = New SqlClient.SqlParameter("@EMP_SEX_bMALE", directory.Item("EMP_SEX_bMALE"))
            param(19) = New SqlClient.SqlParameter("@EMP_QLF_ID", directory.Item("EMP_QLF_ID"))
            param(20) = New SqlClient.SqlParameter("@EMP_BLOODGRP", directory.Item("EMP_BLOODGRP"))
            param(21) = New SqlClient.SqlParameter("@EMP_TICKETFLAG", directory.Item("EMP_TICKETFLAG"))
            param(22) = New SqlClient.SqlParameter("@EMP_TICKETCLASS", directory.Item("EMP_TICKETCLASS"))
            param(23) = New SqlClient.SqlParameter("@EMP_CONTRACTTYPE", directory.Item("EMP_CONTRACTTYPE"))
            param(24) = New SqlClient.SqlParameter("@EMP_bCompanyTransport", directory.Item("EMP_bCompanyTransport"))
            param(25) = New SqlClient.SqlParameter("@EMP_SGD_ID", directory.Item("EMP_SGD_ID"))




            'param.Add("@EMP_bTemp", directory.Item("EMP_bTemp"))
            'param.Add("@EMP_ABC", directory.Item("EMP_ABC"))
            'param.Add("@EMP_bTemp", directory.Item("EMP_bTemp"))
            'param.Add("@EMP_ABC", directory.Item("EMP_ABC"))
            'param.Add("@BSU_ID", directory.Item("BSU_ID"))

            'If HiddenMenuCode.Value = "H000192" Then
            '    param.Add("@ESD_ID", directory.Item("ESD_ID"))
            'ElseIf HiddenMenuCode.Value = "H000191" Or HiddenMenuCode.Value = "H000196" Or HiddenMenuCode.Value = "H000188" Or HiddenMenuCode.Value = "H000202" Or HiddenMenuCode.Value = "H000111" Or HiddenMenuCode.Value = "H000112" Then
            param(26) = New SqlClient.SqlParameter("@BSU_ID_EMP", bsu_ids.ToString)

            'End If
            Dim cond As String = ""
            For Each gridrow As DataRow In Session("PartC_CER").Rows
                Dim lblCond As String = gridrow.Item("STRCOND").ToString
                cond = cond + lblCond
            Next
            param(27) = New SqlClient.SqlParameter("@CONDITION", cond)

            Session("Exportparam") = param
            Response.Redirect("StaffListExportExcel_View.aspx")
            'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STF_FILTER_EMPLOYEE", param)

            'Dim dtEXCEL As New DataTable
            ''ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Exec_Query)
            'dtEXCEL = ds.Tables(0)
            'If dtEXCEL.Rows.Count > 0 Then

            '    Dim grdView As New GridView
            '    grdView.DataSource = dtEXCEL
            '    grdView.DataBind()

            '    Dim str_err As String = String.Empty
            '    Dim errorMessage As String = String.Empty

            '    Response.Clear()
            '    Response.AddHeader("content-disposition", String.Format("inline;filename={0}.xls", "student"))
            '    'Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", "student"))
            '    Response.Charset = ""
            '    Response.ContentType = "application/vnd.xls"
            '    Dim stringWrite As New StringWriter()
            '    Dim htmlWrite As New HtmlTextWriter(stringWrite)
            '    grdView.RenderControl(htmlWrite)
            '    Response.Write(stringWrite.ToString())
            '    Response.End()
            'Else
            '    lblError.Text = "No Records To display with this filter condition....!!!"
            '    lblError.Focus()
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function FillBSUNames(ByVal BSUIDs As String) As Boolean
        Dim IDs As String() = BSUIDs.Split("||")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
            i += 1
        Next
        'str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        str_Sql = "SELECT USR_bSuper FROM OASIS..USERS_M WHERE USR_NAME ='" & Session("sUsr_name") & "'"
        If IIf(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql) Is Nothing, False, True) Then
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ")"
        Else
            str_Sql = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID IN(" + condition + ") AND BSU_ID IN(SELECT USA_BSU_ID FROM USERACCESS_S, USERS_M WHERE USR_ID = USA_USR_ID AND USR_NAME ='" & Session("sUsr_name") & "')"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        grdBSU.DataSource = ds
        grdBSU.DataBind()
        'txtBSUName.Text = ""
        'Dim bval As Boolean = dr.Read
        'While (bval)
        '    txtBSUName.Text += dr(0).ToString()
        '    bval = dr.Read()
        '    If bval Then
        '        txtBSUName.Text += "||"
        '    End If
        'End While
        Return True
    End Function

    Protected Sub lnlbtnAddBSUID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub
    Protected Sub grdBSU_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdBSU.PageIndexChanging
        grdBSU.PageIndex = e.NewPageIndex
        FillBSUNames(h_BSUID.Value)
    End Sub

    Protected Sub txtBSUName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        h_BSUID.Value += "||" + txtBSUName.Text.Replace(",", "||")
        FillBSUNames(h_BSUID.Value)
    End Sub
End Class
