Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Web.Configuration
Imports System.IO

Partial Class OASIS_HR_Administrator_StaffResgination
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Version        Author          Date            Change
    '1.1            swapna          12-Jun-2011     Employee resignation - new fields added in table,code changed accordingly
    '                                               Adding approve functionality
    '1.2            swapna          21-jun-2012     To add contract type and end date
    '2.0            Swapna          28/Mar/2016     To allow FFS outside system for AX units
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        txtCurrentLOPs.Attributes.Add("ReadOnly", "ReadOnly")
        If IsPostBack = False Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")
            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "H000073" And ViewState("MainMnu_code") <> "H000077" And ViewState("MainMnu_code") <> "H000078") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("IsProcessed") = 0
            hfEmpIsfromAX.Value = False

            '-------------------- Add/Edit-------------------
            If ViewState("MainMnu_code") = "H000073" Then
                ddlApprove.Visible = False
                chkForward.Visible = True
                trAction.Visible = False
                trApproval.Visible = False
                If Session("BSU_IsHROnDAX") = 1 Then
                    chkFFSOutsideSystem.Visible = True
                    btnSyncToDAX.Visible = False
                Else
                    chkFFSOutsideSystem.Visible = False
                    btnSyncToDAX.Visible = False
                End If

                '---------------Approve------------------------------
            Else
                ViewState("APSID") = Encr_decrData.Decrypt(Request.QueryString("APSID").Replace(" ", "+"))
                chkForward.Visible = False
                ddlApprove.Visible = True
                trAction.Visible = True
                trApproval.Visible = True

                If ViewState("datamode") <> "edit" Then
                    btnSave.Visible = False
                    btnEdit.Visible = False
                End If
                btnAdd.Visible = False
            End If
            If (ViewState("MainMnu_code") = "H000073" Or ViewState("MainMnu_code") = "H000077" Or ViewState("MainMnu_code") = "H000078") And (ViewState("datamode") = "edit" Or ViewState("datamode") = "view") Then
                ViewState("EREG_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                FillResignationDetails(ViewState("EREG_ID"))
                CheckIfFFSSynchedToDAX()
                SetControlStatus(False)
                If ViewState("MainMnu_code") = "H000077" Then
                    txtlastwrkdate.Enabled = IIf(ViewState("datamode") = "edit", True, False)
                    txtResgDate.Enabled = IIf(ViewState("datamode") = "edit", True, False)
                    img_lastworkingdate.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    img_resgDate.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    trApproval.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    trHRApproval.Visible = False
                    trAction.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    btnEdit.Visible = False
                    ddlApprove.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    btnSave.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    cb_exitinterview.Visible = CheckIfLastApprover(False)
                ElseIf ViewState("MainMnu_code") = "H000078" Then
                    txtlastwrkdate.Enabled = False
                    txtResgDate.Enabled = False
                    img_lastworkingdate.Visible = False
                    img_resgDate.Visible = False
                    btnEdit.Visible = False
                    trApproval.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    trHRApproval.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    trAction.Visible = IIf(ViewState("datamode") = "edit", True, False)
                    btnSave.Visible = IIf(ViewState("datamode") = "edit", True, False)
                ElseIf ViewState("MainMnu_code") = "H000073" Then
                    btnSave.Visible = False
                    If ViewState("datamode") = "view" And chkForward.Checked = True And chkForward.Enabled = False Then
                        btnEdit.Visible = False
                    End If
                End If
            ElseIf ViewState("datamode") = "add" Then
                btnAdd.Visible = False
                btnEdit.Visible = False

            End If
        End If
    End Sub
    Private Sub CheckIfFFSSynchedToDAX()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT   ISNULL(COUNT(*), 0) IsProcessed FROM   OASIS_DAX.dbo.SALARY_FINAL_SETTLEMENT WITH ( NOLOCK ) WHERE    SFNL_EMP_ID =" & hfEmpId.Value & _
                              " AND SFNL_BSU_ID ='" & Session("sBSUID") & "'"
        Dim dr2 As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While (dr2.Read())
            ViewState("IsProcessed") = dr2("IsProcessed")
            If ViewState("IsProcessed") <> 0 Then
                btnSyncToDAX.Enabled = False
            End If
            'ViewState("PAYYEAR") = dr("BSU_PAYYEAR")
            'ViewState("PAY_CUR_ID") = dr("BSU_CURRENCY")
        End While
        dr2.Close()
    End Sub
    Protected Sub txt_employeeno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_employeeno.TextChanged
        lblError.Text = ""
        bindEmployeeDetails()
    End Sub
    Sub SetControlStatus(ByVal bEnable As Boolean)
        txtlastwrkdate.Enabled = bEnable
        txtRemarks.Enabled = bEnable
        txtResgDate.Enabled = bEnable
        chkForward.Enabled = bEnable
        chkFFSOutsideSystem.Enabled = bEnable
        RdbResgtype.Enabled = bEnable
        If hfEmpIsfromAX.Value = True Then  'V2.0
            txtResgDate.Enabled = False
            txtlastwrkdate.Enabled = False
            img_lastworkingdate.Visible = False
            img_resgDate.Visible = False
            RdbResgtype.Enabled = False
        End If

        If chkFFSOutsideSystem.Checked = True And chkFFSOutsideSystem.Enabled = False Then
            If Session("BSU_IsHROnDAX") = 1 Then
                btnSyncToDAX.Visible = True
                CheckIfFFSSynchedToDAX()

            End If
        End If
    End Sub
    Sub FillResignationDetails(ByVal RegId As String)
        Try
            Dim Param(1) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@EREG_ID", RegId)

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetEmpResignationdetails", Param)
            If (ds.Tables(0).Rows.Count > 0) Then
                txt_employeeno.Text = ds.Tables(0).Rows(0).Item("EMPNO").ToString
                lbl_Empname.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
                lbldesg.Text = ds.Tables(0).Rows(0).Item("EMP_DES_DESCR").ToString
                lbldpt.Text = ds.Tables(0).Rows(0).Item("EMP_DEPT_DESCR").ToString
                txtlastwrkdate.Text = Format(ds.Tables(0).Rows(0).Item("EREG_LASTWORKINGDATE"), OASISConstants.DateFormat)
                txtResgDate.Text = Format(ds.Tables(0).Rows(0).Item("EREG_REGDATE"), OASISConstants.DateFormat)
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("EREG_REMARKS").ToString        'V1.1
                txtApvRemarks.Text = ds.Tables(0).Rows(0).Item("EREG_ApprRemarks").ToString
                RdbResgtype.SelectedValue = ds.Tables(0).Rows(0).Item("EREG_REGTYPE").ToString
                chkForward.Checked = ds.Tables(0).Rows(0).Item("EREG_BForwaded")
                hfEmpId.Value = ds.Tables(0).Rows(0).Item("EREG_EMP_ID").ToString
                ViewState("ApprStatus") = ds.Tables(0).Rows(0).Item("EREG_ApprStatus").ToString
                lblVisaBSU.Text = ds.Tables(0).Rows(0).Item("VISA_BSU_NAME").ToString
                lblWorkingBSU.Text = ds.Tables(0).Rows(0).Item("WORK_BSU_NAME").ToString
                lblDOJBSU.Text = Format(ds.Tables(0).Rows(0).Item("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                lblDOJGroup.Text = Format(ds.Tables(0).Rows(0).Item("EMP_JOINDT"), OASISConstants.DateFormat)
                hfEmpIsfromAX.Value = ds.Tables(0).Rows(0).Item("EREG_IsFromAX")  'V2.0
                chkFFSOutsideSystem.Checked = ds.Tables(0).Rows(0).Item("EMP_IsFinalSettled") 'V2.0
                'If chkFFSOutsideSystem.Checked = True And Session("BSU_IsOnDAX") = 1 Then 'V2.0
                '    btnSyncToDAX.Visible = True
                'End If
                If ds.Tables(0).Rows(0).Item("EMP_PHOTO") <> "" Then 'And IO.File.Exists(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & ds.Tables(0).Rows(0).Item("EMP_PHOTO")) Then
                    Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & ds.Tables(0).Rows(0).Item("EMP_PHOTO")
                    imgEmployee.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                    'Else
                    '    imgEmployee.ImageUrl = WebConfigurationManager.AppSettings.Item("NoImagePath")
                End If
                ddlEmpContractType.SelectedValue = ds.Tables(0).Rows(0).Item("EMP_CONTRACTTYPE").ToString  'V1.2
                If ds.Tables(0).Rows(0).Item("EMP_CONTRACTENDDT") <> "1/jan/1900" Then
                    txtContractEndDt.Text = Format(ds.Tables(0).Rows(0).Item("EMP_CONTRACTENDDT"), OASISConstants.DateFormat)
                Else
                    txtContractEndDt.Text = ""
                End If

                lblLopfirst.Text = "As per system:" & ds.Tables(0).Rows(0).Item("EREG_LOP_First5Yr").ToString
                lblLOPLast.Text = "As per system:" & ds.Tables(0).Rows(0).Item("EREG_LOP_After5Yr").ToString
                txtLOPFirstFiveYr.Text = ds.Tables(0).Rows(0).Item("EREG_LOP_First5Yr").ToString
                txtLOPAfterFiveYr.Text = ds.Tables(0).Rows(0).Item("EREG_LOP_After5Yr").ToString
                txtCurrentLOPs.Text = ds.Tables(0).Rows(0).Item("TotalLOP").ToString

            End If
            If ds.Tables(1).Rows.Count > 0 Then
                gvActionDetail.DataSource = ds.Tables(1)
                gvActionDetail.DataBind()
                trActionDetails.Visible = True
            Else
                trActionDetails.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, Page.Title)
        End Try
    End Sub
    Sub bindEmployeeDetails()
        Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
        Dim Param(5) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@EMPNO", txt_employeeno.Text.Trim)
        Using EmpReader As SqlDataReader = SqlHelper.ExecuteReader(sqlcon, CommandType.StoredProcedure, "STF_GETEMPLOYEE_BYNO", Param)
            If EmpReader.HasRows = True Then
                While EmpReader.Read
                    hfEmpId.Value = Convert.ToString(EmpReader("EMP_ID"))
                    lbl_Empname.Text = Convert.ToString(EmpReader("EmployeeName"))
                    lbldpt.Text = Convert.ToString(EmpReader("DPT_DESCR"))
                    lbldesg.Text = Convert.ToString(EmpReader("DES_DESCR"))
                    lblVisaBSU.Text = Convert.ToString(EmpReader("VISA_BSU_NAME"))
                    lblWorkingBSU.Text = Convert.ToString(EmpReader("WORK_BSU_NAME"))
                    lblDOJBSU.Text = Format(EmpReader("EMP_BSU_JOINDT"), OASISConstants.DateFormat)
                    lblDOJGroup.Text = Format(EmpReader("EMP_JOINDT"), OASISConstants.DateFormat)
                    If EmpReader("EMP_PHOTO") <> "" Then 'And IO.File.Exists(WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & "/" & EmpReader("EMP_PHOTO")) Then
                        imgEmployee.ImageUrl = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & EmpReader("EMP_PHOTO") & "?" & DateTime.Now.Ticks.ToString()
                        'Else
                        '    imgEmployee.ImageUrl = WebConfigurationManager.AppSettings.Item("NoImagePath")
                    End If
                    ddlEmpContractType.SelectedValue = Convert.ToString(EmpReader("EMP_CONTRACTTYPE")) 'V1.2
                    If EmpReader("EMP_CONTRACTENDDT") <> "1/1/1900" Then
                        txtContractEndDt.Text = Format(EmpReader("EMP_CONTRACTENDDT"), OASISConstants.DateFormat)
                    Else
                        txtContractEndDt.Text = ""
                    End If
                    txtCurrentLOPs.Text = Convert.ToString(EmpReader("LOPDays"))
                    lblLopfirst.Text = "As per system:" & Convert.ToString(EmpReader("LopFirstFiveyr"))
                    lblLOPLast.Text = "As per system:" & Convert.ToString(EmpReader("LopAfterFiveyr"))
                    txtLOPFirstFiveYr.Text = Convert.ToString(EmpReader("LopFirstFiveyr"))
                    txtLOPAfterFiveYr.Text = Convert.ToString(EmpReader("LopAfterFiveyr"))
                End While
                'Dim sql As String
                'sql = "select count(*) from APPROVALPOLICY_D D,APPROVALPOLICY_H H WHERE isnull(H.LPS_bActive,0)=1 and D.LPD_LPS_ID=H.LPS_ID AND H.LPS_ID IN (SELECT EMP_LPS_ID  FROM EMPLOYEE_M WHERE EMP_ID='" & hfEmpId.Value.Trim & "')"
                'If Mainclass.getDataValue(sql, "OASISConnectionString") = 0 Then
                '    lblError.Text = "No Approval heirarchy defined for the selected employee"
                'End If
            Else
                hfEmpId.Value = ""
                lbl_Empname.Text = ""
                lbldpt.Text = ""
                lbldesg.Text = ""
            End If
        End Using
    End Sub
    Private Function SaveRESIGNATIONAPPROVAL() As String
        Try
            If IsDate(txtlastwrkdate.Text) = False Or IsDate(txtResgDate.Text) = False Then
                lblError.Text = "Not a valid Date....!!!!"
                Exit Function
            End If
            Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
            Dim params(8) As SqlClient.SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@EMP_ID", hfEmpId.Value.Trim, SqlDbType.VarChar)
            params(1) = Mainclass.CreateSqlParameter("@EREG_LASTWORKINGDATE", txtlastwrkdate.Text.Trim, SqlDbType.VarChar)
            params(2) = Mainclass.CreateSqlParameter("@EREG_REGDATE", txtResgDate.Text.Trim, SqlDbType.VarChar)
            params(3) = Mainclass.CreateSqlParameter("@REMARKS", txtApvRemarks.Text.Trim, SqlDbType.VarChar)
            params(4) = Mainclass.CreateSqlParameter("@APS_ID", ViewState("APSID"), SqlDbType.Int)
            params(5) = Mainclass.CreateSqlParameter("@STATUS", ddlApprove.SelectedValue, SqlDbType.VarChar)
            params(6) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
            params(7) = Mainclass.CreateSqlParameter("@BSND_EXIT_INV", cb_exitinterview.CHECKED, SqlDbType.Bit)
            params(8) = Mainclass.CreateSqlParameter("@return_value", "", SqlDbType.VarChar, True, 200)
            Dim retval As Integer
            retval = Mainclass.ExecuteParamQRY(sqlcon, "StaffResignationApproval", params)
            If params(8).Value = "0" Then
                SaveRESIGNATIONAPPROVAL = params(5).Value
                lblError.Text = "Record Updated Successfully......!!!!!!"
                'SendEmailToNextApprover(ddlApprove.SelectedValue, False)
                SetControlStatus(False)
                btnSave.Visible = False
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Response.Redirect("StaffResignationApproval.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt(ViewState("datamode")))
            Else
                lblError.Text = retval '"Record Not Saved.Try Again!!!!!"
                SaveRESIGNATIONAPPROVAL = "1000"
            End If
        Catch ex As Exception
            lblError.Text = "Record Not Saved.Try Again!!!!!"
            UtilityObj.Errorlog(ex.Message)
            SaveRESIGNATIONAPPROVAL = "1000"
        End Try
    End Function
    Private Function SaveRESIGNATIONHRAPPROVAL() As String
        Try
            Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
            Dim params(7) As SqlClient.SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@EREG_ID", ViewState("EREG_ID"), SqlDbType.VarChar)
            params(1) = Mainclass.CreateSqlParameter("@REMARKS", txtApvRemarks.Text.Trim, SqlDbType.VarChar)
            params(2) = Mainclass.CreateSqlParameter("@REMARKS_ACC", txtApvHRRemarks.Text.Trim, SqlDbType.VarChar)
            params(3) = Mainclass.CreateSqlParameter("@STATUS", ddlApprove.SelectedValue, SqlDbType.VarChar)
            params(4) = Mainclass.CreateSqlParameter("@USER", Session("sUsr_name"), SqlDbType.VarChar)
            params(5) = Mainclass.CreateSqlParameter("@return_value", "", SqlDbType.VarChar, True, 200)
            Dim retval As Integer
            retval = Mainclass.ExecuteParamQRY(sqlcon, "StaffResignationHRApproval", params)
            If params(5).Value = "0" Then
                SaveRESIGNATIONHRAPPROVAL = params(5).Value
                lblError.Text = "Record Updated Successfully......!!!!!!"
                'SendEmailToNextApprover(ddlApprove.SelectedValue, True)
                SetControlStatus(False)
                btnSave.Visible = False
                'clearTexts()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Response.Redirect("StaffResignationApproval.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt(ViewState("datamode")))
            Else
                SaveRESIGNATIONHRAPPROVAL = "1000"
                lblError.Text = "Record Not Saved.Try Again!!!!!"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            SaveRESIGNATIONHRAPPROVAL = "1000"
            lblError.Text = "Record Not Saved.Try Again!!!!!"
        End Try
    End Function

    Sub SaveData()
        Try
            If IsNumeric(txtLOPFirstFiveYr.Text) = False Or IsNumeric(txtLOPAfterFiveYr.Text) = False Then
                lblError.Text = "LOP days must be in numbers."
                Exit Sub
            End If
            Dim sqlcon As String = ConnectionManger.GetOASISConnectionString
            Dim Param(18) As SqlClient.SqlParameter 'V2.0
            Param(0) = Mainclass.CreateSqlParameter("@EREG_EMP_ID", hfEmpId.Value.Trim, SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@EREG_REGTYPE", RdbResgtype.SelectedValue, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@EREG_LASTWORKINGDATE", txtlastwrkdate.Text.Trim, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@EREG_REGDATE", txtResgDate.Text.Trim, SqlDbType.VarChar)
            Param(4) = Mainclass.CreateSqlParameter("@EREG_REMARKS", txtRemarks.Text.Trim, SqlDbType.VarChar)
            Param(5) = Mainclass.CreateSqlParameter("@EREG_EnteredBY", Session("sUsr_name"), SqlDbType.VarChar)
            Param(6) = Mainclass.CreateSqlParameter("@EREG_BForwaded", chkForward.Checked, SqlDbType.VarChar)
            Dim strStatus As String = ""
            If (chkForward.Checked = True) Then
                strStatus = "F"
            Else
                strStatus = "N"

            End If


            Param(8) = Mainclass.CreateSqlParameter("@EREG_ApprStatus", strStatus, SqlDbType.VarChar)

            If RdbResgtype.SelectedValue = "4" Then
                Param(9) = Mainclass.CreateSqlParameter("@EREG_bRESIGNED", 1, SqlDbType.Int)
            Else
                Param(9) = Mainclass.CreateSqlParameter("@EREG_bRESIGNED", 0, SqlDbType.Int)
            End If
            Param(10) = Mainclass.CreateSqlParameter("@EREG_ApprovedBy", Session("sUsr_name"), SqlDbType.VarChar)

            If cb_exitinterview.Checked = True Then
                Param(11) = Mainclass.CreateSqlParameter("@EREG_BSND_EXTINTV", "True", SqlDbType.VarChar)
            Else
                Param(11) = Mainclass.CreateSqlParameter("@EREG_BSND_EXTINTV", "False", SqlDbType.VarChar)
            End If

            Param(12) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            If ViewState("EREG_ID") = "" Or ViewState("EREG_ID") = "0" Then
                Param(7) = Mainclass.CreateSqlParameter("@bEdit", False, SqlDbType.Bit)
                Param(13) = Mainclass.CreateSqlParameter("@EREG_ID", "0", SqlDbType.VarChar, True, 100)
            Else
                Param(7) = Mainclass.CreateSqlParameter("@bEdit", True, SqlDbType.Bit)
                Param(13) = Mainclass.CreateSqlParameter("@EREG_ID", ViewState("EREG_ID"), SqlDbType.VarChar)
            End If
            Param(14) = Mainclass.CreateSqlParameter("@ContracttType", ddlEmpContractType.SelectedValue, SqlDbType.Int) 'V1.2

            If txtContractEndDt.Text <> "" Then
                Param(15) = Mainclass.CreateSqlParameter("@ContractEndDate", txtContractEndDt.Text, SqlDbType.VarChar)
            Else
                Param(15) = Mainclass.CreateSqlParameter("@ContractEndDate", DBNull.Value, SqlDbType.VarChar)
            End If
            Param(16) = Mainclass.CreateSqlParameter("@LOPFirstFive", txtLOPFirstFiveYr.Text, SqlDbType.VarChar)
            Param(17) = Mainclass.CreateSqlParameter("@LOPAfterFive", txtLOPAfterFiveYr.Text, SqlDbType.VarChar)
            Param(18) = Mainclass.CreateSqlParameter("@IsFFSOutsideSystem", chkFFSOutsideSystem.Checked, SqlDbType.Bit) 'V2.0
            Dim retVal As Integer
            retVal = Mainclass.ExecuteParamQRY(sqlcon, "SaveEMPRESIGNATION_D", Param)
            If ViewState("EREG_ID") = "" Then
                ViewState("EREG_ID") = Param(13).Value
            End If
            Dim ret As String = Param(12).Value
            If (ret = "0" Or ret = "") And ViewState("EREG_ID") <> "0" And retVal <> -1 Then
                lblError.Text = "Record Saved Successfully......!!!!!!"
                If Session("BSU_IsHROnDAX") = 1 Then
                    If chkForward.Checked = True Then
                        lblError.Text = "Record Saved Successfully !!! Kindly click on 'Sync To DAX' to send F&F information to AX "
                    Else
                        lblError.Text = "Record Saved Successfully !!!"
                    End If

                End If

                If chkForward.Checked Then SendEmailToNextApprover("A", False)
                SetControlStatus(False)
                btnEdit.Visible = True
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                btnSave.Visible = False
                If chkFFSOutsideSystem.Checked = True And Session("BSU_IsHROnDAX") = 1 Then
                    btnSyncToDAX.Visible = True
                    btnSave.Visible = False
                    'CheckIfFFSSynchedToDAX()
                End If
                ' Response.Redirect("StaffResignationList.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt(ViewState("datamode")))
            Else
                lblError.Text = UtilityObj.getErrorMessage(ret) ' "Request Cannot be processed!"
                'lblError.Text = ret
            End If
        Catch ex As Exception
            lblError.Text = "Error Occured.Record Not Saved.Try Again!!!!!"
        End Try
    End Sub
    Sub clearTexts()
        txt_employeeno.Text = ""
        lbl_Empname.Text = ""
        lbldesg.Text = ""
        lbldpt.Text = ""
        txtlastwrkdate.Text = ""
        txtResgDate.Text = ""
        txtRemarks.Text = ""
        txtApvRemarks.Text = ""
        RdbResgtype.SelectedValue = "4"
        chkForward.Checked = False
        hfEmpId.Value = ""
        ViewState("ApprStatus") = ""
        lblVisaBSU.Text = ""
        lblWorkingBSU.Text = ""
        lblDOJBSU.Text = ""
        lblDOJGroup.Text = ""
        ViewState("EREG_ID") = ""
        gvActionDetail.Visible = False
        txtContractEndDt.Text = ""
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txt_employeeno.Text = "" Or txtlastwrkdate.Text = "" Or txtResgDate.Text = "" Then
                lblError.Text = "Please Enter all the Fields ....!!!!"
                Exit Sub
            End If
            If ViewState("MainMnu_code") = "H000077" Then
                If cb_exitinterview.Checked = True And ddlApprove.SelectedValue <> "A" Then
                    lblError.Text = "Cannot schedule exit interview without approval."
                    Exit Sub
                End If
                If cb_exitinterview.Checked = False And ddlApprove.SelectedValue = "A" And cb_exitinterview.Visible Then
                    lblError.Text = "Cannot Approve without schedule Exit Interview."
                    Exit Sub
                End If
            End If
            If ViewState("MainMnu_code") = "H000073" And hfEmpId.Value <> "" And RdbResgtype.SelectedValue <> "" Then
                If IsDate(txtlastwrkdate.Text) = True And IsDate(txtResgDate.Text) = True Then
                    'If CDate(txtResgDate.Text) > CDate(txtlastwrkdate.Text) Then
                    '    lblError.Text = "Last Working Date cannot be less than Resignation Date."
                    '    Exit Sub
                    'End If
                    SaveData()
                Else
                    lblError.Text = "Not a valid Date....!!!!"
                    Exit Sub
                End If
            ElseIf ViewState("MainMnu_code") = "H000077" And ViewState("EREG_ID") <> "" Then
                SaveRESIGNATIONAPPROVAL()
            ElseIf ViewState("MainMnu_code") = "H000078" And ViewState("EREG_ID") <> "" Then
                SaveRESIGNATIONHRAPPROVAL()
            Else
                lblError.Text = "No record to save!!!!"
            End If

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("MainMnu_code") = "H000073" Then
            Response.Redirect("StaffResignationList.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt(ViewState("datamode")))
        ElseIf ViewState("MainMnu_code") = "H000077" Then
            Response.Redirect("StaffResignationApproval.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt(ViewState("datamode")))
        ElseIf ViewState("MainMnu_code") = "H000078" Then
            Response.Redirect("StaffResignationApproval.aspx?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt(ViewState("datamode")))
        End If
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If chkFFSOutsideSystem.Checked = True Then
            lblError.Text = "Final settlement outside system records cannot be edited."
            Exit Sub
        End If
        If ViewState("MainMnu_code") = "H000073" Then
            If ViewState("datamode") = "view" And chkForward.Checked = True And chkForward.Enabled = False Then
                lblError.Text = "Forwarded documents cannot be edited."
                Exit Sub
            Else
                btnSave.Visible = True
                btnEdit.Visible = False
                If Session("BSU_IsHROnDAX") = 1 Then
                    chkFFSOutsideSystem.Visible = True
                    btnSyncToDAX.Visible = False
                    chkFFSOutsideSystem.Attributes.Add("OnClick", "return showconfirm();")
                Else
                    chkFFSOutsideSystem.Visible = False
                    btnSyncToDAX.Visible = False
                End If

                If hfEmpIsfromAX.Value = True And Session("BSU_IsHROnDAX") = 1 Then 'V2.0
                    txtResgDate.Enabled = False
                    txtlastwrkdate.Enabled = False
                    RdbResgtype.Enabled = False
                    If chkFFSOutsideSystem.Checked = True Then
                        btnSyncToDAX.Visible = True
                        CheckIfFFSSynchedToDAX()
                    End If
                End If

            End If
        End If
        If ViewState("MainMnu_code") = "H000077" Then
            If ViewState("ApprStatus") = "R" Or ViewState("ApprStatus") = "A" Then
                lblError.Text = "Approved/Rejected documents cannot be edited."
                Exit Sub
            End If
            If ViewState("ApprStatus") = "N" Or ViewState("ApprStatus") = "F" Then
                lblError.Text = "Documents can be edited only while approving"
                Exit Sub
            End If
        End If
        ViewState("datamode") = "add"
        SetControlStatus(True)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        clearTexts()
        SetControlStatus(True)
        btnSave.Visible = True
    End Sub
    Private Function CheckIfLastApprover(ByVal IsHRLoggedIn As Boolean) As Boolean
        Try
            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim dt As New DataTable
            Dim Param(8) As SqlClient.SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@EMP_ID", hfEmpId.Value.Trim, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@DOC_ID", ViewState("EREG_ID"), SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@DOC_TYPE", "RESGN", SqlDbType.VarChar)
            Param(4) = Mainclass.CreateSqlParameter("@status", "A", SqlDbType.VarChar)
            Param(5) = Mainclass.CreateSqlParameter("@IsFinalApproval", False, SqlDbType.Bit, True)
            Param(6) = Mainclass.CreateSqlParameter("@ALLAPPROVED", False, SqlDbType.Bit, True)
            Param(7) = Mainclass.CreateSqlParameter("@APS_ID", 0, SqlDbType.Int)
            Param(8) = Mainclass.CreateSqlParameter("@HRAPPROVAL", IsHRLoggedIn, SqlDbType.Bit)
            dt = Mainclass.getDataTable("getNextApproverDetails", Param, ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            If Param(5).Value Then
                CheckIfLastApprover = True
            Else
                CheckIfLastApprover = False
            End If
        Catch ex As Exception
            CheckIfLastApprover = False
        End Try
    End Function
    Protected Sub SendEmailToNextApprover(ByVal Status As String, ByVal IsHRApproval As Boolean)
        Dim Mailstatus As String = ""
        Try
            Dim strStatus As String = ""
            Dim strStatusDesc As String = ""
            If Status = "A" Then
                strStatus = "A"
                strStatusDesc = "Approved"
            ElseIf Status = "R" Then
                strStatus = "R"
                strStatusDesc = "Rejected"
            Else
                Exit Sub
            End If

            Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim dt As New DataTable

            Dim Param(8) As SqlClient.SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@EMP_ID", hfEmpId.Value.Trim, SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@DOC_ID", ViewState("EREG_ID"), SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@DOC_TYPE", "RESGN", SqlDbType.VarChar)
            Param(4) = Mainclass.CreateSqlParameter("@status", strStatus, SqlDbType.VarChar)
            Param(5) = Mainclass.CreateSqlParameter("@IsFinalApproval", False, SqlDbType.Bit, True)
            Param(6) = Mainclass.CreateSqlParameter("@ALLAPPROVED", False, SqlDbType.Bit, True)
            Param(7) = Mainclass.CreateSqlParameter("@APS_ID", 0, SqlDbType.Int)
            Param(8) = Mainclass.CreateSqlParameter("@HRAPPROVAL", IsHRApproval, SqlDbType.Bit)
            dt = Mainclass.getDataTable("getNextApproverDetails", Param, ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim ToEmailId As String = ""
            If dt.Rows.Count > 0 Then
                ToEmailId = dt.Rows(0).Item("ApproverEMailID").ToString
            End If
            If ToEmailId = "" Then
                ToEmailId = "system@gemseducation.com"
            End If
            Dim fromemailid = "system@gemseducation.com"
            Dim sb As New StringBuilder
            Dim MainText As String = ""
            Dim Subject As String = ""
            If dt.Rows.Count > 0 Then
                If Status = "A" Then
                    Subject = "ONLINE RESIGNATION APPROVAL"
                    MainText = "An application for Resignation submitted by " & dt.Rows(0).Item("EMPName").ToString() & _
                        "(" & dt.Rows(0).Item("DPT_DESCR").ToString() & ")is pending for approval at your desk "

                    sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                    sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & dt.Rows(0).Item("ApproverName").ToString() & " </td></tr>")
                    sb.Append("<tr><td ><br /><br /></td></tr>")
                    sb.Append("<tr><td><b>SUB:<b> ONLINE RESIGNATION APPROVAL </td></tr>")
                    sb.Append("<tr><td>" & MainText & "</td></tr>")
                    sb.Append("<tr><td>To view and Approve the details ,please click on the following link.<a href='https://www.gemslearninggateway.com/Pages/Default.aspx' >https://www.gemslearninggateway.com/Pages/Default.aspx</a></td></tr>")

                    sb.Append("<tr><td ><br /><br />Thanks,</td></tr>")
                    sb.Append("<tr><td><b>Team OASIS.</b> </td></tr>")
                    sb.Append("<tr><td></td></tr>")
                    sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
                    sb.Append("<tr></tr><tr></tr>")
                    sb.Append("</table>")
                ElseIf Status = "R" Then
                    Subject = "REJECTION OF RESIGNATION SUBMITTED"
                    MainText = "REJECTION OF RESIGNATION SUBMITTED."

                    sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                    sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & dt.Rows(0).Item("EMPName").ToString() & " </td></tr>")
                    sb.Append("<tr><td ><br /><br /></td></tr>")
                    sb.Append("<tr><td><b>SUB:<b> REJECTION OF RESIGNATION SUBMITTED </td></tr>")
                    'sb.Append("<tr><td>" & MainText & "</td></tr>")
                    sb.Append("<tr><td>Your application for Resignation submitted  is being rejected by your higher authority.</td></tr>")

                    sb.Append("<tr><td ><br /><br />Thanks,</td></tr>")
                    sb.Append("<tr><td><b>Team OASIS.</b> </td></tr>")
                    sb.Append("<tr><td></td></tr>")
                    sb.Append("<tr><td>This is an Automated mail from GEMS OASIS. </td></tr>")
                    sb.Append("<tr></tr><tr></tr>")
                    sb.Append("</table>")
                End If
                Dim ds2 As New DataSet
                ds2 = GetCommunicationSettings()

                Dim username = ""
                Dim password = ""
                Dim port = ""
                Dim host = ""

                If ds2.Tables(0).Rows.Count > 0 Then
                    username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                    password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                    port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                    host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                End If
                '  Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, sb.ToString, username, password, host, port, 0, False)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Function GetCommunicationSettings() As DataSet
        Dim ds As DataSet
        Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = "select * from dbo.BSU_COMMUNICATION_M where BSC_BSU_ID = " & Session("sBsuid") & " and BSC_TYPE ='COM' "
        ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
        Return ds
    End Function


    'Protected Sub chkFFSOutsideSystem_CheckedChanged(sender As Object, e As EventArgs) Handles chkFFSOutsideSystem.CheckedChanged
    '    If chkFFSOutsideSystem.Checked = True And Session("BSU_IsOnDAX") = 1 Then
    '        btnSyncToDAX.Visible = True
    '    End If
    'End Sub
    Private Function Get6DigitRandomNumber() As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Dim Min As Integer = 111111
        Dim Max As Integer = 999999
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max)
    End Function

    'Private Function GetUniqueCompensationId(ByVal PosId As String) As String
    '    Dim Min As Integer = 111
    '    'Dim Max As Integer = 999999
    '    Dim Max As Integer = 9999
    '    Static Generator As System.Random = New System.Random()
    '    Return PosId & "-FNST-" & Generator.Next(Min, Max).ToString
    '    'Return Generator.Next(Min, Max).ToString
    'End Function
    Private Function GetUniqueCompensationId(ByVal EmpId As String, ByVal tr2 As SqlTransaction) As String
        Dim Min As Integer = 15000
        'Dim Max As Integer = 999999
        'Dim Max As Integer = 9999
        Dim Max As Integer = 999999999
        Static Generator As System.Random = New System.Random()
        'Return EmpId & "-Comp-" & Generator.Next(Min, Max).ToString
        'Select Case Me.hfCompAction.Value
        '    Case "offer"
        '        Return "OffComp-" & Generator.Next(Min, Max).ToString
        '    Case "promotion"
        '        Return "ProComp-" & Generator.Next(Min, Max).ToString
        '    Case "transfer"
        '        Return "TraComp-" & Generator.Next(Min, Max).ToString
        '    Case "revision"
        '        Return "RevComp-" & Generator.Next(Min, Max).ToString
        '    Case Else
        '        Return "EmpComp-" & Generator.Next(Min, Max).ToString
        'End Select
        'Return (Generator.Next(Min, Max)).ToString.PadLeft(9, "0")
        Dim ds As New DataSet
        Dim compId As String = ""
        Try
            Dim str_query As String = "exec GENERATECOMP_SEQUENCE 'Bulk' ,'" & EmpId & "'"
            ds = SqlHelper.ExecuteDataset(tr2, CommandType.Text, str_query)
            If Not ds.Tables(0) Is Nothing Then
                compId = ds.Tables(0).Rows(0)("SequenceNumber").ToString
            End If
            If compId <> "" Then
                Return compId
            Else
                lblError.Text = "Error in generating unique id for Employee: " & EmpId
                Return "00"
            End If
        Catch exc As Exception
            lblError.Text = "Error in generating unique id for Employee: " & EmpId
            Return "00"

        Finally

        End Try
    End Function
    Private Sub SaveAllData()
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim conn As SqlConnection = New SqlConnection(str_conn)
        Dim tr As SqlTransaction
        Dim retValue As Boolean = False
        Dim ErrorOccured As Boolean = False
        Dim ServiceErrorMsg As String = ""
        Try


            conn.Open()
            tr = conn.BeginTransaction


            retValue = Me.SendToDAX_Employee(tr, Me.Get6DigitRandomNumber(), txt_employeeno.Text, _
                                Me.GetUniqueCompensationId(hfEmpId.Value, tr), _
                                Now.Date, Format(Now.Date.AddYears(1), "dd/MMM/yyyy"), ServiceErrorMsg)

            If retValue = True Then
                tr.Commit()

                lblError.Text = "Data synched successfully!!!"
                Me.btnSyncToDAX.Visible = False
                'Me.btnDelete.Visible = False
            Else
                'tr.Rollback()
                ErrorOccured = True
                Exit Sub
            End If

        Catch ex As Exception
            tr.Rollback()
            If ServiceErrorMsg = "" Then
                lblError.Text = "Error occured while syncing the data"
            Else
                lblError.Text = ServiceErrorMsg
            End If

            'Me.LogError(ex, "EmpFinalSettlement.apsx", "SaveAllData")

        Finally
            If Not conn.State = ConnectionState.Closed Then conn.Close()
        End Try

    End Sub
    Private Function SendToDAX_Employee(ByVal tr As SqlTransaction, ByVal RecId As Integer, ByVal EmpId As String, CompensationId As String, ValidFrom As Date, ValidTo As Date, ByRef ServiceErrorMsg As String) As Boolean
        SendToDAX_Employee = False
        'Private Sub SendToDAX(ByVal RecId As Integer, ByVal PosId As String, ActionState As String, CompensationId As String, GradeId As String, ValidFrom As String, ValidTo As String, gvCompensation As GridView)
        Try
            Dim client As New EmployeeCompensationService.ALE_EmpCompServiceClient


            client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
            client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString

            Dim context As New EmployeeCompensationService.CallContext
            'context.Company = "GCO" ' disable it when not in demo mode and pass actual legal entity
            context.Company = Me.hfCompany.Value
            If CompensationId = "00" Then
                tr.Rollback()
                Return False
            End If


            Dim EmpCompLinesCount As Integer = 0 ' gvCompensation.Rows.Count

            Dim EmpCompFactory As New EmployeeCompensationService.AxdALE_EmpComp
            Dim EmpComps(0) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
            Dim EmpComp As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation = Nothing
            Dim EmpCompLines(0) As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine
            Dim EmpCompLine As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing
            'Dim EmpCompRef As EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine = Nothing

            'Initialize and fill PosComp (header) details
            EmpComp = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensation
            EmpComp.RecId = RecId
            EmpComp.RecIdSpecified = True
            EmpComp.EmployeeId = txt_employeeno.Text
            EmpComp.HcmActionState = "AS000001" ''ActionState 'Ignore this comment. As of now, skipping this as any value passed for it is throwing error message. Need to check with them what valid value we can pass

            EmpComp.CompensationId = CompensationId
            EmpComp.Description = "Full and Final settlement"
            EmpComp.ValidFrom = ValidFrom
            EmpComp.ValidFromSpecified = True
            EmpComp.ValidTo = ValidTo 'Pass some distant future date
            EmpComp.ValidToSpecified = True
            ' EmpComp.ActionType = "Terminate Empl"
            EmpComp.ActionType = "Termination" 'As confirmed by Puneet on skype
            EmpComp.class = "entity" 'This value is hard coded as suggested in the service documentation

            'Now fill up the EmpCompLines() array with all EmpCompLine. Compensation details.

            EmpCompLine = New EmployeeCompensationService.AxdEntity_ALE_EmployeeCompensationLine

            EmpCompLine.RecId = Get6DigitRandomNumber()
            EmpCompLine.RecIdSpecified = True

            EmpCompLine.EmployeeCompensationRef = RecId
            EmpCompLine.EmployeeCompensationRefSpecified = True

            EmpCompLine.Amount = 0 ' Me.GetActualAmount(CType(gvCompensation.Rows(i).FindControl("lbless_earned"), Label).Text)
            EmpCompLine.AmountSpecified = True
            EmpCompLine.CurrencyCode = "AED" 'Me.ddlCurrency.SelectedValue
            EmpCompLine.ComponentCode = "BASIC" ' CType(gvCompensation.Rows(i).FindControl("lblern_Code"), Label).Text
            EmpCompLine.Description = "Final Settlement Outside System" 'CType(gvCompensation.Rows(i).FindControl("lblern_descr"), Label).Text
            EmpCompLine.FromDate = ValidFrom
            EmpCompLine.FromDateSpecified = True
            'EmpCompLine.PayType = IIf(CType(gvCompensation.Rows(i).FindControl("ddlSalFrequency"), DropDownList).SelectedValue = "M", EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Monthly, EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.Annual)
            EmpCompLine.PayType = EmployeeCompensationService.AxdExtType_ALE_HcmCompPayType.NonCyclical
            EmpCompLine.PayTypeSpecified = True
            EmpCompLine.class = "entity" 'This value is hard coded as suggested in the service documentation

            EmpCompLines(0) = EmpCompLine


            'Now assign the EmpCompLines() array to EmpComp
            EmpComp.ALE_EmployeeCompensationLine = EmpCompLines

            'Now add EmpComp to EmpComps array
            EmpComps(0) = EmpComp

            'Now assing EmpComps to EmpComp field of EmpCompFactory
            EmpCompFactory.ALE_EmployeeCompensation = EmpComps

            Dim ent(0) As EmployeeCompensationService.EntityKey
            ent = client.create(context, EmpCompFactory)

            If ent.Length > 0 Then
                Dim tmp1, tmp2 As String
                tmp1 = ent(0).KeyData(0).Field
                tmp2 = ent(0).KeyData(0).Value
                Dim DAXRecId As String = tmp2
                Dim UpdateQuery As String = "SAVE_SALARY_FINAL_SETTLEMENT_DAX_ID " & Me.hfEmpId.Value & ", '" & Me.hfBsuId.Value & "', '" & DAXRecId & "'"
                SqlHelper.ExecuteNonQuery(tr, CommandType.Text, UpdateQuery)
            End If


            Return True

        Catch ex As Exception
            'Me.ShowErrorMessage("Following error occured in DAX service: " & ex.Message)
            ServiceErrorMsg = "Following error occured in DAX service: " & ex.Message
            Throw ex 'throw back the error to the caller and let it be handled there
            Return False
        Finally
        End Try
    End Function

    Protected Sub btnSyncToDAX_Click(sender As Object, e As EventArgs) Handles btnSyncToDAX.Click
        Me.hfBsuId.Value = Me.GetOasisEmpBsuId(Me.hfEmpId.Value)
        Me.hfCompany.Value = Me.GetAXBsuIdFromOasisBsuId(Me.hfBsuId.Value)
        If ViewState("IsProcessed") <> "0" Then
            lblError.Text = "This record is already synched to DAX"
            Exit Sub
        End If
        If Me.hfBsuId.Value = Nothing Then
            Me.btnSyncToDAX.Visible = False
            lblError.Text = "Invalid business unit"
            Exit Sub
        End If
        SaveAllData()
    End Sub
    Private Function GetOasisEmpBsuId(ByVal Emp_Id As Integer) As String
        GetOasisEmpBsuId = Nothing
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Try
            str_query = "SELECT EMP_BSU_ID FROM OASIS.dbo.EMPLOYEE_M WITH (NOLOCK) WHERE EMP_ID = " & Emp_Id
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            lblError.Text = "Error occured getting employee business unit"
        End Try
    End Function
    Private Function GetAXBsuIdFromOasisBsuId(ByVal Bsu_Id As String) As String
        GetAXBsuIdFromOasisBsuId = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_INTEGRATIONConnectionString
        Dim str_query As String
        Try
            str_query = "EXEC dbo.GetAXBsuIdFromOasisBsuId  @BSU_ID = '" & Bsu_Id & "'"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            '  LogError(ex, "EmpFinalSettlement.aspx", "GetAXBsuIdFromOasisBsuId")
            lblError.Text = "Error getting legal entity details"
        End Try
    End Function


End Class
