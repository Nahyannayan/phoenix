<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Job.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Administrator_Job" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            New Job
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <asp:Panel ID="Panel1" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="20%"><span class="field-label">Category</span></td>
                            <td width="30%">
                                <asp:DropDownList ID="ddcategory" runat="server" CausesValidation="True">
                                </asp:DropDownList></td>
                            <td width="20%"><span class="field-label">Job Title</span></td>
                            <td width="30%">
                                <asp:TextBox ID="txtjobdes" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><span class="field-label">Description</span></td>
                            <td colspan="3">
                                <asp:TextBox ID="txtInfo" runat="server" Width="93%" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><span class="field-label">Business Unit</span></td>
                            <td align="left" colspan="3">
                                <%--   <asp:ListBox ID="listUnits" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                                <asp:GridView ID="GrdJobBsu" runat="server" AutoGenerateColumns="false" Width="93%" CssClass="table table-row table-bordered">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Checked">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBSU" runat="server" />
                                                <asp:HiddenField ID="HiddenBsuID" runat="server" Value='<%# Eval("BSU_ID") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Unit">
                                            <ItemTemplate>
                                                <%#Eval("BSU_NAME")%>
                                            </ItemTemplate>
                                            <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reported By">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReportedBy" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vacant">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtvacant" runat="server"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FT1" FilterType="Numbers" TargetControlID="txtvacant" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                            <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Close Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtclosedate" runat="server"></asp:TextBox>
                                                <asp:Image ID="Image1" ImageUrl="~/Images/calendar.gif" runat="server" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                    PopupButtonID="Image1" TargetControlID="txtclosedate">
                                                </ajaxToolkit:CalendarExtender>
                                            </ItemTemplate>
                                            <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                    <RowStyle CssClass="griditem" Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                </asp:GridView>



                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnsave" CssClass="button" runat="server" Text="Save" ValidationGroup="vg1" /></td>
                        </tr>
                    </table>
                    <br />
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="listUnits"
                    Display="None" ErrorMessage="Please Select BSU" SetFocusOnError="True"></asp:RequiredFieldValidator>--%><asp:RequiredFieldValidator
                        ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddcategory" Display="None" ValidationGroup="vg1"
                        ErrorMessage="Please Select a Category" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtjobdes" Display="None" ValidationGroup="vg1"
                        ErrorMessage="Please Enter Job Description" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <%--   <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                    TargetControlID="RequiredFieldValidator1">
                </ajaxToolkit:ValidatorCalloutExtender>--%>
                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                        TargetControlID="RequiredFieldValidator2">
                    </ajaxToolkit:ValidatorCalloutExtender>
                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
                        TargetControlID="RequiredFieldValidator3">
                    </ajaxToolkit:ValidatorCalloutExtender>
                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                </asp:Panel>
                &nbsp;<div>
                </div>
                <asp:Panel ID="Panel2" runat="server" Visible="False" Width="100%">
                    <center>
                    <table  width="100%">
                        <tr>
                            <td>
                                Jobs Successfully Saved, Do you want to Shortlist Candidates for this job</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnyes" runat="server" CssClass="button" CausesValidation="False" Text="Yes"   />
                                <asp:Button ID="btnno" runat="server" CssClass="button" CausesValidation="False" OnClick="btnno_Click"
                                    Text="No"  /></td>
                        </tr>
                    </table>
                </center>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
