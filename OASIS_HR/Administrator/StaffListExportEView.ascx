<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StaffListExportEView.ascx.vb" Inherits="OASIS_HR_Administrator_StaffListExportEView" %>

<script language="javascript" type="text/javascript">
    function change_chk_state(chkThis) {
        var ids = chkThis.id

        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                document.forms[0].elements[i].checked = state;

            }
        }
        //document.forms[0].submit()
        return false;
    }


    function uncheckall(chkThis) {
        var ids = chkThis.id
        alert(ids)
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent

        var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child

        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }
        var uncheckflag = 0

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                if (currentid.indexOf(value) == -1) {
                    if (document.forms[0].elements[i].checked == false) {
                        uncheckflag = 1
                    }
                }
            }
        }

        if (uncheckflag == 1) {
            // uncheck parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }
        else {
            // Check parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = true;

                }
            }
            // document.forms[0].submit();
        }


        return false;
    }


    function change_chk_stateg(chkThis) {
        //             var a=chkThis.id
        //             alert(a)

        var chk_state = !chkThis.checked;


        var a = document.getElementsByName('').length

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>

<table width="100%">
    <tr>

        <td>
            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>

    </tr>
    <tr>
        <td>
            <table cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg" align="left">Select the field to export</td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel2" runat="server"
                            HorizontalAlign="Left" ScrollBars="Auto">

                            <asp:GridView ID="gvSelectedField" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No search condition added yet for Part C."
                                SkinID="GridViewView" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select All">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ckbheader" runat="server" Text="SelectAll" OnCheckedChanged="ckbheader_CheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckbSelect" runat="server" OnCheckedChanged="ckbSelect_CheckedChanged" AutoPostBack="false"></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Field Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSelectfield" runat="server" Text='<%# Bind("SFT_DISP_DES") %>'></asp:Label>
                                            <br />
                                            <asp:HiddenField ID="HF_selectedfield" runat="server" Value='<%# Bind("STF_DISP_CASE") %>'></asp:HiddenField>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
      
        <td align="center">
            <asp:Button ID="btnExport" runat="server" CssClass="button" OnClick="btnExport_Click"
                Text="Export" /></td>
       
    </tr>
</table>
