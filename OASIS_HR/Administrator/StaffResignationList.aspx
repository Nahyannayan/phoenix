<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffResignationList.aspx.vb" Inherits="OASIS_HR_Administrator_StaffResignationList" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Employee Resignation List
        </div>
        <div class="card-body ">
            <div class="table-responsive">


                <table width="100%">
                    <tr>
                        <td class="matters" valign="top" width="50%" align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <input id="h_Grid" runat="server" type="hidden" value="top" />
                            <a id='top'></a></td>
                        <td align="right">
                            <asp:RadioButton ID="rbOpen" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Open" Checked="True" />
                            <asp:RadioButton ID="rbForword" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Forwarded" OnCheckedChanged="RdForword_CheckedChanged" />
                            <asp:RadioButton ID="rbApprove" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Approved" OnCheckedChanged="RdApprove_CheckedChanged" />
                            <asp:RadioButton ID="rbHold" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Hold" />
                            <asp:RadioButton ID="rbReject" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="Rejected" />
                            <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" CssClass="field-label"
                                GroupName="POST" Text="All" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>

                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left">

                                        <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%%">

                                            <tr>
                                                <td align="center" class="matters" colspan="4" valign="top">
                                                    <asp:GridView ID="gvEmpResignationList" runat="server" AllowPaging="True"
                                                        AutoGenerateColumns="False" Width="100%" CaptionAlign="Top" PageSize="15"
                                                        CssClass="table table-row table-bordered" OnPageIndexChanging="gvEmpResignationList_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Employee No">

                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EmpNo") %>'></asp:Label>
                                                                    <asp:HiddenField ID="HF_Emp_ID" runat="server" Value='<%# BIND("Emp_ID") %>' />
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblEmpNoH" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                        Text="Employee No"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtEmpNo" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpNo" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchEmpNo_Click" />

                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee Name" ShowHeader="False">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblEmpNameH" runat="server" CssClass="gridheader_text" Text="Employee Name"
                                                             ></asp:Label><br />
                                                                    <asp:TextBox ID="txtEmpName" runat="server"  ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchEmpName_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblEmpName" runat="server" CausesValidation="False" CommandName="Selected" Text='<%# Eval("EmployeeName") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Last Working Date">

                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblwrkdate" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                        Text="Last Working Date"></asp:Label><br />
                                                                    <asp:TextBox ID="txtLastWrk_date" runat="server"  ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchwrkdate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchwrkdate_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmp_Lastwrkdate" runat="server"
                                                                        Text='<%# Bind("EREG_LASTWORKINGDATE", "{0:dd/MMM/yyyy}") %>'
                                                                        DataFormatString="{0:dd/MMM/yyyy}"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Resignation Date">

                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRsgDate" runat="server"
                                                                        Text='<%# Bind("EREG_REGDATE", "{0:dd/MMM/yyyy}") %>'
                                                                        DataFormatString="{0:dd/MMM/yyyy}"></asp:Label>

                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblRsgDate_H" runat="server" CssClass="gridheader_text" Text="Resignation Date"  ></asp:Label><br />
                                                                    <asp:TextBox ID="txtRsgDate_search" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchRsgDate" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchRsgDate_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Bussiness Unit Name" Visible="False">

                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblSchoolNameH" runat="server" CssClass="gridheader_text" Text="Bussiness Unit Name"
                                                                        ></asp:Label><br />

                                                                    <asp:TextBox ID="txtSchoolName" runat="server"  ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchBsuName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchBsuName_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbBsuName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="lnkEdit" runat="server">View</asp:HyperLink>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Approve">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEREG_ID" runat="server" Text='<%# Bind("EREG_ID") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("EREG_ApprStatus") %>' Visible="false"></asp:Label>
                                                                    <asp:HyperLink ID="lnkApprove" runat="server">Approve</asp:HyperLink>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="Status" HeaderText="Status"
                                                                SortExpression="Status" />

                                                        </Columns>

                                                        <EditRowStyle BorderColor="#0099FF" BorderStyle="Solid" BorderWidth="1px" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" BorderStyle="Solid"
                                                            BorderWidth="1px" />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                            <input id="h_Selected_menu_0" runat="server" type="hidden" value="-1" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

