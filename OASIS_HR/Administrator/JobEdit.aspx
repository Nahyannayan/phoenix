<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JobEdit.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="OASIS_HR_Administrator_JobEdit" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Edit Job
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td width="20%"><span class="field-label">Job Title</span></td>
                        <td width="30%">
                            <asp:TextBox ID="txtjobtitle" runat="server"></asp:TextBox></td>
                        <td width="20%"><span class="field-label">Description</span></td>
                        <td width="30%">
                            <asp:TextBox ID="txtjobdescription" runat="server" SkinID="MultiText" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td><span class="field-label">Business Unit</span></td>
                        <td colspan="3">
                            <asp:GridView ID="GrdJobBsuEdit" AutoGenerateColumns="false" Width="93%" runat="server" CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Checked">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBSU" Checked='<%# Eval("CHECK_B") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenBsuID" Value='<%# Eval("BSU_ID") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenJobCode" Value='<%# Eval("JOB_CODE") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenRecord_id" Value='<%# Eval("RECORD_ID") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenCheck" Value='<%# Eval("CHECK_B") %>' runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                        <ItemTemplate><%# Eval("BSU_NAME") %></ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reported By">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReportedBy" Text='<%# Eval("REPORTED_BY") %>' runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vacant">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtvacant" Text='<%# Eval("VACANT_NO") %>' runat="server"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FT1" FilterType="Numbers" TargetControlID="txtvacant" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Close Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtclosedate" Text='<%# Eval("CLOSE_DATE") %>' runat="server"></asp:TextBox>
                                            <asp:Image ID="Image1" ImageUrl="~/Images/calendar.gif" runat="server" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="Image1" TargetControlID="txtclosedate">
                                            </ajaxToolkit:CalendarExtender>

                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnupdate" runat="server" Text="Update" CssClass="button" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>



                </table>
                <asp:HiddenField ID="HiddenJobId" runat="server" />
                <asp:HiddenField ID="HiddenCategoryId" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>
