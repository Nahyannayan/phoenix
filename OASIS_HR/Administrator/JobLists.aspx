<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JobLists.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="OASIS_HR_Administrator_JobLists" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Job List
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:LinkButton ID="LinkAddNew" runat="server" OnClick="LinkAddNew_Click">Create New Jobs</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <asp:GridView ID="GrdJobsList" Width="100%" CssClass="table table-row table-bordered" AutoGenerateColumns="false" runat="server" OnRowDeleting="GrdJobsList_RowDeleting" OnRowCommand="GrdJobsList_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Job Code">
                                        <ItemTemplate><%# Eval("JOB_CODE") %></ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job Title">
                                        <ItemTemplate>
                                            <%# Eval("JOB_DES") %>
                                            <asp:HiddenField ID="HiddenJobID" Value='<%# Eval("JOB_CODE") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenCategoryId" Value='<%# Eval("CATEGORY_ID") %>' runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job Description">
                                        <ItemTemplate>
                                            <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                                            <asp:Panel ID="T12Panel1" runat="server">
                                                <%#Eval("JOB_INFORMATION")%>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                TextLabelID="T12lblview">
                                            </ajaxToolkit:CollapsiblePanelExtender>

                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="left" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job Category">
                                        <ItemTemplate><%# Eval("CATEGORY_DES") %></ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <%--  <asp:TemplateField HeaderText="Entry Date">
            <ItemTemplate><%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%></ItemTemplate>
            </asp:TemplateField>
           <asp:TemplateField HeaderText="Close Date">
            <ItemTemplate><%#Eval("CLOSE_DATE", "{0:dd/MMM/yyyy}")%></ItemTemplate>
            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Shortlist">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkShortlist" CommandName="select" runat="server">Shortlist</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkEdit" CommandName="edit" runat="server">Edit</asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkDelete" CommandName="delete" runat="server">Delete</asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="Are you sure?" TargetControlID="LinkDelete" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
