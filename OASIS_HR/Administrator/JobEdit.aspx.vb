Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Oasis_Administrator
Partial Class OASIS_HR_Administrator_JobEdit
    Inherits System.Web.UI.Page
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                CheckMenuRights()
                Dim EncDec As New Encryption64
                HiddenCategoryId.Value = EncDec.Decrypt(Request.QueryString("Category_Id").Replace(" ", "+"))
                HiddenJobId.Value = EncDec.Decrypt(Request.QueryString("Job_Code").Replace(" ", "+"))
                BindTextbox()
                BindGrid()
            Catch ex As Exception
                Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
                Response.Redirect("JobLists.aspx" & mInfo)
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



    End Sub
    Public Sub BindTextbox()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim srt_query = "SELECT DISTINCT JOB_CODE,JOB_DES,JOB_INFORMATION FROM JOBS WHERE JOB_CODE='" & Convert.ToInt32(HiddenJobId.Value) & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        txtjobtitle.Text = ds.Tables(0).Rows(0).Item("JOB_DES").ToString()
        txtjobdescription.Text = ds.Tables(0).Rows(0).Item("JOB_INFORMATION").ToString()

    End Sub
    Public Sub BindGrid()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim srt_query = "SELECT B.BSU_ID,BSU_NAME,JOBS.RECORD_ID,JOB_CODE,REPORTED_BY,VACANT_NO,CLOSE_DATE,CONVERT(BIT,(SELECT CASE WHEN JOB_CODE IS NULL THEN 0 ELSE 1  END))AS CHECK_B FROM " & _
                        " OASIS.DBO.BUSINESSUNIT_M  INNER JOIN BSU B ON B.BSU_ID= OASIS.DBO.BUSINESSUNIT_M.BSU_ID " & _
                        " LEFT JOIN JOBS ON OASIS.DBO.BUSINESSUNIT_M.BSU_ID=JOBS.BSU AND JOBS.JOB_CODE='" & Convert.ToInt32(HiddenJobId.Value) & "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        GrdJobBsuEdit.DataSource = ds
        GrdJobBsuEdit.DataBind()

    End Sub

    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        lblmessage.Text = ""
        Dim flag = 0
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@JOB_DES", txtjobtitle.Text.Trim())
        pParms(1) = New SqlClient.SqlParameter("@JOB_INFO", txtjobdescription.Text.Trim())
        pParms(2) = New SqlClient.SqlParameter("@JOB_CODE", Convert.ToInt32(HiddenJobId.Value))
        pParms(3) = New SqlClient.SqlParameter("@OPTION", 4)
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_JOBS", pParms)
        If lblmessage.Text = "0" Then '' Title Updated
            For Each row As GridViewRow In GrdJobBsuEdit.Rows
                Dim checkbox As CheckBox = DirectCast(row.FindControl("CheckBSU"), CheckBox)
                Dim HiddenCheck As HiddenField = DirectCast(row.FindControl("HiddenCheck"), HiddenField)
                Dim HiddenRecord_id As HiddenField = DirectCast(row.FindControl("HiddenRecord_id"), HiddenField)
                Dim HiddenJobCode As HiddenField = DirectCast(row.FindControl("HiddenJobCode"), HiddenField)
                Dim HiddenBsuID As HiddenField = DirectCast(row.FindControl("HiddenBsuID"), HiddenField)
                If checkbox.Checked = True And HiddenCheck.Value = True Then '' Update the record- Reported by, vacant, close date.
                    Dim reportedby = DirectCast(row.FindControl("txtReportedBy"), TextBox).Text
                    Dim vacant = DirectCast(row.FindControl("txtvacant"), TextBox).Text
                    Dim closedate = DirectCast(row.FindControl("txtclosedate"), TextBox).Text
                    If vacant = "" Then
                        vacant = Nothing
                    End If
                    OasisAdministrator.SaveJobInformation(Convert.ToInt32(HiddenJobId.Value), HiddenBsuID.Value.Trim(), reportedby, vacant, closedate)
                End If
                If checkbox.Checked = True And HiddenCheck.Value = False Then '' insert new
                    Dim reportedby = DirectCast(row.FindControl("txtReportedBy"), TextBox).Text
                    Dim vacant = DirectCast(row.FindControl("txtvacant"), TextBox).Text
                    Dim closedate = DirectCast(row.FindControl("txtclosedate"), TextBox).Text
                    If vacant = "" Then
                        vacant = Nothing
                    End If
                    OasisAdministrator.SaveJobsEdit(Convert.ToInt32(HiddenJobId.Value), HiddenBsuID.Value.Trim(), txtjobtitle.Text.Trim(), txtjobdescription.Text.Trim(), HiddenCategoryId.Value, reportedby, vacant, closedate)
                End If
                If checkbox.Checked = False And HiddenCheck.Value = True Then '' Delete Old
                    Dim pParms1(4) As SqlClient.SqlParameter
                    pParms1(0) = New SqlClient.SqlParameter("@JOB_CODE", HiddenJobId.Value)
                    pParms1(1) = New SqlClient.SqlParameter("@BSU", HiddenBsuID.Value.Trim())
                    pParms1(2) = New SqlClient.SqlParameter("@RECORD_ID", HiddenRecord_id.Value.Trim())
                    pParms1(3) = New SqlClient.SqlParameter("@OPTION", 3)
                    lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_JOBS", pParms1)
                    If lblmessage.Text <> "0" Then
                        row.BackColor = Drawing.Color.Red
                        lblmessage.Text = ""
                        flag = 1
                        checkbox.Checked = True
                    End If
                End If
            Next
        End If
        If flag = 1 Then
            lblmessage.Text = "Row Marked as Red cannot be deleted.Candidates can be in one of the following " & _
                              "<br> 1.Interview Scheduled " & _
                              "<br> 2.Interview Cleared " & _
                              "<br> 3.Offered " & _
                              "<br> 4.Offer Accepted By Applicant " & _
                              "<br> 5.Applicant Joined "

        End If
        If lblmessage.Text = "0" Then
            lblmessage.Text = "Updated"
            BindGrid()
        End If

    End Sub
End Class
