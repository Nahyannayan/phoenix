<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffListExportExcel_View.aspx.vb" Inherits="OASIS_HR_Administrator_StaffListExportExcel_View" Title="Untitled Page" %>

<%@ Register Src="StaffListExportEView.ascx" TagName="StaffListExportEView" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Export Staff List
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:StaffListExportEView ID="StaffListExportEView1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

