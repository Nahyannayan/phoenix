<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffListExportExcel.aspx.vb" Inherits="OASIS_HR_Administrator_StaffListExportExcel" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--    <script language="javascript" src="../../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>--%>

    <script language="javascript" type="text/javascript">

        function ChangeAllCheckBoxStates(checkedBox, Cnt) {

            var ChkState = false;
            if (checkedBox.checked) {
                ChkState = true
            }

            for (var i = 0; i < Cnt; i++) {

                var ChkId = checkedBox.id

                ChkId = ChkId.substring(ChkId, ChkId.length - 1)
                ChkId = ChkId + (i + 1)
                document.getElementById(ChkId).checked = ChkState

            }
            //checkedBox.checked = false;
        }

        function ChangeAllCheckBox(checkedBox, Cnt) {

            var truecount = 0;
            var CheckSt = true;
            var FirstChkId = checkedBox.id
            var a = checkedBox.id.length
            var b = checkedBox.id.indexOf("|_") + 2
            FirstChkId = FirstChkId.substring(FirstChkId, FirstChkId.length - (a - b))
            FirstChkId = FirstChkId + (0)

            for (var i = 0; i < Cnt; i++) {
                var ChkId = checkedBox.id
                ChkId = ChkId.substring(ChkId, ChkId.length - (a - b))
                ChkId = ChkId + (i + 1)
                if (!document.getElementById(ChkId).checked == true) {
                    CheckSt = false;
                    document.getElementById(FirstChkId).checked = CheckSt;
                    truecount = 1
                    break;
                }
            }
            if (truecount == 0) {
                document.getElementById(FirstChkId).checked = CheckSt;
            }
        }


        <%--function GetBSUName() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("../../Payroll/Reports/Aspx/SelIDDESC.aspx?ID=BSU", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_BSUID.ClientID %>').value = result;//NameandCode[0];
                document.forms[0].submit();
            }
            else {
                return false;
            }
        }--%>
        function client_OnTreeNodeChecked() {

            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }




        function Select_Cond() {
            var e = document.getElementById('<%= ddlCond1.clientID %>');
            var ddlTxt = e.options[e.selectedIndex].text;
            var CheckStr = ddlTxt.toUpperCase();
            var strTemp = document.getElementById('<%=h_selected_menu_1.ClientID %>').value;
            var strval = strTemp.split('__');
            var TXT2 = document.getElementById('<%=txtCond2.ClientID %>');
            var IMG1 = document.getElementById('<%=imgCalendar1.ClientID %>');
            var IMG2 = document.getElementById('<%=imgCalendar2.ClientID %>');
            var val = strval[0];
            //alert(strval[0]);
            if (CheckStr.indexOf('DATE') != -1) {
                //if selected type is date then do this

                if (strval[0] == 'LI') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'none';
                }
                else if (strval[0] == 'NLI') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'none';
                }
                else if (strval[0] == 'SW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'none';
                }
                else if (strval[0] == 'NSW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'none';
                }
                else if (strval[0] == 'EW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'none';
                }
                else if (strval[0] == 'NEW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'none';
                }
                else if (strval[0] == 'BET') {
                    TXT2.style.display = 'inline';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'inline';
                }
            }
            else {
                IMG1.style.display = 'none';
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
            }
            if (strval[0] == 'BET') {
                val = 'LI';
                var path = '../../Images/operations/like.gif';

                document.getElementById('<%=getid1()%>').src = path;
                document.getElementById('<%=h_selected_menu_1.ClientID %>').value = val + '__' + path;
            }
        }


        function initialLoad_Cond() {

            var e = document.getElementById('<%= ddlCond1.clientID %>');
            var ddlTxt = e.options[e.selectedIndex].text;
            var CheckStr = ddlTxt.toUpperCase();
            var strTemp = document.getElementById('<%=h_selected_menu_1.ClientID %>').value;
        var strval = strTemp.split('__');
        Select_Cond();
        test1(strval[0]);

    }



    function test1(val) {
        var TXT2 = document.getElementById('<%=txtCond2.ClientID %>');
            var IMG2 = document.getElementById('<%=imgCalendar2.ClientID %>');
            var IMG1 = document.getElementById('<%=imgCalendar1.ClientID %>');
            var e = document.getElementById('<%= ddlCond1.clientID %>');
            var ddlTxt = e.options[e.selectedIndex].text;
            var CheckStr = ddlTxt.toUpperCase();

            var path;
            if (val == 'LI') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
                path = '../../Images/operations/like.gif';
            } else if (val == 'NLI') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
                path = '../../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
                path = '../../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
                path = '../../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
                path = '../../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                IMG2.style.display = 'none';
                path = '../../Images/operations/notendswith.gif';
            }
            else if (val == 'BET') {
                if (CheckStr.indexOf('DATE') != -1) {
                    TXT2.style.display = 'inline';
                    IMG1.style.display = 'inline';
                    IMG2.style.display = 'inline';
                    path = '../../Images/operations/between.gif';
                }
                else {
                    alert("Only applicable for date");
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'none';
                    IMG2.style.display = 'none';
                    val = 'LI'
                    path = '../../Images/operations/like.gif';
                }
            }

            document.getElementById("<%=getid1()%>").src = path;
         document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }





        function change_chk_state(chkThis) {
            var ids = chkThis.id

            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = state;

                }
            }
            //document.forms[0].submit()
            return false;
        }


        function uncheckall(chkThis) {
            var ids = chkThis.id
            alert(ids)
            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
            alert(value)
            var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
            alert(value1)

            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }
            var uncheckflag = 0

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                    if (currentid.indexOf(value) == -1) {
                        if (document.forms[0].elements[i].checked == false) {
                            uncheckflag = 1
                        }
                    }
                }
            }

            if (uncheckflag == 1) {
                // uncheck parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
            else {
                // Check parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = true;

                    }
                }
                // document.forms[0].submit();
            }


            return false;
        }


        function change_chk_stateg(chkThis) {
            //             var a=chkThis.id
            //             alert(a)

            var chk_state = !chkThis.checked;


            var a = document.getElementsByName('').length

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }




        function Cond1(event) {
            if (event.srcElement.type == 'checkbox') {
                var childNodes = event.srcElement.parentNode.parentNode.parentNode.parentNode.childNodes[0].childNodes[0].childNodes;
                for (i = 0; i < childNodes.length; ++i) {
                    if (childNodes[i].childNodes[0].type == 'checkbox')
                        childNodes[i].childNodes[0].checked = false;
                }
                event.srcElement.checked = true;
            }
        }

        function CondTALL(chkThis) {
            var chk_state = chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CBLbusnumber") != -1) {
                    // alert(document.forms[0].elements[i].value)
                    document.forms[0].elements[i].checked = chk_state;
                    // document.forms[0].elements[i].click();
                }
            }
        }


        //            var label = CHK.getElementsByTagName("label"); 

        //   for (var i=0;i<checkbox.length;i++) 

        //   { 

        //       if (checkbox[i].checked) 

        //       { 

        //           alert("Selected = " + label[i].innerHTML); 

        //       } 

        //   } 
    </script>
    <script>
        function GetBSUName() {
            var pMode;
            var NameandCode;
            url = "../../Payroll/Reports/Aspx/SelIDDESC.aspx?ID=BSU";
            var oWnd = radopen(url, "pop_bsu");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameandCode;
                document.getElementById('<%=h_BSUID.ClientID %>').value = NameandCode;
                document.forms[0].submit();
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_bsu" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>


    <asp:HiddenField ID="HiddenMenuCode" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfPartB" runat="server"></asp:HiddenField>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Export Staff List
        </div>
        <div class="card-body ">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                EnableViewState="False" ForeColor="" ValidationGroup="cert"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table runat="server" id="tbl2" width="100%">
                                <tr>
                                    <td class="title-bg" colspan="2">Part A</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Business Unit</span></td>
                                    <td>(Enter the BSU ID you want to Add to the Search and click on Add)<br />
                                        <asp:TextBox ID="txtBSUName" runat="server" AutoPostBack="True" OnTextChanged="txtBSUName_TextChanged"></asp:TextBox>
                                        <asp:LinkButton ID="lnlbtnAddBSUID" runat="server" OnClick="lnlbtnAddBSUID_Click">Add</asp:LinkButton>
                                        <asp:ImageButton ID="imgGetBSUName" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="GetBSUName();return false;"></asp:ImageButton>
                                        <asp:GridView ID="grdBSU" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-row table-bordered"
                                            PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="BSU ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBSUID" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="BSU_Name" HeaderText="BSU Name"></asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtngrdDelete" OnClick="lnkbtngrdDelete_Click" runat="server">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="h_BSUID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="2">

                                        <asp:Panel ID="Panel2" runat="server">
                                            Part B
                       <asp:Label ID="Label1" runat="server"></asp:Label>
                                            <asp:ImageButton ID="Image1" runat="server" AlternateText="(Show Details...)" ImageUrl="~/Images/password/expand.gif" />
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeDemo" runat="Server" CollapseControlID="Panel2"
                                            Collapsed="True" CollapsedImage="~/images/password/expand.gif" CollapsedText="(Show Details...)"
                                            ExpandControlID="Panel2" ExpandedImage="~/images/password/collapse.gif" ExpandedText="(Hide Details...)"
                                            ImageControlID="Image1" SkinID="CollapsiblePanelDemo" SuppressPostBack="true"
                                            TargetControlID="Panel1" TextLabelID="Label1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </td>

                                </tr>
                                <tr runat="server" id="trPartB">

                                    <td colspan="2">
                                        <asp:Panel ID="Panel1" runat="server">
                                            <asp:DataList ID="DataStaffFilterMain" runat="server" RepeatColumns="3" RepeatLayout="table" RepeatDirection="Horizontal"  
                                                ShowHeader="False">
                                                <ItemTemplate>

                                                    <table width="100%">
                                                        <tr>
                                                            <td width="100%" valign="middle" class="title-bg">
                                                                <span class="field-label">
                                                                    <%#Eval("STF_FT_DES")%>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="checkbox-list" style="width: 100%">
                                                                    <asp:Panel ID="PanelControl" runat="server">
                                                                    </asp:Panel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:HiddenField ID="HiddenSTF_FT_ID" runat="server" Value='<%# Eval("STF_FT_ID") %>' />
                                                    <asp:HiddenField ID="HiddenSTF_FT_PARAMETER_FIELD" runat="server" Value='<%# Eval("STF_FT_PARAMETER_FIELD") %>' />
                                                    <asp:HiddenField ID="HiddenSTF_FT_CONDITION" runat="server" Value='<%# Eval("STF_FT_CONDITION") %>' />
                                                    <asp:HiddenField ID="HiddenSTF_FT_VALUE_FIELD" runat="server" Value='<%# Eval("STF_FT_VALUE_FIELD") %>' />
                                                    <asp:HiddenField ID="HiddenSTF_FT_TEXT_FIELD" runat="server" Value='<%# Eval("STF_FT_TEXT_FIELD") %>' />
                                                    <asp:HiddenField ID="HiddenSTF_FT_FIELDS" runat="server" Value='<%# Eval("STF_FT_FIELDS") %>' />
                                                    <asp:HiddenField ID="HiddenSTF_FT_CASE" runat="server" Value='<%# Eval("STF_FT_CASE") %>' />
                                                    <asp:HiddenField ID="HiddenSTS_TABLES" runat="server" Value='<%# Eval("STS_TABLES") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30%" VerticalAlign="Top" />
                                                <HeaderStyle HorizontalAlign="Left" Width="30%" VerticalAlign="Top" />
                                            </asp:DataList>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title-bg" colspan="2">Part C</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblError2" CssClass="error"></asp:Label><br />
                                        <table width="100%">
                                            <tr>
                                                <td><span class="field-label">Condition</span> </td>
                                                <td align="left" valign="middle">
                                                    <asp:CheckBoxList ID="chkCond1" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" onClick="Cond1" CellPadding="2" CssClass="field-label">
                                                        <asp:ListItem>AND</asp:ListItem>
                                                        <asp:ListItem>OR</asp:ListItem>
                                                    </asp:CheckBoxList></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCond1" runat="server"></asp:DropDownList></td>
                                                <td>
                                                    <asp:TextBox ID="txtCond1" runat="server"></asp:TextBox><asp:ImageButton ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                    <asp:TextBox ID="txtCond2" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgCalendar2" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                    <span id="Div1">


                                                        <img id="mnu_1_img" alt="Menu" src="../../Images/operations/like.gif" runat="server" visible="true" />
                                                    </span>
                                                    <ajaxToolkit:PopupControlExtender ID="pce" runat="server" PopupControlID="dropmenu1" TargetControlID="mnu_1_img"></ajaxToolkit:PopupControlExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="5">
                                                    <asp:Button ID="btnPartCAdd" runat="server" CssClass="button" Text="Add" OnClick="btnPartCAdd_Click" CausesValidation="False" /></td>

                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <asp:GridView ID="gvPartC" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                        DataKeyNames="id" EmptyDataText="No search condition added yet for Part C." Width="100%" OnRowDeleting="gvPartC_RowDeleting">
                                                        <RowStyle CssClass="griditem" />
                                                        <EmptyDataRowStyle CssClass="gridheader" Wrap="True" HorizontalAlign="Center" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="OPER" HeaderText="Operator" ReadOnly="True"></asp:BoundField>
                                                            <asp:BoundField DataField="COLNAME" HeaderText="Field Name"></asp:BoundField>
                                                            <asp:BoundField DataField="COND" HeaderText="Condition"></asp:BoundField>
                                                            <asp:BoundField DataField="CONDVAL1" HeaderText="Value 1"></asp:BoundField>
                                                            <asp:BoundField DataField="CONDVAL2" HeaderText="Value 2"></asp:BoundField>
                                                            <asp:BoundField DataField="STRCOND" HeaderText="CONDSTR" Visible="False"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="DeleteBtn" runat="server" CommandName="Delete" CommandArgument='<%# Eval("id") %>'>
         Delete</asp:LinkButton>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <HeaderStyle CssClass="gridheader_new" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnProcess"
                                runat="server" CssClass="button" Text="Export Excel" OnClick="btnProcess_Click" ValidationGroup="cert" />&nbsp;<asp:Button
                                    ID="btnBack" runat="server" CausesValidation="False" CssClass="button" OnClick="btnBack_Click"
                                    Text="Cancel" />
                            <ajaxToolkit:CalendarExtender ID="cal1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar1"
                                TargetControlID="txtCond1">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="cal2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar2"
                                TargetControlID="txtCond2">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>


                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <div id="dropmenu1" class="panel-cover" width="160px">
                    <a href="javascript:test1('LI');">
                        <img alt="Any where" class="img_left" src="../../Images/operations/like.gif" />Any
            Where</a><br />
                    <a href="javascript:test1('NLI');">
                        <img alt="Not In" class="img_left" src="../../Images/operations/notlike.gif" />Not
                In</a><br />
                    <a href="javascript:test1('SW');">
                        <img alt="Starts With" class="img_left" src="../../Images/operations/startswith.gif" />Starts
                    With</a><br />
                    <a href="javascript:test1('NSW');">
                        <img alt="Like" class="img_left" src="../../Images/operations/notstartwith.gif" />Not
                        Start With</a>
                    <br />
                    <a href="javascript:test1('EW');">
                        <img alt="Like" class="img_left" src="../../Images/operations/endswith.gif" />Ends
                            With</a><br />
                    <a href="javascript:test1('NEW');">
                        <img alt="Like" class="img_left" src="../../Images/operations/notendswith.gif" />Not
                                Ends With</a><br />
                    <a href="javascript:test1('BET');">
                        <img alt="Like" class="img_left" src="../../Images/operations/between.gif" />Between Dates</a>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

