<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ReportsApplicantSearch.aspx.vb" Inherits="OASIS_HR_Administrator_Reports_ReportsApplicantSearch" %>

<%@ Register Src="../../UserControls/CR_Filter.ascx" TagName="CR_Filter" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTopTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <uc1:CR_Filter ID="CR_Filter1" runat="server" />

            </div>
        </div>
    </div>
</asp:Content>
