<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="ReportsStageCounts.aspx.vb" Inherits="OASIS_HR_Administrator_Reports_ReportsStageCounts" %>

<%@ Register Src="../../UserControls/CR_StageCountFilter.ascx" TagName="CR_StageCountFilter"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTopTitle" runat="server" Text=""></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div align="center">
                    <uc1:CR_StageCountFilter ID="CR_StageCountFilter1" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
