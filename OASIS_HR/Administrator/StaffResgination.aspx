<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StaffResgination.aspx.vb" Inherits="OASIS_HR_Administrator_StaffResgination" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<%@ Register Src="../UserControls/MailScheduleNotification.ascx" TagName="MailScheduleNotification"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js"></script>
    <script language="javascript" type="text/javascript">

        function ShowEmployee(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 500px; ";
            sFeatures += "dialogHeight: 500px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../../Accounts/accShowEmpDetail.aspx?id=' + mode + '&EMPNO=1';
            result = radopen(url, "pop_up");

           <%-- if (result=='' || result==undefined)
            {
                return false;        
            }
            NameandCode = result.split('___');  
            document.getElementById("<%=txt_employeeno.ClientID %>").value=NameandCode[0];
            document.getElementById("<%=hfEmpId.ClientID %>").value=NameandCode[1];
       --%>
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.NameandCode.split('||');
                document.getElementById("<%=txt_employeeno.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfEmpId.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txt_employeeno.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function Cond1(event) {

            if (event.srcElement.type == 'checkbox') {
                var childNodes = event.srcElement.parentNode.parentNode.parentNode.parentNode.childNodes[0].childNodes[0].childNodes;
                for (i = 0; i < childNodes.length; ++i) {
                    if (childNodes[i].childNodes[0].type == 'checkbox')
                        childNodes[i].childNodes[0].checked = false;
                }
                event.srcElement.checked = true;
            }
        }

        function showconfirm() {
            if (confirm('Please click OK to confirm F&F would be processed outside system?Record cannot be edited later.')) {
                aspnetForm.submit()
            }
            else {
                document.getElementById('ctl00_cphMasterpage_chkFFSOutsideSystem').checked = false

                //Use the HTML ID that was generated for the radio button you want to change back to
                document.getElementById('ctl00_cphMasterpage_chkFFSOutsideSystem').focus()
            }

        }
    </script>

    <%--<uc1:MailScheduleNotification ID="MailScheduleNotification1" runat="server" />--%>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Staff Resignation\Termination
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                    <tr>
                        <td align="left" width="100%">
                            <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />

                <table cellpadding="5" cellspacing="0"
                    style="width: 100%">
                    <%-- <tr class="subheader_img">
            <td colspan="3" align="left">Staff Resignation\Termination</td>
            <td align="left"></td>
        </tr>--%>
                    <tr>
                        <td align="left" style="width: 20%" ><span class="field-label">Employee Number</span></td>

                        <td align="left" style="width: 30%" >
                            <asp:TextBox ID="txt_employeeno" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Middle" ImageUrl="../../Images/forum_search.gif" OnClientClick="ShowEmployee('ERS'); return false;" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_employeeno"
                                ErrorMessage="*" ValidationGroup="gr"></asp:RequiredFieldValidator></td>
                        <td align="center"  colspan="2" rowspan="4">
                            <asp:ImageMap ID="imgEmployee" runat="server" Height="250px" Width="200px">
                            </asp:ImageMap>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Employee Name</span></td>
                      
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lbl_Empname" CssClass="field-value" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Department</span></td>
                      
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lbldpt" CssClass="field-value" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Designation</span></td>
                        
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lbldesg" CssClass="field-value" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Visa Business Unit</span></td>
                      
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lblVisaBSU" CssClass="field-value" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Working Business Unit</span></td>
                        
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lblWorkingBSU" CssClass="field-value" runat="server"></asp:Label></td>
                         <td align="left" style="width: 20%"><span class="field-label">Date Of Joining the Unit</span></td>
                      
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lblDOJBSU" CssClass="field-value" runat="server"></asp:Label></td>
                    </tr>
                   <%-- <tr>
                       
                    </tr>--%>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Date Of Joining the Group</span></td>
                        
                        <td align="left" style="width: 30%">
                            <asp:Label ID="lblDOJGroup" CssClass="field-value" runat="server"></asp:Label></td>
                        <td align="left" style="width: 20%"><span class="field-label">Type of Resignation</span></td>
                        
                        <td align="left" style="width: 30%">
                            <asp:RadioButtonList ID="RdbResgtype" runat="server" CssClass="radiobutton"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Value="4" Selected="True">Resigned</asp:ListItem>
                                <asp:ListItem Value="5">Terminated</asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <%--<tr>
                        
                    </tr>--%>
                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Last Working Date</span></td>
                        
                        <td align="left" style="width: 30%">
                            <asp:TextBox ID="txtlastwrkdate" runat="server"></asp:TextBox>
                            <asp:Image ID="img_lastworkingdate" runat="server"
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlastwrkdate"
                                ErrorMessage="*" ValidationGroup="gr"></asp:RequiredFieldValidator>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="img_lastworkingdate"
                                TargetControlID="txtlastwrkdate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            (dd/mmm/yyy)
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="txtlastwrkdate"
                     TargetControlID="txtlastwrkdate" Format="dd/MMM/yyyy">
                 </ajaxToolkit:CalendarExtender>
                        </td>
                        <td align="left" style="width: 20%"><span class="field-label">Resignation Date</span></td>

                        <td align="left" style="width: 30%">
                            <asp:TextBox ID="txtResgDate" runat="server"></asp:TextBox>
                            <asp:Image ID="img_resgDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtResgDate"
                                ErrorMessage="*" ValidationGroup="gr"></asp:RequiredFieldValidator>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="img_resgDate"
                                TargetControlID="txtResgDate" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            (dd/mmm/yyy)
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" PopupButtonID="txtResgDate"
                     TargetControlID="txtResgDate" Format="dd/MMM/yyyy">
                 </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                  <%--  <tr>
                        
                    </tr>--%>


                    <tr id="trActionDetails" runat="server" visible="False">
                        <td colspan="4" align="left">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAction" Text="Action Details:" runat="server"></asp:Label>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvActionDetail" runat="server" CssClass="table table-bordered table-row"
                                            AutoGenerateColumns="False" Width="100%" CaptionAlign="Top" PageSize="15"
                                            SkinID="GridViewView">
                                            <Columns>

                                                <asp:BoundField HeaderText="Employee ID" DataField="EMPNO"
                                                    SortExpression="EMPNO" Visible="False" />
                                                <asp:BoundField HeaderText="Employee Name" DataField="EMP_NAME" />
                                                <asp:BoundField HeaderText="Action" DataField="APS_STATUS" />
                                                <asp:BoundField HeaderText="Action Date" DataField="APS_DATE"
                                                    DataFormatString="{0:dd/MMM/yyyy}" />

                                                <asp:BoundField DataField="REMARKS" HeaderText="Remarks" />

                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" style="width: 20%"><span class="field-label">Remarks</span></td>

                        <td align="left" >
                            <asp:TextBox ID="txtRemarks" runat="server"  TextMode="MultiLine"
                                EnableTheming="False"></asp:TextBox></td>
                    </tr>

                    <tr id="trAction" runat="server">
                        <td align="left" style="width: 20%"><span class="field-label">Action</span></td>

                        <td align="left" >
                            <asp:DropDownList ID="ddlApprove" runat="server" >
                                <asp:ListItem Selected="True" Value="A">Approve</asp:ListItem>
                                <asp:ListItem Value="H">Hold</asp:ListItem>
                                <asp:ListItem Value="R">Reject</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>

                    <tr id="trApproval" runat="server" visible="False">
                        <td align="left" style="width: 20%"><span class="field-label">Approval Remarks</span></td>

                        <td align="left" >
                            <asp:TextBox ID="txtApvRemarks" runat="server" TextMode="MultiLine"
                                 EnableTheming="False"></asp:TextBox></td>
                    </tr>

                    <tr id="trHRApproval" runat="server" visible="False">
                        <td align="left" style="width: 20%"><span class="field-label">Remarks For Accounts (HR)</span></td>

                        <td align="left" >
                            <asp:TextBox ID="txtApvHRRemarks" runat="server" TextMode="MultiLine"
                                 EnableTheming="False" AutoPostBack="True"></asp:TextBox></td>
                    </tr>
                    <tr style="display: none">
                        <td align="left" style="width: 20%"><span class="field-label">Contract Type</span></td>
                     
                        <td align="left" >
                            <asp:DropDownList ID="ddlEmpContractType" runat="server">
                                <asp:ListItem Value="0">Limited</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">Unlimited</asp:ListItem>
                                <asp:ListItem Value="3">N/A</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td align="left"><span class="field-label">Contract End Date</span><br />
                            (for Limited Contract only)</td>

                        <td align="left" >
                            <asp:TextBox ID="txtContractEndDt" runat="server" ></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" /></td>

                    </tr>
                    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                        Format="dd/MMM/yyyy" PopupButtonID="txtContractEndDt" TargetControlID="txtContractEndDt">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="txtVisaCancelDt_CalendarExtender" runat="server" CssClass="MyCalendar"
                        Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtContractEndDt">
                    </ajaxToolkit:CalendarExtender>
                    <tr style="display: none">
                        <td align="left"><span class="field-label">Total LOP Days</span></td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtCurrentLOPs" runat="server"></asp:TextBox>


                        </td>

                    </tr>
                    <tr style="display: none">
                        <td align="left"><span class="field-label">LOP for first 5 years</span></td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtLOPFirstFiveYr" runat="server" ></asp:TextBox>

                            <asp:Label ID="lblLopfirst" runat="server"></asp:Label></td>

                    </tr>
                    <tr style="display: none">
                        <td align="left"><span class="field-label">LOP after first 5 years</span></td>
                      
                        <td align="left" >
                            <asp:TextBox ID="txtLOPAfterFiveYr" runat="server" ></asp:TextBox>

                            <asp:Label ID="lblLOPLast" runat="server"></asp:Label></td>

                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:RadioButton ID="chkForward" runat="server" Text="Forward for Approval" GroupName="chkFF" CssClass="field-label" />
                            <asp:RadioButton ID="chkFFSOutsideSystem" CssClass="field-label" runat="server" Checked="false" Text="Final Settlement Outside system" GroupName="chkFF" />
                            <asp:CheckBox ID="cb_exitinterview" runat="server" CssClass="field-label"
                                Text="Send an exit interview " Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <%--<table width="100%">
                                <tr>
                                    <td align="center">--%>

                                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />

                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" />

                                        <asp:Button ID="btnSyncToDAX" runat="server" CssClass="button" Text="Sync to DAX" Visible="false" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="gr" />

                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />

                                   <%-- </td>
                                </tr>
                            </table>--%>


                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfEmpId" runat="server" />
                <asp:HiddenField ID="hfEmpIsfromAX" runat="server" />
                <asp:HiddenField ID="hfBsuId" runat="server" />
                <asp:HiddenField ID="hfCompany" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

