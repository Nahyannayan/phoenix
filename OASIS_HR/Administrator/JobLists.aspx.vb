Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic

Partial Class OASIS_HR_Administrator_JobLists
    Inherits System.Web.UI.Page
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            CheckMenuRights()
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        Dim directory As New Dictionary(Of String, Object)
        directory.Add("Add", LinkAddNew)

        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))

        For Each Row As GridViewRow In GrdJobsList.Rows
            Dim directoryg As New Dictionary(Of String, Object)
            Dim LinkShortlist As LinkButton = DirectCast(Row.FindControl("LinkShortlist"), LinkButton)
            Dim LinkEdit As LinkButton = DirectCast(Row.FindControl("LinkEdit"), LinkButton)
            Dim LinkDelete As LinkButton = DirectCast(Row.FindControl("LinkDelete"), LinkButton)
            directoryg.Add("Add", LinkShortlist)
            directoryg.Add("Edit", LinkEdit)
            directoryg.Add("Delete", LinkDelete)
            Call AccessRight3.setpage(directoryg, ViewState("menu_rights"), ViewState("datamode"))
        Next


    End Sub
    Public Sub BindGrid()
        Dim ds As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim srt_query = "SELECT DISTINCT JOB_CODE,JOB_DES,JOB_INFORMATION,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(JOB_INFORMATION,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview,JOBS.CATEGORY_ID,CATEGORY.CATEGORY_DES,JOB_STATUS FROM JOBS " & _
                        " INNER JOIN CATEGORY ON CATEGORY.CATEGORY_ID = JOBS.CATEGORY_ID AND JOBS.JOB_STATUS ='OPEN'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, srt_query)
        GrdJobsList.DataSource = ds
        GrdJobsList.DataBind()

    End Sub

    Protected Sub GrdJobsList_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GrdJobsList.SelectedIndexChanging
        Dim EncDec As New Encryption64
        Dim job_code As String = DirectCast(GrdJobsList.Rows(e.NewSelectedIndex).FindControl("HiddenJobID"), HiddenField).Value
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("ShortList_Candidates.aspx?JobeCode=" & EncDec.Encrypt(job_code) & mInfo)
    End Sub

    Protected Sub GrdJobsList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GrdJobsList.RowEditing
        Dim job_code As String = DirectCast(GrdJobsList.Rows(e.NewEditIndex).FindControl("HiddenJobID"), HiddenField).Value
        Dim Category_id As String = DirectCast(GrdJobsList.Rows(e.NewEditIndex).FindControl("HiddenCategoryId"), HiddenField).Value
        Dim EncDec As New Encryption64
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("JobEdit.aspx?Job_Code=" & EncDec.Encrypt(job_code) & "&Category_Id=" & EncDec.Encrypt(Category_id) & mInfo)

    End Sub

    Protected Sub LinkAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("Job.aspx" & mInfo)
    End Sub

    Protected Sub GrdJobsList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        lblmessage.Text = ""
        Dim job_code As String = DirectCast(GrdJobsList.Rows(e.RowIndex).FindControl("HiddenJobID"), HiddenField).Value
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@JOB_CODE", Convert.ToInt32(job_code))
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "SELECT_INSERT_EDIT_DELETE_JOBS", pParms)
        BindGrid()
    End Sub

    Protected Sub GrdJobsList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

    End Sub
End Class
