Imports Oasis_Administrator
Imports System.Collections.Generic

Partial Class Administrator_Job
    Inherits System.Web.UI.Page
    'ts

    Shared JobCode As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CheckMenuRights()
            BindUnits()
            BindCategory()
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try

        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
        Dim directory As New Dictionary(Of String, Object)
        directory.Add("Add", btnsave)

        Call AccessRight3.setpage(directory, ViewState("menu_rights"), ViewState("datamode"))


    End Sub

    Public Sub BindUnits()
        'listUnits.DataSource = OasisAdministrator.Get_BSU()
        'listUnits.DataTextField = "BSU_NAME"
        'listUnits.DataValueField = "BSU_ID"
        'listUnits.DataBind()
        GrdJobBsu.DataSource = OasisAdministrator.Get_BSU()
        GrdJobBsu.DataBind()
        
    End Sub
    Public Sub BindCategory()
        ddcategory.DataSource = OasisAdministrator.GetCategory()
        ddcategory.DataTextField = "CATEGORY_DES"
        ddcategory.DataValueField = "CATEGORY_ID"
        ddcategory.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Category"
        ddcategory.Items.Insert(0, list)

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        JobCode = OasisAdministrator.GetJobCode()
        JobCode = JobCode + 1
        Dim flag = 0

        'For Each list As ListItem In listUnits.Items
        '    If list.Selected Then
        '        Dim Info As String = "Information Not Provided"
        '        If txtInfo.Text.Trim <> "" Then
        '            Info = txtInfo.Text.Trim()
        '        End If
        '        lblmessage.Text = OasisAdministrator.SaveJobs(JobCode, list.Value, txtjobdes.Text.Trim(), Info, Convert.ToInt32(ddcategory.SelectedValue), "", 1, "")
        '    End If
        'Next

        For Each row As GridViewRow In GrdJobBsu.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("CheckBSU"), CheckBox)
            If check.Checked Then
                flag = 1
                Dim Info As String = "Information Not Provided"
                If txtInfo.Text.Trim <> "" Then
                    Info = txtInfo.Text.Trim()
                End If
                Dim bsuid = DirectCast(row.FindControl("HiddenBsuID"), HiddenField).Value
                Dim reportedby = DirectCast(row.FindControl("txtReportedBy"), TextBox).Text
                Dim vacant = DirectCast(row.FindControl("txtvacant"), TextBox).Text
                If vacant = "" Then
                    vacant = Nothing
                End If
                Dim closedate = DirectCast(row.FindControl("txtclosedate"), TextBox).Text


                lblmessage.Text = OasisAdministrator.SaveJobs(JobCode, bsuid, txtjobdes.Text.Trim(), Info, Convert.ToInt32(ddcategory.SelectedValue), reportedby, vacant, closedate)
            End If
        Next
        If flag = 1 Then
            If lblmessage.Text.Trim() = "0" Then
                Panel1.Visible = False
                Panel2.Visible = True
            End If
        Else
            lblmessage.Text = "Please select Business Unit(s)"
        End If


    End Sub

    
    Protected Sub btnyes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnyes.Click
        Dim EncDec As New Encryption64
        Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("ShortList_Candidates.aspx?JobeCode=" & EncDec.Encrypt(JobCode) & mInfo)
    End Sub

    Protected Sub btnno_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        Response.Redirect("Job.aspx" & mInfo)
    End Sub
End Class
