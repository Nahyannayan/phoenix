Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class OASIS_HR_Administrator_StaffResignationList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim SearchMode As String
    'Version        Author          Date            Changes
    '1.1            swapna          15'/jun/2011    changes made to initial version for resignation entry and approval landing screen
    '                                               Menu:"H000077" for Approval,"H000073" for entering resignation details
    '2.0            Swapna          30/Mar/2016     To allow FFS outside system for AX units
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'SearchMode = "SI" 'Request.QueryString("id")
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                gvEmpResignationList.Attributes.Add("bordercolor", "#1b80b6")
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If SearchMode = "SI" Then
                    Page.Title = "Employee Resignation List"
                End If
                If ViewState("MainMnu_code") = "H000077" Then
                    hlAddNew.Visible = False
                    rbForword.Visible = False
                End If
                If ViewState("MainMnu_code") = "H000073" Then
                    hlAddNew.Visible = True
                    rbForword.Visible = False
                End If
               
                h_Selected_menu_0.Value = "LI__../../Images/operations/like.gif"
                h_selected_menu_1.Value = "LI__../../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../../Images/operations/like.gif"

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000073" And ViewState("MainMnu_code") <> "H000077") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hlAddNew.NavigateUrl = "StaffResgination.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    gridbind()
                End If
                If ViewState("MainMnu_code") = "H000073" Then
                    gvEmpResignationList.Columns(6).Visible = False
                    hlAddNew.Visible = True
                ElseIf ViewState("MainMnu_code") = "H000077" Then
                    rbForword.Visible =False
                    'gvEmpResignationList.Columns(5).Visible = False
                    hlAddNew.Visible = False
                End If
                If Session("BSU_IsHROnDAX") = 1 Then 'V2.0
                    hlAddNew.Visible = False
                End If
            Catch ex As Exception

            End Try
        End If

        '  set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Public Function getid0(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpResignationList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEmpResignationList.HeaderRow.FindControl("mnu_0_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpResignationList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEmpResignationList.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpResignationList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEmpResignationList.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpResignationList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEmpResignationList.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpResignationList.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvEmpResignationList.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub gridbind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""


            Dim str_filter_EmpName As String
            Dim str_filter_EmpNo As String
            Dim str_filter_BsuName As String
            Dim str_filter_lstWrkDate As String
            Dim str_filter_RsgDate As String
            Dim temp_name As String = String.Empty
            Dim ds As New DataSet

            Dim lbllstWrkDate As New Label
            Dim lblEmpName As New Label
            Dim lblStudNo As New Label
            Dim lblSchoolName As New Label

            Dim txtSearch As New TextBox

            Dim str_txtlstWrkDate, str_txtEmpName, str_txtEmpNo, str_txtBsuName, str_txtRsgDate As String

            Dim str_search As String
            str_txtlstWrkDate = ""
            str_txtEmpName = ""
            str_txtEmpNo = ""
            str_txtBsuName = ""
            str_txtRsgDate = ""

            str_filter_lstWrkDate = ""
            str_filter_EmpName = ""
            str_filter_EmpNo = ""
            str_filter_BsuName = ""
            str_filter_RsgDate = ""
            'Using Userreader As SqlDataReader = AccessRoleUser.GetBUnitImage(Session("sBsuid"))
            '    While Userreader.Read
            '        temp_name = Convert.ToString(Userreader("BSU_NAME"))
            '    End While
            'End Using


            'str_filter_BsuName = " AND a.SchoolName LIKE '%" & temp_name & "%'"

            ' SearchMode = Request.QueryString("id")

            str_Sql = "SELECT * FROM ( select DISTINCT (EMPNO),EMP_ID,EMP_APPLNO,EREG_ApprStatus,EREG_ID,(isNULL(emp_fname,'') + ' '+ isNULL(emp_mname,'') + ' ' + isNULL(emp_lname,'')) as EmployeeName,DPT_DESCR,DES_DESCR,BSU_NAME,EREG_LASTWORKINGDATE,EREG_REGDATE,EREG_ENTRYDATE" & _
            ", case EREG_ApprStatus " & _
            " when 'A' then 'Approved' " & _
            " when 'H' then 'Hold' " & _
            " when 'R' then 'Rejected' " & _
            " when 'F' then 'Forwarded' " & _
            " when 'N' then 'Not Applied' end as Status " & _
            " from employee_m  INNER JOIN dbo.EMPRESIGNATION_D ON EMP_ID=EREG_EMP_ID INNER JOIN BUSINESSUNIT_M  ON BSU_ID=EMP_BSU_ID LEFT join EMPDESIGNATION_M  on employee_m.emp_des_id= EMPDESIGNATION_M.des_id LEFT join DEPARTMENT_M  on dpt_id=employee_m.EMP_DPT_ID WHERE EMP_BSU_ID='" & Session("sBsuid") & "'"

            If rbForword.Checked = True And ViewState("MainMnu_code") = "H000073" Then
                str_Sql = str_Sql & " and EREG_BForwaded=1 and EREG_ApprStatus='F' "

            ElseIf rbOpen.Checked = True And ViewState("MainMnu_code") = "H000073" Then
                str_Sql = str_Sql & " and EREG_BForwaded=0 and EREG_ApprStatus='N' "

            ElseIf rbOpen.Checked = True And ViewState("MainMnu_code") = "H000077" Then
                str_Sql = str_Sql & " and EREG_BForwaded=1 and EREG_ApprStatus='F' "

            ElseIf rbApprove.Checked = True Then
                str_Sql = str_Sql & " and EREG_BForwaded=1 and EREG_ApprStatus='A' "

            ElseIf rbReject.Checked = True Then
                str_Sql = str_Sql & " and EREG_BForwaded=1 and EREG_ApprStatus='R' "

            ElseIf rbHold.Checked = True Then
                str_Sql = str_Sql & " and EREG_BForwaded=1 and EREG_ApprStatus='H' "

            End If
            

            If gvEmpResignationList.Rows.Count > 0 Then

                Dim str_Sid_search() As String


                ''EmpNo
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtEmpNo")
                str_txtEmpNo = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_EmpNo = " AND a.Empno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EmpNo = "  AND  NOT a.Empno LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EmpNo = " AND a.Empno  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EmpNo = " AND a.Empno  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EmpNo = " AND a.Empno LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EmpNo = " AND a.Empno NOT LIKE '%" & txtSearch.Text & "'"
                End If


                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtEmpName")
                str_txtEmpName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_EmpName = " AND a.EmployeeName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EmpName = "  AND  NOT a.EmployeeName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EmpName = " AND a.EmployeeName  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EmpName = " AND a.EmployeeName  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EmpName = " AND a.EmployeeName LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EmpName = " AND a.EmployeeName NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)


                txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtRsgDate_search")
                str_txtRsgDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_RsgDate = " AND a.EREG_REGDATE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_RsgDate = "  AND  NOT a.EREG_REGDATE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_RsgDate = " AND a.EREG_REGDATE  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_RsgDate = " AND a.EREG_REGDATE  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_RsgDate = " AND a.EREG_REGDATE LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_RsgDate = " AND a.EREG_REGDATE NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtLastWrk_date")
                str_txtlstWrkDate = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_lstWrkDate = " AND a.EREG_LASTWORKINGDATE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_lstWrkDate = "  AND  NOT a.EREG_LASTWORKINGDATE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_lstWrkDate = " AND a.EREG_LASTWORKINGDATE  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_lstWrkDate = " AND a.EREG_LASTWORKINGDATE  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_lstWrkDate = " AND a.EREG_LASTWORKINGDATE LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_lstWrkDate = " AND a.EREG_LASTWORKINGDATE NOT LIKE '%" & txtSearch.Text & "'"
                End If
            End If

            str_Sql = str_Sql & ") as a where 1=1 " & str_filter_lstWrkDate & str_filter_EmpNo & str_filter_EmpName & str_filter_BsuName & str_filter_RsgDate & ""
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


            gvEmpResignationList.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpResignationList.DataBind()
                Dim columnCount As Integer = gvEmpResignationList.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvEmpResignationList.Rows(0).Cells.Clear()
                gvEmpResignationList.Rows(0).Cells.Add(New TableCell)
                gvEmpResignationList.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpResignationList.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpResignationList.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvEmpResignationList.HeaderRow.Visible = True
            Else
                gvEmpResignationList.DataBind()


            End If

            txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtLastWrk_date")
            txtSearch.Text = str_txtlstWrkDate
            txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = str_txtEmpName
            txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtEmpNo")
            txtSearch.Text = str_txtEmpNo
            
            txtSearch = gvEmpResignationList.HeaderRow.FindControl("txtRsgDate_search")
            txtSearch.Text = str_txtRsgDate

            'set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            UtilityObj.Errorlog("Error In collecting sibling info")
        End Try

    End Sub

    Protected Sub btnSearchEmpNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchwrkdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchRsgDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchBsuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvEmpResignationList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvEmpResignationList.RowDataBound
        Try
            Dim lblEREG_ID As New Label
            lblEREG_ID = TryCast(e.Row.FindControl("lblEREG_ID"), Label)

            '--------------Edit------------------------
            Dim hlEdit As New HyperLink
            hlEdit = TryCast(e.Row.FindControl("lnkEdit"), HyperLink)
            If hlEdit IsNot Nothing And lblEREG_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlEdit.NavigateUrl = "StaffResgination.aspx?viewid=" & Encr_decrData.Encrypt(lblEREG_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            '------------- Approve----------------
            Dim hlApprove As New HyperLink
            hlApprove = TryCast(e.Row.FindControl("lnkApprove"), HyperLink)
            Dim lblStatus As New Label
            lblStatus = TryCast(e.Row.FindControl("lblStatus"), Label)
           

            If hlApprove IsNot Nothing And lblEREG_ID IsNot Nothing Then
                If lblStatus.Text = "A" Or lblStatus.Text = "R" Then
                    hlApprove.Enabled = False
                Else
                    ViewState("datamode") = Encr_decrData.Encrypt("edit")
                    hlApprove.Enabled = True
                    hlApprove.NavigateUrl = "StaffResgination.aspx?viewid=" & Encr_decrData.Encrypt(lblEREG_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End If
            End If
            If ViewState("MainMnu_code") = "H000077" Then
                If lblStatus.Text <> "A" And lblStatus.Text <> "R" Then
                    hlApprove.Enabled = True
                Else
                    hlApprove.Enabled = False

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub rbOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOpen.CheckedChanged
        gridbind()
    End Sub

    Protected Sub RdForword_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbForword.CheckedChanged
        gridbind()
    End Sub

    Protected Sub RdApprove_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbApprove.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbHold_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbHold.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbReject_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbReject.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub gvEmpResignationList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvEmpResignationList.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    'ts
End Class
