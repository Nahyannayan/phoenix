<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShortList_Candidates.aspx.vb" MaintainScrollPositionOnPostback="true" MasterPageFile="~/mainMasterPage.master" Inherits="Administrator_ShortList_Candidates" %>

<%@ Register Src="../UserControls/ShortList_Candidates.ascx" TagName="ShortList_Candidates"
    TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Shortlist Applicant
        </div>
        <div class="card-body ">
            <div class="table-responsive">
                <uc1:ShortList_Candidates ID="ShortList_Candidates1" runat="server"></uc1:ShortList_Candidates>
            </div>
        </div>
    </div>
</asp:Content>
