Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports GemBox.Spreadsheet
Imports system
Imports System.Data.SqlTypes
Partial Class OASIS_HR_Administrator_StaffListExportEView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                BindFilterControlsMain()
            Catch ex As Exception

            End Try
        End If
    End Sub
    Public Sub BindFilterControlsMain()
        ''Try
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "Select * from STF_REPORT_DISPLAY_TABLE where STF_DISP_SHOW_FLAG='1' ORDER BY STF_DISP_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim CBL_SelectedField As New CheckBoxList

        gvSelectedField.DataSource = ds.Tables(0)
        gvSelectedField.DataBind()




        'CBL_SelectedField.ID = "|CBL_SelectedField|"
        'CBL_SelectedField.DataSource = ds
        'CBL_SelectedField.DataTextField = "STF_FT_DES"
        'CBL_SelectedField.DataValueField = "STF_FT_ID"
        'CBL_SelectedField.DataBind()
        'For Each clist As ListItem In CBL_SelectedField.Items
        '    clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
        'Next
        'Dim list As New ListItem
        'list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        'list.Value = "-1"
        'list.Attributes.Add("onclick", "javascript:change_chk_state(this);")
        'list.Selected = True
        'CBL_SelectedField.Items.Insert(0, list)

        'Panel1.Controls.Add(CBL_SelectedField)



    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim strfield As String = ""
            For Each gvrow As GridViewRow In gvSelectedField.Rows
                Dim chk As CheckBox
                chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
                If chk.Checked = True Then
                    Dim field_name As HiddenField
                    field_name = TryCast(gvrow.FindControl("HF_selectedfield"), HiddenField)
                    If field_name.Value <> "" Then
                        strfield = strfield + "," + field_name.Value
                    End If

                End If
            Next

            Dim param(40) As SqlClient.SqlParameter

            param = Session("Exportparam")

            param(28) = New SqlClient.SqlParameter("@Select_CONDITION", strfield)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STF_FILTER_EMPLOYEE", param)

            Dim dtEXCEL As New DataTable
            dtEXCEL = ds.Tables(0)

            If dtEXCEL.Rows.Count > 0 Then

                Dim grdView As New GridView
                grdView.DataSource = dtEXCEL
                grdView.DataBind()

                Dim str_err As String = String.Empty
                Dim errorMessage As String = String.Empty
                '''''''''''''''

                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                ws.InsertDataTable(dtEXCEL, "A1", True)
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "Staff.xls")

                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                Dim pathSave As String
                pathSave = Session("EmployeeId") + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xls"

                ef.SaveXls(cvVirtualPath & "StaffExport\" + pathSave)

                Dim path = cvVirtualPath & "\StaffExport\" + pathSave
                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)

                Response.Flush()

                Response.End()

                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()


                'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                'Dim ef As ExcelFile = New ExcelFile

                'Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                'ws.InsertDataTable(dtEXCEL, "A1", True)
                'Response.ContentType = "application/vnd.ms-excel"
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Staff.xls")
                'ef.SaveXls(Response.OutputStream())

                'PrintCv(dtEXCEL)
            Else
                lblError.Text = "No Records To display with this filter condition....!!!"
                lblError.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub PrintCv(ByVal dt As DataTable)

        Try


            SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")

            Dim ExFile As ExcelFile = New ExcelFile
            Dim WrkSht As ExcelWorksheet = ExFile.Worksheets.Add("DataSheet")
            WrkSht.InsertDataTable(dt, "A1", True)
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=PDCIssuedAndOutstanding::Staff.xls")
            ExFile.SaveXls(Response.OutputStream)
            Response.End()


        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ckbheader_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhead As CheckBox
        chkhead = TryCast(gvSelectedField.HeaderRow.FindControl("ckbheader"), CheckBox)
        For Each gvrow As GridViewRow In gvSelectedField.Rows
            Dim chk As CheckBox
            chk = TryCast(gvrow.FindControl("ckbSelect"), CheckBox)
            If chkhead.Checked = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next
    End Sub

    Protected Sub ckbSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim chk As CheckBox
        chk = TryCast(sender.FindControl("ckbSelect"), CheckBox)
        Dim chkhead As CheckBox
        chkhead = TryCast(gvSelectedField.HeaderRow.FindControl("ckbheader"), CheckBox)
        If chk.Checked = False Then
            chkhead.Checked = False
        End If

    End Sub
End Class
