Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class UserControls_usrSelStudent
    Inherits System.Web.UI.UserControl
    Public Event StudentNoChanged As EventHandler

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Try
            Dim ds As DataSet = getTable()
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                h_Student_id.Value = ds.Tables(0).Rows(0)("STU_ID")
                txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
                txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
                h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
                h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
                h_ACD_ID.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")

                h_STU_BSU_NAME.Value = ds.Tables(0).Rows(0)("BSU_NAME").ToString
                h_STU_FEE_ID.Value = ds.Tables(0).Rows(0)("STU_FEE_ID").ToString
                h_STU_FNAME.Value = ds.Tables(0).Rows(0)("STU_FIRSTNAME").ToString
                h_STU_MNAME.Value = ds.Tables(0).Rows(0)("STU_MIDNAME").ToString
                h_STU_LNAME.Value = ds.Tables(0).Rows(0)("STU_LASTNAME").ToString
                h_STU_DOB.Value = ds.Tables(0).Rows(0)("STU_DOB").ToString
                h_STU_GENDER.Value = ds.Tables(0).Rows(0)("STU_GENDER").ToString
                h_STU_PHOTOPATH.Value = ds.Tables(0).Rows(0)("STU_PHOTOPATH").ToString

                h_PARENT_FNAME.Value = ds.Tables(0).Rows(0)("PARENT_FNAME").ToString
                h_PARENT_MNAME.Value = ds.Tables(0).Rows(0)("PARENT_MNAME").ToString
                h_PARENT_LNAME.Value = ds.Tables(0).Rows(0)("PARENT_LNAME").ToString
                h_PARENT_MOBILE.Value = ds.Tables(0).Rows(0)("PARENT_MOBILE").ToString
                h_PARENT_EMAIL.Value = ds.Tables(0).Rows(0)("PARENT_EMAIL").ToString
                If IsStudent Then
                    h_StuStatus.Value = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
                    h_TCdate.Value = ds.Tables(0).Rows(0)("STU_CANCELDATE")
                    h_LDAdate.Value = ds.Tables(0).Rows(0)("STU_LASTATTDATE")
                    h_StuDOJ.Value = ds.Tables(0).Rows(0)("STU_DOJ")
                End If
            Else
                h_Student_id.Value = ""
                txtStudentname.Text = ""
                Dim lblError As Label = Parent.FindControl("lblError")
                If Not lblError Is Nothing Then
                    lblError.Text = "Student # Entered is not valid  !!!"
                End If
            End If
            RaiseEvent StudentNoChanged(Me, e)
        Catch ex As Exception
            Errorlog(ex.Message, "Student User Control")
        End Try
    End Sub
    Function getTable() As DataSet
        Dim dt As DataTable = Nothing
        Dim BSU_ID As String = IIf(STU_BSU_ID = "", Session("sBsuid"), STU_BSU_ID)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        pParms(1) = New SqlClient.SqlParameter("@STU_NO", txtStdNo.Text.Trim)
        pParms(2) = New SqlClient.SqlParameter("@STUtype", IsStudent)
        Dim _table As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.StoredProcedure, "ENR.GET_STUDENT", pParms)
        Return _table
    End Function

    Public ReadOnly Property STUDENT_NO() As String
        Get
            Return txtStdNo.Text
        End Get
    End Property

    Public Property SelectedDate() As Date
        Get
            Return h_SelDate.Value
        End Get
        Set(ByVal value As Date)
            h_SelDate.Value = value
        End Set
    End Property

    Public Property IsStudent() As Boolean
        Get
            Return h_isStudent.Value
        End Get
        Set(ByVal value As Boolean)
            If value Then
                h_isStudent.Value = "1"
            Else
                h_isStudent.Value = "0"
            End If
        End Set
    End Property

    Public Property IsWithDrawal() As Boolean
        Get
            Return h_REASON.Value
        End Get
        Set(ByVal value As Boolean)
            If value Then
                h_REASON.Value = 2
            Else
                h_REASON.Value = 1
            End If
        End Set
    End Property

    Public Property ACD_ID() As String
        Get
            Return h_ACD_ID.Value
        End Get
        Set(ByVal value As String)
            h_ACD_ID.Value = value
        End Set
    End Property

    Public Property STU_BSU_ID() As String
        Get
            Return h_STU_BSU_ID.Value
        End Get
        Set(ByVal value As String)
            h_STU_BSU_ID.Value = value
        End Set
    End Property
    Public Property STU_FEE_ID() As String
        Get
            Return h_STU_FEE_ID.Value
        End Get
        Set(ByVal value As String)
            h_STU_FEE_ID.Value = value
        End Set
    End Property
    Public Property STU_BSU_NAME() As String
        Get
            Return h_STU_BSU_NAME.Value
        End Get
        Set(ByVal value As String)
            h_STU_BSU_NAME.Value = value
        End Set
    End Property
    Public Property STU_FNAME() As String
        Get
            Return h_STU_FNAME.Value
        End Get
        Set(ByVal value As String)
            h_STU_FNAME.Value = value
        End Set
    End Property
    Public Property STU_MNAME() As String
        Get
            Return h_STU_MNAME.Value
        End Get
        Set(ByVal value As String)
            h_STU_MNAME.Value = value
        End Set
    End Property
    Public Property STU_LNAME() As String
        Get
            Return h_STU_LNAME.Value
        End Get
        Set(ByVal value As String)
            h_STU_LNAME.Value = value
        End Set
    End Property

    Public Property PARENT_FNAME() As String
        Get
            Return h_PARENT_FNAME.Value
        End Get
        Set(ByVal value As String)
            h_PARENT_FNAME.Value = value
        End Set
    End Property
    Public Property PARENT_MNAME() As String
        Get
            Return h_PARENT_MNAME.Value
        End Get
        Set(ByVal value As String)
            h_PARENT_MNAME.Value = value
        End Set
    End Property
    Public Property PARENT_LNAME() As String
        Get
            Return h_PARENT_LNAME.Value
        End Get
        Set(ByVal value As String)
            h_PARENT_LNAME.Value = value
        End Set
    End Property
    Public Property PARENT_MOBILE() As String
        Get
            Return h_PARENT_MOBILE.Value
        End Get
        Set(ByVal value As String)
            h_PARENT_MOBILE.Value = value
        End Set
    End Property
    Public Property PARENT_EMAIL() As String
        Get
            Return h_PARENT_EMAIL.Value
        End Get
        Set(ByVal value As String)
            h_PARENT_EMAIL.Value = value
        End Set
    End Property
    Public Property STU_DOB() As String
        Get
            Return h_STU_DOB.Value
        End Get
        Set(ByVal value As String)
            h_STU_DOB.Value = value
        End Set
    End Property
    Public Property STU_GENDER() As String
        Get
            Return h_STU_GENDER.Value
        End Get
        Set(ByVal value As String)
            h_STU_GENDER.Value = value
        End Set
    End Property
    Public Property STU_PHOTOPATH() As String
        Get
            Return h_STU_PHOTOPATH.Value
        End Get
        Set(ByVal value As String)
            h_STU_PHOTOPATH.Value = value
        End Set
    End Property

    Public Property GRM_DISPLAY() As String
        Get
            Return h_GrmDisplay.Value
        End Get
        Set(ByVal value As String)
            h_GrmDisplay.Value = value
        End Set
    End Property

    Public Property STU_STATUS() As String
        Get
            Return h_StuStatus.Value
        End Get
        Set(ByVal value As String)
            h_StuStatus.Value = value
        End Set
    End Property

    Public ReadOnly Property STUDENT_NAME() As String
        Get
            Return txtStudentname.Text
        End Get
    End Property

    Public ReadOnly Property STUDENT_ID() As String
        Get
            Return h_Student_id.Value
        End Get
    End Property

    Public Property STUDENT_GRADE_ID() As String
        Get
            Return h_GRD_ID.Value
        End Get
        Set(ByVal value As String)
            h_GRD_ID.Value = value
        End Set
    End Property

    Public WriteOnly Property IsReadOnly() As Boolean
        Set(ByVal value As Boolean)
            txtStdNo.ReadOnly = value
            imgStudent.Enabled = Not value
        End Set
    End Property
    Public Property TCDATE() As String
        Get
            Return h_TCdate.Value
        End Get
        Set(ByVal value As String)
            h_TCdate.Value = value
        End Set
    End Property

    Public Property LDADATE() As String
        Get
            Return h_LDAdate.Value
        End Get
        Set(ByVal value As String)
            h_LDAdate.Value = value
        End Set
    End Property
    Public Property STUDOJ() As String
        Get
            Return h_StuDOJ.Value
        End Get
        Set(ByVal value As String)
            h_StuDOJ.Value = value
        End Set
    End Property
    Public Sub ClearDetails()
        h_GRD_ID.Value = ""
        h_STU_BSU_ID.Value = ""
        h_STU_BSU_NAME.Value = ""
        h_STU_FEE_ID.Value = ""
        h_STU_FNAME.Value = ""
        h_STU_MNAME.Value = ""
        h_STU_LNAME.Value = ""
        h_Student_id.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        h_GrmDisplay.Value = ""
        h_StuStatus.Value = "EN"
        h_LDAdate.Value = ""
        h_TCdate.Value = ""
        h_StuDOJ.Value = ""
    End Sub

    Public Sub ChangeColor(ByVal ChangeY As Boolean)
        If ChangeY = True Then
            txtStudentname.ForeColor = Drawing.Color.Red
            txtStdNo.ForeColor = Drawing.Color.Red
        Else
            txtStudentname.ForeColor = Drawing.Color.Black
            txtStdNo.ForeColor = Drawing.Color.Black
        End If
    End Sub

    Public Sub ChangeFontColor(ByVal Color As Drawing.Color)

        txtStudentname.ForeColor = Color
        txtStdNo.ForeColor = Color
        
    End Sub

    Public Sub SetStudentDetails(ByVal STU_ID As String)
        Dim str_sql As String = String.Empty
        Dim BSU_ID As String = IIf(STU_BSU_ID = "", Session("sBsuid"), STU_BSU_ID)
        If IsStudent Then
            str_sql = "SELECT STU_ID,STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, " _
            & " isnull(STU_CURRSTATUS,'EN') STU_CURRSTATUS, STU_ACD_ID FROM VW_OSO_STUDENT_M" _
            & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & BSU_ID & "') AND  STU_ID='" & STU_ID & "'"
        Else
            str_sql = "SELECT STU_ID, STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, STU_ACD_ID FROM FEES.vw_OSO_ENQUIRY_COMP" _
             & " WHERE (STU_BSU_ID = '" & BSU_ID & "') AND STU_ID='" & STU_ID & "'"
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, Data.CommandType.Text, str_sql)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            h_Student_id.Value = ds.Tables(0).Rows(0)("STU_ID")
            txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
            h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
            h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
            h_ACD_ID.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")
            If IsStudent Then
                h_StuStatus.Value = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ImgLike.Attributes.Add("onclick", "return ImgInline()")
        If Not IsPostBack Then
            txtStudentname.Attributes.Add("onkeyup", "return SearchName('" & txtStudentname.ClientID & "')")
            txtStudentname.Attributes.Add("onclick", "return getList()")
            txtStudentname.Attributes.Add("Onkeypress", "return selectItem()")
            IsWithDrawal = False
            IsStudent = True
        End If
        If h_Like.Value = "S" Then
            ImgLike.ImageUrl = "../Images/operations/startswith.gif"
        End If
    End Sub

    Public Sub SetStudentDetailsByNO(ByVal STU_NO As String)
        Dim str_data As String = String.Empty
        Dim str_sql As String = String.Empty
        Dim BSU_ID As String = IIf(STU_BSU_ID = "", Session("sBsuid"), STU_BSU_ID)

        If IsStudent Then
            str_sql = "SELECT  STU_ID,STU_NO, STU_NAME,GRD_DISPLAY, STU_GRD_ID , " _
            & " isnull(STU_CURRSTATUS,'EN') STU_CURRSTATUS, STU_ACD_ID FROM VW_OSO_STUDENT_M" _
            & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & BSU_ID & "') AND  STU_NO='" & STU_NO & "'"
        Else
            str_sql = "SELECT STU_ID, STU_NO, STU_NAME, GRD_DISPLAY, STU_GRD_ID, STU_ACD_ID FROM FEES.vw_OSO_ENQUIRY_COMP" _
             & " WHERE (STU_bActive = 1) AND (STU_BSU_ID = '" & BSU_ID & "') AND  STU_NO='" & STU_NO & "'"
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, Data.CommandType.Text, str_sql)
        If Not ds Is Nothing And ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                h_Student_id.Value = ds.Tables(0).Rows(0)("STU_ID")
                txtStdNo.Text = ds.Tables(0).Rows(0)("STU_NO")
                txtStudentname.Text = ds.Tables(0).Rows(0)("STU_NAME")
                h_GrmDisplay.Value = ds.Tables(0).Rows(0)("GRD_DISPLAY")
                h_GRD_ID.Value = ds.Tables(0).Rows(0)("STU_GRD_ID")
                h_ACD_ID.Value = ds.Tables(0).Rows(0)("STU_ACD_ID")
                If IsStudent Then
                    h_StuStatus.Value = ds.Tables(0).Rows(0)("STU_CURRSTATUS")
                End If
            End If
        End If
    End Sub

End Class
