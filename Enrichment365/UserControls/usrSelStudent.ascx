<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrSelStudent.ascx.vb" Inherits="UserControls_usrSelStudent" %>
<table>
    <tr class="matters">
        <td class="matters">
            <asp:TextBox ID="txtStdNo" runat="server" AutoPostBack="True" TabIndex="5" Width="112px"></asp:TextBox>&nbsp;<asp:ImageButton
                ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="getStudent();return true;"
                TabIndex="7" />
            <asp:TextBox ID="txtStudentname" runat="server" Width="253px"></asp:TextBox>

            <asp:Image ID="ImgLike" ImageUrl="~/Images/operations/like.gif" runat="server" ImageAlign="AbsMiddle" />
            <asp:Label ID="lblGCaption" runat="server" Text="Grade :" Visible="False"></asp:Label>
            <asp:Label ID="lblGRADE" runat="server" Visible="False"></asp:Label>
            <asp:HiddenField ID="h_Student_id" runat="server" />
            <asp:HiddenField ID="h_GRD_ID" runat="server" />
            <asp:HiddenField ID="h_isStudent" runat="server" />
            <asp:HiddenField ID="h_ACD_ID" runat="server" />
            <asp:HiddenField ID="h_REASON" runat="server" />
            <asp:HiddenField ID="h_Like" runat="server" Value="A" />
            <asp:HiddenField ID="h_GrmDisplay" runat="server" Value="A" EnableViewState="False" />
            <asp:HiddenField ID="h_SelDate" runat="server" />
            <input type="text" id="txtRowno" value="0" style="width: 20px; display: none" />
            <input type="text" id="txtStunoHide" value="0" style="width: 20px; display: none" />
            <asp:HiddenField ID="h_StuStatus" runat="server" />
            <asp:HiddenField ID="h_TCdate" runat="server" />
            <asp:HiddenField ID="h_LDAdate" runat="server" />
            <asp:HiddenField ID="h_StuDOJ" runat="server" EnableViewState="False" />
            <asp:HiddenField ID="h_STU_BSU_ID" runat="server" />
            <asp:HiddenField ID="h_STU_FEE_ID" runat="server" />
            <asp:HiddenField ID="h_STU_BSU_NAME" runat="server" />
            <asp:HiddenField ID="h_STU_FNAME" runat="server" />
            <asp:HiddenField ID="h_STU_MNAME" runat="server" />
            <asp:HiddenField ID="h_STU_LNAME" runat="server" />
            <asp:HiddenField ID="h_STU_DOB" runat="server" />
            <asp:HiddenField ID="h_STU_GENDER" runat="server" />
            <asp:HiddenField ID="h_STU_PHOTOPATH" runat="server" />
            <asp:HiddenField ID="h_PARENT_FNAME" runat="server" />
            <asp:HiddenField ID="h_PARENT_MNAME" runat="server" />
            <asp:HiddenField ID="h_PARENT_LNAME" runat="server" />
            <asp:HiddenField ID="h_PARENT_MOBILE" runat="server" />
            <asp:HiddenField ID="h_PARENT_EMAIL" runat="server" />
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>

            <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" CommitProperty="value"
                CommitScript="" PopupControlID="panel1" Position="Bottom" TargetControlID="txtStudentname">
            </ajaxToolkit:PopupControlExtender>
            <ajaxToolkit:PopupControlExtender ID="PopupControlLike" runat="server" CommitProperty="value"
                CommitScript="" PopupControlID="panel2" Position="Bottom" TargetControlID="ImgLike">
            </ajaxToolkit:PopupControlExtender>
        </td>
    </tr>
</table>
<asp:Panel ID="panel1" runat="server">

    <div id="InnDiv" style="overflow: auto; width: 331px; scrollbar-face-color: #C34E4E; scrollbar-highlight-color: #ffffff; scrollbar-3dlight-color: #C34E4E; scrollbar-darkshadow-color: #C34E4E; scrollbar-shadow-color: #ffffff; scrollbar-arrow-color: #ffffff; scrollbar-track-color: #fffff0;"
        align="left">

        <table id="LoadImg" class="griditem_red" style="display: none; width: 250px;">
            <tr>
                <td style="vertical-align: middle">
                    <img src="../Images/loading1.gif" />
                </td>
                <td style="vertical-align: middle">Loading Please wait..   </td>
            </tr>
        </table>
        <table id="MainTable" style="cursor: hand;" class="griditem_red">
            <tr class="gridheader_red" id="TrHead" style="display: none">
                <td style="display: none">Id</td>
                <td style="width: 100px">Student No</td>
                <td style="width: 200px">Student Name</td>
            </tr>
        </table>
    </div>
</asp:Panel>


<asp:Panel ID="panel2" runat="server">


    <table id="LikeTable" style="cursor: hand; display: none" class="griditem_red">
        <tr id="Tr1">
            <td align="left" onclick="getImg('like.gif','A')">
                <img class="img_left" alt="Any Where" src="../Images/operations/like.gif" />
                Any Where</td>
        </tr>
        <tr>
            <td align="left" onclick="getImg('startswith.gif','S')">
                <img class="img_left" alt="Any Where" src="../Images/operations/startswith.gif" />
                Start With</td>
        </tr>

    </table>

</asp:Panel>
<script type="text/javascript" lang="javascript">

    var XMLHTTPRequestObject = false;
    var timeout;
    if (window.ActiveXObject) {
        XMLHTTPRequestObject = new ActiveXObject('Microsoft.XMLHTTP');
    }
    function handleError() {
        return true;
    }
    window.onerror = handleError;

    function tableClear() {
        var TName = document.getElementById("MainTable")
        var Trow = TName.rows.length;
        var ss = eval(Trow)
        for (i = 0; i < Trow; i++) {
            if (i != 0) {
                TName.deleteRow(Trow - i)
            }
        }
    }

    function tableRow(valOne, valTwo, valThree, ind) {
        var TName = document.getElementById("MainTable")
        var PassValue = valOne + "_" + valTwo + "_" + valThree
        PassValue = PassValue.replace("'", "");
        var Row = TName.insertRow(ind + 1)
        var TD1 = Row.insertCell(0)
        var TD2 = Row.insertCell(1)
        var TD3 = Row.insertCell(2)
        var TxtIdH = "TxtIdH" + ind
        var TxtId = "TxtId" + ind
        var TxtName = "TxtName" + ind
        var tagOne = '<input   readonly="readonly"  type="text" Value= "' + valOne + '" onclick=\'getOnClick(' + '\"' + PassValue + '\")\'  Id=' + TxtIdH + ' STYLE="border:none;WIDTH:50;height:10;cursor:hand" >';
        var tagTwo = '<input class="matters_Red" readonly="readonly" type="text" Value= "' + valTwo + '" onclick=\'getOnClick(' + '\"' + PassValue + '\")\'  Id=' + TxtId + ' STYLE="border:none;WIDTH:100;height:10;cursor:hand" >';
        var tagThree = '<input class="matters_Red" readonly="readonly"   title= "' + valThree + '" type="text" Value= "' + valThree + '" onclick=\'getOnClick(' + '\"' + PassValue + '\")\'  Id=' + TxtName + ' STYLE="border:none;WIDTH:200;height:10;cursor:hand" >';
        TD1.innerHTML = tagOne;
        TD2.innerHTML = tagTwo;
        TD3.innerHTML = tagThree;
        TD1.style.display = "none";
        TD1.align = "left";
        TD2.align = "left";
        TD3.align = "left";
    }

    function getDetails() {

        if (timeout != null) {
            window.clearTimeout(timeout)
        }
        var BsuId = '<%=Session("sBsuid")%>'
        var FillValue = document.getElementById('<%=txtStudentname.ClientID %>').value
        var LikVal = document.getElementById('<%=h_Like.ClientID %>').value
        var AcdId = document.getElementById('<%=h_ACD_ID.ClientID %>').value;
        var Type = document.getElementById('<%=h_isStudent.ClientID %>').value;
        BsuId = BsuId + '_' + FillValue + '_' + LikVal + '_' + AcdId + '_' + Type;
        var datasource = '../WebServices/AutoComplete.asmx/DisplayName?Bsuid=' + BsuId
        XMLHTTPRequestObject.open('GET', datasource, true);
        XMLHTTPRequestObject.onreadystatechange = function () {
            if (XMLHTTPRequestObject.readystate == 4 && XMLHTTPRequestObject.status == 200) {
                var xmlDoc = XMLHTTPRequestObject.responseXML;
                var xmlDocc = new ActiveXObject("Microsoft.XMLDOM");

                xmlDocc.load(xmlDoc);
                xmlObj = xmlDocc.documentElement
                var i = 0;
                tableClear();
                for (i = 0; i < xmlObj.childNodes.length; i++) {
                    var StuId = xmlObj.childNodes.item(i).childNodes.item(0).text;
                    var StuNo = xmlObj.childNodes.item(i).childNodes.item(1).text;
                    var StuName = xmlObj.childNodes.item(i).childNodes.item(2).text;
                    tableRow(StuId, StuNo, StuName, i);
                }
                document.getElementById('MainTable').style.display = 'inline'
                document.getElementById('TrHead').style.display = 'inline'
                document.getElementById('LoadImg').style.display = 'none'
            }
            else {
                if (XMLHTTPRequestObject.status == 503 || XMLHTTPRequestObject.status == 504) {
                    getDetails()
                }
            }
        }
        XMLHTTPRequestObject.send(null);
    }

    function getOnClick(IdVal) {
        var SplitArr = IdVal.split('_');
        document.getElementById('<%=h_Student_id.ClientID %>').value = SplitArr[0];
        document.getElementById('<%=txtStdNo.ClientID %>').value = SplitArr[1];
        document.getElementById('<%=txtStudentname.ClientID %>').value = SplitArr[2];

        document.getElementById('MainTable').style.display = 'none';
        document.getElementById('TrHead').style.display = 'none'
        document.getElementById('<%=txtStdNo.ClientID %>').onchange();
        return true;
    }

    function selectItem() {
        if (event.keyCode == 13) {
            document.getElementById('MainTable').style.display = 'none';
            document.getElementById('TrHead').style.display = 'none'
            document.getElementById('<%=txtStdNo.ClientID %>').value = document.getElementById('txtStunoHide').value
            document.getElementById('<%=txtStdNo.ClientID %>').onchange();
            return true;
        }
    }
    function SearchName(Obj) {
        if (event.keyCode == 113) {
            document.getElementById('<%=h_Student_id.ClientID %>').value = ""
            document.getElementById('<%=txtStdNo.ClientID %>').value = ""

            document.getElementById('InnDiv').style.height = "200";
            document.getElementById('MainTable').style.display = 'none';
            document.getElementById('TrHead').style.display = 'none'
            document.getElementById('LoadImg').style.display = 'inline'
            getDetails();
            return false;
        }
        var RwoNo = 0
        var RwoNoprv = 0
        if (event.keyCode == 40) {
            RwoNo = document.getElementById("txtRowno").value;
            document.getElementById("TxtName" + RwoNo).className = "select_red";
            document.getElementById("TxtId" + RwoNo).className = "select_red";
            document.getElementById('<%=txtStudentname.ClientID %>').value = document.getElementById("TxtName" + RwoNo).value;
        document.getElementById('txtStunoHide').value = document.getElementById("TxtId" + RwoNo).value;
        document.getElementById('<%=h_Student_id.ClientID %>').value = document.getElementById("TxtIdH" + RwoNo).value;
           if (RwoNo != 0) {
               RwoNoprv = Number(RwoNo) - 1;
               document.getElementById("TxtName" + RwoNoprv).className = "matters_Red";
               document.getElementById("TxtId" + RwoNoprv).className = "matters_Red";
           }
           document.getElementById("txtRowno").value = Number(RwoNo) + 1;
       }

       else if (event.keyCode == 38) {
           RwoNo = Number(document.getElementById("txtRowno").value) - 1
           if (RwoNo == 0)
           { return false; }
           document.getElementById("TxtName" + RwoNo).className = "matters_Red";
           document.getElementById("TxtId" + RwoNo).className = "matters_Red";
           RwoNoprv = Number(RwoNo) - 1;
           document.getElementById("TxtName" + RwoNoprv).className = "select_red";
           document.getElementById("TxtId" + RwoNoprv).className = "select_red";
           document.getElementById('<%=txtStudentname.ClientID %>').value = document.getElementById("TxtName" + RwoNoprv).value;
            document.getElementById('txtStunoHide').value = document.getElementById("TxtId" + RwoNoprv).value;
            document.getElementById('<%=h_Student_id.ClientID %>').value = document.getElementById("TxtIdH" + RwoNo).value;
           document.getElementById("txtRowno").value = RwoNo;
       }
}

function getList() {
    document.getElementById('MainTable').style.display = 'inline'

    var RwoNoprv = document.getElementById("txtRowno").value;
    if (RwoNoprv != 0) {
        RwoNoprv = Number(RwoNoprv - 1);
        document.getElementById("TxtName" + RwoNoprv).className = "matters_Red";
        document.getElementById("TxtId" + RwoNoprv).className = "matters_Red";
        document.getElementById("txtRowno").value = 0
    }
}
function getClick() {
    document.getElementById('<%=txtStudentname.ClientID %>').click();
}
function getStudent() {
    var sFeatures, url;
    sFeatures = "dialogWidth: 755px; ";
    sFeatures += "dialogHeight: 670px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var STUD_TYP = document.getElementById('<%=h_isStudent.ClientID %>').value;
    var selType = document.getElementById('<%=h_REASON.ClientID %>').value;
    var selACD_ID = document.getElementById('<%=h_ACD_ID.ClientID %>').value;
    var selDATE = document.getElementById('<%=h_SelDate.ClientID %>').value;
    var TYPE;
    if (selDATE == '')
    { if (STUD_TYP == '0') TYPE = 'TCENQ'; else TYPE = 'TC'; }
    else
    { if (STUD_TYP == '0') TYPE = 'ENQ_DATE'; else TYPE = 'STUD_DATE'; }
    var url;

    if (STUD_TYP == "0")
        url = 'ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;
    else
        url = 'ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;

    result = window.showModalDialog(url, "", sFeatures);
    if (result == '' || result == undefined) {
        return false;
    }
    NameandCode = result.split('||');
    document.getElementById('<%= h_Student_id.ClientID %>').value = NameandCode[0];
    document.getElementById('<%= txtStudentname.ClientID %>').value = NameandCode[1];
    document.getElementById('<%= txtStdNo.ClientID %>').value = NameandCode[2];
    return true;
}

function getImg(ImgName, Like) {
    document.getElementById('LikeTable').style.display = 'inline';
    var ImgPath = "../Images/operations/" + ImgName;
    document.getElementById('<%=ImgLike.ClientID %>').src = ImgPath;
    document.getElementById('LikeTable').style.display = 'none';
    document.getElementById('<%=h_Like.ClientID %>').value = Like;

}
function ImgInline() {
    document.getElementById('LikeTable').style.display = 'inline';
}
</script>
