Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class createpasswordquestions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Hiddenusername.Value = Session("sUsr_name")
            BindQuestions()
            Checkpreviousinput()

        End If
    End Sub
    Public Property closeflag() As Boolean
        Get
            Return Hiddencloseflag.Value
        End Get

        Set(ByVal value As Boolean)
            Hiddencloseflag.Value = value
        End Set

    End Property

    Public Sub BindQuestions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = "select  * from PASSWORD_QUESTIONS "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

        ddquestions.DataSource = ds
        ddquestions.DataTextField = "QUESTIONS"
        ddquestions.DataValueField = "QUES_ID"
        ddquestions.DataBind()


    End Sub
    Public Sub Checkpreviousinput()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sql_query = "select  * from USER_PASSWORD_QUESTIONS where USER_NAME='" & Hiddenusername.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)

            If ds.Tables(0).Rows.Count > 0 Then

                txtemailid.Text = ds.Tables(0).Rows(0).Item("EMAIL_ID").ToString()
                ddquestions.SelectedValue = ds.Tables(0).Rows(0).Item("QUESTION_ID").ToString()
                txtanswer.Text = ds.Tables(0).Rows(0).Item("ANSWER").ToString()

                btnsave.Visible = False
                btnupdate.Visible = True
                Panel1.Enabled = False
            Else
                btnsave.Visible = True
                btnupdate.Visible = False
                Panel1.Enabled = True
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        btnsave.Visible = True
        btnupdate.Visible = False
        Panel1.Enabled = True
        lblmessage.Text = ""
    End Sub


    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USER_NAME", Hiddenusername.Value)
        pParms(1) = New SqlClient.SqlParameter("@EMAIL_ID", txtemailid.Text.Trim())
        pParms(2) = New SqlClient.SqlParameter("@QUESTION_ID", ddquestions.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@ANSWER", txtanswer.Text.Trim())
        lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_USER_PASSWORD_QUESTIONS", pParms)
        Checkpreviousinput()

        If closeflag() Then
            Response.Write("<script type='text/javascript'>window.close()</script>")
        End If


    End Sub


End Class
