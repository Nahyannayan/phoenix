Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class CoCurricularActivities_ccaAceLevel_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                'ts
                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindGrades()
                    If ViewState("datamode") = "view" Then
                        hfACL_ID.Value = Encr_decrData.Decrypt(Request.QueryString("aclid").Replace(" ", "+"))
                        txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("descr").Replace(" ", "+"))
                        txtDescr.Enabled = False
                        GetGradesandGender(hfACL_ID.Value, txtDescr.Text)
                        btnEdit.Visible = True
                        btnSave.Visible = True
                        btnSave.Enabled = False
                        btnAdd.Visible = False
                    Else
                        btnEdit.Visible = False
                        btnsave.Visible = False
                        btnAdd.Visible = True
                    End If


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

    Sub GetGradesandGender(ByVal aclId As String, ByVal aclDescr As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = "SELECT ACL_GRADES,ACL_GENDER from ACE_LEVEL_M WHERE ACL_ID='" + hfACL_ID.Value + "' AND ACL_DESCR='" + txtDescr.Text + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count <> 0 Then
            Dim tempGrades As String = ds.Tables(0).Rows(0).Item(0)
            Dim gender As String = ds.Tables(0).Rows(0).Item(1)
            Dim grades As String() = tempGrades.Split(New Char() {"|"c})
            Dim i As Integer
            Dim j As Integer
            For i = 0 To cbGrades.Items.Count - 1
                For j = 0 To grades.Length - 1
                    If cbGrades.Items(i).Text = grades(j) Then
                        cbGrades.Items(i).Selected = True
                        'cbGrades.Items(i).Enabled = False
                    Else
                        'cbGrades.Items(i).Selected = False
                        cbGrades.Items(i).Enabled = True
                    End If
                Next
            Next
            For i = 0 To rbGender.Items.Count - 1
                If rbGender.Items(i).Text = gender Then
                    rbGender.Items(i).Selected = True
                Else
                    rbGender.Items(i).Selected = False
                End If
            Next
            rbGender.Enabled = False
            cbGrades.Enabled = False
        End If


    End Sub

    Sub BindGrades()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct GRM_ACD_ID,GRM_GRD_ID,ACD_CURRENT " _
                                & " FROM OASIS..GRADE_BSU_M INNER JOIN OASIS..ACADEMICYEAR_D ON ACD_BSU_ID=GRM_BSU_ID AND ACD_ID=GRM_ACD_ID " _
                                & " WHERE GRM_BSU_ID='" + Session("sBsuid") + "' AND ACD_CURRENT='1'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cbGrades.DataSource = ds
        cbGrades.DataTextField = "GRM_GRD_ID"
        cbGrades.DataValueField = "GRM_GRD_ID"
        cbGrades.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function isSubjectExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "select count(ACL_ID) from ACE_LEVEL_M where ACL_descr='" + txtDescr.Text + "' and ACL_ID<>" + hfACL_ID.Value.ToString
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If sbm = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim strGrade As String = ""
                Dim i As Integer
                For i = 0 To cbGrades.Items.Count - 1
                    If cbGrades.Items(i).Selected = True Then
                        If strGrade <> "" Then
                            strGrade += "|"
                        End If
                        strGrade += cbGrades.Items(i).Text
                    End If
                Next
                Dim str_query As String = "exec saveACELEVEL_M " + hfACL_ID.Value.ToString + "," _
                                          & "'" + Session("sbsuid") + "'," _
                                          & "'" + txtDescr.Text + "'," _
                                          & "'" + ViewState("datamode") + "'," _
                                          & "'" + rbGender.SelectedItem.Value + "'," _
                                          & "'" + strGrade + "'"
                hfACL_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "Add"
        hfACL_ID.Value = "0"
        btnEdit.Visible = False
        btnSave.Visible = False
        Try
            If isSubjectExists() = False Then
                SaveData()
                btnAdd.Enabled = False
                txtDescr.Enabled = False
                cbGrades.Enabled = False
                rbGender.Enabled = False
            Else
                lblError.Text = "This level already exists"
            End If
            txtDescr.Enabled = False
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfACL_ID.Value = 0
            'If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            '    txtDescr.Text = ""
            '    txtDescr.Enabled = False
            '    'clear the textbox and set the default settings
            '    ViewState("datamode") = "none"
            '    btnAdd.Enabled = False

            '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            'Else
            Response.Redirect(ViewState("ReferrerUrl"))
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Sub enableDisableControls(ByVal bool As Boolean)
        txtDescr.Enabled = True
        cbGrades.Enabled = True
        rbGender.Enabled = True
        Dim i As Integer
        
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "Edit"
        btnSave.Enabled = True
        btnSave.Visible = True
        enableDisableControls(True)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim strGrade As String = ""
                Dim i As Integer
                For i = 0 To cbGrades.Items.Count - 1
                    If cbGrades.Items(i).Selected = True Then
                        If strGrade <> "" Then
                            strGrade += "|"
                        End If
                        strGrade += cbGrades.Items(i).Text
                    End If
                Next
                Dim str_query As String = "exec saveACELEVEL_M " + hfACL_ID.Value.ToString + "," _
                                          & "'" + Session("sbsuid") + "'," _
                                          & "'" + txtDescr.Text + "'," _
                                          & "'" + ViewState("datamode") + "'," _
                                          & "'" + rbGender.SelectedItem.Value + "'," _
                                          & "'" + strGrade + "'"
                hfACL_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
End Class
