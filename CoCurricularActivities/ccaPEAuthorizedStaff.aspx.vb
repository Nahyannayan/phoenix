Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class ccaPEAuthorizedStaff
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC50011") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                   
                    GetAcademicYear_Bsu()
                    Bind_STAFF()
                    BindTree_GradeSection()
                    gridbind()
                    btnUpdateSave.Visible = False
                    enabledisable(True)
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
   
    Sub Bind_STAFF() 'for orginating tree view for the grade and section
        Try


            Dim CONN As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim DS As DataSet
            Dim PARAM(1) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            DS = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "PE.GETEMP_LIST", PARAM)

            chkStaff.DataSource = DS.Tables(0)
            chkStaff.DataValueField = "EMP_ID"
            chkStaff.DataTextField = "ENAME"
            chkStaff.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Sub GetAcademicYear_Bsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString



            Dim str_Sql As String
            ddlAca_Year.Items.Clear()
            str_Sql = "SELECT   ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR " & _
" FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
 " where ACADEMICYEAR_D.ACD_BSU_ID='" & Session("sBsuid") & "'  and ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "Y_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAcademicYear_Bsu")
        End Try
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        BindTree_GradeSection()
    End Sub

    Sub BindTree_GradeSection() 'for orginating tree view for the grade and section
        Dim ACD_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAca_Year.SelectedItem.Value
        End If

        Dim dtTable As DataTable = AccessStudentClass.PopulateGrade_Section(ACD_ID)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim drGRD_DESCR As DataRow
        For i As Integer = 0 To dtTable.Rows.Count - 1
            drGRD_DESCR = dtTable.Rows(i)
            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
            If contains Then
                Continue For
            End If
            Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
            drGRD_DESCR("GRD_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
        Next
        tvGRD_SCT.Nodes.Clear()
        tvGRD_SCT.Nodes.Add(trSelectAll)
        tvGRD_SCT.DataBind()

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            If Not tvGRD_SCT Is Nothing Then
                DisableAutoPostBack(tvGRD_SCT.Nodes)

            End If
        End If


    End Sub
    Sub DisableAutoPostBack_ROL(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack_ROL(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub DisableAutoPostBack(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub SelectAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = True

            If tn.ChildNodes.Count > 0 Then
                SelectAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    Private Sub recurseChildren(ByVal tn As TreeNode, ByVal searchValue As String)
        If tn.Text.ToUpper().Contains(searchValue.ToUpper()) Then
            tn.Text = "<font color='#800000'>" & tn.Text & "</font>"
            tn.Parent.Expand()
            tn.Expand()
            tn.Select()
            Exit Sub

        End If

        If tn.ChildNodes.Count > 0 Then

            For Each tnC As TreeNode In tn.ChildNodes
                recurseChildren(tnC, searchValue)

            Next

        End If

    End Sub

    
    Private Sub UNCHECK_STAFF()
        For Each item As ListItem In chkStaff.Items
            If item.Selected = True Then

                item.Selected = False
            End If

        Next
    End Sub
    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEMP_ID As New Label
        Dim SCT_IDS As String = String.Empty
        lblEMP_ID = TryCast(sender.FindControl("lblEMP_ID"), Label)
        Dim CONN As String = ConnectionManger.GetOASIS_CCAConnectionString
        lblEMP_ID = TryCast(sender.FindControl("lblEMP_ID"), Label)

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@AS_EMP_ID", lblEMP_ID.Text)
        param(1) = New SqlParameter("@AS_ACD_ID", ddlAca_Year.SelectedValue)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "PE.GETAUTH_STAFF_SECTION", param)
            While DATAREADER.Read
                SCT_IDS = Convert.ToString(DATAREADER("SCT_IDS"))
            End While
        End Using

        UncheckTreeView(tvGRD_SCT)


        Call checkme(tvGRD_SCT.Nodes, SCT_IDS)


        Dim emp_ids As String = String.Empty
        For Each item As ListItem In chkStaff.Items
            If item.Value = lblEMP_ID.Text.Trim Then

                item.Selected = True
            Else
                item.Selected = False
            End If

        Next

        chkStaff.Enabled = False

        btnAddSave.Visible = False
        btnUpdateSave.Visible = True
        gvUSR_SCT.Columns(4).Visible = False
        gvUSR_SCT.Columns(3).Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        UNCHECK_STAFF()
        UncheckTreeView(tvGRD_SCT)
        btnAddSave.Visible = True
        btnUpdateSave.Visible = False
        gvUSR_SCT.Columns(4).Visible = True
        gvUSR_SCT.Columns(3).Visible = True
        chkStaff.Enabled = True
    End Sub
    Protected Sub lbtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblEMP_ID As New Label
            Dim CONN As String = ConnectionManger.GetOASIS_CCAConnectionString
            lblEMP_ID = TryCast(sender.FindControl("lblEMP_ID"), Label)

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@AS_EMP_ID", lblEMP_ID.Text)
            param(1) = New SqlParameter("@AS_ACD_ID", ddlAca_Year.SelectedValue)

            param(2) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(CONN, CommandType.StoredProcedure, "PE.DELSTAFF_EMP_SECTION", param)
            If param(2).Value <> 0 Then
                lblError.Text = "Request could not be processed "
            Else
                gridbind()
                lblError.Text = "Record deleted successfully"

            End If

        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSave.Click

        Try

            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            Dim emp_ids As String = String.Empty
            Dim sct_ids As String = String.Empty
            If Page.IsValid Then
                emp_ids = populate_emp_id()
                sct_ids = Populate_GRD_SCT()
                If emp_ids = "" Or sct_ids = "" Then


                    lblError.Text = "Please select staff and grade section to be updated !!!"
                Else


                    str_err = calltransaction(errorMessage, emp_ids, sct_ids)
                    If str_err = "0" Then
                        lblError.Text = "Record Saved Successfully"

                        gridbind()
                        UNCHECK_STAFF()
                        UncheckTreeView(tvGRD_SCT)


                        btnAddSave.Visible = True
                        btnUpdateSave.Visible = False
                        gvUSR_SCT.Columns(4).Visible = True
                        gvUSR_SCT.Columns(3).Visible = True
                        chkStaff.Enabled = True
                    Else
                        lblError.Text = errorMessage
                    End If
                End If
            End If




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try


    End Sub
    Protected Sub btnAddNEW_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSave.Click
        Try

            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            Dim emp_ids As String = String.Empty
            Dim sct_ids As String = String.Empty
            If Page.IsValid Then
                emp_ids = populate_emp_id()
                sct_ids = Populate_GRD_SCT()
                If emp_ids = "" Or sct_ids = "" Then
                    lblError.Text = "Please select staff and grade section to be updated !!!"
                Else


                    str_err = calltransaction(errorMessage, emp_ids, sct_ids)
                    If str_err = "0" Then
                        lblError.Text = "Record Saved Successfully"
                        gridbind()
                        UNCHECK_STAFF()
                        UncheckTreeView(tvGRD_SCT)
                        btnAddSave.Visible = True
                        btnUpdateSave.Visible = False

                        chkStaff.Enabled = True
                    Else
                        lblError.Text = errorMessage
                    End If
                End If
            End If




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Function calltransaction(ByRef errorMessage As String, ByVal emp_ids As String, ByVal sct_ids As String) As Integer
        Dim transaction As SqlTransaction
        Dim status As Integer


        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@AS_ACD_ID", ddlAca_Year.SelectedValue)
                pParms(1) = New SqlClient.SqlParameter("@AS_BSU_ID", Session("sBsuid"))
                pParms(2) = New SqlClient.SqlParameter("@AS_EMP_IDS", emp_ids)
                pParms(3) = New SqlClient.SqlParameter("@AS_SCT_IDS", sct_ids)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PE.SAVEAUTHORIZED_STAFF_SECTION", pParms)
                status = pParms(4).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                    UtilityObj.Errorlog("Error while saving records", "SAVEAUTHORIZED_STAFF_SECTION")
                    Return "1"
                End If

                calltransaction = "0"

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage, "SAVEAUTHORIZED_STAFF_SECTION")
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Private Function populate_emp_id() As String
        Try
            Dim emp_ids As String = String.Empty
            For Each item As ListItem In chkStaff.Items
                If item.Selected = True Then

                    emp_ids += item.Value + "|"
                End If

            Next
            emp_ids = emp_ids.TrimStart("|")

            Return emp_ids
        Catch ex As Exception
            lblError.Text = "Error while processing staff details"
        End Try

    End Function

    Private Function Populate_GRD_SCT() As String
        Try

            Dim strAddSection As String = String.Empty
            Dim parentId As String = String.Empty
            Dim tempID As String = String.Empty


            Dim Sct_ids As String = String.Empty

            Dim checkedNodes_SCT As TreeNodeCollection = tvGRD_SCT.CheckedNodes

            If tvGRD_SCT.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvGRD_SCT.CheckedNodes

                    If Not node Is Nothing Then
                        If node.Text <> "Select All" Then
                            If node.Parent.Text <> "Select All" Then
                                If tempID = node.Parent.Text Then
                                    Sct_ids += node.Value + "|"
                                Else
                                    Sct_ids += node.Value + "|"
                                End If
                            End If
                        End If

                    End If
                Next
                Sct_ids = Sct_ids.TrimStart("|")
            End If

            Return Sct_ids

        Catch ex As Exception
            lblError.Text = "Error while allocating grade & section"
        End Try
    End Function
    Public Sub UncheckTreeView(ByVal tv As System.Web.UI.WebControls.TreeView)
        For Each tn As System.Web.UI.WebControls.TreeNode In tv.Nodes
            tn.Checked = False
            If tn.Text <> "Select All" Then
                If tn.Parent.Text <> "Select All" Then
                    tn.Collapse()
                End If
            End If
            tn.Text = tn.Text.Replace("#800000", "#1B80B6")
            UncheckTreeView(tn)
        Next
    End Sub

    Public Sub UncheckTreeView(ByVal node As System.Web.UI.WebControls.TreeNode)
        For Each tn As System.Web.UI.WebControls.TreeNode In node.ChildNodes
            tn.Checked = False
            If tn.Text <> "Select All" Then
                tn.Collapse()

            End If
            tn.Text = tn.Text.Replace("#800000", "#1B80B6")
            UncheckTreeView(tn)
        Next
    End Sub

    Sub enabledisable(ByVal flag As Boolean)

        ddlAca_Year.Enabled = flag

        plUserRole.Enabled = flag
        plGradeSection.Enabled = flag
        btnAddSave.Enabled = flag
        plGRD.Enabled = flag
    End Sub
    Protected Sub gvUSR_SCT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvUSR_SCT.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim DeleteBtn As LinkButton = DirectCast(e.Row.FindControl("lbtnDelete"), LinkButton)
                DeleteBtn.OnClientClick = "if(!confirm('Are you sure you want to delete the record?')) return false;"

                Dim lblEMP_ID As Label = DirectCast(e.Row.FindControl("lblEMP_ID"), Label)
                Dim gvgrd_detail As GridView = DirectCast(e.Row.FindControl("gvgrd_detail"), GridView)
                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim ds As New DataSet
                Dim sql_string As String = String.Empty
                Dim PARAM(2) As SqlParameter

                PARAM(0) = New SqlParameter("@AS_ACD_ID", ddlAca_Year.SelectedValue)
                PARAM(1) = New SqlParameter("@AS_EMP_ID", lblEMP_ID.Text.Trim)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETAUTH_STAFF_SECT_GRIDBIND", PARAM)
                If ds.Tables(0).Rows.Count > 0 Then
                    gvgrd_detail.DataSource = ds.Tables(0)
                    gvgrd_detail.DataBind()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub
    Private Sub gridbind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = String.Empty
            Dim ds As New DataSet
            Dim PARAM(2) As SqlParameter

            PARAM(0) = New SqlParameter("@AS_ACD_ID", ddlAca_Year.SelectedValue)
            PARAM(1) = New SqlParameter("@AS_BSU_ID", Session("sBsuid"))
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETAUTH_STAFF_GRIDBIND", PARAM)




            If ds.Tables(0).Rows.Count > 0 Then
                gvUSR_SCT.DataSource = ds.Tables(0)
                gvUSR_SCT.DataBind()
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'ds.Tables(0).Rows(0)(8) = True
                'ds.Tables(0).Rows(0)(9) = True
                gvUSR_SCT.DataBind()
                Dim columnCount As Integer = gvUSR_SCT.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.



                gvUSR_SCT.Rows(0).Cells.Clear()
                gvUSR_SCT.Rows(0).Cells.Add(New TableCell)
                gvUSR_SCT.Rows(0).Cells(0).ColumnSpan = columnCount
                gvUSR_SCT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvUSR_SCT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    

    Private Sub checkme(ByVal nodeList As TreeNodeCollection, ByVal Ids As String)

        Dim arSCT_IDS As String()
        Dim i As Integer

        Dim splitter As Char = "|"

        If Not nodeList Is Nothing Then

            If Ids.Contains("|") Then
                arSCT_IDS = Ids.Split(splitter)
                For i = 0 To arSCT_IDS.Length - 1

                    For Each nodec As TreeNode In nodeList
                        '  If Not nodec.Parent Is Nothing Then
                        Dim strNodeValue As String = nodec.Value
                        '   For Each node As TreeNode In nodec.Parent.ChildNodes
                        If Array.IndexOf(arSCT_IDS, nodec.Value) <> -1 Then
                            nodec.Parent.Expand()
                            nodec.Expand()
                            nodec.Select()
                            nodec.Checked = True
                            nodec.Text = "<font color='#800000'>" & nodec.Text.Replace("#1B80B6", "#800000") & "</font>"
                        End If

                        checkme(nodec.ChildNodes, Ids)
                    Next

                Next
            Else
                For Each nodec As TreeNode In nodeList
                    '  If Not nodec.Parent Is Nothing Then
                    Dim strNodeValue As String = nodec.Value
                    '   For Each node As TreeNode In nodec.Parent.ChildNodes
                    If strNodeValue = Ids Then
                        'tvmenu.SelectedNode.Checked
                        ' Label2.Text += nodec.Value & " "
                        nodec.Parent.Expand()
                        nodec.Expand()
                        nodec.Select()
                        nodec.Checked = True

                        nodec.Text = "<font color='#800000'>" & nodec.Text.Replace("#1B80B6", "#800000") & "</font>"
                    End If
                    ' Next
                    '  End If
                    checkme(nodec.ChildNodes, Ids)
                Next

            End If

        End If

    End Sub



 
    
End Class
