﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_ccaPEActivity_Copy
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
        'ts
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                BindACADEMICYEAR_FROM()
                BindACADEMICYEAR_TO()


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try
        End If


    End Sub


    Sub BindActivitySchedule_FROM()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcd_ID_FROM.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", System.DBNull.Value)
            PARAM(2) = New SqlParameter("@BSU_ID", System.DBNull.Value)
            PARAM(3) = New SqlParameter("@TYPE", "ACT")

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETDATABIND", PARAM)
            ddlSchedule_From.DataSource = ds
            ddlSchedule_From.DataTextField = "DESCR"
            ddlSchedule_From.DataValueField = "ID"
            ddlSchedule_From.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub
    Sub BindACADEMICYEAR_FROM()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ddlAcd_ID_FROM.SelectedValue)
        PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
        PARAM(2) = New SqlParameter("@BSU_ID", Session("sbsuid"))
        PARAM(3) = New SqlParameter("@TYPE", "ACD")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETDATABIND", PARAM)
        ddlAcd_ID_FROM.DataSource = ds
        ddlAcd_ID_FROM.DataTextField = "DESCR"
        ddlAcd_ID_FROM.DataValueField = "ID"
        ddlAcd_ID_FROM.DataBind()
        ddlAcd_ID_FROM.ClearSelection()
        ddlAcd_ID_FROM.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        ddlAcd_ID_FROM_SelectedIndexChanged(ddlAcd_ID_FROM, Nothing)

    End Sub
    Protected Sub ddlAcd_ID_FROM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcd_ID_FROM.SelectedIndexChanged
        BindActivitySchedule_FROM()
    End Sub
    Sub BindACADEMICYEAR_TO()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ddlAcd_ID_FROM.SelectedValue)
        PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
        PARAM(2) = New SqlParameter("@BSU_ID", Session("sbsuid"))
        PARAM(3) = New SqlParameter("@TYPE", "ACD")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETDATABIND", PARAM)
        ddlAcd_ID_To.DataSource = ds
        ddlAcd_ID_To.DataTextField = "DESCR"
        ddlAcd_ID_To.DataValueField = "ID"
        ddlAcd_ID_To.DataBind()
        ddlAcd_ID_To.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        ddlAcd_ID_To_SelectedIndexChanged(ddlAcd_ID_To, Nothing)
    End Sub
    Protected Sub ddlAcd_ID_To_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcd_ID_To.SelectedIndexChanged
        BindActivitySchedule_TO()
    End Sub
    Sub BindActivitySchedule_TO()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcd_ID_FROM.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", System.DBNull.Value)
            PARAM(2) = New SqlParameter("@BSU_ID", System.DBNull.Value)
            PARAM(1) = New SqlParameter("@TYPE", "ACT")
            chkACT.Items.Clear()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETDATABIND", PARAM)
            chkACT.DataSource = ds
            chkACT.DataTextField = "DESCR"
            chkACT.DataValueField = "ID"
            chkACT.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub
   
    
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click


        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty

        Dim AS_ID_FRM As String = String.Empty

        Dim AS_IDS_TO As String = String.Empty
        Dim GENDERS_TO As String = String.Empty
        If ddlSchedule_From.SelectedIndex = -1 Then
            lblError.Text = "In Copy From : Please add activity schedule for the seleceted academic year."
            Exit Sub
        Else
            AS_ID_FRM = ddlSchedule_From.SelectedValue
        End If
        If chkBoy.Checked = True Then
            GENDERS_TO += "M|"
        End If
        If chkGirl.Checked = True Then
            GENDERS_TO += "F|"
        End If
        If GENDERS_TO.Trim = "" Then
            lblError.Text = "In Copy To : Please select the gender to which activity score need to copied ."
            Exit Sub
        End If


        For Each item As ListItem In chkACT.Items

            If item.Selected = True Then

                AS_IDS_TO += item.Value.ToString + "|"
            End If
        Next

        If AS_IDS_TO.Trim.ToString = "" Then
            lblError.Text = "In Copy To : Please select the activity schedule"
            Exit Sub
        End If




        str_err = calltransaction(errorMessage, GENDERS_TO, AS_ID_FRM, AS_IDS_TO)
        If str_err = "0" Then


            lblError.Text = "Record Saved Successfully"

        Else
            lblError.Text = errorMessage
        End If



    End Sub
    Function calltransaction(ByRef errorMessage As String, ByVal GENDERS_TO As String, ByVal AS_ID_FRM As String, ByVal AS_IDS_TO As String) As Integer


        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer



                Dim PARAM(10) As SqlParameter
                PARAM(0) = New SqlParameter("@ACD_ID_FRM", ddlAcd_ID_FROM.SelectedValue)
                PARAM(1) = New SqlParameter("@AS_ID_FRM", AS_ID_FRM)
                PARAM(2) = New SqlParameter("@GENDER_FRM", ddlGender_From.SelectedValue)
                PARAM(3) = New SqlParameter("@ACD_ID_TO", ddlAcd_ID_To.SelectedValue)
                PARAM(4) = New SqlParameter("@AS_IDS_TO", AS_IDS_TO)
                PARAM(5) = New SqlParameter("@GENDERS_TO", GENDERS_TO)
                PARAM(6) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(6).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PE.COPYACTIVITY_DETAILS", PARAM)

                status = PARAM(6).Value

                If status = -11 Then

                    calltransaction = "-11"
                    errorMessage = "No record is available for copying !!!"
                ElseIf status <> 0 Then

                    calltransaction = "1"
                    errorMessage = "Error Occured While Copying."
                Else

                    calltransaction = "0"
                End If

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Copying."
            Finally
                If calltransaction = "-11" Then
                    calltransaction = "1"
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()

                ElseIf calltransaction <> "0" Then
                    calltransaction = "1"
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function
End Class
