﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaACEAutoAllocate.aspx.vb" Inherits="CoCurricularActivities_ccaACEAutoAllocate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 33) == 'ctl00_cphMasterpage_cblActivities') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ACE - Auto Allocate Activities"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table id="tb1" runat="server" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Level</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True">
                                        </asp:DropDownList><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Type</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Term</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr id="trAct1" runat="server">
                                    <td align="left" class="title-bg" colspan="4"><span class="field-label">Activities</span>  </td>
                                </tr>
                                <tr id="trAct2" runat="server">
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblNoData" runat="server" Visible="false" Text="No Activities For The Chosen Criteria!"></asp:Label>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" onclick="javascript:fnSelectAll(this);" /><br />
                                        <asp:CheckBoxList ID="cblActivities" runat="server" RepeatDirection="Vertical">
                                        </asp:CheckBoxList>
                                    </td>

                                </tr>
                                <tr id="trAllocButton" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button"
                                            Text="Auto Allocate Activities" />
                                    </td>
                                </tr>
                            </table>
                            <asp:XmlDataSource ID="XmlSubjects" runat="server" TransformFile="~/Curriculum/subjectXSLT.xsl" EnableCaching="False" XPath="MenuItems/MenuItem"></asp:XmlDataSource>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4"
                            valign="middle">
                            <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                                    type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

