Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class CoCurricularActivities_ccaAceGroup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                PopulateTeacher()

                Session("dtTeacher") = SetTeacherDataTable()
                ViewState("SelectedRow") = -1
                If ViewState("datamode") = "add" Then

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    PopulateGrade()
                    PopulateSection()
                    BindAce()
                    BindLevel()
                    BindTerm()
                    mnuMaster.Visible = False
                    hfSGR_ID.Value = "0"
                    ViewState("stumode") = "add"
                    btnRemove.Visible = False
                    txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlAce.SelectedItem.Text.ToString + "_"
                    hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlAce.SelectedItem.Text.ToString + "_"
                    hfAddNew.Value = "add"
                    trAdd.Visible = False
                    trBtnAdd.Visible = False

                Else
                    EnableDisableControls(False)

                    Dim li As ListItem

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("academicyear").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    ddlAcademicYear.Items.Add(li)

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("ace").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("acmid").Replace(" ", "+"))
                    ddlAce.Items.Add(li)

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("level").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("aclid").Replace(" ", "+"))
                    ddlLevel.Items.Add(li)

                    li = New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("term").Replace(" ", "+"))
                    If li.Text = "" Then
                        li.Text = "--"
                    End If
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("trmid").Replace(" ", "+"))
                    ddlTerm.Items.Add(li)



                    
                    txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    hfGROUP.Value = Encr_decrData.Decrypt(Request.QueryString("group").Replace(" ", "+"))
                    hfSGR_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sgrid").Replace(" ", "+"))



                    ViewState("stumode") = "view"
                    GridBind(gvView)

                   
                    mnuMaster.Visible = True
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()

                    GetTeacher()


                End If

               
            End If
            gvAdd.Attributes.Add("bordercolor", "#1b80b6")
            gvView.Attributes.Add("bordercolor", "#1b80b6")
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try
        End If
        '        highlight_grid()
        ViewState("slno") = 0
    End Sub


    Sub BindTerm()
        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlTerm.Items.Insert(0, li)
    End Sub





    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try

            GridBind(gvView)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
          
            GridBind(gvView)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
          
            If ViewState("stumode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
  
#Region "Private Methods"
    Public Sub PopulateGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = "SELECT distinct  grm_display  " _
                              & " ,grm_grd_id,grd_displayorder FROM " _
                              & " VW_GRADE_BSU_M AS A INNER JOIN VW_GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                              & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                              & " order by grd_displayorder"



        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

      
    End Sub

    Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM VW_SECTION_M WHERE SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)

    End Sub

    Sub BindAce()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAce.DataSource = ds
        ddlAce.DataTextField = "ACM_DESCR"
        ddlAce.DataValueField = "ACM_ID"
        ddlAce.DataBind()
    End Sub

    Sub BindLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlLevel.Items.Insert(0, li)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub

    Function SetTeacherDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "EMP_NAME"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "FROMDATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "EMP_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_SCHEDULE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SGS_ROOMNO"
        dt.Columns.Add(column)


        dt.PrimaryKey = keys
        Return dt
    End Function


     Sub GetTeacher()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        'if the login user is a teacher then display only the groups for that teacher
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')," _
                                            & " FROMDATE=SGS_FROMDATE,EMP_ID,SGS_ID,ISNULL(SGS_SCHEDULE,'') AS SGS_SCHEDULE,ISNULL(SGS_ROOMNO,'') AS SGS_ROOMNO" _
                                            & "  FROM VW_EMPLOYEE_M AS A INNER JOIN ACE_GROUPS_TEACHER_S AS B " _
                                            & " ON A.EMP_ID=B.SGS_EMP_ID WHERE SGS_SGR_ID=" + hfSGR_ID.Value + " AND " _
                                            & " SGS_TODATE IS NULL"
        ElseIf studClass.isEmpTeacher(Session("EmployeeId")) = True Then
            str_query = "SELECT EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')," _
                                            & " FROMDATE=SGS_FROMDATE,EMP_ID,SGS_ID,ISNULL(SGS_SCHEDULE,'') AS SGS_SCHEDULE,ISNULL(SGS_ROOMNO,'') AS SGS_ROOMNO" _
                                            & " FROM VW_EMPLOYEE_M AS A INNER JOIN ACE_GROUPS_TEACHER_S AS B " _
                                            & " ON A.EMP_ID=B.SGS_EMP_ID WHERE SGS_SGR_ID=" + hfSGR_ID.Value + " AND " _
                                            & " SGS_TODATE IS NULL AND SGS_EMP_ID=" + Session("EmployeeId")
        End If
        ' & " ISNULL(SGS_TODATE,'1900-01-01') BETWEEN '1900-01-01' AND '" + Format(Now.Date, "yyyy-MM-dd") + "'"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = SetTeacherDataTable()
        Dim dr As DataRow
        While reader.Read
            dr = dt.NewRow
            dr.Item(0) = reader.GetString(0)
            dr.Item(1) = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
            dr.Item(2) = reader.GetValue(2).ToString
            dr.Item(3) = "edit"
            dr.Item(4) = reader.GetValue(3)
            dr.Item(5) = reader.GetString(4)
            dr.Item(6) = reader.GetString(5)
            dt.Rows.Add(dr)
        End While
        reader.Close()
        gvTeacher.DataSource = dt
        gvTeacher.DataBind()
        Session("dtTeacher") = dt
    End Sub


    Sub AddTeacher()
        Dim dt As DataTable = Session("dtTeacher")

        Dim dr As DataRow

        Dim keys As Object()
        ReDim keys(0)
        keys(0) = ddlTeacher.SelectedValue.ToString


        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This teacher is already added"
            Exit Sub
        End If

        dr = dt.NewRow
        dr.Item(0) = ddlTeacher.SelectedItem.Text
        dr.Item(1) = txtFrom.Text
        dr.Item(2) = ddlTeacher.SelectedValue.ToString
        dr.Item(3) = "add"
        dr.Item(4) = 0
      
        dt.Rows.Add(dr)

        gvTeacher.DataSource = dt
        gvTeacher.DataBind()
        Session("dtTeacher") = dt
    End Sub


  

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

  

    Sub PopulateTeacher()
        ddlTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
          Dim str_query As String
        Dim empTeacher As Boolean = studClass.isEmpTeacher(Session("EmployeeId"))

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                          & "emp_id FROM employee_m WHERE EMP_ECT_ID IN(1,3,4) and emp_bsu_id='" + Session("sBsuid") + "' AND EMP_STATUS=1 order by emp_fname,emp_mname,emp_lname"
        Else
            str_query = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                    & "emp_id FROM employee_m WHERE EMP_ID=" + Session("EmployeeId")
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTeacher.DataSource = ds
        ddlTeacher.DataTextField = "emp_name"
        ddlTeacher.DataValueField = "emp_id"
        ddlTeacher.DataBind()
        If empTeacher = False Then
            Dim li As New ListItem
            li.Text = ""
            li.Value = 0
            ddlTeacher.Items.Insert(0, li)
        End If
    End Sub

    Sub GridBind(ByVal gvGrid As GridView)

        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim strQuery As String
        Dim options As Integer
         If ViewState("stumode") = "add" Then
            strQuery = "SELECT SSD_ID=0,STU_ID,STU_GRD_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,SCT_DESCR,STU_SCT_ID,GRM_DISPLAY FROM " _
                     & " VW_STUDENT_M AS A INNER JOIN VW_SECTION_M AS B ON A.STU_SCT_ID=B.SCT_ID" _
                     & " INNER JOIN VW_GRADE_BSU_M AS C ON A.STU_GRM_ID=C.GRM_ID" _
                     & " WHERE STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                     & " AND STU_ID NOT IN(SELECT SSD_STU_ID FROM ACE_STUDENT_GROUPS_S" _
                     & " WHERE SSD_SGR_ID=" + hfSGR_ID.Value + ")" _
                     & " AND STU_CURRSTATUS='EN'"

            If ddlSection.SelectedValue.ToString <> "0" Then
                strQuery += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
            End If

            If ddlGrade.SelectedValue.ToString <> "0" Then
                strQuery += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
            End If

            If txtStudentID.Text <> "" Then
                strQuery += " AND STU_NO LIKE '%" + txtStudentID.Text + "%'"
            End If

            If txtStudentName.Text <> "" Then
                strQuery += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtStudentName.Text + "%'"
            End If

        Else
            strQuery = "SELECT STU_ID,STU_GRD_ID,SSD_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
                         & " SCT_DESCR,STU_SCT_ID,GRM_DISPLAY FROM VW_STUDENT_M  AS A INNER JOIN" _
                         & " ACE_STUDENT_GROUPS_S AS B ON A.STU_ID=B.SSD_STU_ID " _
                         & " INNER JOIN vw_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID" _
                          & " INNER JOIN VW_GRADE_BSU_M AS D ON A.STU_GRM_ID=D.GRM_ID" _
                         & " WHERE SSD_SGR_ID = " + hfSGR_ID.Value

        End If



        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox

        Dim ddlgvHouse As New DropDownList
        Dim selectedHouse As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvOption As New DropDownList

        Dim selectedSection As String = ""
        Dim selectedGrade As String = ""
        
        If gvGrid.Rows.Count > 0 Then

            ddlgvHouse = gvGrid.HeaderRow.FindControl("ddlgvHouse")

            txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            ddlgvGrade = gvGrid.HeaderRow.FindControl("ddlgvGrade")


            If ddlgvSection.Text <> "ALL" Then

                strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"

                selectedSection = ddlgvSection.Text

            End If


            If ddlgvGrade.Text <> "ALL" Then

                strFilter = strFilter + " and GRM_DISPLAY='" + ddlgvGrade.Text + "'"

                selectedGrade = ddlgvGrade.Text

            End If



            If strFilter <> "" Then
                strQuery += strFilter
            End If



        End If

        strQuery += " ORDER BY SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvGrid.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            'If ViewState("stumode") = "view" Then
            '    btnRemove.Visible = False
            'Else
            '    btnAllocate.Visible = False
            'End If
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGrid.DataBind()
            Dim columnCount As Integer = gvGrid.Rows(0).Cells.Count
            gvGrid.Rows(0).Cells.Clear()
            gvGrid.Rows(0).Cells.Add(New TableCell)
            gvGrid.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGrid.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            If ViewState("stumode") = "view" Then
                gvGrid.Rows(0).Cells(0).Text = "No records to view"
            Else
                gvGrid.Rows(0).Cells(0).Text = "No records to add"
            End If
        Else
            gvGrid.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = strName

        txtSearch = New TextBox
        txtSearch = gvGrid.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strNo



        Dim dt As DataTable = ds.Tables(0)
        If gvGrid.Rows.Count > 0 Then


            ddlgvSection = gvGrid.HeaderRow.FindControl("ddlgvSection")
            ddlgvGrade = gvGrid.HeaderRow.FindControl("ddlgvGrade")

            Dim dr As DataRow


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")
            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")
    
            Dim i As Integer

                
            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvGrade.Items.FindByText(.Item(7)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(7))
                    End If
                    If ddlgvSection.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(5))
                    End If
                End With

            Next
     

        End If

        If selectedGrade <> "" Then
            ddlgvGrade.Text = selectedGrade
        End If

        If selectedSection <> "" Then
            ddlgvSection.Text = selectedSection
        End If
    End Sub

    Public Function getSerialNoAdd()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvAdd.PageSize * gvAdd.PageIndex)
    End Function
    Public Function getSerialNoView()
        ViewState("slno") += 1
        Return ViewState("slno") + (gvView.PageSize * gvView.PageIndex)
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvView.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAdd.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAdd.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAdd.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAdd.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub highlight_grid()
        If ViewState("stumode") = "view" Then
            For i As Integer = 0 To gvView.Rows.Count - 1
                Dim row As GridViewRow = gvView.Rows(i)
                Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
                If isSelect Then
                    row.BackColor = Drawing.Color.FromName("#f6deb2")
                Else
                    row.BackColor = Drawing.Color.Transparent
                End If
            Next
        Else
            For i As Integer = 0 To gvAdd.Rows.Count - 1
                Dim row As GridViewRow = gvAdd.Rows(i)
                Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
                If isSelect Then
                    row.BackColor = Drawing.Color.FromName("#f6deb2")
                Else
                    row.BackColor = Drawing.Color.Transparent
                End If
            Next
        End If
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim transaction As SqlTransaction
        Dim str_query As String
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                str_query = "exec saveACE_GROUP_M  " _
                             & hfSGR_ID.Value.ToString + "," _
                             & "'" + Session("sbsuid") + "'," _
                             & ddlAcademicYear.SelectedValue.ToString + "," _
                             & "'" + ddlAce.SelectedValue.ToString + "'," _
                             & ddlLevel.SelectedValue.ToString + "," _
                             & "'" + txtDescr.Text + "'," _
                             & "'" + ViewState("datamode") + "'," _
                             & "'" + ddlTerm.SelectedValue.ToString + "'"
                hfSGR_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                SaveTeacher(transaction)

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using

    End Sub
    Sub SaveTeacher(ByVal transaction As SqlTransaction)
        Dim str_query As String
        Dim dt As DataTable = Session("dtTeacher")
        Dim dr As DataRow
        For Each dr In dt.Rows
            If dr.Item(3) <> "edit" And dr.Item(3) <> "remove" Then
                str_query = "exec saveACEGROUPTEACHER " + dr.Item(4) + "," _
                                        & hfSGR_ID.Value.ToString + "," _
                                        & dr.Item(2) + "," _
                                        & "'" + Format(Date.Parse(dr.Item(1)), "MM/dd/yyyy") + "'," _
                                        & "'" + dr.Item(3) + "'," _
                                        & "'" + dr.Item(5) + "'," _
                                        & "'" + dr.Item(6) + "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
            End If
        Next
    End Sub

    Function ValidateTeacher() As Boolean
        If gvTeacher.Rows.Count = 0 Then
            If ddlTeacher.SelectedValue <> 0 And txtFrom.Text = "" Then
                lblError.Text = "Please enter data in the field from date"
                Return False
            End If
        End If
        Return True
    End Function



    Function isGroupNameExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(SGR_ID) FROM GROUPS_M WHERE SGR_DESCR='" + txtDescr.Text + "' AND SGR_BSU_ID='" + Session("sbsuid") + "'" _
                                & " AND SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Function isStudentsExits() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT COUNT(SSD_ID) FROM ACE_STUDENT_GROUPS_S WHERE SSD_SGR_ID=" + hfSGR_ID.Value.ToString
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Sub EnableDisableControls(ByVal value As Boolean)
        ddlAcademicYear.Enabled = value
        ddlAce.Enabled = value
        ddlLevel.Enabled = value
        txtDescr.Enabled = value
        ddlTeacher.Enabled = value
        txtFrom.Enabled = value
        btnAddNew.Enabled = value
        gvTeacher.Enabled = value
        imgFrom.Enabled = value
        ddlTerm.Enabled = value
    End Sub

    Sub SaveStudGroup(ByVal gvGrid As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String
        Dim chkSelect As CheckBox
        Dim lblSsdId As Label
        Dim lblGrdId As Label
        Dim lblStuId As Label
        Dim i As Integer



        'if the optional  subjects has child subjects the add students to the child subject
        For i = 0 To gvGrid.Rows.Count - 1
            chkSelect = gvGrid.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSsdId = gvGrid.Rows(i).FindControl("lblSsdid")
                lblGrdId = gvGrid.Rows(i).FindControl("lblGrdid")
                lblStuId = gvGrid.Rows(i).FindControl("lblStuid")
                str_query = "exec saveACESTUDENTGROUP " + lblSsdId.Text + "," _
                          & ddlAcademicYear.SelectedValue.ToString + "," _
                          & ddlAce.SelectedValue.ToString + "," _
                          & hfSGR_ID.Value.ToString + "," _
                          & "'" + lblGrdId.Text + "'," _
                          & lblStuId.Text + ",'" + IIf(ViewState("stumode") = "view", "delete", "add") + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next




    End Sub

   
  
    Sub BindTeacher(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetTeacherDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(3) <> "delete" And dt.Rows(i)(3) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvTeacher.DataSource = dtTemp
        gvTeacher.DataBind()
    End Sub

    Private Sub ResetScrollPosition()
        If Not ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "CreateResetScrollPosition") Then
            'Create the ResetScrollPosition() function
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "CreateResetScrollPosition", _
                             "function ResetScrollPosition() {" & vbCrLf & _
                             " var scrollX = document.getElementById('__SCROLLPOSITIONX');" & vbCrLf & _
                             " var scrollY = document.getElementById('__SCROLLPOSITIONY');" & vbCrLf & _
                             " if (scrollX && scrollY) {" & vbCrLf & _
                             "    scrollX.value = 0;" & vbCrLf & _
                             "    scrollY.value = 0;" & vbCrLf & _
                             " }" & vbCrLf & _
                             "}", True)

            'Add the call to the ResetScrollPosition() function
            ClientScript.RegisterStartupScript(Me.GetType(), "CallResetScrollPosition", "ResetScrollPosition();", True)
        End If
    End Sub

#End Region


 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Try
        lblError.Text = ""
        If ValidateTeacher() = True Then
            If ViewState("datamode") = "add" Then
                mvMaster.ActiveViewIndex = 1
                If isGroupNameExists() = False Then
                    SaveData()
                Else
                    lblError.Text = "The group " + txtDescr.Text + " already exists for the academicyear" + ddlAcademicYear.SelectedItem.Text
                    Exit Sub
                End If
                ' GridBind(gvAdd)
            End If
            If ViewState("datamode") = "edit" Then
                If hfGROUP.Value.Trim <> txtDescr.Text.Trim Then
                    If isGroupNameExists() = False Then
                        SaveData()
                    Else
                        lblError.Text = "The group " + txtDescr.Text + " already exists for the academicyear" + ddlAcademicYear.SelectedItem.Text
                        Exit Sub
                    End If
                Else
                    SaveData()
                End If
            End If
            ViewState("datamode") = "view"
            hfAddNew.Value = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            EnableDisableControls(False)

            mnuMaster.Visible = True
            'mnuMaster.Items(0).ImageUrl = "~/Images/tabmenu/btnViewStudents1.jpg"
            'mnuMaster.Items(1).ImageUrl = "~/Images/tabmenu/btnAllocateStudents2.jpg"

        End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    lblError.Text = "Request could not be processed"
        'End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "delete"
            If isStudentsExits() = False Then
                SaveData()
            Else
                lblError.Text = "Cannot delete the groups for which students are allocated"
                Exit Sub
            End If
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            txtDescr.Enabled = True
            ddlTeacher.Enabled = True
            txtFrom.Enabled = True
            btnAddNew.Enabled = True
            gvTeacher.Enabled = True
            imgFrom.Enabled = True
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click
        Try
            ' lbView.Visible = True
            ' lbView.Width = 70
            SaveStudGroup(gvAdd)
            GridBind(gvAdd)
            lblError.Text = "Records added successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
            lblError1.BackColor = Drawing.Color.Wheat
        End Try
    End Sub

    Protected Sub gvAdd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAdd.PageIndexChanging
        Try
            gvAdd.PageIndex = e.NewPageIndex
            GridBind(gvAdd)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvView.PageIndexChanging
        Try
            gvView.PageIndex = e.NewPageIndex
            GridBind(gvView)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                BindTerm()
                PopulateGrade()
                PopulateSection()
                BindAce()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
   

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            'If checkAssesmentScheduled() = True Then
            '    lblError1.Text = "Cannot remove students from this group as assesments are already created for this group." _
            '                  & vbCrLf & " Note:To move students from one group to another please use the student change group option."
            '    lblError1.BackColor = Drawing.Color.Wheat
            '    Exit Sub
            'End If
            'If checkReportDataAdded() = True Then
            '    lblError1.Text = "Cannot remove students from this group as data is already add for report processing." _
            '                     & vbCrLf & " Note:To move students from one group to another please use the student change group option."
            '    lblError1.BackColor = Drawing.Color.Wheat
            '    Exit Sub
            'End If
            SaveStudGroup(gvView)
            GridBind(gvView)
            lblError.Text = "Records removed successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError1.Text = "Request could not be processed"
            lblError1.BackColor = Drawing.Color.Wheat
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            If ViewState("datamode") = "add" Then
                txtDescr.Text = ""
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            ElseIf ViewState("datamode") = "edit" Then
                txtDescr.Enabled = False
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            hfAddNew.Value = "add"
            If ViewState("SelectedRow") = -1 Then
                AddTeacher()
            Else
                txtFrom.Enabled = True
                txtFrom.Text = ""
            End If
              ViewState("SelectedRow") = -1
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTeacher_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTeacher.RowCommand
        Try

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvTeacher.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtTeacher")

            Dim lblEmpId As Label


            lblEmpId = selectedRow.FindControl("lblEmpId")

            ReDim keys(0)

            keys(0) = lblEmpId.Text



            If e.CommandName = "delete" Then
                If txtFrom.Text = "" Then
                    lblError.Text = "Please enter data in the field from date"
                    Exit Sub
                End If

                index = dt.Rows.IndexOf(dt.Rows.Find(keys))

                If dt.Rows(index).Item(3) = "add" Then
                    dt.Rows(index).Item(3) = "remove"
                Else
                    dt.Rows(index).Item(1) = txtFrom.Text
                    dt.Rows(index).Item(3) = "delete"
                End If
                Session("dtSubject") = dt
                BindTeacher(dt)
            ElseIf e.CommandName = "edit" Then
                ViewState("SelectedRow") = index
                btnAddNew.Text = "Update Teacher"
                With dt.Rows(index)
                    txtFrom.Text = Format(Date.Parse(.Item(1)), "dd/MMM/yyyy")
                    txtFrom.Enabled = False
                End With
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTeacher_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTeacher.RowDeleting

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' Response.Redirect(Request.Url.AbsoluteUri.Replace("4xCdg%2fcr4Xw%3d", Encr_decrData.Encrypt("add")))
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Curriculum\clmGroup_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub



   

  

    Protected Sub gvTeacher_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvTeacher.RowEditing

    End Sub

    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuMaster.MenuItemClick
        Dim Iindex As Integer = Int32.Parse(e.Item.Value)
        Dim i As Integer
        'Make the selected menu item reflect the correct imageurl
        For i = 0 To mnuMaster.Items.Count - 1
            Select Case i
                Case 0
                    If i = e.Item.Value Then
                        '    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnViewStudents2.jpg"
                    Else
                        '   mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnViewStudents1.jpg"
                    End If
                    mvMaster.ActiveViewIndex = Iindex


                Case 1
                    If i = e.Item.Value Then
                        '   mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnAllocateStudents2.jpg"
                    Else
                      '  mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnAllocateStudents1.jpg"
                    End If
                    mvMaster.ActiveViewIndex = Iindex
            End Select
        Next


        If mvMaster.ActiveViewIndex = 0 Then
            Try

                lblError.Text = ""
                If ViewState("stumode") = "add" Then
                    ViewState("stumode") = "view"
                    btnAllocate.Visible = True
                    btnRemove.Visible = True
                    gvView.DataBind()
                    GridBind(gvView)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
            Try
                If ViewState("stumode") = "view" Then
                    PopulateGrade()
                    PopulateSection()
                    trAdd.Visible = False
                    trBtnAdd.Visible = False
                    ViewState("stumode") = "add"
                    gvAdd.DataBind()
                    ' GridBind(gvAdd)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GridBind(gvAdd)
        trAdd.Visible = True
        trBtnAdd.Visible = True
    End Sub

    Protected Sub ddlAce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAce.SelectedIndexChanged
        If ViewState("stumode") = "add" Then
            txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlAce.SelectedItem.Text.ToString + "_"
            hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlAce.SelectedItem.Text.ToString + "_"
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
    End Sub

    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        If ViewState("stumode") = "add" Then
            If ddlTerm.SelectedValue = 0 Then
                txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlAce.SelectedItem.Text.ToString + "_"
                hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_" + ddlAce.SelectedItem.Text.ToString + "_"
            Else
                txtDescr.Text = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_T" + ddlTerm.SelectedIndex.ToString + "_" + ddlAce.SelectedItem.Text.ToString + "_"
                hfDescr.Value = ddlAcademicYear.SelectedItem.Text.Substring(2, 2) + "_T" + ddlTerm.SelectedIndex.ToString + "_" + ddlAce.SelectedItem.Text.ToString + "_"
            End If
        End If
    End Sub
End Class
