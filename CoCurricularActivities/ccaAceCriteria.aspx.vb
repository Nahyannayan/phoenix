Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Math
Partial Class CoCurricularActivities_ccaAceCriteria
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "CC10040") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindReportCard()
                    BindAce()
                    BindLevel()

                    If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
                        GetRows()
                        ViewState("SelectedRow") = -1
                    End If
                    'ts
                    lblAce.Text = ddlAce.SelectedItem.Text + " " + IIf(ddlLevel.SelectedItem.Text = "--", "", ddlLevel.SelectedItem.Text)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindReportCard()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT RSM_DESCR,RSM_ID FROM RPT.ACE_REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        str_query += " ORDER BY RSM_DISPLAYORDER "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReportCard.DataSource = ds
        ddlReportCard.DataTextField = "RSM_DESCR"
        ddlReportCard.DataValueField = "RSM_ID"
        ddlReportCard.DataBind()
    End Sub

    Sub BindAce()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAce.DataSource = ds
        ddlAce.DataTextField = "ACM_DESCR"
        ddlAce.DataValueField = "ACM_ID"
        ddlAce.DataBind()
    End Sub

    Sub BindLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlLevel.Items.Insert(0, li)
    End Sub
  

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_ID"
        dt.Columns.Add(column)

       
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_HEADER"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_DISPLAYORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.Boolean")
        column.ColumnName = "bENABLE"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function

    Sub AddRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        dt = Session("dtCriteria")
        Dim keys As Object()
        ReDim keys(0)

        keys(0) = txtShort.Text

        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This subject criteria is already added"
            Exit Sub
        End If

        dr = dt.NewRow

        dr.Item(0) = 0
        dr.Item(1) = txtShort.Text
        dr.Item(2) = txtDetail.Text.Replace("'", "''")
        dr.Item(3) = txtOrder.Text
        dr.Item(4) = "add"
        dr.Item(5) = False
        dt.Rows.Add(dr)
        Session("dtCriteria") = dt
        GridBind(dt)

    End Sub

    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(4) <> "delete" And dt.Rows(i)(4) <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvCriteria.DataSource = dtTemp
        gvCriteria.DataBind()
    End Sub

    Sub GridBindDefault(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDefaultValuesDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)("MODE") <> "delete" And dt.Rows(i)("MODE") <> "remove" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next
        gvDefault.DataSource = dtTemp
        gvDefault.DataBind()
    End Sub

   

    Sub EditRows()
        Try
            Dim dt As New DataTable
            dt = Session("dtCriteria")
            Dim index As Integer = ViewState("SelectedRow")
            With dt.Rows(index)
                .Item(1) = txtShort.Text
                .Item(2) = txtDetail.Text.Replace("'", "''")
                .Item(3) = txtOrder.Text
                If .Item(4) = "edit" Then
                    .Item(4) = "update"
                End If
                .Item(5) = True
            End With
            Session("dtCriteria") = dt
            GridBind(dt)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub GetRows()
        Dim dr As DataRow
        Dim dt As New DataTable
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String

        str_query = "SELECT RSD_ID,RSD_HEADER,isnull(RSD_DESCR,''),ISNULL(RSD_DISPLAYORDER,1),bENABLE='TRUE' FROM " _
                 & " RPT.ACE_REPORT_SETUP_D WHERE RSD_ACM_ID='" + ddlAce.SelectedValue.ToString + "'" _
                 & " AND RSD_RSM_ID='" + ddlReportCard.SelectedValue.ToString + "'"

        str_query += " AND ISNULL(RSD_ACL_ID,0)=" + ddlLevel.SelectedValue.ToString
    
        Session("dtCriteria") = SetDataTable()
        dt = Session("dtCriteria")
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                dr = dt.NewRow
                dr.Item(0) = .GetValue(0)
                dr.Item(1) = .GetValue(1)
                dr.Item(2) = .GetString(2)
                dr.Item(3) = .GetValue(3)
                dr.Item(5) = CType(.GetString(4), Boolean)
                dr.Item(4) = "edit"
                dt.Rows.Add(dr)
            End With
        End While
        reader.Close()

        gvCriteria.DataSource = dt
        gvCriteria.DataBind()
        Session("dtCriteria") = dt
    End Sub


    Sub SaveData()
        Dim str_query As String

        Dim dt As DataTable
        dt = Session("dtCriteria")
        Dim dr As DataRow
        Dim RSD_ID As Integer
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        If .Item(4) <> "delete" And .Item(4) <> "edit" Then

                            str_query = " exec RPT.saveACECRITERIA " _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & dr.Item(0) + "," _
                            & ddlAce.SelectedValue.ToString + "," _
                            & "'" + dr.Item(1) + "'," _
                            & "'" + dr.Item(2) + "'," _
                            & dr.Item(3) + "," _
                            & ddlLevel.SelectedValue.ToString + "," _
                            & "'" + .Item(4) + "'"


                            RSD_ID = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                        End If
                    End With
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub ClearControls()
        txtDetail.Text = ""
        txtOrder.Text = ""
        txtShort.Text = ""
    End Sub


    Sub EnableDisableControls(ByVal Value As Boolean)
        ddlReportCard.Enabled = Value
        ddlAcademicYear.Enabled = Value
        ddlAce.Enabled = Value
        ddlLevel.Enabled = Value
    End Sub



    Function SetDefaultValuesDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSD_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ACM_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSP_DESCR"
        keys(0) = column
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSP_COMMENT"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSP_DISPLAYORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "RSP_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "bENABLE"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function


    Sub GetDefaultValues(ByVal rsd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ISNULL(RSP_DESCR,'') AS RSP_DESCR,ISNULL(RSP_COMMENT,'') AS RSP_COMMENT,ISNULL(RSP_DISPLAYORDER,0) RSP_DISPLAYORDER,RSP_ID" _
                                  & " FROM RPT.ACE_REPORTSETUP_DEFAULTS_S WHERE RSP_RSD_ID=" + rsd_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable = Session("DFTABLE")
        Dim dr As DataRow


        Dim i As Integer
        ViewState("SRNO") = 1
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item("ID") = ViewState("SRNO")
                dr.Item("RSD_ID") = hfRSD_ID.Value
                dr.Item("RSP_DESCR") = .Item(0)
                dr.Item("RSP_COMMENT") = .Item(1)
                dr.Item("RSP_DISPLAYORDER") = .Item(2)
                dr.Item("MODE") = "edit"
                dr.Item("RSP_ID") = .Item(3)
                dt.Rows.Add(dr)
                ViewState("SRNO") += 1
            End With
        Next

        gvDefault.DataSource = dt
        gvDefault.DataBind()
        Session("DFTABLE") = dt
    End Sub


    Sub AddDefault()
        Try
            Dim i As Integer
            Dim rDt As DataRow
            Dim dt As New DataTable
            dt = Session("DFTABLE")
            If dt Is Nothing Then
                dt = SetDefaultValuesDataTable()
            End If

            Dim keys As Object()
            ReDim keys(0)

            keys(0) = txtDefault.Text

            Dim row As DataRow = dt.Rows.Find(keys)
            If Not row Is Nothing Then
                lblError1.Text = "This default value is already added"
                Exit Sub
            End If

            rDt = Session("DFTABLE").NewRow
            rDt("ID") = CInt(ViewState("SRNO"))
            rDt("RSP_ID") = 0
            rDt("RSP_DESCR") = txtDefault.Text
            rDt("RSP_COMMENT") = txtComment.Text
            rDt("RSD_ID") = hfRSD_ID.Value
            rDt("RSP_DISPLAYORDER") = txtDefaultOrder.Text
            rDt("MODE") = "add"
            rDt("bENABLE") = "false"
            dt.rows.add(rDt)
            ViewState("SRNO") = ViewState("SRNO") + 1
            GridBindDefault(dt)
            Session("DFTABLE") = dt
        Catch ex As Exception
            lblError.Text = "Request couldnot be processed"
        End Try
    End Sub

    Sub EditDefault()
        Dim row As GridViewRow = gvDefault.Rows(gvDefault.SelectedIndex)

        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        'For iEdit = 0 To Session("BRAND_DETAIL").Rows.Count - 1
        'Next
        For iEdit = 0 To Session("DFTABLE").Rows.Count - 1
            If iIndex = Session("DFTABLE").Rows(iEdit)("ID") Then
                Session("DFTABLE").Rows(iEdit)("RSP_DESCR") = txtDefault.Text
                Session("DFTABLE").Rows(iEdit)("RSP_COMMENT") = txtComment.Text
                Session("DFTABLE").Rows(iEdit)("RSP_DISPLAYORDER") = txtDefaultOrder.Text
                If Session("DFTABLE").Rows(iEdit)("MODE") = "edit" Then
                    Session("DFTABLE").Rows(iEdit)("MODE") = "update"
                End If
                Exit For
            End If
        Next

        gvDefault.Columns(3).Visible = True
        gvDefault.Columns(4).Visible = True


        btnStudSave.Enabled = True
        btnAddDetail.Visible = True
        btnUpdateDetail.Visible = False
        gvDefault.SelectedIndex = -1
        GridBindDefault(Session("DFTABLE"))
    End Sub

    Sub SaveDefault()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String
        Dim dt As DataTable = Session("DFTABLE")

        Dim i As Integer
        Try
            For i = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("MODE") <> "edit" Then
                    str_query = "EXEC [RPT].[saveACEREPORTDEFAULTS]  " _
                            & dt.Rows(i)("RSP_ID") + "," _
                            & ddlReportCard.SelectedValue.ToString + "," _
                            & hfRSD_ID.Value.ToString + "," _
                            & ddlAce.SelectedValue.ToString + "," _
                            & "'" + dt.Rows(i)("RSP_DESCR").ToString.Replace("'", "''") + "'," _
                            & "'" + dt.Rows(i)("RSP_COMMENT").ToString.Replace("'", "''") + "'," _
                            & Val(dt.Rows(i)("RSP_DISPLAYORDER")).ToString + "," _
                            & "'" + dt.Rows(i)("MODE") + "'"
                    dt.Rows(i)("RSP_ID") = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            Next
            lblError1.Text = "Record Saved Successfully"
        Catch myex As ArgumentException
            lblError1.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Catch ex As Exception
            lblError1.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#End Region


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReportCard()
        BindAce()
    End Sub



    Protected Sub btnAddCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCriteria.Click
        If ViewState("SelectedRow") = -1 Then
            AddRows()
        Else
            EditRows()
        End If
        EnableDisableControls(False)
        ClearControls()

        ViewState("SelectedRow") = -1
        btnAddCriteria.Text = "Add Criteria"

    End Sub

    Protected Sub gvCriteria_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCriteria.RowCommand
        Try
            If e.CommandName = "edit" Or e.CommandName = "add" Or e.CommandName = "delete" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvCriteria.Rows(index), GridViewRow)
                Dim lblCriteriaShort As Label
                Dim lblCriteria As Label
                Dim lblRdmId As Label
                Dim lblOrder As Label

                With selectedRow
                    lblRdmId = .FindControl("lblRdmId")
                    lblCriteriaShort = .FindControl("lblCriteriaShort")
                    lblCriteria = .FindControl("lblCriteria")
                    lblOrder = .FindControl("lblOrder")
                End With

                Dim keys As Object()
                Dim dt As DataTable
                dt = Session("dtCriteria")

                ReDim keys(0)
                keys(0) = lblCriteriaShort.Text
                index = dt.Rows.IndexOf(dt.Rows.Find(keys))

                If e.CommandName = "edit" Then
                    txtDetail.Text = lblCriteria.Text
                    txtShort.Text = lblCriteriaShort.Text
                    txtOrder.Text = lblOrder.Text

                    btnAddCriteria.Text = "Update Criteria"
                    ViewState("SelectedRow") = index
                ElseIf e.CommandName = "add" Or e.CommandName = "delete" Then
                    If dt.Rows(index).Item(4) = "add" Then
                        dt.Rows(index).Item(4) = "delete"
                    ElseIf dt.Rows(index).Item(4) = "edit" Then
                        dt.Rows(index).Item(4) = "remove"
                    End If
                    dt.Rows(index).Item(2) = hfRandom.Value
                    hfRandom.Value += 1
                    Session("dtCriteria") = dt
                    GridBind(dt)
                End If
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvCriteria_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCriteria.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblRdmId As Label
            Dim ddlDefaultvalues As DropDownList

            lblRdmId = e.Row.FindControl("lblRdmId")

        End If
    End Sub

    Protected Sub gvCriteria_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvCriteria.RowDeleting

    End Sub

    Protected Sub gvCriteria_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvCriteria.RowEditing

    End Sub

    Protected Sub ddlReportCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportCard.SelectedIndexChanged
        GetRows()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GetRows()
        EnableDisableControls(True)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        EnableDisableControls(True)
        GetRows()
    End Sub

    Protected Sub lblEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblRsdId As Label
        Dim lblRpfId As Label

        lblRsdId = TryCast(sender.findcontrol("lblRsdId"), Label)
        lblRpfId = TryCast(sender.findcontrol("lblRpfId"), Label)

        ViewState("SRNO") = 1
        Session("DFTABLE") = SetDefaultValuesDataTable()

        hfRSD_ID.Value = lblRsdId.Text
        txtDefault.Text = ""
        txtComment.Text = ""
        txtDefaultOrder.Text = ""

        btnAddDetail.Visible = True
        btnUpdateDetail.Visible = True

        btnStudSave.Enabled = True

        GetDefaultValues(lblRsdId.Text)
        mdefault.Show()
    End Sub

    Protected Sub gvDefault_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDefault.PageIndexChanging

    End Sub

    Protected Sub gvDefault_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDefault.RowDeleting
        mdefault.Show()
        Dim COND_ID As Integer = CInt(gvDefault.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("DFTABLE").Rows.Count
            If Session("DFTABLE").rows(i)("Id") = COND_ID Then
                If Session("DFTABLE").rows(i)("MODE") = "add" Then
                    Session("DFTABLE").rows(i).delete()
                Else
                    Session("DFTABLE").rows(i)("MODE") = "delete"
                End If
                Exit While
            Else
                i = i + 1
            End If
        End While
        GridBindDefault(Session("DFTABLE"))
    End Sub


    Protected Sub gvDefault_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDefault.RowEditing
        mdefault.Show()
        btnStudSave.Enabled = False
        btnUpdateDetail.Visible = True
        gvDefault.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvDefault.Rows(e.NewEditIndex)
        gvDefault.Columns(3).Visible = False
        gvDefault.Columns(4).Visible = False
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        For iEdit = 0 To Session("DFTABLE").Rows.Count - 1
            If iIndex = Session("DFTABLE").Rows(iEdit)("ID") Then
                txtDefault.Text = Session("DFTABLE").Rows(iEdit)("RSP_DESCR")
                txtComment.Text = Session("DFTABLE").Rows(iEdit)("RSP_COMMENT")
                txtDefaultOrder.Text = Session("DFTABLE").Rows(iEdit)("RSP_DISPLAYORDER")
                Exit For
            End If
        Next

        btnAddDetail.Visible = False
        btnUpdateDetail.Visible = True
        GridBindDefault(Session("DFTABLE"))

    End Sub

    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetail.Click
        mdefault.Show()
        AddDefault()
        txtDefault.Text = ""
        txtComment.Text = ""
        txtDefaultOrder.Text = ""
    End Sub

    Protected Sub btnUpdateDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDetail.Click
        mdefault.Show()
        EditDefault()
    End Sub

    Protected Sub btnStudSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStudSave.Click
        mdefault.Show()
        SaveDefault()
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub ddlAce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAce.SelectedIndexChanged
        lblAce.Text = ddlAce.SelectedItem.Text + " " + IIf(ddlLevel.SelectedItem.Text = "--", "", ddlLevel.SelectedItem.Text)
        GetRows()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        lblAce.Text = ddlAce.SelectedItem.Text + " " + IIf(ddlLevel.SelectedItem.Text = "--", "", ddlLevel.SelectedItem.Text)
        GetRows()
    End Sub
End Class
