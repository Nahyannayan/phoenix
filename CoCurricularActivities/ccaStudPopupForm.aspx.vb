Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class ccaStudPopupForm
    Inherits System.Web.UI.Page
    'Shared Session("liUserList") As List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Session("liUserList") = Nothing
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
                Session("liUserList") = Nothing
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        Dim strMultiSel As String = "true"
        If strMultiSel <> String.Empty And strMultiSel <> "" Then
            If String.Compare("False", strMultiSel, True) = 0 Then
                gvGroup.Columns(1).Visible = False
                gvGroup.Columns(3).Visible = False
                gvGroup.Columns(4).Visible = True
                'DropDownList1.Visible = False
                btnFinish.Visible = False
                chkSelAll.Visible = False
            Else
                gvGroup.Columns(1).Visible = True
                gvGroup.Columns(3).Visible = True
                gvGroup.Columns(4).Visible = False
                'DropDownList1.Visible = True
                btnFinish.Visible = True
            End If
        Else
            ViewState("multiSel") = True
            gvGroup.Columns(1).Visible = True
            gvGroup.Columns(3).Visible = True
            gvGroup.Columns(4).Visible = False
            'DropDownList1.Visible = True
            btnFinish.Visible = True
        End If
        If Page.IsPostBack = False Then

            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"



            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            h_GRD_ID.Value = Request.QueryString("GRD_ID")
            h_SCT_ID.Value = Request.QueryString("SCT_ID")
            h_ACD_ID.Value = Request.QueryString("ACD_ID")
            h_TYPE.Value = Request.QueryString("TYPE")

            GridBind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
      
        Dim strACD_ID As String = Request.QueryString("ACD_ID")
        Dim strSGR_ID As String = Request.QueryString("SGR_ID")
        Dim strSBG_ID As String = Request.QueryString("ACM_ID")


        h_SGR_ID.value = Request.QueryString("SGR_ID")

        GridBindStudentForMarkDetails(strACD_ID, strSGR_ID, strSBG_ID)

         
    End Sub

   
    Sub GridBindStudentDetails(ByVal vIDs As String, ByVal vTYPE As String, ByVal bGETSTU_NO As Boolean, Optional ByVal vSCT_IDs As String = "")
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name, strOrderBy As String
            Dim str_mode As String
            Dim str_txtCode, str_txtName, str_BSUName As String
            str_filter_code = ""
            str_filter_name = ""
            str_mode = ""

            str_txtCode = ""
            str_txtName = ""
            str_BSUName = ""
            strOrderBy = ""
            If bGETSTU_NO Then
                str_query_header = "SELECT DISTINCT CAST(STU_ID AS VARCHAR) + '___' + STU_NO  ID, STU_NO DESCR1, STU_NAME DESCR2 "
            Else
                str_query_header = "SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2 "
            End If
            Select Case vTYPE
                'code modified by dhanya
                Case "SGR_ID"
                    str_query_header += " FROM STUDENT_GROUPS_S INNER JOIN vw_STUDENT_DETAILS " & _
                    " ON STUDENT_GROUPS_S.SSD_STU_ID = vw_STUDENT_DETAILS.STU_ID " & _
                    "  WHERE STU_BSU_ID = '" & Session("sBSUID") & "'"
                    If vIDs <> "" Then
                        str_query_header += "AND SSD_SGR_ID IN ('" & vIDs.Replace("___", "','") & "')"
                    End If
                Case "GRD_ID"
                    str_query_header += " FROM vw_STUDENT_DETAILS " & _
                        " INNER JOIN OASIS..STUDENT_PROMO_S ON STP_STU_ID=STU_ID " & _
                        "WHERE STU_BSU_ID = '" & Session("sBSUID") & "'"
                    If vIDs <> "" Then
                        str_query_header += "AND STP_GRD_ID IN ('" & vIDs.Replace("___", "','") & "')"
                    End If
                    If vSCT_IDs <> "" Then
                        str_query_header += "AND STP_SCT_ID IN ('" & vSCT_IDs.Replace("___", "','") & "')"
                    End If
                    Dim strACD_ID As String = Request.QueryString("ACD_ID")
                    If strACD_ID <> "" Then
                        str_query_header += " AND STP_ACD_ID =" & strACD_ID
                    End If
                    str_query_header += " and stu_currstatus<>'CN' " ' and (stu_leavedate is not null or stu_leavedate>getDate())"

            End Select

            strOrderBy = " ORDER BY STU_NAME"
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("STU_NO", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If ViewState("multiSel") Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("STU_NAME", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & strOrderBy
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If ViewState("multiSel") Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "Stud. No"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "Student Name"
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub


   
    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    
    Protected Sub gvGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup.Load
        'gvGroup.Columns(2).Visible = False
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim retstring As String = String.Empty
        Dim bGetStudentNo As Boolean = IIf(Request.QueryString("GETSTU_NO") Is Nothing, False, Request.QueryString("GETSTU_NO"))
        If (Not lblcode Is Nothing) Then
            If bGetStudentNo Then
                Dim len As Integer = lblcode.Text.IndexOf("___")
                retstring = lblcode.Text.Insert(len, "___" & lbClose.Text.Replace("'", "\'"))
            Else
                retstring = lblcode.Text & "___" & lbClose.Text.Replace("'", "\'")
            End If
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            ''Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.returnValue = '" & retstring & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & retstring & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & retstring & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")


            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        If chkSelAll.Checked Then
                  
            Dim strGRD_IDs As String = Request.QueryString("GRD_IDs")
            Dim strSCT_IDs As String = Request.QueryString("SCT_IDs")
            If strSCT_IDs = "ALL" Or strSCT_IDs = "0" Then
                strSCT_IDs = ""
            End If
            '  SelectAll_Student(strGRD_IDs, strSCT_IDs)

        Else
            Session("liUserList").Clear()
        End If
        GridBind()
        SetChk(Me.Page)
    End Sub

  
    Private Sub SelectAll_Student(Optional ByVal vGRD_ID As String = "", Optional ByVal vSCT_ID As String = "")

        Dim str_query_header As String

        If h_TYPE.Value = "GROUP" Then
            str_query_header = " SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2  FROM " & _
            " vw_STUDENT_DETAILS INNER JOIN ACE_STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID WHERE STU_BSU_ID ='" & Session("sBSUID") & "'" _
            & " AND SSD_SGR_ID=" + h_SGR_ID.Value

            If vGRD_ID <> "" Then
                str_query_header += " AND STU_GRD_ID IN ('" & vGRD_ID & "')"
            End If
            If vSCT_ID <> "" Then
                str_query_header += " AND STU_SCT_ID IN ('" & vSCT_ID & "')"
            End If
            str_query_header += " ORDER BY STU_NO"
        Else

            str_query_header = "SELECT DISTINCT CAST(VW_STUDENT_M.STU_ID AS VARCHAR) + '___' + VW_STUDENT_M.STU_NO AS ID, VW_STUDENT_M.STU_NO as Descr1, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS DESCR2 " _
                     & " FROM VW_STUDENT_M WHERE STU_ACD_ID= " + h_ACD_ID.Value + " AND STU_GRD_ID='" + h_GRD_ID.Value + "'" _
                     & " AND STU_ID IN(SELECT SSD_STU_ID FROM ACE_STUDENT_GROUPS_S WHERE SSD_ACD_ID=" + h_ACD_ID.Value + ")"

            If h_SCT_ID.Value <> "0" Then
                str_query_header += " AND STU_SCT_ID=" + h_SCT_ID.Value
            End If
            str_query_header += " ORDER BY STU_NO"

        End If


        Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, str_query_header)
        If drReader.HasRows = True Then
            While (drReader.Read())
                Session("liUserList").Remove(drReader(0))
                Session("liUserList").Add(drReader(0))
            End While
        End If
        GridBind()


    End Sub
    Sub GridBindStudentForMarkDetails(ByVal vAcdId As String, ByVal vSgrId As String, ByVal vAcmId As String)
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String

            Dim str_filter_code, str_filter_name, strOrderBy As String
            Dim str_mode As String
            Dim str_txtCode, str_txtName, str_BSUName As String
            str_filter_code = ""
            str_filter_name = ""
            str_mode = ""

            str_txtCode = ""
            str_txtName = ""
            str_BSUName = ""
            strOrderBy = ""

            If h_TYPE.Value = "GROUP" Then
                str_Sql = "SELECT DISTINCT CAST(VW_STUDENT_M.STU_ID AS VARCHAR) + '___' + VW_STUDENT_M.STU_NO AS ID, VW_STUDENT_M.STU_NO as Descr1, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS DESCR2 "

                str_Sql += " FROM ACE_STUDENT_GROUPS_S INNER JOIN  " _
                            & " VW_STUDENT_M ON ACE_STUDENT_GROUPS_S.SSD_STU_ID=VW_STUDENT_M.STU_ID" _
                            & " WHERE VW_STUDENT_M.STU_BSU_ID = '" & Session("sBSUID") & "' " _
                            & " AND  SSD_ACM_ID='" & vAcmId & "'"

                If vSgrId <> "0" Then
                    str_Sql += " AND SSD_SGR_ID=" + vSgrId
                End If
            ElseIf h_TYPE.Value = "GRADE" Then
                str_Sql = "SELECT DISTINCT CAST(VW_STUDENT_M.STU_ID AS VARCHAR) AS ID, VW_STUDENT_M.STU_NO as Descr1, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS DESCR2,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME " _
                      & " FROM VW_STUDENT_M WHERE STU_ACD_ID= " + vAcdId + " AND STU_GRD_ID='" + h_GRD_ID.Value + "'"

                If h_SCT_ID.Value <> "0" Then
                    str_Sql += " AND STU_SCT_ID=" + h_SCT_ID.Value
                End If

            Else
                str_Sql = "SELECT DISTINCT CAST(VW_STUDENT_M.STU_ID AS VARCHAR) + '___' + VW_STUDENT_M.STU_NO AS ID, VW_STUDENT_M.STU_NO as Descr1, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS DESCR2,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME " _
                      & " FROM VW_STUDENT_M WHERE STU_ACD_ID= " + vAcdId + " AND STU_GRD_ID='" + h_GRD_ID.Value + "'" _
                      & " AND STU_ID IN(SELECT SSD_STU_ID FROM ACE_STUDENT_GROUPS_S WHERE SSD_ACD_ID=" + h_ACD_ID.Value + ")"

                If h_SCT_ID.Value <> "0" Then
                    str_Sql += " AND STU_SCT_ID=" + h_SCT_ID.Value
                End If

            End If

            Dim ds As New DataSet
            str_Sql = str_Sql
            'If str_Sql <> "" Then
            '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            'End If
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("VW_STUDENT_M.STU_NO", str_Sid_search(0), str_txtCode)
                If str_txtCode = "" Then
                    str_filter_code = ""
                End If
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                      txtSearch = gvGroup.HeaderRow.FindControl("txtName")

                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'')", str_Sid_search(0), str_txtName)
                If str_txtName = "" Then
                    str_filter_name = ""
                End If
                ''column1
            End If

            str_Sql = str_Sql & str_filter_code & str_filter_name
            str_Sql += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "Stud. No"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "Student Name"
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception

        End Try

    End Sub
    Sub GridBindReportHeader(Optional ByVal vCATID As String = "", Optional ByVal vAllSub As String = "")
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""
            If vCATID <> "" Then
                strCondition = " AND RSD_RSM_ID=" & vCATID
            End If
            If vAllSub <> "" Then
                strCondition += " AND RSD_bAllSubjects=" & vAllSub
            End If
            str_Sql = "SELECT CAST(RSD_ID AS VARCHAR)+'___' +CAST(RSD_RSM_ID AS VARCHAR) AS ID,RSD_ID AS DESCR1,RSD_HEADER AS DESCR2 FROM RPT.REPORT_SETUP_D WHERE RSD_BDirectEntry=1 " _
                    & strCondition & " ORDER BY RSD_ID"

            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''

            Dim ds As New DataSet
            str_Sql = str_Sql
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindGradeGroupDetails(Optional ByVal vGRADEID As String = "", Optional ByVal vACDID As String = "")
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""
            If vGRADEID <> "" Then
                strCondition = " AND GROUPS_M.SGR_GRD_ID=" & vGRADEID
            End If

            ' WHERE SGR_BSU_ID = 125005 AND SGR_GRD_ID = 'KG1'
            'str_Sql = "SELECT     GROUPS_M.SGR_ID as ID, STUDENT_GROUPS_S.SSD_SGR_ID as Descr1, GROUPS_M.SGR_DESCR as Descr2" _
            '            & " FROM GROUPS_M INNER JOIN " _
            '            & " STUDENT_GROUPS_S ON GROUPS_M.SGR_ID = STUDENT_GROUPS_S.SSD_SGR_ID WHERE STUDENT_GROUPS_S.SSD_ACD_ID='" & vACDID & "'"


            'str_Sql = "SELECT     SGR_ID AS ID,SGR_ID AS Descr1,SGR_DESCR AS Descr2 " _
            '        & " FROM GROUPS_M " _
            '        & " WHERE SGR_ACD_ID='" & vACDID & "'"
            If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
                str_Sql = "SELECT     GROUPS_M.SGR_ID AS ID, GROUPS_M.SGR_ID AS Descr1, GROUPS_M.SGR_DESCR AS Descr2, GROUPS_TEACHER_S.SGS_EMP_ID " _
                            & " FROM GROUPS_M INNER JOIN " _
                            & " GROUPS_TEACHER_S ON GROUPS_M.SGR_ID = GROUPS_TEACHER_S.SGS_SGR_ID WHERE GROUPS_M.SGR_ACD_ID='" & vACDID & "'" _
                            & " AND GROUPS_TEACHER_S.SGS_EMP_ID='" & Session("EmployeeId") & "'"
            Else
                str_Sql = "SELECT     GROUPS_M.SGR_ID AS ID, GROUPS_M.SGR_ID AS Descr1, GROUPS_M.SGR_DESCR AS Descr2 " _
                            & " FROM GROUPS_M " _
                            & " WHERE GROUPS_M.SGR_ACD_ID='" & vACDID & "'"

            End If

            If strCondition <> "" Then
                str_Sql += strCondition
            End If
            str_Sql += " ORDER BY ID"
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''

            Dim ds As New DataSet
            str_Sql = str_Sql
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindSyllabus(ByVal vAcdId As String, ByVal vTrmId As String, ByVal vGrdId As String, ByVal vSubjId As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""
            'If vSYLID <> "" Then
            '    strCondition = " WHERE SYD_PARENT_ID=0 AND SYD_SYM_ID=" & vSYLID
            'End If
            'str_Sql = "SELECT SYD_ID AS ID,SYD_ID AS DESCR1,SYD_DESCR AS DESCR2 FROM SYL.SYLLABUS_D " _
            '        & strCondition & " GROUP BY SYD_ID,SYD_DESCR ORDER BY SYD_ID"

            str_Sql = "SELECT SYM_ID AS ID,SYM_ID AS Descr1,SYM_DESCR as Descr2  FROM SYL.SYLLABUS_M WHERE " _
                        & " SYM_BSU_ID='" & Session("SBsuid") & "'"


            If vAcdId <> "" Then
                strCondition = " AND SYM_ACD_ID='" & vAcdId & "'"
            End If
            If vTrmId <> "" Then
                strCondition += " AND SYM_TRM_ID='" & vTrmId & "'"
            End If
            If vGrdId <> "" Then
                strCondition += " AND SYM_GRD_ID='" & vGrdId & "'"
            End If
            If vSubjId <> "" Then
                strCondition += "AND SYM_SBG_ID='" & vSubjId & "'"
            End If
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''

            Dim ds As New DataSet
            str_Sql = str_Sql + strCondition + " ORDER BY SYM_ID"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow Then
        '        If Session("liUserList").count > 0 Then
        '            TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox).Checked = True
        '        Else
        '            TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox).Checked = False
        '        End If
        '    End If
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        'End Try
    End Sub
    Sub GridBindSection(ByVal vAcdId As String, ByVal vGrdId As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""
            'If vSYLID <> "" Then
            '    strCondition = " WHERE SYD_PARENT_ID=0 AND SYD_SYM_ID=" & vSYLID
            'End If
            'str_Sql = "SELECT SYD_ID AS ID,SYD_ID AS DESCR1,SYD_DESCR AS DESCR2 FROM SYL.SYLLABUS_D " _
            '        & strCondition & " GROUP BY SYD_ID,SYD_DESCR ORDER BY SYD_ID"

            str_Sql = "SELECT SCT_ID AS ID,SCT_ID AS Descr1,SCT_DESCR as Descr2  FROM VW_SECTION_M WHERE " _
                        & " SCT_BSU_ID='" & Session("SBsuid") & "' AND " _
                        & " SCT_DESCR<>'TEMP'"


            If vAcdId <> "" Then
                strCondition = " AND SCT_ACD_ID='" & vAcdId & "'"
            End If

            If vGrdId <> "" Then
                strCondition += " AND SCT_GRD_ID='" & vGrdId & "'"
            End If
            'If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
            '    strCondition += " AND SCT_EMP_ID='" & Session("EmployeeId") & "'"
            'End If
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''

            Dim ds As New DataSet
            str_Sql = str_Sql + strCondition + " ORDER BY SCT_DESCR"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindStudentComments(ByVal vACD_IDs As String, ByVal vGRD_IDs As String, ByVal bGETSTU_NO As Boolean, Optional ByVal vSGR_IDs As String = "", Optional ByVal vSCT_IDs As String = "")
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_filter_code, str_filter_name, strOrderBy As String
            Dim str_mode As String
            Dim str_txtCode, str_txtName, str_BSUName As String
            str_filter_code = ""
            str_filter_name = ""
            str_mode = ""

            str_txtCode = ""
            str_txtName = ""
            str_BSUName = ""
            strOrderBy = ""
            If bGETSTU_NO Then
                str_query_header = "SELECT DISTINCT CAST(STU_ID AS VARCHAR) + '___' + STU_NO  ID, STU_NO DESCR1, STU_NAME DESCR2 "
            Else
                str_query_header = "SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2 "
            End If
            'Select Case vTYPE
            'Case "SGR_ID"
            'str_query_header += " FROM STUDENT_GROUPS_S INNER JOIN vw_STUDENT_DETAILS " & _
            '" ON STUDENT_GROUPS_S.SSD_STU_ID = vw_STUDENT_DETAILS.STU_ID WHERE STU_BSU_ID = '" & Session("sBSUID") & "'"
            str_query_header += " FROM VW_STUDENT_DETAILS INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID " _
                                & " INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID " _
                                & " WHERE STU_ACD_ID='" & vACD_IDs & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"
            'If vIDs <> "" Then
            'str_query_header += "AND SSD_SGR_ID IN ('" & vIDs.Replace("___", "','") & "')"
            'End If
            'Case "GRD_ID"
            'str_query_header += " FROM vw_STUDENT_DETAILS WHERE STU_BSU_ID = '" & Session("sBSUID") & "'"
            'If vIDs <> "" Then
            If vGRD_IDs <> "" And vGRD_IDs <> "0" Then
                'str_query_header += "AND STU_GRD_ID IN ('" & vIDs.Replace("___", "','") & "')"
                str_query_header += "AND STU_GRD_ID='" & vGRD_IDs & "'"
            End If
            If vSCT_IDs <> "" And vSCT_IDs <> "0" Then
                'str_query_header += "AND STU_SCT_ID IN ('" & vSCT_IDs.Replace("___", "','") & "')"
                str_query_header += "AND STU_SCT_ID=" & vSCT_IDs & ""
            End If
            If vSGR_IDs <> "" And vSGR_IDs <> "0" Then
                str_query_header += "AND SGR_ID=" & vSGR_IDs & ""
            End If

            'End Select

            strOrderBy = " ORDER BY STU_NAME"
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("STU_NO", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If ViewState("multiSel") Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("STU_NAME", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & strOrderBy
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If ViewState("multiSel") Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "Stud. No"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "Student Name"
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindGenCOMMENTS(Optional ByVal vCATID As String = "")
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""

            str_Sql = "SELECT CMT_ID AS ID,CMT_ID AS DESCR1,CMT_COMMENTS AS DESCR2 FROM ACT.COMMENTS_M " _
                        & " WHERE CMT_bGenCOMMENTS=1 AND CMT_BSU_ID='" & Session("sBSUID") & "'"

            If vCATID <> "" Then
                strCondition = " AND CMT_CAT_ID=" & vCATID & ""
            End If
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''

            Dim ds As New DataSet
            str_Sql = str_Sql & strCondition & " ORDER BY CMT_ID"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Sub GridBindTeachersList(Optional ByVal vCATID As String = "")
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""

            str_Sql = "SELECT DISTINCT EMP_ID AS ID,EMP_ID AS DESCR1,ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') AS DESCR2 " _
                                 & " FROM OASIS.dbo.EMPLOYEE_M AS A INNER JOIN " _
                                 & " GROUPS_TEACHER_S AS B ON A.EMP_ID=B.SGS_EMP_ID AND SGS_TODATE IS NULL" _
                                 & " INNER JOIN GROUPS_M AS C ON B.SGS_SGR_ID=C.SGR_ID" _
                                 & " WHERE SGR_ACD_ID=" + vCATID + "" _
                                 & " ORDER BY DESCR2 "


            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''
            Dim ds As New DataSet
            '

            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindSBJACADEMICGRADES(Optional ByVal v_GradeID As String = "", Optional ByVal v_AcdID As String = "")
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String
            Dim str_txtName As String = ""
            Dim strCondition As String = ""

            If (Session("EmployeeId") <> "") And (Session("CurrSuperUser") <> "Y") Then
                strCondition += " AND SGS_EMP_ID='" & Session("EmployeeId") & "' "
                str_Sql = "SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                           & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                           & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                           & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & v_AcdID & "'" _
                           & " " & strCondition & " AND SBG_GRD_ID='" & v_GradeID & "'"
            Else

                'str_Sql = "SELECT DISTINCT(SBG_ID),SBG_ID AS ID,SBG_ID AS DESCR1,SBG_DESCR AS DESCR2 FROM " _
                '          & " SUBJECTS_GRADE_S INNER JOIN GROUPS_M ON SBG_ID=SGR_SBG_ID " _
                '          & " INNER JOIN GROUPS_TEACHER_S ON  SGR_ID= SGS_SGR_ID " _
                '          & " WHERE SBG_BSU_ID='" & Session("sBsuId") & "' AND SBG_ACD_ID='" & Session("Current_ACD_ID") & "'" _
                '          & " " & strCondition & " AND SBG_GRD_ID='" & v_GradeID & "'"

                str_Sql = " SELECT DISTINCT SBG_ID AS ID, " & _
                                " SBG_ID AS DESCR1, " & _
                                " CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2,GRM_DISPLAY FROM SUBJECTS_GRADE_S " & _
                                " INNER JOIN VW_GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID " & _
                                " AND SUBJECTS_GRADE_S.SBG_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID " & _
                                " INNER JOIN VW_STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = VW_STREAM_M.STM_ID"
                str_Sql += " WHERE SBG_BSU_ID = '" & Session("sbsuid") & "' AND SBG_ACD_ID='" & v_AcdID & "'"
                If v_GradeID <> "" Then
                    str_Sql += "AND SBG_GRD_ID IN ('" & v_GradeID & "') ORDER BY GRM_DISPLAY "
                End If
            End If

            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            ''''''''''

            Dim ds As New DataSet
            str_Sql = str_Sql
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        If chkSelAll.Checked Then
            Dim strGRD_IDs As String
            Dim strACD_ID As String
            Dim strSCT_IDs As String
            Dim str_query_header As String
            If h_TYPE.Value = "GROUP" Then
                str_query_header = " SELECT DISTINCT STU_ID ID, STU_NO DESCR1, STU_NAME DESCR2  FROM " & _
           " vw_STUDENT_DETAILS INNER JOIN ACE_STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID WHERE STU_BSU_ID ='" _
            & Session("sBSUID") & "' AND SSD_SGR_ID=" + h_SGR_ID.Value


                str_query_header += " ORDER BY STU_NO"
            Else

                str_query_header = "SELECT DISTINCT CAST(VW_STUDENT_M.STU_ID AS VARCHAR) AS ID, VW_STUDENT_M.STU_NO as Descr1, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') +' '+ ISNULL(STU_LASTNAME,'') AS DESCR2 " _
                         & " FROM VW_STUDENT_M WHERE STU_ACD_ID= " + h_ACD_ID.Value + " AND STU_GRD_ID='" + h_GRD_ID.Value + "'"

                If h_SCT_ID.Value <> "0" Then
                    str_query_header += " AND STU_SCT_ID=" + h_SCT_ID.Value
                End If
                str_query_header += " ORDER BY STU_NO"
            End If

            Dim drReader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, str_query_header)
            If drReader.HasRows = True Then
                While (drReader.Read())
                    Session("liUserList").Remove(drReader(0))
                    Session("liUserList").Add(drReader(0))
                End While
            End If
        Else
            SetChk(Me.Page)
            h_SelectedId.Value = ""
        End If
            For i As Integer = 0 To Session("liUserList").Count - 1
                If h_SelectedId.Value <> "" Then
                    h_SelectedId.Value += "___"
                End If
                h_SelectedId.Value += Session("liUserList")(i).ToString
            Next

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub

    Protected Sub gvGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup.SelectedIndexChanged

    End Sub
End Class

