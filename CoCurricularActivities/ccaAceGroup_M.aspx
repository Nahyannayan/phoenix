<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAceGroup_M.aspx.vb" Inherits="CoCurricularActivities_ccaAceGroup_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to remove the selected students from this group.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

        var color = '';
        function highlightView(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvView.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }

        function highlightAdd(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvAdd.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }

        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Group Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" Style="text-align: left" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM2" Style="text-align: left" />
                        </td>
                    </tr>
                    <tr>
                        <td align="lEft" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                Style="text-align: center" SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="lEft" valign="bottom">
                            <asp:Label ID="lblError1" runat="server" CssClass="error" EnableViewState="False"
                                Style="text-align: center" SkinID="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton>
                        </td>
                    </tr>


                    <tr>
                        <td align="left" class="matters" width="20%">
                            <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year " CssClass="field-label">
                            </asp:Label>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" class="matters" width="20%">
                            <asp:Label ID="lblAce" runat="server" Text="Select Ace" CssClass="field-label"></asp:Label>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAce" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Select Term</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters">
                            <asp:Label ID="lblShift" runat="server" Text="Select Level" CssClass="field-label"></asp:Label>
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlLevel" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Group Name</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtDescr" runat="server" TabIndex="2" MaxLength="100">
                            </asp:TextBox>
                        </td>
                        <td align="left" class="matters">
                            <asp:Label ID="lbl15" runat="server" Text="Teacher" CssClass="field-label"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTeacher" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">From Date</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtFrom" runat="server" AutoCompleteType="Disabled">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnAddNew" TabIndex="7" runat="server" Text="Add Teacher" CssClass="button"
                                ValidationGroup="groupM2"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvTeacher" PageSize="2" runat="server" AutoGenerateColumns="false" CssClass="table table-row table-bordered"
                                EmptyDataText="No Teachers for this group">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="eid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpId" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Teacher">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTeacher" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schedule">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSchedule" runat="server" Text='<%# Bind("SGS_SCHEDULE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="matters" valign="bottom" colspan="4">
                            <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" ValidationGroup="groupM1"
                                TabIndex="7" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" colspan="4">
                            <asp:Menu ID="mnuMaster" Orientation="Horizontal" CssClass="menu_a" runat="server" OnMenuItemClick="mnuMaster_MenuItemClick">
                                <Items>
                                    <asp:MenuItem Text="View Students" Value="0" Selected="True"></asp:MenuItem>
                                    <asp:MenuItem Text="Allocate Students" Value="1"></asp:MenuItem>
                                </Items>
                                <StaticMenuItemStyle CssClass="menuItem" />
                                <StaticSelectedStyle CssClass="selectedItem" />
                                <StaticHoverStyle CssClass="hoverItem" />
                            </asp:Menu>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="left">
                            <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">
                                <asp:View ID="View1" runat="server">
                                    <table id="tb1" width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:GridView ID="gvView" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                    EmptyDataText="No Records Found" PageSize="20" Width="100%">
                                                    <RowStyle CssClass="griditem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Available">
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                            </EditItemTemplate>
                                                            <HeaderTemplate>
                                                                Select<br />
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                    ToolTip="Click here to select/deselect all rows" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlightView(this);" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SSD_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSsdId" runat="server" Text='<%# Bind("SSD_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SL.No" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNoView() %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stud No.">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblh1" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label><br />
                                                                <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClick="btnStuNo_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student Name">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                                <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClick="btnStudName_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="center">
                                                            <HeaderTemplate>
                                                                Grade<br />
                                                                <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                    OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Section">
                                                            <HeaderTemplate>
                                                                Section<br />
                                                                <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                    OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                    <SelectedRowStyle CssClass="Green" />
                                                    <HeaderStyle CssClass="gridheader_pop" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="Remove from Group"
                                                    OnClientClick="javascript:return confirm_delete();" ValidationGroup="groupM1"
                                                    TabIndex="7" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <table id="tbl" width="100%">
                                        <tr>
                                            <td class="matters"><span class="field-label">Grade</span>
                                            </td>
                                            <td class="matters">
                                                <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true"></asp:DropDownList>
                                            </td>

                                            <td class="matters"><span class="field-label">Section</span>
                                            </td>
                                            <td class="matters">
                                                <asp:DropDownList ID="ddlSection" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="matters"><span class="field-label">Student ID</span></td>
                                            <td class="matters">
                                                <asp:TextBox ID="txtStudentID" runat="server"></asp:TextBox></td>
                                            <td class="matters"><span class="field-label">Student Name</span></td>
                                            <td class="matters">
                                                <asp:TextBox ID="txtStudentName" runat="server"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:Button ID="btnSearch" class="button" runat="server" Text="List" />
                                            </td>
                                        </tr>
                                        <tr id="trAdd" runat="server">
                                            <td align="center" colspan="4">
                                                <asp:GridView ID="gvAdd" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                    EmptyDataText="No Records Found" PageSize="20" Width="100%">
                                                    <RowStyle CssClass="griditem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Available">
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                            </EditItemTemplate>
                                                            <HeaderTemplate>
                                                                Select<br />
                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                    ToolTip="Click here to select/deselect all rows" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlightAdd(this);" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SSD_ID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSsdId" runat="server" Text='<%# Bind("SSD_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="SL.No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSlNo" runat="server" Text="<%# getSerialNoAdd() %>"></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Stud No.">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblh1" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label><br />
                                                                <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClick="btnStuNo_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student Name">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                                <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClick="btnStudName_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Section">
                                                            <HeaderTemplate>
                                                                Grade<br />
                                                                <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                    OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Section">
                                                            <HeaderTemplate>
                                                                Section<br />
                                                                <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                    OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="Green" />
                                                    <HeaderStyle CssClass="gridheader_pop" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr id="trBtnAdd" runat="server">
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnAllocate" runat="server" CssClass="button" Text="Add to Group"
                                                    ValidationGroup="groupM1" TabIndex="7" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:HiddenField ID="hfSGR_ID" runat="server" />
                            <asp:HiddenField ID="hfBOptional" runat="server" />
                            <asp:HiddenField ID="hfSubjectURL" runat="server" />
                            <asp:HiddenField ID="hfSBG_ID" runat="server" />
                            <asp:HiddenField ID="hfGROUP" runat="server" />
                            <asp:HiddenField ID="hfSBG_PARENT_ID" runat="server" />
                            <asp:HiddenField ID="hfAddNew" runat="server" />
                            <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                HorizontalSide="Center" TargetControlID="lblError1" VerticalSide="Middle">
                            </ajaxToolkit:AlwaysVisibleControlExtender>
                            <asp:HiddenField ID="hfDescr" runat="server" />
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="rfDescr" runat="server" ErrorMessage="Please enter the field Description"
                    ControlToValidate="txtDescr" Display="None" ValidationGroup="groupM1">
                </asp:RequiredFieldValidator>&nbsp;
                <asp:RequiredFieldValidator ID="rffrom" runat="server" ErrorMessage="Please enter the field From Date"
                    ControlToValidate="txtFrom" Display="None" ValidationGroup="groupM2">
                </asp:RequiredFieldValidator>&nbsp;
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="txtFrom"
                    TargetControlID="txtFrom" CssClass="MyCalendar" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" CssClass="MyCalendar" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>


</asp:Content>
