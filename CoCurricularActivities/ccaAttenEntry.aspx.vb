﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI

Partial Class CoCurricularActivities_ccaAttenEntry
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
               
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10051") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindAce()
                    BindTerm()
                    H_CAT_ID.Value = "add"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindAce()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
        If (Not ddlActivity.Items Is Nothing) AndAlso (ddlActivity.Items.Count > 1) Then
            ddlActivity.Items.Add(New ListItem("--", "0"))
            ddlActivity.Items.FindByText("--").Selected = True
        End If
    End Sub
    Sub BindDate()
        ddlDate.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim ds As DataSet
        Dim str_query As String
        If ddlGroup.SelectedValue = "" Then Exit Sub
        If H_CAT_ID.Value = "add" Then
            str_query = "SELECT distinct AT_DATE   FROM ATTENDANCE_M WHERE AT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and " _
                                      & " AT_ACM_ID=" + ddlActivity.SelectedValue + " AND AT_GRP_ID=" + ddlGroup.SelectedValue + "  and AT_Date not in (" _
                                    & " SELECT distinct ATD_DATE   FROM ATTENDANCE_D WHERE ATD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and " _
                                         & " ATD_ACM_ID=" + ddlActivity.SelectedValue + " AND ATD_GRP_ID=" + ddlGroup.SelectedValue + ") order by AT_DATE desc "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlDate.DataSource = ds
            ddlDate.DataTextField = "AT_DATE"
            ddlDate.DataValueField = "AT_DATE"
        Else
            str_query = "SELECT distinct ATD_DATE   FROM ATTENDANCE_D WHERE ATD_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " and " _
                                         & " ATD_ACM_ID=" + ddlActivity.SelectedValue
            If ddlGroup.SelectedValue <> "" Then str_query += " AND ATD_GRP_ID=" + ddlGroup.SelectedValue
            str_query += " order by ATD_DATE desc"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlDate.DataSource = ds
            ddlDate.DataTextField = "ATD_DATE"
            ddlDate.DataValueField = "ATD_DATE"
        End If
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            ddlDate.Items.Add(New ListItem(String.Format("{0:dd/MMM/yyyy}", ds.Tables(0).Rows(i).Item(0)), ds.Tables(0).Rows(i).Item(0)))
        Next
        ddlDate.Items.Add(New ListItem("--", "0"))
        ddlDate.Items.FindByText("--").Selected = True
    End Sub
    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_aCD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " ORDER BY TRM_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        If (Not ddlTerm.Items Is Nothing) AndAlso (ddlTerm.Items.Count > 1) Then
            ddlTerm.Items.Add(New ListItem("ALL", "0"))
            ddlTerm.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindAce()
        BindTerm()
        BindGROUP()
        If ddlGroup.SelectedValue <> "" Then
            BindDate()
        End If
    End Sub
    Sub BindGROUP()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String
        Dim preps As Integer
        preps = 0

        str_query = "SELECT ACM_TYPE  FROM ACEMASTER_M where ACM_ID=" & ddlActivity.SelectedValue & " and ACM_TYPE='PREP SESSION' "
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds1.Tables(0).Rows.Count > 0 Then
            preps = 1
        End If

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Or preps = 1 Then
            str_query = "SELECT distinct SGR_ID,SGR_DESCR FROM ACE_GROUP_M inner join ATTENDANCE_m on sgr_id=at_grp_id" _
                                      & " WHERE  SGR_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + " And SGR_ACM_ID = " + ddlActivity.SelectedValue
        ElseIf studClass.isEmpTeacher(Session("EmployeeId")) = True Then
            str_query = "SELECT distinct SGR_ID,SGR_DESCR FROM ACE_GROUP_M inner join ATTENDANCE_m on sgr_id=at_grp_id" _
                                     & " inner join ACE_GROUPS_TEACHER_S on SGR_ID =SGS_SGR_ID " _
                                     & " WHERE  SGR_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + " And SGR_ACM_ID = " + ddlActivity.SelectedValue + " and " _
                                     & " SGS_TODATE IS NULL AND SGS_EMP_ID=" + Session("EmployeeId")
        End If

        If ddlTerm.SelectedValue <> "0" Then
            str_query += " and SGR_TRM_ID=" + ddlTerm.SelectedValue
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
        If (Not ddlGroup.Items Is Nothing) AndAlso (ddlGroup.Items.Count > 1) Then
            ddlGroup.Items.Add(New ListItem("--", "0"))
            ddlGroup.Items.FindByText("--").Selected = True
        End If
    End Sub
    Sub Bindsubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "select distinct sbg_descr from OASIS_CURRICULUM.dbo.SUBJECTS_GRADE_S  " _
                                & " where sbg_grd_id in (select distinct(ssd_grd_id) from ACE_STUDENT_GROUPS_S inner join ACEMASTER_M on SSD_ACM_ID =ACM_ID " _
                                & " where SSD_SGR_ID =" + ddlGroup.SelectedValue + " and ACM_TYPE ='SUPPORT CLASSES') and SBG_ACD_ID =" + ddlAcademicYear.SelectedValue
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "sbg_descr"
        ddlSubject.DataValueField = "sbg_descr"
        ddlSubject.DataBind()
        If (Not ddlSubject.Items Is Nothing) AndAlso (ddlSubject.Items.Count > 1) Then
            ddlSubject.Items.Add(New ListItem("--", "--"))
            ddlSubject.Items.FindByText("--").Selected = True
        End If
        If ddlSubject.Items.Count > 0 Then
            t1.Visible = True
        Else
            t1.Visible = False
        End If
    End Sub
    Private Sub Displayitems()


        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = ""


        str_query = "select at_tfrm,at_tto,isnull(at_subject,'--') as sbg_descr from ATTENDANCE_m  " _
                   & " WHERE at_date='" + CDate(ddlDate.SelectedItem.Text) + "' and AT_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + " And AT_ACM_ID = " + ddlActivity.SelectedValue

        If ddlGroup.SelectedValue <> "" Then
            str_query += " and at_grp_id=" + ddlGroup.SelectedValue
        End If

        If str_query <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then

                txtTfrm.Text = [String].Format("{0:t}", (ds.Tables(0).Rows(0).Item("at_tfrm")))
                'Convert.ToString((ds.Tables(0).Rows(0).Item("at_tfrm")))
                txtTTo.Text = [String].Format("{0:t}", (ds.Tables(0).Rows(0).Item("at_tto")))
                ddlSubject.SelectedValue = ds.Tables(0).Rows(0).Item("sbg_descr")
            End If
        End If

    End Sub
    Protected Sub ddlActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActivity.SelectedIndexChanged
        BindGROUP()
        If ddlGroup.SelectedValue <> "" Then
            BindDate()
        End If
        gridbind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet
            grdStud.DataSource = Nothing
            grdStud.DataBind()


            str_Sql = "select ATD_ID,SSD_STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,ISNULL(AB_REASON,'--') as AB_REASON,ATD_DESC,isnull(ATD_PRESENT,1)ATD_PRESENT" _
                       & " from ACE_STUDENT_GROUPS_S INNER JOIN VW_STUDENT_M ON SSD_STU_ID =STU_ID" _
                        & " LEFT OUTER JOIN ATTENDANCE_d ON ATD_STU_ID=SSD_STU_ID and ATD_DATE='" + CDate(ddlDate.SelectedItem.Value) + "' and ATD_BTEMP =1 " _
                        & " LEFT OUTER JOIN ABSENTEES ON ATD_AB_ID=AB_ID " _
                       & " where SSD_ACD_ID =" + ddlAcademicYear.SelectedValue + " And SSD_ACM_ID =" + ddlActivity.SelectedValue
                       
            If ddlGroup.SelectedValue <> "" Then
                Bindsubject()
                str_Sql += " And SSD_SGR_ID = " + ddlGroup.SelectedValue
                str_Sql += " order by ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            Displayitems()
           

            If ds.Tables(0).Rows.Count > 0 Then
                grdStud.DataSource = ds.Tables(0)
                grdStud.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdStud.DataSource = ds.Tables(0)
                Try
                    grdStud.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdStud.Rows(0).Cells.Clear()
                grdStud.Rows(0).Cells.Add(New TableCell)
                grdStud.Rows(0).Cells(0).ColumnSpan = columnCount
                grdStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        BindDate()
        lblError.Text = ""
    End Sub
    Protected Sub grdStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStud.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim chkisdefault As CheckBox = DirectCast(e.Row.FindControl("cb1"), CheckBox)
            Dim ddl As DropDownList = DirectCast(e.Row.FindControl("ddlReason"), DropDownList)
            Dim lbl As Label = DirectCast(e.Row.FindControl("AtdId"), Label)
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

            Dim str_query As String = "SELECT AB_ID,AB_REASON FROM dbo.ABSENTEES" _
                                    & " WHERE AB_BSU_ID='" + Session("sBsuid") + "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddl.Items.Clear()
            ddl.DataSource = ds
            ddl.DataTextField = "AB_REASON"
            ddl.DataValueField = "AB_ID"
            ddl.DataBind()
            str_query = "select distinct AB_ID,AB_REASON from dbo.ABSENTEES inner join dbo.ATTENDANCE_D on AB_ID=ATD_AB_ID where  ATD_ID= " & Val(lbl.Text)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddl.Items.Add(New ListItem("--", "0"))

            If ds.Tables(0).Rows.Count > 0 Then
                Dim li As New ListItem
                li.Text = ds.Tables(0).Rows(0).Item(1)
                li.Value = ds.Tables(0).Rows(0).Item(0)
                ddl.Items(ddl.Items.IndexOf(li)).Selected = True
            Else
                ddl.Items.FindByText("--").Selected = True
            End If

            If chkisdefault.Text = "True" Then
                chkisdefault.Checked = True
            Else
                chkisdefault.Checked = False

            End If
            chkisdefault.Text = ""
        End If
      

    End Sub
    Sub SaveAttendance()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = ""

        Dim i As Integer
        Dim c As Integer = 0
     

        Dim tyId As Label
        Dim Cb1 As CheckBox
        Dim ddl As DropDownList
        Dim txt As TextBox
        For i = 0 To grdStud.Rows.Count - 1
            With grdStud.Rows(i)

                tyId = .FindControl("AtId")
                Cb1 = .FindControl("Cb1")
                ddl = .FindControl("ddlReason")
                txt = .FindControl("txtdesc")
                Dim cmd As New SqlCommand
                Dim objConn As New SqlConnection(str_conn)

                objConn.Open()
                cmd = New SqlCommand("dbo.SAVE_ATTENDANCE_D", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ATD_ACD_ID", ddlAcademicYear.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_ACM_ID", ddlActivity.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_GRP_ID", ddlGroup.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_DATE", ddlDate.SelectedItem.Text)
                cmd.Parameters.AddWithValue("@ATD_STU_ID", tyId.Text)
                'If Cb1.Checked Then
                cmd.Parameters.AddWithValue("@ATD_PRESENT", Cb1.Checked)
                cmd.Parameters.AddWithValue("@ATD_AB_ID", ddl.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_DESC", txt.Text)
                'Else
                '    cmd.Parameters.AddWithValue("@ATD_PRESENT", 0)
                'End If

                cmd.ExecuteNonQuery()

                cmd.Dispose()
                objConn.Close()

                If c = 0 Then
                    UPDATE_SUBJECT()
                End If

                c = 1


            End With
        Next
        lblError.Text = "Record Saved Successfully"

    End Sub
    Sub UPDATE_SUBJECT()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str As String

        Dim str_query As String = ""
        Dim cmd As New SqlCommand
        Dim objConn As New SqlConnection(str_conn)
        Try
            objConn.Open()
            cmd = New SqlCommand("dbo.UPDATE_ATTEND_M_SUBJ", objConn)

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AT_ACD_ID", ddlAcademicYear.SelectedValue)
            cmd.Parameters.AddWithValue("@AT_ACM_ID", ddlActivity.SelectedValue)
            cmd.Parameters.AddWithValue("@AT_GRP_ID", ddlGroup.SelectedValue)
            cmd.Parameters.AddWithValue("@AT_DATE", ddlDate.SelectedItem.Value)
            If t1.Visible Then
                str = ddlSubject.SelectedItem.Text

            Else
                str = ""

            End If
            cmd.Parameters.AddWithValue("@AT_SUBJECT", str)


            cmd.ExecuteNonQuery()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
        Finally
            cmd.Dispose()
            objConn.Close()
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveAttendance()
    End Sub
    Protected Sub lnkActivity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkActivity.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\CoCurricularActivities\ccaAttenEntry_Temp.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        H_CAT_ID.Value = "add"
        'lnkEdit.CssClass = "button_menu"
        'lnkAdd.CssClass = "button_menu_click"
        BindDate()
    End Sub
    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEdit.Click
        H_CAT_ID.Value = "edit"
        'lnkEdit.CssClass = "button_menu_click"
        'lnkAdd.CssClass = "button_menu"
        BindDate()
    End Sub

   
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        BindGROUP()
    End Sub
End Class
