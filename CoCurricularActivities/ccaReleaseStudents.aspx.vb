Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class CoCurricularActivities_ccaReleaseStudents
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            'ts
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC20020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    'GridBind()
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfRPF_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rpfid").Replace(" ", "+"))
                    hfRSM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("rsmid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
                    '     hfTRM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("trmid").Replace(" ", "+"))
                    lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                    lblSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                    lblReport.Text = Encr_decrData.Decrypt(Request.QueryString("reportcard").Replace(" ", "+"))
                    lblPrintedFor.Text = Encr_decrData.Decrypt(Request.QueryString("schedule").Replace(" ", "+"))
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If

    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String
        If hfACD_ID.Value = Session("Current_ACD_ID") Then
            str_query = "SELECT DISTINCT Stu_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') as Stu_Name," _
                                    & " ISNULL(RPP_bPUBLISH,'FALSE') AS bPUBLISH,ISNULL(RPP_bRELEASEONLINE,'FALSE') AS bRELEASE," _
                                    & " CASE WHEN RPP_bPUBLISH ='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS bENABLE,RPP_RELEASEDATE," _
                                    & " STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME" _
                                    & " FROM VW_STUDENT_M AS A INNER JOIN ACE_STUDENT_GROUPS_S AS C ON A.STU_ID=C.SSD_STU_ID AND A.STU_ACD_ID=C.SSD_ACD_ID " _
                                    & " AND A.STU_ACD_ID=C.SSD_ACD_ID LEFT JOIN RPT.ACE_REPORT_STUDENTS_PUBLISH AS B ON " _
                                    & " A.STU_ID=B.RPP_STU_ID And RPP_RSM_ID = " + hfRSM_ID.Value + " And RPP_RPF_ID = " + hfRPF_ID.Value _
                                    & " WHERE STU_SCT_ID=" + hfSCT_ID.Value _
                                    & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Else

            str_query = "SELECT Stu_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') as Stu_Name," _
                            & " ISNULL(RPP_bPUBLISH,'FALSE') AS bPUBLISH,ISNULL(RPP_bRELEASEONLINE,'FALSE') AS bRELEASE," _
                            & " CASE WHEN RPP_bPUBLISH ='TRUE' THEN 'FALSE' ELSE 'TRUE' END AS bENABLE,RPP_RELEASEDATE" _
                            & " FROM VW_STUDENT_DETAILS_PREVYEARS AS A LEFT JOIN RPT.ACE_REPORT_STUDENTS_PUBLISH AS B ON " _
                            & " A.STU_ID=B.RPP_STU_ID And RPP_RSM_ID = " + hfRSM_ID.Value + " And RPP_RPF_ID = " + hfRPF_ID.Value _
                            & " WHERE STU_SCT_ID=" + hfSCT_ID.Value _
                            & " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim transaction As SqlTransaction
        Dim chkPublish As CheckBox
        Dim chkRelease As CheckBox
        Dim txtDate As TextBox
        Dim str_query As String
        Dim lblStuId As Label

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i = 0 To gvStud.Rows.Count - 1
                    chkPublish = gvStud.Rows(i).FindControl("chkPublish")
                    chkRelease = gvStud.Rows(i).FindControl("chkRelease")
                    lblStuId = gvStud.Rows(i).FindControl("lblStuId")
                    txtDate = gvStud.Rows(i).FindControl("txtDate")
                    str_query = "exec RPT.ACE_savePUBLISHRELEASESTUDENTWISE " _
                             & lblStuId.Text + "," _
                             & hfRSM_ID.Value + "," _
                             & hfRPF_ID.Value + "," _
                             & hfACD_ID.Value + "," _
                             & "'" + hfGRD_ID.Value + "'," _
                             & hfSCT_ID.Value + "," _
                             & chkPublish.Checked.ToString + "," _
                             & chkRelease.Checked.ToString + "," _
                             & IIf(txtDate.Text = "", "NULL", "'" + txtDate.Text + "'") + "," _
                             & "'" + Session("sUsr_name") + "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            Dim i As Integer
            Dim txtDate As TextBox
            Dim chkPublish As CheckBox
            Dim chkRelease As CheckBox
            For i = 0 To gvStud.Rows.Count - 1
                chkPublish = gvStud.Rows(i).FindControl("chkPublish")
                chkRelease = gvStud.Rows(i).FindControl("chkRelease")
                If chkPublish.Checked = True Or chkRelease.Checked = True Then
                    txtDate = gvStud.Rows(i).FindControl("txtDate")
                    txtDate.Text = txtRelease.Text
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub
End Class
