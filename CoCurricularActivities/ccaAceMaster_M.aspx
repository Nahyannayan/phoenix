<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAceMaster_M.aspx.vb" Inherits="CoCurricularActivities_ccaAceMaster_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ACE MASTER"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>

                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>

                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="matters" width="20%"><span class="field-label">Description</span></td>
                        <td class="matters" width="23%">
                            <asp:TextBox ID="txtDescr" runat="server" TabIndex="1" MaxLength="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            <asp:Label ID="lblShort" runat="server" Text="Short Code" CssClass="field-label"></asp:Label></td>
                        <td align="left">
                            <asp:TextBox ID="txtShortCode" runat="server" CausesValidation="True" ValidationGroup="groupM1"
                                TabIndex="2" MaxLength="3"></asp:TextBox></td>
                        <td align="left" class="matters"><span class="field-label">Type</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trSubject" runat="server">
                        <td align="left" class="matters"><span class="field-label">Subject</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" class="matters"><span class="field-label">Main Group</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtMainGroup" runat="server" TabIndex="1" MaxLength="100">
                            </asp:TextBox></td>
                    </tr>

                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" colspan="4">
                            <asp:HiddenField ID="hfACM_ID" runat="server" />
                            <asp:RequiredFieldValidator ID="rfsUBJECT" runat="server" ErrorMessage="Please enter the field Subject"
                                ControlToValidate="txtDescr" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
