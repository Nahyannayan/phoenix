﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_ccaPEProcessActivity
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            'ts

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC60007") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindActivitySchedule()
                    BindGrade()
                    BindSection()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then

            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                          & " ,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,GRD_DISPLAYORDER,STM_ID FROM " _
                                          & " GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                          & " INNER JOIN STREAM_M ON GRM_STM_ID=STM_ID" _
                                          & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                          & " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                         & " ,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,GRD_DISPLAYORDER,STM_ID FROM " _
                                         & " GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                         & " INNER JOIN STREAM_M ON GRM_STM_ID=STM_ID" _
                                         & " INNER JOIN PE.AUTHORIZED_STAFF ON GRD_ID=AS_GRD_ID" _
                                         & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                         & " AS_EMP_ID=" + Session("EmployeeId") + " AND ISNULL(AS_bDELETE,'FALSE')='FALSE'" _
                                         & " ORDER BY GRD_DISPLAYORDER"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

    End Sub

    Sub BindSection()
        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT SCT_DESCR,SCT_ID FROM SECTION_M INNER JOIN " _
                                  & " GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                  & " GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) + " AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT SCT_DESCR,SCT_ID FROM SECTION_M INNER JOIN " _
                         & " GRADE_BSU_M ON SCT_GRM_ID=GRM_ID " _
                         & " INNER JOIN PE.AUTHORIZED_STAFF ON SCT_ID=AS_SCT_ID WHERE " _
                         & " GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) + " AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                         & " AND ISNULL(AS_bDELETE,'FALSE')='FALSE'" _
                         & " ORDER BY SCT_DESCR"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        lstSection.DataSource = ds
        lstSection.DataTextField = "SCT_DESCR"
        lstSection.DataValueField = "SCT_ID"
        lstSection.DataBind()
    End Sub

    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str As String = ""
        Dim i As Integer

        For i = 0 To lstSection.Items.Count - 1
            If lstSection.Items(i).Selected = True Then
                If str <> "" Then
                    str += "|"
                End If
                str += lstSection.Items(i).Value.ToString
            End If
        Next

        Dim str_query As String = "EXEC [PE].[PROCESS_ACTIVITY]" _
                               & " @ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
                               & " @ACS_ID =" + ddlSchedule.SelectedValue.ToString + "," _
                               & " @SCT_IDS ='" + str + "'"

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivitySchedule()
        BindGrade()
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
    End Sub
End Class
