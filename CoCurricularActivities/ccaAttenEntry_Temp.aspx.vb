﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI

Partial Class CoCurricularActivities_ccaAttenEntry_Temp
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                txtFrom.Text = String.Format("{0:dd/MMM/yyyy}", Date.Today)
                rdFrm.Attributes.Add("onClick", "TPick('" & rdFrm.ClientID & "')")
                rdTo.Attributes.Add("onClick", "TPick('" & rdTo.ClientID & "')")
                rdFrm.Attributes.Add("onMouseDown", "TPick('" & rdFrm.ClientID & "')")
                rdTo.Attributes.Add("onMouseDown", "TPick('" & rdTo.ClientID & "')")

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                FillGrade()
                FillSection()
                FillAct()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub FillGrade()

        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER  FROM GRADE_BSU_M  INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID  WHERE " _
                                 & " GRM_BSU_ID ='" + Session("SBSUID") + "' AND GRM_ACD_ID= " + ddlAcademicYear.SelectedValue + "  ORDER BY GRD_DISPLAYORDER "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        If ddlGrade.Items.Count >= 1 Then
            ddlGrade.SelectedIndex = 0
            Try
                FillSection()

            Catch ex As Exception

            End Try
        End If
    End Sub
    Sub FillSection()

        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_Sql As String = " select SCT_ID,SCT_DESCR  from  SECTION_M where " _
                                 & " sct_bsu_id ='" + Session("sBSUID") + "' and SCT_ACD_ID= " + ddlAcademicYear.SelectedValue + "  and " _
                                 & " SCT_GRD_ID = '" + ddlGrade.SelectedValue + "' and SCT_DESCR <>'TEMP'"


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        If (Not ddlSection.Items Is Nothing) AndAlso (ddlSection.Items.Count > 1) Then
            ddlSection.Items.Add(New ListItem("ALL", "ALL"))
            ddlSection.Items.FindByText("ALL").Selected = True
        End If

    End Sub
    Sub FillAct()
        ddlAct.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_Sql As String = " select AT_AC_ID,AT_NAME FROM ATTENDENCE_TEMP  WHERE " _
                                 & "  AT_ACD_ID= " + ddlAcademicYear.SelectedValue


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlAct.DataSource = ds
        ddlAct.DataTextField = "AT_NAME"
        ddlAct.DataValueField = "AT_AC_ID"
        ddlAct.DataBind()
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Public Function GetSelectedStudents(Optional ByVal vSTU_IDs As String = "")
        Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " & _
         " FROM  vw_STUDENT_DETAILS "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, str_sql)
    End Function

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        FillSection()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        FillGrade()
        FillSection()
        FillAct()
    End Sub

    Protected Sub btnAtt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAtt.Click
        SaveData()
    End Sub
    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = ""



        '        Dim cmd As New SqlCommand
        '        Dim objConn As New SqlConnection(str_conn)
        '        objConn.Open()
        'cmd = New SqlCommand("dbo.saveATTENDANCE_TEMP", objConn)

        '        cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.AddWithValue("@ACD_ID", ddlAcademicYear.SelectedValue)
        'cmd.Parameters.AddWithValue("@AC_id", ddlAct.SelectedValue)
        'cmd.Parameters.AddWithValue("@AT_DATE", txtFrom.Text)
        'cmd.Parameters.AddWithValue("@AT_TFRM", rdFrm.DateInput.DisplayText)
        'cmd.Parameters.AddWithValue("@AT_TTO", rdTo.DateInput.DisplayText)
        'cmd.Parameters.AddWithValue("@STU_ID", h_STU_IDs.Value.Replace("___", "'|'"))


        '        cmd.ExecuteNonQuery()

        '        cmd.Dispose()
        '        objConn.Close()
        str_query = "exec [dbo].[saveATTENDANCE_TEMP] " & ddlAcademicYear.SelectedValue & "," & ddlAct.SelectedValue & ",'" & txtFrom.Text & "','" _
                     & "" & rdFrm.Text & "','" & rdTo.Text & "','" & h_STU_IDs.Value.Replace("___", "|") & "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        gridbind()
        lblError.Text = "Record Saved Successfully"

    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Displayitems()
            str_Sql = "select ATD_ID,ATD_STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME,ISNULL(AB_REASON,'--') as AB_REASON,ATD_DESC,stu_grd_id,sct_descr,ATD_PRESENT" _
                      & " from ATTENDANCE_D INNER JOIN VW_STUDENT_M ON ATD_STU_ID =STU_ID" _
                      & " inner join oasis.dbo.SECTION_M on STU_SCT_ID =sct_id " _
                      & " LEFT OUTER JOIN ABSENTEES ON ATD_AB_ID=AB_ID " _
                      & " where ATD_ACD_ID =" + ddlAcademicYear.SelectedValue + "  And ATD_ACM_ID =" + ddlAct.SelectedValue _
                      & " And ATD_DATE  ='" + txtFrom.Text + "' AND ATD_BTEMP=0"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdStud.DataSource = ds.Tables(0)
                grdStud.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdStud.DataSource = ds.Tables(0)
                Try
                    grdStud.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdStud.Rows(0).Cells.Clear()
                grdStud.Rows(0).Cells.Add(New TableCell)
                grdStud.Rows(0).Cells(0).ColumnSpan = columnCount
                grdStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnSaveCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCategory.Click
        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_query As String


            str_query = "INSERT INTO dbo.ATTENDENCE_TEMP(AT_NAME,AT_ACD_ID ) VALUES('" & txtCategory.Text & "'," & ddlAcademicYear.SelectedValue & ") "
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            FillAct()
            ddlAct.Items.FindByText(txtCategory.Text).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAct.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub grdStud_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdStud.RowDeleting
        Try
            grdStud.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdStud.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("AtId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM dbo.ATTENDANCE_d  WHERE ATD_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub grdStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdStud.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim chkisdefault As CheckBox = DirectCast(e.Row.FindControl("cb1"), CheckBox)
            Dim ddl As DropDownList = DirectCast(e.Row.FindControl("ddlReason"), DropDownList)
            Dim lbl As Label = DirectCast(e.Row.FindControl("AtId"), Label)
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_query As String = "SELECT AB_ID,AB_REASON FROM dbo.ABSENTEES" _
                                    & " WHERE AB_BSU_ID='" + Session("sBsuid") + "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddl.Items.Clear()
            ddl.DataSource = ds
            ddl.DataTextField = "AB_REASON"
            ddl.DataValueField = "AB_ID"
            ddl.DataBind()
            str_query = "select distinct AB_ID,AB_REASON from dbo.ABSENTEES inner join dbo.ATTENDANCE_D on AB_ID=ATD_AB_ID where  ATD_ID= " & Val(lbl.Text)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


            ddl.Items.Add(New ListItem("--", "0"))




            If ds.Tables(0).Rows.Count > 0 Then
                Dim li As New ListItem
                li.Text = ds.Tables(0).Rows(0).Item(1)
                li.Value = ds.Tables(0).Rows(0).Item(0)
                ddl.Items(ddl.Items.IndexOf(li)).Selected = True
            Else
                ddl.Items.FindByText("--").Selected = True
            End If

            If chkisdefault.Text = "True" Then
                chkisdefault.Checked = True
            Else
                chkisdefault.Checked = False

            End If
            chkisdefault.Text = ""
        End If

    End Sub
    Private Sub Displayitems()


        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = ""


        str_query = "select at_tfrm,at_tto from ATTENDANCE_m  " _
                   & " WHERE at_date='" + txtFrom.Text + "' and AT_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString + " And AT_ACM_ID = " + ddlAct.SelectedValue


        If str_query <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then


                rdFrm.Text = [String].Format("{0:t}", (ds.Tables(0).Rows(0).Item("at_tfrm")))
                rdTo.Text = [String].Format("{0:t}", (ds.Tables(0).Rows(0).Item("at_tto")))
            End If
        End If

    End Sub
    Sub SaveAttendance()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = ""

        Dim i As Integer



        Dim tyId As Label
        Dim Cb1 As CheckBox
        Dim ddl As DropDownList
        Dim txt As TextBox
        For i = 0 To grdStud.Rows.Count - 1
            With grdStud.Rows(i)

                tyId = .FindControl("stuId")
                Cb1 = .FindControl("Cb1")
                ddl = .FindControl("ddlReason")
                txt = .FindControl("txtdesc")
                Dim cmd As New SqlCommand
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("dbo.SAVE_ATTENDANCE_D", objConn)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ATD_ACD_ID", ddlAcademicYear.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_ACM_ID", ddlAct.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_GRP_ID", ddlAct.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_DATE", txtFrom.Text)
                cmd.Parameters.AddWithValue("@ATD_STU_ID", tyId.Text)

                cmd.Parameters.AddWithValue("@ATD_PRESENT", Cb1.Checked)
                cmd.Parameters.AddWithValue("@ATD_AB_ID", ddl.SelectedValue)
                cmd.Parameters.AddWithValue("@ATD_DESC", txt.Text)

                cmd.ExecuteNonQuery()

                cmd.Dispose()
                objConn.Close()






            End With
        Next
        gridbind()
        grdStudent.DataSource = ""
        grdStudent.DataBind()

        h_STU_IDs.Value = ""
        lblError.Text = "Record Saved Successfully"

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveAttendance()
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
End Class
