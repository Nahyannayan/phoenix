﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_ccaPEActivityDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC50007") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindActivitySchedule()
                    GridBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            End Try
        End If


    End Sub


    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = String.Empty
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACT_ACD_ID", ddlAcademicYear.SelectedValue)
        param(1) = New SqlParameter("@ACT_ACS_ID", ddlSchedule.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETACTIVITY_TERM_GRIDBIND", param)

        If ds.Tables(0).Rows.Count > 0 Then

            gvRpt.DataSource = ds.Tables(0)
            gvRpt.DataBind()


            Dim param_s(2) As SqlParameter
            param_s(0) = New SqlParameter("@ACV_GENDER", ddlGender.SelectedValue)
            param_s(1) = New SqlParameter("@ACV_ACS_ID", ddlSchedule.SelectedValue)
            Dim ds_Score As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETACTIVITY_TERM_SCORE_GRIDBIND", param_s)

            Dim actTable As New Hashtable
            Dim iRow As Integer
            Dim strkey, strvalue As String
            Dim i, j As Integer
            Dim lblAcmId As Label
            Dim txtVal1 As TextBox
            Dim txtVal2 As TextBox
            Dim strValues As String()

            For i = 0 To ds_Score.Tables(0).Rows.Count - 1
                With ds_Score.Tables(0).Rows(i)
                    strkey = .Item("ACV_ACM_ID").ToString + "_" + .Item("ACV_AGE").ToString
                    strvalue = .Item("VAL1").ToString + "_" + .Item("VAL2").ToString
                    actTable.Add(strkey, strvalue)
                End With
            Next
            For iRow = 0 To gvRpt.Rows.Count - 1
                lblAcmId = gvRpt.Rows(iRow).FindControl("lblAcmId")
                For j = 1 To 17
                    strkey = lblAcmId.Text + "_" + j.ToString
                    If Not gvRpt.Rows(iRow).FindControl("txtVal1_" + j.ToString) Is Nothing Then
                        txtVal1 = gvRpt.Rows(iRow).FindControl("txtVal1_" + j.ToString)
                        txtVal2 = gvRpt.Rows(iRow).FindControl("txtVal2_" + j.ToString)
                        If actTable.Contains(strkey) = True Then
                            strValues = actTable.Item(strkey).ToString.Split("_")
                            txtVal1.Text = strValues(0).ToString
                            txtVal2.Text = strValues(1).ToString
                        End If
                    End If
                Next
            Next


        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            'start the count from 1 no matter gridcolumn is visible or not

            ds.Tables(0).Rows(0)(2) = False
            gvRpt.DataSource = ds.Tables(0)
            Try
                gvRpt.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvRpt.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvRpt.Rows(0).Cells.Clear()
            gvRpt.Rows(0).Cells.Add(New TableCell)
            gvRpt.Rows(0).Cells(0).ColumnSpan = columnCount
            gvRpt.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvRpt.Rows(0).Cells(0).Text = "Please add the activity for the selected schedule."
        End If
    End Sub
  
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchedule.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlGender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGender.SelectedIndexChanged
        GridBind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then



            str_err = calltransaction(errorMessage)
            If str_err = "0" Then


                lblError.Text = "Record Saved Successfully"
                gridbind()

            Else
                lblError.Text = errorMessage
            End If
        End If


    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer


        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer

                Dim PARAM(2) As SqlParameter

                PARAM(0) = New SqlParameter("@ACM_XML", Read_GridData())
                PARAM(1) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(1).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PE.SAVEACTIVITY_DETAILS", PARAM)

                status = PARAM(1).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                Else

                    calltransaction = "0"
                End If

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    calltransaction = "1"
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function

    Private Function Read_GridData() As String
        Try

       
            Dim iRow As Integer

            Dim j As Integer
            Dim lblAcmId As Label
            Dim txtVal1 As TextBox
            Dim txtVal2 As TextBox

            Dim STR_XML As String = String.Empty
            STR_XML = "<ACV>"

            For iRow = 0 To gvRpt.Rows.Count - 1
                lblAcmId = gvRpt.Rows(iRow).FindControl("lblAcmId")
                For j = 1 To 17
                    If Not gvRpt.Rows(iRow).FindControl("txtVal1_" + j.ToString) Is Nothing Then
                        txtVal1 = gvRpt.Rows(iRow).FindControl("txtVal1_" + j.ToString)
                        txtVal2 = gvRpt.Rows(iRow).FindControl("txtVal2_" + j.ToString)
                        STR_XML += String.Format("<ACV_ID ACM_ID='{0}'  ACS_ID ='{1}' GENDER='{2}' ACV_AGE='{3}' ACV_VAL1='{4}' ACV_VAL2='{5}'/>", _
                        lblAcmId.Text.Trim, ddlSchedule.SelectedValue, ddlGender.SelectedValue, j, IIf(txtVal1.Text.Trim = "", 0, txtVal1.Text.Trim), IIf(txtVal2.Text.Trim = "", 0, txtVal2.Text.Trim))
                    End If
                Next
            Next
            STR_XML += "</ACV>"

            If STR_XML = "<ACV></ACV>" Then

                STR_XML = ""

            End If

            Return STR_XML
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Function
End Class
