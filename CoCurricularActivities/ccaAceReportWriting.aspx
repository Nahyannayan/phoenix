<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaAceReportWriting.aspx.vb" Inherits="CoCurricularActivities_ccaAceReportWriting" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {
            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }//GRADE CMTSCAT
        //function getcomments(ctlid, code, header, stuid, rsmid) {
        //    //  alert(ctlid); 
        //    var sFeatures;
        //    sFeatures = "dialogWidth: 530px; ";
        //    sFeatures += "dialogHeight: 450px; ";
        //    sFeatures += "help: no; ";
        //    sFeatures += "resizable: no; ";
        //    sFeatures += "scroll: yes; ";
        //    sFeatures += "status: no; ";
        //    sFeatures += "unadorned: no; ";
        //    var result;
        //    var parentid;
        //    var varray = new Array();
        //    var str;
        //    if (code == 'ALLGENCMTS') {

        //        result = window.showModalDialog("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", sFeatures);

        //        if (result != '' && result != undefined) {
        //            str = document.getElementById(ctlid).value;
        //            document.getElementById(ctlid).value = str + result;
        //            //document.getElementById(ctlid.split(';')[1]).value=varray[0];
        //        }
        //        else {
        //            return false;
        //        }
        //    }

        //}
        function getcategory(ctlid, code) {
            //            alert(ctlid); 
            //            alert(ctlid.split(';')[0])
            //            alert(code)            
            var sFeatures;
            sFeatures = "dialogWidth: 429px; ";
            sFeatures += "dialogHeight: 345px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            if (code == 'CMTSCAT') {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code + "", "", sFeatures);

                if (result != '' && result != undefined) {
                    varray = result.split('_');
                    document.getElementById(ctlid.split(';')[0]).value = varray[3];
                    document.getElementById(ctlid.split(';')[1]).value = varray[0];
                }
                else {
                    return false;
                }
            }
            else {

                parentid = document.getElementById(ctlid.split(';')[1]).value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=" + code + "&CAT_ID=" + parentid + "", "", sFeatures);

                if (result != '' && result != undefined) {
                    document.getElementById(ctlid.split(';')[0]).value = result.split('___')[1];
                }
                else {
                    return false;
                }
            }

        }


        function GetPopUp(id) {
            //alert(id);
            var sFeatures;
            sFeatures = "dialogWidth: 429px; ";
            sFeatures += "dialogHeight: 345px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (id == 1)//Accadamic Year
            {
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=ACCYEAR", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_ACD_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }



            else if (id == 3)//Group
            {
                var GradeGrpId
                GradeGrpId = document.getElementById('<%=H_SBJ_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SUBGROUP&SBM_IDs=" + GradeGrpId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_GRP_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }


            else if (id == 5)//Subjejct Grades
            {
                var GradeId
                GradeId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                //alert(GradeId + 'ddd');
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SBJGRD&GRDID=" + GradeId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_SBJ_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }

            else if (id == 7)//Report Schedule
            {
                var RPTId
                RPTId = document.getElementById('<%=H_RPT_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=RPTSCH&RPTID=" + RPTId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_SCH_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }
            else if (id == 8)//Student Info
            {
                var GradeId
                GradeId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT&GETSTU_NO=true&SGR_IDs=" + GradeId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    document.getElementById('<%=H_STU_ID.ClientID %>').value = result;
                }
                else {
                    return false;
                }
            }



            else if (id == 13) {
                var GrdId
                var AcdId

                GrdId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
                AcdId = document.getElementById('<%=H_ACD_ID.ClientID %>').value;

                result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=SECTION&GRADE_ID=" + GrdId + "&ACD_ID=" + AcdId + "", "", sFeatures)
                if (result != '' && result != undefined) {
                    NameandCode = result.split('___');

                    document.getElementById('<%=H_SCT_ID.ClientID %>').value = NameandCode[0];
                }
                else {
                    return false;
                }
            }
    if (id == 14)//Student Info
    {
        var GradeId;
        var acdId;
        var sgrId;
        var sctid;
        //var RepSch;
        //var RepHeader;
        GradeId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
        acdId = document.getElementById('<%=H_ACD_ID.ClientID %>').value;
        sgrId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
        sctid = document.getElementById('<%=H_SCT_ID.ClientID %>').value;
        //sgr_Descr=document.getElementById('<%=H_GRP_ID.ClientID %>').value;
        //RepSch=document.getElementById('<%=H_SCH_ID.ClientID %>').value;
        //RepHeader=''//document.getElementById('<%=H_SETUP.ClientID %>').value;
        // alert(acdId);

        result = window.showModalDialog("clmPopupForm.aspx?multiselect=false&ID=STUDENT_COMMENTS&GETSTU_NO=true&GRD_IDs=" + GradeId + "&ACD_IDs=" + acdId + "&SCT_IDs=" + sctid + "&GRP_IDs=" + sgrId + "", "", sFeatures)
        if (result != '' && result != undefined) {
            //alert(document.getElementById('<%=lblStudNo.ClientID %>').value);
            document.getElementById('<%=H_STU_ID.ClientID %>').value = result.split('___')[0];
            document.getElementById('<%=txtStudName.ClientID %>').value = result.split('___')[1];
            document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = result.split('___')[2];
        }
        else {
            return false;
        }


    }
}//CMTSCAT



function GetStudentValues() {
    var ColsId;
    var cellcnt = 0;
    ColsId = document.getElementById('<%=H_CommentCOLS.ClientID %>').value;
    //alert(ColsId);
    var araay = new Array();
    //alert(txt1);
    for (i = 0; i < txt1.length; i++) {

        araay[i] = new Array(ColsId)
        //alert(txt1.length);
        for (j = 0; j < ColsId; j++) {
            var ArrName = "txt" + (Number(j) + 1);
            //alert(ArrName);
            ArrName = eval(ArrName);
            try {
                var strtext;
                strtext = document.getElementById(ArrName[i]).value.replace(/,/gi, '~');
                // alert(strtext);
                araay[i][j] = strtext.replace(/,/gi, '~');
            }
            catch (err)
            { }
            //alert(araay[i][j]);
            cellcnt = cellcnt + 1;
        }
    }
    //alert(araay.length);
    var strvalue = '';
    //Sending Array values into ServerSide
    for (var i = 0; i < araay.length; i++) {
        strvalue = strvalue + araay[i] + '|';
    }
    //alert(strvalue);   
    document.getElementById('<%=H_CommentCOLS.ClientID %>').value = strvalue;
}


    </script>

    <script>
        function getcomments(ctlid, code, header, stuid, rsmid) {
            //  alert(ctlid); 
            var sFeatures;
            sFeatures = "dialogWidth: 530px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            var parentid;
            var varray = new Array();
            var str;
            if (code == 'ALLGENCMTS') {
                document.getElementById('<%=H_CTL_ID.ClientID%>').value = ctlid
                // result = window.showModalDialog("clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "", sFeatures);
                var oWnd = radopen("/Curriculum/clmCommentsList.aspx?ID=" + code + "&HEADER=" + header + "&STUID=" + stuid + "&RSMID=" + rsmid, "pop_comment");


            }

        }
        function OnClientClose(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            var ctlid = document.getElementById('<%=H_CTL_ID.ClientID%>').value;
            if (arg) {
                NameandCode = arg.Comment.split('||');
                alert(NameandCode[0]);
                str = document.getElementById(ctlid).value;
                document.getElementById(ctlid).value = str + NameandCode[0].replace("/ap/", "'");
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        function StudentInfo() {
            var GradeId;
            var acdId;
            var sgrId;
            var sctid;
            GradeId = document.getElementById('<%=H_GRD_ID.ClientID %>').value;
            acdId = document.getElementById('<%=H_ACD_ID.ClientID %>').value;
            sgrId = document.getElementById('<%=H_GRP_ID.ClientID %>').value;
            sctid = document.getElementById('<%=H_SCT_ID.ClientID %>').value;
            var oWnd = radopen("/Curriculum/clmPopupForm.aspx?multiselect=false&ID=STUDENT_COMMENTS&GETSTU_NO=true&GRD_IDs=" + GradeId + "&ACD_IDs=" + acdId + "&SCT_IDs=" + sctid + "&GRP_IDs=" + sgrId + "", "pop_student");
        }

        function OnClientClose1(oWnd, args) {

            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameCode.split('||');

                document.getElementById('<%=H_STU_ID.ClientID %>').value = NameandCode[0];
                document.getElementById('<%=txtStudName.ClientID %>').value = NameandCode[1];
               // document.getElementById('<%=lblStudNo.ClientID %>').innerHTML = NameandCode[2];
            }
        }




        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_comment" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Ace Report Writing"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive mr-3">
                <table width="100%">
                    <tr valign="top">
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcdID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Report Card</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" class="matters"><span class="field-label">Report Schedule</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlRepSch" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRepSch_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Ace</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlAce" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlAce_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td class="matters" align="left"><span class="field-label">Level</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Group</span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlMainHeader" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="LnkSearch" runat="server" OnClientClick="javascript:return false;">Search Students</asp:LinkButton>
                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td width="20%"><span class="field-label">Student Name</span> </td>
                                        <td width="30%">
                                            <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="StudentInfo(); return false;"></asp:ImageButton></td>
                                        <td width="50%">
                                            <asp:Label ID="lblStudNo" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>

                    </tr>


                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnView" runat="server" CssClass="button" OnClick="btnView_Click1"
                                Text="View Students" />
                            <asp:Button ID="btnCanceldata" runat="server" CssClass="button"
                                Text="Cancel" OnClick="btnCanceldata_Click" /></td>
                    </tr>
                    <tr runat="server" id="trSave1">
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnSave1" runat="server" CssClass="button"
                                Text="Save & Next" ValidationGroup="MAINERROR" OnClick="btnSave1_Click" OnClientClick="GetStudentValues()" />
                            <asp:Button ID="btnCancel1" runat="server" CausesValidation="False"
                                CssClass="button" Text="Cancel" OnClick="btnCancel1_Click" /></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">


                            <telerik:RadSpell ID="RadSpell1" ButtonType="LinkButton" runat="server" IsClientID="true" SupportedLanguages="en-US,English"></telerik:RadSpell>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPages" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                                    <td>
                                        <asp:DataList ID="dlPages" runat="server" RepeatColumns="100">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" Text='<%# Bind("pageno") %>' ID="lblPageNo" OnClick="lblPageNo_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:DataList></td>

                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="lbtnKeys" runat="server" OnClientClick="javascript:return false;">Header Keys</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">


                            <asp:GridView ID="gvAceStudents" runat="server"
                                EmptyDataText="No Data Found" Width="100%" AutoGenerateColumns="False" EnableTheming="True" OnPageIndexChanging="gvAceStudents_PageIndexChanging"
                                CssClass="table table-row table-bordered" OnRowDataBound="gvAceStudents_RowDataBound">
                                <HeaderStyle CssClass="gridheader_pop1222222" />
                                <AlternatingRowStyle CssClass="griditem_alternative222222" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="trSave">

                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnSaveDetails" runat="server" CssClass="button"
                                OnClientClick="GetStudentValues()" Text="Save " ValidationGroup="MAINERROR" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button"
                                Text="Save & Next" ValidationGroup="MAINERROR" OnClick="btnSave1_Click" OnClientClick="GetStudentValues()" />

                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>

                <div id="popup" runat="server" class="subHeader_E" style="display: none; text-align: left; vertical-align: middle; width: 500px; margin-top: -2px; margin-left: -3px; margin-right: 1px;">
                    <div class="title-bg" style="width: 500px; vertical-align: middle; padding-bottom: 2px;">Header Keys</div>
                    <asp:Panel ID="PopupMenu" ScrollBars="Vertical" runat="server" CssClass="panel-cover" Height="25%" Width="100%">
                        <asp:Literal ID="ltProcess" runat="server"></asp:Literal>
                    </asp:Panel>
                </div>
                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="Server"
                    TargetControlID="lbtnKeys"
                    PopupControlID="popup"
                    HoverCssClass="popupHover"
                    PopupPosition="Center"
                    OffsetX="0"
                    OffsetY="20"
                    PopDelay="50" />

                <asp:HiddenField ID="H_ACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_GRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_GRP_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_ACM_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_RPT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SCH_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_STU_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SETUP" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_RPF_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_CommentCOLS" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_SBJ_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_ACL_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_CTL_ID" runat="server"></asp:HiddenField>
                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                    CollapseControlID="LnkSearch" CollapsedSize="0" CollapsedText="Search Students"
                    ExpandControlID="LnkSearch" ExpandedSize="50" ExpandedText="Hide Search" TargetControlID="Panel1"
                    TextLabelID="LnkSearch" Collapsed="True">
                </ajaxToolkit:CollapsiblePanelExtender>
                <br />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
            </div>
        </div>
    </div>
</asp:Content>

