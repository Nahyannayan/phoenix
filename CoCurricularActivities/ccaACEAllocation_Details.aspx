﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaACEAllocation_Details.aspx.vb" Inherits="CoCurricularActivities_ccaACEAllocation_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Ace Allocation Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table width="100%">
                    <tr id="trLabelError" runat="server">
                        <td align="center" valign="bottom" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr align="left" id="tr1" runat="server">
                        <td width="20%"><span class="field-label">Academic Year<font color="maroon">*</font></span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td width="20%"><span class="field-label">Level<font color="maroon">*</font></span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr align="left">
                        <td><span class="field-label">Grade<font color="maroon">*</font></span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:RadioButtonList ID="rbtnStatus" runat="server"
                                RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem Text="All" class="field-label" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Applied" class="field-label" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Not Applied" class="field-label" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td align="left" colspan="2">
                            <asp:RadioButtonList ID="rbtnStudentStatus" runat="server"
                                RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem Text="Existing Students" class="field-label" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="New Students" class="field-label" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td><span class="field-label">Student ID</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtStuID" runat="server"></asp:TextBox>
                        </td>
                        <td><span class="field-label">Student Name</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="button" ValidationGroup="groupM1" />
                        </td>
                    </tr>

                    <tr runat="server" id="trGV1">
                        <td align="left" colspan="2">
                            <asp:GridView ID="gvGamesDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered" 
                                EmptyDataText="No Details For the Chosen Criteria!" width="100%" OnPageIndexChanging="gvGamesDetails_PageIndexChanging"
                                 PageSize="20">

                                <Columns>
                                    <asp:TemplateField HeaderText="Activity">
                                        <HeaderTemplate>
                                            Activity
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActName" runat="server" Text='<%# Bind("GAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seats">
                                        <HeaderTemplate>
                                            Seats
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:Label ID="lblSeatCount" runat="server" Text='<%# Bind("SEATS") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alloted">
                                        <HeaderTemplate>
                                            Alloted
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAlloted" runat="server" Text='<%# Bind("ALLOTED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                               
                            </asp:GridView>
                        </td>
                        <td align="left" colspan="2">
                            <asp:GridView ID="gvClubsDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                EmptyDataText="No Details For the Chosen Criteria!" width="100%" OnPageIndexChanging="gvClubsDetails_PageIndexChanging"
                                PageSize="20">

                                <Columns>
                                    <asp:TemplateField HeaderText="Activity">
                                        <HeaderTemplate>
                                            Activity
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActNameCl" runat="server" Text='<%# Bind("CLUB") %>' BorderColor="#CCFF33"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seats">
                                        <HeaderTemplate>
                                            Seats
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:Label ID="lblSeatCl" runat="server" Text='<%# Bind("SEATS") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alloted">
                                        <HeaderTemplate>
                                            Alloted
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAllotedCl" runat="server" Text='<%# Bind("ALLOTED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <%--<AlternatingRowStyle CssClass="griditem_alternative" />--%>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr runat="server" id="trGV1a">
                        <td width="100%" colspan="4">
                             <asp:GridView ID="gvCommDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                EmptyDataText="No Details For the Chosen Criteria!" width="100%" OnPageIndexChanging="gvCommDetails_PageIndexChanging"
                                 PageSize="20">

                                <Columns>
                                    <asp:TemplateField HeaderText="Activity">
                                        <HeaderTemplate>
                                            Activity
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActNameCOMM" runat="server" Text='<%# Bind("GAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seats">
                                        <HeaderTemplate>
                                            Seats
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:Label ID="lblSeatCountCOMM" runat="server" Text='<%# Bind("SEATS") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alloted">
                                        <HeaderTemplate>
                                            Alloted
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAllotedCOMM" runat="server" Text='<%# Bind("ALLOTED") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                               
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trErrors" runat="server">
                        <td colspan="4">
                            <asp:Label ID="lblGamesError" runat="server" Text="" CssClass="error"></asp:Label>
                            <asp:Label ID="lblClubsError" runat="server" Text="" CssClass="error"></asp:Label>
                            <asp:Label ID="lblCOMMError" runat="server" Text="" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trGV2">
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvAllocationDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-row table-bordered" EmptyDataText="No Details For the Chosen Criteria!"
                                width="100%" PageSize="20">
                                <RowStyle CssClass="griditem"   Wrap="False" />

                                <Columns>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <HeaderTemplate>
                                            Student Id
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            Student Name
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact Email">
                                        <HeaderTemplate>
                                            Contact Email
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblContact" runat="server" Text='<%# Bind("STU_CONTACTEMAIL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date of Join">
                                        <HeaderTemplate>
                                            DOJ
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Bind("STU_DOJ") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrdID" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Game">
                                        <HeaderTemplate>
                                            Game
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlGames" runat="server" AutoPostBack="true" Enabled="False" Visible="false">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblGame" runat="server" Text='<%# Bind("GAMES") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemTemplate>
                                            <asp:Label ID="lblGame" runat="server" Text='<%# Bind("GAMES") %>'></asp:Label>
                                        </ItemTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Club">
                                        <HeaderTemplate>
                                            Club
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlClubs" runat="server" Enabled="False" AutoPostBack="true" Visible="false">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblClub" runat="server" Text='<%# Bind("CLUBS") %>'></asp:Label>

                                        </ItemTemplate>
                                        <%--<ItemTemplate>
                                            <asp:Label ID="lblClub" runat="server" Text='<%# Bind("CLUBS") %>'></asp:Label>
                                        </ItemTemplate>--%>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Communication">
                                        <HeaderTemplate>
                                            Communication
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlCOMM" runat="server" AutoPostBack="true" Enabled="False" Visible="false">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblCOMM" runat="server" Text='<%# Bind("GAMES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemTemplate>
                                            <asp:Label ID="lblGame" runat="server" Text='<%# Bind("GAMES") %>'></asp:Label>
                                        </ItemTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gender" Visible="false">
                                        <HeaderTemplate>
                                            Gender
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGender" runat="server" Text='<%# Bind("STU_GENDER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnEdit" CommandName="Edit" runat="server" Text="Edit" />
                                            <asp:LinkButton ID="lnkbtnUpdate" CommandName="Update" runat="server" Text="Save" Visible="false" />
                                            <asp:LinkButton ID="lnkbtnCancel" CommandName="Cancel" runat="server" Text="Cancel" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfGames" runat="server" />
                <asp:HiddenField ID="hfClubs" runat="server" />
                <asp:HiddenField ID="hfCOMM" runat="server" />
                <asp:HiddenField ID="hfGRD_ID_NEXT" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

