﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaAttenSchedule_M.aspx.vb" Inherits="CoCurricularActivities_ccaAttenSchedule_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Attendance Schedule"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New/Edit Delete</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label"> Academic Year</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                        </td>
                        <td align="left" width="20%"><span class="field-label">Activity</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlActivity" AutoPostBack="true" runat="server" />
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td id="Td1" align="center" class="matters" runat="server" colspan="4">
                            <asp:GridView ID="grdType" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem"   />
                                <Columns>
                                    <asp:TemplateField HeaderText="Group Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl1" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From To Date ">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl2" runat="server" Text='<%# Bind("DTE") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkAmount" runat="server" Text='<%#Bind("DTE") %>' CausesValidation="False"
                                                OnClientClick="return false;"  ></asp:LinkButton><ajaxToolkit:PopupControlExtender
                                                    ID="PopupControlExtender1" runat="server"   DynamicContextKey='<%# Eval("ATL_ID") %>'
                                                    DynamicControlID="Panel1" DynamicServiceMethod="GetDynamicContent" PopupControlID="Panel1"
                                                    Position="Bottom" TargetControlID="lnkAmount">
                                                </ajaxToolkit:PopupControlExtender>

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Usename">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl6" runat="server" Text='<%# Bind("ATL_USER") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Create Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl7" runat="server" Text='<%# Bind("ATL_CRDATE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

                <asp:Panel ID="Panel1" runat="server">
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

