﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaAbsent.aspx.vb" Inherits="CoCurricularActivities_ccaAbsent" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
        <asp:Label ID="lblHeader" runat="server" Text="Absentees Reasons"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                 ></asp:Label>
                        </td>
                    </tr>
                    <tr id="Tr2" runat="server">
                        <td id="Td2" align="center" class="matters" runat="server">
                            <asp:GridView ID="gvSkill" runat="server" AllowPaging="false" AutoGenerateColumns="False" Width="80%"
                                CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                  PageSize="20">
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSksId" runat="server" Text='<%# Bind("AB_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txtSkill" runat="server" TextMode="multiline" SkinID="MultiText"   Text='<%# Bind("AB_REASON") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                Text="Delete"></asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td id="Td1" align="center" class="matters" runat="server">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

