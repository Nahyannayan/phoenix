
Partial Class Library_libraryEditingItem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim edit = Request.QueryString("Edit")
            If edit = 0 Then
                Tab1.ActiveTabIndex = 2
                HT1.Enabled = False
                HT2.Enabled = False
                HT4.Enabled = False

            End If

        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub
End Class
