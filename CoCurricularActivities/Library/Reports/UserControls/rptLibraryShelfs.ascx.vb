Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_Reports_UserControls_rptLibraryShelfs
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindControls()
        End If
    End Sub
    Public Sub BindControls()

        BindLibraryDivisions(ddrptlibrarydivShelfs)
        BindLibrarySubDivisions(ddrptlibsubdivshelfs, ddrptlibrarydivShelfs.SelectedValue)

    End Sub
    Protected Sub ddrptlibrarydivShelfs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddrptlibrarydivShelfs.SelectedIndexChanged
        BindLibrarySubDivisions(ddrptlibsubdivshelfs, ddrptlibrarydivShelfs.SelectedValue)
    End Sub

    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList)
        ddLibraryDivisions.Items.Clear()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()
        End If
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Library Divisions"
        ddLibraryDivisions.Items.Insert(0, list)

    End Sub

    Public Sub BindLibrarySubDivisions(ByVal ddLibrarySubDivisions As DropDownList, ByVal librarydivisions As String)
        ddLibrarySubDivisions.Items.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & librarydivisions & "' ORDER BY LIBRARY_SUB_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then

            ddLibrarySubDivisions.DataSource = ds
            ddLibrarySubDivisions.DataTextField = "LIBRARY_SUB_DIVISION_DES"
            ddLibrarySubDivisions.DataValueField = "LIBRARY_SUB_DIVISION_ID"
            ddLibrarySubDivisions.DataBind()

        End If

        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Library Sub Divisions"
        ddLibrarySubDivisions.Items.Insert(0, list)

    End Sub

    Protected Sub btnlibraryshelfs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlibraryshelfs.Click
        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")
        If ddrptlibrarydivShelfs.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddrptlibrarydivShelfs.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If

        If ddrptlibsubdivshelfs.SelectedIndex > 0 Then
            param.Add("@LIBRARY_SUB_DIVISION_ID", ddrptlibsubdivshelfs.SelectedValue)
        Else
            param.Add("@LIBRARY_SUB_DIVISION_ID", DBNull.Value)
        End If

        Dim reportpath = "~/Library/Reports/Reports/rptLibraryShelfs.rpt"
        ViewReports(param, reportpath)

    End Sub

    Public Sub ViewReports(ByVal param As Hashtable, ByVal reportpath As String)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", HiddenBsuID.Value)
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            .reportPath = Server.MapPath(reportpath)
        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
