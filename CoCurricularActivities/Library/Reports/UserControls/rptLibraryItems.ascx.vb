Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_Reports_UserControls_rptLibraryItems
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindLibraryDivisions()
            BindLibraryDivisionsQuality()
            BindFromTo()
            BindStatus()
            BindItem()
        End If
    End Sub
    Public Sub BindLibraryDivisionsQuality()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlibLibDivQuality.DataSource = ds
        ddlibLibDivQuality.DataTextField = "LIBRARY_DIVISION_DES"
        ddlibLibDivQuality.DataValueField = "LIBRARY_DIVISION_ID"
        ddlibLibDivQuality.DataBind()

        Dim list As New ListItem
        list.Text = "Library Divisions"
        list.Value = "-1"
        ddlibLibDivQuality.Items.Insert(0, list)

    End Sub

    Public Sub BindItem()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_ITEMS"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dditems.DataSource = ds
        dditems.DataTextField = "ITEM_DES"
        dditems.DataValueField = "ITEM_ID"
        dditems.DataBind()

        Dim list As New ListItem
        list.Text = "Items"
        list.Value = "-1"
        dditems.Items.Insert(0, list)

    End Sub

    Public Sub BindFromTo()
        Dim i
        For i = 0 To 70

            Dim list As New ListItem
            list.Value = i + 5
            list.Text = i + 5

            DDFromAge.Items.Insert(i, list)
            DDToAge.Items.Insert(i, list)
        Next

        Dim list1 As New ListItem
        list1.Text = "From"
        list1.Value = "-1"
        DDFromAge.Items.Insert(0, list1)

        Dim list2 As New ListItem
        list2.Text = "To"
        list2.Value = "-1"
        DDToAge.Items.Insert(0, list2)


    End Sub
    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE STATUS_ID != 4 "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddStatus.DataSource = ds
        ddStatus.DataTextField = "STATUS_DESCRIPTION"
        ddStatus.DataValueField = "STATUS_ID"
        ddStatus.DataBind()

        Dim list As New ListItem
        list.Text = "Item Status"
        list.Value = "-1"
        ddStatus.Items.Insert(0, list)

    End Sub
    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivStock.DataSource = ds
        ddLibraryDivStock.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivStock.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivStock.DataBind()

        Dim list As New ListItem
        list.Text = "Library Divisions"
        list.Value = "-1"
        ddLibraryDivStock.Items.Insert(0, list)

        BindLibrarySubDivisions()

    End Sub

    Public Sub BindLibrarySubDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivStock.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibSubDivStock.DataSource = ds
        ddLibSubDivStock.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        ddLibSubDivStock.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        ddLibSubDivStock.DataBind()

        Dim list As New ListItem
        list.Text = "Library Sub Divisions"
        list.Value = "-1"
        ddLibSubDivStock.Items.Insert(0, list)


        BindShelfS()

    End Sub


    Public Sub BindShelfS()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SHELFS " & _
                        "WHERE LIBRARY_SUB_DIVISION_ID='" & ddLibSubDivStock.SelectedValue & "' ORDER BY SHELF_NAME DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddshelfstock.DataSource = ds
        ddshelfstock.DataTextField = "SHELF_NAME"
        ddshelfstock.DataValueField = "SHELF_ID"
        ddshelfstock.DataBind()

        Dim list As New ListItem
        list.Text = "Shelfs"
        list.Value = "-1"
        ddshelfstock.Items.Insert(0, list)

        BindRacks()
    End Sub

    Public Sub BindRacks()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_RACKS " & _
                        "WHERE SHELF_ID='" & ddshelfstock.SelectedValue & "' ORDER BY RACK_NAME DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddrackstock.DataSource = ds
        ddrackstock.DataTextField = "RACK_NAME"
        ddrackstock.DataValueField = "RACK_ID"
        ddrackstock.DataBind()

        Dim list As New ListItem
        list.Text = "Racks"
        list.Value = "-1"
        ddrackstock.Items.Insert(0, list)


    End Sub


    Protected Sub btnlibmembershipsusers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlibmembershipsusers.Click

        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")
        If ddlibLibDivQuality.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddlibLibDivQuality.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If

        If RadioAvailable.SelectedValue <> "ALL" Then
            param.Add("@AVAILABLE", RadioAvailable.SelectedValue)
        Else
            param.Add("@AVAILABLE", DBNull.Value)
        End If

        Dim reportpath = "~/Library/Reports/Reports/rptLibraryItemsQuantityDetails.rpt"
        ViewReports(param, reportpath)


    End Sub

    Public Sub ViewReports(ByVal param As Hashtable, ByVal reportpath As String)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", HiddenBsuID.Value)
        param.Add("@IMG_TYPE", "LOGO")


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            .reportPath = Server.MapPath(reportpath)
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub


    Protected Sub btnstockdetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnstockdetails.Click

        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")
        If ddLibraryDivStock.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddLibraryDivStock.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If

        If ddLibSubDivStock.SelectedIndex > 0 Then
            param.Add("@LIBRARY_SUB_DIVISION_ID", ddLibSubDivStock.SelectedValue)
        Else
            param.Add("@LIBRARY_SUB_DIVISION_ID", DBNull.Value)
        End If

        If ddshelfstock.SelectedIndex > 0 Then
            param.Add("@SHELF_ID", ddshelfstock.SelectedValue)
        Else
            param.Add("@SHELF_ID", DBNull.Value)
        End If

        If ddrackstock.SelectedIndex > 0 Then
            param.Add("@RACK_ID", ddrackstock.SelectedValue)
        Else
            param.Add("@RACK_ID", DBNull.Value)
        End If

        If txtmasterid.Text.Trim() <> "" Then
            param.Add("@MASTER_ID", txtmasterid.Text.Trim())
        Else
            param.Add("@MASTER_ID", DBNull.Value)
        End If

        If txtstockid.Text.Trim() <> "" Then
            param.Add("@STOCK_ID", txtstockid.Text.Trim())
        Else
            param.Add("@STOCK_ID", DBNull.Value)
        End If

        If txtisbn.Text.Trim() <> "" Then
            param.Add("@ISBN", txtisbn.Text.Trim())
        Else
            param.Add("@ISBN", DBNull.Value)
        End If

        If txttitle.Text.Trim() <> "" Then
            param.Add("@ITEM_TITLE", txttitle.Text.Trim())
        Else
            param.Add("@ITEM_TITLE", DBNull.Value)
        End If

        If txtauthor.Text.Trim() <> "" Then
            param.Add("@AUTHOR", txtauthor.Text.Trim())
        Else
            param.Add("@AUTHOR", DBNull.Value)
        End If

        If txtpublisher.Text.Trim() <> "" Then
            param.Add("@PUBLISHER", txtpublisher.Text.Trim())
        Else
            param.Add("@PUBLISHER", DBNull.Value)
        End If


        If txtsubjects.Text.Trim() <> "" Then
            param.Add("@SUBJECT", txtsubjects.Text.Trim())
        Else
            param.Add("@SUBJECT", DBNull.Value)
        End If

        If txtpublishdate.Text.Trim() <> "" Then
            param.Add("@PURCHASE_DATE", txtpublishdate.Text.Trim())
        Else
            param.Add("@PURCHASE_DATE", DBNull.Value)
        End If


        If txtentrydate.Text.Trim() <> "" Then
            param.Add("@ENTRY_DATE", txtentrydate.Text.Trim())
        Else
            param.Add("@ENTRY_DATE", DBNull.Value)
        End If


        If DDFromAge.SelectedIndex > 0 Then
            param.Add("@FROM_AGE_GROUP", DDFromAge.SelectedValue)
        Else
            param.Add("@FROM_AGE_GROUP", 0)
        End If

        If DDToAge.SelectedIndex > 0 Then
            param.Add("@TO_AGE_GROUP", DDToAge.SelectedValue)
        Else
            param.Add("@TO_AGE_GROUP", 100)
        End If

        If dditems.SelectedIndex > 0 Then
            param.Add("@ITEM_ID", dditems.SelectedValue)
        Else
            param.Add("@ITEM_ID", DBNull.Value)
        End If

        If ddStatus.SelectedIndex > 0 Then
            param.Add("@STATUS_ID", ddStatus.SelectedValue)
        Else
            param.Add("@STATUS_ID", DBNull.Value)
        End If

        If RadioAvl.SelectedValue <> "ALL" Then
            param.Add("@AVALABLE", RadioAvl.SelectedValue)
        Else
            param.Add("@AVALABLE", DBNull.Value)
        End If

        param.Add("@ACTIVE", DBNull.Value)

        Dim reportpath = "~/Library/Reports/Reports/rptLibraryItemStockDetails.rpt"
        ViewReports(param, reportpath)


    End Sub

    Protected Sub ddLibraryDivStock_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibraryDivStock.SelectedIndexChanged
        BindLibrarySubDivisions()
    End Sub

    Protected Sub ddLibSubDivStock_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibSubDivStock.SelectedIndexChanged
        BindShelfS()
    End Sub

    Protected Sub ddshelfstock_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddshelfstock.SelectedIndexChanged
        BindRacks()
    End Sub
End Class
