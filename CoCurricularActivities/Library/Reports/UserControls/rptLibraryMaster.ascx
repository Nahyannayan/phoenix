<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryMaster.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryMaster" %>
<div class="matters">
    <div align="left" class="matters">
        <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="5">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                  
<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
                    <tr>
                        <td class="subheader_img">
                            Library Divisions</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnRptLibraryDivision" runat="server" Text="Generate Report" CssClass="button" /></td>
                    </tr>
                </table>
                  
                </ContentTemplate>
                <HeaderTemplate>
                     Library Divisions
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>

<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Library Sub Divisions</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        Library Divisions</td>
                    <td align="left">
                        <asp:DropDownList ID="ddrptlibrarysubdivisions" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnrptLibrarysubdivisions" runat="server" Text="Generate Report" CssClass="button" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
                   
                   
                </ContentTemplate>
                <HeaderTemplate>
                    Library Sub Divisions
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT3" runat="server">
                <ContentTemplate>
<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Library Shelfs</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        Library Divisions</td>
                    <td align="left">
                        <asp:DropDownList ID="ddrptlibrarydivShelfs" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        Library Sub
&nbsp;Divisions</td>
                    <td>
                        <asp:DropDownList ID="ddrptlibsubdivshelfs" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnlibraryshelfs" runat="server" Text="Generate Report" CssClass="button" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
                   
                </ContentTemplate>
                <HeaderTemplate>
                    Library Shelfs
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
             <ajaxToolkit:TabPanel ID="HT4" runat="server">
                <ContentTemplate>
<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Library Racks</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            Library Divisions</td>
                        <td align="left">
                            <asp:DropDownList ID="ddrptlibrarydivRacks" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            Library Sub &nbsp;Divisions</td>
                        <td>
                            <asp:DropDownList ID="ddrptlibsubdivRacks" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnrepracks" runat="server" Text="Generate Report" CssClass="button" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                   
                </ContentTemplate>
                <HeaderTemplate>
                    Library Racks
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
             <ajaxToolkit:TabPanel ID="HT5" runat="server">
                <ContentTemplate>
<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Library Items</td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Button ID="btnlibitems" runat="server" Text="Generate Report" CssClass="button" /></td>
        </tr>
    </table>
                   
                </ContentTemplate>
                <HeaderTemplate>
                    Library Items
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
             <ajaxToolkit:TabPanel ID="HT6" runat="server">
                <ContentTemplate>
<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Library Memberships</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            Library Divisions</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlibmembershipsLibDiv" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnlibmemberships" runat="server" Text="Generate Report" CssClass="button" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                   
                </ContentTemplate>
                <HeaderTemplate>
                    Library Memberships
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
        </ajaxToolkit:TabContainer>
  
    </div>

</div>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
