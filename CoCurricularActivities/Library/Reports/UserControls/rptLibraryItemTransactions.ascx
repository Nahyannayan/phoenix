<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryItemTransactions.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryItemTransactions" %>
    <div>
        <table  cellpadding="5" cellspacing="0" class="matters"
            width="100%">
           <%-- <tr>
                <td class="subheader_img">
                    Library Item Transactions</td>
            </tr>--%>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td align="left" width="20%">
                               <span class="field-label"> Library Divisions </span></td>
                           
                            <td align="left" width="30%">
                                <asp:DropDownList ID="ddlibLibDivT" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="20%">
                               <span class="field-label"> Issued </span></td>
                           
                            <td align="left" width="30%">
                                <asp:CheckBox ID="CheckIssued" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%">
                               <span class="field-label"> Master ID. </span></td>
                          
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtMasterIdT" runat="server" Width="142px"></asp:TextBox>
                            </td>
                            <td align="left" width="20%">
                             <span class="field-label">  Accession No.</span></td>
                           
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtStockidT" runat="server" Width="142px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%">
                                <span class="field-label">User No.</span></td>
                            
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtUserNoT" runat="server" Width="142px"></asp:TextBox>
                            </td>
                            <td align="left" width="20%">
                              <span class="field-label">  User Type</span></td>
                          
                            <td align="left" width="30%">
                                <asp:RadioButtonList ID="RadioUserType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Text="ALL" Value="ALL"></asp:ListItem>
                                    <asp:ListItem Text="STUDENT" Value="STUDENT"></asp:ListItem>
                                    <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>


                         <tr>
                    <td align="left" width="20%">
                      <span class="field-label">  Select Student Grade/Section</span></td>
                    
                    <td align="left" width="30%">
                        <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="True">
                        </asp:DropDownList>
                        
                         <asp:DropDownList ID="ddlSection" runat="server"  AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="20%">
                     <span class="field-label">   SUBJECT</span>
                    </td>
                   
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                    </td>
                </tr>


                        <tr>
                            <td align="left" width="20%">
                             <span class="field-label">   Item Return Date </span></td>
                            
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtreturndate" runat="server" Width="142px"></asp:TextBox>
                            </td>
                            <td align="left" width="20%">
                               <span class="field-label"> Item Returned Date </span></td>
                           
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtreturneddate" runat="server" Width="142px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%">
                               <span class="field-label"> From Date </span></td>
                          
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtfromdateT" runat="server" Width="142px"></asp:TextBox>
                            </td>
                            <td align="left" width="20%">
                               <span class="field-label"> To Date </span></td>
                           
                            <td align="left" width="30%">
                                <asp:TextBox ID="txttodateT" runat="server" Width="142px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="BtnTransactions" runat="server" CssClass="button" Text="Generate Report" />
                            </td>
                        </tr>
                    </table>
                    <ajaxToolkit:CalendarExtender ID="CE3" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                        PopupButtonID="txtreturndate" TargetControlID="txtreturndate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CE4" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                        PopupButtonID="txtreturneddate" TargetControlID="txtreturneddate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CE5" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                        PopupButtonID="txtfromdateT" TargetControlID="txtfromdateT">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CE6" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                        PopupButtonID="txttodateT" TargetControlID="txttodateT">
                    </ajaxToolkit:CalendarExtender>
                </td>
            </tr>
        </table>
    
    </div>
        <asp:HiddenField ID="HiddenBsuID" runat="server" />