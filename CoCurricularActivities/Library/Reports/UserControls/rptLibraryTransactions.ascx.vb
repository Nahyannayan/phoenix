Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_Reports_UserControls_rptLibraryTransactions
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindLibraryDivisions(ddlibLibDiv)
            BindLibraryDivisions(ddlibLibDivT)
        End If

    End Sub

    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList)
        ddLibraryDivisions.Items.Clear()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()


        End If
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Library Divisions"
        ddLibraryDivisions.Items.Insert(0, list)

    End Sub

    Protected Sub btnlibItemtransaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlibItemtransaction.Click

        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")

        If ddlibLibDiv.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddlibLibDiv.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If


        If txtmasterid.Text.Trim() <> "" Then
            param.Add("@MASTER_ID", txtmasterid.Text.Trim())
        Else
            param.Add("@MASTER_ID", DBNull.Value)
        End If

        If txtstockid.Text.Trim() <> "" Then
            param.Add("@STOCK_ID", LibraryData.GetStockIDForAccessonNo(txtstockid.Text.Trim(), HiddenBsuID.Value))
        Else
            param.Add("@STOCK_ID", DBNull.Value)
        End If

        If txtuserno.Text.Trim() <> "" Then
            param.Add("@USER_NO", txtuserno.Text.Trim())
        Else
            param.Add("@USER_NO", DBNull.Value)
        End If

        If RadioUser.SelectedValue <> "ALL" Then
            param.Add("@USER_TYPE", RadioUser.SelectedValue)
        Else
            param.Add("@USER_TYPE", DBNull.Value)
        End If

        If Radioreservationcancel.SelectedValue <> "ALL" Then
            param.Add("@RESERVATION_CANCEL", Radioreservationcancel.SelectedValue)
        Else
            param.Add("@RESERVATION_CANCEL", DBNull.Value)
        End If

        If radioTransactiondone.SelectedValue <> "ALL" Then
            param.Add("@TRANSACTION_DONE", radioTransactiondone.SelectedValue)
        Else
            param.Add("@TRANSACTION_DONE", DBNull.Value)
        End If

        If txtfromdate.Text.Trim() <> "" Then
            param.Add("@FROM_DATE", txtfromdate.Text.Trim())
        Else
            param.Add("@FROM_DATE", DBNull.Value)
        End If

        If txtTodate.Text.Trim() <> "" Then
            param.Add("@TO_DATE", txtTodate.Text.Trim())
        Else
            param.Add("@TO_DATE", DBNull.Value)
        End If


        Dim reportpath = "~/Library/Reports/Reports/rptLibraryItemReservation.rpt"
        ViewReports(param, reportpath)


    End Sub
    Public Sub ViewReports(ByVal param As Hashtable, ByVal reportpath As String)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", HiddenBsuID.Value)
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            .reportPath = Server.MapPath(reportpath)
        End With
        Session("rptClass") = rptClass
        ''Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub BtnTransactions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTransactions.Click
        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")

        If ddlibLibDivT.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddlibLibDivT.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If

        If txtMasterIdT.Text.Trim() <> "" Then
            param.Add("@MASTER_ID", txtMasterIdT.Text.Trim())
        Else
            param.Add("@MASTER_ID", DBNull.Value)
        End If

        If txtStockidT.Text.Trim() <> "" Then
            param.Add("@STOCK_ID", LibraryData.GetStockIDForAccessonNo(txtStockidT.Text.Trim(), HiddenBsuID.Value))
        Else
            param.Add("@STOCK_ID", DBNull.Value)
        End If

        If txtUserNoT.Text.Trim() <> "" Then
            param.Add("@USER_NO", txtUserNoT.Text.Trim())
        Else
            param.Add("@USER_NO", DBNull.Value)
        End If

        If radioTransactiondone.SelectedValue <> "ALL" Then
            param.Add("@USER_TYPE", radioTransactiondone.SelectedValue)
        Else
            param.Add("@USER_TYPE", DBNull.Value)
        End If

        If txtreturndate.Text.Trim() <> "" Then
            param.Add("@ITEM_RETURN_DATE", txtreturndate.Text.Trim())
        Else
            param.Add("@ITEM_RETURN_DATE", DBNull.Value)
        End If

        If txtreturneddate.Text.Trim() <> "" Then
            param.Add("@ITEM_ACTUAL_RETURN_DATE", txtreturneddate.Text.Trim())
        Else
            param.Add("@ITEM_ACTUAL_RETURN_DATE", DBNull.Value)
        End If

        If txtfromdateT.Text.Trim() <> "" Then
            param.Add("@FROM_DATE", txtfromdateT.Text.Trim())
        Else
            param.Add("@FROM_DATE", DBNull.Value)
        End If

        If txttodateT.Text.Trim() <> "" Then
            param.Add("@TO_DATE", txttodateT.Text.Trim())
        Else
            param.Add("@TO_DATE", DBNull.Value)
        End If

        If CheckIssued.Checked Then
            param.Add("@OUT", "True")
        Else
            param.Add("@OUT", DBNull.Value)
        End If


        Dim reportpath = "~/Library/Reports/Reports/rptLibraryTransactions.rpt"
        ViewReports(param, reportpath)

    End Sub


    Protected Sub btncomment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncomment.Click
        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")

        If txtmasteridC.Text.Trim() <> "" Then
            param.Add("@MASTER_ID", txtmasteridC.Text.Trim())
        Else
            param.Add("@MASTER_ID", DBNull.Value)
        End If

        If txtisbn.Text.Trim() <> "" Then
            param.Add("@ISBN", txtisbn.Text.Trim())
        Else
            param.Add("@ISBN", DBNull.Value)
        End If

        If txtfromdateC.Text.Trim() <> "" Then
            param.Add("@FROM_DATE", txtfromdateC.Text.Trim())
        Else
            param.Add("@FROM_DATE", DBNull.Value)
        End If

        If txttodateC.Text.Trim() <> "" Then
            param.Add("@TO_DATE", txttodateC.Text.Trim())
        Else
            param.Add("@TO_DATE", DBNull.Value)
        End If


        Dim reportpath = "~/Library/Reports/Reports/rptLibraryItemComments.rpt"
        ViewReports(param, reportpath)

    End Sub


End Class
