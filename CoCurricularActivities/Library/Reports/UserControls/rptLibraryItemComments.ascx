<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryItemComments.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryItemComments" %>
<table cellpadding="5" cellspacing="0" class="matters"
    width="100%">
   <%-- <tr>
        <td class="subheader_img">
            Library Item Comments</td>
    </tr>--%>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td align="left">
                       <span class="field-label">Master ID. </span> </td>
                   
                    <td align="left">
                        <asp:TextBox ID="txtmasteridC" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td align="left">
                       <span class="field-label">  ISBN </span></td>
                   
                    <td align="left">
                        <asp:TextBox ID="txtisbn" runat="server" Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                       <span class="field-label">  From Date </span></td>
                 
                    <td align="left">
                        <asp:TextBox ID="txtfromdateC" runat="server" Width="142px"></asp:TextBox>
                    </td>
                    <td align="left">
                        <span class="field-label"> To Date </span></td>
                    
                    <td align="left">
                        <asp:TextBox ID="txttodateC" runat="server" Width="142px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="btncomment" runat="server" CssClass="button" Text="Generate Report" />
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CalendarExtender ID="CE7" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtfromdateC" TargetControlID="txtfromdateC">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="CE8" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txttodateC" TargetControlID="txttodateC">
            </ajaxToolkit:CalendarExtender>
            &nbsp;
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
