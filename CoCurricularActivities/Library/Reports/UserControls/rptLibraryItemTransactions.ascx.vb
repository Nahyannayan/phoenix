Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_Reports_UserControls_rptLibraryItemTransactions
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")

            BindLibraryDivisions(ddlibLibDivT)
            callGrade_ACDBind()
        End If

    End Sub
    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList)
        ddLibraryDivisions.Items.Clear()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()


        End If
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Library Divisions"
        ddLibraryDivisions.Items.Insert(0, list)

    End Sub

    Protected Sub BtnTransactions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTransactions.Click
        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")

        If ddlibLibDivT.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddlibLibDivT.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If

        If txtMasterIdT.Text.Trim() <> "" Then
            param.Add("@MASTER_ID", txtMasterIdT.Text.Trim())
        Else
            param.Add("@MASTER_ID", DBNull.Value)
        End If

        If txtStockidT.Text.Trim() <> "" Then
            param.Add("@STOCK_ID", LibraryData.GetStockIDForAccessonNo(txtStockidT.Text.Trim(), HiddenBsuID.Value))
        Else
            param.Add("@STOCK_ID", DBNull.Value)
        End If

        If txtUserNoT.Text.Trim() <> "" Then
            param.Add("@USER_NO", txtUserNoT.Text.Trim())
        Else
            param.Add("@USER_NO", DBNull.Value)
        End If

        If RadioUserType.SelectedValue <> "ALL" Then
            param.Add("@USER_TYPE", RadioUserType.SelectedValue)
        Else
            param.Add("@USER_TYPE", DBNull.Value)
        End If

        If txtreturndate.Text.Trim() <> "" Then
            param.Add("@ITEM_RETURN_DATE", txtreturndate.Text.Trim())
        Else
            param.Add("@ITEM_RETURN_DATE", DBNull.Value)
        End If

        If txtreturneddate.Text.Trim() <> "" Then
            param.Add("@ITEM_ACTUAL_RETURN_DATE", txtreturneddate.Text.Trim())
        Else
            param.Add("@ITEM_ACTUAL_RETURN_DATE", DBNull.Value)
        End If

        If txtfromdateT.Text.Trim() <> "" Then
            param.Add("@FROM_DATE", txtfromdateT.Text.Trim())
        Else
            param.Add("@FROM_DATE", DBNull.Value)
        End If

        If txttodateT.Text.Trim() <> "" Then
            param.Add("@TO_DATE", txttodateT.Text.Trim())
        Else
            param.Add("@TO_DATE", DBNull.Value)
        End If

        If CheckIssued.Checked Then
            param.Add("@OUT", "True")
        Else
            param.Add("@OUT", DBNull.Value)
        End If

        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@STU_GRM_ID", DBNull.Value)
        Else
            param.Add("@STU_GRM_ID", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", DBNull.Value)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
        End If


        'If txtSubject.Text = "" Then
        '    param.Add("@SUBJECT", DBNull.Value)
        'Else
        '    param.Add("@SUBJECT", txtSubject.Text)
        'End If

        Dim reportpath = "~/Library/Reports/Reports/rptLibraryTransactions.rpt"
        ViewReports(param, reportpath)

    End Sub
    Public Sub ViewReports(ByVal param As Hashtable, ByVal reportpath As String)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", HiddenBsuID.Value)
        param.Add("@IMG_TYPE", "LOGO")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            .reportPath = Server.MapPath(reportpath)
        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            ACD_ID = Session("Current_ACD_ID")
            Dim SHF_ID As String = String.Empty
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID_LIB(ACD_ID, Session("CLM"), SHF_ID)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlSection.Items.Clear()
            di = New ListItem("ALL", "0")
            ddlSection.Items.Add(di)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            ACD_ID = Session("Current_ACD_ID")
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty

            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_LIB(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
End Class
