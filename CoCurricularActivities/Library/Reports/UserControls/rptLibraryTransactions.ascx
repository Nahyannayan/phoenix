<%@ Control Language="VB" AutoEventWireup="false" CodeFile="rptLibraryTransactions.ascx.vb" Inherits="Library_Reports_UserControls_rptLibraryTransactions" %>
<div align="left" class="matters">
    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="HT1" runat="server">
            <HeaderTemplate>
                Library Item Reservations
            </HeaderTemplate>
            <ContentTemplate>


<table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Library Item Reservations</td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td align="left">
                        Library Divisions</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:DropDownList ID="ddlibLibDiv" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                    <td align="left">
                    </td>
                    <td align="left">
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        Master ID.</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:TextBox ID="txtmasterid" runat="server" Width="142px"></asp:TextBox></td>
                    <td align="left">
                        Accession No.</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:TextBox ID="txtstockid" runat="server" Width="142px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        User No.</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:TextBox ID="txtuserno" runat="server" Width="142px"></asp:TextBox></td>
                    <td align="left">
                        User Type</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:RadioButtonList ID="RadioUser" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="ALL" Value="ALL"></asp:ListItem>
                            <asp:ListItem  Text="STUDENT" Value="STUDENT"></asp:ListItem>
                            <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                        </asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td align="left">
                        Reservation Cancel</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:RadioButtonList ID="Radioreservationcancel" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td align="left">
                        Transaction Done</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:RadioButtonList ID="radioTransactiondone" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Text="All" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="YES"></asp:ListItem>
                            <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                        </asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td align="left">
                        From Date</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:TextBox ID="txtfromdate" runat="server" Width="142px"></asp:TextBox></td>
                    <td align="left">
                        To Date</td>
                    <td align="left">
                        :</td>
                    <td align="left">
                        <asp:TextBox ID="txtTodate" runat="server" Width="142px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="6">
                        <asp:Button ID="btnlibItemtransaction" runat="server" CssClass="button" Text="Generate Report" /></td>
                </tr>
            </table>
            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtfromdate" TargetControlID="txtfromdate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtTodate" TargetControlID="txtTodate">
            </ajaxToolkit:CalendarExtender>
        </td>
    </tr>
</table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="HT2" runat="server">
            <HeaderTemplate>
                Library Item Transactions
            </HeaderTemplate>
            <ContentTemplate>
    <table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Library Item Transactions</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td align="left">
                            Library Divisions</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlibLibDivT" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left">
                            Issued</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:CheckBox ID="CheckIssued" runat="server" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Master ID.</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtMasterIdT" runat="server" Width="142px"></asp:TextBox></td>
                        <td align="left">
                            Accession No.</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtStockidT" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">
                            User No.</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtUserNoT" runat="server" Width="142px"></asp:TextBox></td>
                        <td align="left">
                            User Type</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:RadioButtonList ID="RadioUserType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="ALL" Value="ALL"></asp:ListItem>
                                <asp:ListItem Text="STUDENT" Value="STUDENT"></asp:ListItem>
                                <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                            </asp:RadioButtonList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            Item Return Date</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtreturndate" runat="server" Width="142px"></asp:TextBox></td>
                        <td align="left">
                            Item Returned Date</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtreturneddate" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">
                            From Date</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtfromdateT" runat="server" Width="142px"></asp:TextBox></td>
                        <td align="left">
                            To Date</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txttodateT" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="BtnTransactions" runat="server" CssClass="button" Text="Generate Report" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CE3" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtreturndate" TargetControlID="txtreturndate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CE4" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtreturneddate" TargetControlID="txtreturneddate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CE5" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtfromdateT" TargetControlID="txtfromdateT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CE6" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txttodateT" TargetControlID="txttodateT">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
         <ajaxToolkit:TabPanel ID="HT3" runat="server">
            <HeaderTemplate>
                Library Item Comments
            </HeaderTemplate>
             <ContentTemplate>
                 <table border="1" class="matters" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
        <tr>
            <td class="subheader_img">
                Library Item Comments</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td align="left">
                            Master ID.</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtmasteridC" runat="server" Width="142px"></asp:TextBox></td>
                        <td align="left">
                            ISBN</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtisbn" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">
                            From Date</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txtfromdateC" runat="server" Width="142px"></asp:TextBox></td>
                        <td align="left">
                            To Date</td>
                        <td align="left">
                            :</td>
                        <td align="left">
                            <asp:TextBox ID="txttodateC" runat="server" Width="142px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btncomment" runat="server" CssClass="button" Text="Generate Report" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CE7" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txtfromdateC" TargetControlID="txtfromdateC">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CE8" runat="server" Enabled="True" Format="dd/MMM/yyyy"
                PopupButtonID="txttodateC" TargetControlID="txttodateC">
                </ajaxToolkit:CalendarExtender>
                &nbsp;
            </td>
        </tr>
    </table>
             </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer><br />
<asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>