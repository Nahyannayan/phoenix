Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_Reports_UserControls_rptLibraryQuantityDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")

            BindLibraryDivisionsQuality()
           
        End If

    End Sub

    Public Sub BindLibraryDivisionsQuality()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlibLibDivQuality.DataSource = ds
        ddlibLibDivQuality.DataTextField = "LIBRARY_DIVISION_DES"
        ddlibLibDivQuality.DataValueField = "LIBRARY_DIVISION_ID"
        ddlibLibDivQuality.DataBind()

        Dim list As New ListItem
        list.Text = "Library Divisions"
        list.Value = "-1"
        ddlibLibDivQuality.Items.Insert(0, list)

    End Sub
    Protected Sub btnlibmembershipsusers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlibmembershipsusers.Click
        Dim param As New Hashtable
        param.Add("@BSU_ID", HiddenBsuID.Value)
        param.Add("Search", "")
        If ddlibLibDivQuality.SelectedIndex > 0 Then
            param.Add("@LIBRARY_DIVISION_ID", ddlibLibDivQuality.SelectedValue)
        Else
            param.Add("@LIBRARY_DIVISION_ID", DBNull.Value)
        End If

        If RadioAvailable.SelectedValue <> "ALL" Then
            param.Add("@AVAILABLE", RadioAvailable.SelectedValue)
        Else
            param.Add("@AVAILABLE", DBNull.Value)
        End If

        Dim reportpath = "~/Library/Reports/Reports/rptLibraryItemsQuantityDetails.rpt"
        ViewReports(param, reportpath)

    End Sub

    Public Sub ViewReports(ByVal param As Hashtable, ByVal reportpath As String)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("@IMG_BSU_ID", HiddenBsuID.Value)
        param.Add("@IMG_TYPE", "LOGO")


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS_LIBRARY"
            .reportParameters = param
            .reportPath = Server.MapPath(reportpath)
        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
