<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptLibraryRack.aspx.vb" Inherits="Library_Reports_FilterPages_rptLibraryRack" %>

<%@ Register Src="../UserControls/rptLibraryRack.ascx" TagName="rptLibraryRack" TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Racks
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">

                            <uc1:rptLibraryRack ID="RptLibraryRack1" runat="server" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>
