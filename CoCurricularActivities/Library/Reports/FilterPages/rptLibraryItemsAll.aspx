<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptLibraryItemsAll.aspx.vb" Inherits="Library_Reports_FilterPages_rptLibraryItemsAll" %>

<%@ Register Src="../UserControls/rptLibraryItemsAll.ascx" TagName="rptLibraryItemsAll"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Items Count
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width1="100%"> 
                    <tr>
                        <td align="left">
                            <uc1:rptLibraryItemsAll ID="RptLibraryItemsAll1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
