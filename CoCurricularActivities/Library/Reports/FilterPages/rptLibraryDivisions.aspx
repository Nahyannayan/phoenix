<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptLibraryDivisions.aspx.vb" Inherits="Library_Reports_FilterPages_rptLibraryDivisions" %>

<%@ Register Src="../UserControls/rptLibraryDivisions.ascx" TagName="rptLibraryDivisions"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Divisions
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <uc1:rptLibraryDivisions ID="RptLibraryDivisions1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
