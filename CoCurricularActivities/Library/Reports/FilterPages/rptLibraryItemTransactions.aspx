<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="rptLibraryItemTransactions.aspx.vb" Inherits="Library_Reports_FilterPages_rptLibraryItemTransactions" %>

<%@ Register Src="../UserControls/rptLibraryItemTransactions.ascx" TagName="rptLibraryItemTransactions"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Library Item Transactions
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left">

                            <uc1:rptLibraryItemTransactions ID="RptLibraryItemTransactions1" runat="server"></uc1:rptLibraryItemTransactions>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
