<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryItemTransactions.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryItemTransactions" %>

<%@ Register Src="UserControls/libraryItemTransactions.ascx" TagName="libraryItemTransactions"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Reservations
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr>
                        <td align="left">

                            <uc1:libraryItemTransactions ID="LibraryItemTransactions1" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
