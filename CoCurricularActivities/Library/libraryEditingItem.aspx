<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryEditingItem.aspx.vb" Inherits="Library_libraryEditingItem" %>

<%@ Register Src="UserControls/libraryEditPrimaryItemDetails.ascx" TagName="libraryEditPrimaryItemDetails"
    TagPrefix="uc4" %>

<%@ Register Src="UserControls/libraryEditRatingsCommentsApprove.ascx" TagName="libraryEditRatingsCommentsApprove"
    TagPrefix="uc3" %>

<%@ Register Src="UserControls/libraryBookRackStatusAssign.ascx" TagName="libraryBookRackStatusAssign"
    TagPrefix="uc2" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="UserControls/libraryEditStockDetails.ascx" TagName="libraryEditStockDetails"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Library Items</title>
<%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />--%>

    
<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<!-- Custom fonts for this template-->
<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- Page level plugin CSS-->
<link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="/cssfiles/sb-admin.css" rel="stylesheet"/>
<link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet"/>
<link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet"/>

</head>
<body>
    <form id="form1" runat="server">
    <div class="table-responsive">
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0" >
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                    <uc4:libraryEditPrimaryItemDetails ID="LibraryEditPrimaryItemDetails1" runat="server" />
                
                 
                </ContentTemplate>
                <HeaderTemplate>
                  Edit Primary Information
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                    <uc1:libraryEditStockDetails ID="LibraryEditStockDetails1" runat="server" />
                  
                                
                </ContentTemplate>
                <HeaderTemplate>
                 Edit Stock Details
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            <ajaxToolkit:TabPanel ID="HT3" runat="server">
                <ContentTemplate>
                    <uc2:libraryBookRackStatusAssign ID="LibraryBookRackStatusAssign1" runat="server" />
                
                
                
                </ContentTemplate>
                <HeaderTemplate>
               Accession No / Call No / Rack / Status - Updating
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
             <ajaxToolkit:TabPanel ID="HT4" runat="server">
                <ContentTemplate>
                    <uc3:libraryEditRatingsCommentsApprove ID="LibraryEditRatingsCommentsApprove1" runat="server" />
                
                              
                </ContentTemplate>
                <HeaderTemplate>
                 Approve User Comments
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            

        </ajaxToolkit:TabContainer>
     
        
       
    
    </div>
    </form>
</body>
</html>
