<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"  CodeFile="libraryMembershipTypes.aspx.vb" Inherits="Library_libraryMembershipTypes" %>

<%@ Register Src="UserControls/libraryMembershipTypes.ascx" TagName="libraryMembershipTypes"
    TagPrefix="uc1" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server" >

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Library Memberships
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <uc1:libraryMembershipTypes ID="LibraryMembershipTypes1" runat="server" />
            </div>
        </div>
    </div>
</asp:Content> 
