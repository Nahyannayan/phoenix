<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryDetailView.aspx.vb" Inherits="Library_libraryDetailView" %>

<%@ Register Src="UserControls/libraryReservationHistoryView.ascx" TagName="libraryReservationHistoryView"
    TagPrefix="uc5" %>

<%@ Register Src="UserControls/libraryRatingsCommentsView.ascx" TagName="libraryRatingsCommentsView"
    TagPrefix="uc4" %>

<%@ Register Src="UserControls/LibraryDetailViewStockTransactions.ascx" TagName="LibraryDetailViewStockTransactions"
    TagPrefix="uc3" %>

<%@ Register Src="UserControls/LibraryDetailViewStockDetails.ascx" TagName="LibraryDetailViewStockDetails"
    TagPrefix="uc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="UserControls/libraryDetailView.ascx" TagName="libraryDetailView"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

   

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">

    <!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <title>Library Item Detail Information </title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    


    <form id="form1" runat="server">
    <div>
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="HT1" runat="server">
                <ContentTemplate>
                 <uc1:libraryDetailView ID="LibraryDetailView1" runat="server" />       
                </ContentTemplate>
                <HeaderTemplate>
                   Primary Informations
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="HT2" runat="server">
                <ContentTemplate>
                    <uc2:LibraryDetailViewStockDetails ID="LibraryDetailViewStockDetails1" runat="server" />
                                
                </ContentTemplate>
                <HeaderTemplate>
                  Stock List Details
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                <ContentTemplate>
                    <uc5:libraryReservationHistoryView id="LibraryReservationHistoryView1" runat="server">
                    </uc5:libraryReservationHistoryView>
                
                
                </ContentTemplate>
                <HeaderTemplate>
                  Stock Reservation History
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
             <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                <ContentTemplate>
                
                  <uc3:LibraryDetailViewStockTransactions ID="LibraryDetailViewStockTransactions1" runat="server" />             
                
                </ContentTemplate>
                <HeaderTemplate>
                  Stock Transaction History
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            
            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                <ContentTemplate>
                    <uc4:libraryRatingsCommentsView id="LibraryRatingsCommentsView1" runat="server">
                    </uc4:libraryRatingsCommentsView>
                
                                  
                </ContentTemplate>
                <HeaderTemplate>
               User Comments 
                </HeaderTemplate>
            </ajaxToolkit:TabPanel>
            
            

        </ajaxToolkit:TabContainer>
     
        
       
    
    </div>
    </form>
</body>
</html>
