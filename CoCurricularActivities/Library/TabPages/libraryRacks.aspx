<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryRacks.aspx.vb" Inherits="Library_TabPages_libraryRacks" %>

<%@ Register Src="../UserControls/libraryRacks.ascx" TagName="libraryRacks" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Library Racks</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:libraryRacks id="LibraryRacks1" runat="server">
        </uc1:libraryRacks></div>
    </form>
</body>
</html>
