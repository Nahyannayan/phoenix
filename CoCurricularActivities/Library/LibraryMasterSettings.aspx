<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LibraryMasterSettings.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_LibraryMasterSettings" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="UserControls/libraryDivisions.ascx" TagName="libraryDivisions"    TagPrefix="uc1" %>
<%@ Register Src="UserControls/librarySubDivisions.ascx" TagName="librarySubDivisions"    TagPrefix="uc2" %>
<%@ Register Src="UserControls/libraryShelfs.ascx" TagName="libraryShelfs"    TagPrefix="uc3" %>
<%@ Register Src="UserControls/libraryRacks.ascx" TagName="libraryRacks"    TagPrefix="uc4" %>
<%@ Register Src="UserControls/libraryShelfTeachers.ascx" TagName="libraryShelfTeachers"    TagPrefix="uc5" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <script type="text/javascript">

        //window.setTimeout('setpathExt()', 1000);

        //function setpathExt() {
        //    var path = window.location.href
        //    var Rpath = ''
        //    if (path.indexOf('?') != '-1') {
        //        Rpath = path.substring(path.indexOf('?'), path.length)
        //    }
        //    var objFrame

        //    //Library Divisions
        //    objFrame = document.getElementById("FEx1");
        //    objFrame.src = "TabPages/libraryDivisions.aspx" + Rpath

        //    //Library Sub Divisions
        //    objFrame = document.getElementById("FEx2");
        //    objFrame.src = "TabPages/librarySubDivisions.aspx" + Rpath

        //    //Library Shelfs
        //    objFrame = document.getElementById("FEx3");
        //    objFrame.src = "TabPages/libraryShelfs.aspx" + Rpath


        //    //Library Racks
        //    objFrame = document.getElementById("FEx4");
        //    objFrame.src = "TabPages/libraryRacks.aspx" + Rpath

        //    //Assign Shelf to Teachers
        //    objFrame = document.getElementById("FEx5");
        //    objFrame.src = "TabPages/libraryShelfTeachers.aspx" + Rpath


        //}

//        function clientActiveTabChanged(sender, args) {

           
//            alert(sender.get_activeTabIndex());
//}

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Library Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <ajaxToolkit:TabContainer ID="Tab1" runat="server" ActiveTabIndex="0" AutoPostBack="true"  OnActiveTabChanged="Tab1_ActiveTabChanged">
                    <ajaxToolkit:TabPanel ID="HT1" runat="server">
                        <ContentTemplate>
                            <%--<iframe id="FEx1"   scrolling="auto" marginwidth="0px" frameborder="0" width="100%" visible="false"></iframe>--%>
                            <uc1:libraryDivisions ID="libraryDivisions1" runat="server" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Library Divisions  
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT2" runat="server">
                        <ContentTemplate>
                            <%--<iframe id="FEx2"  scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>--%>
                            <uc2:librarySubDivisions ID="librarysubdivision1" runat="server" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Library Sub Divisions
                    
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT3" runat="server">
                        <ContentTemplate>
                            <%--<iframe id="FEx3"   scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>--%>
                            <uc3:libraryShelfs ID="libraryshelfs1" runat="server" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Library Shelves   
                  
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT4" runat="server">
                        <ContentTemplate>
<%--                            <iframe id="FEx4"  scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>--%>

                            <uc4:libraryRacks ID="libraryrack1" runat="server" />
                        </ContentTemplate>
                        <HeaderTemplate>
                            Library Racks
                  
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="HT5"   runat="server">
                        <ContentTemplate>
                          <%--  <iframe id="FEx5"   scrolling="auto" marginwidth="0px" frameborder="0" width="100%"></iframe>--%>
                            <uc5:libraryShelfTeachers ID="libraryShelfTeachers1" runat="server" />

                        </ContentTemplate>
                        <HeaderTemplate>
                            Assign Shelves to Teachers
                  
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>


                </ajaxToolkit:TabContainer>
                <asp:HiddenField ID="Hiddenempid" runat="server" />
                <asp:HiddenField ID="Hiddenbsuid" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

