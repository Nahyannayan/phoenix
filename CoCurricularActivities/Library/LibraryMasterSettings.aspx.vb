
Partial Class Library_LibraryMasterSettings
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Tab1.ActiveTabIndex = Session("TabNo")
        End If

    End Sub

    Protected Sub Tab1_ActiveTabChanged(sender As Object, e As EventArgs)
        Session("TabNo") = Tab1.ActiveTabIndex
        Page.Response.Redirect(Page.Request.Url.ToString(), True)
    End Sub
End Class
