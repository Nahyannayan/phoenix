Imports Lesnikowski.Barcode

Partial Class Library_Barcode_Barcode
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim barcode As BaseBarcode

        barcode = BarcodeFactory.GetBarcode(Symbology.Code128)

        barcode.Number = Request.QueryString("number")

        barcode.ChecksumAdd = True

        Response.BinaryWrite(barcode.Render(3))


            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
End Class
