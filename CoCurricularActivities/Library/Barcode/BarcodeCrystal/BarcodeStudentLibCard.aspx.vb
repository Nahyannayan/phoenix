﻿Imports Lesnikowski.Barcode
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports Limilabs.Barcode


Partial Class Library_Barcode_BarcodeCrystal_Barcode
    Inherits System.Web.UI.Page
    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PrintBarcode()
    End Sub


    Public Sub PrintBarcode()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString


        Dim ds1 As DataSet

        Dim dsSet As New dsImageRpt

        Dim str_query = "ISSUE_LIBRARY_CARD"
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("libstuids"))

        ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, pParms)
        Dim i = 0

        If ds1.Tables(0).Rows.Count > 0 Then

            For i = 0 To ds1.Tables(0).Rows.Count - 1
                Dim stuno = ds1.Tables(0).Rows(i).Item("STU_NO").ToString()
                Dim stuname = ds1.Tables(0).Rows(i).Item("STUDNAME").ToString()
                Dim grade_no = ds1.Tables(0).Rows(i).Item("GRD_SEC").ToString()
                ''Barcode
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                barcode.Number = stuno
                barcode.ForeColor = Drawing.Color.Black
                barcode.ChecksumAdd = True
                barcode.NarrowBarWidth = 3
                barcode.Height = 300
                barcode.FontHeight = 0.15
                Dim b As Byte()
                ReDim b(barcode.Render(ImageType.Png).Length)
                b = barcode.Render(ImageType.Png)

                Dim dr As dsImageRpt.DSLIB_CARDRow = dsSet.DSLIB_CARD.NewDSLIB_CARDRow
                dr.STU_NO = stuno
                dr.BARCODE = b
                dr.STU_NAME = stuname
                dr.GRADE_SEC = grade_no
                dsSet.DSLIB_CARD.Rows.Add(dr)
            Next


        End If



        repClassVal = New RepClass
        repClassVal.ResourceName = "Studentcard.rpt"
        repClassVal.SetDataSource(dsSet)
        CrystalReportViewer1.ReportSource = repClassVal


        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As ImageButton = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            ' repClassVal.Dispose()

        End Try

    End Sub

    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()

    End Sub


End Class
