﻿Imports Lesnikowski.Barcode
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration


Partial Class Library_Barcode_BarcodeCrystal_Barcode
    Inherits System.Web.UI.Page
    Dim repSource As MyReportClass
    Dim repClassVal As New RepClass

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PrintBarcode()
    End Sub


    Public Sub PrintBarcode()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim ds As New DataSet

        Dim hash As New Hashtable

        If Session("BarcodehashtableTemp") Is Nothing Then
            Session("BarcodehashtableTemp") = Session("Barcodehashtable")
            hash = Session("BarcodehashtableTemp")
            Session("Barcodehashtable") = Nothing
        Else

            hash = Session("BarcodehashtableTemp")
            Session("Barcodehashtable") = Nothing


        End If
        'If Session("Barcodehashtable") Is Nothing Then
        'Else
        '    hash = Session("Barcodehashtable")
        'End If

        Dim ds1 As DataSet
        Dim idictenum As IDictionaryEnumerator
        idictenum = hash.GetEnumerator()

        Dim dsSet As New dsImageRpt

        While (idictenum.MoveNext())
            Dim key = idictenum.Key
            Dim id = idictenum.Value

            Dim str_query = " select BSU_SHORTNAME +'-'+ ISNULL(LIBRARY_CODE,'') +'-' + ISNULL(CALL_NO,'') AS HEADER ,ACCESSION_NO,CALL_NO,BSU_SHORTNAME +'-'+ ISNULL(LIBRARY_CODE,'') AS LIB_HEADER  from dbo.VIEW_LIBRARY_RECORDS " & _
                            " where STOCK_ID='" & id & "'"
            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds1.Tables(0).Rows.Count > 0 Then
                Dim info = ds1.Tables(0).Rows(0).Item("HEADER").ToString()
                Dim accession_no = ds1.Tables(0).Rows(0).Item("ACCESSION_NO").ToString()
                Dim call_no = ds1.Tables(0).Rows(0).Item("CALL_NO").ToString()
                Dim lib_header = ds1.Tables(0).Rows(0).Item("LIB_HEADER").ToString()
                ''Barcode
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                barcode.Number = accession_no
                barcode.ForeColor = Drawing.Color.Black
                barcode.ChecksumAdd = True
                barcode.NarrowBarWidth = 3
                barcode.Height = 300
                barcode.FontHeight = 0.15
                Dim b As Byte()
                ReDim b(barcode.Render(ImageType.Png).Length)
                b = barcode.Render(ImageType.Png)

                Dim dr As dsImageRpt.DSLIB_BARCODERow = dsSet.DSLIB_BARCODE.NewDSLIB_BARCODERow
                dr.HEADER = info
                dr.BARCODE = b
                dr.ACC_NO = accession_no
                dr.CALL_NO = call_no
                dr.LIB_HEADER = lib_header
                dsSet.DSLIB_BARCODE.Rows.Add(dr)

            End If

        End While

        repClassVal = New RepClass
        repClassVal.ResourceName = "Barcode.rpt"
        repClassVal.SetDataSource(dsSet)
        CrystalReportViewer1.ReportSource = repClassVal

        
        Try
            'For showing print dialog on page load
            Dim btnToClick = GetPrintButton(Me.Page)
            If btnToClick IsNot Nothing Then
                Dim btn As New System.Web.UI.WebControls.ImageButton
                h_print.Value = btnToClick.ClientID
            End If
            'For avoiding showing print dialog on eveyr page load
            Dim btnClicked As ImageButton = PrinterFunctions.GetPostBackControl(Me.Page)

            If btnClicked IsNot Nothing Then
                Dim btnClickstatus As String = btnClicked.CommandName
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                    h_print.Value = "completed"
                End If
            End If
        Catch ex As Exception

        Finally
            ' repClassVal.Dispose()

        End Try

    End Sub

    'Public Sub IssueCard(ByVal ds As DataSet, ByVal type As String)

    '    If type = "STUDENT" Then

    '        Dim photopath As String = WebConfigurationManager.AppSettings("StudentPhotoPath").ToString

    '        Dim dsSet As New dsImageRpt

    '        Dim i = 0
    '        For i = 0 To ds.Tables(0).Rows.Count - 1
    '            ''Barcode
    '            Dim barcode As BaseBarcode
    '            barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
    '            barcode.Number = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
    '            barcode.ChecksumAdd = True
    '            barcode.NarrowBarWidth = 3
    '            barcode.Height = 300
    '            barcode.FontHeight = 0.3F
    '            Dim b As Byte()
    '            ReDim b(barcode.Render(ImageType.Png).Length)
    '            b = barcode.Render(ImageType.Png)
    '            ''Student Image
    '            Dim stupath = photopath & "\" & ds.Tables(0).Rows(i).Item("STU_IMAGE").ToString()
    '            Dim SImage As Byte()

    '            If System.IO.File.Exists(stupath) = False Then
    '                stupath = photopath & "\NOIMG\no_image.jpg"
    '            End If


    '            Dim fs As New System.IO.FileStream(stupath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
    '            SImage = New Byte(fs.Length - 1) {}
    '            fs.Read(SImage, 0, Convert.ToInt32(fs.Length))
    '            fs.Close()

    '            Dim dr As dsImageRpt.DSSTUDENT_IDCARDRow = dsSet.DSSTUDENT_IDCARD.NewDSSTUDENT_IDCARDRow
    '            dr.STU_ID = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
    '            dr.STU_NO = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
    '            dr.STU_NAME = ds.Tables(0).Rows(i).Item("STU_NAME").ToString()
    '            dr.STU_PARENT_NAME = ds.Tables(0).Rows(i).Item("STU_PARENT_NAME").ToString()
    '            dr.STU_RESIDENCE_NO = ds.Tables(0).Rows(i).Item("STU_RESIDENCE_NO").ToString()
    '            dr.STU_OFFICE_NO = ds.Tables(0).Rows(i).Item("STU_OFFICE_NO").ToString()
    '            dr.STU_MOBILE_NO = ds.Tables(0).Rows(i).Item("STU_MOBILE_NO").ToString()
    '            dr.STU_EMERGENCY_NO = ds.Tables(0).Rows(i).Item("STU_EMERGENCY_NO").ToString()
    '            dr.STU_PICKUP_POINT = ds.Tables(0).Rows(i).Item("STU_PICKUP_POINT").ToString()
    '            dr.STU_BUS_NO = ds.Tables(0).Rows(i).Item("STU_BUS_NO").ToString()
    '            dr.STU_OWN_TRANSPORT = ds.Tables(0).Rows(i).Item("STU_OWN_TRANSPORT")
    '            dr.STU_BSU_NAME = ds.Tables(0).Rows(i).Item("STU_BSU_NAME").ToString()
    '            dr.STU_BSU_IMAGE = ds.Tables(0).Rows(i).Item("STU_BSU_IMAGE")
    '            dr.STU_DROPUP_POINT = ds.Tables(0).Rows(i).Item("STU_DROPUP_POINT").ToString()
    '            dr.STU_GRD_SEC = ds.Tables(0).Rows(i).Item("STU_GRD_SEC").ToString()
    '            dr.STU_IMAGE = SImage
    '            dr.STU_BARCODE = b
    '            dr.STU_SHOW_GRADE = ds.Tables(0).Rows(i).Item("STU_SHOW_GRADE").ToString()
    '            dr.STU_BSU_ADDRESS = ds.Tables(0).Rows(i).Item("STU_BSU_ADDRESS").ToString()
    '            dsSet.DSSTUDENT_IDCARD.Rows.Add(dr)

    '        Next

    '        repClassVal = New RepClass
    '        repClassVal.ResourceName = "Studentcard.rpt"
    '        repClassVal.SetDataSource(dsSet)
    '        CrystalReportViewer1.ReportSource = repClassVal

    '    End If

    '    Try
    '        'For showing print dialog on page load
    '        Dim btnToClick = GetPrintButton(Me.Page)
    '        If btnToClick IsNot Nothing Then
    '            Dim btn As New System.Web.UI.WebControls.ImageButton
    '            h_print.Value = btnToClick.ClientID
    '        End If
    '        'For avoiding showing print dialog on eveyr page load
    '        Dim btnClicked As ImageButton = PrinterFunctions.GetPostBackControl(Me.Page)

    '        If btnClicked IsNot Nothing Then
    '            Dim btnClickstatus As String = btnClicked.CommandName
    '            If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
    '                h_print.Value = "completed"
    '            End If
    '        End If
    '    Catch ex As Exception

    '    Finally
    '        ' repClassVal.Dispose()

    '    End Try

    'End Sub

    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        CrystalReportViewer1.Dispose()
        CrystalReportViewer1 = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()
        'Session("Barcodehashtable") = Nothing

    End Sub

End Class
