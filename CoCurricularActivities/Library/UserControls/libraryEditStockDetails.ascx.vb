Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Partial Class Library_UserControls_libraryEditStockDetails
    Inherits System.Web.UI.UserControl

 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenMasterId.Value = Request.QueryString("MasterId")
            HiddenBsuId.Value = Session("sbsuid")

            BindGrid()
        End If

        

    End Sub

    Public Sub BindFromTo(ByVal DropFrom As DropDownList, ByVal DropTo As DropDownList)
        Dim i
        For i = 0 To 70

            Dim list1 As New ListItem
            list1.Value = i + 5
            list1.Text = i + 5
            DropFrom.Items.Insert(i, list1)

            Dim list2 As New ListItem
            list2.Value = i + 5
            list2.Text = i + 5
            DropTo.Items.Insert(i, list2)

        Next

    End Sub

    Public Sub BindCurrency(ByVal DropCurrency As DropDownList)
        Dim str_conn = ConfigurationManager.ConnectionStrings("MainDB").ConnectionString
        Dim str_query = "SELECT CUR_ID,upper(CUR_DESCR)CUR_DESCR FROM CURRENCY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        DropCurrency.DataSource = ds
        DropCurrency.DataTextField = "CUR_DESCR"
        DropCurrency.DataValueField = "CUR_ID"
        DropCurrency.DataBind()

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT STOCK_ID,REPLACE(CONVERT(VARCHAR(11),PURCHASE_DATE, 106), ' ', '/') PURCHASE_DATE,SUPPLIER,PRODUCT_PRICE,PRICE_CURRENCY,FROM_AGE_GROUP,TO_AGE_GROUP,STATUS_DESCRIPTION, " & _
                        "CASE A.ACTIVE WHEN 'TRUE' THEN 'True' ELSE 'False' End ENABLE_ROW " & _
                        " FROM dbo.LIBRARY_ITEMS_QUANTITY A LEFT JOIN LIBRARY_ITEM_STATUS B ON A.STATUS_ID=B.STATUS_ID  WHERE MASTER_ID='" & HiddenMasterId.Value & "' AND PRODUCT_BSU_ID='" & HiddenBsuId.Value & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridItem.DataSource = ds
        GridItem.DataBind()
        For Each row As GridViewRow In GridItem.Rows

            Dim ddcurrency As DropDownList = DirectCast(row.FindControl("ddcurrency"), DropDownList)
            Dim DDFromAge As DropDownList = DirectCast(row.FindControl("DDFromAge"), DropDownList)
            Dim DDToAge As DropDownList = DirectCast(row.FindControl("DDToAge"), DropDownList)
            BindCurrency(ddcurrency)
            BindFromTo(DDFromAge, DDToAge)

            Dim currency = DirectCast(row.FindControl("HiddenCurrency"), HiddenField).Value
            Dim AgeFrom = DirectCast(row.FindControl("HiddenAgeFrom"), HiddenField).Value
            Dim AgeTo = DirectCast(row.FindControl("HiddenAgeTo"), HiddenField).Value
            ddcurrency.SelectedValue = currency
            DDFromAge.SelectedValue = AgeFrom
            DDToAge.SelectedValue = AgeTo

        Next

      

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim LibraryMessage = ""

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            For Each row As GridViewRow In GridItem.Rows

                Dim ddcurrency As DropDownList = DirectCast(row.FindControl("ddcurrency"), DropDownList)
                Dim DDFromAge As DropDownList = DirectCast(row.FindControl("DDFromAge"), DropDownList)
                Dim DDToAge As DropDownList = DirectCast(row.FindControl("DDToAge"), DropDownList)
                Dim txtpurchasedate As TextBox = DirectCast(row.FindControl("txtpurchasedate"), TextBox)
                Dim txtsupplier As TextBox = DirectCast(row.FindControl("txtsupplier"), TextBox)
                Dim txtcost As TextBox = DirectCast(row.FindControl("txtcost"), TextBox)

                Dim HiddenStockId = DirectCast(row.FindControl("HiddenStockId"), HiddenField).Value

                Dim HiddenActive = DirectCast(row.FindControl("HiddenActive"), HiddenField).Value

                If txtcost.Text.Trim() = "" Then
                    txtcost.Text = 0
                End If
                If HiddenActive = "True" Then
                    Dim pParms(7) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@PRODUCT_PRICE", txtcost.Text.Trim())
                    pParms(1) = New SqlClient.SqlParameter("@PRICE_CURRENCY", ddcurrency.SelectedValue)
                    pParms(2) = New SqlClient.SqlParameter("@FROM_AGE_GROUP", DDFromAge.SelectedValue)
                    pParms(3) = New SqlClient.SqlParameter("@TO_AGE_GROUP", DDToAge.SelectedValue)
                    pParms(4) = New SqlClient.SqlParameter("@SUPPLIER", txtsupplier.Text.Trim())
                    If txtpurchasedate.Text.Trim() <> "" Then
                        pParms(5) = New SqlClient.SqlParameter("@PURCHASE_DATE", Convert.ToDateTime(txtpurchasedate.Text.Trim()))
                    End If
                    pParms(6) = New SqlClient.SqlParameter("@STOCK_ID", HiddenStockId)
                    LibraryMessage = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEMS_QUANTITY_DETAILS", pParms)
                End If

            Next

            transaction.Commit()

        Catch ex As Exception

            transaction.Rollback()
            LibraryMessage = "Error occured while saving . " & ex.Message

        Finally

            connection.Close()

        End Try

        lblMessage.Text = LibraryMessage
        MO11.Show()

        BindGrid()



    End Sub

   
    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
