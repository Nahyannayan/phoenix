Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryMembershipTypes
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindLibraryDivisions()
            BindGrid()
        End If

    End Sub

    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivisions.DataSource = ds
        ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivisions.DataBind()

    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT MEMBERSHIP_ID,A.LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES,MEMBERSHIP_DES,USER_TYPE,MAX_ITEMS,LENDING_DAYS " & _
                        " FROM dbo.LIBRARY_MEMBERSHIPS A " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                        " WHERE B.LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridMemberships.DataSource = ds
        GridMemberships.DataBind()


    End Sub



    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        LibraryData.isOffLine(Session("sBusper"))

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MEMBERSHIP_DES", txtMembership.Text.Trim().ToUpper())
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddLibraryDivisions.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@USER_TYPE", ddusertype.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@MAX_ITEMS", txtmaxitems.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@LENDING_DAYS", txtlendingperiods.Text.Trim())
            pParms(5) = New SqlClient.SqlParameter("@OPTIONS", 1)
            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_MEMBERSHIPS", pParms)
            transaction.Commit()
            txtMembership.Text = ""
            txtmaxitems.Text = ""
            txtlendingperiods.Text = ""
            BindGrid()
        Catch ex As Exception

            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub




    Protected Sub GridMemberships_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridMemberships.PageIndexChanging
        lblMessage.Text = ""
        GridMemberships.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridMemberships_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridMemberships.RowCancelingEdit
        lblMessage.Text = ""
        GridMemberships.EditIndex = "-1"
        BindGrid()
    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberships.RowCommand

        If e.CommandName = "deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MEMBERSHIP_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)

                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_MEMBERSHIPS", pParms)
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions." & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If

    End Sub

    Protected Sub GridMemberships_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridMemberships.RowEditing
        lblMessage.Text = ""
        GridMemberships.EditIndex = e.NewEditIndex
        BindGrid()
    End Sub

    Protected Sub GridMemberships_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridMemberships.RowUpdating
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim HiddenMembershipId = DirectCast(GridMemberships.Rows(e.RowIndex).FindControl("HiddenMembershipId"), HiddenField).Value
            Dim HiddenLdivisionId = DirectCast(GridMemberships.Rows(e.RowIndex).FindControl("HiddenLdivisionId"), HiddenField).Value
            Dim txtMembershipE = DirectCast(GridMemberships.Rows(e.RowIndex).FindControl("txtMembershipE"), TextBox).Text.Trim().ToUpper()
            Dim txtmaxitemsE = DirectCast(GridMemberships.Rows(e.RowIndex).FindControl("txtmaxitemsE"), TextBox).Text.Trim()
            Dim txtlendingperiodsE = DirectCast(GridMemberships.Rows(e.RowIndex).FindControl("txtlendingperiodsE"), TextBox).Text.Trim()
        

            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MEMBERSHIP_ID", HiddenMembershipId)
            pParms(1) = New SqlClient.SqlParameter("@MEMBERSHIP_DES", txtMembershipE)
            pParms(2) = New SqlClient.SqlParameter("@MAX_ITEMS", txtmaxitemsE)
            pParms(3) = New SqlClient.SqlParameter("@LENDING_DAYS", txtlendingperiodsE)
            pParms(4) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", HiddenLdivisionId)
            pParms(5) = New SqlClient.SqlParameter("@OPTIONS", 2)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_MEMBERSHIPS", pParms)
            transaction.Commit()
            GridMemberships.EditIndex = "-1"
            BindGrid()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

    End Sub
End Class
