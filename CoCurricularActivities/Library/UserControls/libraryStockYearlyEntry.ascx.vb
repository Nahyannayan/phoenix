﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryStockYearlyEntry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenstocktakeid.Value = Request.QueryString("stkid")
            Hiddenlibrarydivid.Value = Request.QueryString("libid")
            BindGrid()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If
    End Sub

    Protected Sub txtaccessionnumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtaccessionnumber.TextChanged
        TakeStock()
    End Sub


    Public Sub TakeStock()

        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STOCK_TAKE_ID", Hiddenstocktakeid.Value)
            pParms(1) = New SqlClient.SqlParameter("@ACCESSION_NO", txtaccessionnumber.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", Hiddenlibrarydivid.Value)
            pParms(3) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(4) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", Session("sbsuid"))
            pParms(5) = New SqlClient.SqlParameter("@OPTION", 3)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_UPDATE_LIBRARY_STOCK_TAKE_LIST", pParms)
            transaction.Commit()
            BindGrid()
            txtaccessionnumber.Text = ""

        Catch ex As Exception
            transaction.Rollback()
        Finally
            connection.Close()

        End Try

    End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "INSERT_UPDATE_LIBRARY_STOCK_TAKE_LIST"
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STOCK_TAKE_ID", Hiddenstocktakeid.Value)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 4)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, pParms)
        GridData.DataSource = ds
        GridData.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            btnclose.Visible = True
        Else
            btnclose.Visible = False
        End If

    End Sub


    Protected Sub GridData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridData.PageIndexChanging
        GridData.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub



    Protected Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STOCK_TAKE_ID", Hiddenstocktakeid.Value)
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", Hiddenlibrarydivid.Value)
            pParms(2) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Session("EmployeeId"))
            pParms(3) = New SqlClient.SqlParameter("@OPTION", 5)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_UPDATE_LIBRARY_STOCK_TAKE_LIST", pParms)
            transaction.Commit()
            txtaccessionnumber.Visible = False
            btnclose.Visible = False

        Catch ex As Exception
            transaction.Rollback()
        Finally
            connection.Close()

        End Try
    End Sub



End Class
