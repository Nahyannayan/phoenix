﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryShelfTeachers.ascx.vb" Inherits="Library_UserControls_libraryShelfTeachers" %>

<script type="text/javascript">
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }

</script>
<table width="100%">
    <tr>
        <td><span class="field-label">Library Shelf for Teachers </span></td>
        <td>
            <asp:DropDownList ID="dddes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddesschange">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="left" colspan="2">
            <asp:GridView ID="GrdStaff" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                PageSize="15" ShowFooter="True" Width="100%">
                <Columns>

                    <asp:TemplateField HeaderText="Employee Number">
                        <HeaderTemplate>
                            Employee Number
                                <br />
                            <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>  <%#Eval("EMPNO")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Name">
                        <HeaderTemplate>
                            Name
                                <br />
                            <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageSearch" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("ENAME") %>
                            <asp:HiddenField ID="Hiddenempid" runat="server" Value='<%# Eval("EMP_ID") %>' />
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation">
                        <HeaderTemplate>
                            Designation
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenDesId" runat="server" Value='<%# Eval("EMP_DES_ID") %>' />
                            <center><%# Eval("DES_DESCR") %></center>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField>
                        <FooterTemplate>
                            <center>
                                <asp:Button ID="btnaddmembers" runat="server" CommandName="assign" CssClass="button"
                                    Text="Assign"   /></center>

                        </FooterTemplate>
                        <ItemTemplate>
                            <center>
                                <br /> 
                            <asp:GridView ID="GridShelfs" runat="server" Width="100%" cssclass="table  table-bordered table-row" AutoGenerateColumns="False" OnRowCommand="GridShelfs_RowCommand" ShowHeader="False">
                            <Columns>
                            <asp:TemplateField>
                            <ItemTemplate>
                            <asp:HiddenField ID="HiddenRecordId" Value='<%#Eval("ASSIGN_ID")%>' runat="server" />
                            <%#Eval("LIBRARY_DIVISION_DES")%> > 
                            <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                            <ItemTemplate>
                            <%#Eval("SHELF_NAME")%>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                            <ItemTemplate>
                            <asp:LinkButton ID="LinkDelete" CommandName="Deleting" CommandArgument='<%#Eval("ASSIGN_ID")%>' runat="server">Delete</asp:LinkButton>
                            <ajaxToolkit:ConfirmButtonExtender ID="CF1" TargetControlID="LinkDelete" ConfirmText="Do you want to delete this record?.If you do so, then the teacher will no longer have any rights on this shelf. Do you wish to continue?" runat="server"></ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            </Columns>
                            </asp:GridView>
                                <asp:CheckBox ID="Checklist" runat="server" />
                                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:Label ID="Label1" runat="server"></asp:Label>
<asp:Panel ID="PanelMessage" runat="server" BackColor="white" CssClass="modalPopup"
    Style="display: none">
    <table width="100%">
        <tr>
            <td class="title-bg">Library Message</td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table>
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnmok" runat="server" Text="Ok" CssClass="button" ValidationGroup="s" CausesValidation="False"></asp:Button></td>
                                </tr>
                            </tbody>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    DropShadow="true" PopupControlID="PanelMessage" RepositionMode="RepositionOnWindowResizeAndScroll"
    TargetControlID="Label1">
</ajaxToolkit:ModalPopupExtender>
<asp:Label ID="Label3" runat="server"></asp:Label>
<asp:Panel ID="PanelStatusUpdate" runat="server" BackColor="white" CssClass="modalPopup"
    Style="display: none">
    <table width="100%">
        <tr>
            <td class="title-bg">Library Message-Shelfs</td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td align="center">
                                    <table>
                                        <tr>
                                            <td align="left"><span class="field-label">Library Divisions</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddLibraryDivisions_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Library Sub Divisions</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddLibrarySubDivisions" runat="server" AutoPostBack="True" Width="100%"
                                                    OnSelectedIndexChanged="ddLibrarySubDivisions_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Shelfs</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddShelfName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnSave" runat="server" CausesValidation="False" CssClass="button"
                                        Text="Assign" />
                                    <asp:Button ID="btncancel4" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Cancel" /></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>


<ajaxToolkit:ModalPopupExtender ID="MO3" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="btncancel4" DropShadow="true" Enabled="True" PopupControlID="PanelStatusUpdate"
    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label3">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
