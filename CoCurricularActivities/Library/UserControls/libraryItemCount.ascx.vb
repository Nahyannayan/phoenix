﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryItemCount
    Inherits System.Web.UI.UserControl

    Private LIBRARY_DIVISION_ID_VAL As String
    Private MASTER_ID_VAL As String

    Public Property LIBRARY_DIVISION_ID() As String
        Get
            Return LIBRARY_DIVISION_ID_VAL
        End Get
        Set(ByVal value As String)
            LIBRARY_DIVISION_ID_VAL = value
        End Set
    End Property


    Public Property MASTER_ID() As String
        Get
            Return MASTER_ID_VAL
        End Get
        Set(ByVal value As String)
            MASTER_ID_VAL = value
            Count()
        End Set
    End Property

    Public Sub Count()



        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        ''Total Active Item Count 
        Dim Sql_Query = " SELECT  COUNT(*)  FROM dbo.LIBRARY_ITEMS_QUANTITY AS ACC INNER JOIN " & _
                        " dbo.LIBRARY_RACKS AS BCC ON ACC.RACK_ID = BCC.RACK_ID INNER JOIN " & _
                        " dbo.LIBRARY_SHELFS AS CCC ON BCC.SHELF_ID = CCC.SHELF_ID INNER JOIN " & _
                        " dbo.LIBRARY_SUB_DIVISIONS AS DCC ON CCC.LIBRARY_SUB_DIVISION_ID = DCC.LIBRARY_SUB_DIVISION_ID INNER JOIN " & _
                        " dbo.LIBRARY_DIVISIONS AS ECC ON DCC.LIBRARY_DIVISION_ID = ECC.LIBRARY_DIVISION_ID " & _
                        " WHERE CONVERT(VARCHAR,ECC.LIBRARY_DIVISION_ID) = '" & LIBRARY_DIVISION_ID_VAL & "' AND ACC.MASTER_ID = '" & MASTER_ID_VAL & "' AND ACC.ACTIVE = 'True' "

        Dim libActiveCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        lblStatus.Text = libActiveCount

        ''Reserved Count
        Sql_Query = " SELECT  COUNT(*) FROM dbo.LIBRARY_ITEM_RESERVATION AS CDD " & _
                    " WHERE      (MASTER_ID = '" & MASTER_ID_VAL & "')AND RESERVE_START_DATE IS NULL AND  RESERVE_END_DATE IS NULL AND " & _
                    " (ISNULL(TRAN_RECORD_ID, '') = '') AND (LIBRARY_DIVISION_ID = '" & LIBRARY_DIVISION_ID_VAL & "') "
        Dim libReserveCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        lblStatus.Text &= " / " & libReserveCount

        ''Total Issued
        Sql_Query = " SELECT COUNT(*) FROM LIBRARY_TRANSACTIONS D1 " & _
                    " INNER JOIN dbo.LIBRARY_ITEMS_QUANTITY M1 ON M1.STOCK_ID=D1.STOCK_ID " & _
                    " WHERE M1.MASTER_ID= '" & MASTER_ID_VAL & "' " & _
                    " AND isnull(ITEM_ACTUAL_RETURN_DATE,'') = '' AND D1.LIBRARY_DIVISION_ID='" & LIBRARY_DIVISION_ID_VAL & "'"

        Dim libIssuedCount = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        lblStatus.Text &= " / " & libIssuedCount


        ''Total Available
        lblStatus.Text &= " / " & (libActiveCount - libIssuedCount)


        If libActiveCount = 0 And libReserveCount = 0 Then
            lblStatus.Text = ""
        End If



    End Sub

End Class
