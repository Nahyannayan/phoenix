﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
'Imports System.Data.OleDb
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Library_UserControls_libraryUploadExcelData
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HiddenBsuid.Value = Session("sbsuid")
            Hiddenempid.Value = Session("EmployeeId")

        End If

    End Sub

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click
        LibraryData.isOffLine(Session("sBusper"))
        Try

            If FileUpload1.HasFile Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
                Dim Sql_Query = ""
                Dim myDataset As New DataSet()
                Dim strConnE As String = ""
                Dim mySelectStatement As String = ""
                Dim filenam As String = FileUpload1.PostedFile.FileName

                Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                Dim filename = FileUpload1.FileName.ToString()
                Dim savepath = path + "NewsLettersEmails/" + Session("EmployeeId") + "_LIBRARY_DATA_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + "." + GetExtension(filename)
                FileUpload1.SaveAs(savepath)


                Try

                    SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                    Dim mObj As ExcelRowCollection
                    Dim iRowRead As Boolean
                    Dim ef As ExcelFile = New ExcelFile
                    Dim mRowObj As ExcelRow
                    Dim iRow As Integer = 1
                    Dim dataTable As New DataTable
                    Dim dr As DataRow
                    Dim lstrData As String
                    iRowRead = True




                    dataTable.Columns.Add("ISBN", GetType(String))
                    dataTable.Columns.Add("ISBN2", GetType(String))
                    dataTable.Columns.Add("COPIES", GetType(String))
                    dataTable.Columns.Add("CATEGORY", GetType(String))
                    dataTable.Columns.Add("ACCESSION_NO", GetType(String))
                    dataTable.Columns.Add("CALL_NO", GetType(String))
                    dataTable.Columns.Add("AGE_GROUP_FROM", GetType(String))
                    dataTable.Columns.Add("AGE_GROUP_TO", GetType(String))
                    dataTable.Columns.Add("SUPPLIER", GetType(String))
                    dataTable.Columns.Add("PURCHASE_DATE", GetType(String))
                    dataTable.Columns.Add("PRODUCT_PRICE_AED", GetType(String))

                    ef.LoadXls(savepath)
                    mObj = ef.Worksheets(0).Rows
                    While iRowRead
                        mRowObj = mObj(iRow)
                        If Convert.ToString(mRowObj.Cells(0).Value) Is Nothing Then
                            Exit While
                        End If
                        If Convert.ToString(mRowObj.Cells(0).Value) = "" Then
                            Exit While
                        End If

                        If Not Convert.ToString(mRowObj.Cells(0).Value) Is Nothing Then
                            dr = dataTable.NewRow
                            dr.Item(0) = Convert.ToString(mRowObj.Cells(0).Value)
                            dr.Item(1) = Convert.ToString(mRowObj.Cells(1).Value)
                            dr.Item(2) = Convert.ToString(mRowObj.Cells(2).Value)
                            dr.Item(3) = Convert.ToString(mRowObj.Cells(3).Value)
                            dr.Item(4) = Convert.ToString(mRowObj.Cells(4).Value)
                            dr.Item(5) = Convert.ToString(mRowObj.Cells(5).Value)
                            dr.Item(6) = Convert.ToString(mRowObj.Cells(6).Value)
                            dr.Item(7) = Convert.ToString(mRowObj.Cells(7).Value)
                            dr.Item(8) = Convert.ToString(mRowObj.Cells(8).Value)
                            dr.Item(9) = Convert.ToString(mRowObj.Cells(9).Value)
                            dr.Item(10) = Convert.ToString(mRowObj.Cells(10).Value)

                            dataTable.Rows.Add(dr)
                        End If
                        iRow += 1
                    End While

                    myDataset.Tables.Add(dataTable)

                    'strConnE = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & savepath & "; Extended Properties=""Excel 12.0;HDR=YES;"""
                    ''Dim conn As New OleDbConnection(strConnE)
                    ''Dim myData As New OleDbDataAdapter
                    ''myData.SelectCommand = New OleDbCommand("SELECT ISBN,ISBN2,COPIES,CATEGORY,ACCESSION_NO,CALL_NO,AGE_GROUP_FROM,AGE_GROUP_TO,SUPPLIER,PURCHASE_DATE,PRODUCT_PRICE_AED FROM [Sheet1$] ", conn)


                    ''myData.TableMappings.Add("Table", "ExcelTest")
                    ''myData.Fill(myDataset)
                    'mySelectStatement = "SELECT ISBN,ISBN2,COPIES,CATEGORY,ACCESSION_NO,CALL_NO,AGE_GROUP_FROM,AGE_GROUP_TO,SUPPLIER,PURCHASE_DATE,PRODUCT_PRICE_AED FROM [Sheet1$] "
                    'Using myConnection As New OleDbConnection(strConnE)
                    '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                    '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                    '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                    '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                    '        myAdapter.Fill(myDataset)
                    '    End Using
                    'End Using


                Catch ex As Exception
                    'strConnE = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & savepath & ";Extended Properties=""Excel 8.0;"""
                    ''Dim conn As New OleDbConnection(strConnE)
                    ''Dim myData As New OleDbDataAdapter
                    ''myData.SelectCommand = New OleDbCommand("SELECT ISBN,ISBN2,COPIES,CATEGORY,ACCESSION_NO,CALL_NO,AGE_GROUP_FROM,AGE_GROUP_TO,SUPPLIER,PURCHASE_DATE,PRODUCT_PRICE_AED FROM [Sheet1$] ", conn)


                    ''myData.TableMappings.Add("Table", "ExcelTest")
                    ''myData.Fill(myDataset)
                    'mySelectStatement = "SELECT ISBN,ISBN2,COPIES,CATEGORY,ACCESSION_NO,CALL_NO,AGE_GROUP_FROM,AGE_GROUP_TO,SUPPLIER,PURCHASE_DATE,PRODUCT_PRICE_AED FROM [Sheet1$] "
                    'Using myConnection As New OleDbConnection(strConnE)
                    '    Using myCmd As New OleDbCommand(mySelectStatement, myConnection)
                    '        Dim myAdapter As New OleDbDataAdapter(myCmd)
                    '        myCmd.Parameters.AddWithValue("@FieldOne", mySelectStatement)
                    '        myAdapter.TableMappings.Add("Table", "ExcelTest")
                    '        myAdapter.Fill(myDataset)
                    '    End Using
                    'End Using

                End Try

                '' Check the data count before the upload to server.
                If CheckExcel.Checked Then

                    lblmessage.Text = "Total Items in excel :" & myDataset.Tables(0).Rows.Count & ". If the excel data count is correct,Uncheck the checkbox and upload again."

                Else
                    '' Continue to upload to server.
                    Dim i = 0
                    Dim successupload = 0
                    Dim erroruplaod = 0
                    Dim exists = 0
                    Dim notexists = 0
                    Dim attempt = 0

                    Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
                    Dim transaction As SqlTransaction
                    connection.Open()
                    transaction = connection.BeginTransaction()

                    Try
                        '' Check Attempt:
                        Dim attquery As String = "SELECT ISNULL(MAX(UPLOAD_ATTEMPT),0) +1 FROM dbo.LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE ENTRY_BSU_ID='" & HiddenBsuid.Value & "'"
                        attempt = SqlHelper.ExecuteScalar(transaction, CommandType.Text, attquery)

                        For i = 0 To myDataset.Tables(0).Rows.Count - 1

                            Dim ISBN As String = myDataset.Tables(0).Rows(i).Item("ISBN").ToString().Replace("'", "").Replace("-", "").Replace(" ", "").Replace(".", "").Trim()
                            Dim ISBN2 As String = myDataset.Tables(0).Rows(i).Item("ISBN2").ToString().Replace("'", "").Replace("-", "").Replace(" ", "").Replace(".", "").Trim()
                            Dim Stock As String = myDataset.Tables(0).Rows(i).Item("COPIES").ToString().Trim()
                            Dim age_from As String = myDataset.Tables(0).Rows(i).Item("AGE_GROUP_FROM").ToString().Trim()
                            Dim age_to As String = myDataset.Tables(0).Rows(i).Item("AGE_GROUP_TO").ToString().Trim()
                            Dim supplier As String = myDataset.Tables(0).Rows(i).Item("SUPPLIER").ToString().Trim()
                            Dim purchase_date As String = myDataset.Tables(0).Rows(i).Item("PURCHASE_DATE").ToString().Trim()
                            Dim product_price As String = myDataset.Tables(0).Rows(i).Item("PRODUCT_PRICE_AED").ToString().Trim()
                            Dim accession_no As String = myDataset.Tables(0).Rows(i).Item("ACCESSION_NO").ToString().Trim()
                            Dim Call_No As String = myDataset.Tables(0).Rows(i).Item("CALL_NO").ToString().Trim()
                            Dim Category As String = myDataset.Tables(0).Rows(i).Item("CATEGORY").ToString().Trim()

                            If Stock = "" Or Not IsNumeric(Stock) Then
                                Stock = 0
                            End If

                            If accession_no <> "" Then
                                Stock = 1
                            End If

                            If age_from = "" Or Not IsNumeric(age_from) Then
                                age_from = 5
                            Else
                                If age_from < 5 Then
                                    age_from = 5
                                End If
                            End If

                            If age_to = "" Or Not IsNumeric(age_to) Then
                                age_to = 75
                            Else
                                If age_to > 75 Then
                                    age_to = 75
                                End If
                            End If

                            If supplier = "" Then
                                supplier = "" 'Unknown
                            End If

                            If purchase_date = "" Then
                                purchase_date = "" 'DateTime.Now.ToString("dd/MMM/yyyy")
                            Else
                                If Not IsDate(purchase_date) Then
                                    purchase_date = "" 'DateTime.Now.ToString("dd/MMM/yyyy")
                                End If

                            End If

                            If product_price = "" Or Not IsNumeric(product_price) Then
                                product_price = 0
                            End If

                            If Category = "" Or Not IsNumeric(Category) Then
                                Category = 1
                            End If

                            If ISBN.Trim() = "" Then
                                ISBN = ISBN2
                            End If


                            If CheckPreviousEntry(ISBN, transaction, Convert.ToInt16(Stock)) = False Then

                                Dim pParms(15) As SqlClient.SqlParameter
                                pParms(0) = New SqlClient.SqlParameter("@UPLOAD_ISBN", ISBN) ''Book
                                pParms(1) = New SqlClient.SqlParameter("@UPLOAD_ISBN2", ISBN2) ''Book
                                pParms(2) = New SqlClient.SqlParameter("@ENTRY_EMP_ID", Hiddenempid.Value)
                                pParms(3) = New SqlClient.SqlParameter("@ENTRY_BSU_ID", HiddenBsuid.Value)
                                pParms(4) = New SqlClient.SqlParameter("@TOTAL_STOCK", Stock)
                                pParms(5) = New SqlClient.SqlParameter("@FROM_AGE_GROUP", age_from)
                                pParms(6) = New SqlClient.SqlParameter("@TO_AGE_GROUP", age_to)
                                pParms(7) = New SqlClient.SqlParameter("@SUPPLIER", supplier)
                                pParms(8) = New SqlClient.SqlParameter("@PURCHASE_DATE", purchase_date)
                                pParms(9) = New SqlClient.SqlParameter("@PRODUCT_PRICE", product_price)
                                pParms(10) = New SqlClient.SqlParameter("@PRICE_CURRENCY", "AED")

                                If accession_no <> "" Then
                                    pParms(11) = New SqlClient.SqlParameter("@ACCESSION_NO", accession_no)
                                End If

                                If Call_No <> "" Then
                                    pParms(12) = New SqlClient.SqlParameter("@CALL_NO", Call_No)
                                End If

                                pParms(13) = New SqlClient.SqlParameter("@ITEM_ID", Category)
                                pParms(14) = New SqlClient.SqlParameter("@UPLOAD_ATTEMPT", attempt)


                                Dim RECORD_ID = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_ITEMS_MASTER_EXCEL", pParms)

                                If (ISBN.Length = 13 Or ISBN.Length = 10) Then
                                    successupload = successupload + 1
                                    If CheckItemExists(ISBN, transaction, RECORD_ID) Then
                                        exists = exists + 1
                                    Else
                                        notexists = notexists + 1
                                    End If
                                Else
                                    erroruplaod = erroruplaod + 1
                                    Sql_Query = "Update LIBRARY_ITEMS_MASTER_EXCEL set ERROR_UPLOAD='True' , ERROR_UPLOAD_MESSAGE='Please check the ISBN. ISBN Number must be 13 or 10 digit character.' WHERE RECORD_ID='" & RECORD_ID & "' "
                                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, Sql_Query)

                                End If
                            Else
                                If CheckAutoStockUpdate.Checked Then
                                    successupload = successupload + 1
                                End If
                            End If

                        Next
                        transaction.Commit()
                        lblmessage.Text = "Excel uploaded successfully"
                        Label1.Text = myDataset.Tables(0).Rows.Count
                        Label2.Text = successupload
                        Label3.Text = erroruplaod
                        Label4.Text = exists
                        Label5.Text = notexists
                        Panel1.Visible = False
                        Panel2.Visible = True
                    Catch ex As Exception

                        transaction.Rollback()
                        lblmessage.Text = "Error occured while saving . " & ex.Message

                    Finally

                        connection.Close()

                    End Try

                End If

                ''Delete the Excel File
                If System.IO.File.Exists(savepath) Then
                    System.IO.File.Delete(savepath)
                End If

            End If

        Catch ex As Exception

            lblmessage.Text = "Error : " & ex.Message
        End Try


    End Sub

    Public Function CheckPreviousEntry(ByVal ISBN As String, ByVal transaction As SqlTransaction, ByVal stock As Integer) As Boolean
        Dim returnval = False
        'Dim Sql_Query = "Select UPLOAD_ISBN from LIBRARY_ITEMS_MASTER_EXCEL where UPLOAD_ISBN like '%" & ISBN & "%'"
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, Sql_Query)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    returnval = True
        '    '' Update the stock
        '    If CheckAutoStockUpdate.Checked Then
        '        Sql_Query = "Update LIBRARY_ITEMS_MASTER_EXCEL set TOTAL_STOCK = (TOTAL_STOCK + " & stock & " ) WHERE UPLOAD_ISBN='" & ISBN & "' AND ERROR_UPLOAD='False' AND SUCCESS_DATA_RETRIEVE='False' and ERROR_DATA_RETRIEVE='False' AND APPROVED='False'"
        '        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, Sql_Query)
        '    End If
        'End If

        Return returnval
    End Function

    Public Function CheckItemExists(ByVal ISBN As String, ByVal transaction As SqlTransaction, ByVal RECORD_ID As String) As Boolean
        Dim returnval = False

        Dim Sql_Query = "Select MASTER_ID from LIBRARY_ITEMS_MASTER where ISBN like '%" & ISBN & "%'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then
            returnval = True
            Dim masterid = ds.Tables(0).Rows(0).Item("MASTER_ID").ToString()
            Sql_Query = "Update LIBRARY_ITEMS_MASTER_EXCEL set PRIMARY_MASTER_ID='" & masterid & "' WHERE RECORD_ID='" & RECORD_ID & "' "
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, Sql_Query)
        End If

        Return returnval
    End Function
    Private Function GetExtension(ByVal FileName As String) As String
        Dim split As String() = FileName.Split(".")
        Dim Extension As String = split(split.Length - 1)
        Return Extension
    End Function

    Protected Sub LinkSample_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkSample.Click
        Try

            Dim path = WebConfigurationManager.AppSettings("UploadExcelFile").ToString() & "LibraryUpload.xls"

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()


        Catch ex As Exception

        End Try
    End Sub
End Class
