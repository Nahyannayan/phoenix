<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryDetailViewUserTransactions.ascx.vb" Inherits="Library_UserControls_libraryDetailViewUserTransactions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
   
<style type="text/css">
    .book-img {
        max-width:80px;
    }
</style>

<table cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg">
            User Transactions</td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GrdUserTransaction" runat="server" AllowPaging="True"  EmptyDataText="No transactions done yet." AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="Stock ID">
                        <HeaderTemplate>
                           Accession&nbsp;No                                                                           
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("STOCK_ID")%>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Image">
                        <HeaderTemplate> 
                               Item&nbsp;Image                                                                                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:Image ID="Image4" cssclass="book-img" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Name">
                        <HeaderTemplate>
                            Item&nbsp;Name                                                                                                       
                        </HeaderTemplate>
                        <ItemTemplate>
                          
                            <%#Eval("ITEM_TITLE")%>
                           
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <HeaderTemplate>
                           Status
                        </HeaderTemplate>
                        <ItemTemplate>
                            Issue Status 
                            <%#Eval("ISSUE_STATUS") %>
                            <br />
                            <hr />
                            Return Status 
                            <%#Eval("RETURN_STATUS") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Issue Period">
                        <HeaderTemplate>
                              Issue&nbsp;Period                                                                         
                        </HeaderTemplate>
                        <ItemTemplate>
                            Issue On <%#Eval("ITEM_TAKEN_DATE")%>
                            <br />
                            <hr />
                            Return Date <%#Eval("ITEM_RETURN_DATE")%>
                            <br />
                            <hr />
                            Returned Date <%#Eval("ITEM_ACTUAL_RETURN_DATE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reservation">
                        <HeaderTemplate>                           
                              Reservation
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:Image ID="Image3" runat="server" ImageUrl='<%#Eval("RESERVATION")%>' />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Issue Notes">
                        <HeaderTemplate>                            
                          Issue&nbsp;Notes                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("NOTES") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Return Notes">
                        <HeaderTemplate>                            
                              Return&nbsp;Notes                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("RETURN_NOTES") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fine">
                        <HeaderTemplate>
                               Fine                                
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("FINE_AMOUNT") %>
                                <%#Eval("CURRENCY_ID") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Issued By">
                        <HeaderTemplate>
                             Issued&nbsp;By                                                                         
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("ISSUED_BY") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Collected By">
                        <HeaderTemplate>                           
                             Collected&nbsp;By
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("RECEIVED_BY") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
               <%-- <SelectedRowStyle CssClass="Green" Wrap="False" />--%>
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
              <%--  <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenUserID" runat="server" />
<asp:HiddenField ID="HiddenUserType" runat="server" />

