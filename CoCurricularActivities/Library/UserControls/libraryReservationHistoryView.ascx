<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryReservationHistoryView.ascx.vb" Inherits="Library_UserControls_libraryReservationHistoryView" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script type="text/javascript">


    function UserTransactions(usertype, userid) {

        window.open('libraryDetailViewUserTransactions.aspx?UserType=' + usertype + '&UserId=' + userid, '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
        return false;

    }


</script>

<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">Library Reservation History</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GrdReservation" runat="server" AllowPaging="True" EmptyDataText="No Reservation done yet." AutoGenerateColumns="False"
                            OnRowCommand="GrdReservation_RowCommand" CssClass="table table-bordered table-row"
                            Width="100%">
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <Columns>
                                <asp:TemplateField HeaderText="Reservation Date">
                                    <HeaderTemplate>
                                        Contact
                                        <br />
                                        <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch4" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <asp:Image ID="Image3" runat="server" ImageUrl='<%#Eval("RESERVATION_CANCEL")%>' />
                                <asp:Panel ID="Show" runat="server" class="panel-cover"  Height="150px"
                                    Width="200px">
                                    <br />
                                    <%#Eval("CONTACT_ADDRESS")%>
                                    <br />
                                    <%#Eval("RESERVE_START_DATE")%>
                                    <br />
                                    <%#Eval("RESERVE_END_DATE")%>
                                </asp:Panel>
                                <ajaxToolkit:HoverMenuExtender ID="hm2" runat="server" PopupControlID="Show" PopupPosition="Left"
                                    TargetControlID="Image3">
                                </ajaxToolkit:HoverMenuExtender>
                                <br />
                                <%#Eval("CANCEL_RESERVATION")%>
                               
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="  User ID">
                                    <HeaderTemplate>
                                        User ID
                                        <br />
                                        <asp:TextBox ID="txtnumber" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch1" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("USER_ID")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Type">
                                    <HeaderTemplate>
                                        User Type
                                        <br />
                                        <asp:TextBox ID="txtType" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch3" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                <%#Eval("USER_TYPE")%>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name">
                                    <HeaderTemplate>
                                        User Name
                                        <br />
                                        <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageSearch2" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkReservationuser" runat="server" OnClientClick='<%#Eval("USERREDIRECT")%>'><%#Eval("USER_NAME")%></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Date
                                    <br />
                                        <br />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                       <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                    </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                            </Columns>
                            <EditRowStyle />
                            <EmptyDataRowStyle />
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HiddenMasterId" runat="server" />
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>
