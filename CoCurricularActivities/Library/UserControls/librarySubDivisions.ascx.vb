Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_librarySubDivisions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindFromTo()
            BindGrid()
            BindLibraryDivisions(ddLibraryDivisions, "")

        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)


    End Sub


    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList, ByVal SelectVal As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()

            If SelectVal <> "" Then
                ddLibraryDivisions.SelectedValue = SelectVal
            End If

        End If


    End Sub


    Public Sub BindFromTo()
        Dim i
        For i = 0 To 70

            Dim list As New ListItem
            list.Value = i + 5
            list.Text = i + 5

            DDFromAge.Items.Insert(i, list)
            DDToAge.Items.Insert(i, list)

        Next


    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT A.LIBRARY_SUB_DIVISION_ID,A.LIBRARY_DIVISION_ID,A.LIBRARY_SUB_DIVISION_DES,A.AGE_GROUP_FROM,A.AGE_GROUP_TO,LIBRARY_DIVISION_DES  " & _
                        " FROM LIBRARY_SUB_DIVISIONS A INNER JOIN LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID= B.LIBRARY_DIVISION_ID  " & _
                        " WHERE B.LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' GROUP BY A.LIBRARY_DIVISION_ID,A.LIBRARY_SUB_DIVISION_DES,A.LIBRARY_SUB_DIVISION_DES,A.AGE_GROUP_FROM,A.AGE_GROUP_TO,A.LIBRARY_SUB_DIVISION_ID,LIBRARY_DIVISION_DES ORDER BY LIBRARY_SUB_DIVISION_DES  DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridLibraryDivisions.DataSource = ds
        GridLibraryDivisions.DataBind()

    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LIBRARY_SUB_DIVISION_DES", TxtLibraryDivisions.Text.Trim().ToUpper())
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddLibraryDivisions.SelectedItem.Value)
            pParms(2) = New SqlClient.SqlParameter("@AGE_GROUP_FROM", DDFromAge.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@AGE_GROUP_TO", DDToAge.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@OPTIONS", 1)


            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_SUB_DIVISIONS", pParms)
            transaction.Commit()
            BindGrid()
            TxtLibraryDivisions.Text = ""

        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

    Protected Sub GridLibraryDivisions_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridLibraryDivisions.RowCommand

        If e.CommandName = "deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@LIBRARY_SUB_DIVISION_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)

                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_SUB_DIVISIONS", pParms)
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions." & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If




    End Sub



    Protected Sub GridLibraryDivisions_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridLibraryDivisions.RowEditing
        GridLibraryDivisions.EditIndex = e.NewEditIndex
        BindGrid()
    End Sub


    Protected Sub GridLibraryDivisions_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridLibraryDivisions.RowCancelingEdit
        GridLibraryDivisions.EditIndex = "-1"
        BindGrid()
    End Sub

    Protected Sub GridLibraryDivisions_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim HiddenSubLibraryDId = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("HiddenSubLibraryDId"), HiddenField).Value
            Dim HiddenLibraryDId = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("HiddenLibraryDId"), HiddenField).Value
            Dim txtLibraryDivisions = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("txtDataEdit"), TextBox).Text.Trim()
            Dim txtFrom = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("txtFrom"), TextBox).Text.Trim()
            Dim txtTo = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("txtTo"), TextBox).Text.Trim()


            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LIBRARY_SUB_DIVISION_ID", HiddenSubLibraryDId)
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", HiddenLibraryDId)
            pParms(2) = New SqlClient.SqlParameter("@LIBRARY_SUB_DIVISION_DES", txtLibraryDivisions)
            pParms(3) = New SqlClient.SqlParameter("@AGE_GROUP_FROM", txtFrom)
            pParms(4) = New SqlClient.SqlParameter("@AGE_GROUP_TO", txtTo)
            pParms(5) = New SqlClient.SqlParameter("@OPTIONS", 2)

            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_SUB_DIVISIONS", pParms)
            transaction.Commit()
            GridLibraryDivisions.EditIndex = "-1"
            BindGrid()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

End Class
