Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryEditPrimaryItemDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenMasterId.Value = Request.QueryString("MasterId")
            Session("EDIT_ISBN") = Nothing
            Session("EDIT_LBSUBJECTS") = Nothing
            CreateDatatables()
            BindCategory()
            BindSharedInfo()
            Bindcontrols()
        End If

    End Sub

    Public Function GetNodeValueAssign(ByVal ParentNode As TreeNode, ByVal ItemType As String) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Value = ItemType Then
                val = node.Value
                node.Checked = True
                Exit For
            Else
                val = GetNodeValueAssign(node, ItemType)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Public Sub BindItemType(ByVal ItemType As String)
        For Each node As TreeNode In TreeItemCategory.Nodes
            If node.Value = ItemType Then
                node.Checked = True
                Exit For
            Else
                Dim Val = GetNodeValueAssign(node, ItemType)
                If Val <> "" Then
                    Exit For
                End If
            End If
        Next

    End Sub

    Public Sub BindSharedInfo()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim Sql_Query = " select DISTINCT PRODUCT_BSU_ID,BSU_SHORTNAME, " & _
                        " (SELECT COUNT(*) FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID= A.MASTER_ID AND PRODUCT_BSU_ID=A.PRODUCT_BSU_ID AND ACTIVE='True') STOCK_COUNT " & _
                        " ,(CASE  ENTRY_BSU_ID WHEN B.BSU_ID THEN 'Creater' else 'Shared' end)INFO " & _
                        " from dbo.LIBRARY_ITEMS_QUANTITY A " & _
                        " INNER JOIN OASIS.dbo.BUSINESSUNIT_M B ON A.PRODUCT_BSU_ID =B.BSU_ID " & _
                        " INNER JOIN dbo.LIBRARY_ITEMS_MASTER C ON C.MASTER_ID= A.MASTER_ID " & _
                        " where A.MASTER_ID='" & HiddenMasterId.Value & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridShared.DataSource = ds
        GridShared.DataBind()

        'If ds.Tables(0).Rows.Count > 1 Then
        '    btnUpdate.Enabled = False
        'End If

    End Sub

    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID='" & childnodeValue & "' AND ITEM_BSU_ID='" & HiddenBsuID.Value & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString()
                ChildNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next

    End Sub

    Public Sub BindCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID=0 AND ITEM_BSU_ID='" & HiddenBsuID.Value & "' or ITEM_ID='1' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString()
                ParentNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        TreeItemCategory.CollapseAll()

        'If TreeItemCategory.Nodes.Count > 0 Then
        '    TreeItemCategory.Nodes(0).Checked = True '' Book Checked
        'End If

    End Sub

    Public Sub CreateDatatables()
        ''ISBN
        Dim dt As DataTable
        dt = New DataTable
        dt.Columns.Add("EDIT_ISBN")
        Session("EDIT_ISBN") = dt

        ''SUBJECTS
        Dim dt1 As DataTable
        dt1 = New DataTable
        dt1.Columns.Add("EDIT_LBSUBJECTS")
        Session("EDIT_LBSUBJECTS") = dt1


    End Sub

    Public Sub Bindcontrols()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT *,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN  'https://school.gemsoasis.com/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL_SHOW,PDF_LINK FROM  LIBRARY_ITEMS_MASTER WHERE MASTER_ID='" & HiddenMasterId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            lblMasterId.Text = ds.Tables(0).Rows(0).Item("MASTER_ID").ToString()
            txttitle.Text = ds.Tables(0).Rows(0).Item("ITEM_TITLE").ToString()
            txtauthor.Text = ds.Tables(0).Rows(0).Item("AUTHOR").ToString()
            txtpublisher.Text = ds.Tables(0).Rows(0).Item("PUBLISHER").ToString()
            txtdateyear.Text = ds.Tables(0).Rows(0).Item("DATE_YEAR").ToString()
            txtelink.Text = ds.Tables(0).Rows(0).Item("PDF_LINK").ToString()
            BindPreviousISBN(ds.Tables(0).Rows(0).Item("ISBN").ToString())
            BindPreviousCategories(ds.Tables(0).Rows(0).Item("SUBJECT").ToString())


            txtpages.Text = ds.Tables(0).Rows(0).Item("FORMAT").ToString()
            txtdesc.Text = ds.Tables(0).Rows(0).Item("PRODUCT_DESCRIPTION").ToString()
            lblEntryType.Text = ds.Tables(0).Rows(0).Item("ENTRY_TYPE").ToString()
            Image1.ImageUrl = ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL_SHOW").ToString()

            ''Bind Type
            str_query = "SELECT TOP 1 ITEM_ID FROM LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID='" & HiddenMasterId.Value & "'"

            Dim ITEM_ID = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()

            If ITEM_ID = "" Then
                ITEM_ID = 1 ''Book
            End If

            BindItemType(ITEM_ID)

            If ds.Tables(0).Rows(0).Item("ENTRY_TYPE").ToString().IndexOf("Google") > -1 Then
                GrdISBN.Enabled = False
                lnkIsbnAdd.Enabled = False
                'txttitle.Enabled = False
                'If Session("sBusper") = "True" Then
                '    btnUpdate.Enabled = True
                'Else
                '    btnUpdate.Enabled = False
                'End If

            End If

        End If

    End Sub

    Public Sub BindPreviousCategories(ByVal Categories As String)
        Dim subject As String() = Categories.Split(",")

        Dim dt As DataTable
        dt = Session("EDIT_LBSUBJECTS")

        Dim i = 0

        For i = 0 To subject.Length - 1

            If subject(i) <> "" Then
                Dim dr As DataRow
                dr = dt.NewRow()
                dr.Item("EDIT_LBSUBJECTS") = subject(i)
                dt.Rows.Add(dr)
                Session("EDIT_LBSUBJECTS") = dt
                txtSubjects.Text = ""
            End If

        Next

        GrdSubjects.DataSource = dt
        GrdSubjects.DataBind()

    End Sub
    Public Sub BindPreviousISBN(ByVal ISBN As String)

        If ISBN <> "" Then
            Dim AISBN As String()
            AISBN = ISBN.Split(",")
            Dim i
            Dim dt As DataTable
            dt = Session("EDIT_ISBN")
            For i = 0 To AISBN.Length - 1
                Dim RValue As String() = AISBN(i).Split(":")
                If RValue(1) <> "" Then

                    Dim dr As DataRow
                    dr = dt.NewRow()
                    dr.Item("EDIT_ISBN") = RValue(1)
                    dt.Rows.Add(dr)
                    Session("EDIT_ISBN") = dt
                    txtisbndata.Text = ""

                End If
            Next
            GrdISBN.DataSource = dt
            GrdISBN.DataBind()
        End If


    End Sub

    Protected Sub lnkIsbnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkIsbnAdd.Click

        Try
            If (txtisbndata.Text.Trim() <> "") And (txtisbndata.Text.ToString().Length = 10 Or txtisbndata.Text.ToString().Length = 13) Then
                Dim dt As DataTable
                dt = Session("EDIT_ISBN")
                Dim dr As DataRow
                dr = dt.NewRow()
                dr.Item("EDIT_ISBN") = txtisbndata.Text.Trim().ToUpper()
                dt.Rows.Add(dr)
                Session("EDIT_ISBN") = dt
                GrdISBN.DataSource = dt
                GrdISBN.DataBind()
                txtisbndata.Text = ""
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GrdISBN_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Try
            Dim dt As DataTable
            dt = Session("EDIT_ISBN")
            dt.Rows(e.RowIndex).Delete()
            Session("EDIT_ISBN") = dt
            GrdISBN.DataSource = dt
            GrdISBN.DataBind()
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub lnkSubjectAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            If txtSubjects.Text.Trim() <> "" Then
                Dim dt As DataTable
                dt = Session("EDIT_LBSUBJECTS")
                Dim dr As DataRow
                dr = dt.NewRow()
                dr.Item("EDIT_LBSUBJECTS") = txtSubjects.Text.Trim()
                dt.Rows.Add(dr)
                Session("EDIT_LBSUBJECTS") = dt
                GrdSubjects.DataSource = dt
                GrdSubjects.DataBind()
                txtSubjects.Text = ""
            End If
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GrdSubjects_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Try
            Dim dt As DataTable
            dt = Session("EDIT_LBSUBJECTS")
            dt.Rows(e.RowIndex).Delete()
            Session("EDIT_LBSUBJECTS") = dt
            GrdSubjects.DataSource = dt
            GrdSubjects.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Public Function CheckPreviousDataISBN() As Boolean

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim Returnvalue = False
        Dim data As String = ""
        If GrdISBN.Rows.Count > 0 Then

            For Each row As GridViewRow In GrdISBN.Rows
                data = data & "ISBN" & DirectCast(row.FindControl("lblData"), Label).Text.Trim()
            Next

            Dim val = data.Replace(":", "").Replace(",", "")
            Dim isbn As String()
            isbn = Regex.Split(val, "ISBN")

            Dim condition = ""
            Dim flag = 0

            Dim i = 0
            For i = 0 To isbn.Length - 1

                If isbn(i).Trim() <> "" Then
                    If flag = 0 Then
                        condition = "ISBN Like '%ISBN:" & isbn(i).Trim() & "%'"
                        flag = 1
                    Else
                        condition = condition & " OR ISBN Like '%ISBN:" & isbn(i).Trim() & "%'"
                    End If


                End If
            Next

            If condition <> "" Then
                Dim Sql_Query = "SELECT '<img src='''+ CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN 'https://school.gemsoasis.com/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END +'''></img> <br> ' + ITEM_TITLE  as imgurl ,MASTER_ID from LIBRARY_ITEMS_MASTER WHERE MASTER_ID !='" & HiddenMasterId.Value & "' AND (" & condition & ")"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, Sql_Query)
                If ds.Tables(0).Rows.Count > 0 Then

                    lblMessage.Text = "One of the ISBN numbers has been assigned for another item. <br> Please enter unique ISBN number. <br> Kindly search for ISBN in Library Items for details."
                    MO1.Show()
                    Returnvalue = False

                Else
                    Returnvalue = True
                End If

            Else
                lblMessage.Text = "ISBN Number entry not in correct format. <br>  Format: ISBN:XXXXXXXXX , ISBN:YYYYYYYYYY"
                MO1.Show()
                Returnvalue = False
            End If

        Else

            ''User Did not Entered any ISBN (Local Book) or Item not a Book
            Returnvalue = True

        End If

        Return Returnvalue

    End Function
    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                'node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        LibraryData.isOffLine(Session("sBusper"))
        If CheckPreviousDataISBN() Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Dim data As String = ""
            Dim flag = 0

            Dim LibraryMessage As String = ""

            Try

                Dim val = ""
                For Each node As TreeNode In TreeItemCategory.Nodes
                    If node.Checked Then
                        val = node.Value
                        'node.Checked = False
                        Exit For
                    Else
                        val = GetNodeValue(node)
                        If val <> "" Then
                            Exit For
                        End If
                    End If
                Next

                If val = "" Then
                    val = 1 ' Book
                End If


                Dim pParms(21) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@ITEM_TITLE", txttitle.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@AUTHOR", txtauthor.Text.Trim())
                pParms(2) = New SqlClient.SqlParameter("@PUBLISHER", txtpublisher.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@DATE_YEAR", txtdateyear.Text.Trim())

                If GrdISBN.Rows.Count > 0 Then

                    For Each row As GridViewRow In GrdISBN.Rows
                        If flag = 0 Then
                            data = "ISBN:" & DirectCast(row.FindControl("lblData"), Label).Text.Trim()
                            flag = 1
                        Else
                            data = data & " , ISBN:" & DirectCast(row.FindControl("lblData"), Label).Text.Trim()
                        End If

                    Next
                    pParms(4) = New SqlClient.SqlParameter("@ISBN", data)

                End If


                pParms(5) = New SqlClient.SqlParameter("@FORMAT", txtpages.Text.Trim())


                flag = 0
                If GrdSubjects.Rows.Count > 0 Then
                    For Each row As GridViewRow In GrdSubjects.Rows
                        If flag = 0 Then
                            data = DirectCast(row.FindControl("lblData"), Label).Text.Trim()
                            flag = 1
                        Else
                            data = data & "  ,  " & DirectCast(row.FindControl("lblData"), Label).Text.Trim()
                        End If
                    Next

                    pParms(6) = New SqlClient.SqlParameter("@SUBJECT", data)

                End If



                pParms(7) = New SqlClient.SqlParameter("@PRODUCT_DESCRIPTION", txtdesc.Text.Trim())
                pParms(8) = New SqlClient.SqlParameter("@ITEM_ID", val)
                pParms(9) = New SqlClient.SqlParameter("@MASTER_ID", HiddenMasterId.Value)
                pParms(10) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", HiddenBsuID.Value)
                pParms(11) = New SqlClient.SqlParameter("@PDF_LINK", txtelink.Text.Trim())
                LibraryMessage = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEMS_MASTER", pParms)
                transaction.Commit()


            Catch ex As Exception
                transaction.Rollback()
                LibraryMessage = "Error occured while saving . " & ex.Message
            Finally
                connection.Close()

            End Try
            lblMessage.Text = LibraryMessage
            MO1.Show()

        End If


    End Sub

    Protected Sub lnkUpdateType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUpdateType.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""

        Try
            Dim sql_query = ""

            Dim val = ""
            For Each node As TreeNode In TreeItemCategory.Nodes
                If node.Checked Then
                    val = node.Value
                    'node.Checked = False
                    Exit For
                Else
                    val = GetNodeValue(node)
                    If val <> "" Then
                        Exit For
                    End If
                End If
            Next

            If val = "" Then
                val = 1 ' Book
            End If

            sql_query = "UPDATE LIBRARY_ITEMS_QUANTITY SET ITEM_ID='" & val & "' WHERE MASTER_ID='" & HiddenMasterId.Value & "' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'"

            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)
            transaction.Commit()
            lblMessage.Text = "Item type updated successfully"
        Catch ex As Exception
            transaction.Rollback()
            lblMessage.Text = "Error occured while saving . " & ex.Message
        Finally
            connection.Close()

        End Try
        MO1.Show()
    End Sub

End Class
