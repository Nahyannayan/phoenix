﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryViewUserRequest.ascx.vb"
    Inherits="Library_UserControls_libraryViewUserRequest" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <contenttemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">
                    User Request Library Items
                   
                </td>
            </tr>
            <tr>
                <td align="left">
                     <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label>
               </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="GridComments" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                        EmptyDataText="No new request added or approved yet." Width="100%" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   
                                                Request ID
                                           
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("RECORD_ID")%>
                                    </center>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   
                                                Title
                                           
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("TITLE")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   
                                                Author
                                            
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("AUTHOR")%>
                                    </center>
                                </ItemTemplate>
                                <ItemStyle  />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   
                                                Notes
                                          
                                </HeaderTemplate>
                                <ItemTemplate>
                                     <asp:Label ID="Elblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                <asp:Panel ID="E4Panel1" runat="server" Height="25px">
                                   <%#Eval("NOTES")%></asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="Elblview"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel1"
                                    TextLabelID="Elblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                                
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    
                                                Date
                                          
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
                                    </center>
                                </ItemTemplate>
                                <ItemStyle  />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   
                                                Granted
                                           
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <asp:Image ID="Image1"  ImageUrl='~/Images/tick.gif' visible='<%#Eval("ShowTick")%>' runat="server" />
                                     
                                        <br />
                                        <asp:LinkButton ID="lnkgrante" Visible='<%#Eval("ShowLink")%>' CommandName="approve" CommandArgument='<%#Eval("RECORD_ID")%>' runat="server">Approve</asp:LinkButton>  
                                    </center>
                                </ItemTemplate>
                                <ItemStyle  />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem"  />
                        <EmptyDataRowStyle  />
                        <SelectedRowStyle  />
                        <HeaderStyle  />
                        <EditRowStyle  />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                    <br />
                </td>
            </tr>
        </table>
    </contenttemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
