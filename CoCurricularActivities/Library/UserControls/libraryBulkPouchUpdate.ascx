﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryBulkPouchUpdate.ascx.vb" Inherits="Library_UserControls_libraryBulkPouchUpdate" %>


<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">

<div>


    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite" colspan="4">Bulk Pouch Update</td>
        </tr>
        <tr>
            <td align="left" width="30%">
                <asp:FileUpload ID="FileUpload1" runat="server" /></td>
            <td align="left" width="30%">
                <asp:Button ID="btnupload" runat="server" CssClass="button" Text="Upload" />
            </td>
            <td align="left" width="20%"></td>
            <td align="left" width="20%"></td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:Label ID="lblmessage" runat="server" class="text-danger"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="4">Note: Please update the rack of each Pouch and Libray items before you proceed 
                for bulk pouch update.<br />
                Excel Fields : POUCH_NO , ACCESSION_NO</td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Items</td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridInfo" runat="server"
                    AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    EmptyDataText="Information not available."
                    Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Pouch No
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("POUCH_NO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Accession No
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%# Eval("ACCESSION_NO")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Status
                                     
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("STATUS") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>

    <asp:HiddenField ID="HiddenBsuid" runat="server" />
    <asp:HiddenField ID="Hiddenempid" runat="server" />

</div>
