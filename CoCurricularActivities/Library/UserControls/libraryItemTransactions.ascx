<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryItemTransactions.ascx.vb" Inherits="Library_UserControls_libraryItemTransactions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<%@ Register Src="libraryItemCount.ascx" TagName="libraryItemCount" TagPrefix="uc1" %>


<script type="text/javascript">

    function Redirect(value) {
        window.open('libraryDetailView.aspx?id=' + value, '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
        return false;
    }

</script>
<script type="text/javascript">
    function openPopup(Masterid, Libraryid) {


        var sFeatures;
        sFeatures = "dialogWidth: 530px; ";
        sFeatures += "dialogHeight: 500px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";


        var result;
        result = window.open('libraryTransaction.aspx?Masterid=' + Masterid + "&Libraryid=" + Libraryid, "", sFeatures);

        if (result == "1") {
            window.location.reload(true);
        }

    }

</script>


<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <table cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite" colspan="3">Step 1-Select Library</td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Select Library</span></td>
                        <td width="40%">
                            <asp:DropDownList ID="ddLibrary" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddLibrary_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label></td>
                        <td></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" Visible="False">
                <table cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Library Item Search
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="13%"> <span class="field-label">Master ID</span>
                                    </td>

                                    <td  width="20%">
                                        <asp:TextBox ID="txtMasterId" runat="server"></asp:TextBox>
                                    </td>
                                    <td  width="13%"><span class="field-label">Accession No</span>
                                    </td>

                                    <td width="20%">
                                        <asp:TextBox ID="txtstockid" runat="server"></asp:TextBox>
                                    </td>
                                    <td  width="13%"><span class="field-label">Call No</span>
                                    </td>

                                    <td width="20%">
                                        <asp:TextBox ID="txtcallno" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="13%"><span class="field-label">Author</span>
                                    </td>

                                    <td width="20%">
                                        <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="13%"><span class="field-label">ISBN</span>
                                    </td>

                                    <td width="20%">
                                        <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="13%"><span class="field-label">Publisher</span>
                                    </td>

                                    <td width="20%" align="left">
                                        <asp:TextBox ID="txtPublisher" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="13%"><span class="field-label">Item Title</span>
                                    </td>

                                    <td width="20%">
                                        <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click"
                                            Text="Search" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Library Items                 
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridItem" runat="server" AllowPaging="True"
                                AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                EmptyDataText="No Records Found. Please search with some other keywords."
                                ShowFooter="true" Width="100%">
                                <Columns>
                                    <%--                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            MasterID
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                            <%# Eval("MASTER_ID") %>
                                        </center>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">ItemImage
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:HiddenField ID="HiddenMasterId" runat="server" 
                                        Value='<%# Eval("MASTER_ID") %>' />
                                    <asp:ImageButton ID="ImageItem" runat="server" Enabled='<%# Eval("SHOWURL") %>' 
                                        ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' 
                                        PostBackUrl='<%# Eval("PRODUCT_URL") %>' />
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">ItemType
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                            <%# Eval("ITEM_DES") %>
                                        </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">ItemTitle
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("ITEM_TITLE") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">Author
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("AUTHOR") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">Publisher
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("PUBLISHER") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">T/R/I/A
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                     <uc1:libraryItemCount ID="libraryItemCount1"  library_division_id='<%# Eval("LIBRARY_DIVISION_ID_VAL") %>' 
                                        master_id='<%# Eval("MASTER_ID") %>'  runat="server" />
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">Reservation
                                       
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                <asp:LinkButton ID="lnkTran" runat="server" OnClientClick='<%# Eval("REDIRECTTRAN") %>'>Reserve</asp:LinkButton>
                            </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table  width="100%">
                                                <tr >
                                                    <td align="center" colspan="2">Info
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="lnkInfo" runat="server" 
                                        OnClientClick='<%# Eval("REDIRECT") %>'>View</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop"  Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <br />
                            T = Total Active Items, R = Total Items Reserved,I = Total Items Issued, A = 
                Total Items Available
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:HiddenField ID="HiddenBsuID" runat="server" />
            <asp:HiddenField ID="HiddenEmpid" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>

</div>
