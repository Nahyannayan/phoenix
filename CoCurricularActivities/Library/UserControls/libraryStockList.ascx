﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryStockList.ascx.vb"
    Inherits="Library_UserControls_libraryStockList" %>
<div >
    <table cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">
                Enter Yearly Stock Details
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>

                        <td>
                            <span class="field-label">  Enter Yearly Stock Title </span>
                        </td>
                       
                        <td>
                            <asp:TextBox ID="txtstockname" runat="server" ></asp:TextBox>
                        </td>
                         <td>
                           <span class="field-label"> Library Division </span>
                        </td>
                       
                        <td>
                            <asp:DropDownList ID="ddLibraryDivisions" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>                  
                    <tr>                       
                        <td align="center" colspan="4">
                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Create Yearly Stock" ValidationGroup="save" />
                        </td>
                    </tr>
                    <tr>                       
                        <td align="left" colspan="4">
                            <asp:Label ID="lblMessage" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table  cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">
                Yearly Stock List Details
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="GridData" runat="server" AllowPaging="True" EmptyDataText="No Records Found" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Status">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Stock Name
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STOCK_TAKE_NAME")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Library
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("LIBRARY_DIVISION_DES")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Total&nbsp;Active
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("TOTAL")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Scanned
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("SCANNED")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Scan&nbsp;Pending
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                   <%-- <%#Eval("PENDING")%>--%>
                                    
                                     <asp:LinkButton ID="LinkPending" Text='<%# Eval("PENDING_SCAN") %>' CommandName="ExportPending" CommandArgument='<%# Eval("STOCK_TAKE_ID") %>'
                                                    runat="server" CausesValidation="false" ></asp:LinkButton>
                                                    
                                                    
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Due
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("DUE")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Inactive
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("INACTIVE")%></center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Entry&nbsp;Date
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("ENTRY_DATE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Close&nbsp;Date
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("CLOSE_DATE")%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Action
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:LinkButton ID="LinkAction" Text='<%#Eval("LINK_TEXT")%>' OnClientClick='<%#Eval("OPEN_WINDOW")%>'
                                      CausesValidation="false"  Visible='<%#Eval("VISIBLE")%>' runat="server"></asp:LinkButton>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                <table  width="100%">
                                    <tr >
                                        <td align="center" colspan="2">
                                            Report
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                 <asp:LinkButton ID="LinkReport" Text="Report" CommandName="ExportReport" CommandArgument='<%# Eval("STOCK_TAKE_ID") %>'
                                                    runat="server" CausesValidation="false" ></asp:LinkButton>
                                                    
                                   <%-- <asp:LinkButton ID="LinkReport" Text="Report" 
                                      CausesValidation="false"  runat="server"></asp:LinkButton>--%>
                                </center>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop"  Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtstockname"
        Display="None" ErrorMessage="Please Enter Stock Header" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddLibraryDivisions"
        Display="None" ErrorMessage="Please Select Library Division" InitialValue="-1" ValidationGroup="save"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ValidationGroup="save"
        ShowSummary="False" />
</div>
