Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryQuickIssueTransactions
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            BindLibraryDivisions()
            BindItemStatus()
        End If

    End Sub

    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = ""
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
            str_query = "SELECT LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES FROM LIBRARY_DIVISIONS " & _
                          "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Else

            str_query = " SELECT B.LIBRARY_DIVISION_ID,B.LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                            " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                            " WHERE LIBRARIAN_EMP_ID='" & HiddenEmpid.Value & "' AND ACTIVE='True' AND LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' "
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrary.DataSource = ds
        ddLibrary.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibrary.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibrary.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows.Count > 1 Then
                Dim list As New ListItem
                list.Value = "0"
                list.Text = "Select a library"
                ddLibrary.Items.Insert(0, list)
            Else

                Call ddLibrary_SelectedIndexChanged(ddLibrary, Nothing)

            End If
        Else
            lblMessage.Text = "Login User is not a librarian. Please assign librarian and proceed further. "
            ddLibrary.Visible = False
            MO1.Show()
        End If



    End Sub

    Public Sub BindItemStatus()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE STATUS_ID != 4 AND CIRCULATORY='True' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddissuestatus.DataSource = ds
        ddissuestatus.DataTextField = "STATUS_DESCRIPTION"
        ddissuestatus.DataValueField = "STATUS_ID"
        ddissuestatus.DataBind()

        Dim list1 As New ListItem
        list1.Text = "Present Status"
        list1.Value = "0"
        ddissuestatus.Items.Insert(0, list1)

    End Sub

    Public Sub BindMembershipDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "SELECT * FROM LIBRARY_MEMBERSHIPS WHERE MEMBERSHIP_ID='" & ddmemebershipissue.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then

            Td1.InnerHtml = ds.Tables(0).Rows(0).Item("MEMBERSHIP_DES").ToString()
            Td2.InnerHtml = ds.Tables(0).Rows(0).Item("USER_TYPE").ToString()
            Td3.InnerHtml = ds.Tables(0).Rows(0).Item("MAX_ITEMS").ToString()
            Td4.InnerHtml = ds.Tables(0).Rows(0).Item("LENDING_DAYS").ToString()


        End If

    End Sub

    Protected Sub btnIssueNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIssueNext.Click
        HiddenAccessionNo.Value = ""
        GridItem.DataSource = Nothing
        GridItem.DataBind()
        GridItem.Visible = False

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        ''Get the userid for given user number

        If RadioUserTypeIssueItem.SelectedValue = "STUDENT" Then

            Sql_Query = "SELECT STU_ID FROM OASIS.dbo.STUDENT_M WHERE STU_NO='" & Left(txtuserno.Text.Trim(), 14) & "'"

            HiddenUserID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

        ElseIf RadioUserTypeIssueItem.SelectedValue = "EMPLOYEE" Then
            Sql_Query = "SELECT EMP_ID FROM OASIS.dbo.EMPLOYEE_M WHERE EMPNO='" & Left(txtuserno.Text.Trim(), 14) & " '"

            HiddenUserID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        End If


        ''Check for if the user has memeber of this library
        Sql_Query = " SELECT A.MEMBERSHIP_ID,MEMBERSHIP_DES FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
                        " INNER JOIN LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                        " WHERE A.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' AND " & _
                        " A.USER_ID='" & HiddenUserID.Value & "' AND A.USER_TYPE='" & RadioUserTypeIssueItem.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddmemebershipissue.DataSource = ds
            ddmemebershipissue.DataTextField = "MEMBERSHIP_DES"
            ddmemebershipissue.DataValueField = "MEMBERSHIP_ID"
            ddmemebershipissue.DataBind()

            ''Display Memeber Details
            BindMemberDetails()

            ''Display Membership details
            BindMembershipDetails()



            ''Check For Member Item Taken Count
            Panel3.Visible = True
            Panel2.Visible = False
            CheckTakenCount()

            BindPreviousTransactions(HiddenUserID.Value)
            txtstockid.Focus()

        Else


            lblMessage.Text = "Invalid User ID. Please Check User Type. <br> (or) <br> Membership not assigned for this user for this library. Please assign Membership for this user."
            MO1.Show()
            Panel3.Visible = False
        End If


    End Sub

    Public Sub BindMemberDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        Dim ds As DataSet
        If RadioUserTypeIssueItem.SelectedValue = "STUDENT" Then
            Sql_Query = " SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')), " & _
                        " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " & _
                        " FROM oasis.dbo.STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID " & _
                        " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                        " WHERE STU_NO='" & Left(txtuserno.Text.Trim(), 14) & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            If ds.Tables(0).Rows.Count > 0 Then

                stuType.InnerHtml = "STUDENT"
                stuMembername.InnerHtml = ds.Tables(0).Rows(0).Item("STU_NAME").ToString()
                stuMemberNo.InnerHtml = ds.Tables(0).Rows(0).Item("STU_NO").ToString()
                stuGrade.InnerHtml = ds.Tables(0).Rows(0).Item("GRM_DISPLAY").ToString()
                stuSection.InnerHtml = ds.Tables(0).Rows(0).Item("SCT_DESCR").ToString()


                btnIssue.Visible = True
                UserStudent.Visible = True
                UserEmployee.Visible = False
            Else
                btnIssue.Visible = False
            End If

        ElseIf RadioUserTypeIssueItem.SelectedValue = "EMPLOYEE" Then
            Sql_Query = " select EMP_ID,EMPNO, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID from OASIS.dbo.EMPLOYEE_M a " & _
                        " inner join  OASIS.dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID " & _
                        " WHERE EMPNO='" & Left(txtuserno.Text.Trim(), 14) & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                empType.InnerHtml = "EMPLOYEE"
                empMembername.InnerHtml = ds.Tables(0).Rows(0).Item("ENAME").ToString()
                empMemberNo.InnerHtml = ds.Tables(0).Rows(0).Item("EMPNO").ToString()
                empdesc.InnerHtml = ds.Tables(0).Rows(0).Item("DES_DESCR").ToString()

                btnIssue.Visible = True
                UserStudent.Visible = False
                UserEmployee.Visible = True
            Else
                btnIssue.Visible = False
            End If

        End If

    End Sub

    Public Sub BindPreviousTransactions(ByVal Userid As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        Dim ds As DataSet
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuID.Value)
        pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddLibrary.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@USER_NO", Left(txtuserno.Text.Trim(), 14))
        pParms(3) = New SqlClient.SqlParameter("@USER_TYPE", RadioUserTypeIssueItem.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@OUT", True)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_LIBRARY_TRANSACTIONS", pParms)
        GridTransactions.DataSource = ds
        GridTransactions.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            Panel4.Visible = True
        Else
            Panel4.Visible = False
        End If

    End Sub

    Protected Sub ddLibrary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibrary.SelectedIndexChanged

        If ddLibrary.SelectedValue <> "0" Then
            txtuserno.Focus()
            Hiddenlibrarydivid.Value = ddLibrary.SelectedValue
            Panel1.Visible = False
            Panel2.Visible = True
        End If

    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Protected Sub ddmemebershipissue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddmemebershipissue.SelectedIndexChanged
        CheckTakenCount()
        BindMembershipDetails()
    End Sub

    Public Sub issueItem(ByVal accessionnumber As String, Optional ByVal Pouch As Boolean = False)

        Dim Hash As New Hashtable
        Hash.Add("HiddenBsuID", HiddenBsuID.Value)
        Hash.Add("txtstockid", accessionnumber)
        Hash.Add("HiddenUserID", HiddenUserID.Value)
        Hash.Add("RadioUserTypeIssueItem", RadioUserTypeIssueItem.SelectedValue)
        Hash.Add("ddissuestatus", ddissuestatus.SelectedValue)
        Hash.Add("txtissuenotes", txtissuenotes.Text.Trim())
        Hash.Add("HiddenEmpid", HiddenEmpid.Value)
        Hash.Add("ddLibrary", ddLibrary.SelectedValue)
        Hash.Add("Hiddenlibrarydivid", Hiddenlibrarydivid.Value)
        Hash.Add("ddmemebershipissue", ddmemebershipissue.SelectedValue)
        Hash.Add("btnIssue", btnIssue)
        Hash.Add("Pouch", Pouch)
        lblMessage.Text = LibraryTransactions.IssueItems(Hash)

    End Sub

    Public Sub BindPouchItems(Optional ByVal showmessage As Boolean = True)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim ds As DataSet
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID_POUCH_ID", LibraryData.GetStockIDForAccessonNo(HiddenAccessionNo.Value, HiddenBsuID.Value))
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 21)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "LIB_POUCH_TRAN", pParms)
        GridItem.DataSource = ds
        GridItem.DataBind()
        GridItem.Visible = True
        If showmessage Then
            If ds.Tables(0).Rows.Count = 0 Then
                lblMessage.Text = "No available items in pouch/box to issue. Please check the transaction history of pouch and each items in pouch."
                MO1.Show()
            End If
        End If

    End Sub
    Public Function checkPouch() As Boolean
        Dim rval = False
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "select * from dbo.LIBRARY_ITEMS_QUANTITY where STOCK_ID_POUCH_ID=" & LibraryData.GetStockIDForAccessonNo(txtstockid.Text.Trim(), HiddenBsuID.Value) & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        If ds.Tables(0).Rows.Count > 0 Then
            HiddenAccessionNo.Value = txtstockid.Text.Trim()
            BindPouchItems()
            rval = True
        Else
            HiddenAccessionNo.Value = ""
            GridItem.Visible = False
        End If

        Return rval
    End Function

    Public Sub btnIssuepack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ''Issue Pouch first.
        issueItem(HiddenAccessionNo.Value) '' Passing Accession Number.

        ''Issue Items in pouch.
        If lblMessage.Text.Contains("successfully") Then '' If pouch issued successfully.

            For Each row As GridViewRow In GridItem.Rows
                If DirectCast(row.FindControl("ch1"), CheckBox).Checked Then
                    issueItem(DirectCast(row.FindControl("HiddenStockId"), HiddenField).Value, True)  '' Passing Accession Number.
                End If
            Next

        End If

        MO1.Show()
        BindPouchItems(False)
        BindPreviousTransactions(HiddenUserID.Value)

    End Sub

    Protected Sub btnIssue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIssue.Click
        lblMessage.Text = ""
        If checkPouch() = False Then
            issueItem(txtstockid.Text.Trim()) '' Passing Accession Number.
            BindPreviousTransactions(HiddenUserID.Value)
            clearcontrol()
            MO1.Show()
        End If


    End Sub

    Public Sub clearcontrol()
        ddissuestatus.SelectedIndex = 0
        txtissuenotes.Text = ""
        txtstockid.Text = ""
    End Sub

    Protected Sub Nextuser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Nextuser.Click
        Panel3.Visible = False
        Panel2.Visible = True
        txtuserno.Text = ""
        txtstockid.Text = ""
        txtissuenotes.Text = ""
        txtuserno.Focus()
    End Sub
    Protected Sub Nextusertop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Nextusertop.Click
        Panel3.Visible = False
        Panel2.Visible = True
        txtuserno.Text = ""
        txtstockid.Text = ""
        txtissuenotes.Text = ""
        txtuserno.Focus()
    End Sub
    Public Sub CheckTakenCount()
        Dim ReturnValue = ""
        Dim Hash As New Hashtable
        Hash.Add("ddmemebershipissue", ddmemebershipissue.SelectedValue)
        Hash.Add("RadioUserTypeIssueItem", RadioUserTypeIssueItem.SelectedValue)
        Hash.Add("HiddenUserID", HiddenUserID.Value)
        Hash.Add("btnIssue", btnIssue)
        LibraryTransactions.CheckTakenCount(Hash, ReturnValue)
        If ReturnValue <> "" Then
            lblMessage.Text = ReturnValue
            MO1.Show()
        End If

    End Sub

    Protected Sub lnkbtn_reisssue_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            lblMessage.Text = ""
            Dim HF_ACCESSION_NO As HiddenField = TryCast(sender.parent.FindControl("HF_ACCESSION_NO"), HiddenField)
            Dim HF_RECORD_ID As HiddenField = TryCast(sender.parent.FindControl("HF_RECORD_ID"), HiddenField)
            Dim ReturnFlag As Integer = Reissue(HF_RECORD_ID.Value, HF_ACCESSION_NO.Value)
            If (ReturnFlag = 0) Then
                BindPreviousTransactions(HiddenUserID.Value)
                lblMessage.Text = "Reissued sucessfully."
                MO1.Show()
            Else
                lblMessage.Text = "Reissue failed -" + lblMessage.Text
                MO1.Show()
            End If

        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            MO1.Show()
        End Try
    End Sub


    Private Function Reissue(ByVal Record_ID As String, ByVal ACCESSION_NO As String) As Integer
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Dim ReturnFlag As Integer = 1
        Try

            '---------------------Returing & Reissuing the Book internally----------------------'
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            pParms(2) = New SqlClient.SqlParameter("@RECORD_ID", Record_ID.Trim())
            pParms(3) = New SqlClient.SqlParameter("@ISSUE_EMP_ID", HiddenEmpid.Value)
            pParms(4) = New SqlClient.SqlParameter("@ACCESSION_NO", ACCESSION_NO)
            SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_TRANSACTIONS_REISSUE", pParms)
            ReturnFlag = pParms(1).Value
            If (ReturnFlag = 0) Then
                transaction.Commit()
            End If
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
        Finally
            connection.Close()
        End Try
        Return ReturnFlag
    End Function

    Protected Sub lnkbtn_ReturnBook_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_ACCESSION_NO As HiddenField = TryCast(sender.parent.FindControl("HF_ACCESSION_NO"), HiddenField)
        Dim HF_librarydivid As HiddenField = TryCast(sender.parent.FindControl("HF_librarydivid"), HiddenField)

        libraryQuickReturnTransactions_S1.BindDetails(HF_librarydivid.Value, HF_ACCESSION_NO.Value)

        PnlReturn.Visible = True
        PnlReturn.ScrollBars = ScrollBars.Vertical    

        'BindReturnInfoDetails()
    End Sub

    Protected Sub lnkClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        PnlReturn.Visible = False
        BindPreviousTransactions(HiddenUserID.Value)
    End Sub
    Public Sub ClosePanel(ByVal message As String)
        PnlReturn.Visible = False
        BindPreviousTransactions(HiddenUserID.Value)
    End Sub
    Protected Sub lnkbtn_Reservation_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub lnkTransClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTransClose.Click
        PanelTrans.Visible = False
    End Sub

    Protected Sub lnkHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHistory.Click
        PanelTrans.Visible = True
        BindTransHistory(HiddenUserID.Value)
    End Sub
    Public Sub BindTransHistory(ByVal Userid As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        Dim ds As DataSet
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", HiddenBsuID.Value)
        pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddLibrary.SelectedValue)
        pParms(2) = New SqlClient.SqlParameter("@USER_NO", Left(txtuserno.Text.Trim(), 14))
        pParms(3) = New SqlClient.SqlParameter("@USER_TYPE", RadioUserTypeIssueItem.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@OUT", True)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "REPORT_LIBRARY_TRANSACTIONS_HISTORY", pParms)
        grdTransactionHistory.DataSource = ds
        grdTransactionHistory.DataBind()

    End Sub
End Class
