Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryEditRatingsCommentsApprove
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenMasterId.Value = Request.QueryString("MasterId")
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "SELECT COMMENT_ID,COMMENTS,RATING,ENTRY_DATE, " & _
                        " CASE APPROVED WHEN 'True' THEN '~/Images/tick.gif' ELSE '~/Images/cross.png' END APPROVED, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, " & _
                        " (SELECT AVG(RATING) FROM dbo.LIBRARY_ITEMS_COMMENTS WHERE MASTER_ID=" & HiddenMasterId.Value & " AND COMMENT_BSU='" & HiddenBsuID.Value & "') AVG_RATING " & _
                        " FROM dbo.LIBRARY_ITEMS_COMMENTS WHERE MASTER_ID=" & HiddenMasterId.Value & " AND COMMENT_BSU='" & HiddenBsuID.Value & "'  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridComments.DataSource = ds
        GridComments.DataBind()

    End Sub


    Protected Sub GridComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridComments.PageIndexChanging
        GridComments.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim LibraryMessage = ""

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            Dim flag = 0
            Dim query = ""
            Dim Value = DirectCast(GridComments.FooterRow.FindControl("RadioApprovalSelect"), RadioButtonList).SelectedValue

            For Each row As GridViewRow In GridComments.Rows

                Dim HiddenStockId = DirectCast(row.FindControl("HiddenCommentId"), HiddenField).Value
                Dim CheckApprove As CheckBox = DirectCast(row.FindControl("CheckApprove"), CheckBox)


                If CheckApprove.Checked Then
                    flag = 1
                    query = "UPDATE LIBRARY_ITEMS_COMMENTS SET APPROVED='" & Value & "' WHERE COMMENT_ID='" & HiddenStockId & "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                End If

            Next

            If flag = 0 Then
                LibraryMessage = "Please select comments for Approve/Disapprove ."
            Else
                LibraryMessage = "Selected Comments Updated Successfully."
            End If

            transaction.Commit()

        Catch ex As Exception

            transaction.Rollback()
            LibraryMessage = "Error occured while saving . " & ex.Message

        Finally

            connection.Close()

        End Try

        lblMessage.Text = LibraryMessage
        MO12.Show()

        BindGrid()

    End Sub

    Protected Sub btnmok2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO12.Hide()
    End Sub
End Class


