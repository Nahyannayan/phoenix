Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryLibrarian
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Hiddenbsuid.Value = Session("sbsuid")
            BindGrid()
            BindLibraryDevisions()
            BindDesignation()
        End If
        
    End Sub
    'ts
    Public Sub BindLibraryDevisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlibraryDivisions.DataSource = ds
        ddlibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddlibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddlibraryDivisions.DataBind()

    End Sub

    Public Sub BindDesignation()
        
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select des_id , des_descr from EMPDESIGNATION_M where des_flag='SD' order by des_descr "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddes.DataSource = ds
        dddes.DataValueField = "des_id"
        dddes.DataTextField = "des_descr"
        dddes.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Designation"
        dddes.Items.Insert(0, list)

    End Sub
    Public Function GetData() As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select EMP_ID,EMPNO, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID from dbo.EMPLOYEE_M a " & _
                        " inner join  dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID where EMP_BSU_ID='" & Hiddenbsuid.Value & "' and des_flag='SD' and EMP_bACTIVE='True' and EMP_STATUS <> 4 "
        Dim txtname, txtnumber As String
        Dim desigid As DropDownList
        If GrdStaff.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = dddes

            If txtnumber.Trim() <> "" Then
                str_query &= " and EMPNO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                str_query &= " and isnull(EMP_FNAME,'')+ isnull(EMP_MNAME,'')+isnull(EMP_LNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If
            If desigid.SelectedValue > -1 Then
                str_query &= " and EMP_DES_ID= '" & desigid.SelectedValue & "'"
            End If

        End If

        str_query &= " order by EMP_FNAME "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Return ds
    End Function
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim txtname, txtnumber As String
        Dim desigid As DropDownList

        If GrdStaff.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = dddes
        End If


        Dim ds As DataSet
        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("ENAME")
            dt.Columns.Add("EMPNO")
            dt.Columns.Add("MEMBERSHIP_DES")
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMP_DES_ID")
            dt.Columns.Add("DES_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("ENAME") = ""
            dr("EMPNO") = ""
            dr("MEMBERSHIP_DES") = ""
            dr("EMP_ID") = ""
            dr("EMP_DES_ID") = ""
            dr("DES_DESCR") = ""
            dt.Rows.Add(dr)
            GrdStaff.DataSource = dt
            GrdStaff.DataBind()
            DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button).Visible = False

        Else
            GrdStaff.DataSource = ds
            GrdStaff.DataBind()

            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

            For Each row As GridViewRow In GrdStaff.Rows
                Dim emp_id = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                Dim GridLibrary As GridView = DirectCast(row.FindControl("GridLibrary"), GridView)
                Dim query = " SELECT LIBRARIAN_ID,LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                            " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                            " WHERE LIBRARIAN_EMP_ID='" & emp_id & "' AND ACTIVE='True' "

                GridLibrary.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

                GridLibrary.DataBind()

            Next

            DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button).Visible = True

            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button))
           
        End If

        If GrdStaff.Rows.Count > 0 Then

            DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text = txtname

            If desigid Is Nothing Then
            Else
                dddes.SelectedValue = desigid.SelectedValue
            End If
        End If

    End Sub

    Protected Sub ddesschange(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub


    Protected Sub GrdStaff_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStaff.PageIndexChanging

        GrdStaff.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub btncancel4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO3.Hide()
    End Sub


    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Protected Sub GrdStaff_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "assign" Then
            MO3.Show()
        End If

        If e.CommandName = "search" Then
            BindGrid()
        End If
    End Sub

    Protected Sub GridLibrary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim sql_query = "UPDATE LIBRARY_LIBRARIAN SET ACTIVE='False' where LIBRARIAN_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)
            transaction.Commit()
            lblMessage.Text = "Librarian deleted successfully."
        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try
        MO1.Show()
        BindGrid()

    End Sub

    Protected Sub btnStatusupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim flag = 0

            For Each row As GridViewRow In GrdStaff.Rows

                Dim check As CheckBox = DirectCast(row.FindControl("Checklist"), CheckBox)

                If check.Checked Then

                    flag = 1
                    Dim EMPID = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@LIBRARIAN_EMP_ID", EMPID)
                    pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", ddlibraryDivisions.SelectedValue)
                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_LIBRARIAN", pParms)

                End If

            Next

            transaction.Commit()

            If flag = 0 Then
                lblMessage.Text = "Please select employee."
            Else
                'BindStudentView()
            End If

        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

        MO3.Hide()
        MO1.Show()

        BindGrid()

    End Sub


End Class
