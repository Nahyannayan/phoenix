﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class Library_UserControls_libraryItemDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenShowFlag.Value = 0
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            BindLibraryDivisions(ddLibraryDivisions)
            BindStatus()
            BindGrid()
        End If
        If GridItem.Rows.Count > 0 Then
            Dim btn As Button = DirectCast(GridItem.FooterRow.FindControl("btnbarcode"), Button)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn)
            Dim btn2 As Button = DirectCast(GridItem.FooterRow.FindControl("btnbarcode2"), Button)
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn2)
        End If
        If DropEdit.SelectedValue > 1 Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpdate)
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnexport)
    End Sub

    Public Sub BindLibraryDivisions(ByVal ddLibraryDivisions As DropDownList)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        'Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
        '                "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim str_query = ""
        If Library.LibrarySuperAcess(Session("EmployeeId")) Then
            str_query = "SELECT LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES FROM LIBRARY_DIVISIONS " & _
                          "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Else

            str_query = " SELECT B.LIBRARY_DIVISION_ID,B.LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                            " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                            " WHERE LIBRARIAN_EMP_ID='" & HiddenEmpid.Value & "' AND ACTIVE='True' AND LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            ddLibraryDivisions.DataSource = ds
            ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
            ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
            ddLibraryDivisions.DataBind()
            Dim list As New ListItem
            list.Value = "-1"
            list.Text = "Select Library"
            ddLibraryDivisions.Items.Insert(0, list)
            If Session("sBusper") = "False" Then
                ddLibraryDivisions.SelectedIndex = 1
            End If
        End If

        BindLibSubDivisions()

    End Sub

    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE STATUS_ID != 4 "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddStatus.DataSource = ds
        ddStatus.DataTextField = "STATUS_DESCRIPTION"
        ddStatus.DataValueField = "STATUS_ID"
        ddStatus.DataBind()

        Dim list As New ListItem
        list.Text = "All"
        list.Value = "-1"
        ddStatus.Items.Insert(0, list)

    End Sub
    Public Sub BindLibSubDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dropsubdiv.DataSource = ds
        dropsubdiv.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        dropsubdiv.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        dropsubdiv.DataBind()

        Dim list As New ListItem
        list.Text = "Libray Sub Divisions"
        list.Value = "-1"
        dropsubdiv.Items.Insert(0, list)


    End Sub

    Public Function BindGrid() As DataSet
        Dim ds As New DataSet
        Try


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim str_query = " SELECT MASTER_ID,ACCESSION_NO,STOCK_ID,CALL_NO,ISBN,ITEM_TITLE,AUTHOR,PUBLISHER,ITEM_DES,LIBRARY_DIVISION_DES,LIBRARY_SUB_DIVISION_DES,SHELF_NAME,SHELF_NAME,RACK_NAME,PURCHASE_DATE , PRODUCT_PRICE , STATUS_DESCRIPTION, " & _
                            " CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN '~/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL ,PRODUCT_URL, " & _
                            " CASE (SELECT TOP 1 ISNULL(ITEM_ACTUAL_RETURN_DATE,'') FROM dbo.LIBRARY_TRANSACTIONS WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND  STOCK_ID=VIEW_LIBRARY_RECORDS.STOCK_ID)  WHEN '' THEN 'False' ELSE 'True' end EDIT_ENABLE, " & _
                            " CASE AVAILABILITY WHEN 'Y' THEN '~/Images/tick.gif' ELSE '~/Images/cross.png' END AVAILABLE_URL, " & _
                            " CASE ACTIVE WHEN 'True' THEN '~/Images/tick.gif' ELSE '~/Images/cross.png' END ACTIVE_URL " & _
                            ",'javascript:opentran('''+ CONVERT(VARCHAR, STOCK_ID) +''');return false;' OPEN_TRAN" & _
                            ",'javascript:openAction('''+ CONVERT(VARCHAR, STOCK_ID) +''');return false;' OPEN_ACTIONS" & _
                            " FROM dbo.VIEW_LIBRARY_RECORDS WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' "


            Dim Condition As String = ""

            If txtItemName.Text.Trim() <> "" Then

                Condition = Condition & " AND ITEM_TITLE LIKE '%" & txtItemName.Text.Trim() & "%'"

            End If


            If txtAuthor.Text.Trim() <> "" Then

                Condition = Condition & " AND AUTHOR LIKE '%" & txtAuthor.Text.Trim() & "%'"

            End If

            If txtPublisher.Text.Trim() <> "" Then

                Condition = Condition & " AND PUBLISHER LIKE '%" & txtPublisher.Text.Trim() & "%'"

            End If

            If txtISBN.Text.Trim() <> "" Then

                Condition = Condition & " AND ISBN LIKE '%" & txtISBN.Text.Trim() & "%'"

            End If

            If txtMasterId.Text.Trim() <> "" Then

                Condition = Condition & " AND MASTER_ID = '" & txtMasterId.Text.Trim() & "'"

            End If

            If txtstockid.Text.Trim() <> "" Then

                Condition = Condition & " AND STOCK_ID = '" & LibraryData.GetStockIDForAccessonNo(txtstockid.Text.Trim(), HiddenBsuID.Value) & "'"

            End If

            If txtcallno.Text.Trim() <> "" Then

                Condition = Condition & " AND CALL_NO LIKE '%" & txtcallno.Text.Trim() & "%'"

            End If

            If ddLibraryDivisions.SelectedValue > 0 Then

                Condition = Condition & " AND LIBRARY_DIVISION_ID = '" & ddLibraryDivisions.SelectedValue & "'"

            End If

            If dropsubdiv.SelectedValue > 0 Then

                Condition = Condition & " AND LIBRARY_SUB_DIVISION_ID = '" & dropsubdiv.SelectedValue & "'"

            End If


            If ddAvailable.SelectedIndex > 0 Then

                Condition = Condition & " AND AVAILABILITY = '" & ddAvailable.SelectedValue & "'"

            End If

            If DropOptions.SelectedIndex > 0 Then

                If DropOptions.SelectedValue = "0" Then
                    Condition = Condition & " AND ISNULL(ACCESSION_NO,'')='' "
                End If
                If DropOptions.SelectedValue = "1" Then
                    Condition = Condition & " AND ISNULL(CALL_NO,'')='' "
                End If
                If DropOptions.SelectedValue = "2" Then
                    Condition = Condition & " AND ISNULL(RACK_ID,'')='' "
                End If
                If DropOptions.SelectedValue = "3" Then
                    Condition = Condition & " AND ISNULL(STATUS_ID,'')='' "
                End If


            End If

            If ddStatus.SelectedIndex > 0 Then
                Condition = Condition & " AND STATUS_ID = '" & ddStatus.SelectedValue & "' "
            End If

            If txtsubject.Text.Trim() <> "" Then
                Condition = Condition & " AND SUBJECT LIKE '%" & txtsubject.Text.Trim() & "%' "
            End If


            If CheckPouch.Checked Then
                Condition = Condition & " AND STOCK_ID IN (SELECT DISTINCT STOCK_ID_POUCH_ID FROM dbo.VIEW_LIBRARY_RECORDS  WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' )"
            End If


            If Condition <> "" Then

                str_query = str_query & Condition

            End If
            If HiddenBsuID.Value <> "123016" And HiddenBsuID.Value <> "165010" Then
                str_query = str_query & " ORDER BY ITEM_TITLE"
            Else
                str_query = str_query & " ORDER BY CONVERT(INT,ACCESSION_NO)"
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            GridItem.DataSource = ds
            GridItem.DataBind()

            lblTotal.Text = " Total Records : " & ds.Tables(0).Rows.Count

            btnUpdate.Visible = False

            If DropEdit.SelectedIndex > 0 Then
                btnUpdate.Visible = True
                If DropEdit.SelectedValue = 0 Then '' Edit Accession No.
                    For Each row As GridViewRow In GridItem.Rows
                        DirectCast(row.FindControl("txtAccessionNo"), TextBox).Visible = True
                    Next
                End If
                If DropEdit.SelectedValue = 1 Then '' Edit Call No.
                    For Each row As GridViewRow In GridItem.Rows
                        DirectCast(row.FindControl("txtCallNo"), TextBox).Visible = True
                    Next
                End If
                If DropEdit.SelectedValue = 3 Then '' Edit Rack No.
                    For Each row As GridViewRow In GridItem.Rows
                        DirectCast(row.FindControl("CheckRack"), CheckBox).Visible = True
                    Next
                End If
                If DropEdit.SelectedValue = 2 Then '' Edit Item Type
                    For Each row As GridViewRow In GridItem.Rows
                        DirectCast(row.FindControl("CheckType"), CheckBox).Visible = True
                    Next
                End If
                If DropEdit.SelectedValue = 4 Then '' Edit Item Status
                    For Each row As GridViewRow In GridItem.Rows
                        DirectCast(row.FindControl("CheckStatus"), CheckBox).Visible = True
                    Next
                End If

            End If
        Catch ex As Exception

        End Try

        Return ds
    End Function

    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging

        GridItem.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMessage.Text = ""
        BindGrid()

    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddLibraryDivisions.SelectedIndexChanged
        lblMessage.Text = ""
        BindLibSubDivisions()
        BindGrid()
    End Sub

    Protected Sub ddAvailable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAvailable.SelectedIndexChanged
        lblMessage.Text = ""
        BindGrid()
    End Sub

    Protected Sub DropOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropOptions.SelectedIndexChanged
        lblMessage.Text = ""
        BindGrid()
    End Sub

    Protected Sub ddStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddStatus.SelectedIndexChanged
        lblMessage.Text = ""
        BindGrid()
    End Sub

    Protected Sub DropEdit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropEdit.SelectedIndexChanged
        lblMessage.Text = ""
        BindGrid()
    End Sub


    Public Sub Update1()
        For Each row As GridViewRow In GridItem.Rows
            Dim stock_id = DirectCast(row.FindControl("HiddenStockid"), HiddenField).Value

            'If DropEdit.SelectedValue = 2 Then '' Edit Item Type
            '    Dim check As CheckBox = DirectCast(row.FindControl("CheckType"), CheckBox)
            '    If check.Checked Then
            '        HiddenStockIds.Value &= "," & stock_id
            '    End If
            'End If

            If DropEdit.SelectedValue = 3 Then '' Edit Rack No.
                Dim check As CheckBox = DirectCast(row.FindControl("CheckRack"), CheckBox)
                If check.Checked Then
                    HiddenStockIds.Value &= "," & stock_id
                End If
            End If
            If DropEdit.SelectedValue = 4 Then '' Edit Item Status
                Dim check As CheckBox = DirectCast(row.FindControl("CheckStatus"), CheckBox)
                If check.Checked Then
                    HiddenStockIds.Value &= "," & stock_id
                End If
            End If


        Next
        If HiddenStockIds.Value <> "" Then
            HiddenShowFlag.Value = 1
        End If
    End Sub

    Public Sub Update2()
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try
            Dim flag = 0
            'Update Accession No or Call No
            For Each row As GridViewRow In GridItem.Rows

                If DropEdit.SelectedValue = 0 Then '' Edit Accession No.



                    Dim stock_id As HiddenField = DirectCast(row.FindControl("HiddenStockid"), HiddenField)
                    Dim accession_no As TextBox = DirectCast(row.FindControl("txtAccessionNo"), TextBox)

                    If accession_no.Text.Trim() <> "" Then
                        Dim pParms(3) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stock_id.Value.Trim())
                        pParms(1) = New SqlClient.SqlParameter("@ACCESSION_NO", accession_no.Text.Trim())
                        pParms(2) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", HiddenBsuID.Value)
                        Dim ret = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEM_ACCESSION_NO", pParms)
                        If ret = 0 Then
                            accession_no.BackColor = Drawing.Color.Red
                            flag = 1
                        Else
                            accession_no.BackColor = Drawing.Color.White
                        End If
                    End If


                End If

                If DropEdit.SelectedValue = 1 Then '' Edit Call No.

                    Dim stock_id As HiddenField = DirectCast(row.FindControl("HiddenStockid"), HiddenField)
                    Dim call_no As TextBox = DirectCast(row.FindControl("txtCallNo"), TextBox)

                    If call_no.Text.Trim() <> "" Then
                        Dim pParms(3) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stock_id.Value.Trim())
                        pParms(1) = New SqlClient.SqlParameter("@CALL_NO", call_no.Text.Trim())
                        pParms(2) = New SqlClient.SqlParameter("@PRODUCT_BSU_ID", HiddenBsuID.Value)
                        Dim ret = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEM_CALL_NO", pParms)
                        If ret = 0 Then
                            call_no.BackColor = Drawing.Color.Yellow
                            flag = 1
                        Else
                            call_no.BackColor = Drawing.Color.White
                        End If
                    End If

                End If

            Next

            transaction.Commit()

            If flag = 0 Then
                lblMessage.Text = "Records updated successfully."
                BindGrid()
            Else
                lblMessage.Text = "Item Marked as Red Cannot be updated because the Accession No has been used by other Item. Please enter unique Accession Number."
            End If



        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions." & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()
        End Try


    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        LibraryData.isOffLine(Session("sBusper"))
        lblMessage.Text = ""
        HiddenStockIds.Value = ""

        If DropEdit.SelectedValue = "2" Or DropEdit.SelectedValue = "3" Or DropEdit.SelectedValue = "4" Then

            Update1()

        Else

            Update2()

        End If


    End Sub


    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim dt As New DataTable
        dt = BindGrid().Tables(0)
        dt.Columns.Remove("STOCK_ID")
        dt.Columns.Remove("SHELF_NAME1")
        dt.Columns.Remove("PRODUCT_IMAGE_URL")
        dt.Columns.Remove("PRODUCT_URL")
        dt.Columns.Remove("EDIT_ENABLE")
        dt.Columns.Remove("AVAILABLE_URL")
        dt.Columns.Remove("ACTIVE_URL")
        dt.Columns.Remove("OPEN_TRAN")

        ExportExcel(dt)
    End Sub

    Public Sub ExportExcel(ByVal dt As DataTable)

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile
        Dim ws As GemBox.Spreadsheet.ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

        'Response.ContentType = "application/vnd.ms-excel"
        'Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
        'ef.SaveXls(Response.OutputStream)
        Dim pathSave As String
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        pathSave = "LibraryItem" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()


    End Sub

    Public Sub deletebooks(ByVal sender As Object, ByVal e As System.EventArgs)

        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try

            Dim stockid = ""
            For Each row As GridViewRow In GridItem.Rows

                If DirectCast(row.FindControl("ch1"), CheckBox).Checked Then

                    stockid = DirectCast(row.FindControl("HiddenStockid"), HiddenField).Value
                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stockid)
                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "DELETE_LIBRARY_ITEMS", pParms)

                End If

            Next
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            lblMessage.Text = "Error :" & ex.Message
        Finally
            connection.Close()

        End Try

        BindGrid()

    End Sub
    Public Sub GenerateBarcode(ByVal sender As Object, ByVal e As System.EventArgs)
        BarcodePublish(True)

    End Sub
    Public Sub GenerateBarcode2(ByVal sender As Object, ByVal e As System.EventArgs)
        BarcodePublish(False)

    End Sub
    Public Sub BarcodePublish(ByVal Pagewise As Boolean)

        Session("Barcodehashtable") = Nothing
        Session("BarcodehashtableTemp") = Nothing

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim hash As New Hashtable

        If Pagewise Then

            For Each row As GridViewRow In GridItem.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("ch1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("HiddenMasterId"), HiddenField)
                Dim key = Hid.Value.Trim()
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, key)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next
        Else

            Dim ds1 As DataSet = BindGrid()

            Dim i As Integer

            For i = 0 To ds1.Tables(0).Rows.Count - 1

                Dim key = ds1.Tables(0).Rows(i).Item("MASTER_ID").ToString().Trim()
                If hash.ContainsKey(key) Then
                Else
                    hash.Add(key, key)
                End If

            Next

        End If



        Dim idictenum As IDictionaryEnumerator
        idictenum = hash.GetEnumerator()

        Session("Barcodehashtable") = Nothing
        Dim str_query = ""
        Dim ds As DataSet

        Dim hash1 As New Hashtable

        While (idictenum.MoveNext())
            Dim key = idictenum.Key
            Dim id = idictenum.Value


            str_query = " SELECT A.STOCK_ID " & _
                            " FROM  dbo.LIBRARY_ITEMS_QUANTITY A " & _
                            " INNER JOIN dbo.LIBRARY_ITEMS_MASTER BC ON A.MASTER_ID=BC.MASTER_ID " & _
                            " WHERE A.MASTER_ID='" & id & "' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' AND A.ACTIVE='True' ORDER BY STOCK_ID,ACCESSION_NO"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1

                Dim skey = ds.Tables(0).Rows(i).Item("STOCK_ID").ToString().Trim()
                If hash1.ContainsKey(key) Then
                Else
                    hash1.Add(skey, skey)
                End If

            Next

        End While
        Session("Barcodehashtable") = hash1
        Session("BarcodehashtableTemp") = Nothing
        If hash1.Count > 0 Then
            HiddenShowFlag.Value = 1
        Else
            HiddenShowFlag.Value = 0
        End If


        Dim url As String = String.Format("Barcode/BarcodeCrystal/Barcode.aspx")
        Dim jscript As New StringBuilder()
        jscript.Append("<script>window.open('")
        jscript.Append(url)
        jscript.Append("');</script>")
        Page.RegisterStartupScript("OpenWindows", jscript.ToString())

    End Sub
End Class
