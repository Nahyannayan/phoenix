<%@ Control Language="VB" AutoEventWireup="false" CodeFile="librarySubDivisions.ascx.vb" Inherits="Library_UserControls_librarySubDivisions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<div>

    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">


    <asp:LinkButton ID="LinkAdd" runat="server" OnClientClick="javascript:return false;">Add</asp:LinkButton>
    <asp:Panel ID="Panel2" runat="server">

        <table width="100%">
            <tr>
                <td width="20%"><span class="field-label">Library Division</span></td>
                <td width="30%">
                    <asp:DropDownList ID="ddLibraryDivisions" runat="server">
                    </asp:DropDownList></td>
                <td width="20%"><span class="field-label">Library Sub Division</span></td>
                <td width="30%">
                    <asp:TextBox ID="TxtLibraryDivisions" runat="server"></asp:TextBox></td>
            </tr>

          <%--  <tr>
                <td align="left" colspan="4" class="title-bg-lite"><span class="field-label">Age Group</span>
                </td>

            </tr>--%>
            <tr>
                <td><span class="field-label">Age Group - From</span></td>
                <td>
                    <asp:DropDownList ID="DDFromAge" runat="server">
                    </asp:DropDownList></td>
                <td><span class="field-label">To</span></td>
                <td>
                    <asp:DropDownList ID="DDToAge" runat="server"></asp:DropDownList></td>


            </tr>
            <tr>

                <td align="center" colspan="4">
                    <asp:Button ID="BtnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="ts2" /></td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdd" Collapsed="true"
        CollapsedSize="0" CollapsedText="Add" ExpandControlID="LinkAdd" ExpandedSize="200"
        ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel2" TextLabelID="LinkAdd">
    </ajaxToolkit:CollapsiblePanelExtender>
    <br />
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>

    <table width="100%">
        <tr>
            <td class="title-bg">Library Sub Divisions</td>
        </tr>
        <tr>
            <td>



                <asp:GridView ID="GridLibraryDivisions" AutoGenerateColumns="false" Width="100%" runat="server" OnRowUpdating="GridLibraryDivisions_RowUpdating" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Library Divisions">
                            <HeaderTemplate>
                                Library Divisions
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("LIBRARY_DIVISION_DES")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HiddenLibraryDId" Value='<%#Eval("LIBRARY_DIVISION_ID")%>' runat="server" />
                                <center><%#Eval("LIBRARY_DIVISION_DES")%></center>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Library Sub Divisions">
                            <HeaderTemplate>
                                Library Sub Divisions
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HiddenSubLibraryDId" Value='<%#Eval("LIBRARY_SUB_DIVISION_ID")%>' runat="server" />
                                <asp:TextBox ID="txtDataEdit" Text='<%#Eval("LIBRARY_SUB_DIVISION_DES")%>' runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorE1" runat="server" SetFocusOnError="True" ErrorMessage="Please Enter Library Sub Division" Display="None" ControlToValidate="txtDataEdit" ValidationGroup="Edit"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Age Group From">
                            <HeaderTemplate>
                                Age Group From

                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("AGE_GROUP_FROM")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
           <asp:TextBox ID="txtFrom" Text='<%#Eval("AGE_GROUP_FROM")%>'   MaxLength="2" runat="server"></asp:TextBox>
           <asp:RequiredFieldValidator id="RequiredFieldValidatorE2" runat="server" SetFocusOnError="True" ErrorMessage="Please Enter From Age Group" Display="None" ControlToValidate="txtFrom" ValidationGroup="Edit"></asp:RequiredFieldValidator>
           <ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtFrom"  runat="server" ></ajaxToolkit:FilteredTextBoxExtender>
          </center>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Age Group To">
                            <HeaderTemplate>
                                Age Group To
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("AGE_GROUP_TO") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
           <asp:TextBox ID="txtTo" Text='<%#Eval("AGE_GROUP_TO")%>'   MaxLength="2"  runat="server"></asp:TextBox>
           <asp:RequiredFieldValidator id="RequiredFieldValidatorE3" runat="server" SetFocusOnError="True" ErrorMessage="Please Enter To Age Group" Display="None" ControlToValidate="txtTo" ValidationGroup="Edit"></asp:RequiredFieldValidator>
            <ajaxToolkit:FilteredTextBoxExtender ID="F2" FilterType="Numbers" TargetControlID="txtTo"  runat="server" ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:CompareValidator id="CompareValidatorE1" runat="server" SetFocusOnError="True" ValidationGroup="Edit" ErrorMessage='"To Age Group"  Must be greater  than or equal to "From Age Group"' Display="None" ControlToValidate="txtTo" Type="Integer" Operator="GreaterThanEqual" ControlToCompare="txtFrom"></asp:CompareValidator>
          </center>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Edit
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkEdit" CommandName="Edit" CausesValidation="false" CommandArgument='<%#Eval("LIBRARY_SUB_DIVISION_ID")%>' runat="server">Edit</asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <center>
             <asp:LinkButton ID="LinkUpdate" CommandName="Update" ValidationGroup="Edit" CommandArgument='<%#Eval("LIBRARY_SUB_DIVISION_ID")%>' runat="server">Update</asp:LinkButton>
             <asp:LinkButton ID="LinkCancel" CommandName="Cancel" CausesValidation="false" CommandArgument='<%#Eval("LIBRARY_SUB_DIVISION_ID")%>' runat="server">Cancel</asp:LinkButton>
            </center>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Delete
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkDelete" CommandName="deleting" CausesValidation="false" CommandArgument='<%#Eval("LIBRARY_SUB_DIVISION_ID")%>' runat="server"> Delete</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" TargetControlID="LinkDelete" ConfirmText="Are you sure, Do you want this record to be deleted ?"></ajaxToolkit:ConfirmButtonExtender>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>


            </td>
        </tr>
    </table>




    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtLibraryDivisions" ValidationGroup="ts2"
        Display="None" ErrorMessage="Please Enter Library Sub Division" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="DDFromAge"
        ControlToValidate="DDToAge" Display="None" ErrorMessage='"To Age Group"  Must be greater  than or equal to "From Age Group"'
        Operator="GreaterThanEqual" SetFocusOnError="True" Type="Integer"></asp:CompareValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="Edit" />
    <asp:HiddenField ID="HiddenBsuID" runat="server" />

</div>
