Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml

Partial Class Library_UserControls_libraryItems
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsu_id.Value = Session("sbsuid")
            BindJobCategory()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnExport)
    End Sub

    'ts
    Public Sub BindChildNodes(ByVal ParentNode As TreeNode)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim childnodeValue = ParentNode.Value
        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID='" & childnodeValue & "' AND ITEM_BSU_ID='" & Hiddenbsu_id.Value & "'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ChildNode As New TreeNode
                ChildNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString() & " (" & ds.Tables(0).Rows(i).Item("ITEM_ID").ToString() & ")"
                ChildNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                ParentNode.ChildNodes.Add(ChildNode)

            Next
        End If


        For Each node As TreeNode In ParentNode.ChildNodes
            BindChildNodes(node)
        Next


    End Sub

    Public Sub BindJobCategory()
        TreeItemCategory.Nodes.Clear()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()

        Dim Sql_Query = "Select ITEM_ID,ITEM_DES from  LIBRARY_ITEMS where ITEM_PRI_ID=0 AND ITEM_BSU_ID='" & Hiddenbsu_id.Value & "' or ITEM_ID='1' "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim ParentNode As New TreeNode
                ParentNode.Text = ds.Tables(0).Rows(i).Item("ITEM_DES").ToString() & " (" & ds.Tables(0).Rows(i).Item("ITEM_ID").ToString() & ")"
                ParentNode.Value = ds.Tables(0).Rows(i).Item("ITEM_ID").ToString()
                TreeItemCategory.Nodes.Add(ParentNode)
            Next

        End If

        For Each node As TreeNode In TreeItemCategory.Nodes
            BindChildNodes(node)
        Next

        TreeItemCategory.ExpandAll()


    End Sub


    Public Function GetNodeValue(ByVal ParentNode As TreeNode) As String
        Dim val = ""

        For Each node As TreeNode In ParentNode.ChildNodes
            If node.Checked Then
                val = node.Value
                node.Checked = False
                Exit For
            Else
                val = GetNodeValue(node)

                If val <> "" Then
                    Exit For
                End If

            End If

        Next

        Return val

    End Function

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        LibraryData.isOffLine(Session("sBusper"))

        If txtcategory.Text <> "" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try

                Dim val = ""
                For Each node As TreeNode In TreeItemCategory.Nodes
                    If node.Checked Then
                        val = node.Value
                        node.Checked = False
                        Exit For
                    Else
                        val = GetNodeValue(node)
                        If val <> "" Then
                            Exit For
                        End If
                    End If
                Next

                If val = "" Then
                    val = 0
                End If

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ITEM_DES", txtcategory.Text.Trim())
                pParms(1) = New SqlClient.SqlParameter("@ITEM_PRI_ID", val)
                pParms(2) = New SqlClient.SqlParameter("@ITEM_BSU_ID", Hiddenbsu_id.Value)
                pParms(3) = New SqlClient.SqlParameter("@OPTIONS", 1)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_ITEMS", pParms)
                txtcategory.Text = ""
                transaction.Commit()
                BindJobCategory()

            Catch ex As Exception
                transaction.Rollback()

            Finally
                connection.Close()

            End Try


        End If

    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        LibraryData.isOffLine(Session("sBusper"))

        If txtcategory.Text <> "" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try

                Dim val = ""
                For Each node As TreeNode In TreeItemCategory.Nodes
                    If node.Checked Then
                        val = node.Value
                        node.Checked = False
                        Exit For
                    Else
                        val = GetNodeValue(node)
                        If val <> "" Then
                            Exit For
                        End If
                    End If
                Next

                If val = "" Then
                    val = 0
                End If

                If val <> "0" Then
                    If val = "1" Then
                        lblmessage.Text = "'Books' Category cannot be renamed.This category is globally assigned."
                    Else
                        Dim pParms(4) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@ITEM_DES", txtcategory.Text.Trim())
                        pParms(1) = New SqlClient.SqlParameter("@ITEM_ID", val)
                        pParms(2) = New SqlClient.SqlParameter("@ITEM_BSU_ID", Hiddenbsu_id.Value)
                        pParms(3) = New SqlClient.SqlParameter("@OPTIONS", 2)
                        lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_ITEMS", pParms)
                        txtcategory.Text = ""
                        transaction.Commit()
                        BindJobCategory()
                    End If


                End If

            Catch ex As Exception
                transaction.Rollback()

            Finally
                connection.Close()

            End Try


        End If


    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        LibraryData.isOffLine(Session("sBusper"))

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblmessage.Text = ""
        Try

            Dim val = ""
            For Each node As TreeNode In TreeItemCategory.Nodes
                If node.Checked Then
                    val = node.Value
                    node.Checked = False
                    Exit For
                Else
                    val = GetNodeValue(node)
                    If val <> "" Then
                        Exit For
                    End If
                End If
            Next

            If val = "" Then
                val = 0
            End If

            If val <> "0" Then
                If val = "1" Then
                    lblmessage.Text = "'Books' Category cannot be deleted.This category is globally assigned."
                Else

                    Dim pParms(2) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@ITEM_ID", val)
                    pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)
                    lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_ITEMS", pParms)
                    txtcategory.Text = ""
                    transaction.Commit()
                    BindJobCategory()

                End If

            End If

        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

  

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        TreeItemCategory.ShowCheckBoxes = TreeNodeTypes.None

        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.Charset = ""
        Me.EnableViewState = False
        Dim oStringWriter As New System.IO.StringWriter()
        Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)
        TreeItemCategory.RenderControl(oHtmlTextWriter)
        Response.Write(oStringWriter.ToString())
        Response.[End]()

    End Sub



End Class
