<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryItemEntry.ascx.vb" Inherits="Library_UserControls_libraryItemEntry" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">

<script type="text/javascript">



    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");

        if (isChkBoxClick) {
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid != event.srcElement.id) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }

    }



    function RedirectEdit() {
        var value = document.getElementById('<%= HiddenMasterId.ClientID %>').value;
    var direct = document.getElementById('<%= HiddenDirect.ClientID %>').value;
    if (direct == 1) {
        document.getElementById('<%= HiddenDirect.ClientID %>').value = 0;
    window.open('libraryEditingItem.aspx?MasterId=' + value + '&Edit=0', '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes');
}
    return false;
}


</script>

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="3" class="title-bg-lite">Primary Informations</td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Item Title</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txttitle" runat="server" EnableTheming="False" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Author</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtauthor" runat="server" EnableTheming="False" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Publisher</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtpublisher" runat="server" EnableTheming="False" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Date/Year</span></td>
                    <td align="left" coslpan="2">
                        <asp:TextBox ID="txtdateyear" runat="server" EnableTheming="False" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">ISBN</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtisbndata" runat="server" EnableTheming="False"  ValidationGroup="MEntry"></asp:TextBox>
                        <asp:LinkButton ID="lnkIsbnAdd" runat="server" CausesValidation="False" OnClick="lnkIsbnAdd_Click">Add</asp:LinkButton><br />
                        <asp:GridView ID="GrdISBN" runat="server" AutoGenerateColumns="false" OnRowDeleting="GrdISBN_RowDeleting"
                            ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblData" runat="server" Text='<%# Eval("ISBN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <center>
                                <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandName="Delete">Delete</asp:LinkButton>
                            </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem"  />
                            <EmptyDataRowStyle  />
                            <SelectedRowStyle  />
                            <HeaderStyle  />
                            <EditRowStyle  />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                        <ajaxToolkit:FilteredTextBoxExtender ID="F4ISBN" FilterType="Custom, Numbers, UppercaseLetters" TargetControlID="txtisbndata" runat="server">
                        </ajaxToolkit:FilteredTextBoxExtender>
                        Caps Characters and Numbers Only.<br />
                        Please enter only valid ISBN of a book. (ISBN can be 10 or 13 digit characters,
            its unique for a book title.)</td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">No. of Pages</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtpages" runat="server" EnableTheming="False" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Subjects</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtSubjects" runat="server" EnableTheming="False" ValidationGroup="MEntry">

                        </asp:TextBox><asp:LinkButton ID="lnkSubjectAdd" runat="server" CausesValidation="False" OnClick="lnkSubjectAdd_Click">Add</asp:LinkButton><br />
                        <asp:Panel ID="Panel1" runat="server"  ScrollBars="Auto" Width="100%">
                            <asp:GridView ID="GrdSubjects" runat="server" AutoGenerateColumns="false" OnRowDeleting="GrdSubjects_RowDeleting"
                                ShowHeader="false" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblData" runat="server" Text='<%# Eval("LBSUBJECTS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <center>
                                    <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandName="Delete">Delete</asp:LinkButton>
                                </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem"  />
                                <EmptyDataRowStyle />
                                <SelectedRowStyle  />
                                <HeaderStyle  />
                                <EditRowStyle  />
                                <AlternatingRowStyle CssClass="griditem_alternative"  />
                            </asp:GridView>
                        </asp:Panel>
                        <ajaxToolkit:FilteredTextBoxExtender ID="F5SUBJECTS" TargetControlID="txtSubjects" runat="server" FilterMode="InvalidChars" InvalidChars=",">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Item Description</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtdesc" runat="server" EnableTheming="False" Height="142px" TextMode="MultiLine"
                            Width="527px" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="3" class="title-bg-lite">Other Details</td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Item Type</span></td>
                    <td align="left" colspan="2">
                        <div class="checkbox-list">
                        <asp:TreeView ID="TreeItemCategory" runat="server" ImageSet="Arrows" onclick="OnTreeClick(event);" ShowCheckBoxes="All">
                            <ParentNodeStyle Font-Bold="False" />
                            <HoverNodeStyle Font-Underline="True"  />
                            <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px"
                                VerticalPadding="0px" />
                            <NodeStyle HorizontalPadding="5px"
                                NodeSpacing="0px" VerticalPadding="0px" />
                        </asp:TreeView>
                            </div>
                    </td>
                </tr>

                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Item Category</span></td>
                    <td alignb="left" colspan="2">
                        <asp:RadioButtonList ID="rgpItemCat" runat="server" RepeatDirection="Horizontal" >
                            <asp:ListItem Selected="True">Normal</asp:ListItem>
                            <asp:ListItem>E-Book</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>


                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Quantity</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtQuantity" runat="server" Width="75px" ValidationGroup="MEntry"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">E-Book Link</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtelink" runat="server" ></asp:TextBox></td>
                </tr>

                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Age Group</span></td>
                    <td align="left" width="35%" >
                        <span class="field-label">From :</span>
                        <asp:DropDownList ID="DDFromAge" runat="server" ValidationGroup="MEntry">
                        </asp:DropDownList></td>
                         <td align="left" width="35%" ><span class="field-label">  To :</span><asp:DropDownList ID="DDToAge" runat="server" ValidationGroup="MEntry">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Supplier</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtsupplier" runat="server" ValidationGroup="MEntry" Width="304px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Purchase Date</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtpurchasedate" runat="server" ValidationGroup="MEntry"></asp:TextBox>&nbsp;<asp:Image
                            ID="Image1" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                </tr>
                <tr>
                    <td align="left" width="30%">
                        <span class="field-label">Cost of an Item</span></td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtcost" runat="server" Width="75px" ValidationGroup="MEntry"></asp:TextBox>
                        <asp:DropDownList ID="ddcurrency" runat="server">
                        </asp:DropDownList></td>
                </tr>
                <tr>
               
                    <td align="center" colspan="3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="MEntry" /></td>
                </tr>
            </table>
            <ajaxToolkit:FilteredTextBoxExtender ID="F1" FilterType="Numbers" TargetControlID="txtQuantity" runat="server"></ajaxToolkit:FilteredTextBoxExtender>
            <ajaxToolkit:FilteredTextBoxExtender ID="F2" FilterType="Custom, Numbers" TargetControlID="txtcost" runat="server" ValidChars=".,">
            </ajaxToolkit:FilteredTextBoxExtender>
            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Image1"
                TargetControlID="txtpurchasedate">
            </ajaxToolkit:CalendarExtender>
            <asp:Label ID="Label1" runat="server"></asp:Label><br />
            <asp:Panel ID="PanelStatusupdate"
                runat="server" CssClass="panel-cover" Style="display: none">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite">Library Message</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btnmok" runat="server" CssClass="button" OnClientClick="javascript:RedirectEdit(); return false;" Text="Ok"
                                                    ValidationGroup="s" CausesValidation="False" /></td>
                                        </tr>
                                    </table>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                DropShadow="true" PopupControlID="PanelStatusupdate" CancelControlID="btnmok"
                RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label1">
            </ajaxToolkit:ModalPopupExtender>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttitle"
                Display="None" ErrorMessage="Please Enter Title of an Item" SetFocusOnError="True" ValidationGroup="MEntry"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtQuantity"
                Display="None" ErrorMessage="Please Enter Item Quantity" SetFocusOnError="True" ValidationGroup="MEntry"></asp:RequiredFieldValidator>
            <%--            <asp:RequiredFieldValidator
                ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtsupplier" Display="None"
                ErrorMessage="Please Enter Supplier Details" SetFocusOnError="True" ValidationGroup="MEntry"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtpurchasedate"
                    Display="None" ErrorMessage="Please Enter Purchase Date" SetFocusOnError="True"
                    ValidationGroup="MEntry"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcost"
            Display="None" ErrorMessage="Please Enter Cost of an Item" SetFocusOnError="True" ValidationGroup="MEntry"></asp:RequiredFieldValidator>--%>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="DDFromAge"
                ControlToValidate="DDToAge" Display="None" ErrorMessage='"To Age Group"  Must be greater  than or equal to "From Age Group"'
                Operator="GreaterThanEqual" SetFocusOnError="True" Type="Integer" ValidationGroup="MEntry"></asp:CompareValidator>&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" ValidationGroup="MEntry" />
            &nbsp;&nbsp;
        <br />
            <asp:HiddenField ID="HiddenBsuID" runat="server" />
            <asp:HiddenField ID="HiddenEmpid" runat="server" />
            <asp:HiddenField ID="HiddenShowImage" runat="server" Value="0" />
            <asp:HiddenField ID="HiddenImageUrl" runat="server" />
            <asp:HiddenField ID="HiddenProductUrl" runat="server" />
            <asp:HiddenField ID="HiddenIsbnEnc" runat="server" />
            <asp:HiddenField ID="HiddenRadioSelect" runat="server" />
            <asp:HiddenField ID="HiddenMasterId" runat="server" />
            <asp:HiddenField ID="HiddenDirect" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">

    if (document.getElementById('<%= HiddenShowImage.ClientID %>').value == '1') {

        search(this.form)
        document.getElementById('<%= HiddenShowImage.ClientID %>').value = 0;
}


</script>

