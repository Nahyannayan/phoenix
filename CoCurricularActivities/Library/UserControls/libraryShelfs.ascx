<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryShelfs.ascx.vb" Inherits="Library_UserControls_libraryShelfs" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<div class="matters">


    <asp:LinkButton ID="LinkAdd" runat="server" OnClientClick="javascript:return false;">Add</asp:LinkButton>
    <asp:Panel ID="Panel2" runat="server">
        <table width="100%">
            <tr>
                <td width="20%"><span class="field-label">Library Divisions</span></td>
                <td width="30%">
                    <asp:DropDownList ID="ddLibraryDivisions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddLibraryDivisions_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td width="20%"><span class="field-label">Library Sub Divisions</span></td>
                <td width="30%">
                    <asp:DropDownList ID="ddLibrarySubDivisions" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td><span class="field-label">Row</span></td>
                <td>
                    <asp:DropDownList ID="ddRow" runat="server">
                    </asp:DropDownList></td>
                <td><span class="field-label">Shelf Name / Id</span></td>

                <td>
                    <asp:TextBox ID="txtShelfName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><span class="field-label">Shelf Description</span></td>

                <td>
                    <asp:TextBox ID="txtShelfDescription" runat="server" EnableTheming="False"
                        TextMode="MultiLine"></asp:TextBox></td>
                <td><span class="field-label">Shelf Route Description</span></td>

                <td>
                    <asp:TextBox ID="txtRoutDescription" runat="server" EnableTheming="False" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Add" ValidationGroup="ts3" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdd" Collapsed="true"
        CollapsedSize="0" CollapsedText="Add" ExpandControlID="LinkAdd" ExpandedSize="400"
        ExpandedText="Hide" ScrollContents="false" TargetControlID="Panel2" TextLabelID="LinkAdd">
    </ajaxToolkit:CollapsiblePanelExtender>
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
    <table width="100%">
        <tr>
            <td class="title-bg">Library Shelves</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridShelfs" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                    Width="100%" AllowPaging="True" OnPageIndexChanging="GridShelfs_PageIndexChanging" OnRowCancelingEdit="GridShelfs_RowCancelingEdit" OnRowCommand="GridShelfs_RowCommand" OnRowEditing="GridShelfs_RowEditing" OnRowUpdating="GridShelfs_RowUpdating">
                    <Columns>
                        <asp:TemplateField HeaderText="Library Divisions">
                            <HeaderTemplate>
                                Library Divisions
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("LIBRARY_DIVISION_DES")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Library Sub Divisions">
                            <HeaderTemplate>
                                Library Sub-Divisions
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Row">
                            <HeaderTemplate>
                                Row
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("SHELF_ROW")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HiddenRow" Value='<%#Eval("SHELF_ROW")%>' runat="server" />
                                <asp:DropDownList ID="DropRowEdit" runat="server">
                                </asp:DropDownList>

                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Shelf">
                            <HeaderTemplate>
                                Shelf
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%#Eval("SHELF_NAME")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HiddenSubLibraryDId" runat="server" Value='<%#Eval("LIBRARY_SUB_DIVISION_ID")%>' />
                                <asp:HiddenField ID="HiddenShelfId" runat="server" Value='<%#Eval("SHELF_ID")%>' />
                                <asp:TextBox ID="txtDataEdit" runat="server" Text='<%#Eval("SHELF_NAME")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorE1" runat="server" ControlToValidate="txtDataEdit"
                                    Display="None" ErrorMessage="Please Enter Shelf Name" SetFocusOnError="True"
                                    ValidationGroup="Edit"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descriptions">
                            <HeaderTemplate>
                                Descriptions
                            </HeaderTemplate>
                            <ItemTemplate>


                                <asp:Label ID="Elblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                <asp:Panel ID="E4Panel1" runat="server">
                                    <%#Eval("SHELF_DES")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="Elblview"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel1"
                                    TextLabelID="Elblview">
                                </ajaxToolkit:CollapsiblePanelExtender>


                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="txtShelfDescription" runat="server" Text='<%#Eval("SHELF_DES")%>' EnableTheming="False"
                                    TextMode="MultiLine" Width="200px"></asp:TextBox>


                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Rout Descriptions">
                            <HeaderTemplate>
                                Route Descriptions
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:Label ID="Elblview1" runat="server" Text='<%#Eval("tempview2")%>'></asp:Label>
                                <asp:Panel ID="E4Panel11" runat="server">
                                    <%#Eval("SHELF_ROUT_DES")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview1" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview2")%>' ExpandControlID="Elblview1"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel11"
                                    TextLabelID="Elblview1">
                                </ajaxToolkit:CollapsiblePanelExtender>

                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="txtRoutDescription" runat="server" Text='<%#Eval("SHELF_ROUT_DES")%>' EnableTheming="False"
                                    TextMode="MultiLine" Width="200px"></asp:TextBox>

                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Edit
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:LinkButton ID="LinkEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("SHELF_ID")%>'
                                    CommandName="Edit">Edit</asp:LinkButton>

                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:LinkButton ID="LinkUpdate" runat="server" CommandArgument='<%#Eval("SHELF_ID")%>'
                                    CommandName="Update" ValidationGroup="Edit">Update</asp:LinkButton>
                                <asp:LinkButton ID="LinkCancel" runat="server" CausesValidation="false" CommandArgument='<%#Eval("SHELF_ID")%>'
                                    CommandName="Cancel">Cancel</asp:LinkButton>

                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Delete
                            </HeaderTemplate>
                            <ItemTemplate>

                                <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("SHELF_ID")%>'
                                    CommandName="deleting"> Delete</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="CF1" runat="server" ConfirmText="Are you sure, Do you want this record to be deleted ?"
                                    TargetControlID="LinkDelete">
                                </ajaxToolkit:ConfirmButtonExtender>

                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtShelfName" ValidationGroup="ts3"
        Display="None" ErrorMessage="Please Enter Shelf Name" SetFocusOnError="True"></asp:RequiredFieldValidator><asp:ValidationSummary
            ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="Edit" />

</div>
