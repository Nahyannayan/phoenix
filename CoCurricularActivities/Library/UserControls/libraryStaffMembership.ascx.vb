Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryStaffMembership
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Hiddenbsuid.Value = Session("sbsuid")
            BindGrid()
            BindLibraryDevisions()
            BindDesignation()
            BindCategory()
        End If
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStaff.FooterRow.FindControl("btnbarcode"), Button))
        'ts
    End Sub
    Public Sub BindCategory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " select ECT_ID ,ECT_DESCR from EMPCATEGORY_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dropcategory.DataSource = ds
        Dropcategory.DataSource = ds
        Dropcategory.DataTextField = "ECT_DESCR"
        Dropcategory.DataValueField = "ECT_ID"
        Dropcategory.DataBind()

        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a category"
        Dropcategory.Items.Insert(0, list)

    End Sub
    Public Sub BindLibraryDevisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlibraryDivisions.DataSource = ds
        ddlibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddlibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddlibraryDivisions.DataBind()

        BindMemberships()


    End Sub

    Public Sub BindMemberships()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT MEMBERSHIP_ID,MEMBERSHIP_DES FROM dbo.LIBRARY_MEMBERSHIPS A " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                        " WHERE LIBRARY_BSU_ID='" & Hiddenbsuid.Value & "' AND A.LIBRARY_DIVISION_ID='" & ddlibraryDivisions.SelectedValue & "' AND USER_TYPE='EMPLOYEE'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddMemberships.DataSource = ds
        ddMemberships.DataTextField = "MEMBERSHIP_DES"
        ddMemberships.DataValueField = "MEMBERSHIP_ID"
        ddMemberships.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            btnStatusupdate.Visible = True
        Else
            btnStatusupdate.Visible = False
        End If

    End Sub

    Public Sub BindDesignation()
        'Dim dddes As DropDownList = DirectCast(GrdStaff.HeaderRow.FindControl("dddes"), DropDownList)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select DISTINCT des_id , des_descr from EMPDESIGNATION_M A WITH (NOLOCK) INNER JOIN EMPLOYEE_M B WITH (NOLOCK) ON DES_ID=EMP_DES_ID  AND EMP_STATUS IN (1,2)  AND EMP_BSU_ID='" & Hiddenbsuid.Value & "' where des_flag='SD' order by des_descr "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dddes.DataSource = ds
        dddes.DataValueField = "des_id"
        dddes.DataTextField = "des_descr"
        dddes.DataBind()
        Dim list As New ListItem
        list.Value = "-1"
        list.Text = "Select a Designation"
        dddes.Items.Insert(0, list)

    End Sub

    Public Function GetData() As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select EMP_ID,EMPNO, isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') ENAME, DES_DESCR,EMP_DES_ID from dbo.EMPLOYEE_M a " & _
                        " inner join  dbo.EMPDESIGNATION_M b on a.EMP_DES_ID=b.DES_ID where EMP_BSU_ID='" & Hiddenbsuid.Value & "' and des_flag='SD' and EMP_bACTIVE='True' and EMP_STATUS <> 4 "
        Dim txtname, txtnumber As String
        Dim desigid As DropDownList
        If GrdStaff.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            desigid = dddes

            If txtnumber.Trim() <> "" Then
                str_query &= " and EMPNO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                str_query &= " and isnull(EMP_FNAME,'')+ isnull(EMP_MNAME,'')+isnull(EMP_LNAME,'') like '%" & txtname.Replace(" ", "") & "%' "
            End If
            If desigid.SelectedValue > -1 Then
                str_query &= " and EMP_DES_ID= '" & desigid.SelectedValue & "'"
            End If

            If Dropcategory.SelectedValue <> "-1" Then
                str_query &= " and EMP_ECT_ID= '" & Dropcategory.SelectedValue & "'"
            End If

        End If

        str_query &= " order by EMP_FNAME ,EMPNO,EMP_ECT_ID "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Return ds
    End Function

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim txtname, txtnumber As String


        If GrdStaff.Rows.Count > 0 Then
            txtnumber = DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()

        End If


        Dim ds As DataSet
        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("ENAME")
            dt.Columns.Add("EMPNO")
            dt.Columns.Add("MEMBERSHIP_DES")
            dt.Columns.Add("EMP_ID")
            dt.Columns.Add("EMP_DES_ID")
            dt.Columns.Add("DES_DESCR")

            Dim dr As DataRow = dt.NewRow()
            dr("ENAME") = ""
            dr("EMPNO") = ""
            dr("MEMBERSHIP_DES") = ""
            dr("EMP_ID") = ""
            dr("EMP_DES_ID") = ""
            dr("DES_DESCR") = ""
            dt.Rows.Add(dr)
            GrdStaff.DataSource = dt
            GrdStaff.DataBind()
            DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button).Visible = False
            'DirectCast(GrdStaff.FooterRow.FindControl("btnbarcode"), Button).Visible = False
        Else
            GrdStaff.DataSource = ds
            GrdStaff.DataBind()

            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

            For Each row As GridViewRow In GrdStaff.Rows
                Dim emp_id = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                Dim GridMemberships As GridView = DirectCast(row.FindControl("GridMemberships"), GridView)
                Dim query = " SELECT RECORD_ID,LIBRARY_DIVISION_DES,MEMBERSHIP_DES FROM  LIBRARY_MEMBERSHIP_USERS A" & _
                            " INNER JOIN LIBRARY_MEMBERSHIPS B on A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                            " INNER JOIN LIBRARY_DIVISIONS C ON C.LIBRARY_DIVISION_ID=A.LIBRARY_DIVISION_ID  WHERE A.USER_TYPE='EMPLOYEE' AND USER_ID='" & emp_id & "'"
                GridMemberships.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

                GridMemberships.DataBind()

            Next

            DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button).Visible = True
            'DirectCast(GrdStaff.FooterRow.FindControl("btnbarcode"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStaff.FooterRow.FindControl("btnaddmembers"), Button))
            'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStaff.FooterRow.FindControl("btnbarcode"), Button))

        End If

        If GrdStaff.Rows.Count > 0 Then

            DirectCast(GrdStaff.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStaff.HeaderRow.FindControl("txtname"), TextBox).Text = txtname

        End If

    End Sub


    Protected Sub ddesschange(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrid()
    End Sub


    Protected Sub GrdStaff_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStaff.PageIndexChanging

        GrdStaff.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStaff.RowCommand

        If e.CommandName = "Deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim Record_id = e.CommandArgument
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            Try
                '' Check if item taken for this membership
                Dim sqlquery = " SELECT * FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
                               " INNER JOIN dbo.LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
                               " INNER JOIN dbo.LIBRARY_TRANSACTIONS C ON C.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
                               " WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND A.RECORD_ID='" & Record_id & "' " & _
                               " AND C.USER_ID=(SELECT USER_ID FROM dbo.LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' AND USER_TYPE='EMPLOYEE') "

                Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sqlquery)

                If ds.Tables(0).Rows.Count > 0 Then
                    lblMessage.Text = "Library Memebership is in use. <br> " & ds.Tables(0).Rows.Count & " Item(s) has been taken by this user using this membership. Record cannot be deleted.Return the item(s) and proceed with deletion."
                Else
                    Dim query = "DELETE LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' "
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                    transaction.Commit()
                    lblMessage.Text = "User Membership Deleted Successfully."
                End If

            Catch ex As Exception
                lblMessage.Text = "Error while transaction. Error :" & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

            MO1.Show()

            BindGrid()

        End If
        If e.CommandName = "barcode" Then

            HiddenShowFlag1.Value = 1
            Dim hash As New Hashtable

            If Session("Cardhashtable") Is Nothing Then
            Else
                hash = Session("Cardhashtable")
            End If

            For Each row As GridViewRow In GrdStaff.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("CheckBar"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("Hiddenempid"), HiddenField)
                Dim key = Hid.Value
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, Hid.Value)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next

            If DirectCast(GrdStaff.FooterRow.FindControl("CheckIssueAll"), CheckBox).Checked Then

                hash.Clear()

                Dim i = 0

                Dim ds As DataSet = GetData()
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    Dim key = ds.Tables(0).Rows(i).Item("EMP_ID").ToString()

                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, key)
                    End If
                Next

            End If

            If hash.Count = 0 Then
                HiddenShowFlag1.Value = 0
            End If
            Session("Cardhashtable") = hash

        End If

    End Sub


    Protected Sub GrdStaff_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStaff.RowCommand

        If e.CommandName = "assign" Then
            MO3.Show()
        End If

        If e.CommandName = "search" Then
            BindGrid()
        End If

    End Sub

    Protected Sub ddlibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindMemberships()
        MO3.Show()
    End Sub

    Protected Sub btncancel4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO3.Hide()
    End Sub

    Protected Sub btnStatusupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim flag = 0

            For Each row As GridViewRow In GrdStaff.Rows

                Dim check As CheckBox = DirectCast(row.FindControl("Checklist"), CheckBox)

                If check.Checked Then

                    flag = 1
                    Dim EMPID = DirectCast(row.FindControl("Hiddenempid"), HiddenField).Value
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@USER_ID", EMPID)
                    pParms(1) = New SqlClient.SqlParameter("@LIB_MEM_ID", ddMemberships.SelectedValue)
                    pParms(2) = New SqlClient.SqlParameter("@USER_TYPE", "EMPLOYEE")
                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_MEMBERSHIPS", pParms)

                End If

            Next

            transaction.Commit()

            If flag = 0 Then
                lblMessage.Text = "Please select employee."
            Else
                'BindStudentView()
            End If

        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

        MO3.Hide()
        MO1.Show()

        BindGrid()
    End Sub

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub

    Protected Sub Dropcategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dropcategory.SelectedIndexChanged
        BindGrid()
    End Sub
    Protected Sub btnupdatebulk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdatebulk.Click
        HiddenBulkUpdate.Value = 1
        MO3.Show()
        ShowUpdateButton()
    End Sub


    Public Sub ShowUpdateButton()
        If HiddenBulkUpdate.Value = 0 Then
            btnStatusupdate.Visible = True
            btnStatusupdate2.Visible = False
        Else
            btnStatusupdate.Visible = False
            btnStatusupdate2.Visible = True

        End If
    End Sub

    Protected Sub btnStatusupdate2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim ds As DataSet = GetData()

        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1

                Dim empid = ds.Tables(0).Rows(i).Item("EMP_ID").ToString()
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USER_ID", empid)
                pParms(1) = New SqlClient.SqlParameter("@LIB_MEM_ID", ddMemberships.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@USER_TYPE", "EMPLOYEE")
                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_MEMBERSHIPS", pParms)

            Next
            transaction.Commit()
            lblMessage.Text = "Bulk - User Membership Updated Successfully."
            BindGrid()

        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()
        End Try

        MO1.Show()

    End Sub
End Class
