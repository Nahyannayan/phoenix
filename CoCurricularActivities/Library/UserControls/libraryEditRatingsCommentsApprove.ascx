<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryEditRatingsCommentsApprove.ascx.vb" Inherits="Library_UserControls_libraryEditRatingsCommentsApprove" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%--<link href="../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />--%>
<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">User Comments and Ratings Approval</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridComments" runat="server" AutoGenerateColumns="false" EmptyDataText="No User Comments added yet." AllowPaging="True" Width="100%" ShowFooter="True" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Comment ID
                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                    <%#Eval("COMMENT_ID")%>
                    <asp:HiddenField ID="HiddenCommentId" Value='<%#Eval("COMMENT_ID")%>' runat="server" />
                </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Comments
                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="T5lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                        <asp:Panel ID="T5Panel1" runat="server">
                                            <%#Eval("COMMENTS")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T5lblview"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T5Panel1"
                                            TextLabelID="T5lblview">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Ratings
                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                    <ajaxToolkit:Rating ID="CRatings" runat="server" CurrentRating='<%# Eval("AVG_RATING") %>'
                        EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar" MaxRating="5"
                        ReadOnly="true" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
                    </ajaxToolkit:Rating>
                </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Date
                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
               <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Approved
                     
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                <asp:Image ID="Image1" ImageUrl='<%#Eval("APPROVED")%>' runat="server" />
            </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Approval
                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                <asp:CheckBox ID="CheckApprove" runat="server" />
            </center>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <center>
            <asp:RadioButtonList ID="RadioApprovalSelect"  runat="server">
            <asp:ListItem  Text="Approve"  Selected="True"  Value="True" > </asp:ListItem>
            <asp:ListItem  Text="Disapprove" Value="False"> </asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Button ID="btnUpdate" CssClass="button" Width="86px" OnClick="btnUpdate_Click" CausesValidation="false"   runat="server" Text="Update" /></center>
                                    </FooterTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>


                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HiddenMasterId" runat="server" />
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
    <asp:Label ID="Label12" runat="server"></asp:Label><br />
    <asp:Panel ID="PanelMessage2" runat="server" CssClass="panel-cover" Style="display: none" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">Library Message</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblMessage" runat="server" CssClass="text-danger"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnmok2" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Ok" ValidationGroup="s" OnClick="btnmok2_Click" /></td>
                                </tr>
                            </table>
                            &nbsp;
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="MO12" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnmok2" DropShadow="true" PopupControlID="PanelMessage2" RepositionMode="RepositionOnWindowResizeAndScroll"
        TargetControlID="Label12">
    </ajaxToolkit:ModalPopupExtender>
</div>
