﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_UserControls_libraryEditOption
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Hiddenbsu_id.Value = Session("sbsuid")
            HiddenStock_id.Value = Request.QueryString("Stock_id")
            BindLibraryDivisions()
            BindStatus()
            Dim val = Request.QueryString("Option")

            If val = 3 Then
                Panel1.Visible = True
                Panel2.Visible = False
            End If

            If val = 4 Then
                Panel1.Visible = False
                Panel2.Visible = True
            End If

        End If

    End Sub
    Public Sub BindStatus()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT STATUS_ID,STATUS_DESCRIPTION FROM LIBRARY_ITEM_STATUS WHERE STATUS_ID != 4 "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddStatus.DataSource = ds
        ddStatus.DataTextField = "STATUS_DESCRIPTION"
        ddStatus.DataValueField = "STATUS_ID"
        ddStatus.DataBind()


    End Sub
    Public Sub BindLibraryDivisions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & Hiddenbsu_id.Value & "' ORDER BY LIBRARY_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddLibraryDivisions.DataSource = ds
        ddLibraryDivisions.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibraryDivisions.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibraryDivisions.DataBind()

        BindLibrarySubDivisions()

    End Sub

    Public Sub BindLibrarySubDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SUB_DIVISIONS " & _
                        "WHERE LIBRARY_DIVISION_ID='" & ddLibraryDivisions.SelectedValue & "' ORDER BY LIBRARY_SUB_DIVISION_DES DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrarySubDivisions.DataSource = ds
        ddLibrarySubDivisions.DataTextField = "LIBRARY_SUB_DIVISION_DES"
        ddLibrarySubDivisions.DataValueField = "LIBRARY_SUB_DIVISION_ID"
        ddLibrarySubDivisions.DataBind()

        BindShelfS()

    End Sub


    Public Sub BindShelfS()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_SHELFS " & _
                        "WHERE LIBRARY_SUB_DIVISION_ID='" & ddLibrarySubDivisions.SelectedValue & "' ORDER BY SHELF_NAME DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddShelfName.DataSource = ds
        ddShelfName.DataTextField = "SHELF_NAME"
        ddShelfName.DataValueField = "SHELF_ID"
        ddShelfName.DataBind()

        BindRacks()

    End Sub

    Public Sub BindRacks()
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "select RACK_ID,RACK_NAME from  dbo.LIBRARY_RACKS where SHELF_ID='" & ddShelfName.SelectedValue & "' ORDER BY RACK_NAME DESC "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddRacks.DataSource = ds
        ddRacks.DataTextField = "RACK_NAME"
        ddRacks.DataValueField = "RACK_ID"
        ddRacks.DataBind()

        Dim list As New ListItem
        list.Value = "0"
        list.Text = "No Rack"
        ddRacks.Items.Insert(0, list)


    End Sub

    Protected Sub ddLibraryDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindLibrarySubDivisions()

    End Sub

    Protected Sub ddLibrarySubDivisions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindShelfS()

    End Sub

    Protected Sub ddShelfName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindRacks()

    End Sub


    Protected Sub btnstockupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdaterack.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try

            Dim stock As String() = HiddenStock_id.Value.Split(",")
            Dim flag = 0
            Dim i = 0
            For i = 0 To stock.Length - 1

                If stock(i).Trim() <> "" Then
                    flag = 1
                    Dim pParms(3) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stock(i).Trim())
                    If ddRacks.SelectedValue <> "0" Then
                        pParms(1) = New SqlClient.SqlParameter("@RACK_ID", ddRacks.SelectedValue)
                    End If
                    lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_BOOK_RACK", pParms)
                End If

            Next

            transaction.Commit()
            If flag = 1 Then
                'lblmessage.Text = "Rack assigned successfully."
            Else
                lblmessage.Text = "Please select Items.Stock not found."
            End If


        Catch ex As Exception

            lblmessage.Text = "Error occurred while transactions." & ex.Message
            transaction.Rollback()

        Finally

            connection.Close()

        End Try

    End Sub

    Protected Sub btnStatusupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatusupdate.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()

        Try

            Dim stock As String() = HiddenStock_id.Value.Split(",")
            Dim flag = 0
            Dim i = 0
            For i = 0 To stock.Length - 1
                flag = 1
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STOCK_ID", stock(i).Trim())
                pParms(1) = New SqlClient.SqlParameter("@STATUS_ID", ddStatus.SelectedValue)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_ITEM_STATUS", pParms)

            Next

            transaction.Commit()

            If flag = 1 Then
                lblmessage.Text = "Status updated successfully."
            Else
                lblmessage.Text = "Please select Items.Stock not found."
            End If

        Catch ex As Exception
            lblmessage.Text = "Error occurred while transactions." & ex.Message
            transaction.Rollback()
        Finally
            connection.Close()

        End Try

    End Sub
End Class
