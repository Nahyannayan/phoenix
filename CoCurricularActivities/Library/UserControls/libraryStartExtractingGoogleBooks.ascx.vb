﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports GoogleBooks

Partial Class Library_UserControls_libraryStartExtractingGoogleBooks
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("sBusper").ToString() = "True" Then

        If Not IsPostBack Then
            HiddenBsuid.Value = Session("sbsuid")
        End If
        GetData()
        GetCount()

        'End If

    End Sub

    Public Sub GetCount()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = ""

        'Pending retrieving 
        str_query = " select count(*) from dbo.LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) where APPROVED='False' and ENTRY_BSU_ID='" & HiddenBsuid.Value & "' and PRIMARY_MASTER_ID is null and STARTED='False' AND ERROR_UPLOAD='FALSE' AND GOOGLE_BOOKS='True' "
        Label1.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_query = "select count(*) from dbo.LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) where APPROVED='False' and ENTRY_BSU_ID='" & HiddenBsuid.Value & "' and PRIMARY_MASTER_ID is null AND ERROR_UPLOAD='FALSE' AND GOOGLE_BOOKS='True'"
        Label2.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        'Total error while retrieving    
        str_query = "select count(*) from dbo.LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) where APPROVED='False' and ENTRY_BSU_ID='" & HiddenBsuid.Value & "' and PRIMARY_MASTER_ID is null and ERROR_DATA_RETRIEVE='True' and STARTED='True' AND GOOGLE_BOOKS='True'"
        Label3.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        'Total success while retrieving
        str_query = "select count(*) from dbo.LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) where APPROVED='False' and ENTRY_BSU_ID='" & HiddenBsuid.Value & "' and PRIMARY_MASTER_ID is null and SUCCESS_DATA_RETRIEVE='True'  and STARTED='True' AND GOOGLE_BOOKS='True'"
        Label4.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If Label1.Text = 0 Then
            lblstatus.Text = "Finished"
            TD1.Visible = False
            TD2.Visible = False
        Else
            lblstatus.Text = "On Progress...."
            TD1.Visible = True
            TD2.Visible = True
        End If


    End Sub

    Public Function PreviousExtraction(ByVal transaction As SqlTransaction, ByRef hash As Hashtable, ByVal ISBN As String, ByVal ISBN2 As String) As Boolean
        Dim returnvalue = False

        Dim str_query = ""

        If ISBN <> "" Or ISBN2 <> "" Then

            If ISBN <> "" Then
                str_query = "SELECT * FROM LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE (UPLOAD_ISBN LIKE '% " & ISBN & " %') AND STARTED='True' AND SUCCESS_DATA_RETRIEVE='1' and ENTRY_BSU_ID='" & HiddenBsuid.Value & "' ORDER BY UPLOAD_ISBN desc,UPLOAD_ISBN2 desc"
            Else
                str_query = "SELECT * FROM LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE (UPLOAD_ISBN2 LIKE '% " & ISBN2 & " %') AND STARTED='True' AND SUCCESS_DATA_RETRIEVE='1' and ENTRY_BSU_ID='" & HiddenBsuid.Value & "' ORDER BY UPLOAD_ISBN desc,UPLOAD_ISBN2 desc"
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = True
                hash.Item("DataFound") = "YES"
                hash.Item("title") = ds.Tables(0).Rows(0).Item("ITEM_TITLE").ToString()
                hash.Item("Author") = ds.Tables(0).Rows(0).Item("AUTHOR").ToString()
                hash.Item("isbndata") = ds.Tables(0).Rows(0).Item("ISBN").ToString()
                hash.Item("ImageUrl") = ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL").ToString()
                hash.Item("ISBNEnc") = ds.Tables(0).Rows(0).Item("ISBN_ENC").ToString()
                hash.Item("Publisher") = ds.Tables(0).Rows(0).Item("PUBLISHER").ToString()
                hash.Item("Year") = ds.Tables(0).Rows(0).Item("DATE_YEAR").ToString()
                hash.Item("Pages") = ds.Tables(0).Rows(0).Item("FORMAT").ToString()
                hash.Item("Subject") = ds.Tables(0).Rows(0).Item("SUBJECT").ToString()
                hash.Item("Description") = ds.Tables(0).Rows(0).Item("PRODUCT_DESCRIPTION").ToString()
                hash.Item("InfoUrl") = ds.Tables(0).Rows(0).Item("PRODUCT_URL").ToString()

            End If

        End If
       


        Return returnvalue
    End Function

    Public Sub GetData()
        LibraryData.isOffLine(Session("sBusper"))
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT TOP 1 RECORD_ID,UPLOAD_ISBN,UPLOAD_ISBN2 FROM LIBRARY_ITEMS_MASTER_EXCEL WITH (NOLOCK) WHERE ENTRY_BSU_ID='" & HiddenBsuid.Value & "' AND SUCCESS_DATA_RETRIEVE='False' and ERROR_UPLOAD='False' and APPROVED = 'False' AND STARTED='False' AND PRIMARY_MASTER_ID IS NULL AND GOOGLE_BOOKS='True' ORDER BY UPLOAD_ISBN desc,UPLOAD_ISBN2 desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim sql_query = ""
        Dim Record_id As String = ""
        Dim ISBN As String = ""
        Dim ISBN2 As String = ""
        Dim errorflag = 0

        ''Data Fields

        Dim ISBNEnc
        Dim title
        Dim isbndata
        Dim Author, Publisher, Year, Pages, Subject, Description, ImageUrl, InfoUrl


        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)

        If ds.Tables(0).Rows.Count > 0 Then
            Label8.Text = ""
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()

            Try
                Record_id = ds.Tables(0).Rows(0).Item("RECORD_ID").ToString()
                ISBN = ds.Tables(0).Rows(0).Item("UPLOAD_ISBN").ToString()
                ISBN2 = ds.Tables(0).Rows(0).Item("UPLOAD_ISBN2").ToString()
                sql_query = "Update LIBRARY_ITEMS_MASTER_EXCEL set STARTED='True' where RECORD_ID='" & Record_id & "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

                Dim hash As New Hashtable

                If PreviousExtraction(transaction, hash, ISBN, ISBN2) = False Then

                    hash = GoogleBooks.GoogleBooks.GoogleBookData(ISBN)
                    Dim rflag = hash.Item("DataFound")
                    If rflag <> "YES" Then
                        ISBN = ISBN2
                        hash = GoogleBooks.GoogleBooks.GoogleBookData(ISBN2)
                    End If


                End If

                Dim idictenum As IDictionaryEnumerator
                idictenum = hash.GetEnumerator()

                errorflag = 1

                While (idictenum.MoveNext())
                    Dim key = idictenum.Key
                    Dim id = idictenum.Value

                    If key = "DataFound" Then
                        If id = "YES" Then
                            errorflag = 0
                        End If
                    End If

                    If key = "title" And id <> "" Then
                        Label5.Text = id
                        title = id
                    End If

                    If key = "Author" And id <> "" Then
                        Label6.Text = id
                        Author = id
                    End If
                    If key = "isbndata" And id <> "" Then
                        Label7.Text = id
                        isbndata = id
                    End If
                    If key = "ImageUrl" Then
                        Image1.ImageUrl = id
                        ImageUrl = id
                    End If

                    If hash.ContainsKey("ImageUrl") = False Then
                        Image1.ImageUrl = "~\Images\Library\noImage.gif"
                    End If

                    ''Other Data
                    If key = "ISBNEnc" Then
                        ISBNEnc = id
                    End If
                    If key = "Publisher" Then
                        Publisher = id
                    End If
                    If key = "Year" Then
                        Year = id
                    End If
                    If key = "Pages" Then
                        Pages = id
                    End If
                    If key = "Subject" Then
                        Subject = id
                    End If
                    If key = "Description" Then
                        Description = id
                    End If
                    If key = "InfoUrl" Then
                        InfoUrl = id
                    End If


                End While
                ''Saving Info 

                Dim pParms(13) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RECORD_ID", Record_id)
                pParms(1) = New SqlClient.SqlParameter("@ITEM_TITLE", title)
                pParms(2) = New SqlClient.SqlParameter("@AUTHOR", Author)
                pParms(3) = New SqlClient.SqlParameter("@PUBLISHER", Publisher)
                pParms(4) = New SqlClient.SqlParameter("@DATE_YEAR", Year)
                pParms(5) = New SqlClient.SqlParameter("@ISBN", isbndata)
                pParms(6) = New SqlClient.SqlParameter("@ISBN_ENC", ISBN & "/" & ISBNEnc)
                pParms(7) = New SqlClient.SqlParameter("@FORMAT", Pages)
                pParms(8) = New SqlClient.SqlParameter("@SUBJECT", Subject)
                pParms(9) = New SqlClient.SqlParameter("@PRODUCT_URL", InfoUrl)
                pParms(10) = New SqlClient.SqlParameter("@PRODUCT_IMAGE_URL", ImageUrl)
                pParms(11) = New SqlClient.SqlParameter("@PRODUCT_DESCRIPTION", Description)
                pParms(12) = New SqlClient.SqlParameter("@ENTRY_TYPE", "Google Book-Provider-(Excel)")
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "UPDATE_LIBRARY_ITEMS_MASTER_EXCEL", pParms)

                If errorflag = "0" Then

                    ''Success Flag Setting
                    sql_query = "Update LIBRARY_ITEMS_MASTER_EXCEL set SUCCESS_DATA_RETRIEVE='True' where RECORD_ID='" & Record_id & "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

                Else
                    ''Error Flag Setting
                    sql_query = "Update LIBRARY_ITEMS_MASTER_EXCEL set ERROR_DATA_RETRIEVE='True', ERROR_DATA_MESSAGE='No data found for uploaded ISBN number. Please check the ISBN.' where RECORD_ID='" & Record_id & "'"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql_query)

                End If

                transaction.Commit()
                TD1.Visible = True
                TD2.Visible = True
            Catch ex As Exception
                transaction.Rollback()
                Label8.Text = "Error :" & ex.Message & ". While extracting : " & Record_id & " ISBN : & " & ISBN
                CheckStop.Checked = True
            Finally
                connection.Close()
            End Try

        Else
            TD1.Visible = False
            TD2.Visible = False
        End If

    End Sub


End Class
