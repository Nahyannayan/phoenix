Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_LibraryDetailViewStockDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenMasterID.Value = Request.QueryString("id")
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT STOCK_ID,ACCESSION_NO,CALL_NO,LIBRARY_DIVISION_DES,LIBRARY_SUB_DIVISION_DES,SHELF_ROW,SHELF_NAME,RACK_NAME,STATUS_DESCRIPTION " & _
                        " ,CASE (SELECT TOP 1 ISNULL(ITEM_ACTUAL_RETURN_DATE,'') FROM dbo.LIBRARY_TRANSACTIONS WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND  STOCK_ID=A.STOCK_ID)  WHEN '' THEN '~/Images/cross.png' ELSE '~/Images/tick.gif' end AVALABLE " & _
                        " FROM  dbo.LIBRARY_ITEMS_QUANTITY A " & _
                        " LEFT JOIN dbo.LIBRARY_RACKS B ON A.RACK_ID=B.RACK_ID " & _
                        " LEFT JOIN dbo.LIBRARY_SHELFS C ON C.SHELF_ID=B.SHELF_ID " & _
                        " LEFT JOIN dbo.LIBRARY_SUB_DIVISIONS  D ON D.LIBRARY_SUB_DIVISION_ID=C.LIBRARY_SUB_DIVISION_ID " & _
                        " LEFT JOIN dbo.LIBRARY_DIVISIONS  E ON E.LIBRARY_DIVISION_ID=D.LIBRARY_DIVISION_ID " & _
                        " LEFT JOIN dbo.LIBRARY_ITEM_STATUS F ON F.STATUS_ID=A.STATUS_ID " & _
                        " WHERE A.MASTER_ID='" & HiddenMasterID.Value & "' AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridStocks.DataSource = ds
        GridStocks.DataBind()

    End Sub


End Class
