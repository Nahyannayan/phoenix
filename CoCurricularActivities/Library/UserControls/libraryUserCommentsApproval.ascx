﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryUserCommentsApproval.ascx.vb" Inherits="Library_UserControls_libraryUserCommentsApproval" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">

<div>


    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">Search</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"> <span class="field-label">Master ID</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtMasterId" runat="server"></asp:TextBox></td>
                        <td align="left" width="20%"> <span class="field-label">Item Title</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"> <span class="field-label">Author</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtAuthor" runat="server"></asp:TextBox></td>
                        <td align="left"> <span class="field-label">ISBN</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtISBN" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left"> <span class="field-label">Publisher</span></td>

                        <td align="left">
                            <asp:TextBox ID="txtPublisher" runat="server"></asp:TextBox></td>
                        <td align="left"> <span class="field-label">Status</span></td>

                        <td align="left">
                            <asp:DropDownList ID="ddStatus" runat="server">
                                <asp:ListItem Text="All" Selected="True" Value="0"> </asp:ListItem>
                                <asp:ListItem Text="Approved" Value="True"> </asp:ListItem>
                                <asp:ListItem Text="Disapproved" Value="False"> </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click" Text="Search" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg-lite">User Comments Approval</td>
        </tr>
        <tr>
            <td>

                <asp:GridView ID="GridItem" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-row"
                    EmptyDataText="No Records Found" ShowFooter="true" Width="100%" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField>

                            <HeaderTemplate>
                                Comment ID
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
<%#Eval("COMMENT_ID")%>
</center>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Item Image
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>

    <asp:HiddenField ID="HiddenCommentId" Value='<%#Eval("COMMENT_ID")%>' runat="server" />
    <asp:ImageButton ID="ImageItem" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' OnClientClick="javascript:return false;"  runat="server" />

</center>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Item Type
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
    <%# Eval("ITEM_DES") %>
   
</center>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Item Title
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Eval("ITEM_TITLE") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Author
                                      
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("AUTHOR") %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Publisher
                                      
                            </HeaderTemplate>
                            <ItemTemplate>

                                <%# Eval("PUBLISHER") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Comments
                                       
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Elblview" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                <asp:Panel ID="E4Panel1" runat="server" Height="50px">
                                    <%#Eval("COMMENTS")%>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="Elblview"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel1"
                                    TextLabelID="Elblview">
                                </ajaxToolkit:CollapsiblePanelExtender>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Approved
                                      
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>

   <asp:Image ID="Image1"  ImageUrl='~/Images/tick.gif' visible='<%#Eval("SHOW_TICK")%>' runat="server" />
   <asp:Image ID="Image2"  ImageUrl='~/Images/cross.png' visible='<%#Eval("SHOW_CROSS")%>' runat="server" />
    <asp:CheckBox ID="CheckApprove"  runat="server" />
    
</center>
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
            <asp:RadioButtonList ID="RadioApprovalSelect"  runat="server">
            <asp:ListItem  Text="Approve"  Selected="True"  Value="True" > </asp:ListItem>
            <asp:ListItem  Text="Disapprove" Value="False"> </asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Button ID="btnUpdate" CssClass="button" OnClick="btnUpdate_Click" CausesValidation="false"   runat="server" Text="Update" /></center>
                            </FooterTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="griditem" />
                    <EmptyDataRowStyle />
                    <SelectedRowStyle />
                    <HeaderStyle CssClass="gridheader_pop" />
                    <EditRowStyle />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>


            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>
