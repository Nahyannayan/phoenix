<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryDetailViewStockTransactions.ascx.vb" Inherits="Library_UserControls_LibraryDetailViewStockTransactions" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Transactions</td>
    </tr>
    <tr>
        <td>

            <asp:GridView ID="GrdTransaction" runat="server" AutoGenerateColumns="false" EmptyDataText="No Transactions done yet."
                CssClass="table table-bordered table-row" Width="100%" AllowPaging="True">
                <Columns>
                    <%--             <asp:TemplateField HeaderText="Stock ID">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Reference&nbsp;No
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("STOCK_ID")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Accession No">
                        <HeaderTemplate>
                            Accession No
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("ACCESSION_NO")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--  <asp:TemplateField HeaderText="Call No">
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Call No
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("CALL_NO")%>
                </center>
            </ItemTemplate>
        </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="User ID">
                        <HeaderTemplate>
                            User ID
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("USER_ID") %>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="User Name">
                        <HeaderTemplate>
                            User Name
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            Name: <%#Eval("USER_NAME") %>
                            <br />
                            <hr />
                            Type: <%#Eval("USER_TYPE") %>
                            <br />
                            <hr />
                            <%#Eval("MEMBERSHIP_DES") %>
                            <br />
                            <hr />
                            Grade: <%#Eval("GRADE_SEC") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <HeaderTemplate>
                            Status
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            Issue Status  : <%#Eval("ISSUE_STATUS") %>
                            <br />
                            <hr />
                            Return Status : <%#Eval("RETURN_STATUS") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Issue Period">
                        <HeaderTemplate>
                            Issue Period
                     
                        </HeaderTemplate>
                        <ItemTemplate>
                            Issue On      :<%#Eval("ITEM_TAKEN_DATE")%>
                            <br />
                            <hr />
                            Return Date   :<%#Eval("ITEM_RETURN_DATE")%>
                            <br />
                            <hr />
                            User Returned Date :<%#Eval("ITEM_ACTUAL_RETURN_DATE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reservation">
                        <HeaderTemplate>
                            Reservation
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <asp:Image ID="Image3" ImageUrl='<%#Eval("RESERVATION")%>' runat="server" />
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Issue Notes">
                        <HeaderTemplate>
                            Issue Notes
                     
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%#Eval("NOTES") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Return Notes">
                        <HeaderTemplate>
                            Return Notes
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("RETURN_NOTES") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Fine">
                        <HeaderTemplate>
                            Fine
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                   <%#Eval("FINE_AMOUNT") %>  <%#Eval("CURRENCY_ID") %>
               </center>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Issued By">
                        <HeaderTemplate>
                            Issued By
                     
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%#Eval("ISSUED_BY") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Collected By">
                        <HeaderTemplate>
                            Collected By
                     
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("RECEIVED_BY") %>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>

<asp:HiddenField ID="HiddenBsuID" runat="server" />
<asp:HiddenField ID="HiddenMasterID" runat="server" />
