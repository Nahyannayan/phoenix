Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryItemTransactions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            BindLibraryDivisions()
        End If

    End Sub

    Public Sub BindLibraryDivisions()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = ""
        If Session("sBusper") = "True" Then

            str_query = "SELECT LIBRARY_DIVISION_ID,LIBRARY_DIVISION_DES FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "

        Else
            str_query = " SELECT B.LIBRARY_DIVISION_ID,B.LIBRARY_DIVISION_DES FROM dbo.LIBRARY_LIBRARIAN A " & _
                                  " INNER JOIN dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                                  " WHERE LIBRARIAN_EMP_ID='" & HiddenEmpid.Value & "' AND ACTIVE='True' AND LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' "

        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddLibrary.DataSource = ds
        ddLibrary.DataTextField = "LIBRARY_DIVISION_DES"
        ddLibrary.DataValueField = "LIBRARY_DIVISION_ID"
        ddLibrary.DataBind()

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows.Count > 1 Then
                Dim list As New ListItem
                list.Value = "0"
                list.Text = "Select a library"
                ddLibrary.Items.Insert(0, list)
            Else

                Call ddLibrary_SelectedIndexChanged(ddLibrary, Nothing)

            End If
        Else
            lblMessage.Text = "Login User is not a librarian. Please assign librarian and proceed further. "
            ddLibrary.Visible = False

        End If

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT DISTINCT A.MASTER_ID,ITEM_DES,ITEM_TITLE,AUTHOR,PUBLISHER,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN '~/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL ,PRODUCT_URL,  " & _
                        " 'javascript:Redirect(''' + CONVERT(VARCHAR,A.MASTER_ID) + '''); return false;' REDIRECT, " & _
                        " 'javascript:openPopup(''' + CONVERT(VARCHAR,A.MASTER_ID) + ''','''+ CONVERT(VARCHAR,G.LIBRARY_DIVISION_ID) + '''); return false;' as REDIRECTTRAN, " & _
                        " CASE ISNULL(PRODUCT_URL,'') WHEN '' THEN 'FALSE' ELSE 'TRUE' END SHOWURL, " & _
                        " '" & ddLibrary.SelectedValue & "' AS LIBRARY_DIVISION_ID_VAL " & _
                        " FROM  dbo.LIBRARY_ITEMS_MASTER A WITH (NOLOCK)" & _
                        " INNER JOIN dbo.LIBRARY_ITEMS_QUANTITY C WITH (NOLOCK) ON C.MASTER_ID= A.MASTER_ID " & _
                        " INNER JOIN  dbo.LIBRARY_ITEMS B WITH (NOLOCK) ON B.ITEM_ID= C.ITEM_ID"

        If ddLibrary.SelectedValue <> 0 Then
            str_query = str_query & _
                        " INNER JOIN dbo.LIBRARY_RACKS D ON D.RACK_ID=C.RACK_ID " & _
                        " INNER JOIN dbo.LIBRARY_SHELFS E ON E.SHELF_ID= D.SHELF_ID " & _
                        " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS F ON F.LIBRARY_SUB_DIVISION_ID= E.LIBRARY_SUB_DIVISION_ID " & _
                        " INNER JOIN dbo.LIBRARY_DIVISIONS G ON G.LIBRARY_DIVISION_ID =  F.LIBRARY_DIVISION_ID "

        End If

        str_query = str_query & " WHERE A.MASTER_ID IN  (SELECT DISTINCT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' AND ACTIVE='True') AND  PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'"

        If ddLibrary.SelectedIndex > 0 Then
            str_query = str_query & " AND F.LIBRARY_DIVISION_ID='" & ddLibrary.SelectedValue & "'"
        End If

        Dim Condition As String = ""

        If txtItemName.Text.Trim() <> "" Then

            Condition = Condition & " AND ITEM_TITLE LIKE '%" & txtItemName.Text.Trim() & "%'"

        End If


        If txtAuthor.Text.Trim() <> "" Then

            Condition = Condition & " AND AUTHOR LIKE '%" & txtAuthor.Text.Trim() & "%'"

        End If

        If txtPublisher.Text.Trim() <> "" Then

            Condition = Condition & " AND PUBLISHER LIKE '%" & txtPublisher.Text.Trim() & "%'"

        End If

        If txtISBN.Text.Trim() <> "" Then

            Condition = Condition & " AND ISBN LIKE '%" & txtISBN.Text.Trim() & "%'"

        End If

        If txtMasterId.Text.Trim() <> "" Then

            Condition = Condition & " AND A.MASTER_ID = '" & txtMasterId.Text.Trim() & "'"

        End If

        If txtstockid.Text.Trim() <> "" Then

            Condition = Condition & " AND C.STOCK_ID = '" & LibraryData.GetStockIDForAccessonNo(txtstockid.Text.Trim(), HiddenBsuID.Value) & "'"

        End If

        If txtcallno.Text.Trim() <> "" Then

            Condition = Condition & " AND C.CALL_NO = '" & txtcallno.Text.Trim() & "'"

        End If


        If Condition <> "" Then

            str_query = str_query & Condition

        End If
        str_query = str_query & " ORDER BY ITEM_TITLE"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GridItem.DataSource = ds
        GridItem.DataBind()

        lblTotal.Text = " Total Records : " & ds.Tables(0).Rows.Count


    End Sub

    'Public Sub BindGrid()
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

    '    Dim str_query = "SELECT *, (LIBCOUNT - TCOUNT) INCOUNT FROM (  SELECT DISTINCT A.MASTER_ID,ISBN,ITEM_TITLE,AUTHOR,PUBLISHER,PRODUCT_URL,CASE ISNULL(PRODUCT_IMAGE_URL,'') WHEN '' THEN '~/Images/Library/noImage.gif' ELSE PRODUCT_IMAGE_URL END PRODUCT_IMAGE_URL,ITEM_DES, " & _
    '                    " 'javascript:Redirect(''' + CONVERT(VARCHAR,A.MASTER_ID) + '''); return false;' REDIRECT, " & _
    '                    " CASE ISNULL(PRODUCT_URL,'') WHEN '' THEN 'FALSE' ELSE 'TRUE' END SHOWURL " & _
    '                    " ,'javascript:openPopup(''' + CONVERT(VARCHAR,A.MASTER_ID) + ''','''+ CONVERT(VARCHAR,G.LIBRARY_DIVISION_ID) + '''); return false;' as REDIRECTTRAN " & _
    '                    " ,(SELECT COUNT(*)  FROM dbo.LIBRARY_ITEMS_QUANTITY A1 " & _
    '                       " INNER JOIN dbo.LIBRARY_RACKS B1 ON A1.RACK_ID=B1.RACK_ID " & _
    '                       " INNER JOIN dbo.LIBRARY_SHELFS C1 ON B1.SHELF_ID=C1.SHELF_ID " & _
    '                       " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS D1 ON C1.LIBRARY_SUB_DIVISION_ID= D1.LIBRARY_SUB_DIVISION_ID " & _
    '                       " INNER JOIN dbo.LIBRARY_DIVISIONS E1 ON D1.LIBRARY_DIVISION_ID=E1.LIBRARY_DIVISION_ID " & _
    '                       " WHERE E1.LIBRARY_DIVISION_ID='" & ddLibrary.SelectedValue & "' AND A1.MASTER_ID=A.MASTER_ID AND A1.ACTIVE='True') LIBCOUNT " & _
    '                       " ,( " & _
    '                       " SELECT COUNT(*) FROM LIBRARY_ITEM_RESERVATION C1  " & _
    '                       " WHERE C1.MASTER_ID=A.MASTER_ID " & _
    '                       " AND isnull(RESERVE_START_DATE,'') != '' " & _
    '                       " AND isnull(RESERVE_END_DATE,'') != '' AND ISNULL(TRAN_RECORD_ID,'') = '' " & _
    '                       " AND C1.LIBRARY_DIVISION_ID='" & ddLibrary.SelectedValue & "' " & _
    '                       " )RCOUNT " & _
    '                       " ,( " & _
    '                       " SELECT count(*) FROM LIBRARY_TRANSACTIONS D1 " & _
    '                       " INNER JOIN dbo.LIBRARY_ITEMS_QUANTITY M1 ON M1.STOCK_ID=D1.STOCK_ID " & _
    '                       " WHERE M1.MASTER_ID= A.MASTER_ID " & _
    '                       " AND isnull(ITEM_ACTUAL_RETURN_DATE,'') = '' AND D1.LIBRARY_DIVISION_ID='" & ddLibrary.SelectedValue & "' " & _
    '                       " )TCOUNT" & _
    '                    " ,G.LIBRARY_DIVISION_ID " & _
    '                    " FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
    '                    " INNER JOIN dbo.LIBRARY_ITEMS_MASTER B ON A.MASTER_ID=B.MASTER_ID " & _
    '                    " INNER JOIN dbo.LIBRARY_ITEMS C ON C.ITEM_ID= (SELECT TOP 1 ISNULL(ITEM_ID,'1') FROM LIBRARY_ITEMS_QUANTITY WHERE MASTER_ID=B.MASTER_ID ) " & _
    '                    " INNER JOIN dbo.LIBRARY_RACKS D ON D.RACK_ID=A.RACK_ID " & _
    '                    " INNER JOIN dbo.LIBRARY_SHELFS E ON E.SHELF_ID=D.SHELF_ID " & _
    '                    " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS F ON F.LIBRARY_SUB_DIVISION_ID=E.LIBRARY_SUB_DIVISION_ID " & _
    '                    " INNER JOIN dbo.LIBRARY_DIVISIONS G ON G.LIBRARY_DIVISION_ID =  F.LIBRARY_DIVISION_ID " & _
    '                    " WHERE A.MASTER_ID IN (SELECT DISTINCT MASTER_ID FROM dbo.LIBRARY_ITEMS_QUANTITY WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' ) AND ISNULL(A.RACK_ID,'') !='' AND A.ACTIVE='True'" & _
    '                    "AND G.LIBRARY_DIVISION_ID='" & ddLibrary.SelectedValue & "' )GTABLE WHERE LIBRARY_DIVISION_ID='" & ddLibrary.SelectedValue & "' "


    '    Dim Condition As String = ""

    '    If txtItemName.Text.Trim() <> "" Then

    '        Condition = Condition & " AND ITEM_TITLE LIKE '%" & txtItemName.Text.Trim() & "%'"

    '    End If


    '    If txtAuthor.Text.Trim() <> "" Then

    '        Condition = Condition & " AND AUTHOR LIKE '%" & txtAuthor.Text.Trim() & "%'"

    '    End If

    '    If txtPublisher.Text.Trim() <> "" Then

    '        Condition = Condition & " AND PUBLISHER LIKE '%" & txtPublisher.Text.Trim() & "%'"

    '    End If

    '    If txtISBN.Text.Trim() <> "" Then

    '        Condition = Condition & " AND ISBN LIKE '%" & txtISBN.Text.Trim() & "%'"

    '    End If

    '    If txtMasterId.Text.Trim() <> "" Then

    '        Condition = Condition & " AND MASTER_ID = '" & txtMasterId.Text.Trim() & "'"

    '    End If

    '    'If txtBookId.Text.Trim() <> "" Then

    '    '    Condition = Condition & " AND STOCK_ID = '" & txtBookId.Text.Trim() & "'"

    '    'End If

    '    If Condition <> "" Then

    '        str_query = str_query & Condition '& " ORDER BY A.MASTER_ID,A.STOCK_ID "
    '    Else

    '        str_query = str_query '& " ORDER BY A.MASTER_ID,A.STOCK_ID "
    '    End If

    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    GridItem.DataSource = ds
    '    GridItem.DataBind()

    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Protected Sub GridItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridItem.PageIndexChanging
        GridItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub ddLibrary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If ddLibrary.SelectedValue <> "0" Then
            BindGrid()
            Panel1.Visible = False
            Panel2.Visible = True
        Else
            Panel2.Visible = False
        End If

    End Sub

End Class
