﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryQuickReturnTransactions_S.ascx.vb" Inherits="Library_UserControls_libraryQuickReturnTransactions_S" %>

 <%@ Register src="libraryTransactionReservationApproval.ascx" tagname="libraryTransactionReservationApproval" tagprefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


    <script type="text/javascript">

        function UserTransactions(usertype, userid) {
            alert("inside UserTransactions");
            var result;
            result = radopen('libraryDetailViewUserTransactions.aspx?UserType=' + usertype + '&UserId=' + userid, '', 'Height=800px,Width=1020px,scrollbars=yes,resizable=no,directories=yes', "pop_up");
            return false;

        }

        function Rview() {
            alert("inside Rview");
            var result;
            var accno = document.getElementById('<%= HiddenAccessionReserve.ClientID %>').value;
            result = radopen("libraryTransactionReservationApproval2.aspx?accno=" + accno, "pop_up2");
            return false;
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

 <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="800px" >
            </telerik:RadWindow>
        </Windows>
      <Windows>
            <telerik:RadWindow ID="pop_up2" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="800px" >
            </telerik:RadWindow>
        </Windows>
     </telerik:RadWindowManager>


 <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <tr>
                <td class="title-bg-lite">
                    Item Return Transactions
                </td>
            </tr>
            <tr>
               <td align="left">
<table>
                        <tr>
                           <td align="left">
                              <span class="field-label">  User Details</span>
                            </td>
                           
                           <td align="left">
                                <asp:Label ID="lblReturnUserDetails" runat="server" CssClass="field-value"></asp:Label>
                            </td>
                            <td rowspan="11" align="left">
                                <asp:GridView ID="GridItem" runat="server" AutoGenerateColumns="false" ShowFooter="false"
                                    Width="100%"  CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Check">
                                            <HeaderTemplate>
                                                <center>
                                                    <%--<asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);"
                                                            ToolTip="Click here to select/deselect all rows (Deleting Items)" />--%>
                                                </center>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:CheckBox ID="ch1" runat="server" Checked='True' 
                                                        Text="" />
                                                </center>
                                            </ItemTemplate>
                                            <%--<FooterTemplate>
                                                <center>
                                                    <asp:Button ID="btnreturn" runat="server" CssClass="button" OnClick="btnReturnPouch_Click"
                                                        Text="Confirm" ToolTip="Return selected items" Width="40px" />
                                                </center>
                                            </FooterTemplate>--%>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                               
                                                            Accession No
                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:HiddenField ID="HiddenBookId" runat="server" Value='<%# Eval("STOCK_ID") %>' />
                                                    <asp:HiddenField ID="HiddenStockId" runat="server" Value='<%#Eval("ACCESSION_NO")%>' />
                                                    <asp:HiddenField ID="HiddenTranRecordid" runat="server" Value='<%#Eval("TRAN_RECORD_ID")%>' />
                                                    <%#Eval("STOCK_ID")%>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                
                                                            Item Image
                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:HiddenField ID="HiddenMasterId" runat="server" Value='<%# Eval("MASTER_ID") %>' />
                                                    <asp:Image ID="Image" runat="server" Height="50px" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>'
                                                        Width="40px" />
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                
                                                            Call No
                                                       
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%#Eval("CALL_NO")%>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                
                                                            Status
                                                     
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                                    <%#Eval("STATUS_DESCRIPTION")%>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem"  />
                                    <EmptyDataRowStyle  />
                                    <SelectedRowStyle  />
                                    <HeaderStyle  />
                                    <EditRowStyle  />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span class="field-label"> Membership Type</span>
                            </td>
                            
                            <td align="left">
                                <asp:Label ID="lblReturnMembershipType" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span class="field-label"> View History</span>
                            </td>
                            
                            <td align="left">
                                <asp:LinkButton ID="LinkReturnHistory" runat="server">History</asp:LinkButton>
                                &nbsp;of this user
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span class="field-label"> Item Taken On</span>
                            </td>
                           
                            <td align="left">
                                <asp:Label ID="lblReturnTakenon" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span class="field-label"> Tentative Return Date</span>
                            </td>
                           
                            <td align="left">
                                <asp:Label ID="lblReturnReturnDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span class="field-label"> Item Issue Status</span>
                            </td>
                           
                            <td align="left">
                                <asp:Label ID="lblreturnIssueStatus" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span class="field-label">  Issue Notes</span>
                            </td>
                           
                            <td align="left">
                                <asp:Label ID="lblreturnIssueNotes" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                           <td align="left">
                               <span class="field-label"> Item Status</span>
                            </td>
                            
                            <td align="left">
                                <asp:DropDownList ID="ddreturnstatusentry" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span class="field-label">  Notes</span>
                            </td>
                           
                            <td align="left">
                                <asp:TextBox ID="txtReturnnotes" runat="server" EnableTheming="False" 
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span class="field-label">  Fine</span>
                            </td>
                           
                            <td align="left">
                                <asp:TextBox ID="txtreturnfineamount" runat="server" >0</asp:TextBox>
                                <asp:DropDownList ID="ddReturncurrency" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                         <tr>
                           <td align="left">
                              <span class="field-label">  Reservations</span>
                            </td>
                           
                            <td align="left">
                                
                                <asp:LinkButton ID="lnkreservations"  runat="server">View</asp:LinkButton>
                                
                            </td>
                        </tr>

                        <tr>
                            <td align="left">
                            </td>
                          
                            <td align="left">
                                <asp:Button ID="btnReturn" runat="server" CssClass="button" Text="Confirm" />
                            </td>
                        </tr>
                    </table>
 </td>
 </tr> 
 <tr>
 <td>
 
     <uc1:libraryTransactionReservationApproval ID="libraryTransactionReservationApproval1" 
         runat="server" />
 
 </td>
 </tr>
 </table>  

                       <asp:Label ID="Label1" runat="server"></asp:Label>
    <asp:Panel ID="PanelStatusupdate"
        runat="server" CssClass="panel-cover" Style="display: none">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="title-bg-lite">
                    Library Message
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table>
                                <tbody>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnmok" OnClick="btnmessageok_Click" runat="server" Text="Ok" CssClass="button"
                                                CausesValidation="False" ValidationGroup="s" ></asp:Button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            &nbsp;
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" 
    BackgroundCssClass="modalBackground" DropShadow="true" 
    PopupControlID="PanelStatusupdate" 
    RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="Label1">
</ajaxToolkit:ModalPopupExtender>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
<asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
<asp:HiddenField ID="HiddenUserID" runat="server" />
<asp:HiddenField ID="HiddenEmpid" runat="server" />
<asp:HiddenField ID="HiddenRecordID" runat="server" />
<asp:HiddenField ID="HiddenAccessionNo" runat="server" />
<asp:HiddenField ID="HiddenUserType" runat="server" />
<asp:HiddenField ID="HiddenUserNo" runat="server" />
<asp:HiddenField ID="HiddenAccessionReserve" runat="server" />

