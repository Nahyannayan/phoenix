Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryTransaction
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HiddenMasterID.Value = Request.QueryString("Masterid").ToString()
            Hiddenlibrarydivid.Value = Request.QueryString("Libraryid").ToString()

            If Request.QueryString("user") <> "" Then
                HT4.Enabled = False
                T1.Visible = False
                T2.Visible = False
                RadioUserReservation.SelectedValue = Request.QueryString("user")
                txtUserreservation.Text = Request.QueryString("user_no")
                RadioUserReservation.Enabled = False
                txtUserreservation.Enabled = False
                BindEmailid()
            End If

            HiddenBsuID.Value = Session("sbsuid")
            HiddenEmpid.Value = Session("EmployeeId")
            HiddenUserID.Value = ""

            ''Cancel Reservation Tab
            BindReservation()

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Public Function CheckReservationCount() As Boolean
        Dim rval = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT  COUNT(USER_ID) COUNT,LIBRARY_ONLINE_RESERVER_COUNT FROM VIEW_USER_RESERVATION_HISTORY A " & _
                        " INNER JOIN  dbo.LIBRARY_DIVISIONS B ON A.LIBRARY_DIVISION_ID=B.LIBRARY_DIVISION_ID " & _
                        " WHERE [RESERVE_START_DATE] IS NULL AND [RESERVE_END_DATE] IS NULL AND USER_ID='" & txtUserreservation.Text.Trim() & "' AND USER_TYPE='" & RadioUserReservation.SelectedValue & "' AND TRAN_RECORD_ID IS NULL AND " & _
                        " A.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "'  GROUP BY A.LIBRARY_DIVISION_ID,LIBRARY_ONLINE_RESERVER_COUNT "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("COUNT").ToString() >= ds.Tables(0).Rows(0).Item("LIBRARY_ONLINE_RESERVER_COUNT").ToString() Then
                rval = False
            End If
        End If

        If rval = False Then
            lblMessage.Text = "Student had exceeded the reservation count"
        End If

        Return rval
    End Function

    Protected Sub btnmessageok_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MO1.Hide()
    End Sub


    'Public Function Reservations() As Boolean
    '    Dim returnvalue As Boolean = True
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
    '    Dim Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
    '                    " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'" & _
    '                    " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID='" & HiddenMasterID.Value & "'" & _
    '                    " AND USER_ID != '" & HiddenUserID.Value & "' AND REPLACE(CONVERT(VARCHAR(11), GETDATE() , 106), ' ', '/') BETWEEN REPLACE(CONVERT(VARCHAR(11), RESERVE_START_DATE , 106), ' ', '/') AND REPLACE(CONVERT(VARCHAR(11), RESERVE_END_DATE , 106), ' ', '/')  AND RESERVATION_CANCEL='FALSE' "

    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

    '    If ds.Tables(0).Rows.Count > 0 Then

    '        '' Check if current user has reserved this item
    '        Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
    '                    " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'" & _
    '                    " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID='" & HiddenMasterID.Value & "'" & _
    '                    " AND USER_ID = '" & HiddenUserID.Value & "' AND USER_TYPE='" & RadioUserReservation.SelectedValue & "' AND REPLACE(CONVERT(VARCHAR(11), GETDATE() , 106), ' ', '/') BETWEEN REPLACE(CONVERT(VARCHAR(11), RESERVE_START_DATE , 106), ' ', '/') AND REPLACE(CONVERT(VARCHAR(11), RESERVE_END_DATE , 106), ' ', '/')  AND RESERVATION_CANCEL='FALSE' "

    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

    '        If ds.Tables(0).Rows.Count > 0 Then
    '            HiddenReservationId.Value = ds.Tables(0).Rows(0).Item("RESERVATION_ID").ToString()
    '        Else

    '            ''Check for excess stock for circulation available.
    '            Dim remitems = 0
    '            Sql_Query = " select count(*)LIBCOUNT, " & _
    '                            " (SELECT COUNT(*) FROM LIBRARY_ITEM_RESERVATION C WHERE C.MASTER_ID = A.MASTER_ID  AND isnull(RESERVE_START_DATE,'') != '' AND isnull(RESERVE_END_DATE,'') != '' AND ISNULL(TRAN_RECORD_ID,'') = '' ) RCOUNT " & _
    '                            " from dbo.LIBRARY_ITEMS_QUANTITY A " & _
    '                            " left JOIN dbo.LIBRARY_TRANSACTIONS B ON B.STOCK_ID=A.STOCK_ID  " & _
    '                            " where A.MASTER_ID='" & HiddenMasterID.Value & "'" & _
    '                            " AND PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' AND ISNULL(RACK_ID,'') != '' " & _
    '                            " AND A.STOCK_ID NOT IN (SELECT STOCK_ID FROM dbo.LIBRARY_TRANSACTIONS WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'') = '') " & _
    '                            " GROUP BY A.MASTER_ID "


    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
    '            Dim lcount = ds.Tables(0).Rows(0).Item("LIBCOUNT").ToString()
    '            Dim rcount = ds.Tables(0).Rows(0).Item("RCOUNT").ToString()
    '            remitems = lcount - rcount

    '            If remitems > 0 Then
    '                '' has excess stock
    '                returnvalue = True
    '            Else
    '                lblMessage.Text = "This item has been reserved by another user.Please view the reservation history."
    '                MO1.Show()

    '                returnvalue = False
    '            End If

    '        End If
    '    Else

    '        returnvalue = True
    '    End If


    '    Return returnvalue
    'End Function


    'Public Function Circulation() As Boolean

    '    Dim returnvalue As Boolean = True

    '    ''Check Circulatory Status
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
    '    Dim Sql_Query = " SELECT CASE CIRCULATORY WHEN 'False' THEN STATUS_DESCRIPTION ELSE '' END STATUS FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
    '                    " INNER JOIN  dbo.LIBRARY_ITEM_STATUS B ON A.STATUS_ID=B.STATUS_ID " & _
    '                    " WHERE STOCK_ID='" & HiddenMasterID.Value & "' AND CIRCULATORY='FALSE' "
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        lblMessage.Text = "This item has been, " & ds.Tables(0).Rows(0).Item("STATUS").ToString() & " .<br>Item cannot be circulated."
    '        returnvalue = False
    '    Else
    '        returnvalue = True
    '    End If

    '    Return returnvalue

    'End Function


    Public Function SendMessageIfItemExists(ByRef transaction As SqlTransaction) As Integer

        ''This code is defined in mail scheduled

        Dim messageflag = 0

        Try

            '' Check if Book Exists in library,if yes then update the reserve start date as today.
            Dim remitems = 0

            Dim sql_query = " SELECT * FROM ( " & _
                            " select " & _
                            " (SELECT COUNT(*)  FROM dbo.LIBRARY_ITEMS_QUANTITY A " & _
                            " INNER JOIN dbo.LIBRARY_RACKS B ON A.RACK_ID=B.RACK_ID " & _
                            " INNER JOIN dbo.LIBRARY_SHELFS C ON B.SHELF_ID=C.SHELF_ID " & _
                            " INNER JOIN dbo.LIBRARY_SUB_DIVISIONS D ON C.LIBRARY_SUB_DIVISION_ID= D.LIBRARY_SUB_DIVISION_ID " & _
                            " INNER JOIN dbo.LIBRARY_DIVISIONS E ON D.LIBRARY_DIVISION_ID=E.LIBRARY_DIVISION_ID " & _
                            " WHERE E.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' AND A.MASTER_ID='" & HiddenMasterID.Value & "'  AND A.ACTIVE='True' )LIBCOUNT, " & _
                            " ( " & _
                            " SELECT COUNT(*) FROM LIBRARY_ITEM_RESERVATION C  " & _
                            " WHERE C.MASTER_ID ='" & HiddenMasterID.Value & "' " & _
                            " AND isnull(RESERVE_START_DATE,'') != '' " & _
                            " AND isnull(RESERVE_END_DATE,'') != '' AND ISNULL(TRAN_RECORD_ID,'') = '' " & _
                            " AND C.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' " & _
                            " )RCOUNT " & _
                            " )AB "


            Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sql_query)
            Dim lcount = ds.Tables(0).Rows(0).Item("LIBCOUNT").ToString()
            Dim rcount = ds.Tables(0).Rows(0).Item("RCOUNT").ToString()

            remitems = lcount - rcount

            If remitems > 0 Then
                Dim query = ""
                ''Check if only one record (reservation), if yes then set start and end date and send message
                query = " select TOP 1  RESERVATION_ID from LIBRARY_ITEM_RESERVATION A " & _
                            " where A.MASTER_ID='" & HiddenMasterID.Value & "' AND  A.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "'" & _
                            " AND ISNULL(TRAN_RECORD_ID,'') = '' AND RESERVATION_CANCEL='False' AND isnull(RESERVE_START_DATE,'') = '' AND isnull(RESERVE_END_DATE,'') = ''"

                ds = SqlHelper.ExecuteDataset(transaction, CommandType.Text, query)

                'If ds.Tables(0).Rows.Count = 1 Then

                Dim NoReserveDays = 1
                query = "UPDATE LIBRARY_ITEM_RESERVATION SET RESERVE_START_DATE=GETDATE(),RESERVE_END_DATE=DATEADD(Day," & NoReserveDays & " , GETDATE()) WHERE RESERVATION_ID='" & ds.Tables(0).Rows(0).Item("RESERVATION_ID").ToString() & "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                messageflag = 1

                'End If

            End If


        Catch ex As Exception
            messageflag = 0
        End Try

        Return messageflag

    End Function

    Public Function CheckUser()

        Dim returnvalue = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        ''Get the userid for given user number

        If RadioUserReservation.SelectedValue = "STUDENT" Then

            Sql_Query = "SELECT STU_ID FROM OASIS.dbo.STUDENT_M WHERE STU_NO='" & txtUserreservation.Text.Trim() & "'"

            HiddenUserID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

        ElseIf RadioUserReservation.SelectedValue = "EMPLOYEE" Then
            Sql_Query = "SELECT EMP_ID FROM OASIS.dbo.EMPLOYEE_M WHERE EMPNO='" & txtUserreservation.Text.Trim() & " '"

            HiddenUserID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        End If


        ''Check for if the user has memeber of this library
        Sql_Query = " SELECT A.MEMBERSHIP_ID,MEMBERSHIP_DES FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
                        " INNER JOIN LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                        " WHERE A.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' AND " & _
                        " A.USER_ID='" & HiddenUserID.Value & "' AND A.USER_TYPE='" & RadioUserReservation.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then

            '' Check if current user has reserved this item
            Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
                        " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'" & _
                        " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID='" & HiddenMasterID.Value & "' AND LIBRARY_ITEM_RESERVATION.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "'" & _
                        " AND USER_ID = '" & HiddenUserID.Value & "' AND USER_TYPE='" & RadioUserReservation.SelectedValue & "' AND RESERVATION_CANCEL='FALSE' "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = False
                lblMessage.Text = "This user already reserved this item. Please view the reservation history"

            End If



        Else

            returnvalue = False
            lblMessage.Text = "Invalid User ID. Please Check User Type. <br> (or) <br> Membership not assigned for this user. Please assign Membership for this user."

        End If

        Return returnvalue

    End Function

    Protected Sub btnReserve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReserve.Click
        If CheckReservationCount() Then


            LibraryData.isOffLine(Session("sBusper"))
            If CheckUser() Then

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                Dim query = "UPDATE STUDENT_M SET STU_ALT_EMAIL='" & txtreservationcontactadd.Text.Trim() & "' WHERE  STU_NO='" & txtUserreservation.Text.Trim() & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)

                Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
                Dim transaction As SqlTransaction
                connection.Open()
                transaction = connection.BeginTransaction()
                lblMessage.Text = ""
                Try
                    Dim pParms(5) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@USER_TYPE", RadioUserReservation.SelectedValue)
                    pParms(1) = New SqlClient.SqlParameter("@USER_ID", HiddenUserID.Value)
                    pParms(2) = New SqlClient.SqlParameter("@MASTER_ID", HiddenMasterID.Value)
                    pParms(3) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", Hiddenlibrarydivid.Value)
                    pParms(4) = New SqlClient.SqlParameter("@CONTACT_ADDRESS", txtreservationcontactadd.Text.Trim())

                    lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_ITEM_RESERVATION", pParms)


                    'Dim messageflag = 0

                    ' ''Send message if book exists 
                    'messageflag = SendMessageIfItemExists(transaction)

                    transaction.Commit()

                    'If messageflag = 1 Then
                    '    ''Send Message
                    '    '' Message is delivered by Mail Schedule
                    'End If

                    txtreservationcontactadd.Text = ""
                    txtUserreservation.Text = ""
                    BindReservation()

                Catch ex As Exception
                    lblMessage.Text = "Error occurred while transactions. " & ex.Message
                    transaction.Rollback()

                Finally
                    connection.Close()

                End Try

            End If

           

        End If

        MO1.Show()

    End Sub


    Public Sub BindReservation()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim filter As String = ""
        Dim Sql_Query = " SELECT * " & _
                        " ,'javascript:UserTransactions(''' + USER_TYPE + ''','''+ CONVERT(VARCHAR,GET_USER_ID)  +'''); return false;' USERREDIRECT " & _
                        " FROM ( " & _
                        " SELECT RESERVATION_ID,USER_ID AS GET_USER_ID, " & _
                        " CASE ISNULL(RESERVE_START_DATE,'') WHEN '' " & _
                        " THEN CASE RESERVATION_CANCEL WHEN 'TRUE' THEN 'FALSE' ELSE 'TRUE' END " & _
                        " ELSE CASE RESERVATION_CANCEL WHEN 'TRUE' THEN 'FALSE' ELSE 'FALSE' END " & _
                        " END RESERVATION_CANCEL_VISIBLE ," & _
                        " CASE USER_TYPE WHEN 'STUDENT' THEN (SELECT STU_NO FROM OASIS.dbo.STUDENT_M WHERE STU_ID=USER_ID ) ELSE " & _
                        " (SELECT EMPNO FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=USER_ID) END USER_ID " & _
                        " ,USER_TYPE,CONTACT_ADDRESS, " & _
                        " CASE ISNULL(RESERVE_START_DATE,'') WHEN '' " & _
                        " THEN CASE RESERVATION_CANCEL WHEN 'TRUE' THEN '~/Images/cross.png' ELSE '~/Images/tick.gif' END " & _
                        " ELSE CASE RESERVATION_CANCEL WHEN 'TRUE' THEN '~/Images/cross.png' ELSE '~/Images/FilledStar.png' END " & _
                        " END RESERVATION_CANCEL, " & _
                        " CASE TRAN_RECORD_ID WHEN '0' THEN 'Cancelled' ELSE '' END CANCEL_RESERVATION , " & _
                        " CASE USER_TYPE WHEN 'STUDENT' THEN (SELECT (ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) FROM OASIS.dbo.STUDENT_M WHERE STU_ID=USER_ID ) ELSE " & _
                        " (SELECT isnull(EMP_FNAME,'')+' ' +isnull(EMP_MNAME,'')+' '+isnull(EMP_LNAME,'') FROM OASIS.dbo.EMPLOYEE_M WHERE EMP_ID=USER_ID) END USER_NAME,A.ENTRY_DATE,REPLACE(CONVERT(VARCHAR(11), RESERVE_START_DATE , 106), ' ', '/')RESERVE_START_DATE ,REPLACE(CONVERT(VARCHAR(11), RESERVE_END_DATE , 106), ' ', '/')RESERVE_END_DATE  FROM dbo.LIBRARY_ITEM_RESERVATION A" & _
                        " INNER JOIN LIBRARY_DIVISIONS B ON B.LIBRARY_DIVISION_ID =A.LIBRARY_DIVISION_ID " & _
                        " WHERE A.MASTER_ID='" & HiddenMasterID.Value & "' AND B.LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' AND A.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "'" & _
                        " )A "

        If Request.QueryString("user") <> "" Then
            filter &= "   USER_ID like '%" & Request.QueryString("user_no").Replace(" ", "") & "%' "
        End If

        Dim txtnumber As String
        Dim txtname As String
        Dim txttype As String
        Dim txtContact As String



        If GrdReservation.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdReservation.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdReservation.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txttype = DirectCast(GrdReservation.HeaderRow.FindControl("txtType"), TextBox).Text.Trim()
            txtContact = DirectCast(GrdReservation.HeaderRow.FindControl("txtContact"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                If filter = "" Then
                    filter &= " USER_ID like '%" & txtnumber.Replace(" ", "") & "%' "
                Else
                    filter &= " and USER_ID like '%" & txtnumber.Replace(" ", "") & "%' "
                End If
            End If

            If txtname.Trim() <> "" Then
                If filter = "" Then
                    filter &= " USER_NAME like '%" & txtname.Replace(" ", "") & "%' "
                Else
                    filter &= " and USER_NAME like '%" & txtname.Replace(" ", "") & "%' "
                End If

            End If

            If txttype.Trim() <> "" Then
                If filter = "" Then
                    filter &= " USER_TYPE like '%" & txttype.Replace(" ", "") & "%' "
                Else
                    filter &= " and USER_TYPE like '%" & txttype.Replace(" ", "") & "%' "
                End If

            End If

            If txtContact.Trim() <> "" Then
                If filter = "" Then
                    filter &= "  CONTACT_ADDRESS like '%" & txtContact.Replace(" ", "") & "%' "
                Else
                    filter &= " and  CONTACT_ADDRESS  like '%" & txtContact.Replace(" ", "") & "%' "

                End If
            End If

        End If

        If filter <> "" Then
            Sql_Query = Sql_Query & " WHERE " & filter
        End If

        Sql_Query &= " order by ENTRY_DATE  DESC "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("RESERVATION_ID")
            dt.Columns.Add("USERREDIRECT")
            dt.Columns.Add("USER_ID")
            dt.Columns.Add("USER_TYPE")
            dt.Columns.Add("CONTACT_ADDRESS")
            dt.Columns.Add("RESERVATION_CANCEL")
            dt.Columns.Add("RESERVATION_CANCEL_VISIBLE")
            dt.Columns.Add("USER_NAME")
            dt.Columns.Add("RESERVE_START_DATE")
            dt.Columns.Add("RESERVE_END_DATE")
            dt.Columns.Add("CANCEL_RESERVATION")

            Dim dr As DataRow = dt.NewRow()
            dr("RESERVATION_ID") = ""
            dr("USERREDIRECT") = "" ''"javascript:UserTransactions('0','0')"
            dr("USER_ID") = ""
            dr("USER_TYPE") = ""
            dr("CONTACT_ADDRESS") = ""
            dr("RESERVATION_CANCEL") = ""
            dr("USER_NAME") = ""
            dr("RESERVATION_CANCEL_VISIBLE") = "False"
            dr("RESERVE_START_DATE") = ""
            dr("RESERVE_END_DATE") = ""
            dr("CANCEL_RESERVATION") = ""

            dt.Rows.Add(dr)
            GrdReservation.DataSource = dt
            GrdReservation.DataBind()

        Else
            GrdReservation.DataSource = ds
            GrdReservation.DataBind()


        End If

        If GrdReservation.Rows.Count > 0 Then

            DirectCast(GrdReservation.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdReservation.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdReservation.HeaderRow.FindControl("txtType"), TextBox).Text = txttype
            DirectCast(GrdReservation.HeaderRow.FindControl("txtContact"), TextBox).Text = txtContact

        End If



    End Sub

    Protected Sub GrdReservation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        If e.CommandName = "search" Then
            BindReservation()
        End If

        If e.CommandName = "CancelReservation" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try

                Dim query = "UPDATE LIBRARY_ITEM_RESERVATION SET TRAN_RECORD_ID=0 , RESERVATION_CANCEL='True' WHERE RESERVATION_ID='" & e.CommandArgument & "'"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
                lblMessage.Text = "Reservation cancellation done successfully"
                transaction.Commit()

                'Tab1.ActiveTabIndex = 2

            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions. " & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

            BindReservation()
            MO1.Show()

        End If


    End Sub


    Protected Sub GrdReservation_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs)
        GrdReservation.PageIndex = e.NewSelectedIndex
        BindReservation()
    End Sub

    Protected Sub txtUserreservation_TextChanged(sender As Object, e As System.EventArgs) Handles txtUserreservation.TextChanged
        BindEmailid()
    End Sub

    Public Sub BindEmailid()

        If RadioUserReservation.SelectedValue = "STUDENT" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = " SELECT isnull(STU_ALT_EMAIL,'') STU_ALT_EMAIL  FROM STUDENT_M WHERE  STU_NO='" & txtUserreservation.Text.Trim() & "'"
            txtreservationcontactadd.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        End If


    End Sub

End Class
