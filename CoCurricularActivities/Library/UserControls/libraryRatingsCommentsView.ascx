<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryRatingsCommentsView.ascx.vb" Inherits="Library_UserControls_libraryRatingsCommentsView" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%--<link href="../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />--%>
<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg-lite">User Comments and Ratings</td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridComments" runat="server" AutoGenerateColumns="false" EmptyDataText="No comments added yet." AllowPaging="True" Width="100%" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Comment ID
                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                    <%#Eval("COMMENT_ID")%>
                </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Comments
                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="T5lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                        <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                                            <%#Eval("COMMENTS")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T5lblview"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T5Panel1"
                                            TextLabelID="T5lblview">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Ratings
                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                    <ajaxToolkit:Rating ID="CRatings" runat="server" CurrentRating='<%# Eval("AVG_RATING") %>'
                        EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar" MaxRating="5"
                        ReadOnly="true" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
                    </ajaxToolkit:Rating>
                </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Date
                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
               <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Approved
                      
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                <asp:Image ID="Image1" ImageUrl='<%#Eval("APPROVED")%>' runat="server" />
            </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HiddenMasterId" runat="server" />
    <asp:HiddenField ID="HiddenBsuID" runat="server" />
</div>
