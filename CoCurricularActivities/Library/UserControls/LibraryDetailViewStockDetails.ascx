<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryDetailViewStockDetails.ascx.vb" Inherits="Library_UserControls_LibraryDetailViewStockDetails" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!-- Bootstrap core CSS-->
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Stock Details</td>
    </tr>
    <tr>
        <td>

            <asp:GridView ID="GridStocks" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText=" Reference No">
                        <HeaderTemplate>
                            Reference No
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("STOCK_ID")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accession No">
                        <HeaderTemplate>
                            Accession No
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("ACCESSION_NO")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Call No">
                        <HeaderTemplate>
                            Call No
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("CALL_NO")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Library Divisions">
                        <HeaderTemplate>
                            Library Divisions
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("LIBRARY_DIVISION_DES")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Library Sub Divisions">
                        <HeaderTemplate>
                            Library Sub Divisions
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("LIBRARY_SUB_DIVISION_DES")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Row">
                        <HeaderTemplate>
                            Row
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("SHELF_ROW")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Shelf">
                        <HeaderTemplate>
                            Shelf
                       
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("SHELF_NAME")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Racks">
                        <HeaderTemplate>
                            Racks
                      
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                    <%#Eval("RACK_NAME")%>
                </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <HeaderTemplate>
                            Status
                    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("STATUS_DESCRIPTION")%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Available">
                        <HeaderTemplate>
                            Available
                     
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                   <asp:Image ID="Image1" ImageUrl='<%#Eval("AVALABLE")%>' runat="server" />
               </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
</table>

<asp:HiddenField ID="HiddenBsuID" runat="server" />
<asp:HiddenField ID="HiddenMasterID" runat="server" />
