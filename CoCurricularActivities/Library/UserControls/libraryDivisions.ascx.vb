Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_UserControls_libraryDivisions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Session("sbsuid")
            BindGrid()
        End If

        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT * FROM LIBRARY_DIVISIONS " & _
                        "WHERE LIBRARY_BSU_ID='" & HiddenBsuID.Value & "' ORDER BY LIBRARY_DIVISION_DES "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridLibraryDivisions.DataSource = ds
        GridLibraryDivisions.DataBind()

    End Sub

    Protected Sub GridLibraryDivisions_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim HiddenLibraryDId = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("HiddenLibraryDId"), HiddenField).Value
            Dim txtLibraryDivisions = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("txtDataEdit"), TextBox).Text.Trim()
            Dim txtLibraryDivisionsCode = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("txtDataEdit1"), TextBox).Text.Trim()
            Dim txtLibraryReservationCount = DirectCast(GridLibraryDivisions.Rows(e.RowIndex).FindControl("txtDataEdit2"), TextBox).Text.Trim()


            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", HiddenLibraryDId)
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_DES", txtLibraryDivisions)
            pParms(2) = New SqlClient.SqlParameter("@LIBRARY_BSU_ID", HiddenBsuID.Value)
            pParms(3) = New SqlClient.SqlParameter("@LIBRARY_CODE", txtLibraryDivisionsCode.ToString().ToUpper().Trim())
            pParms(4) = New SqlClient.SqlParameter("@LIBRARY_ONLINE_RESERVER_COUNT", txtLibraryReservationCount)
            pParms(5) = New SqlClient.SqlParameter("@OPTIONS", 2)
            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_DIVISIONS", pParms)
            transaction.Commit()
            GridLibraryDivisions.EditIndex = "-1"
            BindGrid()
        Catch ex As Exception
            lblMessage.Text = "Error occurred while transactions. " & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

    End Sub

    Protected Sub GridLibraryDivisions_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        If e.CommandName = "deleting" Then
            LibraryData.isOffLine(Session("sBusper"))
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTIONS", 3)
                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_DIVISIONS", pParms)
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions." & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If

    End Sub

    Protected Sub GridLibraryDivisions_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs)
        GridLibraryDivisions.EditIndex = "-1"
        BindGrid()
    End Sub

    Protected Sub GridLibraryDivisions_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)

        GridLibraryDivisions.EditIndex = e.NewEditIndex
        BindGrid()
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        LibraryData.isOffLine(Session("sBusper"))
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        lblMessage.Text = ""
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_DES", TxtLibraryDivisions.Text.Trim().ToUpper())
            pParms(1) = New SqlClient.SqlParameter("@LIBRARY_BSU_ID", HiddenBsuID.Value)
            pParms(2) = New SqlClient.SqlParameter("@LIBRARY_CODE", txtcode.Text.ToString().ToUpper().Trim())
            pParms(3) = New SqlClient.SqlParameter("@LIBRARY_ONLINE_RESERVER_COUNT", txtreservecount.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@OPTIONS", 1)
            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_EDIT_DELETE_LIBRARY_DIVISIONS", pParms)
            transaction.Commit()
            BindGrid()
            TxtLibraryDivisions.Text = ""
            txtcode.Text = ""
        Catch ex As Exception
            transaction.Rollback()

        Finally
            connection.Close()

        End Try


    End Sub

End Class
