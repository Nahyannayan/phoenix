﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="libraryItemDetails.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Library_libraryItemDetails" %>

<%@ Register Src="UserControls/libraryItemDetails.ascx" TagName="libraryItemDetails" TagPrefix="uc1" %>

<asp:Content ID="c1" ContentPlaceHolderID="cphMasterpage" runat="server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Library Stocks
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <div>

                    <uc1:libraryItemDetails ID="libraryItemDetails1" runat="server" />

                </div>
            </div>
        </div>
    </div>

</asp:Content>
