﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_Users_LibraryUserMaster
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Encr_decrData.Decrypt(Request.QueryString("sbsuid").Replace(" ", "+"))
            Hiddenlibrarydivid.Value = Encr_decrData.Decrypt(Request.QueryString("Libraryid").Replace(" ", "+"))
            BindBSUText()
            BindLibraryDetails()
        End If

    End Sub

    Public Function countlib(ByVal status As String) As Integer
        Dim val As Integer = 0
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "SELECT  count(*) CNT FROM dbo.VIEW_LIBRARY_RECORDS WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'"

        If Hiddenlibrarydivid.Value <> 0 Then
            Sql_Query &= " and library_division_id='" & Hiddenlibrarydivid.Value & "' "
        End If

        If status <> "" Then
            Sql_Query &= " AND AVAILABILITY = '" & status & "' "
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            val = ds.Tables(0).Rows(0).Item("CNT").ToString()
        End If

        Return val
    End Function

    Public Sub BindLibraryDetails()

        Dim quantity = countlib("")
        Dim available = countlib("Y")

        rdmessage.InnerHtml &= "<br><br>&nbsp;Total Library Items : " & quantity
        rdmessage.InnerHtml &= "<br>&nbsp;Total Available Items :" & available
        rdmessage.InnerHtml &= "</b>"

    End Sub

    Public Sub BindBSUText()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT (SELECT BSU_SHORTNAME FROM OASIS.dbo.BUSINESSUNIT_M WHERE BSU_ID='" & HiddenBsuID.Value & "') BSU, " & _
                        " (SELECT ISNULL(LIBRARY_DIVISION_DES,'') FROM  dbo.LIBRARY_DIVISIONS WHERE LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "') LIBRARY "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then
            lblbsu.Text = ds.Tables(0).Rows(0).Item("BSU").ToString()
            lbllibrary.Text = ds.Tables(0).Rows(0).Item("LIBRARY").ToString()
            If lbllibrary.Text = "" Then
                lbllibrary.Text = "LIBRARY"
            End If

            rdmessage.InnerHtml &= "<br><b>&nbsp;<u>" & ds.Tables(0).Rows(0).Item("BSU").ToString()
            rdmessage.InnerHtml &= "&nbsp;" & lbllibrary.Text & "</u>"
        Else
            Response.Write("<script type='text/javascript' > alert('Not a valid URL.'); window.close(); </script>")
        End If

    End Sub

End Class
