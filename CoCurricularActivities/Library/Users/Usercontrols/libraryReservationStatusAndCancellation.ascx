﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryReservationStatusAndCancellation.ascx.vb"
    Inherits="Library_Users_Usercontrols_libraryReservationStatusAndCancellation" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<div align="center">


<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<br />
<br />
<table width="100%">
    <tr>
        <td class="title-bg">
            Reservation Status Enquiry
        </td>
    </tr>
    <tr>
        <td align="left">
            <table>
                <tr>
                    <td align="left" width="20%">
                        Member Type
                    </td>
                    
                    <td>
                        <asp:RadioButtonList ID="RadioUserReservation" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="STUDENT" Selected="True" Value="STUDENT"></asp:ListItem>
                            <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Member ID.
                    </td>
                    
                    <td>
                        <asp:TextBox ID="txtUserreservationNo" runat="server" ValidationGroup="Reservation"
                            ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnStatus" runat="server" CssClass="button" 
                            Text="Check Status" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                     
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="txtUserreservationNo" Display="None" 
        ErrorMessage="Please enter Member ID" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ShowMessageBox="True" ShowSummary="False" />
<br />
<table id="TableHistory" runat="server" visible="false"  width="100%">
    <tr>
        <td class="title-bg">
           Reservation History
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridHistory" CssClass="table table-bordered table-row" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                 Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                   
                                        Master&nbsp;ID <br />
                                    <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%# Eval("MASTER_ID") %>
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Item&nbsp;Image
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <asp:HiddenField ID="HiddenMasterId" runat="server" Value='<%# Eval("MASTER_ID") %>' />
                                <asp:ImageButton ID="ImageItem" runat="server" ImageUrl='<%# Eval("PRODUCT_IMAGE_URL") %>' />
                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Item&nbsp;Title
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("ITEM_TITLE") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                        Author
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("AUTHOR") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       Library Divisions
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                        <center><%#Eval("LIBRARY_DIVISION_DES")%></center>  
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                       Reserved Date
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                        <center><%#Eval("ENTRY_DATE")%></center>  
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                      Time Period
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <center>
                          <%#Eval("RESERVE_START_DATE")%>
                           <br />
                          <%#Eval("RESERVE_END_DATE")%>
                          </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                       <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="BlueTable" width="100%">
                                <tr class="matterswhite">
                                    <td align="center" colspan="2">
                                      Status
                                      <br />
                                       <br />
                                      
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                         <center>
                         <asp:Image ID="Image3" ImageUrl='<%#Eval("RESERVATION_CANCEL")%>' runat="server" /><br /><%#Eval("CANCEL_RESERVATION")%>
                         <%--<br />
                          <asp:LinkButton ID="LinkCancel" CausesValidation="false" Visible='<%#Eval("RESERVATION_CANCEL_VISIBLE")%>' CommandName="CancelReservation" CommandArgument='<%#Eval("RESERVATION_ID")%>' runat="server">Cancel</asp:LinkButton>
                          <br />
                          <ajaxToolkit:ConfirmButtonExtender ID="CF12" TargetControlID="LinkCancel" ConfirmText="Cancel this reservation ?" runat="server"></ajaxToolkit:ConfirmButtonExtender>  
--%>                         </center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    
                </Columns>
                <RowStyle CssClass="griditem" Wrap="False" />
                <EmptyDataRowStyle Wrap="False" />
                <SelectedRowStyle CssClass="Green" Wrap="False" />
                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                <EditRowStyle Wrap="False" />
                <%--<AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />--%>
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:HiddenField ID="HiddenBsuID" runat="server" />
</ContentTemplate>
</asp:UpdatePanel>

</div>