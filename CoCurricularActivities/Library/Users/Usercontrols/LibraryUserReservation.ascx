﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LibraryUserReservation.ascx.vb" Inherits="Library_Users_Usercontrols_LibraryUserReservation" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                                </ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<br />
<br />
<br />

<div class="" align="center">
           <table border="1" bordercolor="#8dc24c" cellpadding="5" cellspacing="0" Width="400" >
                        <tr>
                            <td class="title-bg-lite">
                                Item Reservation 
                            </td>
                        </tr>
                        <tr>
                            <td  align="left"  >
   
    <table>
                <tr>
                    <td >
                        Member Type</td>
                    <td>
                        :</td>
                    <td >
                    <asp:RadioButtonList ID="RadioUserReservation" runat="server" RepeatDirection="Horizontal">
                       <asp:ListItem Text="STUDENT" Selected="True" Value="STUDENT"></asp:ListItem>
                       <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE"></asp:ListItem>
                    </asp:RadioButtonList>
     
                    </td>
                </tr>
                <tr>
                    <td>
                        Member ID.</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="txtUserreservation" runat="server" 
                            ValidationGroup="Reservation" Width="282px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email ID</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="txtreservationcontactadd"  runat="server" ValidationGroup="Reservation"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Button ID="btnReserve" runat="server" CssClass="button" Text="Reserve" 
                            ValidationGroup="Reservation" Width="100px" />
                        <asp:Button ID="btnclose" runat="server" CssClass="button" 
                            OnClientClick="javascript:window.close();" Text="Close" Width="100px" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
     
                                
     
      </td>
                    </tr>
               </table>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
    ControlToValidate="txtUserreservation" Display="None" 
    ErrorMessage="Please Enter Member ID" ValidationGroup="Reservation">
</asp:RequiredFieldValidator>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
    ControlToValidate="txtreservationcontactadd" Display="None" 
    ErrorMessage="Please Enter Email ID." ValidationGroup="Reservation">
</asp:RequiredFieldValidator>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="Reservation" />
<asp:HiddenField ID="HiddenMasterID" runat="server" />
<asp:HiddenField ID="HiddenBsuID" runat="server" />
<asp:HiddenField ID="HiddenUserID" runat="server" />
<asp:HiddenField ID="Hiddenlibrarydivid" runat="server" />
</div> 
</ContentTemplate>
</asp:UpdatePanel>
 
