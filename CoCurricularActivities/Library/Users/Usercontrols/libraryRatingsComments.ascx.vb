Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Xml
Partial Class Library_UserControls_libraryRatingsComments
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HiddenMasterId.Value = Request.QueryString("MasterId")
            Hiddenbsuid.Value = Request.QueryString("bsu_id")
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        Dim transaction As SqlTransaction
        connection.Open()
        transaction = connection.BeginTransaction()
        Try

            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MASTER_ID", HiddenMasterId.Value)
            pParms(1) = New SqlClient.SqlParameter("@COMMENTS", txtComments.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@RATING", CRatings.CurrentRating)
            pParms(3) = New SqlClient.SqlParameter("@COMMENT_BSU", Hiddenbsuid.Value)
            lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_ITEMS_COMMENTS", pParms)

            transaction.Commit()


        Catch ex As Exception
            lblMessage.Text = "Error while transaction. Error :" & ex.Message
            transaction.Rollback()

        Finally
            connection.Close()

        End Try

        MO1.Show()

    End Sub

End Class
