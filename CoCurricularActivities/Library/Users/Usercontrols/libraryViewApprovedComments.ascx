﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryViewApprovedComments.ascx.vb" Inherits="Library_Users_Usercontrols_libraryViewApprovedComments" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
<asp:UpdatePanel id="UpdatePanel1" runat="server">
    <contenttemplate>
    <br />
    <br />
    
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            
            Approved Comments and Ratings</td>
    </tr>
    <tr>
        <td>
<asp:GridView ID="GridComments" runat="server" AutoGenerateColumns="false" EmptyDataText="No comments added or approved yet."  AllowPaging="True" Width="100%">
    <Columns>
        <asp:TemplateField>
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Comment ID
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <%#Eval("COMMENT_ID")%>
                </center>
            </ItemTemplate>
            <ItemStyle Width="100px" />
        </asp:TemplateField>
       
        <asp:TemplateField>
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Comments
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
             <ItemTemplate>
                                <asp:Label ID="T5lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                <asp:Panel ID="T5Panel1" runat="server" Height="50px">
                                    <%#Eval("COMMENTS")%></asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="T5CollapsiblePanelExtender1" runat="server"
                                    AutoCollapse="False" AutoExpand="False" CollapseControlID="T5lblview" Collapsed="true"
                                    CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T5lblview"
                                    ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T5Panel1"
                                    TextLabelID="T5lblview">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ItemTemplate>
                            
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Ratings
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <center>
                    <ajaxToolkit:Rating ID="CRatings" runat="server" CurrentRating='<%# Eval("AVG_RATING") %>'
                        EmptyStarCssClass="emptyRatingStar" FilledStarCssClass="filledRatingStar" MaxRating="5"
                        ReadOnly="true" StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar">
                    </ajaxToolkit:Rating>
                </center>
            </ItemTemplate>
            <ItemStyle Width="70px" />
        </asp:TemplateField>
                 <asp:TemplateField>
            <HeaderTemplate>
                <table class="BlueTable" width="100%">
                    <tr class="matterswhite">
                        <td align="center" colspan="2">
                            Date
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
            <center>
               <%#Eval("ENTRY_DATE", "{0:dd/MMM/yyyy}")%>
            </center>
            </ItemTemplate>
             <ItemStyle Width="100px" />
        </asp:TemplateField>
    </Columns>
    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
    <EmptyDataRowStyle Wrap="False" />
    <SelectedRowStyle CssClass="Green" Wrap="False" />
    <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
    <EditRowStyle Wrap="False" />
    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
</asp:GridView>
<br />
<br />
            <asp:Button ID="btnmok" runat="server" CausesValidation="False" 
                CssClass="button" OnClientClick="javascript:window.close();" Text="Ok" 
                ValidationGroup="s" Width="80px" />
        </td>
    </tr>
</table></contenttemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="HiddenMasterId" runat="server" />
<asp:HiddenField ID="HiddenBsuID" runat="server" />