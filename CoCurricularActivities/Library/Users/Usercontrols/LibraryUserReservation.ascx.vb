﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_Users_Usercontrols_LibraryUserReservation
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            HiddenMasterID.Value = Request.QueryString("Masterid").ToString()
            Hiddenlibrarydivid.Value = Request.QueryString("Libraryid").ToString()
            HiddenBsuID.Value = Request.QueryString("sbsuid").ToString()
            HiddenUserID.Value = ""

        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub


    Public Function CheckUser()

        Dim returnvalue = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = ""
        ''Get the userid for given user number

        If RadioUserReservation.SelectedValue = "STUDENT" Then

            Sql_Query = "SELECT STU_ID FROM OASIS.dbo.STUDENT_M WHERE STU_NO='" & txtUserreservation.Text.Trim() & "'"

            HiddenUserID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)

        ElseIf RadioUserReservation.SelectedValue = "EMPLOYEE" Then
            Sql_Query = "SELECT EMP_ID FROM OASIS.dbo.EMPLOYEE_M WHERE EMPNO='" & txtUserreservation.Text.Trim() & " '"

            HiddenUserID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, Sql_Query)
        End If


        ''Check for if the user has memeber of this library
        Sql_Query = " SELECT A.MEMBERSHIP_ID,MEMBERSHIP_DES FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
                        " INNER JOIN LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                        " WHERE A.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' AND " & _
                        " A.USER_ID='" & HiddenUserID.Value & "' AND A.USER_TYPE='" & RadioUserReservation.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then

            '' Check if current user has reserved this item
            Sql_Query = " SELECT DISTINCT RESERVATION_ID FROM dbo.LIBRARY_ITEM_RESERVATION " & _
                        " INNER JOIN LIBRARY_ITEMS_QUANTITY B ON B.PRODUCT_BSU_ID='" & HiddenBsuID.Value & "'" & _
                        " WHERE LIBRARY_ITEM_RESERVATION.MASTER_ID='" & HiddenMasterID.Value & "' AND LIBRARY_ITEM_RESERVATION.LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "'" & _
                        " AND USER_ID = '" & HiddenUserID.Value & "' AND USER_TYPE='" & RadioUserReservation.SelectedValue & "' AND RESERVATION_CANCEL='FALSE' "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
            If ds.Tables(0).Rows.Count > 0 Then
                returnvalue = False
                lblMessage.Text = "This user already reserved this item. Please view the reservation status"
                btnReserve.Visible = False
            End If



        Else
            btnReserve.Visible = False
            returnvalue = False
            lblMessage.Text = "<center>Invalid Member ID. Please Check Member Type. <br> (or) <br> Membership not assigned for this user. Please assign Membership for this user.</center>"

        End If

        Return returnvalue

    End Function

    Protected Sub btnReserve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReserve.Click

        If CheckUser() Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@USER_TYPE", RadioUserReservation.SelectedValue)
                pParms(1) = New SqlClient.SqlParameter("@USER_ID", HiddenUserID.Value)
                pParms(2) = New SqlClient.SqlParameter("@MASTER_ID", HiddenMasterID.Value)
                pParms(3) = New SqlClient.SqlParameter("@LIBRARY_DIVISION_ID", Hiddenlibrarydivid.Value)
                pParms(4) = New SqlClient.SqlParameter("@CONTACT_ADDRESS", txtreservationcontactadd.Text.Trim())

                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_LIBRARY_ITEM_RESERVATION", pParms)


                transaction.Commit()

                btnReserve.Visible = False
                txtreservationcontactadd.Text = ""
                txtUserreservation.Text = ""


            Catch ex As Exception
                lblMessage.Text = "Error occurred while transactions. " & ex.Message
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        End If


    End Sub

End Class
