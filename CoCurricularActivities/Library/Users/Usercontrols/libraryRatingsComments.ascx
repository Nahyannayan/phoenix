<%@ Control Language="VB" AutoEventWireup="false" CodeFile="libraryRatingsComments.ascx.vb" Inherits="Library_UserControls_libraryRatingsComments" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<script type="text/javascript">


    function callfun() 
    {

        var bsu_id = document.getElementById('<% = Hiddenbsuid.ClientID %>').value;
        var master_id = document.getElementById('<% = HiddenMasterId.ClientID %>').value;
        window.open('libraryViewApprovedComments.aspx?sbsuid=' + bsu_id + '&master_id=' + master_id, '', 'Height=600px,Width=800px,scrollbars=yes,resizable=no,directories=yes');
        return false;

    }


</script>

<div class="matters">
<center>
<ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
</ajaxToolkit:ToolkitScriptManager>
<br />
<br />
<br />
<br />
<br />
<br />

 <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="300px" >
                        <tr>
                            <td class="subheader_img">
                                Enter Comments and Rate &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="Linkcomments" OnClientClick="javascript:callfun();return false;" Text="Approved Comments"  ForeColor="Red" runat="server"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td >

<table>
    <tr>
        <td colspan="1">
            <asp:TextBox ID="txtComments" runat="server" EnableTheming="False" Height="222px" TextMode="MultiLine"
                Width="335px"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
        <ajaxtoolkit:rating id="CRatings" runat="server" 
                                            maxrating="5" starcssclass="ratingStar" waitingstarcssclass="savedRatingStar"
                                            filledstarcssclass="filledRatingStar" emptystarcssclass="emptyRatingStar" />
        </td>
    </tr>
    <tr>
        <td  align="center">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" 
                Width="84px" />
            <asp:Button ID="btnclose" runat="server" Text="Close" OnClientClick="javascript:window.close();" CssClass="button" 
                Width="84px" /></td>
    </tr>
</table>
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                <asp:Panel ID="PanelMessage" runat="server" BackColor="white" CssClass="modalPopup"
                                    Style="display: none">
                                    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="240">
                                        <tr>
                                            <td class="subheader_img">
                                                Library Message</td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                                    <contenttemplate>
<TABLE><TR><TD align=center><asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR>
    <TD><asp:Button id="btnmok"  runat="server" Text="Ok" CssClass="button" Width="80px" OnClientClick="javascript:window.close();" ValidationGroup="s" CausesValidation="False"></asp:Button></TD></TR></TABLE>&nbsp; 
</contenttemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
                                    DropShadow="true" PopupControlID="PanelMessage" CancelControlID="btnmok" RepositionMode="RepositionOnWindowResizeAndScroll"
                                    TargetControlID="Label1">
                                </ajaxToolkit:ModalPopupExtender>
 </td>
                    </tr>
               </table>
<asp:HiddenField ID="HiddenMasterId" runat="server" />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComments"
        Display="None" ErrorMessage="Please Enter Comments" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
</center>
</div>