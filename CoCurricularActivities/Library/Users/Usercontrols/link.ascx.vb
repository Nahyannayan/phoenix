﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports GemBox.Spreadsheet

Partial Class Library_Users_Usercontrols_link
    Inherits System.Web.UI.UserControl

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetDefaultUrl()
            HelpDesk.BindBsu(ddbsu)
            Dim list As New ListItem
            list.Text = "Select a BSU"
            list.Value = "0"
            ddbsu.Items.Insert(0, list)

        End If
    End Sub

    Public Sub GetDefaultUrl()
        ''commented by nahyan on 28nov2018
        'Dim val As String = Request.Url.OriginalString
        'txturl.Text = val.Substring(0, val.IndexOf(".com") + 4)
        txturl.Text = "https://school.gemsoasis.com"
    End Sub
    Protected Sub ddbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddbsu.SelectedIndexChanged
        GetLink(False)
    End Sub

    Public Sub GetLink(ByVal export As Boolean)
        lblMessage.Text = ""
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = " SELECT BSU_SHORTNAME,'ENTIRE SCHOOL' AS LIBRARY_DIVISION_DES,'' as LIBRARY_DIVISION_ID,BSU_ID FROM oasis.dbo.BUSINESSUNIT_M WHERE BSU_ID='" & ddbsu.SelectedValue & "' " & _
                        " UNION " & _
                        " select BSU_SHORTNAME,LIBRARY_DIVISION_DES,LIBRARY_DIVISION_ID,A.LIBRARY_BSU_ID from dbo.LIBRARY_DIVISIONS A " & _
                        " inner join oasis.dbo.BUSINESSUNIT_M B ON A.LIBRARY_BSU_ID= B.BSU_ID " & _
                        " WHERE A.LIBRARY_BSU_ID='" & ddbsu.SelectedValue & "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 1 Then '' If Library Divisions Addded

            If txturl.Text <> "" Then
                Dim dt As New DataTable

                dt.Columns.Add("BSU")
                dt.Columns.Add("LIBRARY")
                dt.Columns.Add("PATH")

                Dim i = 0

                For i = 0 To ds.Tables(0).Rows.Count - 1

                    Dim dr As DataRow = dt.NewRow

                    dr.Item("BSU") = ds.Tables(0).Rows(i).Item("BSU_SHORTNAME").ToString()
                    dr.Item("LIBRARY") = ds.Tables(0).Rows(i).Item("LIBRARY_DIVISION_DES").ToString()
                    dr.Item("PATH") = txturl.Text & "/Library/Users/LibraryUserMaster.aspx?sbsuid=" & Encr_decrData.Encrypt(ds.Tables(0).Rows(i).Item("BSU_ID").ToString()) & "&Libraryid=" & Encr_decrData.Encrypt(ds.Tables(0).Rows(i).Item("LIBRARY_DIVISION_ID").ToString())
                    dt.Rows.Add(dr)
                Next
                Gridlinks.DataSource = dt
                Gridlinks.DataBind()
                Gridlinks.Visible = True
                btnexport.Visible = True

                If export Then
                    SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                    Dim ef As ExcelFile = New ExcelFile

                    Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                    ws.InsertDataTable(dt, "A1", True)
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + "Data.xls")
                    ef.SaveXls(Response.OutputStream)


                End If

            Else
                lblMessage.Text = "<b>Please enter a valid url in the above Textbox.</b>"
            End If

        Else
            Gridlinks.Visible = False
            btnexport.Visible = False
            lblMessage.Text = "<b>This Business unit did not assigned any library divisions.<br> Please assign and try again.</b>"
        End If

    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        GetLink(True)
    End Sub

End Class
