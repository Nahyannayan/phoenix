﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="link.ascx.vb" Inherits="Library_Users_Usercontrols_link" %>


<!-- Bootstrap core CSS-->
<link href="../../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="../../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<link href="../../../cssfiles/sb-admin.css" rel="stylesheet">
<link href="../../../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td colspan="2" class="title-bg-lite">Enter URL</td>
    </tr>
    <tr>
        <td align="left" width="50%">
            <asp:TextBox ID="txturl" runat="server"></asp:TextBox>
        </td>
        <td align="left" width="50%"></td>
    </tr>
    <tr>
        <td colspan="2" class="title-bg-lite">Select BSU</td>
    </tr>
    <tr>
        <td align="left" width="50%">
            <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" width="50%"></td>
    </tr>

    <tr>
        <td align="left" colspan="2">
            <asp:Label ID="lblMessage" runat="server" class="text-danger"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2">
            <asp:GridView ID="Gridlinks" runat="server" AutoGenerateColumns="false"
                Width="100%" CssClass="table table-bordered table-row">
                <Columns>

                    <asp:TemplateField HeaderText="Library Divisions">
                        <HeaderTemplate>
                            Library Divisions
                                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                                <%#Eval("LIBRARY")%>
                                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Library User Link">
                        <HeaderTemplate>
                            Link
                                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                                <asp:LinkButton ID="LinkButton1" PostBackUrl='<%#Eval("PATH")%>' Text='<%#Eval("PATH")%>' runat="server"></asp:LinkButton>  
                                            </center>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <RowStyle CssClass="griditem" />
                <EmptyDataRowStyle />
                <SelectedRowStyle />
                <HeaderStyle />
                <EditRowStyle />
                <AlternatingRowStyle CssClass="griditem_alternative" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export to Excel" Visible="False" />
        </td>
    </tr>
</table>
