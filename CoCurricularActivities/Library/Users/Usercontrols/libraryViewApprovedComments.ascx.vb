﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class Library_Users_Usercontrols_libraryViewApprovedComments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Request.QueryString("sbsuid")
            HiddenMasterId.Value = Request.QueryString("master_id")
            BindGrid()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

    End Sub

    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString()
        Dim Sql_Query = "SELECT COMMENT_ID,COMMENTS,RATING,ENTRY_DATE, " & _
                        " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, " & _
                        " (SELECT AVG(RATING) FROM dbo.LIBRARY_ITEMS_COMMENTS WHERE MASTER_ID=" & HiddenMasterId.Value & " AND COMMENT_BSU='" & HiddenBsuID.Value & "') AVG_RATING " & _
                        " FROM dbo.LIBRARY_ITEMS_COMMENTS WHERE MASTER_ID=" & HiddenMasterId.Value & " AND COMMENT_BSU='" & HiddenBsuID.Value & "'  AND APPROVED='True' ORDER BY COMMENT_ID DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)
        GridComments.DataSource = ds
        GridComments.DataBind()

    End Sub


    Protected Sub GridComments_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridComments.PageIndexChanging
        GridComments.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

End Class
