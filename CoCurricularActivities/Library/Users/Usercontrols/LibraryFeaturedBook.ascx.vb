﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Library_Users_Usercontrols_LibraryFeaturedBook
    Inherits System.Web.UI.UserControl

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            HiddenBsuID.Value = Encr_decrData.Decrypt(Request.QueryString("sbsuid").Replace(" ", "+"))
            Hiddenlibrarydivid.Value = Encr_decrData.Decrypt(Request.QueryString("Libraryid").Replace(" ", "+"))
            BindData()
        End If

    End Sub


    Public Sub BindData()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = "SELECT TOP 1 * FROM dbo.VIEW_USERS_SEARCH WHERE PRODUCT_BSU_ID='" & HiddenBsuID.Value & "' AND LEN(PRODUCT_DESCRIPTION) >80 "

        If Hiddenlibrarydivid.Value <> "0" Then
            str_query &= " AND LIBRARY_DIVISION_ID='" & Hiddenlibrarydivid.Value & "' "
        End If

        str_query &= " ORDER BY NEWID() "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then

            ImageItem.ImageUrl = ds.Tables(0).Rows(0).Item("PRODUCT_IMAGE_URL").ToString()
            lbltitle.Text = ds.Tables(0).Rows(0).Item("ITEM_TITLE").ToString()
            lbldes.Text = ds.Tables(0).Rows(0).Item("ITEM_DES").ToString()
            lblauthor.Text = ds.Tables(0).Rows(0).Item("AUTHOR").ToString()
            lblpublisher.Text = ds.Tables(0).Rows(0).Item("PUBLISHER").ToString()
            lbldescription.Text = ds.Tables(0).Rows(0).Item("PRODUCT_DESCRIPTION").ToString()
            CRatings.CurrentRating = ds.Tables(0).Rows(0).Item("AVG_RATING")

        End If


    End Sub

    Protected Sub btnnext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnext.Click
        BindData()
    End Sub

End Class
