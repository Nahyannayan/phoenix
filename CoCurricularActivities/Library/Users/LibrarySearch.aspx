﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LibrarySearch.aspx.vb" Inherits="Library_Users_LibrarySearch" %>

<%@ Register Src="~/Library/Users/Usercontrols/librarySearchRatingsComments.ascx" TagName="librarySearchRatingsComments"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Library Search</title>
    <link href="../../vendor/bootstrap/css/bootstrap.css" type="text/css" />
    <link href="../../cssfiles/Librarystyle.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Ratings.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        body {
           background-image:none !important;
           background-color : transparent !important;
       }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:librarySearchRatingsComments ID="librarySearchRatingsComments1" runat="server" />
    </form>
</body>
</html>
