<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaAceLevel_M.aspx.vb" Inherits="CoCurricularActivities_ccaAceLevel_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Ace Level"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Style="text-align: left"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table width="100%">


                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Description</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grades</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:CheckBoxList ID="cbGrades" CssClass="field-label" runat="server" 
                                            RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Gender</span>
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:RadioButtonList ID="rbGender" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Boys" Value="Boys" Class="field-label" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Girls" Class="field-label" Value="Girls"></asp:ListItem>
                                            <asp:ListItem Text="Boys & Girls" Class="field-label" Value="Boys & Girls"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="center" colspan="3">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" TabIndex="6" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" TabIndex="6" />
                                        <asp:Button ID="btnSave" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Save" TabIndex="6" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td valign="bottom">&nbsp;<asp:HiddenField ID="hfACL_ID" runat="server" />
                            <asp:RequiredFieldValidator ID="rfsUBJECT" runat="server" ErrorMessage="Please enter the field Subject" ControlToValidate="txtDescr" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

