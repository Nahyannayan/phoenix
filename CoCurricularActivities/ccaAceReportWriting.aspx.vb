Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Configuration
Imports CURRICULUM
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security
Imports Telerik.QuickStart
Imports Telerik.Web.UI
Imports System.Drawing

Partial Class CoCurricularActivities_ccaAceReportWriting
    Inherits System.Web.UI.Page
    'ts
    Dim Encr_decrData As New Encryption64
    Private NVCStudentPage As NameValueCollection

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        lblError.Text = ""
        Session.Timeout = 60

        If Page.IsPostBack = False Then
            Session("StuComments") = CreateTableStuComments()
            'Session("StuComments") = ReportFunctions.CreateTableStuMarks()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC20010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    ViewState("datamode") = "add"

                    If ViewState("datamode") = "add" Then
                        FillAcd()
                        FillReport()
                        BindMainHeader()
                        BindAce()
                        FillRepSchedule()
                        BindLevel()
                        BindGroup()
                        trSave1.Visible = False
                        trSave.Visible = False
                    Else

                        H_ACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("AccId").Replace(" ", "+"))
                        H_ACM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("GradeId").Replace(" ", "+"))
                        H_ACM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acmid").Replace(" ", "+"))
                        H_RPT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("MarksId").Replace(" ", "+"))

                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed$"
            End Try

        End If
    End Sub

#Region "Private Functions"

    Private Function CreateTableStuComments() As DataTable
        Dim dtStuComments As DataTable
        dtStuComments = New DataTable

        Try

            Dim Id As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim RST_RSM_ID As New DataColumn("RST_RSM_ID", System.Type.GetType("System.String"))
            Dim RST_RPF_ID As New DataColumn("RST_RPF_ID", System.Type.GetType("System.String"))
            Dim RST_RSD_ID As New DataColumn("RST_RSD_ID", System.Type.GetType("System.String"))
            Dim RST_ACD_ID As New DataColumn("RST_ACD_ID", System.Type.GetType("System.String"))
            Dim RST_ACM_ID As New DataColumn("RST_ACM_ID", System.Type.GetType("System.String"))
            Dim RST_ACL_ID As New DataColumn("RST_ACL_ID", System.Type.GetType("System.String"))
            Dim RST_STU_ID As New DataColumn("RST_STU_ID", System.Type.GetType("System.String"))
            Dim RST_COMMENTS As New DataColumn("RST_COMMENTS", System.Type.GetType("System.String"))

            dtStuComments.Columns.Add(Id)
            dtStuComments.Columns.Add(RST_RSM_ID)
            dtStuComments.Columns.Add(RST_RPF_ID)
            dtStuComments.Columns.Add(RST_RSD_ID)
            dtStuComments.Columns.Add(RST_ACD_ID)
            dtStuComments.Columns.Add(RST_ACM_ID)
            dtStuComments.Columns.Add(RST_ACL_ID)
            dtStuComments.Columns.Add(RST_STU_ID)
            dtStuComments.Columns.Add(RST_COMMENTS)

            Return dtStuComments
        Catch ex As Exception
            Return dtStuComments
        End Try
    End Function



    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function

    Sub BindMainHeader()
        ddlMainHeader.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT RSD_ID,RSD_HEADER FROM RPT.ACE_REPORT_SETUP_D WHERE RSD_RSM_ID='" + ddlReport.SelectedValue.ToString + "' AND RSD_bHASCHILD='TRUE' AND RSD_bALLSUBJECTS='FALSE' ORDER BY RSD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMainHeader.DataSource = ds
        ddlMainHeader.DataTextField = "RSD_HEADER"
        ddlMainHeader.DataValueField = "RSD_ID"
        ddlMainHeader.DataBind()
        If ddlMainHeader.Items.Count = 0 Then
            ddlMainHeader.Visible = False
        Else
            ddlMainHeader.Visible = True
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Private Sub SaveRecord()

        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim cmd As New SqlCommand
        Dim lblStudId As New Label
        Dim lblStudName As New Label
        Dim txtComments As New TextBox
        Dim txtCmntsShort As New TextBox
        Dim StrSql As String
        Dim ds As DataSet
        Dim RSTID As Integer
        Dim strComments As String
        Dim stTrans As SqlTransaction
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try

            objConn.Open()
            stTrans = objConn.BeginTransaction

            For iIndex = 0 To Session("StuComments").Rows.Count - 1
                If IsDBNull(Session("StuComments").Rows(iIndex)("Id")) = False Then

                    cmd = New SqlCommand("[RPT].[SaveACE_REPORT_STUDENT_S]", objConn, stTrans)


                    cmd.CommandType = CommandType.StoredProcedure
                    With Session("StuComments")
                        If ViewState("datamode") = "add" Then
                            cmd.Parameters.AddWithValue("@RST_ID", 0)
                        End If
                        If ViewState("datamode") = "edit" Then
                            cmd.Parameters.AddWithValue("@RST_ID", .Rows(iIndex)("id"))
                        End If

                        cmd.Parameters.AddWithValue("@RST_RPF_ID", .Rows(iIndex)("RST_RPF_ID"))
                        cmd.Parameters.AddWithValue("@RST_RSD_ID", .Rows(iIndex)("RST_RSD_ID"))
                        cmd.Parameters.AddWithValue("@RST_ACD_ID", .Rows(iIndex)("RST_ACD_ID"))
                        cmd.Parameters.AddWithValue("@RST_STU_ID", .Rows(iIndex)("RST_STU_ID"))
                        cmd.Parameters.AddWithValue("@RST_SGR_ID", ddlGroup.SelectedValue)
                        cmd.Parameters.AddWithValue("@RST_ACM_ID", ddlAce.SelectedValue)
                        cmd.Parameters.AddWithValue("@RST_ACL_ID", ddlLevel.SelectedValue)
                        cmd.Parameters.AddWithValue("@RST_COMMENTS", .Rows(iIndex)("RST_COMMENTS"))
                        If ViewState("datamode") = "add" Then
                            cmd.Parameters.AddWithValue("@bEdit", 0)
                        End If
                        If ViewState("datamode") = "edit" Then
                            cmd.Parameters.AddWithValue("@bEdit", 1)
                        End If
                        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        cmd.ExecuteNonQuery()
                        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)

                    End With
                End If
            Next
            If iReturnvalue <> 0 Then
                stTrans.Rollback()
                lblError.Text = "Unexpected error"
                Session("StuComments").Rows.Clear()
                Exit Sub
            End If
            stTrans.Commit()
            lblError.Text = "Successfully Saved"
            Session("StuComments").Rows.Clear()
            'Session("SaveTrue") = "Successfully Saved"


        Catch ex As Exception
            stTrans.Rollback()
            Session("StuComments").Rows.Clear()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
            'Session("SaveTrue") = "Request could not be processed"
        Finally
            objConn.Close()
        End Try
    End Sub






    Private Sub GetStudentsList()


        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim strCondition As String = ""
        Dim intRowCnt As Integer = 0
        Try
            'Instantiate a New Hash Table And Keeps into a ViewState
            NVCStudentPage = New NameValueCollection
            ViewState("StuPages") = NVCStudentPage
            ViewState("PageNo") = "0"


            If H_GRP_ID.Value <> "" And H_GRP_ID.Value <> "0" Then
                strCriteria += " AND SGR_ID='" & H_GRP_ID.Value & "' "
            End If
            'If Not ddlLevel.SelectedIndex = -1 Then
            '    strCriteria += " AND STU_SCT_ID='" & H_SCT_ID.Value & "' "
            'End If
            If txtStudName.Text <> "" Then
                strCriteria += " AND STU_ID=" & H_STU_ID.Value & " "
            End If

            If Session("CURRENT_ACD_ID") = H_ACD_ID.Value Then
                If Session("sbsuid") <> "126008" Then
                    strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName,GRM_DISPLAY as Grade,SCT_DESCR as Section, " & GetReportHeader() & "  " & _
                            " FROM VW_STUDENT_DETAILS " & _
                            " INNER JOIN ACE_STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID AND SSD_ACD_ID='" & H_ACD_ID.Value & "'" & _
                            " INNER JOIN ACE_GROUP_M ON SSD_SGR_ID=SGR_ID " & _
                            " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"

                    strSql += strCriteria & " ORDER BY GRM_DISPLAY,SCT_DESCR,STU_NAME"
                Else
                    strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,isnull(STU_PASSPORT_NAME,STU_NAME) as StudentName,GRM_DISPLAY as Grade,SCT_DESCR as Section, " & GetReportHeader() & "  " & _
                " FROM VW_STUDENT_DETAILS " & _
                " INNER JOIN ACE_STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID AND SSD_ACD_ID='" & H_ACD_ID.Value & "'" & _
                " INNER JOIN ACE_GROUP_M ON SSD_SGR_ID=SGR_ID " & _
                " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"

                    strSql += strCriteria & " ORDER BY  GRM_DISPLAY,SCT_DESCR,STU_NAME"
                End If
            Else
                strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName,GRM_DISPLAY as Grade,SCT_DESCR AS Section " & GetReportHeader() & "  " & _
                       " FROM VW_STUDENT_DETAILS_PREVYEARS " & _
                       " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"

                strSql += strCriteria & " ORDER BY GRM_DISPLAY,SCT_DESCR,STU_NAME"
            End If
            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If Session("sbsuid") = "125017" Then
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 11 Then
                            NVCStudentPage.Add(1, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 11 And intRowCnt < 21 Then
                            NVCStudentPage.Add(2, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 21 And intRowCnt < 31 Then
                            NVCStudentPage.Add(3, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 31 And intRowCnt < 41 Then
                            NVCStudentPage.Add(4, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 41 And intRowCnt < 51 Then
                            NVCStudentPage.Add(5, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 51 And intRowCnt < 61 Then
                            NVCStudentPage.Add(6, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 61 And intRowCnt < 71 Then
                            NVCStudentPage.Add(7, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 71 And intRowCnt < 81 Then
                            NVCStudentPage.Add(8, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 81 And intRowCnt < 91 Then
                            NVCStudentPage.Add(9, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 91 And intRowCnt < 101 Then
                            NVCStudentPage.Add(10, drRow.Item("StudentID"))
                        Else
                            NVCStudentPage.Add(11, drRow.Item("StudentID"))
                        End If
                    Next
                End If
            ElseIf Session("sbsuid") <> "125010" And Session("sbsuid") <> "123004" Then
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 6 Then
                            NVCStudentPage.Add(1, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 6 And intRowCnt < 11 Then
                            NVCStudentPage.Add(2, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 11 And intRowCnt < 16 Then
                            NVCStudentPage.Add(3, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 16 And intRowCnt < 21 Then
                            NVCStudentPage.Add(4, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 21 And intRowCnt < 26 Then
                            NVCStudentPage.Add(5, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 26 And intRowCnt < 31 Then
                            NVCStudentPage.Add(6, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 31 And intRowCnt < 36 Then
                            NVCStudentPage.Add(7, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 36 And intRowCnt < 41 Then
                            NVCStudentPage.Add(8, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 41 And intRowCnt < 46 Then
                            NVCStudentPage.Add(9, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 46 And intRowCnt < 51 Then
                            NVCStudentPage.Add(10, drRow.Item("StudentID"))
                        Else
                            NVCStudentPage.Add(11, drRow.Item("StudentID"))
                        End If

                    Next
                End If
            ElseIf Session("sbsuid") = "125010" Then  'for tws show 10records in one page
                If dsStudents.Tables(0).Rows.Count >= 1 Then
                    For Each drRow As DataRow In dsStudents.Tables(0).Rows
                        intRowCnt += 1
                        If intRowCnt < 11 Then
                            NVCStudentPage.Add(1, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 11 And intRowCnt < 21 Then
                            NVCStudentPage.Add(2, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 21 And intRowCnt < 31 Then
                            NVCStudentPage.Add(3, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 31 And intRowCnt < 41 Then
                            NVCStudentPage.Add(4, drRow.Item("StudentID"))
                        ElseIf intRowCnt >= 41 And intRowCnt < 51 Then
                            NVCStudentPage.Add(5, drRow.Item("StudentID"))
                        Else
                            NVCStudentPage.Add(6, drRow.Item("StudentID"))
                        End If

                    Next
                End If
            Else
                For Each drRow As DataRow In dsStudents.Tables(0).Rows
                    NVCStudentPage.Add(1, drRow.Item("StudentID"))
                Next
                btnSave.Text = "Save"
                btnSave1.Text = "Save"
            End If

            ViewState("StuPages") = NVCStudentPage
            ViewState("PageNo") = "1"
            ViewState("TotalPages") = NVCStudentPage.Count
            lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")

            Dim i As Integer
            Dim dt As New DataTable
            dt.Columns.Add(New DataColumn("pageno", GetType(String)))
            Dim dr As DataRow
            For i = 1 To Val(ViewState("TotalPages"))
                dr = dt.NewRow
                dr.Item(0) = i
                dt.Rows.Add(dr)
            Next

            dlPages.DataSource = dt
            dlPages.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lblPageNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblPageNo As LinkButton
        lblPageNo = TryCast(sender.FindControl("lblPageNo"), LinkButton)
        ViewState("PageNo") = CInt(lblPageNo.Text)
        lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")
        gvAceStudents.Columns.Clear()
        BindStudents()
    End Sub
    Private Function BindStudents()
        Dim strSql As String = ""
        Dim strCriteria As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        gvAceStudents.DataSourceID = ""
        Dim tmpClmCount As Integer = 0
        Dim str_filter_StudentNo As String = String.Empty
        Dim str_filter_StudName As String = String.Empty
        Dim txtSearch As New TextBox
        Dim str_search As String
        Dim str_STUD_NO As String = String.Empty
        Dim str_STUD_NAME As String = String.Empty
        Dim ds As DataSet
        Dim GvHeader As NameValueCollection
        Dim strCondition As String = ""
        Dim strStuIDs As String = ""
        Dim cArr As New ArrayList


        Try

            If Not ViewState("StuPages") Is Nothing Then
                NVCStudentPage = ViewState("StuPages")
                If Not NVCStudentPage.GetValues(ViewState("PageNo").ToString) Is Nothing Then
                    Dim x As String() = NVCStudentPage.GetValues(ViewState("PageNo").ToString)
                    For Each x1 As String In x
                        strStuIDs = strStuIDs + x1 + ","
                    Next
                    If strStuIDs.Contains(",") = True Then
                        strStuIDs = strStuIDs.Substring(0, Len(strStuIDs) - 1)
                    End If
                    strCriteria += " AND STU_ID IN (" & strStuIDs & ")"
                End If
            End If



            If H_GRP_ID.Value <> "" And H_GRP_ID.Value <> "0" Then
                strCriteria += " AND SGR_ID='" & H_GRP_ID.Value & "' "
            End If
            'If Not ddlLevel.SelectedIndex = -1 Then
            '    strCriteria += " AND STU_SCT_ID='" & H_SCT_ID.Value & "' "
            'End If
            If txtStudName.Text <> "" Then
                strCriteria += " AND STU_ID=" & H_STU_ID.Value & " "
            End If


            If Session("CURRENT_ACD_ID") = H_ACD_ID.Value Then
                strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName,GRM_DISPLAY as Grade,SCT_DESCR as Section, " & GetReportHeader() & "  " & _
                        " FROM VW_STUDENT_DETAILS " & _
                        " INNER JOIN ACE_STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID AND SSD_ACD_ID='" & H_ACD_ID.Value & "'" & _
                        " INNER JOIN ACE_GROUP_M ON SSD_SGR_ID=SGR_ID " & _
                        " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"
            Else
                strSql = "SELECT DISTINCT(STU_NO) as StudentNo,STU_ID as StudentID,STU_NAME as StudentName,GRM_DISPLAY,SCT_DESCR " & GetReportHeader() & "  " & _
                 " FROM VW_STUDENT_DETAILS_PREVYEARS " & _
                 " WHERE STU_ACD_ID='" & H_ACD_ID.Value & "' AND STU_BSU_ID = '" & Session("sBSUID") & "'"
            End If


            strSql += strCriteria & " ORDER BY GRM_DISPLAY,SCT_DESCR,STU_NAME"
            GvHeader = Session("ReportHeader")

            If GvHeader.Count > 0 Then
                dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
                If dsStudents.Tables(0).Rows.Count = 0 Then
                    BindBlankRow()
                    trSave1.Visible = False
                    trSave.Visible = False

                Else
                    Dim Col As DataControlField
                    Dim clmCnt As Integer = 0

                    For Each clm As DataColumn In dsStudents.Tables(0).Columns

                        If clmCnt < 5 Then
                            Dim Field As New BoundField
                            Field.DataField = clm.ColumnName
                            Field.HeaderText = clm.ColumnName
                            Col = Field
                            gvAceStudents.Columns.Insert(clmCnt, Col)
                        ElseIf clmCnt >= 5 Then
                            tmpClmCount = tmpClmCount + 1
                            Dim TField As New TemplateField
                            Dim DTField As DynamicTemplate = New DynamicTemplate(ListItemType.Item)
                            Dim TxtItem As New TextBox()
                            Dim PnlItem As New Panel()
                            TxtItem.ID = "txt" + tmpClmCount.ToString

                            If GvHeader.Item(clm.ColumnName).Split("_")(3) <> "" Then
                                TxtItem.CssClass = GvHeader.Item(clm.ColumnName).Split("_")(3)
                                If TxtItem.CssClass = "textboxmulti" Then
                                    TxtItem.TextMode = TextBoxMode.MultiLine
                                    TxtItem.EnableTheming = False

                                End If
                            End If
                            If GvHeader.Item(clm.ColumnName).Split("_")(2) = "C" And TxtItem.CssClass = "textboxmulti" Then
                                TxtItem.SkinID = "MultiText"
                                TxtItem.Visible = True
                                TxtItem.Text = ""

                                '-----------------------------------------
                                Dim imgbtn As New ImageButton
                                imgbtn.ID = "img" + tmpClmCount.ToString
                                imgbtn.ImageUrl = "../Images/comment.jpg"
                                imgbtn.Width = 20
                                imgbtn.Height = 20
                                imgbtn.ToolTip = "Press Alt + Z "
                                imgbtn.AccessKey = "Z"
                                DTField.AddControl(TxtItem, "Text", clm.ColumnName)
                                DTField.AddControl(imgbtn, "ToolTip", clm.ColumnName)

                                cArr.Add(TxtItem.ID)
                            ElseIf GvHeader.Item(clm.ColumnName).Split("_")(2) = "D" Then
                                Dim ddlItem As New DropDownList
                                ddlItem = GetHeaderValues(ddlItem, GvHeader.Item(clm.ColumnName).Split("_")(0))
                                If Session("sbsuid") = "125010" Then
                                    ' If ddlItem.Width > 70 Then
                                    If ddlAce.SelectedValue = "11" Then
                                        ddlItem.Width = 70
                                    End If

                                    'End If
                                End If
                                If ddlItem.Items.Count > 1 Then
                                    ddlItem.ID = "txt" + tmpClmCount.ToString
                                    DTField.AddControl(ddlItem, "Text", clm.ColumnName)
                                Else
                                    TxtItem.TextMode = TextBoxMode.SingleLine
                                    DTField.AddControl(TxtItem, "Text", clm.ColumnName)
                                End If
                            Else
                                'TxtItem.CssClass = "inputboxnor" 'inputbox_multi
                                TxtItem.TextMode = TextBoxMode.SingleLine
                                TxtItem.Visible = True
                                TxtItem.Text = ""
                                'TxtItem.Width = 150

                                '------------------------------------------
                                DTField.AddControl(TxtItem, "Text", clm.ColumnName)

                            End If
                            TField.ItemTemplate = DTField
                            TField.HeaderText = clm.ColumnName
                            gvAceStudents.Columns.Insert(clmCnt, TField)

                        End If
                        clmCnt = clmCnt + 1
                    Next
                    H_CommentCOLS.Value = tmpClmCount
                    Session("ColummnCnt") = tmpClmCount
                    gvAceStudents.DataSource = dsStudents.Tables(0)
                    gvAceStudents.DataBind()

                    gvAceStudents.Columns(1).HeaderStyle.Width = 0
                    gvAceStudents.Columns(1).ItemStyle.Width = 0
                    'imgReport.Enabled = False
                    'imgRptSchedule.Enabled = False
                    'ImgRptHeader.Enabled = False
                    ddlReport.Enabled = False
                    ddlAce.Enabled = False
                    btnView.Enabled = False
                    ddlRepSch.Enabled = False
                    ddlLevel.Enabled = False
                    ddlGroup.Enabled = False
                    'txtSection.Enabled = False
                    trSave1.Visible = True
                    trSave.Visible = True

                End If
            Else
                BindBlankRow()
                trSave1.Visible = False
                trSave.Visible = False
            End If

            Dim cAr As String()
            ' ReDim cAr(cArr.Count * gvAceStudents.Rows.Count)
            Dim k As Integer
            Dim p As Integer
            Dim t As Integer = 0
            Dim cst As String = ""


            Dim LENGTH_TEXT As Integer

            Dim lengthFunction As String

            If Session("sbsuid") = "125017" Then
                If ddlReport.SelectedItem.Text.ToString.ToUpper.Contains("MID SEMESTER") Then
                    LENGTH_TEXT = 300
                Else
                    LENGTH_TEXT = 560
                End If
            Else
                LENGTH_TEXT = 3000
            End If
            lengthFunction = "function isMaxLength(txtBox,evt) {"
            lengthFunction += " if(txtBox) { "
            '   lengthFunction += "alert(txtBox.value.length);"
            lengthFunction += " var charCode = (evt.which) ? evt.which : event.keyCode ;"
            lengthFunction += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            lengthFunction += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            lengthFunction += "{ return true;} else {return false;}"
            '            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            lengthFunction += " }"
            lengthFunction += "}"




            lengthFunction += "function isMaxLength1(txtBox,evt) {"
            lengthFunction += " if(txtBox) { "
            '   lengthFunction += "alert(txtBox.value.length);"
            lengthFunction += "var str=txtBox.value; var charCode = (evt.which) ? evt.which : event.keyCode ;"
            lengthFunction += " if (charCode==46 || charCode==8 || charCode==37 || charCode==38 || charCode==39 || charCode==40 || charCode==35 || charCode==36    ){return true;}"
            lengthFunction += "  if (txtBox.value.length<" + LENGTH_TEXT.ToString + ")"
            lengthFunction += "{ return true;} else {alert('The comment length has exceeded then maximum permissible length of " + LENGTH_TEXT.ToString + " characters.The remaining text will be truncated.');txtBox.value=str.substring(0," + (LENGTH_TEXT - 1).ToString + ");return false;}"
            '            lengthFunction += "     return ( txtBox.value.length <=" + LENGTH_TEXT.ToString + ");"
            lengthFunction += " }"
            lengthFunction += "}"


            Dim txtItem1 As TextBox

            For p = 0 To gvAceStudents.Rows.Count - 1
                For k = 0 To cArr.Count - 1
                    If cst <> "" Then
                        cst += "|"
                    End If
                    cst += gvAceStudents.Rows(p).FindControl(cArr.Item(k).ToString).ClientID
                    Try
                        txtItem1 = gvAceStudents.Rows(p).FindControl(cArr.Item(k).ToString)
                        txtItem1.Attributes.Add("onkeydown", "return isMaxLength(this,event);")
                        txtItem1.Attributes.Add("onblur", "return isMaxLength1(this,event);")
                        ClientScript.RegisterClientScriptBlock(Page.GetType(), txtItem1.ClientID, lengthFunction, True)
                    Catch ex As Exception
                    End Try
                Next
            Next

            If cst <> "" Then
                cAr = cst.Split("|")
                RadSpell1.ControlsToCheck = cAr
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed-"
        End Try
    End Function

    Private Function GetReportHeader() As String
        Try
            Dim NVCReportSetup As New NameValueCollection
            Dim strHeader As String = ""
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim dsHeader As DataSet


            Dim strSQL As String = "SELECT CAST(RSD_ID AS VARCHAR) + '_' + CAST(RSD_RSM_ID AS VARCHAR) + '_' + RSD_RESULT + '_' + RSD_CSSCLASS AS Value ,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER FROM RPT.ACE_REPORT_SETUP_D WHERE RSD_RSM_ID=" & H_RPT_ID.Value & " AND RSD_BDIRECTENTRY=1" _
            & " AND RSD_ACM_ID=" + ddlAce.SelectedValue.ToString + " AND ISNULL(RSD_ACL_ID,0)=" + ddlLevel.SelectedValue.ToString

            If ddlMainHeader.Items.Count <> 0 Then
                strSQL += " AND RSD_MAIN_HEADER_ID=" + ddlMainHeader.SelectedValue.ToString
            End If

            strSQL += " ORDER BY RSD_DISPLAYORDER"
            dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
            With dsHeader.Tables(0)
                For intCnt As Integer = 0 To .Rows.Count - 1
                    strHeader += "'' AS '" + .Rows(intCnt).Item("RSD_HEADER").ToString + "' ,"
                    NVCReportSetup.Add(.Rows(intCnt).Item("RSD_HEADER").ToString, .Rows(intCnt).Item("Value").ToString)
                Next
                If strHeader.Length > 1 Then
                    strHeader = strHeader.Substring(0, strHeader.Length - 1)
                End If
            End With
            Session("ReportHeader") = NVCReportSetup
            Return strHeader
        Catch ex As Exception

        End Try

    End Function

    Sub GetHeaderKeys()
        Dim conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim dsProcessRule As New DataSet()
        Dim sb As New StringBuilder

        Dim grade As String() = ddlAce.SelectedValue.Split("|")

        Dim sqlstr As String = ""
        Dim reader As SqlDataReader

        Try
            If Session("SBSUID") = "125017" Then

                sb.Append("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>PA - Positive Attitude</Div>")
                sb.Append("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>SA - Satisfactory Attitude</Div>")
                sb.Append("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>GE - Greater Effort</Div>")
                sb.Append("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>N/A- Not Applicable</Div>")


                ltProcess.Text = sb.ToString
                If sb.ToString = "" Then
                    lbtnKeys.Visible = False
                Else
                    lbtnKeys.Visible = True
                End If

                lbtnKeys.Text = "Keys : "
            Else

                Dim strSQL As String = "SELECT ISNULL(RSD_DESC,'-') AS RSD_SUB_DESC ,REPLACE(RSD_HEADER,'.','') AS RSD_HEADER " _
                                       & " FROM RPT.ACE_REPORT_SETUP_D WHERE RSD_RSM_ID=" & ddlReport.SelectedValue.ToString _
                                       & " AND RSD_BDIRECTENTRY=1  " _
                                       & " RSD_ACM_ID=" + ddlAce.SelectedValue.ToString _
                                       & " ISNULL(RSD_ACL_ID,0)=" + ddlLevel.SelectedValue.ToString _
                                       & " ORDER BY RSD_DISPLAYORDER "

                reader = SqlHelper.ExecuteReader(conn, CommandType.Text, strSQL)

                While reader.Read
                    sb.Append("<DIV style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #800000;font-weight: bold;'>" + reader.GetString(1) + " - " + reader.GetString(0) + "</Div>")
                End While
                reader.Close()
                'If sb.ToString = "" Then
                '    sb.Append("<div style='text-align:left;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #000000;padding:5pt;font-weight: bold;'>No Rule Available !!!</div>")
                'End If
                ltProcess.Text = sb.ToString
                If sb.ToString = "" Then
                    lbtnKeys.Visible = False
                Else
                    lbtnKeys.Visible = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed--"
        End Try
    End Sub



#End Region

#Region "Pick Up Values"










#End Region

#Region " Button event Handlings"



    Protected Sub btnSubCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblPages.Text = ""
    End Sub



#End Region

    'ts

    Protected Sub btnView_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If ddlLevel.Items.Count <> 0 Then

                H_GRP_ID.Value = ddlGroup.SelectedValue.ToString

                GetStudentsList()
                BindStudents()
                'Session("SBGID") = H_ACM_ID.Value
                Session("Grade") = ddlAce.SelectedValue

                If Session("sbsuid") = "114003" Or Session("sbsuid") = "125017" Then
                    GetHeaderKeys()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed---"
        End Try
    End Sub

    Protected Sub gvAceStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'Try
        '    Try
        '        gvAceStudents.PageIndex = e.NewPageIndex
        '        BindStudents()
        '    Catch ex As Exception
        '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        '    End Try
        'Catch ex As Exception

        'End Try
    End Sub


    Sub FillAcd()

        ddlAcdID.DataSource = ActivityFunctions.GetBSU_ACD_YEAR(Session("sBsuid"), Session("clm"))
        ddlAcdID.DataTextField = "ACY_DESCR"
        ddlAcdID.DataValueField = "ACD_ID"
        ddlAcdID.DataBind()
        Dim str_con As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        str_query = " SELECT ACY_DESCR,ACD_ID FROM OASIS..ACADEMICYEAR_M AS A INNER JOIN OASIS..ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" & Session("sBsuid") & "' AND ACD_CLM_ID=" & Session("clm")
        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcdID.Items(ddlAcdID.Items.IndexOf(li)).Selected = True
        If Not ddlAcdID.SelectedIndex = -1 Then
            H_ACD_ID.Value = ddlAcdID.SelectedItem.Value
        End If

        'ddlAcademicYear.Items.FindByValue(Session("ACY_ID")).Selected = True
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuComments") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                    gvAceStudents.Columns.Clear()
                    'BindBlankRow()
                    BindStudents()
                End If
            End If

            Dim url As String
            'ViewState("datamode") = "add"
            'ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            'url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            'Response.Redirect(url)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed="
        End Try
    End Sub
    Private Function GetStudComments(ByVal STUID As String) As DataSet
        Dim i As Integer
        Dim txtComments As New TextBox
        Dim lblStudId As New Label
        Dim strSql As String
        Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CCAConnectionString
        Try

            'strSql = "SELECT RST_ID,RST_RSD_ID,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER,RSD_RESULT,RST_COMMENTS FROM RPT.ACE_REPORT_STUDENT_S S INNER JOIN RPT.ACE_REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID WHERE " _
            '        & " S.RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
            '        & " S.RST_GRD_ID='" & H_ACM_ID.Value & "' AND " _
            '        & " S.RST_STU_ID ='" & STUID & "' AND " _
            '        & " S.RST_RPF_ID='" & H_SCH_ID.Value & "' AND " _
            '        & " REPLACE(D.RSD_HEADER,'.',' ')='" & ColHeader & "' ORDER BY RST_ID"

            'If ddlAcdID.SelectedValue.ToString = Session("current_acd_id") Then

            strSql = "SELECT RST_ID,RST_RSD_ID,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER,RSD_RESULT,RST_COMMENTS FROM " _
                     & " RPT.ACE_REPORT_STUDENT_S S INNER JOIN RPT.ACE_REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID WHERE " _
                     & " S.RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
                     & " S.RST_ACM_ID='" & ddlAce.SelectedValue.ToString & "' AND " _
                     & " S.RST_ACL_ID='" & ddlLevel.SelectedValue.ToString & "' AND " _
                     & " S.RST_STU_ID ='" & STUID & "' AND " _
                     & " S.RST_RPF_ID='" & H_SCH_ID.Value & "' AND " _
                     & " RST_ACM_ID= " + ddlAce.SelectedValue.ToString
            'Else

            '    strSql = "SELECT RST_ID,RST_RSD_ID,REPLACE(RSD_HEADER,'.',' ') AS RSD_HEADER,RSD_RESULT,RST_COMMENTS FROM RPT.REPORT_STUDENT_PREVYEARS S INNER JOIN RPT.ACE_REPORT_SETUP_D D ON S.RST_RSD_ID=D.RSD_ID WHERE " _
            '         & " S.RST_ACD_ID='" & H_ACD_ID.Value & "' AND " _
            '         & " S.RST_GRD_ID='" & H_ACM_ID.Value & "' AND " _
            '         & " S.RST_STU_ID ='" & STUID & "' AND " _
            '         & " S.RST_RPF_ID='" & H_SCH_ID.Value & "' AND " _
            '         & " RSD_bALLSUBJECTS='FALSE' "


            'End If


            If ddlMainHeader.Items.Count <> 0 Then
                strSql += " AND RSD_MAIN_HEADER_ID=" + ddlMainHeader.SelectedValue.ToString
            End If

            strSql += " ORDER BY RST_ID"

            Dim dsHeader As DataSet
            dsHeader = SqlHelper.ExecuteDataset(str_con, CommandType.Text, strSql)
            Return dsHeader

            '        End If
            '    Next
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed=="
        End Try
    End Function

    Protected Sub btnSearchStud_No_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudents()
    End Sub

    Protected Sub btnSearchStud_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudents()
    End Sub

    Protected Sub gvAceStudents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim cs As ClientScriptManager = Page.ClientScript
        Dim dsHeader As DataSet
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                e.Row.Cells(0).CssClass = "locked"
                e.Row.Cells(1).CssClass = "locked"


                Dim i As Integer
                For intcnt As Integer = 1 To CInt(Session("ColummnCnt"))

                    ''------- To Attach the Client Id For Building Javascript Array.
                    cs.RegisterArrayDeclaration("txt" + intcnt.ToString, String.Concat("'", e.Row.FindControl("txt" + intcnt.ToString).ClientID, "'"))
                    ''-------- To Handle Image Button ---------------
                    Dim imgBtn As ImageButton = TryCast(e.Row.FindControl("img" + intcnt.ToString), ImageButton)
                    If Not imgBtn Is Nothing Then
                        Dim strStuId As String = e.Row.Cells(1).Text
                        Dim str As String = gvAceStudents.Columns(intcnt + 2).HeaderText
                        imgBtn.OnClientClick = "javascript:getcomments('" + e.Row.FindControl("txt" + intcnt.ToString).ClientID + "','ALLGENCMTS','" + str + "','" + strStuId + "','" + H_RPT_ID.Value + "'); return false;"
                    End If

                    'Dim spell As RadSpell = TryCast(e.Row.FindControl("spell" + intcnt.ToString), RadSpell)
                    'If Not spell Is Nothing Then
                    '    spell.ControlToCheck = e.Row.FindControl("txt" + intcnt.ToString).ID
                    '    spell.IsClientID = False
                    '    'spell.
                    'End If

                    dsHeader = GetStudComments(e.Row.Cells(1).Text)

                    For Each DsCol As DataColumn In TryCast(gvAceStudents.DataSource, DataTable).Columns

                        If Not dsHeader Is Nothing AndAlso dsHeader.Tables(0).Rows.Count >= 1 Then
                            For i = 0 To dsHeader.Tables(0).Rows.Count - 1
                                With dsHeader.Tables(0).Rows(i)
                                    If .Item("RSD_RESULT").ToString = "C" Then
                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("RST_COMMENTS")
                                                    'TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("FTC_COMMENTS")
                                                End If
                                            End If

                                        End If
                                    ElseIf .Item("RSD_RESULT").ToString = "D" Then
                                        If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                            If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                                If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                                    If e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0).GetType.ToString = "System.Web.UI.WebControls.DropDownList" Then
                                                        TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_COMMENTS").ToString

                                                    Else
                                                        TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), TextBox).Text = .Item("RST_COMMENTS")

                                                    End If
                                                End If
                                            End If

                                        End If
                                        'ElseIf .Item("RSD_RESULT").ToString = "M" Then
                                        '    If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                        '        If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                        '            If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                        '                TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_MARK").ToString
                                        '            End If
                                        '        End If

                                        '    End If
                                        'ElseIf .Item("RSD_RESULT").ToString = "G" Then
                                        '    If .Item("RSD_HEADER").ToString = DsCol.ColumnName Then
                                        '        If e.Row.Cells(DsCol.Ordinal).Controls.Count >= 1 Then
                                        '            If e.Row.Cells(DsCol.Ordinal).Controls(0).GetType.ToString = "System.Web.UI.WebControls.PlaceHolder" Then
                                        '                TryCast(e.Row.Cells(DsCol.Ordinal).Controls(0).Controls(0), DropDownList).SelectedValue = .Item("RST_GRADING").ToString
                                        '            End If
                                        '        End If

                                        '    End If
                                    Else

                                    End If
                                End With
                            Next
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed*"
        End Try

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Style.Value = "display:none"
        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Style.Value = "display:none"
        End If

    End Sub

   
    Sub BindAce()
        Dim strSql As String
        Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcdID.SelectedValue.ToString

        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        ddlAce.DataSource = ds
        ddlAce.DataTextField = "ACM_DESCR"
        ddlAce.DataValueField = "ACM_ID"
        ddlAce.DataBind()
        If Not ddlAce.SelectedIndex = -1 Then
            H_ACM_ID.Value = ddlAce.SelectedItem.Value
        End If
    End Sub

    Protected Sub ddlAce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlAce.SelectedIndex = -1 Then
            H_ACM_ID.Value = ddlAce.SelectedItem.Value
        End If
        BindLevel()
        BindMainHeader()
        BindGroup()
    End Sub

    Protected Sub ddlAcdID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlAcdID.SelectedIndex = -1 Then
            H_ACD_ID.Value = ddlAcdID.SelectedItem.Value
        End If
        FillReport()
        BindAce()
        FillRepSchedule()
        BindLevel()
        BindMainHeader()
        BindGroup()
    End Sub

    Private Function AddRecord()
        Dim ldrNew As DataRow
        Dim lintIndex As Integer
        Dim GvHeader As NameValueCollection
        Dim txtCtrl As TextBox
        Dim strVArray() As String
        Dim strColArray() As String
        Dim IntRow As Integer = 0
        Dim IntCols As Integer = 0
        Dim dsHeader As DataSet
        Try
            If ViewState("datamode") = "add" And Not Session("ReportHeader") Is Nothing Then ' Session("ReportHeader")
                GvHeader = Session("ReportHeader")
                strVArray = H_CommentCOLS.Value.Split("|")
                For Each gvRow As GridViewRow In gvAceStudents.Rows
                    strColArray = strVArray(IntRow).Split(",")
                    IntRow = IntRow + 1
                    If CheckArray(strColArray) = True Then
                        For IntCol As Integer = 5 To gvAceStudents.Columns.Count - 1
                            If strColArray(IntCol - 5).ToString <> "" Then

                                ldrNew = Session("StuComments").NewRow
                                ldrNew("ID") = gvAceStudents.Rows.Count + 1

                                ldrNew("RST_RPF_ID") = H_SCH_ID.Value
                                ldrNew("RST_RSD_ID") = GvHeader.Item(gvAceStudents.Columns(IntCol).HeaderText).Split("_")(0) 'H_SETUP.Value
                                ldrNew("RST_ACD_ID") = H_ACD_ID.Value
                                ldrNew("RST_ACM_ID") = ddlAce.SelectedValue
                                ldrNew("RST_ACL_ID") = ddlLevel.SelectedValue

                                'ldrNew("RST_STU_ID") = H_STU_ID.Value & gvRow.Cells(1).Text
                                ldrNew("RST_STU_ID") = gvRow.Cells(1).Text

                                If GvHeader.Item(gvAceStudents.Columns(IntCol).HeaderText).Split("_")(2) <> "" Then
                                    If GvHeader.Item(gvAceStudents.Columns(IntCol).HeaderText).Split("_")(2) = "C" Then
                                        'ldrNew("RST_COMMENTS") = strColArray(IntCol - 3).ToString 'txtComments.Text
                                        ldrNew("RST_COMMENTS") = strColArray(IntCol - 5).ToString.Replace("~", ",")
                                    ElseIf GvHeader.Item(gvAceStudents.Columns(IntCol).HeaderText).Split("_")(2) = "D" Then
                                        ldrNew("RST_COMMENTS") = strColArray(IntCol - 5).ToString
                                    End If
                                End If
                                Session("StuComments").Rows.Add(ldrNew)
                            Else
                                DeleteHeaderComments(H_SCH_ID.Value, GvHeader.Item(gvAceStudents.Columns(IntCol).HeaderText).Split("_")(0), H_ACD_ID.Value, H_ACM_ID.Value, Convert.ToInt32(gvRow.Cells(1).Text))
                            End If
                        Next
                 
                    End If
                Next
            End If
            If ViewState("datamode") = "edit" Then
                If Not Session("StuComments") Is Nothing Then
                    For lintIndex = 0 To Session("StuComments").Rows.Count - 1
                        If (Session("StuComments").Rows(lintIndex)("ID") = Session("gintGridLine")) Then

                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed***"
        End Try
    End Function
    Private Function CheckArray(ByVal StrArr() As String) As Boolean
        Dim blnReturn As Boolean = False
        For Each Str As String In StrArr
            If Str <> "" And Str <> "0" And Str <> "--" Then
                blnReturn = True
            End If
        Next
        Return blnReturn
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            If gvAceStudents.Rows.Count >= 1 Then
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
            lblPages.Text = ""

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed!"
        End Try
    End Sub
    Private Function BindBlankRow()

        Dim strSql As String = ""
        Dim dsStudents As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        gvAceStudents.DataSourceID = ""
        Try
            strSql = "SELECT DISTINCT(STU_NO) " & _
                    " FROM VW_STUDENT_DETAILS " & _
                    " INNER JOIN ACE_STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID " & _
                    " INNER JOIN ACE_GROUP_M ON SSD_SGR_ID=SGR_ID " & _
                    " INNER JOIN ACE_GROUPS_TEACHER_S ON SGS_SGR_ID=SGR_ID " & _
                    " WHERE SSD_ACM_ID='" & H_ACM_ID.Value & "' AND STU_ACD_ID=" & ddlAcdID.SelectedValue & " AND SGR_ID='" & H_GRP_ID.Value & "' " & _
                    " AND STU_ID=1 AND SGS_EMP_ID=" & Session("EmployeeId")
            dsStudents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)

            If dsStudents.Tables(0).Rows.Count = 0 Then
                gvAceStudents.DataSource = dsStudents.Tables(0)
                dsStudents.Tables(0).Rows.Add(dsStudents.Tables(0).NewRow())
                gvAceStudents.DataBind()
                gvAceStudents.Columns.Clear()
                gvAceStudents.DataSource = Nothing
                gvAceStudents.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed!!"
        End Try
    End Function

    Protected Sub btnCanceldata_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gvAceStudents.Columns.Clear()
            BindBlankRow()
            btnView.Enabled = True
            txtStudName.Text = ""
            H_SCH_ID.Value = 0
            H_STU_ID.Value = 0


            H_SCT_ID.Value = 0
            ddlReport.Enabled = True
            ddlAce.Enabled = True
            ddlRepSch.Enabled = True
            ddlLevel.Enabled = True
            ddlLevel.Enabled = True
            ddlGroup.Enabled = True
            trSave1.Visible = False
            trSave.Visible = False
            FillAcd()
            FillReport()
            BindAce()
            FillRepSchedule()
            BindLevel()
            lblPages.Text = ""
            BindMainHeader()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed!!!"
        End Try

    End Sub
    Sub FillReport()
        Dim strSQL As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        strSQL = "SELECT RSM_ID,RSM_DESCR " _
                    & " FROM RPT.ACE_REPORT_SETUP_M " _
                    & " WHERE " _
                    & " RSM_BSU_ID='" & Session("SBsuid") & "' AND " _
                    & " RSM_ACD_ID='" & H_ACD_ID.Value & "' order by RSM_DISPLAYORDER"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        ddlReport.DataSource = ds
        ddlReport.DataTextField = "RSM_DESCR"
        ddlReport.DataValueField = "RSM_ID"
        ddlReport.DataBind()
        If Not ddlReport.SelectedIndex = -1 Then
            H_RPT_ID.Value = ddlReport.SelectedItem.Value
        End If

    End Sub

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlReport.SelectedIndex = -1 Then
            H_RPT_ID.Value = ddlReport.SelectedItem.Value
        End If
        BindAce()
        FillRepSchedule()
        BindLevel()
        BindMainHeader()
    End Sub

    Protected Sub ImgSection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
    Sub FillRepSchedule()
        Dim str_Sql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_critirea As String = ""
        If H_RPT_ID.Value <> "" Then
            str_critirea = " WHERE RPF_RSM_ID=" & H_RPT_ID.Value & ""
        End If
        str_Sql = "SELECT RPF_ID,RPF_DESCR FROM RPT.ACE_REPORT_PRINTEDFOR_M"
        str_Sql = str_Sql & str_critirea & " ORDER BY RPF_DISPLAYORDER"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlRepSch.DataSource = ds
        ddlRepSch.DataTextField = "RPF_DESCR"
        ddlRepSch.DataValueField = "RPF_ID"
        ddlRepSch.DataBind()
        If Not ddlRepSch.SelectedIndex = -1 Then
            H_SCH_ID.Value = ddlRepSch.SelectedItem.Value
        End If

    End Sub

    Protected Sub ddlRepSch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlRepSch.SelectedIndex = -1 Then
            H_SCH_ID.Value = ddlRepSch.SelectedItem.Value
        End If
        BindLevel()
    End Sub
    Sub BindLevel()
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_critirea As String = ""

        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("SBSUID") + "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlLevel.Items.Insert(0, li)
        If Not ddlLevel.SelectedIndex = -1 Then
            H_SCT_ID.Value = ddlLevel.SelectedItem.Value
        End If

    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM ACE_GROUP_M WHERE SGR_ACM_ID=" + ddlAce.SelectedValue.ToString _
                       & " AND ISNULL(SGR_ACL_ID,0)=" + ddlLevel.SelectedValue.ToString
        Else

            str_query = "SELECT SGR_ID,SGR_DESCR FROM ACE_GROUP_M " _
                   & " INNER JOIN ACE_GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID" _
                   & " AND SGS_TODATE IS NULL" _
                   & " WHERE SGR_ACM_ID=" + ddlAce.SelectedValue.ToString _
                   & " AND ISNULL(SGR_ACL_ID,0)=" + ddlLevel.SelectedValue.ToString _
                   & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel.SelectedIndex = -1 Then
            H_SCT_ID.Value = ddlLevel.SelectedItem.Value
        End If
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuComments") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                    gvAceStudents.Columns.Clear()

                    If CInt(ViewState("PageNo")) < CInt(ViewState("TotalPages")) Then
                        ViewState("PageNo") = CInt(ViewState("PageNo")) + 1
                    End If
                    lblPages.Text = "Page " & ViewState("PageNo") & " Out of " & ViewState("TotalPages")

                    BindStudents()
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed#"
        End Try
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String = ""
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            If gvAceStudents.Rows.Count >= 1 Then
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Else
                url = String.Format("~\Curriculum\rptStudentComments.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
            lblError.Text = ""

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed##"
        End Try
    End Sub
    Private Function GetHeaderValues(ByVal ddlList As DropDownList, ByVal RSDID As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim dsHeader As DataSet
        Dim StrSql As String
        'Dim StrSql As String = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID & " UNION SELECT '--' as RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S "


        '      StrSql = "SELECT RSP_DESCR FROM RPT.REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID & " ORDER BY RSP_DISPLAYORDER"
        StrSql = "SELECT RSP_DESCR FROM(SELECT RSP_DESCR,isnull(RSP_DISPLAYORDER,0) as RSP_DISPLAYORDER FROM RPT.ACE_REPORTSETUP_DEFAULTS_S WHERE RSP_RSM_ID=" & H_RPT_ID.Value & " AND RSP_RSD_ID=" & RSDID _
      & " UNION ALL SELECT '--',-1" _
              & " )PP ORDER BY RSP_DISPLAYORDER"



        dsHeader = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSql)

        ddlList.DataSource = dsHeader
        ddlList.DataTextField = "RSP_DESCR"
        ddlList.DataValueField = "RSP_DESCR"
        ddlList.DataBind()

        Session("ddlListSelValue") = ddlList.Items(0).Value.ToString
        ddlList.ClearSelection()
        Return ddlList

    End Function
    Private Function DeleteHeaderComments(ByVal RPFID As Integer, ByVal RSDID As Integer, ByVal ACDID As Integer, ByVal GRDID As String, ByVal STUID As Integer)
        Try
            Dim strSQL As String
            strSQL = "DELETE FROM RPT.ACE_REPORT_STUDENT_S WHERE RST_RPF_ID=" & RPFID & " " & _
                     " AND RST_RSD_ID=" & RSDID & " AND RST_ACD_ID=" & ACDID & " AND RST_GRD_ID='" & GRDID & "' AND RST_STU_ID=" & STUID & ""

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim IntResult As Integer
            IntResult = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strSQL)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Function



    Protected Sub btnSaveDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDetails.Click
        Try
            If ViewState("datamode") = "edit" Then
                'UpdateMarksentry()


            ElseIf ViewState("datamode") = "add" Then
                If Not Session("StuComments") Is Nothing Then
                    AddRecord()
                    SaveRecord()
                    gvAceStudents.Columns.Clear()

                    BindStudents()
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed#"
        End Try
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindGroup()
        BindMainHeader()
    End Sub
End Class
