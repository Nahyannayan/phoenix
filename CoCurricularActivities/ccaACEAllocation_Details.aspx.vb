﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class CoCurricularActivities_ccaACEAllocation_Details
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10022") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    trErrors.Visible = False
                    'ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindAcademicYear()
                    BindLevel()
                    BindGrades()
                    trGV1.Visible = False
                    trGV1a.Visible = False
                    trGV2.Visible = False

                    If ddlAcademicYear.SelectedValue.ToString = Session("Current_ACD_ID") Then
                        rbtnStudentStatus.Visible = False
                    Else
                        rbtnStudentStatus.Visible = True
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try
        End If
    End Sub

    Sub BindAcademicYear()

        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACD_ID FROM OASIS..ACADEMICYEAR_D INNER JOIN OASIS..ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID" _
                                & " WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID='" + Session("clm") + "' AND ACD_ID IN (" + Session("Current_Acd_ID") + "," + Session("Next_ACD_ID") + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()

    End Sub

    Sub BindGrid()

        trGV2.Visible = True
        gvAllocationDetails.Visible = True
        Dim flag As Integer


        If ddlAcademicYear.SelectedValue.ToString = Session("NEXT_ACD_ID") Then
            'GetPrevGrade(ddlGrade.SelectedValue.ToString)
            flag = 0
        Else
            'hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
            flag = 1
        End If

        Dim i As Integer
        Dim strGrade As String = ""

        If ddlGrade.SelectedIndex = 0 Then
            For i = 1 To ddlGrade.Items.Count - 1
                If strGrade <> "" Then
                    strGrade += "|"

                End If
                strGrade += ddlGrade.Items(i).Text
            Next
        Else
            strGrade = ddlGrade.SelectedItem.Text
        End If

        Dim stuStatus As Integer
        If rbtnStudentStatus.Visible = True Then
            stuStatus = rbtnStudentStatus.SelectedIndex
        Else
            stuStatus = 0
        End If


        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(11) As SqlParameter
        param(0) = New SqlParameter("@ALG_ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@ALG_ACL_ID", ddlLevel.SelectedItem.Value)
        param(2) = New SqlParameter("@ALD_STU_GRDID", strGrade)
        param(3) = New SqlParameter("@STATUS", rbtnStatus.SelectedItem.Value)
        param(4) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(5) = New SqlParameter("@FLAG", flag)
        param(6) = New SqlParameter("@STU_NAME", txtStuName.Text)
        param(7) = New SqlParameter("@STU_NO", txtStuID.Text)
        param(8) = New SqlParameter("@CURR_ACD_ID", Session("Current_ACD_ID"))
        param(9) = New SqlParameter("@STU_STATUS", stuStatus)
        param(10) = New SqlParameter("@CLM_ID", Session("clm"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_ALLOCATION_DETAILS", param)
        gvAllocationDetails.DataSource = ds
        gvAllocationDetails.DataBind()

    End Sub

    Sub BindGamesGrid()

        trGV1.Visible = True
        gvGamesDetails.Visible = True
        Dim i As Integer
        Dim strGrade As String = ""
        Dim flag As Integer
        If ddlAcademicYear.SelectedValue.ToString = Session("NEXT_ACD_ID") Then
            flag = 0
        Else
            flag = 1
        End If

        If ddlGrade.SelectedIndex = 0 Then
            For i = 1 To ddlGrade.Items.Count - 1
                If strGrade <> "" Then
                    strGrade += "|"

                End If
                strGrade += ddlGrade.Items(i).Text
            Next
        Else
            strGrade = ddlGrade.SelectedItem.Text
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@ALG_ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@ALG_ACL_ID", ddlLevel.SelectedItem.Value)
        param(2) = New SqlParameter("@ALD_STU_NEXTGRD_ID", strGrade)
        param(3) = New SqlParameter("@ALG_ACT_ID", "1")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GETGAMESDETAILS", param)
        gvGamesDetails.DataSource = ds
        gvGamesDetails.DataBind()
    End Sub

    Protected Sub gvGamesDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvGamesDetails.PageIndex = e.NewPageIndex
        BindGamesGrid()
    End Sub

    Sub BindClubsGrid()

        trGV1.Visible = True
        gvClubsDetails.Visible = True

        Dim flag As Integer
        Dim strGrade As String = ""
        If ddlAcademicYear.SelectedValue.ToString = Session("NEXT_ACD_ID") Then
            flag = 0
        Else
            flag = 1
        End If
        Dim i As Integer
        If ddlGrade.SelectedIndex = 0 Then


            For i = 1 To ddlGrade.Items.Count - 1
                If strGrade <> "" Then
                    strGrade += "|"

                End If
                strGrade += ddlGrade.Items(i).Text
            Next
        Else
            strGrade = ddlGrade.SelectedItem.Text
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@ALG_ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@ALG_ACL_ID", ddlLevel.SelectedItem.Value)
        param(2) = New SqlParameter("@ALD_STU_NEXTGRD_ID", strGrade)
        param(3) = New SqlParameter("@ALG_ACT_ID", "2")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GETCLUBSDETAILS", param)
        gvClubsDetails.DataSource = ds
        gvClubsDetails.DataBind()

    End Sub

    Protected Sub gvClubsDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvClubsDetails.PageIndex = e.NewPageIndex
        BindClubsGrid()
    End Sub

    Sub BindCOMMGrid()

        trGV1a.Visible = True
        gvCommDetails.Visible = True
        Dim i As Integer
        Dim strGrade As String = ""
        Dim flag As Integer
        If ddlAcademicYear.SelectedValue.ToString = Session("NEXT_ACD_ID") Then
            flag = 0
        Else
            flag = 1
        End If

        If ddlGrade.SelectedIndex = 0 Then
            For i = 1 To ddlGrade.Items.Count - 1
                If strGrade <> "" Then
                    strGrade += "|"

                End If
                strGrade += ddlGrade.Items(i).Text
            Next
        Else
            strGrade = ddlGrade.SelectedItem.Text
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@ALG_ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@ALG_ACL_ID", ddlLevel.SelectedItem.Value)
        param(2) = New SqlParameter("@ALD_STU_NEXTGRD_ID", strGrade)
        param(3) = New SqlParameter("@ALG_ACT_ID", "5")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GETGAMESDETAILS", param)
        gvCommDetails.DataSource = ds
        gvCommDetails.DataBind()

    End Sub

    Protected Sub gvCommDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvCommDetails.PageIndex = e.NewPageIndex
        BindCOMMGrid()
    End Sub


    Sub BindGrades()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACL_ID", ddlLevel.SelectedItem.Value)
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_ACEGRADES", param)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_GRD_ID"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("ALL", "0"))
    End Sub

    Sub BindLevel()
        trLabelError.Visible = True
        ddlLevel.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM OASIS_CCA..ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("sBsuid") + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()

    End Sub

    Public Sub GetPrevGrade(ByVal vGRD_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER - 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            hfGRD_ID.Value = dr("GRD_ID")
        End While

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Dim grade As String
        If ddlAcademicYear.SelectedValue.ToString = Session("Next_ACD_ID") Then
            GetPrevGrade(ddlGrade.SelectedValue.ToString)
        Else
            hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        End If
        'BindGamesGrid()
        'BindClubsGrid()
        'BindGrid()
        trGV1.Visible = False
        trGV1a.Visible = False
        trGV2.Visible = False
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        If ddlAcademicYear.SelectedValue.ToString = Session("Current_ACD_ID") Then
            rbtnStudentStatus.Visible = False
        Else
            rbtnStudentStatus.Visible = True
        End If
        'BindGamesGrid()
        'BindClubsGrid()
        'BindGrid()
        trGV1.Visible = False
        trGV1a.Visible = False
        trGV2.Visible = False
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindGrades()
        'BindGamesGrid()
        'BindClubsGrid()
        trGV1.Visible = False
        trGV1a.Visible = False
        trGV2.Visible = False
    End Sub

    Public Sub GetNextGrade(ByVal vGRD_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER + 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            hfGRD_ID_NEXT.Value = dr("GRD_ID")
        End While

    End Sub

    Protected Sub gvAllocationDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAllocationDetails.RowDataBound


        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim lblGame As Label = CType(e.Row.FindControl("lblGame"), Label)
            Dim lblClub As Label = CType(e.Row.FindControl("lblClub"), Label)
            Dim lblCOMM As Label = CType(e.Row.FindControl("lblCOMM"), Label)
            Dim lblGrade As Label = CType(e.Row.FindControl("lblGrdID"), Label)
            Dim strGrade As String
            If ddlAcademicYear.SelectedValue.ToString = Session("NEXT_ACD_ID") Then
                GetNextGrade(lblGrade.Text.Substring(0, 2))
                strGrade = hfGRD_ID_NEXT.Value
            Else
                strGrade = lblGrade.Text.Substring(0, 2)
            End If

            Dim lblStuID As Label = CType(e.Row.FindControl("lblStuID"), Label)
            Dim stuID As String = lblStuID.Text

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

            Dim lblGender As Label = CType(e.Row.FindControl("lblGender"), Label)
            Dim strGender As String = lblGender.Text

            'If ddlGrade.SelectedIndex = 0 Then
            '    strGrade = ddlGrade.Items(1).Text
            'Else
            '    strGrade = ddlGrade.SelectedItem.Text
            'End If

            Dim paramSet(4) As SqlParameter
            paramSet(0) = New SqlParameter("@STU_NO", stuID)
            paramSet(1) = New SqlParameter("@ACL_ID", ddlLevel.SelectedItem.Value)
            paramSet(2) = New SqlParameter("@GRD_ID", strGrade)
            paramSet(3) = New SqlParameter("@TYPE", 1)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_CURRENT_GAMECLUB", paramSet)
            Dim flag As Integer = 0
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblGame.Text = ds.Tables(0).Rows(0).Item(0)
                Else
                    lblGame.Text = ""
                End If
            Else
                lblGame.Text = ""
            End If


            paramSet(0) = New SqlParameter("@STU_NO", stuID)
            paramSet(1) = New SqlParameter("@ACL_ID", ddlLevel.SelectedItem.Value)
            paramSet(2) = New SqlParameter("@GRD_ID", strGrade)
            paramSet(3) = New SqlParameter("@TYPE", 2)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_CURRENT_GAMECLUB", paramSet)
            flag = 0
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblClub.Text = ds.Tables(0).Rows(0).Item(0)
                Else
                    lblClub.Text = ""
                End If
            Else
                lblClub.Text = ""
            End If

            paramSet(0) = New SqlParameter("@STU_NO", stuID)
            paramSet(1) = New SqlParameter("@ACL_ID", ddlLevel.SelectedItem.Value)
            paramSet(2) = New SqlParameter("@GRD_ID", strGrade)
            paramSet(3) = New SqlParameter("@TYPE", 5)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_CURRENT_GAMECLUB", paramSet)
            flag = 0
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblCOMM.Text = ds.Tables(0).Rows(0).Item(0)
                Else
                    lblCOMM.Text = ""
                End If
            Else
                lblCOMM.Text = ""
            End If




        End If


    End Sub

    Protected Sub gvAllocationDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvAllocationDetails.RowEditing

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim index As Integer = e.NewEditIndex
        Dim mySelectedRow As GridViewRow = gvAllocationDetails.Rows(index)
        Dim ddlGames As DropDownList
        Dim ddlClubs As DropDownList
        Dim ddlCOMM As DropDownList
        Dim lnkbtnEdit As LinkButton
        Dim lnkbtnUpdate As LinkButton
        Dim lnkbtnCancel As LinkButton
        Dim lblGame As Label
        Dim lblClub As Label
        Dim lblCOMM As Label
        Dim lblStuID As Label
        lblStuID = mySelectedRow.FindControl("lblStuID")
        Dim stuID As String = lblStuID.Text
        Dim lblGender As Label = mySelectedRow.FindControl("lblGender")
        Dim strGender As String = lblGender.Text
        lblClub = mySelectedRow.FindControl("lblClub")
        lblGame = mySelectedRow.FindControl("lblGame")
        lblCOMM = mySelectedRow.FindControl("lblCOMM")
        ddlGames = mySelectedRow.FindControl("ddlGames")
        ddlClubs = mySelectedRow.FindControl("ddlClubs")
        ddlCOMM = mySelectedRow.FindControl("ddlCOMM")
        lnkbtnUpdate = mySelectedRow.FindControl("lnkbtnUpdate")
        lnkbtnCancel = mySelectedRow.FindControl("lnkbtnCancel")
        lnkbtnEdit = mySelectedRow.FindControl("lnkbtnEdit")
        Dim lblGrade As Label
        lblGrade = mySelectedRow.FindControl("lblGrdID")
        Dim strGrade As String
        If ddlAcademicYear.SelectedValue.ToString = Session("NEXT_ACD_ID") Then
            GetNextGrade(lblGrade.Text.Substring(0, 2))
            strGrade = hfGRD_ID_NEXT.Value
        Else
            strGrade = lblGrade.Text.Substring(0, 2)
        End If

        lnkbtnUpdate.Visible = True
        lnkbtnCancel.Visible = True
        lnkbtnEdit.Visible = False
        ddlGames.Enabled = True
        ddlClubs.Enabled = True
        ddlGames.Visible = True
        ddlCOMM.Enabled = True
        ddlClubs.Visible = True
        ddlCOMM.Visible = True
        lblClub.Visible = False
        lblGame.Visible = False
        lblCOMM.Visible = False


        ddlGames.Items.Clear()
        ddlClubs.Items.Clear()
        ddlCOMM.Items.Clear()

        Dim strAction As String = "LOADCURRENTGRADE"
        ' Code to bind the Games for the chosen level
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_GENDER", strGender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", strGrade)
        param(3) = New SqlParameter("@TYPE", 1)
        param(4) = New SqlParameter("@ACTION", strAction)
        param(5) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_ACE_LEVELS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlGames.DataSource = ds
            ddlGames.DataTextField = "ACM_DESCR"
            ddlGames.DataValueField = "ACM_ID"
            ddlGames.DataBind()
            ddlGames.Items.Insert(0, New ListItem("--", "0"))
        End If

        ' Code to bind the clubs for the chosen level

        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_GENDER", strGender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", strGrade)
        param(3) = New SqlParameter("@TYPE", 2)
        param(4) = New SqlParameter("@ACTION", strAction)
        param(5) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_ACE_LEVELS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlClubs.DataSource = ds
            ddlClubs.DataTextField = "ACM_DESCR"
            ddlClubs.DataValueField = "ACM_ID"
            ddlClubs.DataBind()
            ddlClubs.Items.Insert(0, New ListItem("--", "0"))
        End If

        ' Code to bind the Communications for the chosen level

        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_GENDER", strGender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", strGrade)
        param(3) = New SqlParameter("@TYPE", 5)
        param(4) = New SqlParameter("@ACTION", strAction)
        param(5) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_ACE_LEVELS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlCOMM.DataSource = ds
            ddlCOMM.DataTextField = "ACM_DESCR"
            ddlCOMM.DataValueField = "ACM_ID"
            ddlCOMM.DataBind()
            ddlCOMM.Items.Insert(0, New ListItem("--", "0"))
        End If




        Dim flag As Integer = 0
        If Not ddlGames.Items.FindByText(lblGame.Text) Is Nothing Then
            ddlGames.ClearSelection()
            ddlGames.Items.FindByText(lblGame.Text).Selected = True
            flag = 1
        End If

        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_GENDER", strGender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", strGrade)
        param(3) = New SqlParameter("@TYPE", "1")
        param(4) = New SqlParameter("@ACTION", "CHECK")
        param(5) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACTIVITIES_NOTFILLED]", param)
        Dim ddlCount As Integer = ddlGames.Items.Count
        Dim i As Integer
        Dim j As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ddlGames.Items.Count - 1
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If i > ddlGames.Items.Count - 1 Then
                        Exit For
                    Else
                        If ddlGames.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                            If lblGame.Text <> ds.Tables(0).Rows(j).Item(0) Then
                                ddlGames.Items.RemoveAt(i)
                                If i >= 2 Then
                                    i = i - 2
                                Else
                                    i = 0
                                End If

                            End If
                        End If
                    End If
                Next
            Next
        End If
        hfClubs.Value = lblClub.Text
        hfGames.Value = lblGame.Text
        hfCOMM.Value = lblCOMM.Text

        If Not ddlClubs.Items.FindByText(lblClub.Text) Is Nothing Then
            ddlClubs.ClearSelection()
            ddlClubs.Items.FindByText(lblClub.Text).Selected = True
            'flag = 1
        End If

        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_GENDER", strGender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", strGrade)
        param(3) = New SqlParameter("@TYPE", "2")
        param(4) = New SqlParameter("@ACTION", "CHECK")
        param(5) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACTIVITIES_NOTFILLED]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ddlClubs.Items.Count - 1
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If i > ddlClubs.Items.Count - 1 Then
                        Exit For
                    Else
                        If ddlClubs.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                            If lblClub.Text <> ds.Tables(0).Rows(j).Item(0) Then
                                ddlClubs.Items.RemoveAt(i)
                                If i >= 2 Then
                                    i = i - 2
                                Else
                                    i = 0
                                End If

                            End If
                        End If
                    End If
                Next
            Next
        End If

        'FOR COMMUNICATION

        If Not ddlCOMM.Items.FindByText(lblCOMM.Text) Is Nothing Then
            ddlCOMM.ClearSelection()
            ddlCOMM.Items.FindByText(lblCOMM.Text).Selected = True
            'flag = 1    'ENABLE TO MAKE IT MANDATORY
        End If

        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(1) = New SqlParameter("@STU_GENDER", strGender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", strGrade)
        param(3) = New SqlParameter("@TYPE", "5")
        param(4) = New SqlParameter("@ACTION", "CHECK")
        param(5) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACTIVITIES_NOTFILLED]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ddlCOMM.Items.Count - 1
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If i > ddlCOMM.Items.Count - 1 Then
                        Exit For
                    Else
                        If ddlCOMM.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                            If lblCOMM.Text <> ds.Tables(0).Rows(j).Item(0) Then
                                ddlCOMM.Items.RemoveAt(i)
                                If i >= 2 Then
                                    i = i - 2
                                Else
                                    i = 0
                                End If

                            End If
                        End If
                    End If
                Next
            Next
        End If


    End Sub

    Protected Sub gvAllocationDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvAllocationDetails.RowUpdating

        Try

            Dim index As Integer = e.RowIndex
            Dim mySelectedRow As GridViewRow = gvAllocationDetails.Rows(index)
            Dim ddlGames As DropDownList
            ddlGames = mySelectedRow.FindControl("ddlGames")
            Dim ddlClubs As DropDownList
            ddlClubs = mySelectedRow.FindControl("ddlClubs")
            Dim ddlCOMM As DropDownList
            ddlCOMM = mySelectedRow.FindControl("ddlCOMM")
            Dim lblStuID As Label
            lblStuID = mySelectedRow.FindControl("lblStuID")
            Dim strStuID As String = lblStuID.Text
            Dim lnkbtnUpdate As LinkButton
            Dim lnkbtnCancel As LinkButton
            Dim lnkbtnEdit As LinkButton
            Dim lblGrdID As Label
            Dim lblGame As Label
            Dim lblClub As Label
            Dim lblCOMM As Label
            lblGame = mySelectedRow.FindControl("lblGame")
            lblClub = mySelectedRow.FindControl("lblClub")
            lblCOMM = mySelectedRow.FindControl("lblCOMM")
            lnkbtnUpdate = mySelectedRow.FindControl("lnkbtnUpdate")
            lnkbtnCancel = mySelectedRow.FindControl("lnkbtnCancel")
            lnkbtnEdit = mySelectedRow.FindControl("lnkbtnEdit")
            lblGrdID = mySelectedRow.FindControl("lblGrdID")
            Dim strGrade = lblGrdID.Text.Substring(0, 2)
            Dim flag As Integer
            Dim gamesError As Integer
            Dim clubsError As Integer
            Dim COMMError As Integer
            If ddlAcademicYear.SelectedValue = Session("Next_ACD_ID") Then
                flag = 0
                If strGrade = "04" Or strGrade = "05" Or strGrade = "06" Then
                    If ddlGames.SelectedIndex = 0 Then
                        gamesError = 1
                    End If
                    If ddlClubs.SelectedIndex = 0 Then
                        clubsError = 1
                    End If
                End If
                If strGrade = "07" Or strGrade = "08" Or strGrade = "09" Or strGrade = "10" Or strGrade = "11" Then
                    If ddlGames.SelectedIndex = 0 Then
                        gamesError = 1
                    End If
                End If
                If strGrade = "09" Or strGrade = "10" Or strGrade = "11" Or strGrade = "12" Then
                    If ddlCOMM.SelectedIndex = 0 Then
                        COMMError = 1
                    End If
                End If

            Else
                flag = 1
                If strGrade = "05" Or strGrade = "06" Or strGrade = "07" Then
                    If ddlGames.SelectedIndex = 0 Then
                        gamesError = 1
                    End If
                    If ddlClubs.SelectedIndex = 0 Then
                        clubsError = 1
                    End If
                End If
                If strGrade = "08" Or strGrade = "09" Or strGrade = "10" Or strGrade = "11" Or strGrade = "12" Then
                    If ddlGames.SelectedIndex = 0 Then
                        gamesError = 1
                    End If
                End If
                If strGrade = "08" Or strGrade = "09" Or strGrade = "10" Or strGrade = "11" Then
                    If ddlCOMM.SelectedIndex = 0 Then
                        COMMError = 1
                    End If
                End If
            End If



            If gamesError = 1 Or clubsError = 1 Or COMMError = 1 Then
                trErrors.Visible = True
                If gamesError = 1 Then
                    lblGamesError.Text = "Please choose a Game"
                Else
                    lblGamesError.Text = ""
                End If
                If clubsError = 1 Then
                    lblClubsError.Text = "Please choose a Club"
                Else
                    lblClubsError.Text = ""
                End If
                If COMMError = 1 Then
                    lblCOMMError.Text = "Please choose a Communication"
                Else
                    lblCOMMError.Text = ""
                End If
                lnkbtnUpdate.Visible = True
                lnkbtnCancel.Visible = True
                lnkbtnEdit.Visible = False
                Exit Sub
            Else
                lnkbtnUpdate.Visible = False
                lnkbtnCancel.Visible = False
                lnkbtnEdit.Visible = True
                lblClubsError.Text = ""
                lblGamesError.Text = ""
                lblCOMMError.Text = ""
            End If

            'Dim str_conn1 As String = ConnectionManger.GetOASISConnectionString
            'Dim str As String = "SELECT ACD_CLM_ID FROM OASIS..STUDENT_M INNER JOIN OASIS..ACADEMICYEAR_D ON STU_ACD_ID=ACD_ID WHERE STU_NO='" + strStuID + "'"
            'Dim ds2 As DataSet = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, str)
            'Dim strClm As String = ds2.Tables(0).Rows(0).Item(0)

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim param(11) As SqlParameter
            param(0) = New SqlParameter("@STU_NO", strStuID)
            param(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
            param(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(3) = New SqlParameter("@ACL_ID", ddlLevel.SelectedItem.Value)
            param(4) = New SqlParameter("@GRD_ID", strGrade)
            param(5) = New SqlParameter("@GAME", ddlGames.SelectedValue)
            param(6) = New SqlParameter("@CLUB", ddlClubs.SelectedValue)
            param(7) = New SqlParameter("@COMM", ddlCOMM.SelectedValue)
            param(8) = New SqlParameter("@FLAG", flag)
            param(9) = New SqlParameter("@CLM_ID", Session("clm"))
            param(10) = New SqlParameter("@STATUS", "0")

            ddlGames.Enabled = False
            ddlClubs.Enabled = False
            ddlCOMM.Enabled = False
            If ddlGames.SelectedIndex > -1 Then
                lblGame.Text = ddlGames.SelectedItem.Text
            End If
            If ddlGames.SelectedIndex > -1 Then
                lblClub.Text = ddlClubs.SelectedItem.Text
            End If
            If ddlCOMM.SelectedIndex > -1 Then
                lblCOMM.Text = ddlCOMM.SelectedItem.Text
            End If
            ddlGames.Visible = False
            ddlClubs.Visible = False
            ddlCOMM.Visible = False
            lblGame.Visible = True
            lblClub.Visible = True
            lblCOMM.Visible = True
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.UPDATEACEDETAILS", param)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblError.Text = "Record has been updated successfully"

                    BindGamesGrid()
                    BindClubsGrid()
                    BindCOMMGrid()

                End If
            End If
            lblError.Text = "Record has been updated successfully"
            BindGamesGrid()
            BindClubsGrid()
            BindCOMMGrid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub rbtnStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnStatus.SelectedIndexChanged
        'BindGrid()
        trGV1.Visible = False
        trGV1a.Visible = False
        trGV2.Visible = False
    End Sub

    Protected Sub gvAllocationDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAllocationDetails.PageIndexChanging
        Try
            gvAllocationDetails.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGamesGrid()
        BindClubsGrid()
        BindCOMMGrid()
        BindGrid()

    End Sub

    Protected Sub gvAllocationDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvAllocationDetails.RowCancelingEdit
        Try

            Dim index As Integer = e.RowIndex
            Dim mySelectedRow As GridViewRow = gvAllocationDetails.Rows(index)
            Dim ddlGames As DropDownList
            ddlGames = mySelectedRow.FindControl("ddlGames")
            Dim ddlClubs As DropDownList
            ddlClubs = mySelectedRow.FindControl("ddlClubs")
            Dim ddlCOMM As DropDownList
            ddlCOMM = mySelectedRow.FindControl("ddlCOMM")
            Dim lblStuID As Label
            lblStuID = mySelectedRow.FindControl("lblStuID")
            Dim lblGame As Label
            lblGame = mySelectedRow.FindControl("lblGame")
            Dim lblClub As Label
            lblClub = mySelectedRow.FindControl("lblClub")
            Dim lblCOMM As Label
            lblCOMM = mySelectedRow.FindControl("lblCOMM")
            Dim strStuID As String = lblStuID.Text
            Dim lnkbtnEdit As LinkButton
            lnkbtnEdit = mySelectedRow.FindControl("lnkbtnEdit")
            lnkbtnEdit.Visible = True
            Dim lnkbtnUpdate As LinkButton
            lnkbtnUpdate = mySelectedRow.FindControl("lnkbtnUpdate")
            lnkbtnUpdate.Visible = False
            Dim lnkbtnCancel As LinkButton
            lnkbtnCancel = mySelectedRow.FindControl("lnkbtnCancel")
            lnkbtnCancel.Visible = False
            Dim strGrade = ddlGrade.SelectedItem.Text

            If Not ddlClubs.Items.FindByText(hfClubs.Value) Is Nothing Then
                ddlClubs.ClearSelection()
                ddlClubs.Items.FindByText(hfClubs.Value).Selected = True

            End If

            If Not ddlGames.Items.FindByText(hfGames.Value) Is Nothing Then
                ddlGames.ClearSelection()
                ddlGames.Items.FindByText(hfGames.Value).Selected = True
            End If

            If Not ddlCOMM.Items.FindByText(hfCOMM.Value) Is Nothing Then
                ddlCOMM.ClearSelection()
                ddlCOMM.Items.FindByText(hfCOMM.Value).Selected = True
            End If

            'lblGame.Text = hfGames.Value
            'lblClub.Text = hfClubs.Value

            If lblGame.Text = "--" Then
                lblGame.Text = ""
            End If
            If lblClub.Text = "--" Then
                lblClub.Text = ""
            End If
            If lblCOMM.Text = "--" Then
                lblCOMM.Text = ""
            End If

            ddlGames.Enabled = False
            ddlClubs.Enabled = False
            ddlCOMM.Enabled = False
            ddlGames.Visible = False
            ddlClubs.Visible = False
            ddlCOMM.Visible = False
            lblGame.Visible = True
            lblClub.Visible = True
            lblCOMM.Visible = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvGamesDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGamesDetails.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblActName As Label = CType(e.Row.FindControl("lblActName"), Label)
            Dim lblAlloted As Label = CType(e.Row.FindControl("lblAlloted"), Label)
            Dim lblSeatCount As Label = CType(e.Row.FindControl("lblSeatCount"), Label)
            lblSeatCount.ForeColor = Drawing.Color.Red
            lblAlloted.ForeColor = Drawing.Color.Green

        End If
    End Sub

    Protected Sub gvClubsDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvClubsDetails.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblActNameCl As Label = CType(e.Row.FindControl("lblActNameCl"), Label)
            Dim lblAllotedCl As Label = CType(e.Row.FindControl("lblAllotedCl"), Label)
            Dim lblSeatCl As Label = CType(e.Row.FindControl("lblSeatCl"), Label)
            lblSeatCl.ForeColor = Drawing.Color.Red
            lblAllotedCl.ForeColor = Drawing.Color.Green

        End If
    End Sub
    Protected Sub gvCommDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCommDetails.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblActNameCOMM As Label = CType(e.Row.FindControl("lblActNameCOMM"), Label)
            Dim lblAllotedCOMM As Label = CType(e.Row.FindControl("lblAllotedCOMM"), Label)
            Dim lblSeatCOMM As Label = CType(e.Row.FindControl("lblSeatCountCOMM"), Label)
            lblSeatCOMM.ForeColor = Drawing.Color.Red
            lblAllotedCOMM.ForeColor = Drawing.Color.Green

        End If
    End Sub

    Protected Sub rbtnStudentStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnStudentStatus.SelectedIndexChanged
        trGV1.Visible = False
        trGV1a.Visible = False
        trGV2.Visible = False
    End Sub

  
  
 
End Class
