<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAttenEntry.aspx.vb" Inherits="CoCurricularActivities_ccaAttenEntry"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Attendance Entry"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="left" class="matters">
                            <asp:Button ID="lnkAdd" runat="server" CssClass="button"
                                Text="Add"></asp:Button>
                            <asp:Button ID="lnkEdit" runat="server" CssClass="button" Text="Edit"></asp:Button>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                        </td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Activity</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlActivity" AutoPostBack="true" runat="server" /><br />
                            <asp:LinkButton ID="lnkActivity" runat="server">Create/Edit Temp</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Term</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTerm" AutoPostBack="true" runat="server" />
                        </td>
                        <td align="left" class="matters"><span class="field-label">Group</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlGroup" AutoPostBack="true" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Date</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="true" />
                        </td>
                        <td align="left" class="matters"><span class="field-label">Time From</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtTfrm" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Time To</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtTTo" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="t1" runat="server" visible="false">
                        <td align="left" class="matters"><span class="field-label">Subject</span>
                        </td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlSubject" runat="server" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td id="Td1" align="center" class="matters" runat="server" colspan="4">
                            <asp:GridView ID="grdStud" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="atd" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="AtdId" runat="server" Text='<%# bind("ATD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CMTID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="AtId" runat="server" Text='<%# bind("SSD_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Present">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cb1" runat="server" Text='<%# Bind("ATD_PRESENT") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Absent Reason">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlReason" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtdesc" SkinID="MultiText" TextMode="multiline" runat="server"
                                                Text='<%# Bind("ATD_DESC") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="H_CAT_ID" runat="server" />
</asp:Content>
