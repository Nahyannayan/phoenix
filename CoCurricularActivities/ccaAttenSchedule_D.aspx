<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAttenSchedule_D.aspx.vb" Inherits="CoCurricularActivities_ccaAttenSchedule_D"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script type="text/javascript" language="javascript">

        function CheckAll(Checkbox) {
            var GridVwHeaderChckbox = document.getElementById("<%=grdType.ClientID %>");
            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
            }
        }



        function TPick(txt) {

            $("#" + txt).timepicker(
        {
            showPeriod: true,
            onHourShow: OnHourShowCallback,
            onMinuteShow: OnMinuteShowCallback
        });

        }


        function OnHourShowCallback(hour) {
            if ((hour > 20) || (hour < 6)) {
                return false; // not valid
            }
            return true; // valid
        }
        function OnMinuteShowCallback(hour, minute) {
            if ((hour == 20) && (minute >= 30)) { return false; } // not valid
            if ((hour == 6) && (minute < 30)) { return false; }   // not valid
            return true;  // valid
        }






    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Attendance Schedule"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <asp:Menu ID="mnuMaster" Orientation="Horizontal" CssClass="menu_a" runat="server" OnMenuItemClick="mnuMaster_MenuItemClick">
                                <Items>
                                    <asp:MenuItem Text="Add New" Value="0" Selected="True"></asp:MenuItem>
                                    <asp:MenuItem Text="Edit/Delete" Value="1"></asp:MenuItem>
                                </Items>
                                <StaticMenuItemStyle CssClass="menuItem" />
                                <StaticSelectedStyle CssClass="selectedItem" />
                                <StaticHoverStyle CssClass="hoverItem" />
                            </asp:Menu>
                            <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">
                                <asp:View ID="View1" runat="server">
                                    <table id="Table2" runat="server" width="100%">
                                        <tr>
                                            <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                                            </td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                                            </td>
                                            <td align="left" class="matters" width="20%"><span class="field-label">Activity</span>
                                            </td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlActivity" AutoPostBack="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="matters"><span class="field-label">Activity level</span>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlLevel" AutoPostBack="true" runat="server"  />
                                            </td>
                                            <td align="left" class="matters"><span class="field-label">Term</span>
                                            </td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlTerm" AutoPostBack="true" runat="server"   />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="matters"><span class="field-label">Date From</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFrom" runat="server" ></asp:TextBox>
                                                <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                                    Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                            <td align="left" class="matters"><span class="field-label">Date To</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTo" runat="server" ></asp:TextBox>
                                                <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                    Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="matters" colspan="4">
                                                <asp:GridView ID="GrdApply" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                    Width="100%">
                                                    <RowStyle CssClass="griditem"   />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblappl" runat="server"  Text='<%# Bind("dt_name") %>'> </asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="From Time">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="tfrm" runat="server"> </asp:TextBox>
                                                            </ItemTemplate>
                                                           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="To Time">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="tTo" runat="server"> </asp:TextBox>
                                                                <asp:Label ID="lber" runat="server" CssClass="error" Text="*" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                          <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="Green" />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply" />
                                            </td>
                                        </tr>
                                        <tr id="Tr6" runat="server">
                                            <td id="Td12" align="center" class="matters" runat="server" colspan="4">
                                                <asp:GridView ID="grdType" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                    PageSize="20" Width="100%">
                                                    <RowStyle CssClass="griditem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="CMTID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="tyId" runat="server" Text='<%# bind("SGR_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkboxSelectAll" runat="server" onclick="CheckAll(this);" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="Cb1" runat="server" />
                                                            </ItemTemplate>
                                                     
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Group Name">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Group Name"></asp:Label><br />
                                                                <asp:TextBox ID="txtStudName" runat="server" ></asp:TextBox>
                                                                <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                    OnClick="btnStudName_Search_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgroup" runat="server"  Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:GridView ID="Grdday" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                                    Width="100%">
                                                                    <RowStyle CssClass="griditem"   />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblday" runat="server"   Text='<%# Bind("dt_name") %>'> </asp:Label>
                                                                            </ItemTemplate>
                                                                           <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="From Time">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="fromT" runat="server" Text='<%# Bind("dt_ft") %>'> </asp:TextBox>
                                                                                <%-- <telerik:RadTimePicker ID="fromT" runat="server" DbSelectedDate='<%# Bind("dt_ft") %>'></telerik:RadTimePicker>--%>
                                                                            </ItemTemplate>
                                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="To Time">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="ToT" runat="server" Text='<%# Bind("dt_tt") %>'> </asp:TextBox>
                                                                                <%--<telerik:RadTimePicker ID="ToT" runat="server" DbSelectedDate='<%# Bind("dt_tt") %>'></telerik:RadTimePicker>--%>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <SelectedRowStyle CssClass="Green" />
                                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" ></HeaderStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="Green" />
                                                    <HeaderStyle CssClass="gridheader_pop"   />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                            </td>
                                        </tr>
                                    </table>
                                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <table id="tbl" width="100%">
                                        <tr>
                                            <td align="left" class="matters" width="20%"><span class="field-label"> Academic Year</span>
                                            </td>
                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlacyear" AutoPostBack="true" runat="server" />
                                            </td>
                                            <td align="left" class="matters" width="20%"><span class="field-label">Activity</span>
                                            </td>
                                            <td align="left" width="40%">
                                                <asp:DropDownList ID="ddlact" AutoPostBack="true" runat="server" />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="left" class="matters"><span class="field-label">Term</span>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlTermEdit" AutoPostBack="true" runat="server"   />
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="matters"><span class="field-label">Date From</span>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtDtefrm" runat="server" ></asp:TextBox><asp:ImageButton
                                                    ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" TargetControlID="txtDtefrm">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                            <td align="left" class="matters"><span class="field-label">Date To</span>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtDteTo" runat="server" ></asp:TextBox>
                                                <asp:ImageButton
                                                    ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" CssClass="MyCalendar"
                                                    Format="dd/MMM/yyyy" PopupButtonID="ImageButton2" TargetControlID="txtDteTo">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:ImageButton ID="btnSearch" runat="server"
                                                    ImageUrl="../Images/forum_search.gif" ImageAlign="Top" ></asp:ImageButton>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="left" class="matters"><span class="field-label">Group</span>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlGroup" AutoPostBack="true" runat="server" />
                                            </td>
                                            <td align="left" class="matters"><span class="field-label">Day Name</span>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlDayName" AutoPostBack="true" runat="server">
                                                    <asp:ListItem>--</asp:ListItem>
                                                    <asp:ListItem>Sunday</asp:ListItem>
                                                    <asp:ListItem>Monday</asp:ListItem>
                                                    <asp:ListItem>Tuesday</asp:ListItem>
                                                    <asp:ListItem>Wednesday</asp:ListItem>
                                                    <asp:ListItem>Thursday</asp:ListItem>
                                                    <asp:ListItem>Friday</asp:ListItem>
                                                    <asp:ListItem>Saturday</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="Tr1" runat="server">
                                            <td id="Td1" align="center" class="matters" runat="server" colspan="4">
                                                <asp:GridView ID="grdEdit" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                    PageSize="20" Width="100%">
                                                    <RowStyle CssClass="griditem"   />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="CMTID" Visible="False">
                                                            <ItemTemplate>
                                                                <asp:Label ID="AtId" runat="server" Text='<%# bind("AT_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                            <asp:TextBox ID="txtDate" runat="server"  Text='<%# bind("AT_date","{0:dd/MMMM/yyyy}")  %>'></asp:TextBox>
                                                                            <asp:ImageButton ID="ImageB" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                                                                Format="dd/MMM/yyyy" PopupButtonID="ImageB" TargetControlID="txtDate">
                                                                            </ajaxToolkit:CalendarExtender>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Day Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblday" runat="server"   Text='<%# Bind("at_dayname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Time From">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="tpFrom" runat="server" Text='<%# Bind("at_tfrm") %>'>
                                                                </asp:TextBox>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Time To">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="tpTo" runat="server" Text='<%# Bind("at_tto") %>'>
                                                                </asp:TextBox>
                                                                <asp:Label ID="lberr" runat="server" CssClass="error" Text="*" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  ></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:ButtonField CommandName="update" Text="Update" HeaderText="Update">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                        </asp:ButtonField>
                                                        <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                                    Text="Delete"></asp:LinkButton>
                                                                <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                                    runat="server">
                                                                </ajaxToolkit:ConfirmButtonExtender>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="Green" />
                                                    <HeaderStyle CssClass="gridheader_pop"  />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
