﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_ccaPEActivity_Settings
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC50006") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindActivitySchedule()
                    Gridbind()
                    gvRpt.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub


    Sub Gridbind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM PE.ACTIVITY_M WHERE ACM_BSU_ID='" + Session("SBSUID") + "'" _
                                 & " ORDER BY ACM_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvRpt.DataSource = ds
        gvRpt.DataBind()

    End Sub


    Sub BindActivityDetails(ByVal acm_id As String, ByVal chkSelect As CheckBox, ByVal chkAward As CheckBox)
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACT_ACS_ID,isnull(ACT_bAWARD,'FALSE') FROM PE.ACTIVITY_D WHERE ACT_ACS_ID='" + ddlSchedule.SelectedValue.ToString + "'" _
                                & " AND ACT_ACM_ID='" + acm_id + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            chkSelect.Checked = True
            chkAward.Checked = ds.Tables(0).Rows(i).Item(1)
        Next

    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim chkAward As CheckBox
        Dim lblAcmId As Label


        For i = 0 To gvRpt.Rows.Count - 1
            With gvRpt.Rows(i)
                chkSelect = .FindControl("chkSelect")
                chkAward = .FindControl("chkAward")
                lblAcmId = .FindControl("lblAcmId")
            End With

            str_query = "exec PE.saveACTIVITY_D " _
                      & " @bSELECT=" + chkSelect.Checked.ToString + "," _
                      & " @ACT_ACM_ID=" + lblAcmId.Text + "," _
                      & " @ACT_ACS_ID=" + ddlSchedule.SelectedValue.ToString + "," _
                      & " @ACT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                      & " @ACT_bAWARD='" + chkAward.Checked.ToString + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub


    'ts

#End Region

    Protected Sub gvRpt_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRpt.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblAcmId As Label
            Dim chkSelect As CheckBox
            Dim chkAward As CheckBox

            lblAcmId = e.Row.FindControl("lblAcmId")
            chkSelect = e.Row.FindControl("chkSelect")
            chkAward = e.Row.FindControl("chkAward")


            BindActivityDetails(lblAcmId.Text, chkSelect, chkAward)
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        Gridbind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivitySchedule()
        Gridbind()
    End Sub

    Protected Sub ddlSchedule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchedule.SelectedIndexChanged
        Gridbind()
    End Sub
End Class
