﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_ccaPEStudentActivityDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC60005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindActivitySchedule()
                    BindGrade()
                    BindSection()
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                    tr1.Visible = False
                    tr2.Visible = False
                    tr3.Visible = False
                    tr4.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub

    Sub BindGrade()
         Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then

            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                          & " ,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,GRD_DISPLAYORDER,STM_ID FROM " _
                                          & " GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                          & " INNER JOIN STREAM_M ON GRM_STM_ID=STM_ID" _
                                          & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                          & " ORDER BY GRD_DISPLAYORDER"
        Else
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN GRM_DISPLAY ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                         & " ,GRM_GRD_ID+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,GRD_DISPLAYORDER,STM_ID FROM " _
                                         & " GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                                         & " INNER JOIN STREAM_M ON GRM_STM_ID=STM_ID" _
                                         & " INNER JOIN OASIS_CCA.PE.AUTHORIZED_STAFF ON GRD_ID=AS_GRD_ID" _
                                         & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                         & " AND AS_EMP_ID=" + Session("EmployeeId") + " AND ISNULL(AS_bDELETE,'FALSE')='FALSE'" _
                                         & " ORDER BY GRD_DISPLAYORDER"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

    End Sub

    Sub BindSection()
        Dim grade() As String = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT SCT_DESCR,SCT_ID FROM SECTION_M INNER JOIN " _
                                  & " GRADE_BSU_M ON SCT_GRM_ID=GRM_ID WHERE " _
                                  & " GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) + " AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT SCT_DESCR,SCT_ID FROM SECTION_M INNER JOIN " _
                         & " GRADE_BSU_M ON SCT_GRM_ID=GRM_ID " _
                         & " INNER JOIN OASIS_CCA.PE.AUTHORIZED_STAFF ON SCT_ID=AS_SCT_ID WHERE " _
                         & " GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID=" + grade(1) + " AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                         & " AND ISNULL(AS_bDELETE,'FALSE')='FALSE'" _
                         & " ORDER BY SCT_DESCR"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub


    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_PASPRTNAME,(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))) AS STU_NAME FROM " _
                                & " OASIS..STUDENT_M WHERE STU_SCT_ID='" + ddlSection.SelectedValue.ToString + "'" _
                                & " AND STU_CURRSTATUS='EN'" _
                                & " ORDER BY STU_PASPRTNAME,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()

        If gvStud.Rows.Count > 0 Then
            BindHeaders()
            BindData()
        End If
    End Sub

    Sub BindHeaders()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,CASE ACM_DESCR WHEN 'BMI' THEN 'HEIGHT(CM)' ELSE ACM_DISPLAY END AS ACM_DESCR FROM PE.ACTIVITY_M AS A " _
                                & " INNER JOIN PE.ACTIVITY_D AS B ON A.ACM_ID=B.ACT_ACM_ID " _
                                & " WHERE ACT_ACS_ID='" + ddlSchedule.SelectedValue.ToString + "'"

        str_query += " UNION ALL "

        str_query += " SELECT ACM_ID,'WEIGHT(KG)' AS ACM_DESCR FROM PE.ACTIVITY_M AS A " _
                                & " INNER JOIN PE.ACTIVITY_D AS B ON A.ACM_ID=B.ACT_ACM_ID " _
                                & " WHERE ACT_ACS_ID='" + ddlSchedule.SelectedValue.ToString + "'" _
                                & " AND ACM_DESCR='BMI'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hfTotalActivity.Value = ds.Tables(0).Rows.Count


        Dim i As Integer
        Dim lblActivityH As Label
        Dim lblActivityId As Label



        For i = 0 To ds.Tables(0).Rows.Count - 1
            lblActivityH = gvStud.HeaderRow.FindControl("lblActivity" + (i + 1).ToString + "H")
            lblActivityId = gvStud.HeaderRow.FindControl("lblActivity" + (i + 1).ToString + "Id")
            lblActivityH.Text = ds.Tables(0).Rows(i).Item(1)
            lblActivityId.Text = ds.Tables(0).Rows(i).Item(0)
        Next
    End Sub

    Sub BindData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT STD_STU_ID,CASE ACM_DESCR WHEN 'BMI' THEN 'HEIGHT(CM)' ELSE ACM_DISPLAY END ACM_DISPLAY,ACM_ID,ISNULL(STD_VAL1,0) STD_VAL1,ISNULL(STD_VAL2,0) STD_VAL2 FROM" _
                                & " PE.STUDENT_ACTIVITY_DETAILS AS A INNER JOIN PE.ACTIVITY_M AS B ON A.STD_ACM_ID=B.ACM_ID" _
           & " INNER JOIN OASIS..STUDENT_M ON STD_STU_ID=STU_ID WHERE STU_SCT_ID=" + ddlSection.SelectedValue.ToString _
          & " AND STD_ACS_ID='" + ddlSchedule.SelectedValue.ToString + "'"


        'Dim str_query As String = "SELECT STD_STU_ID,CASE ACM_DESCR WHEN 'BMI' THEN 'HEIGHT(CM)' ELSE ACM_DISPLAY END,ACM_ID,ISNULL(STD_VAL1,0) STD_VAL1,ISNULL(STD_VAL2,0) STD_VAL2 FROM" _
        '                      & " PE.STUDENT_ACTIVITY_DETAILS AS A INNER JOIN PE.ACTIVITY_M AS B ON A.STD_ACM_ID=B.ACM_ID" _
        '                      & " AND STD_ACS_ID='" + ddlSchedule.SelectedValue.ToString + "'"

        str_query += " UNION ALL "

        str_query += "SELECT STD_STU_ID,'WEIGHT(KG)',ACM_ID,ISNULL(STD_VAL1,0) STD_VAL1,ISNULL(STD_VAL2,0) STD_VAL2 FROM" _
                            & " PE.STUDENT_ACTIVITY_DETAILS AS A INNER JOIN PE.ACTIVITY_M AS B ON A.STD_ACM_ID=B.ACM_ID" _
                            & " AND STD_ACS_ID='" + ddlSchedule.SelectedValue.ToString + "' AND ACM_DESCR='BMI'"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        Dim actData As New Hashtable
        Dim strKey As String
        Dim strValue As String


        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                strKey = .Item(0).ToString + "_" + .Item(1) + "_" + .Item(2).ToString
                strValue = .Item(3).ToString + "_" + .Item(4).ToString
                actData.Add(strKey, strValue)
            End With
        Next


        Dim j As Integer
        Dim lblActivityH As Label
        Dim lblActivityId As Label
        Dim txtActivityId As TextBox
        Dim lblStuId As Label
        Dim strValues As String()

        For i = 0 To gvStud.Rows.Count - 1
            With gvStud.Rows(i)
                lblStuId = .FindControl("lblStuId")

                For j = 0 To 14
                    lblActivityH = gvStud.HeaderRow.FindControl("lblActivity" + (j + 1).ToString + "H")
                    lblActivityId = gvStud.HeaderRow.FindControl("lblActivity" + (j + 1).ToString + "Id")
                    txtActivityId = .FindControl("txtActivity" + (j + 1).ToString + "Id")
                    txtActivityId.Attributes.Add("onkeypress", "return  Numeric_Only()")
                    If lblActivityH.Text <> "" Then
                        strKey = lblStuId.Text + "_" + lblActivityH.Text + "_" + lblActivityId.Text
                        If actData.ContainsKey(strKey) Then
                            strValues = actData.Item(strKey).ToString.Split("_")
                            If lblActivityH.Text <> "WEIGHT(KG)" Then
                                txtActivityId.Text = IIf(strValues(0) = "0.00", "", strValues(0))
                            Else
                                txtActivityId.Text = IIf(strValues(1) = "0.00", "", strValues(1))
                            End If


                        End If
                        gvStud.Columns(j + 3).Visible = True
                    Else
                        gvStud.Columns(j + 3).Visible = False
                    End If

                Next
            End With
        Next

    End Sub


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String

        Dim strXml As String = ""
        Dim strXml1 As String = ""
        Dim i As Integer
        Dim j As Integer
        Dim lblActivityH As Label
        Dim lblActivityId As Label
        Dim txtActivityId As TextBox
        Dim lblStuId As Label
        strXml = "<IDS>"
        For i = 0 To gvStud.Rows.Count - 1
            With gvStud.Rows(i)
                lblStuId = .FindControl("lblStuId")

                For j = 0 To 14
                    lblActivityH = gvStud.HeaderRow.FindControl("lblActivity" + (j + 1).ToString + "H")
                    lblActivityId = gvStud.HeaderRow.FindControl("lblActivity" + (j + 1).ToString + "Id")
                    txtActivityId = .FindControl("txtActivity" + (j + 1).ToString + "Id")
                    strXml1 = ""
                    If lblActivityH.Text <> "" Then
                        strXml1 += "<STD_STU_ID>" + lblStuId.Text + "</STD_STU_ID>"
                        strXml1 += "<STD_ACD_ID>" + ddlAcademicYear.SelectedValue.ToString + "</STD_ACD_ID>"
                        strXml1 += "<STD_ACS_ID>" + ddlSchedule.SelectedValue.ToString + "</STD_ACS_ID>"
                        strXml1 += "<STD_ACM_ID>" + lblActivityId.Text + "</STD_ACM_ID>"
                        If txtActivityId.Text <> "" Then
                            strXml1 += "<STD_VAL1>" + txtActivityId.Text + "</STD_VAL1>"
                            strXml1 += "<STD_VAL2>" + txtActivityId.Text + "</STD_VAL2>"
                        Else
                            strXml1 += "<STD_VAL1>0</STD_VAL1>"
                            strXml1 += "<STD_VAL2>0</STD_VAL2>"
                        End If
                        strXml1 += "<ACTIVITY>" + lblActivityH.Text + "</ACTIVITY>"

                             strXml += "<ID>" + strXml1 + "</ID>"


                    End If
                Next
            End With
        Next
        strXml += "</IDS>"

        str_query = "exec PE.saveSTUDENT_ACTIVITYIES_DETAIL " _
                    & " @STD_XML ='" + strXml + "'," _
                    & " @STD_USER ='" + Session("susr_name") + "'"

        str_query = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivitySchedule()
        BindGrade()
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GridBind()
        tr1.Visible = True
        tr2.Visible = True
        tr3.Visible = True
        tr4.Visible = True
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        lblError.Text = "Record Saved Successfully"
    End Sub
End Class
'ts