<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaPEAuthorizedStaff.aspx.vb" Inherits="ccaPEAuthorizedStaff" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);


            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../Images/VerArrow.png";
                }
                else {
                    img.src = "../Images/VerArrow.png";
                }
                img.alt = "Close to view other details";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../Images/HorArrow.png";
                }
                else {
                    img.src = "../Images/HorArrow.png";
                }
                img.alt = "Expand to show details";
            }



        }

        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }

        function client_OnTreeNodeChecked_grd() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Authorized Staff"> </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">

                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"   ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <table   width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" colspan="4" >
                                        <table   width="100%">
                                            <tr >
                                                <td width="45%" class="title-bg">Staff Name
                                                </td>
                                                <td width="10%"></td>
                                                <td width="45%" class="title-bg">Grade &amp;  Section
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  >
                                                    <div class="checkbox-list-full">

                                                    <asp:Panel ID="plUserRole" runat="server" >
                                                        <asp:CheckBoxList ID="chkStaff" RepeatDirection="Vertical"
                                                            runat="server">
                                                        </asp:CheckBoxList>
                                                    </asp:Panel>
                                                    </div>
                                                </td>
                                                <td></td>
                                                <td  >
                                                    <div class="checkbox-list-full">

                                                    <asp:Panel ID="plGradeSection" runat="server"  >
                                                        <asp:TreeView ID="tvGRD_SCT" runat="server" ShowCheckBoxes="All" Font-Bold="false"  
                                                            onclick="client_OnTreeNodeChecked_grd();" ExpandDepth="1"  EnableClientScript="False">
                                                            <DataBindings>
                                                                <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand"
                                                                    TextField="Text" ValueField="ValueField"></asp:TreeNodeBinding>
                                                            </DataBindings>
                                                        </asp:TreeView>
                                                    </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAddSave" runat="server" CssClass="button" TabIndex="1" Text="Add / Save"
                                             /><asp:Button ID="btnUpdateSave" runat="server" CssClass="button"
                                                Text="Update / Save"   /><asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"
                                                    />
                                        <div  >
                                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                                 ></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="gridheader_pop">
                                    <td class="title-bg" colspan="4">Authorized Staff</td>
                                </tr>

                                <tr>

                                    <td   colspan="4">
                                        
                                        <asp:Panel ID="plGRD" runat="server"  >

                                            <asp:GridView ID="gvUSR_SCT" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                                Width="100%" DataKeyNames="EMP_ID" OnRowDataBound="gvUSR_SCT_RowDataBound">
                                                <RowStyle CssClass="griditem"   />
                                                <EmptyDataRowStyle CssClass="matters" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Details">
                                                        <ItemTemplate>
                                                            &nbsp; <a href="javascript:expandcollapse('div<%# Eval("EMP_ID") %>', 'one');">
                                                                <img id='imgdiv<%# Eval("EMP_ID") %>' alt="Click to show/hide info" border="0" height="19px"
                                                                    width="32px" src="../Images/HorArrow.png" />
                                                            </a>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sl.No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblR1" runat="server" Text='<%# Bind("R1") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblENAME" runat="server" Text='<%# Bind("ENAME") %>'></asp:Label>
                                                            <asp:Label ID="lblEMP_ID" runat="server" Text='<%# bind("EMP_ID") %>'
                                                                Visible="False"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnEdit" runat="server" OnClick="lbtnEdit_Click">Edit</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lbtnDelete" runat="server" OnClick="lbtnDelete_Click">Delete</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <tr align="center" style="margin: 0px; padding: 0px; border-collapse: collapse; border-style: none;">
                                                                <td colspan="100%" align="center" style="margin: 0px; padding: 0px; border-collapse: collapse; border-style: none;">
                                                                    <div id="div<%# Eval("EMP_ID") %>" style="display: none; position: relative; left: 5px; margin: 0px; overflow: auto; width: 100%">
                                                                        <table width="100%" style="margin: 0px; padding: 0px; border-collapse: collapse; border-style: none;">
                                                                            <tr>
                                                                                <td>

                                                                                    <asp:GridView ID="gvgrd_detail" runat="server" AutoGenerateColumns="False"
                                                                                        EmptyDataText="No record available !!!" EnableModelValidation="True"
                                                                                        CssClass="gridstyle" Width="98%" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px">

                                                                                        <RowStyle CssClass="griditem" Height="19px" />
                                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Wrap="True" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblAS_ID_p" runat="server" Text='<%# Bind("AS_ID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Sr.No">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblr1_P" runat="server" Text='<%# Bind("RS1") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />

                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderText="Grade">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblGRM_DISPLAY" runat="server" Text='<%# BIND("GRM_DISPLAY") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderText="Section">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblSCT_DESCR" runat="server" Text='<%# BIND("SCT_DESCR") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <PagerStyle CssClass="PagerStyle" />
                                                                                        <SelectedRowStyle BackColor="Wheat" />
                                                                                        <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                                    </asp:GridView>

                                                                                </td>
                                                                            </tr>

                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="griditem_hilight" />
                                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

