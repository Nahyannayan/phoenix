﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ccaPEActivity_Copy.aspx.vb" Inherits="CoCurricularActivities_ccaPEActivity_Copy" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
</head>
<body>

    <form id="form1" runat="server">
     <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div>
        <table width="100%" >
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td class="title-bg" colspan="2">
                                
                                   COPY ACTIVITY SCORE 
                            </td>
                        </tr>
                        <tr>
                            <td><br /></td>
                        </tr>
                        <tr class="matters">
                            <td  class="title-bg">Copy From</td> 
                           
                                            <td class="title-bg">Copy To</td> 
                        </tr>
                        <tr class="matters">
                            <td >
                               <table  width="100%">
                               <tr>
                               <td   ><span class="field-label"> Academic Year</span></td>
                               <td><asp:DropDownList ID="ddlAcd_ID_FROM" runat="server" AutoPostBack="True">
                                   </asp:DropDownList>
                               </td>
                            </tr>
                               <tr>
                               <td><span class="field-label">Activity Schedule</span></td>
                               <td><asp:DropDownList ID="ddlSchedule_From" runat="server">
                                   </asp:DropDownList>
                               </td>
                            </tr>
                            <tr>
                               <td><span class="field-label">Gender</span></td>
                               <td><asp:DropDownList ID="ddlGender_From" runat="server" >
                                <asp:ListItem Value="M">Boys</asp:ListItem>
                                <asp:ListItem Value="G">Girls</asp:ListItem>
                            </asp:DropDownList>
                               </td>
                            </tr>
                               </table></td>
                           
                            <td >
                                <table  width="100%">
                               <tr>
                               <td><span class="field-label">Academic Year</span></td>
                               <td><asp:DropDownList ID="ddlAcd_ID_To" runat="server" AutoPostBack="True">
                                   </asp:DropDownList>
                               </td>
                            </tr>
                                <tr>
                               <td align="left" class="title-bg">Select Activity Schedule</td>
                               <td align="left"  class="title-bg">Select Gender Group</td> 
                                
                               
                            </tr>
                            
                            <tr><td>
                                <asp:CheckBoxList ID="chkACT" runat="server">
                                </asp:CheckBoxList>
                            </td><td>
                                    <asp:CheckBox ID="chkBoy" runat="server" Text="Boys" />
                                    <br />
                                    <asp:CheckBox ID="chkGirl" runat="server" Text="Girls" />
                            </td></tr>
                               </table>
                               </td>
                        </tr>
                  
            
            </table>
            
                    </td>
            </tr>  
            <tr>
                <td align="center">
                 <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            Font-Size="10px"></asp:Label></div>
                </td></tr>
            <tr>
                <td align="center">
                  
                                <asp:Button ID="btnCopy" runat="server" Text="Copy" 
                                CssClass="button" />
                 
                </td>
            </tr>                      
                    </table>
              
    </div>
    <asp:Button ID="btnShow" runat="server" Style="display: none;" Text="show" />
    <ajaxToolkit:ModalPopupExtender ID="mdlReopen" runat="server" TargetControlID="btnShow"
        PopupControlID="plReopen" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    </form>
</body>
</html>
