﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_ccaPEActivity_Schedule
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            'ts

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC50005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    'Dim li As New ListItem
                    'li.Text = "2011-2012"
                    'li.Value = 200047
                    'ddlAcademicYear.Items.Add(li)

                    GridBind()
                    gvRpt.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub

  

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn '---------- column 0
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ACS_ID"
        dt.Columns.Add(column)



        column = New DataColumn '---------- column 1
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ACS_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn '---------- column 2
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ACS_DISPLAYORDER"
        dt.Columns.Add(column)


        column = New DataColumn '---------- column 3
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ACS_AGECUTOFFDATE"
        dt.Columns.Add(column)



        column = New DataColumn '---------- column 4
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn '---------- column 5
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn '---------- column 6
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "ACS_AGECUTOFFDATE_DB"
        dt.Columns.Add(column)

        column = New DataColumn '---------- column 6
        column.DataType = System.Type.GetType("System.Boolean")
        column.ColumnName = "bEnabled"
        dt.Columns.Add(column)
        Return dt
    End Function
    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim bShowSlno As Boolean = False

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@ACS_ACD_ID", ddlAcademicYear.SelectedValue.ToString)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "PE.GETACTIVITY_SCHEDULE_GRIDBIND", PARAM)
        Dim i As Integer

        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item("ACS_ID") = .Item("ACS_ID")
                dr.Item("ACS_DESCR") = .Item("ACS_DESCR")
                dr.Item("ACS_DISPLAYORDER") = .Item("ACS_DISPLAYORDER")
                If Format(.Item("ACS_AGECUTOFFDATE"), "dd/MMM/yyyy").ToLower <> "01/jan/2100" Then
                    dr.Item("ACS_AGECUTOFFDATE") = Format(.Item("ACS_AGECUTOFFDATE"), "dd/MMM/yyyy")
                Else
                    dr.Item("ACS_AGECUTOFFDATE") = ""
                End If
                dr.Item("MODE") = .Item("MODE")
                dr.Item("index") = i.ToString
                If Format(.Item("ACS_AGECUTOFFDATE_DB"), "dd/MMM/yyyy").ToLower <> "01/jan/2100" Then
                    dr.Item("ACS_AGECUTOFFDATE_DB") = Format(.Item("ACS_AGECUTOFFDATE_DB"), "dd/MMM/yyyy")
                Else
                    dr.Item("ACS_AGECUTOFFDATE_DB") = ""
                End If

                dr.Item("bEnabled") = .Item("bEnabled")
                dt.Rows.Add(dr)
            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 50 rows
        For i = 0 To 10 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item("ACS_ID") = "0"
            dr.Item("ACS_DESCR") = ""
            dr.Item("ACS_DISPLAYORDER") = ""
            dr.Item("ACS_AGECUTOFFDATE") = ""
            dr.Item("MODE") = "add"
            dr.Item("index") = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item("ACS_AGECUTOFFDATE_DB") = ""
            dr.Item("bEnabled") = True
            dt.Rows.Add(dr)
        Next

        Session("dtSch") = dt
        gvRpt.DataSource = dt
        gvRpt.DataBind()

        gvRpt.Columns(2).Visible = bShowSlno

    End Sub


    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String

        Dim i As Integer
        Dim lblAcsId As Label
        Dim lblOrder As Label
        Dim txtOrder As TextBox
        Dim lblAcs As Label
        Dim lblDate As Label
        Dim txtDate As TextBox
        Dim txtAcs As TextBox
        Dim lblDelete As Label
  
        Dim mode As String = ""

        For i = 0 To gvRpt.Rows.Count - 1
            With gvRpt.Rows(i)
                lblAcsId = .FindControl("lblAcsId")
                txtAcs = .FindControl("txtAcs")
                txtOrder = .FindControl("txtOrder")
                lblOrder = .FindControl("lblOrder")
                txtDate = .FindControl("txtDate")
                lblDate = .FindControl("lblDate")
                 lblDelete = .FindControl("lblDelete")
                lblAcs = .FindControl("lblAcs")

                If lblDelete.Text = "1" Then
                    mode = "delete"
                ElseIf lblAcsId.Text = "0" And txtAcs.Text <> "" Then
                    mode = "add"
                ElseIf lblAcs.Text <> txtAcs.Text Or lblOrder.Text <> txtOrder.Text Or txtDate.Text <> lblDate.Text Then
                    mode = "edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then

                    Dim param(10) As SqlParameter

                    param(0) = New SqlParameter("@ACS_ID", lblAcsId.Text.Trim)
                    param(1) = New SqlParameter("@ACS_DESCR", txtAcs.Text.Trim)
                    param(2) = New SqlParameter("@ACS_BSU_ID", Session("Sbsuid"))
                    param(3) = New SqlParameter("@ACS_ACD_ID", ddlAcademicYear.SelectedValue.ToString)
                    param(4) = New SqlParameter("@ACS_DISPLAYORDER", IIf(txtOrder.Text = "", "0", txtOrder.Text))
                    param(5) = New SqlParameter("@ACS_AGECUTOFFDATE", IIf(txtDate.Text = "", System.DBNull.Value, txtDate.Text))
                    param(6) = New SqlParameter("@MODE", mode)

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "PE.SAVEACTIVITYSCHEDULE", param)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    'str_query = "exec PE.SAVEACTIVITYSCHEDULE" _
    '         & " @ACS_ID =" + lblAcsId.Text + "," _
    '         & " @ACS_DESCR ='" + txtAcs.Text + "'," _
    '         & " @ACS_BSU_ID='" + Session("Sbsuid") + "'," _
    '         & " @ACS_ACD_ID =" + ddlAcademicYear.SelectedValue.ToString + "," _
    '         & " @ACS_DISPLAYORDER =" + IIf(txtOrder.Text = "", "0", txtOrder.Text) + "," _
    '         & " @ACS_AGECUTOFFDATE =" + IIf(txtDate.Text = "", "NULL", "'" + txtDate.Text + "'") + "," _
    '         & " @MODE ='" + mode + "'"

#End Region

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvRpt.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
        GridBind()
    End Sub

    
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub
End Class
