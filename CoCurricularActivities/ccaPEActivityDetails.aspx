﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaPEActivityDetails.aspx.vb" Inherits="CoCurricularActivities_ccaPEActivityDetails" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
           
        function Popup(url) {
            $.fancybox({
                'width': '80%',
                'height': '60%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
        function PopupMaster() {
            //var sFeatures;
            //sFeatures = "dialogWidth: 800px; ";
            //sFeatures += "dialogHeight:300px; ";
            //sFeatures += "help: no; ";
            //sFeatures += "resizable: no; ";
            //sFeatures += "scroll: no; ";
            //sFeatures += "status: no; ";
            //sFeatures += "unadorned: no; ";
            //var NameandCode;
            //var result;


            //result = window.showModalDialog("ccaPEActivity_Copy.aspx?", "", sFeatures)
            //if (result == '' || result == undefined) {
            //    return false;
            //}

            Popup("ccaPEActivity_Copy.aspx?")
        }

    </script>

    <style>
        table td input[type=text] {
            width: 49% !important;
            min-width: 49% !important;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Activity Score Settings"> </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ForeColor=""
                                    ValidationGroup="GRP"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Activity Schedule</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSchedule" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"><span class="field-label">Gender</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGender" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="M">Boys</asp:ListItem>
                                            <asp:ListItem Value="F">Girls</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left"><span class="field-label">Copy Option</span></td>
                                    <td align="left">
                                        <asp:LinkButton ID="btnClickCopy" runat="server" OnClientClick="Javascript:PopupMaster();">Click to Copy</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="4">
                                        <asp:GridView ID="gvRpt" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAcmId" runat="server" Text='<%# Bind("ACM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Activity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblActivity" runat="server" Text='<%# Bind("ACM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Age(6)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_6" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_6" runat="server" TargetControlID="txtVal1_6"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_6" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_6" runat="server" TargetControlID="txtVal2_6"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Age(7)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>

                                                        <asp:TextBox ID="txtVal1_7" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_7" runat="server" TargetControlID="txtVal1_7"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_7" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_7" runat="server" TargetControlID="txtVal2_7"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Age(8)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_8" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_8" runat="server" TargetControlID="txtVal1_8"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_8" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_8" runat="server" TargetControlID="txtVal2_8"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Age(9)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_9" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_9" runat="server" TargetControlID="txtVal1_9"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_9" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_9" runat="server" TargetControlID="txtVal2_9"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Age(10)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_10" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_10" runat="server" TargetControlID="txtVal2_10"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_10" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_10" runat="server" TargetControlID="txtVal2_10"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Age(11)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_11" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_11" runat="server" TargetControlID="txtVal1_11"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_11" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_11" runat="server" TargetControlID="txtVal2_11"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age(12)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_12" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_12" runat="server" TargetControlID="txtVal1_12"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_12" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_12" runat="server" TargetControlID="txtVal2_12"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age(13)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_13" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_13" runat="server" TargetControlID="txtVal1_13"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_13" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_13" runat="server" TargetControlID="txtVal2_13"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age(14)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_14" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_14" runat="server" TargetControlID="txtVal1_14"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_14" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_14" runat="server" TargetControlID="txtVal2_14"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age(15)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_15" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_15" runat="server" TargetControlID="txtVal1_15"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_15" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_15" runat="server" TargetControlID="txtVal2_15"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Age(16)">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_16" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_16" runat="server" TargetControlID="txtVal1_16"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_16" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv2_16" runat="server" TargetControlID="txtVal2_16"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age(17)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVal1_17" runat="server" CssClass="txtBlock1"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="atktxtv1_17" runat="server" TargetControlID="txtVal1_17"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtVal2_17" runat="server" Visible='<%# Bind("bTEXTSHOW") %>' CssClass="txtBlock2"></asp:TextBox>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtVal2_17"
                                                            FilterType="Custom,Numbers" ValidChars=".">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>

