﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI

Partial Class CoCurricularActivities_ccaAttenSchedule_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            'ts

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    'h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    'set_Menu_Img()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindAce()
                    gridbind()
                    'gvclmDescr.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\CoCurricularActivities\ccaAttenSchedule_D.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub BindAce()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindAce()
        gridbind()
    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet




            str_Sql = "select ATL_ID,SGR_DESCR,( replace(convert(varchar(11),ATL_DTEFRM,113),' ','/')  + ' -- ' + replace(convert(varchar(11),ATL_DTETO,113),' ','/')) AS DTE," _
                      & "ATL_USER,ATL_CRDATE FROM " _
                       & "ATTENDANCE_log INNER JOIN ACE_GROUP_M ON ATL_GRP_ID=SGR_ID " _
                         & " WHERE ATL_ACD_ID = " + ddlAcademicYear.SelectedValue + " And ATL_ACM_ID = " + ddlActivity.SelectedValue _
                     & " ORDER BY ATL_GRP_ID,ATL_DTEFRM"



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdType.DataSource = ds.Tables(0)
                grdType.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdType.DataSource = ds.Tables(0)
                Try
                    grdType.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdType.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdType.Rows(0).Cells.Clear()
                grdType.Rows(0).Cells.Add(New TableCell)
                grdType.Rows(0).Cells(0).ColumnSpan = columnCount
                grdType.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdType.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActivity.SelectedIndexChanged
        gridbind()
    End Sub
    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()> _
Public Shared Function GetDynamicContent(ByVal contextKey As String) As String
        Dim sTemp As New StringBuilder()

        Dim vGSD_GSM_SLAB_ID As String = contextKey
        Dim drReader As SqlDataReader
        Try
            Dim str_Sql As String = "select replace(convert(varchar(11),AT_DATE,113),' ','/') AT_DATE ,AT_DAYNAME, convert(char(5), AT_TFRM, 108) AT_TFRM, convert(char(5), AT_TTO, 108) AT_TTO  from ATTENDANCE_M where " & _
            " AT_ATL_ID = " & vGSD_GSM_SLAB_ID & _
            " order by AT_DATE "

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim objConn As New SqlConnection(str_conn)
            drReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            If Not drReader.HasRows Then
                Return ""
            End If
            sTemp.Append("<table class='popdetails'>")
            sTemp.Append("<tr>")
            sTemp.Append("<td colspan=4><b>Details</b></td>")
            sTemp.Append("</tr>")
            sTemp.Append("<tr>")
            sTemp.Append("<td><b>Date</b></td>")
            sTemp.Append("<td><b>Day Name</b></td>")
            sTemp.Append("<td><b>Time From</b></td>")
            sTemp.Append("<td><b>Time To</b></td>")
            sTemp.Append("</tr>")
            While (drReader.Read())
                sTemp.Append("<tr>")
                sTemp.Append("<td>" & drReader("AT_DATE").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AT_DAYNAME").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AT_TFRM").ToString & "</td>")
                sTemp.Append("<td>" & drReader("AT_TTO").ToString & "</td>")
                sTemp.Append("</tr>")
            End While
            
        Catch

        Finally
            drReader.Close()
            sTemp.Append("</table>")
        End Try
        Return sTemp.ToString()
    End Function
End Class
