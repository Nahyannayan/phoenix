<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAceMater_View.aspx.vb" Inherits="CoCurricularActivities_ccaAceMater_View"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ACE MASTER"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                        </td>
                        <td>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td  colspan="4">
                            <asp:GridView ID="gvclmDescr" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
                                CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                  PageSize="20" >
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="acm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcmId" runat="server" Text='<%# Bind("ACM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="acm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMainGroup" runat="server" Text='<%# Bind("ACM_MAINGROUP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="acm_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSbmId" runat="server" Text='<%# Bind("ACM_SBM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <HeaderTemplate>
                                                        <asp:Label ID="lblSub" runat="server" CssClass="gridheader_text" Text="Description"></asp:Label><br />
                                                                    <asp:TextBox ID="txtDescr" runat="server"  ></asp:TextBox>
                                                                        <asp:ImageButton ID="btnDescr_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnDescr_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescr" runat="server" Text='<%# Bind("ACM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Short Code">
                                        <HeaderTemplate>
                                                        <asp:Label ID="lblsh" runat="server" CssClass="gridheader_text" Text="Short Code"></asp:Label><br />
                                                                    <asp:TextBox ID="txtShort" runat="server"  ></asp:TextBox>
                                                                        <asp:ImageButton ID="btnShort_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnShort_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblShortCode" runat="server" Text='<%# Bind("ACM_SHORTCODE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Short Code">
                                        <HeaderTemplate>
                                                        <asp:Label ID="lbltp" runat="server" CssClass="gridheader_text" Text="Type"></asp:Label><br />
                                                                    <asp:TextBox ID="txtType" runat="server"  ></asp:TextBox>
                                                                        <asp:ImageButton ID="btnType_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnType_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("ACM_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>


                                    <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle   CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</asp:Content>
