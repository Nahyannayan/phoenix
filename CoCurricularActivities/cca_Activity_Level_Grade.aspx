﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="cca_Activity_Level_Grade.aspx.vb" Inherits="CoCurricularActivities_cca_Activity_Level_Grade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript">       

      

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Ace Allocation"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table width="100%">
                    <tr id="trLabelError" runat="server">
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"  ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="groupM1" />
                            <asp:RequiredFieldValidator ID="rfMaxSeat" runat="server" ControlToValidate="txtMaxSeat"
                                Display="None" ErrorMessage="Please enter the Maximum Seat Capacity" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr id="trActError" runat="server">
                        <td class="matters" align="left" valign="bottom">
                            <asp:Label ID="lblActError" runat="server" CssClass="error"
                                EnableViewState="False" Text="*Please choose an Activity"
                                SkinID="error" Style="text-align: left"></asp:Label></td>
                    </tr>
                    <tr id="trDayError" runat="server">
                        <td class="matters" align="left" valign="bottom">
                            <asp:Label ID="lblDayError" runat="server" CssClass="error"
                                EnableViewState="False" Text="*Please choose the Day"
                                SkinID="error" Style="text-align: left"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;" valign="top">
                            <table id="tblCategory" runat="server" clientidmode="Static" width="100%">
                                <tr align="left" id="tr1" runat="server">
                                    <td class="matters" width="20%"><span class="field-label">Academic Year<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" width="20%"><span class="field-label">Level<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" width="30%">
                                        <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr align="left">
                                    <td class="matters"><span class="field-label">Type<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>

                                    </td>
                                    <td class="matters"><span class="field-label">Activities<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters">
                                        <asp:CheckBoxList ID="cbActivities" runat="server" RepeatColumns="3"
                                            RepeatDirection="Horizontal">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="matters"><span class="field-label">Days<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" colspan="3">
                                        <asp:CheckBoxList ID="cbDays" runat="server" RepeatDirection="Horizontal"
                                            RepeatColumns="7">
                                            <asp:ListItem Text="Sunday" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Monday" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Tuesday" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Wednesday" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Thursday" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Friday" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="Saturday" Value="6"></asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Max Seat Capacity<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:TextBox MaxLength="3" runat="server" ID="txtMaxSeat"></asp:TextBox>
                                    </td>

                                    <td class="matters"><span class="field-label">Is Wait List Allowed?</span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:CheckBox ID="chkWaitList" runat="server" />
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="4" class="matters">
                                        <asp:Button ID="btnAddGrd" Text="Save" runat="server" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="title-bg">
                            <asp:Label ID="lblAllocationDetails" runat="server" Text="Details Of Allocation"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trDelError" runat="server">
                        <td align="left" class="matters">
                            <asp:Label ID="lblDelError" runat="server" CssClass="error"
                                Text="The Activity cannot be deleted as Students have already chosen them."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvActivity" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvActivity_PageIndexChanging" 
                                CssClass="table table-row table-bordered" EmptyDataText="No activity has been allotted yet." PageSize="10">
                                <RowStyle CssClass="griditem" Wrap="False" />

                                <Columns>
                                    <asp:TemplateField HeaderText="Activity ID" Visible="False">
                                        <HeaderTemplate>
                                            Activity Level-Grade Id
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblALGID" runat="server" Text='<%# Bind("ALG_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACL ID" Visible="False">
                                        <HeaderTemplate>
                                            Activity Level Id
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAclID" runat="server" Text='<%# Bind("ALG_ACL_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACT ID" Visible="False">
                                        <HeaderTemplate>
                                            Activity Type Id
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActID" runat="server" Text='<%# Bind("ALG_ACT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU ID" Visible="False">
                                        <HeaderTemplate>
                                            BSU ID
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBsuID" runat="server" Text='<%# Bind("ALG_BSU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACD ID" Visible="False">
                                        <HeaderTemplate>
                                            Academic Year ID
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcdID" runat="server" Text='<%# Bind("ALG_ACD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activity id" Visible="false">
                                        <HeaderTemplate>
                                            Activity ID
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActivityID" runat="server" Text='<%# Bind("ACM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Activity">
                                        <HeaderTemplate>
                                            Activity
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActivity" runat="server" Text='<%# Bind("ACM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Days">
                                        <HeaderTemplate>
                                            Days
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbSunday" runat="server" Text="Sun" Checked='<%# Bind("ALG_DAYS_SUN") %>' />
                                            <asp:CheckBox ID="cbMonday" runat="server" Text="Mon" Checked='<%# Bind("ALG_DAYS_MON") %>' />
                                            <asp:CheckBox ID="cbTuesday" runat="server" Text="Tue" Checked='<%# Bind("ALG_DAYS_TUE") %>' />
                                            <asp:CheckBox ID="cbWednesday" runat="server" Text="Wed" Checked='<%# Bind("ALG_DAYS_WED") %>' />
                                            <asp:CheckBox ID="cbThursday" runat="server" Text="Thu" Checked='<%# Bind("ALG_DAYS_THU") %>' />
                                            <asp:CheckBox ID="cbFriday" runat="server" Text="Fri" Checked='<%# Bind("ALG_DAYS_FRI") %>' />
                                            <asp:CheckBox ID="cbSaturday" runat="server" Text="Sat" Checked='<%# Bind("ALG_DAYS_SAT") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seat Capacity">
                                        <HeaderTemplate>
                                            Max Seat Capacity
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCapacity" runat="server" Text='<%# Bind("ALG_MAX_CAPACITY") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is waitlist allowed">
                                        <HeaderTemplate>
                                            Wait List Allowed
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbWaitlist" runat="server" Checked='<%# Bind("ALG_BWAITLISTALLOWED") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnDelete" CommandName="Delete" runat="server" Text="Delete" />
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="lnkbtnDelete"
                                                ConfirmText="Schedule will be deleted permanently.Are you sure you want to continue?" runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center">
                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CssClass="button" ValidationGroup="saveGRD" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnId" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
