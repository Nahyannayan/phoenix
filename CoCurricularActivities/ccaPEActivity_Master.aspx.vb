Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class CoCurricularActivities_ccaPEActivity_Master
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        'ts
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC50003") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    btnUpdate.Visible = False
                    rbGE.Checked = True
                    gridbind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub


    Sub gridbind()
        Dim conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ACM_BSU_ID", Session("sBsuid"))


        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "PE.GETACTIVITY_M_GRIDBIND", param)


            If ds.Tables(0).Rows.Count > 0 Then

                gvInfo.DataSource = ds.Tables(0)
                gvInfo.DataBind()



            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not


                gvInfo.DataSource = ds.Tables(0)
                Try
                    gvInfo.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvInfo.Rows(0).Cells.Clear()
                gvInfo.Rows(0).Cells.Add(New TableCell)
                gvInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvInfo.Rows(0).Cells(0).Text = "No Details Added Yet."
            End If



    End Sub


    
    Sub reset_state()
        rbGE.Checked = True
        rbLE.Checked = False
        rbRange.Checked = False
        txtDescr.Text = ""
        txtDisplay.Text = ""
        txtOrder.Text = ""
        txtUnit.Text = ""
        hfACM_ID.Value = 0
        btnAdd.Visible = True
        btnUpdate.Visible = False
        gvInfo.Columns(4).Visible = True
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try

            reset_state()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim OPT_TYPE As String = String.Empty
        hfACM_ID.Value = TryCast(sender.FindControl("lblACM_ID"), Label).Text
        txtDescr.Text = TryCast(sender.FindControl("lblACM_DESCR"), Label).Text
        OPT_TYPE = TryCast(sender.FindControl("lblACM_TYPE"), Label).Text
        txtUnit.Text = TryCast(sender.FindControl("lblACM_UNIT"), Label).Text
        txtDisplay.Text = TryCast(sender.FindControl("lblACM_DISPLAY"), Label).Text
        txtOrder.Text = TryCast(sender.FindControl("lblACM_DISPLAYORDER"), Label).Text
        If OPT_TYPE = "GE" Then
            rbGE.Checked = True
        ElseIf OPT_TYPE = "LE" Then
            rbLE.Checked = True
        ElseIf OPT_TYPE = "RANGE" Then
            rbRange.Checked = True
        End If

        gvInfo.Columns(4).Visible = False
        btnUpdate.Visible = True
        btnAdd.Visible = False
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then



            str_err = calltransaction(errorMessage, True)
            If str_err = "0" Then


                lblError.Text = "Record Saved Successfully"
                gridbind()
                reset_state()
            Else
                lblError.Text = errorMessage
            End If
        End If

    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then



            str_err = calltransaction(errorMessage, False)
            If str_err = "0" Then


                lblError.Text = "Record Saved Successfully"
                gridbind()
                reset_state()
            Else
                lblError.Text = errorMessage
            End If
        End If


    End Sub
    Function calltransaction(ByRef errorMessage As String, ByVal bEdit As Boolean) As Integer
       

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer
                Dim ACM_TYPE As String = String.Empty

                If rbGE.Checked = True Then
                    ACM_TYPE = "GE"
                ElseIf rbLE.Checked = True Then
                    ACM_TYPE = "LE"
                ElseIf rbRange.Checked = True Then
                    ACM_TYPE = "RANGE"
                End If


                Dim PARAM(10) As SqlParameter
                PARAM(0) = New SqlParameter("@ACM_ID", hfACM_ID.Value)
                PARAM(1) = New SqlParameter("@ACM_BSU_ID", Session("sBsuid"))
                PARAM(2) = New SqlParameter("@ACM_DESCR", txtDescr.Text.Trim)
                PARAM(3) = New SqlParameter("@ACM_DISPLAY", txtDisplay.Text.Trim)
                PARAM(4) = New SqlParameter("@ACM_TYPE", ACM_TYPE)
                PARAM(5) = New SqlParameter("@ACM_UNIT", txtUnit.Text.Trim)
                PARAM(6) = New SqlParameter("@ACM_DISPLAYORDER", txtOrder.Text.Trim)
                PARAM(7) = New SqlParameter("@bEDIT", bEdit)
                PARAM(8) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(8).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "PE.SAVEACTIVITY_M", PARAM)

                status = PARAM(8).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                Else

                    calltransaction = "0"
                End If

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    calltransaction = "1"
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function
   
    

    Protected Sub gvInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvInfo.PageIndex = e.NewPageIndex
        Call gridbind()
    End Sub




End Class
