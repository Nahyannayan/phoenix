<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaReleaseGradeSection.aspx.vb" Inherits="CoCurricularActivities_ccaReleaseGradeSection" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script>

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvGrade.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkPublish") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            if (document.forms[0].elements[i].disabled == false) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
        }
    }
}


function change_chk_state1(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkRelease") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Ace Report Release"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr align="left">
                        <td  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                 SkinID="error" Style="text-align: center"  ></asp:Label></td>
                    </tr>




                    <tr align="left">
                        <td  >
                            <table width="100%">
                              <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Report Card</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Report Schedule</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlPrintedFor" runat="server"  >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Select Grade</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Release Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox runat="server" ID="txtRelease">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgRelease" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                        </td>
                                    <td colspan="2" align="center">
                            <asp:Button ID="btnApply" runat="server" CssClass="button" Text="Apply to All"  /></td>
                                </tr>
                                <tr>

                                    <td align="center" class="matters" colspan="4">

                                        <asp:GridView ID="gvGrade" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-row table-bordered" >
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>


                                                <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# BIND("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Publish" Visible="FALSE">
                                                    <HeaderTemplate>
                                                        Publish<br />
                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>

                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkPublish" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkPublish" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Release Online">
                                                    <HeaderTemplate>
                                                        Release<br />
                                                                    <asp:CheckBox ID="chkAll1" runat="server" onclick="javascript:change_chk_state1(this);"
                                                                        ToolTip="Click here to select/deselect all rows" /></td>
                                                    </HeaderTemplate>

                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkRelease" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRelease" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Release Date">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                                    <asp:TextBox ID="txtDate" runat="server" CausesValidation="true"  ></asp:TextBox>
                                                                    <asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                                                    <asp:Label ID="lblErr" runat="server" Text="*" ForeColor="RED" Visible="false"></asp:Label>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                            Format="dd/MMM/yyyy" PopupButtonID="imgDate" PopupPosition="BottomLeft" TargetControlID="txtDate">
                                                        </ajaxToolkit:CalendarExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View Students">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>

                                            </Columns>

                                            <HeaderStyle CssClass="gridheader_pop"  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"   OnClick="btnSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters"  
                            valign="middle">
                            <asp:HiddenField ID="hfHeaderCount" runat="server"></asp:HiddenField>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgRelease" PopupPosition="BottomLeft" TargetControlID="txtRelease">
                            </ajaxToolkit:CalendarExtender>
                            
                    <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                        runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                            type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

