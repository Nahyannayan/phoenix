﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaPEActivity_Settings.aspx.vb" Inherits="CoCurricularActivities_ccaPEActivity_Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Activity Settings"> </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>


                        </td>
                    </tr>

                    <tr>
                        <td>

                            <table width="100%">
                                <tr>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Academic Year     </span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Activity Schedule</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSchedule" AutoPostBack="true" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                <br />
                                </tr>
                                <tr>
                                    <td colspan="4" align="center"  >
                                        <asp:GridView ID="gvRpt" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAcmId" runat="server" Text='<%# Bind("ACM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Activity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("ACM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Consider For Award">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>

                                                        <asp:CheckBox ID="chkAward" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

