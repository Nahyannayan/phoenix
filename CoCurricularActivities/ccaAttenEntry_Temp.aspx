﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAttenEntry_Temp.aspx.vb" Inherits="CoCurricularActivities_ccaAttenEntry_Temp"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

       <%-- function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_ID = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_ID = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var SGR_ID = "";
            var ACM_ID = "";
            var TYPE = "GRADE";


            if (SCT_ID == 'ALL') {
                SCT_ID = '0';
            }
            if (GRD_ID == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            var s = document.getElementById('<%=h_STU_IDs.ClientID %>').value
            if (s != '') {
                s = s + "___"
            }
            result = window.showModalDialog("../CoCurricularActivities/ccaStudPopupForm.aspx?multiselect=true&ACM_ID=" + ACM_ID + "&SGR_ID=" + SGR_ID + "&ACD_ID=" + ACD_ID + "&TYPE=" + TYPE + "&GRD_ID=" + GRD_ID + "&SCT_ID=" + SCT_ID, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = s + result; //NameandCode[0];

            }
            else {
                return false;
            }
        }--%>
        function Validate(sender, args) {
            args.IsValid = false;
            if (args.Value != "") {
                args.IsValid = true;
            }
        }

        function TPick(txt) {

            $("#" + txt).timepicker(
        {
            showPeriod: true,
            onHourShow: OnHourShowCallback,
            onMinuteShow: OnMinuteShowCallback
        });

        }


        function OnHourShowCallback(hour) {
            if ((hour > 20) || (hour < 6)) {
                return false; // not valid
            }
            return true; // valid
        }
        function OnMinuteShowCallback(hour, minute) {
            if ((hour == 20) && (minute >= 30)) { return false; } // not valid
            if ((hour == 6) && (minute < 30)) { return false; }   // not valid
            return true;  // valid
        }



    </script>
    
 <script>
     function GetSTUDENTS() {
            var NameandCode;
            var GRD_ID = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_ID = document.getElementById('<%=ddlSection.ClientID %>').value;
                var ACD_ID = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
                var SGR_ID = "";
                var ACM_ID = "";
                var TYPE = "GRADE";
                if (SCT_ID == 'ALL') {
                    SCT_ID = '0';
                }
                if (GRD_ID == '') {
                    alert('Please select atleast one Grade')
                    return false;
                }
                var s = document.getElementById('<%=h_STU_IDs.ClientID %>').value
            if (s != '') {
                s = s + "___"
            }
            url = "../CoCurricularActivities/ccaStudPopupForm.aspx?multiselect=true&ACM_ID=" + ACM_ID + "&SGR_ID=" + SGR_ID + "&ACD_ID=" + ACD_ID + "&TYPE=" + TYPE + "&GRD_ID=" + GRD_ID + "&SCT_ID=" + SCT_ID;
            var oWnd = radopen(url, "pop_student");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                var s = document.getElementById('<%=h_STU_IDs.ClientID %>').value
                if (s != '') {
                    s = s + "___"
                }
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = s + NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[0];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>

 <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Attendance Entry Temp"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr >
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                        </td>
                        <td align="left" class="matters" width="20%"><span class="field-label">Date</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFrom" runat="server"  ></asp:TextBox><asp:ImageButton
                                ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Activity</span>
                        </td>
                        <td align="left"  >
                                        <asp:DropDownList ID="ddlAct" AutoPostBack="true" runat="server" />
                                        <asp:LinkButton ID="lnkNew" runat="server">New</asp:LinkButton>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Time Frm</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="rdFrm" runat="server">
                            </asp:TextBox>

                        </td>
                        <td align="left" class="matters"><span class="field-label">Time To</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="rdTo" runat="server">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Grade</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" AutoPostBack="true" runat="server" />
                        </td>
                        <td align="left" class="matters"><span class="field-label">Section</span>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSection" AutoPostBack="true" runat="server" />
                        </td>
                    </tr>


                    <tr>
                        <td align="left" valign="top"><span class="field-label">Student</span>
                        </td>
                        <td align="left" >
                            <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged" ></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS(); return false;"
                                OnClick="imgStudent_Click"></asp:ImageButton>
                            </td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="false" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                PageSize="20" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAtt" runat="server" CssClass="button" Text="Create Attendance" />
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td id="Td1" align="center" class="matters" runat="server" colspan="4">
                            <asp:GridView ID="grdStud" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                Width="100%">
                                <RowStyle CssClass="griditem"   />
                                <Columns>
                                    <asp:TemplateField HeaderText="CMTID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="AtId" runat="server" Text='<%# bind("ATD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CMTID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="stuId" runat="server" Text='<%# bind("ATD_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblno" runat="server"   Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server"   Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgrade" runat="server"   Text='<%# Bind("stu_grd_id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsec" runat="server"   Text='<%# Bind("sct_descr") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Present">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cb1" runat="server" Text='<%# Bind("ATD_PRESENT") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Absent Reason">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlReason" runat="server"   />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtdesc" runat="server" SkinID="MultiText" TextMode="MultiLine"   Text='<%# Bind("ATD_DESC") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                Text="Delete"></asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle CssClass="gridheader_pop"  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" Text="Save" class="button" />
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    PopupControlID="Panel1" TargetControlID="lnkNew" DynamicServicePath="" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server"   CssClass="panel-cover">
                    <table style="width: 100%" >
                        <tr class="title-bg">
                            <td align="left" colspan="2" valign="middle">
                                
                        Activity 
                            </td>
                        </tr>
                        <tr id="trcatGrade" runat="server">
                            <td id="Td2" align="left" class="matters"   runat="server"></td>
                        </tr>
                        <tr>
                            <td align="left" class="matters" >
                                <asp:Label ID="lblH" runat="server" Text="Activity Name"  CssClass="field-label"  ></asp:Label>
                                <asp:Label ID="lbl2" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Red"
                                      Text="*"  ></asp:Label>
                            </td>
                            <td align="left" class="matters" >
                                <asp:TextBox ID="txtCategory" runat="server"  SkinID="TextBoxNormal"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters" valign="bottom" align="center" colspan="2">
                                <asp:Button ID="btnSaveCategory" runat="server" CssClass="button" TabIndex="7" Text="Save"
                                    />
                                <asp:Button ID="btnClose" runat="server" CausesValidation="False" CssClass="button"
                                    TabIndex="8" Text="Close"   />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
