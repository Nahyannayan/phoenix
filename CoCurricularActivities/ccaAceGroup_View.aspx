<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaAceGroup_View.aspx.vb" Inherits="CoCurricularActivities_ccaAceGroup_View"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ACE Group Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">

                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                            <asp:LinkButton ID="lbAuto" runat="server" Font-Bold="True" Visible="False">Auto Allocate Groups</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" width="20%"><span class="field-label">Select Academic Year</span>
                        </td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"> </asp:DropDownList>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" >
                            <asp:GridView ID="gvGroup" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                             PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem"  Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="sgr_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSgrId" runat="server" Text='<%# Bind("SGR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="sgr_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcmId" runat="server" Text='<%# Bind("SGR_ACM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="sgr_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAclId" runat="server" Text='<%# Bind("SGR_ACL_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="sgr_id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTrmId" runat="server" Text='<%# Bind("SGR_TRM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Name">
                                        <HeaderTemplate>
                                                        <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Group Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtGroup" runat="server"  ></asp:TextBox>
                                                                        <asp:ImageButton ID="btnGroup_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnGroup_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ace">
                                        <HeaderTemplate>
                                                        <asp:Label ID="lblac" runat="server" CssClass="gridheader_text" Text="Ace"></asp:Label><br />
                                                                    <asp:TextBox ID="txtAce" runat="server"  ></asp:TextBox> 
                                                                        <asp:ImageButton ID="btnAce_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                            OnClick="btnAce_Search_Click" /> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAce" runat="server" Text='<%# Bind("ACM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Level" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Level<br />
                                                        <asp:DropDownList ID="ddlgvLevel" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvLevel_SelectedIndexChanged"  >
                                                        </asp:DropDownList> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLevel" runat="server" Text='<%# Bind("ACL_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Term" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Term<br />
                                            <asp:DropDownList ID="ddlgvTerm" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvTerm_SelectedIndexChanged"  >
                                                        </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTerm" runat="server" Text='<%# Bind("TRM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  ></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>
