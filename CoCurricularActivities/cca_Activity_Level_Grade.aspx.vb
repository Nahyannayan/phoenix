﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class CoCurricularActivities_cca_Activity_Level_Grade
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'ts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10021") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindLevel()
                    BindType()
                    BindActivities()
                    BindGrid()
                    trDayError.Visible = False
                    trActError.Visible = False
                    trLabelError.Visible = False
                    trDelError.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try
        End If
    End Sub

    Sub BindGrid()
        trLabelError.Visible = True
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        'Dim str_query As String = "SELECT ALG_ID,ALG_ACL_ID,ALG_ACT_ID,ALG_BSU_ID,ALG_ACD_ID,ALG_ACTIVITIES,ALG_DAYS_SUN ,ALG_DAYS_MON ,ALG_DAYS_TUE ,ALG_DAYS_WED ,ALG_DAYS_THU,ALG_MAX_CAPACITY,ALG_BWAITLISTALLOWED FROM ACE_LEVEL_GRADE " _
        '                        & " WHERE ALG_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "' AND ALG_ACT_ID='" + ddlType.SelectedItem.Value + "' AND ALG_ACL_ID='" + ddlLevel.SelectedItem.Value + "'" _
        '                        & " AND ALG_BSU_ID='" + Session("sBsuid") + "'"

        Dim str_query As String = "SELECT ALG_ID,ALG_ACL_ID,ALG_ACT_ID,ALG_BSU_ID,ALG_ACD_ID,ACM_DESCR,ACM_ID,ALG_DAYS_SUN ,ALG_DAYS_MON ," _
                                 & " ALG_DAYS_TUE, ALG_DAYS_WED, ALG_DAYS_THU, ALG_DAYS_FRI,ALG_DAYS_SAT,ALG_MAX_CAPACITY, ALG_BWAITLISTALLOWED " _
                                 & " FROM ACE_LEVEL_GRADE INNER JOIN ACEMASTER_M ON ACM_ID=ALG_ACTIVITIES " _
                                 & " WHERE ALG_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "' AND ALG_ACT_ID='" + ddlType.SelectedItem.Value + "' AND ALG_ACL_ID='" + ddlLevel.SelectedItem.Value + "' AND ALG_BSU_ID='" + Session("sBsuid") + "' ORDER BY ACM_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvActivity.DataSource = ds
        gvActivity.DataBind()

    End Sub

    Protected Sub gvActivity_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvActivity.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Sub BindLevel()
        trLabelError.Visible = True
        ddlLevel.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM OASIS_CCA..ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("sBsuid") + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()

    End Sub

    Sub BindType()
        trLabelError.Visible = True
        ddlType.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACT_ID,ACT_DESCR FROM OASIS_CCA..ACE_TYPE_M "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlType.DataSource = ds
        ddlType.DataTextField = "ACT_DESCR"
        ddlType.DataValueField = "ACT_ID"
        ddlType.DataBind()

    End Sub

    Sub BindActivities()
        trLabelError.Visible = True
        cbActivities.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM DBO.ACEMASTER_M WHERE ACM_TYPE='" + ddlType.SelectedItem.Text + "' AND ACM_BSU_ID='" + Session("sBsuid") + "' AND ACM_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "' ORDER BY ACM_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cbActivities.DataSource = ds
        cbActivities.DataTextField = "ACM_DESCR"
        cbActivities.DataValueField = "ACM_ID"
        cbActivities.DataBind()
        Dim i As Integer
        Dim j As Integer

        str_query = "SELECT ACM_DESCR FROM ACE_LEVEL_GRADE INNER JOIN ACEMASTER_M ON ACM_ID=ALG_ACTIVITIES WHERE ALG_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "' AND ALG_ACT_ID='" + ddlType.SelectedItem.Value + "' AND " _
                  & " ALG_ACL_ID='" + ddlLevel.SelectedItem.Value + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To cbActivities.Items.Count - 1
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If cbActivities.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                        cbActivities.Items(i).Enabled = False
                    End If
                Next
            Next
        End If

    End Sub


    Protected Sub btnAddGrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGrd.Click

        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim strActivities As String = ""
            Dim strDays As String = ""
            Dim i As Integer
            For i = 0 To cbActivities.Items.Count - 1
                If cbActivities.Items(i).Selected = True Then
                    If strActivities <> "" Then
                        strActivities += "|"
                    End If
                    strActivities += cbActivities.Items(i).Value
                End If
            Next

            For i = 0 To cbDays.Items.Count - 1
                If cbDays.Items(i).Selected = True Then
                    If strDays <> "" Then
                        strDays += "|"
                    End If
                    strDays += cbDays.Items(i).Text
                End If
            Next

            If strActivities = "" Then
                trActError.Visible = True
                If strDays = "" Then
                    trDayError.Visible = True
                Else
                    trDayError.Visible = False
                End If
                Exit Sub
            Else
                If strDays = "" Then
                    trDayError.Visible = True
                    Exit Sub
                Else
                    trDayError.Visible = False
                    trActError.Visible = False
                    Dim param(14) As SqlParameter
                    param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
                    param(1) = New SqlParameter("@ACL_ID", ddlLevel.SelectedItem.Value)
                    param(2) = New SqlParameter("@ACT_ID", ddlType.SelectedItem.Value)
                    param(3) = New SqlParameter("@ACTIVITIES", strActivities)
                    param(4) = New SqlParameter("@bSun", cbDays.Items(0).Selected)
                    param(5) = New SqlParameter("@bMon", cbDays.Items(1).Selected)
                    param(6) = New SqlParameter("@bTue", cbDays.Items(2).Selected)
                    param(7) = New SqlParameter("@bWed", cbDays.Items(3).Selected)
                    param(8) = New SqlParameter("@bThu", cbDays.Items(4).Selected)
                    param(9) = New SqlParameter("@bFri", cbDays.Items(5).Selected)
                    param(10) = New SqlParameter("@bSat", cbDays.Items(6).Selected)
                    param(11) = New SqlParameter("@MAX_CAPACITY", txtMaxSeat.Text)
                    param(12) = New SqlParameter("@WAITLISTALLOWED", chkWaitList.Checked)
                    param(13) = New SqlParameter("@BSU_ID", Session("sBsuid"))

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.SAVE_GRADELEVEL", param)

                    lblError.Text = "Record Saved Successfully"
                    'enableDisableControls(False)
                    BindGrid()
                    BindActivities()
                    cbActivities.ClearSelection()
                    cbDays.ClearSelection()
                    txtMaxSeat.Text = ""
                    chkWaitList.Checked = False
                End If
            End If

        Catch ex As Exception
            lblError.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivities()
        BindGrid()
        txtMaxSeat.Text = ""
        chkWaitList.Checked = False
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        BindActivities()
        BindGrid()
    End Sub

    Protected Sub gvActivity_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvActivity.RowDataBound
        Dim cbActivityList As CheckBoxList
        cbActivityList = e.Row.FindControl("cbDaysList") 'as CheckBoxList 

    End Sub


    Protected Sub gvActivity_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvActivity.RowDeleting
        Dim index As Integer = e.RowIndex
        Dim mySelectedRow As GridViewRow = gvActivity.Rows(index)
        Dim lblALGID As Label = mySelectedRow.FindControl("lblALGID")
        Dim lblAclID As Label = mySelectedRow.FindControl("lblAclID")
        Dim lblActID As Label = mySelectedRow.FindControl("lblActID")
        Dim lblBsuID As Label = mySelectedRow.FindControl("lblBsuID")
        Dim lblAcdID As Label = mySelectedRow.FindControl("lblAcdID")
        Dim lblActivityID As Label = mySelectedRow.FindControl("lblActivityID")

        Dim algID As Integer = Convert.ToInt32(lblALGID.Text)
        Dim aclID As Integer = Convert.ToInt32(lblAclID.Text)
        Dim actID As Integer = Convert.ToInt32(lblActID.Text)
        Dim bsuID As String = lblBsuID.Text.ToString
        Dim acdID As Integer = Convert.ToInt32(lblAcdID.Text)
        Dim activity As String = lblActivityID.Text.ToString

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(6) As SqlParameter

        param(0) = New SqlParameter("@ALG_ID", algID)
        param(1) = New SqlParameter("@ALG_ACL_ID", aclID)
        param(2) = New SqlParameter("@ALG_ACT_ID", actID)
        param(3) = New SqlParameter("@ALG_BSU_ID", bsuID)
        param(4) = New SqlParameter("@ALG_STU_NEXTACD_ID", acdID)
        param(5) = New SqlParameter("@ALG_ACTIVITIES", activity)
        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.DELETE_GRADELEVEL", param)
        Dim i As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "DBO.DELETE_GRADELEVEL", param)
        If i = 1 Then
            trDelError.Visible = False
            BindGrid()
            BindActivities()
        Else
            trDelError.Visible = True
            Exit Sub
        End If

    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindActivities()
        BindGrid()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        trLabelError.Visible = True
        Dim i As Integer
        Dim j As Integer

        Dim strXml As String = ""
        Try

            For i = 0 To gvActivity.Rows.Count - 1

                With gvActivity.Rows(i)
                    Dim cbSun As CheckBox
                    Dim cbMon As CheckBox
                    Dim cbTue As CheckBox
                    Dim cbWed As CheckBox
                    Dim cbThu As CheckBox
                    Dim cbFri As CheckBox
                    Dim cbSat As CheckBox

                    Dim lblActID As Label = gvActivity.Rows(i).Cells(0).FindControl("lblActID")
                    Dim lblALGID As Label = gvActivity.Rows(i).Cells(0).FindControl("lblALGID")
                    Dim lblActivity As Label = gvActivity.Rows(i).Cells(1).FindControl("lblActivity")
                    Dim txtCapacity As TextBox = gvActivity.Rows(i).Cells(3).FindControl("txtCapacity")
                    Dim chkWaitlist As CheckBox = gvActivity.Rows(i).Cells(4).FindControl("cbWaitlist")

                    cbSun = gvActivity.Rows(i).Cells(2).FindControl("cbSunday")
                    cbMon = gvActivity.Rows(i).Cells(2).FindControl("cbMonday")
                    cbTue = gvActivity.Rows(i).Cells(2).FindControl("cbTuesday")
                    cbWed = gvActivity.Rows(i).Cells(2).FindControl("cbWednesday")
                    cbThu = gvActivity.Rows(i).Cells(2).FindControl("cbThursday")
                    cbFri = gvActivity.Rows(i).Cells(2).FindControl("cbFriday")
                    cbSat = gvActivity.Rows(i).Cells(2).FindControl("cbSaturday")

                    Dim bsun As Integer, bmon As Integer, btue As Integer, bwed As Integer, bthu As Integer, bFri As Integer, bSat As Integer, cbWait As Integer
                    If cbSun.Checked Then
                        bsun = 1
                    Else
                        bsun = 0
                    End If
                    If cbMon.Checked Then
                        bmon = 1
                    Else
                        bmon = 0
                    End If
                    If cbTue.Checked Then
                        btue = 1
                    Else
                        btue = 0
                    End If
                    If cbWed.Checked Then
                        bwed = 1
                    Else
                        bwed = 0
                    End If
                    If cbThu.Checked Then
                        bthu = 1
                    Else
                        bthu = 0
                    End If
                    If cbFri.Checked Then
                        bFri = 1
                    Else
                        bFri = 0
                    End If
                    If cbSat.Checked Then
                        bSat = 1
                    Else
                        bSat = 0
                    End If
                    If chkWaitlist.Checked Then
                        cbWait = 1
                    Else
                        cbWait = 0
                    End If

                    strXml += "<ID><ALG_ID>" + lblALGID.Text + "</ALG_ID>"
                    'strXml += "<ALG_ACTIVITIES>" + lblActivity.Text + "</ALG_ACTIVITIES>"
                    strXml += "<ALG_DAYS_SUN>" + bsun.ToString + "</ALG_DAYS_SUN>"
                    strXml += "<ALG_DAYS_MON>" + bmon.ToString + "</ALG_DAYS_MON>"
                    strXml += "<ALG_DAYS_TUE>" + btue.ToString + "</ALG_DAYS_TUE>"
                    strXml += "<ALG_DAYS_WED>" + bwed.ToString + "</ALG_DAYS_WED>"
                    strXml += "<ALG_DAYS_THU>" + bthu.ToString + "</ALG_DAYS_THU>"
                    strXml += "<ALG_DAYS_FRI>" + bFri.ToString + "</ALG_DAYS_FRI>"
                    strXml += "<ALG_DAYS_SAT>" + bSat.ToString + "</ALG_DAYS_SAT>"
                    strXml += "<ALG_MAX_CAPACITY>" + txtCapacity.Text + "</ALG_MAX_CAPACITY>"
                    strXml += "<ALG_BWAITLISTALLOWED>" + cbWait.ToString + "</ALG_BWAITLISTALLOWED></ID>"


                End With
            Next


            strXml = "<IDS>" + strXml + "</IDS>"

            Dim datetime As String = FormatDateTime(Now, DateFormat.ShortDate).ToString + " " + Now.TimeOfDay.ToString

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@aceData", strXml)
            param(1) = New SqlParameter("@userid", Session("sUsr_name"))
            'param(2) = New SqlParameter("@time", Format(CDate(datetime), "dd:MM:yyyy"))
            param(2) = New SqlParameter("@time", "2/8/2015 3:12:53 PM")

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "DBO.UPDATE_ACE_LEVEL_GRADE", param)
            lblError.Text = "Records saved successfully"
        Catch ex As Exception
            lblError.Text = "Records could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cbActivities.ClearSelection()
        cbDays.ClearSelection()
        txtMaxSeat.Text = ""
        chkWaitList.Checked = False
    End Sub


End Class
