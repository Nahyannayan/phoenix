﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAbsentees.aspx.vb" Inherits="CoCurricularActivities_Reports_Aspx_rptAbsentees" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Absentees Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Activity Type</span>
                                    </td>
                                    <td align="left" width="30%" class="matters">
                                        <asp:RadioButtonList ID="rdType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdType_SelectedIndexChanged">
                                            <asp:ListItem Selected="True"> Normal</asp:ListItem>
                                            <asp:ListItem> Temp</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Activity </span>
                                    </td>
                                    <td align="left" id="td4" runat="server" width="30%">
                                        <asp:DropDownList ID="ddlActivity" AutoPostBack="true" runat="server"  />
                                    </td>
                                    <td align="left" class="matters" id="td1" runat="server" width="20%"><span class="field-label">Activity level</span>
                                    </td>
                                    <td align="left" id="td3" runat="server" width="30%">
                                        <asp:DropDownList ID="ddlLevel" AutoPostBack="true" runat="server"   />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Date From </span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom"
                                            runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtStDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Date To</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgEnd"
                                            runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgEnd" TargetControlID="txtEndDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="text-align: center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                                            Text="Download Report in Pdf" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="H_TYPE" runat="server"></asp:HiddenField>
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>
