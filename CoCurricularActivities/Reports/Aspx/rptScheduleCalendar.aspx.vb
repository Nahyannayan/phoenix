﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI
Partial Class CoCurricularActivities_Reports_Aspx_rptScheduleCalendar
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC30012") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    'h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    'set_Menu_Img()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindTerm()

                    BindAce()
                    BindAceLevel()
                    txtDate.Text = String.Format("{0:MMM/yyyy}", Date.Today)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                ' lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_aCD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " ORDER BY TRM_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        If (Not ddlTerm.Items Is Nothing) AndAlso (ddlTerm.Items.Count > 1) Then
            ddlTerm.Items.Add(New ListItem("ALL", "0"))
            ddlTerm.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Sub BindAce()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
        If (Not ddlActivity.Items Is Nothing) AndAlso (ddlActivity.Items.Count > 1) Then
            ddlActivity.Items.Add(New ListItem("--", "0"))
            ddlActivity.Items.FindByText("--").Selected = True
        End If
    End Sub
    Sub BindAceLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM ACE_LEVEL_M WHERE ACL_BSU_ID=" + Session("sBsuid")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()
        If (Not ddlLevel.Items Is Nothing) AndAlso (ddlLevel.Items.Count > 1) Then
            ddlLevel.Items.Add(New ListItem("ALL", "0"))
            ddlLevel.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindAce()
        BindTerm()
    End Sub

    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        showdets()
    End Sub
    Sub showdets()
        calHomework.Visible = True

        calHomework.VisibleDate = New Date(Year(txtDate.Text), Month(txtDate.Text), 1)
    End Sub
    Protected Sub calHomework_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calHomework.DayRender
        Dim SStyle As New Style()
        With SStyle
            .BackColor = System.Drawing.Color.FromArgb(31, 73, 125)
            .BorderColor = System.Drawing.Color.White
            .BorderWidth = New Unit(3)
            .ForeColor = System.Drawing.Color.White
            .Height = "70"
        End With

        If e.Day.IsOtherMonth Then
            e.Cell.Controls.Clear()
        End If
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_Sql As String = ""

        Dim dt As New Date(Year(txtDate.Text), Month(txtDate.Text), 1)


        Dim ds As New DataSet
        str_Sql = "select SGR_DESCR,AT_DATE,(convert(char(5), AT_TFRM, 108) +' - '+ convert(char(5), AT_TTO, 108)) as AT_TIME  from ATTENDANCE_m inner join ACE_GROUP_M on AT_GRP_ID=SGR_ID " _
                    & " where  AT_ACD_ID =" + ddlAcademicYear.SelectedValue + " and " _
                    & "  MONTH(AT_DATE)=" & dt.Month & " and YEAR(AT_DATE)=" & dt.Year


        If ddlActivity.SelectedValue <> 0 Then
            str_Sql += " and AT_ACM_ID='" + ddlActivity.SelectedValue + "' "
        End If
        If ddlTerm.SelectedValue <> 0 Then
            str_Sql += " and SGR_TRM_ID=" + ddlTerm.SelectedValue
        End If
        If ddlLevel.SelectedValue <> 0 Then
            str_Sql += " and SGR_ACL_ID =" + ddlLevel.SelectedValue
        End If

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim i As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If e.Day.Date = ds.Tables(0).Rows(i).Item("AT_DATE") Then
                    e.Cell.ApplyStyle(SStyle)
                    e.Cell.Style.Add("vertical-align", "top")
                    e.Cell.Style.Add("text-align", "left")

                    Dim aLabel As Label = New Label()
                    '  aLabel.Style.Add("text-align", "center")
                    aLabel.Text = "<br>&nbsp;&nbsp;&nbsp;" & ds.Tables(0).Rows(i).Item("SGR_DESCR")
                    aLabel.ToolTip = ds.Tables(0).Rows(i).Item("AT_TIME")
                    aLabel.Style.Add("cursor", "default")
                    e.Cell.Controls.Add(aLabel)

                End If
            Next
        End If


    End Sub
End Class
