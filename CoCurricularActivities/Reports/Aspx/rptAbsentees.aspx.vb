﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI
Partial Class CoCurricularActivities_Reports_Aspx_rptAbsentees
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC30013") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    'h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    'h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    'set_Menu_Img()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))


                    BindAce()
                    BindAceLevel()
                    H_TYPE.Value = "1"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                ' lblError.Text = "Request could not be processed"
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
  
    Sub BindAce()
        ddlActivity.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " order by ACM_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
        If (Not ddlActivity.Items Is Nothing) AndAlso (ddlActivity.Items.Count > 1) Then
            ddlActivity.Items.Add(New ListItem("ALL", "0"))
            ddlActivity.Items.FindByText("ALL").Selected = True
        End If
    End Sub

    Sub BindAceTemp()
        ddlActivity.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT AT_AC_ID ACM_ID,AT_NAME ACM_DESCR FROM ATTENDENCE_TEMP WHERE AT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " order by AT_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
        If (Not ddlActivity.Items Is Nothing) AndAlso (ddlActivity.Items.Count >= 1) Then
            ddlActivity.Items.Add(New ListItem("ALL", "0"))
            ddlActivity.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Sub BindAceLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM ACE_LEVEL_M WHERE ACL_BSU_ID=" + Session("sBsuid")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()
        If (Not ddlLevel.Items Is Nothing) AndAlso (ddlLevel.Items.Count > 1) Then
            ddlLevel.Items.Add(New ListItem("ALL", "0"))
            ddlLevel.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindAce()

    End Sub
   

    Protected Sub rdType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdType.SelectedIndexChanged
        If rdType.Items(1).Selected Then
            BindAceTemp()
            td1.Visible = False
            ' td2.Visible = False
            td3.Visible = False
            'td4.ColSpan = "10"
            H_TYPE.Value = "0"
        Else
            BindAce()
            td1.Visible = True
            '  td2.Visible = True
            td3.Visible = True
            'td4.ColSpan = "4"
            H_TYPE.Value = "1"
        End If
    End Sub
    Sub CallReport()


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ATD_ACD_ID", ddlAcademicYear.SelectedValue)
        param.Add("@ATD_ACM_ID", ddlActivity.SelectedValue)
        param.Add("@FROMDTE", txtStDate.Text)
        param.Add("@TODTE", txtEndDate.Text)
        param.Add("@ATD_BTEMP", H_TYPE.Value)
        param.Add("@ATD_LEVEL", IIf(ddlLevel.SelectedValue = "", 0, ddlLevel.SelectedValue))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_cca"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptAbsentees.rpt")
        End With

        If hfbDownload.Value = 1 Then
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If
    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
