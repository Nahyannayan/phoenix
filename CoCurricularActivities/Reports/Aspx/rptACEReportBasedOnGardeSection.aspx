﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptACEReportBasedOnGardeSection.aspx.vb" Inherits="CoCurricularActivities_Reports_Aspx_rptACEReportBasedOnGardeSection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ACE Report - Grade & Section Wise"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr id="trLabelError" runat="server">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table class="BlueTableView" id="tblCategory" runat="server" clientidmode="Static" width="100%">
                                <tr align="left" id="tr1" runat="server">
                                    <td class="matters" align="left" width="20%"><span class="field-label">Academic Year<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>


                                    <td class="matters" width="20%"><span class="field-label">Level<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" width="30%">
                                        <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters"><span class="field-label">Current
                           Grade<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <br />
                                    </td>
                                    <td class="matters"><span class="field-label">Current
                           Section<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" colspan="4" align="center">
                                        <asp:Button ID="btnGetReport" Text="Generate Report" runat="server" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnPDFdownload" Text="Download PDF" runat="server" CssClass="button" ValidationGroup="groupM1" />
                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfGames" runat="server" />
                <asp:HiddenField ID="hfClubs" runat="server" />
                <asp:HiddenField ID="hfGRD_ID_NEXT" runat="server" />
                <asp:HiddenField ID="hfbDownload" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

