﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Math
Partial Class CoCurricularActivities_Reports_Aspx_rptDMHSPEActivityCertificateReport
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "CC70010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("dtCriteria") = SetDataTable()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindActivitySchedule()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub



    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@ACS_ID", ddlSchedule.SelectedValue.ToString)
        param.Add("@TYPE", ddlType.SelectedValue.ToString)
        param.Add("accYear", ddlAcademicYear.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_cca"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptDMHSPEActivityCertificateReport.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = "0" Then
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        End If
    End Sub
#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = "0"
        CallReport()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = "1"
        CallReport()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivitySchedule()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
