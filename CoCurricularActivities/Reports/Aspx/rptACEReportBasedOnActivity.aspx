﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptACEReportBasedOnActivity.aspx.vb" Inherits="CoCurricularActivities_Reports_Aspx_rptACEReportBasedOnActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="ACE Report - Activity wise"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table  width="100%">
                    <tr id="trLabelError" runat="server">
                        <td >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td  >
                            <table  id="tblCategory" runat="server" clientidmode="Static" width="100%" >
                                <tr align="left" id="tr1" runat="server">
                                    <td class="matters" align="left" width="20%"><span class="field-label">Academic Year<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters" align="left" width="20%"><span class="field-label">Level<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" align="left"  >
                                        <asp:RadioButtonList ID="rblActType" runat="server"
                                            RepeatDirection="Horizontal" Width="100%" AutoPostBack="True">
                                            <asp:ListItem Text="Game" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Club" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td></td>
                                    <td class="matters" align="left"><span class="field-label">Activity<font color="maroon">*</font></span>
                                    </td>
                                    <td class="matters" align="left">
                                        <asp:DropDownList ID="ddlActivity" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" colspan="4" align="center">
                                        <asp:Button ID="btnGetReport" Text="Generate Report" runat="server" CssClass="button" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnPDFdownload" Text="Download PDF" runat="server" CssClass="button" ValidationGroup="groupM1" />
                                        <br />
                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfbDownload" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

