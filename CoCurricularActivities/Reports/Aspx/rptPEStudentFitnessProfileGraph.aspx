<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPEStudentFitnessProfileGraph.aspx.vb" Inherits="rptPEStudentFitnessProfileGraph" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">

        function openWin() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }

            var oWnd = radopen("../../../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "RadWindow1");

        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg) {
                NameandCode = arg.NameCode.split('||');
                document.getElementById("<%=h_STU_IDs.ClientID %>").value = NameandCode[0];
             document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[0];
             __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
             // document.getElementById("<%=btnCheck.ClientID %>").click();
         }
     }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            result = window.showModalDialog("../../../Curriculum/clmPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result; //NameandCode[0];
        }
        else {
            return false;
        }
    }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>

        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Fitness Profile Graph"></asp:Label>
            <asp:Button ID="btnCheck" Style="display: none;" runat="server" />
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Activity Schedule</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlSchedule" runat="server" Width="264px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trGrade" runat="server">
                        <td align="left"><span class="field-label">Grade</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td><span class="field-label">Section</span></td>
                        <td>
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="field-label">Student</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="openWin();return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                        </td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />&nbsp;
                <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                    Text="Download Report in Pdf" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_TYPE" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

