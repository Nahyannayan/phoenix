﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptScheduleCalendar.aspx.vb" Inherits="CoCurricularActivities_Reports_Aspx_rptScheduleCalendar" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function onCalendarShown() {
            var cal = $find("calendar1");
            //Setting the default mode to month    
            cal._switchMode("months", true);
            //Iterate every month Item and attach click event to it    
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.addHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }
        function onCalendarHidden() {
            var cal = $find("calendar1");
            //Iterate every month Item and remove click event from it
            if (cal._monthsBody) {
                for (var i = 0; i < cal._monthsBody.rows.length; i++) {
                    var row = cal._monthsBody.rows[i];
                    for (var j = 0; j < row.cells.length; j++) {
                        Sys.UI.DomEvent.removeHandler(row.cells[j].firstChild, "click", call);
                    }
                }
            }
        }
        function call(eventElement) {
            var target = eventElement.target;
            switch (target.mode) {
                case "month":
                    var cal = $find("calendar1");
                    cal._visibleDate = target.date;
                    cal.set_selectedDate(target.date);
                    cal._switchMonth(target.date);
                    cal._blur.post(true);
                    cal.raiseDateSelectionChanged();
                    break;
            }
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Attendance Schedule Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"
                                SkinID="error" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" AutoPostBack="true" runat="server" />
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Activity</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlActivity" AutoPostBack="true" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Activity level</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLevel" AutoPostBack="true" runat="server" />
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Term</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlTerm" AutoPostBack="true" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Submission Month</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtDate_CalendarExtender" runat="server" OnClientHidden="onCalendarHidden"
                                            OnClientShown="onCalendarShown" BehaviorID="calendar1" Enabled="True" TargetControlID="txtDate" Format="MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" colspan="2" class="matters">
                                        <asp:Button ID="btnShow" CssClass="button" runat="server" Text="Show" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" class="matters">
                                        <asp:Calendar ID="calHomework" runat="server" Height="506px" Width="100%"
                                            ShowNextPrevMonth="False" Visible="False" ShowGridLines="True"></asp:Calendar>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

