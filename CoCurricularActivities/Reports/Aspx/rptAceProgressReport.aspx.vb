Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class rptAceProgressReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim rs As New ReportDocument
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC30010") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAca_Year, Session("clm"), Session("sBSUID"))
                    txtStudIDs.Attributes.Add("ReadOnly", "ReadOnly")
                    trGrade.Visible = True
                    trGroup.Visible = False
                    h_TYPE.Value = "GRADE"

                    BindReportType()
                    BindReportPrintedFor()

                    BindGrade()
                    BindSection()

                    BindAce()
                    BindGroup()

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub
    Sub BindReportPrintedFor()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_sql As String = "SELECT RPF_ID, RPF_DESCR FROM " & _
                   "RPT.ACE_REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & ddlReportType.SelectedValue.ToString
    
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddlReportPrintedFor.DataSource = ds
        ddlReportPrintedFor.DataTextField = "RPF_DESCR"
        ddlReportPrintedFor.DataValueField = "RPF_ID"
        ddlReportPrintedFor.DataBind()

    End Sub
    Sub BindAce()
          Dim ds As DataSet
        Dim str_con As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAca_Year.SelectedValue.ToString

        ds = SqlHelper.ExecuteDataset(str_con, CommandType.Text, str_query)
        ddlAce.DataSource = ds
        ddlAce.DataTextField = "ACM_DESCR"
        ddlAce.DataValueField = "ACM_ID"
        ddlAce.DataBind()
    End Sub
    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM ACE_GROUP_M WHERE SGR_ACM_ID=" + ddlAce.SelectedValue.ToString
        Else
            str_query = "SELECT SGR_ID,SGR_DESCR FROM ACE_GROUP_M " _
                   & " INNER JOIN ACE_GROUPS_TEACHER_S ON SGR_ID=SGS_SGR_ID" _
                   & " AND SGS_TODATE IS NULL" _
                   & " WHERE SGR_ACM_ID=" + ddlAce.SelectedValue.ToString _
                   & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRM_DISPLAY,GRD_DISPLAYORDER FROM VW_GRADE_BSU_M AS A" _
                                 & " INNER JOIN VW_GRADE_M AS B ON A.GRM_GRD_ID=B.GRD_ID " _
                                 & " INNER JOIN RPT.ACE_REPORTSETUP_GRADE_S AS C ON B.GRD_ID=C.RSG_GRD_ID" _
                                 & " WHERE RSG_RSM_ID=" + ddlReportType.SelectedValue.ToString _
                                 & " AND GRM_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                                 & " ORDER BY GRD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM VW_SECTION_M WHERE " _
                                 & " SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                 & " AND SCT_ACD_ID=" + ddlAca_Year.SelectedValue.ToString _
                                 & " ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub

    Function GetSelectedStudents(Optional ByVal vSTU_IDs As String = "")
        Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " & _
         " FROM  vw_STUDENT_DETAILS "
        If vSTU_IDs <> "" Then
            str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
        End If
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, str_sql)
    End Function


    Sub BindReportType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_sql As String = "SELECT RSM_ID,RSM_DESCR FROM RPT.ACE_REPORT_SETUP_M WHERE RSM_ACD_ID=" + ddlAca_Year.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlReportType.DataSource = ds
        ddlReportType.DataTextField = "RSM_DESCR"
        ddlReportType.DataValueField = "RSM_ID"
        ddlReportType.DataBind()
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub

   

 
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hfbDownload.Value = 0
        CallReports()
    End Sub

    Private Function GetAllCBSEBusinessUnit(ByVal BSU_ID As String) As String
        Dim strQuery As String = " SELECT BSU_ID FROM BUSINESSUNIT_M WHERE BSU_CLM_ID = 1 AND BSU_ID = '" & BSU_ID & "'"
        Dim vBSU_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        Return vBSU_ID
    End Function

    Private Sub GenerateDMHSAceReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_IDS", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "oasis_CCA"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptDMHSAceProgressReport.rpt")
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateTMSAceReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "logo")
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_IDS", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "oasis_CCA"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTMSAceProgressReport.rpt")
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Function GetAllStudentsInSection(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, ByVal vSCT_ID As String) As String
        Dim str_sql As String = String.Empty
        Dim str As String = ""
        'str_sql = "SELECT  STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M  " & _
        '" WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
        '"' AND STU_BSU_ID = '" & Session("sbsuid") & "'" & _
        '" AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,1,'') "
        If Session("Current_ACD_ID") = ddlAca_Year.SelectedValue.ToString And rdGroup.Checked = True Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " VW_STUDENT_M INNER JOIN ACE_STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
           "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
           " AND SSD_SGR_ID='" + ddlGroup.SelectedValue.ToString + "'" & _
           " for xml path('')),1,0,''),0) "
        ElseIf Session("Current_ACD_ID") = ddlAca_Year.SelectedValue.ToString And rdGrade.Checked = True Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " VW_STUDENT_M WHERE STU_ACD_ID ='" & vACD_ID & "' AND STU_GRD_ID = '" & vGRD_ID & _
           "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
          " AND STU_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,0,''),0) "

        ElseIf rdGroup.Checked = True Then
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
     " VW_STUDENT_M INNER JOIN ACE_STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
     " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
     "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
      " AND SSD_SGR_ID='" + ddlGroup.SelectedValue.ToString + "'" & _
     " for xml path('')),1,0,''),0) "
        Else
            str_sql = "SELECT  ISNULL(STUFF((SELECT distinct CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
    " VW_STUDENT_M  INNER JOIN STUDENT_PROMO_S ON STU_ID=STP_STU_ID " & _
    " WHERE STP_ACD_ID ='" & vACD_ID & "' AND STP_GRD_ID = '" & vGRD_ID & _
    "' AND STU_BSU_ID = '" & Session("sbsuid") & "' AND STU_CURRSTATUS <> 'CN' " & _
       " AND STP_SCT_ID in (" & vSCT_ID & ") for xml path('')),1,0,''),0) "
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, str_sql)
        While reader.Read
            str += reader.GetString(0)
        End While
        reader.Close()

        Return str
    End Function

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        BindReportPrintedFor()

        BindGrade()
        BindSection()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        BindReportType()
        BindReportPrintedFor()

        BindGrade()
        BindSection()

        BindAce()
        BindGroup()
    End Sub


    Sub LoadReports(ByVal rptClass)
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If

                If ViewState("MainMnu_code") = "PdfReport" Then
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.ContentType = "application/pdf"
                    rs.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                    rs.Close()
                    rs1.Dispose()
                Else
                    Try
                        exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                    Catch ee As Exception
                    End Try
                    rs.Close()
                    rs1.Dispose()
                End If

            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            rs.Close()
            rs1.Dispose()
        End Try
        'GC.Collect()
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("~/Curriculum/ReportDownloads/")
        Dim tempFileName As String = Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                Dim hop As New CrystalDecisions.Shared.ExportOptions
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        ' HttpContext.Current.Response.ContentType = "application/octect-stream"
        Dim bytes() As Byte = File.ReadAllBytes(tempFileNameUsed)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/pdf"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(bytes)

        Response.Flush()

        Response.End()
        'HttpContext.Current.Response.ContentType = "application/pdf"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        System.IO.File.Delete(tempFileNameUsed)
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        If Session("sbsuid") = "125017" Then
                            If Not Session("noimg") Is Nothing Then
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                            Else
                                vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/NOIMG/no_img_gwa.jpg")
                            End If

                        ElseIf Session("sbsuid") = "114003" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                        Else
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        End If
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function



    Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function


    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '/125017/'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ") AND SFU_RPF_ID=" + RPF_ID


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\125017\'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                    & " ,STU_ID" _
                                    & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON SFU_STU_ID=STU_ID AND SFU_RPF_ID=" + RPF_ID _
                                    & " LEFT OUTER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
                                    & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                    & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CCAConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function


    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub
    Private Function GetSelectedSection(ByVal sec_id As String) As String
        Dim str_sec_ids As String = String.Empty
        Dim comma As String = String.Empty
        Dim bSuperUsr As Boolean = False
        If Session("CurrSuperUser") = "Y" Then
            bSuperUsr = True
        End If

        If sec_id = "0" Then
            Dim ds As DataSet = ReportFunctions.GetSectionForGrade(Session("sBSUID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, Session("EmployeeID"), bSuperUsr)
            If (Not ds Is Nothing) AndAlso (Not ds.Tables(0) Is Nothing) AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    If str_sec_ids <> "" Then
                        str_sec_ids += ","
                    End If
                    str_sec_ids += dr("SCT_ID").ToString
                Next
            End If
            Return str_sec_ids
        Else
            Return sec_id
        End If
    End Function

    Sub CallReports()
        If rdGrade.Checked = True Then
            If h_STU_IDs.Value = "" Then
                Dim str_sec As String = GetSelectedSection(ddlSection.SelectedValue)
                h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, str_sec)
            End If
        Else
            If h_STU_IDs.Value = "" Then
                h_STU_IDs.Value = GetAllStudentsInSection(Session("sBSU_ID"), ddlAca_Year.SelectedValue, ddlGrade.SelectedValue, "")
            End If
        End If
        Select Case Session("sbsuid")
            Case "123004"
                GenerateDMHSAceReport()
            Case "123006"
                GenerateTMSAceReport()
        End Select

    End Sub

    Private Sub GenerateSummativeAssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Then
            param.Add("RPT_CAPTION", "TERM I REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("SA_PERC", "20%")
            param.Add("TOTAL_PERC", "40%")
            param.Add("TOTAL_FA_PERC", "20%")
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("RPT_CAPTION", "TERM II REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("SA_PERC", "40%")
            param.Add("TOTAL_PERC", "60%")
            param.Add("TOTAL_FA_PERC", "20%")
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If


        Dim vSA_HEADER As String = "SA"

        If ddlAce.SelectedValue = "10" Then
            If ddlReportPrintedFor.SelectedItem.Text.ToUpper <> "SUMMATIVE REPORT 1" Then
                param.Add("SA_HEADER", "Rehearsal Exam")
            Else
                param.Add("SA_HEADER", vSA_HEADER)
            End If
        Else
            param.Add("SA_HEADER", vSA_HEADER)
        End If


        param.Add("UserName", Session("sUsr_name"))


        param.Add("GRD_ID", ddlAce.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub




    Private Sub GenerateCBSETermReport01_04()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAca_Year.SelectedValue)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", ddlReportType.SelectedValue)
        param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
        param.Add("@STU_ID", h_STU_IDs.Value.Replace("___", "|"))
        param.Add("accYear", ddlAca_Year.SelectedItem.Text)
        param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)

        param.Add("UserName", Session("sUsr_name"))
        param.Add("SA_HEADER", "SA")
        param.Add("FA1_PERC", "10%")
        param.Add("FA2_PERC", "10%")
        param.Add("SA_PERC", "20%")
        param.Add("TOTAL_PERC", "60%")
        param.Add("TOTAL_FA_PERC", "20%")

        If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If

        param.Add("GRD_ID", ddlAce.SelectedValue.ToString)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("../Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04.rpt")
        End With
        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            If ViewState("MainMnu_code") = "StudentProfile" Then
                Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
            ElseIf ViewState("MainMnu_code") = "PdfReport" Then
                LoadReports(rptClass)
            Else
                ReportLoadSelection()
            End If
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReports()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            rs.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub rdGrade_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGrade.CheckedChanged
        If rdGrade.Checked = True Then
            trGrade.Visible = True
            trGroup.Visible = False
            h_TYPE.Value = "GRADE"
        End If
    End Sub

    Protected Sub rdGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGroup.CheckedChanged
        If rdGroup.Checked = True Then
            trGrade.Visible = False
            trGroup.Visible = True
            h_TYPE.Value = "GROUP"
        End If
    End Sub

    Protected Sub ddlAce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAce.SelectedIndexChanged
        BindGroup()
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        txtStudIDs.Text = ""
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
