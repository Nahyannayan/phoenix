﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDMHSPEActivityCertificateReport.aspx.vb" Inherits="CoCurricularActivities_Reports_Aspx_rptDMHSPEActivityCertificateReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Fitness Awards"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server"   width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label><span style="color: #c00000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True"  >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Activity Schedule</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSchedule" runat="server"  >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Type</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlType" runat="server"  
                                            AppendDataBoundItems="True">
                                            <asp:ListItem>GOLD</asp:ListItem>
                                            <asp:ListItem>SILVER</asp:ListItem>
                                            <asp:ListItem>BRONZE</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" />
                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                                            Text="Download Report in Pdf" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom">
                            <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>


                            <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                            </CR:CrystalReportSource>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

