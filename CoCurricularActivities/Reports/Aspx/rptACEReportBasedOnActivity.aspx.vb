﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class CoCurricularActivities_Reports_Aspx_rptACEReportBasedOnActivity
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC30014") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindAcademicYear()
                    BindLevel()
                    BindActivity()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnPDFdownload)

    End Sub

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACD_ID FROM OASIS..ACADEMICYEAR_D INNER JOIN OASIS..ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID" _
                                & " WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID='" + Session("clm") + "' AND ACD_ID IN (" + Session("Current_Acd_ID") + "," + Session("Next_ACD_ID") + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
    End Sub

    Sub BindLevel()

        ddlLevel.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM OASIS_CCA..ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("sBsuid") + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()


    End Sub

    Sub BindActivity()

        ddlActivity.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(1) = New SqlParameter("@ACL_ID", ddlLevel.SelectedValue)
        param(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(3) = New SqlParameter("@ACT_ID", rblActType.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACTIVITIES_BASEDON_LEVELANDTYPE]", param)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
        If ddlActivity.Items.Count > 0 Then
            ddlActivity.Items.Insert(0, New ListItem("All", "0"))
        Else
            ddlActivity.Items.Insert(0, New ListItem("--", "0"))
        End If


    End Sub

    Protected Sub btnGetReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetReport.Click
        CallReport()
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActivity.SelectedIndexChanged

    End Sub

    Sub CallReport()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0
        Dim bsu_ids As String
        Dim reportPath As String
        Dim bsuID As String = Session("sBsuid")

        Dim strActivity As String
        If ddlActivity.SelectedIndex = 0 Then
            For i = 1 To ddlActivity.Items.Count - 1
                If strActivity <> "" Then
                    strActivity += "|"
                End If
                strActivity += ddlActivity.Items(i).Value
            Next
        Else
            strActivity = ddlActivity.SelectedItem.Value
        End If

        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param.Add("@ACL_ID", ddlLevel.SelectedValue)
        param.Add("@ACT_ID", rblActType.SelectedValue)
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("@ACM_ID", strActivity)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("ACY_DESCR", ddlAcademicYear.SelectedItem.Text)
        param.Add("LVL_DESCR", ddlLevel.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_cca"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptACEReportBasedOnActivity.rpt")
        End With

        If hfbDownload.Value = "1" Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx", False)
            ReportLoadSelection()
        End If

    End Sub

    Protected Sub rblActType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblActType.SelectedIndexChanged
        BindActivity()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindActivity()
    End Sub

    Protected Sub btnPDFdownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPDFdownload.Click
        hfbDownload.Value = "1"
        CallReport()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivity()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
