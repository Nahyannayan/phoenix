<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAceProgressReport.aspx.vb" Inherits="rptAceProgressReport" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


 <%--   <script language="javascript" type="text/javascript">
        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

            var TYPE = document.getElementById('<%=h_TYPE.ClientID %>').value;
            if (TYPE == "GROUP") {
                var SGR_ID = document.getElementById('<%=ddlGroup.ClientID %>').value;
                var ACM_ID = document.getElementById('<%=ddlAce.ClientID %>').value;
                var GRD_ID = "";
                var SCT_ID = "";
            }
            else {
                var GRD_ID = document.getElementById('<%=ddlGrade.ClientID %>').value;
                var SCT_ID = document.getElementById('<%=ddlSection.ClientID %>').value;
                var SGR_ID = "";
                var ACM_ID = "";
            }

            var ACD_ID = document.getElementById('<%=ddlAca_Year.ClientID %>').value;


            result = window.showModalDialog("../../ccaStudPopupForm.aspx?multiselect=true&ACM_ID=" + ACM_ID + "&SGR_ID=" + SGR_ID + "&ACD_ID=" + ACD_ID + "&TYPE=" + TYPE + "&GRD_ID=" + GRD_ID + "&SCT_ID=" + SCT_ID, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }

    </script>--%>
    <script>
        function GetSTUDENTS() {
           var TYPE = document.getElementById('<%=h_TYPE.ClientID %>').value;
            if (TYPE == "GROUP") {
                var SGR_ID = document.getElementById('<%=ddlGroup.ClientID %>').value;
                var ACM_ID = document.getElementById('<%=ddlAce.ClientID %>').value;
                var GRD_ID = "";
                var SCT_ID = "";
            }
            else {
                var GRD_ID = document.getElementById('<%=ddlGrade.ClientID %>').value;
                var SCT_ID = document.getElementById('<%=ddlSection.ClientID %>').value;
                var SGR_ID = "";
                var ACM_ID = "";
            }

            var ACD_ID = document.getElementById('<%=ddlAca_Year.ClientID %>').value;

            url = "../../ccaStudPopupForm.aspx?multiselect=true&ACM_ID=" + ACM_ID + "&SGR_ID=" + SGR_ID + "&ACD_ID=" + ACD_ID + "&TYPE=" + TYPE + "&GRD_ID=" + GRD_ID + "&SCT_ID=" + SCT_ID;
            var oWnd = radopen(url, "pop_employee");
        }
        function OnClientClose1(oWnd, args) {
            var NameandCode;
            var arg = args.get_argument();
            if (arg) {

                NameandCode = arg.NameandCode.split('||');
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = NameandCode[0];
                document.getElementById("<%=txtStudIDs.ClientID()%>").value = NameandCode[0];
             __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');
        }
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>
      <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_employee" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Progress Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReportType_SelectedIndexChanged" Width="331px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="field-label">Report Printed For</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlReportPrintedFor" runat="server" Width="264px">
                            </asp:DropDownList></td>
                        <td><span class="field-label">Filter</span></td>
                        <td align="left">
                            <asp:RadioButton ID="rdGrade" runat="server" AutoPostBack="True" Checked="True" CssClass="field-label"
                                GroupName="g1" Text="By Grade &amp; Section" />
                            <asp:RadioButton ID="rdGroup" runat="server" AutoPostBack="True" GroupName="g1" CssClass="field-label"
                                Text="By Group" />
                        </td>
                    </tr>
                    <tr id="trGrade" runat="server">
                        <td align="left"><span class="field-label">Grade</span></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td><span class="field-label">Section</span></td>
                        <td>
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trGroup" runat="server">
                        <td align="left" valign="middle"><span class="field-label">Ace</span></td>
                        <td align="left" valign="middle">
                            <asp:DropDownList ID="ddlAce" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" valign="middle"><span class="field-label">Group</span></td>
                        <td align="left" colspan="1" valign="middle">
                            <asp:DropDownList ID="ddlGroup" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><span class="field-label">Student</span></td>
                        <td align="left">
                            <asp:TextBox ID="txtStudIDs" runat="server" Width="330px" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS(); return false" OnClick="imgStudent_Click"></asp:ImageButton>
                        </td>
                        <td colspan="2">
                            <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_new" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report" OnClick="btnGenerateReport_Click" />
                            <asp:Button ID="btnDownload" runat="server" CssClass="button" EnableViewState="False"
                                Text="Download Report in Pdf" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_GRD_IDs" runat="server" />
                <asp:HiddenField ID="h_STU_IDs" runat="server" />
                <asp:HiddenField ID="hfbFinalReport" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_TYPE" runat="server" />
                <CR:CrystalReportSource ID="rs1" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <asp:HiddenField ID="hfbDownload" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

