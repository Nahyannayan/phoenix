﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.IO


Partial Class CoCurricularActivities_Reports_Aspx_rptACEReportBasedOnGardeSection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC30015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindAcademicYear()
                    BindLevel()
                    BindGrade()
                    BindSection()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnPDFdownload)

    End Sub

    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACY_DESCR,ACD_ID FROM OASIS..ACADEMICYEAR_D INNER JOIN OASIS..ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID" _
                                & " WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID='" + Session("clm") + "' AND ACD_ID IN (" + Session("Current_Acd_ID") + "," + Session("Next_ACD_ID") + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
    End Sub

    Sub BindLevel()

        ddlLevel.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM OASIS_CCA..ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("sBsuid") + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()
        'ddlLevel.Items.Insert(0, New ListItem("All", "0"))

    End Sub

    Sub BindGrade()

        Dim i As Integer
        Dim strLevelIDS As String = ""


        If ddlLevel.SelectedIndex = 0 Then
            For i = 1 To ddlLevel.Items.Count - 1
                If strLevelIDS <> "" Then
                    strLevelIDS += "|"
                End If
                strLevelIDS += ddlLevel.Items(i).Value
            Next
        Else
            strLevelIDS = ddlLevel.SelectedItem.Value
        End If

        Dim flag As Integer
        If ddlAcademicYear.SelectedValue = Session("NEXT_ACD_ID") Then
            flag = 0
        Else
            flag = 1
        End If

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ACL_ID", strLevelIDS)
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlParameter("@FLAG", flag)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.GET_ACELEVELGRADES", param)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_ID"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()


    End Sub

    Sub BindSection()

        Dim strAcd As String = ""
        If ddlAcademicYear.SelectedItem.Value = Session("NEXT_ACD_ID") Then
            strAcd = Session("Current_ACD_ID")
        Else
            strAcd = ddlAcademicYear.SelectedItem.Value
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM OASIS..SECTION_M WHERE SCT_BSU_ID='" + Session("sBsuid") + "' " _
                                & " AND SCT_ACD_ID='" + strAcd + "' AND SCT_GRD_ID='" + ddlGrade.SelectedItem.Value + "' AND SCT_DESCR <> 'TEMP'" _
                                & " ORDER BY SCT_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        ddlSection.Items.Insert(0, New ListItem("All", "0"))

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlLevel.SelectedIndex = 0
        'ddlGrade.SelectedIndex = 0
        'ddlSection.SelectedIndex = 0
        BindGrade()
        BindSection()
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindGrade()
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged

    End Sub


    Sub CallReport()

        Try

            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim Ason_Date As String = ""
            Dim i As Integer
            Dim strLevelIDS As String = ""
            Dim strSections As String = ""

            'If ddlLevel.SelectedIndex = 0 Then
            '    For i = 1 To ddlLevel.Items.Count - 1
            '        If strLevelIDS <> "" Then
            '            strLevelIDS += "|"
            '        End If
            '        strLevelIDS += ddlLevel.Items(i).Value
            '    Next
            'Else
            '    strLevelIDS = ddlLevel.SelectedItem.Value
            'End If

            If ddlSection.SelectedIndex = 0 Then
                For i = 1 To ddlSection.Items.Count - 1
                    If strSections <> "" Then
                        strSections += "|"
                    End If
                    strSections += ddlSection.Items(i).Value
                Next
            Else
                strSections = ddlSection.SelectedItem.Value
            End If

            Dim flag As Integer
            If ddlAcademicYear.SelectedItem.Value = Session("Next_ACD_ID") Then
                flag = 0
            Else
                flag = 1
            End If


            Dim param As New Hashtable

            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
            param.Add("@ACL_IDS", ddlLevel.SelectedValue)
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
            param.Add("@SCT_IDS", strSections)
            param.Add("@CLM_ID", Session("clm"))
            param.Add("@FLAG", flag)
            param.Add("@CURR_ACD_ID", Session("Current_ACD_ID"))
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("ACY_DESCR", ddlAcademicYear.SelectedItem.Text)
            param.Add("LVL_DESCR", ddlLevel.SelectedItem.Text)

            'param.Add("@IMG_BSU_ID", Session("sBsuid"))
            'param.Add("@IMG_TYPE", "LOGO")
            'param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
            'param.Add("@ACL_IDS", ddlLevel.SelectedItem.Value)
            'param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
            'param.Add("@SCT_IDS", strSections)
            'param.Add("@CLM_ID", Session("clm"))
            'param.Add("@FLAG ", flag)
            'param.Add("@CURR_ACD_ID", Session("Current_ACD_ID"))
            

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "oasis_cca"
                .reportParameters = param
                .reportPath = Server.MapPath("../Rpt/rptACEReportBasedOnGradeSection.rpt")
            End With

            If hfbDownload.Value = "1" Then
                'Session("rptClass") = rptClass
                Dim rptDownload As New ReportDownload
                rptDownload.LoadReports(rptClass, rs)
                rptDownload = Nothing
            Else
                Session("rptClass") = rptClass
                ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
                ReportLoadSelection()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
        End Try


    End Sub

    Protected Sub btnGetReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetReport.Click
        CallReport()
    End Sub

    Protected Sub btnPDFdownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPDFdownload.Click
        hfbDownload.Value = "1"
        CallReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
