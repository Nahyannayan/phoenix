﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Partial Class CoCurricularActivities_Reports_Aspx_rptPEAgeGroupAnalysis
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC70030") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Populate Academic Year
                    Dim studCl As New studClass
                    studCl.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    BindActivitySchedule()



                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)

    End Sub

  
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindActivitySchedule()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACS_ID,ACS_DESCR FROM PE.ACTIVITY_SCHEDULE WHERE ACS_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "'" _
                                & " ORDER BY ACS_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSchedule.DataSource = ds
        ddlSchedule.DataTextField = "ACS_DESCR"
        ddlSchedule.DataValueField = "ACS_ID"
        ddlSchedule.DataBind()
    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")

        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@ACS_ID", ddlSchedule.SelectedValue.ToString)

        Dim strAge As String = ""
        Dim i As Integer

        For i = 0 To lstAge.Items.Count - 1
            If lstAge.Items(i).Selected = True Then
                If strAge <> "" Then
                    strAge += "|"
                End If
                strAge += lstAge.Items(i).Value.ToString
            End If
        Next

        param.Add("@AGES", strAge)


        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "oasis_CCA"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptPEAgeGroupAnalysis.rpt")
        End With

        Session("rptClass") = rptClass
        If hfbDownload.Value = 0 Then
            '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        End If


    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivitySchedule()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        hfbDownload.Value = 1
        CallReport()
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
