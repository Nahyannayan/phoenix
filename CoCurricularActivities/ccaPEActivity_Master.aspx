<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaPEActivity_Master.aspx.vb" Inherits="CoCurricularActivities_ccaPEActivity_Master" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Activity Master"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td>

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ForeColor=""
                                    ValidationGroup="GRP"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="center" class="matters" valign="middle">Fields Marked with (<font color="#c00000">*</font>) are mandatory</td>
                    </tr>

                    <tr>
                        <td>
                            <table width="100%">
                                <tr class="matters">
                                    <td align="left" width="20%"><span class="field-label">Description<span style="color: #800000">*</span></span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDescr" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDescr" runat="server"
                                            ControlToValidate="txtDescr" CssClass="error" Display="Dynamic"
                                            ErrorMessage="Description required" ForeColor="" ValidationGroup="GRP">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Display text<span style="color: #800000">*</span></span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtDisplay" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDisplay" runat="server"
                                            ControlToValidate="txtDisplay" CssClass="error" Display="Dynamic"
                                            ErrorMessage="Display text required" ForeColor="" ValidationGroup="GRP">*</asp:RequiredFieldValidator>
                                    </td>

                                </tr>
                                <tr class="matters">
                                    <td align="left"><span class="field-label">Unit Info</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUnit" runat="server" MaxLength="50"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Display Order<span style="color: #800000">*</span></span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOrder" runat="server" MaxLength="3"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvOrder" runat="server"
                                            ControlToValidate="txtOrder" CssClass="error" Display="Dynamic"
                                            ErrorMessage="Display order required" ForeColor="" ValidationGroup="GRP">*</asp:RequiredFieldValidator>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbOrder" runat="server" FilterType="Numbers"
                                            TargetControlID="txtOrder">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>

                                </tr>
                                <tr class="matters">
                                    <td align="left"><span class="field-label">Operation Type<span style="color: #800000">*</span></span></td>
                                    <td align="left" colspan="3">
                                        <asp:RadioButton ID="rbGE" runat="server" GroupName="OPT"
                                            Text="Greater than and equal to" />
                                        <asp:RadioButton ID="rbLE" runat="server" GroupName="OPT"
                                            Text="Less than and equal to" />
                                        <asp:RadioButton ID="rbRange" runat="server" GroupName="OPT" Text="Range" />
                                        
                                    </td>
                                </tr>
                                <tr class="matters">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" ValidationGroup="GRP"
                                            CssClass="button" Text="Add / Save" /><asp:Button ID="btnUpdate" runat="server"
                                                ValidationGroup="GRP" CssClass="button" Text="Update / Save" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                                    CssClass="button" Text="Cancel" /></td>
                                </tr>
                                <tr class="gridheader_pop">
                                    <td colspan="4" align="left" class="title-bg">Activity</td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" EmptyDataText="No Details Added Yet" Width="100%" Height="100%" OnPageIndexChanging="gvInfo_PageIndexChanging">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ACM_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACM_ID" runat="server" Text='<%# Bind("ACM_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblACM_DESCR" runat="server" Text='<%# Bind("ACM_DESCR") %>'></asp:Label>
                                                        <asp:Label ID="lblACM_TYPE" runat="server" Text='<%# Bind("ACM_TYPE") %>'></asp:Label>
                                                        <asp:Label ID="lblACM_UNIT" runat="server" Text='<%# Bind("ACM_UNIT") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Order">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACM_DISPLAYORDER" runat="server"
                                                            Text='<%# Bind("ACM_DISPLAYORDER") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="center" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Text">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblACM_DISPLAY" runat="server"
                                                            Text='<%# Bind("ACM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="left" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Operation Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACM_TYPE_DETAIL" runat="server"
                                                            Text='<%# Bind("ACM_TYPE_DETAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" runat="server" OnClick="lbtnEdit_Click">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="hfACM_ID" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

