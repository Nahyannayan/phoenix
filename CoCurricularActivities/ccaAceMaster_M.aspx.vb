Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class CoCurricularActivities_ccaAceMaster_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            'ts

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    BindType()
                    If ViewState("datamode") = "add" Then
                        hfACM_ID.Value = 0
                        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                        trSubject.Visible = False
                        BindSubject()
                    Else
                        hfACM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acmid").Replace(" ", "+"))
                        txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("descr").Replace(" ", "+"))
                        txtShortCode.Text = Encr_decrData.Decrypt(Request.QueryString("short").Replace(" ", "+"))
                        txtMainGroup.Text = Encr_decrData.Decrypt(Request.QueryString("maingroup").Replace(" ", "+"))

                        Dim type As String = Encr_decrData.Decrypt(Request.QueryString("type").Replace(" ", "+"))

                        ddlType.Items.FindByText(type).Selected = True

                        txtDescr.Enabled = False
                        txtShortCode.Enabled = False
                        txtMainGroup.Enabled = False

                        Dim li As New ListItem
                        li.Text = Encr_decrData.Decrypt(Request.QueryString("year").Replace(" ", "+"))
                        li.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                        ddlAcademicYear.Items.Add(li)
                        BindSubject()

                        Dim sbmid As String = Encr_decrData.Decrypt(Request.QueryString("sbmid").Replace(" ", "+"))
                        If Not ddlSubject.Items.FindByValue(sbmid) Is Nothing Then
                            ddlSubject.Items.FindByValue(sbmid).Selected = True
                        End If

                        If ddlType.SelectedItem.Text = "SUPPORT CLASSES" Then
                            trSubject.Visible = True
                        Else
                            trSubject.Visible = False
                        End If

                    End If

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
#Region "Private methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACT_ID,ACT_DESCR FROM ACE_TYPE_M ORDER BY ACT_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddltype.datasource = ds
        ddltype.datatextfield = "ACT_DESCR"
        ddltype.datavaluefield = "ACT_ID"
        ddltype.databind()
    End Sub

    Private Function isSubjectExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "select count(ACM_ID) from ACEMASTER_M where ACM_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' and  ACM_descr='" + txtDescr.Text + "' and ACM_ID<>" + hfACM_ID.Value.ToString
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If sbm = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE SBG_ACd_ID=" + Session("CURRENT_ACD_ID") _
                               & " AND SBG_DESCR NOT IN('THEORY','PRACT','PRACT.')" _
                               & " ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = 0
        ddlSubject.Items.Insert(0, li)
    End Sub
    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASIS_CCAConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = "exec saveACEMASTER_M " + hfACM_ID.Value.ToString + "," _
                                          & ddlAcademicYear.SelectedValue.ToString + "," _
                                          & "'" + Session("sbsuid") + "'" _
                                          & ",'" + txtDescr.Text.ToString _
                                          & "'," + txtShortCode.Text + ",'" _
                                          & ddlType.SelectedItem.Text _
                                          & "','" + ViewState("datamode") + "'," _
                                          & "'" + txtMainGroup.Text + "'," _
                                          & IIf(ddlSubject.SelectedValue = 0, "null", ddlSubject.SelectedValue.ToString)
                hfACM_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "add"
            txtDescr.Text = ""
            txtShortCode.Text = ""
            txtDescr.Enabled = True
            txtShortCode.Enabled = True
            txtMainGroup.Enabled = True
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            If hfACM_ID.Value = 0 Then
                lblError.Text = "No records to edit"
                Exit Sub
            End If
            txtDescr.Enabled = True
            txtShortCode.Enabled = True
            txtMainGroup.Enabled = True
            ViewState("datamode") = "edit"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfACM_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                txtDescr.Text = ""
                txtShortCode.Text = ""
                txtDescr.Enabled = False
                txtShortCode.Enabled = False
                txtMainGroup.Enabled = False
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If isSubjectExists() = False Then
                SaveData()
            Else
                lblError.Text = "This subject already exists"
            End If
            txtDescr.Enabled = False
            txtShortCode.Enabled = False
            txtMainGroup.Enabled = False
            ViewState("datamode") = "view"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ViewState("datamode") = "delete"
            SaveData()
            ViewState("datamode") = "none"
            hfACM_ID.Value = 0
            txtDescr.Text = ""
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

  
    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedItem.Text = "SUPPORT CLASSES" Then
            trSubject.Visible = True
        Else
            trSubject.Visible = False
        End If
    End Sub
End Class
