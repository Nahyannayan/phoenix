<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ccaStudPopupForm.aspx.vb" Inherits="ccaStudPopupForm" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    </script>
    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }

        function menu_click(val, mid) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            if (mid == 1) {
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
                   document.getElementById('<%=getid1() %>').src = path;
               }
               else if (mid == 2) {
                   document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
                     document.getElementById('<%=getid2() %>').src = path;
                 }
                 else if (mid == 3) {
                     document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
                      document.getElementById('<%=getid3() %>').src = path;
                  }
      }//end fn
    </script>
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
        <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
     <link href="/cssfiles/sb-admin.css" rel="stylesheet">
</head>
<body onload="listen_window();">

    <form id="form1" runat="server">
        <table width="100%">
            <tr valign="top" align="center" class="title-bg">
                <td>Select Value</td>
            </tr>
            <tr valign="top">
                <td>
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                        EmptyDataText="No Data" Width="100%" AllowPaging="True" CssClass="table table-row table-bordered" PageSize="15">
                        <Columns>
                            <asp:TemplateField HeaderText="Sel_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                <ItemTemplate>
                                    &nbsp;<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    Select<br />
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                <EditItemTemplate>
                                    &nbsp;
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSecDescr" runat="server" Text='<%# Bind("DESCR1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                                <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                                            <asp:TextBox ID="txtCode" runat="server" Width="90px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DESCR2") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                                            <asp:TextBox ID="txtName" runat="server" Width="120px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                <HeaderTemplate>
                                                <asp:Label ID="lblBSUName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                                            <asp:TextBox ID="txtBSUName" runat="server" Width="72px"></asp:TextBox>
                                                            <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                   <asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR2") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                        Width="76px" AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="" />
                    <asp:HiddenField ID="h_SGR_ID" runat="server" />
                    <asp:HiddenField ID="h_GRD_ID" runat="server" />
                    <asp:HiddenField ID="h_SCT_ID" runat="server" />
                    <asp:HiddenField ID="h_ACD_ID" runat="server" />
                    <asp:HiddenField ID="h_TYPE" runat="server" />
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
