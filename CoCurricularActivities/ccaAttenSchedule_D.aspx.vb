﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports Telerik.Web.UI

Partial Class CoCurricularActivities_ccaAttenSchedule_D
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                ddlacyear = studClass.PopulateAcademicYear(ddlacyear, Session("clm"), Session("sbsuid"))
                BindTerm()
                BindTermEdit()
                BindAce()
                BindAceLevel()
                BindAceEdit()
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                set_Menu_Img()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Try
    '        Dim smScriptManager As New ScriptManager
    '        smScriptManager = Master.FindControl("ScriptManager1")

    '        smScriptManager.EnablePartialRendering = False
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Sub BindAce()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlActivity.DataSource = ds
        ddlActivity.DataTextField = "ACM_DESCR"
        ddlActivity.DataValueField = "ACM_ID"
        ddlActivity.DataBind()
        If (Not ddlActivity.Items Is Nothing) AndAlso (ddlActivity.Items.Count > 1) Then
            ddlActivity.Items.Add(New ListItem("--", "0"))
            ddlActivity.Items.FindByText("--").Selected = True
        End If
    End Sub
    Sub BindAceLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM ACE_LEVEL_M WHERE ACL_BSU_ID=" + Session("sBsuid")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()
        If (Not ddlLevel.Items Is Nothing) AndAlso (ddlLevel.Items.Count > 1) Then
            ddlLevel.Items.Add(New ListItem("ALL", "0"))
            ddlLevel.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Sub BindAceEdit()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACM_ID,ACM_DESCR FROM ACEMASTER_M WHERE ACM_ACD_ID=" + ddlacyear.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlact.DataSource = ds
        ddlact.DataTextField = "ACM_DESCR"
        ddlact.DataValueField = "ACM_ID"
        ddlact.DataBind()
        If (Not ddlact.Items Is Nothing) AndAlso (ddlact.Items.Count > 1) Then
            ddlact.Items.Add(New ListItem("--", "0"))
            ddlact.Items.FindByText("--").Selected = True
        End If
    End Sub
    Sub Bindgroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM ACE_GROUP_M WHERE SGR_ACD_ID=" + ddlacyear.SelectedValue.ToString + " and SGR_ACM_ID=" + ddlact.SelectedValue
        If ddlTermEdit.SelectedValue <> "0" Then
            str_query += " and SGR_TRM_ID=" + ddlTermEdit.SelectedValue
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub
    Sub BindTerm()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString 
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_aCD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                             & " ORDER BY TRM_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()
        If (Not ddlTerm.Items Is Nothing) AndAlso (ddlTerm.Items.Count > 1) Then
            ddlTerm.Items.Add(New ListItem("ALL", "0"))
            ddlTerm.Items.FindByText("ALL").Selected = True
        End If
    End Sub
    Sub BindTermEdit()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM VW_TRM_M WHERE TRM_aCD_ID=" + ddlacyear.SelectedValue.ToString _
                             & " ORDER BY TRM_DESCRIPTION"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTermEdit.DataSource = ds
        ddlTermEdit.DataTextField = "TRM_DESCRIPTION"
        ddlTermEdit.DataValueField = "TRM_ID"
        ddlTermEdit.DataBind()
        If (Not ddlTermEdit.Items Is Nothing) AndAlso (ddlTermEdit.Items.Count > 1) Then
            ddlTermEdit.Items.Add(New ListItem("ALL", "0"))
            ddlTermEdit.Items.FindByText("ALL").Selected = True
        End If
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindAce()
        BindTerm()
    End Sub
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If grdType.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = grdType.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try

            gridbind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Public Sub gridbindEdit()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet




            str_Sql = "select AT_ID,AT_DATE,AT_DAYNAME,convert(char(5), AT_TFRM, 108) AT_TFRM,convert(char(5), AT_TTO, 108) AT_TTO from ATTENDANCE_M  " _
                       & " WHERE AT_ACD_ID=" + ddlacyear.SelectedValue + " and AT_ACM_ID=" + ddlact.SelectedValue + " AND AT_GRP_ID=" + ddlGroup.SelectedValue

            If ((txtDtefrm.Text <> "") And (txtDteTo.Text <> "")) Then
                str_Sql += " and AT_DATE BETWEEN '" + txtDtefrm.Text + "' AND '" + txtDteTo.Text + "'"
            End If

            If ddlDayName.Text <> "--" Then
                str_Sql += " and AT_DAYNAME='" + ddlDayName.Text + "'"
            End If
            str_Sql += " order by AT_DATE desc"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                grdEdit.DataSource = ds.Tables(0)
                grdEdit.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdEdit.DataSource = ds.Tables(0)
                Try
                    grdEdit.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdEdit.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdEdit.Rows(0).Cells.Clear()
                grdEdit.Rows(0).Cells.Add(New TableCell)
                grdEdit.Rows(0).Cells(0).ColumnSpan = columnCount
                grdEdit.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdEdit.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            grdEdit.DataSource = Nothing
            grdEdit.DataBind()
        End Try

    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = ""
            Dim str As String = ""
            Dim ds As New DataSet
            Dim dstype As New DataSet
            Dim strSidsearch As String()
            Dim strSearch As String
            Dim strFilter As String = ""
            Dim strName As String = ""
            Dim strNo As String = ""
            Dim txtSearch As New TextBox



            str_Sql = "select SGR_ID,SGR_DESCR from ACE_GROUP_M  " _
                       & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue + " and SGR_ACM_ID=" + ddlActivity.SelectedValue + " "
            If ddlLevel.SelectedValue <> "0" Then
                str_Sql = str_Sql + " and SGR_ACL_ID=" + ddlLevel.SelectedValue
            End If
            If ddlTerm.SelectedValue <> "0" Then
                str_Sql = str_Sql + " and SGR_TRM_ID=" + ddlTerm.SelectedValue
            End If




            If grdType.Rows.Count > 0 Then

                strName = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = grdType.HeaderRow.FindControl("txtStudName")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("SGR_DESCR", txtSearch.Text.Replace("/", " "), strSearch)
                strNo = txtSearch.Text
                If strFilter <> "" Then
                    str_Sql += strFilter
                End If
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            str = "exec getemp_datename '" + txtFrom.Text + "','" + txtTo.Text + "'"

            dstype = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str)

            If ds.Tables(0).Rows.Count > 0 Then
                grdType.DataSource = ds.Tables(0)
                grdType.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                grdType.DataSource = ds.Tables(0)
                Try
                    grdType.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = grdType.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                grdType.Rows(0).Cells.Clear()
                grdType.Rows(0).Cells.Add(New TableCell)
                grdType.Rows(0).Cells(0).ColumnSpan = columnCount
                grdType.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdType.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If
            Dim gv As GridView
            Dim i As Integer
            If ds.Tables(0).Rows.Count > 0 Then

                For i = 0 To grdType.Rows.Count - 1
                    With grdType.Rows(i)

                        gv = DirectCast(.FindControl("grdday"), GridView)

                        If dstype.Tables(0).Rows.Count > 0 Then
                            gv.DataSource = dstype.Tables(0)
                            gv.DataBind()
                        Else
                            dstype.Tables(0).Rows.Add(dstype.Tables(0).NewRow())
                            gv.DataSource = dstype.Tables(0)

                            gv.DataBind()
                        End If
                        Dim j As Integer
                        Dim txtfrm As TextBox
                        Dim txtTo As TextBox

                        For j = 0 To gv.Rows.Count - 1
                            With gv.Rows(j)

                                txtfrm = .FindControl("fromT")
                                txtTo = .FindControl("ToT")
                                txtfrm.Attributes.Add("onClick", "TPick('" & txtfrm.ClientID & "')")
                                txtTo.Attributes.Add("onClick", "TPick('" & txtTo.ClientID & "')")
                                txtfrm.Attributes.Add("onMouseDown", "TPick('" & txtfrm.ClientID & "')")
                                txtTo.Attributes.Add("onMouseDown", "TPick('" & txtTo.ClientID & "')")

                            End With
                        Next
                    End With
                Next
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        lblError.Text = ""
        If (Convert.ToDateTime(txtTo.Text) < Convert.ToDateTime(txtFrom.Text)) Then
            lblError.Text = "Date range is wrong"
            Exit Sub
        End If


        gridbindDays()

        gridbind()

        'ts

    End Sub

    'Protected Sub grdType_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdType.RowDataBound
    '    Try
    '        Dim str As String = ""
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim gv = DirectCast(e.Row.FindControl("grdday"), GridView)
    '            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
    '            Dim str_Sql As String = ""

    '            Dim ds As New DataSet




    '            ' str_Sql = "select dt_name,dt_ft,dt_tt from temp_datename "

    '            str_Sql = "exec getemp_datename '" + txtFrom.Text + "','" + txtTo.Text + "'"

    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

    '            If ds.Tables(0).Rows.Count > 0 Then
    '                gv.DataSource = ds.Tables(0)
    '                gv.DataBind()
    '            Else
    '                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
    '                gv.DataSource = ds.Tables(0)

    '                gv.DataBind()
    '            End If

    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    Public Sub gridbindDays()
        lblError.Text = ""
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet

            'str_Sql = "delete from temp_datename"
            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_Sql)
            str_Sql = "exec getemp_datename '" + txtFrom.Text + "','" + txtTo.Text + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_Sql)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                GrdApply.DataSource = ds.Tables(0)
                GrdApply.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                GrdApply.DataSource = ds.Tables(0)

                GrdApply.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Sub SaveData()
        lblError.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = ""

        Dim i As Integer
        Dim lblappl As Label
        Dim tfrm As TextBox
        Dim tto As TextBox
        Dim gv As GridView
        Dim lbe As Label

        Dim dt As New DataTable
        dt.Clear()
        dt.Columns.Add("dt_name")
        dt.Columns.Add("dt_ft")
        dt.Columns.Add("dt_tt")
        For i = 0 To GrdApply.Rows.Count - 1
            With GrdApply.Rows(i)
                lblappl = .FindControl("lblappl")
                tfrm = .FindControl("tfrm")
                tto = .FindControl("tto")
                If tfrm.Text <> "" Then
                    Dim s As Integer = (Convert.ToDateTime(tfrm.Text)).Hour
                    Dim s1 As Integer = (Convert.ToDateTime(tto.Text)).Hour


                    Dim t1 As Integer = s * 100
                    Dim t2 As Integer = s1 * 100

                    If t2 - t1 < 0 Then
                        lbe = .FindControl("lber")
                        lbe.Visible = True
                        lblError.Text = "Time range is wrong"
                        Exit Sub
                    End If


                    Dim dr As DataRow = dt.NewRow()
                    dr("dt_name") = lblappl.Text
                    dr("dt_ft") = tfrm.Text
                    dr("dt_tt") = tto.Text
                    dt.Rows.Add(dr)
                    'Dim cmd As New SqlCommand
                    'Dim objConn As New SqlConnection(str_conn)
                    'objConn.Open()
                    'cmd = New SqlCommand("dbo.updatetemp_datename", objConn)

                    'cmd.CommandType = CommandType.StoredProcedure
                    'cmd.Parameters.AddWithValue("@dt_name", lblappl.Text)
                    'cmd.Parameters.AddWithValue("@dt_ft", tfrm.DateInput.DbSelectedDate)
                    'cmd.Parameters.AddWithValue("@dt_tt", tto.DateInput.DbSelectedDate)



                    'cmd.ExecuteNonQuery()

                    'cmd.Dispose()
                    'objConn.Close()
                End If
            End With
        Next

        For i = 0 To grdType.Rows.Count - 1
            With grdType.Rows(i)

                gv = DirectCast(.FindControl("grdday"), GridView)

                If dt.Rows.Count > 0 Then
                    gv.DataSource = dt
                    gv.DataBind()

                End If
                
            End With
        Next



    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        lblError.Text = ""

        SaveData()
        'gridbind()
    End Sub

    Sub SaveAttendance()
        lblError.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = ""

        Dim i As Integer, j As Integer
        Dim lblappl As Label
        Dim tfrm As TextBox
        Dim tto As TextBox
        Dim grd As GridView
        Dim tyId As Label
        Dim Cb1 As CheckBox

        For i = 0 To grdType.Rows.Count - 1
            With grdType.Rows(i)
                grd = .FindControl("Grdday")
                tyId = .FindControl("tyId")
                Cb1 = .FindControl("Cb1")

                If Cb1.Checked Then
                    For j = 0 To grd.Rows.Count - 1
                        With grd.Rows(j)
                            lblappl = .FindControl("lblday")
                            tfrm = .FindControl("fromT")
                            tto = .FindControl("ToT")

                            If tfrm.Text <> "" Then
                                Dim cmd As New SqlCommand
                                Dim objConn As New SqlConnection(str_conn)
                                objConn.Open()
                                cmd = New SqlCommand("dbo.SAVE_ATTENDANCE_M", objConn)

                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.AddWithValue("@AT_ACD_ID", ddlAcademicYear.SelectedValue)
                                cmd.Parameters.AddWithValue("@AT_ACM_ID", ddlActivity.SelectedValue)
                                cmd.Parameters.AddWithValue("@AT_GRP_ID", tyId.Text)
                                cmd.Parameters.AddWithValue("@AT_DAYNAME", lblappl.Text)
                                cmd.Parameters.AddWithValue("@AT_TFRM", tfrm.Text)
                                cmd.Parameters.AddWithValue("@AT_TTO", tto.Text)
                                cmd.Parameters.AddWithValue("@dtfrm", txtFrom.Text)
                                cmd.Parameters.AddWithValue("@dtto", txtTo.Text)
                                cmd.Parameters.AddWithValue("@username", Session("sUsr_name"))
                                cmd.ExecuteNonQuery()

                                cmd.Dispose()
                                objConn.Close()
                            End If

                        End With
                    Next
                End If



            End With
        Next
        lblError.Text = "Record Saved Successfully"

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblError.Text = ""
        SaveAttendance()
    End Sub
    'Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuMaster.MenuItemClick
    '    Dim Iindex As Integer = Int32.Parse(e.Item.Value)
    '    Dim i As Integer
    '    'Make the selected menu item reflect the correct imageurl
    '    For i = 0 To mnuMaster.Items.Count - 1
    '        Select Case i
    '            Case 0
    '                If i = e.Item.Value Then
    '                    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btneditDelete.jpg"
    '                Else
    '                    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnaddnew.jpg"
    '                End If
    '                mvMaster.ActiveViewIndex = Iindex


    '            Case 1
    '                If i = e.Item.Value Then
    '                    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btneditDelete.jpg"
    '                Else
    '                    mnuMaster.Items(i).ImageUrl = "~/Images/tabmenu/btnaddnew.jpg"
    '                End If
    '                mvMaster.ActiveViewIndex = Iindex
    '        End Select
    '    Next
    'End Sub

    Protected Sub ddlacyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlacyear.SelectedIndexChanged
        lblError.Text = ""
        BindTermEdit()
        BindAceEdit()
        Bindgroup()
        gridbindEdit()
    End Sub

    Protected Sub ddlact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlact.SelectedIndexChanged
        lblError.Text = ""
        Bindgroup()
        gridbindEdit()
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        lblError.Text = ""
        gridbindEdit()
    End Sub

    Protected Sub grdEdit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEdit.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
           
            Dim txtfrm = DirectCast(e.Row.FindControl("tpFrom"), TextBox)
            Dim txtTo = DirectCast(e.Row.FindControl("tpTo"), TextBox)
            txtfrm.Attributes.Add("onClick", "TPick('" & txtfrm.ClientID & "')")
            txtTo.Attributes.Add("onClick", "TPick('" & txtTo.ClientID & "')")
            txtfrm.Attributes.Add("onMouseDown", "TPick('" & txtfrm.ClientID & "')")
            txtTo.Attributes.Add("onMouseDown", "TPick('" & txtTo.ClientID & "')")

        End If
    End Sub

    Protected Sub grdEdit_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEdit.RowDeleting
        Try
            grdEdit.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdEdit.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("AtId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM dbo.ATTENDANCE_M  WHERE AT_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbindEdit()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdEdit_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdEdit.RowUpdating

        lblError.Text = ""
        Try
            grdEdit.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = grdEdit.Rows(e.RowIndex)
            Dim lblID As New Label
            Dim tfrm As TextBox
            Dim tto As TextBox
            Dim txtdt As TextBox
            Dim lbe As Label
            lblID = TryCast(row.FindControl("AtId"), Label)
            txtdt = TryCast(row.FindControl("txtDate"), TextBox)
            tfrm = TryCast(row.FindControl("tpFrom"), TextBox)
            tto = TryCast(row.FindControl("tpTo"), TextBox)
            lbe = TryCast(row.FindControl("lberr"), Label)
            If lblID.Text <> "" Then

                If tfrm.Text <> "" Then
                    Dim s As Integer = (Convert.ToDateTime(tfrm.Text)).Hour
                    Dim s1 As Integer = (Convert.ToDateTime(tto.Text)).Hour


                    Dim t1 As Integer = s * 100
                    Dim t2 As Integer = s1 * 100

                    If t2 - t1 < 0 Then

                        lbe.Visible = True
                        lblError.Text = "Time range is wrong"
                        Exit Sub
                    End If
                End If


                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_query As String = ""

                str_query = " exec update_ATTENDANCE_M " & lblID.Text & "," & ddlacyear.SelectedValue & "," & ddlact.SelectedValue & "," & ddlGroup.SelectedValue & "," _
                            & " '" & txtdt.Text & "','" & tfrm.Text & "','" & tto.Text & "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbindEdit()
            lblError.Text = "Updated"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlDayName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDayName.SelectedIndexChanged
        lblError.Text = ""
        gridbindEdit()
    End Sub

    Protected Sub GrdApply_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdApply.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim txtfrm = DirectCast(e.Row.FindControl("tfrm"), TextBox)
            Dim txtTo = DirectCast(e.Row.FindControl("tto"), TextBox)
            txtfrm.Attributes.Add("onClick", "TPick('" & txtfrm.ClientID & "')")
            txtTo.Attributes.Add("onClick", "TPick('" & txtTo.ClientID & "')")
            txtfrm.Attributes.Add("onMouseDown", "TPick('" & txtfrm.ClientID & "')")
            txtTo.Attributes.Add("onMouseDown", "TPick('" & txtTo.ClientID & "')")

        End If
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        lblError.Text = ""
        gridbindEdit()
    End Sub

    Protected Sub ddlTermEdit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTermEdit.SelectedIndexChanged
        Bindgroup()
        gridbindEdit()
    End Sub

    Protected Sub mnuMaster_MenuItemClick(sender As Object, e As MenuEventArgs)
        Dim Iindex As Integer = Int32.Parse(e.Item.Value)
        mvMaster.ActiveViewIndex = Iindex
    End Sub
End Class
