Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class CoCurricularActivities_ccaAceGroup_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '     Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10030") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                set_Menu_Img()
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                GridBind()
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '    lblError.Text = "Request could not be processed"
            'End Try

        End If
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Try
    '        Dim smScriptManager As New ScriptManager
    '        smScriptManager = Master.FindControl("ScriptManager1")

    '        smScriptManager.EnablePartialRendering = False
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        lblError.Text = "Request could not be processed"
    '    End Try
    'End Sub
    Protected Sub btnGroup_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnAce_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlgvTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

 
#Region "Private methods"
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnOption_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        'if the login user is a teacher then display only the groups for that teacher
        Dim str_query As String


        If Session("CurrSuperUser") = "Y" Or Session("AceSuperUser") = "Y" Then

            str_query = "SELECT DISTINCT SGR_ID,SGR_ACM_ID,SGR_ACL_ID,SGR_DESCR,ACM_DESCR,ISNULL(ACL_DESCR,'') ACL_DESCR,ISNULL(TRM_DESCRIPTION,'') TRM_DESCR,ISNULL(SGR_TRM_ID,0) SGR_TRM_ID " _
                       & " FROM ACE_GROUP_M AS A INNER JOIN ACEMASTER_M AS B ON A.SGR_ACM_ID=B.ACM_ID" _
                       & " LEFT OUTER JOIN ACE_LEVEL_M AS C ON A.SGR_ACL_ID=C.ACL_ID" _
                       & " LEFT OUTER JOIN VW_TRM_M ON SGR_TRM_ID=TRM_ID AND TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        ElseIf studClass.isEmpTeacher(Session("EmployeeID")) = True Then
            str_query = "SELECT DISTINCT SGR_ID,SGR_ACM_ID,SGR_ACL_ID,SGR_DESCR,ACM_DESCR,ISNULL(ACL_DESCR,'') ACL_DESCR,ISNULL(TRM_DESCRIPTION,'') TRM_DESCR,ISNULL(SGR_TRM_ID,0) SGR_TRM_ID " _
                 & " FROM ACE_GROUP_M AS A INNER JOIN ACEMASTER_M AS B ON A.SGR_ACM_ID=B.ACM_ID" _
                 & " INNER JOIN ACE_LEVEL_M AS C ON A.SGR_ACL_ID=C.ACL_ID" _
                 & " INNER JOIN ACE_GROUPS_TEACHER_S AS D ON A.SGR_ID=D.SGS_SGR_ID AND SGS_TODATE IS NULL " _
                 & " AND SGS_EMP_ID=" + Session("EmployeeID") _
                 & " LEFT OUTER JOIN VW_TRM_M ON SGR_TRM_ID=TRM_ID AND TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                 & " WHERE SGR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        End If

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim txtSearch As New TextBox
        Dim grpSearch As String = ""
        Dim aceSearch As String = ""

        Dim selectedLevel As String = ""
        Dim selectedShift As String = ""
        Dim selectedStream As String = ""
        Dim selectedTerm As String = ""
        Dim ddlgvLevel As New DropDownList
        Dim ddlgvTerm As New DropDownList

        Dim i As Integer

        If gvGroup.Rows.Count > 0 Then

            txtSearch = gvGroup.HeaderRow.FindControl("txtGroup")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SGR_DESCR", txtSearch.Text, strSearch)
            grpSearch = txtSearch.Text

            txtSearch = gvGroup.HeaderRow.FindControl("txtAce")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ACM_DESCR", txtSearch.Text, strSearch)
            aceSearch = txtSearch.Text


            ddlgvLevel = gvGroup.HeaderRow.FindControl("ddlgvLevel")
            If ddlgvLevel.Text <> "ALL" And ddlgvLevel.Text <> "" Then
                strFilter += " and ACL_DESCR='" + ddlgvLevel.Text + "'"
                selectedLevel = ddlgvLevel.Text
            End If

            ddlgvTerm = gvGroup.HeaderRow.FindControl("ddlgvTerm")
            If ddlgvTerm.Text <> "ALL" And ddlgvTerm.Text <> "" Then
                strFilter += " and TRM_DESCRIPTION='" + ddlgvTerm.Text + "'"
                selectedTerm = ddlgvTerm.Text
            End If

            If strFilter.Trim <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " order by sgr_descr"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvGroup.DataSource = ds


        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGroup.DataBind()
            Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
            gvGroup.Rows(0).Cells.Clear()
            gvGroup.Rows(0).Cells.Add(New TableCell)
            gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvGroup.DataBind()
        End If

        txtSearch = New TextBox
        txtSearch = gvGroup.HeaderRow.FindControl("txtGroup")
        txtSearch.Text = grpSearch

        txtSearch = New TextBox
        txtSearch = gvGroup.HeaderRow.FindControl("txtAce")
        txtSearch.Text = aceSearch

        Dim dt As DataTable = ds.Tables(0)

        If gvGroup.Rows.Count > 0 Then
            ddlgvLevel = gvGroup.HeaderRow.FindControl("ddlgvLevel")
            ddlgvTerm = gvGroup.HeaderRow.FindControl("ddlgvTerm")

            Dim dr As DataRow

            ddlgvLevel.Items.Clear()
            ddlgvLevel.Items.Add("ALL")

            ddlgvTerm.Items.Clear()
            ddlgvTerm.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvLevel.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvLevel.Items.Add(.Item(5))
                    End If

                    If ddlgvTerm.Items.FindByText(.Item(6)) Is Nothing Then
                        ddlgvTerm.Items.Add(.Item(6))
                    End If
                End With
            Next

            If selectedLevel <> "" Then
                ddlgvLevel.Text = selectedLevel
            End If

            If selectedTerm <> "" Then
                ddlgvTerm.Text = selectedTerm
            End If

        End If


        set_Menu_Img()




    End Sub

#End Region

    Protected Sub lbAuto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAuto.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Curriculum\clmGroupAutoAllocate_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        Try
            gvGroup.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            gvGroup.DataBind()
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\CocurricularActivities\ccaAceGroup_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGroup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGroup.RowCommand
        Try

            Dim url As String


            If e.CommandName = "View" Then
                Dim lblGroup As New Label
                    Dim lblSgrId As Label
                Dim lblAcmId As Label
                Dim lblAclId As Label
                Dim lblAce As Label
                Dim lblLevel As Label
                Dim lblTrmId As Label
                Dim lblTerm As Label

                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvGroup.Rows(index), GridViewRow)


                ViewState("datamode") = Encr_decrData.Encrypt("view")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

                     lblGroup = selectedRow.FindControl("lblGroup")
                lblSgrId = selectedRow.FindControl("lblSgrId")
                lblAcmId = selectedRow.FindControl("lblAcmId")
                lblAclId = selectedRow.FindControl("lblAclId")
                lblAce = selectedRow.FindControl("lblAce")
                lblLevel = selectedRow.FindControl("lblLevel")
                lblTrmId = selectedRow.FindControl("lblTrmId")
                lblTerm = selectedRow.FindControl("lblTerm")

                url = String.Format("~\CocurricularActivities\ccaAceGroup_M.aspx?MainMnu_code={0}&datamode={1}" _
                 & "&ace=" + Encr_decrData.Encrypt(lblAce.Text) + "&academicyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                 & "&level=" + Encr_decrData.Encrypt(lblLevel.Text) + "&group=" + Encr_decrData.Encrypt(lblGroup.Text) _
                 & "&acmid=" + Encr_decrData.Encrypt(lblAcmId.Text) + "&sgrid=" + Encr_decrData.Encrypt(lblSgrId.Text) _
                 & "&aclid=" + Encr_decrData.Encrypt(lblAclId.Text) _
                 & "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                 & "&trmid=" + Encr_decrData.Encrypt(lblTrmId.Text) _
                 & "&term=" + Encr_decrData.Encrypt(lblTerm.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
