﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaPEStudentActivityDetails.aspx.vb" Inherits="CoCurricularActivities_ccaPEStudentActivityDetails"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        function Numeric_Only() {


            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return true; }
                event.keyCode = 0
            }
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Activity Details"> </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%"  >
                    <tr  >
                        <td  >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                            <table id="tblTC" runat="server"  width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear"  runat="server" AutoPostBack="True"
                                             >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Activity Schedule</span>
                                    </td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlSchedule"  runat="server" AutoPostBack="True"
                                             >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Select Grade</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGrade"  runat="server" AutoPostBack="True"
                                            >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Select Section</span>
                                    </td>
                                    <td align="left" class="matters"  >
                                        <asp:DropDownList ID="ddlSection"  runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Student ID</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left" class="matters"><span class="field-label">Student Name</span>
                                    </td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td  colspan="4" align="center" >
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="tr1" runat="server">
                                    <td  colspan="4" >
                                      
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" id="tr2" runat="server">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4"  />
                                    </td>
                                </tr>
                                <tr id="tr3" runat="server">
                                    <td align="center" class="matters" colspan="4" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                            EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                              PageSize="5" AllowPaging="FALSE" Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo"   runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                  <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                   <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity1H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity1Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity1Id"  runat="server"></asp:TextBox>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity2H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity2Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity2Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity3H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity3Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity3Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity4H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity4Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity4Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity5H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity5Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity5Id"   runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity6H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity6Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity6Id"   runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity7H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity7Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity7Id"   runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity8H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity8Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity8Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity9H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity9Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity9Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity10H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity10Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity10Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity11H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity11Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity11Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity12H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity12Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity12Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity13H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity13Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity13Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity14H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity14Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity14Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblActivity15H" runat="server" ></asp:Label>
                                                        <asp:Label ID="lblActivity15Id" runat="server" Visible="false"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtActivity15Id"  runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                                </asp:TemplateField>



                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle   CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" id="tr4" runat="server" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4"   />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfTotalActivity" runat="server"></asp:HiddenField>


                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRSM_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfRPF_ID" runat="server"></asp:HiddenField>
                            <input id="h_DivStyle" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
