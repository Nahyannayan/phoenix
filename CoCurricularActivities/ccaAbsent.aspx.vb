﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CURRICULUM
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Partial Class CoCurricularActivities_ccaAbsent
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC10052") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    GridBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)

    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim grade As String()
        Dim bShowSlno As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString


        Dim str_query As String = "SELECT AB_ID,AB_REASON FROM dbo.ABSENTEES" _
                                & " WHERE AB_BSU_ID='" + Session("sBsuid") + "'"



        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = "edit"
                dr.Item(3) = i.ToString
                dt.Rows.Add(dr)
            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 50 rows
        For i = 0 To 15 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = "edit"
            dr.Item(3) = i.ToString
            dt.Rows.Add(dr)
        Next

        Session("dtUnit") = dt



        gvSkill.DataSource = dt
        gvSkill.DataBind()

    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AB_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "AB_REASON"
        dt.Columns.Add(column)

        
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        Return dt
    End Function
    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString

        Dim str_query As String = ""

        Dim i As Integer
        Dim lblSksId As Label
        Dim txtSkill As TextBox
       


        Dim mode As String = ""
    
        For i = 0 To gvSkill.Rows.Count - 1
            With gvSkill.Rows(i)
                lblSksId = .FindControl("lblSksId")
                txtSkill = .FindControl("txtSkill")

                If lblSksId.Text = "0" And txtSkill.Text <> "" Then
                    mode = "add"
                ElseIf txtSkill.Text <> "" Then
                    mode = "edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then

                    Dim cmd As New SqlCommand
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    cmd = New SqlCommand("dbo.saveAbsentees", objConn)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@ID", lblSksId.Text)
                    cmd.Parameters.AddWithValue("@AB_BSU_ID", Session("sBsuid"))
                    cmd.Parameters.AddWithValue("@AB_REASON", txtSkill.Text)

                    cmd.Parameters.AddWithValue("@MODE", mode)

                    cmd.ExecuteNonQuery()

                    cmd.Dispose()
                    objConn.Close()
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub
    Protected Sub gvSkill_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSkill.RowDeleting
        Try
            gvSkill.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = gvSkill.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblSksId"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM dbo.ABSENTEES  WHERE AB_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            GridBind()
        Catch ex As Exception

        End Try
    End Sub
End Class
'ts