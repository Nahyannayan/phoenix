﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ccaPEActivity_Schedule.aspx.vb" Inherits="CoCurricularActivities_ccaPEActivity_Schedule" Title="Untitled Page" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           <asp:Label id="lblCaption" runat="server" Text="Activity Schedule"> </asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td  >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table  width="100%">

                                <tr>
                                    <td class="matters" align="left" width="20%"  ><span class="field-label">Select Academic Year</span>
                                    </td>
                                    <td class="matters" align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="50%"></td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave1" runat="server" CssClass="button"
                                Text="Save" ValidationGroup="groupM1" 
                                TabIndex="7" /></td>
                    </tr>

                    <tr>
                        <td align="left"><font color='maroon' size='2'>Note:Delete is applicable only for those schedule for which student's are not allocated </font></td>
                    </tr>

                    <tr>
                        <td>
                            <asp:GridView ID="gvRpt" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                  PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem"   Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>

                                    <asp:TemplateField HeaderText="index" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ACS_ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcsId" runat="server" Text='<%# Bind("ACS_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Order">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" 
                                                Text='<%# Bind("ACS_DISPLAYORDER") %>'></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbtxtorder" FilterType="Numbers"
                                                TargetControlID="txtOrder" runat="server">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("ACS_DISPLAYORDER") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Schedule">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAcs" runat="server"  Text='<%# Bind("ACS_DESCR") %>'></asp:TextBox>
                                            <asp:Label ID="lblAcs" runat="server" Text='<%# Bind("ACS_DESCR") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Age Cutoff Date">
                                        <ItemTemplate>
                                                        <asp:TextBox ID="txtDate" Text='<%# Bind("ACS_AGECUTOFFDATE") %>'   runat="server"></asp:TextBox>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("ACS_AGECUTOFFDATE_DB") %>' Visible="false"></asp:Label>
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbtxtDate" FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters"
                                                            TargetControlID="txtDate" runat="server" ValidChars="/">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                        <asp:ImageButton ID="imgStartDate" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                                Format="dd/MMM/yyyy" PopupButtonID="imgStartDate" PopupPosition="BottomLeft" TargetControlID="txtDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete" Enabled='<%# Bind("bEnabled") %>'></asp:LinkButton>
                                            <asp:Label ID="lblDelete" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle   CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:HiddenField ID="hfRSM_ID" runat="server" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button"
                                Text="Save" ValidationGroup="groupM1" 
                                TabIndex="7" />
                        </td>
                    </tr>



                </table>
            </div>
        </div>
    </div>
</asp:Content>

