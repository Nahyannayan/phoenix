﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System


Partial Class CoCurricularActivities_ccaACEAutoAllocate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")ts

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "CC30016") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindLevels()
                    BindType()
                    BindTerm()
                    BindActivities()
                    'If cblActivities.Items.Count = 0 Then
                    '    trAct1.Visible = False
                    '    trAct2.Visible = False
                    'Else
                    '    trAct1.Visible = True
                    '    trAct2.Visible = True
                    'End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
            'Else
            'studClass.SetChk(gvSubjects, Session("liUserList"))
        End If
    End Sub

    Sub BindLevels()

        ddlLevel.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACL_ID,ACL_DESCR FROM OASIS_CCA..ACE_LEVEL_M WHERE ACL_BSU_ID='" + Session("sBsuid") + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLevel.DataSource = ds
        ddlLevel.DataTextField = "ACL_DESCR"
        ddlLevel.DataValueField = "ACL_ID"
        ddlLevel.DataBind()

    End Sub

    Sub BindType()

        ddlType.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ACT_ID,ACT_DESCR FROM OASIS_CCA..ACE_TYPE_M "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlType.DataSource = ds
        ddlType.DataTextField = "ACT_DESCR"
        ddlType.DataValueField = "ACT_ID"
        ddlType.DataBind()

    End Sub

    Function GetActivityXML() As String

        Dim strActivities As String = ""
        Dim i As Integer
        For i = 0 To cblActivities.Items.Count - 1
            If cblActivities.Items(i).Selected = True Then
                strActivities += "<ID><ACM_ID>" + cblActivities.Items(i).Value + "</ACM_ID></ID>"
                'If strActivities <> "" Then
                '    strActivities += "|"
                'End If
                'strActivities += cblActivities.Items(i).Value
            End If
        Next

        'For Each row In cblActivities.Rows
        '    chkSelect = row.FindControl("chkSelect")
        '    If chkSelect.Checked = True Then
        '        lblSbgId = row.FindControl("lblSbgId")
        '        str += "<ID><SBG_ID>" + lblSbgId.Text + "</SBG_ID></ID>"
        '    End If
        'Next
        If strActivities <> "" Then
            strActivities = "<IDS>" + strActivities + "</IDS>"
        End If
        Return strActivities
    End Function

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        BindActivities()
        'If cblActivities.Items.Count = 0 Then
        '    trAct1.Visible = False
        '    trAct2.Visible = False
        'Else
        '    trAct1.Visible = True
        '    trAct2.Visible = True
        'End If
    End Sub

    Sub BindActivities()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String = "SELECT ALG_ID,ALG_ACL_ID,ALG_ACT_ID,ALG_BSU_ID,ALG_ACD_ID,ACM_DESCR,ACM_ID," _
                                 & " ALG_MAX_CAPACITY, ALG_BWAITLISTALLOWED,SGR_ID " _
                                 & " FROM ACE_LEVEL_GRADE INNER JOIN ACEMASTER_M ON ACM_ID=ALG_ACTIVITIES " _
                                 & " LEFT OUTER JOIN ACE_GROUP_M ON SGR_BSU_ID=ALG_BSU_ID AND SGR_ACD_ID=ALG_ACD_ID AND SGR_ACM_ID=ACM_ID AND SGR_ACL_ID=ALG_ACL_ID " _
                                 & " WHERE ALG_ACD_ID='" + ddlAcademicYear.SelectedItem.Value + "' AND ALG_ACT_ID='" + ddlType.SelectedItem.Value + "' AND ALG_ACL_ID='" + ddlLevel.SelectedItem.Value + "'" _
                                 & " AND ALG_BSU_ID='" + Session("sBsuid") + "' AND SGR_ID IS NULL ORDER BY ACM_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cblActivities.DataSource = ds
        cblActivities.DataTextField = "ACM_DESCR"
        cblActivities.DataValueField = "ACM_ID"
        cblActivities.DataBind()
        'gvActivity.DataSource = ds
        'gvActivity.DataBind()

        If cblActivities.Items.Count = 0 Then
            'trAct1.Visible = True
            'trAct2.Visible = True
            chkSelectAll.Visible = False
            lblNoData.Visible = True
        Else
            'trAct1.Visible = True
            'trAct2.Visible = True
            chkSelectAll.Visible = True
            lblNoData.Visible = False
        End If

    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindActivities()
        BindTerm()
        'If cblActivities.Items.Count = 0 Then
        '    trAct1.Visible = False
        '    trAct2.Visible = False
        'Else
        '    trAct1.Visible = True
        '    trAct2.Visible = True
        'End If
    End Sub

    Protected Sub BindTerm()

        ddlTerm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TRM_ID,TRM_DESCRIPTION FROM  DBO.VW_TRM_M WHERE TRM_BSU_ID='" + Session("sbsuid") + "' AND TRM_ACD_ID=" + ddlAcademicYear.SelectedItem.Value
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTerm.DataSource = ds
        ddlTerm.DataTextField = "TRM_DESCRIPTION"
        ddlTerm.DataValueField = "TRM_ID"
        ddlTerm.DataBind()

    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLevel.SelectedIndexChanged
        BindActivities()
        'If cblActivities.Items.Count = 0 Then
        '    trAct1.Visible = False
        '    trAct2.Visible = False
        'Else
        '    trAct1.Visible = True
        '    trAct2.Visible = True
        'End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            'ViewState("datamode") = Encr_decrData.Encrypt("view")
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt("C100050")
            'Dim url As String
            'url = String.Format("~\Curriculum\clmGroup_View.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            'Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim sbgXml As String = GetActivityXML()
        If sbgXml = "" Then
            lblError.Text = "Please select a subject"
        End If
        Dim strTerm As String = ddlTerm.SelectedItem.Text
        Dim len As Integer = strTerm.Length
        Dim strTrmShortName As String = strTerm.Substring(0, 1) + strTerm.Substring(len - 1, 1)
        str_query = "exec [dbo].[saveGROUPS_AUTOALLOCATEACTIVITIES] '" + Session("sbsuid") + "'," + ddlAcademicYear.SelectedItem.Value + "," + ddlLevel.SelectedItem.Value + "," + ddlType.SelectedItem.Value + ",'" + sbgXml + "','" + strTrmShortName + "'," + ddlTerm.SelectedValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        lblError.Text = "The Activity has been Allocated successfully!"
        BindActivities()
        'End With
    End Sub

End Class
