<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ccaAceCriteria.aspx.vb" Inherits="CoCurricularActivities_ccaAceCriteria" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Ace Criteria"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <asp:Panel ID="panel1" runat="server" DefaultButton="btnAddCriteria">
                                <table id="tb1" runat="server" width="100%">

                                    <tr>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                                        <td align="left" class="matters" width="30%">
                                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td align="left" class="matters" width="20%"><span class="field-label">Report Card</span></td>
                                        <td align="left" class="matters" width="30%">
                                            <asp:DropDownList ID="ddlReportCard" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Ace Description</span></td>
                                        <td align="left" class="matters">
                                            <asp:DropDownList ID="ddlAce" runat="server" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td class="matters" align="left"><span class="field-label">Level</span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left" class="matters"><span class="field-label">Criteria short name</span></td>
                                        <td>
                                            <asp:TextBox ID="txtShort" runat="server"></asp:TextBox></td>
                                        <td align="left" class="matters" colspan="1">
                                            <asp:Label ID="lblText" runat="server" Text="Criteria Description" CssClass="field-label"></asp:Label>
                                        </td>
                                        <td align="left" class="matters">
                                            <asp:TextBox SkinID="MultiText" ID="txtDetail" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="matters" colspan="1"><span class="field-label">Display Order</span></td>
                                        <td align="left" class="matters" colspan="1">
                                            <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="center" class="matters"
                                            colspan="2">
                                            <asp:Button ID="btnAddCriteria" runat="server" CssClass="button" TabIndex="7"
                                                Text="Add Criteria" ValidationGroup="groupM1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="matters" colspan="6">
                                            <asp:Panel ID="pnlPopup" runat="server" BackColor="white" CssClass="panel-cover">
                                                <asp:UpdatePanel ID="uppnlPopup" runat="server">
                                                    <ContentTemplate>
                                                        <table id="Table2" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <asp:Label ID="lblError1" runat="server" CssClass="error" EnableViewState="False"
                                                                            SkinID="error" Style="text-align: center"></asp:Label></td>
                                                                </tr>
                                                                <tr class="title-bg">
                                                                    <td colspan="4">Default Values
                                                                    </td>
                                                                </tr>
                                                                <tr align="left">
                                                                    <td align="left" class="matters"><span class="field-label">Ace</span></td>
                                                                    <td align="left" class="matters" colspan="3">
                                                                        <asp:Label ID="lblAce" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="matters"><span class="field-label">Default Value</span></td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtDefault" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="matters"><span class="field-label">Comment</span></td>
                                                                    <td align="left" class="matters">
                                                                        <asp:TextBox ID="txtComment" runat="server"
                                                                            TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="matters"><span class="field-label">Display Order</span>
                                                                    </td>
                                                                    <td align="left" class="matters">
                                                                        <asp:TextBox ID="txtDefaultOrder" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                                                    </td>
                                                                    <td align="center" colspan="2">
                                                                        <asp:Button ID="btnAddDetail" runat="server" CssClass="button" TabIndex="7"
                                                                            Text="Add Detail" />
                                                                        <asp:Button ID="btnUpdateDetail" runat="server" CssClass="button"
                                                                            Text="Update Detail" Visible="False" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="4">
                                                                        <asp:GridView ID="gvDefault" runat="server" AllowPaging="false" CssClass="table table-row table-bordered" Width="100%"
                                                                            AutoGenerateColumns="False" DataKeyNames="ID" EmptyDataText="No Details Added">
                                                                            <RowStyle CssClass="griditem" />
                                                                            <EmptyDataRowStyle CssClass="matters" HorizontalAlign="Center"
                                                                                VerticalAlign="Middle" />
                                                                            <Columns>
                                                                                <asp:BoundField DataField="RSP_DESCR" HeaderText="Default value"
                                                                                    HtmlEncode="False">
                                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="RSP_COMMENT" HeaderText="Comment">
                                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="RSP_DISPLAYORDER" HeaderText="Display Order">
                                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                                                                    <EditItemTemplate>
                                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True"
                                                                                            CommandName="Update" Text="Update"></asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False"
                                                                                            CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                                                    </EditItemTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lblEdit" runat="server" CausesValidation="False"
                                                                                            CommandName="Edit" Text="Edit"></asp:LinkButton>

                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lblDelete" runat="server" CausesValidation="False"
                                                                                            CommandName="Delete" Text="Delete"></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                                                            <HeaderStyle CssClass="gridheader_new" />
                                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="4">
                                                                        <asp:Button ID="btnStudSave" runat="server" CausesValidation="False"
                                                                            CssClass="button" TabIndex="7" Text="Save" ValidationGroup="groupM1" />
                                                                        <asp:Button ID="btnClose" runat="server" CausesValidation="false"
                                                                            CssClass="button" Text="Close" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                            <br />
                                            <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="False"
                                                BorderStyle="None" CssClass="table table-row table-bordered" EmptyDataText="No record added."
                                                PageSize="20" Width="100%">
                                                <RowStyle CssClass="griditem" Wrap="False" />
                                                <EmptyDataRowStyle Wrap="False" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRsdId" runat="server" Text='<%# Bind("RSD_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Criteria Shortname">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCriteriaShort" runat="server"
                                                                Text='<%# Bind("RSD_HEADER") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Criteria">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCriteria" runat="server" Text='<%# Bind("RSD_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Display Order">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("RSD_DISPLAYORDER") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:ButtonField>
                                                    <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:ButtonField>
                                                    <asp:TemplateField HeaderText="Default Values">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblEdit" runat="server" OnClick="lblEdit_Click"
                                                                Text="Default Values" Enabled='<%# Bind("bENABLE") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                                <EditRowStyle Wrap="False" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="4">

                            <ajaxToolkit:ModalPopupExtender ID="mdefault" runat="server" BackgroundCssClass="modalBackground"
                                DropShadow="true" PopupControlID="pnlPopup" CancelControlID="btnClose"
                                RepositionMode="RepositionOnWindowResizeAndScroll" TargetControlID="lblError">
                            </ajaxToolkit:ModalPopupExtender>

                        </td>
                    </tr>

                    <tr>
                        <td class="matters" valign="bottom" colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" CausesValidation="False" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:HiddenField ID="hfRSD_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:HiddenField ID="hfRandom" runat="server" Value="1000000000"></asp:HiddenField>
                            <asp:RegularExpressionValidator ID="regOrder" runat="server" ControlToValidate="txtOrder"
                                Display="None" ErrorMessage="Display order bust be a number" ValidationExpression="^\d+$"
                                ValidationGroup="groupM1">
                            </asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rdShort" runat="server" ControlToValidate="txtShort"
                                Display="None" ErrorMessage="Please enter data in the field subject criteria short name"
                                ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator>

                            <asp:RequiredFieldValidator ID="rfOrder" runat="server" ControlToValidate="txtOrder"
                                Display="None" ErrorMessage="Please enter data in the field  display order" ForeColor="RosyBrown"
                                ValidationGroup="groupM1">
                            </asp:RequiredFieldValidator></td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

