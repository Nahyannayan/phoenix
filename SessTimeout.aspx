﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SessTimeout.aspx.vb" Inherits="SessTimeout" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
    <style type="text/css">
        .clockSession
{
    width: 409px;
    height: 174px;
    background: url(images/clockSession.jpg) no-repeat;
}
         
.contSession
{
    width: 270px;
    float: right;
    text-align: center;
    margin: 20px 20px 0 0 ;
}
         
.sessionExpire
{
    color: #3366ff;
    padding-top: 30px;
}
         
.showNotification
{
    padding-top: 15px;
    color: #666;
}
         
.timeRemain
{
    padding-top: 5px; 
    color: #000;
}
         
.timeSeconds
{
    font-size: 30px;
    padding-right: 5px;
}
         
.infoIcon, .notificationContent
{
    display: inline-block;
    zoom: 1;
    *display: inline;
}
         
.infoIcon
{
    width: 32px;
    height: 32px;
    margin: 0 10px ;
    vertical-align: top;
}
         
.notificationContent
{
    width: 160px;
    vertical-align: bottom;
}
 
.demo-container
{
    width: 410px;
}
 
div.timeoutNotification {
    background-color: #c3d8f1;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
  <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" />
    <div class="demo-container no-bg">
    <div class="clockSession">
        <div class="contSession">
            <div class="sesseionExpire">
                Your Session will expire in
                <%= Session.Timeout %>
                minutes</div>
            <div class="showNotification">
                Notification will be shown in:</div>
            <div class="timeRemain">
                <span class="timeSeconds"><span id="mainLbl">60 </span></span>seconds</div>
        </div>
    </div>
    </div>
    <telerik:RadNotification RenderMode="Lightweight" ID="RadNotification1" runat="server" Position="Center" Width="270"
        Height="100" OnCallbackUpdate="OnCallbackUpdate" LoadContentOn="PageLoad" AutoCloseDelay="60000"
        OnClientShowing="notification_showing" OnClientHidden="notification_hidden"
        Title="Continue Your Session" Skin="Office2007" EnableRoundedCorners="true"
        ShowCloseButton="false" KeepOnMouseOver="false" CssClass="timeoutNotification">
        <ContentTemplate>
            <div class="infoIcon">
                <img src="images/infoIcon.jpg" alt="info icon" /></div>
            <div class="notificationContent">
                Time remaining:&nbsp; <span id="timeLbl">60</span>
                <telerik:RadButton RenderMode="Lightweight" Skin="Office2007" ID="continueSession" runat="server" Text="Continue Your Session" Width="190px"
                    Style="margin-top: 10px;" AutoPostBack="false" OnClientClicked="ContinueSession">
                </telerik:RadButton>
            </div>
        </ContentTemplate>
    </telerik:RadNotification>

        <script type="text/javascript" src="scripts.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        serverIDs({ notificationID: '<%= RadNotification1.ClientID %>' });
        //]]>
    </script>
    </form>
</body>
</html>
