<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" 
    CodeFile="modulelogin.aspx.vb" Inherits="modulelogin"  %>
 <%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
  

   
    <!-- module page script starts here -->
   <%-- <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>--%>
    <!-- module page script ends here -->
    <link href="/cssfiles/module.css" rel="stylesheet" />

    <!-- Added by Jacob for the global search text box -->
 <%--   <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" />
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>--%>

     <style>
        .RadNotification_Material .rnTitleBar .rnTitleBarTitle 
        {
            font-size: 15px !important;
            padding: 10px 0!important;
        }
        .RadNotification_Material .rnTitleBar .rnIcon:before 
        {
            font-size: 16px !important;
        }
        .RadNotification_Material .rnTitleBar 
        {
            padding: 5px 5px 5px !important; 
        }
        .RadNotification .rnTitleBar .rnIcon 
        {
            display:none !important; 
        }
        .RadWindow_Material .rwCommands {
            margin: -1em 0 0 !important;
            right: 0em !important;
        }
       .RadWindow_Material .rwTitleWrapper .rwTitle {
               padding: 0px 0 9px 0px !important;
    margin: -3.5px 0 -4px !important;
    font-size: 1em !important;
       }
        .RadWindow .rwIcon:before
         {
            display:none !important; 
        }
        .RadWindow .rwIcon {
            display:none !important; 
        }
        
        /*.popup-bg {
    background-color: rgba(0,0,0,0.3);
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 1;
}*/
    </style>

    <script type="text/javascript">
        var tempObj = '';

        //$(document).ready(function () { popup(); });


        function popup() {
            var url = "/common/feedback.aspx";
            var oWnd = radopen(url, "pop_up");
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
        function popUpDetails(url) {

            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {

                    window.open(url);

                }
            }
            catch (e) { }

            return false;
        }
    </script>

     <telerik:radwindowmanager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Shortcuts>
             <telerik:WindowShortcut CommandName="CloseAll" Shortcut="Esc"></telerik:WindowShortcut>
        </Shortcuts>
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" 
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="820px" Height="745px" >
            </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>
<!-- module layout-->
<div class="grid mt-lg-5">
    
   		<div class="row cf mb-lg-4 brd-edu">
               <h3 class="text-center mod-title edu-title">Educational</h3>
			<div class="slide-in four col m-3">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-graduation-cap fa-3x" aria-hidden="true"></i>
						<p>Student Management</p>
					</span>
                    <asp:LinkButton ID="lbStud_Mag" runat="server"  >
					<div class="overlay">
						<i class="fa fa-graduation-cap fa-3x" aria-hidden="true"></i>
                        <p>Student Management</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
			<div class="slide-in four col m-3">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-certificate fa-3x" aria-hidden="true"></i>
						<p>Curriculum Management</p>
					</span>
                    <asp:LinkButton ID="lbCur_mag" runat="server">
					<div class="overlay">						 
                             <i class="fa fa-certificate fa-3x" aria-hidden="true"></i>
                             <p>Curriculum Management</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
			<div class="slide-in four col m-3">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-address-book fa-3x" aria-hidden="true"></i>
						<p>Co-Curricular Activities</p>
					</span>
                    <asp:LinkButton ID="lbCca_mag" runat="server">
                    <div class="overlay">
						<i class="fa fa-address-book fa-3x" aria-hidden="true"></i>
                            <p>Co-Curricular Activities</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-in four col m-3">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-book fa-3x" aria-hidden="true"></i>
						<p>Library Management</p>
					</span>
                    <asp:LinkButton ID="lbLib_mag" runat="server">
					<div class="overlay">
                            <i class="fa fa-book fa-3x" aria-hidden="true"></i>
                            <p>Library Management</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-in four col m-3">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-cogs fa-3x" aria-hidden="true"></i>
						<p>ACe</p>
					</span>
                    <asp:LinkButton ID="lbace_mag" runat="server">
					<div class="overlay">
						    <i class="fa fa-cogs fa-3x" aria-hidden="true"></i>
                            <p>ACe</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-in four col m-3">
				<div class="box brd-edu">
					<span class="original">
						<i class="fa fa-envelope-open fa-3x" aria-hidden="true"></i>
						<p>Messaging</p>
					</span>
                    <asp:LinkButton ID="lbMessage" runat="server">
					<div class="overlay">
                        <i class="fa fa-envelope-open fa-3x" aria-hidden="true"></i>
                        <p>Messaging</p>
					</div>
                    </asp:LinkButton>
                    
				</div>
			</div>

		</div>
    
    <div class="row cf mb-lg-4 brd-fin">
			<h3 class="text-center mod-title fin-title">Financial</h3>
            <div class="slide-down-delay four col m-3">
				<div class="box brd-fin">
					<span class="original">
						<i class="fa fa-money fa-3x" aria-hidden="true"></i>
						<p>Fees Management</p>
					</span>
                    <asp:LinkButton ID="lbFees" runat="server">
					<div class="overlay">
                            <i class="fa fa-money fa-3x" aria-hidden="true"></i>
                            <p>Fees Management</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-down-delay four col m-3">
				<div class="box brd-fin">
					<span class="original">
						<i class="fa fa-usd fa-3x" aria-hidden="true"></i>
						<p>Payroll</p>
					</span>
                    <asp:LinkButton ID="lbPayroll" runat="server" >
					<div class="overlay">
                             <i class="fa fa-usd fa-3x" aria-hidden="true"></i>
                            <p>Payroll</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-down-delay four col m-3">
				<div class="box brd-fin">
					<span class="original">
						<i class="fa fa-bus fa-3x" aria-hidden="true"></i>
						<p>Transport Management</p>
					</span>
                    <asp:LinkButton ID="lbTran_mag" runat="server">
					<div class="overlay">
                            <i class="fa fa-bus fa-3x" aria-hidden="true"></i>
                            <p>Transport Management</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-down-delay four col m-3">
				<div class="box brd-fin">
					<span class="original">
						<i class="fa fa-calculator fa-3x" aria-hidden="true"></i>
						<p>Financial Accounting</p>
					</span>
                    <asp:LinkButton ID="lbFinance_mag" runat="server" >
					<div class="overlay">
                             <i class="fa fa-calculator fa-3x" aria-hidden="true"></i>
                        <p>Financial Accounting</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-down-delay four col m-3">
				<div class="box brd-fin">
					<span class="original">
						<i class="fa fa-credit-card fa-3x" aria-hidden="true"></i>
						<p>Purchase</p>
					</span>
                    <asp:LinkButton ID="lbPur_mag" runat="server" >
					<div class="overlay">
                             <i class="fa fa-credit-card fa-3x" aria-hidden="true"></i>
                            <p>Purchase</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
           <div class="slide-down-delay four col m-3">
				<div class="box brd-fin">
					<span class="original">
						<i class="fa fa-phone-square fa-3x" aria-hidden="true"></i>
						<p>Help Desk</p>
					</span>
                    <asp:LinkButton ID="lnkHelpDesk" runat="server" >
					<div class="overlay">
                             <i class="fa fa-phone-square fa-3x" aria-hidden="true"></i>
                            <p>Help Desk</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
		</div>
    
    <div class="row cf mb-lg-4 brd-adm">
        <h3 class="text-center mod-title adm-title">Administration</h3>
         <div class="slide-up four col m-3">
				<div class="box brd-adm">
					<span class="original">
						<i class="fa fa-desktop fa-3x" aria-hidden="true"></i>
						<p>System Administration</p>
					</span>
                    <asp:LinkButton ID="lbSys_mag" runat="server" >
					<div class="overlay">
                            <i class="fa fa-desktop fa-3x" aria-hidden="true"></i>
                            <p>System Administration</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>    
            <div class="slide-up four col m-3">
				<div class="box brd-adm">
					<span class="original">
						<i class="fa fa-file-o fa-3x" aria-hidden="true"></i>
						<p>Contracts & Licensing Management</p>
					</span>
                    <asp:LinkButton ID="lbDocument" runat="server"> 
					<div class="overlay">
                             <i class="fa fa-file-o fa-3x" aria-hidden="true"></i>
                             <p>Contracts & Licensing Management </p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-up four col m-3">
				<div class="box brd-adm">
					<span class="original">
						<i class="fa fa-user-plus fa-3x" aria-hidden="true"></i>
						<p>HR</p>
					</span>
                    <asp:LinkButton ID="lbHR" runat="server">
					<div class="overlay">
                            <i class="fa fa-user-plus fa-3x" aria-hidden="true"></i>
                        <p>HR</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-up four col m-3">
				<div class="box brd-adm">
					<span class="original">
						<i class="fa fa-users fa-3x" aria-hidden="true"></i>
						<p>Professional Development</p>
					</span>
                    <asp:LinkButton ID="lbPD" runat="server">
					<div class="overlay">
                            <i class="fa fa-users fa-3x" aria-hidden="true"></i>
                        <p>Professional Development</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
             <div class="slide-up four col m-3">
				<div class="box brd-adm">
					<span class="original">
						<i class="fa fa-tasks fa-3x" aria-hidden="true"></i>
						<p>Facility & Service Management</p>
					</span>
                    <asp:LinkButton ID="lbFacility" runat="server">
					<div class="overlay">
                         <i class="fa fa-tasks fa-3x" aria-hidden="true"></i>
                         <p>Facility & Service Management</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
            <div class="slide-up four col m-3">
				<div class="box brd-adm">
					<span class="original">
						<i class="fa fa-calendar fa-3x" aria-hidden="true"></i>
						<p>Employee Self Service</p>
					</span>
                    <asp:LinkButton  ID="lblEmpSelfSrv" runat="server">
					<div class="overlay">
                            <i class="fa fa-calendar fa-3x" aria-hidden="true"></i>
                            <p>Employee Self Service</p>
					</div>
                    </asp:LinkButton>
				</div>
			</div>
			     
            
          
		</div>

   

    </div>


    



        </div>
    <asp:HiddenField ID="hfweek1" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfweek2" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfstartdt" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfenddt" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfYear" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfAttQuick" runat="server"></asp:HiddenField>
</asp:Content>
