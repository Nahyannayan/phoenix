﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encFollowup.aspx.vb" Inherits="Enrollment_encFollowup" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <%--  <link href="css/enroll.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" media="screen" />
    <link href="css/gems.css" rel="stylesheet" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
    <link href="css/Dashboard.css" rel="stylesheet" />
    <link href="css/buttons.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/bootstrap.css" media="screen" />
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        function fancyClose() {
            parent.location.reload(true);
        }
    </script>
    <style type="text/css">
         .RadGrid_Default  {
            width:100% !important;
        }

        #fancybox-content {
            border-color: #00a3e0 !important;
            background-color: #00a3e0 !important;
            width: 100% !important;
            overflow-x: hidden !important;
        }

        #fancybox-inner {
            overflow-x: hidden !important;
        }

        .RadGrid {
            border-radius: 10px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            background-color: #00a3e0;
        }

        .RadGrid_Office2010Blue th.rgSorted {
            background-color: #00a3e0;
        }            
          
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div>
            <asp:UpdatePanel ID="upLogin" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Follow Up Status<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddlCurStatus" runat="server" AutoPostBack="true" Style="width: 100%;"></asp:DropDownList></td>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Status Reason<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="true" Style="width: 100%;"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Mode<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddmode" runat="server" Style="width: 100%;">
                                    <asp:ListItem Text="" Selected="true" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Telephone" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Email" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Alert On<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txtAob" runat="server" Style="width: 100%;"></asp:TextBox>
                                <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/calendar.gif" Visible="false" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtAob"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtAOB" runat="server" PopupButtonID="imgDOB" Format="dd/MMM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Remarks<span style="color:red ;">*</span></label>
                            </td>
                            <td style="width: 30%;" >
                                <asp:TextBox ID="txtremarks" runat="server" MaxLength="300" SkinID="MultiText"
                                    TextMode="MultiLine" Style="width: 100%;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv_txtremarks" runat="server" ControlToValidate="txtremarks"
                                    Display="Dynamic" ErrorMessage="Please enter the Remarks." InitialValue=" "
                                    ValidationGroup="groupM1" EnableClientScript="false" Visible="false">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                <br />
                                <asp:Label ID="lblmessage" runat="server" CssClass="error" Width="100%"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <telerik:RadGrid ID="grdReason" runat="server" 
                        AllowPaging="false" AllowSorting="True"
                        CellSpacing="0" GridLines="None" EnableTheming="False" AutoGenerateColumns="False"
                        ShowGroupPanel="True">
                        <ClientSettings AllowColumnsReorder="True"></ClientSettings>
                        <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true">
                            <Excel Format="ExcelML" />
                        </ExportSettings>
                        <MasterTableView Caption="Status Logs">
                            <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToWordButton="true" ShowExportToExcelButton="true"></CommandItemSettings>
                            <RowIndicatorColumn Visible="False" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column" Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn
                                    HeaderText="Log Date" UniqueName="column" DataField="ENR_LOG_DATE" DataFormatString="{0:dd/MMM/yyyy}">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn
                                    HeaderText="Current Status" UniqueName="column" DataField="EFR_REASON">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Reason" UniqueName="column" DataField="EFD_REASON">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Remarks" UniqueName="column" DataField="ENR_REMARKS">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="User" UniqueName="column" DataField="ENR_USER_ID">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                    </telerik:RadGrid>
                    <br />
                    <telerik:RadGrid ID="grdFollowuphis" runat="server"
                        AllowPaging="false" AllowSorting="True"
                        CellSpacing="0" GridLines="None" EnableTheming="False" AutoGenerateColumns="False"
                        ShowGroupPanel="True">
                        <ClientSettings AllowColumnsReorder="True"></ClientSettings>
                        <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true">
                            <Excel Format="ExcelML" />
                        </ExportSettings>
                        <MasterTableView Caption="Follow Up Logs">
                            <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToWordButton="true" ShowExportToExcelButton="true"></CommandItemSettings>
                            <RowIndicatorColumn Visible="False" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column" Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn
                                    HeaderText="Log Date" UniqueName="column" DataField="FOL_FOLL_UP_DATE" DataFormatString="{0:dd/MMM/yyyy}">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn
                                    HeaderText="Mode" UniqueName="column" DataField="MODE_DESCR">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Remarks" UniqueName="column" DataField="FOL_REMARKS">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn
                                    HeaderText="Alert Date" UniqueName="column" DataField="alertdate">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="User" UniqueName="column" DataField="FOL_USER_ID">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                    </telerik:RadGrid>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
