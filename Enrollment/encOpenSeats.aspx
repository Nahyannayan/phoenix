﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encOpenSeats.aspx.vb" Inherits="Enrollment_encOpenSeats" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    <%-- <link href="css/enroll.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" media="screen" />
    <link href="css/gems.css" rel="stylesheet" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="css/Dashboard.css" rel="stylesheet" />
    <link href="css/buttons.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
</head>
<body >

    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div align="center" style="overflow:auto !important;">
            <asp:HiddenField ID="hdbsu" runat="server" />
            <div style="width: 80%; padding: 10px;">
                <table style="width: 100%;" >
                    <tr>
                        <td style="width: 20%;">
                            <label for="exampleInputEmail1" ><span class="field-label">Region</span><span style="color: #FF3D7F;">*</span></label>
                        </td>
                        <td style="width: 30%;">
                            <asp:DropDownList ID="ddl_region" runat="server"  Style="width: 100%;" AutoPostBack="true"></asp:DropDownList></td>
                        <td style="width: 20%;">
                            <label for="exampleInputEmail1"><span class="field-label">Academic Year</span><span style="color: #FF3D7F;">*</span></label>
                        </td>
                        <td style="width: 30%;">
                            <asp:DropDownList ID="ddl_acyear" runat="server"  Style="width: 100%;" AutoPostBack="true"></asp:DropDownList></td>
                    </tr>
                </table>
            </div>
            <div class="dbcontainerMain">
                <div class="dbULcontainer">
                    <asp:Repeater ID="rptTabDB" runat="server">
                        <HeaderTemplate>
                            <div style="list-style: none; text-align: left; padding: 0px; margin: 0px 8px 0px 0px;">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="dbinactiveCat" id="divTabHolder" runat="server" style="width: 130px;">
                                <asp:HiddenField ID="hfWidthENQ" runat="server" Value='<%# Eval("PER_ENQ_WIDTH")%>' />
                                <asp:HiddenField ID="hfZoneID" runat="server" Value='<%# Eval("BSU_ID")%>' />
                                <asp:LinkButton ID="lbtnZoneID" runat="server" CssClass="dblinkbtnCat" CommandName='<%# Eval("BSU_NAME")%>' CommandArgument='<%# Eval("BSU_ID")%>'> <div  class='<%# Eval("cssLine")%>'"></div>
                 <div style="margin-top:2px;margin-left:4px;"><%# Eval("BSU_NAME")%></div>
                
                 <!--Enrolled  -->
                 <div class='<%# Eval("StuTotCountCSS")%>' ><%# Eval("ENQS")%></div>                                    
                                </asp:LinkButton>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>

                <div class="dbStudDetailcontainer" style="margin-left: 140px; margin-top: 0px;">
                    <div id="buttonContainer" style="float: left;">
                        <asp:Repeater ID="rptRegion" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfdefRegion" runat="server" Value='<%# Eval("PER_ENQ_WIDTH")%>' />
                                <asp:HiddenField ID="hfBSU" runat="server" Value='<%# Eval("BSU_NAME")%>' />
                                <asp:LinkButton ID="lnkregion" runat="server" CommandName='<%# Eval("BSU_NAME")%>' CommandArgument='<%# Eval("BSU_ID")%>' CssClass='<%# Eval("REG_CountCSS")%>'>
                   <%# Eval("BSU_NAME")%><br />
                   <span class='<%# Eval("ENQ_CountCSS")%>'><%# Eval("ENQS")%></span>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div>
                        <telerik:RadGrid ID="gv_seats" runat="server"
                            AllowPaging="false" AllowSorting="True"
                            CellSpacing="0" GridLines="None" EnableTheming="False" AutoGenerateColumns="False" 
                            ShowGroupPanel="True"
                            OnItemCommand="gv_seats_ItemCommand">
                            <ClientSettings AllowColumnsReorder="True"></ClientSettings>
                            <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true">
                                <Excel Format="ExcelML" />
                            </ExportSettings>
                            <MasterTableView CommandItemDisplay="Top">
                                <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToWordButton="true" ShowExportToExcelButton="true"></CommandItemSettings>
                                <RowIndicatorColumn Visible="False" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column" Created="True">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn
                                        HeaderText="SlNo" UniqueName="column" DataField="slno">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="Short Code" UniqueName="column" DataField="BSU_SHORTNAME">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn
                                        HeaderText="School Name" UniqueName="column" DataField="BSU_NAME">
                                        <ColumnValidationSettings>
                                            <ModelErrorMessage Text="" />
                                        </ColumnValidationSettings>
                                        <HeaderStyle Font-Bold="True" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Seats" UniqueName="TemplateColumn" Groupable="False"
                                        AllowFiltering="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSeats" runat="server" Style="color: #800000; font-weight: bold;" CommandName='<%# Eval("slno")%>' CommandArgument='<%# Eval("BSU_ID")%>'><%#Eval("ENQS")%></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Font-Bold="True" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                        </telerik:RadGrid>
                    </div>
                </div>
                <div class="dbURcontainer" style="float: right; margin-right: 10px; margin-top: 100px;">
                    <telerik:RadGrid ID="gv_Grade" runat="server"
                        AllowPaging="false" AllowSorting="True"
                        CellSpacing="0" GridLines="None" EnableTheming="False" AutoGenerateColumns="False" 
                        ShowGroupPanel="false">
                        <MasterTableView>
                            <Columns>
                                <telerik:GridBoundColumn
                                    HeaderText="Grade" UniqueName="column" DataField="GRM_DISPLAY">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Seats" UniqueName="column" DataField="cnt">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>

                    </telerik:RadGrid>
                </div>

            </div>
        </div>
    </form>
    <%--<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>--%>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
