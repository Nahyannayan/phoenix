﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports Telerik.Web.UI
Imports System.Globalization
Partial Class Enrollment_encChangeStatus
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                bind_enq_status()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Sub bind_enq_status()
        ddlEnqStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT SM_ID ,SM_DESCR  FROM [ENC].[STATUS_M] WHERE SM_BVISIBLE=1 "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlEnqStatus.DataSource = ds
        ddlEnqStatus.DataTextField = "SM_DESCR"
        ddlEnqStatus.DataValueField = "SM_ID"
        ddlEnqStatus.DataBind()
        ddlEnqStatus.Items.Add(New ListItem("--Select--", "0"))
        ddlEnqStatus.Items.FindByText("--Select--").Selected = True
    End Sub
    Private Sub change_status()
        Dim ReturnFlag As Integer
        Dim con As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim pParms(5) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@EQM_IDS", Session("eqmids"))
            pParms(1) = New SqlClient.SqlParameter("@ESM_ID", ddlEnqStatus.SelectedItem.Value)
            pParms(2) = New SqlClient.SqlParameter("@USER", Session("sUsr_name"))
            pParms(3) = New SqlClient.SqlParameter("@REMARKS", txtremarks.Text)


            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SAVE_ENQUIRY_STATUS", pParms)

            ReturnFlag = pParms(4).Value
        Catch ex As Exception
            lblmessage.Text = ex.Message
            lblmessage.Focus()

            ReturnFlag = 1


        Finally
            If ReturnFlag = 0 Then
                sqltran.Commit()
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
            Else
                sqltran.Rollback()
            End If
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Panel_Close.Visible = True
        divMsg.Visible = True
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
    End Sub

    Protected Sub btncan_Click(sender As Object, e As EventArgs) Handles btncan.Click
        Panel_Close.Visible = False
        divMsg.Visible = False
    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Panel_Close.Visible = False
        divMsg.Visible = False
        change_status()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)

    End Sub

    'Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    Panel_Close.Visible = False
    '    divMsg.Visible = False
    'End Sub
End Class
