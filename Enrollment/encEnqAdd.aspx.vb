﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports Telerik.Web.UI
Partial Class Enrollment_encEnqAdd
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then

                If Session("sUsr_name") & "" = "" Then
                    Response.Redirect("~/login.aspx")
                End If
                Bind_school()
                bind_cur_schoool()
                bind_accyear()
                bind_grade()
                bind_year()
                getQuestions(1, chkQ1)
                visible_others()
                bind_source()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Bind_school()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_SCHOOLS", PARAM)
            ddl_school.DataSource = dsDetails
            ddl_school.DataTextField = "BSU_NAME"
            ddl_school.DataValueField = "BSU_ID"
            ddl_school.DataBind()
            If (Session("sbsuid") <> "500606") Then
                ddl_school.Items.FindItemByValue(Session("sBsuid")).Selected = True
                ddl_school.Enabled = False
            Else
                ddl_school.CheckBoxes = True
                '   ddl_school.Items.FindItemByText("--All--").Selected = True
            End If
            'ddl_school.Items.Add(New radlisti("--Select--", "0"))
            'ddl_school.Items.FindByText("--Select--").Selected = True

            'rdcombo.DataSource = dsDetails
            'rdcombo.DataTextField = "BSU_NAME"
            'rdcombo.DataValueField = "BSU_ID"
            'rdcombo.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_cur_schoool()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@INFO_TYPE", "CURR_SCHOOL")
            PARAM(1) = New SqlClient.SqlParameter("@ACCESS_BY", "0")
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GetEnq_School_list", PARAM)
            ddl_Curschool.DataSource = dsDetails
            ddl_Curschool.DataTextField = "BSU_NAME"
            ddl_Curschool.DataValueField = "BSU_ID"
            ddl_Curschool.DataBind()
            ddl_Curschool.Items.Add(New ListItem("--Select--", "0"))
            ddl_Curschool.Items.Add(New ListItem("Other", ""))
            ddl_Curschool.Items.FindByText("--Select--").Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_accyear()
        Try            


            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            If (Session("sbsuid") = "500606") Then
                PARAM(0) = New SqlParameter("@Bsu_id", ddl_school.SelectedValue)
            Else
                PARAM(0) = New SqlParameter("@Bsu_id", Session("Sbsuid"))
            End If
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_ACCYEAR", PARAM)
            ddl_academicyear.DataSource = dsDetails
            ddl_academicyear.DataTextField = "ACY_DESCR"
            ddl_academicyear.DataValueField = "ACY_ID"
            ddl_academicyear.DataBind()
            For Each rowACD As DataRow In dsDetails.Tables(0).Rows
                If rowACD("ACD_CURRENT") Then
                    ddl_academicyear.Items.FindByValue(rowACD("ACY_ID")).Selected = True
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_grade()
        Try
            'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim PARAM(2) As SqlParameter
            'Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_GRADE", PARAM)
            'ddl_grade.DataSource = dsDetails
            'ddl_grade.DataTextField = "GRD_DISPLAY"
            'ddl_grade.DataValueField = "GRD_ID"
            'ddl_grade.DataBind()
            'ddl_grade.Items.Add(New ListItem("--Select--", "0"))
            'ddl_grade.Items.FindByText("--Select--").Selected = True
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            If (Session("sbsuid") = "500606") Then
                Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddl_school.SelectedValue, SqlDbType.VarChar)
            Else
                Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            End If
            Param(1) = Mainclass.CreateSqlParameter("@ACY_ID", ddl_academicyear.SelectedValue, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ONLY_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                ddl_grade.DataSource = dsData
                ddl_grade.DataTextField = "GRM_GRD_ID"
                ddl_grade.DataValueField = "GRM_GRD_ID"
                ddl_grade.DataBind()
                ddl_grade.Items.Add(New ListItem("--Select--", "0"))
                ddl_grade.Items.FindByText("--Select--").Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_year()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ENC].[Get_Year_List]", PARAM)
            ddlYear.DataSource = dsDetails
            ddlYear.DataTextField = "year"
            ddlYear.DataValueField = "year"
            ddlYear.DataBind()
            ddlYear.Items.Add(New ListItem("YYYY", "0"))
            ddlYear.Items.FindByText("YYYY").Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Clear_all()
        txt_ParentFName.Text = ""
        txt_ParentLName.Text = ""
        txt_Email.Text = ""
        txt_MobArea.Text = ""
        txt_MobCountry.Text = ""
        txtMobNumber.Text = ""

        txt_StuFirstName.Text = ""
        txt_StuLastName.Text = ""
        txt_addNote.Text = ""
        rbl_Gender.ClearSelection()

        ddl_academicyear.ClearSelection()
        ddl_grade.ClearSelection()
        ddl_Curschool.ClearSelection()
        txtOther.Text = ""
        clear_qus()

        ddl_school.ClearCheckedItems()
        ddl_Curschool.SelectedValue = "0"
        ddl_grade.SelectedValue = "0"

        ddlDay.SelectedValue = "0"
        ddlMonth.SelectedValue = "0"
        ddlYear.SelectedValue = "0"
        ddlSource.SelectedValue = "0"

        txt_ParentFName.Enabled = True
        txt_ParentLName.Enabled = True
        txt_Email.Enabled = True
        txt_MobArea.Enabled = True
        txt_MobCountry.Enabled = True
        txtMobNumber.Enabled = True
        visible_others()
    End Sub
    Sub clear_qus()
        Dim i As Integer
        For i = 0 To chkQ1.Items.Count - 1
            If chkQ1.Items(i).Selected Then
                chkQ1.Items(i).Selected = False
            End If
        Next
    End Sub
    Private Sub Clear_all_sib()
        txt_ParentFName.Enabled = False
        txt_ParentLName.Enabled = False
        txt_Email.Enabled = False
        txt_MobArea.Enabled = False
        txt_MobCountry.Enabled = False
        txtMobNumber.Enabled = False

        txt_StuFirstName.Text = ""
        txt_StuLastName.Text = ""
        txt_addNote.Text = ""
        rbl_Gender.ClearSelection()

        ddl_academicyear.ClearSelection()
        ddl_grade.ClearSelection()
        ddl_Curschool.ClearSelection()
        txtOther.Text = ""

        ddl_school.ClearCheckedItems()
        ddl_Curschool.SelectedValue = "0"
        ddl_grade.SelectedValue = "0"

        ddlDay.SelectedValue = "0"
        ddlMonth.SelectedValue = "0"
        ddlYear.SelectedValue = "0"

        visible_others()
    End Sub
    Private Function check_school_sel() As Integer
        Dim i As Integer, cnt As Integer
        For i = 0 To ddl_school.Items.Count - 1
            If ddl_school.Items(i).Checked Then
                cnt = cnt + 1
            End If
        Next
        Return cnt
    End Function
    Private Function school_sel() As String
        Dim i As Integer, str As String = ""
        For i = 0 To ddl_school.Items.Count - 1
            If ddl_school.Items(i).Checked Then
                If str = "" Then
                    str = ddl_school.Items(i).Value
                Else
                    str = str + "|" + ddl_school.Items(i).Value
                End If

            End If
        Next
        If check_school_sel() = 0 Then
            str = "500606"
        End If
        Return str
    End Function
    Private Function SaveStudENQUIRY_M(ByRef errmsg As String, ByRef cn As Integer) As Integer
        Dim err As Integer = 0

        Dim EQM_ENQID As String = String.Empty

        Dim con As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            'ViewState("EQM_ENQID") = ""
            ' ViewState("EEQM_SIBLING_ENQID") = IIf(ViewState("EEQM_SIBLING_ENQID") = "", "", ViewState("EEQM_SIBLING_ENQID"))
            Dim pParms(30) As SqlClient.SqlParameter



            pParms(0) = New SqlClient.SqlParameter("@EQM_ENQDATE", Convert.ToString(DateTime.Now))   'DateTime.Now)
            pParms(1) = New SqlClient.SqlParameter("@EQM_APPLFIRSTNAME", txt_StuFirstName.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", txt_StuLastName.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@EQM_APPLDOB", ddlYear.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlDay.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@EQM_APPLGENDER", rbl_Gender.SelectedValue)

            pParms(5) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", ddl_RelationToStudent.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@EQM_ENQTYPE", "O")
            pParms(7) = New SqlClient.SqlParameter("@EQM_STATUS", "NEW")
            pParms(8) = New SqlClient.SqlParameter("@EQM_FILLED_BY", System.DBNull.Value)
            pParms(9) = New SqlClient.SqlParameter("@EQM_SIBLING_ENQID", "")
            pParms(10) = New SqlClient.SqlParameter("@EQM_EMGCONTACT", txt_MobCountry.Text.Trim() + txt_MobArea.Text.Trim() + txtMobNumber.Text.Trim())

            pParms(11) = New SqlClient.SqlParameter("@TEMP_EQM_ENQID_STR", SqlDbType.VarChar, 100)
            pParms(11).Direction = ParameterDirection.Output

            pParms(12) = New SqlClient.SqlParameter("@EQM_APPLNATIONALITY", 0)
            pParms(13) = New SqlClient.SqlParameter("@EQM_APPL_CONTACTME", "YES")

            pParms(14) = New SqlClient.SqlParameter("@EQM_CNT", cn)
            pParms(15) = New SqlClient.SqlParameter("@EQM_ENC_AGENT", Session("sUsr_name"))

            pParms(16) = New SqlClient.SqlParameter("@EQM_ENC_NOTES", txt_addNote.Text)
            pParms(17) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_BSU_ID", ddl_Curschool.SelectedValue)
            pParms(18) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL", txtOther.Text)
            pParms(19) = New SqlClient.SqlParameter("@EQM_ENC_SOURCE", ddlSource.SelectedValue)

            pParms(20) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(20).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SaveStudENQUIRY_M", pParms)
            Dim ReturnFlag As Integer = pParms(20).Value
            If ReturnFlag = 0 Then
                EQM_ENQID = pParms(11).Value
                ViewState("EQM_ENQID") = EQM_ENQID

                If (EQM_ENQID <> "") Then
                    ReturnFlag = SaveStudENQUIRY_PARENT_M(sqltran, EQM_ENQID)
                    If (ReturnFlag = 0) Then
                        ReturnFlag = SaveStudENQUIRY_SCHOOLPRIO_S(sqltran, EQM_ENQID)
                        If (ReturnFlag = 0) Then
                            ViewState("EEQM_SIBLING_ENQID") = IIf(ViewState("EEQM_SIBLING_ENQID") = "", EQM_ENQID, ViewState("EEQM_SIBLING_ENQID"))

                        End If
                        ' Dim dtStudentEnq_Details As DataTable = ViewState("StudentEnq_Details")
                        If (ReturnFlag <> 0) Then
                            errmsg = "Duplicate enquiry of student" + txt_StuFirstName.Text.Trim() + " " + txt_StuLastName.Text.Trim() + " in " + ddl_grade.SelectedItem.Text + " found.Please contact school."
                            'Throw New System.Exception("An exception has occurred.")

                            err = 1
                        End If
                    Else
                        ' Throw New System.Exception("An exception has occurred.")
                        err = 1
                    End If
                End If



                ViewState("RecordAdded") = "True"

                lblErrorlog.Focus()
                ' divbox_savemsg.Visible = True
            Else
                Throw New System.Exception("An exception has occurred.")
            End If

        Catch ex As Exception

            lblErrorlog.Text = ex.Message
            lblErrorlog.Focus()



            err = 1
        Finally
            If err = 0 Then
                sqltran.Commit()
            Else
                sqltran.Rollback()
            End If

        End Try

        Return err
        ViewState("EQM_ENQID_COPY") = ViewState("EQM_ENQID")
        'ViewState("EQM_ENQID") = ""
    End Function
    Private Function SaveStudENQUIRY_PARENT_M(ByVal sqltran As SqlTransaction, ByVal EQM_ENQID As String) As Integer

        Dim pParms(15) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@EQP_EQM_ENQID", EQM_ENQID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            If (ddl_RelationToStudent.SelectedValue = "F") Then

                pParms(2) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", txt_ParentFName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EQP_FLASTNAME", txt_ParentLName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@EQP_FMOBILE", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@EQP_FEMAIL", txt_Email.Text.Trim())
            ElseIf (ddl_RelationToStudent.SelectedValue = "M") Then
                pParms(2) = New SqlClient.SqlParameter("@EQP_MFIRSTNAME", txt_ParentFName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EQP_MLASTNAME", txt_ParentLName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@EQP_MMOBILE", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@EQP_MEMAIL", txt_Email.Text.Trim())
            ElseIf (ddl_RelationToStudent.SelectedValue = "G") Then
                pParms(2) = New SqlClient.SqlParameter("@EQP_GFIRSTNAME", txt_ParentFName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EQP_GLASTNAME", txt_ParentLName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@EQP_GMOBILE", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@EQP_GEMAIL", txt_Email.Text.Trim())
            End If

            pParms(6) = New SqlClient.SqlParameter("@EQP_bSCHOOLTOUR", 0)

            Dim ur As String = HttpContext.Current.Request.Url.AbsoluteUri

            pParms(7) = New SqlClient.SqlParameter("@EQP_URL", ur)

            Dim EQP_QUS1 As String = ""

            For Each item As ListItem In chkQ1.Items
                If (item.Selected) Then

                    EQP_QUS1 = EQP_QUS1 & item.Value
                    EQP_QUS1 = EQP_QUS1 & "|"
                End If
            Next
            pParms(8) = New SqlClient.SqlParameter("@EQP_QUS1", EQP_QUS1)
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SaveStudENQUIRY_PARENT_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        Catch ex As Exception

        End Try

    End Function

    Private Function SaveStudENQUIRY_SCHOOLPRIO_S(ByVal sqltran As SqlTransaction, ByVal EQM_ENQID As String) As Integer
        Dim pParms(15) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQM_ENQID)
        pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@EQS_ACD_ID", ddl_academicyear.SelectedValue)
        'pParms(3) = New SqlClient.SqlParameter("@EQS_BSU_ID", Convert.ToString(school_sel()))
        If (Session("sbsuid") <> "500606") Then
            pParms(3) = New SqlClient.SqlParameter("@EQS_BSU_ID", ddl_school.SelectedValue)
        Else
            pParms(3) = New SqlClient.SqlParameter("@EQS_BSU_ID", Convert.ToString(school_sel()))
        End If
        pParms(4) = New SqlClient.SqlParameter("@EQS_GRM_ID", ddl_grade.SelectedValue)
        pParms(5) = New SqlClient.SqlParameter("@EQS_CLM_ID", 1)
        'pParms(6) = New SqlClient.SqlParameter("@EQS_REMARKS", txt_Remarks.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@TEMP_ApplNo", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.Output
        pParms(7) = New SqlClient.SqlParameter("@EQS_SIB_ENQID", 0)
        'pParms(9) = New SqlClient.SqlParameter("@EQS_DOJ", txt_DOJ.Text)
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SaveStudENQUIRY_SCHOOLPRIO_S", pParms)


        Dim ReturnFlag As Integer = pParms(1).Value

        If ReturnFlag = 0 Then

            ViewState("EQS_ID") = pParms(6).Value
        End If

        Return ReturnFlag

    End Function
    Sub getQuestions(ByVal id As Integer, ByVal chk As CheckBoxList)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select EQD_ID,EQD_ANS  from ONLINE_ENQ.ENQUIRY_QUESTION_M " _
                                & " inner join ONLINE_ENQ.ENQUIRY_QUESTION_D on EQU_ID=EQD_EQU_ID " _
                                & " where EQU_ID=" & id & " and EQD_BSU_ID='500606'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


        chk.DataSource = ds
        chk.DataTextField = "EQD_ANS"
        chk.DataValueField = "EQD_ID"
        chk.DataBind()



    End Sub
    Sub bind_source()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT SM_ID,SM_DESCR FROM ENC.SOURCE_M "
                              
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


        ddlSource.DataSource = ds
        ddlSource.DataTextField = "SM_DESCR"
        ddlSource.DataValueField = "SM_ID"
        ddlSource.DataBind()
        ddlSource.Items.Add(New ListItem("--Select--", "0"))
        ddlSource.Items.FindByText("--Select--").Selected = True
    End Sub
    Sub visible_others()
        If ddl_Curschool.SelectedItem.Text = "Other" Then
            trOther.Visible = True
        Else
            trOther.Visible = False
        End If
    End Sub
    Protected Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click



        If Page.IsValid = True Then

            Dim errmsg As String = String.Empty
            Dim CNT As Integer = 0
            Try
                Dim NoDataEntered As Boolean
                If ((ViewState("RecordAdded") = "False") Or (ViewState("RecordAdded") Is Nothing)) Then
                    RequiredFieldValidator6.Validate()
                    RequiredFieldValidator3.Validate()
                    If Page.IsValid = False Then Exit Sub
                End If

                If txt_StuFirstName.Text <> "" Then
                    If rbl_Gender.SelectedIndex = -1 Then
                        NoDataEntered = True
                        RequiredFieldValidator3.Validate()
                    End If

                    If ddlDay.SelectedValue = "0" Then
                        rfvDay.Visible = True
                        rfvDay.Validate()
                        NoDataEntered = True
                        Exit Sub
                    End If
                    If ddlMonth.SelectedValue = "0" Then
                        rfMonth.Visible = True
                        rfMonth.Validate()
                        NoDataEntered = True
                        Exit Sub
                    End If
                    If ddlYear.SelectedValue = "0" Then
                        rfYear.Visible = True
                        rfYear.Validate()
                        NoDataEntered = True
                        Exit Sub
                    End If
                    CNT = check_school_sel()

                    'If CNT = 0 Then
                    '    divNote.Visible = True
                    '    divNote.Attributes("class") = "msgInfoBox msgInfoError"
                    '    lblErrorlog.Text = "Please select 1 school . Please Check"
                    '    Exit Sub
                    '    NoDataEntered = True
                    'End If
                    If CNT > 3 Then
                        divNote.Visible = True
                        divNote.Attributes("class") = "msgInfoBox msgInfoError"
                        lblErrorlog.Text = "Dont select more than 3 schools . Please Check"
                        Exit Sub
                        NoDataEntered = True
                    End If

                Else
                    NoDataEntered = True

                    RequiredFieldValidator3.Enabled = False
                    RequiredFieldValidator3.Visible = False

                    rfvDay.Enabled = False
                    rfvDay.Visible = False
                    rfMonth.Visible = False
                    rfMonth.Enabled = False
                    rfYear.Visible = False
                    rfYear.Enabled = False
                End If

                If NoDataEntered = False Then
                    If SaveStudENQUIRY_M(errmsg, CNT) = 0 Then
                        errmsg = "Record Added Successfully."
                        Panel_Close.Visible = True
                        divMsg.Visible = True
                    Else
                        If errmsg <> "" Then
                            lblErrorlog.Text = errmsg
                        Else
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoError"
                            lblErrorlog.Text = "Error occured.Please contact school"
                        End If
                        Exit Sub
                    End If
                End If
            Catch ex As Exception

            End Try
        

        End If
    End Sub

    Protected Sub btnMsg_Click(sender As Object, e As EventArgs) Handles btnMsg.Click
        divNote.Visible = False
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        divMsg.Visible = False
        Panel_Close.Visible = False
        Clear_all()
        ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        divMsg.Visible = False
        Panel_Close.Visible = False
    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        divMsg.Visible = False
        Panel_Close.Visible = False
        Clear_all_sib()
    End Sub

    Protected Sub btnCancel_1_Click(sender As Object, e As EventArgs) Handles btnCancel_1.Click
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
    End Sub

    Protected Sub ddl_Curschool_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Curschool.SelectedIndexChanged
        visible_others()
    End Sub
End Class
