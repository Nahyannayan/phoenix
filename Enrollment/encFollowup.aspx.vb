﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports Telerik.Web.UI
Imports System.Globalization
Partial Class Enrollment_encFollowup
    Inherits System.Web.UI.Page
    Dim applno As String = "", parentname As String = "", parentemail As String = "", parentmob As String = "", acyear As String = "", grade As String = "", eqm_id As Long, bsu As String = ""


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                DISPLAY_ITEMS(Request.QueryString("id").Replace(" ", "+"))
                bind_cur_status()
                bind_reason()
                BindGrid()
                bind_gridReason()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

    Sub bind_cur_status()
        ddlCurStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurStatus.DataSource = ds
        ddlCurStatus.DataTextField = "EFR_REASON"
        ddlCurStatus.DataValueField = "EFR_ID"
        ddlCurStatus.DataBind()
        ddlCurStatus.Items.Add(New ListItem("--", "0"))
        ddlCurStatus.Items.FindByText("--").Selected = True
    End Sub
    Sub bind_reason()
        ddlReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFD_ID,EFD_REASON  FROM ENQ_FOLLOW_UP_REASON_D where EFD_EFR_ID=" & ddlCurStatus.SelectedValue



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "EFD_REASON"
        ddlReason.DataValueField = "EFD_ID"
        ddlReason.DataBind()

        If (Not ddlReason.Items Is Nothing) AndAlso (ddlReason.Items.Count = 0) Then
            ddlReason.Items.Add(New ListItem("--", "0"))
            ddlReason.Items.FindByText("--").Selected = True
        End If

    End Sub
    Sub bind_gridReason()
        Dim Encr_decrData As New Encryption64
        Dim id = Request.QueryString("id").Replace(" ", "+")
        Dim bsu_id = bsu.ToString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT ENR_LOG_DATE,EFR_REASON,EFD_REASON,ENR_REMARKS,ENR_USER_ID  FROM ENQUIRY_FOLLOW_UP_REASON " _
                        & " INNER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=ENR_EFR_ID " _
                        & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_D ON EFd_ID=ENR_EFD_ID " _
                        & " WHERE ENR_EQS_ID =" & id

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grdReason.DataSource = ds
        grdReason.DataBind()
    End Sub
    Public Sub BindGrid()
        Dim Encr_decrData As New Encryption64
        Dim id = Request.QueryString("id").Replace(" ", "+")
        Dim bsu_id = bsu.ToString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select *,(case fol_mode when '1' then 'Telephone' else 'Email' end) as mode_descr ,replace(CONVERT(varchar,ALERT_DATE,106),' ','/') as alertdate from ENQUIRY_FOLLOW_UP " & _
                        " where fol_eqs_id='" & id & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grdFollowuphis.DataSource = ds
        grdFollowuphis.DataBind()
    End Sub
    Private Sub SAVE_REASON(ByVal EQSID As Integer, ByVal ENQID As Integer)
        Dim Status As Integer
        Dim param(8) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@ENR_BSU_ID", bsu)
                param(1) = New SqlParameter("@ENR_EQS_ID", EQSID)
                param(2) = New SqlClient.SqlParameter("@ENR_ENQ_ID", ENQID)
                param(3) = New SqlClient.SqlParameter("@ENR_EFR_ID", ddlCurStatus.SelectedValue)
                param(4) = New SqlClient.SqlParameter("@ENR_EFD_ID", ddlReason.SelectedValue)
                param(5) = New SqlClient.SqlParameter("@ENR_USER_ID", Session("sUsr_name"))
                param(6) = New SqlClient.SqlParameter("@ENR_REMARKS", txtremarks.Text)

                param(7) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(7).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENC.SAVE_ENQ_FOLLOW_UP_REASON", param)
                Status = param(7).Value





            Catch ex As Exception

                lblmessage.Text = "Record could not be updated"
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog(lblmessage.Text)
                    transaction.Rollback()
                Else
                    lblmessage.Text = ""
                    transaction.Commit()
                    bind_gridReason()
                End If
            End Try

        End Using
    End Sub
    Private Sub DISPLAY_ITEMS(ByVal id As Long)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@EQS_ID", id)


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.getEnquiry_byEQS_ID", PARAM)


            If dsDetails.Tables(0).Rows.Count >= 1 Then



                applno = dsDetails.Tables(0).Rows(0).Item("EQS_APPLNO")
                parentname = dsDetails.Tables(0).Rows(0).Item("parent_fname") + " " + dsDetails.Tables(0).Rows(0).Item("parent_lname")
                parentemail = dsDetails.Tables(0).Rows(0).Item("parent_email")
                parentmob = dsDetails.Tables(0).Rows(0).Item("parent_mobno")
                acyear = dsDetails.Tables(0).Rows(0).Item("ACY_DESCR")
                grade = dsDetails.Tables(0).Rows(0).Item("EQS_GRD_ID")
                eqm_id = dsDetails.Tables(0).Rows(0).Item("EQM_ENQID")
                bsu = dsDetails.Tables(0).Rows(0).Item("EQS_BSU_ID")
            End If



        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlCurStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCurStatus.SelectedIndexChanged
        bind_reason()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If ddmode.SelectedValue <> "0" Then
                rfv_txtremarks.Visible = True
                rfv_txtremarks.Validate()
            Else
                rfv_txtremarks.Visible = False
            End If

            If txtAob.Text.Trim <> "" Then
                Dim strfDate As String = txtAob.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    lblmessage.Text = "Please check the alert date"
                    Exit Sub
                End If
            End If
            DISPLAY_ITEMS(Request.QueryString("id").Replace(" ", "+"))
            Dim Encr_decrData As New Encryption64()
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim bsu_id = bsu ''"125016" ''
            Dim eqs_id = Request.QueryString("id").Replace(" ", "+")
            Dim enq_id = eqm_id
            Dim userid = Session("sUsr_name") ''"admin" ''

            SAVE_REASON(eqs_id, enq_id)
            If ddmode.SelectedValue <> "0" Then
                Dim pParms(17) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@bsu_id", bsu_id)
                pParms(1) = New SqlClient.SqlParameter("@eqs_id", eqs_id)
                pParms(2) = New SqlClient.SqlParameter("@enq_id", enq_id)
                pParms(3) = New SqlClient.SqlParameter("@user_id", userid)
                pParms(4) = New SqlClient.SqlParameter("@mode", Convert.ToInt16(ddmode.SelectedItem.Value))
                pParms(5) = New SqlClient.SqlParameter("@remarks", txtremarks.Text)
                pParms(6) = New SqlClient.SqlParameter("@alertdays", "1")
                pParms(11) = New SqlClient.SqlParameter("@ALERT_DATE", txtAob.Text)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ENC.stu_enq_insert_followup", pParms)

                BindGrid()


                'If txtAob.Text.Trim <> "" Then
                '    Dim lstrEMPNAME
                '    Dim lstrEMPEMAIL
                '    Dim pParmss(3) As SqlClient.SqlParameter
                '    pParmss(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                '    pParmss(1) = New SqlClient.SqlParameter("@EQS_ID", eqs_id)

                '    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetEMP_Name_Email", pParmss)
                '        While reader.Read
                '            lstrEMPNAME = Convert.ToString(reader("EMP_FNAME"))
                '            lstrEMPEMAIL = Convert.ToString(reader("EMD_EMAIL"))
                '        End While
                '    End Using

                '    Dim MSG As String, SUBJECT As String
                '    SUBJECT = "OASIS ALERT - Enquiry Followup : " + applno
                '    MSG = "<table width=100% border=0 style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>              <tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Enquiry Followup Alert : " + applno + "</td></tr> <tr><td>Please accept this email as confirmation for your enquiry follow up with details given below : <br></td></tr>  <tr><td> Enquiry Number: " + applno + " <br></td></tr> <tr><td> Academic Year: " + acyear + " <br></td></tr> <tr><td> Grade: " + grade + " <br></td></tr>     <tr><td> Parent Name: " + parentname + " <br></td></tr>   <tr><td> Mobile: " + parentmob + " <br></td></tr>    <tr><td> Email: " + parentemail + " <br></td></tr>   <tr><td> Remarks:<br> " + txtremarks.Text + " </td></tr>          </table>                      <table width=100% border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>    <tr><td><br><br>This is an Automated mail from GEMS OASIS. </td></tr>              <tr></tr><tr></tr>            </table>    "
                '    Dim SendAppt As New AppointmentBLL(txtAob.Text + " 08:00AM", txtAob.Text + " 08:00AM", SUBJECT, MSG, "School", lstrEMPNAME, lstrEMPEMAIL, SUBJECT, "communication@gemsedu.com")
                '    SendAppt.EmailAppointment()
                'End If
            End If
            ''lblmessage.Text = "Saved"
            '  Response.Redirect("~/Students/StuFollowUp.aspx?MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")
            ' Response.Redirect("~/Enrollment/encDashboard.aspx")
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
        Catch ex As Exception
            lblmessage.Text = "Error"
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
    End Sub
End Class
