<%@ Page Language="VB" AutoEventWireup="true" CodeFile="encDashboardTest.aspx.vb"  Inherits="Enrollment_encDashboardTest" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>
<html>
<!--<![endif]-->
<head runat="server">
    <title>GEMS Enrollment Center</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    
    <link href="css/buttons.css" rel="stylesheet" />

    <%--    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>--%>

    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

    <script type="text/javascript">
        function showHide() {
            $header = $(".header");
            $content = $(".FindJob");
            $ColExpId = $("#ColExpCss");
            $content.slideToggle(500, function () {
                if ($content.is(":visible") == true) {
                    $ColExpId.removeClass("colClass").addClass("ExpClass");
                }
                else {
                    $ColExpId.removeClass("ExpClass").addClass("colClass");
                }
            });
        }
        var tempObj = '';
        function popUpDetails(url) {
            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {
                    window.open(url);
                }
            }
            catch (e) { }

            return false;
        }

        

        function showSeats() {



            $.fancybox({
                type: 'iframe',
                href: 'encOpenSeats.aspx',

                fitToView: false,
                width: '90%',
                height: '100%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'


            });
        }

        function showSchooltour() {



            $.fancybox({
                type: 'iframe',
                href: 'encSchoolTour.aspx',

                fitToView: false,
                width: '50%',
                height: '50%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'


            });
        }
       
        function showJournel(val) {



            $.fancybox({
                type: 'iframe',
                href: 'encJournel.aspx?id=' + val,

                fitToView: false,
                width: '50%',
                height: '75%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        }
       
        function fancyClose() {

            parent.$.fancybox.close();

        }
       

    </script>



    <style type="text/css">
        #fancybox-content {
            border-color: #00a3e0 !important;
            background-color: #00a3e0 !important;
            width: 100% !important;
            overflow-x: hidden !important;
        }

        #fancybox-inner {
            overflow-x: hidden !important;
        }
        .RadGrid
{
   border-radius: 10px;
   overflow: hidden;
}
          .RadGrid_Office2010Blue .rgCommandRow table

    {

        background-color: #00a3e0;

    }
            .RadGrid_Office2010Blue th.rgSorted

    { 

       

        background-color: #00a3e0; 

    } 
           
    .MyCalendar .ajax__calendar_container {
        
    border:1px solid #646464;
    background-color: #00a3e0;
    color: white;
    border-radius: 10px;
  
   

}  
         
    </style>


    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0 )
                     {
                args.set_enableAjax(false);
            }
        }

        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>


</head>
<body>
      <script type="text/javascript">

          function showChangeStatus() {



              $.fancybox({
                  type: 'iframe',
                  href: 'encChangeStatus.aspx',

                  fitToView: false,
                  width: '50%',
                  height: '50%',
                  autoSize: false,
                  closeClick: false,
                  openEffect: 'none',
                  closeEffect: 'none',
                  'onClosed': function () {
                      parent.$.fancybox.close();

                      document.getElementById('<%= CausePostBack.ClientID%>').click();

                  }


              });
          }

          function showFollowup(val) {



              $.fancybox({
                  type: 'iframe',
                  href: 'encFollowup.aspx?ID=' + val,

                  fitToView: false,
                  width: '60%',
                  height: '95%',
                  autoSize: false,
                  closeClick: false,
                  openEffect: 'none',
                  closeEffect: 'none',
                  'onClosed': function () {
                      parent.$.fancybox.close();

                      document.getElementById('<%= CausePostBack.ClientID%>').click();
                  }
              });
          }

          function showPop(val) {



              $.fancybox({
                  type: 'iframe',
                  href: 'encEnqEdit.aspx?ID=' + val,

                  fitToView: false,
                  width: '90%',
                  height: '100%',
                  autoSize: false,
                  closeClick: false,
                  openEffect: 'none',
                  closeEffect: 'none',
                  'onClosed': function () {
                      parent.$.fancybox.close();

                      document.getElementById('<%= CausePostBack.ClientID%>').click();
                  }
              });
          }

          function showBulkEmail() {



              $.fancybox({
                  type: 'iframe',
                  href: 'encBulkEmail.aspx?BSU=' + document.getElementById('<%= ddlSchool.ClientID%>').value,

                    fitToView: false,
                    width: '50%',
                    height: '50%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none'


                });
          }
          function showEnq() {



              $.fancybox({
                  type: 'iframe',
                  href: 'encEnqAdd.aspx',

                  fitToView: false,
                  width: '90%',
                  height: '100%',
                  autoSize: false,
                  closeClick: false,
                  openEffect: 'none',
                  closeEffect: 'none',
                  'onClosed': function () {
                      parent.$.fancybox.close();
                      
                      document.getElementById('<%= CausePostBack.ClientID%>').click();
                  }


              });
          }
        </script>
    <form id="form1" runat="server">
      <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
    
        <asp:LinkButton ID="CausePostBack" runat="server"></asp:LinkButton>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <img src="images/GEMS-banner.png" class="img-responsive" />
        </div>
        <asp:UpdatePanel ID="upLogin" runat="server">
                            <ContentTemplate>
                                 <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static">
                        <asp:Button ID="btnMsg" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X" CausesValidation="false"></asp:Button>

                        <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>

                    </div>
                <section class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                   
                    <div class="header" onclick="showHide();">

                        <span id="ColExpCss" class="ExpClass"></span>
                        <span style="font-size: 16px; padding-left: 6px; color: #19a9e5;">Filter By</span>
                    </div>

                     
                    <div class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1 FindJob"  runat="server" id="FindJob">

                        <div class="col-md-12 col-lg-12 col-sm-12 ">

                            <table style="width: 100%; border-collapse: separate; border-spacing: 10px;" cellpadding="0" cellspacing="0">
                                <tr style="padding-bottom: 25px;">
                                    <td style="width: 15%;"><span class="inputStyleLabel">Parent Name:</span></td>
                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_ParentName" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox></td>
                                    <td style="width: 15%;"><span class="inputStyleLabel">Parent Mobile:&nbsp; &nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_ParentMob" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox></td>

                                    <td style="width: 15%;"><span class="inputStyleLabel">Parent Email:</span></td>
                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_ParentEmail" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox></td>
                                </tr>
                                <tr style="padding-bottom: 25px;">
                                    <td style="width: 5%;"><span class="inputStyleLabel">Child Name:&nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_childName" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox></td>

                                    <td style="width: 15%;"><span class="inputStyleLabel">Date of Birth:&nbsp; &nbsp;&nbsp;  </span></td>
                                    <td style="width: 20%;">
                                         
                                        <asp:TextBox ID="txt_DOB" runat="server" class="inputStyle" Style="width: 100%;" ></asp:TextBox>
                                        <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/calendar.gif" visible="false"/>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txt_DOB" runat="server" 
  CssClass="MyCalendar" PopupButtonID="imgDOB" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                       
                                    </td>
                                    <td style="width: 15%;"><span class="inputStyleLabel">Gender:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:DropDownList ID="ddlGender" runat="server" class="ddl" Style="width: 100%;">
                                            <asp:ListItem Text="--All--" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                            <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr style="padding-bottom: 25px;">
                                    <td style="width: 15%;"><span class="inputStyleLabel">School:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:DropDownList ID="ddlSchool" runat="server" class="ddl" Style="width: 100%;"></asp:DropDownList></td>
                                    <td style="width: 15%;"><span class="inputStyleLabel">Academic Year:</span></td>

                                    <td style="width: 20%;">
                                        <asp:DropDownList ID="ddlAcademicyear" runat="server" class="ddl" Style="width: 100%;"></asp:DropDownList></td>
                                    <td style="width: 15%;"><span class="inputStyleLabel">Year/Grade:&nbsp; &nbsp; </span></td>

                                    <td style="width: 20%;">
                                        <asp:DropDownList ID="ddlGrade" runat="server" class="ddl" Style="width: 100%;"></asp:DropDownList></td>
                                </tr>
                                <tr style="padding-bottom: 25px;">
                                     <td style="width: 15%;"><span class="inputStyleLabel">EnqNo:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_Enqno" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox>
                                        </td>
                                    <td style="width: 15%;"><span class="inputStyleLabel">Agent:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:DropDownList ID="ddlAgent" runat="server" class="ddl" Style="width: 100%;"></asp:DropDownList></td>
                                </tr>
                                <tr style="padding-bottom: 25px;">
                                    <td style="width: 15%;"><span class="inputStyleLabel">Enq Date From:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></td>
                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_enqdtefrom" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox>
                                        <asp:ImageButton ID="img_enqdtefrom" runat="server" ImageUrl="~/Images/calendar.gif"  Visible="false"/>

                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="txt_enqdtefrom" PopupPosition="BottomLeft" runat="server" CssClass="MyCalendar" PopupButtonID="img_enqdtefrom" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td style="width: 15%;"><span class="inputStyleLabel">To:</span></td>

                                    <td style="width: 20%;">
                                        <asp:TextBox ID="txt_enqdteto" runat="server" class="inputStyle" Style="width: 100%;"></asp:TextBox>
                                        <asp:ImageButton ID="img_enqdteto" runat="server" ImageUrl="~/Images/calendar.gif" Visible="false"/>

                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" TargetControlID="txt_enqdteto" runat="server" CssClass="MyCalendar" PopupButtonID="img_enqdteto" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-primary" Style="background: #fedb00; color: black; font-weight: normal;" />
                                         <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-primary" Style="background: #fedb00; color: black; font-weight: normal;" />
                                    </td>
                                </tr>
                            </table>

                        </div>


                    </div>
                </section>
                <section class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                    <div style="float: right;">
                        <br />
                        <a href="#" id="hrefEnq" runat="server" visible="true" class="enrolnew" onclick="showEnq();">Add Enquiry</a>
                        <a href="#" id="hrefTour" runat="server" visible="true" class="enrolnew">School Tour</a>
                        <a href="#" id="hrefchangestatus" runat="server" visible="true" class="enrolnew">Change Status</a>
                        <a href="#" id="hrefBulkemail" runat="server" visible="true" class="enrolnew">Bulk Email</a>
                        <a href="#" id="hrefOpenseat" runat="server" visible="true" class="enrolnew" onclick="showSeats();" >Open Seats</a>
                    </div>
                </section>
                <section class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">
                    <div id="buttonContainer">
                        <asp:Repeater ID="rptStatus" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdStatus" runat="server" Value='<%# Eval("sm_id")%>' />
                                <asp:LinkButton  class='<%# Eval("sm_class")%>' id="btnREF" runat="server" commandname='<%# Eval("sm_id")%>' commandargument='<%# Eval("sm_id")%>'
                                    title='<%# Eval("SM_DESCR")%>'><%# Eval("sm_shortcode")%>(<%# Eval("cnt")%>)</asp:LinkButton>
                            </ItemTemplate>
                        </asp:Repeater>

                    </div>
                </section>
                <section class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1 col-lg-offset-1 col-sm-offset-1">

                    <div class="table-responsive divDashboardContainer">
                        <br />
                        <asp:Label ID="lblMsg" runat="server" ForeColor="red" Text=""></asp:Label>
                        <asp:GridView id="gv_Enquiry" runat="server"
                            allowpaging="True" 
                            cellspacing="0" gridlines="None" enabletheming="False"   autogeneratecolumns="False" bordercolor="#00A3E0" 
                            
                              onneeddatasource="gv_Enquiry_NeedDataSource"
                            onitemcommand="gv_Enquiry_ItemCommand"
                            showgrouppanel="true" pagesize="15" >
                   
                        <Columns>
                             <asp:TemplateField>
                                <HeaderTemplate>
 <asp:CheckBox ID="chkAll" runat="server" Visible="true" ToolTip="Click here to select/deselect all rows"
                                        onclick="javascript:change_chk_state(this);"></asp:CheckBox></HeaderTemplate>

                                 <ItemTemplate > 
                                           
                                            <asp:CheckBox ID="ch1" runat="server"  ></asp:CheckBox>   
                                     <asp:HiddenField id="hdEnqID" runat="server" value='<%#Eval("eqm_enqid")%>' visible="false"></asp:HiddenField>     
                            </ItemTemplate> 
                            <HeaderStyle Font-Bold="True" /> <ItemStyle HorizontalAlign="Center" /> </asp:TemplateField> 
                         <asp:BoundField  
                                HeaderText="Sl No"  DataField="RowNumber"  >
                            
                             <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField>
                             <asp:BoundField   
                                HeaderText="School"  DataField="BSU_SHORTNAME"  >
                               
                               <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField  >
                            <asp:BoundField    
                                HeaderText="Ac Year" DataField="ACYEAR"  >
                              
                               <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField>
                          <asp:BoundField   
                                HeaderText="Enq No"  DataField="eqs_applno"  >
                              
                               <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField  >
                          <asp:BoundField    
                                HeaderText="Enq Date" DataField="eqm_enqdate"  DataFormatString="{0:dd/MMM/yyyy}">
                              
                               <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField>
                          
                           <asp:BoundField    
                                HeaderText="Grade"  DataField="grm_display"  >
                                
                                <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField>
                         <asp:BoundField    
                                HeaderText="Age" DataField="Appl_Age"  >
                              
                              <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField>
                         
                          <asp:BoundField   
                                HeaderText="Status"  DataField="SM_SHORTCODE"  >
                              
                               <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField  >
                             <asp:TemplateField>
                                 <ItemTemplate > 
                                     <a id="frameAppName" class="frameView" style="color:#800000;"    onclick="showPop('<%#Eval("eqm_enqid")%>');"><%#Eval("appl_name")%></a>
                                     </ItemTemplate>
                                   <HeaderStyle Font-Bold="True" font-size="14px"/> 
                                  </asp:TemplateField>
                       
                          <asp:BoundField  
                                HeaderText="Gender"  DataField="GENDER"  >
                               
                               <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" />
                          </asp:BoundField >
                          <asp:BoundField   
                                HeaderText="Parent Name"  DataField="parent_name"  >
                              
                               <HeaderStyle Font-Bold="True" font-size="12px"/> 
                          </asp:BoundField>
                            <asp:TemplateField>
                                 <ItemTemplate > 
                                           
                                            
                                      <a id="framelnkView" class="frameView" style="color:#800000;"    onclick="showJournel('<%#Eval("eqm_enqid")%>');">Journal</a>
                               
                            </ItemTemplate> 
                            <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" /> </asp:TemplateField>

                             <asp:TemplateField>
                                 <ItemTemplate > 
                                           
                                            
                                      <a id="framelnkFollowup" class="frameView" style="color:#800000; "    onclick="showFollowup('<%#Eval("EQS_ID")%>');">Follow up</a>
                               
                            </ItemTemplate> 
                            <HeaderStyle Font-Bold="True" font-size="12px"/> <ItemStyle HorizontalAlign="Center" /> </asp:TemplateField>
                       </Columns>

                        

                    </asp:GridView>


                    </div>

                </section>
           


        <telerik:RadNotification ID="RadNotification1" runat="server" EnableRoundedCorners="true"
            EnableShadow="true" Title="OASIS ALERT - Enquiry Followup" Animation="Slide" AnimationDuration="2000" AutoCloseDelay="30000"
            Width="400" Skin="Outlook">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>

                            <asp:Repeater ID="rptNotif" runat="server">
                                <ItemTemplate>

                                    <span>Enq No : <%# Eval("EQS_APPLNO")%>&nbsp; in School &nbsp;<%# Eval("BSU_SHORTNAME")%>&nbsp;<%# Eval("FOL_REMARKS")%></span><br /><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
            <NotificationMenu ID="TitleMenu">
            </NotificationMenu>
        </telerik:RadNotification>

 
</ContentTemplate>
             </asp:UpdatePanel>

         <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progBgFilter_Show" runat="server">
                <div id="processMessage" class="progMsg_Show">
                    <img alt="Loading..." src="Images/Loading.gif" /><br />
                   
                </div>
                    </div>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="progressBackgroundFilter"
                    VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                    HorizontalOffset="10">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>

    <%--<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>--%>

    <%--<script type="text/javascript" src="js/bootstrap.min.js"></script>--%>

</body>
</html>
