﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encEnqAdd.aspx.vb" Inherits="Enrollment_encEnqAdd" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="css/enroll.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/bootstrap.css" media="screen" />
    <%--<link href="css/gems.css" rel="stylesheet" />--%>

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        function fancyClose() {
            parent.$.fancybox.close();
        }
    </script>
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 20%;
            position: fixed;
            width: 70%;
            background-color: white;
        }

        .RadComboBox_Default .rcbReadOnly {
            background-image: none !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbDisabled {
            background-color: rgba(0,0,0,0.01) !important;
        }

            .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
                background-color: transparent !important;
                border-radius: 0px !important;
                border: 0px !important;
                /*padding: initial !important;*/
                box-shadow: none !important;
            }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
            padding:9px !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
            border-radius: 0px !important;
            border-color: transparent !important;
        }

        table td input[type=text], table td select {
            min-width: 20% !important;
        }


        .RadComboBox_Default .rcbReadOnly .rcbInput {
            padding: 8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div class="title">
            <asp:UpdatePanel ID="upLogin1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4" class="title-bg">Parent Details
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1">Parent First Name<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_ParentFName" runat="server" placeholder="First Name"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator13" runat="server"
                                    Style="color: green; font-size: small;" ErrorMessage="<br/>Please, enter first name." ControlToValidate="txt_ParentFName"
                                    ValidationGroup="Save"></asp:RequiredFieldValidator>

                            </td>
                            <td style="width: 20%;">
                                <label for="exampleInputPassword1" class="field-label">Parent Last Name</label></td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_ParentLName" runat="server" placeholder="Last Name"></asp:TextBox>


                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputmob" class="field-label">Mobile<span style="color: red;">*</span></label></td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_MobCountry" runat="server" placeholder="Code" Style="width: 20%;" MaxLength="6"></asp:TextBox>
                                <asp:TextBox ID="txt_MobArea" runat="server" placeholder="Area" Style="width: 20%;" MaxLength="6"></asp:TextBox>
                                <asp:TextBox ID="txtMobNumber" runat="server" placeholder="Mobile No." Style="width: 34%;" MaxLength="14"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server"
                                    Style="color: green; font-size: small;" ErrorMessage="<br/>Please, enter mobile number." ControlToValidate="txtMobNumber"
                                    ValidationGroup="Save"></asp:RequiredFieldValidator>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftxMCty" runat="server" FilterType="Numbers"
                                    TargetControlID="txt_MobCountry">
                                </ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftxMArea" runat="server" FilterType="Numbers"
                                    TargetControlID="txt_MobArea">
                                </ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftxMNum" runat="server" FilterType="Numbers"
                                    TargetControlID="txtMobNumber">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 20%;">
                                <label for="exampleInputPassword1" class="field-label">Email<span style="color: red;">*</span></label></td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_Email" runat="server" placeholder="Email"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="emailValidator" runat="server" Display="Dynamic"
                                    Style="color: green; font-size: small;" ErrorMessage="<br/>Please, enter valid e-mail address." ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                    ControlToValidate="txt_Email" ValidationGroup="Save">
                                </asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server"
                                    Style="color: green; font-size: small;" ErrorMessage="<br/>Please, enter email." ControlToValidate="txt_Email" ValidationGroup="Save"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Relation to Student<span style="color: red;">*</span></label></td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddl_RelationToStudent" runat="server">
                                    <asp:ListItem Text="Father" Value="F"></asp:ListItem>
                                    <asp:ListItem Text="Mother" Value="M"></asp:ListItem>
                                    <asp:ListItem Text="Guardian" Value="G"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>

                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4" class="title-bg">Student Details
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">First Name<span style="color: red;">*</span></label></td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_StuFirstName" runat="server" placeholder="First Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="<br/>Please, enter first name"
                                    Style="color: green; font-size: small;" Display="Dynamic" ControlToValidate="txt_StuFirstName" ValidationGroup="Save"></asp:RequiredFieldValidator>

                            </td>
                            <td style="width: 20%;">
                                <label for="exampleInputPassword1" class="field-label">Last Name</label>
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_StuLastName" runat="server" placeholder="Last Name"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputPassword1" class="field-label">Gender<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:RadioButtonList ID="rbl_Gender" runat="server" CssClass="field-label" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="<br/>Please, select gender"
                                    Style="color: green; font-size: small;" Display="Dynamic" ControlToValidate="rbl_Gender" ValidationGroup="Save"></asp:RequiredFieldValidator>

                            </td>
                            <td style="width: 15%;">
                                <label for="exampleInputEmail1" class="field-label">Date of Birth<span style="color: red;">*</span></label></td>
                            <td style="width: 20%;" colspan="2">
                                <asp:DropDownList ID="ddlDay" runat="server" Style="width: 20%;">
                                    <asp:ListItem Text="DD" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="01" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="02" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="03" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="04" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="06" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="07" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="08" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="09" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                    <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                    <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                    <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                    <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                    <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlMonth" runat="server" Style="width: 30%;">
                                    <asp:ListItem Text="MMM" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="January" Value="01"></asp:ListItem>
                                    <asp:ListItem Text="February" Value="02"></asp:ListItem>
                                    <asp:ListItem Text="March" Value="03"></asp:ListItem>
                                    <asp:ListItem Text="April" Value="04"></asp:ListItem>
                                    <asp:ListItem Text="May" Value="05"></asp:ListItem>
                                    <asp:ListItem Text="June" Value="06"></asp:ListItem>
                                    <asp:ListItem Text="July" Value="07"></asp:ListItem>
                                    <asp:ListItem Text="August" Value="08"></asp:ListItem>
                                    <asp:ListItem Text="September" Value="09"></asp:ListItem>
                                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlYear" runat="server" Style="width: 24%;"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvDay" runat="server" Visible="false" ControlToValidate="ddlDay"
                                    Style="color: green; font-size: small;" InitialValue="0" ErrorMessage="<br/>Select Day"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfMonth" Visible="false" runat="server" ControlToValidate="ddlMonth"
                                    Style="color: green; font-size: small;" InitialValue="0" ErrorMessage="<br/>Select Month"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfYear" Visible="false" runat="server" ControlToValidate="ddlYear"
                                    Style="color: green; font-size: small;" InitialValue="0" ErrorMessage="<br/>Select Year"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Current School</label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddl_Curschool" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trOther" runat="server">
                            <td style="width: 20%;" align="left">
                                <label for="exampleInputEmail1" class="field-label">Please Specify Other</label>
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txtOther" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>




                    <table style="width: 100%; border-collapse: separate; border-spacing: 10px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="4" class="title-bg">Academic Details
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Academic Year<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddl_academicyear" runat="server"></asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="<br/>Please, select academic year "
                                    Display="Dynamic" ControlToValidate="ddl_academicyear" ValidationGroup="Save"
                                    Style="color: green; font-size: small;" InitialValue="0"></asp:RequiredFieldValidator>

                            </td>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Year / Grade<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddl_grade" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="<br/>Please, select Class "
                                    Style="color: green; font-size: small;" Display="Dynamic" ControlToValidate="ddl_grade" ValidationGroup="Save" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">School(s)<span style="color: red;">*</span></label>

                            </td>
                            <td colspan="2">
                                <telerik:RadComboBox ID="ddl_school" runat="server" RenderMode="Lightweight" Width="95%">
                                </telerik:RadComboBox>
                                <%--<asp:Panel ID="plOLang" runat="server" Height="100px" ScrollBars="Vertical" Width="300px">
                                                                                                    <asp:CheckBoxList ID="ddl_school1" runat="server" " Style="width: 100%;font-size:10px;">
                                                                                                    </asp:CheckBoxList>
                                                                                                </asp:Panel>--%>
                                        
                                    
                            </td>
                        </tr>
                        <%--<tr><td><telerik:RadComboBox ID="rd" runat="server" CheckBoxes="true" "></telerik:RadComboBox></td></tr>   --%>
                    </table>



                    <table style="width: 100%; border-collapse: separate; border-spacing: 10px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="4" class="title-bg">How did you hear about us?
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">How did you hear about our school?<span style="color: red;">*</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:CheckBoxList ID="chkQ1" runat="server"
                                    RepeatColumns="2" RepeatDirection="Horizontal" AutoPostBack="true">
                                </asp:CheckBoxList>

                            </td>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail12" class="field-label">Source</label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddlSource" runat="server"></asp:DropDownList>

                            </td>
                        </tr>
                        <tr>

                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Additional Notes</label>
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txt_addNote" runat="server" placeholder="Additional Notes" TextMode="MultiLine"></asp:TextBox>

                            </td>
                        </tr>

                    </table>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnRegister" CssClass="button" runat="server" Text="Submit" ValidationGroup="Save" />
                                <asp:Button ID="btnCancel_1" CssClass="button" Text="Cancel" runat="server" />
                            </td>
                        </tr>
                    </table>



                    <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static" class="msgInfoBox msgInfoError">
                        <asp:Button ID="btnMsg" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X" CausesValidation="false"></asp:Button>

                        <div>
                            <asp:Label ID="lblErrorlog" runat="server" EnableViewState="false" CssClass="error"></asp:Label>
                        </div>
                    </div>
                    <asp:Panel ID="Panel_Close" runat="server" CssClass="darkPanlAlumini" Visible="false">
                        <div id="divMsg" runat="server" title="Click on the message box to drag it up and down" visible="false" class="inner_darkPanlAlumini" clientidmode="Static">
                            <asp:Button ID="Button1" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X" CausesValidation="false"></asp:Button>
                            <div class="panel-cover">
                                <table align="center">
                                    <tr>
                                        <td align="center">
                                           <%-- <div>
                                                <h4 class="modal-title">
                                                    <img src="images/info.gif" />
                                                    Information</h4>
                                                <hr />
                                            </div>--%>
                                            <img src="images/inf.png" style="width: 25px; height: 25px;" />
                                        <strong>Successfully Saved. Would you like to add Sibling enquiry?</strong>    <br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnContinue" runat="server" Text="Yes" CssClass="button" />
                                            <asp:Button ID="btnCancel" runat="server" Text="No" CssClass="button" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
