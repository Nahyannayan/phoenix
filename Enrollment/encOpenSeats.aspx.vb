﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports Telerik.Web.UI
Imports System.Globalization
Partial Class Enrollment_encOpenSeats
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                bind_region()
                bind_accyear()
                BIND_REGION_TAB()
                ApplyCss()
                BIND_REGION_sub()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    Private Sub bind_region()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "SELECT CTG_ID,CTG_DESCR FROM CORP.COUNTRY_GROUPS ORDER BY CTG_DESCR desc"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddl_region.DataSource = ds
            ddl_region.DataTextField = "CTG_DESCR"
            ddl_region.DataValueField = "CTG_ID"
            ddl_region.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_accyear()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_ACCYEAR", PARAM)
            ddl_acyear.DataSource = dsDetails
            ddl_acyear.DataTextField = "ACY_DESCR"
            ddl_acyear.DataValueField = "ACY_ID"
            ddl_acyear.DataBind()
            ddl_acyear.Items.FindByText("2015-2016").Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BIND_REGION_TAB()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            
            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@ACT_LVL", "0")
            param(1) = New SqlParameter("@LVL_BSU", "0")
            param(2) = New SqlParameter("@ACY_ID", ddl_acyear.SelectedValue)
            param(3) = New SqlParameter("@BSU_ID", ddl_region.SelectedValue)
           
            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.SEATS_DASHBOARD_STATISTICS", param)
            rptTabDB.DataSource = DS.Tables(0)
            rptTabDB.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub
    Private Sub BIND_REGION_sub()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@ACT_LVL", "2")
            param(1) = New SqlParameter("@LVL_BSU", "1")
            param(2) = New SqlParameter("@ACY_ID", ddl_acyear.SelectedValue)
            param(3) = New SqlParameter("@BSU_ID", ddl_region.SelectedValue)

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.SEATS_DASHBOARD_STATISTICS", param)
            If DS.Tables(0).Rows.Count > 0 Then
                rptRegion.DataSource = DS.Tables(0)
                rptRegion.DataBind()
            Else
                rptRegion.DataSource = Nothing
                rptRegion.DataBind()
                BindGrid("")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub
    Private Sub BindGrid(ByVal BSU As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@ACT_LVL", "3")
            param(1) = New SqlParameter("@LVL_BSU", "1")
            param(2) = New SqlParameter("@ACY_ID", ddl_acyear.SelectedValue)
            param(3) = New SqlParameter("@BSU_ID", ddl_region.SelectedValue)
            param(4) = New SqlParameter("@BSU_CITY", BSU)

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.SEATS_DASHBOARD_STATISTICS", param)
            If DS.Tables(0).Rows.Count > 0 Then


                gv_seats.DataSource = DS
                gv_seats.DataBind()
            Else
                gv_seats.DataSource = Nothing
                gv_seats.DataBind()
                BindGrade_Grid("")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ApplyCss()
        Try


            For Each item As RepeaterItem In rptTabDB.Items
                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                    Dim hfWidthENQ As HiddenField = DirectCast(item.FindControl("hfWidthENQ"), HiddenField)
                    Dim dbbarENQ As HtmlControl = DirectCast(item.FindControl("dbbarENQ"), HtmlControl)

                    Dim hfwidthREG As HiddenField = DirectCast(item.FindControl("hfwidthREG"), HiddenField)
                    Dim dbbarREG As HtmlControl = DirectCast(item.FindControl("dbbarREG"), HtmlControl)

                    Dim hfwidthTC As HiddenField = DirectCast(item.FindControl("hfwidthTC"), HiddenField)
                    Dim dbbarTC As HtmlControl = DirectCast(item.FindControl("dbbarTC"), HtmlControl)

                    Dim hfZoneID As HiddenField = DirectCast(item.FindControl("hfZoneID"), HiddenField)
                    Dim divTabHolder As HtmlControl = DirectCast(item.FindControl("divTabHolder"), HtmlControl)
                    Dim divRightArrow As HtmlControl = DirectCast(item.FindControl("divRightArrow"), HtmlControl)
                    Dim lbtnZoneID As LinkButton = DirectCast(item.FindControl("lbtnZoneID"), LinkButton)



                    If ViewState("REGION_ID") = "0" Then

                        ViewState("REGION_ID") = hfZoneID.Value
                        divTabHolder.Attributes.Add("class", "dbactiveCat")
                        divRightArrow.Attributes.Add("class", "arrRightActive")
                    ElseIf ViewState("REGION_ID") = hfZoneID.Value Then

                        divTabHolder.Attributes.Add("class", "dbactiveCat")
                        divRightArrow.Attributes.Add("class", "arrRightActive")
                    Else
                        divTabHolder.Attributes.Add("class", "dbinactiveCat")
                        divRightArrow.Attributes.Add("class", "arrRightInactive")
                    End If


                End If
            Next
        Catch ex As Exception

        End Try
    End Sub
    Sub BindGrade_Grid(ByVal BSU As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@ACT_LVL", "4")
            param(1) = New SqlParameter("@LVL_BSU", "1")
            param(2) = New SqlParameter("@ACY_ID", ddl_acyear.SelectedValue)
            param(3) = New SqlParameter("@BSU_ID", BSU)
            param(4) = New SqlParameter("@BSU_CITY", "")

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.SEATS_DASHBOARD_STATISTICS", param)
            gv_Grade.DataSource = DS
            gv_Grade.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    'Protected Sub ChangeBSU_Command(ByVal sender As Object, ByVal e As CommandEventArgs)


    '    tt.Text = e.CommandArgument



    'End Sub
    'Protected Sub lnk_click(ByVal sender As Object, ByVal e As EventArgs)
    '    tt.Text = "hii"
    'End Sub
    Protected Sub ddl_region_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_region.SelectedIndexChanged
        BIND_REGION_TAB()
        ApplyCss()
        BIND_REGION_sub()

    End Sub

    Protected Sub ddl_acyear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_acyear.SelectedIndexChanged
        BIND_REGION_TAB()
        ApplyCss()
        BIND_REGION_sub()
    End Sub

    Protected Sub rptRegion_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptRegion.ItemCommand
        Dim hfBSU As New HiddenField
        Dim hfchk As New HiddenField
        Dim hfdefRegion As New HiddenField

        Dim lnkregion As LinkButton
        hfBSU = DirectCast(e.Item.FindControl("hfBSU"), HiddenField)
        lnkregion = DirectCast(e.Item.FindControl("lnkregion"), LinkButton)
        hfdefRegion = DirectCast(e.Item.FindControl("hfdefRegion"), HiddenField)
        BindGrid(hfBSU.Value)
        reset_style()
        lnkregion.CssClass = "button small rounded pad chk"
       

    End Sub
    Private Sub reset_style()
        Dim hfchk As New HiddenField
        Dim lnkregion As LinkButton
        Dim i As Integer
        For i = 0 To rptRegion.Items.Count - 1
            With rptRegion.Items(i)
                lnkregion = .FindControl("lnkregion")
                hfchk = .FindControl("hfBSU")
                lnkregion.CssClass = "button small rounded pad unchk"
            End With

        Next
    End Sub
    Protected Sub rptRegion_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptRegion.ItemDataBound
        Dim hfdefRegion As New HiddenField
        Dim hfBSU As New HiddenField

        If e.Item.DataItem Is Nothing Then
            Return

        Else
            hfdefRegion = DirectCast(e.Item.FindControl("hfdefRegion"), HiddenField)

            If hfdefRegion.Value = 1 Then
                hfBSU = DirectCast(e.Item.FindControl("hfBSU"), HiddenField)
                BindGrid(hfBSU.Value)

            End If
        End If
    End Sub

   
    Protected Sub gv_seats_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gv_seats.ItemCommand
        Dim lnkSeats As LinkButton
        Dim i As Integer
        Dim j As Integer
        For j = 0 To gv_seats.Items.Count - 1
            gv_seats.Items(j).BackColor = Drawing.Color.Empty
        Next
        lnkSeats = DirectCast(e.Item.FindControl("lnkSeats"), LinkButton)
        i = (lnkSeats.CommandName) - 1
        BindGrade_Grid(lnkSeats.CommandArgument)
        gv_seats.Items(i).BackColor = System.Drawing.Color.FromArgb(246, 222, 178)
    End Sub
End Class
