﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports Telerik.Web.UI
Partial Class Enrollment_encEnqEdit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If Session("sUsr_name") & "" = "" Then
                    Response.Redirect("~/login.aspx")
                End If
                Bind_school()
                bind_cur_schoool()
                bind_accyear()
                bind_grade()
                bind_year()
                getQuestions(1, chkQ1)
                bind_source()

                Enable_all(False)
                ViewState("ID") = Request.QueryString("ID")
                DISPLAY_ITEMS(ViewState("ID"))
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Bind_school()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_SCHOOLS", PARAM)
            ddl_school.DataSource = dsDetails
            ddl_school.DataTextField = "BSU_NAME"
            ddl_school.DataValueField = "BSU_ID"
            ddl_school.DataBind()
            If (Session("sbsuid") <> "500606") Then
                ddl_school.Items.FindByValue(Session("sBsuid")).Selected = True
                ddl_school.Enabled = False
            Else
                ddl_school.Items.Add(New ListItem("--Select--", "0"))
                ddl_school.Items.FindByText("--Select--").Selected = True
            End If

            'rdcombo.DataSource = dsDetails
            'rdcombo.DataTextField = "BSU_NAME"
            'rdcombo.DataValueField = "BSU_ID"
            'rdcombo.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Sub bind_source()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT SM_ID,SM_DESCR FROM ENC.SOURCE_M "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


        ddlSource.DataSource = ds
        ddlSource.DataTextField = "SM_DESCR"
        ddlSource.DataValueField = "SM_ID"
        ddlSource.DataBind()
        ddlSource.Items.Add(New ListItem("--Select--", "0"))
        ddlSource.Items.FindByText("--Select--").Selected = True
    End Sub
    Private Sub bind_cur_schoool()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@INFO_TYPE", "CURR_SCHOOL")
            PARAM(1) = New SqlClient.SqlParameter("@ACCESS_BY", "0")

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GetEnq_School_list", PARAM)
            ddl_Curschool.DataSource = dsDetails
            ddl_Curschool.DataTextField = "BSU_NAME"
            ddl_Curschool.DataValueField = "BSU_ID"
            ddl_Curschool.DataBind()
            ddl_Curschool.Items.Add(New ListItem("--Select--", "0"))
            ddl_Curschool.Items.Add(New ListItem("Other", ""))
            ddl_Curschool.Items.FindByText("--Select--").Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_accyear()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            If (Session("sbsuid") = "500606") Then
                PARAM(0) = New SqlParameter("@Bsu_id", ddl_school.SelectedValue)
            Else
                PARAM(0) = New SqlParameter("@Bsu_id", Session("Sbsuid"))
            End If
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_ACCYEAR", PARAM)
            ddl_academicyear.DataSource = dsDetails
            ddl_academicyear.DataTextField = "ACY_DESCR"
            ddl_academicyear.DataValueField = "ACY_ID"
            ddl_academicyear.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_grade()
        Try
            'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim PARAM(2) As SqlParameter
            'Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_GRADE", PARAM)
            'ddl_grade.DataSource = dsDetails
            'ddl_grade.DataTextField = "GRD_DISPLAY"
            'ddl_grade.DataValueField = "GRD_ID"
            'ddl_grade.DataBind()
            'ddl_grade.Items.Add(New ListItem("--Select--", "0"))
            'ddl_grade.Items.FindByText("--Select--").Selected = True
            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            If (Session("sbsuid") = "500606") Then
                Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddl_school.SelectedValue, SqlDbType.VarChar)
            Else
                Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            End If
            Param(1) = Mainclass.CreateSqlParameter("@ACY_ID", ddl_academicyear.SelectedValue, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ONLY_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                ddl_grade.DataSource = dsData
                ddl_grade.DataTextField = "GRM_GRD_ID"
                ddl_grade.DataValueField = "GRM_GRD_ID"
                ddl_grade.DataBind()
                ddl_grade.Items.Add(New ListItem("--Select--", "0"))
                ddl_grade.Items.FindByText("--Select--").Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_year()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ENC].[Get_Year_List]", PARAM)
            ddlYear.DataSource = dsDetails
            ddlYear.DataTextField = "year"
            ddlYear.DataValueField = "year"
            ddlYear.DataBind()
            ddlYear.Items.Add(New ListItem("Y", "0"))
            ddlYear.Items.FindByText("Y").Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Clear_all()
        txt_ParentFName.Text = ""
        txt_ParentLName.Text = ""
        txt_Email.Text = ""
        txt_MobArea.Text = ""
        txt_MobCountry.Text = ""
        txtMobNumber.Text = ""

        txt_StuFirstName.Text = ""
        txt_StuLastName.Text = ""
        txt_addNote.Text = ""
        rbl_Gender.ClearSelection()

        ddl_academicyear.ClearSelection()
        ddl_grade.ClearSelection()
        ddl_Curschool.ClearSelection()

        ddl_school.ClearSelection()
        ddl_Curschool.SelectedValue = "0"
        ddl_grade.SelectedValue = "0"

        ddlDay.SelectedValue = "0"
        ddlMonth.SelectedValue = "0"
        ddlYear.SelectedValue = "0"
        ddlSource.SelectedValue = "0"
    End Sub
    Private Sub Enable_all(ByVal v As Boolean)
        txt_ParentFName.Enabled = v
        txt_ParentLName.Enabled = v
        txt_Email.Enabled = v
        txt_MobArea.Enabled = v
        txt_MobCountry.Enabled = v
        txtMobNumber.Enabled = v

        txt_StuFirstName.Enabled = v
        txt_StuLastName.Enabled = v
        txt_addNote.Enabled = v
        rbl_Gender.Enabled = v

        ddl_academicyear.Enabled = v
        ddl_grade.Enabled = v
        ddl_Curschool.Enabled = v

        ddl_school.Enabled = v


        ddlDay.Enabled = v
        ddlMonth.Enabled = v
        ddlYear.Enabled = v
        txtOther.Enabled = v
        chkQ1.Enabled = v
        ddlSource.Enabled = v
    End Sub
    Sub visible_others()
        If ddl_Curschool.SelectedItem.Text = "Other" Then
            trOther.Visible = True
        Else
            trOther.Visible = False
        End If
    End Sub
    Private Sub DISPLAY_ITEMS(ByVal id As Long)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@ENQ_ID", id)


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.DISPLAY_ENQ", PARAM)


            If dsDetails.Tables(0).Rows.Count >= 1 Then


                ViewState("EQSID") = dsDetails.Tables(0).Rows(0).Item("EQS_ID")
                txt_ParentFName.Text = dsDetails.Tables(0).Rows(0).Item("parent_fname")
                txt_ParentLName.Text = dsDetails.Tables(0).Rows(0).Item("parent_lname")
                txt_Email.Text = dsDetails.Tables(0).Rows(0).Item("parent_email")
                ddl_RelationToStudent.SelectedValue = dsDetails.Tables(0).Rows(0).Item("EQM_PRIMARYCONTACT")
                Dim mob As String = dsDetails.Tables(0).Rows(0).Item("parent_mobno")
                Dim ind As Integer = mob.IndexOf("-")
                Dim a As String = mob.Substring(0, ind)
                Dim temp As String = mob.Substring((ind + 1), (mob.Length - 1) - ind)
                ind = temp.IndexOf("-")
                Dim m As String = temp.Substring(0, ind)
                temp = temp.Substring(ind + 1, (temp.Length - 1) - ind)

                txt_MobArea.Text = a
                txt_MobCountry.Text = m
                txtMobNumber.Text = temp

                txt_StuFirstName.Text = dsDetails.Tables(0).Rows(0).Item("eqm_applfirstname")
                txt_StuLastName.Text = dsDetails.Tables(0).Rows(0).Item("eqm_appllastname")
                txt_addNote.Text = dsDetails.Tables(0).Rows(0).Item("EQM_ENC_NOTES")
                Dim g As String = dsDetails.Tables(0).Rows(0).Item("EQM_APPLGENDER")
                If g = "M" Then
                    rbl_Gender.Items(0).Selected = True
                Else
                    rbl_Gender.Items(1).Selected = True
                End If
               
                ddl_academicyear.SelectedValue = dsDetails.Tables(0).Rows(0).Item("EQS_ACY_ID")
                ' ddl_academicyear.Items.FindByValue(dsDetails.Tables(0).Rows(0).Item("EQS_ACY_ID")).Selected = True
                ddl_grade.SelectedValue = dsDetails.Tables(0).Rows(0).Item("EQS_GRD_ID")
                ddl_Curschool.SelectedValue = dsDetails.Tables(0).Rows(0).Item("Prev_School_GEMS")
                ViewState("school") = dsDetails.Tables(0).Rows(0).Item("EQS_BSU_ID")
                ddl_school.SelectedValue = ViewState("school")
               
                ddlDay.SelectedValue = Day(dsDetails.Tables(0).Rows(0).Item("EQM_APPLDOB"))
                Dim mo As String = Month(dsDetails.Tables(0).Rows(0).Item("EQM_APPLDOB"))
                If mo.Length = 1 Then mo = "0" + mo
                ddlMonth.SelectedValue = mo
                ddlYear.SelectedValue = Year(dsDetails.Tables(0).Rows(0).Item("EQM_APPLDOB"))

                txtOther.Text = dsDetails.Tables(0).Rows(0).Item("EQM_PREVSCHOOL")
                get_checkedVal(dsDetails.Tables(0).Rows(0).Item("EQP_QUS1"))
                ddlSource.SelectedValue = dsDetails.Tables(0).Rows(0).Item("EQM_ENC_SOURCE")
                ddl_academicyear.ClearSelection()
                ddl_academicyear.Items.FindByValue(dsDetails.Tables(0).Rows(0).Item("EQS_ACY_ID")).Selected = True
            End If

            visible_others()

        Catch ex As Exception

        End Try
    End Sub
    Private Sub get_checkedVal(ByVal lst As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT id FROM     OASISFIN.[dbo].[fnSplitMe]('" & lst & "', '|')  "
                   
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)



        Dim i As Integer, j As Integer
    



        For i = 0 To chkQ1.Items.Count - 1

            For j = 0 To ds.Tables(0).Rows.Count - 1
                If chkQ1.Items(i).Value = ds.Tables(0).Rows(j).Item("id") Then
                    chkQ1.Items(i).Selected = True
                End If

            Next
        Next


    End Sub
    Private Sub update_enquiry()
        Dim ReturnFlag As Integer
        Dim con As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim pParms(18) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@ENQ_ID", ViewState("ID"))
            pParms(1) = New SqlClient.SqlParameter("@EQM_ENC_NOTES", txt_addNote.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@eqm_applfirstname", txt_StuFirstName.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@eqm_appllastname", txt_StuLastName.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", ddl_RelationToStudent.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@EQM_APPLGENDER", rbl_Gender.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@EQM_APPLDOB", ddlYear.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlDay.SelectedValue)
            pParms(7) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_BSU_ID", ddl_Curschool.SelectedValue)
            pParms(8) = New SqlClient.SqlParameter("@EQS_ACD_ID", ddl_academicyear.SelectedValue)
            pParms(9) = New SqlClient.SqlParameter("@EQS_BSU_ID", ddl_school.SelectedValue)
            pParms(10) = New SqlClient.SqlParameter("@EQS_GRD_ID", ddl_grade.SelectedValue)
            pParms(11) = New SqlClient.SqlParameter("@parent_Fname", txt_ParentFName.Text.Trim())
            pParms(12) = New SqlClient.SqlParameter("@parent_Lname", txt_ParentLName.Text.Trim())
            pParms(13) = New SqlClient.SqlParameter("@PARENT_EMAIL", txt_Email.Text.Trim())
            pParms(14) = New SqlClient.SqlParameter("@PARENT_MOBNO", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())

            If ddl_Curschool.SelectedItem.Text <> "Other" Then
                txtOther.Text = ""
            End If

            pParms(15) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL", txtOther.Text.Trim())

            Dim EQP_QUS1 As String = ""

            For Each item As ListItem In chkQ1.Items
                If (item.Selected) Then

                    EQP_QUS1 = EQP_QUS1 & item.Value
                    EQP_QUS1 = EQP_QUS1 & "|"
                End If
            Next
            pParms(16) = New SqlClient.SqlParameter("@EQP_QUS1", EQP_QUS1)
            pParms(17) = New SqlClient.SqlParameter("@EQM_ENC_SOURCE", ddlSource.SelectedValue)

            pParms(18) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(18).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.UPDATE_ENQUIRY", pParms)

            ReturnFlag = pParms(18).Value
        Catch ex As Exception
            lblErrorlog.Text = ex.Message
            lblErrorlog.Focus()

            ReturnFlag = 1


        Finally
            If ReturnFlag = 0 Then
                sqltran.Commit()
            Else
                sqltran.Rollback()
            End If
        End Try
    End Sub
    Private Function del_enq(ByVal sqltran As SqlTransaction) As Integer
        Dim pParms(2) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@ENQ_ID", ViewState("ID"))
           
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.DEL_ENQ", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        Catch ex As Exception

        End Try

    End Function
    Sub getQuestions(ByVal id As Integer, ByVal chk As CheckBoxList)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select EQD_ID,EQD_ANS  from ONLINE_ENQ.ENQUIRY_QUESTION_M " _
                                & " inner join ONLINE_ENQ.ENQUIRY_QUESTION_D on EQU_ID=EQD_EQU_ID " _
                                & " where EQU_ID=" & id & " and EQD_BSU_ID='500606'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


        chk.DataSource = ds
        chk.DataTextField = "EQD_ANS"
        chk.DataValueField = "EQD_ID"
        chk.DataBind()



    End Sub
    Private Function SaveStudENQUIRY_M(ByRef errmsg As String, ByRef cn As Integer) As Integer
        Dim err As Integer = 0

        Dim EQM_ENQID As String = String.Empty

        Dim con As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            'ViewState("EQM_ENQID") = ""
            ' ViewState("EEQM_SIBLING_ENQID") = IIf(ViewState("EEQM_SIBLING_ENQID") = "", "", ViewState("EEQM_SIBLING_ENQID"))
            Dim pParms(30) As SqlClient.SqlParameter



            pParms(0) = New SqlClient.SqlParameter("@EQM_ENQDATE", Convert.ToString(DateTime.Now))   'DateTime.Now)
            pParms(1) = New SqlClient.SqlParameter("@EQM_APPLFIRSTNAME", txt_StuFirstName.Text.Trim())
            pParms(2) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", txt_StuLastName.Text.Trim())
            pParms(3) = New SqlClient.SqlParameter("@EQM_APPLDOB", ddlYear.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlDay.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@EQM_APPLGENDER", rbl_Gender.SelectedValue)

            pParms(5) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", ddl_RelationToStudent.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@EQM_ENQTYPE", "O")
            pParms(7) = New SqlClient.SqlParameter("@EQM_STATUS", "NEW")
            pParms(8) = New SqlClient.SqlParameter("@EQM_FILLED_BY", System.DBNull.Value)
            pParms(9) = New SqlClient.SqlParameter("@EQM_SIBLING_ENQID", "")
            pParms(10) = New SqlClient.SqlParameter("@EQM_EMGCONTACT", txt_MobCountry.Text.Trim() + txt_MobArea.Text.Trim() + txtMobNumber.Text.Trim())

            pParms(11) = New SqlClient.SqlParameter("@TEMP_EQM_ENQID_STR", SqlDbType.VarChar, 100)
            pParms(11).Direction = ParameterDirection.Output

            pParms(12) = New SqlClient.SqlParameter("@EQM_APPLNATIONALITY", 0)
            pParms(13) = New SqlClient.SqlParameter("@EQM_APPL_CONTACTME", "YES")

            pParms(14) = New SqlClient.SqlParameter("@EQM_CNT", cn)
            pParms(15) = New SqlClient.SqlParameter("@EQM_ENC_AGENT", Session("sUsr_name"))

            pParms(16) = New SqlClient.SqlParameter("@EQM_ENC_NOTES", txt_addNote.Text)
            pParms(17) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_BSU_ID", ddl_Curschool.SelectedValue)

            If ddl_Curschool.SelectedItem.Text <> "Other" Then
                txtOther.Text = ""
            End If
            pParms(18) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL", txtOther.Text)
            pParms(19) = New SqlClient.SqlParameter("@EQM_ENC_SOURCE", ddlSource.SelectedValue)



            pParms(20) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(20).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SaveStudENQUIRY_M", pParms)
            Dim ReturnFlag As Integer = pParms(20).Value
            If ReturnFlag = 0 Then
                EQM_ENQID = pParms(11).Value
                ViewState("EQM_ENQID") = EQM_ENQID

                If (EQM_ENQID <> "") Then
                    ReturnFlag = SaveStudENQUIRY_PARENT_M(sqltran, EQM_ENQID)
                    If (ReturnFlag = 0) Then
                        ReturnFlag = SaveStudENQUIRY_SCHOOLPRIO_S(sqltran, EQM_ENQID)
                        If (ReturnFlag = 0) Then
                            ReturnFlag = del_enq(sqltran)
                        End If
                        If (ReturnFlag = 0) Then
                            ViewState("EEQM_SIBLING_ENQID") = IIf(ViewState("EEQM_SIBLING_ENQID") = "", EQM_ENQID, ViewState("EEQM_SIBLING_ENQID"))

                        End If
                        ' Dim dtStudentEnq_Details As DataTable = ViewState("StudentEnq_Details")
                        If (ReturnFlag <> 0) Then
                            errmsg = "Duplicate enquiry of student" + txt_StuFirstName.Text.Trim() + " " + txt_StuLastName.Text.Trim() + " in " + ddl_grade.SelectedItem.Text + " found.Please contact school."
                            'Throw New System.Exception("An exception has occurred.")

                            err = 1
                        End If
                    Else
                        ' Throw New System.Exception("An exception has occurred.")
                        err = 1
                    End If
                End If



                ViewState("RecordAdded") = "True"

                lblErrorlog.Focus()
                ' divbox_savemsg.Visible = True
            Else
                Throw New System.Exception("An exception has occurred.")
            End If

        Catch ex As Exception

            lblErrorlog.Text = ex.Message
            lblErrorlog.Focus()



            err = 1
        Finally
            If err = 0 Then
                sqltran.Commit()
            Else
                sqltran.Rollback()
            End If

        End Try

        Return err
        ViewState("EQM_ENQID_COPY") = ViewState("EQM_ENQID")
        'ViewState("EQM_ENQID") = ""
    End Function
    Private Function SaveStudENQUIRY_PARENT_M(ByVal sqltran As SqlTransaction, ByVal EQM_ENQID As String) As Integer

        Dim pParms(15) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@EQP_EQM_ENQID", EQM_ENQID)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            If (ddl_RelationToStudent.SelectedValue = "F") Then

                pParms(2) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", txt_ParentFName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EQP_FLASTNAME", txt_ParentLName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@EQP_FMOBILE", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@EQP_FEMAIL", txt_Email.Text.Trim())
            ElseIf (ddl_RelationToStudent.SelectedValue = "M") Then
                pParms(2) = New SqlClient.SqlParameter("@EQP_MFIRSTNAME", txt_ParentFName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EQP_MLASTNAME", txt_ParentLName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@EQP_MMOBILE", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@EQP_MEMAIL", txt_Email.Text.Trim())
            ElseIf (ddl_RelationToStudent.SelectedValue = "G") Then
                pParms(2) = New SqlClient.SqlParameter("@EQP_GFIRSTNAME", txt_ParentFName.Text.Trim())
                pParms(3) = New SqlClient.SqlParameter("@EQP_GLASTNAME", txt_ParentLName.Text.Trim())

                pParms(4) = New SqlClient.SqlParameter("@EQP_GMOBILE", txt_MobCountry.Text.Trim() + "-" + txt_MobArea.Text.Trim() + "-" + txtMobNumber.Text.Trim())
                pParms(5) = New SqlClient.SqlParameter("@EQP_GEMAIL", txt_Email.Text.Trim())
            End If

            pParms(6) = New SqlClient.SqlParameter("@EQP_bSCHOOLTOUR", 0)

            Dim ur As String = HttpContext.Current.Request.Url.AbsoluteUri

            pParms(7) = New SqlClient.SqlParameter("@EQP_URL", ur)
            Dim EQP_QUS1 As String = ""

            For Each item As ListItem In chkQ1.Items
                If (item.Selected) Then

                    EQP_QUS1 = EQP_QUS1 & item.Value
                    EQP_QUS1 = EQP_QUS1 & "|"
                End If
            Next
            pParms(8) = New SqlClient.SqlParameter("@EQP_QUS1", EQP_QUS1)
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SaveStudENQUIRY_PARENT_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        Catch ex As Exception

        End Try

    End Function

    Private Function SaveStudENQUIRY_SCHOOLPRIO_S(ByVal sqltran As SqlTransaction, ByVal EQM_ENQID As String) As Integer
        Dim pParms(15) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQM_ENQID)
        pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        pParms(2) = New SqlClient.SqlParameter("@EQS_ACD_ID", ddl_academicyear.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@EQS_BSU_ID", ddl_school.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@EQS_GRM_ID", ddl_grade.SelectedValue)
        pParms(5) = New SqlClient.SqlParameter("@EQS_CLM_ID", 1)
        'pParms(6) = New SqlClient.SqlParameter("@EQS_REMARKS", txt_Remarks.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@TEMP_ApplNo", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.Output
        pParms(7) = New SqlClient.SqlParameter("@EQS_SIB_ENQID", 0)
        'pParms(9) = New SqlClient.SqlParameter("@EQS_DOJ", txt_DOJ.Text)
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SaveStudENQUIRY_SCHOOLPRIO_S", pParms)


        Dim ReturnFlag As Integer = pParms(1).Value

        If ReturnFlag = 0 Then

            ViewState("EQS_ID") = pParms(6).Value
        End If

        Return ReturnFlag

    End Function

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            Enable_all(True)
            btnEdit.Text = "Update"
        Else
            Enable_all(False)

            Dim errmsg As String = String.Empty
            If ViewState("school") <> ddl_school.SelectedValue Then
                If SaveStudENQUIRY_M(errmsg, 1) = 0 Then
                    errmsg = "Record Added Successfully."


                Else
                    If errmsg <> "" Then
                        lblErrorlog.Text = errmsg
                    Else
                        lblErrorlog.Text = "Error occured.Please contact school"
                    End If
                    Exit Sub
                End If
            Else
                update_enquiry()
            End If




            btnEdit.Text = "Edit"
        End If

    End Sub


    
    Protected Sub btnFollowup_Click(sender As Object, e As EventArgs) Handles btnFollowup.Click
        Response.Redirect("~/Enrollment/encFollowup.aspx?id=" & ViewState("EQSID"))
    End Sub

    Protected Sub btnChangeStatus_Click(sender As Object, e As EventArgs) Handles btnChangeStatus.Click
        Session("eqmids") = ViewState("ID")
        Response.Redirect("~/Enrollment/encChangeStatus.aspx")
    End Sub

    Protected Sub btnJournel_Click(sender As Object, e As EventArgs) Handles btnJournel.Click
        Response.Redirect("~/Enrollment/encJournel.aspx?id=" & ViewState("ID"))
    End Sub

    Protected Sub ddl_Curschool_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Curschool.SelectedIndexChanged
        visible_others()
    End Sub
End Class
