﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports Telerik.Web.UI
Imports System.Globalization
Partial Class Enrollment_encBulkEmail
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                BIND_TEMPLATE(Request.QueryString("BSU"))

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Private Sub BIND_TEMPLATE(ByVal BSU As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(1) As SqlParameter

            param(0) = New SqlParameter("@BSU_ID", BSU)


            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.GET_TEMPLATE_NAME", param)
            ddlTemplate.DataSource = DS
            ddlTemplate.DataTextField = "ET_DESCR"
            ddlTemplate.DataValueField = "ET_ID"
            ddlTemplate.DataBind()
            ddlTemplate.Items.Add(New ListItem("--Select--", "0"))
            ddlTemplate.Items.FindByText("--Select--").Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub
    Private Sub send_bulk_email()
        Dim ReturnFlag As Integer
        Dim con As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BE_ET_ID", ddlTemplate.SelectedItem.Value)
            pParms(1) = New SqlClient.SqlParameter("@BE_EQM_ENQID", Session("eqmids"))

            pParms(2) = New SqlClient.SqlParameter("@BE_USER", Session("sUsr_name"))
            pParms(3) = New SqlClient.SqlParameter("@BE_REMARKS", txtremarks.Text)


            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ENC.SAVE_BULKEMAIL_M", pParms)

            ReturnFlag = pParms(4).Value
        Catch ex As Exception
            lblmessage.Text = ex.Message
            lblmessage.Focus()

            ReturnFlag = 1


        Finally
            If ReturnFlag = 0 Then
                sqltran.Commit()
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
            Else
                sqltran.Rollback()
            End If
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Panel_Close.Visible = True
        divMsg.Visible = True
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)
    End Sub

    Protected Sub btncan_Click(sender As Object, e As EventArgs) Handles btncan.Click
        divMsg.Visible = False
        Panel_Close.Visible = False
    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        divMsg.Visible = False
        Panel_Close.Visible = False
        send_bulk_email()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "fancyClose", "fancyClose();", True)

    End Sub

    'Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    divMsg.Visible = False
    '    Panel_Close.Visible = False
    'End Sub
End Class
