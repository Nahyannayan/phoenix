<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="encDashboard.aspx.vb" Inherits="Enrollment_encDashboard" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <%--<!DOCTYPE html>
<html>--%>
    <!--<![endif]-->
    <%--<head runat="server">--%>
    <%--<title>GEMS Enrollment Center</title>--%>

    <%--<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />--%>
    <link href="css/enroll.css" rel="stylesheet" type="text/css" />
    <%-- <link rel="stylesheet" href="css/bootstrap.css" media="screen" />--%>
    <%--<link href="css/gems.css" rel="stylesheet" />--%>
    <link href="css/buttons.css" rel="stylesheet" />

    <%--    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>--%>
    <%--<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />--%>
    <script type="text/javascript">
        function showHide() {
            $header = $(".header");
            $content = $(".FindJob");
            $ColExpId = $("#ColExpCss");
            $content.slideToggle(500, function () {
                if ($content.is(":visible") == true) {
                    $ColExpId.removeClass("colClass").addClass("ExpClass");
                }
                else {
                    $ColExpId.removeClass("ExpClass").addClass("colClass");
                }
            });
        }
        var tempObj = '';
        function popUpDetails(url) {
            try {
                if (tempObj && !tempObj.closed) {
                    tempObj.focus();
                }
                else {
                    window.open(url);
                }
            }
            catch (e) { }

            return false;
        }        
        function showSeats() {
            $.fancybox({
                type: 'iframe',
                href: 'encOpenSeats.aspx',

                fitToView: false,
                width: '90%',
                height: '100%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    //window.location.reload();
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
            });
        }
        function showSchooltour() {
            $.fancybox({
                type: 'iframe',
                href: 'encSchoolTour.aspx',
                fitToView: false,
                width: '50%',
                height: '50%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    // window.location.reload();
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
            });
        }

        function showJournel(val) {
            $.fancybox({
                type: 'iframe',
                href: 'encJournel.aspx?id=' + val,

                fitToView: false,
                width: '50%',
                height: '75%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    //  window.location.reload();
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
            });
        }

        function fancyClose() {            
            parent.$.fancybox.close();
           
        }

       

    </script>
    <%--<style>
       .RadGrid .rgRow, .RadGrid .rgAltRow, .RadGrid .rgResizeCol, .RadGrid .rgPager, .RadGrid .rgGroupPanel{
           cursor:pointer !important;
       }

    </style>--%>
 <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="gv_Enquiry">
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <style type="text/css">

         .RadComboBox_Default .rcbReadOnly {
            background-image:none !important;
            background-color:transparent !important;

        }
        .RadComboBox_Default .rcbDisabled {
            background-color:rgba(0,0,0,0.01) !important;
        }
        .RadComboBox_Default .rcbDisabled input[type=text]:disabled {
            background-color:transparent !important;
            border-radius:0px !important;
            border:0px !important;
            padding:initial !important;
            box-shadow: none !important;
        }
        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        #fancybox-content {
            border-color: #00a3e0 !important;
            background-color: #00a3e0 !important;
            width: 100% !important;
            overflow-x: hidden !important;
        }

        #fancybox-inner {
            overflow-x: hidden !important;
        }

        .RadGrid {
            border-radius: 10px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            background-color: #00a3e0;
        }

        .RadGrid_Office2010Blue th.rgSorted {
            background-color: #00a3e0;
        }

        .MyCalendar .ajax__calendar_container {
            border: 1px solid #646464;
            background-color: #00a3e0;
            color: white;
            border-radius: 10px;
        }

        .colClass {
            padding: 5px 10px 2px 10px;
            background: #ececec url("Images/down.png") no-repeat 3px 5px;
            border: 1px solid #d1cfcf;
        }

        .ExpClass {
            padding: 5px 10px 2px 10px;
            background: #ececec url("Images/up.png") no-repeat 3px 5px;
            border: 1px solid #d1cfcf;
        }
    </style>


    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                args.set_enableAjax(false);
            }
        }

        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>



    <script type="text/javascript">

        function showChangeStatus() {



            $.fancybox({
                type: 'iframe',
                href: 'encChangeStatus.aspx',

                fitToView: false,
                width: '50%',
                height: '50%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                'onClosed': function () {
                    parent.$.fancybox.close();                    
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                },
                afterClose: function () {
                    // window.location.reload();
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }


            });
        }

        function showFollowup(val) {



            $.fancybox({
                type: 'iframe',
                href: 'encFollowup.aspx?ID=' + val,

                fitToView: false,
                width: '60%',
                height: '95%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                'onClosed': function () {
                    parent.$.fancybox.close();                   
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                },
                afterClose: function () {
                    // window.location.reload();
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
            });
        }

        function showPop(val) {



            $.fancybox({
                type: 'iframe',
                href: 'encEnqEdit.aspx?ID=' + val,

                fitToView: false,
                width: '90%',
                height: '100%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                'onClosed': function () {
                    parent.$.fancybox.close();                 
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                },
                afterClose: function () {
                    // window.location.reload();
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
            });
        }

        function showBulkEmail() {



            $.fancybox({
                type: 'iframe',
                href: 'encBulkEmail.aspx?BSU=' + document.getElementById('<%= ddlSchool.ClientID%>').value,

                fitToView: false,
                width: '50%',
                height: '50%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    // window.location.reload();  
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }

            });
        }
        function showEnq() {
            
            $.fancybox({
                type: 'iframe',
                href: 'encEnqAdd.aspx',

                fitToView: false,
                width: '90%',
                height: '100%',
                autoSize: false,
                closeClick: true,
                openEffect: 'none',
                closeEffect: 'none',
                'onClosed': function () {                
                    parent.$.fancybox.close();                   
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                },
                afterClose: function () {
                    // window.location.reload();   
                    document.getElementById('<%= CausePostBack.ClientID%>').click();
                }
              });
          }
    </script>
    <%--<form id="form1" runat="server">--%>
    <%--<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">--%>
    <%--</ajaxToolkit:ToolkitScriptManager>--%>



    <asp:LinkButton ID="CausePostBack" runat="server" OnClick="CausePostBack_Click"></asp:LinkButton>
    <%--  <div class="col-md-12 col-lg-12 col-sm-12">
            <img src="images/GEMS-banner.png" class="img-responsive" />
        </div>--%>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            GEMS Enrollment Center
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="upLogin" runat="server">
                                <ContentTemplate>
                                    <div id="divNote" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static">
                                        <asp:Button ID="btnMsg" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; "
                                            ForeColor="White" Text="X" CausesValidation="false"></asp:Button>
                                        <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="error"></asp:Label>
                                    </div>
                                    <section>
                                        <div class="header" onclick="showHide();">

                                            <span id="ColExpCss" class="colClass"></span>
                                            <span style="font-size: 16px; padding-left: 6px; color: #19a9e5;">Filter By</span>
                                        </div>
                                        <div class="panel-cover FindJob" runat="server" id="FindJob">
                                            <div>
                                                <table style="width: 100%; border-collapse: separate; border-spacing: 10px;" cellpadding="0" cellspacing="0">
                                                    <tr style="padding-bottom: 25px;">
                                                        <td style="width: 15%;"><span class="field-label">Parent Name</span></td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_ParentName" runat="server" Style="width: 100%;"></asp:TextBox></td>
                                                        <td style="width: 15%;"><span class="field-label">Parent Mobile  </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_ParentMob" runat="server" Style="width: 100%;"></asp:TextBox></td>

                                                        <td style="width: 15%;"><span class="field-label">Parent Email</span></td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_ParentEmail" runat="server" Style="width: 100%;"></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="padding-bottom: 25px;">
                                                        <td style="width: 5%;"><span class="field-label">Child Name </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_childName" runat="server" Style="width: 100%;"></asp:TextBox></td>

                                                        <td style="width: 15%;"><span class="field-label">Date of Birth  </span></td>
                                                        <td style="width: 20%;">

                                                            <asp:TextBox ID="txt_DOB" runat="server" Style="width: 100%;"></asp:TextBox>
                                                            <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/calendar.gif" Visible="false" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txt_DOB" runat="server"
                                                                PopupButtonID="imgDOB" Format="dd/MMM/yyyy">
                                                            </ajaxToolkit:CalendarExtender>

                                                        </td>
                                                        <td style="width: 15%;"><span class="field-label">Gender     </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:DropDownList ID="ddlGender" runat="server" Style="width: 100%;">
                                                                <asp:ListItem Text="--All--" Value=""></asp:ListItem>
                                                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="padding-bottom: 25px;">
                                                        <td style="width: 15%;"><span class="field-label">School     </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:DropDownList ID="ddlSchool" runat="server" Style="width: 100%;" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged"></asp:DropDownList></td>
                                                        <td style="width: 15%;"><span class="field-label">Academic Year</span></td>

                                                        <td style="width: 20%;">
                                                            <asp:DropDownList ID="ddlAcademicyear" runat="server" Style="width: 100%;" OnSelectedIndexChanged="ddlAcademicyear_SelectedIndexChanged"></asp:DropDownList></td>
                                                        <td style="width: 15%;"><span class="field-label">Year/Grade  </span></td>

                                                        <td style="width: 20%;">
                                                            <asp:DropDownList ID="ddlGrade" runat="server" Style="width: 100%;"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="padding-bottom: 25px;">
                                                        <td style="width: 15%;"><span class="field-label">EnqNo     </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_Enqno" runat="server" Style="width: 100%;"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 15%;"><span class="field-label">Agent     </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:DropDownList ID="ddlAgent" runat="server" Style="width: 100%;"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="padding-bottom: 25px;">
                                                        <td style="width: 15%;"><span class="field-label">Enq Date From     </span></td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_enqdtefrom" runat="server" Style="width: 100%;"></asp:TextBox>
                                                            <asp:ImageButton ID="img_enqdtefrom" runat="server" ImageUrl="~/Images/calendar.gif" Visible="false" />

                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="txt_enqdtefrom" PopupPosition="BottomLeft" runat="server"  PopupButtonID="img_enqdtefrom" Format="dd/MMM/yyyy">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </td>
                                                        <td style="width: 15%;"><span class="field-label">To</span></td>

                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txt_enqdteto" runat="server" Style="width: 100%;"></asp:TextBox>
                                                            <asp:ImageButton ID="img_enqdteto" runat="server" ImageUrl="~/Images/calendar.gif" Visible="false" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" TargetControlID="txt_enqdteto" runat="server"  PopupButtonID="img_enqdteto" Format="dd/MMM/yyyy">
                                                            </ajaxToolkit:CalendarExtender>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" class="button" />
                                                            <asp:Button ID="btnReset" runat="server" Text="Reset" class="button" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </section>
                                    <section>

                                        <div align="right" width="100%">
                                            <br />
                                            <a href="#" id="hrefEnq" runat="server" visible="true" onclick="showEnq();" class="button" >Add Enquiry</a>
                                            <%--<a href="#" id="hrefTour" runat="server" visible="true" class="button">School Tour</a>--%>
                                            <a href="#" id="hrefchangestatus" runat="server" visible="true" class="button">Change Status</a>
                                            <a href="#" id="hrefBulkemail" runat="server" visible="true" class="button">Bulk Email</a>
                                            <a href="#" id="hrefOpenseat" runat="server" visible="true" onclick="showSeats();" class="button">Open Seats</a>
                                        </div>
                                    </section>
                                    <section>
                                        <br />
                                        <div id="buttonContainer" width="100%">
                                            <asp:Repeater ID="rptStatus" runat="server">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdStatus" runat="server" Value='<%# Eval("sm_id")%>' />
                                                    <asp:LinkButton class='<%# Eval("sm_class")%>' ID="btnREF" runat="server" CommandName='<%# Eval("sm_id")%>' CommandArgument='<%# Eval("sm_id")%>'
                                                        title='<%# Eval("SM_DESCR")%>'><%# Eval("sm_shortcode")%>(<%# Eval("cnt")%>)</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </section>
                                    <section>

                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <div class="table-responsive divDashboardContainer">
                                                        <br />
                                                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                                        <telerik:RadGrid ID="gv_Enquiry" runat="server" Width="100%"
                                                            AllowPaging="True"
                                                            CellSpacing="0" GridLines="None" EnableTheming="False" AutoGenerateColumns="False"
                                                            OnNeedDataSource="gv_Enquiry_NeedDataSource"
                                                            OnItemCommand="gv_Enquiry_ItemCommand"
                                                            ShowGroupPanel="true" PageSize="15">
                                                            <ClientSettings AllowColumnsReorder="true" AllowDragToGroup="True"></ClientSettings>
                                                            <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" />

                                                            <MasterTableView CommandItemDisplay="top">

                                                                <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToWordButton="true" ShowExportToExcelButton="true"></CommandItemSettings>
                                                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                                                    <HeaderStyle Width="20px" BackColor="#00a3e0"></HeaderStyle>
                                                                </RowIndicatorColumn>
                                                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                                                    <HeaderStyle Width="20px"></HeaderStyle>
                                                                </ExpandCollapseColumn>
                                                                <Columns>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Select" UniqueName="TemplateColumn" Groupable="False"
                                                                        AllowFiltering="False">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkAll" runat="server" Visible="true" ToolTip="Click here to select/deselect all rows"
                                                                                onclick="javascript:change_chk_state(this);"></asp:CheckBox>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ch1" runat="server"></asp:CheckBox>
                                                                            <asp:HiddenField ID="hdEnqID" runat="server" Value='<%#Eval("eqm_enqid")%>' Visible="false"></asp:HiddenField>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Sl No" UniqueName="column" DataField="RowNumber">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True"  HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center"  />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="School" UniqueName="column" DataField="BSU_SHORTNAME">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Acc Year" UniqueName="column" DataField="ACYEAR">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Enq No" UniqueName="column" DataField="eqs_applno">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Enq Date" UniqueName="column" DataField="eqm_enqdate" DataFormatString="{0:dd/MMM/yyyy}">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Grade" UniqueName="column" DataField="grm_display">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Age" UniqueName="column" DataField="Appl_Age">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Status" UniqueName="column" DataField="SM_SHORTCODE">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Applicant Name" UniqueName="TemplateColumn" Groupable="False"
                                                                        AllowFiltering="False">
                                                                        <ItemTemplate>
                                                                            <a id="frameAppName" class="frameView" style="color: green; cursor:pointer !important;" onclick="showPop('<%#Eval("eqm_enqid")%>');"><%#Eval("appl_name")%></a>
                                                                        </ItemTemplate >
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                         <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Gender" UniqueName="column" DataField="GENDER">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn
                                                                        HeaderText="Parent Name" UniqueName="column" DataField="parent_name">
                                                                        <ColumnValidationSettings>
                                                                            <ModelErrorMessage Text="" />
                                                                        </ColumnValidationSettings>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Journal" UniqueName="TemplateColumn" Groupable="False"
                                                                        AllowFiltering="False">
                                                                        <ItemTemplate>
                                                                            <a id="framelnkView" class="frameView" style="color: green;  cursor:pointer !important;" onclick="showJournel('<%#Eval("eqm_enqid")%>');">Journal</a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Follow up" UniqueName="TemplateColumn" Groupable="False"
                                                                        AllowFiltering="False">
                                                                        <ItemTemplate>
                                                                            <a id="framelnkFollowup" class="frameView" style="color: green;  cursor:pointer !important;" onclick="showFollowup('<%#Eval("EQS_ID")%>');">Follow up</a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center"  />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <EditFormSettings>
                                                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                                                </EditFormSettings>
                                                            </MasterTableView>
                                                            <FilterMenu EnableImageSprites="False"></FilterMenu>
                                                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                                                        </telerik:RadGrid>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </section>
                                   <telerik:RadNotification ID="RadNotification1" runat="server" EnableRoundedCorners="true"
                                        EnableShadow="true" Title="OASIS ALERT - Enquiry Followup" Animation="Slide" AnimationDuration="2000" AutoCloseDelay="30000"
                                        Width="400" Skin="Outlook">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td>

                                                        <asp:Repeater ID="rptNotif" runat="server">
                                                            <ItemTemplate>

                                                                <span>Enq No : <%# Eval("EQS_APPLNO")%> in School <%# Eval("BSU_SHORTNAME")%> <%# Eval("FOL_REMARKS")%></span>
                                                                <br />
                                                                <br />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <NotificationMenu ID="TitleMenu">
                                        </NotificationMenu>
                                    </telerik:RadNotification>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--  <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
                                <ProgressTemplate>
                                    <div id="progressBackgroundFilter" class="progBgFilter_Show" runat="server">
                                        <div id="processMessage" class="progMsg_Show">
                                            <img alt="Loading..." src="Images/Loading.gif" /><br />

                                        </div>
                                    </div>
                                    <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="progressBackgroundFilter"
                                        VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                                        HorizontalOffset="10">
                                    </ajaxToolkit:AlwaysVisibleControlExtender>
                                </ProgressTemplate--%>
                            <%--</asp:UpdateProgress>--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
