﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encChangeStatus.aspx.vb" Inherits="Enrollment_encChangeStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="css/enroll.css" rel="stylesheet" type="text/css" />--%>
    <%--<link rel="stylesheet" href="css/bootstrap.css" media="screen" />--%>
    <%--<link href="css/gems.css" rel="stylesheet" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
      <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        function fancyClose() {

            parent.location.reload(true);

        }
    </script>
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 20%;
            position: fixed;
            width: 70%;
            background-color:white ;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div class="title">



            <asp:UpdatePanel ID="upLogin" runat="server">
                <ContentTemplate>


                    <table style="width: 100%; border-collapse: separate; border-spacing: 10px;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label"><span class="field-label">Status</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddlEnqStatus" runat="server"  Style="width: 100%;"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label"><span class="field-label">Remarks</span></label>
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="txtremarks" runat="server" MaxLength="300" 
                                    TextMode="MultiLine"  Style="width: 100%;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    ControlToValidate="txtremarks"
                                    Display="Static"
                                    ErrorMessage="*Enter Remarks"
                                    runat="server" ValidationGroup="MAINERROR" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="Center">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Change Status" ValidationGroup="MAINERROR" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                <br />
                                <asp:Label ID="lblmessage" runat="server" CssClass="error" Width="100%"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel_Close" runat="server" CssClass="darkPanlAlumini" Visible="false" >
                        <div id="divMsg" runat="server" title="Click on the message box to drag it up and down" class="inner_darkPanlAlumini" visible="false" clientidmode="Static">
                          <%--  <asp:Button ID="Button1" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X" CausesValidation="false"></asp:Button>--%>
                           <%-- <div>
                                <img src="images/info.gif" />
                                Information
                            <hr />
                            </div>--%>
                            <div class="panel-cover">
                                <table align="center">
                                    <tr>
                                        <td align="center">
                                            <img src="images/inf.png" style="width: 25px; height: 25px;" />
                                          <strong> Do u want to Change status? </strong><br />
                                            <br />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnContinue" runat="server" Text="Yes" CssClass="button" />
                                            <asp:Button ID="btncan" runat="server" Text="No" CssClass="button" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
