﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encJournel.aspx.vb" Inherits="Enrollment_encJournel" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">  
    <title></title>
<%--<link href="css/enroll.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" media="screen" />
    <link href="css/gems.css" rel="stylesheet" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
    <link href ="../cssfiles/sb-admin.css" rel="stylesheet" />
    <%--<link href="css/Dashboard.css" rel="stylesheet" />--%>
    <%--<link href="css/buttons.css" rel="stylesheet" />--%>
    <%--<link rel="stylesheet" href="css/bootstrap.css" media="screen" />--%>
    <%--<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>--%>
    <style>
        .RadGrid_Default  {
            width:100% !important;
        }
        
        #fancybox-content {
            border-color: #00a3e0 !important;
            background-color: #00a3e0 !important;
            width: 100% !important;
            overflow-x: hidden !important;
        }

        #fancybox-inner {
            overflow-x: hidden !important;
        }

        .RadGrid {
            border-radius: 10px;
            overflow: hidden;
        }

        .RadGrid_Office2010Blue .rgCommandRow table {
            background-color: #00a3e0;
        }

        .RadGrid_Office2010Blue th.rgSorted {
            background-color: #00a3e0;
        }

        .MyCalendar .ajax__calendar_container {
            border: 1px solid #646464;
            background-color: #00a3e0;
            color: white;
            border-radius: 10px;
        }
    </style>
    
</head>
<body >
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div style="width:100% !important" >
            <asp:UpdatePanel ID="upLogin" runat="server">
                <ContentTemplate>                    
                    <telerik:RadGrid ID="gv_journal" runat="server" width="100%"
                        AllowPaging="false" AllowSorting="True"  AutoGenerateColumns="false"
                        CellSpacing="0" GridLines="None" EnableTheming="False" 
                        ShowGroupPanel="True">
                        <ClientSettings AllowColumnsReorder="True"></ClientSettings>
                        <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true">
                            <Excel Format="ExcelML" />
                        </ExportSettings>
                        <MasterTableView CommandItemDisplay="Top">
                            <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToWordButton="true" ShowExportToExcelButton="true"></CommandItemSettings>
                            <RowIndicatorColumn Visible="False" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column" Created="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn
                                    HeaderText="SlNo" UniqueName="column" DataField="slno">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Date" UniqueName="column" DataField="DTE" DataFormatString="{0:dd/MMM/yyyy hh:mm tt}">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Action" UniqueName="column" DataField="DESCR">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="User" UniqueName="column" DataField="USR">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn
                                    HeaderText="Remarks" UniqueName="column" DataField="REMARKS">
                                    <ColumnValidationSettings>
                                        <ModelErrorMessage Text="" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Font-Bold="True" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                           </Columns>
                        </MasterTableView>
                        <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                    </telerik:RadGrid>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
