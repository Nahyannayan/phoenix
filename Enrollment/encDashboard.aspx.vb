﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports Telerik.Web.UI
Partial Class Enrollment_encDashboard
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                'Session("sUsr_name") = "rajesh.kumar"
                ViewState("EnqStatus") = "0"
                Bind_school()               
                bind_accyear()
                bind_grade()
                bind_agent()
                ' bind_enq_status()
                'bind_followup_status()

                show_notif()
                BIND_status()
                ' BindGrid(0)
                FindJob.Style.Add("display", "none")


            Catch ex As Exception

            End Try
        End If

    End Sub
    Private Sub BindGrid(ByVal ENC_STATUS As Integer)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter

            PARAM(0) = New SqlParameter("@USERNAME", ddlAgent.SelectedValue)
            PARAM(1) = New SqlParameter("@ENC_STATUS", ENC_STATUS)
            PARAM(2) = New SqlParameter("@PARENTNAME", txt_ParentName.Text)
            PARAM(3) = New SqlParameter("@PARENTMOBNO", txt_ParentMob.Text)
            PARAM(4) = New SqlParameter("@PARENTEMAIL", txt_ParentEmail.Text)
            PARAM(5) = New SqlParameter("@STUDNAME", txt_childName.Text)
            PARAM(6) = New SqlParameter("@DOB", txt_DOB.Text)
            PARAM(7) = New SqlParameter("@GENDER", ddlGender.SelectedValue)
            'PARAM(8) = New SqlParameter("@BSU_ID", ddlSchool.SelectedValue)
            If (Session("sbsuid") = "500606") Then
                PARAM(8) = New SqlParameter("@BSU_ID", ddlSchool.SelectedValue)
            Else
                PARAM(8) = New SqlParameter("@BSU_ID", Session("sbsuid"))              
            End If
            PARAM(9) = New SqlParameter("@ACY_ID", ddlAcademicyear.SelectedValue)
            PARAM(10) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
            PARAM(11) = New SqlParameter("@ENQDTEFRM", txt_enqdtefrom.Text)
            PARAM(12) = New SqlParameter("@ENQDTETO", txt_enqdteto.Text)
            PARAM(13) = New SqlParameter("@ENQNO", txt_Enqno.Text)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GETENQUIRY", PARAM)

            gv_Enquiry.DataSource = dsDetails
            gv_Enquiry.DataBind()
            Dim resultCount As String, TotalRecords As String
            resultCount = "0"
            TotalRecords = "0"
            resultCount = dsDetails.Tables(0).Rows.Count.ToString()

            str_query = "SELECT COUNT(*)cnt FROM enquiry_m  WITH (NOLOCK) INNER JOIN dbo.ENQUIRY_SCHOOLPRIO_S WITH (NOLOCK) ON EQM_ENQID=EQS_EQM_ENQID WHERE EQM_bENC=1 AND EQS_STATUS<>'DEL' AND EQS_ACY_ID=" & ddlAcademicyear.SelectedValue
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            TotalRecords = ds.Tables(0).Rows(0).Item("cnt").ToString
            lblMsg.Text = "<span style='font-weight:bold;font-size:22px;'>" & resultCount & "</span> Records Found, from  " & TotalRecords & " records"
        Catch ex As Exception

        End Try

    End Sub
    Private Sub BIND_notif()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))


            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.GET_NOTIFICATIONS", param)
            If DS.Tables(0).Rows.Count >= 1 Then
                rptNotif.DataSource = DS.Tables(0)
                rptNotif.DataBind()
                RadNotification1.Show()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub
    Private Sub BIND_status()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(13) As SqlParameter

            param(0) = New SqlParameter("@user", Session("sUsr_name"))

            param(1) = New SqlParameter("@PARENTNAME", txt_ParentName.Text)
            param(2) = New SqlParameter("@PARENTMOBNO", txt_ParentMob.Text)
            param(3) = New SqlParameter("@PARENTEMAIL", txt_ParentEmail.Text)
            param(4) = New SqlParameter("@STUDNAME", txt_childName.Text)
            param(5) = New SqlParameter("@DOB", txt_DOB.Text)
            param(6) = New SqlParameter("@GENDER", ddlGender.SelectedValue)
            '  param(7) = New SqlParameter("@BSU_ID", ddlSchool.SelectedValue)           
            If (Session("sbsuid") = "500606") Then
                param(7) = New SqlParameter("@BSU_ID", ddlSchool.SelectedValue)
            Else
                param(7) = New SqlParameter("@BSU_ID", Session("sbsuid"))
            End If
            param(8) = New SqlParameter("@ACY_ID", ddlAcademicyear.SelectedValue)
            param(9) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
            param(10) = New SqlParameter("@ENQDTEFRM", txt_enqdtefrom.Text)
            param(11) = New SqlParameter("@ENQDTETO", txt_enqdteto.Text)
            param(12) = New SqlParameter("@ENQNO", txt_Enqno.Text)

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.get_status_cnt", param)
            rptStatus.DataSource = DS.Tables(0)
            rptStatus.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub
    Private Sub show_notif()
        RadNotification1.ShowSound = "warning"
        System.Media.SystemSounds.Asterisk.Play()
        BIND_notif()

    End Sub
    Private Sub bind_accyear()
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter

            If (Session("sbsuid") = "500606") Then
                PARAM(0) = New SqlParameter("@Bsu_id", ddlSchool.SelectedValue)
            Else
                PARAM(0) = New SqlParameter("@Bsu_id", Session("Sbsuid"))
            End If
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_ACCYEAR", PARAM)
            ddlAcademicyear.DataSource = dsDetails
            ddlAcademicyear.DataTextField = "ACY_DESCR"
            ddlAcademicyear.DataValueField = "ACY_ID"
            ddlAcademicyear.DataBind()
            For Each rowACD As DataRow In dsDetails.Tables(0).Rows
                If rowACD("ACD_CURRENT") Then
                    ddlAcademicyear.Items.FindByValue(rowACD("ACY_ID")).Selected = True
                    Exit For
                End If
            Next
            '  ddlAcademicyear.Items.FindByValue("26").Selected = True

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlSchool_SelectedIndexChanged(sender As Object, e As EventArgs)
        bind_accyear()
        bind_grade()
    End Sub
    Protected Sub ddlAcademicyear_SelectedIndexChanged(sender As Object, e As EventArgs)
        bind_grade()
    End Sub
    Private Sub bind_agent()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_AGENT", PARAM)
            ddlAgent.DataSource = dsDetails
            ddlAgent.DataTextField = "agent"
            ddlAgent.DataValueField = "agent"
            ddlAgent.DataBind()
            ddlAgent.Items.FindByValue(Session("sUsr_name")).Selected = True
        Catch ex As Exception

        End Try
    End Sub
    Private Sub bind_grade()
        Try
            'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim PARAM(2) As SqlParameter
            'Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_GRADE", PARAM)

            Dim dsData As DataSet = Nothing
            Dim Param(1) As SqlParameter
            If (Session("sbsuid") = "500606") Then
                Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlSchool.SelectedValue, SqlDbType.VarChar)
            Else
                Param(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sbsuid"), SqlDbType.VarChar)
            End If
            Param(1) = Mainclass.CreateSqlParameter("@ACY_ID", ddlAcademicyear.SelectedValue, SqlDbType.BigInt)
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = "[OASIS].[GET_ONLY_GRADES_INSCHOOL]"
                dsData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sql_query, Param)
            End Using
            If Not dsData Is Nothing Then
                ddlGrade.DataSource = dsData
                ddlGrade.DataTextField = "GRM_GRD_ID"
                ddlGrade.DataValueField = "GRM_GRD_ID"
                ddlGrade.DataBind()
                ddlGrade.Items.Add(New ListItem("--All--", ""))
                ddlGrade.Items.FindByText("--All--").Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Bind_school()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_SCHOOLS", PARAM)
            ddlSchool.DataSource = dsDetails
            ddlSchool.DataTextField = "BSU_NAME"
            ddlSchool.DataValueField = "BSU_ID"
            ddlSchool.DataBind()
            ddlSchool.Items.Add(New ListItem("--All--", ""))
            If (Session("sbsuid") <> "500606") Then
                ddlSchool.Items.FindByValue(Session("sBsuid")).Selected = True
                ddlSchool.Enabled = False
            Else
                ddlSchool.Items.FindByText("--All--").Selected = True
            End If
            
        Catch ex As Exception

        End Try
    End Sub


    'Sub bind_followup_status()
    '    ddlFollowupStatus.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String

    '    str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlFollowupStatus.DataSource = ds
    '    ddlFollowupStatus.DataTextField = "EFR_REASON"
    '    ddlFollowupStatus.DataValueField = "EFR_ID"
    '    ddlFollowupStatus.DataBind()
    '    ddlFollowupStatus.Items.Add(New ListItem("--All--", "0"))
    '    ddlFollowupStatus.Items.FindByText("--All--").Selected = True
    'End Sub
    'Sub bind_enq_status()
    '    ddlEnqStatus.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String

    '    str_query = "SELECT SM_ID ,SM_DESCR  FROM [ENC].[STATUS_M]  "



    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlEnqStatus.DataSource = ds
    '    ddlEnqStatus.DataTextField = "SM_DESCR"
    '    ddlEnqStatus.DataValueField = "SM_ID"
    '    ddlEnqStatus.DataBind()
    '    ddlEnqStatus.Items.Add(New ListItem("--All--", "0"))
    '    ddlEnqStatus.Items.FindByText("--All--").Selected = True
    'End Sub
    Private Function get_sel_ids() As String
        Dim chk As CheckBox
        Dim hdEnqID As HiddenField
        Dim str As String = ""
        For Each rowItem As GridDataItem In gv_Enquiry.MasterTableView.Items

            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            hdEnqID = DirectCast(rowItem.FindControl("hdEnqID"), HiddenField)

            If chk.Checked = True Then
                If str = "" Then
                    str = hdEnqID.Value
                Else
                    str = str & "|" & hdEnqID.Value
                End If
            End If

        Next
        Return str
    End Function
    Protected Sub gv_Enquiry_NeedDataSource(source As Object, e As GridNeedDataSourceEventArgs)

        BindGrid(ViewState("EnqStatus"))
    End Sub
    
    Protected Sub gv_Enquiry_ItemCommand(sender As Object, e As GridCommandEventArgs)

        Try           
            If e.CommandName = Telerik.Web.UI.RadGrid.ExportToExcelCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToWordCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToCsvCommandName OrElse e.CommandName = Telerik.Web.UI.RadGrid.ExportToPdfCommandName Then
                ConfigureExport()
            End If
        Catch
        End Try



       
    End Sub
    Public Sub ConfigureExport()

        gv_Enquiry.ExportSettings.ExportOnlyData = True
        gv_Enquiry.ExportSettings.IgnorePaging = True

        

    End Sub
    Protected Sub hrefEnq_ServercClick(sender As Object, e As EventArgs) Handles hrefEnq.ServerClick


        hrefEnq.Attributes.Add("class", "button")
        hrefchangestatus.Attributes.Add("class", "button")
        hrefOpenseat.Attributes.Add("class", "button")
        '   hrefTour.Attributes.Add("class", "button")
        hrefBulkemail.Attributes.Add("class", "button")
    End Sub

    Protected Sub hrefchangestatus_ServerClick(sender As Object, e As EventArgs) Handles hrefchangestatus.ServerClick
        hrefEnq.Attributes.Add("class", "button")
        hrefchangestatus.Attributes.Add("class", "button")
        hrefOpenseat.Attributes.Add("class", "button")
        '   hrefTour.Attributes.Add("class", "button")
        hrefBulkemail.Attributes.Add("class", "button")

        Session("eqmids") = ""
        Dim eqmids As String = get_sel_ids()
        If eqmids <> "" Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "showChangeStatus", "showChangeStatus();", True)
            Session("eqmids") = eqmids
        Else

            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Please select the records to Change Status !! </div>"
            hrefchangestatus.Attributes.Add("class", "")
        End If
    End Sub

    Protected Sub hrefOpenseat_ServerClick(sender As Object, e As EventArgs) Handles hrefOpenseat.ServerClick
        hrefEnq.Attributes.Add("class", "button")
        hrefchangestatus.Attributes.Add("class", "button")
        hrefOpenseat.Attributes.Add("class", "button")
        ' hrefTour.Attributes.Add("class", "button")
        hrefBulkemail.Attributes.Add("class", "button")
    End Sub

    'Protected Sub hrefTour_ServerClick(sender As Object, e As EventArgs) Handles hrefTour.ServerClick
    '    hrefEnq.Attributes.Add("class", "button")
    '    hrefchangestatus.Attributes.Add("class", "button")
    '    hrefOpenseat.Attributes.Add("class", "button")
    '    '  hrefTour.Attributes.Add("class", "button")
    '    hrefBulkemail.Attributes.Add("class", "button")
    '    Session("eqmids") = ""
    '    Dim eqmids As String = get_sel_ids()
    '    If eqmids <> "" Then
    '        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "showSchooltour", "showSchooltour();", True)
    '        Session("eqmids") = eqmids
    '    Else

    '        divNote.Visible = True
    '        divNote.Attributes("class") = "msgInfoBox msgInfoError"
    '        lblError.Text = "<div>Please select the records to School Tour !! </div>"
    '        '   hrefTour.Attributes.Add("class", "")
    '    End If
    'End Sub


    Protected Sub hrefBulkemail_ServerClick(sender As Object, e As EventArgs) Handles hrefBulkemail.ServerClick
        hrefEnq.Attributes.Add("class", "button")
        hrefchangestatus.Attributes.Add("class", "button")
        hrefOpenseat.Attributes.Add("class", "button")
        '   hrefTour.Attributes.Add("class", "button")
        hrefBulkemail.Attributes.Add("class", "button")
        Session("eqmids") = ""
        Dim eqmids As String = get_sel_ids()
        If eqmids <> "" Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "showBulkEmail", "showBulkEmail();", True)
            Session("eqmids") = eqmids
        Else

            divNote.Visible = True
            divNote.Attributes("class") = "msgInfoBox msgInfoError"
            lblError.Text = "<div>Please select the records to send Bulk Email !! </div>"
            hrefBulkemail.Attributes.Add("class", "")
        End If
    End Sub



    Protected Sub btnMsg_Click(sender As Object, e As EventArgs) Handles btnMsg.Click
        divNote.Visible = False
    End Sub

    Protected Sub rptStatus_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptStatus.ItemCommand
        Dim hdStatus As New HiddenField

        hdStatus = DirectCast(e.Item.FindControl("hdStatus"), HiddenField)
        ViewState("EnqStatus") = hdStatus.Value
        BindGrid(ViewState("EnqStatus"))

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BIND_status()
        BindGrid(0)
        FindJob.Visible = True
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txt_ParentName.Text = ""
        txt_ParentMob.Text = ""
        txt_ParentEmail.Text = ""
        txt_childName.Text = ""
        txt_DOB.Text = ""
        ddlGender.SelectedValue = ""
        ddlSchool.SelectedValue = ""
        ddlAcademicyear.SelectedValue = "26"
        ddlGrade.SelectedValue = ""
        txt_enqdtefrom.Text = ""
        txt_enqdteto.Text = ""
        txt_Enqno.Text = ""
        ddlAgent.SelectedValue = Session("sUsr_name")
        BIND_status()
        BindGrid(0)
    End Sub

    Protected Sub CausePostBack_Click(sender As Object, e As EventArgs)
        BIND_status()
        BindGrid(0)
    End Sub
End Class
