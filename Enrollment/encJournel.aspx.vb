﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports Telerik.Web.UI
Imports System.Globalization
Partial Class Enrollment_encJournel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try

                BIND_journal(Request.QueryString("id"))

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Private Sub BIND_journal(ByVal enqid As Long)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@eqm_id", enqid)


            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ENC.GET_JOURNAL", param)
            gv_journal.DataSource = DS.Tables(0)
            gv_journal.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub
End Class
