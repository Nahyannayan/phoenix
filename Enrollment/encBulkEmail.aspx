﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encBulkEmail.aspx.vb" Inherits="Enrollment_encBulkEmail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="css/enroll.css" rel="stylesheet" type="text/css" />--%>
    <%--<link rel="stylesheet" href="css/bootstrap.css" media="screen" />--%>
    <%--<link href="css/gems.css" rel="stylesheet" />--%>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript">
        function fancyClose() {

            parent.$.fancybox.close();

        }
    </script>
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 20%;
            position: fixed;
            width: 70%;
            background-color:white ;
        }
    </style>
</head>
<body >
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <div >
            <asp:UpdatePanel ID="upLogin" runat="server">
                <ContentTemplate>
                    <table style="width: 100%; ">
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Mail Template</label>
                            </td>
                            <td >
                                <asp:DropDownList ID="ddlTemplate" runat="server"  ></asp:DropDownList></td>
                       </tr>
                        <tr>
                            <td style="width: 20%;">
                                <label for="exampleInputEmail1" class="field-label">Remarks</label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtremarks" runat="server" MaxLength="300"
                                    TextMode="MultiLine" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    ControlToValidate="txtremarks"
                                    Display="Static"
                                    ErrorMessage="*Enter Remarks"
                                    runat="server" ValidationGroup="MAINERROR" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="Center">
                                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Send Mail" ValidationGroup="MAINERROR" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                <br />
                                <asp:Label ID="lblmessage" runat="server" CssClass="error" Width="100%"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel_Close" runat="server" CssClass="darkPanlAlumini" Visible="false">
                        <div id="divMsg" runat="server" title="Click on the message box to drag it up and down" visible="false" clientidmode="Static" class="inner_darkPanlAlumini" >
                            <%--<asp:Button ID="Button1" type="button" runat="server" Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X" CausesValidation="false"></asp:Button>--%>
                           <%-- <div>
                                <h4 class="modal-title">
                                    <img src="images/info.gif" />
                                    Information</h4>
                                <hr />
                            </div>--%>
                            <div class="panel-cover">

                                <table align="center">
                                    <tr>
                                        <td align="center">
                                            <img src="images/inf.png" style="width: 25px; height: 25px;" />
                                            Do u want to Change status?<br />
                                            <br />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnContinue" runat="server" Text="Yes" CssClass="button" />
                                            <asp:Button ID="btncan" runat="server" Text="No" CssClass="button" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
