<%@ Page Language="VB"  AutoEventWireup="true" CodeFile="editmenuAccess.aspx.vb" Inherits="editmenuAccess" Title="::::GEMS OASIS:::: Online Student Administration System::::" MaintainScrollPositionOnPostback="true" %>
<%@ OutputCache VaryByParam="none"  Duration="1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
   <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="cssfiles/example.css" rel="stylesheet" type="text/css" />
    <script  Language="javascript" type="text/javascript">   
    </script>
    <base target="_self" />
    </head>
  
<body topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0"> 
    <form id="form1" runat="server">
<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0">     
    <tr>
        <td class="mattersheading" width="30%">
                <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
    </tr>
        <tr>
            <td style="height: 27px;" class="mattersheading" width="30%">
                Select &nbsp;Menu for Quick Access:</td>
        </tr>
       
        <tr>
            <td colspan="1" style="height: 241px" valign="top" width="30%">
                <asp:Panel ID="Panel1" runat="server" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px"
                                Height="100%" ScrollBars="Both" Width="98%" CssClass="matters">
                                <asp:TreeView ID="tvmenu" runat="server" DataSourceID="XmlDataSourceMenu" ExpandDepth="0" ShowCheckBoxes="Leaf">
                                    <SelectedNodeStyle BackColor="#FFC080" />
                                    <DataBindings>
                                        <asp:TreeNodeBinding DataMember="menuItem" TextField="Text" ToolTipField="modules"
                                            ValueField="ValueField" />
                                    </DataBindings> 
                                </asp:TreeView>
                            </asp:Panel>
            </td>
        </tr>
    <tr>
        <td colspan="1" valign="top" width="30%" align="center">
                <asp:Button ID="btnSave" runat="server" CssClass="button"  Text="Save" />
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close();"
                    Text="Close" Width="47px" />
                                </td>
    </tr>
    <tr>
        <td colspan="1" style="height: 241px" valign="top" width="30%">
        <asp:Panel ID="Panel3" runat="server" BorderColor="#1B80B6" BorderStyle="Solid" BorderWidth="1px" Width="100%" Visible="False">
            <asp:GridView ID="InfoGridView" runat="server" AutoGenerateColumns="False" CssClass="gridstyle" Width="100%" Height="100%" UseAccessibleHeader="False">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="False">
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="idLabel" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BusUnit" HeaderText="Business Unit" Visible="False">
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="menuCode" HeaderText="MenuCode" Visible="False">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="menuText" HeaderText="Menu Name">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MNA_Usr_ID" HeaderText="User ID" Visible="False">
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Description" HeaderText="Description" Visible="False">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RoleId" HeaderText="Role ID" Visible="False" />
                        <asp:BoundField DataField="modules" HeaderText="Module" Visible="False">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:CommandField HeaderText="Delete" ShowCancelButton="False" ShowDeleteButton="True" >
                            <HeaderStyle Wrap="False" />
                        </asp:CommandField>
                        <asp:BoundField DataField="OperationID" HeaderText="MenuRight" Visible="False" />
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                    <RowStyle CssClass="griditem" Height="25px" />
                </asp:GridView>
            <asp:XmlDataSource ID="XmlDataSourceMenu" runat="server" EnableCaching="False" TransformFile="~/TransformXSLT.xsl" XPath="MenuItems/MenuItem">
            </asp:XmlDataSource>
        </asp:Panel>
        </td>
    </tr>
    </table>
    </form>
</body>
</html>


