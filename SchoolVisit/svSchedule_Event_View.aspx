<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="svSchedule_Event_View.aspx.vb"
 Inherits="SchoolVisit_svSchedule_Event_View" Title="::::GEMS OASIS:::: Online Student Administration System::::"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
    function Hideme() {


    var hfFlag = document.getElementById('<%=hfFlag.ClientID %>')

        if (hfFlag.value == '0') {
            document.getElementById('hide-search').style.display = "none";
            //document.getElementById('I18').style.display = "none";
            //document.getElementById('I19').style.display = "none";
            //document.getElementById('I20').style.display = "none";
            //document.getElementById('I21').style.display = "none";
            //document.getElementById('I22').style.display = "none";
            document.getElementById('<%=hfFlag.ClientID %>').value = "1";
            document.getElementById('<%=lbtnShow.ClientID %>').innerText = "Show";
           
            
        }
        else {
            document.getElementById('hide-search').style.display = "block";
            document.getElementById('hide-search').style.width = "100%";
            //document.getElementById('I18').style.display = "block";
            //document.getElementById('I19').style.display = "block";
            //document.getElementById('I20').style.display = "block";
            //document.getElementById('I21').style.display = "block";
            //document.getElementById('I22').style.display = "block";
            document.getElementById('<%=hfFlag.ClientID %>').value = "0";
            document.getElementById('<%=lbtnShow.ClientID %>').innerText = "Hide";
        }

 
    }

</script>


      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal id="ltHeader" runat="server" Text="Schedule Events"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

   
    <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                
                <asp:LinkButton id="lbAddNew" runat="server" onclick="lbAddNew_Click">Add New</asp:LinkButton></td>
        </tr>
        <tr>
            <td align="left"  valign="top" >
                <table id="tbl_test"  align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                            <td class="title-bg" colspan="4">
                                                      
                                  Search Events</td>
                            
                                 <asp:LinkButton ID="lbtnShow" runat="server"  Text="Hide" ForeColor="Maroon" Font-Size="11px"  Visible="false"
                                OnClientClick="javascript:Hideme();return false;"  >
                                 </asp:LinkButton>
                            
                        </tr>
                    <tr >
                        <td align="left">
                           <table align="center" id="hide-search" width="100%">
                        
                        <tr id="I18">
                            <td align="left">
                                <span class="field-label">Event Category</span></td>
                           
                            <td align="left" colspan="2">
                               <asp:DropDownList id="ddlEM_ID" runat="server" ></asp:DropDownList></td>
                           <td></td>
                           
                        </tr>
                        <tr id="I19">
                            <td align="left">
                              <span class="field-label">Title</span>
                            </td>
                           
                            <td align="left" colspan="2">
                              <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox> 
                            </td>
                            <td></td>
                        </tr>
                        
                        
                        <tr id="I20">
                            <td align="left">
                             <span class="field-label">Start Date</span>
                            </td>
                           
                            <td align="left">
                               <asp:TextBox ID="txtEventStartDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgBtnEventStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                       Height="16px" /><div>(dd/mmm/yyyy)         </div>
                                       
                            </td>
                            <td align="left">
                                <span class="field-label">End Date</span>
                            </td>
                           
                            <td align="left">
                               <asp:TextBox ID="txtEventEndDt" runat="server" ></asp:TextBox> 
                                 <asp:ImageButton ID="imgBtnEventEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                       Height="16px" /><div>(dd/mmm/yyyy)         </div>
                                    
                            </td>
                        </tr>
                         <tr id="I21">
                            <td align="left">
                             <span class="field-label">Access Start Date</span>
                            </td>
                            
                            <td align="left">
                               <asp:TextBox ID="txtAccessStartDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgbtnAccessStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                       Height="16px" /><div>(dd/mmm/yyyy)         </div>
                                      
                            </td>
                            <td align="left">
                              <span class="field-label">Access End Date</span>
                            </td>
                            
                            <td align="left">
                               <asp:TextBox ID="txtAccessEndDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgbtnAccessEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                       Height="16px" /> <div>(dd/mmm/yyyy) </div> 
                            </td>
                        </tr>
                         <tr id="I22">
                            <td align="left">
                               <span class="field-label"> Accessible For</span></td>
                           
                            <td align="left">
                                <asp:DropDownList ID="ddlAccessFor" runat="server">
                                  
                                </asp:DropDownList>
                            </td>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"  />
                            </td>
                        </tr>
                        </table>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgbtnAccessStartDt" TargetControlID="txtAccessStartDt"> </ajaxToolkit:CalendarExtender>
                               <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgbtnAccessEndDt" TargetControlID="txtAccessEndDt"> </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgBtnEventEndDt" TargetControlID="txtEventEndDt"> </ajaxToolkit:CalendarExtender>

   <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgBtnEventStartDt" TargetControlID="txtEventStartDt">
                       </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:GridView id="gvSchedule" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <rowstyle cssclass="griditem" />
                                <columns>
<asp:TemplateField HeaderText="Sr.No"><ItemTemplate>
<%--<div style="display:inline;float:right;padding-right:2px;">
<asp:Image ID="imgComp" runat="server" 
        ImageUrl='<%# bind("FLAG_COMPLETE") %>' ToolTip='<%# bind("TOOL_TIP") %>'  /> </div>--%>
<asp:Label id="lblR1" runat="server" Text='<%# bind("R1") %>'></asp:Label>
     
</ItemTemplate>

    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

</asp:TemplateField>
<asp:TemplateField HeaderText="Event Category">
<ItemTemplate>
<asp:Label id="lblEventType" runat="server" Text='<%# Bind("EM_TITLE") %>' 
       ></asp:Label> 
</ItemTemplate>
<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Title">

<ItemTemplate>
    <asp:Label id="lblESM_DISPLAY_TITLE" runat="server" 
        Text='<%# Bind("ESM_DISPLAY_TITLE") %>'></asp:Label> 
</ItemTemplate>

<HeaderStyle Wrap="False"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
</asp:TemplateField>


<asp:TemplateField HeaderText="Event Date">

<ItemTemplate>
<asp:Label id="lblEventDate" runat="server" Text='<%# Bind("EventDate") %>'></asp:Label>
</ItemTemplate>

    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
    <ItemStyle HorizontalAlign="Center" Wrap="False" />
</asp:TemplateField>


<asp:TemplateField HeaderText="Access Date">
<ItemTemplate>
<asp:Label id="lblReleaseDT" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:Label> 
</ItemTemplate>
    <ItemStyle HorizontalAlign="Center" Wrap="False" />
</asp:TemplateField>


<asp:TemplateField HeaderText="Accessible For">


    <ItemTemplate>
        <asp:Label ID="lblAvail_For" runat="server"  
            Text='<%# Bind("Avail_For") %>'></asp:Label>
    </ItemTemplate>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField>
<HeaderTemplate>
<asp:Label id="lblEditH" runat="server" Text="Edit"></asp:Label> 
</HeaderTemplate>
<ItemTemplate>
&nbsp;<asp:LinkButton id="lblEdit" onclick="lblView_Click" runat="server">Edit</asp:LinkButton> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="ESM_ID" Visible="False">
<ItemTemplate>
<asp:Label id="lblESM_ID" runat="server" Text='<%# Bind("ESM_ID") %>' ></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
</columns>
                                <selectedrowstyle backcolor="Wheat" />
                                <headerstyle cssclass="gridheader_pop" height="25px" horizontalalign="Center" verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                           <br />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      
    </table>
    <asp:HiddenField ID="hfFlag" runat="server" value="1"/>  

                

            </div>
        </div>
    </div>

</asp:Content>

