<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="svSch_Parent_AttRemark.aspx.vb" Inherits="SchoolVisit_svSch_Parent_AttRemark"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="cphMasterpage" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkControl") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }


        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var oWnd = radopen(url, "pop_form");
            <%--result = window.showModalDialog("svPopupForm.aspx?multiselect=true&ID=ENQ", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EQS_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }--%>
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
          
            var arg = args.get_argument();

            if (arg) {
               
                //NameandCode = arg.Result.split('___');
                
                document.getElementById('<%=h_EQS_IDs.ClientID %>').value = arg.Result;

               <%-- if (NameandCode.length > 2) {
                    document.getElementById('TabContainer1_TabPanel2_HF_SIBLING_TYPE').value = NameandCode[2];

                     <%-- __doPostBack('<% =txtSibno.ClientID%>', 'TextChanged');--%>
                }
            }

    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_form" runat="server" Behaviors="Close,Move"   OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>                
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Update Attendance / Remarks
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <div id="lblError" runat="server"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Event Category</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCat" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                    <td align="left" width="20%"><span class="field-label"> Title</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTitle" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                </tr>


                                <tr runat="server" id="tr_staff">
                                    <td align="left" width="20%"><span class="field-label">Select Staff</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlEMP_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Event Date</span>
                                    </td>
                                    <td width="30%">
                                        <asp:Panel ID="plEventDt" runat="server" ScrollBars="Vertical">
                                            <asp:RadioButtonList ID="rbEventDt" runat="server" AutoPostBack="True">
                                            </asp:RadioButtonList>
                                        </asp:Panel>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Time Slot</span></td>

                                    <td width="30%">
                                        <asp:Panel ID="plTimeSlot" runat="server"
                                            ScrollBars="Vertical">
                                            <asp:RadioButtonList ID="rbTimeSlot" runat="server" AutoPostBack="True">
                                            </asp:RadioButtonList>
                                        </asp:Panel>
                                    </td>
                                </tr>

                            </table>


                            <table id="tbl_test" align="center"
                                cellpadding="5" cellspacing="0" width="100%">


                                <tr>
                                    <td align="center" valign="top">
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button"
                                            Text="Update" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" valign="top">
                                        <asp:Panel ID="plStaff_Parent" runat="server" ScrollBars="Vertical">

                                            <asp:GridView ID="gvStaff_Parent" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                PageSize="20" Width="100%">
                                                <RowStyle CssClass="griditem" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <HeaderTemplate>
                                                            <input id="Checkbox1" name="chkAL" onclick="change_chk_state(this);"
                                                                type="checkbox" value="Check All" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("PSV_ID") %>' />
                                                            <asp:Label ID="lblTrack_id" runat="server" Text='<%# Bind("PSV_TRACK_ID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Parent Name">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                                        </ItemTemplate>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Child Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblChildName" runat="server" Text='<%# Bind("Child_Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Contact No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblContactNo" runat="server" Text='<%# Bind("Contact_No") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email Address">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmailAddress" runat="server" Text='<%# Bind("EMAIL_ID") %>'></asp:Label>

                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Date &amp; Time">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDate" runat="server" Text='<%# bind("Visit_date") %>'></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblTime" runat="server" Text='<%# bind("Visit_time") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Attendance">

                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlAtt" runat="server" Font-Names="Verdana"
                                                                Font-Size="10px">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblEAP_ID" runat="server" Text='<%# Bind("EAP_ID") %>' Visible="false"></asp:Label>

                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Parent Remarks">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtParRemark" runat="server" ReadOnly="True"
                                                                Text='<%# bind("ParRemark") %>'
                                                                TextMode="MultiLine" Width="150px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Staff Remarks">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtStaffRemark" runat="server" Text='<%# bind("StaffRemark") %>'
                                                                TextMode="MultiLine" Width="150px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText">
                                                            </asp:TextBox><ajaxToolkit:FilteredTextBoxExtender ID="ftbStaffRemark" runat="server"
                                                                TargetControlID="txtStaffRemark"
                                                                FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                                ValidChars="@#?%&,;.'/ ">
                                                            </ajaxToolkit:FilteredTextBoxExtender>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle BackColor="Wheat" />
                                                <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>



                <asp:HiddenField ID="hfEM_ID" runat="server" Value="0" />
                <asp:HiddenField ID="h_EQS_IDs" runat="server" />

                </div></div></div>
</asp:Content>

