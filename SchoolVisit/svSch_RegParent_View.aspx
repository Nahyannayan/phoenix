<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="svSch_RegParent_View.aspx.vb"
    Inherits="SchoolVisit_svSchedule_Event_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function Hideme() {


            var hfFlag = document.getElementById('<%=hfFlag.ClientID %>')

        if (hfFlag.value == '0') {
            //document.getElementById('I18').style.display = "none";
            //document.getElementById('I19').style.display = "none";
            //document.getElementById('I20').style.display = "none";
            //document.getElementById('I21').style.display = "none";
            //document.getElementById('I22').style.display = "none";
            document.getElementById('tbl_view').style.display = "none";
            
            
            document.getElementById('<%=hfFlag.ClientID %>').value = "1";
            document.getElementById('<%=lbtnShow.ClientID %>').innerText = "Show";


        }
        else {
            //document.getElementById('I18').style.display = "block";
            //document.getElementById('I19').style.display = "block";
            //document.getElementById('I20').style.display = "block";
            //document.getElementById('I21').style.display = "block";
            //document.getElementById('I22').style.display = "block";
            document.getElementById('tbl_view').style.display = "table";

            document.getElementById('<%=hfFlag.ClientID %>').value = "0";
            document.getElementById('<%=lbtnShow.ClientID %>').innerText = "Hide";
        }


    }

    </script>


    <%-- <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title">
                <asp:Literal id="ltHeader" runat="server" Text="SCHEDULE REGISTERED PARENT"></asp:Literal></td>
        </tr>
    </table>--%>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Search Events
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table id="tbl_test" align="center" border="0"
                                cellpadding="5" cellspacing="0" width="100%">
                                 <tr>
                                                <td align="right"  >

                                                    <asp:LinkButton ID="lbtnShow" runat="server" Text="Hide"
                                                        OnClientClick="javascript:Hideme();return false;">
                                                    </asp:LinkButton>

                                                </td>


                                            </tr>
                                <tr>
                                    <td align="left" valign="middle">
                                        <table id="tbl_view" align="center" cellpadding="5" cellspacing="0"
                                            style="width: 100%; border-collapse: collapse;">
                                           
                                            <tr id="I18" >
                                                <td align="left" width="20%"><span class="field-label">Event Category</span></td>

                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlEM_ID" runat="server"></asp:DropDownList></td>

                                            </tr>
                                            <tr id="I19">
                                                <td align="left"><span class="field-label">Title</span>
                                                </td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtTitle" runat="server" ></asp:TextBox>
                                                </td>

                                            </tr>


                                            <tr id="I20" >
                                                <td align="left"><span class="field-label">Start Date</span>
                                                </td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtEventStartDt" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnEventStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />(dd/mmm/yyyy)

                                                </td>
                                                <td align="left"><span class="field-label"> End Date</span>
                                                </td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtEventEndDt" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnEventEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        />(dd/mmm/yyyy) 
                                                </td>
                                            </tr>
                                            <tr id="I21" >
                                                <td align="left"><span class="field-label">Access Start Date</span>
                                                </td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtAccessStartDt" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgbtnAccessStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                                         />(dd/mmm/yyyy)

                                                </td>
                                                <td align="left"><span class="field-label">Access End Date</span>
                                                </td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtAccessEndDt" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgbtnAccessEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        />
                                                    (dd/mmm/yyyy) 
                                                </td>
                                            </tr>
                                            <tr id="I22" >
                                                <td align="left"><span class="field-label">Accessible For</span></td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddlAccessFor" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                               
                                            </tr>
                                             <tr>
                                                 <td align="center" colspan="4" >
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"
                                                         />
                                                </td>
                                            </tr>
                                            
                                        </table>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgbtnAccessStartDt" TargetControlID="txtAccessStartDt">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgbtnAccessEndDt" TargetControlID="txtAccessEndDt">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEventEndDt" TargetControlID="txtEventEndDt">
                                        </ajaxToolkit:CalendarExtender>

                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEventStartDt" TargetControlID="txtEventStartDt">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="left" valign="middle"></td>
                                </tr>
                                <tr >
                                    <td align="left" valign="top">
                                        <asp:GridView ID="gvSchedule" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"  />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblR1" runat="server" Text='<%# bind("R1") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Event Category">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEventType" runat="server" Text='<%# Bind("EM_TITLE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Title">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblESM_DISPLAY_TITLE" runat="server"
                                                            Text='<%# Bind("ESM_DISPLAY_TITLE") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Event Date">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEventDate" runat="server" Text='<%# Bind("EventDate") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Access Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReleaseDT" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Accessible For">


                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvail_For" runat="server"
                                                            Text='<%# Bind("Avail_For") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Allocate Staff">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAllocateH" runat="server" Text="Allocate Staff"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblAllocate" OnClick="lblView_Click" runat="server">Allocate</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ESM_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblESM_ID" runat="server" Text='<%# Bind("ESM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="hfFlag" runat="server" Value="1" />

            </div>
        </div>
    </div>

</asp:Content>

