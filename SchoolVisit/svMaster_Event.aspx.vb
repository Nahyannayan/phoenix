Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class svMaster_Event
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
          
                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

              
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S425010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("EM_ID") = 0
                    btnUpdateEvent.Visible = False

                    gridbind()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception

                lblError.InnerHtml = "Request could not be processed "
            End Try

        End If


    End Sub

    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Dim ds As New DataSet

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV.GETEVENT_M", params)
            If ds.Tables(0).Rows.Count > 0 Then

                gvCommonEmp.DataSource = ds.Tables(0)
                gvCommonEmp.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                '  ds.Tables(0).Rows(0)(2) = True
                gvCommonEmp.DataSource = ds.Tables(0)
                Try
                    gvCommonEmp.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCommonEmp.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCommonEmp.Rows(0).Cells.Clear()
                gvCommonEmp.Rows(0).Cells.Add(New TableCell)
                gvCommonEmp.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCommonEmp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCommonEmp.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEM_ID As New Label
        Dim lblEM_DESCR As New Label
        Dim lblEM_TITLE As New Label


        lblEM_ID = TryCast(sender.FindControl("lblEM_ID"), Label)
        lblEM_TITLE = TryCast(sender.FindControl("lblEM_TITLE"), Label)
        lblEM_DESCR = TryCast(sender.FindControl("lblEM_DESCR"), Label)


        txtTitle.Text = lblEM_TITLE.Text
        txtDescr.Text = lblEM_DESCR.Text
        ViewState("EM_ID") = lblEM_ID.Text

        btnAddEvent.Visible = False

        btnUpdateEvent.Visible = True
        btnCancel.Visible = True
        gvCommonEmp.Columns(3).Visible = False

    End Sub




    Protected Sub btnAddEvent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEvent.Click
        ViewState("EM_ID") = 0
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
            gridbind()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record saved successfully !!!</div>"
        End If
    End Sub

    Protected Sub btnUpdateEvent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateEvent.Click
        Dim errormsg As String = String.Empty
        If callTransaction(errormsg) <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else
            gridbind()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        resetall()
    End Sub

    Private Function callTransaction(ByRef errormsg As String) As Integer


        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction



        Dim EM_ID As String = ViewState("EM_ID")

        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try


                Dim param(6) As SqlParameter
                param(0) = New SqlParameter("@EM_ID", EM_ID)
                param(1) = New SqlParameter("@EM_BSU_ID", Session("sBsuid"))
                param(2) = New SqlParameter("@EM_TITLE", txtTitle.Text.Trim)
                param(3) = New SqlParameter("@EM_DESCR", txtDescr.Text.Trim)
                param(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "SV.SAVEEVENT_M", param)
                Dim ReturnFlag As Integer = param(4).Value



                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("EM_ID") = "0"
                    ViewState("datamode") = "none"

                    callTransaction = "0"
                    resetall()
                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-1" Then
                    errormsg = "Record cannot be saved.Duplicate title entry not allowed !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function
    Sub resetall()
        gvCommonEmp.Columns(3).Visible = True
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "none")
        btnAddEvent.Visible = True
        txtDescr.Text = ""
        txtTitle.Text = ""
        btnUpdateEvent.Visible = False
        btnCancel.Visible = False
        ViewState("EM_ID") = 0

    End Sub

  
  
  
End Class
