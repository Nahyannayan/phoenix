Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Partial Class SchoolVisit_svSchedule_Event_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S888805") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    ViewState("ESM_ID") = 0
                    ViewState("ESM_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    GETSCHEDULE_BASIC_INFO()
                    reset_control()
                    getEvent_Dates()


                End If
                ' Call bindSchedule_M()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub
    Private Sub GETSCHEDULE_BASIC_INFO()
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        ltTitle.Text = ""
        hfEM_ID.Value = "0"
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@ESM_ID", ViewState("ESM_ID"))

        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "SV.GETEVENT_SCHEDULE_BASIC_INFO", PARAM)
            If dataread.HasRows = True Then

                While dataread.Read
                    ltTitle.Text = "<font color='black' style='font-weight:bold; font-size:11px;'>" & Convert.ToString(dataread("ESM_DISPLAY_TITLE")) & "</font>"
                    hfEM_ID.Value = Convert.ToString(dataread("ESM_EM_ID"))
                End While
            End If

        End Using

    End Sub
    Private Sub getEvent_Dates()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ESDW_ESM_ID", ViewState("ESM_ID"))
        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETVALID_DATES", param)
            If dataread.HasRows = True Then
                ddlEventDate.DataSource = dataread
                ddlEventDate.DataTextField = "VISIT_DT"
                ddlEventDate.DataValueField = "ESDW_ID"
                ddlEventDate.DataBind()
            Else
                ddlEventDate.Items.Clear()

            End If

        End Using
        ddlEventDate_SelectedIndexChanged(ddlEventDate, Nothing)



    End Sub
    Protected Sub ddlEventDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventDate.SelectedIndexChanged
        bindTimeSlot()
    End Sub
    Private Sub bindTimeSlot()

        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ESTW_ESDW_ID", ddlEventDate.SelectedValue)
            param(1) = New SqlParameter("@ESTW_ESM_ID", ViewState("ESM_ID"))
            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "SV.GETVAILD_SLOT", param)
            If ds.Tables(0).Rows.Count > 0 Then
                gvTime.DataSource = ds.Tables(0)
                gvTime.DataBind()

                For i As Integer = 0 To gvTime.Rows.Count - 1
                    Dim rbTimeSlot As RadioButton = CType(gvTime.Rows(i).FindControl("rbTimeSlot"), RadioButton)
                    Dim lblESTW_ID As Label = CType(gvTime.Rows(i).FindControl("lblESTW_ID"), Label)
                    If rbTimeSlot.Enabled = True Then
                        rbTimeSlot.Checked = True
                        bindStaffSlot(lblESTW_ID.Text)

                        ViewState("ESTW_ID") = lblESTW_ID.Text
                        If rbReg.checked = True Then
                            gridbind()
                        End If
                        Exit For

                    End If


                Next

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(3) = False


                gvTime.DataSource = ds.Tables(0)
                Try
                    gvTime.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvTime.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvTime.Rows(0).Cells.Clear()
                gvTime.Rows(0).Cells.Add(New TableCell)
                gvTime.Rows(0).Cells(0).ColumnSpan = columnCount
                gvTime.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvTime.Rows(0).Cells(0).Text = "Record not available !!!"
                bindStaffSlot(0)
                If rbReg.checked = True Then
                    gridbind()
                End If

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindStaffSlot(ByVal ESTW_ID As String)

        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ESTW_ESDW_ID", ddlEventDate.SelectedValue)
            param(1) = New SqlParameter("@ESTW_ID", ESTW_ID)
            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "SV.GETVAILD_STAFF_LIST", param)
            If ds.Tables(0).Rows.Count > 0 Then
                gvStaff.DataSource = ds.Tables(0)
                gvStaff.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(2) = False


                gvStaff.DataSource = ds.Tables(0)
                Try
                    gvStaff.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStaff.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStaff.Rows(0).Cells.Clear()
                gvStaff.Rows(0).Cells.Add(New TableCell)
                gvStaff.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStaff.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStaff.Rows(0).Cells(0).Text = "Record not available !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private ReadOnly Property StaffSelectedIndex() As Integer
        Get
            If String.IsNullOrEmpty(Request.Form("StaffGRP")) Then
                Return -1
            Else
                Return Convert.ToInt32(Request.Form("StaffGRP"))
            End If
        End Get
    End Property
    Protected Sub gvStaff_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStaff.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Grab a reference to the Literal control
            Dim output As Literal = CType(e.Row.FindControl("RadioButtonStaff"), Literal)
            Dim lblESSW_ID As Label = CType(e.Row.FindControl("lblESSW_ID"), Label)

            ' Output the markup except for the "checked" attribute
            output.Text = String.Format("<input type=""radio"" name=""StaffGRP"" id=""RowSelector{0}"" value=""{0}""", e.Row.RowIndex) ' lblESSW_ID.Text)

            ' OPTIONAL: If you want to have the first radio button selected on the first
            ' page load, use the following if statement instead of the one below
            'If SuppliersSelectedIndex = e.Row.RowIndex OrElse (Not Page.IsPostBack AndAlso e.Row.RowIndex = 0) Then

            If e.Row.RowIndex = 0 Then
                output.Text &= " checked=""checked"""
            End If

            '' See if we need to add the "checked" attribute
            'If StaffSelectedIndex = e.Row.RowIndex Then
            '    output.Text &= " checked=""checked"""
            'End If

            ' Add the closing tag
            output.Text &= " />"
        End If
    End Sub
    Protected Sub rbTimeSlot_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblESTW_ID As New Label

        deselect_rbgvTime()

        Dim SenderRB As RadioButton = sender
        SenderRB.Checked = True
        lblESTW_ID = TryCast(sender.FindControl("lblESTW_ID"), Label)
        bindStaffSlot(lblESTW_ID.Text) '--------------------------------------
        ViewState("ESTW_ID") = lblESTW_ID.Text
        gridbind()
        'Reflect the event
        '---------------------------------------

        '  fire_visible_window()



    End Sub
    Sub deselect_rbgvTime()
        Dim gvr As GridViewRow
        Dim i As Int32
        'deslect all radiobutton in gridview
        For Each gvr In gvTime.Rows
            Dim rb As RadioButton
            rb = CType(gvTime.Rows(i).FindControl("rbTimeSlot"), RadioButton)
            rb.Checked = False
            i += 1

        Next
    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            Dim PARAM(4) As SqlParameter
            Dim txtSearch As New TextBox
            Dim Str_ParentName As String = String.Empty
            Dim str_ChildName As String = String.Empty
            Dim str_EMAILID As String = String.Empty
            Dim str_ContactNo As String = String.Empty
            Dim str_StaffName As String = String.Empty
            Dim rbSELECT As New RadioButton
            Dim lblESTW_ID As New Label
            Dim PSV_ESTW_ID As String = "0"
            Dim PSV_ESDW_ID As String = String.Empty
            If gvStaff_Parent.Rows.Count > 0 Then
                txtSearch = gvStaff_Parent.HeaderRow.FindControl("txtParentName")
                Str_ParentName = " AND PARENT_NAME like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvStaff_Parent.HeaderRow.FindControl("txtChildName")
                str_ChildName = " AND CHILD_NAME like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvStaff_Parent.HeaderRow.FindControl("txtEmailAddress")
                str_EMAILID = " AND EMAIL_ID like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvStaff_Parent.HeaderRow.FindControl("txtContactNo")
                str_ContactNo = " AND CONTACT_NO like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvStaff_Parent.HeaderRow.FindControl("txtAllStaff")
                str_StaffName = " AND STAFF_NAME like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

            End If

            If gvTime.Rows.Count > 0 Then
                For Each GVROW As GridViewRow In gvTime.Rows
                    rbSELECT = CType(GVROW.FindControl("rbTimeSlot"), RadioButton)
                    lblESTW_ID = CType(GVROW.FindControl("lblESTW_ID"), Label)
                    If Not rbSELECT Is Nothing Then
                        If rbSELECT.Checked = True Then
                            PSV_ESTW_ID = lblESTW_ID.Text.Trim
                        End If
                    Else
                        PSV_ESTW_ID = "0"
                    End If

                Next
            End If

            PARAM(0) = New SqlParameter("@PSV_ESTW_ID", ViewState("ESTW_ID"))
            PARAM(1) = New SqlParameter("@PSV_ESDW_ID", ddlEventDate.SelectedValue)
            PARAM(2) = New SqlParameter("@FILTER_COND", Str_ParentName + str_ChildName + str_EMAILID + str_ContactNo + str_StaffName)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV.GETPARENT_SLOT_DETAILS", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvStaff_Parent.DataSource = ds.Tables(0)
                gvStaff_Parent.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvStaff_Parent.DataSource = ds.Tables(0)
                Try
                    gvStaff_Parent.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStaff_Parent.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStaff_Parent.Rows(0).Cells.Clear()
                gvStaff_Parent.Rows(0).Cells.Add(New TableCell)
                gvStaff_Parent.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStaff_Parent.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStaff_Parent.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnSearchParentName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchChildName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchContactNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchEmailAddress_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchAllStaff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
  
    Private Sub reset_control()
        lblError.InnerText = ""
        gvAddParent.visible = False
        gvStaff_Parent.visible = True
        btnSave.visible = False
        btnupdate.visible = True
        spAddP.visible = False

        lbtnAddParent.visible = False
    End Sub
  
    Protected Sub rbReg_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbReg.CheckedChanged
        gvAddParent.Visible = False
        gvStaff_Parent.Visible = True
        btnSave.Visible = False
        btnUpdate.Visible = True
        btnCancel.Visible = True
        spAddP.Visible = False

        lbtnAddParent.Visible = False
        bindTimeSlot()
    End Sub
    Protected Sub rbUnReg_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbUnReg.CheckedChanged
        gvAddParent.visible = True
        gvStaff_Parent.visible = False
        btnSave.visible = True
        btnupdate.visible = False
        spAddP.visible = True
        btnSave.visible = False
        btnCancel.visible = False
        lbtnAddParent.visible = True
        GridBindStudents("0|")
    End Sub
    Protected Sub lbtnENQ_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If h_EQS_IDs.Value <> "" Then
            GridBindStudents(h_EQS_IDs.Value)
        End If
    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)

        Dim ds As New DataSet
        ds = GetSelectedStudents_LIST(vSTU_IDs)
        If ds.Tables(0).Rows.Count > 0 Then

            gvAddParent.DataSource = ds.Tables(0)
            gvAddParent.DataBind()

            btnSave.visible = True
            btnCancel.visible = True

        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

            'start the count from 1 no matter gridcolumn is visible or not
            ' ds.Tables(0).Rows(0)(6) = True

            gvAddParent.DataSource = ds.Tables(0)
            Try
                gvAddParent.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvAddParent.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvAddParent.Rows(0).Cells.Clear()
            gvAddParent.Rows(0).Cells.Add(New TableCell)
            gvAddParent.Rows(0).Cells(0).ColumnSpan = columnCount
            gvAddParent.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvAddParent.Rows(0).Cells(0).Text = "No record added yet !!! "
            btnSave.visible = False
            btnCancel.visible = False
        End If

      
    End Sub
    Private Function GetSelectedStudents_LIST(Optional ByVal vSTU_IDs As String = "") As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@EQS_ID", vSTU_IDs)


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV.GETENQUIRY_LIST_NEW", PARAM)

       
        Return ds
    End Function

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If StaffSelectedIndex < 0 Then
            ' ChooseSupplierMsg.Visible = True
        Else
            ' Send the user to ~/Filtering/ProductsForSupplierDetails.aspx

            Dim PSV_IDs As String = String.Empty
            Dim errormsg As String = String.Empty
            Dim errNo As Integer = 0
            lblError.InnerText = ""
            For Each gvChk As GridViewRow In gvStaff_Parent.Rows
                Dim chk As HtmlInputCheckBox = CType(gvChk.FindControl("chkControl"), HtmlInputCheckBox)
                If chk.Checked = True Then
                    PSV_IDs += chk.Value + "|"
                End If
            Next
            If PSV_IDs = "" Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Please select the record(s) to be updated !!!</div>"
                Exit Sub
            End If

            errNo = callTransaction_RegParent(errormsg, PSV_IDs)
            If errNo <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
            Else
                gridbind()
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
            End If
        End If
    End Sub

    Private Function callTransaction_RegParent(ByRef errormsg As String, ByVal PSV_IDs As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ESSW_ID As Integer = Convert.ToInt32(gvStaff.DataKeys(StaffSelectedIndex).Value)
        Dim tran As SqlTransaction
        Dim ESM_ID As String = ViewState("ESM_ID")
        Dim ESTW_ID As String = ViewState("ESTW_ID")
        Dim ESDW_ID As String = ddlEventDate.SelectedValue
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                tran = CONN.BeginTransaction("simpleTran")
                Dim param(8) As SqlParameter
                param(0) = New SqlParameter("@ESM_ID", ESM_ID)
                param(1) = New SqlParameter("@ESTW_ID", ESTW_ID)
                param(2) = New SqlParameter("@ESDW_ID", ESDW_ID)
                param(3) = New SqlParameter("@ESSW_ID", ESSW_ID)
                param(4) = New SqlParameter("@PSV_IDs", PSV_IDs)
                param(5) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "SV.SAVEPARENT_SCHOOL_VISIT_STAFF", param)
                Dim ReturnFlag As Integer = param(5).Value

                If ReturnFlag <> 0 Then
                    callTransaction_RegParent = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("datamode") = "none"

                    callTransaction_RegParent = "0"

                End If
            Catch ex As Exception
                callTransaction_RegParent = "1"
                errormsg = ex.Message
            Finally
                If callTransaction_RegParent <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try
        End Using





    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
       

        Dim errormsg As String = String.Empty
        Dim errNo As Integer = 0
        lblError.InnerText = ""

        errNo = callTransaction_unRegParent(errormsg)
        If errNo = -101 Then 'Record cannot be updated since following date
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
        ElseIf errNo = -102 Then 'Time/Staff has already been set for the following date
        ElseIf errNo <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
        Else
            btnSave.Visible = False
            btnCancel.Visible = False
            gvStaff_Parent.Enabled = True
            'Call bindSchedule_M()
            rbReg.Checked = True
            rbUnReg.Checked = False
            rbReg_CheckedChanged(rbReg, Nothing)
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
    End Sub
    Private Function callTransaction_unRegParent(ByRef errormsg As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction
        Dim ESM_ID As String = ViewState("ESM_ID")
        Dim ESTW_ID As String = ViewState("ESTW_ID")
        Dim ESDW_ID As String = ddlEventDate.SelectedValue
        Dim lblEQS_ID As New Label
        Dim lblEQS_EQM_ENQID As New Label
        Dim EQM_EQS_IDS As New StringBuilder
        Dim ESSW_ID As Integer = Convert.ToInt32(gvStaff.DataKeys(StaffSelectedIndex).Value)

        For Each irow As GridViewRow In gvAddParent.Rows
            lblEQS_EQM_ENQID = CType(irow.FindControl("lblEQS_EQM_ENQID"), Label)
            lblEQS_ID = CType(irow.FindControl("lblEQS_ID"), Label)
            EQM_EQS_IDS.Append(lblEQS_EQM_ENQID.Text & "_" & lblEQS_ID.Text & "|")
        Next
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                tran = CONN.BeginTransaction("simpleTran")
                Dim param(12) As SqlParameter
                param(0) = New SqlParameter("@ESM_ID", ESM_ID)
                param(1) = New SqlParameter("@ESTW_ID", ESTW_ID)
                param(2) = New SqlParameter("@ESDW_ID", ESDW_ID)
                param(3) = New SqlParameter("@ESSW_ID", ESSW_ID)
                param(4) = New SqlParameter("@EQM_EQS_IDS", EQM_EQS_IDS.ToString())
                param(5) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                param(6) = New SqlParameter("@USR_ID", Session("sUsr_id"))
                param(7) = New SqlParameter("@OUT_TRACK_ID", SqlDbType.VarChar, 1000)
                param(7).Direction = ParameterDirection.Output
                param(8) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "SV.SAVEPARENT_SCHOOL_VISIT_UNREG", param)
                Dim ReturnFlag As Integer = param(8).Value
              If ReturnFlag <> 0 Then
                    callTransaction_unRegParent = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("NEW_TRACK_ID") = IIf(TypeOf (param(7).Value) Is DBNull, "0", param(7).Value)


                    ViewState("datamode") = "none"

                    callTransaction_unRegParent = "0"

                End If

            Catch ex As Exception
                callTransaction_unRegParent = "1"
                errormsg = ex.Message
            Finally
                If callTransaction_unRegParent <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try
        End Using
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ViewState("datamode") = "none"

        lblError.InnerHtml = ""
        reset_control()
        ' gvStaff_Parent.Enabled = True
        'Call bindSchedule_M()
    End Sub
End Class
