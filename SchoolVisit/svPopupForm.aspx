<%@ Page Language="VB" AutoEventWireup="false" CodeFile="svPopupForm.aspx.vb" Inherits="SelBussinessUnit" Theme="General" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <base target="_self" />
    
    <script language="javascript" type="text/javascript">

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }


    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
             </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
     <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0"> 
             <tr class="matters">
         <td>Select Academic Year : <asp:DropDownList ID="ddlAcd_id" runat="server">
             </asp:DropDownList>
         </td> <td>Status : <asp:DropDownList ID="ddlStatus" runat="server"
         
          AutoPostBack="True" Width="108px">
                             <asp:ListItem Value="0">ALL</asp:ListItem>
                             <asp:ListItem Value="2">ENQUIRY</asp:ListItem>     
                             <asp:ListItem Value="3">REGISTER</asp:ListItem>                                                   
                             <asp:ListItem Value="4">SCREENING</asp:ListItem>
                             <asp:ListItem Value="5">APPROVAL</asp:ListItem>         
                             <asp:ListItem Value="6">OFFER LETTER</asp:ListItem>                               
                          </asp:DropDownList> 
             </asp:DropDownList>
         </td>
         </tr>
                    <tr valign = "top">
                            <td colspan="2">
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True" SkinID="GridViewPickDetails" PageSize="15">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("eqs_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Select">
                                 
                                        <ItemTemplate>
<input id="chkControl" runat="server" type="checkbox" value='<%# Bind("eqs_id") %>' />
                                        </ItemTemplate>
                                           <HeaderTemplate>
                                          <table><tr><td align="center" colspan="2" 
                                          style="color:#1b80b6;font-size:14px; font-weight:bold;">
     </td></tr><tr><td><input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" /></td></tr></table>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField HeaderText="User Id" >
                                                                             <ItemTemplate>
                                            <asp:Label ID="lbleqs_applno" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                        </ItemTemplate>
                                               <HeaderTemplate>
<table><tr><td align="center" colspan="2" style="color:#1b80b6;font-size:14px; font-weight:bold;">
   Enq. No</td></tr>
<tr><td><asp:TextBox ID="txteqs_applno" runat="server" Width="50px"></asp:TextBox></td>
<td valign="middle"><asp:ImageButton 
                    id="btneqs_applnoSearch"
               OnClick="btneqs_applnoSearch_Click"
                    runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" 
                  ></asp:ImageButton></td></tr></table>
</HeaderTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("enqdate") %>'></asp:Label>&nbsp;<br />
                                        </ItemTemplate>
                                        
                                          <HeaderTemplate>
<table><tr><td align="center" colspan="2" style="color:#1b80b6;font-size:14px; font-weight:bold;">
    Enq. Date</td></tr>
<tr><td>
                      <asp:TextBox ID="txtenqdate" runat="server" Width="60px"></asp:TextBox></td><td valign="middle">
                      <asp:ImageButton 
                      ID="btnenqdateSearch" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnenqdateSearch_Click" 
                                  ></asp:ImageButton></td></tr></table>
</HeaderTemplate>

                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="grm_display">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgrm_display" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                        </ItemTemplate>
                                        
                                          <HeaderTemplate>
<table><tr><td align="center" colspan="2" style="color:#1b80b6;font-size:14px; font-weight:bold;">
  Grade</td></tr>
<tr><td>
                      <asp:TextBox ID="txtgrm_display" runat="server" Width="60px"></asp:TextBox></td><td valign="middle">
                      <asp:ImageButton 
                      ID="btngrm_displaySearch" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                       OnClick="btngrm_displaySearch_Click" 
                                  ></asp:ImageButton></td></tr></table>
</HeaderTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField >
                                    
 <HeaderTemplate>
<table><tr><td align="center" colspan="2" style="color:#1b80b6;font-size:14px; font-weight:bold;">
   Applicant Name</td></tr>
<tr><td>
                       <asp:TextBox ID="txtAPPL_NAME" runat="server" Width="72px"></asp:TextBox></td><td valign="middle">
                      <asp:ImageButton                    
                       ImageUrl="~/Images/forum_search.gif" ID="btnAPPL_NAMESearch" runat="server" ImageAlign="Top"  
                       OnClick="btnAPPL_NAMESearch_Click"
                                  ></asp:ImageButton></td></tr></table>
</HeaderTemplate>
 <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("APPL_NAME") %>'
                                             OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Parent  Name">
                                     <HeaderTemplate>
<table><tr><td align="center" colspan="2" style="color:#1b80b6;font-size:14px; font-weight:bold;">
   Parent Name</td></tr>
<tr><td>
                       <asp:TextBox ID="txtParent_NAME" runat="server" Width="72px"></asp:TextBox></td><td valign="middle">
                      <asp:ImageButton                    
                       ImageUrl="~/Images/forum_search.gif" ID="btnParent_NAMESearch" runat="server" ImageAlign="Top"  
                       OnClick="btnParent_NAMESearch_Click"
                                  ></asp:ImageButton></td></tr></table>
</HeaderTemplate>
                                         <ItemTemplate>
                                             <asp:Label ID="lblParent_NAME" runat="server" Text='<%# Bind("Parent_NAME") %>'></asp:Label>
                                         </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contact No">
                                     <HeaderTemplate>
<table><tr><td align="center" colspan="2" style="color:#1b80b6;font-size:14px; font-weight:bold;">
   Contact No</td></tr>
<tr><td>
                       <asp:TextBox ID="txtContact_No" runat="server" Width="72px"></asp:TextBox></td><td valign="middle">
                      <asp:ImageButton                    
                       ImageUrl="~/Images/forum_search.gif" ID="btnContact_NoSearch" runat="server" ImageAlign="Top"  
                       OnClick="btnContact_NoSearch_Click"
                                  ></asp:ImageButton></td></tr></table>
</HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:Label ID="lblcontact_no" runat="server" Text='<%# Bind("Contact_no") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                         
                        </td>
                    </tr>
                    <tr> 
                        <td align="center" colspan="3" >
                         <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                          <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        </td>
                    </tr>
                </table>
            
    </form>
</body>
</html>