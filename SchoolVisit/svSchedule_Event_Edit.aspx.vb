Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class SchoolVisit_svSchedule_Event_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S425015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    ViewState("ESM_ID") = 0
                    getEvent_Group()
                    BIND_ACCESS_FOR()

                    If ViewState("datamode") = "view" Then
                        btnSave.Visible = False
                        btnCancel.Visible = False
                        ViewState("ESM_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        reset_control(False)


                    ElseIf ViewState("datamode") = "add" Then
                        btnSave.Visible = True
                        btnCancel.Visible = True
                        btnEdit.Visible = False
                        reset_control(True)
                        ViewState("ESM_ID") = "0"
                    End If
                    Call bindSchedule_M()
                    Call gridbind()





                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub

    Private Sub BIND_ACCESS_FOR()
        Try
            ddlAccessFor.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETEVENT_ACCESS_FOR")
                If datareader.HasRows = True Then
                    While datareader.Read
                        ddlAccessFor.Items.Add(New ListItem(Convert.ToString(datareader("EAF_DESCR")), Convert.ToString(datareader("EAF_ID"))))
                    End While
                End If
            End Using
            ddlAccessFor.Items.Add(New ListItem("Select", "0"))
            ddlAccessFor.ClearSelection()
            ddlAccessFor.Items.FindByValue("0").Selected = True

        Catch ex As Exception

        End Try
    End Sub
    Private Sub getEvent_Group()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[SV].[GETEVENT_M]", param)
            If dataread.HasRows = True Then
                ddlEventType.DataSource = dataread
                ddlEventType.DataTextField = "EM_TITLE"
                ddlEventType.DataValueField = "EM_ID"
                ddlEventType.DataBind()
            Else
                ddlEventType.Items.Clear()

            End If

        End Using

    End Sub
    Private Sub bindSchedule_M()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ESM_ID", ViewState("ESM_ID"))
        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETSCHEDULE_M_EDIT", param)
            If dataread.HasRows = True Then

                While dataread.Read


                    txtTitle.Text = Convert.ToString(dataread("ESM_DISPLAY_TITLE"))
                    txtDescr.Text = Convert.ToString(dataread("ESM_DESCR"))
                    chkShow.Checked = Convert.ToBoolean(dataread("ESM_bSHOW_DESCR"))
                    txtEventStartDt.Text = Convert.ToString(dataread("ESM_STARTDT"))
                    txtEventEndDt.Text = Convert.ToString(dataread("ESM_ENDDT"))
                    txtAccessStartDt.Text = Convert.ToString(dataread("ESM_RELEASEDT"))
                    txtAccessEndDt.Text = Convert.ToString(dataread("ESM_HIDEDT"))

                    If Not ddlAccessFor.Items.FindByValue(Convert.ToString(dataread("ESM_EAF_ID"))) Is Nothing Then
                        ddlAccessFor.ClearSelection()
                        ddlAccessFor.Items.FindByValue(Convert.ToString(dataread("ESM_EAF_ID"))).Selected = True

                    End If

                    If Not ddlEventType.Items.FindByValue(Convert.ToString(dataread("ESM_EM_ID"))) Is Nothing Then
                        ddlEventType.ClearSelection()
                        ddlEventType.Items.FindByValue(Convert.ToString(dataread("ESM_EM_ID"))).Selected = True

                    End If



                End While


            End If

        End Using

    End Sub
   


    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet

            Dim PARAM(2) As SqlParameter


            PARAM(0) = New SqlParameter("@ESDW_ESM_ID", ViewState("ESM_ID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV.GETSCHEDULE_DAY_WISE_GRIDBIND", PARAM)


            If ds.Tables(0).Rows.Count > 0 Then

                gvSchedule.DataSource = ds.Tables(0)
                gvSchedule.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(2) = False


                gvSchedule.DataSource = ds.Tables(0)
                Try
                    gvSchedule.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSchedule.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSchedule.Rows(0).Cells.Clear()
                gvSchedule.Rows(0).Cells.Add(New TableCell)
                gvSchedule.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSchedule.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSchedule.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub gvSchedule_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSchedule.PageIndexChanging
        gvSchedule.PageIndex = e.NewPageIndex
        gridbind()
    End Sub




   
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub







    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim errormsg As String = String.Empty
        hfFORCEDELETE.Value = 0
        Dim errNo As Integer = 0
        lblError.InnerText = ""
        divErr_force.InnerText = ""
        errNo = callTransaction(errormsg)
        If errNo = -101 Then 'Record cannot be updated since following date


            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; text-align: left; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"


        ElseIf errNo = -102 Then 'Time/Staff has already been set for the following date


            divErr_force.InnerHtml = "<div style=' text-align: left; color:red;padding-top:15pt;'>" & errormsg & "</div>"
            mdlForce_delete.Show()

        ElseIf errNo <> 0 Then


            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; text-align: left; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            btnSave.Visible = False
            btnCancel.Visible = False
            btnEdit.Visible = True
            reset_control(False)
            gvSchedule.Enabled = True
            Call bindSchedule_M()
            Call gridbind()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; text-align: left; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
    End Sub



    Private Function callTransaction(ByRef errormsg As String) As Integer


        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction
        Dim ESM_ID As String = ViewState("ESM_ID")
        Dim ESM_EM_ID As String = ddlEventType.SelectedValue
        Dim ESM_DISPLAY_TITLE As String = txtTitle.Text.Trim
        Dim ESM_DESCR As String = txtDescr.Text.Trim
        Dim ESM_bSHOW_DESCR As Boolean = chkShow.Checked
        Dim ESM_STARTDT As String = txtEventStartDt.Text
        Dim ESM_ENDDT As String = txtEventEndDt.Text
        Dim ESM_RELEASEDT As String = txtAccessStartDt.Text
        Dim ESM_HIDEDT As String = txtAccessEndDt.Text
        Dim ESM_EAF_ID As String = ddlAccessFor.SelectedValue
        Dim XML_DATA As String = String.Empty

        XML_DATA = String.Format("<ESM_M><ESM_bSHOW_DESCR='{0}' ESM_STARTDT='{1}' ESM_ENDDT='{2}' ESM_RELEASEDT='{3}'  ESM_HIDEDT='{4}' ESM_EAF_ID='{5}' /></ESM_M>", _
chkShow.Checked, txtEventStartDt.Text, txtEventEndDt.Text, txtAccessStartDt.Text, txtAccessEndDt.Text, ddlAccessFor.SelectedValue)


        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim param(20) As SqlParameter
                param(0) = New SqlParameter("@ESM_ID", ESM_ID)
                param(1) = New SqlParameter("@ESM_EM_ID", ESM_EM_ID)
                param(2) = New SqlParameter("@ESM_DISPLAY_TITLE", ESM_DISPLAY_TITLE)
                param(3) = New SqlParameter("@ESM_DESCR", ESM_DESCR)
                param(4) = New SqlParameter("@ESM_bSHOW_DESCR", ESM_bSHOW_DESCR)
                param(5) = New SqlParameter("@ESM_BSU_ID", Session("sBsuid"))
                param(6) = New SqlParameter("@ESM_STARTDT", ESM_STARTDT)
                param(7) = New SqlParameter("@ESM_ENDDT", ESM_ENDDT)
                param(8) = New SqlParameter("@ESM_RELEASEDT", ESM_RELEASEDT)
                param(9) = New SqlParameter("@ESM_HIDEDT", ESM_HIDEDT)
                param(10) = New SqlParameter("@ESM_EAF_ID", ESM_EAF_ID)
                param(11) = New SqlParameter("@USR_ID", Session("sUsr_id"))
                param(12) = New SqlParameter("@ClientBrw", AuditInfo.ClientBrw)
                param(13) = New SqlParameter("@clientIp", AuditInfo.ClientIP)
                param(14) = New SqlParameter("@XML_DATA", XML_DATA)

                param(15) = New SqlParameter("@bFORCEDELETE", IIf(hfFORCEDELETE.Value = 0, False, True))

                param(16) = New SqlParameter("@OUTPUT_ESM_ID", SqlDbType.Int)
                param(16).Direction = ParameterDirection.Output
                param(17) = New SqlParameter("@OUTPUT_MSG", SqlDbType.VarChar, 2000)
                param(17).Direction = ParameterDirection.Output



                param(18) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(18).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[SV].[SAVESCHEDULE_M]", param)
                Dim ReturnFlag As Integer = param(18).Value


                If ReturnFlag = -101 Then
                    callTransaction = "-101"
                    errormsg = IIf(TypeOf (param(17).Value) Is DBNull, "", param(17).Value)
                ElseIf ReturnFlag = -102 Then
                    callTransaction = "-102"
                    errormsg = IIf(TypeOf (param(17).Value) Is DBNull, "", param(17).Value)


                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("ESM_ID") = IIf(TypeOf (param(16).Value) Is DBNull, 0, param(16).Value)


                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction = "-101" Then
                    ' errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction = "-102" Then
                    ' errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function



    Protected Sub gvSchedule_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSchedule.RowCommand
        Dim ARG As String = e.CommandArgument

        If e.CommandName = "Status_Date" Then
            changeStatus(ARG)
        End If


    End Sub
    Sub changeStatus(ByVal ESDW_ID As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ESDW_ID", ESDW_ID)
            pParms(1) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            pParms(2) = New SqlParameter("@ClientBrw", AuditInfo.ClientBrw)
            pParms(3) = New SqlParameter("@clientIp", AuditInfo.ClientIP)

            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "SV.SAVESCHEDULE_DAY_WISE_DISABLE", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value

            If ReturnFlag <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px;  text-align: left; color:red;padding:5pt;background-color:#edf3fa;'>Error Occured While Saving.</div>"

            Else
                gridbind()
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px;  text-align: left; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated Successfully !!!</div>"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "changeStatus")
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px;  text-align: left; color:red;padding:5pt;background-color:#edf3fa;'>Error Occured While Saving.</div>"
        End Try

    End Sub

    Private Sub reset_control(ByVal flag As Boolean)
        If ViewState("datamode") = "add" Then
            ddlEventType.Enabled = True
        Else
            ddlEventType.Enabled = False
        End If

        chkShow.Enabled = flag
        txtTitle.Enabled = flag
        txtDescr.Enabled = flag
        txtEventStartDt.Enabled = flag
        imgBtnEventStartDt.Visible = flag
        txtEventEndDt.Enabled = flag
        imgBtnEventEndDt.Visible = flag
        txtAccessStartDt.Enabled = flag
        imgbtnAccessStartDt.Visible = flag
        txtAccessEndDt.Enabled = flag
        imgbtnAccessEndDt.Visible = flag
        ddlAccessFor.Enabled = flag
        lblError.InnerText = ""
        divErr_force.InnerText = ""

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        btnEdit.Visible = False
        btnCancel.Visible = True
        btnSave.Visible = True
        reset_control(True)
        gvSchedule.Enabled = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ViewState("datamode") = "none"
        btnEdit.Visible = True
        btnCancel.Visible = False
        btnSave.Visible = False
        lblError.InnerHtml = ""
        reset_control(False)
        gvSchedule.Enabled = True
        Call bindSchedule_M()
    End Sub



    Protected Sub lbtnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            Dim lblESDW_ID As New Label
            Dim lblEventDate As New Label
            lblESDW_ID = TryCast(sender.FindControl("lblESDW_ID"), Label)
            lblEventDate = TryCast(sender.FindControl("lblEventDate"), Label)
            ltTime.Text = "Schedule time slot and staff  for " & lblEventDate.Text '<asp:Literal  id="ltTime" runat="server"></asp:Literal>
            lblError.InnerHtml = ""
            hfESDW_ID.Value = lblESDW_ID.Text

            ucST_Settings.INTIAL_CALL()
            ucST_Settings.bind_dates()
            ucST_Settings.reset_state()
            mdlSch_Event.Show()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_force_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave_force.Click
        Dim errormsg As String = String.Empty
        hfFORCEDELETE.Value = 1
        Dim errNo As Integer = 0
        lblError.InnerText = ""
        divErr_force.InnerText = ""
        errNo = callTransaction(errormsg)
        mdlForce_delete.Hide()
        If errNo = -101 Then 'Record cannot be updated since following date


            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; text-align: left; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"


        ElseIf errNo = -102 Then 'Time/Staff has already been set for the following date


            divErr_force.InnerHtml = "<div style=' text-align: left; color:red;padding-top:15pt;'>" & errormsg & "</div>"
            mdlForce_delete.Show()

        ElseIf errNo <> 0 Then


            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; text-align: left; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"

        Else

            btnSave.Visible = False
            btnCancel.Visible = False
            btnEdit.Visible = True
            reset_control(False)

            Call bindSchedule_M()
            Call gridbind()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6; border-radius:6px; text-align: left; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
    End Sub

    Protected Sub btnCancel_Force_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel_Force.Click
        ViewState("datamode") = "none"
        btnEdit.Visible = True
        btnCancel.Visible = False
        btnSave.Visible = False
        Call bindSchedule_M()
        reset_control(False)
        lblError.InnerHtml = ""
        gvSchedule.Enabled = True
        mdlForce_delete.Hide()
    End Sub

    Protected Sub imgclose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgclose.Click
        gridbind()
        mdlSch_Event.Hide()
    End Sub
End Class
