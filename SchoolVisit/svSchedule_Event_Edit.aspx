<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
 CodeFile="svSchedule_Event_Edit.aspx.vb" Inherits="SchoolVisit_svSchedule_Event_Edit" 
 Title="::::GEMS OASIS:::: Online Student Administration System::::"%>
 <%@ Register Src="~/SchoolVisit/UserControl/ucSch_Set_Time.ascx" 
  TagName="uc_Set_Time" TagPrefix="ucST"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

<asp:Button ID="btnShowTime" runat="server" Style="display: none;" Text="show" />
   <ajaxToolkit:ModalPopupExtender ID="mdlSch_Event" runat="server" TargetControlID="btnShowTime" 
    BehaviorID="mdlSch_Event"     PopupControlID="plSch_Event"  BackgroundCssClass="modalBackground"    />
<asp:Button ID="btnForceDelete" runat="server" Style="display: none;" Text="show" />
 <ajaxToolkit:ModalPopupExtender ID="mdlForce_delete" runat="server" TargetControlID="btnForceDelete" 
    BehaviorID="mdlForce_delete"     PopupControlID="plForce_delete"  BackgroundCssClass="modalBackground"    />
    

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal id="ltHeader" runat="server" Text="Schedule Events"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


   
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left"  valign="top">
               <div ID="lblError" runat="server" class="error" ></div></td>
        </tr>
        <tr>
            <td align="left"  valign="top" >
                <table id="tbl_test"  align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr >
                        <td align="left">
                           <table align="left" width="100%">
                        <tr>
                            <td align="left">
                            <span class="field-label">Event Category</span><span class="text-danger">*</span>
                            </td>
                            
                            <td align="left">
                                <asp:DropDownList ID="ddlEventType" runat="server">
                                </asp:DropDownList>
                            </td>
                           <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="field-label">Event Title</span><span class="text-danger">*</span>
                            </td>
                            
                            <td align="left">
                              <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox> 
                            </td>
                           <td></td>
                            <td></td>
                        </tr>
                        
                        
                        <tr>
                            <td align="left">
                                <span class="field-label">Description</span><span class="text-danger">*</span></td>
                            
                            <td align="left">
                              <asp:TextBox ID="txtDescr" runat="server"  Width="80%"  
                                    TextMode="MultiLine"></asp:TextBox> 
                                    <div  class="text-danger"><asp:CheckBox ID="chkShow" runat="server"  Text="Tick to show title description on the login site"/></div>
                            </td>
                           <td></td>
                            <td></td>
                        </tr>
                        
                        
                        <tr>
                            <td align="left">
                                <span class="field-label">Event Start Date</span><span class="text-danger">*</span>
                            </td>
                           
                            <td align="left">
                               <asp:TextBox ID="txtEventStartDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgBtnEventStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                       /><div>(dd/mmm/yyyy)</div>
                            </td>
                            <td align="left">
                                <span class="field-label">Event End Date</span><span class="text-danger">*</span>
                            </td>
                           
                            <td align="left">
                               <asp:TextBox ID="txtEventEndDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgBtnEventEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                       /><div>(dd/mmm/yyyy)</div>
                                    
                            </td>
                        </tr>
                        
                        
                         <tr>
                            <td align="left" rowspan="2">
                             <span class="field-label">Access Start Date</span><span class="text-danger">*</span>
                                <br />
                            </td>
                            
                            <td align="left">
                               <asp:TextBox ID="txtAccessStartDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgbtnAccessStartDt" runat="server" ImageUrl="~/Images/calendar.gif" /><div>(dd/mmm/yyyy)</div>
                                        
                            </td>
                            <td align="left">
                              <span class="field-label">Access End Date</span><span class="text-danger">*</span>
                            </td>
                            
                            <td align="left">
                               <asp:TextBox ID="txtAccessEndDt" runat="server"></asp:TextBox> 
                                 <asp:ImageButton ID="imgbtnAccessEndDt" runat="server" ImageUrl="~/Images/calendar.gif"/> <div>(dd/mmm/yyyy)</div> 
                            </td>
                        </tr>
                         <tr>
                            <td align="left" colspan="4">
                              <span class="text-danger">Access date is used for defining the period from when the parent can access the event online 
                                for selecting the slot</span> </td>
                        </tr>
                          <tr>
                            <td align="left">
                               <span class="field-label">Accessible For</span><span class="text-danger">*</span>
                            </td>
                           
                            <td align="left">
                               <asp:DropDownList ID="ddlAccessFor" runat="server">
                                                                
                                </asp:DropDownList>
                            </td>
                            <td align="left" colspan="2">
                               <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit"/>
                                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"/></td>
                        </tr>
                        
                        </table>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgbtnAccessEndDt" TargetControlID="txtAccessEndDt"> </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgbtnAccessStartDt" TargetControlID="txtAccessStartDt"> </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgBtnEventStartDt" TargetControlID="txtEventStartDt"></ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                           PopupButtonID="imgBtnEventEndDt" TargetControlID="txtEventEndDt"></ajaxToolkit:CalendarExtender>
                       
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            &nbsp;</td>
                    </tr>
                    <tr >
                        <td align="center" valign="top">
                            <asp:GridView id="gvSchedule" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <rowstyle cssclass="griditem" />
                                <columns>
<asp:TemplateField HeaderText="Valid Dates"><ItemTemplate>
    <asp:ImageButton ID="imgComp" runat="server"  
        ImageUrl='<%# bind("Valid_Date") %>' 
        ToolTip='<%# bind("Valid_Date_tooltip") %>'  
        OnClientClick="return confirm('Are you sure you want to update the status?'); "
         CommandArgument='<%# Bind("ESDW_ID") %>' CommandName="Status_Date"   />
</ItemTemplate>

    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />

</asp:TemplateField>


<asp:TemplateField HeaderText="Event Date (Day Of Week)">

<ItemTemplate>
<asp:Label id="lblEventDate" runat="server" Text='<%# Bind("Event_Date") %>'></asp:Label>
</ItemTemplate>

    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
    <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
</asp:TemplateField>


                                    <asp:TemplateField HeaderText="Time Allotted">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOT_TIME_LOT" runat="server" Text='<%# Bind("TOT_TIME_LOT") %>'></asp:Label>
                                        </ItemTemplate> <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Staff Allotted">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOT_STAFF_LOT" runat="server" Text='<%# Bind("TOT_STAFF_LOT") %>'></asp:Label>
                                        </ItemTemplate>
                                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Parent Alloted">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOT_PAR_LOT" runat="server" Text='<%# Bind("TOT_PAR_LOT") %>'></asp:Label>
                                            
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
<asp:TemplateField HeaderText="ESDW_ID" Visible="False">
<ItemTemplate>
    <asp:Label ID="lblESDW_ID" runat="server" Text='<%# Bind("ESDW_ID") %>'></asp:Label>
</ItemTemplate>

</asp:TemplateField>


<asp:TemplateField HeaderText="Allocate">
<ItemTemplate>
    <asp:LinkButton ID="lbtnAllocate" CssClass="gridLinkButton"   Enabled='<%# bind("bENABLED") %>' 
        runat="server" onclick="lbtnAllocate_Click">Allocate</asp:LinkButton>
        <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Image ID="imgtime" runat="server" 
        ImageUrl='<%# bind("TIME_FLAG") %>' ToolTip='<%# bind("TOT_TIME_LOT") %>'  
        CssClass="gridImgRow" Visible='<%# bind("bENABLED") %>' /> --%>

    
</ItemTemplate>

    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

</asp:TemplateField>
</columns>
                                <selectedrowstyle backcolor="Wheat" />
                                <headerstyle cssclass="gridheader_pop" horizontalalign="Center" verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>                          
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      
    </table>
   

   <asp:Panel id="plSch_Event" runat="server" style="background-color: White;" ScrollBars="Auto" CssClass="border p-3" Width="80%">
     <div class="msg_header" style="margin-top:1px;vertical-align:middle;background-color:White;"> 
            <div class="title-bg"  style="margin-top:0px;vertical-align:middle; text-align:left;">
            <asp:Literal  id="ltTime" runat="server"></asp:Literal>   <span style="clear: right;display: inline; float: right; visibility: visible;margin-top:-4px; vertical-align:top; ">
                                 <asp:ImageButton ID="imgclose" runat="server" ImageUrl="../Images/closeme.png"  
                                  AlternateText="Close"  />
                                 </span></div>
             </div>
 
 
 <ucST:uc_Set_Time ID="ucST_Settings" runat="server" />


    </asp:Panel>
    
    
    <asp:Panel id="plForce_delete" runat="server" class="panel-cover" ScrollBars="None">
     <div class="msg_header"> 
            <div class="title-bg-small">
             Confirm</div>
             </div>
      <div ID="divErr_force" runat="server" ></div>

<div style="margin-top:60px;text-align:center;"> <asp:Button ID="btnSave_force" runat="server" CssClass="button" Text="Yes" 
                                     />
                              
                                <asp:Button ID="btnCancel_Force" runat="server" CssClass="button" Text="No" 
                                     />
</div>
    </asp:Panel>
    <asp:HiddenField ID="hfESDW_ID" runat="server" Value="0" />
  <asp:HiddenField ID="hfFORCEDELETE" runat="server" Value="0" />

                </div>
            </div>
         </div>

</asp:Content>

