Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections
Partial Class SchoolVisit_svSch_Parent_AttRemark
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S888810") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    getAccess_rights()
                    GETATT_PARAM_D()
                    getEvent_CAT()

                End If
                ' Call bindSchedule_M()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub

    Private Sub getAccess_rights()
        Try
            ViewState("bEDIT") = False
            ViewState("Can_access") = False
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETACCESS_RIGHT_TIME_SLOT", param)
                If dataread.HasRows = True Then
                    While dataread.Read

                        ViewState("bEDIT") = Convert.ToBoolean(dataread("ATS_bEDIT"))

                    End While
                    ViewState("Can_access") = True
                    tr_staff.Visible = True
                Else
                    tr_staff.Visible = False
                End If

            End Using
            'ddlCat_SelectedIndexChanged(ddlCat, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getEvent_CAT()
        Try


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETEVENT_M", param)
                If dataread.HasRows = True Then
                    ddlCat.DataSource = dataread
                    ddlCat.DataTextField = "EM_TITLE"
                    ddlCat.DataValueField = "EM_ID"
                    ddlCat.DataBind()
                Else
                    ddlCat.Items.Clear()

                End If

            End Using
            ddlCat_SelectedIndexChanged(ddlCat, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getEvent_STAFF()
        Try


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ESM_ID", ddlTitle.SelectedValue)
            param(1) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETEVENT_STAFF_LIST", param)
                If dataread.HasRows = True Then
                    ddlEMP_ID.DataSource = dataread
                    ddlEMP_ID.DataTextField = "ENAME"
                    ddlEMP_ID.DataValueField = "EMP_ID"
                    ddlEMP_ID.DataBind()
                Else
                    ddlEMP_ID.Items.Clear()

                End If

            End Using
            If ViewState("Can_access") = True Then
                If ViewState("bEDIT") = False Then
                    btnUpdate.Enabled = False
                Else
                    btnUpdate.Enabled = True
                End If

            Else
                If Not ddlEMP_ID.Items.FindByValue(Session("EmployeeId")) Is Nothing Then
                    ddlEMP_ID.ClearSelection()
                    ddlEMP_ID.Items.FindByValue(Session("EmployeeId")).Selected = True
                    '  ddlEMP_ID_SelectedIndexChanged(ddlEMP_ID, Nothing)
                End If

            End If
            ddlEMP_ID_SelectedIndexChanged(ddlEMP_ID, Nothing)

        Catch ex As Exception

        End Try
    End Sub
    
    Private Sub getEvent_TITLE()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETSCHEDULE_LIST", param)
                If dataread.HasRows = True Then
                    ddlTitle.DataSource = dataread
                    ddlTitle.DataTextField = "ESM_DISPLAY_TITLE"
                    ddlTitle.DataValueField = "ESM_ID"
                    ddlTitle.DataBind()
                Else
                    ddlTitle.Items.Clear()

                End If

            End Using
            ddlTitle_SelectedIndexChanged(ddlTitle, Nothing)
        Catch ex As Exception

        End Try
    End Sub

  
    Private Sub getSTAFF_EVENT_DATE()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@ESM_ID", ddlTitle.SelectedValue)
            param(1) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
            param(2) = New SqlParameter("@EMP_ID", ddlEMP_ID.SelectedValue)
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETVALID_STAFF_EVENT_DATES", param)
                If dataread.HasRows = True Then
                    rbEventDt.DataSource = dataread
                    rbEventDt.DataTextField = "VISIT_DT"
                    rbEventDt.DataValueField = "ESSW_ESDW_ID"
                    rbEventDt.DataBind()
                    For Each i As ListItem In rbEventDt.Items
                        i.Selected = True
                        Exit For
                    Next
                Else
                    rbEventDt.Items.Clear()

                End If
            End Using
            rbEventDt_SelectedIndexChanged(rbEventDt, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getSTAFF_EVENT_TIME()
        Try

       
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim ESDW_ID As String = "0"
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@ESM_ID", ddlTitle.SelectedValue)
            param(1) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
            param(2) = New SqlParameter("@EMP_ID", ddlEMP_ID.SelectedValue)
            For Each i As ListItem In rbEventDt.Items
                If i.Selected = True Then
                    ESDW_ID = i.Value
                    Exit For
                End If
            Next
            param(3) = New SqlParameter("@ESDW_ID", ESDW_ID)

            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETVALID_STAFF_DAY_TIME_SLOT", param)
                If dataread.HasRows = True Then
                    rbTimeSlot.DataSource = dataread
                    rbTimeSlot.DataTextField = "TIME_SLOT"
                    rbTimeSlot.DataValueField = "ESTW_ID"
                    rbTimeSlot.DataBind()
                    For Each i As ListItem In rbTimeSlot.Items
                        i.Selected = True
                        Exit For
                    Next
                Else
                    rbTimeSlot.Items.Clear()
                End If
            End Using
            rbTimeSlot_SelectedIndexChanged(rbTimeSlot, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GETATT_PARAM_D()
        Dim DS As New DataSet
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Session("SV_Att_param") = Nothing
        Session("SV_Att_param") = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "SV.GETEVENT_ATT_PARAM")
    End Sub
    
    Protected Sub ddlCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCat.SelectedIndexChanged
        getEvent_TITLE()
    End Sub
    Protected Sub ddlTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTitle.SelectedIndexChanged
        getEvent_STAFF()
    End Sub
    Protected Sub ddlEMP_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEMP_ID.SelectedIndexChanged
        getSTAFF_EVENT_DATE()
    End Sub
    Protected Sub rbEventDt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEventDt.SelectedIndexChanged
        getSTAFF_EVENT_TIME()
    End Sub
    Protected Sub rbTimeSlot_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTimeSlot.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub gvStaff_Parent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStaff_Parent.RowDataBound
        Try

      
            Dim DS As New DataSet '= AccessStudentClass.GetATTENDANCE_PARAM_D(ACD_ID, BSU_ID, SESS_TYPE)
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlAtt As DropDownList = DirectCast(e.Row.FindControl("ddlAtt"), DropDownList)
                Dim lblEAP_ID As Label = DirectCast(e.Row.FindControl("lblEAP_ID"), Label)
                ddlAtt.Font.Size = "8"
                If Not Session("SV_Att_param") Is Nothing Then
                    ddlAtt.Items.Clear()
                    DS = Session("SV_Att_param")
                    If DS.Tables(0).Rows.Count > 0 Then
                        ddlAtt.DataSource = DS.Tables(0)
                        ddlAtt.DataTextField = "EAP_DESCR"
                        ddlAtt.DataValueField = "EAP_ID"
                        ddlAtt.DataBind()
                    End If

                    If Not ddlAtt.Items.FindByValue(lblEAP_ID.Text) Is Nothing Then
                        ddlAtt.ClearSelection()
                        ddlAtt.Items.FindByValue(lblEAP_ID.Text).Selected = True
                        If ddlAtt.SelectedValue = "5" Then
                            ddlAtt.Enabled = False
                        End If
                    End If


                End If

            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            Dim ESTW_ID As String = String.Empty
            Dim ESDW_ID As String = String.Empty
            For Each i As ListItem In rbEventDt.Items
                If i.Selected = True Then
                    ESDW_ID = i.Value
                    Exit For
                End If
            Next
            For Each i As ListItem In rbTimeSlot.Items
                If i.Selected = True Then
                    ESTW_ID = i.Value
                    Exit For
                End If
            Next

            Dim PARAM(4) As SqlParameter
            Dim txtSearch As New TextBox
            PARAM(0) = New SqlParameter("@ESTW_ID", ESTW_ID)
            PARAM(1) = New SqlParameter("@ESDW_ID", ESDW_ID)
            PARAM(2) = New SqlParameter("@EMP_ID", ddlEMP_ID.SelectedValue)
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV.GETPARENT_SCHOOL_VISIT_STAFF_ATT", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvStaff_Parent.DataSource = ds.Tables(0)
                gvStaff_Parent.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvStaff_Parent.DataSource = ds.Tables(0)
                Try
                    gvStaff_Parent.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStaff_Parent.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStaff_Parent.Rows(0).Cells.Clear()
                gvStaff_Parent.Rows(0).Cells.Add(New TableCell)
                gvStaff_Parent.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStaff_Parent.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStaff_Parent.Rows(0).Cells(0).Text = "Record not available !!!"
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim errormsg As String = String.Empty
        Dim errNo As Integer = 0
        lblError.InnerText = ""

        errNo = callTransaction(errormsg)
    If errNo <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
        Else
            'btnSave.Visible = False
            'btnCancel.Visible = False
            'gvStaff_Parent.Enabled = True
            'Call bindSchedule_M()
            gridbind()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
    End Sub
    Private Function callTransaction(ByRef errormsg As String) As Integer


        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim tran As SqlTransaction
        Dim ESM_ID As String = ddlCat.SelectedValue
        Dim ESM_EM_ID As String = ddlTitle.SelectedValue
        Dim EMP_ID As String = ddlEMP_ID.SelectedValue
        Dim PSV_ID As String = String.Empty
        Dim ESDW_ID As String = String.Empty
        Dim ESTW_ID As String = String.Empty
        Dim XML_DATA As New StringBuilder
        Dim chkControl As New HtmlInputCheckBox
        Dim lblTrack_id As New Label
        Dim ddlAtt As New DropDownList
        Dim txtStaffRemark As New TextBox
        Dim isCHK As Boolean
        For Each IDAY As ListItem In rbEventDt.Items
            If IDAY.Selected = True Then
                ESDW_ID = IDAY.Value
                Exit For
            End If
        Next
        For Each ITIME As ListItem In rbTimeSlot.Items
            If ITIME.Selected = True Then
                ESTW_ID = ITIME.Value
                Exit For
            End If
        Next

        For Each irow As GridViewRow In gvStaff_Parent.Rows
            chkControl = CType(irow.FindControl("chkControl"), HtmlInputCheckBox)
            If chkControl.Checked = True Then
                PSV_ID = chkControl.Value
                lblTrack_id = CType(irow.FindControl("lblTrack_id"), Label)
                ddlAtt = CType(irow.FindControl("ddlAtt"), DropDownList)
                txtStaffRemark = CType(irow.FindControl("txtStaffRemark"), TextBox)
                XML_DATA.Append(String.Format("<PSV_M><PSV ID='{0}' PSV_TRACK_ID='{1}' PSV_ATT_PARAM='{2}' STAFF_REMARK='{3}'  /></PSV_M>", _
 PSV_ID, lblTrack_id.Text, ddlAtt.SelectedValue, txtStaffRemark.Text.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")))

                isCHK = True
            End If
        Next
        If isCHK = False Then
            errormsg = "Please select the record(s) to be updated !!!"
            Return -1
        End If

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim param(12) As SqlParameter

                param(0) = New SqlParameter("@ESM_ID", ESM_ID)
                param(1) = New SqlParameter("@ESM_EM_ID", ESM_EM_ID)
                param(2) = New SqlParameter("@EMP_ID", EMP_ID)
                param(3) = New SqlParameter("@ESDW_ID", ESDW_ID)
                param(4) = New SqlParameter("@ESTW_ID", ESTW_ID)
                param(5) = New SqlParameter("@XML_DATA", XML_DATA.ToString())
                param(6) = New SqlParameter("@USR_ID", Session("sUsr_id"))
                param(7) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                param(8) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SV.SAVEPARENT_SCHOOL_VISIT_STAFF_ATT", param)
                Dim ReturnFlag As Integer = param(8).Value


                If ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    'ViewState("ESM_ID") = IIf(TypeOf (param(16).Value) Is DBNull, 0, param(16).Value)


                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    transaction.Rollback()
                Else
                    errormsg = ""
                    transaction.Commit()
                End If
            End Try

        End Using



    End Function
    Private Sub reset_control()
        lblError.InnerText = ""

        gvStaff_Parent.Visible = True

        btnUpdate.Visible = True

    End Sub

    


   
   
  
End Class
