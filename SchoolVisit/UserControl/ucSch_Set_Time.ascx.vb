﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class SchoolVisit_UserControl_ucSch_Set_Time
    Inherits System.Web.UI.UserControl


    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            bindHrs()
            bindMin()
            bindStaff()
            'bind_dates()
            btnSaveSlot.Visible = True
            btnUpdateSlot.Visible = False
            btnCancel.Visible = False
            ' gridbind_Alloc()
        End If





    End Sub

    Private Sub bindHrs()
        Dim i As Integer = 0
        Dim hrs As String = String.Empty
        For i = 6 To 20
            If i < 10 Then
                hrs = "0" & i.ToString
            Else
                hrs = i.ToString
            End If

            ddlStartHrs.Items.Add(New ListItem(hrs, hrs))
            ddlEndHrs.Items.Add(New ListItem(hrs, hrs))
        Next

    End Sub
    Private Sub bindMin()
        Dim i As Integer = 0
        Dim min As String = String.Empty
        For i = 0 To 55 Step 5
            If i < 10 Then
                min = "0" & i.ToString
            Else
                min = i.ToString
            End If
            ddlStartMin.Items.Add(New ListItem(min, min))
            ddlEndMin.Items.Add(New ListItem(min, min))
        Next

    End Sub
    Private Sub bindStaff()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter

        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETSCH_EMP_LIST", param)
            If DATAREADER.HasRows = True Then
                chkStaff.DataSource = DATAREADER
                chkStaff.DataTextField = "ENAME"
                chkStaff.DataValueField = "EMP_ID"
                chkStaff.DataBind()
               
            Else
               
                chkStaff.Items.Clear()

            End If

        End Using
    End Sub
    Public Sub bind_dates()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim hfESDW_ID As HiddenField = Me.Parent.FindControl("hfESDW_ID")
        If Me.Parent.FindControl("hfESDW_ID") Is Nothing Then
            hfESDW_ID.Value = "0"
        End If
        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@ESDW_ID", hfESDW_ID.Value)
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETSCH_DATE_FOR_COPY", param)
            If DATAREADER.HasRows = True Then
                chkDates.DataSource = DATAREADER
                chkDates.DataTextField = "EVENT_DATE"
                chkDates.DataValueField = "ESDW_ID"
                chkDates.DataBind()

                tr_copy.Visible = True
                tr_chk.Visible = True
                ViewState("Hide_chk") = True
            Else
                ViewState("Hide_chk") = False
                tr_copy.Visible = False
                tr_chk.Visible = False
                chkDates.Items.Clear()

            End If

        End Using
    End Sub



   

    Public Sub reset_state()
        Try
            hfID.Value = "0"

            txtTot.Text = "0"
            For Each item As ListItem In chkStaff.Items
                If item.Selected = True Then
                    item.Selected = False
                    item.Enabled = True
                End If
            Next
            chkYes.Checked = False
            ddlStartHrs.ClearSelection()
            ddlStartHrs.SelectedIndex = 0
            ddlEndHrs.ClearSelection()
            ddlEndHrs.SelectedIndex = 0

            ddlStartMin.ClearSelection()
            ddlStartMin.SelectedIndex = 0
            ddlEndMin.ClearSelection()
            ddlEndMin.SelectedIndex = 0

            btnSaveSlot.Visible = True
            btnCancel.Visible = False
            btnUpdateSlot.Visible = False
            lblError.InnerText = ""


            gvTime.Columns(5).Visible = True
            gvTime.Columns(6).Visible = True
            If ViewState("Hide_chk") = True Then
                tr_copy.Visible = True
                tr_chk.Visible = True
            End If

        Catch ex As Exception

        End Try


    End Sub
    

    
 
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.Int32"))
            Dim ESTW_ID As New DataColumn("ESTW_ID", System.Type.GetType("System.String"))
            Dim FLAG_ACTIVE As New DataColumn("FLAG_ACTIVE", System.Type.GetType("System.String"))
            Dim FLAG_AUTOALLOT As New DataColumn("FLAG_AUTOALLOT", System.Type.GetType("System.String"))
            Dim START_TIME As New DataColumn("START_TIME", System.Type.GetType("System.String"))
            Dim END_TIME As New DataColumn("END_TIME", System.Type.GetType("System.String"))
            Dim TOT_ENTRY As New DataColumn("TOT_ENTRY", System.Type.GetType("System.String"))
            Dim TOT_BOOKED As New DataColumn("TOT_BOOKED", System.Type.GetType("System.String"))
            Dim STAFF_NAME As New DataColumn("STAFF_NAME", System.Type.GetType("System.String"))
            Dim STAFF_ID As New DataColumn("STAFF_ID", System.Type.GetType("System.String"))
            Dim ESTW_bENABLED As New DataColumn("ESTW_bENABLED", System.Type.GetType("System.Boolean"))
            Dim ESTW_bAUTOALLOT As New DataColumn("ESTW_bAUTOALLOT", System.Type.GetType("System.Boolean"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(ESTW_ID)
            dtDt.Columns.Add(FLAG_ACTIVE)
            dtDt.Columns.Add(FLAG_AUTOALLOT)
            dtDt.Columns.Add(START_TIME)
            dtDt.Columns.Add(END_TIME)
            dtDt.Columns.Add(TOT_ENTRY)
            dtDt.Columns.Add(TOT_BOOKED)
            dtDt.Columns.Add(STAFF_NAME)
            dtDt.Columns.Add(STAFF_ID)
            dtDt.Columns.Add(ESTW_bENABLED)
            dtDt.Columns.Add(ESTW_bAUTOALLOT)
            dtDt.Columns.Add(STATUS)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Create table")

            Return dtDt
        End Try
    End Function
    Private Sub GETSCH_TIME_STAFF_DATABIND()
        Try
            Session("TABLE_SCH_TIME_STAFF").Rows.Clear()
            ViewState("id") = 1

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter

            Dim hfESDW_ID As HiddenField = Me.Parent.FindControl("hfESDW_ID")
            If Me.Parent.FindControl("hfESDW_ID") Is Nothing Then
                hfESDW_ID.Value = "0"
            End If


            param(0) = New SqlParameter("@ESDW_ID", hfESDW_ID.Value)

            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETSCH_TIME_STAFF_DATABIND", param)
                While reader.Read
                    If reader.HasRows = True Then
                        Dim rDt As DataRow
                        rDt = Session("TABLE_SCH_TIME_STAFF").NewRow
                        rDt("ID") = CInt(ViewState("id"))
                        rDt("ESTW_ID") = Convert.ToString(reader("ESTW_ID"))
                        rDt("FLAG_ACTIVE") = Convert.ToString(reader("FLAG_ACTIVE"))
                        rDt("START_TIME") = Convert.ToString(reader("START_TIME"))
                        rDt("END_TIME") = Convert.ToString(reader("END_TIME"))
                        rDt("TOT_ENTRY") = Convert.ToString(reader("TOT_ENTRY"))
                        rDt("TOT_BOOKED") = Convert.ToString(reader("TOT_BOOKED"))
                        rDt("STAFF_NAME") = Convert.ToString(reader("STAFF_NAME"))
                        rDt("STAFF_ID") = Convert.ToString(reader("STAFF_ID"))
                        rDt("ESTW_bENABLED") = Convert.ToString(reader("ESTW_bENABLED"))
                        rDt("ESTW_bAUTOALLOT") = Convert.ToString(reader("ESTW_bAUTOALLOT"))
                        rDt("FLAG_AUTOALLOT") = Convert.ToString(reader("FLAG_AUTOALLOT"))

                        rDt("STATUS") = "old"
                        ViewState("id") = ViewState("id") + 1
                        Session("TABLE_SCH_TIME_STAFF").Rows.Add(rDt)

                    End If

                End While
            End Using
            gridbind_Alloc()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GETSCH_TIME_STAFF_DATABIND")
        End Try
    End Sub
    Private Sub gridbind_Alloc()
        Dim dtDt As New DataTable


        'If Not Session("TABLE_SCH_TIME_STAFF") Is Nothing Then
        '    dtDt = Session("TABLE_SCH_TIME_STAFF")
        'End If

        dtDt = CreateDataTable()

        If Session("TABLE_SCH_TIME_STAFF").Rows.Count > 0 Then
            For i As Integer = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.Count - 1
                If Session("TABLE_SCH_TIME_STAFF").Rows(i)("STATUS") & "" <> "delete" Then
                    Dim rDt As DataRow
                    rDt = dtDt.NewRow
                    For j As Integer = 0 To Session("TABLE_SCH_TIME_STAFF").Columns.Count - 1
                        rDt.Item(j) = Session("TABLE_SCH_TIME_STAFF").Rows(i)(j)
                    Next

                    dtDt.Rows.Add(rDt)
                End If
            Next

        End If

        If dtDt.Rows.Count > 0 Then

            gvTime.DataSource = dtDt
            gvTime.DataBind()
        Else
            dtDt.Rows.Add(dtDt.NewRow())

            'start the count from 1 no matter gridcolumn is visible or not
            dtDt.Rows(0)(10) = True
            dtDt.Rows(0)(11) = True
            gvTime.DataSource = dtDt
            Try
                gvTime.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvTime.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvTime.Rows(0).Cells.Clear()
            gvTime.Rows(0).Cells.Add(New TableCell)
            gvTime.Rows(0).Cells(0).ColumnSpan = columnCount
            gvTime.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvTime.Rows(0).Cells(0).Text = "Record not available !!!"


        End If
    End Sub
    Public Sub INTIAL_CALL()
        Session("TABLE_SCH_TIME_STAFF") = CreateDataTable()
        Session("TABLE_SCH_TIME_STAFF").Rows.Clear()
        ViewState("id") = 1
        GETSCH_TIME_STAFF_DATABIND()
    End Sub
    Protected Sub btnAddSlot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSlot.Click
        Try
            Dim mdlSch_Event As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlSch_Event")
            mdlSch_Event.Show()

            Dim hfESDW_ID As HiddenField = Me.Parent.FindControl("hfESDW_ID")
            If Me.Parent.FindControl("hfESDW_ID") Is Nothing Then
                hfESDW_ID.Value = "0"
            End If
            Dim ESTW_ID As String = hfESTW_ID.Value
            Dim ESTW_START_TIME As String = ddlStartHrs.SelectedValue.ToString() & ":" & ddlStartMin.SelectedValue.ToString()
            Dim ESTW_END_TIME As String = ddlEndHrs.SelectedValue.ToString() & ":" & ddlEndMin.SelectedValue.ToString()
            Dim ESTW_bENABLED As Boolean = chkYes.Checked
            Dim ESTW_TOT_ENTRY As String = txtTot.Text.Trim
            Dim ESSW_EMP_ID As String = String.Empty
            Dim iAdd As Integer
            Dim Sess_status As String = String.Empty
            Dim staff_name As String = String.Empty
            Dim Sess_Start_TIME As String = String.Empty
            Dim Sess_End_Time As String = String.Empty
            For Each item As ListItem In chkStaff.Items
                If item.Selected = True Then
                    ESSW_EMP_ID += item.Value.Trim + "|"
                    staff_name += item.Text + ","
                End If
            Next
            'IF START TIME IS GREATER THAN END TIME THEN THROUGH ERROR
            If CInt(ESTW_START_TIME.Replace(":", "")) >= CInt(ESTW_END_TIME.Replace(":", "")) Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Start time cannot be greater than or equal to end time !!!</div>"
                Exit Sub
            End If
            If ESSW_EMP_ID.Trim = "" Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Please allocate the staff to the selected time slot !!!</div>"
                Exit Sub
            End If
            For iAdd = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.Count - 1
                Sess_Start_TIME = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("START_TIME")
                Sess_End_Time = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("END_TIME")
                Sess_status = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("STATUS")
                If (ESTW_START_TIME = Sess_Start_TIME) And (ESTW_END_TIME = Sess_End_Time) And (Sess_status <> "delete") Then
                    lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Duplicate entry not allowed !!!</div>"
                    Exit Sub
                End If
            Next
            Dim rDt As DataRow
            rDt = Session("TABLE_SCH_TIME_STAFF").NewRow
            rDt("ID") = ViewState("id")
            rDt("ESTW_ID") = "0"
            rDt("FLAG_ACTIVE") = IIf(chkYes.Checked = True, "~/Images/tick.gif", "~/Images/cross.gif")
            rDt("START_TIME") = ESTW_START_TIME
            rDt("END_TIME") = ESTW_END_TIME
            rDt("TOT_ENTRY") = ESTW_TOT_ENTRY
            rDt("TOT_BOOKED") = "0"
            rDt("STAFF_NAME") = staff_name.TrimEnd(",")
            rDt("STAFF_ID") = ESSW_EMP_ID
            rDt("ESTW_bENABLED") = IIf(chkYes.Checked = True, True, False)
            rDt("ESTW_bAUTOALLOT") = IIf(chkEvenYes.Checked = True, True, False)
            rDt("FLAG_AUTOALLOT") = IIf(chkEvenYes.Checked = True, "~/Images/tick.gif", "~/Images/cross.gif")

            rDt("STATUS") = "add"
            ViewState("id") = ViewState("id") + 1
            Session("TABLE_SCH_TIME_STAFF").Rows.Add(rDt)
            gridbind_Alloc()
            reset_state()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;background-color:#edf3fa;'>Error while adding record</div>"

        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        reset_state()
        btnAddSlot.Visible = True
        btnSaveSlot.Visible = True
        btnUpdateSlot.Visible = False
        btnCancel.Visible = False

        Dim mdlSch_Event As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlSch_Event")
        mdlSch_Event.Show()
    End Sub
    Protected Sub lbtnTDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim mdlSch_Event As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlSch_Event")
        mdlSch_Event.Show()
        Dim iIndex As Integer = 0
        Dim lblID As New Label
        lblID = TryCast(sender.FindControl("lblID"), Label)
        iIndex = CInt(lblID.Text)

        Dim i As Integer = 0

        For i = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.Count - 1
            If Session("TABLE_SCH_TIME_STAFF").rows(i)("Id") = iIndex Then
                If Session("TABLE_SCH_TIME_STAFF").rows(i)("ESTW_ID") = "0" Then
                    Session("TABLE_SCH_TIME_STAFF").Rows(i).Delete()
                Else
                    Session("TABLE_SCH_TIME_STAFF").Rows(i)("STATUS") = "delete"
                End If
                Exit For
            End If
        Next

        gridbind_Alloc()

    End Sub
    Protected Sub lbtnTedit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mdlSch_Event As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlSch_Event")
        mdlSch_Event.Show()
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim lblID As New Label
        Dim END_TIME As String = String.Empty
        Dim START_TIME As String = String.Empty
        Dim STAFF_ID As String = String.Empty
        Dim TOT_BOOKED As Integer = 0
        Dim Flag As Boolean
        Dim ESTW_bAUTOALLOT As Boolean
        lblID = TryCast(sender.FindControl("lblID"), Label)
        iIndex = CInt(lblID.Text)
        hfID.Value = lblID.Text
        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.Count - 1
            If iIndex = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("ID") Then
                START_TIME = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("START_TIME")
                END_TIME = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("END_TIME")
                txtTot.Text = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("TOT_ENTRY")
                STAFF_ID = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("STAFF_ID")
                Flag = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("ESTW_bENABLED")
                TOT_BOOKED = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("TOT_BOOKED")
                ESTW_bAUTOALLOT = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("ESTW_bAUTOALLOT")
                If Flag = True Then
                    chkYes.Checked = True
                Else
                    chkYes.Checked = False
                End If
                If ESTW_bAUTOALLOT = True Then

                    chkEvenYes.Checked = True
                Else
                    chkEvenYes.Checked = False

                End If



                If Not ddlStartHrs.Items.FindByText(Left(START_TIME.Trim, 2)) Is Nothing Then
                    ddlStartHrs.ClearSelection()
                    ddlStartHrs.Items.FindByText(Left(START_TIME.Trim, 2)).Selected = True
                End If
                If Not ddlStartMin.Items.FindByText(Right(START_TIME.Trim, 2)) Is Nothing Then
                    ddlStartMin.ClearSelection()
                    ddlStartMin.Items.FindByText(Right(START_TIME.Trim, 2)).Selected = True
                End If
                If Not ddlEndHrs.Items.FindByText(Left(END_TIME.Trim, 2)) Is Nothing Then
                    ddlEndHrs.ClearSelection()
                    ddlEndHrs.Items.FindByText(Left(END_TIME.Trim, 2)).Selected = True
                End If
                If Not ddlEndMin.Items.FindByText(Right(END_TIME.Trim, 2)) Is Nothing Then
                    ddlEndMin.ClearSelection()
                    ddlEndMin.Items.FindByText(Right(END_TIME.Trim, 2)).Selected = True
                End If
                For Each item As ListItem In chkStaff.Items
                    If STAFF_ID.ToString.Contains(item.Value) = True Then
                        item.Selected = True
                        If TOT_BOOKED > 0 Then
                            item.Enabled = False
                        End If
                    End If
                Next
                Exit For
            End If
        Next

        If ViewState("Hide_chk") = True Then
            tr_copy.Visible = False
            tr_chk.Visible = False
        End If
        gvTime.Columns(5).Visible = False
        gvTime.Columns(6).Visible = False
        btnSaveSlot.Visible = False
        btnAddSlot.Visible = False
        btnUpdateSlot.Visible = True
        btnCancel.Visible = True



    End Sub
    Protected Sub btnUpdateSlot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSlot.Click
        Dim mdlSch_Event As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlSch_Event")
        mdlSch_Event.Show()
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = hfID.Value
        If ViewState("Hide_chk") = True Then
            tr_copy.Visible = True
            tr_chk.Visible = True
        End If

        Dim ESTW_START_TIME As String = ddlStartHrs.SelectedValue.ToString() & ":" & ddlStartMin.SelectedValue.ToString()
        Dim ESTW_END_TIME As String = ddlEndHrs.SelectedValue.ToString() & ":" & ddlEndMin.SelectedValue.ToString()
        Dim ESTW_bENABLED As Boolean = chkYes.Checked
        Dim ESTW_bAUTOALLOT As Boolean = chkEvenYes.Checked
        Dim ESTW_TOT_ENTRY As String = txtTot.Text.Trim
        Dim TOT_BOOKED As Integer = 0
        Dim ESSW_EMP_ID As String = String.Empty
        Dim staff_name As String = String.Empty
        Dim Sess_Start_TIME As String = String.Empty
        Dim Sess_End_Time As String = String.Empty
        Dim Sess_status As String = String.Empty
        For Each item As ListItem In chkStaff.Items
            If item.Selected = True Then
                ESSW_EMP_ID += item.Value.Trim + "|"
                staff_name += item.Text + ","
            End If
        Next
        'IF START TIME IS GREATER THAN END TIME THEN THROUGH ERROR
        If CInt(ESTW_START_TIME.Replace(":", "")) >= CInt(ESTW_END_TIME.Replace(":", "")) Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Start time cannot be greater than or equal to end time !!!</div>"
            Exit Sub
        End If
        If ESSW_EMP_ID.Trim = "" Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Please allocate the staff to the selected time slot !!!</div>"
            Exit Sub
        End If
        Dim iAdd As Integer

        For iAdd = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.Count - 1
            Sess_Start_TIME = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("START_TIME")
            Sess_End_Time = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("END_TIME")
            Sess_status = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("STATUS")
            TOT_BOOKED = Session("TABLE_SCH_TIME_STAFF").Rows(iAdd)("TOT_BOOKED")
            If (ESTW_START_TIME = Sess_Start_TIME) And (ESTW_END_TIME = Sess_End_Time) And (Sess_status <> "delete") And (iIndex = iAdd) Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Duplicate entry not allowed !!!</div>"
                Exit Sub
            End If

            If TOT_BOOKED > ESTW_TOT_ENTRY Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Total allocated cannot be less than total booked !!!</div>"
                Exit Sub
            End If
        Next
        For iEdit = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.Count - 1
            If iIndex = Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("ID") Then

                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("FLAG_ACTIVE") = IIf(chkYes.Checked = True, "~/Images/tick.gif", "~/Images/cross.gif")
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("START_TIME") = ESTW_START_TIME
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("END_TIME") = ESTW_END_TIME
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("TOT_ENTRY") = txtTot.Text.Trim
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("STAFF_NAME") = staff_name.TrimEnd(",")
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("STAFF_ID") = ESSW_EMP_ID
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("ESTW_bENABLED") = ESTW_bENABLED
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("ESTW_bAUTOALLOT") = ESTW_bAUTOALLOT
                Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("FLAG_AUTOALLOT") = IIf(chkEvenYes.Checked = True, "~/Images/tick.gif", "~/Images/cross.gif")

                If Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("STATUS") = "old" Then
                    Session("TABLE_SCH_TIME_STAFF").Rows(iEdit)("STATUS") = "update"
                End If


                Exit For
            End If
        Next

        If ViewState("Hide_chk") = True Then
            tr_copy.Visible = True
            tr_chk.Visible = True
        End If
        gvTime.Columns(5).Visible = True
        gvTime.Columns(6).Visible = True
        btnAddSlot.Visible = True
        btnSaveSlot.Visible = True
        btnUpdateSlot.Visible = False
        btnCancel.Visible = False
        gridbind_Alloc()
        reset_state()


    End Sub
    Protected Sub btnSaveSlot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSlot.Click

        Dim mdlSch_Event As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlSch_Event")
        mdlSch_Event.Show()
        Dim errormsg As String = String.Empty
        Dim ESTW_ID As String = String.Empty
        Dim ESTW_START_TIME As String = String.Empty
        Dim ESTW_END_TIME As String = String.Empty
        Dim ESTW_TOT_ENTRY As String = String.Empty
        Dim ESTW_bENABLED As Boolean
        Dim ESTW_bAUTOALLOT As Boolean
        Dim ESSW_EMP_ID As String = String.Empty
        Dim STATUS As String = String.Empty
        Dim XML_STR As String = String.Empty
        Dim i As Integer

        If Session("TABLE_SCH_TIME_STAFF") IsNot Nothing Then
            For i = 0 To Session("TABLE_SCH_TIME_STAFF").Rows.count - 1
                ESTW_ID = Session("TABLE_SCH_TIME_STAFF").Rows(i)("ESTW_ID")
                ESTW_START_TIME = Session("TABLE_SCH_TIME_STAFF").Rows(i)("START_TIME")
                ESTW_END_TIME = Session("TABLE_SCH_TIME_STAFF").Rows(i)("END_TIME")
                ESTW_TOT_ENTRY = Session("TABLE_SCH_TIME_STAFF").Rows(i)("TOT_ENTRY")
                ESSW_EMP_ID = Session("TABLE_SCH_TIME_STAFF").Rows(i)("STAFF_ID")
                ESTW_bENABLED = Session("TABLE_SCH_TIME_STAFF").Rows(i)("ESTW_bENABLED")
                STATUS = Session("TABLE_SCH_TIME_STAFF").Rows(i)("STATUS")
                ESTW_bAUTOALLOT = Session("TABLE_SCH_TIME_STAFF").Rows(i)("ESTW_bAUTOALLOT")
                If STATUS = "ADD" Then
                    XML_STR += String.Format("<ESTW ESTW_ID='{0}' ESTW_START_TIME='{1}' ESTW_END_TIME='{2}' ESTW_TOT_ENTRY='{3}' ESSW_EMP_ID='{4}' ESTW_bENABLED='{5}' ESTW_bAUTOALLOT='{6}' STATUS='{7}'/>", _
                                             ESTW_ID, ESTW_START_TIME, ESTW_END_TIME, ESTW_TOT_ENTRY, ESSW_EMP_ID.TrimEnd("|"), ESTW_bENABLED, ESTW_bAUTOALLOT, STATUS)

                ElseIf (STATUS <> "old") Then
                    If Not ((STATUS = "DELETED") And (ESTW_ID = "0")) Then
                        XML_STR += String.Format("<ESTW ESTW_ID='{0}' ESTW_START_TIME='{1}' ESTW_END_TIME='{2}' ESTW_TOT_ENTRY='{3}' ESSW_EMP_ID='{4}' ESTW_bENABLED='{5}' ESTW_bAUTOALLOT='{6}' STATUS='{7}'/>", _
                                          ESTW_ID, ESTW_START_TIME, ESTW_END_TIME, ESTW_TOT_ENTRY, ESSW_EMP_ID.TrimEnd("|"), ESTW_bENABLED, ESTW_bAUTOALLOT, STATUS)

                    End If

                End If
            Next
        End If

        If XML_STR.Trim <> "" Then
            XML_STR = "<ESTW_M>" + XML_STR + "</ESTW_M>"
        Else

            XML_STR += String.Format("<ESTW ESTW_ID='{0}' ESTW_START_TIME='{1}' ESTW_END_TIME='{2}' ESTW_TOT_ENTRY='{3}' ESSW_EMP_ID='{4}' ESTW_bENABLED='{5}' ESTW_bAUTOALLOT='{6}' STATUS='{7}'/>", _
                                          ESTW_ID, ESTW_START_TIME, ESTW_END_TIME, ESTW_TOT_ENTRY, ESSW_EMP_ID.TrimEnd("|"), ESTW_bENABLED, ESTW_bAUTOALLOT, "old")
            XML_STR = "<ESTW_M>" + XML_STR + "</ESTW_M>"
            ' lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Add new record to save !!!</div>"
        End If



        mdlSch_Event.Show()





        Dim ERRNO As Integer = callTransaction(errormsg, XML_STR)





        If ERRNO <> 0 Then
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>" & errormsg & "</div>"
        Else
            reset_state()
            lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98.1%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated successfully !!!</div>"
        End If
        bind_dates()
        ' gridbind_Alloc()
        GETSCH_TIME_STAFF_DATABIND()
    End Sub

    Private Function callTransaction(ByRef errormsg As String, ByVal XML_STR As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim tran As SqlTransaction
        Dim ESDW_IDS As String = String.Empty

        Dim hfESDW_ID As HiddenField = Me.Parent.FindControl("hfESDW_ID")
        If Me.Parent.FindControl("hfESDW_ID") Is Nothing Then
            hfESDW_ID.Value = "0"
        End If

        For Each item As ListItem In chkDates.Items
            If item.Selected = True Then
                ESDW_IDS += item.Value + "|"

            End If
        Next





        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")
            Try

                Dim param(10) As SqlParameter
                param(0) = New SqlParameter("@USR_ID", Session("sUsr_id"))
                param(1) = New SqlParameter("@ClientBrw", AuditInfo.ClientBrw)
                param(2) = New SqlParameter("@clientIp", AuditInfo.ClientIP)
                param(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                param(4) = New SqlParameter("@ESTW_ESDW_ID", hfESDW_ID.Value)
                param(5) = New SqlParameter("@ESDW_IDS", ESDW_IDS)
                param(6) = New SqlParameter("@XML_DATA", XML_STR)
                param(7) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(7).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "SV.SAVEEVENT_SCHEDULE_TIME_WISE", param)
                Dim ReturnFlag As Integer = param(7).Value

                If ReturnFlag <> 0 Then
                    callTransaction = "1"
                    errormsg = "Error occured while processing info !!!"
                Else
                    ViewState("datamode") = "none"

                    callTransaction = "0"

                End If
            Catch ex As Exception
                callTransaction = "1"
                errormsg = ex.Message
            Finally
                If callTransaction <> "0" Then
                    errormsg = "Error occured while saving !!!"
                    UtilityObj.Errorlog(errormsg)
                    tran.Rollback()
                Else
                    errormsg = ""
                    tran.Commit()
                End If
            End Try

        End Using



    End Function

End Class
