﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucSch_Set_Time.ascx.vb" Inherits="SchoolVisit_UserControl_ucSch_Set_Time" %>
<%--<link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
<%--<link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<table align="center" width="100%">


    <tr id="I20">
        <td align="left" width="30%">
            <span class="field-label">Start Time</span></td>

        <td align="left" width="35%"><span class="field-label">Hrs</span>&nbsp;<asp:DropDownList ID="ddlStartHrs" runat="server"></asp:DropDownList></td>
        <td align="left" width="35%"><span class="field-label">Min&nbsp;</span><asp:DropDownList ID="ddlStartMin" runat="server"></asp:DropDownList>

        </td>
        
    </tr>
    <tr id="I21">
        <td align="left">
            <span class="field-label">End Time</span></td>

        <td align="left"><span class="field-label">Hrs</span>&nbsp;<asp:DropDownList ID="ddlEndHrs" runat="server">
        </asp:DropDownList></td>
        <td align="left"><span class="field-label">Min</span>&nbsp;<asp:DropDownList ID="ddlEndMin" runat="server">
        </asp:DropDownList>
        </td>
        <td></td>
    </tr>
    <tr id="I22">
        <td align="left">
            <span class="field-label">Active</span></td>

        <td align="left">
            <asp:CheckBox ID="chkYes" runat="server" CssClass="field-label" Text="Yes" />
        </td>
        <td></td>
    </tr>
    <tr id="I23">
        <td align="left">
            <span class="field-label">Total Entries Allowed</span></td>

        <td align="left">
            <asp:TextBox ID="txtTot" runat="server" MaxLength="8" Text="0"></asp:TextBox>
            <asp:RequiredFieldValidator ID="refTot" runat="server" ErrorMessage="Required"
                Text="Required" ValidationGroup="slot" ControlToValidate="txtTot"></asp:RequiredFieldValidator>
            <ajaxToolkit:FilteredTextBoxExtender ID="fxTot" runat="server"
                FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txttot">
            </ajaxToolkit:FilteredTextBoxExtender>
            <div class="text-danger">For unlimited entries enter Zero(0). </div>
        </td>
        <td></td>
    </tr>
    <%--<tr  id="I23" style="display:none;">
                            <td align="left">
                                Evenly Distributed</td>
                            <td align="center">
                                :</td>
                            <td align="left">
                                <asp:CheckBox ID="chkEvenYes" runat="server" Text="Yes" Checked="true" />
                                <div style="font-size:9px;color:Maroon;"><div>Number of staff allocated to a slot determines the number of batches</div>
Parent gets evenly distributed between these batches
 </div>
                            </td>
                        </tr>--%>
    <tr id="I24">
        <td align="left">
            <span class="field-label">Allocate to Staff</span></td>

        <td align="left">
            <div class="checkbox-list">
                <asp:Panel ID="plStaff" runat="server">

                    <asp:CheckBoxList ID="chkStaff" runat="server">
                    </asp:CheckBoxList>
                </asp:Panel>
            </div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Panel ID="plgvtime" runat="server" ScrollBars="Vertical" Width="100%">

                <asp:GridView ID="gvTime" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="3" DataKeyNames="ID" PageSize="30" CssClass="table table-bordered table-row">
                    <RowStyle HorizontalAlign="Center" CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField HeaderText="Active">
                            <ItemTemplate>
                                <asp:Image ID="imgActive" runat="server"
                                    ImageUrl='<%# bind("FLAG_ACTIVE") %>' />

                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Allocated Staff">
                            <ItemTemplate>
                                <asp:Label ID="lblSTAFF_NAME" runat="server" Text='<%# bind("STAFF_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Auto Allot" Visible="False">
                            <ItemTemplate>
                                <asp:Image ID="imgAutoAllot" runat="server"
                                    ImageUrl='<%# bind("FLAG_AUTOALLOT") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time">
                            <ItemTemplate>
                                <asp:Label ID="lblSTART_TIME" runat="server" Text='<%# bind("START_TIME") %>'></asp:Label>
                                -
                                <asp:Label ID="lblEND_TIME" runat="server" Text='<%# bind("END_TIME") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Allowed">
                            <ItemTemplate>
                                <asp:Label ID="lblTOT_ENTRY" runat="server" Text='<%# bind("TOT_ENTRY") %>'></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Occupied">
                            <ItemTemplate>
                                <asp:Label ID="lblTOT_BOOKED" runat="server" Text='<%# bind("TOT_BOOKED") %>'></asp:Label>

                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnTedit" runat="server" OnClick="lbtnTedit_Click">Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnTDelete" runat="server" OnClick="lbtnTDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ESTW_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblESTW_ID" runat="server" Text='<%# Bind("ESTW_ID") %>'></asp:Label>
                                <asp:Label ID="lblSTAFF_ID" runat="server" Text='<%# Bind("STAFF_ID") %>'></asp:Label>
                                <asp:Label ID="lblFlag" runat="server" Text='<%# Bind("ESTW_bENABLED") %>'></asp:Label>
                                <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Left" />

                </asp:GridView>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td colspan="4" align="center">
            <asp:Button ID="btnAddSlot" runat="server" CssClass="button" Text="Add Slot" />
            <asp:Button ID="btnUpdateSlot" runat="server" CssClass="button" Text="Update Slot" />
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" /></td>
    </tr>

    <tr id="tr_copy" runat="server">
        <td class="title-bg" colspan="4">Copy slot(s)                                                  
        </td>
    </tr>
    <tr id="tr_chk" runat="server">
        <td align="left" colspan="4">
            <asp:Panel ID="PLCHK_dATE" runat="server" Height="50px" Width="100%" ScrollBars="Vertical">
                <asp:CheckBoxList ID="chkDates" runat="server" RepeatDirection="Horizontal" CellPadding="3"
                    RepeatColumns="5" RepeatLayout="Table">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
    </tr>


</table>
<table width="100%" style="margin-top: 0px; padding-top: 0px;">
    <tr>
        <td align="center">
            <asp:Button ID="btnSaveSlot" runat="server" CssClass="button" Text="Save" /></td>
    </tr>
    <tr>
        <td align="left">
            <div id="lblError" runat="server" width="100%"></div>
            <asp:HiddenField ID="hfESTW_ID" runat="server" />
            <asp:HiddenField ID="hfID" runat="server" />
            <asp:CheckBox ID="chkEvenYes" runat="server" Text="Yes" Checked="true" Visible="false" /></td>
    </tr>
</table>
