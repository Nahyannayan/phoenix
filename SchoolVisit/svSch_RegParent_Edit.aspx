<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="svSch_RegParent_Edit.aspx.vb" Inherits="SchoolVisit_svSchedule_Event_Edit"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="cphMasterpage" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkControl") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }


        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var oWnd = radopen(url, "pop_form");
            <%--result = window.showModalDialog("svPopupForm.aspx?multiselect=true&ID=ENQ", "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_EQS_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }--%>
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            //get the transferred arguments
          
            var arg = args.get_argument();

            if (arg) {
               
                //NameandCode = arg.Result.split('___');
                
                document.getElementById('<%=h_EQS_IDs.ClientID %>').value = arg.Result;

               <%-- if (NameandCode.length > 2) {
                    document.getElementById('TabContainer1_TabPanel2_HF_SIBLING_TYPE').value = NameandCode[2];

                     <%-- __doPostBack('<% =txtSibno.ClientID%>', 'TextChanged');--%>
                }
            }
        
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_form" runat="server" Behaviors="Close,Move"   OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>                
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Allocate Parents
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <div id="lblError" runat="server"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table id="tbl_test" align="center" 
                                cellpadding="5" cellspacing="0" width="100%">
                               
                                    
                               <tr>
                                   <td align="left" valign="middle">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Event title</span> </td>
                                                <td>
                                                    <span class="field-value">
                                                        <asp:Literal ID="ltTitle" runat="server"></asp:Literal></span>
                                                </td>
                                                <td align="left" width="20%" ><span class="field-label">Select event date</span> </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlEventDate" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <br /><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="title-bg-small">
                                                                Select time slot
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="plReq" runat="server"  ScrollBars="Vertical" >
                                                        <asp:GridView ID="gvTime" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            CellPadding="3" DataKeyNames="ESTW_ID" PageSize="30"
                                                            CssClass="table table-bordered table-row">
                                                            <RowStyle HorizontalAlign="Center" CssClass="griditem"  />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Select">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rbTimeSlot" runat="server"
                                                                            GroupName="TimeGRP" AutoPostBack="True" Enabled='<%# bind("ESTW_bENABLED") %>'
                                                                            OnCheckedChanged="rbTimeSlot_CheckedChanged" />

                                                                        <asp:Label ID="lblESTW_ID" runat="server" Text='<%# bind("ESTW_ID") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Time Slot">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTIME_SLOT" runat="server" Text='<%# bind("TIME_SLOT") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total Allowed">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblESTW_TOT_ENTRY" runat="server"
                                                                            Text='<%# bind("ESTW_TOT_ENTRY") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Occupied">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTOT_BOOKED" runat="server" Text='<%# bind("TOT_BOOKED") %>'></asp:Label>

                                                                    </ItemTemplate>

                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Left"  />

                                                        </asp:GridView>
                                                    </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>                                                                                                                                                           
                                                </td>
                                                <td colspan="2" >
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="title-bg-small">
                                                                Select staff
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="plStaff" runat="server"  ScrollBars="Vertical" >
                                                        <asp:GridView ID="gvStaff" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            CellPadding="3" DataKeyNames="ESSW_ID" PageSize="30"
                                                            CssClass="table table-bordered table-row">
                                                            <RowStyle HorizontalAlign="Center" CssClass="griditem"  />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Select">
                                                                    <ItemTemplate>
                                                                        <asp:Literal ID="RadioButtonStaff" runat="server"></asp:Literal>
                                                                        <asp:Label ID="lblESSW_ID" runat="server" Text='<%# bind("ESSW_ID") %>'
                                                                            Visible="False"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblENAME" runat="server" Text='<%# bind("ENAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"  />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total Alloted">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTOT_ALLOTED" runat="server"
                                                                            Text='<%# bind("TOT_ALLOTED") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Left"  />

                                                        </asp:GridView>
                                                    </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>                                                                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4"><span class="field-label"> Filter  Type </span>
                                                    <asp:RadioButton ID="rbReg" runat="server" Text="Registered Parent" CssClass="field-label"
                                                    GroupName="grp" Checked="true" ForeColor="Black" AutoPostBack="True" />
                                                    <asp:RadioButton ID="rbUnReg" runat="server" Text="Unregistered Parent" CssClass="field-label"
                                                        GroupName="grp" ForeColor="Black" AutoPostBack="True" /><span id="spAddP" runat="server">Click on the link to </span>
                                                    <asp:LinkButton                                                         
                                                        ID="lbtnAddParent" runat="server" OnClientClick="GetSTUDENTS();" OnClick="lbtnENQ_Click">Add Parent</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                               </tr>

                                <tr>
                                    <td align="center"  valign="top">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" 
                                            Text="Save"  />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button" 
                                            Text="Update"  />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="button" 
                                            Text="Cancel"  />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center"  valign="top">
                                        <asp:GridView ID="gvStaff_Parent" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <HeaderTemplate>
                                                      
                                                         Select <br />
                                                                    <input id="Checkbox1" name="chkAL" onclick="change_chk_state(this);" type="checkbox"
                                                                        value="Check All" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("PSV_ID") %>' />
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Parent name">
                                                    <HeaderTemplate>
                                                      Parent Name <br />
                                                                    <asp:TextBox ID="txtParentName" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchParentName"
                                                                        OnClick="btnSearchParentName_Click"
                                                                        runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>


                                                    <ItemTemplate>
                                                        <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Child Name">
                                                    <HeaderTemplate>
                                                       Child Name<br />
                                                                    <asp:TextBox ID="txtChildName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchChildName"
                                                                        OnClick="btnSearchChildName_Click"
                                                                        runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChildName" runat="server" Text='<%# Bind("Child_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact No">
                                                    <HeaderTemplate>
                                                       Contact No <br />
                                                                    <asp:TextBox ID="txtContactNo" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchContactNo"
                                                                        OnClick="btnSearchContactNo_Click"
                                                                        runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContactNo" runat="server" Text='<%# Bind("Contact_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email Address">
                                                    <HeaderTemplate>
                                                       Email Address<br />
                                                                    <asp:TextBox ID="txtEmailAddress" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmailAddress"
                                                                        OnClick="btnSearchEmailAddress_Click"
                                                                        runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                        ImageAlign="Top" ></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmailAddress" runat="server" Text='<%# Bind("EMAIL_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Allocated Staff">
                                                    <HeaderTemplate>
                                                        Allocated Staff<br />

                                                                    <asp:TextBox ID="txtAllStaff" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchAllStaff"
                                                                        OnClick="btnSearchAllStaff_Click"
                                                                        runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblAllocated_staff" runat="server" Text='<%# Bind("STAFF_NAME") %>'></asp:Label>


                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>

                                        <asp:GridView ID="gvAddParent" runat="server" AutoGenerateColumns="False"
                                           CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            Width="100%">
                                            <RowStyle CssClass="griditem"  />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Enq.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbleqs_applno" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                                        <asp:Label ID="lblEQS_ID" runat="server" Text='<%# bind("EQS_ID") %>'
                                                            Visible="False"></asp:Label>
                                                        <asp:Label ID="lblEQS_EQM_ENQID" runat="server"
                                                            Text='<%# bind("EQS_EQM_ENQID") %>' Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPPL_NAME" runat="server" Text='<%# Bind("APPL_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblgrm_display" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Parent Name">
                                                    <ItemTemplate>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="LEFT" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Contact No">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContactNo" runat="server" Text='<%# Bind("Contact_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>

                <asp:HiddenField ID="hfEM_ID" runat="server" Value="0" />
                <asp:HiddenField ID="h_EQS_IDs" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

