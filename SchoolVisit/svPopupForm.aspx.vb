Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class SelBussinessUnit
    Inherits BasePage
    'Shared Session("liUserList") As List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Session("liUserList") = Nothing
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
                Session("liUserList") = Nothing
            End If
        End If
        getAcademicYear_date()
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        ViewState("multiSel") = True
        If Page.IsPostBack = False Then


            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()

        SetChk(Me.Page)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID")
           

            Case "ENQ"
             
                GridBindEnquiryDetails()

        End Select
    End Sub

    

    Sub GridBindEnquiryDetails()
        Try

            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim param(3) As SqlParameter
            Dim txtSearch As New TextBox
            Dim ds As New DataSet
            Dim Str_eqs_applno As String = String.Empty
            Dim Str_enqdate As String = String.Empty
            Dim Str_APPL_NAME As String = String.Empty
            Dim str_grm_display As String = String.Empty
            Dim str_PARENT_NAME As String = String.Empty
            Dim str_EQS_CURRSTATUS As String = String.Empty
            Dim str_CONTACT_NO As String = String.Empty
            If gvGroup.Rows.Count > 0 Then
                txtSearch = gvGroup.HeaderRow.FindControl("txteqs_applno")
                Str_eqs_applno = " AND eqs_applno like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvGroup.HeaderRow.FindControl("txtenqdate")
                Str_enqdate = " AND enqdate like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvGroup.HeaderRow.FindControl("txtAPPL_NAME")
                Str_APPL_NAME = " AND APPL_NAME like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvGroup.HeaderRow.FindControl("txtgrm_display")
                str_grm_display = " AND grm_display like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvGroup.HeaderRow.FindControl("txtPARENT_NAME")
                str_PARENT_NAME = " AND PARENT_NAME like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

                txtSearch = gvGroup.HeaderRow.FindControl("txtCONTACT_NO")
                str_CONTACT_NO = " AND CONTACT_NO like '%" & txtSearch.Text.Replace(" ", "%") & "%'"

            End If
            If ddlStatus.SelectedValue <> "0" Then
                str_EQS_CURRSTATUS = " AND EQS_CURRSTATUS=' " & ddlStatus.SelectedValue & "'"
            End If

          

            param(0) = New SqlParameter("@ACD_ID", ddlAcd_id.selectedvalue)
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(2) = New SqlParameter("@FILTER_COND", Str_eqs_applno + Str_enqdate + Str_APPL_NAME + str_grm_display + str_PARENT_NAME + str_CONTACT_NO + str_EQS_CURRSTATUS)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV.GETPOPUP_INFO", param)

            If ds.Tables(0).Rows.Count > 0 Then

                gvGroup.DataSource = ds.Tables(0)
                gvGroup.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(6) = True

                gvGroup.DataSource = ds.Tables(0)
                Try
                    gvGroup.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub getAcademicYear_date()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim DATE_STRING As String = String.Empty
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@PREV_ACD_ID", Session("prev_ACD_ID"))
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETACADEMIC_PREV_CURR_NEXT", param)
            If datareader.HasRows = True Then
                ddlAcd_id.DataSource = datareader
                ddlAcd_id.DataTextField = "ACY_DESCR"
                ddlAcd_id.DataValueField = "ACD_ID"
                ddlAcd_id.DataBind()


            End If
        End Using


        If Not ddlAcd_id.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
            ddlAcd_id.ClearSelection()
            ddlAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        End If

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        
        SetChk(Me.Page)
        h_SelectedId.Value = ""

        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "|"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("var oArg = new Object();")
        Response.Write("oArg.Result = document.getElementById('h_SelectedId').value;")
        Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value;)")
        Response.Write("oWnd.close(oArg);")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        Response.Write("} </script>")

    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim retstring As String = String.Empty
        If (Not lblcode Is Nothing) Then
         
            retstring = lblcode.Text & "|" '& "___" & lbClose.Text.Replace("'", "\'")
            '   Response.Write(lblcode.Text)
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("var oArg = new Object();")
            Response.Write("oArg.Result = document.getElementById('h_SelectedId').value;")
            Response.Write("var oWnd = GetRadWindow(document.getElementById('h_SelectedId').value;)")
            Response.Write("oWnd.close(oArg);")
            'Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.returnValue = '" & retstring & "';")
            'Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub
    Protected Sub btneqs_applnoSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnenqdateSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnAPPL_NAMESearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btngrm_displaySearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnParent_NAMESearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnContact_NoSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
End Class

