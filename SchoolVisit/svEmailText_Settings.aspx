<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" 
CodeFile="svEmailText_Settings.aspx.vb" Inherits="svEmailText_Settings"    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

   
      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Literal id="ltHeader" runat="server" Text="Email Text"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="Table1" border="0" width="100%">
        
          <tr>
            <td align="left">
          <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    
   <table align="center" width="100%" cellpadding="5" cellspacing="0" >
      
        
        
         <tr>
            <td align="left" width="25%">
              <span class="field-label">Content Type</span> 
            </td>
            <td align="left" width="35%"><asp:DropDownList ID="ddlInfoType" runat="server"  AutoPostBack="true">
               
               <asp:ListItem Text="Enquiry Popup Text" Value="EPT"></asp:ListItem>
               <asp:ListItem Text="GEMS Online Enquiry Login Credentials" Value="ETE"></asp:ListItem>
                   <asp:ListItem Text="Tracking WebSite Address" Value="TWA"></asp:ListItem>
               <asp:ListItem Text="Booking Email" Value="BE"></asp:ListItem>
               <asp:ListItem Text="Booking Cancellation Email" Value="ECE"></asp:ListItem>
               <asp:ListItem Text="Booking Reschedule Email" Value="ERE"></asp:ListItem>
               </asp:DropDownList></td>
            <td width="20%"></td>
             <td width="20%"></td>
        </tr>
        <tr><td  align="left"><span class="field-label">Copy to all Schools</span></td>
        <td align="left">
            <asp:CheckBox ID="chkCopy" runat="server"  Checked="true"/></td>
            <td></td>
            <td></td>
        </tr>
          <tr>    <td colspan="4">
                                                <telerik:RadEditor ID="txtOfferText" runat="server" EditModes="All" 
                                                    Height="350px"  Width="100%">
                                                </telerik:RadEditor>

                                            </td>
                                        </tr>
                                        
                                         <tr>
            <td align="center"  colspan="4">
               <asp:Button id="btnSave" runat="server" CssClass="button"  Text="Save" />
            </td>
        </tr>
        
        
   </table>

                </div>
            </div>
          </div>
  
</asp:Content>

