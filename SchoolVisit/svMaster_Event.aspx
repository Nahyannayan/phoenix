<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="svMaster_Event.aspx.vb" Inherits="svMaster_Event" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Event Master"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom">
                            <div id="lblError" runat="server"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="5" cellspacing="0" width="100%">
                                <tr align="left">
                                    <td width="20%"><span class="field-label">Event Category</span> <span class="text-danger">*</span></td>
                                    <td>
                                        <asp:TextBox ID="txtTitle" runat="server" MaxLength="80"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtTitle" Font-Size="Medium" CssClass="text-danger" Display="Dynamic"
                                            ErrorMessage="Category required" SetFocusOnError="True"
                                            ValidationGroup="usr">Category required</asp:RequiredFieldValidator>
                                    </td>
                                    <td width="20%"></td>
                                    <td width="20%"></td>
                                </tr>
                                <tr align="left">
                                    <td><span class="field-label">Description</span></td>
                                    <td>
                                        <asp:TextBox ID="txtDescr" runat="server" SkinID="MultiText" TextMode="MultiLine"
                                            ></asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnAddEvent" runat="server" Text="Add/Save"
                                            CssClass="button" ValidationGroup="usr" />
                                        <asp:Button ID="btnUpdateEvent"
                                            runat="server" Text="Update/Save" CssClass="button"
                                            ValidationGroup="usr" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CausesValidation="False"/>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvCommonEmp" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" DataKeyNames="EM_ID" EmptyDataText="Record not available !!!"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEM_ID" runat="server" Text='<%# Bind("EM_ID") %>'></asp:Label>
                                            <asp:Label ID="lblEM_DESCR" runat="server" Text='<%# bind("EM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("r1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEM_TITLE" runat="server" Text='<%# bind("EM_TITLE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" Enabled='<%# bind("bEdit") %>'
                                                Text="Edit"
                                                OnClick="lbtnEdit_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>


</asp:Content>

