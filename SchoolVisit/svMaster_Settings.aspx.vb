Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class svMaster_Settings
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> "S425005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                End If


                gridbind_Parent()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:red;'>Request could not be processed</div>"
            End Try
        End If
    End Sub

    Private Sub gridbind_Parent()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = String.Empty
            Dim STR_FILTER_CATEGORY_DES As String = String.Empty
            Dim str_filter_BName As String = String.Empty
            Dim param(3) As SqlParameter

            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV].[GetSchool_Visit_List]")


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'ds.Tables(0).Rows(0)(8) = True
                ds.Tables(0).Rows(0)("bENQ_USR_NAME") = True
                ds.Tables(0).Rows(0)("bPAY_REG_FEE") = True
                ds.Tables(0).Rows(0)("bSCHOOL_TOUR") = True
                ds.Tables(0).Rows(0)("bSCHOOL_VISIT") = True

                gvManageUser.DataSource = ds.Tables(0)
                gvManageUser.DataBind()
                Dim columnCount As Integer = gvManageUser.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvManageUser.Rows(0).Cells.Clear()
                gvManageUser.Rows(0).Cells.Add(New TableCell)
                gvManageUser.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUser.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUser.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUser.DataSource = ds.Tables(0)
                gvManageUser.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub gvManageUser_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUser.PageIndexChanging
        gvManageUser.PageIndex = e.NewPageIndex
        gridbind_Parent()
    End Sub


    Protected Sub gvManageUser_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvManageUser.RowCommand

        Dim ARG As String = e.CommandArgument

        If e.CommandName = "Enq_Log" Then
            changeStatus(ARG, "Enq_Log")

        ElseIf e.CommandName = "Reg_Fee" Then
            changeStatus(ARG, "Reg_Fee")
        ElseIf e.CommandName = "Sch_Tour" Then
            changeStatus(ARG, "Sch_Tour")
        ElseIf e.CommandName = "Sch_Visit" Then
            changeStatus(ARG, "Sch_Visit")

        End If

    End Sub

    Sub changeStatus(ByVal BSU_ID As String, ByVal FLAG As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@FLAG", FLAG)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[SV].[SaveSchool_Visit_List]", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value

            If ReturnFlag <> 0 Then
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:red;padding:5pt;background-color:#edf3fa;'>Error Occured While Saving.</div>"

            Else
                gridbind_Parent()
                lblError.InnerHtml = "<div style='border: 1px solid #1B80B6;width: 98%; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color:#1b80b6;padding:5pt;background-color:#edf3fa;'>Record updated Successfully !!!</div>"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "changeStatus")
        End Try

    End Sub



End Class
