﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="rptSchTour_Statistics.aspx.vb" Inherits="rptSchTour_Statistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>School Tour Statistics
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblErr" runat="server" EnableViewState="false" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" style="width: 100%;">

                                <tr id="I19" <%--style="display: table;"--%>>
                                    <td align="left" style="width: 20%"><span class="field-label">School</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" style="width: 20%"><span class="field-label">Staff</span></td>
                                    <td align="left" style="width: 30%">
                                        <asp:DropDownList ID="ddlEmp_id" runat="server">
                                        </asp:DropDownList>

                                    </td>
                                </tr>


                                <tr id="I20" <%--style="display: table;"--%>>
                                    <td align="left"  style="width:20%"><span class="field-label">Start Date</span>
                                    </td>
                                    <td align="left"  style="width:30%">
                                        <asp:TextBox ID="txtEventStartDt" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnEventStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Height="16px" /><div style="font-size: 10px;">(dd/mmm/yyyy)         </div>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEventStartDt" TargetControlID="txtEventStartDt">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left"  style="width:20%"><span class="field-label">End Date</span>
                                    </td>
                                    <td align="left"  style="width:30%">
                                        <asp:TextBox ID="txtEventEndDt" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnEventEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Height="16px" /><div style="font-size: 10px;">(dd/mmm/yyyy)         </div>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEventEndDt" TargetControlID="txtEventEndDt">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                </tr>
                                <tr id="I22" <%--style="display: table;"--%>>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenRpt" runat="server" CssClass="button" Text="Generate Report"/>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
