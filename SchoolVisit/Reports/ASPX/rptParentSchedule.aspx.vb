﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.IO
Partial Class rptParentSchedule
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S285010") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights               
                getAccess_rights()
                getEvent_CAT()
                GETATT_PARAM_D()

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Sub geteventDate()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ESM_ID", ddlTitle.SelectedValue)
        param(1) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETEVENT_REG_DATE", param)
            If dataread.HasRows = True Then
                While dataread.Read

                    txtEventStartDt.Text = Convert.ToString(dataread("MIN_VISIT_DATE"))

                    txtEventEndDt.Text = Convert.ToString(dataread("MAX_VISIT_DATE"))
                End While

            Else
                txtEventStartDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtEventEndDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

            End If

        End Using
    End Sub
    Private Sub GETATT_PARAM_D()

        Dim conn As String = ConnectionManger.GetOASISConnectionString

        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETEVENT_ATT_PARAM")
            If dataread.HasRows = True Then
                ddlAttendance.DataSource = dataread
                ddlAttendance.DataTextField = "EAP_DESCR"
                ddlAttendance.DataValueField = "EAP_ID"
                ddlAttendance.DataBind()

            End If
        End Using

       
    End Sub
    Private Sub getAccess_rights()
        Try
            ViewState("bEDIT") = False
            ViewState("Can_access") = False
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETACCESS_RIGHT_TIME_SLOT", param)
                If dataread.HasRows = True Then
                    While dataread.Read

                        ViewState("bEDIT") = Convert.ToBoolean(dataread("ATS_bEDIT"))

                    End While
                    ViewState("Can_access") = True
                    chkStaff.Enabled = True
                Else
                    chkStaff.Enabled = False
                End If

            End Using
            'ddlCat_SelectedIndexChanged(ddlCat, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getEvent_CAT()
        Try


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETEVENT_M", param)
                If dataread.HasRows = True Then
                    ddlCat.DataSource = dataread
                    ddlCat.DataTextField = "EM_TITLE"
                    ddlCat.DataValueField = "EM_ID"
                    ddlCat.DataBind()
                Else
                    ddlCat.Items.Clear()

                End If

            End Using
            ddlCat_SelectedIndexChanged(ddlCat, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getEvent_TITLE()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETSCHEDULE_LIST", param)
                If dataread.HasRows = True Then
                    ddlTitle.DataSource = dataread
                    ddlTitle.DataTextField = "ESM_DISPLAY_TITLE"
                    ddlTitle.DataValueField = "ESM_ID"
                    ddlTitle.DataBind()
                Else
                    ddlTitle.Items.Clear()

                End If

            End Using


            ddlTitle_SelectedIndexChanged(ddlTitle, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub getEvent_STAFF()
        Try


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ESM_ID", ddlTitle.SelectedValue)
            param(1) = New SqlParameter("@EM_ID", ddlCat.SelectedValue)
            Using dataread As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GETEVENT_STAFF_LIST", param)
                If dataread.HasRows = True Then
                    chkStaff.DataSource = dataread
                    chkStaff.DataTextField = "ENAME"
                    chkStaff.DataValueField = "EMP_ID"
                    chkStaff.DataBind()
                Else
                    chkStaff.Items.Clear()

                End If

            End Using


            If ViewState("Can_access") = True Then
                For Each chkItem As ListItem In chkStaff.Items
                    chkItem.Selected = True

                Next

            Else
                For Each chkItem As ListItem In chkStaff.Items
                    If chkItem.Value = Session("EmployeeId") Then
                        chkItem.Selected = True
                    End If



                Next

            End If


        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCat.SelectedIndexChanged
        Call getEvent_TITLE()
    End Sub
    Protected Sub ddlTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTitle.SelectedIndexChanged
        Call getEvent_STAFF()
        Call geteventDate()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtEventStartDt.Text.Trim <> "" Then
                Dim strfDate As String = txtEventStartDt.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtEventStartDt.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtEventStartDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtEventEndDt.Text.Trim <> "" Then

                Dim strfDate As String = txtEventEndDt.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtEventEndDt.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtEventStartDt.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtEventEndDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtEventStartDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If
            lblErr.Text = "<div>" & CommStr & "</div>"
            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return "-1"
        End Try
    End Function

    Private Sub CallReport(ByVal emp_ids As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_TIMETABLEConnectionString
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EMP_IDS", emp_ids)
        param.Add("@FROMDT", Format(Date.Parse(txtEventStartDt.Text.Trim), "dd/MMM/yyyy"))
        param.Add("@TODATE", Format(Date.Parse(txtEventEndDt.Text.Trim), "dd/MMM/yyyy"))
        param.Add("@ATT_PARAM", ddlAttendance.SelectedValue)
        param.Add("FROMDT", Format(Date.Parse(txtEventStartDt.Text), "dd/MMM/yyyy"))
        param.Add("TODT", Format(Date.Parse(txtEventEndDt.Text), "dd/MMM/yyyy"))      
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "OASIS"
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptParentSchedule.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub btnGenRpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenRpt.Click
        Dim Str_ValidateDate As String = String.Empty
        Str_ValidateDate = ValidateDate()
        Dim emp_ids As String = String.Empty
        For Each chkItem As ListItem In chkStaff.Items
            If chkItem.Selected = True Then
                emp_ids += chkItem.Value & "|"
            End If
        Next
        If Str_ValidateDate <> "" Then
            Exit Sub
        End If
        If emp_ids.ToString = "" Then
            lblErr.Text = "<div>Please select the staff </div>"
            Exit Sub
        End If
        CallReport(emp_ids.ToString)
    End Sub


   
  
End Class
