﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master"
    AutoEventWireup="false" CodeFile="rptParentSchedule.aspx.vb" Inherits="rptParentSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>School Tour Schedule

        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table width="100%">
                    <tr>                        
                        <td colspan="4" align="left">
                            <span class="error"><asp:Literal ID="lblErr" runat="server" EnableViewState="false"></asp:Literal></span>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <table align="center" style="width: 100%;">
                                <tr >
                                    <td align="left" width="20%"><span class="field-label">Event Category</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCat" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Event Title</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlTitle" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr  id="I20" <%--style="display: table;"--%> >  
                                    <td align="left"><span class="field-label">Start Date</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEventStartDt" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnEventStartDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Height="16px" /><div style="font-size: 10px;">(dd/mmm/yyyy)         </div>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEventStartDt" TargetControlID="txtEventStartDt">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left"><span class="field-label">End Date</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEventEndDt" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnEventEndDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                            Height="16px" /><div style="font-size: 10px;">(dd/mmm/yyyy)         </div>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEventEndDt" TargetControlID="txtEventEndDt">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Attendance Type</span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAttendance" runat="server">
                                        </asp:DropDownList>

                                    </td>
                                </tr>

                                <tr  id="I21"  <%--style="display: table;"--%> >                                             
                                    <td align="left"><span class="field-label">Staff</span>
                                    </td>
                                    <td align="left">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="chkStaff" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>

                                </tr>
                                <tr  id="I22" <%--style="display: table;"--%>>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenRpt" runat="server" CssClass="button" Text="Generate Report"
                                            />
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>
