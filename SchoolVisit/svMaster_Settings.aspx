<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="svMaster_Settings.aspx.vb"  Inherits="svMaster_Settings" Theme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">


</script>

  <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal id="ltHeader" runat="server" Text="School Tour Settings"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">



    <table align="center" cellpadding="0" cellspacing="0" width="100%">
            <tr valign="top" > 
                <td align="left">
                  <div ID="lblError" runat="server"></div></td>
            </tr>
   
                   
            <tr>
                <td align="center" > 
           <asp:GridView id="gvManageUser" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="50" Width="100%" DataKeyNames="BSU_ID">
                                <rowstyle cssclass="griditem" />
                                <columns>
                                    <asp:BoundField DataField="r1" HeaderText="Sr.No">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
<asp:TemplateField HeaderText="Bsu_id" Visible="False">
    <ItemTemplate>
        <asp:Label ID="lblBSU_ID" runat="server"      Text='<%# Bind("BSU_ID") %>'></asp:Label>
    </ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="School name">
<ItemTemplate>
                                <asp:Label ID="lblSchoolH" runat="server" Text='<%# Bind("bsu_name") %>'></asp:Label>
                            </ItemTemplate>
<HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

</asp:TemplateField>
<asp:TemplateField HeaderText="Enquiry Login">
 
                            <ItemStyle HorizontalAlign="Left"  VerticalAlign="Middle" Wrap="True"></ItemStyle>
    <ItemTemplate>
        <asp:ImageButton ID="btnEnq" runat="server" CausesValidation="False" 
            CommandArgument='<%# Bind("BSU_ID") %>' CommandName="Enq_Log" 
            ImageUrl='<%# Bind("bENQ_IMG") %>' 
            OnClientClick="return confirm('Are you sure you want to update the status?'); " />
    </ItemTemplate>
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
</asp:TemplateField>
<asp:TemplateField HeaderText="Registration Fees">

  
    <ItemTemplate>
        <asp:ImageButton ID="btnReg" runat="server" CausesValidation="False" 
            CommandArgument='<%# Bind("BSU_ID") %>' CommandName="Reg_Fee" 
            ImageUrl='<%# Bind("bPAY_IMG") %>' 
            OnClientClick="return confirm('Are you sure you want to update the status?'); " />
    </ItemTemplate>
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
</asp:TemplateField>
<asp:TemplateField HeaderText="School Tour">

  
    <ItemTemplate>
        <asp:ImageButton ID="btnSchTour" runat="server" CausesValidation="False" 
            CommandArgument='<%# Bind("BSU_ID") %>' CommandName="Sch_Tour" 
            ImageUrl='<%# Bind("bSCHOOL_TOUR_IMG") %>' 
            OnClientClick="return confirm('Are you sure you want to update the status?'); " />
    </ItemTemplate>
    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
</asp:TemplateField>



</columns>
                                <selectedrowstyle backcolor="Wheat" />
                                <headerstyle cssclass="gridheader_pop" horizontalalign="Center" verticalalign="Middle" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
            
            
            </td>
            </tr>
            </table>

                </div>
            </div>
      </div>

</asp:Content>