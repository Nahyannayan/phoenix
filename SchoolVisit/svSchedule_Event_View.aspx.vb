Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class SchoolVisit_svSchedule_Event_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S425015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    bind_event()
                    BIND_ACCESS_FOR()
                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub
    Private Sub bind_event()
        Try
            ddlEM_ID.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETEVENT_M", params)
                If datareader.HasRows = True Then
                    While datareader.Read
                        ddlEM_ID.Items.Add(New ListItem(Convert.ToString(datareader("EM_TITLE")), Convert.ToString(datareader("EM_ID"))))
                    End While
                End If
            End Using
            ddlEM_ID.Items.Add(New ListItem("ALL", "0"))
            ddlEM_ID.ClearSelection()
            ddlEM_ID.Items.FindByValue("0").Selected = True

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BIND_ACCESS_FOR()
        Try
            ddlAccessFor.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETEVENT_ACCESS_FOR")
                If datareader.HasRows = True Then
                    While datareader.Read
                        ddlAccessFor.Items.Add(New ListItem(Convert.ToString(datareader("EAF_DESCR")), Convert.ToString(datareader("EAF_ID"))))
                    End While
                End If
            End Using
            ddlAccessFor.Items.Add(New ListItem("Select", "0"))
            ddlAccessFor.ClearSelection()
            ddlAccessFor.Items.FindByValue("0").Selected = True

        Catch ex As Exception

        End Try
    End Sub
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            Dim EventGRP As String = String.Empty
            Dim EventTitle As String = String.Empty
            Dim EventStartDT As String = String.Empty
            Dim EventEndDt As String = String.Empty
            Dim AccessStartDt As String = String.Empty
            Dim AccessEndDt As String = String.Empty
            Dim Open_for As String = String.Empty
            Dim PARAM(2) As SqlParameter
            If ddlEM_ID.SelectedValue <> "0" Then
                EventGRP = " AND ESM_EM_ID = '" & ddlEM_ID.SelectedValue.Trim & "'"
            End If

            If txtTitle.Text.Trim <> "" Then
                EventTitle = " AND ESM_DISPLAY_TITLE like '%" & txtTitle.Text.Trim & "%'"
            End If

            If txtEventStartDt.Text.Trim <> "" Then
                EventStartDT = " AND EVENT_STARTDT like '%" & txtEventStartDt.Text.Trim & "%'"
            End If

            If txtEventEndDt.Text.Trim <> "" Then
                EventEndDt = " AND EVENT_ENDDT like '%" & txtEventEndDt.Text.Trim & "%'"
            End If
            If txtAccessStartDt.Text.Trim <> "" Then
                AccessStartDt = " AND ACCESS_STARTDT like '%" & txtAccessStartDt.Text.Trim & "%'"
            End If

            If txtAccessEndDt.Text.Trim <> "" Then
                AccessEndDt = " AND ACCESS_ENDDT like '%" & txtAccessEndDt.Text.Trim & "%'"
            End If


            If ddlAccessFor.SelectedValue <> "0" Then
                Open_for = " AND ESM_EAF_ID = '" & ddlAccessFor.SelectedValue.Trim & "'"

            End If
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@FILTER_COND", EventGRP + EventTitle + EventStartDT + EventEndDt + AccessStartDt + AccessEndDt + Open_for)


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV].[GETSCHEDULE_M_GRIDBIND]", PARAM)


            If ds.Tables(0).Rows.Count > 0 Then

                gvSchedule.DataSource = ds.Tables(0)
                gvSchedule.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(6) = True

                gvSchedule.DataSource = ds.Tables(0)
                Try
                    gvSchedule.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSchedule.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSchedule.Rows(0).Cells.Clear()
                gvSchedule.Rows(0).Cells.Add(New TableCell)
                gvSchedule.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSchedule.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSchedule.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub gvSchedule_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSchedule.PageIndexChanging
        gvSchedule.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        gridbind()
    End Sub



    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblESM_ID As New Label
            Dim url As String
            Dim viewid As String
            lblESM_ID = TryCast(sender.FindControl("lblESM_ID"), Label)
            viewid = lblESM_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\SchoolVisit\svSchedule_Event_Edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\SchoolVisit\svSchedule_Event_Edit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub





End Class
