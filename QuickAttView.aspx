﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="QuickAttView.aspx.vb" Inherits="QuickAttView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">



        function AddDetails() {
            var sFeatures;
            sFeatures = "dialogWidth: 380px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var result;
            result = window.showModalDialog("editmenuAccess.aspx", "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }
            return false;
        }

        function onCancel() {
            // var postBack = new Sys.WebForms.PostBackAction(); 
            //postBack.set_target('CancelButton'); 
            //postBack.set_eventArgument(''); 
            // postBack.performAction();
        }

        function onOk() {

        }


        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {

                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvAttQuick.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }

    </script>

    <script type="text/javascript">
        function doBlink() {
            var i = document.getElementById("BLINK").style.color;
            if (i == '#1b80b6')
                document.getElementById("BLINK").style.color = '#ffffff';
            else
                document.getElementById("BLINK").style.color = '#1b80b6';
        }

        function startBlink() {
            document.getElementById("BLINK").style.color = '#1b80b6';
            if (document.all)
                setInterval("doBlink()", 800)
        }
        window.onload = startBlink;
    </script>
</head>
<body class="bg-dark bg-white">

    <form id="form1" runat="server">

        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upd1" runat="server">
            <ContentTemplate>
                <div id="pnlQuickAtt" runat="server">
                    <div>
                        <div align="center">
                            <span class="field-label">Quick Attendance Preview</span>
                            <br />
                        </div>
                    </div>
                    <div class="table-responsive">
                        <asp:GridView ID="gvAttQuick" runat="server" Width="90%" align="center" AutoGenerateColumns="False"
                            OnRowDataBound="gvAttQuick_RowDataBound" CssClass="table table-bordered table-row" OnRowEditing="gvAttQuick_RowEditing">
                            <RowStyle HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Select">

                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this); " runat="server"
                                            ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                        <asp:Label ID="lblGroup" runat="server" Text='<%# bind("GROUP_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Grade">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("GRD_DES") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGRD_ID" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Strength">
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("TOT_STR") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TOT_MARKSTR" HeaderText="Att. Marked">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Present">
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("TOT_PRS") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Absent">
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("TOT_ABS") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Per(%)">
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("TOT_PERC", "{0:0.00}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                        </asp:GridView>
                    </div>

                    <div align="center">
                        <div align="center">
                            <span class="text-warnning">
                                <asp:Label ID="lblAttGrd" runat="server" Text="No grade(s) selected for detail view !!!"></asp:Label></span>
                            <br />
                            <asp:Button ID="btnDetailView" runat="server" Text="Detail View" CssClass="button"
                                OnClick="btnDetailView_Click" />
                             <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button"
                                OnClick="btnPrint_Click" />

                        </div>
                    </div>
                    <asp:HiddenField ID="hfweek1" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hfweek2" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hfstartdt" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hfenddt" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hfYear" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hfAttQuick" runat="server"></asp:HiddenField>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
