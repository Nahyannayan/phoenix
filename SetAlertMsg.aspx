<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="SetAlertMsg.aspx.vb" Inherits="SetAlertMsg" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <table id="tblMain" runat="server" align="center" border="1" bordercolor="#1b80b6"
        cellpadding="5" cellspacing="0" style="width: 80%; height: 25px">
        <tr class="subheader_img">
            <td align="left" colspan="8" valign="middle">
                <asp:Label ID="lblrptCaption" runat="server" Text="Set Alert Message"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 134px">
                Alert Message</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6" style="text-align: left">
                <asp:TextBox ID="txtAlert" runat="server" MaxLength="300" Width="400px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" class="matters" style="width: 134px">
                Time</td>
            <td class="matters">
                :</td>
            <td align="left" class="matters" colspan="6" style="text-align: left">
                <asp:TextBox ID="txtTime" runat="server" Width="55px"></asp:TextBox>
                (How much time(in Minutes) the Alert should be displayed. eg. if you set to 5 the
                alert messge will be displayed 5 minutes from now)</td>
        </tr>
        <tr>
            <td align="left" class="matters" colspan="8" style="text-align: right">
                &nbsp;
                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Set Alert" />
                &nbsp;&nbsp;
                <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

