<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="UserDataRequestEntry.aspx.vb" Inherits="UserDataRequestEntry" Title="Untitled Page" EnableViewState="True" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>--%>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.mtz.monthpicker.js" type="text/javascript"></script>

    <%-- <link href="../cssfiles/StyleSheet.css" rel="stylesheet" />--%>
    <%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>
    <style>
        /*bootstrap class overrides starts here*/
        .card-body {
            padding: 0.25rem !important;
        }

        table th, table td {
            padding: 0.1rem !important;
        }

        /*bootstrap class overrides ends here*/


        /***/
        html body .riSingle .riTextBox[type="text"] {
            padding: 10px;
        }

        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
        }

        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
            background-position: 0 !important;
        }

        .RadPicker { /*width:80% !important;*/
        }

        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }

        /***/


        .RadGrid {
            border-radius: 0px !important;
            overflow: hidden;
        }

        .RadGrid_Default .rgCommandTable td {
            border: 0;
        }




        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            /*width: 80%;*/
            background-image: none !important;
            background-color: #fff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        .RadComboBox .rcbInputCell {
            width: 100%;
            height: 31px !important;
            background-color: transparent !important;
            border-radius: 6px !important;
            background-image: none !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
            padding: 0px !important;
        }

        .RadComboBox_Default {
            /* color: #333; */
            font: inherit;
            width: 80% !important;
            font-style: normal !important;
        }

            .RadComboBox_Default .rcbEmptyMessage {
                font-style: normal !important;
            }

            .RadComboBox_Default .rcbFocused .rcbInput {
                color: black;
                /*padding: 10px;*/
            }

        .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image: none !important;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0px !important;
            border-width: 0;
        }

        /* overwriting bootstrap css class for caption*/
        .caption {
            padding: 0px inherit !important;
        }

        .RadComboBox .rcbReadOnly .rcbInput {
            cursor: default;
            /*border: 1px solid rgba(0,0,0,0.12) !important;
    padding: 20px 10px !important;*/
        }

        table.RadCalendarMonthView_Default {
            background-color: #ffffff !important;
        }

        .RadGrid_Default {
            border: 0px !important;
        }

            .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
                border: 1px solid rgba(0,0,0,0.16);
            }

        .RadComboBoxDropDown .rcbList {
            line-height: 30px !important;
            font-size: 14px;
        }

        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border: 1px solid rgba(0, 0, 0, 0.16) !important;
        }

        .RadGrid_Default .rgGroupPanel {
            background-image: none !important;
        }

            .RadGrid_Default .rgGroupPanel td {
                padding: 0px !important;
            }

        .RadGrid .rgNumPart a {
            line-height: 6px !important;
            padding: 0px 1px !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
       
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server" Font-Bold="True"></asp:Label>
        </div>
        <div class="card-body">
            <div class="m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
                <table id="Table1" align="center" width="100%">

                    <tr>
                        <td>
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left">
                                        <table width="100%">
                                            <tr id="tr1" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Title </span>
                                                </td>
                                                <td align="left" runat="server" width="80%">
                                                    <asp:Label ID="lblTitle" runat="server" CssClass="field-value"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trSearchStr" runat="server">
                                                <td id="Td9" width="20%" align="left" runat="server"><span class="field-label">Description</span>
                                                </td>
                                                <td id="Td10" align="left" runat="server" width="80%">
                                                    <asp:Label ID="lblDescription" runat="server" CssClass="field-value"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trAsOnDate" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Last Date for Submission</span>
                                                </td>
                                                <td align="left" runat="server" width="80%">
                                                    <telerik:RadDatePicker ID="txtLastDate" runat="server" DateInput-EmptyMessage="MinDate">
                                                        <DateInput ID="DateInput3" EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" runat="server">
                                                        </DateInput>
                                                        <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                                            ViewSelectorText="x" runat="server">
                                                        </Calendar>
                                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr id="tr3" runat="server">
                                                <td id="Td1" align="left" runat="server" width="20%"><span class="field-label">Remarks</span>
                                                </td>
                                                <td id="Td2" align="left" runat="server" width="80%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trStatus" runat="server">
                                                <td id="Td111" align="left" runat="server" width="20%"></td>
                                                <td id="TdChkActive" align="left" runat="server" width="80%">
                                                    <asp:CheckBox ID="chkForward" runat="server" Text="Submit the entry" CssClass="field-label" />
                                                    <asp:Label ID="lblEntryForwarded" runat="server" Font-Bold="True" CssClass="field-label"
                                                        ForeColor="#009933"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <center>
                                            <telerik:RadGrid ID="RadGridReport" runat="server" AllowSorting="True" Width="100%"
                                                GridLines="Vertical" AllowPaging="True" PageSize="100" EnableTheming="False"
                                                CellSpacing="0" AutoGenerateColumns="false">
                                                <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="True">
                                                </ClientSettings>
                                                <AlternatingItemStyle CssClass="input-RadGridAltRow" HorizontalAlign="Left" />
                                                <MasterTableView GridLines="Both" EnableColumnsViewState="False">
                                                    <EditFormSettings>
                                                    </EditFormSettings>
                                                    <ItemStyle CssClass="input-RadGridRow" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <telerik:GridTemplateColumn UniqueName="MyTemplateColumn">
                                                            <HeaderTemplate>
                                                                DESCRIPTION
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDescr"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "DESCRIPTION")%>'></asp:Label>
                                                                <asp:HiddenField ID="h_Grd_Row_ID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ID")%>' />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </center>
                                                </td>
                                            </tr>
                                            <tr id="tr2" runat="server">
                                                <td align="center" colspan="2">
                                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdn_DRI_ID" runat="server" />
    <asp:HiddenField ID="hdn_DRH_ID" runat="server" />
    <asp:HiddenField ID="hdnRowKeys" runat="server" />
    <asp:HiddenField ID="hdnColKeys" runat="server" />
</asp:Content>
