Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Data.OleDb
Imports Telerik.Web
Imports Telerik.Web.UI

Partial Class UserDataRequestInit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property MainMnu_code() As String
        Get
            Return ViewState("MainMnu_code")
        End Get
        Set(ByVal value As String)
            ViewState("MainMnu_code") = value
        End Set
    End Property
    Private Property myBindDataTable() As DataTable
        Get
            Return ViewState("myBindDataTable")
        End Get
        Set(ByVal value As DataTable)
            ViewState("myBindDataTable") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        '   smScriptManager.RegisterPostBackControl(btnPreviewTable)
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            lblReportCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            h_Sel_Bsu_Id.Value = Session("sBsuid")
            txtLastDate.SelectedDate = Format(Now.Date.AddMonths(1), OASISConstants.DateFormat)
            FillBSU()
            btnPrint.Attributes.Add("onClick", "return ShowReport();")
            If ViewState("datamode") = "view" Then
                Dim DRI_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                FillDetails(DRI_ID)
                SetDataMode("view")
            Else : ViewState("datamode") = "add"
                trPendingBSU.Visible = False
                chkActive.Checked = True
            End If
        End If
        FillDesignations(h_Designations.Value)

    End Sub
    Sub FillDetails(ByVal DRI_ID As Int16)
        Dim sql As String
        Dim dtDataReq As New DataTable
        sql = "SP_GET_DATA_REQUEST_ENTRY_INIT_DETAILS"
        Dim sqlParam(0) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@DRI_ID", DRI_ID, SqlDbType.Int)
        dtDataReq = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)
        If dtDataReq.Rows.Count > 0 Then
            hdn_DRI_ID.Value = dtDataReq.Rows(0).Item("DRI_ID")
            txtTitle.Text = dtDataReq.Rows(0).Item("DRI_TITLE").ToString
            txtDescription.Text = dtDataReq.Rows(0).Item("DRI_DESCRIPTION").ToString
            txtLastDate.SelectedDate = dtDataReq.Rows(0).Item("DRI_REQ_LASTDT")
            chkActive.Checked = dtDataReq.Rows(0).Item("DRI_bActive")
            chkForward.Checked = dtDataReq.Rows(0).Item("DRI_bForward")
            h_Designations.Value = dtDataReq.Rows(0).Item("DRI_DES_IDS").ToString
            FillSelectedBusinessUnits(dtDataReq.Rows(0).Item("DRI_BSU_IDS").ToString)
            lblSelectedBSU.Text = dtDataReq.Rows(0).Item("BSU_NAMES").ToString
            lblPendingBSU.Text = dtDataReq.Rows(0).Item("PENDING_BSU_NAMES").ToString
            If dtDataReq.Rows(0).Item("PENDING_BSU_NAMES").ToString <> "" Then
                btnAlertPendingBSU.Visible = True
            Else
                btnAlertPendingBSU.Visible = False
            End If
            If dtDataReq.Rows(0).Item("DRI_bForward") Then
                lblEntryForwarded.Text = "Record Forwarded for data entry."
                chkForward.Visible = False
                trPendingBSU.Visible = True
            Else
                lblEntryForwarded.Text = " "
                chkForward.Visible = True
                trPendingBSU.Visible = False
            End If



            Dim RowKeys, ColKeys As String, ColDataTypes As String, RowKeysArr(), ColKeysArr() As String, ColDataTypeArr() As String
            RowKeys = ""
            ColKeys = ""
            RowKeys = dtDataReq.Rows(0).Item("DRI_ROW_KEYS").ToString
            ColKeys = dtDataReq.Rows(0).Item("DRI_COL_KEYS").ToString
            ColDataTypes = dtDataReq.Rows(0).Item("DRI_COL_DATA_TYPE").ToString

            RowKeysArr = RowKeys.Split("|")
            ColKeysArr = ColKeys.Split("|")
            ColDataTypeArr = ColDataTypes.Split("|")

            Dim lstItemStr As String
            lstRowKeyVal.Items.Clear()
            For Each lstItemStr In RowKeysArr
                If lstItemStr <> "" Then
                    Dim lstItem As New ListItem
                    lstItem.Text = lstItemStr
                    lstItem.Value = lstItemStr
                    lstRowKeyVal.Items.Add(lstItem)
                End If
            Next
            lstColumnKeyVal.Items.Clear()
            Dim i As Int16
            Dim lstColDataType As String = ""
            For i = 0 To ColKeysArr.Length - 1
                lstColDataType = ""
                lstItemStr = ""
                lstItemStr = ColKeysArr(i)
                If ColDataTypeArr.Length > i Then
                    lstColDataType = ColDataTypeArr(i)
                End If
                If lstColDataType = "" Then
                    lstColDataType = "Text"
                End If
                If lstItemStr <> "" Then
                    Dim lstItem As New ListItem
                    lstItem.Text = lstItemStr & " | " & lstColDataType
                    lstItem.Value = lstItemStr
                    lstColumnKeyVal.Items.Add(lstItem)
                End If
            Next
            PreviewTable()
        End If
    End Sub
    Private Sub FillSelectedBusinessUnits(ByVal BSU_IDS As String)
        Dim BSU_Arr() As String, BSU As String
        BSU_Arr = BSU_IDS.Split("|")
        Dim node As RadTreeNode
        For Each BSU In BSU_Arr
            For Each node In RadBSUTreeView.GetAllNodes
                If node.Value = BSU Then
                    node.Checked = True
                End If
            Next
        Next
    End Sub
    Sub FillBSU()
        Dim sql As String
        Dim dtTree As New DataTable
        sql = "SP_GET_BSU_FOR_GEN_REPORTS"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", True, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)
        RadBSUTreeView.Nodes.Clear()
        PopulateTree(RadBSUTreeView, dtTree, "0")
    End Sub
    Private Sub FillDesignations(ByVal DES_IDs As String)
        Try
            Dim IDs As String() = DES_IDs.Split("|")
            Dim DES_ID As String
            Dim condition As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim i As Int16
            For Each DES_ID In IDs
                If DES_ID.Trim <> "" Then
                    If condition.Trim <> "" Then
                        condition += ", "
                    End If
                    condition += "'" & DES_ID & "'"
                End If
                i += 1
            Next
            str_Sql = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M  WHERE DES_ID IN (" + condition + ")"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            RadGridDesignation.DataSource = ds
            RadGridDesignation.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lnkbtnDesigDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim h_Grd_Desig_ID As New HiddenField
        h_Grd_Desig_ID = TryCast(sender.FindControl("h_Grd_Desig_ID"), HiddenField)
        If Not h_Grd_Desig_ID Is Nothing Then
            Dim CurDesigIDs As String
            CurDesigIDs = "|" & h_Designations.Value & "|"
            h_Designations.Value = CurDesigIDs.Replace("|" & h_Grd_Desig_ID.Value.ToString & "|", "|") '.Replace("||||", "||")
            '  RadGridDesignation.PageIndex = RadGridDesignation.PageIndex
            FillDesignations(h_Designations.Value)
        End If

    End Sub
    Protected Sub PopulateTree(ByVal RadBSUTreeView As RadTreeView, ByRef dtTree As DataTable, ByVal PARENTID As String)
        Dim mRow As DataRow
        For Each mRow In dtTree.Select("PARENTID='" & PARENTID & "'")
            Dim root As New RadTreeNode(mRow(1).ToString(), mRow(0).ToString())
            RadBSUTreeView.Nodes.Add(root)
            CreateNode(root, dtTree, dtTree.Select("PARENTID='" & PARENTID & "'"), mRow(0).ToString())
        Next
    End Sub
    Private Function CreateNode(ByVal node As RadTreeNode, ByVal dtTree As DataTable, ByVal dtSubTree() As DataRow, ByVal ParentID As String) As Int16
        If dtSubTree.Length = 0 Then
            Return 0
        End If
        Dim mRow As DataRow
        For Each mRow In dtSubTree
            Dim tnode As New RadTreeNode(mRow(1).ToString(), mRow(0).ToString())
            If mRow(2).ToString() <> "0" Then
                node.Nodes.Add(tnode)
            Else
                tnode = node
            End If
            If dtTree.Select("PARENTID='" & mRow(0).ToString() & "'").Length > 0 Then
                CreateNode(tnode, dtTree, dtTree.Select("PARENTID='" & mRow(0).ToString() & "'"), mRow(0).ToString())
            End If
        Next
        Return 1
    End Function
    Public Function GetSelectedNode(ByVal RadTree As RadTreeView, Optional ByVal seperator As String = "||") As String
        Dim strBSUnits As New StringBuilder
        For Each node As RadTreeNode In RadTree.CheckedNodes
            'If node.Value.Length > 2 Then
            strBSUnits.Append(node.Value)
            strBSUnits.Append(seperator)
            ' End If
        Next
        Return strBSUnits.ToString()
    End Function


    Protected Sub btnLstAddRow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstAddRow.Click
        Try
            If txtRowKey.Text <> "" Then
                If lstRowKeyVal.Items.FindByValue(txtRowKey.Text) Is Nothing Then
                    Dim lstItem As New ListItem
                    lstItem.Text = txtRowKey.Text
                    lstItem.Value = txtRowKey.Text
                    lstRowKeyVal.Items.Add(lstItem)
                    txtRowKey.Text = ""
                Else
                    lblError.Text = "Row Key Value Already Exists !!!"
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnLstRemoveRow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstRemoveRow.Click
        Try
            If lstRowKeyVal.SelectedItem IsNot Nothing Then
                Dim lstItem As ListItem
                lstItem = lstRowKeyVal.SelectedItem
                lstRowKeyVal.Items.Remove(lstItem)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnLstAddCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstAddCol.Click
        Try
            If txtColumnKey.Text <> "" Then
                If lstColumnKeyVal.Items.FindByValue(txtColumnKey.Text) Is Nothing Then
                    Dim lstItem As New ListItem
                    lstItem.Text = txtColumnKey.Text & " | " & RadColDataType.SelectedValue
                    lstItem.Value = txtColumnKey.Text
                    lstColumnKeyVal.Items.Add(lstItem)
                    txtColumnKey.Text = ""
                Else
                    lblError.Text = "Row Key Value Already Exists !!!"
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnLstRemoveCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLstRemoveCol.Click
        Try
            If lstColumnKeyVal.SelectedItem IsNot Nothing Then
                Dim lstItem As ListItem
                lstItem = lstColumnKeyVal.SelectedItem
                lstColumnKeyVal.Items.Remove(lstItem)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub PreviewTable()
        Dim PreviewTable As New DataTable
        If Not PreviewTable.Columns.Contains("ID") Then
            PreviewTable.Columns.Add("ID", Type.GetType("System.Int16"))
        End If

        If Not PreviewTable.Columns.Contains("DESCRIPTION") Then
            PreviewTable.Columns.Add("DESCRIPTION", Type.GetType("System.String"))
        End If

        Dim lstItem As ListItem
        Dim Index As Int16 = 0
        Dim ColKeyDataType() As String
        For Each lstItem In lstColumnKeyVal.Items
            If Not PreviewTable.Columns.Contains("[" & lstItem.Value & "]") Then
                PreviewTable.Columns.Add("[" & lstItem.Value & "]", Type.GetType("System.String"))
            End If
            ColKeyDataType = lstItem.Text.Split("|")
            If ColKeyDataType.Length > 1 Then
                CreateNewTemplateColumn(Index, lstItem.Value, ColKeyDataType(1).Trim)
            Else
                CreateNewTemplateColumn(Index, lstItem.Value, "Text")
            End If
            Index = Index + 1
        Next
        Index = 1
        PreviewTable.Rows.Clear()
        For Each lstItem In lstRowKeyVal.Items
            Dim mRow As DataRow
            mRow = PreviewTable.NewRow
            mRow("ID") = Index
            Index = Index + 1
            mRow("DESCRIPTION") = lstItem.Text
            PreviewTable.Rows.Add(mRow)
        Next
        myBindDataTable = PreviewTable
        RadGridReport.DataSource = myBindDataTable
        RadGridReport.DataBind()
    End Sub
    Protected Sub btnPreviewTable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreviewTable.Click
        PreviewTable()
    End Sub
    Private Sub CreateNewLabelTemplateColumn(ByVal Index As Int16, ByVal ColumnKey As String)
        Dim templateColumnName As String = ColumnKey
        Dim templateColumn As New GridTemplateColumn()
        templateColumn.ItemTemplate = New MyLabelTemplate(templateColumnName)
        templateColumn.HeaderText = templateColumnName
        If Not RadGridReport.MasterTableView.Columns.Contains(templateColumn) Then
            RadGridReport.MasterTableView.Columns.Add(templateColumn)
        End If
    End Sub
    Private Sub CreateNewTemplateColumn(ByVal Index As Int16, ByVal ColumnKey As String, ByVal ColumnDataType As String)
        Dim templateColumnName As String = ColumnKey
        Dim templateColumn As New GridTemplateColumn()
        templateColumn.ItemTemplate = New MyTemplate(templateColumnName, ColumnDataType)
        templateColumn.HeaderText = templateColumnName
        templateColumn.UniqueName = templateColumnName
        If Not RadGridReport.MasterTableView.Columns.Contains(templateColumn) Then
            RadGridReport.MasterTableView.Columns.Add(templateColumn)
        End If
    End Sub
    Private Class MyTemplate
        Implements ITemplate
        Protected validatorTextBox As RequiredFieldValidator
        Protected NumericExpressionValidator As RegularExpressionValidator
        Protected hdn_colDataType As HiddenField
        Protected DatePicker As RadDatePicker

        Protected textBox As TextBox
        Private colname As String
        Private colDataType As String
        Public Sub New(ByVal cName As String, ByVal ColumnDataType As String)
            colname = cName.Replace(" ", "___")
            colDataType = ColumnDataType
        End Sub

        Protected Sub GridTxtBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim i As Int16
            i = i + 1
        End Sub

        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            If colDataType.ToLower = "text" Then
                textBox = New TextBox()
                textBox.ID = colname
                textBox.Width = "100"
                If container.FindControl(colname) Is Nothing Then
                    container.Controls.Add(textBox)
                End If
            ElseIf colDataType.ToLower = "numeric" Then
                textBox = New TextBox()
                textBox.ID = colname
                textBox.Width = "100"
                textBox.Text = "0"
                textBox.Style("text-align") = "right"
                textBox.ValidationGroup = "NumBox"
                NumericExpressionValidator = New RegularExpressionValidator
                NumericExpressionValidator.ControlToValidate = colname
                NumericExpressionValidator.ErrorMessage = "*"
                NumericExpressionValidator.ValidationExpression = "^[1-9]\d*(\.\d+)?$"
                NumericExpressionValidator.ValidationGroup = "NumBox"
                If container.FindControl(colname) Is Nothing Then
                    container.Controls.Add(textBox)
                    container.Controls.Add(NumericExpressionValidator)
                End If
            ElseIf colDataType.ToLower = "date" Then
                DatePicker = New RadDatePicker
                DatePicker.ID = colname
                DatePicker.Width = "100"
                DatePicker.DateInput.DateFormat = "dd/MMM/yyyy"
                DatePicker.DatePopupButton.ImageUrl = ""
                DatePicker.DatePopupButton.HoverImageUrl = ""
                DatePicker.Calendar.ID = "Calendar3"
                DatePicker.Calendar.UseRowHeadersAsSelectors = False
                DatePicker.Calendar.UseColumnHeadersAsSelectors = False
                DatePicker.Calendar.ViewSelectorText = "x"
                If container.FindControl(colname) Is Nothing Then
                    container.Controls.Add(DatePicker)
                End If
            End If
            hdn_colDataType = New HiddenField
            hdn_colDataType.ID = "hdn_colDataType_" & colname
            hdn_colDataType.Value = colDataType.ToLower
            container.Controls.Add(hdn_colDataType)
        End Sub
    End Class
    Private Class MyLabelTemplate
        Implements ITemplate
        Protected label As Label
        Private colname As String
        Public Sub New(ByVal cName As String)
            colname = cName
        End Sub
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            label = New Label()
            label.ID = colname
            If container.FindControl(colname) Is Nothing Then
                container.Controls.Add(label)
            End If
        End Sub
    End Class

    Protected Sub RadGridReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGridReport.ItemDataBound
        If TypeOf e.Item Is GridDataItem Then
            Dim h_Grd_Row_ID As HiddenField
            h_Grd_Row_ID = TryCast(e.Item.FindControl("h_Grd_Row_ID"), HiddenField)
            If h_Grd_Row_ID IsNot Nothing Then
                Dim ID As Int16
                ID = h_Grd_Row_ID.Value
                Dim myColumnName As String
                If myBindDataTable.Select("ID=" & ID.ToString).Length > 0 Then
                    Dim myRow As DataRow
                    myRow = myBindDataTable.Select("ID=" & ID.ToString)(0)
                    Dim i As Int16
                    For i = 0 To myBindDataTable.Columns.Count - 1
                        myColumnName = myBindDataTable.Columns(i).ColumnName.Replace(" ", "___").Replace(" ", "___").Replace("[", "").Replace("]", "")
                        If Not e.Item.FindControl(myColumnName) Is Nothing Then
                            If Not CType(e.Item.FindControl("hdn_colDataType_" & myColumnName), HiddenField) Is Nothing Then
                                If CType(e.Item.FindControl("hdn_colDataType_" & myColumnName), HiddenField).Value = "date" Then
                                    If IsDate(myRow(myBindDataTable.Columns(i)).ToString) Then
                                        CType(e.Item.FindControl(myColumnName), RadDatePicker).SelectedDate = CDate(myRow(myBindDataTable.Columns(i)).ToString).ToString("dd/MMM/yyyy")
                                    Else
                                        ' CType(e.Item.FindControl(myColumnName), RadDatePicker). = ""
                                    End If

                                ElseIf CType(e.Item.FindControl("hdn_colDataType_" & myColumnName), HiddenField).Value = "numeric" Then
                                    If myRow(myBindDataTable.Columns(i)).ToString <> "" Then
                                        CObj(e.Item.FindControl(myColumnName)).Text = myRow(myBindDataTable.Columns(i)).ToString
                                    Else
                                        CObj(e.Item.FindControl(myColumnName)).Text = "0"
                                    End If
                                Else
                                    CObj(e.Item.FindControl(myColumnName)).Text = myRow(myBindDataTable.Columns(i)).ToString
                                End If

                            End If

                        End If
                    Next
                End If
            End If
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try


            If txtTitle.Text.ToString.Trim = "" Then
                lblError.Text = "Please enter a title for the data request."
                Exit Sub
            End If


            If GetSelectedNode(RadBSUTreeView, "|").ToString.Replace("|", "").Trim = "" Then
                lblError.Text = "Please select atleast one Business unit."
                Exit Sub
            End If

            Dim Trandate As String = txtLastDate.SelectedDate.ToString
            If Not IsDate(Trandate) Then
                lblError.Text = "Invalid Transaction Date !!!!"
                Exit Sub
            End If

            If h_Designations.Value.ToString.Replace("|", "").Trim = "" Then
                lblError.Text = "Please select atleast one designation."
                Exit Sub
            End If

            If ViewState("datamode") = "add" Then
                hdn_DRI_ID.Value = 0
            End If

            Dim RowKeys, ColKeys As String, ColDataTypes As String
            RowKeys = ""
            ColKeys = ""
            ColDataTypes = ""
            Dim lstItem As ListItem

            For Each lstItem In lstRowKeyVal.Items
                RowKeys = RowKeys & lstItem.Value & "|"
            Next
            Dim lstColKeys() As String
            For Each lstItem In lstColumnKeyVal.Items
                ColKeys = ColKeys & lstItem.Value.Trim & "|"
                lstColKeys = lstItem.Text.Split("|")
                If lstColKeys.Length >= 2 Then
                    ColDataTypes = ColDataTypes & lstColKeys(1).Trim & "|"
                Else
                    ColDataTypes = ColDataTypes & "Text" & "|"
                End If
            Next

            If RowKeys.ToString.Replace("|", "").Trim = "" Then
                lblError.Text = "Please enter atleast one Row Keys."
                Exit Sub
            End If

            If ColKeys.ToString.Replace("|", "").Trim = "" Then
                lblError.Text = "Please enter atleast one Column Keys."
                Exit Sub
            End If


            Dim sqlParam(13) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@DRI_ID", hdn_DRI_ID.Value, SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@DRI_TITLE", txtTitle.Text, SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@DRI_DESCRIPTION", txtDescription.Text, SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@DRI_REQ_LASTDT", CDate(txtLastDate.SelectedDate).ToString("dd/MMM/yyyy"), SqlDbType.DateTime)
            sqlParam(4) = Mainclass.CreateSqlParameter("@DRI_bActive", chkActive.Checked, SqlDbType.Bit)
            sqlParam(5) = Mainclass.CreateSqlParameter("@DRI_bForward", chkForward.Checked, SqlDbType.Bit)
            sqlParam(6) = Mainclass.CreateSqlParameter("@DRI_DES_IDS", h_Designations.Value, SqlDbType.VarChar)
            sqlParam(7) = Mainclass.CreateSqlParameter("@DRI_BSU_IDS", GetSelectedNode(RadBSUTreeView, "|"), SqlDbType.VarChar)
            sqlParam(8) = Mainclass.CreateSqlParameter("@DRI_ROW_KEYS", RowKeys, SqlDbType.VarChar)
            sqlParam(9) = Mainclass.CreateSqlParameter("@DRI_COL_KEYS", ColKeys, SqlDbType.VarChar)
            sqlParam(10) = Mainclass.CreateSqlParameter("@DRI_COL_DATA_TYPE", ColDataTypes, SqlDbType.VarChar)
            sqlParam(11) = Mainclass.CreateSqlParameter("@DRI_INIT_EMP_ID", Session("EmployeeID"), SqlDbType.Int)
            sqlParam(12) = Mainclass.CreateSqlParameter("@DRI_INIT_BSU_ID", Session("sBsuId"), SqlDbType.VarChar)
            sqlParam(13) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = 0
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_SAVE_USER_DATA_REQUEST_INIT", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(13).Value = 0 Then
                hdn_DRI_ID.Value = sqlParam(0).Value
                stTrans.Commit()
                FillDetails(hdn_DRI_ID.Value)
                SetDataMode("view")
                lblError.Text = "Data Saved Successfully !!!"
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, hdn_DRI_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
            Else
                stTrans.Rollback()
                lblError.Text = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
            lblEntryForwarded.Text = ""
            chkForward.Visible = True
            chkForward.Checked = False
            chkActive.Checked = True
            btnAlertPendingBSU.Visible = False
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"
            PreviewTable()
        End If
        Dim Forwarded As Boolean
        Forwarded = chkForward.Checked
        btnSave.Visible = Not mDisable
        btnEdit.Visible = mDisable
        btnAdd.Visible = mDisable
        btnDelete.Visible = mDisable
        RadGridDesignation.Enabled = Not mDisable And Not Forwarded
        RadBSUTreeView.Enabled = Not mDisable And Not Forwarded
        txtLastDate.Enabled = Not mDisable
        txtColumnKey.Enabled = Not mDisable And Not Forwarded
        txtRowKey.Enabled = Not mDisable And Not Forwarded
        txtTitle.Enabled = Not mDisable And Not Forwarded
        txtDescription.Enabled = Not mDisable And Not Forwarded
        imgDesigID.Enabled = Not mDisable And Not Forwarded
        btnLstAddRow.Enabled = Not mDisable And Not Forwarded
        btnLstRemoveRow.Enabled = Not mDisable And Not Forwarded
        btnLstAddCol.Enabled = Not mDisable And Not Forwarded
        btnLstRemoveCol.Enabled = Not mDisable And Not Forwarded
        chkActive.Enabled = Not mDisable
        chkForward.Enabled = Not mDisable And Not Forwarded
        '  btnAlertPendingBSU.Enabled = Not mDisable
    End Sub

    Sub clear_All()
        h_Designations.Value = ""
        hdn_DRI_ID.Value = ""
        FillSelectedBusinessUnits("")
        FillDesignations(h_Designations.Value)
        txtLastDate.SelectedDate = Nothing
        txtColumnKey.Text = ""
        txtRowKey.Text = ""
        txtTitle.Text = ""
        txtDescription.Text = ""
        lblSelectedBSU.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            FillDetails(hdn_DRI_ID.Value)
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        SetDataMode("add")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Try
        '    Dim retval As String
        '    If (retval = "0" Or retval = "") Then
        '        UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
        '        lblError.Text = "Data Deleted Successfully !!!!"
        '        Response.Redirect(ViewState("ReferrerUrl"))
        '    Else
        '        lblError.Text = retval
        '    End If
        'Catch ex As Exception
        '    Errorlog(ex.Message)
        '    lblError.Text = ex.Message
        'End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click

    End Sub

    Protected Sub btnAlertPendingBSU_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlertPendingBSU.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@DRI_ID", hdn_DRI_ID.Value, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_ALERT_PENDING_DATA_REQUEST_BSU", sqlParam)
            If sqlParam(1).Value = 0 Then
                stTrans.Commit()
                lblAlertMessage.Text = "Email alert sent successfully. "
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, hdn_DRI_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
            Else
                stTrans.Rollback()
                lblError.Text = IIf(sqlParam(1).Value = "", "Unexpected Error !!!", sqlParam(1).Value)
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
End Class
