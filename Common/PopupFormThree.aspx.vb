Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PopupFormThree
    Inherits BasePage
    Dim multiSel As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        multiSel = IIf(Request.QueryString("multiSelect") = "true", True, False)
        Dim strMultiSel As String = Request.QueryString("multiSelect")
        If strMultiSel <> String.Empty And strMultiSel <> "" Then
            If String.Compare("False", strMultiSel, True) = 0 Then
                gvGroup.Columns(0).Visible = False
                'gvGroup.Columns(2).Visible = False
                gvGroup.Columns(3).Visible = True
                'DropDownList1.Visible = False
                btnFinish.Visible = False
                chkSelAll.Visible = False
            Else
                gvGroup.Columns(0).Visible = True
                gvGroup.Columns(2).Visible = True
                gvGroup.Columns(3).Visible = False
                btnFinish.Visible = True
            End If
        Else
            multiSel = True
            gvGroup.Columns(0).Visible = True
            gvGroup.Columns(2).Visible = True
            gvGroup.Columns(3).Visible = False
            btnFinish.Visible = True
        End If
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            Session("liUserList") = New List(Of String)
            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub

    Sub BindDocNo()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            Dim str_act, str_doctype, str_date, str_filter, str_txtCode, str_txtDescr, str_txtDescr1, str_txtDescr2 As String
            str_filter = ""
            str_txtCode = ""
            str_txtDescr = ""
            str_txtDescr1 = ""
            str_txtDescr2 = ""
            str_act = Request.QueryString("act")
            str_doctype = Request.QueryString("type")
            str_date = Request.QueryString("dt")
            Dim str_docNo As String = Request.QueryString("docno")

            If Not IsDate(str_date) Then
                str_date = Now.Date
            End If
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter = set_search_filter("ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
                str_txtDescr = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR", str_Sid_search(0), str_txtDescr)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

            End If
            Dim str_Sql As String

            Dim str_query_header As String = String.Empty
            str_query_header = "SELECT JNL_DOCNO as ID,  REPLACE(CONVERT(VARCHAR(11), " _
               & " JNL_DOCDT, 113), ' ', '/') AS DESCR, SUM(JNL_DEBIT) as DESCR1, " _
               & " JNL_NARRATION as DESCR2 FROM vw_OSA_JournalData " _
               & " WHERE JNL_BSU_ID='" & Session("sBSUID") & "' AND JNL_FYEAR='" & Session("F_YEAR") & "' " _
               & " AND JNL_DEBIT>0 AND JNL_ACT_ID='" & str_act & "' and JNL_DOCTYPE='" & str_doctype & "'" _
               & " AND JNL_DOCNO  NOT IN ( SELECT ISNULL( PRP_REFDOCNO ,'') FROM PREPAYMENTS_H " _
               & " WHERE PRP_BSU_ID='" & Session("sBSUID") & "' AND " _
               & " PRP_FYEAR='" & Session("F_YEAR") & "' AND PRP_REFTYPE='" & str_doctype & "' ) " & str_filter _
               & " GROUP BY JNL_DOCNO ,REPLACE(CONVERT(VARCHAR(11),JNL_DOCDT, 113),' ', '/'), JNL_NARRATION" _
               & "|" & "Docno|Date|Amount|Narration"
            Select Case str_doctype
                Case "BP", "CP", "DN", "CN"
                    str_query_header = "SELECT * FROM (" _
                      & " SELECT  VHH.VHH_DOCNO AS ID, REPLACE(CONVERT(VARCHAR(11), VHH.VHH_DOCDT, 113), ' ', '/') AS DESCR, SUM(VHD.VHD_AMOUNT)  AS DESCR1, " _
                      & " VHH.VHH_NARRATION AS DESCR2,0 AS REFID FROM  VOUCHER_H AS VHH INNER JOIN VOUCHER_D AS VHD " _
                      & " ON VHH.VHH_BSU_ID = VHD.VHD_BSU_ID AND VHH.VHH_FYEAR = VHD.VHD_FYEAR " _
                      & " AND VHH.VHH_DOCTYPE = VHD.VHD_DOCTYPE AND VHH.VHH_DOCNO = VHD.VHD_DOCNO " _
                      & " WHERE (ISNULL(VHH.VHH_bPOSTED, 0) = 1) AND (VHD.VHD_ACT_ID = '" & str_act & "') " _
                      & " AND (VHH.VHH_SUB_ID = '" & Session("Sub_ID") & "') AND (VHH.VHH_BSU_ID = '" & Session("sBSUID") & "') " _
                      & " AND (VHH.VHH_FYEAR = '" & Session("F_YEAR") & "') AND (VHH.VHH_DOCTYPE = '" & str_doctype & "')" _
                      & " AND VHH.VHH_DOCDT<='" & str_date & "' AND VHH_DOCNO  NOT IN ( SELECT ISNULL( PRP_REFDOCNO ,'') " _
                      & " FROM PREPAYMENTS_H WHERE PRP_BSU_ID='" & Session("sBSUID") & "' AND PRP_FYEAR='" _
                      & Session("F_YEAR") & "' AND PRP_REFTYPE='" & str_doctype & "' AND PRP_ID <> '" & str_docNo & "')" _
                      & " GROUP BY VHH.VHH_SUB_ID, VHH.VHH_BSU_ID, VHH.VHH_FYEAR, " _
                      & " VHH.VHH_DOCTYPE, VHH.VHH_DOCNO,REPLACE(CONVERT(VARCHAR(11), VHH.VHH_DOCDT, 113), ' ', '/'),  VHH.VHH_NARRATION" _
                      & " ) AS DB WHERE 1=1" & str_filter _
                      & "|" & "Docno|Date|Amount|Narration"
                Case Else
                    str_query_header = "SELECT * FROM (" _
                     & " SELECT JNL_DOCNO as ID, REPLACE(CONVERT(VARCHAR(11), JNL_DOCDT, 113), ' ', '/') AS DESCR," _
                     & "  JNL_DEBIT  as DESCR1 , JNL_NARRATION as DESCR2,JNL_SLNO as REFID FROM vw_OSA_JournalData JNL " _
                     & " WHERE JNL_BSU_ID='" & Session("sBSUID") & "' AND JNL_FYEAR='" & Session("F_YEAR") & "' " _
                     & " AND JNL_DEBIT>0 AND JNL_ACT_ID='" & str_act & "' " _
                     & " AND JNL_DOCTYPE='" & str_doctype & "' AND JNL_DOCNO  NOT IN ( SELECT ISNULL( PRP_REFDOCNO ,'') " _
                     & " FROM PREPAYMENTS_H WHERE PRP_BSU_ID='" & Session("sBSUID") & "' AND PRP_FYEAR='" _
                     & Session("F_YEAR") & "' AND ISNULL(PRP_REFTYPE,'')='" & str_doctype & "' AND ISNULL(PRP_REFDOCID,0)=JNL.JNL_SLNO) " _
                     & " AND JNL_DOCDT<= '" & str_date & "'" _
                     & " ) AS DB WHERE 1=1" & str_filter _
                     & "|" & "Docno|Date|Amount|Narration"
            End Select
            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
            txtSearch.Text = str_txtDescr
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2

            'lblHCode lblHDescr lblHDescr1  lblHDescr2 
            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHCode")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(4)
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub BindRecNo()
        'Try
    'Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
    'Dim str_BSU, str_doctype, str_date, str_filter, str_txtCode, str_txtDescr, str_txtDescr1, str_txtDescr2 As String
    '        str_filter = ""
    '        str_txtCode = ""
    '        str_txtDescr = ""
    '        str_txtDescr1 = ""
    '        str_txtDescr2 = ""
    '        str_BSU = Request.QueryString("BSU")
    '        str_doctype = Request.QueryString("TYPE")
    '        str_date = Request.QueryString("dt")
    '        If Not IsDate(str_date) Then
    '            str_date = Now.Date
    '        End If
    'Dim txtSearch As New TextBox
    '        If gvGroup.Rows.Count > 0 Then
    ' ''code
    'Dim str_Sid_search() As String
    '            str_Sid_search = h_Selected_menu_1.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
    '            str_txtCode = txtSearch.Text.Trim
    '            str_filter = set_search_filter("ID", str_Sid_search(0), str_txtCode)

    ' ''name
    '            str_Sid_search = h_selected_menu_2.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
    '            str_txtDescr = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR", str_Sid_search(0), str_txtDescr)

    '            str_Sid_search = h_Selected_menu_3.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
    '            str_txtDescr1 = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

    '            str_Sid_search = h_Selected_menu_4.Value.Split("__")
    '            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
    '            str_txtDescr2 = txtSearch.Text.Trim
    '            str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

    '        End If
    'Dim str_Sql As String


    'Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT"
    '        If str_doctype = "ENQ" Then
    '            STR_RECEIPT = "FEES.VW_OSO_FEES_RECEIPT_ENQUIRY"
    '        End If

    'Dim str_query_header As String = String.Empty

    '        str_query_header = "SELECT * FROM (" _
    '        & " SELECT  FCL_RECNO AS ID , STU_NO AS DESCR , STU_NAME AS DESCR1, FCL_AMOUNT AS DESCR2, 0 AS REFID  FROM " & STR_RECEIPT & " where  FCL_BSU_ID='" & str_BSU & "'" _
    '        & " AND isnull(FCL_bDELETED,0)=0  " _
    '        & " ) AS DB WHERE 1=1" & str_filter _
    '          & "|" & "Docno|Stu #|Name|Amount"


    '        str_Sql = str_query_header.Split("||")(0)
    'Dim str_headers As String()
    '        str_headers = str_query_header.Split("|")
    'Dim ds As New DataSet
    '        If str_Sql <> "" Then
    '            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        End If
    '        gvGroup.DataSource = ds
    '        If ds.Tables(0).Rows.Count = 0 Then
    '            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
    '            gvGroup.DataBind()
    'Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
    ''Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
    '            gvGroup.Rows(0).Cells.Clear()
    '            gvGroup.Rows(0).Cells.Add(New TableCell)
    '            gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
    '            gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '            gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
    '        Else
    '            gvGroup.DataBind()
    '        End If
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
    '        txtSearch.Text = str_txtCode
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
    '        txtSearch.Text = str_txtDescr
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
    '        txtSearch.Text = str_txtDescr1
    '        txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
    '        txtSearch.Text = str_txtDescr2

    ''lblHCode lblHDescr lblHDescr1  lblHDescr2 
    'Dim lblheader As New Label
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHCode")
    '        lblheader.Text = str_headers(1)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr")
    '        lblheader.Text = str_headers(2)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
    '        lblheader.Text = str_headers(3)
    '        lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
    '        lblheader.Text = str_headers(4)
    '        set_Menu_Img()
    '        SetChk(Me.Page)
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '    End Try

Try
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim str_BSU, str_doctype, str_date, str_filter, str_txtCode, str_txtDescr, str_txtDescr1, str_txtDescr2 As String
            str_filter = ""
            str_txtCode = ""
            str_txtDescr = ""
            str_txtDescr1 = ""
            str_txtDescr2 = ""
            str_BSU = Request.QueryString("BSU")
            str_doctype = Request.QueryString("TYPE")
            str_date = Request.QueryString("dt")
            If Not IsDate(str_date) Then
                str_date = Now.Date
            End If
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter = set_search_filter("ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
                str_txtDescr = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR", str_Sid_search(0), str_txtDescr)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

            End If
            Dim str_Sql As String


            Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT"
            If str_doctype = "ENQ" Then
                STR_RECEIPT = "FEES.VW_OSO_FEES_RECEIPT_ENQUIRY"
            End If

            Dim str_query_header As String = String.Empty

            'str_query_header = "SELECT * FROM (" _
            '& " SELECT  FCL_RECNO AS ID , STU_NO AS DESCR , STU_NAME AS DESCR1, FCL_AMOUNT AS DESCR2, 0 AS REFID  FROM " & STR_RECEIPT & " where  FCL_BSU_ID='" & str_BSU & "'" _
            '& " AND isnull(FCL_bDELETED,0)=0  " _
            '& " ) AS DB WHERE 1=1" & str_filter _
            '  & "|" & "Docno|Stu #|Name|Amount"


            Dim pParms(3) As SqlParameter

            pParms(0) = Mainclass.CreateSqlParameter("@BSUID", str_BSU, SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@TYPE", str_doctype, SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@WHERE", str_filter, SqlDbType.VarChar)


            'str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_headers = str_query_header.Split("|")

            str_headers = "|Docno|Stu #|Name|Amount".Split("|")
            Dim ds As New DataSet

            'If str_Sql <> "" Then
            'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            'End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FEES.GET_FEES_RECEIPT_LIST", pParms)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
            txtSearch.Text = str_txtDescr
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2

            'lblHCode lblHDescr lblHDescr1  lblHDescr2 
            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHCode")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(4)
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub BindRecNo_Oth()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim str_BSU, str_filter, str_txtCode, str_txtDescr, str_txtDescr1, str_txtDescr2 As String
            str_filter = ""
            str_txtCode = ""
            str_txtDescr = ""
            str_txtDescr1 = ""
            str_txtDescr2 = ""
            str_BSU = Request.QueryString("BSU")

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter = set_search_filter("ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
                str_txtDescr = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR", str_Sid_search(0), str_txtDescr)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

            End If
            Dim str_Sql As String
            Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT_OTHER"

            Dim str_query_header As String = String.Empty

            str_query_header = "SELECT * FROM (" _
            & " SELECT  FOC_RECNO AS ID , COL_DESCR AS DESCR , ACT_NAME AS DESCR1, FOC_AMOUNT AS DESCR2, 0 AS REFID  FROM " & STR_RECEIPT & " where  FOC_BSU_ID='" & str_BSU & "'" _
            & " AND isnull(FOC_bDELETED,0)=0  " _
            & " ) AS DB WHERE 1=1" & str_filter _
            & "|" & "Docno|Stu #|Name|Amount"

            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
            txtSearch.Text = str_txtDescr
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2

            'lblHCode lblHDescr lblHDescr1  lblHDescr2 
            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHCode")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(4)
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID").ToString
            Case "DOCNO"
                BindDocNo()
            Case "RECNO"
                BindRecNo()
            Case "RECNO_OTH"
                BindRecNo_Oth()
            Case "SERVICE"
                BindServiceRecNo()
        End Select
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        Response.Write("window.close();")
        Response.Write("} </script>")
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        Dim lblREFID As New Label
        lblcode = sender.Parent.FindControl("lblDESCR1")

        lblREFID = sender.Parent.FindControl("lblREFID")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text.Replace("'", "\'") & "___" & lbClose.Text.Replace("'", "\'") & "___" & lblREFID.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")


            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblcode.Text.Replace("'", "\'") & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblREFID.Text.Replace("'", "\'") & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text.Replace("'", "\'") & "||" & lbClose.Text.Replace("'", "\'") & "||" & lblREFID.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        If chkSelAll.Checked Then
            Select Case ViewState("ID").ToString
                Case "BSU"

                    Dim tblbUSuper As Boolean = Session("sBusper")
                    Dim BUnitreaderSuper As SqlDataReader
                    Dim tblbUsr_id As String = Session("sUsr_id")

                    If tblbUSuper = True Then
                        BUnitreaderSuper = AccessRoleUser.GetBusinessUnits()
                    Else
                        BUnitreaderSuper = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                    End If

                    Dim strAccessibleBSUIDs As String = String.Empty

                    If BUnitreaderSuper.HasRows = True Then
                        While (BUnitreaderSuper.Read())
                            Session("liUserList").Remove(BUnitreaderSuper(0))
                            Session("liUserList").Add(BUnitreaderSuper(0))
                        End While
                    End If
                Case "DEPT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DPT_ID as ID FROM DEPARTMENT_M where 1=1"
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DOCTYPE"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ESD_ID FROM EMPSTATDOCUMENTS_M WHERE ESD_bCHKEXPIRY =1 "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "CAT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ECT_ID as ID FROM EMPCATEGORY_M "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DESG"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DES_ID as ID FROM EMPDESIGNATION_M "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "EMP"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "select EMP_ID as ID from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND")
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
            End Select

        Else
            Session("liUserList").Clear()
        End If
        GridBind()
    End Sub
    Sub BindServiceRecNo()
        Try
            Dim str_conn As String

            str_conn = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim str_BSU, str_date, str_filter, str_txtCode, str_txtDescr, str_txtDescr1, str_txtDescr2 As String
            str_filter = ""
            str_txtCode = ""
            str_txtDescr = ""
            str_txtDescr1 = ""
            str_txtDescr2 = ""
            str_BSU = Request.QueryString("BSU")

            str_date = Request.QueryString("dt")
            If Not IsDate(str_date) Then
                str_date = Now.Date
            End If
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter = set_search_filter("ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
                str_txtDescr = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR", str_Sid_search(0), str_txtDescr)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

            End If
            Dim str_Sql As String


            Dim STR_RECEIPT As String = "FEES.VW_OSO_FEES_RECEIPT"

            Dim str_query_header As String = String.Empty

            str_query_header = "SELECT * FROM (" _
            & " SELECT  FCL_RECNO AS ID , STU_NO AS DESCR , STU_NAME AS DESCR1, FCL_AMOUNT AS DESCR2, 0 AS REFID  FROM " & STR_RECEIPT & " where  FCL_BSU_ID='" & Session("sBsuid") & "'" _
            & " AND isnull(FCL_bDELETED,0)=0 AND FCL_STU_BSU_ID='" & str_BSU & "' AND FCL_DATE='" & str_date & "'" _
            & " ) AS DB WHERE 1=1" & str_filter _
              & "|" & "Docno|Stu #|Name|Amount"


            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
            txtSearch.Text = str_txtDescr
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2

            'lblHCode lblHDescr lblHDescr1  lblHDescr2 
            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHCode")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(4)
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

End Class

