﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FeedBack.aspx.vb" Inherits="Accounts_FeedBack" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Phoenix User Survey - We would love to hear from you ! </title>
    <meta http-equiv="X-UA-Compatible" content="IE=11" />

    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <script src="/PHOENIXBETA/js/feedback.js" type="text/javascript"></script>
    <script src="/PHOENIXBETA/js/feedback-SVGPlugin.js" type="text/javascript"></script>
    <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">
        .popupmsg {
            position: fixed;
            /* left: 20%; */
            top: 20%;
            text-align: center;
            width: 100%;
            /*background-color:rgba(0,0,0,0.3);
            display:block;*/
        }

        .smily-icon {
            width: 50px;
            padding: 2px 4px;
            opacity: 1;
        }

            .smily-icon:hover {
                width: 50px;
                opacity: 0.6;
            }

        .blur {
            opacity: 0.3;
            width: 50px;
            padding: 2px 4px;
        }

        .q-line {
            border-bottom: 1px solid rgba(0,0,0,0.04);
        }

        .text-big {
            font-size: 18px;
            font-weight: bold;
        }

        .top-bg {
            background-color: rgba(0,0,0,0.09);
        }

        ul {
            padding-left: 16px;
            list-style: circle;
        }


        /*new css for the animation smily*/

        .top-buffer {
            margin-top: 40px;
        }

        .font-consistent {
            color: #404040;
            font-family: inherit;
        }

        *, *:before, *:after {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            /*font-family: Helvetica, Arial, sans-serif;*/
        }

        @media screen and (max-width: 700px) {
            h1 {
                font-size: 30px;
            }
        }

        .fb-cont {
            overflow: hidden;
            position: relative;
            margin: auto;
            width: 330px;
            padding: /*22px 22px 50px*/ 14px 22px 40px;
            /*padding-bottom: 140px;*/
            background: #fff;
            float:left !important;
        }

        .fb-cont__inner {
            position: relative;
            display: flex;
            justify-content: space-between;
        }

            .fb-cont__inner:before {
                content: "";
                position: absolute;
                left: 50%;
                top: 50%;
                width: 90%;
                height: 4px;
                margin-top: -2px;
                background: rgba(200, 206, 211, 0.5);
                border-radius: 2px;
                -webkit-transform: translateX(-50%);
                transform: translateX(-50%);
            }

        .fb-cont__drag-cont {
            z-index: 2;
            position: absolute;
            left: 0%;
            top: 15%;
            width: 330px;
            height: 100%;
            margin-left: -40px;
            pointer-events: none;
            -webkit-transform: translate3d(50%, 0, 0);
            transform: translate3d(50%, 0, 0);
        }

        .fb-emote {
            z-index: 1;
            position: relative;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            width: 36px;
            height: 36px;
            cursor: pointer;
        }

            .fb-emote svg {
                display: block;
                margin-bottom: 6px;
                width: 100%;
                height: 100%;
                background: #c8ced3;
                border-radius: 50%;
                transition: -webkit-transform 0.5s;
                transition: transform 0.5s;
                transition: transform 0.5s, -webkit-transform 0.5s;
            }

            .fb-emote.s--active svg {
                -webkit-transform: scale(0.7);
                transform: scale(0.7);
            }

        .fb-emote__caption {
            text-align: center;
            font-size: 16px;
            font-weight: 500;
            color: #c8ced3;
            transition: all 0.5s;
        }

        .fb-emote.s--active .fb-emote__caption {
            color: #655e53;
            -webkit-transform: translateY(15px);
            transform: translateY(15px);
        }

        .fb-emote__eye {
            stroke: none;
            fill: #fff;
        }

        .fb-emote__smile {
            stroke: #fff;
            stroke-width: 10;
            stroke-linecap: round;
            fill: none;
        }

        .fb-active-emote {
            position: absolute;
            left: 0;
            top: 50%;
            width: 50px;
            height: 50px;
            margin-left: -50px;
            margin-top: -35px;
            /*background: #ffd68c;*/
            border-radius: 50%;
        }

            .fb-active-emote svg {
                width: 100%;
                height: 100%;
            }

        .fb-active-emote__eye {
            stroke: none;
            fill: #655e53;
        }

        .fb-active-emote__smile {
            stroke: #655e53;
            stroke-width: 10;
            stroke-linecap: round;
            fill: none;
        }

        .icon-link {
            position: absolute;
            left: 5px;
            bottom: 5px;
            width: 32px;
        }

            .icon-link img {
                width: 100%;
                vertical-align: top;
            }

           
    </style>
</head>
<body>

 
    <form id="form1" runat="server">
       
        <div id="divpopup" class="popupmsg" runat="server" visible="false">
            <asp:Label ID="lblError" runat="server" Text="" CssClass=""></asp:Label>
        </div>
        <div id="tblReview" class="p-3 m-0" runat="server">
            <div class="mb-2 text-block text-left">Please take a moment to indicate your satisfaction levels with Phoenix</div>

            <table width="100%">

                <asp:Repeater ID="rptrQuestionair" runat="server" OnItemDataBound="rptrQuestionair_ItemDataBound" OnItemCommand="rptrQuestionair_ItemCommand">
                    <ItemTemplate>


                        <div class="container-fluid">
                            <div class="row row-centered">
                                <div class="col-xs-6 col-md-6 m-auto">
			                    <h2 class="text-left font-consistent">
                                        <asp:Label ID="lblReview_Question" runat="server" Text='<%# Eval("FQS_QUESTION")%>' CssClass="text-big"></asp:Label>
                                        <asp:HiddenField ID="hidQuestionId" runat="server" Value='<%# Eval("FQS_ID")%>' />
                                    </h2>
		</div>
                                <div class="col-xs-6 col-md-6 col-lg-6 col-centered mb-2 text-center">
                                    
                                    <!-- <div class="top-buffer"></div>-->
                                    <svg class="fb-emotes-svg" style="display: none;">
				<symbol id="terrible" runat="server"  data-emote="terrible" viewBox="0 0 100 100">
					<path class="fb-emote__eye fb-emote__eye--left" d="M32,25 l10,10 a10,10 0 0,1 -20,0 a10,10 0 0,1 10,-10"></path>
					<path class="fb-emote__eye fb-emote__eye--right" d="M58,35 l10,-10 a10,10 0 0,1 0,20 a10,10 0 0,1 -10,-10"></path>
					<path class="fb-emote__smile" d="M30,68 q20,-13 40,0 M30,68 q20,-13 40,0"></path>
				</symbol>
				<symbol id="bad" runat="server"  data-emote="bad" viewBox="0 0 100 100">
					<path class="fb-emote__eye fb-emote__eye--left" d="M22,35 l10,-10 a10,10 0 0,1 0,20 a10,10 0 0,1 -10,-10"></path>
					<path class="fb-emote__eye fb-emote__eye--right" d="M68,25 l10,10 a10,10 0 0,1 -20,0 a10,10 0 0,1 10,-10"></path>
					<path class="fb-emote__smile" d="M30,68 q20,-10 40,0 M30,68 q20,-10 40,0"></path>
				</symbol>
				<symbol id="okay" runat="server" viewBox="0 0 100 100">
					<path class="fb-emote__eye fb-emote__eye--left" d="M32,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
					<path class="fb-emote__eye fb-emote__eye--right" d="M68,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
					<path class="fb-emote__smile" d="M35,73 q20,0 35,0 M35,73 q20,0 35,0"></path>
				</symbol>
				<symbol id="good" runat="server"  viewBox="0 0 100 100">
					<path class="fb-emote__eye fb-emote__eye--left" d="M32,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
					<path class="fb-emote__eye fb-emote__eye--right" d="M68,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
					<path class="fb-emote__smile" d="M30,68 q20,10 40,0 M30,68 q20,10 40,0"></path>
				</symbol>
				<symbol id="great" runat="server"  viewBox="0 0 100 100">
					<path class="fb-emote__eye fb-emote__eye--left" d="M32,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
					<path class="fb-emote__eye fb-emote__eye--right" d="M68,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
					<path class="fb-emote__smile" d="M30,68 q20,15 40,0 M30,68 q20,0 40,0"></path>
				</symbol>
			</svg>


                                    <div class="fb-cont">

                                        <div class="fb-cont__inner" id="smilySet" runat="server">
                                            <div class="fb-emote" id="smilyTerrible" runat="server" data-emote="terrible" data-progress="0.25" >
                                                <svg><use   id="terribleUse" runat="server" test="terrible" class="svgUse" ></use>
                                                </svg>
                                                <p class="fb-emote__caption">Terrible</p>
                                            </div>
                                            <div class="fb-emote" id="smilybad" runat="server" data-emote="bad" data-progress="0.44">
                                                <svg><use id="badUse" runat="server" test="bad"  class="svgUse"></use></svg>
                                                <p class="fb-emote__caption">Bad</p>
                                            </div>
                                            <div class="fb-emote" id="smilyokay" runat="server" data-emote="okay" data-progress="0.63">
                                                <svg><use  id="okayUse" runat="server" test="okay" class="svgUse"></use></svg>
                                                <p class="fb-emote__caption">Okay</p>
                                            </div>
                                            <div class="fb-emote" id="smilygood" runat="server" data-emote="good" data-progress="0.82">
                                                <svg><use  id="goodUse" runat="server" test="good" class="svgUse"></use></svg>
                                                <p class="fb-emote__caption">Good</p>
                                            </div>
                                            <div class="fb-emote s--active" id="smilygreat" runat="server" data-emote="great" data-progress="1">
                                                <svg><use  id="greatUse" runat="server" test="great" class="svgUse"></use></svg>
                                                <p class="fb-emote__caption">Great</p>
                                            </div>
                                            <div class="fb-cont__drag-cont" id="dragcont" runat="server" style="transform: translate(100%, 0%) matrix(1, 0, 0, 1, 0, 0);">
                                                <div class="fb-active-emote" id="dragcontActive" runat="server" style="background-color: #8dc24c;">
                                                    <svg viewbox="0 0 100 100">
								<path class="fb-active-emote__eye fb-active-emote__eye--left" id="dragcontEyeLeft" runat="server" d="M32 25 C37.52 25 42 29.47 42 35 42 40.52 37.52 45 32 45 26.47 45 22 40.52 22 35 22 29.47 26.47 25 32 25 " data-original="M32,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
								<path class="fb-active-emote__eye fb-active-emote__eye--right" id="dragcontEyeRight" runat="server" d="M68 25 C73.52 25 78 29.47 78 35 78 40.52 73.52 45 68 45 62.47 45 58 40.52 58 35 58 29.47 62.47 25 68 25 " data-original="M68,25 a10,10 0 0,1 0,20 a10,10 0 0,1 0,-20"></path>
								<path class="fb-active-emote__smile" id="dragcontSmile" runat="server" d="M30,68 q20,15 40,0 M30,68 q20,0 40,0"></path>
							</svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    

                                </div>
                            </div>
                        </div>


                    </ItemTemplate>
                </asp:Repeater>
            </table>
            
            <table width="100%">
                <tr>

                    <td align="left" colspan="3">
                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" placeholder="General Comments / Feedback on Phoenix" Width="100%"></asp:TextBox>
                        <%--<p>
                                    <span>Characters used - </span>
                                    <asp:Label ID="txttaken" runat="server" ReadOnly="True" placeholder="0" Text="0"></asp:Label>
                                    out of 500.
                                </p>--%>
                    </td>
                   
                    <td align="center">
                         <asp:Button ID="btnSave" runat="server" Text="Submit" OnClick="btnSave_Click" CssClass="button"  />
                        <%--<asp:Button ID="btnClose" runat="server" Text="Go To Homepage" OnClick="btnClose_Click" CssClass="button" Visible="false" />--%>
                    </td>
                    
                </tr>
            </table>

            <br />

        </div>
        
    </form>




    <script type="text/javascript">

        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }


        var a = "0";
        function $$(selector, context) {
            var context = context || document;
            var elements = context.querySelectorAll(selector);
            var nodesArr = [].slice.call(elements);
            return nodesArr.length === 1 ? nodesArr[0] : nodesArr;
        };

        var $emotesArr = $$('.fb-emote');
        var numOfEmotes = $emotesArr.length;

        //var $clsterrible = $$('.cls-terrible');
        //$clsterrible.forEach(function ($terrible, i) {
        //   // $terrible.getAttribute('id')
        //    alert($terrible.getAttribute('id'));  //terribleUse
        //});

        //var position = this.getAttribute('id').indexOf("_", this.getAttribute('id').indexOf("_") + 1);
        //var sub_id = this.getAttribute('id').substring(0, position + 1);
        //var comp_id = sub_id + type;

               

        var emoteColors = {
            terrible: '#e86363',
            bad: '#ec912f', //  #f9c686
            okay: '#f9c686',
            good: '#d4f575',
            great: '#8dc24c',  
            default: '#ffd68c'
        };


        var animTime = 0.5;

        $emotesArr.forEach(function ($emote, i) {
            var mode = $emote.querySelector('.svgUse').getAttribute('test');
            var svg_use_position = $emote.querySelector('.svgUse').getAttribute('id').indexOf("_", $emote.querySelector('.svgUse').getAttribute('id').indexOf("_") + 1);
            var svg_sub_id = $emote.querySelector('.svgUse').getAttribute('id').substring(0, svg_use_position + 1);
            $emote.querySelector('.svgUse').setAttribute('href', '#' + svg_sub_id + mode);
            // alert(svg_use);

            if (isIE()) {
            document.querySelector('#' + svg_sub_id + 'smilyTerrible').setAttribute('data-progress', '0.3');
            document.querySelector('#' + svg_sub_id + 'smilybad').setAttribute('data-progress', '0.47');
            document.querySelector('#' + svg_sub_id + 'smilyokay').setAttribute('data-progress', '0.64');
            document.querySelector('#' + svg_sub_id + 'smilygood').setAttribute('data-progress', '0.79');
            document.querySelector('#' + svg_sub_id + 'smilygreat').setAttribute('data-progress', '0.96');              
            }


            var progressStep = $emote.getAttribute('data-progress');
            $emote.dataset.progress = progressStep;


            $emote.addEventListener('mousemove', function () {
                
                    var progressTo = +this.dataset.progress;
                    var type = this.dataset.emote;
                    var position = this.getAttribute('id').indexOf("_", this.getAttribute('id').indexOf("_") + 1);
                    var sub_id = this.getAttribute('id').substring(0, position + 1);
                    var comp_id = sub_id + type;
                    //alert(comp_id);
                    var $target = document.querySelector('#' + comp_id);
                    
                    // alert($target);
                    var $lEye = $target.querySelector('.fb-emote__eye--left');
                    var $rEye = $target.querySelector('.fb-emote__eye--right');
                    var leftEyeTargetD = $lEye.getAttribute('d');
                    var rightEyeTargetD = $rEye.getAttribute('d');
                    var smileTargetD = $target.querySelector('.fb-emote__smile').getAttribute('d');
                    var bgColor = emoteColors[type];
                    if (!bgColor) bgColor = emoteColors.default;



                    //alert($$('.fb-emote.s--active'))
                    //alert($$('.fb-emote.s--active').classList)
                    //$$('.fb-emote.s--active').classList.remove('s--active');
                    // this.classList.add('s--active');
                    if (document.querySelector('#' + sub_id + 'smilySet').getAttribute('block') != 'true') {
                    TweenMax.to(document.querySelector('#' + sub_id + 'dragcontActive'), animTime, { backgroundColor: bgColor });
                    TweenMax.to(document.querySelector('#' + sub_id + 'dragcont'), animTime, { x: progressTo * 100 + '%' });
                    TweenMax.to(document.querySelector('#' + sub_id + 'dragcontEyeLeft'), animTime, { morphSVG: $lEye });
                    TweenMax.to(document.querySelector('#' + sub_id + 'dragcontEyeRight'), animTime, { morphSVG: $rEye });
                    TweenMax.to(document.querySelector('#' + sub_id + 'dragcontSmile'), animTime, { attr: { d: smileTargetD } });

                    
                    }
                    MyConfirmMethod(document.querySelector('#' + sub_id + 'hidQuestionId').getAttribute('value'), type);
            });

            $emote.addEventListener('click', function () {
               
                var progressTo = +this.dataset.progress;
                var type = this.dataset.emote;
                var position = this.getAttribute('id').indexOf("_", this.getAttribute('id').indexOf("_") + 1);
                var sub_id = this.getAttribute('id').substring(0, position + 1);
                var comp_id = sub_id + type;
                var $target = document.querySelector('#' + comp_id);
                var blockdiv = document.querySelector('#' + sub_id + 'smilySet');
                blockdiv.setAttribute('block', 'true');
                var $lEye = $target.querySelector('.fb-emote__eye--left');
                var $rEye = $target.querySelector('.fb-emote__eye--right');
                var leftEyeTargetD = $lEye.getAttribute('d');
                var rightEyeTargetD = $rEye.getAttribute('d');
                var smileTargetD = $target.querySelector('.fb-emote__smile').getAttribute('d');
                var bgColor = emoteColors[type];
                if (!bgColor) bgColor = emoteColors.default;
                //alert($$('.fb-emote.s--active'));
                //$$('.fb-emote.s--active').classList.remove('s--active');
                document.querySelector('#' + sub_id + 'smilyTerrible').classList.remove('s--active');
                document.querySelector('#' + sub_id + 'smilybad').classList.remove('s--active');
                document.querySelector('#' + sub_id + 'smilyokay').classList.remove('s--active');
                document.querySelector('#' + sub_id + 'smilygood').classList.remove('s--active');
                document.querySelector('#' + sub_id + 'smilygreat').classList.remove('s--active');
                this.classList.add('s--active');

                TweenMax.to(document.querySelector('#' + sub_id + 'dragcontActive'), animTime, { backgroundColor: bgColor });
                TweenMax.to(document.querySelector('#' + sub_id + 'dragcont'), animTime, { x: progressTo * 100 + '%' });
                TweenMax.to(document.querySelector('#' + sub_id + 'dragcontEyeLeft'), animTime, { morphSVG: $lEye });
                TweenMax.to(document.querySelector('#' + sub_id + 'dragcontEyeRight'), animTime, { morphSVG: $rEye });
                TweenMax.to(document.querySelector('#' + sub_id + 'dragcontSmile'), animTime, { attr: { d: smileTargetD } });
                a = 1;
              
                MyConfirmMethod(document.querySelector('#' + sub_id + 'hidQuestionId').getAttribute('value'), type);
            });
        });


        


        function MyConfirmMethod(hv, Review) {
            //if (confirm('Are your sure?')) {
                $.ajax({
                    type: "POST",
                    url: "FeedBack.aspx/a",
                    data: '{ Id: "' + hv + '", Review: "'+ Review +'" }',
                    contentType: "application/json; charset=utf-8"
                });
                return true;
            //}
            //else {
            //    return false;
            //}
        }
        function Validate() {
            var $blocksArr = $$('.fb-cont__inner');
            var numOfblocks = $blocksArr.length;
            var j = 0;
            $blocksArr.forEach(function ($block, i) {
                var index = $block.querySelector('.svgUse').getAttribute('id').indexOf("_", $block.querySelector('.svgUse').getAttribute('id').indexOf("_") + 1);
                var part_id = $block.querySelector('.svgUse').getAttribute('id').substring(0, index + 1);
                if (document.querySelector('#' + part_id + 'smilySet').hasAttribute('block')) {
                    j = j + 1;
                }
            });
            if (numOfblocks == j) {
                return true;
            }
            else {
                alert("Please fill all the fields!!!")
                event.preventDefault();

            }
        }


    </script>
</body>
</html>



