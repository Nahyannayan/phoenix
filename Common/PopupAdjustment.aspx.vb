Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PopupAdjustment
    Inherits BasePage
    Dim multiSel As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        multiSel = IIf(Request.QueryString("multiSelect") = "true", True, False)
        Dim strMultiSel As String = Request.QueryString("multiSelect")
        If strMultiSel <> String.Empty And strMultiSel <> "" Then
            If String.Compare("False", strMultiSel, True) = 0 Then
                gvGroup.Columns(0).Visible = False
                'gvGroup.Columns(2).Visible = False
                gvGroup.Columns(3).Visible = True
                'DropDownList1.Visible = False
                btnFinish.Visible = False
                chkSelAll.Visible = False
            Else
                gvGroup.Columns(0).Visible = True
                gvGroup.Columns(2).Visible = True
                gvGroup.Columns(3).Visible = False
                'DropDownList1.Visible = True
                btnFinish.Visible = True
            End If
        Else
            multiSel = True
            gvGroup.Columns(0).Visible = True
            gvGroup.Columns(2).Visible = True
            gvGroup.Columns(3).Visible = False
            'DropDownList1.Visible = True
            btnFinish.Visible = True
        End If
        If Page.IsPostBack = False Then

            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub


    Sub gridbindBSUnit()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_filter As String
            Dim str_txtCode, str_txtDescr, str_txtDescr1, str_txtDescr2 As String
            str_filter = ""
            str_txtCode = ""
            str_txtDescr = ""
            str_txtDescr1 = ""
            str_txtDescr2 = ""
            Dim str_act, str_doctype, str_date As String
            str_act = Request.QueryString("act")
            str_doctype = Request.QueryString("type")
            str_date = Request.QueryString("dt")
            If Not IsDate(str_date) Then
                str_date = Now.Date
            End If
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter = set_search_filter("ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
                str_txtDescr = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR", str_Sid_search(0), str_txtDescr)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

            End If
            Dim str_Sql As String

            Dim str_query_header As String = String.Empty
            str_query_header = "SELECT FAR.FAR_ID AS ID, REPLACE(CONVERT(VARCHAR(11), FAR.FAR_ApprovalDt, 113), ' ', '/') AS DESCR, " _
          & " FAR.FAR_ApprovalRemarks AS DESCR1, TCM.TCM_bRegAppr, " _
          & " TCM.TCM_bLabAppr, TCM.TCM_bFeeAppr, TCM.TCM_bLibAppr, " _
          & " CASE FAR.FAR_EVENT WHEN 'W' THEN 'With Drawal' " _
          & " WHEN 'R' THEN 'Refund' END AS DESCR3 ,TCM.TCM_TCSO AS DESCR2" _
          & " FROM  FEES.FEEADJREQUEST_H AS FAR LEFT OUTER JOIN TCM_M AS TCM " _
          & " ON FAR.FAR_EVENT_REF_ID = TCM.TCM_ID WHERE (FAR.FAR_BSU_ID = '" & Session("sBSUID") & "') " _
          & " AND (FAR.FAR_STU_TYP = '" & Request.QueryString("stype") & "') AND (FAR.FAR_ApprStatus = 'A') " _
          & " AND (FAR.FAR_EVENT IN ('R', 'W')) AND (ISNULL(TCM.TCM_bCANCELLED, 0) = 0) " _
          & " AND FAR_ID NOT IN (SELECT FRH_FAR_ID FROM FEES.FEE_REFUND_H)" _
          & " AND (FAR.FAR_STU_ID='" & Request.QueryString("stu") & "')" _
          & "|" & "Date|Desc|Type|TC/SO"
            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")

            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR")
            txtSearch.Text = str_txtDescr
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2

            'lblHCode lblHDescr lblHDescr1  lblHDescr2 
            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHCode")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(4)
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub


    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID").ToString
            Case "ADJUSTMENT"
                gridbindBSUnit()
        End Select
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode = '" & h_SelectedId.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub



    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("lblDESCR1")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim lblid As New Label

        lblid = sender.Parent.FindControl("lblID")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblid.Text & "||" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblid.Text & "||" & lbClose.Text.Replace("'", "\'") & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblid.Text & "||" & lbClose.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged

        If chkSelAll.Checked Then
            Select Case ViewState("ID").ToString
                Case "BSU"

                    Dim tblbUSuper As Boolean = Session("sBusper")
                    Dim BUnitreaderSuper As SqlDataReader
                    Dim tblbUsr_id As String = Session("sUsr_id")

                    If tblbUSuper = True Then
                        BUnitreaderSuper = AccessRoleUser.GetBusinessUnits()
                    Else
                        BUnitreaderSuper = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                    End If

                    Dim strAccessibleBSUIDs As String = String.Empty

                    If BUnitreaderSuper.HasRows = True Then
                        While (BUnitreaderSuper.Read())
                            Session("liUserList").Remove(BUnitreaderSuper(0))
                            Session("liUserList").Add(BUnitreaderSuper(0))
                        End While
                    End If
                Case "DEPT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DPT_ID as ID FROM DEPARTMENT_M where 1=1"
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DOCTYPE"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ESD_ID FROM EMPSTATDOCUMENTS_M WHERE ESD_bCHKEXPIRY =1 "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "CAT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ECT_ID as ID FROM EMPCATEGORY_M "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DESG"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DES_ID as ID FROM EMPDESIGNATION_M "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "EMP"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "select EMP_ID as ID from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND")
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
            End Select

        Else
            Session("liUserList").Clear()
        End If
        GridBind()
    End Sub


    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

End Class

