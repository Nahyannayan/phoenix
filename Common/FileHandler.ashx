﻿<%@ WebHandler Language="VB" Class="Handler" %>

Imports System
Imports System.Web
Imports System.IO
Imports UtilityObj
Imports System.Web.Configuration
Imports System.Web.HttpContext
Imports System.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Public Class Handler : Implements IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState
    Dim Encr_decrData As New Encryption64
    Dim PathOfFile As String
    Dim ReferrerUrl As String
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            Dim TYPE As String = Encr_decrData.Decrypt(context.Request.QueryString("TYPE").Replace(" ", "+"))
            If TYPE Is Nothing Then Exit Sub
            If Not context.Request.UrlReferrer Is Nothing Then
                ReferrerUrl = context.Request.UrlReferrer.ToString()
            End If
            If TYPE = "XLSDATA" Then
                Dim Title As String
                Title =Encr_decrData.Decrypt(context.Request.QueryString("Title").Replace(" ", "+"))  
                If Title Is Nothing Or Title = "" Then
                    Title = "MyExportData"
                ElseIf Title.Length > 30 Then
                    Title = Title.Substring(0, 30).ToString
                End If
                Dim myDT As New DataTable
                myDT = context.Session("myData")
                Dim PathOfFile As String
                Dim NameOfFile As String = Title & ".xls"
                PathOfFile = WebConfigurationManager.AppSettings.Item("TempFileFolder") & NameOfFile
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                ef.Worksheets.Add(Title)
                ''commented and added by nahyan on 18apr2016 for new gembox
                'ef.Worksheets(0).InsertDataTable(myDT, 0, 0, True)
                'ef.SaveXls(PathOfFile)
                
                ef.Worksheets(0).InsertDataTable(myDT, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                ''  ef.Worksheets(0).HeadersFooters.AlignWithMargins = True
                ef.Save(PathOfFile)
                
                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(PathOfFile)
                If IO.File.Exists(PathOfFile) Then
                    IO.File.Delete(PathOfFile)
                End If
                Dim ContentType, Extension As String
                ContentType = "application/vnd.ms-excel"
                Extension = GetFileExtension(ContentType)
                DownloadFile(context, BytesData, ContentType, Extension, Title)
            ElseIf TYPE = "XLSFILE" Then
                Dim PathOfFile, Title As String
                PathOfFile = Encr_decrData.Decrypt(context.Request.QueryString("Path").Replace(" ", "+"))
                Title = Encr_decrData.Decrypt(context.Request.QueryString("Title").Replace(" ", "+"))
                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(PathOfFile)
                If IO.File.Exists(PathOfFile) Then
                    IO.File.Delete(PathOfFile)
                End If
                Dim ContentType, Extension As String
                ContentType = "application/vnd.ms-excel"
                Extension = GetFileExtension(ContentType)
                DownloadFile(context, BytesData, ContentType, Extension, Title)
            ElseIf TYPE = "PCDOCDOWNLOAD" Then
                Dim DocID As String = ""
                DocID = Encr_decrData.Decrypt(context.Request.QueryString("DocID").Replace(" ", "+"))
                If DocID <> "" Then
                    OpenFileFromDB(context, DocID)
                    context.Response.Redirect(ReferrerUrl)
                Else
                    context.Response.Redirect(ReferrerUrl)
                End If
             ElseIf TYPE = "XLSDOWNLOAD" Then
                Dim PathOfFile, Title As String
                PathOfFile = Encr_decrData.Decrypt(context.Request.QueryString("Path").Replace(" ", "+"))
                Title = Encr_decrData.Decrypt(context.Request.QueryString("Title").Replace(" ", "+"))
                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(PathOfFile)
                             Dim ContentType, Extension As String
                ContentType = "application/vnd.ms-excel"
                Extension = GetFileExtension(ContentType)
                DownloadFile(context, BytesData, ContentType, Extension, Title)  
            End If
        Catch ex As Exception
            ' Response.Redirect(ViewState("ReferrerUrl"))
        Finally

        End Try
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Private Sub OpenFileFromDB(ByVal context As HttpContext, ByVal DocId As String)
        Try
            Dim conn As String = ""
            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim param(0) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@DOC_ID", DocId, SqlDbType.VarChar)
            Dim mTable As New DataTable
            mTable = Mainclass.getDataTable("OpenDocumentFromDB", param, conn)
            If mTable.Rows.Count > 0 Then
                Dim contentType As String
                contentType = mTable.Rows(0).Item("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(mTable.Rows(0).Item("DOC_DOCUMENT"), Byte())
                context.Response.Clear()
                context.Response.AddHeader("Content-Length", bytes.Length.ToString())
                context.Response.ContentType = contentType
                context.Response.AddHeader("Expires", "0")
                context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                context.Response.AddHeader("Pragma", "public")
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" & mTable.Rows(0).Item("DOC_DOCNO").ToString())
                context.Response.BinaryWrite(bytes)
                context.Response.Flush()
                context.Response.[End]()
            End If
        Catch ex As Exception
 
        End Try
    End Sub
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & Title & Extension)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.[End]()
    End Sub
    Private Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Public Function ConvertBytesToFile(ByVal BytesData As Byte(), ByVal FilePath As String) As Boolean
        If IsNothing(BytesData) = True Then
            Return False
            'Throw New ArgumentNullException("Image Binary Data Cannot be Null or Empty", "ImageData")
        End If
        Try
            If IO.File.Exists(FilePath) Then IO.File.Delete(FilePath)
            Dim fs As IO.FileStream = New IO.FileStream(FilePath, IO.FileMode.Create, IO.FileAccess.Write)
            Dim bw As IO.BinaryWriter = New IO.BinaryWriter(fs)
            bw.Write(BytesData)
            bw.Flush()
            bw.Close()
            fs.Close()
            bw = Nothing
            fs.Dispose()
            Return True
        Catch ex As Exception
            Return True
        End Try
    End Function
End Class