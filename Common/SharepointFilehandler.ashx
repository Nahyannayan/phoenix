﻿<%@ WebHandler Language="VB" Class="SharepointFilehandler" %>

Imports System
Imports System.Web
Imports System.IO
Imports UtilityObj
Imports System.Web.Configuration
Imports System.Web.HttpContext
Imports System.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports Microsoft.SharePoint.Client

Public Class SharepointFilehandler : Implements IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState
    Dim Encr_decrData As New SHA256EncrDecr
    Dim ReferrerUrl As String

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If Not context.Request.UrlReferrer Is Nothing Then
            ReferrerUrl = context.Request.UrlReferrer.ToString()
        End If
        DownloadFilesFromSharePoint(context)
    End Sub

    Private Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Title As String)
        UtilityObj.Errorlog("in DownloadFile()", "OASIS_SHAREPOINT")
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "inline; filename=" & Title)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.[End]()
    End Sub
    Private Function GetFileExtension(ByVal ContentType As String) As String
        GetFileExtension = ""
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Function GetContentType(ByVal Extension As String) As String
        GetContentType = ""
        Select Case Extension
            Case "doc"
                GetContentType = "application/vnd.ms-word"
                Exit Select
            Case "docx"
                GetContentType = "application/vnd.ms-word"
                Exit Select
            Case "xls"
                GetContentType = "application/vnd.ms-excel"
                Exit Select
            Case "jpg"
                GetContentType = "image/jpg"
                Exit Select
            Case "png"
                GetContentType = "image/png"
                Exit Select
            Case "gif"
                GetContentType = "image/gif"
                Exit Select
            Case "pdf"
                GetContentType = "application/pdf"
                Exit Select
        End Select
    End Function

    Public Sub DownloadFilesFromSharePoint(ByRef cntxt As HttpContext)
        Dim encr As New SHA256EncrDecr
        Dim ContentType As String = "", Extension As String = ""

        Dim objSPC As New SharepointClass
        Try
            'Dim ctx = objSPC.LoginToSharepoint(1)
            'commented on 29-Aug-2020
            Dim DptID As Integer = 0
            'DptID = Mainclass.getDataValue("select DOC_DPT_ID from DocImage with(nolock) inner join DocUpload with(nolock) on doc_id=Doi_Doc_id where doi_id=" & cntxt.Request.QueryString("DOIID"), "OASIS_CLMConnectionString")
            DptID = Mainclass.getDataValue("select dot_dep_id from DocImage with(nolock) inner join DocUpload with(nolock) on doc_id=Doi_Doc_id inner join DocType on dot_id=Doc_Dot_id where doi_id=" & cntxt.Request.QueryString("DOIID"), "OASIS_CLMConnectionString")
            Dim ctx = objSPC.LoginToSharepoint(DptID)

            Dim PathOfFile = encr.Decrypt_SHA256(cntxt.Request.QueryString("Path").Replace(" ", "+"))

            UtilityObj.Errorlog(PathOfFile, "OASIS_SHAREPOINT")
            Dim web As Web = ctx.Web
            ctx.Load(web)
            Dim url As String = New Uri(PathOfFile).AbsolutePath
            Dim f As Microsoft.SharePoint.Client.File = web.GetFileByServerRelativeUrl(url)
            ctx.Load(f)
            ctx.ExecuteQuery()
            If f.Exists Then
                UtilityObj.Errorlog(f.Name, "OASIS_SHAREPOINT")
                Dim strarray As String() = f.Name.Split(".")
                If strarray.Length > 1 Then
                    Extension = strarray(1)
                    ContentType = GetContentType(Extension)
                End If
            End If
            Dim fileInfo As FileInformation = Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, f.ServerRelativeUrl)
            Dim ByteArray() As Byte = Nothing
            Using mStream As New System.IO.MemoryStream()
                If fileInfo.Stream IsNot Nothing Then
                    fileInfo.Stream.CopyTo(mStream)
                    ByteArray = mStream.ToArray()
                End If
            End Using
            DownloadFile(cntxt, ByteArray, ContentType, f.Name)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "OASIS_SHAREPOINT")
        End Try
        
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class