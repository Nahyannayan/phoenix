<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupBanks.aspx.vb" Inherits="Common_PopupBanks" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
         </script>
</head>
<body  onload="listen_window();"  >

    <form id="form1" runat="server">
           <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
    <table width="100%" align="center">
                    <tr valign="top">
                        <td >
                        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True" PageSize="16"  CssClass="table table-row table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Descr1">
                                        <EditItemTemplate> 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Descr1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                                                    <asp:TextBox ID="txtCode" runat="server" Width="72px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descr1">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                                                    <asp:TextBox ID="txtBSUName" runat="server" Width="72px"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"
                                                                         />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:LinkButton ID="lnkDESCR" runat="server" Text='<%# Bind("Descr2") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TYPE" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbnktype" runat="server" Text='<%# Bind("BNK_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td> 
                    </tr> 
                </table>
    </form>
                      
</body>
</html>