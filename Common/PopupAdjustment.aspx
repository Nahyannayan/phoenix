<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupAdjustment.aspx.vb" Inherits="Common_PopupAdjustment" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" />

    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }
        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;
            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }

    </script>
</head>
<body onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">


    <form id="form1" runat="server">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                        EmptyDataText="No Data" Width="100%" AllowPaging="True" PageSize="18" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                <ItemTemplate>
                                    <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>' Visible="False"></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHCode" runat="server" Text="Date"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtDESCR" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCR1" runat="server" Text='<%# Bind("DESCR1") %>'></asp:Label>&nbsp;<br />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDescr" Text="Amount" runat="server"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtDESCR1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnNameSearch4" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCR2" runat="server" Text='<%# Bind("DESCR2") %>'></asp:Label>&nbsp;<br />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDescr1" runat="server" Text="Narration"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtDESCR2" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnNameSearch5" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDescr2" Text="Docno" runat="server"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnBSUNameSearch2" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR3") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CheckBoxField DataField="TCM_bRegAppr" HeaderText="Reg.Appr" />
                            <asp:CheckBoxField DataField="TCM_bLabAppr" HeaderText="Lab Appr" />
                            <asp:CheckBoxField DataField="TCM_bFeeAppr" HeaderText="Fee Appr" />
                            <asp:CheckBoxField DataField="TCM_bLibAppr" HeaderText="Lib. Appr" />
                        </Columns>
                    </asp:GridView>
                    <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                         AutoPostBack="True" Visible="False" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
            </tr>
        </table>

    </form>
</body>
</html>
