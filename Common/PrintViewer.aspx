<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintViewer.aspx.vb" Inherits="Common_PrintViewer" MasterPageFile="~/mainMasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        window.onload = CheckForPrint
        function CheckForPrint() {

        }
        function CloseWindow() {
            if (confirm('Close the window?'))
                window.close();
        }
    </script>

   <table width="100%">
        <tr>
            <td align="left" valign="top">
                <div>
                    <CR:CrystalReportViewer ID="MyReportViewer" runat="server" AutoDataBind="true"
                        HasCrystalLogo="False" Height="50px" PrintMode="ActiveX" Width="350px">
                    </CR:CrystalReportViewer>
                 </div>
            </td>
        </tr>
    </table>
</asp:Content>
