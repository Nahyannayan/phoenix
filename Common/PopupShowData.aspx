<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupShowData.aspx.vb" Inherits="Common_PopupShowData" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css" />
    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">

        function CallPrint() {

            var prtContent = document.getElementById('divGrid');
            var strOldOne = prtContent.innerHTML;
            var WinPrint = window.open('', '', 'left=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            prtContent.innerHTML = strOldOne;


        }

        function AddTHEAD() {
            var table = document.getElementById('GridViewShowDetails');
            if (table != null) {
                var head = document.createElement("THEAD");
                head.style.display = "table-header-group";
                head.style.border.color = "Blue";
                head.appendChild(table.rows[0]);
                table.insertBefore(head, table.childNodes[0]);
            }
        }




    </script>
    <style type="text/css">
        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 39%;
        }

        @media print {
            th {
                color: black;
                background-color: white;
            }

            tHead {
                display: table-header-group;
                border-top-style: inherit;
                border-color: Blue;
            }
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" onload="javascript: AddTHEAD('GridViewShowDetails');window.focus();">


    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <br />
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr id="trdays" runat="server" visible="false">
                <td align="left" width="20%"><span class="field-label">Starting Date</span> </td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    <asp:ImageButton ID="imgFrom" runat="server" CssClass="MyCalendar" ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                </td>

                <td align="left" width="20%"><span class="field-label">No. of Days planned</span>
                </td>
                <td align="left" width="30%">
                    <asp:TextBox ID="txtDays"
                        runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="Button1" runat="server" Width="50%" CssClass="button" Text="Refresh" />

                    <asp:Label ID="lblRejoinDate" runat="server" Visible="false"></asp:Label></td>
            </tr>
            <tr valign="top">
                <td align="center" colspan="4">
                    <div id="divGrid">

                        <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Data Found" PageSize="15" AllowPaging="True">
                            <PagerStyle BorderStyle="Solid" />
                            <HeaderStyle />
                        </asp:GridView>

                    </div>
                </td>
            </tr>

        </table>
        <br />
        <table width="100%" align="center">
            <tr>
                <td align="center">
                    <asp:Button ID="btnPrint" runat="Server" CssClass="button" Visible="False" Text="Print" OnClientClick="javascript:CallPrint()" />
                    &nbsp;<asp:Button ID="btnCLOSE" OnClientClick="window.close()" runat="server" Text="Close" CssClass="button" Style="visibility: hidden;" /></td>
            </tr>
        </table>
        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
            PopupButtonID="imgFrom" TargetControlID="txtFrom">
        </ajaxToolkit:CalendarExtender>
    </form>

</body>

</html>
