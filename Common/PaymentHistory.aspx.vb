Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PaymentHistory
    Inherits BasePage
    Dim FeeTotAmnt As Double
    Dim PayTotAmnt As Double
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Session("sUsr_name") Is Nothing OrElse Session("sUsr_name").ToString = "" Then
                Response.Redirect("~\noAccess.aspx")
            End If
            Page.Title = "::::GEMS PHOENIX:::: Payment History"
            GridViewShowDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvDetails.Attributes.Add("bordercolor", "#1b80b6")
            gvHead.Attributes.Add("bordercolor", "#1b80b6")
            gvPayment.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
    End Sub
    Sub gridbind()
        gridbindReceiptsHistory("")
    End Sub
    Sub gridbindReceiptsHistory(Optional ByVal Jquery As String = "")
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As New StringBuilder
            str_Sql.Append("SELECT PAY.FEE_DESCR AS 'Fee', PAY.FCL_RECNO as 'ReceiptNo', ")
            str_Sql.Append("REPLACE(CONVERT(VARCHAR(11),FCL.FCL_DATE, 113), ' ', '/') as Date,PAY.FCS_AMOUNT as 'Amount',")
            str_Sql.Append("FCL.FCL_EMP_ID AS 'Employee', CASE WHEN ISNULL(FCL.FCL_bDELETED,0) = 0 THEN 'No' ELSE 'Yes' END AS 'Status' ")
            str_Sql.Append("FROM FEES.VW_OSO_FEES_FEECOLLECTED AS PAY INNER JOIN ")
            str_Sql.Append("FEES.FEECOLLECTION_H AS FCL ON PAY.FCL_ID = FCL.FCL_ID ")
            str_Sql.Append("WHERE PAY.FCL_STU_ID = @STU_ID " & Jquery)
            str_Sql.Append("ORDER BY FCL_DATE DESC")
            Dim ds As New DataSet
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = Mainclass.cleanString(Request.QueryString("stuid"))
            If Jquery <> "" Then
                ReDim Preserve pParms(3)
                pParms(1) = New SqlClient.SqlParameter("@FROM_DT", SqlDbType.VarChar, 15)
                pParms(1).Value = Mainclass.cleanString(txtFromDate.Text)
                pParms(2) = New SqlClient.SqlParameter("@TO_DT", SqlDbType.VarChar, 15)
                pParms(2).Value = Mainclass.cleanString(txtTodate.Text)
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql.ToString, pParms)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Dim Jquery As String = " AND FCL.FCL_DATE BETWEEN @FROM_DT AND @TO_DT "
        gridbindReceiptsHistory(Jquery)
        TrDetails.Visible = False
    End Sub
    Sub bindDetails(ByVal RecptNo As String, ByVal BsuId As String)
        Try
            TrDetails.Visible = True
            RecptNo = Mainclass.cleanString(RecptNo)
            BsuId = Mainclass.cleanString(BsuId)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As New StringBuilder
            str_Sql.Append("SELECT FCL_RECNO AS [Receipt No],STU_NO as [Student ID],STU_NAME AS [Student Name], ")
            str_Sql.Append("REPLACE(CONVERT(VARCHAR(11),FCL_DATE, 113), ' ', '/') AS [Date],GRD_DISPLAY AS [Grade],FCL_NARRATION as [Narration] ")
            str_Sql.Append("FROM FEES.VW_OSO_FEES_RECEIPT_DUPLICATE WHERE FCL_BSU_ID = @BSU_ID AND FCL_RECNO = @REC_NO  ")
            str_Sql.Append("GROUP BY FCL_RECNO,FCL_DATE,STU_NO,STU_NAME,GRD_DISPLAY,FCL_NARRATION ")

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = BsuId
            pParms(1) = New SqlClient.SqlParameter("@REC_NO", SqlDbType.VarChar, 20)
            pParms(1).Value = RecptNo

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql.ToString, pParms)
            str_Sql.Clear()
            gvHead.DataSource = getDataTable(ds.Tables(0))
            gvHead.DataBind()

            str_Sql.Append("SELECT FEE_DESCR AS [Fee Head],FCS_AMOUNT AS [Amount] FROM FEES.VW_OSO_FEES_FEECOLLECTED ")
            str_Sql.Append(" WHERE FCL_BSU_ID = @BSU_ID AND FCL_RECNO = @REC_NO")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql.ToString, pParms)
            str_Sql.Clear()
            gvDetails.DataSource = ds.Tables(0)
            gvDetails.DataBind()

            gvDetails.FooterRow.Cells(0).Text = "Total"
            gvDetails.FooterRow.Cells(1).Text = FeeTotAmnt.ToString("####0.00")
            gvDetails.FooterRow.Cells(1).HorizontalAlign = HorizontalAlign.Left
            str_Sql.Append("SELECT CLT_DESCR, CASE WHEN FCD_CLT_ID = 1 THEN NULL ELSE FCD_REFNO END AS FCD_REFNO, ")
            str_Sql.Append(" CASE WHEN FCD_CLT_ID = 2 THEN REPLACE(CONVERT(VARCHAR(11),FCD_DATE, 113), ' ', '/') ELSE null END AS FCD_DATE, ")
            str_Sql.Append(" CASE WHEN FCD_CLT_ID IN (2,3) THEN CRI_DESCR ELSE NULL END AS CRI_DESCR,FCD_AMOUNT ")
            str_Sql.Append(" FROM FEES.VW_OSO_FEES_PAYMENTS WHERE FCL_BSU_ID = @BSU_ID AND FCL_RECNO = @REC_NO")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql.ToString, pParms)
            str_Sql.Clear()
            gvPayment.DataSource = ds.Tables(0)
            gvPayment.DataBind()

            gvPayment.FooterRow.Cells(0).Text = "Total"
            gvPayment.FooterRow.Cells(4).Text = PayTotAmnt.ToString("####0.00")
            gvPayment.FooterRow.Cells(4).HorizontalAlign = HorizontalAlign.Left
            txtFocus.Focus()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub linkRecptNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim linkRecptNo As LinkButton = sender

        bindDetails(linkRecptNo.Text, Session("sBsuid"))
    End Sub
    Private Function getDataTable(ByVal ds As DataTable) As DataTable
        Dim dt As New DataTable
        dt = CreateDataTable()

        For x As Integer = 0 To ds.Columns.Count - 1
            Dim dr As DataRow
            dr = dt.NewRow
            dr("Name") = ds.Columns(x).ColumnName
            dr("NameValue") = ds.Rows(0)(x).ToString()
            dt.Rows.Add(dr)
        Next
        Return dt
    End Function
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Dim Name As New DataColumn("Name", System.Type.GetType("System.String"))
        Dim NameValue As New DataColumn("NameValue", System.Type.GetType("System.String"))
        dtDt.Columns.Add(Name)
        dtDt.Columns.Add(NameValue)
        Return dtDt
    End Function

    Protected Sub gvHead_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvHead.RowDataBound
        e.Row.Cells(0).Width = Unit.Percentage(50)
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        e.Row.Cells(0).Width = Unit.Percentage(50)
        If e.Row.RowType = DataControlRowType.DataRow Then
            FeeTotAmnt += Convert.ToDouble(e.Row.Cells(1).Text)
        End If
    End Sub

    Protected Sub gvPayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPayment.RowDataBound
        'e.Row.Cells(0).Width = Unit.Percentage(50)
        If e.Row.RowType = DataControlRowType.DataRow Then
            PayTotAmnt += Convert.ToDouble(e.Row.Cells(4).Text)
        End If
    End Sub

    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        gridbindReceiptsHistory()

    End Sub
End Class
