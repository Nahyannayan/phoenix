Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PopupFormIDhidden
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = 0
        Response.Cache.SetNoStore()
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            gridbind()
        End If
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Select Case Request.QueryString("ID")
            Case "LOCATION"
                GridBindLocation()
            Case "LOCATIONFEE"
                GridBindLocationFee()
            Case "CONCESSION"
                GridBindConcession()
            Case "BANK"
                GridBindBank()
            Case "EMIRATE"
                GridBindEmirate()
            Case "BUDGET"
                GridBindBudget()
            Case "REFEP_CONC"
                GridBindREFEP_CONC()
            Case "SUBACC"
                GridBindSUBACC()
        End Select
    End Sub
  
    Sub GridBindSUBACC()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ASM_ID", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ASM_NAME", str_Sid_search(0), str_txtName)
            End If
            str_filter_name = str_filter_name & " and ASL_ACT_ID ='" & Request.QueryString("ACTID") & "'"
            str_query_header = "SELECT ID, Descr1, Descr2, ASL_ACT_ID " _
            & " FROM (SELECT ASM_ID AS ID, ASM_ID AS Descr1, ASM_NAME AS Descr2, ASL_ACT_ID " _
            & "FROM ACCOUNTS_SUB_ACC_M_list) AS db where 1=1 " _
            & str_filter_code & str_filter_name & "|Code|Descr "
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Session("sBsuid"))

            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindREFEP_CONC()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("REFNO", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("REFNAME", str_Sid_search(0), str_txtName)
            End If
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim CONCTYP As String = Request.QueryString("CONCTYP")
            Dim CONC_FCM_ID As String = Request.QueryString("CONC_FCM_ID")
            str_query_header = UtilityObj.GetDataFromSQL("SELECT FCT_QUERY FROM FEES.FEE_CONCESSIONTYPE_M WHERE FCT_ID=" & CONCTYP, str_conn)

            Dim str_STAFF_BSU_ID As String = UtilityObj.GetDataFromSQL("SELECT FCM_BSU_ID FROM FEES.FEE_CONCESSION_M WHERE  FCM_ID='" & CONC_FCM_ID & "' AND FCM_FCT_ID='" & CONCTYP & "'", str_conn)

            str_query_header = "Select ID, REFNO as DESCR1,REFNAME as DESCR2  from(" _
            & str_query_header & " )a where a.id<>'' " & str_filter_code & str_filter_name & " |ID|Name "
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", str_STAFF_BSU_ID)

            Dim ds As New DataSet

            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindBudget()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("LOC_DESCRIPTION", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("SBL_DESCRIPTION", str_Sid_search(0), str_txtName)
            End If
            str_query_header = "SELECT BUH.BUH_ID AS ID,  BUH.BUH_REMARKS AS DESCR2, sum(BUD.BUD_TOTAL) AS DESCR1" _
                      & " FROM BUDGET_H AS BUH INNER JOIN " _
                      & " BUDGET_D AS BUD ON BUH.BUH_ID = BUD.BUD_BUH_ID WHERE BUH.BUH_BSU_ID='###'" _
                      & str_filter_code & str_filter_name _
                      & " group by  BUH.BUH_ID, BUH.BUH_BSU_ID, BUH.BUH_FYEAR,  BUH.BUH_DTFROM, " _
                      & " BUH.BUH_Months , BUH.BUH_REMARKS " & "|Budget|Amount "
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Session("sBsuid"))

            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindLocation()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("LOC_DESCRIPTION", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("SBL_DESCRIPTION", str_Sid_search(0), str_txtName)
            End If
            str_query_header = "SELECT DISTINCT SBL.SBL_DESCRIPTION AS DESCR2, RTM.RTm_BSU_ID, " _
                    & " RTM.RTm_AMOUNT, LOC.LOC_DESCRIPTION AS DESCR1, SBL.SBL_ID AS ID" _
                    & " FROM TRANSPORT.RATES_S AS RTS INNER JOIN" _
                    & " TRANSPORT.RATES_M AS RTM ON RTS.RTS_RTM_ID = RTM.RTm_ID INNER JOIN" _
                    & " TRANSPORT.SUBLOCATION_M AS SBL ON RTM.RTm_SBL_ID = SBL.SBL_ID INNER JOIN" _
                    & " TRANSPORT.LOCATION_M AS LOC ON SBL.SBL_LOC_ID = LOC.LOC_ID WHERE RTM.RTm_BSU_ID='###'" _
                    & str_filter_code & str_filter_name _
                    & " ORDER BY LOC.LOC_DESCRIPTION, SBL.SBL_DESCRIPTION|" & "Area|Location "
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Request.QueryString("bsu"))

            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindLocationFee()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("LOC_DESCRIPTION", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("SBL_DESCRIPTION", str_Sid_search(0), str_txtName)
            End If
            str_query_header = "SELECT SBL_ID AS ID, SBL_DESCRIPTION AS DESCR2, LOC_DESCRIPTION AS Descr1 FROM FEES.fn_GetSubLocationForBSU('###') WHERE 1=1" _
                    & str_filter_code & str_filter_name _
                    & " ORDER BY  LOC_DESCRIPTION,  SBL_DESCRIPTION|" & "Area|Location "
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Request.QueryString("bsu"))
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
                CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindConcession()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("FCT_DESCR", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("FCM_DESCR", str_Sid_search(0), str_txtName)
            End If

            str_query_header = "SELECT   LTRIM( RTRIM( FCM.FCM_FCT_ID))+'___'+   LTRIM( RTRIM(FCM.FCM_ID)) AS ID, " _
            & " FCT.FCT_DESCR AS DESCR1,FCM.FCM_DESCR AS DESCR2" _
            & " FROM FEES.FEE_CONCESSION_M AS FCM INNER JOIN " _
            & " FEES.FEE_CONCESSIONTYPE_M AS FCT ON FCM.FCM_FCT_ID = FCT.FCT_ID" _
            & " WHERE 1=1 " _
                    & str_filter_code & str_filter_name _
                    & "ORDER BY FCM.FCM_DESCR|" & "Concession|Descr"
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")

            Dim ds As New DataSet
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindBank()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BNK_SHORT", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BNK_DESCRIPTION", str_Sid_search(0), str_txtName)
            End If
            str_filter_code = str_filter_code & " and  (BANK_CTY_ID IN (SELECT BSU_COUNTRY_ID FROM BUSINESSUNIT_M WHERE (BSU_ID = '" & Session("sBsuid") & "')))"
            str_query_header = "SELECT BNK_ID AS ID, BNK_DESCRIPTION AS DESCR2, BNK_SHORT AS DESCR1 FROM BANK_M WHERE BNK_bENABLED=1 " _
                    & str_filter_code & str_filter_name _
                    & " ORDER BY BNK_DESCRIPTION|" & "Short Name|Bank"
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GridBindEmirate()
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("EMR_CODE", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("EMR_DESCR", str_Sid_search(0), str_txtName)
            End If 
            str_query_header = "SELECT EMR_CODE AS ID, EMR_DESCR AS DESCR2, EMR_CODE AS DESCR1 FROM EMIRATE_M WHERE 1=1 " _
                    & str_filter_code & str_filter_name _
                    & " ORDER BY EMR_DESCR|" & "Short Name|Emirate"
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblid As New Label
        Dim lnkDESCR As New LinkButton
        lnkDESCR = sender
        lblid = sender.Parent.FindControl("lblid")
        If (Not lblid Is Nothing) Then
            Dim BankType As String = ""
            If Request.QueryString("ID").ToString().Trim().ToUpper = "BANK" Then
                BankType = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BNK_TYPE, '') BNK_TYPE FROM dbo.BANK_M WITH(NOLOCK) WHERE ISNULL(BNK_bENABLED, 0) = 1 AND BNK_ID = '" & lblid.Text.Trim.Replace("___", "") & "'")
            End If
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblid.Text.Replace("'", "\'") & "___" & lnkDESCR.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            lblid.Text = lblid.Text.Replace("___", "||")
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameCode = '" & lblid.Text.Replace("'", "\'") & "||" & lnkDESCR.Text.Replace("'", "\'") & "||" & BankType & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblid.Text.Replace("'", "\'") & "||" & lnkDESCR.Text.Replace("'", "\'") & "||" & BankType & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")

            h_SelectedId.Value = "Close"
        End If
    End Sub
    Protected Sub gvGroup_RowDataBound(sender As Object, e As GridViewRowEventArgs) 'Handles gvGroup.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then

        '    Dim lblid As Label = DirectCast(e.Row.FindControl("lblid"), Label)
        '    Dim lnkDESCR As LinkButton = DirectCast(e.Row.FindControl("lnkDESCR"), LinkButton)

        '    If Not lnkDESCR Is Nothing AndAlso Not lblid Is Nothing Then
        '        'hf_bankid.Value =lblid.Text
        '        hf_bankid.Value = lblid.Text.Replace("'", "\'") & "___" & lnkDESCR.Text.Replace("'", "\'") & ""
        '        hf_ctrlid.Value = Request.QueryString("ctrl")
        '        If Request.QueryString("iD") = "BANK" Then
        '            lnkDESCR.Attributes.Add("onClick", "return SetValuetoParent('" & hf_bankid.Value & "','" & hf_ctrlid.Value & "');")
        '        ElseIf Request.QueryString("iD") = "CONCESSION" Then
        '            lnkDESCR.Attributes.Add("onClick", "return SetValuetoConcessionParent('" & hf_bankid.Value & "');")
        '        ElseIf Request.QueryString("iD") = "REFEP_CONC" Then
        '            lnkDESCR.Attributes.Add("onClick", "return SetValuetoReferenceParent('" & hf_bankid.Value & "','" & Request.QueryString("CONCTYP") & "');")
        '        End If

        '    End If
        'End If
    End Sub
End Class

