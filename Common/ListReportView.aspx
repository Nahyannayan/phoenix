<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="ListReportView.aspx.vb" Inherits="Common_ListReportView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <style>

        table td input[type=text]
        {
            min-width : 50% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
	     function CheckForPrint(printval, menucode) {
            //alert(printval);
            //alert(menucode);
            if (menucode == "PI01144") {
                if (isIE()) {
                    window.showModalDialog('../Inventory/BookSale/rptBS_Receipt.aspx?options=2&saleid=' + printval, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                } else {

                    return ShowWindowWithClose('../Inventory/BookSale/rptBS_Receipt.aspx?options=2&saleid=' + printval, 'search', '55%', '85%');
                    return false;
                }
            }
        }
        function isIE() {
            ua = navigator.userAgent;
            /* MSIE used to detect old browsers and Trident used to newer ones*/
            var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

            return is_ie;
        }


        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }

		
        function ajaxToolkit_CalendarExtenderClientShowing(e) {
            if (!e.get_selectedDate() || !e.get_element().value)
                e._selectedDate = (new Date()).getDateOnly();
        }

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
                    if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                        if (document.forms[0].elements[i].type == 'checkbox') {
                            document.forms[0].elements[i].checked = chk_state;
                        }
                    }
                }
            }
        }
        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
            }
            else if (pId == 4) {
                document.getElementById("<%=getid("mnu_4_img") %>").src = path;
            }
            else if (pId == 5) {
                document.getElementById("<%=getid("mnu_5_img") %>").src = path;
            }
            else if (pId == 6) {
                document.getElementById("<%=getid("mnu_6_img") %>").src = path;
            }
            else if (pId == 7) {
                document.getElementById("<%=getid("mnu_7_img") %>").src = path;
            }
            else if (pId == 8) {
                document.getElementById("<%=getid("mnu_8_img") %>").src = path;
            }
            else if (pId == 9) {
                document.getElementById("<%=getid("mnu_9_img") %>").src = path;
            }
            else if (pId == 10) {
                document.getElementById("<%=getid("mnu_10_img") %>").src = path;
            }
    document.getElementById(ctrl1).value = val + '__' + path;
}

    </script>


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" 
                    height="0">
                    <tr valign="top">
                        <td  valign="bottom" width="50%" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                        <td align="right" >
                            <table style="margin-left: 0px; height: 1px;" width="100%">
                                <tr>
                                    <td width="0"  height="0px">
                                        <asp:CheckBox ID="chkSelection1" runat="server" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0"  height="0px">
                                        <asp:CheckBox ID="chkSelection2" runat="server" AutoPostBack="True" Height="16px" />
                                    </td>
                                    <td  height="0px">&nbsp;
                                    </td>
                                    <td width="0"  height="0px">
                                        <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad4" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad5" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                    <td width="0">
                                        <asp:RadioButton ID="rad6" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td visible="false" runat="server" id="dateCol"  align="right">
                            <asp:Label ID="lblDate" runat="server" Text="Expiry As on Date" CssClass="field-label"></asp:Label>
                            <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True" Width="60%"></asp:TextBox>
                            <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand" ></asp:ImageButton>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server" TargetControlID="txtDate" PopupButtonID="lnkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center"  cellpadding="5" cellspacing="0" width="100%">
                    <tr  valign="top" id="fltrrow" runat="server">
                        <th align="right" valign="middle" width="70%">

                            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True" DataSourceID="namesXML" Visible="false"
                                DataTextField="FilterText" DataValueField="FilterValue" >
                            </asp:DropDownList>
                            <asp:XmlDataSource ID="namesXML" runat="server" DataFile="~/XMLData/XMLTopFilter.xml"
                                XPath="FilterElements/FiletrList"></asp:XmlDataSource>
                        </th>
                        <th align="right" valign="middle" style="text-align: right" width="30%">
                            <asp:LinkButton ID="btnPrint" runat="server" Visible="False">Print</asp:LinkButton>
                            &nbsp;
                <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink>
                        </th>
                    </tr>
                    <tr>
                        <td align="center" valign="top"  width="100%" colspan="2">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <%--<table style="width: 100%; height: 100%">
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col1"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol1" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                   <%-- </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table style="width: 100%; height: 100%">

                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col2"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol2" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                  <%--  </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col3"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol3" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                    <%--</td>

                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col4"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol4" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                   <%-- </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col5"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol5" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                   <%-- </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol6" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col6"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol6" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                  <%--  </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col7" SortExpression="Col7" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol7" runat="server" Text='<%# Bind("Col7") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <%--<table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol7" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col7"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol7" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                  <%--  </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol8" runat="server" Text='<%# Bind("Col8") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol8" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col8"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol8" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                   <%-- </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col9" SortExpression="Col9" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol9" runat="server" Text='<%# Bind("Col9") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol9" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col9"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol9" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                  <%--  </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col10" SortExpression="Col10" >
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol10" runat="server" Text='<%# Bind("Col10") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                           <%-- <table>
                                                <tr>
                                                    <td align="center">--%>
                                                        <asp:Label ID="lblHeaderCol10" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                            Text="Col10"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtCol10" runat="server" Width="80%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearch_Click" />
                                                   <%-- </td>
                                                </tr>
                                            </table>--%>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False" >
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                            Text="Select All" Visible="False" />
                                    </td>
                                    <td align="center" width="60%">
                                        <input type="button" id="ListButton1" class="button"  runat="server" onclick="this.disabled = true;" onserverclick="ListButton1_Click" />
                                        <asp:Button ID="ListButton2" runat="server" CssClass="button" CausesValidation="False"
                                            Width="142px" OnClick="ListButton2_Click" Visible="False"></asp:Button>
                                    </td>
                                    <td width="20%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    <%--    <script type="text/javascript">

        if (document.getElementById("chromemenu1") != null) { cssdropdown.startchrome("chromemenu1") }
        if (document.getElementById("chromemenu2") != null) { cssdropdown.startchrome("chromemenu2") }
        if (document.getElementById("chromemenu3") != null) { cssdropdown.startchrome("chromemenu3") }
        if (document.getElementById("chromemenu4") != null) { cssdropdown.startchrome("chromemenu4") }
        if (document.getElementById("chromemenu5") != null) { cssdropdown.startchrome("chromemenu5") }
        if (document.getElementById("chromemenu6") != null) { cssdropdown.startchrome("chromemenu6") }
        if (document.getElementById("chromemenu7") != null) { cssdropdown.startchrome("chromemenu7") }
        if (document.getElementById("chromemenu8") != null) { cssdropdown.startchrome("chromemenu8") }
        if (document.getElementById("chromemenu9") != null) { cssdropdown.startchrome("chromemenu9") }
        if (document.getElementById("chromemenu10") != null) { cssdropdown.startchrome("chromemenu10") }
        //cssdropdown.startchrome("chromemenu8")
    </script>--%>

    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
    <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
    <input id="h_SelectedId" runat="server" type="hidden" value="0" />
</asp:Content>
