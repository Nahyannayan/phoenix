<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupForm.aspx.vb" Inherits="SelBussinessUnit" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>


    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <%--        <link href="/cssfiles/sb-admin.css" rel="stylesheet" >--%>
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet" />
    
     

    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="/cssfiles/all-ie-only.css" />
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->

    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <base target="_self" />
  

    <script language="javascript" type="text/javascript">
        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            if (CheckBoxIDs != null) {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                    ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
   <%--    
      function menu_click(val,mid)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
               if (mid==1)
                 {
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid1() %>').src = path;
                 }
                 else  if (mid==2)
                 {
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid2() %>').src = path;
                 }
                  else  if (mid==3)
                 {
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
                 document.getElementById('<%=getid3() %>').src = path;
                 }                 
      }//end fn--%>


        function SetValuetoCompanyParent(result) {s
            //alert(result);
            parent.setCompanyValue(result);
            return false;
        }
    </script>
</head>
<body onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td>
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                        EmptyDataText="No Data" Width="100%" AllowPaging="True" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField HeaderText="Select" SortExpression="ID">
                                <ItemTemplate>
                                    <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    Select
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>

                                    <asp:Label ID="lblID" runat="server"></asp:Label><br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />

                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name" SortExpression="NAME">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label><br />
                                </ItemTemplate>
                                <HeaderTemplate>

                                    <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnNameSearch_Click" />

                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                <HeaderTemplate>

                                    <asp:Label ID="lblBSUName" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                    <asp:TextBox ID="txtBSUName" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnNameSearch_Click" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:CheckBox ID="chkSelAll" runat="server" CssClass="radiobutton" Text="Select All"
                        AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="button" OnClick="Button2_Click"/>
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
            </tr>
        </table>
        <%-- <script type="text/javascript">
                cssdropdown.startchrome("chromemenu1");
                if('<%=Request.QueryString("multiSelect") %>' == '' || '<%=Request.QueryString("multiSelect") %>' == 'true')
                { 
                    cssdropdown.startchrome("chromemenu2");
                }
                else
                {
                    cssdropdown.startchrome("chromemenu3");
                }
                
            </script>--%>
    </form>
</body>
</html>
