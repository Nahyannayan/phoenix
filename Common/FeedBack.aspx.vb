﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Accounts_FeedBack
    Inherits BasePage
    Dim Encr_decrData As New Encryption64

   
    Private Property VSReviewDetails() As DataTable
        Get
            Return Session("DtReviewDetails")
        End Get
        Set(ByVal value As DataTable)
            Session("DtReviewDetails") = value
        End Set
    End Property
    Dim dt As DataTable = New DataTable
   
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try               
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If                  

                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Else
                        ViewState("ViewId") = ""
                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)

                    'Checking whether already participated in the survey
                    Dim pIn As Integer = LoadReviewPage()
                    If pIn > 0 And 1 = 2 Then
                        lblError.Text = "Sorry! You have already participated in this survey"
                        tblReview.Visible = False
                        divpopup.Visible = True

                    Else
                        dt.Clear()
                        VSReviewDetails = dt
                        dt.Columns.Add("ID")
                        dt.Columns.Add("REVIEW")                        
                        LoadQuestions()
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub LoadQuestions()
      
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[GET_QUESIONS_ON_FEEDBACK]")
        'Section To load questions
        If (Not ds.Tables(0) Is Nothing) Then
            Dim dt As DataTable = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                rptrQuestionair.DataSource = dt
                rptrQuestionair.DataBind()
            Else

            End If
        End If
    End Sub
    Protected Sub rptrQuestionair_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            Dim hid_id As HiddenField = DirectCast(e.Item.FindControl("hidQuestionId"), HiddenField)
            dt.Rows.Add(Convert.ToInt32(hid_id.Value), "Great")
            If VSReviewDetails Is Nothing Then
                VSReviewDetails = dt
            Else
                VSReviewDetails.Merge(dt)
            End If
        End If
    End Sub

    Protected Function LoadReviewPage() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString       
        'Section To check on reviewed STATUS
        Dim Param(3) As SqlParameter
        Dim iIN As Integer = 0
        Param(0) = Mainclass.CreateSqlParameter("@username", Session("sUsr_name"), SqlDbType.VarChar)
        Param(1) = Mainclass.CreateSqlParameter("@bsuId", Session("sBsuid"), SqlDbType.VarChar)
        Param(2) = Mainclass.CreateSqlParameter("@remarks", "", SqlDbType.VarChar)
        Param(3) = Mainclass.CreateSqlParameter("@mode", "C", SqlDbType.VarChar)
        iIN = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[INSERT_USERFEEDBACK]", Param)
        Return iIN
    End Function
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        '[OASIS].[INSERT_USER_FEEDBACK]
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim iIn As Integer = 0
        ' Dim isSavable = False

        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            'Code to check whether all question has selected            
            Dim dt As DataTable = VSReviewDetails
            'For Each row In dt.Rows
            '    If (row("REVIEW") = "") Then
            '        isSavable = False
            '        lblError.Text = "Please fill your comments for *" & row("ID") & "Question"
            '        Exit For
            '    Else
            '        isSavable = True
            '    End If
            'Next

            ' If isSavable = True Then
            'insert to main table
            Dim Param(3) As SqlParameter
            Param(0) = Mainclass.CreateSqlParameter("@username", Session("sUsr_name"), SqlDbType.VarChar)
            Param(1) = Mainclass.CreateSqlParameter("@bsuId", Session("sBsuid"), SqlDbType.VarChar)
            Param(2) = Mainclass.CreateSqlParameter("@remarks", txtRemarks.Text, SqlDbType.VarChar)
            Param(3) = Mainclass.CreateSqlParameter("@mode", "I", SqlDbType.VarChar)
            iIn = SqlHelper.ExecuteScalar(strans, "[OASIS].[INSERT_USERFEEDBACK]", Param)

            'insert to sub table with reviews
            Dim param_sub(2) As SqlParameter
            For Each row In dt.Rows
                param_sub(0) = Mainclass.CreateSqlParameter("@Ques_id", row("ID"), SqlDbType.BigInt)
                param_sub(1) = Mainclass.CreateSqlParameter("@Ref_id", iIn, SqlDbType.BigInt)
                param_sub(2) = Mainclass.CreateSqlParameter("@Review", row("REVIEW"), SqlDbType.VarChar)
                SqlHelper.ExecuteNonQuery(strans, "[OASIS].[INSERT_USERREVIEWS]", param_sub)
            Next

            'commiting the transactoin and showing successmessage.
            strans.Commit()
            Session("DtReviewDetails") = ""
            lblError.Text = "<img src='https://school.gemsoasis.com/PHOENIXBETA/images/oasis-logo.png' /><br/><br/>Thank You for taking the time to complete the survey.<br/> Your feedback will help us enhance our customer experience.</p>"
            tblReview.Visible = False
            'btnSave.Visible = False
            divpopup.Visible = True
            'btnClose.Visible = True
            'Else

            'tblReview.Visible = True
            'divpopup.Visible = True
            'End If

        Catch ex As Exception
            strans.Rollback()
        End Try
    End Sub
    'Protected Sub btnClose_Click(sender As Object, e As EventArgs)
    '    Dim URL As String = ""
    '    'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
    '    URL = String.Format("~/Modulelogin.aspx.aspx")
    '    Response.Redirect(URL)
    'End Sub

   
    
    'Protected Sub imgBtn1_Command(sender As Object, e As CommandEventArgs)
    '    Dim id As String = e.CommandArgument.ToString()
    '    Dim foundRows() As Data.DataRow
    '    Dim dt As DataTable = VSReviewDetails
    '    foundRows = dt.Select("ID = " + id)
    '    foundRows(0)("REVIEW") = "Terrible"
    '    VSReviewDetails = dt
    'End Sub
    'Protected Sub imgBtn2_Command(sender As Object, e As CommandEventArgs)
    '    Dim id As String = e.CommandArgument.ToString()
    '    Dim foundRows() As Data.DataRow
    '    Dim dt As DataTable = VSReviewDetails
    '    foundRows = dt.Select("ID = " + id)
    '    foundRows(0)("REVIEW") = "Bad"
    '    VSReviewDetails = dt
    'End Sub
    'Protected Sub imgBtn3_Command(sender As Object, e As CommandEventArgs)
    '    Dim id As String = e.CommandArgument.ToString()
    '    Dim foundRows() As Data.DataRow
    '    Dim dt As DataTable = VSReviewDetails
    '    foundRows = dt.Select("ID = " + id)
    '    foundRows(0)("REVIEW") = "Ok"
    '    VSReviewDetails = dt
    'End Sub
    'Protected Sub imgBtn4_Command(sender As Object, e As CommandEventArgs)
    '    Dim id As String = e.CommandArgument.ToString()
    '    Dim foundRows() As Data.DataRow
    '    Dim dt As DataTable = VSReviewDetails
    '    foundRows = dt.Select("ID = " + id)
    '    foundRows(0)("REVIEW") = "Good"
    '    VSReviewDetails = dt
    'End Sub

    'Protected Sub imgBtn5_Command(sender As Object, e As CommandEventArgs)
    '    Dim id As String = e.CommandArgument.ToString()
    '    Dim foundRows() As Data.DataRow
    '    Dim dt As DataTable = VSReviewDetails
    '    foundRows = dt.Select("ID = " + id)
    '    foundRows(0)("REVIEW") = "Great"
    '    VSReviewDetails = dt
    'End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function a(ByVal Id As String, ByVal Review As String) As String
        Dim foundRows() As Data.DataRow
        Dim dt As DataTable = HttpContext.Current.Session("DtReviewDetails")
        foundRows = dt.Select("ID = " + Id)
        foundRows(0)("REVIEW") = Review
        HttpContext.Current.Session("DtReviewDetails") = dt
        Return "ABC"
    End Function


    Protected Sub rptrQuestionair_ItemCommand(source As Object, e As RepeaterCommandEventArgs)
        Dim img1 As ImageButton = CType(e.Item.FindControl("imgBtn1"), ImageButton)
        Dim img2 As ImageButton = CType(e.Item.FindControl("imgBtn2"), ImageButton)
        Dim img3 As ImageButton = CType(e.Item.FindControl("imgBtn3"), ImageButton)

        If ViewState("Choice") = 1 Then
            img1.CssClass = "smily-icon"
            img2.CssClass = "blur"
            img3.CssClass = "blur"
        ElseIf ViewState("Choice") = 2 Then
            img1.CssClass = "blur"
            img2.CssClass = "smily-icon"
            img3.CssClass = "blur"

        ElseIf ViewState("Choice") = 3 Then
            img1.CssClass = "blur"
            img2.CssClass = "blur"
            img3.CssClass = "smily-icon"
        End If
    End Sub
End Class
