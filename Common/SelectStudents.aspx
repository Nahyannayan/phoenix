﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelectStudents.aspx.vb"
    Inherits="Common_SelectStudents" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>

    <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>
    <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Page level plugin CSS-->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet"/>
    <script>
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <script language="javascript" type="text/javascript">
      

        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            // var lstrChk = document.getElementById("chkAL").checked;
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function GetCheckedValues(chk) {
            var stuid = $("#" + chk).val();
            var stuids = $("#<%=hf_STUID.ClientID %>").val();
            if ($("#" + chk).is(":checked") == true) {
                stuids = stuids + "||" + stuid;
            }
            else {
                stuids = stuids.replace("||" + stuid, "");
            }
            $("#<%=hf_STUID.ClientID %>").val(stuids);
            return true;
        }
        function SetValuetoParent() {
            parent.setValue($("#<%=hf_STUID.ClientID %>").val());
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
       
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td >
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <input id="hf_STUID" runat="server" type="hidden" value="" />
                    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                </td>
            </tr>
            <tr>
                <td >
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                        EmptyDataText="No Data" AllowPaging="True" PageSize="12">
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <HeaderTemplate>
                                    <%--<input id="chkAL" name="chkAL" visible="false" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                    value="Check All" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("STU_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Code">
                                <HeaderTemplate>
                                    Student#
                                    <br />
                                    <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Name">
                                <HeaderTemplate>
                                    Student Name<br />
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbCodeSubmit" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Control Account" SortExpression="ACT_CTRLACC">
                                <HeaderTemplate>
                                    Grade
                                    <br />
                                    <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchControl" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parent Name">
                                <HeaderTemplate>
                                    Parent Name
                                    <br />
                                    <asp:TextBox ID="txtPName" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parent Mobile">
                                <HeaderTemplate>
                                    Parent Mobile
                                    <br />
                                    <asp:TextBox ID="txtMobile" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("PARENT_MOBILE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>               
            </tr>
            <tr>               
                <td align="center">
                     <asp:CheckBox ID="chkSelAll" runat="server" Visible="false" AutoPostBack="True" CssClass="radiobutton"
                        Text="Select All"  />
                    <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" />
                </td>
            </tr>
        </table>      
    </form>
</body>
</html>
