Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PopupBanks
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Expires = 0
        Response.Cache.SetNoStore()
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            gridbind()
        End If
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub gridbind()
        Select Case Request.QueryString("ID")
            Case "BANK"
                GridBindBank(False)
            Case "BANKOTHFC" ''other fee collection
                GridBindBank(True)
        End Select
    End Sub

    Sub GridBindBank(ByVal bExcludeUBC As Boolean)
        Try
            Dim lblheader As New Label
            Dim str_txtCode As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_xcludeUBC As String = String.Empty
            Dim str_Sql As String
            Dim str_query_header As String
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BNK_SHORT", str_Sid_search(0), str_txtCode)
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BNK_DESCRIPTION", str_Sid_search(0), str_txtName)
            End If
            If bExcludeUBC Then
                str_xcludeUBC = " ISNULL(BNK_TYPE,'')<>'UBC' AND "
            Else
                str_xcludeUBC = ""
            End If
            str_filter_code = str_filter_code & " and  (BANK_CTY_ID IN (SELECT BSU_COUNTRY_ID FROM BUSINESSUNIT_M WITH(NOLOCK) WHERE (BSU_ID = '" & Session("sBsuid") & "')))"
            str_query_header = "SELECT BNK_ID AS ID, BNK_DESCRIPTION AS DESCR2, BNK_SHORT AS DESCR1, ISNULL(BNK_TYPE,'')AS BNK_TYPE FROM BANK_M WHERE " & str_xcludeUBC & " BNK_bENABLED=1 " _
                    & str_filter_code & str_filter_name _
                    & " ORDER BY BNK_DESCRIPTION|" & "Short Name|Bank"
            str_Sql = str_query_header.Split("|")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvGroup.DataBind()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            txtSearch.Text = str_txtName

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'set_Menu_Img()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblid As New Label, lblbnktype As New Label
        Dim lnkDESCR As New LinkButton
        lnkDESCR = sender
        lblid = sender.Parent.FindControl("lblid")
        lblbnktype = sender.Parent.FindControl("lblbnktype")
        If (Not lblid Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblid.Text.Replace("'", "\'") & "___" & lnkDESCR.Text.Replace("'", "\'") & "___" & lblbnktype.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode = '" & lblid.Text.Replace("'", "\'") & "||" & lnkDESCR.Text.Replace("'", "\'") & "||" & lblbnktype.Text.Replace("'", "\'") & "';")
            Response.Write("var oWnd = GetRadWindow('" & lblid.Text.Replace("'", "\'") & "||" & lnkDESCR.Text.Replace("'", "\'") & "||" & lblbnktype.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

End Class

