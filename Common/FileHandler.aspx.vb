
Imports System.IO
Imports UtilityObj
Imports System.Web.Configuration
Imports System.Web.HttpContext
Imports System.Data
Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Partial Class Common_FileHandler
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Dim PathOfFile As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim TYPE As String = Encr_decrData.Decrypt(Context.Request.QueryString("TYPE"))
            If TYPE Is Nothing Then Exit Sub
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If TYPE = "XLSDATA" Then
                Dim Title As String
                Title = Encr_decrData.Decrypt(Request.QueryString("Title"))
                If Title Is Nothing Or Title = "" Then
                    Title = "MyExportData"
                ElseIf Title.Length > 30 Then
                    Title = Title.Substring(0, 30).ToString
                End If

                Dim myDT As New DataTable
                myDT = Session("myData")
                Dim PathOfFile As String
                Dim NameOfFile As String = Title & ".xlsx"
                PathOfFile = WebConfigurationManager.AppSettings.Item("TempFileFolder") & NameOfFile
                ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
                '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                Dim ef As ExcelFile = New ExcelFile
                ef.Worksheets.Add(Title)
                ''commented and added by nahyan on 18apr2016 for new gembox
                'ef.Worksheets(0).InsertDataTable(myDT, 0, 0, True)
                'ef.SaveXls(PathOfFile)

                ef.Worksheets(0).InsertDataTable(myDT, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '' ef.Worksheets(0).HeadersFooters.AlignWithMargins = True
                ef.Save(PathOfFile)


                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(PathOfFile)
                If IO.File.Exists(PathOfFile) Then
                    IO.File.Delete(PathOfFile)
                End If
                Dim ContentType, Extension As String
                ContentType = "application/vnd.ms-excel"
                Extension = GetFileExtension(ContentType)
                DownloadFile(BytesData, ContentType, Extension, Title)
            ElseIf TYPE = "PCDOCDOWNLOAD" Then
                Dim DocID As String = ""
                DocID = Encr_decrData.Decrypt(Request.QueryString("DocID").Replace(" ", "+"))
                If DocID <> "" Then
                    OpenFileFromDB(DocID)
                Else
                    Response.Redirect(ViewState("ReferrerUrl"))
                End If

            End If
        Catch ex As Exception
            ' Response.Redirect(ViewState("ReferrerUrl"))
        Finally
            'Response.Redirect(ViewState("ReferrerUrl"))
        End Try
    End Sub
    Private Sub OpenFileFromDB(ByVal DocId As String)
        Try
            Dim conn As String = ""
            conn = WebConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString
            Dim param(0) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@DOC_ID", DocId, SqlDbType.VarChar)
            Dim mTable As New DataTable
            mTable = Mainclass.getDataTable("OpenDocumentFromDB", param, conn)
            If mTable.Rows.Count > 0 Then
                Response.ContentType = mTable.Rows(0).Item("DOC_CONTENT_TYPE").ToString()
                Dim bytes() As Byte = CType(mTable.Rows(0).Item("DOC_DOCUMENT"), Byte())

                Response.Clear()
                Response.AddHeader("Content-Length", bytes.Length.ToString())
                Response.ContentType = ContentType
                Response.AddHeader("Expires", "0")
                Response.AddHeader("Cache-control", "private")
                'Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                Response.AddHeader("Pragma", "private")
                Response.AddHeader("Content-Disposition", "attachment; filename=" & mTable.Rows(0).Item("DOC_DOCNO").ToString())
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.[End]()

                'Response.Buffer = True
                'Response.Charset = ""
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                'Response.AddHeader("content-disposition", "attachment;filename=" & mTable.Rows(0).Item("DOC_DOCNO").ToString())
                'Response.BinaryWrite(bytes)
                'Response.Flush()
                'Response.End()
            End If
        Catch ex As Exception
 
        End Try
    End Sub
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub DownloadFile(ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String)
        Response.Clear()
        Response.AddHeader("Content-Length", BytesData.Length.ToString())
        Response.ContentType = contentType
        Response.AddHeader("Expires", "0")
        'Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        'Response.AddHeader("Pragma", "public")
        Response.AddHeader("Cache-control", "private")
        Response.AddHeader("Pragma", "private")
        Response.AddHeader("Content-Disposition", "attachment; filename=" & Title & Extension)
        Response.BinaryWrite(BytesData)
        Response.Flush()
        Response.[End]()
    End Sub
    Private Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Public Function ConvertBytesToFile(ByVal BytesData As Byte(), ByVal FilePath As String) As Boolean
        If IsNothing(BytesData) = True Then
            Return False
            'Throw New ArgumentNullException("Image Binary Data Cannot be Null or Empty", "ImageData")
        End If
        Try
            If IO.File.Exists(FilePath) Then IO.File.Delete(FilePath)
            Dim fs As IO.FileStream = New IO.FileStream(FilePath, IO.FileMode.Create, IO.FileAccess.Write)
            Dim bw As IO.BinaryWriter = New IO.BinaryWriter(fs)
            bw.Write(BytesData)
            bw.Flush()
            bw.Close()
            fs.Close()
            bw = Nothing
            fs.Dispose()
            Return True
        Catch ex As Exception
            Return True
        End Try
    End Function
End Class
