Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Data.OleDb
Imports Telerik.Web
Imports Telerik.Web.UI

Partial Class UserDataRequestEntry
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property MainMnu_code() As String
        Get
            Return ViewState("MainMnu_code")
        End Get
        Set(ByVal value As String)
            ViewState("MainMnu_code") = value
        End Set
    End Property
    Private Property myBindDataTable() As DataTable
        Get
            Return ViewState("myBindDataTable")
        End Get
        Set(ByVal value As DataTable)
            ViewState("myBindDataTable") = value
        End Set
    End Property
    Private Property MyEntryData() As DataTable
        Get
            Return ViewState("MyEntryData")
        End Get
        Set(ByVal value As DataTable)
            ViewState("MyEntryData") = value
        End Set
    End Property
    Private Property IsTableCreated() As Boolean
        Get
            Return ViewState("IsTableCreated")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsTableCreated") = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+")) = "view" Then
            Dim DRI_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            FillDetails(DRI_ID)
            SetDataMode("view")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        '      smScriptManager.RegisterPostBackControl(btnSave)
        If Not IsPostBack Then
            '  IsTableCreated = False
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            lblReportCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            txtLastDate.SelectedDate = Format(Now.Date.AddDays(-1), OASISConstants.DateFormat)

            'If ViewState("datamode") = "view" Then
            '    Dim DRI_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            '    FillDetails(DRI_ID)
            '    SetDataMode("view")
            'End If
        End If
    End Sub
    Sub FillDetails(ByVal DRI_ID As Int16)
        Dim sql As String
        Dim dtDataReq As New DataTable
        Dim DataSetDataReq As DataSet
        sql = "SP_GET_DATA_REQUEST_ENTRY_ENTRY_DETAILS"
        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@DRI_ID", DRI_ID, SqlDbType.Int)
        sqlParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        DataSetDataReq = Mainclass.getDataSet(sql, sqlParam, ConnectionManger.GetOASISConnectionString)
        If DataSetDataReq.Tables.Count > 0 Then
            dtDataReq = DataSetDataReq.Tables(0)
            If dtDataReq.Rows.Count > 0 Then
                hdn_DRI_ID.Value = dtDataReq.Rows(0).Item("DRI_ID")
                lblTitle.Text = dtDataReq.Rows(0).Item("DRI_TITLE").ToString
                lblDescription.Text = dtDataReq.Rows(0).Item("DRI_DESCRIPTION").ToString
                txtLastDate.SelectedDate = dtDataReq.Rows(0).Item("DRI_REQ_LASTDT")
                Dim RowKeys, ColKeys As String, ColDataTypes As String, RowKeysArr(), ColKeysArr() As String, ColDataTypeArr() As String
                RowKeys = ""
                ColKeys = ""
                RowKeys = dtDataReq.Rows(0).Item("DRI_ROW_KEYS").ToString
                ColKeys = dtDataReq.Rows(0).Item("DRI_COL_KEYS").ToString
                ColDataTypes = dtDataReq.Rows(0).Item("DRI_COL_DATA_TYPE").ToString

                hdnRowKeys.Value = RowKeys
                hdnColKeys.Value = ColKeys
                RowKeysArr = RowKeys.Split("|")
                ColKeysArr = ColKeys.Split("|")
                ColDataTypeArr = ColDataTypes.Split("|")

                If DataSetDataReq.Tables.Count > 1 Then
                    MyEntryData = DataSetDataReq.Tables(2)
                    '    myBindDataTable = MyEntryData
                    PreviewTable(ColKeysArr, RowKeysArr, ColDataTypeArr, MyEntryData)
                End If

            End If
        End If
        If DataSetDataReq.Tables.Count > 1 Then
            dtDataReq = DataSetDataReq.Tables(1)
            If dtDataReq.Rows.Count > 0 Then
                hdn_DRH_ID.Value = dtDataReq.Rows(0).Item("DRH_ID")
                chkForward.Checked = dtDataReq.Rows(0).Item("DRH_bForward")
                If dtDataReq.Rows(0).Item("DRH_bForward") Then
                    lblEntryForwarded.Text = "Record submitted."
                    chkForward.Visible = False
                Else
                    lblEntryForwarded.Text = " "
                    chkForward.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub PreviewTable(ByVal lstColumnKeyVal() As String, ByVal lstRowKeyVal() As String, ByVal lstColDataType() As String, ByVal dtDataReq As DataTable)
        Dim Index As Int16
        Index = 1
        Dim iRow As DataRow
        If Not IsTableCreated Then
            CreateTable(lstColumnKeyVal, lstRowKeyVal, lstColDataType, dtDataReq)
            IsTableCreated = True
        End If
        For Each lstItem As String In lstRowKeyVal
            Dim mRow As DataRow
            If lstItem <> "" Then
                If myBindDataTable.Select("DESCRIPTION='" & lstItem & "'", "").Length > 0 Then
                    mRow = myBindDataTable.Select("DESCRIPTION='" & lstItem & "'", "")(0)
                    mRow("ID") = Index
                    Index = Index + 1
                    mRow("DESCRIPTION") = lstItem
                    For Each iRow In dtDataReq.Select("DRD_ROW_KEY='" & lstItem & "'")
                        Dim myColName As String
                        myColName = "[" & iRow("DRD_COL_KEY") & "]"
                        If myBindDataTable.Columns.Contains(myColName) Then
                            mRow(myColName) = iRow("DRD_KEY_VALUE")
                        End If
                    Next
                End If
            End If
        Next
        RadGridReport.DataSource = myBindDataTable
        RadGridReport.DataBind()
    End Sub

    Private Sub CreateTable(ByVal lstColumnKeyVal() As String, ByVal lstRowKeyVal() As String, ByVal lstColDataType() As String, ByVal dtDataReq As DataTable)
        Dim dtPreviewTable As New DataTable
        If Not dtPreviewTable.Columns.Contains("ID") Then
            dtPreviewTable.Columns.Add("ID", Type.GetType("System.Int16"))
        End If

        If Not dtPreviewTable.Columns.Contains("DESCRIPTION") Then
            dtPreviewTable.Columns.Add("DESCRIPTION", Type.GetType("System.String"))
        End If

        Dim lstItem As String
        Dim StrColDataType As String = ""
        Dim Index As Int16 = 0
        For i As Int16 = 0 To lstColumnKeyVal.Length - 1
            lstItem = lstColumnKeyVal(i)
            If lstColDataType.Length > i Then
                StrColDataType = lstColDataType(i)
            End If
            If StrColDataType = "" Then
                StrColDataType = "Text"
            End If

            If lstItem.Trim = "" Then
                Continue For
            End If
            If Not dtPreviewTable.Columns.Contains("[" & lstItem & "]") Then
                dtPreviewTable.Columns.Add("[" & lstItem & "]", Type.GetType("System.String"))
            End If

            CreateNewTemplateColumn(Index, lstItem, StrColDataType)
            Index = Index + 1
        Next
        RadGridReport.MasterTableView.Rebind()
        Index = 1
        dtPreviewTable.Rows.Clear()
        For Each lstItem In lstRowKeyVal
            If lstItem <> "" Then
                Dim mRow As DataRow
                mRow = dtPreviewTable.NewRow
                mRow("ID") = Index
                Index = Index + 1
                mRow("DESCRIPTION") = lstItem
                dtPreviewTable.Rows.Add(mRow)
            End If

        Next
        myBindDataTable = dtPreviewTable
        RadGridReport.DataSource = myBindDataTable
        RadGridReport.DataBind()
    End Sub

    Private Sub CreateNewLabelTemplateColumn(ByVal Index As Int16, ByVal ColumnKey As String)
        Dim templateColumnName As String = ColumnKey
        Dim templateColumn As New GridTemplateColumn()
        templateColumn.ItemTemplate = New MyLabelTemplate(templateColumnName)
        templateColumn.HeaderText = templateColumnName
        If Not RadGridReport.MasterTableView.Columns.Contains(templateColumn) Then
            RadGridReport.MasterTableView.Columns.Add(templateColumn)
        End If
    End Sub
    Private Sub CreateNewTemplateColumn(ByVal Index As Int16, ByVal ColumnKey As String, ByVal ColumnDataType As String)
        Dim templateColumnName As String = ColumnKey
        Dim templateColumn As New GridTemplateColumn()
        templateColumn.ItemTemplate = New MyTemplate(templateColumnName, ColumnDataType)
        templateColumn.HeaderText = templateColumnName
        templateColumn.UniqueName = templateColumnName
        If Not RadGridReport.MasterTableView.Columns.Contains(templateColumn) Then
            RadGridReport.MasterTableView.Columns.Add(templateColumn)
        End If
    End Sub
    Private Class MyTemplate
        Implements ITemplate
        Protected validatorTextBox As RequiredFieldValidator
        Protected NumericExpressionValidator As RegularExpressionValidator
        Protected hdn_colDataType As HiddenField
        Protected DatePicker As RadDatePicker

        Protected textBox As TextBox
        Private colname As String
        Private colDataType As String
        Public Sub New(ByVal cName As String, ByVal ColumnDataType As String)
            colname = cName.Replace(" ", "___")
            colDataType = ColumnDataType
        End Sub

        Protected Sub GridTxtBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim i As Int16
            i = i + 1
        End Sub

        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            If colDataType.ToLower = "text" Then
                textBox = New TextBox()
                textBox.ID = colname
                textBox.Width = "100"
                If container.FindControl(colname) Is Nothing Then
                    container.Controls.Add(textBox)
                End If
            ElseIf colDataType.ToLower = "numeric" Then
                textBox = New TextBox()
                textBox.ID = colname
                textBox.Width = "100"
                textBox.Text = "0"
                textBox.Style("text-align") = "right"
                textBox.ValidationGroup = "NumBox"
                NumericExpressionValidator = New RegularExpressionValidator
                NumericExpressionValidator.ControlToValidate = colname
                NumericExpressionValidator.ErrorMessage = "*"
                NumericExpressionValidator.ValidationExpression = "^[1-9]\d*(\.\d+)?$"
                NumericExpressionValidator.ValidationGroup = "NumBox"
                If container.FindControl(colname) Is Nothing Then
                    container.Controls.Add(textBox)
                    container.Controls.Add(NumericExpressionValidator)
                End If
            ElseIf colDataType.ToLower = "date" Then
                DatePicker = New RadDatePicker
                DatePicker.ID = colname
                DatePicker.Width = "100"
                DatePicker.DateInput.DateFormat = "dd/MMM/yyyy"
                DatePicker.DatePopupButton.ImageUrl = ""
                DatePicker.DatePopupButton.HoverImageUrl = ""
                DatePicker.Calendar.ID = "Calendar3"
                DatePicker.Calendar.UseRowHeadersAsSelectors = False
                DatePicker.Calendar.UseColumnHeadersAsSelectors = False
                DatePicker.Calendar.ViewSelectorText = "x"
                If container.FindControl(colname) Is Nothing Then
                    container.Controls.Add(DatePicker)
                End If
            End If
            hdn_colDataType = New HiddenField
            hdn_colDataType.ID = "hdn_colDataType_" & colname
            hdn_colDataType.Value = colDataType.ToLower
            container.Controls.Add(hdn_colDataType)
        End Sub
    End Class
    Private Class MyLabelTemplate
        Implements ITemplate
        Protected label As Label
        Private colname As String
        Public Sub New(ByVal cName As String)
            colname = cName
        End Sub
        Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
            label = New Label()
            label.ID = colname
            If container.FindControl(colname) Is Nothing Then
                container.Controls.Add(label)
            End If
        End Sub
    End Class

    Protected Sub RadGridReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGridReport.ItemDataBound
        If TypeOf e.Item Is GridDataItem Then
            Dim h_Grd_Row_ID As HiddenField
            h_Grd_Row_ID = TryCast(e.Item.FindControl("h_Grd_Row_ID"), HiddenField)
            If h_Grd_Row_ID IsNot Nothing Then
                Dim ID As Int16
                ID = h_Grd_Row_ID.Value
                Dim myColumnName As String
                If myBindDataTable.Select("ID=" & ID.ToString).Length > 0 Then
                    Dim myRow As DataRow
                    myRow = myBindDataTable.Select("ID=" & ID.ToString)(0)
                    Dim i As Int16
                    For i = 0 To myBindDataTable.Columns.Count - 1
                        myColumnName = myBindDataTable.Columns(i).ColumnName.Replace(" ", "___").Replace(" ", "___").Replace("[", "").Replace("]", "")
                        If Not e.Item.FindControl(myColumnName) Is Nothing Then
                            If Not CType(e.Item.FindControl("hdn_colDataType_" & myColumnName), HiddenField) Is Nothing Then
                                If CType(e.Item.FindControl("hdn_colDataType_" & myColumnName), HiddenField).Value = "date" Then
                                    If IsDate(myRow(myBindDataTable.Columns(i)).ToString) Then
                                        CType(e.Item.FindControl(myColumnName), RadDatePicker).SelectedDate = CDate(myRow(myBindDataTable.Columns(i)).ToString).ToString("dd/MMM/yyyy")
                                    Else
                                        ' CType(e.Item.FindControl(myColumnName), RadDatePicker). = ""
                                    End If

                                ElseIf CType(e.Item.FindControl("hdn_colDataType_" & myColumnName), HiddenField).Value = "numeric" Then
                                    If myRow(myBindDataTable.Columns(i)).ToString <> "" Then
                                        CObj(e.Item.FindControl(myColumnName)).Text = myRow(myBindDataTable.Columns(i)).ToString
                                    Else
                                        CObj(e.Item.FindControl(myColumnName)).Text = "0"
                                    End If
                                Else
                                    CObj(e.Item.FindControl(myColumnName)).Text = myRow(myBindDataTable.Columns(i)).ToString
                                End If

                            End If

                        End If
                    Next
                End If
            End If
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Try

            Dim Trandate As String = txtLastDate.SelectedDate.ToString
            If Not IsDate(Trandate) Then
                lblError.Text = "Invalid Transaction Date !!!!"
                Exit Sub
            End If

            If ViewState("datamode") = "add" Then
                hdn_DRI_ID.Value = 0
            End If

            Dim RowKeys, ColKeys As String
            RowKeys = ""
            ColKeys = ""
            Dim strError As String
            strError = ""
            Dim sqlParam(12) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@DRH_ID", Val(hdn_DRH_ID.Value), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@DRH_DRI_ID", hdn_DRI_ID.Value, SqlDbType.Int)
            sqlParam(2) = Mainclass.CreateSqlParameter("@DRH_BSU_ID", Session("sBsuId"), SqlDbType.VarChar)
            sqlParam(3) = Mainclass.CreateSqlParameter("@DRH_EMP_ID", Session("EmployeeID"), SqlDbType.Int)
            sqlParam(4) = Mainclass.CreateSqlParameter("@DRH_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@DRH_bForward", chkForward.Checked, SqlDbType.Bit)
            sqlParam(6) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)

            Dim Retval As String
            Retval = 0
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim Failure As Boolean = False

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_SAVE_USER_DATA_REQUEST_TRAN_H", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(6).Value = 0 Then
                hdn_DRH_ID.Value = sqlParam(0).Value
                Dim RadGridVwRow As Telerik.Web.UI.GridDataItem
                Dim lblDescription As Label
                Dim Col_Key_Val As String
                Dim hdn_colDataType As HiddenField
                Dim myColumnName As String
                Dim sqlValues(5) As SqlParameter
                Dim RowKeysArr(), ColKeysArr() As String
                RowKeys = hdnRowKeys.Value
                ColKeys = hdnColKeys.Value
                RowKeysArr = RowKeys.Split("|")
                ColKeysArr = ColKeys.Split("|")
                For Each RadGridVwRow In RadGridReport.Items
                    lblDescription = CType(RadGridVwRow.FindControl("lblDescr"), Label)
                    If Not lblDescription Is Nothing Then
                        For i As Int16 = 0 To myBindDataTable.Columns.Count - 1
                            myColumnName = myBindDataTable.Columns(i).ColumnName.Replace(" ", "___").Replace("[", "").Replace("]", "")
                            Col_Key_Val = ""
                            If Not RadGridVwRow.FindControl(myColumnName) Is Nothing Then
                                hdn_colDataType = CType(RadGridVwRow.FindControl("hdn_colDataType_" & myColumnName), HiddenField)
                                If Not hdn_colDataType Is Nothing Then
                                    If hdn_colDataType.Value = "date" Then
                                        If IsDate(CType(RadGridVwRow.FindControl(myColumnName), RadDatePicker).SelectedDate) Then
                                            Col_Key_Val = CDate(CType(RadGridVwRow.FindControl(myColumnName), RadDatePicker).SelectedDate).ToString("dd/MMM/yyyy")
                                        End If
                                        If Not IsDate(Col_Key_Val) Then
                                            Col_Key_Val = ""
                                        End If
                                    ElseIf hdn_colDataType.Value = "numeric" Then
                                        Col_Key_Val = CType(RadGridVwRow.FindControl(myColumnName), TextBox).Text
                                        If Not IsNumeric(Col_Key_Val) Then
                                            Col_Key_Val = "0"
                                            Failure = True
                                            strError = "Invalid numeric amount."
                                            Exit For
                                        End If
                                    Else
                                        Col_Key_Val = CType(RadGridVwRow.FindControl(myColumnName), TextBox).Text
                                    End If
                                    sqlValues(0) = Mainclass.CreateSqlParameter("@DRD_ID", "0", SqlDbType.Int, True)
                                    sqlValues(1) = Mainclass.CreateSqlParameter("@DRD_DRH_ID", hdn_DRH_ID.Value, SqlDbType.Int)
                                    sqlValues(2) = Mainclass.CreateSqlParameter("@DRD_ROW_KEY", lblDescription.Text, SqlDbType.VarChar)
                                    sqlValues(3) = Mainclass.CreateSqlParameter("@DRD_COL_KEY", myColumnName.Replace("___", " "), SqlDbType.VarChar)
                                    sqlValues(4) = Mainclass.CreateSqlParameter("@DRD_KEY_VALUE", Col_Key_Val, SqlDbType.VarChar)
                                    sqlValues(5) = Mainclass.CreateSqlParameter("@ReturnValue", "0", SqlDbType.Int, True)
                                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SP_SAVE_USER_DATA_REQUEST_TRAN_D", sqlValues)
                                    If sqlValues(5).Value <> "0" Then
                                        Failure = True
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    End If
                    If Failure Then
                        Exit For
                    End If
                Next
            Else
                Failure = True
            End If

            If Not Failure Then
                stTrans.Commit()
                FillDetails(hdn_DRI_ID.Value)
                SetDataMode("view")
                lblError.Text = "Data Saved Successfully !!!"
                UtilityObj.operOnAudiTable(CObj(Master).MenuName, hdn_DRI_ID.Value, IIf(ViewState("datamode") = "add", "Insert", "Edit"), Page.User.Identity.Name.ToString, Me.Page)
            Else
                stTrans.Rollback()
                If strError <> "" Then
                    lblError.Text = strError
                Else
                    lblError.Text = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(8).Value)
                End If

            End If

        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Sub SetDataMode(ByVal mode As String)
        Dim mDisable As Boolean
        If mode = "view" Then
            mDisable = True
            ViewState("datamode") = "view"
        ElseIf mode = "add" Then
            clear_All()
            mDisable = False
            ViewState("datamode") = "add"
            lblEntryForwarded.Text = ""
            chkForward.Visible = True
            chkForward.Checked = False
        ElseIf mode = "edit" Then
            mDisable = False
            ViewState("datamode") = "edit"

            Dim RowKeys, ColKeys As String, RowKeysArr(), ColKeysArr() As String
            RowKeys = ""
            ColKeys = ""
            RowKeys = hdnRowKeys.Value
            ColKeys = hdnColKeys.Value
            RowKeysArr = RowKeys.Split("|")
            ColKeysArr = ColKeys.Split("|")
        End If
        Dim Forwarded As Boolean
        Forwarded = chkForward.Checked
        btnSave.Enabled = Not mDisable And Not Forwarded
        btnEdit.Visible = mDisable And Not Forwarded
        btnDelete.Visible = mDisable And Not Forwarded
        txtLastDate.Enabled = False
        txtRemarks.Enabled = Not mDisable And Not Forwarded
        RadGridReport.Enabled = Not mDisable And Not Forwarded
        chkForward.Enabled = Not mDisable And Not Forwarded
    End Sub

    Sub clear_All()
        hdn_DRI_ID.Value = ""
        hdnRowKeys.Value = ""
        hdnColKeys.Value = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "edit" Then
            FillDetails(hdn_DRI_ID.Value)
            SetDataMode("view")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        SetDataMode("edit")
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim retval As String
            '    retval = DeletePayrollFreezeDetail(h_PFZ_ID.Value)
            If (retval = "0" Or retval = "") Then
                UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                lblError.Text = "Data Deleted Successfully !!!!"
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                lblError.Text = retval
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblError.Text = ex.Message
        End Try
    End Sub


End Class
