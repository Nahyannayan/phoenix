Imports Lesnikowski.Barcode
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration

Partial Class Common_PrintViewer
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim repSource As ReportClass
    Dim repClassVal As New ReportClass
    Shared rnd As New Random()

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    If Web.Configuration.WebConfigurationManager.AppSettings("AlwaysSSL").ToLower = "true" Then
                        sTxt = sTxt.Replace(Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString(), _
                        Web.Configuration.WebConfigurationManager.AppSettings("webvirtualpath").ToString().Replace("http://", "https://"))
                    End If
                    Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ReportData As New DataTable
        Page.Title = OASISConstants.Gemstitle
        If Cache.Item("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
            Cache.Remove("reportDS" & ViewState("h_uniqueID"))
            ViewState("h_uniqueID") = Session.SessionID & Rnd.Next()
            Session("TempReportClassSource" & ViewState("h_uniqueID")) = repSource
        End If
        If Not IsPostBack Then
            repSource = DirectCast(Session("ReportClassSource"), ReportClass)
            Session("TempReportClassSource" & ViewState("h_uniqueID")) = repSource
        Else
            repSource = DirectCast(Session("TempReportClassSource" & ViewState("h_uniqueID")), ReportClass)
        End If
        If Cache("reportDS" & ViewState("h_uniqueID")) Is Nothing Then
            ReportData = repSource.ReportData
            If ReportData Is Nothing OrElse ReportData.Rows.Count <= 0 Then
                If Not Request.UrlReferrer Is Nothing Then
                    If Request.UrlReferrer.ToString() <> "" And "/" & Request.UrlReferrer.GetComponents(UriComponents.Path, UriFormat.SafeUnescaped) <> Request.CurrentExecutionFilePath Then
                        Dim urlstring As String = IIf(Request.UrlReferrer.ToString().Contains("?"), "&nodata=true", "?nodata=true")
                        Response.Redirect(Request.UrlReferrer.ToString() & urlstring)
                    End If
                Else
                    Exit Sub
                End If
            End If
            Cache.Insert("reportDS" & ViewState("h_uniqueID"), ReportData, Nothing, DateTime.Now.AddMinutes(30), Nothing)
        End If
        repClassVal = New ReportClass
        repClassVal.ResourceName = repSource.ResourceName
        ReportData = DirectCast(Cache.Item("reportDS" & ViewState("h_uniqueID")), DataTable)
        repClassVal.SetDataSource(ReportData)
        If repSource.IncludeBSUImage Then
            If repSource.HeaderBSUID Is Nothing Then
                IncludeBSUmage(repClassVal)
            Else
                IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
            End If
        End If
        SubReport(repSource, repClassVal)
        MyReportViewer.ReportSource = repClassVal
        repClassVal.PrintOptions.PaperOrientation = repClassVal.PrintOptions.PaperOrientation
        Dim ienum As IDictionaryEnumerator
        If repSource.Parameter IsNot Nothing Then
            ienum = repSource.Parameter.GetEnumerator()
            While ienum.MoveNext()
                If ienum.Key = "userName" Then
                    If Not Session("lastMnuCode") Is Nothing Then
                        repClassVal.SetParameterValue(ienum.Key, ienum.Value + " [Ref. - " + Session("lastMnuCode") + "]")
                    End If
                Else
                    Try
                        repClassVal.SetParameterValue(ienum.Key, ienum.Value)
                    Catch ex As Exception

                    End Try
                End If
            End While
        End If
        Try
            If repSource.Formulas IsNot Nothing Then
                If Not repSource.Formulas.GetEnumerator() Is Nothing Then
                    Dim iformula As IDictionaryEnumerator = repSource.Formulas.GetEnumerator()
                    While iformula.MoveNext()
                        If Not repClassVal.DataDefinition.FormulaFields(iformula.Key) Is Nothing Then
                            repClassVal.DataDefinition.FormulaFields(iformula.Key).text = iformula.Value
                        End If
                    End While
                End If
            End If
        Catch ex As Exception
        End Try
        MyReportViewer.DisplayGroupTree = repSource.DisplayGroupTree
        Try
            Dim btnClick = PrinterFunctions.GetPostBackControl(Me.Page)
            If btnClick IsNot Nothing Then
                Dim btnClickstatus As String = CObj(btnClick).COMMANDNAME
                If btnClickstatus = "Export" Or btnClickstatus = "Print" Then
                Else
                    Session("PrintClicked") = False
                End If
            End If
        Catch ex As Exception
            Session("PrintClicked") = False
        End Try
        repSource = Nothing
    End Sub
    Private Sub SubReport(ByVal subRep As ReportClass, ByRef repClassVal As ReportClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass '= subRep.Subreports.Item(subRep.ResourceName)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(subRep, repClassVal)
                    Else
                        Dim SubRepData As New DataTable
                        Try
                            SubRepData = subRep.ReportData
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message, "Report Viewer(SUB_FIN)")
                            Exit Sub
                        Finally
                        End Try
                        ViewState("reportHeader") = repClassVal.Subreports(i + ii).Name
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub IncludeBSUmage(ByRef repClassVal As ReportClass, Optional ByVal BSUID As String = "")
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = Session("sbSUID")
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If
        cmd.Parameters.Add(sqlpIMG_BSU_ID)
        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)
        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports(repClassVal.HeaderReportName) Is Nothing Then
            repClassVal.Subreports(repClassVal.HeaderReportName).SetDataSource(ds.Tables(0))
        End If
    End Sub
    Public Function GetPrintButton(ByVal p_Controls As Control) As System.Web.UI.Control
        Dim ctrlStr As String = [String].Empty
        For Each ctl As Control In p_Controls.Controls
            If ctl.HasControls() Then
                Dim PrintControl As New Control
                PrintControl = GetPrintButton(ctl)
                If Not PrintControl Is Nothing Then
                    Return PrintControl
                End If
            End If
            ' handle ImageButton controls 
            If ctl.ToString.EndsWith(".x") OrElse ctl.ToString.EndsWith(".y") Then
                ctrlStr = ctl.ToString.Substring(0, ctl.ToString.Length - 2)
            End If
            If TypeOf ctl Is System.Web.UI.WebControls.ImageButton Then
                Response.Write(ctl.ID)
                Dim btn As New System.Web.UI.WebControls.ImageButton
                btn = ctl
                If btn.CommandName = "Print" Then
                    Return ctl
                End If
            End If
        Next
        Return Nothing
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        MyReportViewer.Dispose()
        MyReportViewer = Nothing
        repClassVal.Close()
        repClassVal.Dispose()
        'GC.Collect()
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Dim smScriptManager As New ScriptManager
            smScriptManager = Master.FindControl("ScriptManager1")
            smScriptManager.EnablePartialRendering = False
        Catch ex As Exception

        End Try

        'If Session("sModule") = "SS" Then
        '    Me.MasterPageFile = "../../mainMasterPageSS.master"
        'Else
        '    Me.MasterPageFile = "../../mainMasterPage.master"
        'End If
    End Sub
End Class
