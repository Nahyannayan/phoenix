<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaymentHistory.aspx.vb" Inherits="Common_PaymentHistory" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">

        function printTable() {
            var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            disp_setting += "scrollbars=yes,width=1280, height=600, left=100, top=25";
            var content_vlue = document.getElementById('PrintTable').innerHTML;
            var docprint = window.open("", "", disp_setting);
            docprint.document.open();
            var HeaderRow = '<table width="100%" ><tr><td class="title" align="center" >';
            HeaderRow = HeaderRow + 'Payment History </td></tr></table>';
            docprint.document.write('<html><head>');
            docprint.document.write('<link href="../cssfiles/sb-admin.css" rel="stylesheet" type="text/css" />');
            docprint.document.write('<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
            docprint.document.write('</head><body onLoad="self.print();self.close();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');
            docprint.document.write(HeaderRow);
            docprint.document.write(content_vlue);
            docprint.document.write('</center></body></html>');
            docprint.document.close();
            docprint.focus();
        }
    </script>
</head>
<body class="bg-dark bg-white m-3">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>Payment History
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <form id="form1" runat="server">
                    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
                    </ajaxToolkit:ToolkitScriptManager>
                    <br />
                    <table width="100%" align="center">
                        <tr id="Tr1" runat="server">
                            <td align="center" valign="top" width="20%"><span class="field-label">Date</span></td>
                            <td width="30%">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox"></asp:TextBox>
                                <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDate"
                                    ErrorMessage="From Date required" ValidationGroup="MAINERROR">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                        ID="revFromdate" runat="server" ControlToValidate="txtFromDate" Display="Dynamic"
                                        ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                        ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                            <td width="20%">

                                <span class="field-label">To Date</span>  </td>
                            <td width="30%">
                                <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox">
                                </asp:TextBox>
                                <asp:ImageButton ID="ImgTo" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTodate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator
                                    ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTodate" Display="Dynamic"
                                    ErrorMessage="Enter the From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc])/(19|20)\d\d$"
                                    ValidationGroup="MAINERROR">*</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">

                                <asp:Button ID="btnFind"
                                    runat="server" CssClass="button" Text="Find.." />
                                <asp:Button ID="btnCLOSE" OnClientClick="$.fancybox.close();" runat="server" Text="Close" CssClass="button" Visible="false" />
                                <a onclick='printTable()' id="a_print" style="vertical-align: middle; text-decoration: underline; cursor: hand" runat="server">
                                    <img align="absmiddle" src="../Images/Misc/print_popup.gif" start="" />
                                </a>

                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%" align="center" cellpadding="0" cellspacing="0" id="PrintTable">


                        <tr valign="top">
                            <td align="center">


                                <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-row table-bordered" Width="100%" EmptyDataText="No Data Found" AutoGenerateColumns="False" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:BoundField DataField="Fee" HeaderText="Fee Head" />
                                        <asp:TemplateField HeaderText="Receipt No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="linkRecptNo" runat="server" OnClick="linkRecptNo_Click" Text='<%# Bind("ReceiptNo") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" HeaderText="Date" />
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                        <asp:BoundField DataField="Employee" HeaderText="Employee" />
                                        <asp:BoundField DataField="Status" HeaderText="Deleted" />
                                    </Columns>
                                </asp:GridView>


                            </td>
                        </tr>
                        <tr id="TrDetails" runat="server" visible="false">
                            <td>
                                <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <th align="left" valign="middle" style="height: 15px" colspan="2">Header Details</th>
                                    </tr>
                                    <tr valign="top">
                                        <td align="center">
                                            <asp:GridView ID="gvHead" runat="server"
                                                EmptyDataText="No Cash Payment Vouchers found" Width="100%" CssClass="table table-row table-bordered" ShowHeader="False" PageSize="15" AllowPaging="True">
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="center" valign="middle" style="height: 15px" colspan="2">Fee Details</th>
                                    </tr>
                                    <tr valign="top">
                                        <td align="center">
                                            <asp:GridView ID="gvDetails" runat="server"
                                                EmptyDataText="No Cash Payment Vouchers found" Width="100%" CssClass="table table-row table-bordered" ShowHeader="False" ShowFooter="True">
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="center" valign="middle" style="height: 15px" colspan="2">Payment Details</th>
                                    </tr>
                                    <tr valign="top">
                                        <td align="center">
                                            <asp:GridView ID="gvPayment" runat="server"
                                                EmptyDataText="No Cash Payment Vouchers found" Width="100%" CssClass="table table-row table-bordered" AutoGenerateColumns="False" ShowFooter="True" PageSize="15" AllowPaging="True">
                                                <Columns>
                                                    <asp:BoundField DataField="CLT_DESCR" HeaderText="Payment Mode" />
                                                    <asp:BoundField DataField="CRI_DESCR" HeaderText="Bank / Cr.Card" />
                                                    <asp:BoundField DataField="FCD_REFNO" HeaderText="ChqNo / Auth.Code" />
                                                    <asp:BoundField DataField="FCD_DATE" HeaderText="Chque Date" />
                                                    <asp:BoundField DataField="FCD_AMOUNT" HeaderText="Amount (AED)" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:TextBox ID="txtFocus" runat="server" Style="height: 1px !important; width: 1px !important; border: none !important;"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                        TargetControlID="txtFromDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgTo"
                        TargetControlID="txtTodate">
                    </ajaxToolkit:CalendarExtender>
                </form>

            </div>
        </div>
    </div>

</body>
</html>

