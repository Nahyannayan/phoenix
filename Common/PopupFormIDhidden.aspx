<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupFormIDhidden.aspx.vb" Inherits="Common_PopupFormIDhidden" %>

<%@ OutputCache Duration="1" VaryByParam="none" Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Select</title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
      
        function SetValuetoParent(id, ctrlid) {
            //alert(id);
            parent.setBankValue(id, ctrlid);
            return false;
        }
        

        function SetValuetoConcessionParent(result) {
            //alert(result);
            parent.setConcessionValue(result);
            return false;
        }

        function SetValuetoReferenceParent(result, REF_TYPE) {
            //alert(result);
            //alert(REF_TYPE);
            parent.setReferenceValue(result, REF_TYPE);
            return false;
        }

         </script>

     <!-- Bootstrap core JavaScript-->
     <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js" type="text/javascript"></script>
     <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
     <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
              
        
    <!-- Bootstrap core CSS-->
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
<%--        <link href="/cssfiles/sb-admin.css" rel="stylesheet" >--%>
        <link href="/PHOENIXBETA/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/PHOENIXBETA/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="/cssfiles/all-ie-only.css">
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
            <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
               <input id="hf_ctrlid" runat="server" type="hidden" value="=" />
         <input id="hf_bankid" runat="server" type="hidden" value="=" />
         
    <table width="100%" align="center">
                    <tr valign="top">
                        <td >
                        <asp:GridView ID="gvGroup" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                EmptyDataText="No Data" Width="100%" AllowPaging="True" PageSize="16" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Descr1">
                                        <EditItemTemplate> 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Descr1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server"></asp:Label><br />
                                            <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click"/>
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descr1">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblName" runat="server"></asp:Label><br />
                                            <asp:TextBox ID="txtBSUName" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click"/>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--OnClick="linklblBSUName_Click"--%>
                                           <asp:LinkButton ID="lnkDESCR" runat="server" OnClick="linklblBSUName_Click" Text='<%# Bind("Descr2") %>' ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td> 
                    </tr> 
                </table>
           
    </form>
                      

</body>
</html>