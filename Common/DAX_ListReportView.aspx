<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="DAX_ListReportView.aspx.vb" Inherits="Common_DAX_ListReportView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">

        Sys.Application.add_load(
function CheckForPrint() {
    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
        document.getElementById('<%= h_print.ClientID %>').value = '';
        if (document.getElementById('<%=hPrintOnLandscape.ClientID %>').value == "1") {
            var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
            return false;
        }
        else {
            var result = radopen('../Reports/ASPX Report/RptViewerModal.aspx', 'pop_up');
            return false;
        }

    }
}
    );

function ajaxToolkit_CalendarExtenderClientShowing(e) {
    if (!e.get_selectedDate() || !e.get_element().value)
        e._selectedDate = (new Date()).getDateOnly();
}

function ChangeAllCheckBoxStates(checkState) {
    var chk_state = document.getElementById("chkAL").checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0) {
            if (document.forms[0].elements[i].name != 'ctl00$cphMasterpage$chkSelectAll') {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
    }
}

function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;
    var height = body.scrollHeight;
    var width = body.scrollWidth;
    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;
    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}

    </script>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager> 

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-calculator  mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td valign="bottom" width="100%" align="right">
                            <asp:Label ID="lblBSUDD" runat="server" Text="Business Unit" Visible="false"></asp:Label>
                            <telerik:RadComboBox ID="ddlBSU" runat="server" EmptyMessage="Type here to search..."
                                EnableScreenBoundaryDetection="False" Width="455px" CssClass="cellMatters" AutoPostBack="True" Visible="false">
                            </telerik:RadComboBox>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                                <tr valign="top">
                                    <td valign="bottom" width="10%" align="left">
                                        <asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                                    </td>
                                    <td align="right">
                                        <table style="margin-left: 0px; height: 1px;" width="0px">
                                            <tr>
                                                <td width="0" height="0px" colspan="4">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblDDDescr1" runat="server" Text="" Visible="false" CssClass="field-label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="DropDown1" runat="server" AutoPostBack="True"
                                                                    TabIndex="5" SkinID="DropDownListNormal" Visible="false">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblDDDescr2" runat="server" Text="" Visible="false" CssClass="field-label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="DropDown2" runat="server" AutoPostBack="True"
                                                                    TabIndex="5" SkinID="DropDownListNormal" Visible="false">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="0" height="0px">
                                                    <asp:CheckBox ID="chkSelection1" runat="server" AutoPostBack="True" Visible="False" />
                                                </td>
                                                <td width="0" height="0px">
                                                    <asp:CheckBox ID="chkSelection2" runat="server" AutoPostBack="True" />
                                                </td>
                                                <td height="0px">&nbsp;
                                                </td>
                                                <td width="0" height="0px">
                                                    <asp:RadioButton ID="rad1" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rad2" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                                </td>
                                                <td width="0">
                                                    <asp:RadioButton ID="rad3" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                                </td>
                                                <td width="0">
                                                    <asp:RadioButton ID="rad4" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                                </td>
                                                <td width="0">
                                                    <asp:RadioButton ID="rad5" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                                </td>
                                                <td width="0">
                                                    <asp:RadioButton ID="rad6" runat="server" GroupName="Rad" AutoPostBack="True" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td runat="server" id="dateCol" align="right">
                                        <%--  <asp:Label ID="lblDate" runat="server" Text="Expiry As on Date:"></asp:Label>
                <asp:TextBox ID="txtDate" runat="server" Width="90px"  AutoPostBack="True"></asp:TextBox>
                <asp:ImageButton ID="lnkDate" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"
                    Width="16px"></asp:ImageButton>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MMM/yyyy" runat="server"
                    TargetControlID="txtDate" PopupButtonID="lnkDate" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                </ajaxToolkit:CalendarExtender>--%>

                                        <table id="trDateRange" runat="server" visible="False">
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblFromDT" runat="server" Text="From" CssClass="field-label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFromDT" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="imgFromDT" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"
                                                                    Width="16px"></asp:ImageButton>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MMM/yyyy" runat="server"
                                                                    TargetControlID="txtFromDT" PopupButtonID="imgFromDT" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblToDt" runat="server" Text="To" CssClass="field-label"></asp:Label>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtToDt" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="imgDTTo" runat="server" ImageUrl="~/Images/calendar.gif" Style="cursor: hand"></asp:ImageButton>
                                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" Format="dd/MMM/yyyy" runat="server"
                                                                    TargetControlID="txtToDt" PopupButtonID="imgDTTo" OnClientShowing="ajaxToolkit_CalendarExtenderClientShowing">
                                                                </ajaxToolkit:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                                <td>
                                                    <asp:Button ID="btnGO" runat="server" CssClass="button" CausesValidation="False"
                                                        Visible="true" Text="GO"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <th align="right" valign="middle" width="70%">

                            <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="True" DataSourceID="namesXML"
                                Visible="false" DataTextField="FilterText" DataValueField="FilterValue" SkinID="DropDownListNormal">
                            </asp:DropDownList>
                            <asp:XmlDataSource ID="namesXML" runat="server" DataFile="~/XMLData/XMLTopFilter.xml"
                                XPath="FilterElements/FiletrList"></asp:XmlDataSource>
                        </th>
                        <th align="right" valign="middle" style="text-align: right" width="30%">
                            <asp:LinkButton ID="btnPrint" runat="server" Visible="False">Print</asp:LinkButton>
                            &nbsp;
                <asp:HyperLink ID="btnExport" runat="server">Export</asp:HyperLink>
                        </th>
                    </tr>
                    <tr>
                        <td align="center" valign="top" width="100%" colspan="2">
                            <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="100%"
                                CssClass="table table-bordered table-row" AllowPaging="True" PageSize="20">
                                <Columns>
                                    <asp:TemplateField HeaderText=" ">
                                        <HeaderTemplate>
                                            <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                                value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="chkControl" runat="server" type="checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col1" SortExpression="Col1">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol1" runat="server" Text='<%# Bind("Col1") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol1" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col1"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol1" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col2" SortExpression="Col2">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol2" runat="server" Text='<%# Bind("Col2") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol2" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col2"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol2" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col3" SortExpression="Col3">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol3" runat="server" Text='<%# Bind("Col3") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol3" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col3"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col4" SortExpression="Col4">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol4" runat="server" Text='<%# Bind("Col4") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol4" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col4"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol4" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col5" SortExpression="Col5">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol5" runat="server" Text='<%# Bind("Col5") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol5" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col5"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol5" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col6" SortExpression="Col6">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol6" runat="server" Text='<%# Bind("Col6") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol6" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col6"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol6" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch6" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col7" SortExpression="Col7">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol7" runat="server" Text='<%# Bind("Col7") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol7" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col7"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol7" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch7" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col8" SortExpression="Col8">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol8" runat="server" Text='<%# Bind("Col8") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol8" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col8"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol8" runat="server" Width="100%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch8" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col9" SortExpression="Col9">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol9" runat="server" Text='<%# Bind("Col9") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol9" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col9"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol9" runat="server" Width="100%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch9" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Col10" SortExpression="Col10">
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCol10" runat="server" Text='<%# Bind("Col10") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCol10" runat="server" CssClass="gridheader_text" EnableViewState="True"
                                                Text="Col10"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCol10" runat="server" Width="100%"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch10" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlView" runat="server">View</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="hlPrint" OnClick="lblPrint_Click" runat="server">Print</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <table style="width: 100%;">
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All"
                                            Visible="False" />
                                    </td>
                                    <td align="center" width="60%">
                                        <input type="button" id="ListButton1" class="button" style="color: White; background-color: #1B80B6; border-style: Solid; font-family: Verdana; font-weight: bold;"
                                            runat="server"
                                            onclick="this.disabled = true;" onserverclick="ListButton1_Click" />
                                        <asp:Button ID="ListButton2" runat="server" CssClass="button" CausesValidation="False"
                                            Width="142px" OnClick="ListButton2_Click" Visible="False"></asp:Button>
                                    </td>
                                    <td width="20%">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%" colspan="2" style="width: 80%">
                                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="false"></asp:Label>
                                    </td>
                                    <td width="20%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_print" runat="server" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_8" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_9" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_10" runat="server" type="hidden" value="=" />
                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                <input id="hPrintOnLandscape" runat="server" type="hidden" value="0" />

            </div>
        </div>
    </div>
</asp:Content>
