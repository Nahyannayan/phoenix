Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Data.OleDb
Imports Telerik.Web
Imports Telerik.Web.UI
Imports System.Xml.Linq

Partial Class GenericReportViewer
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property SPName() As String
        Get
            Return ViewState("SPName")
        End Get
        Set(ByVal value As String)
            ViewState("SPName") = value
        End Set
    End Property
    Private Property DBName() As String
        Get
            Return ViewState("DBName")
        End Get
        Set(ByVal value As String)
            ViewState("DBName") = value
        End Set
    End Property
    Private Property GroupBy() As String
        Get
            Return ViewState("GroupBy")
        End Get
        Set(ByVal value As String)
            ViewState("GroupBy") = value
        End Set
    End Property
    Private Property RESULTTYPE_ITEMS() As String
        Get
            Return ViewState("RESULTTYPE_ITEMS")
        End Get
        Set(ByVal value As String)
            ViewState("RESULTTYPE_ITEMS") = value
        End Set
    End Property
    Private Property MainMnu_code() As String
        Get
            Return ViewState("MainMnu_code")
        End Get
        Set(ByVal value As String)
            ViewState("MainMnu_code") = value
        End Set
    End Property

    Private Property DataLoaded() As Boolean
        Get
            Return ViewState("DataLoaded")
        End Get
        Set(ByVal value As Boolean)
            ViewState("DataLoaded") = value
        End Set
    End Property
    Private Property ShowTitleOnExport() As Boolean
        Get
            Return ViewState("ShowTitleOnExport")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowTitleOnExport") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(lnkExportToExcel)
        smScriptManager.RegisterPostBackControl(lnkExportToPDF)
        If Not IsPostBack Then
            Session("GridData2") = ""
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                'disable the control based on the rights 
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            lblReportCaption.Text = Mainclass.GetMenuCaption(MainMnu_code)
            h_Sel_Bsu_Id.Value = Session("sBsuid")
            FillReportType()
            Me.rblFilter.SelectedValue = 0
            txtDate.SelectedDate = Format(Now.Date, OASISConstants.DateFormat)
            txtFromDate.SelectedDate = Format(Now.Date.AddMonths(-1), OASISConstants.DateFormat)
            txtToDate.SelectedDate = Format(Now.Date, OASISConstants.DateFormat)
            HideFilterRows()
            CheckAndSetIfExclusive(MainMnu_code)
        End If
        Dim sm As ScriptManager = Master.FindControl("ScriptManager1")
        sm.RegisterPostBackControl(btnView)

    End Sub
    Private Sub CheckAndSetIfExclusive(MenuCode As String)
        Dim sql As String
        Dim ds As New DataTable
        sql = "Select * from GENERIC_REPORTS_LISTS where ISNULL(GRL_MNU_CODE,'')='" & MenuCode & "'"
        ds = Mainclass.getDataTable(sql, ConnectionManger.GetOASISConnectionString)
        If ds.Rows.Count = 1 Then
            radReportType.SelectedValue = ds.Rows(0).Item("GRL_ID")
            lblReportCaption.Text = ds.Rows(0).Item("GRL_TITLE")
            SetReportParameters(ds.Rows(0).Item("GRL_ID"))
            ' trReportType.Style.Add("display", "none")
            trReportType.Visible = False

        End If


    End Sub
    Sub FillACD()
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
        ddlAcademicYear.DataSource = dtACD
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        For Each rowACD As DataRow In dtACD.Rows
            If rowACD("ACD_CURRENT") Then
                ddlAcademicYear.Items.FindItemByValue(rowACD("ACD_ID")).Selected = True
                Exit For
            End If
        Next
    End Sub
    Sub FillLeaveType(ByVal TREEVIEW As Boolean)
        Dim sql As String
        Dim dtTree As New DataTable
        sql = "SP_GET_LEAVE_TYPES_FOR_GEN_REPORTS"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", TREEVIEW, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)

        If TREEVIEW Then
            RadTreeLeaveTypes.Nodes.Clear()
            PopulateTree(RadTreeLeaveTypes, dtTree, "0")
        Else
            ddlLeaveType.DataSource = dtTree
            ddlLeaveType.DataTextField = "NAME"
            ddlLeaveType.DataValueField = "ID"
            ddlLeaveType.DataBind()
            Dim LstItem As New RadComboBoxItem
            LstItem.Value = "0"
            LstItem.Text = "-----All Leave Type-----"
            ddlLeaveType.Items.Insert(0, LstItem)
        End If

    End Sub
    Sub FillEmpCategory(ByVal TREEVIEW As Boolean)
        Dim sql As String
        Dim dtTree As New DataTable
        sql = "SP_GET_EMPCATEGORY_FOR_GEN_REPORTS"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", TREEVIEW, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)

        If TREEVIEW Then
            RadTreeEmpCategory.Nodes.Clear()
            PopulateTree(RadTreeEmpCategory, dtTree, "0")
        End If

    End Sub
    Sub FillEmpABC(ByVal TREEVIEW As Boolean)
        Dim sql As String
        Dim dtTree As New DataTable
        sql = "SP_GET_EMPABC_FOR_GEN_REPORTS"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", TREEVIEW, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)

        If TREEVIEW Then
            RadTreeABC.Nodes.Clear()
            PopulateTree(RadTreeABC, dtTree, "0")
        End If

    End Sub
    Sub FillBSU(ByVal TREEVIEW As Boolean)
        Dim sql As String
        Dim dtTree As New DataTable
        sql = "SP_GET_BSU_FOR_GEN_REPORTS"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", TREEVIEW, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)

        If TREEVIEW Then
            RadBSUTreeView.Nodes.Clear()
            PopulateTree(RadBSUTreeView, dtTree, "0")

        Else
            ddlBSU.DataSource = dtTree
            ddlBSU.DataTextField = "NAME"
            ddlBSU.DataValueField = "ID"
            ddlBSU.DataBind()
            If ddlBSU.Items.Count > 0 Then
                ddlBSU.SelectedValue = Session("sBsuid")
                h_Sel_Bsu_Id.Value = ddlBSU.SelectedValue
            End If
        End If

    End Sub
    Sub FillAXSubTypes(ByVal TREEVIEW As Boolean)
        Dim sql As String
        Dim dtTree As New DataTable
        Dim strConn As String
        strConn = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        sql = "SP_GET_AX_SUBTYPES_FOR_GEN_REPORTS"
        Dim sqlParam(4) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", TREEVIEW, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, strConn)

        If TREEVIEW Then
            RadTreeAXSubTypes.Nodes.Clear()
            PopulateTree(RadTreeAXSubTypes, dtTree, "0")
        Else
            ddlAXSubType.DataSource = dtTree
            ddlAXSubType.DataTextField = "NAME"
            ddlAXSubType.DataValueField = "ID"
            ddlAXSubType.DataBind()
        End If

    End Sub
    Protected Sub PopulateTree(ByVal RadBSUTreeView As RadTreeView, ByRef dtTree As DataTable, ByVal PARENTID As String)

        Dim mRow As DataRow
        For Each mRow In dtTree.Select("PARENTID='" & PARENTID & "'")
            Dim root As New RadTreeNode(mRow(1).ToString(), mRow(0).ToString())
            RadBSUTreeView.Nodes.Add(root)
            CreateNode(root, dtTree, dtTree.Select("PARENTID='" & PARENTID & "'"), mRow(0).ToString())
        Next
    End Sub
    Private Function CreateNode(ByVal node As RadTreeNode, ByVal dtTree As DataTable, ByVal dtSubTree() As DataRow, ByVal ParentID As String) As Int16
        If dtSubTree.Length = 0 Then
            Return 0
        End If
        Dim mRow As DataRow
        For Each mRow In dtSubTree
            Dim tnode As New RadTreeNode(mRow(1).ToString(), mRow(0).ToString())
            If mRow(2).ToString() <> "0" Then
                If tnode.Value = Session("sBsuid") Then
                    tnode.Checked = True
                End If
                node.Nodes.Add(tnode)
            Else
                tnode = node
            End If
            If dtTree.Select("PARENTID='" & mRow(0).ToString() & "'").Length > 0 Then
                CreateNode(tnode, dtTree, dtTree.Select("PARENTID='" & mRow(0).ToString() & "'"), mRow(0).ToString())
            End If
        Next
        Return 1
    End Function
    Public Function GetSelectedNode(ByVal RadTree As RadTreeView, Optional ByVal seperator As String = "||", Optional ByRef IsAllChecked As Boolean = False) As String
        Dim strBSUnits As New StringBuilder
        IsAllChecked = True
        For Each node As RadTreeNode In RadTree.GetAllNodes
            If Not node.Checked Then
                IsAllChecked = False
                Exit For
            End If
        Next

        For Each node As RadTreeNode In RadTree.CheckedNodes
            'If node.Value.Length > 2 Then
            strBSUnits.Append(node.Value)
            strBSUnits.Append(seperator)
            ' End If
        Next
        Return strBSUnits.ToString()
    End Function
    Protected Sub ddlBSU_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBSU.SelectedIndexChanged
        h_Sel_Bsu_Id.Value = ddlBSU.SelectedItem.Value
    End Sub

    Sub FillFeeType()
        Dim sql As String
        Dim ds As New DataTable
        sql = "SP_GET_FEE_TYPE_FOR_GEN_REPORTS"
        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@ACD_ID", 0, SqlDbType.Int)
        ds = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)
        ddlFeeType.DataSource = ds
        ddlFeeType.DataTextField = "FEE_DESCR"
        ddlFeeType.DataValueField = "FEE_ID"
        ddlFeeType.DataBind()
        Dim LstItem As New RadComboBoxItem
        LstItem.Value = "0"
        LstItem.Text = "-----All Fee Type-----"
        ddlFeeType.Items.Insert(0, LstItem)

    End Sub
    Sub FillServices()
        Dim sql As String
        Dim ds As New DataTable
        sql = "SP_GET_SERVICE_FOR_GEN_REPORTS"
        Dim sqlParam(1) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@ACD_ID", 0, SqlDbType.Int)
        ds = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)
        ddlService.DataSource = ds
        ddlService.DataTextField = "SVC_DESCRIPTION"
        ddlService.DataValueField = "SVC_ID"
        ddlService.DataBind()
        Dim LstItem As New RadComboBoxItem
        LstItem.Value = "0"
        LstItem.Text = "-----All Service-----"
        ddlService.Items.Insert(0, LstItem)
    End Sub

    'Protected Sub ddlAcdY_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcdY.SelectedIndexChanged
    '    FillFeeType()
    'End Sub


#Region "Reports"

    Sub FillReportType()
        Dim sql As String
        Dim ds As New DataTable
        sql = "SP_GET_GENERIC_REPORTS_LIST"
        Dim sqlParam(3) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMnu_code, SqlDbType.VarChar)
        ds = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)
        radReportType.DataSource = ds
        radReportType.DataTextField = "GRL_TITLE"
        radReportType.DataValueField = "GRL_ID"
        radReportType.DataBind()

        Dim LstItem As New RadComboBoxItem
        LstItem.Value = "0"
        LstItem.Text = "-----Select Report Type-----"
        radReportType.Items.Insert(0, LstItem)
    End Sub
    Private Sub FillReportData(ByVal bind As Boolean)
        Dim SubscribeParam As Hashtable = New Hashtable()
        Try
            Dim ConnectionString As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim strSQL As String
            Dim ParamList As DataTable, ParamRow As DataRow

            If SPName = "" Then Exit Sub
            strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from  " & DBName & ".information_schema.parameters where PARAMETER_MODE='IN' AND specific_schema + '.' + specific_name = '" & SPName & "'"
            ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
            Dim cmd As New SqlCommand
            cmd.Dispose()
            cmd = New SqlCommand(DBName & "." & SPName, ConnectionManger.GetOASISConnection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim ParamName As String
            If ParamList IsNot Nothing Then
                For Each ParamRow In ParamList.Rows
                    ParamName = UCase(ParamRow("parameter_name"))
                    Select Case ParamName
                        Case "@BSU_IDS"
                            If trBSUIDs.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, GetSelectedNode(RadBSUTreeView, "|"), SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, GetSelectedNode(RadBSUTreeView, "|"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@BSU_ID"
                            If trBSUID.Style("display") = "table" Then
                                If ddlBSU.SelectedItem IsNot Nothing Then 'Added by vikranth on 18th Aug 2019
                                    Dim param0 As New SqlParameter
                                    param0 = Mainclass.CreateSqlParameter(ParamName, ddlBSU.SelectedItem.Value, SqlDbType.VarChar)
                                    SubscribeParam.Add(ParamName, ddlBSU.SelectedItem.Value)
                                    cmd.Parameters.Add(param0)
                                End If
                            End If
                        Case "@ACD_ID"
                            If trAcademicYear.Visible = True Then
                                If ddlAcademicYear.SelectedItem IsNot Nothing Then 'Added by vikranth on 18th Aug 2019
                                    Dim param0 As New SqlParameter
                                    param0 = Mainclass.CreateSqlParameter(ParamName, ddlAcademicYear.SelectedItem.Value, SqlDbType.Int)
                                    SubscribeParam.Add(ParamName, ddlAcademicYear.SelectedItem.Value)
                                    cmd.Parameters.Add(param0)
                                End If
                            End If
                        Case "@SVC_ID"
                            If trService.Style("display") = "table" Then
                                If ddlService.SelectedItem IsNot Nothing Then 'Added by vikranth on 18th Aug 2019
                                    Dim param0 As New SqlParameter
                                    param0 = Mainclass.CreateSqlParameter(ParamName, ddlService.SelectedItem.Value, SqlDbType.Int)
                                    SubscribeParam.Add(ParamName, ddlService.SelectedItem.Value)
                                    cmd.Parameters.Add(param0)
                                End If
                            End If
                        Case "@FEE_ID"
                            If trFeeType.Style("display") = "table" Then
                                If ddlFeeType.SelectedItem IsNot Nothing Then 'Added by vikranth on 18th Aug 2019
                                    Dim param0 As New SqlParameter
                                    param0 = Mainclass.CreateSqlParameter(ParamName, ddlFeeType.SelectedItem.Value, SqlDbType.Int)
                                    SubscribeParam.Add(ParamName, ddlFeeType.SelectedItem.Value)
                                    cmd.Parameters.Add(param0)
                                End If
                            End If
                        Case "@STU_ID"
                            If trStudents.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Val(h_STUD_ID.Value), SqlDbType.BigInt)
                                SubscribeParam.Add(ParamName, Val(h_STUD_ID.Value))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@STU_TYPE"
                            If trStudents.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, IIf(radStud.Checked, "S", "E"), SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, IIf(radStud.Checked, "S", "E"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@USR_ID"
                            If trUserID.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, txtUserID.Text, SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, txtUserID.Text)
                                cmd.Parameters.Add(param0)
                            End If

                        Case "@ELT_IDS"
                            If trLeaveTypes.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, GetSelectedNode(RadTreeLeaveTypes, "|"), SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, GetSelectedNode(RadTreeLeaveTypes, "|"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@ELT_ID"
                            If trLeaveType.Style("display") = "table" Then
                                If ddlLeaveType.SelectedItem IsNot Nothing Then 'Added by vikranth on 18th Aug 2019
                                    Dim param0 As New SqlParameter
                                    If ddlLeaveType.SelectedItem.Value = "0" Then 'Added by vikranth on 18th Aug 2019
                                        param0 = Mainclass.CreateSqlParameter(ParamName, "", SqlDbType.VarChar) 'Added by vikranth on 18th Aug 2019
                                        SubscribeParam.Add(ParamName, "") 'Added by vikranth on 18th Aug 2019
                                    Else 'Added by vikranth on 18th Aug 2019
                                        param0 = Mainclass.CreateSqlParameter(ParamName, ddlLeaveType.SelectedItem.Value, SqlDbType.VarChar)
                                        SubscribeParam.Add(ParamName, ddlLeaveType.SelectedItem.Value)
                                    End If
                                    cmd.Parameters.Add(param0)
                                End If
                            End If
                        Case "@ECT_IDS"
                            If trEmpCategory.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, GetSelectedNode(RadTreeEmpCategory, "|"), SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, GetSelectedNode(RadTreeEmpCategory, "|"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@EMP_ABC"
                            If trEMPABC.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, GetSelectedNode(RadTreeABC, "|"), SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, GetSelectedNode(RadTreeABC, "|"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@EMP_ID"
                            If trEmployee.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, h_Emp_ID.Value, SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, h_Emp_ID.Value)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@ASONDT"
                            If trAsOnDate.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, CDate(txtDate.SelectedDate).ToString("dd/MMM/yyyy"), SqlDbType.DateTime)
                                SubscribeParam.Add(ParamName, CDate(txtDate.SelectedDate).ToString("dd/MMM/yyyy"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@FROMDT"
                            If trDateRange.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, CDate(txtFromDate.SelectedDate).ToString("dd/MMM/yyyy"), SqlDbType.DateTime)
                                SubscribeParam.Add(ParamName, CDate(txtFromDate.SelectedDate).ToString("dd/MMM/yyyy"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@TODT"
                            If trDateRange.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, CDate(txtToDate.SelectedDate).ToString("dd/MMM/yyyy"), SqlDbType.DateTime)
                                SubscribeParam.Add(ParamName, CDate(txtToDate.SelectedDate).ToString("dd/MMM/yyyy"))
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@RESULTTYPE"
                            ' If trRESULTTYPE.Style("display") = "block" Then
                            If trRESULTTYPE.Visible = True Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, Me.rblFilter.SelectedValue, SqlDbType.TinyInt)
                                SubscribeParam.Add(ParamName, Me.rblFilter.SelectedValue)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@SEARCHSTR"
                            If trSearchStr.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, txtSearchStr.Text, SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, txtSearchStr.Text)
                                cmd.Parameters.Add(param0)
                            End If
                        Case "@LOG_BSU_ID"
                            Dim param0 As New SqlParameter
                            param0 = Mainclass.CreateSqlParameter(ParamName, Session("sBsuid"), SqlDbType.VarChar)
                            SubscribeParam.Add(ParamName, Session("sBsuid"))
                            cmd.Parameters.Add(param0)
                        Case "@LOG_USER_NAME"
                            Dim param0 As New SqlParameter
                            param0 = Mainclass.CreateSqlParameter(ParamName, Session("sUsr_name"), SqlDbType.VarChar)
                            SubscribeParam.Add(ParamName, Session("sUsr_name"))
                            cmd.Parameters.Add(param0)
                        Case "@LOG_F_YEAR"
                            Dim param0 As New SqlParameter
                            param0 = Mainclass.CreateSqlParameter(ParamName, Session("F_YEAR"), SqlDbType.VarChar)
                            SubscribeParam.Add(ParamName, Session("F_YEAR"))
                            cmd.Parameters.Add(param0)
                        Case "@AX_SUB_TYPE"
                            If trAXSubType.Style("display") = "table" Then
                                If ddlAXSubType.SelectedItem IsNot Nothing Then 'Added by vikranth on 18th Aug 2019
                                    Dim param0 As New SqlParameter
                                    param0 = Mainclass.CreateSqlParameter(ParamName, ddlAXSubType.SelectedItem.Value, SqlDbType.VarChar)
                                    SubscribeParam.Add(ParamName, ddlAXSubType.SelectedItem.Value)
                                    cmd.Parameters.Add(param0)
                                End If
                            End If
                        Case "@AX_SUB_TYPES"
                            If trAXSubTypes.Style("display") = "table" Then
                                Dim param0 As New SqlParameter
                                param0 = Mainclass.CreateSqlParameter(ParamName, GetSelectedNode(RadTreeAXSubTypes, "|"), SqlDbType.VarChar)
                                SubscribeParam.Add(ParamName, GetSelectedNode(RadTreeAXSubTypes, "|"))
                                cmd.Parameters.Add(param0)
                            End If
                    End Select
                Next
            End If
            ViewState("SubscribeParam") = SubscribeParam
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.Fill(ds)
            Dim i As Int16
            If ds.Tables.Count > 0 Then
                Session("GridData") = ds.Tables(0)
                Session("GridData2") = ConvertDataTableToHTML(ds.Tables(0))
                RadGridReport.DataSource = ds.Tables(0)
                If bind Then RadGridReport.DataBind()
                DataLoaded = True
                If GroupBy <> "" Then
                    Dim ge As New GridGroupByExpression
                    Dim gf As New GridGroupByField
                    gf.FieldName = GroupBy
                    ge.GroupByFields.Add(gf)
                    ge.SelectFields.Add(gf)
                    RadGridReport.MasterTableView.GroupByExpressions.Add(ge)
                    If bind Then RadGridReport.DataBind()
                    For i = 0 To RadGridReport.MasterTableView.AutoGeneratedColumns.Length - 1
                        If RadGridReport.MasterTableView.AutoGeneratedColumns(i).UniqueName = GroupBy Then
                            RadGridReport.MasterTableView.AutoGeneratedColumns(i).Visible = False
                        End If
                    Next
                Else
                    For i = 0 To RadGridReport.MasterTableView.GroupByExpressions.Count - 1
                        RadGridReport.MasterTableView.GroupByExpressions.RemoveAt(i)
                    Next
                End If
                Dim col As GridColumn
                Try
                    For i = 1 To RadGridReport.MasterTableView.AutoGeneratedColumns.Length '- 1
                        col = RadGridReport.MasterTableView.AutoGeneratedColumns(i - 1)
                        If ds.Tables(0).Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or ds.Tables(0).Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or ds.Tables(0).Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Or ds.Tables(0).Columns(i - 1).DataType Is System.Type.GetType("System.Currency") Then
                            col.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                        Else
                            col.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                        End If
                    Next
                Catch ex As Exception

                End Try

            End If
            Exit Sub
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetStudentDetails(ByVal ProviderBSUID As String, ByVal STU_TYPE As String, ByVal STU_ID As String)
        Dim str_sql As String = String.Empty
        Dim ds As New DataTable
        Dim sqlParam(2) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@ProviderBsu_id", ProviderBSUID, SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@STU_TYPE", STU_TYPE, SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@STU_ID", STU_ID, SqlDbType.VarChar)
        ds = Mainclass.getDataTable("GetAllStudentDetails", sqlParam, ConnectionManger.GetOASIS_FEESConnectionString)
        If ds.Rows.Count > 0 Then
            txtStdNo.Text = ds.Rows(0).Item("STU_NO")
            txtStudentname.Text = ds.Rows(0).Item("STU_NAME")
        End If
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgStudent.Click
        SetStudentDetails(ddlBSU.SelectedItem.Value, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
    End Sub

    Protected Sub txtStdNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStdNo.TextChanged
        Try
            Dim str_data, str_sql As String
            txtStdNo.Text = txtStdNo.Text.Trim
            Dim iStdnolength As Integer = txtStdNo.Text.Length
            If radStud.Checked Then
                If iStdnolength < 9 Then
                    If txtStdNo.Text.Trim.Length < 8 Then
                        For i As Integer = iStdnolength + 1 To 8
                            txtStdNo.Text = "0" & txtStdNo.Text
                        Next
                    End If
                    txtStdNo.Text = ddlBSU.SelectedValue & txtStdNo.Text
                End If
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM VW_OSO_STUDENT_M" _
                 & " WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & ddlBSU.SelectedValue & "') AND  STU_NO='" & txtStdNo.Text & "'"
                str_data = Mainclass.getDataValue(str_sql, "OASIS_FEESConnectionString")
            Else
                str_sql = "SELECT RTRIM(STU_ID)+'|'+RTRIM(STU_NO)+'|'+STU_NAME FROM FEES.vw_OSO_ENQUIRY_COMP" _
                & " WHERE     (STU_BSU_ID = '" & ddlBSU.SelectedValue & "') AND  STU_NO='" & txtStdNo.Text & "'"

                str_data = Mainclass.getDataValue(str_sql, "OASIS_FEESConnectionString")
            End If
            If str_data <> "--" Then
                h_STUD_ID.Value = str_data.Split("|")(0)
                SetStudentDetails(ddlBSU.SelectedValue, IIf(radEnq.Checked, "E", "S"), h_STUD_ID.Value)
            Else
                h_STUD_ID.Value = "0"
                txtStudentname.Text = ""
                lblError.Text = "Student # Entered is not valid  !!!"
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            Dim customHTML As String
            customHTML = GetHeaderTitle_JS()
            hdn_HeaderTitle.Value = customHTML

            FillReportData(True)
            'Session("GridData2") = "<table>majo </table>"
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(Master.MenuName, radReportType.SelectedItem.Value, "GENERATE REPORT CLICK", Page.User.Identity.Name.ToString, Me.Page, "")
        Catch Ex As Exception
        End Try
    End Sub
    Private Sub HideFilterRows()
        trBSUIDs.Style.Add("display", "none")

        trAsOnDate.Style.Add("display", "none")
        'trRESULTTYPE.Style.Add("display", "none")
        trRESULTTYPE.Visible = False
        trDateRange.Style.Add("display", "none")
        trUserID.Style.Add("display", "none")
        trStudents.Style.Add("display", "none")
        trSearchStr.Style.Add("display", "none")
        trBSUID.Style.Add("display", "none")
        'trAcademicYear.Style.Add("display", "none")
        trAcademicYear.Visible = False
        trFeeType.Style.Add("display", "none")
        trService.Style.Add("display", "none")
        'trGenerateReport.Style.Add("display", "none")
        trGenerateReport.Visible = False
        trLeaveType.Style.Add("display", "none")
        trLeaveTypes.Style.Add("display", "none")
        trEmpCategory.Style.Add("display", "none")
        trEMPABC.Style.Add("display", "none")
        trEmployee.Style.Add("display", "none")
        trAXSubType.Style.Add("display", "none")
        trAXSubTypes.Style.Add("display", "none")

    End Sub
    Private Sub ClearFilters()
        h_STUD_ID.Value = ""
        h_Emp_ID.Value = ""
        txtStdNo.Text = ""
        txtStudentname.Text = ""
        txtEmpNo.Text = ""
        txtSearchStr.Text = ""
        chkSubscribe.Checked = False
    End Sub
    Private Sub SetFilters()
        Try
            Dim ConnectionString As String = ConnectionManger.GetOASISConnectionString
            Dim strSQL As String
            Dim ParamList As DataTable, ParamRow As DataRow
            HideFilterRows()
            If SPName = "" Then Exit Sub
            strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from " & DBName & ".information_schema.parameters where PARAMETER_MODE='IN' AND specific_schema + '.' + specific_name = '" & SPName & "'"
            ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
            If ParamList IsNot Nothing Then
                For Each ParamRow In ParamList.Rows
                    Select Case UCase(ParamRow("parameter_name"))
                        Case "@BSU_IDS"
                            trBSUIDs.Style.Add("display", "table")
                            FillBSU(True)
                        Case "@BSU_ID"
                            trBSUID.Style.Add("display", "table")
                            FillBSU(False)
                        Case "@USR_ID"
                            trUserID.Style.Add("display", "table")
                        Case "@ACD_ID"
                            ' trAcademicYear.Style.Add("display", "block")
                            trAcademicYear.Visible = True
                            FillACD()
                        Case "@SVC_ID"
                            trService.Style.Add("display", "table")
                            FillServices()
                        Case "@FEE_ID"
                            trFeeType.Style.Add("display", "table")
                            FillFeeType()
                        Case "@STU_ID"
                            trStudents.Style.Add("display", "table")

                        Case "@ELT_ID"
                            trLeaveType.Style.Add("display", "table")
                            FillLeaveType(False)
                        Case "@ELT_IDS"
                            trLeaveTypes.Style.Add("display", "table")
                            FillLeaveType(True)
                        Case "@EMP_ABC"
                            trEMPABC.Style.Add("display", "table")
                            FillEmpABC(True)
                        Case "@ECT_IDS"
                            trEmpCategory.Style.Add("display", "table")
                            FillEmpCategory(True)
                        Case "@EMP_ID"
                            trEmployee.Style.Add("display", "table")
                        Case "@ASONDT"
                            trAsOnDate.Style.Add("display", "table")
                        Case "@FROMDT", "@TODT"
                            trDateRange.Style.Add("display", "table")
                        Case "@RESULTTYPE"
                            'trRESULTTYPE.Style.Add("display", "block")
                            trRESULTTYPE.Visible = True
                            rblFilter.Items.Clear()
                            If RESULTTYPE_ITEMS <> "" Then
                                Dim rItems As String()
                                Dim itm As String()
                                Dim str As String
                                rItems = RESULTTYPE_ITEMS.Split(",")
                                For Each str In RESULTTYPE_ITEMS.Split(",")
                                    itm = str.Split("|")
                                    If itm.Length > 1 Then
                                        Dim lstItem As New ListItem
                                        lstItem.Value = itm(0)
                                        lstItem.Text = itm(1)
                                        rblFilter.Items.Add(lstItem)
                                    End If
                                Next
                                If rblFilter.Items.Count > 0 Then rblFilter.Items(0).Selected = True
                            End If
                        Case "@SEARCHSTR"
                            trSearchStr.Style.Add("display", "table")
                        Case "@AX_SUB_TYPE"
                            trAXSubType.Style.Add("display", "table")
                            FillAXSubTypes(False)
                        Case "@AX_SUB_TYPES"
                            trAXSubTypes.Style.Add("display", "table")
                            FillAXSubTypes(True)
                    End Select
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub SetReportParameters(ReportType As String)
        Try
            If ReportType = "0" Then
                SPName = ""
                GroupBy = ""
                DBName = ""
                lblReportDescription.Text = ""
            Else
                Dim sql As String
                Dim ds As New DataTable
                sql = "Select * from GENERIC_REPORTS_LISTS where GRL_ID='" & ReportType & "'"
                ds = Mainclass.getDataTable(sql, ConnectionManger.GetOASISConnectionString)
                If ds.Rows.Count > 0 Then
                    SPName = ds.Rows(0).Item("GRL_PROC_NAME").ToString
                    DBName = ds.Rows(0).Item("GRL_DB_NAME").ToString
                    GroupBy = ds.Rows(0).Item("GRL_GROUPBY_COLS").ToString
                    RESULTTYPE_ITEMS = ds.Rows(0).Item("GRL_FILTER_TYPE").ToString
                    lblReportDescription.Text = ds.Rows(0).Item("GRL_DESCR").ToString
                    ViewState("bSubscribe") = IIf(ds.Rows(0).Item("GRL_bEnableSubscribe").ToString = "", 0, ds.Rows(0).Item("GRL_bEnableSubscribe"))
                    ViewState("bOnlyExcel") = IIf(ds.Rows(0).Item("GRL_bOnlyExcel").ToString = "", 0, ds.Rows(0).Item("GRL_bOnlyExcel"))
                    If ds.Rows(0).Item("GRL_SEARCHSTR_TITLE").ToString <> "" Then
                        lblSearchStr.Text = ds.Rows(0).Item("GRL_SEARCHSTR_TITLE").ToString
                    Else
                        lblSearchStr.Text = "Search String :"
                    End If
                    If ds.Rows(0).Item("GRL_FilterByTitle").ToString <> "" Then
                        lblFilterByStr.Text = ds.Rows(0).Item("GRL_FilterByTitle").ToString
                    Else
                        lblFilterByStr.Text = "Filter By"
                    End If
                    ShowTitleOnExport = IIf(ds.Rows(0).Item("GRL_bShowTitleOnExport").ToString.ToLower = "true", True, False)
                End If

                Dim i As Int16
                For i = 0 To RadGridReport.MasterTableView.GroupByExpressions.Count - 1
                    RadGridReport.MasterTableView.GroupByExpressions.RemoveAt(i)
                Next
                Me.RadGridReport.AllowPaging = False
                Me.RadGridReport.DataSource = ""
                Me.RadGridReport.DataBind()
                Me.RadGridReport.AllowPaging = True
                SetFilters()
                If ReportType <> "0" Then
                    '  trGenerateReport.Style.Add("display", "block")
                    trGenerateReport.Visible = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub radReportType_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radReportType.SelectedIndexChanged
        Try
            Dim ReportType As String
            ClearFilters()
            If radReportType.SelectedItem IsNot Nothing Then
                ReportType = radReportType.SelectedItem.Value
                SetReportParameters(ReportType)
                If ViewState("bSubscribe") Then
                    chkSubscribe.Visible = True

                Else
                    chkSubscribe.Visible = False
                    tdSubscribe.Visible = False
                End If
                If ViewState("bOnlyExcel") Then
                    btnView.Visible = False
                Else
                    btnView.Visible = True
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub RadGridReport_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGridReport.NeedDataSource
        If DataLoaded Then
            'FillReportData(False)
            RadGridReport.DataSource = Session("GridData")
        End If

    End Sub
    Protected Sub RadGridReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGridReport.ItemDataBound
        If TypeOf e.Item Is GridGroupHeaderItem Then
            Dim groupHeader As GridGroupHeaderItem = TryCast(e.Item, GridGroupHeaderItem)
            groupHeader.DataCell.Text = groupHeader.DataCell.Text.Split(":")(1).ToString()
        End If

    End Sub
    Private Function GetBSUShortnames(BSU_IDs As String) As String
        Dim Sql As String
        GetBSUShortnames = ""
        If BSU_IDs.ToString <> "" Then
            Sql = "SELECT  STUFF(( SELECT    ',' + CAST(BSU_SHORTNAME AS VARCHAR(MAX)) FROM OASIS..BUSINESSUNIT_M" &
                           " WHERE  BSU_ID IN ('" & BSU_IDs.Replace(",", "','") & "')" &
                          " FOR XML PATH('')), 1, 1, '') "
            GetBSUShortnames = Mainclass.getDataValue(Sql, "OASISConnectionString")
        End If
    End Function
    Private Function GetHeaderTitle() As String
        Dim HeaderTitle As String = ""
        Dim ConnectionString As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim strSQL As String
        Dim ParamList As DataTable, ParamRow As DataRow
        If SPName = "" Then
            Return ""
            Exit Function
        End If
        strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from  " & DBName & ".information_schema.parameters where PARAMETER_MODE='IN' AND specific_schema + '.' + specific_name = '" & SPName & "'"
        ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand(DBName & "." & SPName, ConnectionManger.GetOASISConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        ' HeaderTitle &= "<span>" & radReportType.SelectedItem.Text & "</span><br/>"
        HeaderTitle &= "<div style=""font-weight: bold; text-align: center;"">" & Mainclass.GetBSUName(Session("sBsuid")) & "</div>"
        HeaderTitle &= "<div style=""font-weight: bold; text-align: center;"">" & radReportType.SelectedItem.Text & "</div>"


        '   HeaderTitle &= "<span>Business Unit : " & Mainclass.GetBSUName(Session("sBsuid")) & "</span><br/>"
        Dim ParamName As String, IsAllNodeChecked As Boolean

        If ParamList IsNot Nothing Then
            For Each ParamRow In ParamList.Rows
                ParamName = UCase(ParamRow("parameter_name"))
                IsAllNodeChecked = False
                Select Case ParamName
                    Case "@BSU_IDS"
                        If trBSUIDs.Style("display") = "table" Then
                            Dim BSU_IDs As String
                            BSU_IDs = GetSelectedNode(RadBSUTreeView, ",", IsAllNodeChecked)
                            If BSU_IDs.Length > 0 Then
                                If IsAllNodeChecked Then
                                    BSU_IDs = "All"
                                Else
                                    BSU_IDs = IIf(BSU_IDs.Length > 0, BSU_IDs.Substring(0, BSU_IDs.Length - 1), "")
                                    BSU_IDs = GetBSUShortnames(BSU_IDs)
                                End If
                                HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Selected Business Unit(s) : " & BSU_IDs & "</div>"
                            End If
                        End If
                    Case "@BSU_ID"
                        If trBSUID.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Selected Business Unit : " & ddlBSU.SelectedItem.Text & "</div>"
                        End If
                    Case "@ACD_ID"
                        ' If trAcademicYear.Style("display") = "block" Then
                        If trAcademicYear.Visible = True Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Academic Year : " & ddlAcademicYear.SelectedItem.Text & "</div>"
                        End If
                    Case "@SVC_ID"
                        If trService.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Service : " & ddlService.SelectedItem.Text & "</div>"
                        End If
                    Case "@FEE_ID"
                        If trFeeType.Style("display") = "table" Then
                            HeaderTitle &= "<span>Fee Type : " & ddlFeeType.SelectedItem.Text & "</div>"
                        End If
                    Case "@STU_ID"
                        If trStudents.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Student : " & txtStdNo.Text & "-" & txtStudentname.Text & "</div>"
                        End If
                    Case "@STU_TYPE"
                        If trStudents.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Student Type : " & IIf(radStud.Checked, "Student", "Enquiry") & "</div>"
                        End If
                    Case "@USR_ID"
                        If trUserID.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">User Id : " & txtUserID.Text & "</div>"
                        End If

                    Case "@ELT_IDS"
                        If trLeaveTypes.Style("display") = "table" Then
                            Dim ELTIDS As String
                            ELTIDS = GetSelectedNode(RadTreeLeaveTypes, ",", IsAllNodeChecked)
                            If ELTIDS.Length > 0 Then
                                If IsAllNodeChecked Then
                                    ELTIDS = "All"
                                Else
                                    ELTIDS = IIf(ELTIDS.Length > 0, ELTIDS.Substring(0, ELTIDS.Length - 1), "")
                                End If
                                HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Leave Type(s) : " & ELTIDS & "</div>"
                            End If

                        End If
                    Case "@ELT_ID"
                        If trLeaveType.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Leave Type : " & ddlLeaveType.SelectedItem.Text & "</div>"
                        End If
                    Case "@ECT_IDS"
                        If trEmpCategory.Style("display") = "table" Then
                            Dim ECTIDS As String
                            ECTIDS = GetSelectedNode(RadTreeEmpCategory, ",", IsAllNodeChecked)
                            If ECTIDS.Length > 0 Then
                                If IsAllNodeChecked Then
                                    ECTIDS = "All"
                                Else
                                    ECTIDS = IIf(ECTIDS.Length > 0, ECTIDS.Substring(0, ECTIDS.Length - 1), "")
                                End If
                                HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Employee Category(s) : " & ECTIDS & "</div>"
                            End If
                        End If
                    Case "@EMP_ABC"
                        If trEMPABC.Style("display") = "table" Then
                            Dim ABC As String
                            ABC = GetSelectedNode(RadTreeABC, ",")
                            If ABC.Length > 0 Then
                                ABC = IIf(ABC.Length > 0, ABC.Substring(0, ABC.Length - 1), "")
                                HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Employee ABC : " & ABC & "</div>"
                            End If

                        End If
                    Case "@EMP_ID"
                        If trEmployee.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Employee : " & txtEmpNo.Text & "</div>"
                        End If
                    Case "@ASONDT"
                        If trAsOnDate.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">As On : " & CDate(txtDate.SelectedDate).ToString("dd/MMM/yyyy") & "</div>"
                        End If
                    Case "@FROMDT"
                        If trDateRange.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">From : " & CDate(txtFromDate.SelectedDate).ToString("dd/MMM/yyyy")
                            If trDateRange.Style("display") = "block" Then
                                HeaderTitle &= "   To : " & CDate(txtToDate.SelectedDate).ToString("dd/MMM/yyyy")
                            End If
                            HeaderTitle &= "</div>"
                        End If
                    Case "@RESULTTYPE"
                        If trRESULTTYPE.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; ""> " & lblFilterByStr.Text & " : " & Me.rblFilter.SelectedItem.Text & "</div>"
                        End If
                    Case "@SEARCHSTR"
                        If trSearchStr.Style("display") = "table" And txtSearchStr.Text.Trim <> "" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">" & lblSearchStr.Text & " : " & txtSearchStr.Text & "</div>"
                        End If
                    Case "@LOG_F_YEAR"
                        HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Financial Year : " & Session("F_YEAR") & "</div>"
                    Case "@AX_SUB_TYPE"
                        If trAXSubType.Style("display") = "table" Then
                            HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Sub Type : " & ddlAXSubType.SelectedItem.Text & "</div>"
                        End If
                    Case "@AX_SUB_TYPES"
                        If trAXSubTypes.Style("display") = "table" Then
                            Dim SUBTYPES As String
                            SUBTYPES = GetSelectedNode(RadTreeAXSubTypes, ",", IsAllNodeChecked)
                            If SUBTYPES.Length > 0 Then
                                If IsAllNodeChecked Then
                                    SUBTYPES = "All"
                                Else
                                    SUBTYPES = IIf(SUBTYPES.Length > 0, SUBTYPES.Substring(0, SUBTYPES.Length - 1), "")
                                End If
                                HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Sub Type(s) : " & SUBTYPES & "</div>"
                            End If

                        End If

                End Select
            Next
        End If
        HeaderTitle &= "<div style=""font-weight: normal; text-align: left; "">Exported By : " & Session("sUsr_name") & " On " & Now.ToString & "</div>"
        Return HeaderTitle
    End Function


    Private Function GetHeaderTitle_JS() As String
        Dim HeaderTitle As String = ""
        Dim ConnectionString As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim strSQL As String
        Dim ParamList As DataTable, ParamRow As DataRow
        If SPName = "" Then
            Return ""
            Exit Function
        End If
        strSQL = "select parameter_name ,replace(replace(replace(parameter_name,'@',''),' ',''),'_','') FilterName, data_type FilterDataType from  " & DBName & ".information_schema.parameters where PARAMETER_MODE='IN' AND specific_schema + '.' + specific_name = '" & SPName & "'"
        ParamList = Mainclass.getDataTable(strSQL, ConnectionString)
        Dim cmd As New SqlCommand
        cmd.Dispose()
        cmd = New SqlCommand(DBName & "." & SPName, ConnectionManger.GetOASISConnection)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        ' HeaderTitle &= "<span>" & radReportType.SelectedItem.Text & "</span><br/>"
        HeaderTitle &= " 00CENTER00 " & Mainclass.GetBSUName(Session("sBsuid")) & " 00RIGHT00 "
        HeaderTitle &= " 00CENTER00 " & radReportType.SelectedItem.Text & " 00RIGHT00 "


        '   HeaderTitle &= "<span>Business Unit : " & Mainclass.GetBSUName(Session("sBsuid")) & "</span><br/>"
        Dim ParamName As String, IsAllNodeChecked As Boolean

        If ParamList IsNot Nothing Then
            For Each ParamRow In ParamList.Rows
                ParamName = UCase(ParamRow("parameter_name"))
                IsAllNodeChecked = False
                Select Case ParamName
                    Case "@BSU_IDS"
                        If trBSUIDs.Style("display") = "table" Then
                            Dim BSU_IDs As String
                            BSU_IDs = GetSelectedNode(RadBSUTreeView, ",", IsAllNodeChecked)
                            If BSU_IDs.Length > 0 Then
                                If IsAllNodeChecked Then
                                    BSU_IDs = "All"
                                Else
                                    BSU_IDs = IIf(BSU_IDs.Length > 0, BSU_IDs.Substring(0, BSU_IDs.Length - 1), "")
                                    BSU_IDs = GetBSUShortnames(BSU_IDs)
                                End If
                                HeaderTitle &= " 00LEFT00 Selected Business Unit(s) : " & BSU_IDs & " 00RIGHT00 "
                            End If
                        End If
                    Case "@BSU_ID"
                        If trBSUID.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Selected Business Unit : " & ddlBSU.SelectedItem.Text & " 00RIGHT00 "
                        End If
                    Case "@ACD_ID"
                        ' If trAcademicYear.Style("display") = "block" Then
                        If trAcademicYear.Visible = True Then
                            HeaderTitle &= " 00LEFT00 Academic Year : " & ddlAcademicYear.SelectedItem.Text & " 00RIGHT00 "
                        End If
                    Case "@SVC_ID"
                        If trService.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Service : " & ddlService.SelectedItem.Text & " 00RIGHT00 "
                        End If
                    Case "@FEE_ID"
                        If trFeeType.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Fee Type : " & ddlFeeType.SelectedItem.Text & " 00RIGHT00 "
                        End If
                    Case "@STU_ID"
                        If trStudents.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Student : " & txtStdNo.Text & "-" & txtStudentname.Text & " 00RIGHT00 "
                        End If
                    Case "@STU_TYPE"
                        If trStudents.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Student Type : " & IIf(radStud.Checked, "Student", "Enquiry") & " 00RIGHT00 "
                        End If
                    Case "@USR_ID"
                        If trUserID.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 User Id : " & txtUserID.Text & " 00RIGHT00 "
                        End If

                    Case "@ELT_IDS"
                        If trLeaveTypes.Style("display") = "table" Then
                            Dim ELTIDS As String
                            ELTIDS = GetSelectedNode(RadTreeLeaveTypes, ",", IsAllNodeChecked)
                            If ELTIDS.Length > 0 Then
                                If IsAllNodeChecked Then
                                    ELTIDS = "All"
                                Else
                                    ELTIDS = IIf(ELTIDS.Length > 0, ELTIDS.Substring(0, ELTIDS.Length - 1), "")
                                End If
                                HeaderTitle &= " 00LEFT00 Leave Type(s) : " & ELTIDS & " 00RIGHT00 "
                            End If

                        End If
                    Case "@ELT_ID"
                        If trLeaveType.Style("display") = "table" Then
                            If ddlLeaveType.SelectedItem IsNot Nothing Then
                                HeaderTitle &= " 00LEFT00 Leave Type : " & ddlLeaveType.SelectedItem.Text & " 00RIGHT00 "
                            End If
                        End If
                    Case "@ECT_IDS"
                        If trEmpCategory.Style("display") = "table" Then
                            Dim ECTIDS As String
                            ECTIDS = GetSelectedNode(RadTreeEmpCategory, ",", IsAllNodeChecked)
                            If ECTIDS.Length > 0 Then
                                If IsAllNodeChecked Then
                                    ECTIDS = "All"
                                Else
                                    ECTIDS = IIf(ECTIDS.Length > 0, ECTIDS.Substring(0, ECTIDS.Length - 1), "")
                                End If
                                HeaderTitle &= " 00LEFT00 Employee Category(s) : " & ECTIDS & " 00RIGHT00 "
                            End If
                        End If
                    Case "@EMP_ABC"
                        If trEMPABC.Style("display") = "table" Then
                            Dim ABC As String
                            ABC = GetSelectedNode(RadTreeABC, ",")
                            If ABC.Length > 0 Then
                                ABC = IIf(ABC.Length > 0, ABC.Substring(0, ABC.Length - 1), "")
                                HeaderTitle &= " 00LEFT00 Employee ABC : " & ABC & " 00RIGHT00 "
                            End If

                        End If
                    Case "@EMP_ID"
                        If trEmployee.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Employee : " & txtEmpNo.Text & " 00RIGHT00 "
                        End If
                    Case "@ASONDT"
                        If trAsOnDate.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 As On : " & CDate(txtDate.SelectedDate).ToString("dd/MMM/yyyy") & " 00RIGHT00 "
                        End If
                    Case "@FROMDT"
                        If trDateRange.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 From : " & CDate(txtFromDate.SelectedDate).ToString("dd/MMM/yyyy")
                            If trDateRange.Style("display") = "block" Then
                                HeaderTitle &= "   To : " & CDate(txtToDate.SelectedDate).ToString("dd/MMM/yyyy")
                            End If
                            HeaderTitle &= " 00RIGHT00 "
                        End If
                    Case "@RESULTTYPE"
                        If trRESULTTYPE.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 " & lblFilterByStr.Text & " : " & Me.rblFilter.SelectedItem.Text & " 00RIGHT00 "
                        End If
                    Case "@SEARCHSTR"
                        If trSearchStr.Style("display") = "table" And txtSearchStr.Text.Trim <> "" Then
                            HeaderTitle &= " 00LEFT00 " & lblSearchStr.Text & " : " & txtSearchStr.Text & " 00RIGHT00 "
                        End If
                    Case "@LOG_F_YEAR"
                        HeaderTitle &= " 00LEFT00 Financial Year : " & Session("F_YEAR") & " 00RIGHT00 "
                    Case "@AX_SUB_TYPE"
                        If trAXSubType.Style("display") = "table" Then
                            HeaderTitle &= " 00LEFT00 Sub Type : " & ddlAXSubType.SelectedItem.Text & " 00RIGHT00 "
                        End If
                    Case "@AX_SUB_TYPES"
                        If trAXSubTypes.Style("display") = "table" Then
                            Dim SUBTYPES As String
                            SUBTYPES = GetSelectedNode(RadTreeAXSubTypes, ",", IsAllNodeChecked)
                            If SUBTYPES.Length > 0 Then
                                If IsAllNodeChecked Then
                                    SUBTYPES = "All"
                                Else
                                    SUBTYPES = IIf(SUBTYPES.Length > 0, SUBTYPES.Substring(0, SUBTYPES.Length - 1), "")
                                End If
                                HeaderTitle &= " 00LEFT00 Sub Type(s) : " & SUBTYPES & " 00RIGHT00 "
                            End If

                        End If

                End Select
            Next
        End If
        HeaderTitle &= " 00LEFT00 Exported By : " & Session("sUsr_name") & " On " & Now.ToString & " 00RIGHT00 "
        Return HeaderTitle
    End Function

    Protected Sub RadGridReport_GridExporting(sender As Object, e As GridExportingArgs) Handles RadGridReport.GridExporting
        If ShowTitleOnExport Then
            Dim customHTML As String
            customHTML = GetHeaderTitle() '"<div width=""100%"" style=""clear:both;text-align:center;font-size:12px;font-family:Verdana;"">" + "Caption name" + "</div>"
            e.ExportOutput = e.ExportOutput.Replace("<body>", String.Format("<body>{0}", customHTML))
        End If


    End Sub
    Protected Sub lnkExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportToExcel.Click
        FillReportData(False)
        'If ShowTitleOnExport Then
        '    RadGridReport.MasterTableView.Caption = GetHeaderTitle()
        'Else
        '    RadGridReport.MasterTableView.Caption = ""
        'End If
        RadGridReport.ExportSettings.OpenInNewWindow = True
        RadGridReport.ExportSettings.ExportOnlyData = True
        RadGridReport.ExportSettings.IgnorePaging = True
        RadGridReport.ExportSettings.FileName = radReportType.SelectedItem.Text.Replace("/", "").Replace("\", "")
        RadGridReport.MasterTableView.ExportToExcel()
        'RadGridReport.ExportSettings.ExportOnlyData = True
        'RadGridReport.ExportSettings.IgnorePaging = True
        'RadGridReport.ExportSettings.OpenInNewWindow = True
        'RadGridReport.MasterTableView.ExportToExcel()
    End Sub
    Protected Sub btnGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrid.Click

    End Sub
    Protected Sub lnkExportToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportToPDF.Click
        FillReportData(False)
        If ShowTitleOnExport Then
            RadGridReport.MasterTableView.Caption = GetHeaderTitle()
        Else
            RadGridReport.MasterTableView.Caption = ""
        End If
        RadGridReport.ExportSettings.ExportOnlyData = True
        RadGridReport.ExportSettings.IgnorePaging = True
        RadGridReport.ExportSettings.OpenInNewWindow = True
        RadGridReport.ExportSettings.FileName = radReportType.SelectedItem.Text.Replace("/", "").Replace("\", "")
        RadGridReport.MasterTableView.ExportToPdf()
    End Sub

#End Region

    Protected Sub chkShowFilters_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowFilters.CheckedChanged
        Try
            RadGridReport.AllowFilteringByColumn = chkShowFilters.Checked
            RadGridReport.Rebind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub chkSubscribe_CheckedChanged(sender As Object, e As EventArgs)
        If chkSubscribe.Checked = True Then
            tdSubscribe.Visible = True
        Else
            tdSubscribe.Visible = False
        End If
    End Sub
    Protected Sub btnOkay_Click(sender As Object, e As EventArgs)
        '  Dim param As Hashtable = ParameterList()
        FillReportData(False)
        getparams(ViewState("SubscribeParam"))
        SaveSubscription()
    End Sub
    'Function ParameterList() As Hashtable
    '    Dim param As New Hashtable
    '    'If txtToDate.Text = "" Then
    '    '    txtToDate.Text = txtAsOnDate.Text
    '    'End If
    '    'param.Add("@ACD_XML", GetACDFilter)
    '    'param.Add("@GRD_XML", GetGRDFilter)
    '    'param.Add("@FromDate", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
    '    'param.Add("@ToDate", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
    '    'param.Add("@ACD_ID", Session("Current_ACD_ID"))
    '    'param.Add("@BSU_ID", Session("sBSUID"))
    '    'param.Add("@QRY_ID", ddlQuery.SelectedItem.Value)
    '    param = ViewState("SubscribeParam")
    '    getparams(param)
    '    Return param

    'End Function
    Protected Sub SaveSubscription()
        ViewState("EDD_EMP_ID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))

        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(12) As SqlParameter
            PARAM(0) = New SqlParameter("@RSM_MNU_CODE", ViewState("MainMnu_code"))
            PARAM(1) = New SqlParameter("@RSM_RPT_NAME", radReportType.SelectedItem.Text)
            PARAM(2) = New SqlParameter("@RSM_PARAMS", ViewState("ParamListName"))
            If trAsOnDate.Style("display") = "table" Then
                PARAM(3) = New SqlParameter("@RSM_DATE_PARAM1", Format(CDate(txtDate.SelectedDate), "yyyy-MM-dd"))
                PARAM(4) = New SqlParameter("@RSM_DATE_PARAM2", "")
            ElseIf trDateRange.Style("display") = "table" Then
                PARAM(3) = New SqlParameter("@RSM_DATE_PARAM1", Format(CDate(txtFromDate.SelectedDate), "yyyy-MM-dd"))
                PARAM(4) = New SqlParameter("@RSM_DATE_PARAM2", Format(CDate(txtToDate.SelectedDate), "yyyy-MM-dd"))
            End If
            PARAM(5) = New SqlParameter("@RSS_PATTERN", rblSubscribe.SelectedItem.Value)
            PARAM(6) = New SqlParameter("@RSS_PARAM_VALUE", ViewState("ParamListValue"))
            PARAM(7) = New SqlParameter("@RSS_EMP_ID", ViewState("EDD_EMP_ID"))
            PARAM(8) = New SqlParameter("@RSM_REPORT_TYPE", ViewState("REPORT_TYPE"))
            PARAM(9) = New SqlParameter("@RSM_DATE_PARAM1_NAME", ViewState("DATE_PARAM1"))
            PARAM(10) = New SqlParameter("@RSM_DATE_PARAM2_NAME", ViewState("DATE_PARAM2"))
            PARAM(11) = New SqlParameter("@RSM_PROC_NAME", ViewState("DB_NAME") + "." + ViewState("PROC_NAME"))

            SqlHelper.ExecuteNonQuery(CONN, CommandType.StoredProcedure, "[dbo].[SaveReportSubscription]", PARAM)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try




    End Sub
    Sub getparams(param As Hashtable)
        '-------------------------------------------------NEED TO CHANGE DEPENDING ON THE REPORT------------------------------------------------------------
        ViewState("DB_NAME") = DBName
        ViewState("PROC_NAME") = SPName
        ViewState("REPORT_TYPE") = "Excel"
        If trAsOnDate.Style("display") = "table" Then
            ViewState("DATE_PARAM1") = "@ASONDT"
            ViewState("DATE_PARAM2") = ""
        Else
            ViewState("DATE_PARAM1") = "@FROMDT"
            ViewState("DATE_PARAM2") = "@TODT"
        End If
        '---------------------------------------------------------------------------------------------------------------------------------------------------
        Dim name As String = ""
        Dim value As String = ""
        Dim collection As Hashtable = param

        If (collection.Count <> 0) Then
            For Each item In collection
                If item.key <> ViewState("DATE_PARAM1") And item.key <> ViewState("DATE_PARAM2") Then
                    name += item.key.ToString()
                    value += item.value.ToString()
                    If name <> "" Then
                        name += "|$|"
                        value += "|$|"
                    End If
                End If
            Next
            name = name.Trim("|", "$")
            value = value.Trim("|", "$")
            ViewState("ParamListName") = name
            ViewState("ParamListValue") = value
        End If

    End Sub
    Protected Sub Page_UnLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not IsPostBack Then
            Session.Remove("GridData")
            radReportType.Dispose()
        End If
    End Sub

    Public Function ConvertDataTableToHTML(ByVal dt As DataTable) As String

        Dim html As StringBuilder = New StringBuilder()
        'html.Append("<table id='myTable' width='100%' class='display' cellspacing='0'>")
        html.Append("<table border=1 cellspacing=0 cellpadding=0 width=100%>")
        html.Append("<thead><tr >")

        For Each column As DataColumn In dt.Columns
            html.Append("<th>")
            html.Append(column.ColumnName)
            html.Append("</th>")

        Next

        html.Append("</tr></thead>")
        html.Append("<tbody>")
        Dim i As Integer = 0

        For Each row As DataRow In dt.Rows
            Dim s As Integer
            s = i Mod 2

            If s = 0 Then
                html.Append("<tr>")
            Else
                html.Append("<tr>")
            End If

            For Each column As DataColumn In dt.Columns
                html.Append("<td>")
                html.Append(row(column.ColumnName).ToString.Replace("'", ""))
                html.Append("</td>")

            Next

            html.Append("</tr>")
            i += 1
        Next

        html.Append("</tbody>")
        html.Append("</table>")

        Return html.ToString
    End Function
End Class
