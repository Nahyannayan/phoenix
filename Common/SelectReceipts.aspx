﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelectReceipts.aspx.vb" Inherits="Common_SelectReceipts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />

    <script src="../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script lang="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>
    <!-- Bootstrap core CSS-->
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <script lang="javascript" type="text/javascript">
        function SetValuetoParent(fclid) {
            parent.SetFCLIDs(fclid);
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                        <input id="hf_STUID" runat="server" type="hidden" value="" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row"
                            EmptyDataText="No Data" AllowPaging="True" PageSize="12" DataKeyNames="FCL_ID">
                            <Columns>
                                <asp:TemplateField HeaderText="Receipt No">
                                    <HeaderTemplate>
                                        Receipt#
                                    <br />
                                        <asp:TextBox ID="txtRecNo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchRecNo" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                            OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnReceiptNo" runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FCL_DATE" HeaderText="Receipt Date" />
                                <asp:TemplateField HeaderText="Amount">
                                    <HeaderTemplate>
                                        Amount<br />
                                        <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchAmount" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                            OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Student Id">
                                    <HeaderTemplate>
                                        Student Id<br />
                                        <asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchId" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                            OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <HeaderTemplate>
                                        Name<br />
                                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                            OnClick="btnSearchName_Click" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
