﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Common_SelectReceipts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            gvGroup.Attributes.Add("bordercolor", "#1b80b6")
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            ViewState("gvGroup") = Nothing
            LoadData()
            btnClose.Attributes.Add("onClick", "parent.CloseFrame(); return false;")
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub LoadData()
        Try
            Dim str_search, str_txtRecNo, str_txtAmount, str_txtStuno, str_txtStuname As String
            str_txtStuname = "" : str_txtRecNo = "" : str_txtAmount = "0" : str_txtStuno = ""
            
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                Dim str_Sid_search() As String

                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtRecNo")
                str_txtRecNo = txtSearch.Text.Trim
                
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtAmount")
                str_txtAmount = txtSearch.Text.Trim

               
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtStudNo")
                str_txtStuno = txtSearch.Text.Trim

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtStudName")
                str_txtStuname = txtSearch.Text.Trim
            End If
            str_txtAmount = FeeCollection.GetDoubleVal(str_txtAmount)

            Dim STU_BSU_ID As String = IIf(Request.QueryString("stubsu") Is Nothing, "", Request.QueryString("stubsu"))
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PROVIDER_BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("sBsuid")
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = STU_BSU_ID
            pParms(2) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 20)
            pParms(2).Value = str_txtRecNo
            pParms(3) = New SqlClient.SqlParameter("@FCL_AMOUNT", SqlDbType.Decimal)
            pParms(3).Value = str_txtAmount
            pParms(4) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(4).Value = str_txtStuno
            pParms(5) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar, 303)
            pParms(5).Value = str_txtStuname

            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
              CommandType.StoredProcedure, "FEES.GET_RECEIPTS_FOR_CANCELLATION", pParms)
            If Not dsData Is Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                ViewState("gvGroup") = dsData.Tables(0)
                BindGrid()
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtRecNo")
            txtSearch.Text = str_txtRecNo
            txtSearch = gvGroup.HeaderRow.FindControl("txtAmount")
            txtSearch.Text = str_txtAmount
            txtSearch = gvGroup.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = str_txtStuno
            txtSearch = gvGroup.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = str_txtStuname
            set_Menu_Img()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindGrid()
        gvGroup.DataSource = ViewState("gvGroup")
        gvGroup.DataBind()
    End Sub
    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        LoadData()
    End Sub

    Protected Sub gvGroup_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbtnReceiptNo As LinkButton = DirectCast(e.Row.FindControl("lbtnReceiptNo"), LinkButton)
            Dim FCL_ID As Long = gvGroup.DataKeys(e.Row.RowIndex)(0)
            If Not lbtnReceiptNo Is Nothing Then
                lbtnReceiptNo.Attributes.Add("onClick", "return SetValuetoParent(" & FCL_ID & ");")
            End If
        End If
    End Sub
End Class
