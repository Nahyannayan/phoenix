﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic

Partial Class Common_SelectStudents
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                gvGroup.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

                Session("liUserList") = New ArrayList

                Bind_All()
                btnFinish.Attributes.Add("onClick", "return SetValuetoParent();")
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid4(str_Sid_img(2))
    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub Bind_All()
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Select Case type
            Case "ENQ_COMP"
                Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
                Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
                GridbindEnquiryCompany(COMP_ID, ACD_ID)
            Case "STU_TRAN"
                Dim str_ACD As String = Request.QueryString("acd") & ""
                Dim str_Prevacd As String = Request.QueryString("prevacd") & ""
                Dim BSU_ID As String = Request.QueryString("bsu")
                GridbindTransport(BSU_ID, str_ACD, str_Prevacd)
            Case Else
                Gridbind()
        End Select
    End Sub

    Private Sub GridbindEnquiryCompany(ByVal COMP_ID As Integer, ByVal ACD_ID As Integer)
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtGrade As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty

            Dim ddacctype As New DropDownList
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'Try
                '    Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
                '    ddacctype = gvGroup.HeaderRow.FindControl("DDAccountType")
                'Catch ex As Exception
                'End Try

                'If ddacctype.SelectedItem.Value <> "All" Then
                '    str_filter_acctype = " AND STU_GENDER='" & ddacctype.SelectedItem.Value & "'"
                'End If
                str_mode = Request.QueryString("ShowType") 'ShowType
                i_dd_acctype = ddacctype.SelectedIndex
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
            str_Sql += " WHERE  STU_BSU_ID='" & Request.QueryString("bsu") & "'"
            If ACD_ID <> "-1" Then
                str_Sql += " AND  STU_ACD_ID = " & ACD_ID
            End If
            If COMP_ID <> "-1" Then
                str_Sql += " AND  COMP_ID = " & COMP_ID
            End If
            Dim ds As New DataSet
            Dim str_orderby As String = " ORDER BY STU_NAME "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_name & str_filter_pmobile & str_filter_pname & str_orderby)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            '  gvGroup.Columns(0).Visible = False
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()
            SetChk(Me.Page)

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GridbindTransport(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal PREV_ACD_ID As String)
        Try
            Dim str_search As String = String.Empty
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_filter_grade As String = String.Empty
            Dim str_filter_parname As String = String.Empty
            Dim str_filter_parmobile As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_txtGrade As String = String.Empty
            Dim str_txtPName As String = String.Empty
            Dim str_txtMobile As String = String.Empty
            ''''''''''
            Dim str_CodeorName As String = Request.QueryString("codeorname") & ""
            str_CodeorName = str_CodeorName.Replace("'", "")
            If IsNumeric(str_CodeorName) Then
                str_txtCode = str_CodeorName
                str_filter_code = " AND STU_NO LIKE '%" & str_CodeorName & "%'"
            Else
                str_txtName = str_CodeorName
                str_filter_name = " AND STU_NAME LIKE '%" & str_CodeorName & "%'"
            End If
            If IsNumeric(ACD_ID) And IsNumeric(PREV_ACD_ID) Then
                str_filter_name = str_filter_name & " AND SSV_ACD_ID in ( " & ACD_ID & "," & PREV_ACD_ID & ")"
            ElseIf IsNumeric(ACD_ID) Then
                str_filter_name = str_filter_name & " AND SSV_ACD_ID = '" & ACD_ID & "'"
            End If

            '''''''''''''''''''''''

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                'str_mode = Request.QueryString("ShowType") 'ShowType
                ''code
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_grade = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                ''par name

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtPName = txtSearch.Text
                str_filter_parname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''par mobile  = ""
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtMobile = txtSearch.Text
                str_filter_parmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))
            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT DISTINCT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, PARENT_MOBILE, PARENT_NAME ," _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive" _
            & " FROM VW_OSO_STUDENT_M INNER JOIN VW_STUDENT_SERVICES_D_MAX ON STU_ID=SSV_STU_ID" _
            & " WHERE STU_bActive=1 AND STU_BSU_ID='" & BSU_ID & "' "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_grade & str_filter_code & _
            str_filter_name & str_filter_parname & str_filter_parmobile & " ORDER BY STU_NAME")
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtPName
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtMobile
            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub Gridbind(Optional ByVal STUD_ID As String = "")
        Try
            Dim str_filter_acctype, str_mode As String
            Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
            Dim str_txtCode, str_txtName, str_txtGrade As String

            Dim i_dd_acctype As Integer = 0

            str_filter_acctype = ""
            str_filter_code = ""
            str_filter_name = ""
            str_filter_control = ""
            Dim str_filter_debit As String = String.Empty
            str_txtCode = ""
            str_txtName = ""
            str_txtGrade = ""
            str_mode = Request.QueryString("ShowType") 'PARTY_D

            Dim str_filter_pname As String = String.Empty
            Dim str_filter_pmobile As String = String.Empty
            Dim str_txtpname As String = String.Empty
            Dim str_txtpmobile As String = String.Empty

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text
                str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text
                str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
                str_txtGrade = txtSearch.Text
                str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
                str_txtpname = txtSearch.Text
                str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))
                ''control
                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
                str_txtpmobile = txtSearch.Text
                str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

            End If
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String = " SELECT STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER, " _
            & " GRD_DISPLAY, STU_ACD_ID, ACY_DESCR, STU_GRD_ID, STU_bActive,PARENT_NAME, PARENT_MOBILE" _
            & " FROM VW_OSO_STUDENT_M " _
            & " WHERE STU_bActive=1  AND STU_BSU_ID='" & Request.QueryString("bsu") & "' "
            If STUD_ID <> "" Then
                str_Sql += " AND STU_SIBLING_ID <> STU_ID AND STU_SIBLING_ID=" & STUD_ID
            End If
            Dim str_orderby As String = " ORDER BY STU_NAME "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, _
            str_Sql & str_filter_debit & str_filter_acctype & _
            str_filter_control & str_filter_code & str_filter_name & str_filter_pmobile & str_filter_pname & str_orderby)
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = str_txtGrade
            txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
            txtSearch.Text = str_txtpname
            txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
            txtSearch.Text = str_txtpmobile

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        SetChk(Me.Page)
        Bind_All()
    End Sub

    Protected Sub btnSearchName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gvGroup.PageIndex = 0
        Bind_All()
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click

    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        Dim COMP_ID As String = IIf(Request.QueryString("COMP_ID") = "", -1, Request.QueryString("COMP_ID"))
        Dim type As String = IIf(Request.QueryString("type") Is Nothing, "", Request.QueryString("type"))
        Dim BSU_ID As String = Request.QueryString("bsu")
        If chkSelAll.Checked Then
            Dim str_Filter As String = GetFilter()
            Dim BUnitreaderSuper As SqlDataReader
            Select Case type
                Case "STUD_TRAN"
                    BUnitreaderSuper = GetAllStudentsList(COMP_ID, str_Filter)
                Case "ENQ_COMP"
                    BUnitreaderSuper = GetAllEnquiryList(COMP_ID, str_Filter)
            End Select
            If BUnitreaderSuper.HasRows = True Then
                While (BUnitreaderSuper.Read())
                    Session("liUserList").Remove(BUnitreaderSuper(0).ToString)
                    Session("liUserList").Add(BUnitreaderSuper(0).ToString)
                End While
            End If
        Else
            Session("liUserList").Clear()
        End If
        Bind_All()
        SetChk(Me.Page)

    End Sub

    Private Function GetFilter() As String
        Dim str_filter_acctype, str_mode As String
        Dim str_search, str_filter_code, str_filter_name, str_filter_control As String
        Dim str_txtCode, str_txtName, str_txtGrade, str_filter_pname, str_filter_pmobile As String
        Dim i_dd_acctype As Integer = 0
        str_filter_acctype = ""
        str_filter_code = ""
        str_filter_name = ""
        str_filter_control = ""
        Dim str_filter_debit As String = String.Empty
        str_txtCode = ""
        str_txtName = ""
        str_txtGrade = ""
        str_mode = Request.QueryString("ShowType") 'PARTY_D
        Dim txtSearch As New TextBox
        Try
            Dim s As HtmlControls.HtmlImage = gvGroup.HeaderRow.FindControl("mnu_2_img")
        Catch ex As Exception
        End Try

        str_mode = Request.QueryString("ShowType") 'ShowType
        ''code
        Dim str_Sid_search() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_search = h_selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
        str_txtCode = txtSearch.Text
        str_filter_code = SetCondn(str_search, "STU_NO", Trim(txtSearch.Text))
        ''name
        str_Sid_search = h_Selected_menu_2.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtName")
        str_txtName = txtSearch.Text
        str_filter_name = SetCondn(str_search, "STU_NAME", Trim(txtSearch.Text))
        ''control
        str_Sid_search = h_Selected_menu_3.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtGrade")
        str_filter_control = SetCondn(str_search, "GRD_DISPLAY", Trim(txtSearch.Text))

        str_Sid_search = h_Selected_menu_4.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtPName")
        str_filter_pname = SetCondn(str_search, "PARENT_NAME", Trim(txtSearch.Text))

        ''control
        str_Sid_search = h_Selected_menu_5.Value.Split("__")
        str_search = str_Sid_search(0)
        txtSearch = gvGroup.HeaderRow.FindControl("txtMobile")
        str_filter_pmobile = SetCondn(str_search, "PARENT_MOBILE", Trim(txtSearch.Text))

        Return str_filter_debit & str_filter_acctype & str_filter_control & str_filter_code & str_filter_name & str_filter_pname & str_filter_pmobile
    End Function

    Private Function GetAllEnquiryList(ByVal COMP_ID As String, ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_Sql As String = " Select * from FEES.vw_OSO_ENQUIRY_COMP "
        str_Sql += " WHERE  STU_BSU_ID='" & Request.QueryString("bsu") & "'"
        Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") = "", -1, Request.QueryString("ACD_ID"))
        If ACD_ID <> "-1" Then
            str_Sql += " AND  STU_ACD_ID = " & ACD_ID
        End If
        If COMP_ID <> "-1" Then
            str_Sql += " AND  COMP_ID = " & COMP_ID
        End If
        str_Sql += str_Filter
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Private Function GetAllStudentsList(ByVal COMP_ID As String, ByVal str_Filter As String) As SqlDataReader
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim str_Sql As String = " select STU_ID,STU_BSU_ID,  STU_bActive from " & _
        "( SELECT STU_ID, STU_BSU_ID, STU_bActive ," & _
        " CASE dbo.VW_OSO_STUDENT_M .STU_PRIMARYCONTACT WHEN 'F' " & _
        " THEN IsNULL(dbo.STUDENT_D.STS_F_COMP_ID, '') " & _
        " WHEN 'M' THEN IsNULL(dbo.STUDENT_D.STS_M_COMP_ID, '') " & _
        " WHEN 'G' THEN IsNULL(dbo.STUDENT_D.STS_G_COMP_ID, '') END AS COMP_ID" & _
        " FROM VW_OSO_STUDENT_M  INNER JOIN" & _
        " dbo.STUDENT_D ON dbo.VW_OSO_STUDENT_M .STU_ID = dbo.STUDENT_D.STS_STU_ID WHERE 1=1 " & _
        str_Filter & ") a" & _
        " WHERE a.STU_BSU_ID='" & Request.QueryString("bsu") & "' AND STU_bActive =1 "
        If COMP_ID <> "-1" Then
            str_Sql += " AND a.COMP_ID = " & COMP_ID
        End If
        Return SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
    End Function

    Protected Sub gvGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGroup.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chk As HtmlInputCheckBox = DirectCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox)
            If Not chk Is Nothing Then
                chk.Attributes.Add("onClick", "return GetCheckedValues('" & chk.ClientID & "');")
            End If
        End If
    End Sub
End Class
