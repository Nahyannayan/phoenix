﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet

Partial Class Common_StudStaffAttendanceHR
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("ID") IsNot Nothing Then
                Dim MainID As String = Request.QueryString("ID").ToString
                Dim status As String = Request.QueryString("status").ToString
                ViewState("ID") = MainID

                If status = "P" Then
                    status = "PRESENT"
                Else
                    status = "ABSENT"
                End If
                ViewState("STATUS") = status
                BindStudStaffAttendance(MainID, status)
            End If
        End If
    End Sub

    Sub BindStudStaffAttendance(ByVal TYPEID As String, ByVal status As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim txtSearch As New TextBox
            Dim strNo As String = String.Empty
            Dim strName As String = String.Empty
            Dim strGrade As String = String.Empty
            Dim strSection As String = String.Empty
            Dim strStream As String = String.Empty
            Dim strMobile As String = String.Empty
            Dim strEmail As String = String.Empty

            Dim strEmpNo As String = String.Empty
            Dim strEmpName As String = String.Empty
            Dim strEmpCategory As String = String.Empty
            Dim strEmpDesig As String = String.Empty
            Dim strEmpDept As String = String.Empty
            Dim strEmpMobile As String = String.Empty
            Dim strEmpEmail As String = String.Empty
            Dim searchFilter As String = String.Empty
            If gvStud.Rows.Count > 0 Then

                txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")

                If txtSearch.Text.Trim <> "" Then
                    strNo = " AND replace(STU_NO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    'strNo = txtSearch.Text.Trim
                End If
                txtSearch = gvStud.HeaderRow.FindControl("txtStuName")

                If txtSearch.Text.Trim <> "" Then
                    strName = " AND replace(STUDNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

                txtSearch = gvStud.HeaderRow.FindControl("txtSTUGRade")

                If txtSearch.Text.Trim <> "" Then
                    strGrade = " AND replace(GRADE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

                txtSearch = gvStud.HeaderRow.FindControl("txtSTUSECTION")

                If txtSearch.Text.Trim <> "" Then
                    strSection = " AND replace(SECTION,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

                txtSearch = gvStud.HeaderRow.FindControl("txtSTUSTREAM")

                If txtSearch.Text.Trim <> "" Then
                    strGrade = " AND replace(STREAM,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

                txtSearch = gvStud.HeaderRow.FindControl("txtSTUMOBILE")

                If txtSearch.Text.Trim <> "" Then
                    strMobile = " AND replace(MOBILE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

                txtSearch = gvStud.HeaderRow.FindControl("txtSTUEMAIL")

                If txtSearch.Text.Trim <> "" Then
                    strEmail = " AND replace(EMAIL,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

            End If

            If gvstaff.Rows.Count > 0 Then
                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPNo")

                If txtSearch.Text.Trim <> "" Then
                    strEmpNo = " AND replace(EMPNO,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    'strNo = txtSearch.Text.Trim
                End If

                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPName")

                If txtSearch.Text.Trim <> "" Then
                    strEmpName = " AND replace(EMPNAME,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    'strNo = txtSearch.Text.Trim
                End If

                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPCATEGORY")

                If txtSearch.Text.Trim <> "" Then
                    strEmpCategory = " AND replace(CATEGORY,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    'strNo = txtSearch.Text.Trim
                End If

                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPDESIG")

                If txtSearch.Text.Trim <> "" Then
                    strEmpDesig = " AND replace(DESIGNATION,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    'strNo = txtSearch.Text.Trim
                End If


                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPDEPT")

                If txtSearch.Text.Trim <> "" Then
                    strEmpDept = " AND replace(DEPARTMENT,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    'strNo = txtSearch.Text.Trim
                End If


                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPMOBILE")

                If txtSearch.Text.Trim <> "" Then
                    strEmpMobile = " AND replace(MOBILE,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If

                txtSearch = gvstaff.HeaderRow.FindControl("txtEMPEMAIL")

                If txtSearch.Text.Trim <> "" Then
                    strEmpEmail = " AND replace(EMAIL,' ','') like '%" & txtSearch.Text.Trim.Replace(" ", "") & "%' "
                    ' str_CODE = txtSearch.Text.Trim
                End If
            End If
            searchFilter = strNo + strName + strGrade + strStream + strEmpNo + strEmpName + strEmpCategory + strEmpDesig + strEmpDept + strMobile + strEmail + strEmpMobile + strEmpEmail + strSection

            Dim param(5) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlClient.SqlParameter("@OPTION", 2)
            param(2) = New SqlClient.SqlParameter("@TYPE", TYPEID)
            param(3) = New SqlClient.SqlParameter("@SearchFilter", searchFilter)
            param(4) = New SqlClient.SqlParameter("@Status", status)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[QRY].[PRESENT_SCHOOL]", param)
            If ds.Tables(0).Rows.Count > 0 Then

                If TYPEID = "0" Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        gvStud.DataSource = ds
                        gvStud.DataBind()
                    Else
                        ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                        gvStud.DataSource = ds.Tables(0)
                        Try
                            gvStud.DataBind()
                        Catch ex As Exception
                        End Try
                        Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
                        ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                        gvStud.Rows(0).Cells.Clear()
                        gvStud.Rows(0).Cells.Add(New TableCell)
                        gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
                        gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                        gvStud.Rows(0).Cells(0).Text = "Record not available !!!"
                    End If

                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        gvstaff.DataSource = ds
                        gvstaff.DataBind()
                    Else
                        ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                        gvstaff.DataSource = ds.Tables(0)
                        Try
                            gvstaff.DataBind()
                        Catch ex As Exception
                        End Try
                        Dim columnCount As Integer = gvstaff.Rows(0).Cells.Count
                        ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                        gvstaff.Rows(0).Cells.Clear()
                        gvstaff.Rows(0).Cells.Add(New TableCell)
                        gvstaff.Rows(0).Cells(0).ColumnSpan = columnCount
                        gvstaff.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                        gvstaff.Rows(0).Cells(0).Text = "Record not available !!!"
                    End If

                End If


            Else
                If TYPEID = "0" Then
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    gvStud.DataSource = ds.Tables(0)
                    Try
                        gvStud.DataBind()
                    Catch ex As Exception
                    End Try

                    Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
                    ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvStud.Rows(0).Cells.Clear()
                    gvStud.Rows(0).Cells.Add(New TableCell)
                    gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvStud.Rows(0).Cells(0).Text = "Record not available !!!"
                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    gvstaff.DataSource = ds.Tables(0)
                    Try
                        gvstaff.DataBind()
                    Catch ex As Exception
                    End Try

                    Dim columnCount As Integer = gvstaff.Rows(0).Cells.Count
                    ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvstaff.Rows(0).Cells.Clear()
                    gvstaff.Rows(0).Cells.Add(New TableCell)
                    gvstaff.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvstaff.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvstaff.Rows(0).Cells(0).Text = "Record not available !!!"
                End If

            End If
        Catch ex As Exception
            gvStud.Visible = False
            gvstaff.Visible = False
            UtilityObj.Errorlog(ex.Message, "HR Rporting to grid")
        End Try
    End Sub

    Protected Sub gvstaff_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvstaff.PageIndex = e.NewPageIndex
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub


    Protected Sub gvStud_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        gvStud.PageIndex = e.NewPageIndex
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchSTUNO_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchNAme_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchGRade_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchSTREAM_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPNO_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPNAME_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPCATEGORY_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPDESIG_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPDEPT_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub btnExcelDownload_Click(sender As Object, e As EventArgs)
        ExportStudStaffAttendance()
    End Sub

    Sub ExportStudStaffAttendance()
        Try
            Dim IDs As String = String.Empty

            Dim Type As String = ViewState("ID")
            ''done till here 
            Dim SelecteType As String = String.Empty

            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlClient.SqlParameter("@OPTION", 2)
            param(2) = New SqlClient.SqlParameter("@TYPE", ViewState("ID"))
            param(3) = New SqlClient.SqlParameter("@Status", ViewState("STATUS"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[QRY].[PRESENT_SCHOOL]", param)
            Dim dtEXCEL As New DataTable

            If Not ds.Tables(0) Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dtEXCEL = ds.Tables(0)
                    If dtEXCEL.Rows.Count > 0 Then
                        Dim stitle As String = String.Empty

                        If Type = "0" Then
                            stitle = "STUDENTS LIST"
                        ElseIf Type = "1" Then
                            stitle = "TEACHING"
                        ElseIf Type = "2" Then
                            stitle = "SUPPORT STAFF"
                        ElseIf Type = "3" Then
                            stitle = "ADMINISTRATION"
                        ElseIf Type = "4" Then
                            stitle = "TEACHING ASSISTANT"

                        End If

                        Dim ws As ExcelWorksheet = ef.Worksheets.Add(stitle)
                        ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})



                        Response.ContentType = "application/vnd.ms-excel"
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
                        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                        Dim pathSave As String
                        pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                        Dim bytes() As Byte = File.ReadAllBytes(path)
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Response.Clear()
                        Response.ClearHeaders()
                        Response.ContentType = "application/octect-stream"
                        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()


                    Else

                    End If
                End If
            End If

        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, "HR download excel home")
        End Try
    End Sub

    Protected Sub imgSearchMOBILE_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMAIL_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPMOBILE_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchEMPEMAIL_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub

    Protected Sub imgSearchSEction_Click(sender As Object, e As ImageClickEventArgs)
        BindStudStaffAttendance(ViewState("ID"), ViewState("STATUS"))
    End Sub
End Class
