﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudStaffAttendanceHR.aspx.vb" Inherits="Common_StudStaffAttendanceHR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%-- <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
       <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
         <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
          <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>--%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../cssfiles/BSUstyles.css" rel="stylesheet" />
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="../cssfiles/Accordian.css" rel="stylesheet" />

    <!--[if IE]-->
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css">
    <!--[endif]-->

    <!-- Bootstrap header files ends here -->

    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet">
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript">
        function fancyClose() {

            parent.$.fancybox.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="table-responsive">
            <div align="right">
                <asp:Button ID="btnExcelDownload" runat="server" Text="Export to excel" OnClick="btnExcelDownload_Click" CssClass="button" />
            </div>
            <br />
            <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="true" PageSize="50"
                OnPageIndexChanging="gvStud_PageIndexChanging" EmptyDataText="Sorry, No records exist." CssClass="table table-bordered table-row">
                <RowStyle HorizontalAlign="Center" />
                <Columns>


                    <asp:TemplateField HeaderText="STU NO">
                        <HeaderTemplate>
                            STU NO<br />
                            <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchSTUNO" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchSTUNO_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lbSTU_NO" runat="server" ForeColor="#1B80B6" Text='<%# Bind("STU_NO")%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NAME">
                        <HeaderTemplate>
                            NAME<br />
                            <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchNAme" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchNAme_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStuName" runat="server" ForeColor="#1B80B6" Text='<%# Bind("STUDNAME")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GRADE">
                        <HeaderTemplate>
                            GRADE<br />
                            <asp:TextBox ID="txtSTUGRade" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchGRade" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchGRade_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStugrade" runat="server" ForeColor="#1B80B6" Text='<%# Bind("GRADE")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SECTION">
                        <HeaderTemplate>
                            SECTION<br />
                            <asp:TextBox ID="txtSTUSECTION" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchSEction" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchSEction_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStugrade" runat="server" ForeColor="#1B80B6" Text='<%# Bind("SECTION")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="STREAM">
                        <HeaderTemplate>
                            STREAM<br />
                            <asp:TextBox ID="txtSTUSTREAM" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchSTREAM" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchSTREAM_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSream" runat="server" ForeColor="#1B80B6" Text='<%# Bind("STREAM")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="MOBILE">
                        <HeaderTemplate>
                            MOBILE<br />
                            <asp:TextBox ID="txtSTUMOBILE" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchMOBILE" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchMOBILE_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMOBILE" runat="server" ForeColor="#1B80B6" Text='<%# Bind("MOBILE")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="EMAIL">
                        <HeaderTemplate>
                            EMAIL<br />
                            <asp:TextBox ID="txtSTUEMAIL" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMAIL" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMAIL_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblEMAIL" runat="server" ForeColor="#1B80B6" Text='<%# Bind("EMAIL")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:GridView>

            <asp:GridView ID="gvstaff" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="true" PageSize="50"
                OnPageIndexChanging="gvstaff_PageIndexChanging" EmptyDataText="Sorry, No records exist." CssClass="table table-bordered table-row">
                <RowStyle HorizontalAlign="Center" />
                <Columns>


                    <asp:TemplateField HeaderText="EMP NO">
                        <HeaderTemplate>
                            EMP NO<br />
                            <asp:TextBox ID="txtEMPNo" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPNO" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPNO_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblempId" runat="server" Text='<%# Bind("EMP_ID")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lbemp_NO" runat="server" ForeColor="#1B80B6" Text='<%# Bind("EMPNO")%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NAME">
                        <HeaderTemplate>
                            NAME<br />
                            <asp:TextBox ID="txtEMPName" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPNAME" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPNAME_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblempName" runat="server" ForeColor="#1B80B6" Text='<%# Bind("EMPNAME")%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CATEGORY">
                        <HeaderTemplate>
                            CATEGORY<br />
                            <asp:TextBox ID="txtEMPCATEGORY" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPCATEGORY" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPCATEGORY_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblStugrade" runat="server" ForeColor="#1B80B6" Text='<%# Bind("CATEGORY")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DESIGNATION">
                        <HeaderTemplate>
                            DESIGNATION<br />
                            <asp:TextBox ID="txtEMPDESIG" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPDESIG" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPDESIG_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSream" runat="server" ForeColor="#1B80B6" Text='<%# Bind("DESIGNATION")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DEPARTMENT">
                        <HeaderTemplate>
                            DEPARTMENT<br />
                            <asp:TextBox ID="txtEMPDEPT" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPDEPT" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPDEPT_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSream" runat="server" ForeColor="#1B80B6" Text='<%# Bind("DEPARTMENT")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="MOBILE">
                        <HeaderTemplate>
                            MOBILE<br />
                            <asp:TextBox ID="txtEMPMOBILE" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPMOBILE" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPMOBILE_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbleMOBILE" runat="server" ForeColor="#1B80B6" Text='<%# Bind("MOBILE")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="EMAIL">
                        <HeaderTemplate>
                            EMAIL<br />
                            <asp:TextBox ID="txtEMPEMAIL" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgSearchEMPEMAIL" runat="server" ImageAlign="Middle" ImageUrl="~/Images/cal.gif" OnClick="imgSearchEMPEMAIL_Click" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lbleEMAIL" runat="server" ForeColor="#1B80B6" Text='<%# Bind("EMAIL")%>'></asp:Label>

                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
