Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode

Partial Class Common_ListReportView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Private Property AllRecordsIDs() As String
        Get
            Return ViewState("AllRecordsIDs")
        End Get
        Set(ByVal value As String)
            ViewState("AllRecordsIDs") = value
        End Set
    End Property
    Private Property FilterText1() As String
        Get
            Return chkSelection1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection1.Text = value
                chkSelection1.Visible = True
            Else
                chkSelection1.Visible = False
                chkSelection1.Text = ""
            End If
        End Set
    End Property
    Private Property FilterText2() As String
        Get
            Return chkSelection2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection2.Text = value
                chkSelection2.Visible = True
            Else
                chkSelection2.Visible = False
                chkSelection2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox1Text() As String
        Get
            Return rad1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad1.Text = value
                rad1.Visible = True
            Else
                rad1.Visible = False
                rad1.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox2Text() As String
        Get
            Return rad2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad2.Text = value
                rad2.Visible = True
            Else
                rad2.Visible = False
                rad2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox3Text() As String
        Get
            Return rad3.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad3.Text = value
                rad3.Visible = True
            Else
                rad3.Visible = False
                rad3.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox4Text() As String
        Get
            Return rad4.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad4.Text = value
                rad4.Visible = True
            Else
                rad4.Visible = False
                rad4.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox5Text() As String
        Get
            Return rad5.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad5.Text = value
                rad5.Visible = True
            Else
                rad5.Visible = False
                rad5.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox6Text() As String
        Get
            Return rad6.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad6.Text = value
                rad6.Visible = True
            Else
                rad6.Visible = False
                rad6.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText1() As String
        Get
            Return ListButton1.Value
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton1.Value = value
                ListButton1.Visible = True
            Else
                ListButton1.Visible = False
                ListButton1.Value = ""
            End If
        End Set
    End Property
    Private Property ButtonText2() As String
        Get
            Return ListButton2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton2.Text = value
                ListButton2.Visible = True
            Else
                ListButton2.Visible = False
                ListButton2.Text = ""
            End If
        End Set
    End Property

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
        Catch ex As Exception

        End Try
    End Sub
    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"

                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    lblError.Text = msgText
                End If
                ShowGridCheckBox = False
                NoAddingAllowed = False
                NoViewAllowed = False
                HideExportButton = False
                IsStoredProcedure = False
                ButtonText1 = ""
                ButtonText2 = ""
                rad1.Checked = True
                CheckBox1Text = ""
                CheckBox2Text = ""
                CheckBox3Text = ""
                CheckBox4Text = ""
                CheckBox5Text = ""
                CheckBox6Text = ""
                FilterText1 = ""
                FilterText2 = ""
                BuildListView()
                gridbind(False)
                btnExport.Visible = Not HideExportButton
                If NoAddingAllowed Then
                    hlAddNew.Visible = False
                Else
                    hlAddNew.Visible = True
                    Dim url As String
                    url = EditPagePath & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = url
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean, Optional ByVal ForceResetColumns As Boolean = False)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            FilterTextboxString = ""
            If gvDetails.Rows.Count > 0 And Not ForceResetColumns Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If
            If ddlFilter.Visible Then
                If ddlFilter.SelectedItem Is Nothing Then
                    SQLSelect = SQLSelect.Replace("~", " top 10 ")
                ElseIf ddlFilter.SelectedItem.Value = "All" Then
                    SQLSelect = SQLSelect.Replace("~", " top 99999999 ")
                Else
                    SQLSelect = SQLSelect.Replace("~", " top " & ddlFilter.SelectedItem.Value)
                End If
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                str_Filter = " 1 = 1 " & str_Filter
                GridData.Select(str_Filter)
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridData.Copy
                mySelectDT.Rows.Clear()
                For Each mRow In GridData.Select(str_Filter)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If

            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy

            Try
                Dim mRow As DataRow
                AllRecordsIDs = ""
                If myData.Columns.Count > 0 Then
                    For Each mRow In myData.Rows
                        AllRecordsIDs &= IIf(AllRecordsIDs <> "", "|", "") & mRow(0).ToString
                    Next
                End If
            Catch ex As Exception

            End Try


            If myData.Columns.Count > 0 Then
                If (MainMenuCode = "PI01163") And Session("sBSUID").ToString.Trim <> "135005" Then
                    myData.Columns.RemoveAt(0)
                End If
            End If
            ViewState("GridTable") = GridData
            Session("myData") = myData
            SetExportFileLink()
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right

                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 10 Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If

            If (MainMenuCode = "PI01080" Or MainMenuCode = "PI01085") Then
                gvDetails.Columns(2).Visible = False
            End If

            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(11).Visible = Not NoAddingAllowed
            gvDetails.Columns(1).Visible = False
            gvDetails.Columns(11).Visible = Not NoViewAllowed
            If (MainMenuCode = "PI01163") And Session("sBSUID").ToString.Trim = "135005" Then
                gvDetails.Columns(1).Visible = True
            End If
            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                'gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblItemCol1"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                If (MainMenuCode = "PI02008" Or MainMenuCode = "PI02025" Or MainMenuCode = "T200142" Or MainMenuCode = "T200216" Or MainMenuCode = "T200219" Or MainMenuCode = "T200250") And rad1.Checked Then '-----------------------------------
                    hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Encr_decrData.Encrypt(MainMenuCode) & "&datamode=" & Encr_decrData.Encrypt("add")
                ElseIf MainMenuCode = "F300138" Then
                    hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Encr_decrData.Encrypt(MainMenuCode) & "&datamode=" & Encr_decrData.Encrypt("approve")
                ElseIf MainMenuCode = "PI01144" Then
                    'hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Encr_decrData.Encrypt(MainMenuCode)
                    hlview.Attributes.Add("onclick", "CheckForPrint(" & lblItemColID.Text & ",'PI01144')")
                    hlview.Text = "Receipt"
                Else
                    hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
                End If
            End If
            Try
                Dim FloatColIndexArr(), i, ControlName As String
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                'lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("####0.00")
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("#,###,##0.00")

                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        RememberOldValues()
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
        SetChk(Me.Page)
        RePopulateValues()

    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt(lblTitle.Text)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub rad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad1.CheckedChanged, rad2.CheckedChanged, rad3.CheckedChanged, rad4.CheckedChanged, rad5.CheckedChanged, rad6.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub chkSelection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelection1.CheckedChanged, chkSelection2.CheckedChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim MySelectedIDS() As String
            Dim itmIdList As New ArrayList()
            If chkSelectAll.Checked Then
                MySelectedIDS = AllRecordsIDs.Split("|")
                If MySelectedIDS.Length > 0 Then
                    Dim MySelectedID As String
                    For Each MySelectedID In MySelectedIDS
                        If Not itmIdList.Contains(MySelectedID) Then
                            itmIdList.Add(MySelectedID)
                        End If
                    Next
                End If
            End If
            ViewState("checked_items") = itmIdList
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub

    Protected Sub ListButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkControl As New HtmlInputCheckBox
        Dim chkControl1 As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""

        Dim lblID1 As Label
        Dim IDs1 As String = ""

        Dim SelectedIds As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl1 = grow.FindControl("chkControl")
            If chkControl1 IsNot Nothing Then
                If chkControl1.Checked Then
                    lblID1 = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs1 &= IIf(IDs <> "", "|", "") & lblID1.Text
                End If
            End If
        Next


        If IDs1 <> "" Then
            RememberOldValues()
        End If


        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If

        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    If Not ("|" & SelectedIds.ToString & "|").ToString.Contains("|" & lblID.Text.ToString & "|") Then
                        SelectedIds &= IIf(SelectedIds <> "", "|", "") & lblID.Text
                    End If
                End If
            End If
        Next
        If IDs = "" And SelectedIds = "" Then
            lblError.Text = "No Item Selected " '!!!
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "AST0610"
                PrintBarcode(IDs)
            Case "P153067"
                GeneratePassword(SelectedIds)
            Case "PI02014" '-------------------------------------
                PostPurchase(IDs)
            Case "PI04005", "PI04007"
                ApprovePurchase(IDs)
            Case "PI04013"
                ApproveTenancy(IDs)
            Case "PI02012", "PI02016"
                BuyerAcceptRelease(IDs)
            Case "FD00017"
                VisaProcess(IDs)
            Case "T200135"
                FOCJobs(IDs)
            Case "T200125"
                CancelJobs(IDs)
            Case "T200140"
                InvoiceJobs(SelectedIds)
            Case "T200141"
                ApproveJobs(IDs)
            Case "T200200"
                PrintProforma(IDs)
            Case "T200245"
                ApproveNESCreditNote(IDs)
            Case "T200127"
                ConfirmMultipleJobs(IDs)
            Case "PI02017"
                ApproveBookItemMaster(IDs)

            Case "PI04020"
                ApproveHouseWaiver(IDs)
            Case "PI04030"
                ApproveDehiring(IDs)
        End Select
    End Sub
    Private Function ApproveBookItemMaster(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, " update dbo.[BOOK_ITEM_M]  SET [BIM_bAPPROVE]=1 ,[BIM_APPROVED_DATE]='" + CDate(DateTime.Now) + "',[BIM_APPROVED_USER]='" + Session("sUsr_id") + "'  WHERE  BIM_ID in(" & ID & ")")
                'writeWorkFlowTPTH(ID, "Approved", "Book Item Master", "A")
            Next
            gridbind(True)
            lblError.Text = "Items approved successfully"
        End If
    End Function
    Private Function RejectBookItemMaster(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID

                'If (SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT COUNT(BSAD_ITM_ID) FROM [OASIS_PUR_INV].[dbo].Book_SALE_D WHERE 1=1 AND BSAD_ITM_ID = " & ID) >= 1) Then
                clsBookSalesOnline.REJECT_BOOK_ITEM_MASTER(1, ID, Session("sUsr_id"))
                'Else
                'SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, " update dbo.[BOOK_ITEM_M]  SET [BIM_bAPPROVE]=2 ,[BIM_APPROVED_DATE]='" + CDate(DateTime.Now) + "',[BIM_APPROVED_USER]='" + Session("sUsr_id") + "'  WHERE  BIM_ID in(" & ID & ")")
                'End If
                'writeWorkFlowTPTH(ID, "Rejected", "Book Item Master", "R")
            Next
            gridbind(True)

            lblError.Text = "Items not used in Sales transactions are rejected and changes of items used in transactions are reverted"
        End If
    End Function
    Private Function FOCJobs(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='F' where TPT_Deleted=0 and TRN_No=" & ID)
                writeWorkFlowTPTH(ID, "Modified", "Job Order converted to FOC", "F")
            Next
            gridbind(True)
        End If
    End Function
    Private Function ConfirmMultipleJobs(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='' where TPT_Deleted=0 and TRN_No in(" & ID & ")")
                writeWorkFlowTPTH(ID, "Modified", "Job Order converted to Confirmed JOb", "")
            Next
            gridbind(True)
        End If
    End Function
    Private Function ApproveHouseWaiver(ByVal THW_IDs As String) As Boolean
        If THW_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, TCH_ID() As String = THW_IDs.Split("|")
            For Each ID In TCH_ID
                Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverHW(" & ID & ")").ToString.Split(";")
                If strApprover(0) = 500 Then 'finally approved
                    If writeWorkFlowHW(ID, "Approval", "Archived", 0, "C") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TCT_HOUSEWAIVER set THW_APR_ID=500, THW_STATUS='C', THW_STATUS_STR='Housing Waiver:Archived' where THW_ID=" & ID)
                    End If
                Else
                    If writeWorkFlowHW(ID, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TCT_HOUSEWAIVER set THW_STATUS_STR='Approval:with " & strApprover(1) & "', THW_STATUS=case when THW_STATUS='N' then 'S' else THW_STATUS end, THW_APR_ID=" & strApprover(0) & " where thW_id=" & ID)
                    End If
                End If
            Next
            gridbind(True)
        End If
    End Function
    Private Function writeWorkFlowHW(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_THW_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "saveWorkFlowHW", iParms)
        writeWorkFlowHW = iParms(8).Value
    End Function
    Private Function ApproveDehiring(ByVal TCD_IDs As String) As Boolean
        If TCD_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, TCD_ID() As String = TCD_IDs.Split("|")
            For Each ID In TCD_ID
                Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverDH(" & ID & ")").ToString.Split(";")

                If strApprover(0) = 500 Then 'finally approved
                    If writeWorkFlowDH(ID, "Approval", "Archived", 0, "C") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TCT_DEHIRING set TCD_APR_ID=500, TCD_STATUS='C', TCD_STATUS_STR='DeHiring:Archived' where TCD_ID=" & ID)

                    End If
                Else
                    If writeWorkFlowDH(ID, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TCT_DEHIRING set TCD_STATUS_STR='Approval:with " & strApprover(1) & "', TCD_STATUS=case when TCD_STATUS='N' then 'S' else TCD_STATUS end, TCD_APR_ID=" & strApprover(0) & " where TCD_id=" & ID)
                    End If
                End If
            Next
            gridbind(True)
        End If
    End Function
    Private Function writeWorkFlowDH(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TCD_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "saveWorkFlowDH", iParms)
        writeWorkFlowDH = iParms(8).Value
    End Function


    Private Function CancelJobs(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='C' where TPT_Deleted=0 and TRN_No=" & ID)
                writeWorkFlowTPTH(ID, "Modified", "Job Order cancelled", "C")
            Next
            gridbind(True)
        End If
    End Function

    Private Function ApproveJobs(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='S' where TPT_Deleted=0 and TRN_No=" & ID)
                writeWorkFlowTPTH(ID, "Modified", "Job Order Approved and Executed", "S")
            Next
            gridbind(True)
        End If
    End Function

    Private Function InvoiceJobs(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim dtInvoice As New DataTable
            'dtInvoice = Mainclass.getDataTable("select TRN_no, LPO_no, STS_No, TPT_date, TPT_Loc_id, TPT_Veh_id, TPT_Drv_id, TPT_Client_id, TPT_Service_id, TPT_Description, TPT_STSSt_Time, TPT_JobSt_Time, TPT_STSSt_km, TPT_JOBCl_Time, TPT_STSArr_Time, TPT_JobSt_km, TPT_STSArr_km, TPT_JOBCl_km, TPT_Client_RefNo, TPT_Remarks, TPT_Add_Service, TPT_Xtra_Hrs, TPT_Feedback, TPT_QuoteFlag, TPT_QuoteDate, TPT_JobFlag, TPT_JobDate, TPT_InvFlag, TPT_InvDate, TPT_Bsu_id, TPT_BllId, TPT_JobType, TRN_number, TRN_numberstr, TRN_Veh_CatId, TRN_JobnumberStr, TRN_Status, TRN_Salesman, TPT_PickupPoint, TPT_DropOffPoint, TPT_Capacity, TRN_SEAS_ID, TPT_ENQUIRY, TRN_TRL_ID, TPT_SUPPLIER, TPT_DRIVER, TPT_VEHREGNO, TPT_DATA, TPT_SC_VEHTYPE, TPT_SC_CAPACITY, TRN_BATCHDATE, TRN_BATCHSTR, TRN_BATCH, TPT_ADDRESS, TPT_JHD_DOCNO, TPT_STAFFNAME, TPT_FYEAR, TPT_Deleted, TPT_Cancel_Remarks, TPT_bEMAILED, TPT_Declined_Remarks, TPT_PRGM_DIM_CODE, isnull(TPT_TAX_AMOUNT,0)TPT_TAX_AMOUNT, TPT_TAX_NET_AMOUNT, isnull(TPT_TAX_CODE,'NA')TPT_TAX_CODE, isnull(TPT_EMR_CODE,'NA')TPT_EMR_CODE,ISNULL(CASE WHEN vm1.VEH_PLATE_COLOR='Private Hire' THEN 'WHITE BUS' ELSE 'YELLOW BUS' END,CASE WHEN vm2.VEH_PLATE_COLOR='Private Hire' THEN 'WHITE BUS' ELSE 'YELLOW BUS' END )BUS_TYPE from TPTHiring LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id LEFT OUTER JOIN VEHICLE_M VM1 with(nolock) ON VM1.VEH_REGNO=TPTVEHICLE_M.VEH_REGNO LEFT OUTER JOIN VEHICLE_M VM2 with(nolock) ON VM2.VEH_ID=TPT_Veh_id where TPT_Deleted=0 and TRN_no in (select ID  from oasis.dbo.fnSplitMe('" & TRN_IDs & "','|')) order by TPT_Client_id asc", ConnectionManger.GetOASISTRANSPORTConnectionString)
            'dtInvoice = Mainclass.getDataTable("select TRN_no, LPO_no, STS_No, TPT_date, TPT_Loc_id, TPT_Veh_id, TPT_Drv_id, TPT_Client_id, TPT_Service_id, TPT_Description, TPT_STSSt_Time, TPT_JobSt_Time, TPT_STSSt_km, TPT_JOBCl_Time, TPT_STSArr_Time, TPT_JobSt_km, TPT_STSArr_km, TPT_JOBCl_km, TPT_Client_RefNo, TPT_Remarks, TPT_Add_Service, TPT_Xtra_Hrs, TPT_Feedback, TPT_QuoteFlag, TPT_QuoteDate, TPT_JobFlag, TPT_JobDate, TPT_InvFlag, TPT_InvDate, TPT_Bsu_id, TPT_BllId, TPT_JobType, TRN_number, TRN_numberstr, TRN_Veh_CatId, TRN_JobnumberStr, TRN_Status, TRN_Salesman, TPT_PickupPoint, TPT_DropOffPoint, TPT_Capacity, TRN_SEAS_ID, TPT_ENQUIRY, TRN_TRL_ID, TPT_SUPPLIER, TPT_DRIVER, TPT_VEHREGNO, TPT_DATA, TPT_SC_VEHTYPE, TPT_SC_CAPACITY, TRN_BATCHDATE, TRN_BATCHSTR, TRN_BATCH, TPT_ADDRESS, TPT_JHD_DOCNO, TPT_STAFFNAME, TPT_FYEAR, TPT_Deleted, TPT_Cancel_Remarks, TPT_bEMAILED, TPT_Declined_Remarks, TPT_PRGM_DIM_CODE, isnull(TPT_TAX_AMOUNT,0)TPT_TAX_AMOUNT, isnull(TPT_TAX_NET_AMOUNT,0)TPT_TAX_NET_AMOUNT, isnull(TPT_TAX_CODE,'NA')TPT_TAX_CODE, isnull(TPT_EMR_CODE,'NA')TPT_EMR_CODE,case when (case when isnull(vm2.VEH_PLATE_COLOR,'')='' then case when TPT_Veh_id=0 and isnull(TPT_VEHREGNO,'')<>'' then 'Private Hire' else vm1.VEH_PLATE_COLOR  end else vm2.VEH_PLATE_COLOR end)='Private Hire' THEN 'WHITE BUS' ELSE 'YELLOW BUS' END BUS_TYPE,b.INVOICEAMT from TPTHiring left outer join (select sum(inv_AMT)INVOICEAMT,INV_TRN_NO from tpthiringinvoice with(nolock) group by INV_TRN_NO) b on b.inv_trn_no=trn_no LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id LEFT OUTER JOIN VEHICLE_M VM1 with(nolock) ON VM1.VEH_REGNO=TPTVEHICLE_M.VEH_REGNO LEFT OUTER JOIN VEHICLE_M VM2 with(nolock) ON VM2.VEH_ID=TPT_Veh_id where TPT_Deleted=0 and TRN_no in (select ID  from oasis.dbo.fnSplitMe('" & TRN_IDs & "','|')) order by TPT_Client_id asc", ConnectionManger.GetOASISTRANSPORTConnectionString)
            dtInvoice = Mainclass.getDataTable("select TRN_no, LPO_no, STS_No, TPT_date, TPT_Loc_id, TPT_Veh_id, TPT_Drv_id, TPT_Client_id, TPT_Service_id, TPT_Description, TPT_STSSt_Time, TPT_JobSt_Time, TPT_STSSt_km, TPT_JOBCl_Time, TPT_STSArr_Time, TPT_JobSt_km, TPT_STSArr_km, TPT_JOBCl_km, TPT_Client_RefNo, TPT_Remarks, TPT_Add_Service, TPT_Xtra_Hrs, TPT_Feedback, TPT_QuoteFlag, TPT_QuoteDate, TPT_JobFlag, TPT_JobDate, TPT_InvFlag, TPT_InvDate, TPT_Bsu_id, TPT_BllId, TPT_JobType, TRN_number, TRN_numberstr, TRN_Veh_CatId, TRN_JobnumberStr, TRN_Status, TRN_Salesman, TPT_PickupPoint, TPT_DropOffPoint, TPT_Capacity, TRN_SEAS_ID, TPT_ENQUIRY, TRN_TRL_ID, TPT_SUPPLIER, TPT_DRIVER, TPT_VEHREGNO, TPT_DATA, TPT_SC_VEHTYPE, TPT_SC_CAPACITY, TRN_BATCHDATE, TRN_BATCHSTR, TRN_BATCH, TPT_ADDRESS, TPT_JHD_DOCNO, TPT_STAFFNAME, TPT_FYEAR, TPT_Deleted, TPT_Cancel_Remarks, TPT_bEMAILED, TPT_Declined_Remarks, TPT_PRGM_DIM_CODE, isnull(TPT_TAX_AMOUNT,0)TPT_TAX_AMOUNT, isnull(TPT_TAX_NET_AMOUNT,0)TPT_TAX_NET_AMOUNT, isnull(TPT_TAX_CODE,'NA')TPT_TAX_CODE, isnull(TPT_EMR_CODE,'NA')TPT_EMR_CODE,case when (case when isnull(vm2.VEH_PLATE_COLOR,'')='' then case when TPT_Veh_id=0 and isnull(TPT_VEHREGNO,'')<>'' then 'Private Hire' else vm2.VEH_PLATE_COLOR  end else vm2.VEH_PLATE_COLOR end)='Private Hire' THEN 'WHITE BUS' ELSE 'YELLOW BUS' END BUS_TYPE,b.INVOICEAMT from TPTHiring left outer join (select sum(inv_AMT)INVOICEAMT,INV_TRN_NO from tpthiringinvoice with(nolock) group by INV_TRN_NO) b on b.inv_trn_no=trn_no LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id LEFT OUTER JOIN VEHICLE_M VM1 with(nolock) ON VM1.VEH_REGNO=TPTVEHICLE_M.VEH_REGNO LEFT OUTER JOIN VEHICLE_M VM2 with(nolock) ON VM2.VEH_ID=TPT_Veh_id where TPT_Deleted=0 and TRN_no in (select ID  from oasis.dbo.fnSplitMe('" & TRN_IDs & "','|')) order by TPT_Client_id asc", ConnectionManger.GetOASISTRANSPORTConnectionString)
            Dim mRow As DataRow
            Dim PartyCode As String = ""
            Dim i As Integer = 0
            Dim J As Integer = 0.0
            Dim InvAmount As Decimal = 0.0
            Dim InvVATAmount As Decimal = 0.0
            Dim NetAmount As Decimal = 0.0
            Dim bsu_shortcode As String = "STS"
            Dim DifferentClientIDs As Integer = 0
            Dim DifferentLocation As Integer = 0
            Dim DifferentTax As Integer = 0
            Dim DifferentBUSType As Integer = 0
            Dim Location_ID As String = ""
            Dim Client_id As String = ""
            Dim TAX_Code As String = ""
            Dim BUSTYPE As String = ""
            Dim Errormsg As String = ""
            lblError.Text = ""
            If Session("sBsuid") = "900500" Then bsu_shortcode = "BBT"
            For Each mRow In dtInvoice.Rows

                InvAmount = InvAmount + mRow("INVOICEAMT")
                InvVATAmount = InvVATAmount + mRow("TPT_TAX_AMOUNT")
                NetAmount = NetAmount + mRow("TPT_TAX_NET_AMOUNT")

                If J = 0 Then
                    Location_ID = (mRow("TPT_EMR_CODE"))
                    Client_id = (mRow("TPT_Client_id"))
                    TAX_Code = (mRow("TPT_TAX_CODE"))
                    BUSTYPE = (mRow("BUS_TYPE"))
                    If Location_ID = "NA" Then Errormsg = "This Emirate you cannot Invoice,"
                    If Client_id = "" Then Errormsg = "This Customer you cannot Invoice "
                    If TAX_Code = "NA" Then Errormsg = "This TAX Code you cannot Invoice "

                Else
                    If Location_ID <> mRow("TPT_EMR_CODE") Or mRow("TPT_EMR_CODE").ToString = "NA" Then
                        DifferentLocation = DifferentLocation + 1
                    End If
                    If Client_id <> mRow("TPT_Client_id") Or mRow("TPT_Client_id") = "" Then
                        DifferentClientIDs = DifferentClientIDs + 1

                    End If
                    If TAX_Code <> mRow("TPT_TAX_CODE") Or mRow("TPT_TAX_CODE") = "NA" Then
                        DifferentTax = DifferentTax + 1
                    End If
                    If BUSTYPE <> mRow("BUS_TYPE") Or mRow("BUS_TYPE") = "" Then
                        DifferentBUSType = DifferentBUSType + 1
                        Errormsg = "Different BUS Types selected,"
                    End If
                End If
                J = J + 1
            Next

            If InvAmount + InvVATAmount <> NetAmount Then Errormsg = "Amounts are not Matching in the Job Order,"
            If DifferentLocation > 0 Then Errormsg = Errormsg & "Different Emirates selected,"
            If DifferentClientIDs > 0 Then Errormsg = Errormsg & Errormsg & "Different Customers selected,"
            If DifferentTax > 0 Then Errormsg = Errormsg & "Different TAX codes selected,"
            If DifferentBUSType > 0 Then Errormsg = Errormsg & "Different BUS Types selected,"
            If Errormsg <> "" Then
                lblError.Text = Errormsg.ToString
                Exit Function
            End If
            Try
                For Each mRow In dtInvoice.Rows
                    Dim BatchID As Integer
                    Dim Count As Integer
                    If i = 0 Then
                        PartyCode = mRow("TPT_Client_id")
                        Count = Mainclass.getDataValue("select count(*) from TPTHiring where TPT_Deleted=0  and TPT_Bsu_id ='" & Session("sBsuid") & "' and left(trn_batchstr,2)=right('" & Session("F_YEAR") & "',2)", "OASIS_TRANSPORTConnectionString")
                        If Count = 0 Then
                            BatchID = 1
                        Else
                            BatchID = Mainclass.getDataValue("SELECT TOP 1 RIGHT(TRN_BATCHSTR,6) +1 from TPTHiring where TPT_Deleted=0  and TPT_Bsu_id ='" & Session("sBsuid") & "' and left(trn_batchstr,2)=right('" & Session("F_YEAR") & "',2) order by TRN_BATCHSTR desc", "OASIS_TRANSPORTConnectionString")
                        End If
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='I',TRN_BATCH=" & BatchID & ",TRN_BATCHDATE=GETDATE(),TRN_BATCHSTR='" & Right(Session("F_YEAR"), 2) & bsu_shortcode & "-'+ oasis.dbo.fN_GetPadded(cast(isnull(" & BatchID & ",0) AS varchar),'',6,0) where TPT_Deleted=0 and TRN_No=" & mRow("TRN_NO"))
                        writeWorkFlowTPTH(BatchID, "Modified", "Job Order Invoiced, Batch ID=" & BatchID, "I")
                    Else
                        If PartyCode = mRow("TPT_Client_id") Then
                            SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='I',TRN_BATCH=" & BatchID & ",TRN_BATCHDATE=GETDATE(),TRN_BATCHSTR='" & Right(Session("F_YEAR"), 2) & bsu_shortcode & "-'+oasis.dbo.fN_GetPadded(cast(isnull(" & BatchID & ",0) AS varchar),'',6,0) where TPT_Deleted=0 and TRN_No=" & mRow("TRN_NO"))
                            writeWorkFlowTPTH(BatchID, "Modified", "Job Order Invoiced, Batch ID=" & BatchID, "I")
                        Else
                            BatchID = Mainclass.getDataValue("select max(isnull(TRN_BATCH,0))+1 from TPTHiring where TPT_Deleted=0 and TPT_Bsu_id ='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                            SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TPTHiring set TRN_STATUS='I',TRN_BATCH=" & BatchID & ",TRN_BATCHDATE=GETDATE(),TRN_BATCHSTR='" & Right(Session("F_YEAR"), 2) & bsu_shortcode & "-'+oasis.dbo.fN_GetPadded(cast(isnull(" & BatchID & ",0) AS varchar),'',6,0) where TPT_Deleted=0 and TRN_No=" & mRow("TRN_NO"))
                            writeWorkFlowTPTH(BatchID, "Modified", "Job Order Invoiced, Batch ID=" & BatchID, "I")
                            PartyCode = mRow("TPT_Client_id")
                        End If
                    End If
                    i = i + 1
                Next
                gridbind(True)
                lblError.Text = "Job Orders Invoiced Sucessfully...!"
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        End If
    End Function



    'Private Function VisaProcess(ByVal TRN_IDs As String) As Boolean
    '    If TRN_IDs <> "" Then
    '        Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
    '        Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
    '        For Each ID In PRF_ID
    '            If ButtonText1 = "Process" Then
    '                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update VISTRANS set TRN_STATUS=1, TRN_STATUS_DATE=getdate() where TRN_ID=" & ID)
    '            Else
    '                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update VISTRANS set TRN_STATUS=0, TRN_STATUS_DATE=getdate() where TRN_ID=" & ID)
    '            End If
    '        Next
    '        gridbind(True)
    '    End If
    'End Function

    Private Function VisaProcess(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                If ButtonText1 = "Process" Then
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update VISTRANS set TRN_STATUS=1, TRN_STATUS_DATE=getdate() where TRN_ID=" & ID)
                Else
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update VISTRANS set TRN_STATUS=0, TRN_STATUS_DATE=getdate() where TRN_ID=" & ID)
                End If
            Next
            gridbind(True)
        End If
    End Function

    Private Function BuyerAcceptRelease(ByVal PRF_IDs As String) As Boolean
        If PRF_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, PRF_ID() As String = PRF_IDs.Split("|")
            For Each ID In PRF_ID
                Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverMain(" & ID & ")").ToString.Split(";")
                If ButtonText1 = "Release" Then
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set QUO_ID1=0 where QUO_ID2=" & ID & " or PRF_ID=" & ID)
                Else
                    SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set QUO_ID1='" & Session("EmployeeId") & "' where QUO_ID2=" & ID & " or PRF_ID=" & ID)
                End If
            Next
            gridbind(True)
        End If
    End Function

    Private Function ApproveTenancy(ByVal TCH_IDs As String) As Boolean
        If TCH_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, TCH_ID() As String = TCH_IDs.Split("|")
            For Each ID In TCH_ID
                Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverTCT(" & ID & ")").ToString.Split(";")
                If strApprover(0) = 500 Then 'finally approved
                    If writeWorkFlowTCT(ID, "Tenancy", "Tenancy Contract archived", 0, "C") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TCT_H set TCH_APR_ID=500, TCH_STATUS='C', TCH_STATUS_STR='Tenancy:Archived' where TCH_ID=" & ID)
                    End If
                Else
                    If writeWorkFlowTCT(ID, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update TCT_H set TCH_STATUS_STR='Approval:with " & strApprover(1) & "', TCH_STATUS=case when TCH_STATUS='N' then 'S' else TCH_STATUS end, TCH_APR_ID=" & strApprover(0) & " where tch_id=" & ID)
                    End If
                End If
            Next
            gridbind(True)
        End If
    End Function

    Private Function ApprovePurchase(ByVal PRF_IDs As String) As Boolean
        If PRF_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim ID As String, PRF_ID() As String = PRF_IDs.Split("|")
            For Each ID In PRF_ID
                Dim strApprover() As String = SqlHelper.ExecuteScalar(ConnectionString, CommandType.Text, "select cast(GRPA_ID as varchar)+';'+GRPA_DESCR from groupsa where GRPA_ID=dbo.selectApproverMain(" & ID & ")").ToString.Split(";")
                If strApprover(0) = 500 Then 'finally approved
                    If writeWorkFlow(ID, "Procurement", "Procurement Department, PRF archived", 0, "C") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set PRF_APR_ID=500, PRF_STATUS='C', PRF_STATUS_STR='Procurement:PRF Archived' where PRF_ID=" & ID)
                    End If
                Else
                    If writeWorkFlow(ID, "Approval", "Sent for Approval to " & strApprover(1), strApprover(0), "S") = 0 Then
                        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update PRF_H set PRF_STATUS_STR='Approval:with " & strApprover(1) & "', PRF_STATUS=case when PRF_STATUS='N' then 'S' else PRF_STATUS end, PRF_APR_ID=" & strApprover(0) & " where PRF_ID=" & ID)
                    End If
                End If
            Next
            gridbind(True)
        End If
    End Function

    Private Function writeWorkFlow(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_PRF_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "saveWorkFlowMain", iParms)
        writeWorkFlow = iParms(8).Value
    End Function

    Private Function writeWorkFlowTCT(ByVal strPrf As String, ByVal strAction As String, ByVal strDetails As String, ByVal intGrpaID As Integer, ByVal strStatus As String) As Integer
        Dim iParms(8) As SqlClient.SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TCH_ID", strPrf, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_GRPA_ID", intGrpaID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@WRK_COMMENTS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(8).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "saveWorkFlowTCT", iParms)
        writeWorkFlowTCT = iParms(8).Value
    End Function

    Private Function PostPurchase(ByVal PRF_IDs As String) As Boolean
        'If PRF_IDs <> "" Then
        '    lblError.Text = ""
        '    If txtInvNo.Text.Trim.Length = 0 Then lblError.Text &= "Invoice No,"
        '    If txtINVDate.Text.Trim.Length = 0 Then lblError.Text &= "Invoice Date,"
        '    If txtNarration.Text.Trim.Length = 0 Then lblError.Text &= "Narration,"
        '    Dim strConn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        '    Dim sqlParam(4) As SqlParameter
        '    sqlParam(1) = Mainclass.CreateSqlParameter("@GRN_IDs", PRF_IDs.Replace("|", ","), SqlDbType.VarChar)
        '    sqlParam(2) = Mainclass.CreateSqlParameter("@GRN_INVNO", txtInvNo.Text, SqlDbType.VarChar)
        '    sqlParam(3) = Mainclass.CreateSqlParameter("@GRN_INVDATE", txtINVDate.Text, SqlDbType.VarChar)
        '    sqlParam(4) = Mainclass.CreateSqlParameter("@GRN_INVREMARKS", txtNarration.Text, SqlDbType.VarChar)
        '    Try
        '        Mainclass.ExecuteParamQRY(strConn, "PostGRN", sqlParam)
        '        SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "insert into WORKFLOW (WRK_USERNAME, WRK_ACTION, WRK_DETAILS, WRK_PRF_ID, WRK_GRPA_ID, WRK_STATUS) select '" & Session("sUsr_name") & "','" & "Invoice" & "','" & "Invoice Generated" & "',GRN_PRF_ID,0,'I' from GRN_H where GRN_ID in (" & PRF_IDs.Replace("|", ",") & ")")
        '        tblPurchase.Visible = False
        '    Catch ex As Exception

        '    End Try
        '    gridbind(True)
        'End If
    End Function

    Protected Sub ListButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton2.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim lblTotal As Label
        Dim lblAmount As Label
        Dim lblTAX As Label

        Dim IDs As String = ""
        Dim SelectedAmount As Decimal = 0.0
        Dim SelectedTAX As Decimal = 0.0
        Dim SelectedTotal As Decimal = 0.0

        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
            End If
        Next
        If MainMenuCode = "T200140" Then
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                If chkControl.Checked Then
                    lblAmount = TryCast(grow.FindControl("lblItemCol6"), Label)
                    lblTAX = TryCast(grow.FindControl("lblItemCol7"), Label)
                    lblTotal = TryCast(grow.FindControl("lblItemCol8"), Label)

                    If lblTotal.Text <> "" Then
                        SelectedTotal = SelectedTotal + Val(lblTotal.Text)
                    End If
                    If lblAmount.Text <> "" Then
                        SelectedAmount = SelectedAmount + Val(lblAmount.Text)
                    End If
                    If lblTAX.Text <> "" Then
                        SelectedTAX = SelectedTAX + Val(lblTAX.Text)
                    End If


                End If
            Next
        End If

        Dim SelectedIds As String = ""
        RememberOldValues()
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If


        If IDs = "" Then
            lblError.Text = "No Item Selected " '!!!
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "P153067"
                IDs &= IIf(IDs <> "", "|", "") & SelectedIds
                PrintPasswords(IDs)
            Case "T200140"
                lblError.Text = " Amount: " & SelectedAmount.ToString & " ,TAX: " & SelectedTAX.ToString & " , Net Amount : " & SelectedTotal.ToString
            Case "PI02017"
                RejectBookItemMaster(IDs)
                lblError.Text = "Items not used in Sales transactions are rejected and changes of items used in transactions are reverted"
        End Select
    End Sub
    Private Function PrintBarcode(ByVal ASM_IDs As String) As Boolean
        Try
            If ASM_IDs <> "" Then
                Dim sqlParam(0) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@ASM_IDs", ASM_IDs, SqlDbType.VarChar)
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
                Dim dtBarcode As New DataTable
                dtBarcode = Mainclass.getDataTable("[ASSETS].[RPT_PrintAssetBarcode]", sqlParam, ConnectionManger.GetOASIS_ASSETConnectionString)
                Dim mRow As DataRow
                For Each mRow In dtBarcode.Rows
                    barcode.Number = mRow("ASM_CODE")
                    barcode.CustomText = mRow("ASM_CODE")
                    barcode.ChecksumAdd = True
                    barcode.NarrowBarWidth = 3
                    barcode.Height = 400
                    barcode.FontHeight = 0.2F
                    barcode.ForeColor = Drawing.Color.Black
                    Dim b As Byte()
                    ReDim b(barcode.Render(ImageType.Png).Length)
                    b = barcode.Render(ImageType.Png)
                    mRow("ImgBarCode") = b
                Next
                Session("ReportClassSource") = Nothing
                Dim RepClass As New ReportClass
                Dim strResourseName As String = String.Empty
                strResourseName = "../Asset/Reports/Rpt/rptPrintAssetBarcode.rpt"
                RepClass.ResourceName = strResourseName
                RepClass.ReportData = dtBarcode
                Session("ReportClassSource") = RepClass
                Response.Redirect("PrintViewer.aspx", True)
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Function GeneratePassword(ByVal EMP_IDs As String) As Boolean
        If EMP_IDs <> "" Then
            Dim sql_Str As String
            Dim EMP_ID(), ID, Password, EncrPassword As String
            EMP_ID = EMP_IDs.Split("|")
            Dim passwordEncr As New Encryption64
            Dim strConn As String = ConnectionManger.GetOASISConnectionString
            For Each ID In EMP_ID
                sql_Str = "GeneratePasswordForEmployee '" & ID & "'"
                Password = Mainclass.getDataValue(sql_Str, "OASISConnectionString")
                If Password <> "" Then
                    EncrPassword = passwordEncr.Encrypt(Password)
                    Dim sqlParam(1) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@EMP_ID", ID, SqlDbType.VarChar)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@Password", EncrPassword, SqlDbType.VarChar)
                    Mainclass.ExecuteParamQRY(strConn, "UpdatePasswordForEmployee", sqlParam)
                End If
            Next
            lblError.Text = "Password Generated For Selected Employees " '!!!!
            gridbind(True)
        End If
    End Function
    Private Function PrintPasswords(ByVal EMP_IDs As String) As Boolean
        If EMP_IDs <> "" Then
            Try
                Dim sqlParam(0) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@EMP_Ids", EMP_IDs, SqlDbType.VarChar)
                Dim dtPasswords As New DataTable
                dtPasswords = Mainclass.getDataTable("RPT_PrintEmployeePasswords", sqlParam, ConnectionManger.GetOASISConnectionString)
                Dim mRow As DataRow
                Dim passwordEncr As New Encryption64
                For Each mRow In dtPasswords.Rows
                    mRow("EMP_PASSWORD") = passwordEncr.Decrypt(Convert.ToString(mRow("EMP_PASSWORD")).Replace(" ", "+"))
                Next
                Dim RepClass As New ReportClass
                Dim strResourseName As String = String.Empty
                Dim params As New Hashtable




                strResourseName = "../Payroll/Reports/Rpt/rptPrintEmpPwd.rpt"
                RepClass.ResourceName = strResourseName
                RepClass.ReportData = dtPasswords
                Session("ReportClassSource") = RepClass
                Response.Redirect("PrintViewer.aspx", True)
            Catch ex As Exception
                'Response.Redirect(ViewState("ReferrerUrl"))
            End Try
        End If
    End Function
    Private Sub BuildListView()
        Try
            Dim strConn, StrSQL, StrSortCol As String
            strConn = "" : StrSQL = "" : StrSortCol = ""
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            Select Case MainMenuCode
                Case "PI01100", "A150050"
                    CheckBox1Text = "Summary"
                    CheckBox5Text = "Detail"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        StrSQL &= "select  ID,[DOC NO],[DATE],[CURRENCY],[ACCOUNT] from ("
                        ApprStatus = " and (RCD_SLNO = '1') "
                    Else
                        StrSQL &= "select  ID,[DOC NO],[DATE],[CURRENCY],[ACCOUNT], [VCH DOCNO], [VCH DATE], [CHEQUE NO] from ("
                    End If
                    StrSQL &= "SELECT RECONCILIATION_D.RCD_DOCNO ID, RECONCILIATION_D.RCD_DOCNO [DOC NO], replace(convert(varchar(30),RECONCILIATION_D.RCD_DOCDT,106),' ','/') [DATE],  "
                    StrSQL &= "RECONCILIATION_D.RCD_JNL_CUR_ID CURRENCY, (RECONCILIATION_D.RCD_JNL_ACT_ID+' - '+ACCOUNTS_M.ACT_NAME ) as ACCOUNT, RCD_JNL_DOCNO [VCH DOCNO], "
                    StrSQL &= "replace(convert(varchar(30),RECONCILIATION_D.RCD_JNL_DOCDT,106),' ','/') [VCH DATE], RCD_JNL_CHQNO [CHEQUE NO], RECONCILIATION_D.RCD_SLNO "
                    StrSQL &= "FROM RECONCILIATION_D INNER JOIN ACCOUNTS_M ON RECONCILIATION_D.RCD_JNL_ACT_ID = ACCOUNTS_M.ACT_ID "
                    StrSQL &= "WHERE  (RECONCILIATION_D.RCD_bTemp = '0') AND (RECONCILIATION_D.RCD_BSU_ID = '" & Session("sBsuid") & "') "
                    StrSQL &= "AND (RECONCILIATION_D.RCD_JNL_SUB_ID = '007') AND (RECONCILIATION_D.RCD_bDeleted = '0') AND RCD_FYEAR='" & Session("F_YEAR") & "' "
                    StrSQL &= "union all "
                    StrSQL &= "SELECT distinct RECONCILIATION_D.RCD_DOCNO ID, RECONCILIATION_D.RCD_DOCNO [DOC NO], replace(convert(varchar(30),RECONCILIATION_D.RCD_DOCDT,106),' ','/') [DATE],  "
                    StrSQL &= "RECONCILIATION_D.RCD_JNL_CUR_ID CURRENCY, (RECONCILIATION_D.RCD_JNL_ACT_ID+' - '+ACCOUNTS_M.ACT_NAME ) as ACCOUNT, RCN_DOCNO [VCH DOCNO], "
                    StrSQL &= "replace(convert(varchar(30),RCN_DATE,106),' ','/') [VCH DATE], RCN_CHQNO [CHEQUE NO], 0 "
                    StrSQL &= "FROM RECONCILIATION_D INNER JOIN ACCOUNTS_M ON RECONCILIATION_D.RCD_JNL_ACT_ID = ACCOUNTS_M.ACT_ID "
                    StrSQL &= "INNER JOIN RECONCILIATION_N on RCN_RCD_DOCNO=RCD_DOCNO and RCD_BSU_ID=RCN_BSU_ID and RCD_JNL_ACT_ID=RCN_ACT_ID "
                    StrSQL &= "WHERE  (RECONCILIATION_D.RCD_bTemp = '0') AND (RECONCILIATION_D.RCD_BSU_ID = '" & Session("sBsuid") & "') AND (RECONCILIATION_D.RCD_JNL_SUB_ID = '007') "
                    StrSQL &= "AND (RECONCILIATION_D.RCD_bDeleted = '0') AND RCD_FYEAR='" & Session("F_YEAR") & "' "
                    StrSQL &= ") A WHERE 1=1 "
                    StrSQL &= ApprStatus
                    EditPagePath = "../Accounts/accraReconcileAccount.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
                    StrSortCol = "[DATE] DESC"
                    HeaderTitle = "Bank Reconciliation"

                Case "A100090", "FD00015"
                    StrSQL &= "select  ID,[Account Code],[Account Name],[Document Number],[Bill Date], [Pay Date], [Duration] from ("
                    StrSQL &= "select UTI_ID ID,UTI_ACT_ID [Account Code],act_name [Account Name],UTI_DOC_NO [Document Number],"
                    StrSQL &= "UTI_START_DATE [Bill Date],UTI_PAY_DATE [Pay Date],UTI_DUR_TYPE+'-'+cast(UTI_DUR as varchar) [Duration] "
                    StrSQL &= "from Utility inner join ACCOUNTS_M on UTI_ACT_ID=ACT_ID "
                    StrSQL &= "and UTI_BSU_ID='" & Session("sBsuid") & "' and uti_bdeleted=0) a where 1=1"
                    EditPagePath = "../Accounts/UtilityMaster.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Utility Master"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "A100060"
                    StrSQL &= "SELECT CHB_ID, [Cost Center], [Cost Center Description], Parent, MainParent from (SELECT CCS_ID CHB_ID,CCS_ID [Cost Center], CCS_DESCR [Cost Center Description],Parent,MainParent FROM OASISFIN.dbo.vw_OSA_COSTCENTER_S) a where 1=1 "
                    EditPagePath = "../Accounts/AccAddCCS.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "CHB_ID"
                    HeaderTitle = "Cost Centers"

                Case "A200380"
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, "PostCostcenter '" & Session("sBSUid") & "'")
                    ddlFilter.Visible = True
                    CheckBox1Text = "Pending"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and (abs(Amount)<>abs(CC_Amount) or (abs(SL_Amount)>0 and abs(Amount)<>abs(SL_Amount))) "
                    Else
                        ApprStatus = " "
                    End If
                    StrSQL = "SELECT JNL_ID, [Type], [ActType], [Doc No], [Doc Date], [Code], [Account], [Amount] from ("
                    StrSQL &= "select JNL_ID, JHD_DOCTYPE Type, ACT_TYPE ACTTYPE, JHD_DOCNO [Doc No], replace(convert(varchar(30),JNL_DOCDT,106),' ','/') [Doc Date], ACT_ID [Code], ACT_NAME [Account], CONVERT(varchar, CAST(JNL_DEBIT-JNL_CREDIT AS money), 1) [Amount], "
                    StrSQL &= "(select count(*) from journal_d_s where JOURNAL_H.JHD_BSU_ID = JDS_BSU_ID AND JOURNAL_H.JHD_FYEAR = JDS_FYEAR AND "
                    StrSQL &= "JOURNAL_H.JHD_DOCTYPE = JDS_DOCTYPE AND JOURNAL_H.JHD_DOCNO = JDS_DOCNO AND JNL_ACT_ID = JDS_ACT_ID AND JNL_DOCDT = JDS_DOCDT) CCC "
                    StrSQL &= "from journal_h INNER JOIN "
                    StrSQL &= "JOURNAL_D ON JOURNAL_H.JHD_BSU_ID = JOURNAL_D.JNL_BSU_ID AND JOURNAL_H.JHD_FYEAR = JOURNAL_D.JNL_FYEAR AND "
                    StrSQL &= "JOURNAL_H.JHD_DOCTYPE = JOURNAL_D.JNL_DOCTYPE And JOURNAL_H.JHD_DOCNO = JOURNAL_D.JNL_DOCNO "
                    StrSQL &= "inner join ACCOUNTS_M on JNL_ACT_ID=ACT_ID AND ACT_TYPE in ('INCOME','EXPENSES') AND JOURNAL_H.JHD_BSU_ID='" & Session("sBsuid") & "' and JHD_DOCDT>='1-apr-" & Session("F_YEAR") & "' " 'AND JOURNAL_H.JHD_FYEAR='" & Session("F_YEAR") & "' "
                    StrSQL &= "where isnull(JHD_bPOSTED,0)=1 and isnull(JHD_bdeleted,0)=0  ) A WHERE 1=1 "

                    StrSQL = "SELECT JNL_ID, [Type], [ActType], [Doc No], [Doc Date], [Code], [Account], [Amount] Amount, CC_Amount, SL_Amount from ("
                    StrSQL &= "select JNL_ID, JHD_DOCTYPE Type, ACT_TYPE ACTTYPE, JHD_DOCNO [Doc No], replace(convert(varchar(30),JNL_DOCDT,106),' ','/') [Doc Date], ACT_ID [Code], "
                    StrSQL &= "ACT_NAME [Account], JNL_DEBIT-JNL_CREDIT [Amount], "
                    StrSQL &= "isnull((select sum(case when jds_drcr='DR' then jds_amount else -jds_amount end) from journal_d_s "
                    StrSQL &= "where jds_bsu_id=jnl_bsu_id and jds_fyear=jnl_fyear and jds_docno=jnl_docno and act_id=jnl_act_id and jnl_docdt=jds_docdt),0) CC_Amount, "
                    StrSQL &= "isnull((select sum(jsb_amount) from journal_d_s inner join journal_d_sub_alloc on jsb_jds_d=jds_id "
                    StrSQL &= "where jds_bsu_id=jnl_bsu_id and jds_fyear=jnl_fyear and jds_docno=jnl_docno and act_id=jnl_act_id and jnl_docdt=jds_docdt),0) SL_Amount  "
                    StrSQL &= "from journal_h INNER JOIN JOURNAL_D ON JOURNAL_H.JHD_BSU_ID = JOURNAL_D.JNL_BSU_ID AND JOURNAL_H.JHD_FYEAR = JOURNAL_D.JNL_FYEAR "
                    StrSQL &= "AND JOURNAL_H.JHD_DOCTYPE = JOURNAL_D.JNL_DOCTYPE And JOURNAL_H.JHD_DOCNO = JOURNAL_D.JNL_DOCNO "
                    StrSQL &= "inner join ACCOUNTS_M on JNL_ACT_ID=ACT_ID AND ACT_TYPE in ('INCOME','EXPENSES') AND JOURNAL_H.JHD_BSU_ID='" & Session("sBsuid") & "' and JHD_DOCDT>='1-apr-" & Session("F_YEAR") & "' " 'AND JOURNAL_H.JHD_FYEAR='" & Session("F_YEAR") & "' "
                    StrSQL &= "where isnull(JHD_bPOSTED, 0) = 1 And isnull(JHD_bdeleted, 0) = 0"
                    StrSQL &= ") a where 1=1 "

                    StrSQL = "selecT JHD_ID, [Type], [ActType], [Doc No], [Doc Date], [Code], [Account], [Amount], CC_Amount, SL_Amount from ("
                    StrSQL &= "SELECT JHD_ID, [Type], [ActType], JNL_DOCNO [Doc No], replace(convert(varchar(30),JNL_DOCDT,106),' ','/') [Doc Date], ACT_ID [Code], [Account], [Amount] Amount,  "
                    StrSQL &= "isnull((select sum(case when jds_drcr='DR' then jds_amount else -jds_amount end) from journal_d_s where jds_bsu_id=jnl_bsu_id and jds_fyear=jnl_fyear and jds_docno=jnl_docno and act_id=jds_act_id and jnl_docdt=jds_docdt),0) CC_Amount, "
                    StrSQL &= "isnull((select sum(jsb_amount) from journal_d_s inner join journal_d_sub_alloc on jsb_jds_d=jds_id where jds_bsu_id=jnl_bsu_id and jds_fyear=jnl_fyear and jds_docno=jnl_docno and act_id=jds_act_id and jnl_docdt=jds_docdt),0) SL_Amount  "
                    StrSQL &= "from ("
                    StrSQL &= "select JHD_ID, JNL_BSU_ID, JNL_FYEAR, JHD_DOCTYPE [Type], ACT_TYPE ACTTYPE, JNL_DOCNO, JNL_DOCDT, "
                    StrSQL &= "ACT_ID, ACT_NAME [Account], sum(JNL_DEBIT-JNL_CREDIT) [Amount] "
                    StrSQL &= "from journal_h INNER JOIN JOURNAL_D ON JOURNAL_H.JHD_BSU_ID = JOURNAL_D.JNL_BSU_ID AND JOURNAL_H.JHD_FYEAR = JOURNAL_D.JNL_FYEAR "
                    StrSQL &= "AND JOURNAL_H.JHD_DOCTYPE = JOURNAL_D.JNL_DOCTYPE And JOURNAL_H.JHD_DOCNO = JOURNAL_D.JNL_DOCNO "
                    StrSQL &= "inner join ACCOUNTS_M on JNL_ACT_ID=ACT_ID AND ACT_TYPE in ('EXPENSES') AND JOURNAL_H.JHD_BSU_ID='" & Session("sBsuid") & "' and JHD_DOCDT>='1-apr-" & Session("F_YEAR") & "' "
                    StrSQL &= "where(isnull(JHD_bPOSTED, 0) = 1 And isnull(JHD_bdeleted, 0) = 0)"
                    StrSQL &= "group by JHD_ID, JNL_BSU_ID, JNL_FYEAR, JHD_DOCTYPE, ACT_TYPE, JNL_DOCNO, JNL_DOCDT, ACT_ID, ACT_NAME "
                    StrSQL &= ") a ) b where 1=1 and (abs(Amount)<>abs(CC_Amount) or (abs(SL_Amount)>0 and abs(Amount)<>abs(SL_Amount))) "
                    StrSQL &= ApprStatus
                    EditPagePath = "../Accounts/accCCAllocation.aspx"
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = "JHD_ID desc"
                    HeaderTitle = "Cost Centers Allocation"

                Case "A850045"
                    StrSQL &= "select ID, NO, DESCRIPTION, DETAILS from (select CCT_ID ID, CCT_ID NO, CCT_CODE CODE, CCT_DESCR DESCRIPTION, CCT_NARRAT DETAILS from OASISFIN..COSTCENTER_M) a where 1=1 "
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Cost Center Master"

                Case "A200367"
                    StrSQL &= "select ID, [USER], BSU, DETAILS, [STATUS], [DOC SERIES], [UPLOAD DATE], [POST DATE] from ( "
                    StrSQL &= "select UJV_ID ID, UJV_USERID [USER], BSU_SHORTNAME BSU, UJV_NARRATION DETAILS, replace(convert(varchar(30),UJV_UDATE,106),' ','/') [UPLOAD DATE], "
                    StrSQL &= "replace(convert(varchar(30),UJV_PDATE,106),' ','/') [POST DATE], case when UJV_STATUS='U' then 'Uploaded' else 'Posted' end [STATUS], UJV_DOCNO [DOC SERIES], BSU_ID, UJV_DELETED "
                    StrSQL &= "from oasisfin..uploadIJV inner join oasis..businessunit_m on bsu_id=ujv_bsu_id "
                    StrSQL &= ") a where UJV_DELETED=0 and BSU_ID='" & Session("sBsuid") & "' "
                    EditPagePath = "../Accounts/UploadIJV.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "ID desc"
                    HeaderTitle = "Upload Interunit Journal"

                Case "A200368" 'Payment Authorisation Form
                    FilterText1 = "Detailed"
                    Dim ApprStatus As String
                    If Session("sBSUID") = "999998" Then
                        CheckBox1Text = "Forwarded"
                        CheckBox2Text = "Approved"
                        If rad1.Checked Then
                            ApprStatus = " and SPH_DELETED=0 and SPH_STATUS='S' "
                            ApprStatus &= " and '" & Session("sUsr_name") & "' in (select USRA_USR_NAME from USERSA where USRA_GRP_ID=SPH_APR_ID) " 'correct intray
                            ApprStatus &= " and (SPH_BSU_ID in (select case when usra_all=1 then SPH_BSU_ID else USRA_BSU_ID end from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "' and USRA_GRP_ID=SPH_APR_ID)) "
                        ElseIf rad2.Checked Then
                            ApprStatus = " and SPH_DELETED=0 and SPH_STATUS = 'C' "
                            ApprStatus &= " and (SPH_BSU_ID in (select case when usra_all=1 then SPH_BSU_ID else USRA_BSU_ID end from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "' and USRA_GRP_ID=SPH_APR_ID)) "
                        End If
                    Else
                        CheckBox1Text = "New"
                        CheckBox2Text = "Forwarded"
                        CheckBox3Text = "Approved"
                        If rad1.Checked Then
                            ApprStatus = " and SPH_DELETED=0 and SPH_STATUS='N' and SPH_BSU_ID='" & Session("sBSUID") & "' "
                        ElseIf rad2.Checked Then
                            ApprStatus = " and SPH_DELETED=0 and SPH_STATUS='S' and SPH_BSU_ID='" & Session("sBSUID") & "' "
                        ElseIf rad3.Checked Then
                            ApprStatus = " and SPH_DELETED=0 and SPH_STATUS='C' and SPH_BSU_ID='" & Session("sBSUID") & "' "
                        End If
                    End If

                    StrSQL = "select ID,[TRNO],[BUSINESS UNIT],[DATE],[STATUS],[DUE AMOUNT], [PAY AMOUNT] from (select SPH_ID ID,SPH_TRNO [TRNO],SPH_BSU_ID,SPH_DATE DATE,SPH_STATUS_STR [STATUS],BSU_NAME [BUSINESS UNIT],BSU_SHORTNAME [BSU SHORT NAME],PAYAMOUNT [PAY AMOUNT],DUEAMOUNT [DUE AMOUNT] "
                    StrSQL &= " from SPR_H left outer join oasis..BUSINESSUNIT_M on bsu_id=sph_bsu_id"
                    StrSQL &= " left outer join(SELECT SUM(SPD_TOTALDUE) DUEAMOUNT, SUM(SPD_PAYAMOUNT) PAYAMOUNT,SPD_SPH_ID FROM SPR_D  WHERE SPD_STATUS='approved' GROUP BY SPD_SPH_ID)B ON B.SPD_SPH_ID=SPH_ID"
                    StrSQL &= " WHERE SPH_FYEAR='" & Session("F_YEAR") & "'" & ApprStatus & " ) A where 1=1"
                    EditPagePath = "../Accounts/AccSupplierPaymentRun.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
                    StrSortCol = "[BSU SHORT NAME]"
                    HeaderTitle = "Payment Authorisation Form"
                    HideExportButton = True

                Case "A100010", "A100002"
                    CheckBox1Text = "Active"
                    CheckBox2Text = "InActive"
                    CheckBox3Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and Act_BActive = 1 "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and Act_BActive = 0 "
                    End If
                    strConn = ConnectionManger.GetOASISFINConnectionString()
                    If MainMenuCode = "A100002" Then
                        StrSQL = "Select ACT_ID, [ACCOUNT CODE], [ACCOUNT NAME], [ACT TYPE], [BANK CASH] from ("
                    Else
                        StrSQL = "Select ACT_ID, [ACCOUNT CODE], [ACCOUNT NAME], [DAX CODE], [DAX NAME], [ACT TYPE], [BANK CASH] from ("
                    End If
                    StrSQL &= "SELECT AM.ACT_ID, AM.ACT_ID [ACCOUNT CODE],AM.ACT_NAME [ACCOUNT NAME],AM.ACT_SGP_ID ,ASG.SGP_Descr,AM.ACT_TYPE [ACT TYPE], isnull(act_code,'') [DAX CODE], isnull(a.act_name,'') [DAX NAME], "
                    StrSQL &= "CASE AM.ACT_BANKCASH WHEN 'B' THEN 'Bank'  WHEN 'C' THEN 'Cash' WHEN 'N' THEN 'Normal' ELSE 'Not Applicable' END  [BANK CASH],AM.Act_BActive,"
                    StrSQL &= "AM.ACT_CTRLACC ,AM.ACT_Bctrlac , CASE AM.ACT_FLAG WHEN 'S' THEN 'Supliers' WHEN 'C' THEN 'Customers' WHEN 'N' THEN 'Normal' ELSE 'Others' END ACT_FLAG  "
                    StrSQL &= "FROM ACCOUNTS_M AM left outer join ACCSGRP_M ASG on AM.ACT_SGP_ID=ASG.SGP_ID  "
                    StrSQL &= "left outer join  (select act_code, act_name, DOA_OASIS_ACT_ID from oasis_dax.dax.DAX_OASIS_ACCOUNTS_MAPPING "
                    StrSQL &= "inner join oasis_dax.dax.DAX_ACCOUNTS_M on ACT_CODE=DOA_DAX_ACT_CODE) a  on AM.act_id=DOA_OASIS_ACT_ID  "
                    StrSQL &= "WHERE AM.ACT_SGP_ID = ASG.SGP_ID ) a where 1=1 "
                    StrSQL &= ApprStatus
                    StrSortCol = "[ACT_ID]"
                    HeaderTitle = "Chart of Accounts"
                    EditPagePath = "../Accounts/AccEditAccount.aspx"

                Case "PI01010"
                    StrSQL &= "select ID, DESCRIPTION from (select PRI_ID ID, PRI_DESCR DESCRIPTION from PRIORITY) a where 1=1 "
                    EditPagePath = "../Inventory/Priority.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Priority Master"

                Case "PI01020"
                    StrSQL &= "select ID, DESCRIPTION from (select PRT_ID ID, PRT_DESCR DESCRIPTION from PRFTYPE) a where 1=1 "
                    EditPagePath = "../Inventory/PrfType.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "PRF Type"

                Case "PI01030"
                    StrSQL &= "select ID, [NAME], [DESCRIPTION], [STATUS] from (select BYR_ID ID, USR_DISPLAY_NAME [NAME], BYR_DESCR DESCRIPTION, case when BYR_ACTIVE=0 then 'Disabled' else 'Active' end [STATUS] from BYR_H inner join oasis.dbo.users_m on BYR_USR_ID=USR_NAME) a WHERE 1=1 "
                    EditPagePath = "../Inventory/Buyer.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Buyer Master"

                Case "PI01060"
                    StrSQL = "select ID, CODE, SEGMENT from ( "
                    StrSQL &= "SELECT distinct USG_CODE ID, USG_CODE CODE, USG_DESCR SEGMENT FROM ITEM "
                    StrSQL &= "inner JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE INNER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= " ) A WHERE 1=1 "
                    'EditPagePath = "../Inventory/ItemCategory.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Segment"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case "PI01110"
                    StrSQL = "SELECT  ID, CODE, SEGMENT, FAMILY FROM ("
                    StrSQL &= "SELECT distinct USF_CODE ID, USF_CODE CODE, USG_DESCR SEGMENT, USF_DESCR FAMILY FROM ITEM "
                    StrSQL &= "inner JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE INNER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= "INNER JOIN UNSPSC_FAMILY on UNSPSC_COMMODITY.USF_ID=UNSPSC_FAMILY.USF_ID "
                    StrSQL &= " ) A WHERE 1=1 "
                    'EditPagePath = "../Inventory/ItemSubCategory.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Family"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case "PI01120"
                    StrSQL = "SELECT ID,CODE, SEGMENT, FAMILY, CLASS FROM ("
                    StrSQL &= "SELECT distinct UCL_CODE ID, UCL_CODE CODE, USG_DESCR SEGMENT, USF_DESCR FAMILY, UCL_DESCR CLASS FROM ITEM "
                    StrSQL &= "inner JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE INNER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= "INNER JOIN UNSPSC_FAMILY on UNSPSC_COMMODITY.USF_ID=UNSPSC_FAMILY.USF_ID "
                    StrSQL &= "INNER JOIN UNSPSC_CLASS on UNSPSC_CLASS.UCL_ID=UNSPSC_COMMODITY.UCL_ID "
                    StrSQL &= ") A WHERE 1=1 "
                    'EditPagePath = "../Asset/AssetMakeMaster.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "CLASS"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case "PI01130"
                    StrSQL = "SELECT ID,CODE,SEGMENT,FAMILY,CLASS,COMMODITY FROM ("
                    StrSQL &= "SELECT distinct UCL_CODE ID, UCL_CODE CODE, USG_DESCR SEGMENT, USF_DESCR FAMILY, UCL_DESCR CLASS, UCO_DESCR COMMODITY FROM ITEM "
                    StrSQL &= "inner JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE INNER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= "INNER JOIN UNSPSC_FAMILY on UNSPSC_COMMODITY.USF_ID=UNSPSC_FAMILY.USF_ID "
                    StrSQL &= "INNER JOIN UNSPSC_CLASS on UNSPSC_CLASS.UCL_ID=UNSPSC_COMMODITY.UCL_ID "
                    StrSQL &= ") A WHERE 1=1 "
                    'EditPagePath = "../Asset/AssetModelMaster.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "COMMODITY"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case "PI01080", "PI01085"
                    StrSQL &= "select ID, ID CODE, SEGMENT, FAMILY, CLASS, COMMODITY, ITEM from ("
                    StrSQL &= "SELECT distinct ITM_ID ID,ITM_ID CODE,USG_DESCR SEGMENT,USF_DESCR FAMILY,UCL_DESCR CLASS,UCO_DESCR COMMODITY,ITM_DESCR ITEM FROM ITEM "
                    StrSQL &= "LEFT OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE LEFT OUTER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= "LEFT OUTER JOIN UNSPSC_FAMILY on UNSPSC_COMMODITY.USF_ID=UNSPSC_FAMILY.USF_ID "
                    StrSQL &= "LEFT OUTER JOIN UNSPSC_CLASS on UNSPSC_CLASS.UCL_ID=UNSPSC_COMMODITY.UCL_ID"
                    StrSQL &= ") A WHERE 1=1 "
                    EditPagePath = "../Inventory/Item.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Item Master"

                Case "PI01140"
                    StrSQL = "select ID,DESCRIPTION,ISBN,CATEGORY,PUBLICATION from (select ITM_ID id, isnull(ITB_DESCR,ITM_DESCR) DESCRIPTION,ITM_ISBN ISBN,ITM_CATEGORY CATEGORY,ITM_PUBLICATION PUBLICATION from ITEM_SALE left outer join ITEM_SALEBSU on ITM_ID=ITB_ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "') a where 1=1"
                    EditPagePath = "../Inventory/itemsale.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Item Master Sale"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI01165"
                    'StrSQL = "select ID,DESCRIPTION,ISBN,CATEGORY,PUBLICATION from (select ITM_ID id, isnull(ITB_DESCR,ITM_DESCR) DESCRIPTION,ITM_ISBN ISBN,ITM_CATEGORY CATEGORY,ITM_PUBLICATION PUBLICATION from ITEM_SALE left outer join ITEM_SALEBSU on ITM_ID=ITB_ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "') a where 1=1"
                    StrSQL = "select ID, [BOOK NAME], DESCRIPTION,ISBN,CATEGORY,PUBLICATION from (select BIM_ID id,[BIM_BOOK_NAME] [BOOK NAME], BIM_DESCR DESCRIPTION,BIM_ISBN ISBN,BIM_CATEGORY CATEGORY,BIM_PUBLICATION PUBLICATION from BOOK_ITEM_M WITH (NOLOCK) WHERE BIM_BSU_ID='" & Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1) a where 1=1"
                    'EditPagePath = "../Inventory/itemsale.aspx"
                    EditPagePath = "../Inventory/BookSale/BS_Item.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Book Item Master"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI01166"
                    CheckBox1Text = "Summary"
                    CheckBox5Text = "Detail"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        StrSQL &= "select distinct ID,DESCR, [SET ISBN], GRADE  from ("
                    Else
                        StrSQL &= "select ID,ISBN,DESCR,[ITEM DESCR], QTY, PRICE from ("
                    End If
                    'StrSQL &= "select SEH_ID ID,SEH_BSU_ID BSUID ,SEH_DESCR DESCR, isnull(ITB_DESCR,ITM_DESCR) [ITEM DESCR], ITB_SELL PRICE, ITM_ISBN ISBN "
                    'StrSQL &= "from set_H inner join SET_D on SEH_ID=SED_SEH_ID inner join item_sale on SED_ITM_ID=ITM_ID "
                    'StrSQL &= "inner join item_salebsu on ITB_ITM_ID=ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "' where ISNULL(SEH_DELETED,0)=0 and SEH_BSU_ID='" & Session("sBSUID") & "'"
                    'StrSQL &= ") a where 1=1"
                    StrSQL &= "select BSH_ID ID,BSH_BSU_ID BSUID ,BSH_DESCR DESCR, [BIM_DESCR] [ITEM DESCR], [BIM_SELL_PRICE] PRICE, [BIM_ISBN] ISBN,[BSH_ISBN] AS [SET ISBN], [BSH_GRD_ID] GRADE , BSD_QTY QTY "
                    StrSQL &= " from [BOOK_SET_H] WITH (NOLOCK) inner join [BOOK_SET_D] WITH (NOLOCK) on BSH_ID=BSD_BSH_ID inner join [BOOK_ITEM_M] WITH (NOLOCK) on BSD_BIM_ID=BIM_ID AND [BIM_BSU_ID]= BSH_BSU_ID "
                    StrSQL &= "  and [BIM_BSU_ID]='" & Session("sBSUID") & "'  AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1 where ISNULL(BSH_DELETED,0)=0 and BSH_BSU_ID='" & Session("sBSUID") & "'"
                    StrSQL &= ") a where 1=1"
                    'EditPagePath = "../Inventory/setitem.aspx"
                    EditPagePath = "../Inventory/BookSale/BS_ItemSet.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Item Set"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI02017"
                    CheckBox1Text = "Pending to Approve"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    Dim filter_check As String = ""
                    If rad1.Checked Then
                        filter_check = " AND [BIM_bAPPROVE]=0 "
                    End If
                    If rad2.Checked Then
                        filter_check = " AND [BIM_bAPPROVE]=1 "
                    End If
                    If rad3.Checked Then
                        filter_check = " AND [BIM_bAPPROVE]=2 "
                    End If

                    'StrSQL = "select ID,ISBN,DESCRIPTION, [COST PRICE],[SALE PRICE] from (select itb_id id,ITB_ITM_ID ITEMID, "
                    'StrSQL &= "isnull(ITB_DESCR,ITM_DESCR) DESCRIPTION,ITM_ISBN ISBN, ITB_BSU_ID BUSINESSUNIT,ITB_COST [COST PRICE],ITB_SELL [SALE PRICE] "
                    'StrSQL &= "from ITEM_SALEBSU inner join ITEM_SALE on ITEM_SALEBSU.ITb_itm_ID =ITEM_SALE.ITM_ID and ITB_BSU_ID='" & Session("sBsuid") & "') a where 1=1"
                    StrSQL = "select ID,ISBN,[BOOK NAME],DESCRIPTION, [COST PRICE],[SALE PRICE],[TAX CODE] ,[TAX AMOUNT] ,[NET AMOUNT] from (select [BIM_ID] id,[BIM_ID] ITEMID, "
                    StrSQL &= " [BIM_DESCR] DESCRIPTION,[BIM_BOOK_NAME] [BOOK NAME],[BIM_ISBN] ISBN, [BIM_BSU_ID] BUSINESSUNIT,[BIM_COST_PRICE] [COST PRICE],[BIM_SELL_PRICE] [SALE PRICE],[BIM_TAX_CODE] [TAX CODE],[BIM_TAX_AMOUNT] [TAX AMOUNT],[BIM_NET_AMOUNT] [NET AMOUNT]"
                    StrSQL &= "from [BOOK_ITEM_M] WITH (NOLOCK) where [BIM_bDELETE]=0 AND [BIM_BSU_ID]='" & Session("sBsuid") & "' " & filter_check & " ) a where 1=1"
                    'EditPagePath = "../Inventory/itemprice.aspx"
                    EditPagePath = "../Inventory/BookSale/BS_ItemApprove.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Item Price Approval"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                    ShowGridCheckBox = True
                    ButtonText1 = "Approve"
                    ButtonText2 = "Reject"
                    ListButton1.Visible = True
                    ListButton2.Visible = True

                Case "PI01144"
                    StrSQL = "select BSAH_ID AS ID, BSAH_ID AS [Receipt ID],BSAH_NO [Receipt No],BSAH_SOURCE [Source], BSAH_DATE [Date],BSAH_CUST_NO [Customer No],BSAH_CUST_NAME [Customer Name] ,BSAH_NET_AMOUNT [Net Amount] from [dbo].[BOOK_SALE_H] where [BSAH_BSU_ID]='" & Session("sBsuid") & "' "
                    EditPagePath = "../Inventory/BookSale/BS_SalesInvoice.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "BSAH_ID DESC"
                    HeaderTitle = "Book Sales Invoice"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI01162"
                    lblDate.Text = "As on Date:"
                    dateCol.Visible = True
                    If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                    CheckBox1Text = "Summary"
                    CheckBox2Text = "Detailed"
                    If rad1.Checked Then

                        StrSQL &= "select [ITEM ID], [ISBN],[DESCRIPTION],STOCK,[COST VALUE],[SELL VALUE] from ("
                        StrSQL &= "SELECT BSAD_ITM_ID [ITEM ID],BIM_ISBN [ISBN],BIM_BOOK_NAME [DESCRIPTION], sum(case when  BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBsuid") & "' then  -(ISNULL(D.BSAD_QTY,0))  else (ISNULL(D.BSAD_QTY,0)) end) STOCK"
                        StrSQL &= ",sum((case when  BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBsuid") & "' then  -(ISNULL(D.BSAD_QTY,0))  else (ISNULL(D.BSAD_QTY,0)) end) * (BIM_COST_PRICE )) [COST VALUE]"
                        StrSQL &= ",sum((case when BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBsuid") & "' then  -(ISNULL(D.BSAD_QTY,0))  else (ISNULL(D.BSAD_QTY,0)) end) * (BIM_SELL_PRICE )) [SELL VALUE], null [DATE], null [DOC NO]	"
                        StrSQL &= " FROM    [BOOK_SALE_H] H WITH (NOLOCK) INNER JOIN [BOOK_SALE_D] D WITH (NOLOCK) ON D.BSAD_BSAH_ID=H.BSAH_ID	"
                        StrSQL &= " INNER JOIN 	[dbo].[BOOK_ITEM_M] M  WITH (NOLOCK) ON ISNULL(M.BIM_bDELETE,0)=0 AND ISNULL(M.BIM_bSHOW,0)=1 AND D.BSAD_ITM_ID=M.BIM_ID"
                        StrSQL &= " WHERE   1=1 AND ISNULL(BSAH_DELETED,0)=0  AND BSAH_BSU_ID='" & Session("sBsuid") & "' AND BSAH_DATE <='" & txtDate.Text & "' group by BSAD_ITM_ID,BIM_ISBN,BIM_BOOK_NAME)a where 1=1 "
                    ElseIf rad2.Checked Then

                        StrSQL &= "select [ITEM ID],[DOC NO], [DATE], [ISBN],  [DESCRIPTION],STOCK,[COST VALUE],[SELL VALUE] from ("
                        StrSQL &= " SELECT BSAD_ITM_ID [ITEM ID],BIM_ISBN [ISBN],BIM_BOOK_NAME [DESCRIPTION], sum(case when  BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBsuid") & "' then  -(ISNULL(D.BSAD_QTY,0))  else (ISNULL(D.BSAD_QTY,0)) end) STOCK"
                        StrSQL &= ",sum((case when  BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBsuid") & "' then  -(ISNULL(D.BSAD_QTY,0))  else (ISNULL(D.BSAD_QTY,0)) end) * (BIM_COST_PRICE )) [COST VALUE]"
                        StrSQL &= ",sum((case when BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBsuid") & "' then  -(ISNULL(D.BSAD_QTY,0))  else (ISNULL(D.BSAD_QTY,0)) end) * (BIM_SELL_PRICE )) [SELL VALUE]"
                        StrSQL &= ",replace(convert(varchar(30),BSAH_DATE,106),' ','/') [DATE]	,BSAH_NO [DOC NO]"
                        StrSQL &= " FROM    [BOOK_SALE_H] H WITH (NOLOCK) INNER JOIN [BOOK_SALE_D] D WITH (NOLOCK) ON D.BSAD_BSAH_ID=H.BSAH_ID	"
                        StrSQL &= " INNER JOIN 	[dbo].[BOOK_ITEM_M] M  WITH (NOLOCK) ON ISNULL(M.BIM_bDELETE,0)=0 AND ISNULL(M.BIM_bSHOW,0)=1 AND D.BSAD_ITM_ID=M.BIM_ID"
                        StrSQL &= " WHERE   1=1 AND ISNULL(BSAH_DELETED,0)=0   AND BSAH_BSU_ID='" & Session("sBsuid") & "' AND BSAH_DATE <='" & txtDate.Text & "' group by BSAD_ITM_ID,BIM_ISBN,BIM_BOOK_NAME,BSAH_DATE,BSAH_NO)a where 1=1 "

                    End If
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[DATE], [DESCRIPTION]"
                    HeaderTitle = "Book Sale Stock Report"

                Case "PI01163"

                    CheckBox1Text = ""
                    CheckBox2Text = ""

                    If Session("sBSUID").ToString.Trim = "135005" Then
                        StrSQL &= "select  [DESCRIPTION],[SALE QTY],[BUY QTY],[INTERNAL SALE QTY],[RETURN QTY],[STK QTY],[STK VALUE], [SALE VALUE], [UNIT PRICE] , [CATEGORY] from (select [ITEM ID], [DESCRIPTION],CONVERT(int,sum(SALEQTY)) [SALE QTY],CONVERT(int,sum(BUYQTY)) [BUY QTY],CONVERT(int, sum(INTERNALSALEQTY)) [INTERNAL SALE QTY],CONVERT(int,sum(RETURNQTY)) [RETURN QTY]"
                        StrSQL &= ",sum(BUYQTY)+sum(RETURNQTY)-sum(SALEQTY)- sum(INTERNALSALEQTY) [STK QTY]"
                        StrSQL &= ",(sum(BUYQTY)+sum(RETURNQTY)-sum(SALEQTY)- sum(INTERNALSALEQTY))*BIM_COST_PRICE [STK VALUE], (sum(BUYQTY)+sum(RETURNQTY)-sum(SALEQTY)- sum(INTERNALSALEQTY))*BIM_SELL_PRICE [SALE VALUE],BIM_SELL_PRICE [UNIT PRICE],BIM_COST_PRICE,BIM_CATEGORY [CATEGORY] from ("
                        StrSQL &= " SELECT BSAD_ITM_ID [ITEM ID],BIM_BOOK_NAME [DESCRIPTION],case when  BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBSUID") & "' then  (ISNULL(D.BSAD_QTY,0))  else 0 end SALEQTY"
                        StrSQL &= ",case when  BSAH_TYPE='A' AND BSAH_BSU_ID='" & Session("sBSUID") & "' AND (ISNULL(D.BSAD_QTY,0))>0 then  (ISNULL(D.BSAD_QTY,0))  else 0 end BUYQTY"
                        StrSQL &= ",case when  BSAH_TYPE='A' AND BSAH_BSU_ID='" & Session("sBSUID") & "'  AND (ISNULL(D.BSAD_QTY,0))<0  then  (ISNULL(D.BSAD_QTY,0))  *-1 else 0 end INTERNALSALEQTY"
                        StrSQL &= ",case when BSAH_TYPE='R' AND BSAH_BSU_ID='" & Session("sBSUID") & "' then  (ISNULL(D.BSAD_QTY,0))  else 0 end RETURNQTY"
                        StrSQL &= ",BIM_SELL_PRICE,BIM_COST_PRICE,BIM_CATEGORY"
                        StrSQL &= " FROM    [BOOK_SALE_H] H WITH (NOLOCK) INNER JOIN [BOOK_SALE_D] D WITH (NOLOCK) ON D.BSAD_BSAH_ID=H.BSAH_ID"
                        StrSQL &= " INNER JOIN 	[dbo].[BOOK_ITEM_M] M  WITH (NOLOCK) ON ISNULL(M.BIM_bDELETE,0)=0 AND ISNULL(M.BIM_bSHOW,0)=1 AND D.BSAD_ITM_ID=M.BIM_ID"
                        StrSQL &= " WHERE   1=1  AND ISNULL(BSAH_DELETED,0)=0  AND BSAH_BSU_ID='" & Session("sBSUID") & "')a WHERE   1=1   group by [ITEM ID],[DESCRIPTION],BIM_SELL_PRICE,BIM_COST_PRICE ,BIM_CATEGORY  ) b where 1=1" 'group by BSAD_ITM_ID,BSAD_DESCR,BIM_SELL_PRICE,BIM_COST_PRICE 
                    Else
                        StrSQL &= "select [ITEM ID] , [DESCRIPTION],[SALE QTY],[BUY QTY],[RETURN QTY],[STK QTY],[STK VALUE], [SALE VALUE] from (select [ITEM ID], [DESCRIPTION],CONVERT(int,sum(SALEQTY)) [SALE QTY],CONVERT(int,sum(BUYQTY)) [BUY QTY],CONVERT(int,sum(RETURNQTY)) [RETURN QTY]"
                        StrSQL &= ",sum(BUYQTY)+sum(RETURNQTY)-sum(SALEQTY) [STK QTY]"
                        StrSQL &= ",(sum(BUYQTY)+sum(RETURNQTY)-sum(SALEQTY))*BIM_COST_PRICE [STK VALUE], (sum(BUYQTY)+sum(RETURNQTY)-sum(SALEQTY))*BIM_SELL_PRICE [SALE VALUE] from ("
                        StrSQL &= " SELECT BSAD_ITM_ID [ITEM ID],BIM_BOOK_NAME [DESCRIPTION],case when  BSAH_TYPE='S' AND BSAH_BSU_ID='" & Session("sBSUID") & "' then  (ISNULL(D.BSAD_QTY,0))  else 0 end SALEQTY"
                        StrSQL &= ",case when  BSAH_TYPE='A' AND BSAH_BSU_ID='" & Session("sBSUID") & "' then  (ISNULL(D.BSAD_QTY,0))  else 0 end BUYQTY"
                        StrSQL &= ",case when BSAH_TYPE='R' AND BSAH_BSU_ID='" & Session("sBSUID") & "' then  (ISNULL(D.BSAD_QTY,0))  else 0 end RETURNQTY"
                        StrSQL &= ",BIM_SELL_PRICE,BIM_COST_PRICE"
                        StrSQL &= " FROM    [BOOK_SALE_H] H WITH (NOLOCK) INNER JOIN [BOOK_SALE_D] D WITH (NOLOCK) ON D.BSAD_BSAH_ID=H.BSAH_ID"
                        StrSQL &= " INNER JOIN 	[dbo].[BOOK_ITEM_M] M  WITH (NOLOCK) ON ISNULL(M.BIM_bDELETE,0)=0 AND ISNULL(M.BIM_bSHOW,0)=1 AND D.BSAD_ITM_ID=M.BIM_ID"
                        StrSQL &= " WHERE   1=1  AND ISNULL(BSAH_DELETED,0)=0  AND BSAH_BSU_ID='" & Session("sBSUID") & "')a WHERE   1=1   group by [ITEM ID],[DESCRIPTION],BIM_SELL_PRICE,BIM_COST_PRICE  ) b where 1=1" 'group by BSAD_ITM_ID,BSAD_DESCR,BIM_SELL_PRICE,BIM_COST_PRICE 

                    End If



                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[DESCRIPTION]"
                    HeaderTitle = "Book Sale Stock Movement Report"

                Case "PI01167"

                    CheckBox1Text = ""
                    CheckBox2Text = ""
                    'StrSQL &= "'' "       

                    Dim ds5 As New DataSet
                    Dim F_Year As String = ""
                    ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1")
                    F_Year = (ds5.Tables(0).Rows(0)("FYR_ID").ToString())


                    StrSQL &= "SELECT BSAH_ID ,[RECEIPT NO], [TYPE], [DATE] , [STUDENT NO],[STUDENT NAME], [GRADE],[USER],AMOUNT "
                    StrSQL &= "FROM ( select BSAH_ID, BSAH_NO [RECEIPT NO], case when BSAH_type='S' then 'SALE' else 'RETURN' END [TYPE], BSAH_GRADE +'-'+ BSAH_SECTION [GRADE],"
                    StrSQL &= "replace(convert(varchar(30),BSAH_DATE,106),' ','/') [DATE],  BSAH_CUST_NAME [STUDENT NAME], BSAH_CUST_NO [STUDENT NO], "
                    StrSQL &= "CAST(ISNULL(BSAH_TOTAL, 0) + ISNULL(BSAH_TAX_AMOUNT, 0) AS VARCHAR) [AMOUNT], BSAH_USR_NAME [USER]  from OASIS_PUR_INV..BOOK_SALE_H "
                    StrSQL &= "WHERE '" & Session("sBSUID") & "' in (BSAH_BSU_ID) and BSAH_FYEAR='" & F_Year & "'  and BSAH_DELETED=0 and BSAH_POSTED=0 AND BSAH_SOURCE='COUNTER' AND BSAH_TYPE='S'  ) A WHERE 1=1 "
                    NoAddingAllowed = True
                    NoViewAllowed = False
                    EditPagePath = "../Inventory/BookSale/BS_SalesAdjustments.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "BSAH_ID DESC"
                    HeaderTitle = "Book Sale Adjustments"

                Case "PI01164"
                    ddlFilter.Visible = True
                    lblDate.Text = "Date:"
                    dateCol.Visible = True
                    If txtDate.Text = "" Then txtDate.Text = DateAdd("d", -30, Now).ToString("dd/MMM/yyyy")
                    StrSQL &= "SELECT ID, [DOCNO], [DATE], [STUDENT NO], [STUDENT NAME], CONCAT([GRADE],'-', [SECTION]) [GRADE], [DESCRIPTION], [RATE] from ( "
                    StrSQL &= "SELECT BSAH_ID ID, BSAH_NO DOCNO, BSAH_DATE , replace(convert(varchar(30),BSAH_DATE,106),' ','/') [DATE],"
                    StrSQL &= "case when BSAH_TYPE='S' then 1 else -1 end*cast(BSAD_QTY*BSAD_RATE as decimal(12,2)) RATE, "
                    StrSQL &= "BSAH_CUST_NO [STUDENT NO], BSAH_CUST_NAME [STUDENT NAME], BSAH_GRADE [GRADE], BSAH_SECTION [SECTION], isnull(BSAD_DETAILS,BIM_BOOK_NAME) [DESCRIPTION] "
                    StrSQL &= "from [dbo].[BOOK_SALE_H] WITH (NOLOCK) inner join [dbo].[BOOK_SALE_D] WITH (NOLOCK) on BSAH_ID=BSAD_BSAH_ID and BSAH_DELETED=0 and BSAH_TYPE in ('S','R') "
                    StrSQL &= "inner join [dbo].[BOOK_ITEM_M] WITH (NOLOCK) on BIM_ID=BSAD_ITM_ID AND  BSAH_BSU_ID=BIM_BSU_ID WHERE '" & Session("sBSUID") & "' in (BSAH_BSU_ID)"
                    StrSQL &= " AND BSAH_DATE >= DATEADD(DAY,0,'" & txtDate.Text & "')) A WHERE 1=1 "
                    'StrSQL &= " AND BSAH_DATE BETWEEN DATEADD(DAY,-30,'" & txtDate.Text & "') AND '" & txtDate.Text & "') A WHERE 1=1 "

                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "BSAH_DATE DESC "
                    HeaderTitle = "Book Sales Item Wise - (Transactions from " & txtDate.Text & ")"

                Case "PI01150"
                    StrSQL = "select ID,ISBN,DESCRIPTION, [COST PRICE],[SALE PRICE] from (select itb_id id,ITB_ITM_ID ITEMID, "
                    StrSQL &= "isnull(ITB_DESCR,ITM_DESCR) DESCRIPTION,ITM_ISBN ISBN, ITB_BSU_ID BUSINESSUNIT,ITB_COST [COST PRICE],ITB_SELL [SALE PRICE] "
                    StrSQL &= "from ITEM_SALEBSU inner join ITEM_SALE on ITEM_SALEBSU.ITb_itm_ID =ITEM_SALE.ITM_ID and ITB_BSU_ID='" & Session("sBsuid") & "') a where 1=1"
                    EditPagePath = "../Inventory/itemprice.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Item Price"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI01160"
                    CheckBox1Text = "Summary"
                    CheckBox5Text = "Detail"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        StrSQL &= "select distinct ID,BSUID,DESCR from ("
                    Else
                        StrSQL &= "select ID,BSUID,ISBN,DESCR,[ITEM DESCR], PRICE from ("
                    End If
                    StrSQL &= "select SEH_ID ID,SEH_BSU_ID BSUID ,SEH_DESCR DESCR, isnull(ITB_DESCR,ITM_DESCR) [ITEM DESCR], ITB_SELL PRICE, ITM_ISBN ISBN "
                    StrSQL &= "from set_H inner join SET_D on SEH_ID=SED_SEH_ID inner join item_sale on SED_ITM_ID=ITM_ID "
                    StrSQL &= "inner join item_salebsu on ITB_ITM_ID=ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "' where ISNULL(SEH_DELETED,0)=0 and SEH_BSU_ID='" & Session("sBSUID") & "'"
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Inventory/setitem.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Item Set"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI01090"
                    StrSQL &= "select ID, [REFERENCE NO], SUPPLIER, [SUPPLIER NUMBER], DATE, [FROM DATE], [TO DATE] from ("
                    StrSQL &= "SELECT QUO_ID ID, QUO_REF_NO [REFERENCE NO], QUO_NO [SUPPLIER NUMBER], ACT_NAME SUPPLIER, "
                    StrSQL &= "replace(convert(varchar(30),QUO_DATE,106),' ','/') DATE, replace(convert(varchar(30),QUO_FDATE,106),' ','/') [FROM DATE], "
                    StrSQL &= "replace(convert(varchar(30),QUO_TDATE,106),' ','/') [TO DATE] "
                    StrSQL &= "FROM QUOTE_H inner JOIN OASISFIN.dbo.accounts_M ON QUO_SUP_ID=ACT_ID and QUO_DELETED=0) a where 1=1 "
                    EditPagePath = "../Inventory/Quotation.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Framework Agreement"

                Case "PI04001"
                    StrSQL &= "select ID, DESCRIPTION from (select GRPA_ID ID, GRPA_DESCR DESCRIPTION from GROUPSA) a where 1=1 "
                    'EditPagePath = "../Inventory/Brand.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Approval Groups"

                Case "PI04003"
                    StrSQL &= "select ID, [GROUP], EMPLOYEE, [FROM DATE], [BSU], [BSU CODE] from "
                    StrSQL &= "(select usra_ID ID, GRPA_DESCR [GROUP], USR_DISPLAY_NAME EMPLOYEE, BSU_ID [BSU], BSU_SHORTNAME [BSU CODE], "
                    StrSQL &= "replace(convert(varchar(30),USRA_DATE,106),' ','/') [FROM DATE], case when USRA_ENABLE=1 then 'Active' else 'Disabled' end STATUS "
                    StrSQL &= "FROM usersa inner join oasis..businessunit_m on bsu_id=usra_bsu_id inner JOIN groupsa on GRPA_ID=USRA_GRP_ID INNER JOIN OASIS.dbo.USERS_M on USR_ID=USRA_USR_NAME "
                    StrSQL &= ") a where 1=1 "
                    EditPagePath = "../Inventory/AppGrpUsers.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Approval Groups Linking"

                Case "PI05010"
                    StrSQL = "select ACT_ID, SUPPLIER, TERMS, ID, DESCRIPTION, DETAILS, UNIT, PACKING, RATE from "
                    StrSQL &= "(SELECT ACT_ID, ACT_NAME SUPPLIER, QUO_TERMS TERMS, ITM_ID ID, ITM_DESCR DESCRIPTION, ITM_DETAILS DETAILS, ITM_UNIT UNIT, ITM_PACKING PACKING, QUD_RATE RATE "
                    StrSQL &= "FROM QUOTE_H INNER JOIN QUOTE_D ON QUO_ID = QUD_QUO_ID and QUO_DELETED=0 and QUD_DELETED=0 "
                    StrSQL &= "INNER JOIN ITEM ON ITM_ID = QUD_ITM_ID  "
                    StrSQL &= "INNER JOIN OASISFIN.dbo.ACCOUNTS_M ON QUO_SUP_ID = ACT_ID  "
                    StrSQL &= "UNION ALL "
                    StrSQL &= "select '', 'NONFRAMEWORK', '', ASC_ID,ASC_DESCR, ACM_DESCR, '', '', 0"
                    StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID) a where 1=1 "
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "SUPPLIER"
                    HeaderTitle = "Framework List"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case "PI02003", "PI02005" 'Framework Purchase Request Form  
                    ddlFilter.Visible = True
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    NoAddingAllowed = (Session("F_YEAR") < "2016") Or (MainMenuCode = "PI02003") Or (SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from daxbsu where bsuid='" & Session("sBsuid") & "' and prfdate<getdate()") = 1)
                    If rad1.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'N' "
                        If MainMenuCode = "PI02003" Then NoViewAllowed = True
                    ElseIf rad2.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS in ('S','P') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'C' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and PRF_STATUS in ('D', 'R') "
                    End If
                    Dim singleUser As Boolean = False
                    If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from USERSP where USRP_USR_NAME='" & Session("sUsr_name") & "' and USRP_BLOCK=1") = 1 Then
                        ApprStatus &= " and PRF_USR_NAME='" & Session("sUsr_name") & "' "
                        singleUser = True
                    End If
                    FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                        If singleUser Then
                            StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE], [SUPPLIER], [STATUS], ITEM, QTY, MESSAGE, AMOUNT FROM ("
                        Else
                            StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE], [USER], [SUPPLIER], [STATUS], ITEM, QTY, MESSAGE, AMOUNT FROM ("
                        End If
                        'StrSQL &= " select PRF_STATUS_STR [STATUS], PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                        'StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        'StrSQL &= " replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME SUPPLIER, "
                        'StrSQL &= " ITM_DESCR [ITEM], PRD_QTY [QTY], CONVERT(varchar, CAST(PRD_RATE*PRD_QTY AS money), 1) [AMOUNT], PRF_USR_NAME [USER] "
                        'StrSQL &= " from PRF_H inner JOIN PRF_D on PRF_ID=PRD_PRF_ID left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                        'StrSQL &= " left outer JOIN ITEM on ITM_ID=PRD_ITM_ID "
                        StrSQL &= " select PRF_STATUS_STR [STATUS], PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                        StrSQL &= " CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end MESSAGE, "
                        StrSQL &= " replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME SUPPLIER, "
                        StrSQL &= " ITM_DESCR [ITEM], PRD_QTY [QTY], CONVERT(varchar, CAST(PRD_RATE*PRD_QTY AS money), 1) [AMOUNT], PRF_USR_NAME [USER] "
                        StrSQL &= " from PRF_H inner JOIN PRF_D on PRF_ID=PRD_PRF_ID left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                        StrSQL &= " left outer JOIN ITEM on ITM_ID=PRD_ITM_ID "
                    Else
                        If singleUser Then
                            StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE] ,[REQUIRED BY],[SUPPLIER],[STATUS], MESSAGE, AMOUNT FROM ("
                        Else
                            StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE] ,[REQUIRED BY],[USER],[SUPPLIER],[STATUS], MESSAGE, AMOUNT FROM ("
                        End If
                        'StrSQL &= " select PRF_STATUS_STR [STATUS], PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                        'StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        'StrSQL &= " 0 ITEM, 0 QTY, replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], "
                        'StrSQL &= " CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) [AMOUNT], PRF_USR_NAME [USER] "
                        'StrSQL &= " from PRF_H left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "

                        StrSQL &= " select PRF_STATUS_STR [STATUS], PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                        StrSQL &= " CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end MESSAGE, "
                        StrSQL &= " 0 ITEM, 0 QTY, replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], "
                        'StrSQL &= " CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) [AMOUNT], PRF_USR_NAME [USER] "
                        StrSQL &= " PRF_TOTAL  [AMOUNT], PRF_USR_NAME [USER] "
                        StrSQL &= " from PRF_H left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                    End If
                    HeaderTitle = "Framework Purchase Request Form"
                    StrSQL &= " WHERE isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' AND PRF_TYPE='F' "
                    If Not (Session("sroleid") = 257 Or Session("sroleid") = 262 Or Session("sroleid") = 204) Then 'Not Procurement Department
                        StrSQL &= " and PRF_BSU_ID='" & Session("sBsuid") & "' " 'Filter by BSU and Filter by Department for Corporate
                        If Session("sBsuid") = "999998" Or Session("sBsuid") = "800030" Or Session("sBsuid") = "900001" Then
                            StrSQL &= " and PRF_BSU_ID+cast(PRF_DPT_ID as varchar)+cast(PRF_SUBDPT_ID as varchar) in (select USRA_BSU_ID+cast(USRA_DPT_ID as varchar)+cast(USRA_SUBDPT_ID as varchar) from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "')  "
                            'StrSQL &= " and PRF_DPT_ID in (select USRA_DPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') "
                            'StrSQL &= " and PRF_SUBDPT_ID in (select USRA_SUBDPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') "
                        End If
                    End If
                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "PRF_ID DESC"
                    Session("prf_id") = 0


                Case "PI02004", "PI02006" 'Non Framework Purchase Request
                    ddlFilter.Visible = True
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    NoAddingAllowed = (Session("F_YEAR") < "2016") Or (MainMenuCode = "PI02004") Or (SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from daxbsu where bsuid='" & Session("sBsuid") & "' and prfdate<getdate()") = 1)
                    If rad1.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'N' "
                        If MainMenuCode = "PI02004" Then NoViewAllowed = True
                    ElseIf rad2.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS in ('P', 'S') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'C' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and PRF_STATUS in ('D', 'R') "
                    End If
                    Dim singleUser As Boolean = False
                    If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from USERSP where USRP_USR_NAME='" & Session("sUsr_name") & "' and USRP_BLOCK=1") = 1 Then
                        ApprStatus &= " and PRF_USR_NAME='" & Session("sUsr_name") & "' "
                        singleUser = True
                    End If

                    FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                        'If singleUser Then
                        '    StrSQL = " SELECT PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],[STATUS],ITEM, QTY, MESSAGE FROM ("
                        'Else
                        '    StrSQL = " SELECT PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, MESSAGE FROM ("
                        'End If
                        'StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, CURR, MESSAGE FROM ("
                        'StrSQL &= " select prf_alt_cur_id CURR, PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        'StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        'StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], PRD_DETAILS [ITEM], PRD_QTY [QTY], '' [BUDGET ACCOUNT], CONVERT(varchar, CAST(PRD_RATE*PRD_QTY AS money), 1) [AMOUNT] "
                        'StrSQL &= " from PRF_H P inner JOIN PRF_D on PRF_ID=PRD_PRF_ID inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "

                        StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, CURR, MESSAGE FROM ("
                        StrSQL &= " select prf_alt_cur_id CURR, PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        StrSQL &= " CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end MESSAGE, "
                        'StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], PRD_DETAILS [ITEM], PRD_QTY [QTY], '' [BUDGET ACCOUNT], CONVERT(varchar, CAST(PRD_RATE*PRD_QTY AS money), 1) [AMOUNT] "
                        StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], PRD_DETAILS [ITEM], PRD_QTY [QTY], '' [BUDGET ACCOUNT], PRD_RATE*PRD_QTY  [AMOUNT] "
                        StrSQL &= " from PRF_H P inner JOIN PRF_D on PRF_ID=PRD_PRF_ID inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    Else
                        'StrSQL = " SELECT ~ PRF_ID, [PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS], CURR, [BUDGET ACCOUNT], MESSAGE FROM ("
                        'StrSQL &= " select prf_alt_cur_id CURR, PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        'StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        'StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], a1.ACT_NAME [BUDGET ACCOUNT], 0 ITEM, 0 QTY, CONVERT(varchar, CAST(PRF_ALT_TOTAL AS money), 1) [AMOUNT] "
                        'StrSQL &= " from PRF_H P inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "

                        StrSQL = " SELECT ~ PRF_ID, [PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS], CURR, [BUDGET ACCOUNT], MESSAGE FROM ("
                        StrSQL &= " select prf_alt_cur_id CURR, PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        StrSQL &= " CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end  MESSAGE, "
                        'StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], a1.ACT_NAME [BUDGET ACCOUNT], 0 ITEM, 0 QTY, CONVERT(varchar, CAST(PRF_ALT_TOTAL AS money), 1) [AMOUNT] "
                        StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], a1.ACT_NAME [BUDGET ACCOUNT], 0 ITEM, 0 QTY, PRF_ALT_TOTAL [AMOUNT] "
                        StrSQL &= " from PRF_H P inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    End If
                    HeaderTitle = "Non Framework Purchase Request Form"
                    StrSQL &= " WHERE isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' AND P.PRF_TYPE in ('C','N') "
                    If Not (Session("sroleid") = 257 Or Session("sroleid") = 262 Or Session("sroleid") = 204) Then 'Not Procurement Department
                        StrSQL &= " and PRF_BSU_ID='" & Session("sBsuid") & "' " 'Filter by BSU and Filter by Department
                        If Session("sBsuid") = "999998" Or Session("sBsuid") = "800030" Or Session("sBsuid") = "900001" Then
                            StrSQL &= " and PRF_BSU_ID+cast(PRF_DPT_ID as varchar)+cast(PRF_SUBDPT_ID as varchar) in (select USRA_BSU_ID+cast(USRA_DPT_ID as varchar)+cast(USRA_SUBDPT_ID as varchar) from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "')  "
                            'StrSQL &= " and PRF_DPT_ID in (select USRA_DPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') "
                            'StrSQL &= " and PRF_SUBDPT_ID in (select USRA_SUBDPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') "
                        End If
                    End If

                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "PRF_ID DESC"
                    Session("prf_id") = 0

                Case "PI02007" 'Capex Purchase Request
                    ddlFilter.Visible = True
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    NoAddingAllowed = (Session("F_YEAR") < "2016") Or (SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from daxbsu where bsuid='" & Session("sBsuid") & "' and prfdate<getdate()") = 1)
                    If rad1.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'N' "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS in ('P', 'S') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'C' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and PRF_STATUS in ('D', 'R') "
                    End If
                    Dim singleUser As Boolean = False
                    If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from USERSP where USRP_USR_NAME='" & Session("sUsr_name") & "' and USRP_BLOCK=1") = 1 Then
                        ApprStatus &= " and PRF_USR_NAME='" & Session("sUsr_name") & "' "
                        singleUser = True
                    End If

                    FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                        'If singleUser Then
                        '    StrSQL = " SELECT PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],[STATUS],ITEM, QTY, MESSAGE FROM ("
                        'Else
                        '    StrSQL = " SELECT PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, MESSAGE FROM ("
                        'End If
                        StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, MESSAGE FROM ("
                        StrSQL &= " select PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], PRD_DETAILS [ITEM], PRD_QTY [QTY], '' [BUDGET ACCOUNT], CONVERT(varchar, CAST(PRD_RATE*PRD_QTY AS money), 1) [AMOUNT] "
                        StrSQL &= " from PRF_H P inner JOIN PRF_D on PRF_ID=PRD_PRF_ID inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    Else
                        StrSQL = " SELECT ~ PRF_ID, [PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS], [BUDGET ACCOUNT], MESSAGE FROM ("
                        StrSQL &= " select PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], a1.ACT_NAME [BUDGET ACCOUNT], 0 ITEM, 0 QTY, CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) [AMOUNT] "
                        StrSQL &= " from PRF_H P inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    End If
                    HeaderTitle = "CAPEX Purchase Request Form"
                    StrSQL &= " WHERE isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' AND P.PRF_TYPE='C' "
                    If Not (Session("sroleid") = 204 Or Session("sroleid") = 262 Or Session("sroleid") = 327) Then 'Not Murthy buyer
                        StrSQL &= " and PRF_BSU_ID='" & Session("sBsuid") & "' " 'Filter by BSU and Filter by Department
                        If Session("sBsuid") = "999998" Or Session("sBsuid") = "800030" Or Session("sBsuid") = "900001" Then
                            StrSQL &= " and PRF_BSU_ID+cast(PRF_DPT_ID as varchar)+cast(PRF_SUBDPT_ID as varchar) in (select USRA_BSU_ID+cast(USRA_DPT_ID as varchar)+cast(USRA_SUBDPT_ID as varchar) from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "')  "
                            'StrSQL &= " and PRF_DPT_ID in (select USRA_DPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') "
                            'StrSQL &= " and PRF_SUBDPT_ID in (select USRA_SUBDPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') "
                        End If
                    End If

                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "PRF_ID DESC"
                    Session("prf_id") = 0

                Case "PI02020"
                    StrSQL &= "select ID, [PRF NO], [PRF DATE], [REQUEST DATE], [REQUESTER], [REQUEST DETAILS] from ( "
                    'EditPagePath = "../Inventory/Brand.aspx"
                    StrSQL &= "select PRF_ID ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [PRF DATE], replace(convert(varchar(30),WRK_DATE,106),' ','/') [REQUEST DATE],  "
                    StrSQL &= "usr_display_name [REQUESTER], wrk_details [REQUEST DETAILS] "
                    StrSQL &= "from workflow inner join prf_h on prf_id=wrk_prf_id inner join oasis..users_m on usr_name=wrk_username where wrk_status='A' and prf_request<>'' "
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    NoAddingAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[REQUEST DATE] "
                    HeaderTitle = "Purchase Change Request"

                Case "PI02002", "HD02020" 'Purchase Requisition
                    ddlFilter.Visible = True
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    NoAddingAllowed = (Session("F_YEAR") < "2016") Or (SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from daxbsu where bsuid='" & Session("sBsuid") & "' and prfdate<getdate()") = 1)
                    If rad1.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'N' "
                        If MainMenuCode = "PI02004" Then NoViewAllowed = True
                    ElseIf rad2.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS in ('P', 'S') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'C' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and PRF_STATUS in ('D', 'R') "
                    End If
                    Dim singleUser As Boolean = False
                    If SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from USERSP where USRP_USR_NAME='" & Session("sUsr_name") & "' and USRP_BLOCK=1") = 1 Then
                        ApprStatus &= " and PRF_USR_NAME='" & Session("sUsr_name") & "' "
                        singleUser = True
                    End If

                    FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                        'If singleUser Then
                        '    StrSQL = " SELECT PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],[STATUS],ITEM, QTY, MESSAGE FROM ("
                        'Else
                        '    StrSQL = " SELECT PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, MESSAGE FROM ("
                        'End If
                        StrSQL = " SELECT ~ PRF_ID ,[PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS],ITEM, QTY, MESSAGE FROM ("
                        StrSQL &= " select PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], PRD_DESCR [ITEM], PRD_QTY [QTY], '' [BUDGET ACCOUNT], CONVERT(varchar, CAST(PRD_RATE*PRD_QTY AS money), 1) [AMOUNT] "
                        StrSQL &= " from PRF_H P inner JOIN PRF_D on PRF_ID=PRD_PRF_ID inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    Else
                        StrSQL = " SELECT ~ PRF_ID, [PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS], [BUDGET ACCOUNT], MESSAGE FROM ("
                        StrSQL &= " select PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], "
                        StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], a1.ACT_NAME [BUDGET ACCOUNT], 0 ITEM, 0 QTY, CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) [AMOUNT] "
                        StrSQL &= " from PRF_H P inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    End If
                    HeaderTitle = "Capex Works"
                    StrSQL &= " WHERE '" & Session("sBsuid") & "' in ('900100','999998',PRF_BSU_ID) and isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' AND P.PRF_TYPE='C' "
                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "PRF_ID DESC"
                    Session("prf_id") = 0

                Case "PI02018" 'Purchase List
                    ddlFilter.Visible = True
                    NoAddingAllowed = True
                    StrSQL = " SELECT ~ PRF_ID, [PRF NO],[DATE] ,[SUPPLIER],AMOUNT,[STATUS], [BUDGET ACCOUNT], [BUYER], [FC AMOUNT], [ACTION DATE] FROM ("
                    StrSQL &= " select isnull(EMP_FNAME,'NA') BUYER, PRF_STATUS_STR [STATUS],PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_USR_NAME [USER], bsu_name School, "
                    StrSQL &= " '<font color='''+CASE when PRF_MSG_STATUS='' then '#1B80B6' else case when PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when PRF_MSG_STATUS='' then ' NA' else case when PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                    StrSQL &= " (select replace(convert(varchar(30),max(WRK_DATE),106),' ','/') from workflow where prf_id=wrk_prf_id and prf_status in ('C','S','R','D')) [ACTION DATE], "
                    StrSQL &= " isnull(a2.ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], a1.ACT_NAME [BUDGET ACCOUNT], 0 ITEM, 0 QTY, CONVERT(varchar, CAST(P.PRF_TOTAL AS money), 1) [AMOUNT], "
                    StrSQL &= " PRF_ALT_CUR_ID [FC CURR], CONVERT(varchar, CAST(P.PRF_ALT_TOTAL AS money), 1) [FC Amount] "
                    StrSQL &= " from PRF_H P inner JOIN oasisfin..accounts_m a1 on PRF_ACT_ID=a1.act_id left outer join oasisfin..accounts_m a2 on prf_sup_id=a2.act_id "
                    StrSQL &= " inner join oasis..businessunit_m on bsu_id=prf_bsu_id left OUTER JOIN OASIS..EMPLOYEE_M on EMP_ID=quo_id1 "
                    HeaderTitle = "Purchase Request List"
                    StrSQL &= " WHERE isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' "
                    StrSQL &= " and prf_type in (select trn_type from vw_ViewData where usr_name='" & Session("sUsr_name") & "') "
                    StrSQL &= " and prf_bsu_id in (select case when bsu_id='' then prf_bsu_id else bsu_id end from vw_ViewData where usr_name='" & Session("sUsr_name") & "') "
                    StrSQL &= " and prf_act_id in (select case when act_id='' then prf_act_id else act_id end from vw_ViewData where usr_name='" & Session("sUsr_name") & "') "
                    StrSQL &= " and prf_sup_id in (select case when sup_id='' then prf_sup_id else sup_id end from vw_ViewData where usr_name='" & Session("sUsr_name") & "') "
                    StrSQL &= " ) A WHERE 1=1 "

                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "PRF_ID DESC"

                Case "PI02019" 'Purchase List
                    ddlFilter.Visible = True
                    NoAddingAllowed = True
                    StrSQL = "select prf_id, prf_no [PRF NO], " 'prf_total, prf_type, prf_date, prf_status, " 'msodate, aodate, hosdate, dsadate, byrdate, msoodate, dopdate, bcdate, cfodate, "
                    StrSQL &= " datediff(hh,msodate,aodate) aohr,datediff(hh,aodate,hosdate) hoshr,datediff(hh,hosdate,dsadate) dsahr,datediff(hh,dsadate,byrdate) byrhr, arcdate, "
                    StrSQL &= " datediff(hh,byrdate,msoodate) msoohr,datediff(hh,msoodate,dopdate) dophr,datediff(hh,dopdate,bcdate) bchr,datediff(hh,bcdate,cfodate) cfohr,datediff(hh,isnull(dopdate,isnull(bcdate,hosdate)),arcdate) archr from ("
                    StrSQL &= " select ~ prf_id, prf_no, CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) prf_total, prf_type, prf_date, prf_status,  "
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=20) msodate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=40) aodate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id in (80,160)) hosdate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=180) dsadate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=200) byrdate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=220) msoodate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=280) dopdate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=285) bcdate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status='S' and wrk_grpa_id=300) cfodate,"
                    StrSQL &= " (select top 1 wrk_date from workflow where wrk_prf_id=prf_id and wrk_status in ('C')) arcdate"
                    StrSQL &= " from prf_h where prf_status in ('S', 'C')"
                    StrSQL &= " ) a where 1=1"

                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "prf_id DESC"

                Case "PI02008" 'Goods Receipt Note
                    CheckBox1Text = "Pending PRF"
                    CheckBox2Text = "Goods Receipt"
                    CheckBox2Text = "GRN Summary"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        HeaderTitle = "Pending Purchase Request Form"
                    ElseIf rad2.Checked Then
                        HeaderTitle = "Goods Receipt Note"
                    End If

                    FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                        If rad1.Checked Then
                            StrSQL = " SELECT PRF_ID, [PRF NO],[DATE], [REQUIRED BY], [SUPPLIER], ITEM, [PRF AMT], [GRN AMT], TOTAL FROM ("
                            StrSQL &= " SELECT 0 GRN_ID, '' [GRN NO], PRF_ID, [PRF NO],[DATE], [REQUIRED BY], [SUPPLIER], PENDING [PRF AMT], AMOUNT-PENDING [GRN AMT],ITEM, AMOUNT TOTAL FROM ( "
                            StrSQL &= " select PRF_ID, 0 GRN_ID, '' [GRN NO], PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                            StrSQL &= " CAST(PRF_TOTAL AS money) AMOUNT, replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], "
                            StrSQL &= " cast((select sum(PRD_TOTAL) from pendingPRF where PRD_PRF_ID=PRF_ID) as decimal(12,3)) PENDING, isnull(ITM_DESCR,PRD_DESCR) [ITEM] "
                            StrSQL &= " from PRF_H inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                            StrSQL &= " and PRF_ID in (select DISTINCT PRD_PRF_ID FROM pendingPRF) "
                            StrSQL &= " inner join PRF_D on PRF_ID=PRD_PRF_ID left outer join ITEM on PRD_ITM_ID=ITM_ID "
                            StrSQL &= " WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "')"
                            StrSQL &= " and PRF_OLDNO='' and PRF_DELETED=0 and PRF_STATUS='C' and '" & Session("sBsuid") & "' in (PRF_BSU_ID) ? ) B "
                        ElseIf rad2.Checked Then
                            StrSQL = " SELECT GRN_ID ,[GRN NO],[PRF NO], [DATE] ,[REQUIRED BY],[SUPPLIER], ITEM, TOTAL FROM ("
                            StrSQL &= " select GRN_ID, GRN_NO [GRN NO], replace(convert(varchar(30),GRN_DATE,106),' ','/') [DATE], cast(isnull(PRF_TOTAL,0) as decimal(12,3)) AMOUNT, "
                            StrSQL &= " PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], 0 [PRF AMT], 0 [GRN AMT], "
                            StrSQL &= " cast((select sum(GRD_QTY*GRD_RATE) from GRN_D where GRD_GRN_ID = GRN_ID) as decimal(12,3)) TOTAL, isnull(ITM_DESCR,PRD_DESCR) [ITEM] "
                            StrSQL &= " from GRN_H left JOIN PRF_H on PRF_ID=GRN_PRF_ID and GRN_DELETED=0 "
                            StrSQL &= " left JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                            StrSQL &= " inner join PRF_D on PRF_ID=PRD_PRF_ID left outer join ITEM on PRD_ITM_ID=ITM_ID "
                            StrSQL &= " WHERE PRF_OLDNO='' and PRF_DELETED=0 and isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' and PRF_STATUS='C' and '" & Session("sBsuid") & "' in (PRF_BSU_ID)"
                            StrSQL &= " and PRF_OLDNO='' and PRF_DELETED=0 and PRF_STATUS='C' and '" & Session("sBsuid") & "' in (PRF_BSU_ID) ? "
                        End If
                    Else
                        If rad1.Checked Then
                            StrSQL = " SELECT PRF_ID, [PRF NO],[DATE], [REQUIRED BY], [SUPPLIER], [PRF AMT], [GRN AMT], TOTAL FROM ("
                            StrSQL &= " SELECT 0 GRN_ID, '' [GRN NO], PRF_ID, [PRF NO],[DATE], [REQUIRED BY], [SUPPLIER], "
                            StrSQL &= " CONVERT(varchar, CAST(PENDING AS money), 1) [PRF AMT], CONVERT(varchar, CAST(AMOUNT-PENDING AS money), 1) [GRN AMT], "
                            StrSQL &= " CONVERT(varchar, CAST(AMOUNT  AS money), 1) TOTAL, '' ITEM FROM ( "
                            StrSQL &= " select PRF_ID, 0 GRN_ID, '' [GRN NO], PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                            StrSQL &= " PRF_TOTAL AMOUNT, replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], cast(sum(PRD_TOTAL) as decimal(12,3)) PENDING "
                            StrSQL &= " from PRF_H inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID inner join pendingPRF on prd_prf_id=prf_id "
                            StrSQL &= " WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "') "
                            StrSQL &= " and PRF_OLDNO='' and PRF_DELETED=0 and PRF_STATUS='C' and '" & Session("sBsuid") & "' in (PRF_BSU_ID) ? "
                            StrSQL &= " group by PRF_ID, PRF_NO, replace(convert(varchar(30),PRF_DATE,106),' ','/'),  PRF_TOTAL , replace(convert(varchar(30),PRF_DELDATE,106),' ','/'), ACT_NAME ) B "

                            'StrSQL = " SELECT PRF_ID, [PRF NO],[DATE], [REQUIRED BY], [SUPPLIER], [PRF AMT], [GRN AMT], TOTAL FROM ("
                            'StrSQL &= " SELECT 0 GRN_ID, '' [GRN NO], PRF_ID, [PRF NO],[DATE], [REQUIRED BY], [SUPPLIER], "
                            'StrSQL &= " CONVERT(varchar, CAST(PENDING AS money), 1) [PRF AMT], CONVERT(varchar, CAST(AMOUNT-PENDING AS money), 1) [GRN AMT], "
                            'StrSQL &= " CONVERT(varchar, CAST(AMOUNT  AS money), 1) TOTAL, '' ITEM FROM ( "
                            'StrSQL &= " select PRF_ID, 0 GRN_ID, '' [GRN NO], PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                            'StrSQL &= " PRF_TOTAL AMOUNT, replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], "
                            'StrSQL &= " cast((select sum(PRD_TOTAL) from pendingPRF where PRD_PRF_ID=PRF_ID) as decimal(12,3)) PENDING "
                            'StrSQL &= " from PRF_H inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                            'StrSQL &= " and PRF_ID in (select DISTINCT PRD_PRF_ID FROM pendingPRF) "
                            ''StrSQL &= " WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' or isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "') "
                            'StrSQL &= " WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "') "
                            'StrSQL &= " and PRF_OLDNO='' and PRF_DELETED=0 and PRF_STATUS='C' and '" & Session("sBsuid") & "' in (PRF_BSU_ID) ? ) B "
                        ElseIf rad2.Checked Then
                            StrSQL = " SELECT GRN_ID ,[GRN NO], [PRF NO], [DATE] ,[REQUIRED BY],[SUPPLIER], TOTAL FROM ("
                            StrSQL &= " select GRN_ID, GRN_NO [GRN NO], replace(convert(varchar(30),GRN_DATE,106),' ','/') [DATE], CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) AMOUNT, '' ITEM, "
                            StrSQL &= " PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER], 0 [PRF AMT], 0 [GRN AMT], "
                            StrSQL &= " CONVERT(varchar, CAST((select sum(GRD_QTY*GRD_RATE) from GRN_D where GRD_GRN_ID = GRN_ID) AS money), 1) TOTAL "
                            StrSQL &= " from GRN_H left JOIN PRF_H on PRF_ID=GRN_PRF_ID and GRN_DELETED=0 "
                            StrSQL &= " left JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                            'StrSQL &= " WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' or isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "')"
                            StrSQL &= " WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "')"
                            StrSQL &= " and PRF_OLDNO='' and PRF_DELETED=0 and PRF_STATUS='C' and '" & Session("sBsuid") & "' in (PRF_BSU_ID) ? "
                        End If
                    End If
                    If (Session("sBsuid") = "999998" Or Session("sBsuid") = "800030" Or Session("sBsuid") = "900001" And Session("sroleid") <> 262) Then
                        'StrSQL = StrSQL.Replace("?", " and PRF_DPT_ID in (select USRA_DPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "') and PRF_SUBDPT_ID in (select USRA_SUBDPT_ID from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "')")
                        StrSQL = StrSQL.Replace("?", " and PRF_BSU_ID+cast(PRF_DPT_ID as varchar)+cast(PRF_SUBDPT_ID as varchar) in (select USRA_BSU_ID+cast(USRA_DPT_ID as varchar)+cast(USRA_SUBDPT_ID as varchar) from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "')")
                    Else
                        StrSQL = StrSQL.Replace("?", "")
                    End If
                    StrSQL &= ") A WHERE 1=1 "
                    NoAddingAllowed = True
                    EditPagePath = "../Inventory/GoodsReceipt.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "GRN_ID DESC, PRF_ID DESC"

                Case "PI02010"
                    StrSQL = " SELECT APM_ID,[BUSINESS UNIT],[DEPARTMENT] ,[DOCUMENT]  FROM ("
                    StrSQL &= " SELECT cast(vw_OSO_APPROVAL_M.APM_DOC_ID as varchar) + '_' + cast(vw_OSO_APPROVAL_M.APM_BSU_ID as varchar) + '_' + cast(vw_OSO_APPROVAL_M.APM_DPT_ID as varchar) APM_ID,"
                    StrSQL &= "  vw_OSO_BUSINESSUNIT_M.BSU_NAME [BUSINESS UNIT], VW_OSO_DEPARTMENT_M.DPT_DESCR [DEPARTMENT], "
                    StrSQL &= "  DOCUMENT_M.DOC_NAME [DOCUMENT] "
                    StrSQL &= "  FROM vw_OSO_APPROVAL_M INNER JOIN  vw_OSO_BUSINESSUNIT_M  "
                    StrSQL &= "  ON vw_OSO_APPROVAL_M.APM_BSU_ID = vw_OSO_BUSINESSUNIT_M.BSU_ID INNER JOIN  VW_OSO_DEPARTMENT_M  "
                    StrSQL &= "   ON vw_OSO_APPROVAL_M.APM_DPT_ID = VW_OSO_DEPARTMENT_M.DPT_ID   "
                    StrSQL &= "   INNER JOIN DOCUMENT_M ON vw_OSO_APPROVAL_M.APM_DOC_ID = DOCUMENT_M.DOC_ID  "
                    StrSQL &= " WHERE vw_OSO_APPROVAL_M.APM_BSU_ID='" & Session("sBsuid") & "'"
                    StrSQL &= " GROUP BY vw_OSO_APPROVAL_M.APM_DOC_ID,DOCUMENT_M.DOC_NAME,vw_OSO_APPROVAL_M.APM_BSU_ID,vw_OSO_APPROVAL_M.APM_DPT_ID,vw_OSO_BUSINESSUNIT_M.BSU_NAME,VW_OSO_DEPARTMENT_M.DPT_DESCR"
                    StrSQL &= " ) A WHERE 1=1 "

                    EditPagePath = "../Inventory/InvApprovalmaster.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "APM_ID"
                    HeaderTitle = "Approval Heirarchy"

                Case "PI02012", "PI02016" 'Purchase Request List, Purchase Request List (Buyer)                    
                    ddlFilter.Visible = True
                    CheckBox1Text = "Intray"
                    CheckBox2Text = "Pending"
                    CheckBox3Text = "Print"
                    CheckBox4Text = "Archive"
                    CheckBox5Text = "All Intrays"
                    Dim ApprStatus As String = ""
                    'QUO_ID1=0 means release, else Session("EmployeeId"), QUO_ID2=PRF_ID or h_EntryId.Value in Quotation, QUO_ID3=PRF_ID winning Quotation
                    If rad1.Checked Then
                        ApprStatus = " and P.PRF_STATUS in ('Q','S') AND P.PRF_APR_ID in (0,200) and P.QUO_ID3=0 and P.QUO_ID1>0 and " & Session("EmployeeId") & " in (select P.QUO_ID1)"
                        ButtonText1 = "Release"
                    ElseIf rad2.Checked Then
                        ApprStatus = " and P.PRF_STATUS='S' AND P.PRF_APR_ID=200 and P.QUO_ID1=0 "
                        ButtonText1 = "Accept"
                    ElseIf rad3.Checked Then
                        ApprStatus = " and P.PRF_STATUS = 'S' AND P.PRF_APR_ID=380 "
                        ButtonText1 = ""
                    ElseIf rad4.Checked Then
                        ApprStatus = " and P.PRF_TYPE in ('N') and P.PRF_STATUS = 'C' "
                        ButtonText1 = ""
                    Else
                        ApprStatus = " and P.PRF_STATUS in ('Q','S') AND P.PRF_APR_ID in (0,200) and P.QUO_ID3=0 and P.QUO_ID1>0 and " & Session("EmployeeId") & " in (select emp_id from vw_super)"
                        ButtonText1 = "Release"
                    End If
                    ShowGridCheckBox = Not (ButtonText1 = "")

                    If chkSelection1.Checked Then
                    Else
                        If MainMenuCode = "PI02012" Then
                            StrSQL = " SELECT ~ PRF_ID, [TRN NO], [PRF NO],[PRF DATE] ,[SENT DATE],[SUPPLIER],[PRIORITY], BUYER, AMOUNT, MESSAGE, [OLD PRF NO] FROM ( "
                        Else
                            StrSQL = " SELECT ~ PRF_ID, [TRN NO], [PRF NO],[PRF DATE] ,[SENT DATE],[SUPPLIER],[PRIORITY], AMOUNT, MESSAGE, [OLD PRF NO] FROM ( "
                        End If
                        StrSQL &= " select P.PRF_ID, P.PRF_NO [TRN NO], isnull(PH.PRF_NO,P.PRF_NO) [PRF NO], P.PRF_OLDNO [OLD PRF NO], P.PRF_STATUS_STR STATUS, replace(convert(varchar(30),P.PRF_DATE,106),' ','/') [PRF DATE], "
                        StrSQL &= " '<font color='''+CASE when P.PRF_MSG_STATUS='' then '#1B80B6' else case when P.PRF_MSG_STATUS='A' THEN 'green' ELSE 'red' end end+'''>'+CASE when P.PRF_MSG_STATUS='' then ' NA' else case when P.PRF_MSG_STATUS='A' THEN 'Actioned' ELSE 'Pending' end end+'</font>' MESSAGE, "
                        StrSQL &= " (select replace(convert(varchar(30),max(WRK_DATE),106),' ','/') from workflow where wrk_prf_id=P.PRF_ID) [SENT DATE], "
                        StrSQL &= " isnull(ACT_NAME,'['+left(P.prf_supplier,30)+']') [SUPPLIER], PRI_DESCR [PRIORITY], CONVERT(varchar, CAST(P.PRF_TOTAL AS money), 1) [AMOUNT], isnull(emp_displayname,'None') BUYER "
                        StrSQL &= " from PRF_H P left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=P.PRF_SUP_ID  "
                        StrSQL &= " left OUTER JOIN oasis..EMPLOYEE_M on EMP_ID=P.QUO_ID1 "
                        StrSQL &= " left outer JOIN PRIORITYCAPEX on PRF_PRI_ID=PRI_ID  left outer join PRF_H PH on P.QUO_ID2=PH.PRF_ID "
                    End If
                    HeaderTitle = "Purchase Request List"
                    StrSQL &= " WHERE P.PRF_DELETED=0 and P.PRF_FYEAR<='" & Session("F_YEAR") & "' " 'and P.isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "' "
                    StrSQL &= ApprStatus
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    NoAddingAllowed = True
                    StrSortCol = "PRIORITY desc, cast([SENT DATE] as smalldatetime) "
                    Session("prf_id") = 0
                Case "PI02038"
                    ddlFilter.Visible = True
                    HeaderTitle = "PO Email List"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    StrSQL = " SELECT ~ PRF_ID, [PRF NO],[PRF DATE] ,[PRF SENT],[SUPPLIER],BUYER, [EMAILED TO], TOTAL FROM ( "
                    StrSQL &= " select PRF_ID, PRF_NO [PRF NO], PRF_STATUS_STR STATUS, "
                    StrSQL &= " replace(convert(varchar(30),PRF_DATE,106),' ','/') [PRF DATE], replace(convert(varchar(30),PRM_UPDATED,106),' ','/') [PRF SENT], "
                    StrSQL &= " isnull(ACT_NAME,'['+left(prf_supplier,30)+']') [SUPPLIER], PRI_DESCR [PRIORITY], CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) [TOTAL], "
                    StrSQL &= " isnull(emp_displayname,'None') BUYER, PRM_RESULT [EMAILED TO] "
                    StrSQL &= " from PRF_M inner join PRF_H on prf_id=prm_prf_id left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                    StrSQL &= " left OUTER JOIN oasis..EMPLOYEE_M on EMP_ID=QUO_ID1  "
                    StrSQL &= " left outer JOIN PRIORITYCAPEX on PRF_PRI_ID=PRI_ID  "
                    StrSQL &= " WHERE PRF_DELETED=0 and PRF_FYEAR<='" & Session("F_YEAR") & "' and PRF_TYPE in ('N') and PRF_STATUS = 'C' "
                    StrSQL &= " and PRF_BSU_ID='" & Session("sBsuid") & "' "
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    NoAddingAllowed = True
                    StrSortCol = "cast([PRF DATE] as smalldatetime) desc"

                Case "PI02014" 'Purchase Journal Voucher
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Posted"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " "
                        HeaderTitle = "Purchase Journal Voucher Posting"
                        NoViewAllowed = False
                        EditPagePath = "../Inventory/GoodsReceipt.aspx"
                    ElseIf rad2.Checked Then
                        ApprStatus = " "
                        HeaderTitle = "Posted Purchase Journal Voucher"
                        EditPagePath = "../Inventory/GoodsReceipt.aspx"
                    End If

                    If chkSelection1.Checked Then
                    Else
                        If rad1.Checked Then
                            StrSQL = "SELECT GRN_ID ,[PRF NO], [GRN NO],[DATE] ,[REQUIRED BY],[SUPPLIER NAME],TOTAL  FROM ( "
                            StrSQL &= "select GRN_ID, GRN_NO [GRN NO], replace(convert(varchar(30),GRN_DATE,106),' ','/') [DATE], "
                            StrSQL &= "PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DELDATE,106),' ','/') [REQUIRED BY], ACT_NAME [SUPPLIER NAME], "
                            StrSQL &= "0 DEBIT, 0 CREDIT, '' [PURCHASE NAME], '' PURCHASE, '' [PJV NO], "
                            StrSQL &= "cast((select sum(GRD_QTY*GRD_RATE) from GRN_D where GRD_GRN_ID = GRN_ID) as decimal(12,2)) TOTAL "
                            StrSQL &= "from GRN_H left JOIN PRF_H on PRF_ID=GRN_PRF_ID " '--and PRF_ID not in (select DISTINCT PRD_PRF_ID FROM pendingPRF) 
                            StrSQL &= "left JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID "
                            StrSQL &= "WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "') "
                            StrSQL &= "and GRN_NOPJ=0 and GRN_DELETED=0 and PRF_BSU_ID='" & Session("sBsuid") & "' and PRF_DELETED=0 and PRF_STATUS = 'C' "
                            StrSQL &= "and GRN_PJV_NO is null "
                        Else
                            StrSQL = "SELECT GRN_ID ,[PJV NO],DATE, [PRF NO], [GRN NO], [SUPPLIER NAME], CREDIT, PURCHASE, [PURCHASE NAME], DEBIT FROM ( "
                            StrSQL &= "select GRN_ID GRN_ID, GRN_PJV_NO [PJV NO], GRN_NO [GRN NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], "
                            StrSQL &= "PRF_ID, PRF_NO [PRF NO], ACT1.ACT_NAME [SUPPLIER NAME], 0 TOTAL, '' [REQUIRED BY], "
                            StrSQL &= "cast(sum(GRD_QTY*GRD_RATE) as decimal(12,2)) [CREDIT], PRF_ACT_ID [PURCHASE], act2.ACT_NAME [PURCHASE NAME], cast(sum(GRD_QTY*GRD_RATE) as decimal(12,2)) [DEBIT] "
                            StrSQL &= "from GRN_H inner JOIN GRN_D on GRN_ID=GRD_GRN_ID inner JOIN PRF_H on PRF_ID=GRN_PRF_ID "
                            StrSQL &= "inner JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M ACT1 on ACT_ID=PRF_SUP_ID "
                            StrSQL &= "LEFT OUTER JOIN item on GRD_ITM_ID=ITM_ID LEFT OUTER JOIN ITEM_CATEGORY_M on ITM_ICM_ID=ICM_ID "
                            StrSQL &= "LEFT OUTER JOIN OASISFIN.dbo.ACCOUNTS_M ACT2 ON PRF_ACT_ID=ACT2.ACT_ID "
                            StrSQL &= "WHERE (isnull(PRF_NEWYEAR,PRF_FYEAR)='" & Session("F_YEAR") & "') "
                            StrSQL &= "and GRN_NOPJ=0 and GRN_DELETED=0 and PRF_DELETED=0 and PRF_STATUS='C' and PRF_BSU_ID='" & Session("sBsuid") & "' "
                            StrSQL &= "AND GRN_PJV_NO IS NOT null "
                            StrSQL &= "GROUP by GRN_ID,GRN_PJV_NO, PRF_ID, PRF_NO, GRN_NO, PRF_DATE, PRF_SUP_ID, ACT1.ACT_NAME, PRF_ACT_ID, ACT2.ACT_NAME "
                        End If
                    End If
                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    StrSQL &= " ) A WHERE 1=1 "
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "PRF_ID"
                    NoAddingAllowed = True

                Case "PI04005", "PI04007"
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'S' and PRF_APR_ID not in (200,380) "
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select USRA_USR_NAME from USERSA where USRA_GRP_ID=PRF_APR_ID and case when usra_budget=1 then prf_budgeted else 0 end=0) " 'correct intray
                        'ApprStatus &= " and (PRF_BSU_ID='" & Session("sBsuid") & "' or '" & Session("sUsr_name") 'correct prf bsu or intray bsu can see all
                        'ApprStatus &= "' in (select USRA_USR_NAME from USERSA inner JOIN GROUPSA on GRPA_ID=USRA_GRP_ID where USRA_GRP_ID=PRF_APR_ID AND USRA_ALL=1)) "
                        ApprStatus &= " and (PRF_BSU_ID in (select case when usra_all=1 then PRF_BSU_ID else USRA_BSU_ID end from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "' and USRA_GRP_ID=PRF_APR_ID and USRA_SUBDPT_ID in (0,PRF_SUBDPT_ID) and USRA_DPT_ID in (0,PRF_DPT_ID))) "

                        NoViewAllowed = False
                        ShowGridCheckBox = (SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "select count(*) from daxbsu where bsuid='" & Session("sBsuid") & "' and aprdate<getdate()") = 0)
                        If ShowGridCheckBox Then ButtonText1 = "Approve Purchase"
                        StrSortCol = "PRIORITY desc, MESSAGE, ACTIVITY"
                    ElseIf rad2.Checked Then 'as long as the user is there in workflow no need to check BSU
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS in ('C','S') " 'and PRF_APR_ID not in (200,380)
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflow where WRK_PRF_ID=PRF_ID and WRK_STATUS='S') "
                        'ApprStatus &= " and '" & Session("sBsuid") & "' in (PRF_BSU_ID) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "cast(ACTIVITY as smalldatetime) Desc"
                    ElseIf rad3.Checked Then
                        ApprStatus = " and PRF_DELETED=0 and PRF_STATUS = 'R' " 'and PRF_APR_ID not in (200,380)
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflow where WRK_PRF_ID=PRF_ID and WRK_STATUS in ('S','R')) "
                        'ApprStatus &= " and '" & Session("sBsuid") & "' in (PRF_BSU_ID) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "cast(ACTIVITY as smalldatetime) Desc"
                    Else
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflow where WRK_PRF_ID=PRF_ID and WRK_STATUS in ('S','R')) "
                        'ApprStatus &= " and '" & Session("sBsuid") & "' in (PRF_BSU_ID) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "cast(ACTIVITY as smalldatetime) Desc"
                    End If
                    'FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                    Else
                        If rad4.Checked Then
                            StrSQL = " SELECT PRF_ID,[PRF NO],[DATE],[ACTIVITY],[USER],[SUPPLIER],[BUDGET],[STATUS],[PRIORITY],MESSAGE,AMOUNT FROM ("
                        ElseIf rad1.Checked Then
                            StrSQL = " SELECT distinct PRF_ID,[PRF NO],[DATE],[ACTIVITY],[USER],[SUPPLIER],[BUDGET],[PRIORITY],MESSAGE,AMOUNT FROM ("
                        ElseIf rad2.Checked Then
                            StrSQL = " SELECT PRF_ID,[PRF NO],[DATE],[ACTIVITY],[USER],[SUPPLIER],[BUDGET],[PRIORITY],[ESTIMATE],AMOUNT FROM ("
                        Else
                            StrSQL = " SELECT PRF_ID,[PRF NO],[DATE],[ACTIVITY],[USER],[SUPPLIER],[BUDGET],[PRIORITY],MESSAGE,AMOUNT FROM ("
                        End If
                        StrSQL &= " select PRF_ID, PRF_NO [PRF NO], replace(convert(varchar(30),PRF_DATE,106),' ','/') [DATE], PRF_STATUS_STR STATUS, case when prf_budgeted=0 then 'YES' else 'NO' end BUDGET, case when prf_type='C' then CONVERT(varchar, CAST(PRF_ALT_TOTAL AS money), 1) else 'Na' end [ESTIMATE],"
                        StrSQL &= " '<font color='''+CASE when PRF_MSG_USR='" & Session("sUsr_name") & "' then 'red''>Pending' else case when wrk_prf_id is not null then 'green''>Actioned' else '#1B80B6''>NA' end end+'</font>' MESSAGE, "
                        StrSQL &= " replace(convert(varchar(30),(select max(wrk_date) from workflow where wrk_prf_id=prf_id),106),' ','/') [ACTIVITY], isnull(ACT_NAME,left(prf_supplier,30)) [SUPPLIER], PRF_USR_NAME [USER], '' [ITEM], 0 [QTY], "
                        StrSQL &= " case when PRF_MSG_USR='" & Session("sUsr_name") & "' then '  ' else case when wrk_prf_id is not null then ' ' else '' end end+PRI_DESCR [PRIORITY], CONVERT(varchar, CAST(PRF_TOTAL AS money), 1) [AMOUNT] "
                        StrSQL &= " from PRF_H left outer JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M on ACT_ID=PRF_SUP_ID left outer JOIN PRIORITYCAPEX on PRF_PRI_ID=PRI_ID "
                        StrSQL &= " left outer join workflow on prf_id=wrk_prf_id and wrk_status='M' and wrk_username='" & Session("sUsr_name") & "' "
                        StrSQL &= " WHERE PRF_FYEAR<='" & Session("F_YEAR") & "' "
                    End If
                    If MainMenuCode = "PI04005" Then
                        HeaderTitle = "Framework Purchase Request Form Approval"
                        StrSQL &= " AND PRF_TYPE='F'"
                    Else
                        HeaderTitle = "Non Framework Purchase Request Form Approval"
                        StrSQL &= " AND PRF_TYPE in ('N','C') "
                    End If
                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    NoAddingAllowed = True
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/PurchaseRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    Session("prf_id") = 0

                Case "PI02009", "PI02011", "PI02013", "PI02015" 'Retail Sales\Sales Return Form  
                    ddlFilter.Visible = True
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Posted"
                    CheckBox3Text = "Deleted"
                    CheckBox4Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    NoAddingAllowed = (Session("F_YEAR") < "2016")
                    If rad1.Checked Then
                        ApprStatus = " and SAL_DELETED=0 and SAL_POSTED=0 "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and SAL_DELETED=0 and SAL_POSTED=1 "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and SAL_DELETED=1 "
                    ElseIf rad4.Checked Then
                        ApprStatus = " "
                    End If
                    If MainMenuCode = "PI02009" Then
                        HeaderTitle = "Sales Form"
                        StrSQL = " SELECT SAL_ID ,[SALE NO],[DATE] ,[STUDENT NO],[STUDENT NAME],[USER],AMOUNT FROM ("
                        StrSQL &= " select SAL_ID, SAL_NO [SALE NO], '' [RETURN NO], replace(convert(varchar(30),SAL_DATE,106),' ','/') [DATE], "
                    ElseIf MainMenuCode = "PI02011" Then
                        HeaderTitle = "Sales Return Form"
                        StrSQL = " SELECT SAL_ID ,[RETURN NO], [DATE] , [SALE NO], [STUDENT NO],[STUDENT NAME],[USER],AMOUNT FROM ("
                        StrSQL &= " select SAL_ID, SAL_NO [RETURN NO], SAL_OLDSAL_NO [SALE NO], replace(convert(varchar(30),SAL_DATE,106),' ','/') [DATE], "
                    ElseIf MainMenuCode = "PI02013" Then
                        NoAddingAllowed = True
                        HeaderTitle = "Sales Posting"
                        StrSQL = " SELECT SAL_ID ,[TRAN NO], [TRAN TYPE], [DATE] , [SALE NO], [STUDENT NO],[STUDENT NAME],[USER],AMOUNT FROM ("
                        StrSQL &= " select SAL_ID, SAL_NO [TRAN NO], case when sal_type='S' then 'SALE' else 'RETURN' END [TRAN TYPE], SAL_OLDSAL_NO [SALE NO], replace(convert(varchar(30),SAL_DATE,106),' ','/') [DATE], "
                    Else
                        HeaderTitle = "Stock Adjustment"
                        StrSQL = " SELECT SAL_ID ,[TRAN NO], [DATE] ,[USER],AMOUNT FROM ("
                        StrSQL &= " select SAL_ID, SAL_NO [TRAN NO], replace(convert(varchar(30),SAL_DATE,106),' ','/') [DATE], "
                    End If
                    StrSQL &= " SAL_CUS_NAME [STUDENT NAME], SAL_CUS_NO [STUDENT NO], "
                    StrSQL &= " CAST(ISNULL(SAL_TOTAL, 0) + ISNULL(SAL_TAX_AMOUNT, 0) AS VARCHAR) [AMOUNT], SAL_USR_NAME [USER] "
                    StrSQL &= " from SAL_H "

                    StrSQL &= " WHERE '" & Session("sBsuid") & "' in (SAL_BSU_ID) and SAL_FYEAR='" & Session("F_YEAR")
                    If MainMenuCode = "PI02009" Then
                        StrSQL &= "' AND SAL_TYPE='S' "
                    ElseIf MainMenuCode = "PI02011" Then
                        StrSQL &= "' AND SAL_TYPE='R' "
                    ElseIf MainMenuCode = "PI02015" Then
                        StrSQL &= "' AND SAL_TYPE='A' "
                    Else
                        StrSQL &= "' "
                    End If
                    StrSQL &= ApprStatus
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/SalesRequest.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "SAL_ID DESC"
                    Session("sal_id") = 0

                Case "PI02027" ' Tenancy Contract Form 
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    'NoAddingAllowed = (Session("F_YEAR") < "2014")
                    NoAddingAllowed = (Session("F_YEAR") <= "2014" And Now.Date > "2015-03-31")

                    If rad1.Checked Then
                        ApprStatus = " and TCH_DELETED=0 and TCH_STATUS = 'N' "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and TCH_DELETED=0 and TCH_STATUS in ('S','P') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and TCH_DELETED=0 and TCH_STATUS = 'C' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and TCH_STATUS in ('D', 'R') "
                    End If
                    StrSQL = "SELECT ID,[CONTRACT NO],EMPLOYEE, [STATUS],[RENT],[START DATE],[END DATE], [CONTRACT TYPE],[CITY] FROM ("
                    StrSQL &= "SELECT TCH_ID ID, TCH_NO [CONTRACT NO], TCH_STATUS_STR [STATUS], "
                    StrSQL &= "TCH_CIT_ID, TCH_TOTAL [RENT],TCH_TOTAL, TCH_START [START DATE], TCH_END [END DATE],case when TCH_TYPE='N' then 'New' else 'Renewal' end [CONTRACT TYPE], TCH_ENT_AMOUNT, TCH_ENT_DEDUCTION, TCH_BUDGETED, TCH_TERMS, TCH_RECOVERYPROCESS, "
                    StrSQL &= "TCH_UNIT_PAYING, TCH_OCCUPANTS, TCH_OLDCONTRACT, TCH_REA_ID, TCH_REL_ID, TCH_BSU_ID, TCH_USR_NAME, TCH_FYEAR, TCH_STATUS, TCH_APR_ID, TCH_STATUS_STR, TCH_DELETED, "
                    'StrSQL &= "stuff((select isnull(emp_name,'School') +'<br>' FROM TCT_E left outer join oasis_docs..VW_EMPLOYEE_V on TCE_EMP_ID=EMP_ID where TCH_ID=TCE_TCH_ID and TCE_DELETED=0 "
                    StrSQL &= "stuff((select case when tce_emp_id=0 then 'TBC' else case when emp_id=1 then 'Vacant' else emp_name end end +'<br>' FROM TCT_E left outer join oasis_docs..VW_EMPLOYEE_V on TCE_EMP_ID=EMP_ID where TCH_ID=TCE_TCH_ID and TCE_DELETED=0 "
                    StrSQL &= "for xml path(''), type ).value('.','varchar(max)'), 1, 0, '') EMPLOYEE,RCM_DESCR CITY "
                    StrSQL &= "FROM TCT_H left outer join RECITY_M on RCM_ID=TCH_CIT_ID  WHERE TCH_BSU_ID='" & Session("sBsuid") & "' and TCH_FYEAR='" & Session("F_YEAR") & "' ) a where 1=1"
                    StrSQL &= ApprStatus
                    HeaderTitle = "Tenancy Contract List"
                    StrSortCol = "[CONTRACT NO] DESC"
                    EditPagePath = "../Inventory/TenancyContract.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "PI02035" ' Tenancy Contract List
                    'NoAddingAllowed = True
                    'StrSQL = "SELECT ID,[CONTRACT NO],EMPLOYEE, [STATUS],[RENT],MESSAGE,[START DATE],[END DATE] FROM ("
                    'StrSQL &= "SELECT TCH_ID ID, TCH_NO [CONTRACT NO], TCH_STATUS_STR [STATUS], "
                    'StrSQL &= "TCH_CIT_ID, TCH_TOTAL [RENT],TCH_TOTAL, TCH_START [START DATE], TCH_END [END DATE], TCH_ENT_AMOUNT, TCH_ENT_DEDUCTION, TCH_BUDGETED, TCH_TERMS, TCH_RECOVERYPROCESS, "
                    'StrSQL &= " '<font color='''+CASE when TCH_MSG_USR='" & Session("sUsr_name") & "' then 'red''>Pending' else case when wrk_TCH_id is not null then 'green''>Actioned' else '#1B80B6''>NA' end end+'</font>' MESSAGE, "
                    'StrSQL &= "TCH_UNIT_PAYING, TCH_OCCUPANTS, TCH_OLDCONTRACT, TCH_REA_ID, TCH_REL_ID, TCH_BSU_ID, TCH_USR_NAME, TCH_FYEAR, TCH_STATUS, TCH_APR_ID, TCH_STATUS_STR, TCH_DELETED, "
                    ''StrSQL &= "stuff((select isnull(emp_name,'School') +'<br>' FROM TCT_E left outer join oasis_docs..VW_EMPLOYEE_V on TCE_EMP_ID=EMP_ID where TCH_ID=TCE_TCH_ID and TCE_DELETED=0 "
                    'StrSQL &= "stuff((select case when tce_emp_id=0 then 'TBC' else case when tce_emp_id=1 then 'Vacant' else emp_name end end +'<br>' FROM TCT_E left outer join oasis_docs..VW_EMPLOYEE_V on TCE_EMP_ID=EMP_ID where TCH_ID=TCE_TCH_ID and TCE_DELETED=0 "
                    'StrSQL &= "for xml path(''), type ).value('.','varchar(max)'), 1, 0, '') EMPLOYEE "
                    'StrSQL &= "FROM TCT_H  left outer join workflowTCT on TCH_id=wrk_TCH_id and wrk_status='M' and wrk_username='" & Session("sUsr_name") & "' WHERE TCH_STATUS<>'N' and '" & Session("sUsr_name") & "' in (select usr_name from vw_ViewData where TRN_TYPE='T')) a where 1=1"
                    'HeaderTitle = "Tenancy Contract List"
                    'StrSortCol = "[CONTRACT NO] DESC"
                    'EditPagePath = "../Inventory/TenancyContract.aspx"
                    'strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                    IsStoredProcedure = True
                    StrSQL = "GetTenancyContractList"
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                    StrSortCol = "[CONTRACT NO] DESC"
                    EditPagePath = "../Inventory/TenancyContract.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    HeaderTitle = "Tenancy Contract List"
                    NoAddingAllowed = False

                Case "PI02029" ' Real Estate Agent
                    CheckBox1Text = ""
                    CheckBox2Text = ""
                    StrSQL &= "'' "
                    StrSQL = "SELECT ID,[AGENT NAME],[CONTACT PERSON],[MOBILE NO],[OFFICE NO],[FAX] FROM ("
                    StrSQL &= "select REA_ID ID,REA_NAME [AGENT NAME],REA_CONTACT_PERSON [CONTACT PERSON],REA_MOBILE [MOBILE NO],REA_PHONE [OFFICE NO],REA_FAX [FAX] from REAGENT WHERE REA_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(REA_DELETED,0)=0"
                    StrSQL &= ") a where 1=1"
                    HeaderTitle = "Real Estate Agent List"
                    StrSortCol = "[AGENT NAME] DESC"
                    EditPagePath = "../Inventory/Reagent.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "PI02031" ' Real Estate Landlords
                    CheckBox1Text = ""
                    CheckBox2Text = ""
                    StrSQL &= "'' "
                    StrSQL = "SELECT ID,[LANDLORD NAME],[MOBILE NO],[OFFICE NO],[FAX],[POBOX] FROM ("
                    StrSQL &= "SELECT REL_ID ID, REL_NAME [LANDLORD NAME], REL_MOBILE [MOBILE NO], REL_PHONE [OFFICE NO], REL_FAX [FAX], REL_PAYNAME, REL_DETAILS, REL_POBOX [POBOX] FROM RELANDLORD WHERE REL_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(REL_DELETED,0)=0 "
                    StrSQL &= ") a where 1=1"
                    HeaderTitle = "Real Estate Landlord List"
                    StrSortCol = "[LANDLORD NAME] DESC"
                    EditPagePath = "../Inventory/Relandlord.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "PI02036" ' Building Master
                    CheckBox1Text = ""
                    CheckBox2Text = ""
                    StrSQL &= "'' "
                    StrSQL = "SELECT ID,[BUILDING NAME],[ADDRESS],[DM NO] FROM ("
                    StrSQL &= "select BLD_ID ID,BLD_NAME [BUILDING NAME],BLD_ADDRESS [ADDRESS],BLD_DMNO [DM NO] from BUILDING WHERE BLD_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(BLD_DELETED,0)=0"
                    StrSQL &= ") a where 1=1"
                    HeaderTitle = "BUILDING MASTER"
                    StrSortCol = "[BUILDING NAME]"
                    EditPagePath = "../Inventory/ReBuilding.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "PI02033" ' Real Estate Services
                    CheckBox1Text = ""
                    CheckBox2Text = ""
                    StrSQL &= "'' "
                    StrSQL = "SELECT ID,DESCR,[CITY],[PERCENT],[DEFAULT],[ADD/DED],[REFUNDABLE], IMAGE FROM ("
                    StrSQL &= "SELECT RES_ID ID,RES_DESCR[DESCR],ISNULL(CIT_DESCR,'ALL')[CITY],CASE WHEN REC_PERCENT=1 THEN 'YES' ELSE 'NO' END [PERCENT],REC_DEFAULT[DEFAULT],RES_ADDDED [ADD/DED],CASE WHEN RES_REFUND=1 THEN 'YES' ELSE 'NO' END [REFUNDABLE], case when RES_IMAGE=1 then 'YES' else 'NO' end IMAGE FROM RESERVICES "
                    StrSQL &= "LEFT OUTER JOIN OASIS..CITY_M  ON CIT_CTY_ID=REC_CIT_ID WHERE RES_DELETED=0  "
                    StrSQL &= ") a where 1=1"
                    HeaderTitle = "Additions Deductions List"
                    StrSortCol = "[DESCR] DESC"
                    EditPagePath = "../Inventory/Reservices.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                    'Staff Investment Details
                Case "T200220"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'StrSQL = "SELECT [ID],[EMP ID],[EMPLOYEE NAME],[PRINCIPLE],[PAYMENT],[BALANCE] FROM (select t1.SIF_EMP_ID ID,t1.SIF_EMP_ID [EMP ID],e.EMP_NAME [EMPLOYEE NAME],  t1.PRINCIPLE, coalesce(t2.PRINCIPLE, 0) as [PAYMENT],ISNULL(T1.PRINCIPLE,0) -ISNULL(T2.PRINCIPLE,0) [BALANCE] "
                    'StrSQL &= " from  (SELECT SIF_EMP_ID, sUM(SIF_PAMOUNT) PRINCIPLE FROM TPT_SIF WHERE SIF_TYPE not in('P') GROUP BY SIF_EMP_ID) t1 "
                    'StrSQL &= "left join   (SELECT SUM(SIF_PAMOUNT) PRINCIPLE,SIF_EMP_ID FROM  TPT_SIF WHERE  SIF_TYPE  in('P') GROUP BY SIF_EMP_ID) t2 "
                    'StrSQL &= " on   t1.SIF_EMP_ID = t2.SIF_EMP_ID left outer join oasis_docs..VW_EMPLOYEE_V E on e.EMP_ID =t1.SIF_EMP_ID ) A WHERE 1=1"
                    'EditPagePath = "../Transport/ExtraHiring/tptStaffInvestmentDetails.aspx"
                    'HeaderTitle = "Staff Investment Details"
                    'StrSortCol = "[ID] Desc"
                    'NoViewAllowed = False
                    StrSQL = "SELECT [ID],[EMP ID],[EMP NO],[EMPLOYEE NAME],[Contribution],Interest,[Payment],[TotalWithInte]-[Payment] [Balance] FROM (  "
                    StrSQL &= "select t1.EMPID ID,t1.empid [EMP ID],t1.EMPNO [EMP NO],e.EMP_NAME [EMPLOYEE NAME],(isnull(t3.SALADD,0)+isnull(t4.contr,0)) [Contribution],(isnull(t1.Contribution,0)-(isnull(t3.SALADD,0)+isnull(t4.contr,0)))Interest, "
                    StrSQL &= "isnull(t1.Contribution,0)[TotalWithInte], isnull(t3.SALADD,0)[Contr], isnull(t4.contr,0)[Open], coalesce(t2.PRINCIPLE, 0) as [PAYMENT],(isnull(t1.Contribution,0)) -ISNULL(T2.PRINCIPLE,0) [BALANCE] "
                    StrSQL &= "From (select emp_fname+' '+emp_lname [Employee Name], oasis.dbo.calcStaffContribution(empid,getdate()) [Contribution], posdata..employee.*     from posdata..employee inner join oasis..employee_m on empid=emp_id) t1  "
                    StrSQL &= "left join (SELECT SUM(SIF_PAMOUNT) PRINCIPLE,SIF_EMP_ID FROM  TPT_SIF WHERE  SIF_TYPE  in('P') GROUP BY SIF_EMP_ID) t2  on    t2.SIF_EMP_ID= t1.EMPID "
                    StrSQL &= "left join (select edd_emp_id,sum(edd_amount) SALADD from oasis..vw_OSO_EMPMONTHLY_E_D WHERE  EDD_ERNCODE='SV' and EDD_PAYMONTH>8 and EDD_PAYYEAR>=2015 GROUP by edd_emp_ID) t3  on    t3.EDD_EMP_ID= t1.EMPID  "
                    StrSQL &= "left join (select empid,contr from posdata..employee) t4  on    t4.EMPID= t1.EMPID left outer join oasis_docs..VW_EMPLOYEE_V E on e.EMP_ID =t1.EMPID ) A WHERE 1=1  "
                    EditPagePath = "../Transport/ExtraHiring/tptStaffInvestmentDetails.aspx"
                    HeaderTitle = "Staff Investment Details"
                    StrSortCol = "[ID] Desc"
                    NoViewAllowed = False
                    NoAddingAllowed = True

                Case "T200235"     'NES Credit Note 
                    IsStoredProcedure = True
                    StrSQL = "GetNESCreditNoteList"
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                    StrSortCol = "[NCN_NO] DESC"
                    EditPagePath = "../Transport/ExtraHiring/tptCreditNote.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    HeaderTitle = "NES CreditNote Charges"
                    NoAddingAllowed = False
                Case "T200240" 'Credit Note Posting
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Invoice"
                    IsStoredProcedure = True
                    StrSQL = "GetNESCreditNoteList"
                    ReDim SPParam(2)
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                    Else
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                    End If
                    EditPagePath = "../Transport/ExtraHiring/tptCreditNote.aspx"
                    NoAddingAllowed = True
                    HeaderTitle = "NES CreditNote Posting"
                Case "T200245" 'Approve NES Credit Note  
                    IsStoredProcedure = True
                    StrSQL = "GetNESCreditNoteList"
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "4", SqlDbType.VarChar)
                    ButtonText1 = "Approve"
                    StrSortCol = "[VOUCHER NO] DESC"
                    EditPagePath = "../Transport/ExtraHiring/tptCreditNote.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    HeaderTitle = "Approve NES Credit Note Charges"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    ShowGridCheckBox = True
                Case "T200250" 'SDET
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Invoice"
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'If rad1.Checked Then
                    '    StrSQL = "SELECT [id],[StrNo],Posted,[NET AMOUNT] FROM (SELECT LEFT(DATENAME(M,A.EGS_TRDATE),3)+DATENAME(YEAR,A.EGS_TRDATE )+BSU_SHORTNAME id,LEFT(DATENAME(M,A.EGS_TRDATE),3)+DATENAME(YEAR,A.EGS_TRDATE )+BSU_SHORTNAME [NO], '' TRNO,getdate() Date,"
                    '    StrSQL &= " LEFT(DATENAME(M,A.EGS_TRDATE),3)+DATENAME(YEAR,A.EGS_TRDATE )+BSU_SHORTNAME [StrNo],SUM([NetAmount]) [NET AMOUNT],"
                    '    StrSQL &= " (select isnull(SUM(ENI_NET),0) from TPTEXGRATIANES_INV where ENI_DELETE=0 and ENI_STRNO =LEFT(DATENAME(M,EGS_TRDATE),3)+DATENAME(YEAR,EGS_TRDATE )+BSU_SHORTNAME  )[POSTED], "
                    '    StrSQL &= " ''[Document Number] FROM  (select EGS_BSU_ID, CASE WHEN Sum(Cast(EGS_TTOTAL AS FLOAT)) > 10 THEN 10*10 ELSE Sum(Cast(EGS_TTOTAL AS FLOAT))*10 END + SUM(EGS_OTHER_AMOUNT) [NetAmount],EGS_EMP_ID,EGS_TRDATE from TPTEXGRATIACHARGESSDET "
                    '    StrSQL &= " WHERE EGS_ESI_ID = 0 and EGS_emp_id<>0 and EGS_DELETED=0 and EGS_BSU_ID='" & Session("sBsuid") & "' and EGS_FYEAR='" & Session("F_YEAR") & "'  group by EGS_emp_id,EGS_TRDATE,EGS_BSU_ID)a INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=a.EGS_BSU_ID  "
                    '    StrSQL &= " Group BY LEFT(DATENAME(M,A.EGS_TRDATE),3)+DATENAME(YEAR,A.EGS_TRDATE  )+BSU_SHORTNAME) A WHERE 1=1   "
                    '    EditPagePath = "../Transport/ExtraHiring/tptPostingSDET.aspx"
                    '    HeaderTitle = "Pending SDET Charges for Invoicing"
                    '    StrSortCol = "[StrNo] Desc"
                    '    NoViewAllowed = False
                    '    'ElseIf rad2.Checked Then
                    '    '    StrSQL = "SELECT [id],[Trno],StrNo,[Date],[Net Amount],[Document Number] FROM (select ENI_ID [id],ENI_TRNO[Trno],ENI_DATE[Date],ENI_STRNO[StrNo], 0 posted,  ENI_NET[Net Amount],"
                    '    '    StrSQL &= "case when ENI_POSTED=1 then 'Posted' else 'Not Posted' end [Status],ENI_POSTING_DATE [Posting Date],  ENI_USER_NAME [user],0[NES Amount],0[IIT Amount],0 [VILLA Amount],0 [Out Station],0 [Over Night Stay],ENI_JHD_DOCNO [Document Number]"
                    '    '    StrSQL &= "from TPTEXGRATIANES_INV WHERE ENI_BSU_ID='" & Session("sBsuid") & "' and ENI_FYEAR='" & Session("F_YEAR") & "') A WHERE 1=1  "
                    '    '    EditPagePath = "../Transport/ExtraHiring/tptPostingSDET.aspx"
                    '    '    HeaderTitle = "SDET Charges Invoice"
                    '    '    StrSortCol = "[ID] Desc"
                    '    '    NoViewAllowed = False
                    'End If
                    'NoAddingAllowed = True


                    IsStoredProcedure = True
                    StrSQL = "LoadingExgratiaEntriesSDET"
                    ReDim SPParam(8)
                    NoAddingAllowed = True
                    SPParam(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(4) = Mainclass.CreateSqlParameter("@DATE", "01-01-1900", SqlDbType.DateTime)
                    If rad1.Checked Then
                        SPParam(5) = Mainclass.CreateSqlParameter("@Option", 4, SqlDbType.Int)
                        HeaderTitle = "Pending SDET ExGratia Charges for Invoicing"
                        StrSortCol = "[StrNo] Desc"
                    Else
                        SPParam(5) = Mainclass.CreateSqlParameter("@Option", 5, SqlDbType.Int)
                        HeaderTitle = "SDET ExGratia Charges Invoice"
                        StrSortCol = "[ID] Desc"
                    End If
                    SPParam(6) = Mainclass.CreateSqlParameter("@EGS_ID", 0, SqlDbType.Int)
                    SPParam(7) = Mainclass.CreateSqlParameter("@FDATE", "01-01-1900", SqlDbType.DateTime)
                    SPParam(8) = Mainclass.CreateSqlParameter("@TDATE", "01-01-1900", SqlDbType.DateTime)
                    EditPagePath = "../Transport/ExtraHiring/tptPostingSDET.aspx"
                    NoViewAllowed = False
                    NoAddingAllowed = True
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString







                Case "T200107" 'SDET Project Master
                    StrSQL = ""
                    StrSQL = "select ID,[PROJECT DESCR],[FROM DATE],[TO DATE], [AMOUNT],[STATUS] from (select SP_ID ID, SP_DESCR [PROJECT DESCR],SP_FROMDATE[FROM DATE],SP_TODATE [TO DATE],SP_MAX_AMOUNT [AMOUNT],Case when isnull(SP_STATUS,'O')='O' then 'Open' else 'Closed' end [STATUS] from SDETPROJECTMASTER where isnull(SP_DELETED,0)=0) A where 1=1 "
                    EditPagePath = "../Transport/ExtraHiring/tptSDETProjectMaster.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[ID]"
                    HeaderTitle = "SDET Project Master"





                Case "PI05015"
                    lblDate.Text = "As on Date:"
                    dateCol.Visible = True
                    If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                    CheckBox1Text = "Summary"
                    CheckBox2Text = "Detailed"
                    If rad1.Checked Then
                        StrSQL &= "select ITB_BSU_ID, [DESCRIPTION], [ISBN], [STOCK], [COST VALUE], [SELL VALUE] from ( "
                        StrSQL &= "select null SAL_DATE, ITB_BSU_ID, null [DOC NO], 0 qTY, null [DATE], itm_isbn [ISBN], "
                        StrSQL &= "isnull(ITB_DESCR,ITM_DESCR) [DESCRIPTION], sum(case when sal_type='S' then -sad_qty else sad_qty end) STOCK, "
                        StrSQL &= "sum((case when sal_type='S' then -sad_qty else sad_qty end)*itb_cost) [COST VALUE], "
                        StrSQL &= "sum((case when sal_type='S' then -sad_qty else sad_qty end)*itb_sell) [SELL VALUE] "
                        StrSQL &= "from sal_h inner join sal_d on sal_id=sad_sal_id inner join ITEM_SALE on sad_itm_id=itm_id inner join item_salebsu on itb_itm_id=itm_id and sal_bsu_id=itb_bsu_id "
                        StrSQL &= "WHERE SAL_DATE<='" & txtDate.Text & "' and '" & Session("sBsuid") & "' in (ITB_BSU_ID)  "
                        StrSQL &= "group by ITB_BSU_ID, itm_isbn , itb_descr, itm_descr "
                        StrSQL &= " ) A WHERE 1=1 "
                    ElseIf rad2.Checked Then
                        StrSQL &= "select ITB_BSU_ID, [DOC NO], [DATE], [DESCRIPTION], [ISBN], [QTY], [COST VALUE], [SELL VALUE] from ( "
                        StrSQL &= "select SAL_DATE, ITB_BSU_ID, SAL_NO [DOC NO], 0 STOCK, replace(convert(varchar(30),SAL_DATE,106),' ','/') [DATE], itm_isbn [ISBN], "
                        StrSQL &= "isnull(ITB_DESCR,ITM_DESCR) [DESCRIPTION], (case when sal_type='S' then -sad_qty else sad_qty end) QTY, "
                        StrSQL &= "((case when sal_type='S' then -sad_qty else sad_qty end)*itb_cost) [COST VALUE], "
                        StrSQL &= "((case when sal_type='S' then -sad_qty else sad_qty end)*itb_sell) [SELL VALUE] "
                        StrSQL &= "from sal_h inner join sal_d on sal_id=sad_sal_id inner join ITEM_SALE on sad_itm_id=itm_id inner join item_salebsu on itb_itm_id=itm_id and sal_bsu_id=itb_bsu_id "
                        StrSQL &= "WHERE SAL_DATE<='" & txtDate.Text & "' and '" & Session("sBsuid") & "' in (ITB_BSU_ID)  "
                        StrSQL &= " ) A WHERE 1=1 "
                    End If
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "SAL_DATE, [DESCRIPTION]"
                    HeaderTitle = "Stock Report"

                Case "PI05016"
                    StrSQL &= "select [ISBN], [DESCRIPTION], [OP QTY], [ADJ QTY], [SAL QTY], [RET QTY], [STK QTY], [COST RATE], [SELL RATE], [STK VALUE], [SALE VALUE] from ("
                    StrSQL &= "select ITM_DESCR [DESCRIPTION], ITM_ISBN [ISBN], sum(opqty) [OP QTY], sum(inqty) [ADJ QTY], sum(salqty) [SAL QTY],sum(retqty) [RET QTY], "
                    StrSQL &= "sum(opqty)+sum(inqty)-sum(salqty)+sum(retqty) [STK QTY], itb_cost [COST RATE], itb_sell [SELL RATE], "
                    StrSQL &= "(sum(opqty)+sum(inqty)-sum(salqty)+sum(retqty))*itb_cost [STK VALUE], (sum(opqty)+sum(inqty)-sum(salqty)+sum(retqty))*itb_sell [SALE VALUE] "
                    StrSQL &= "from ("
                    StrSQL &= "select ITB_BSU_ID, ITM_DESCR, ITM_ISBN, sum(opqty) opqty, sum(inqty) inqty, sum(salqty) salqty,sum(retqty) retqty, itb_cost, itb_sell from ("
                    StrSQL &= "select ITB_BSU_ID, ITM_DESCR, ITM_ISBN, ITB_COST, ITB_SELL, "
                    StrSQL &= "case when sal_id=(select top 1 sal_id from sal_h where sal_type='A' and sal_bsu_id='" & Session("sBSUID") & "' and sal_deleted=0) then sad_qty else 0 end opqty, "
                    StrSQL &= "case when sal_type='A' and sal_id<>(select top 1 sal_id from sal_h where sal_type='A' and sal_bsu_id='" & Session("sBSUID") & "' and sal_deleted=0) then sad_qty else 0 end inqty, "
                    StrSQL &= "case when sal_type='S' then sad_qty else 0 end salqty, "
                    StrSQL &= "case when sal_type='R' then sad_qty else 0 end retqty, sad_qty  "
                    StrSQL &= "from ITEM_SALE inner join item_salebsu on itm_id=itb_itm_id and itb_bsu_id='" & Session("sBSUID") & "' inner join sal_d on sad_itm_id=itm_id inner join sal_h on sad_sal_id=sal_id "
                    StrSQL &= "where sal_bsu_id='" & Session("sBSUID") & "' and sal_deleted=0) a group by ITB_BSU_ID, ITM_DESCR, ITM_ISBN, ITB_COST, itb_sell"
                    StrSQL &= ") b group by ITM_DESCR, ITM_ISBN, [ITB_COST], ITB_SELL"
                    StrSQL &= " ) A WHERE 1=1 "
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[DESCRIPTION]"
                    HeaderTitle = "Stock Movement Report"

                Case "PI05098"
                    ddlFilter.Visible = True
                    StrSQL &= "select ~ ID, [DOCNO], [DATE], [STUDENT NO], [STUDENT NAME], [GRADE], [SECTION], [DESCRIPTION], [RATE] from ( "
                    'EditPagePath = "../Inventory/Brand.aspx"
                    StrSQL &= "select sal_id ID, sal_no DOCNO, SAL_DATE, replace(convert(varchar(30),SAL_DATE,106),' ','/') [DATE], case when sal_type='S' then 1 else -1 end*cast(sad_qty*sad_rate as decimal(12,2)) RATE, "
                    StrSQL &= "sal_cus_no [STUDENT NO], sal_cus_name [STUDENT NAME], sal_grade [GRADE], sal_section [SECTION], isnull(ITB_DESCR,ITM_DESCR) [DESCRIPTION] "
                    StrSQL &= "from sal_h inner join sal_d on sal_id=sad_sal_id and sal_deleted=0 and sal_type in ('S','R') inner join ITEM_SALE on itm_id=sad_itm_id "
                    StrSQL &= "inner join ITEM_SALEBSU on itm_id=itb_itm_id and sal_bsu_id=itb_bsu_id "
                    StrSQL &= "WHERE '" & Session("sBsuid") & "' in (sal_bsu_id) "
                    StrSQL &= ") A WHERE 1=1 "

                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "SAL_DATE DESC "
                    HeaderTitle = "Sales Item Wise"

                Case "PI04011"
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Archive"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "Payment"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and ARF_DELETED=0 and ARF_STATUS = 'S' and ARF_APR_ID not in (200,380) "
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select USRA_USR_NAME from USERSA where USRA_GRP_ID=ARF_APR_ID) " 'correct intray
                        ApprStatus &= " and (ARF_BSU_ID='" & Session("sBsuid") & "' or '" & Session("sUsr_name") 'correct prf bsu or intray bsu can see all
                        ApprStatus &= "' in (select USRA_USR_NAME from USERSA inner JOIN GROUPSA on GRPA_ID=USRA_GRP_ID where USRA_GRP_ID=ARF_APR_ID AND GRPA_ALL=1)) "
                        NoViewAllowed = False
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                    ElseIf rad2.Checked Then
                        ApprStatus = " and ARF_DELETED=0 and ARF_STATUS = 'C' and ARF_APR_ID not in (200,380) "
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflowARF where WRK_ARF_ID=ARF_ID and WRK_STATUS='S') "
                        ApprStatus &= " and '" & Session("sBsuid") & "' in ('900100','999998',ARF_BSU_ID) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                    ElseIf rad3.Checked Then
                        ApprStatus = " and ARF_DELETED=0 and ARF_STATUS = 'R' and ARF_APR_ID not in (200,380) "
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflowARF where WRK_ARF_ID=ARF_ID and WRK_STATUS in ('S','R')) "
                        ApprStatus &= " and '" & Session("sBsuid") & "' in ('900100','999998',ARF_BSU_ID) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                    ElseIf rad4.Checked Then
                        ApprStatus = " and ARF_DELETED=0 and ARF_STATUS = 'P' and ARF_APR_ID not in (200,380) "
                        ApprStatus &= " and '" & Session("sBsuid") & "' in ('900100','999998',ARF_BSU_ID) "
                        NoViewAllowed = False
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                    Else
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflowARF where WRK_ARF_ID=ARF_ID and WRK_STATUS in ('S','R')) "
                        ApprStatus &= " and '" & Session("sBsuid") & "' in ('900100','999998',ARF_BSU_ID) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                    End If
                    'FilterText1 = "Detailed"
                    If chkSelection1.Checked Then
                    Else
                        If rad4.Checked Then
                            StrSQL = " SELECT distinct ARF_ID ,[ARF NO],[DATE] ,[ACTIVITY], [USER], [STATUS],AMOUNT FROM ("
                        Else
                            StrSQL = " SELECT distinct ARF_ID ,[ARF NO],[DATE] ,[ACTIVITY], [USER],AMOUNT FROM ("
                        End If
                        StrSQL &= " select ARF_ID, ARF_NO [ARF NO], replace(convert(varchar(30),ARF_DATE,106),' ','/') [DATE], ARF_STATUS_STR STATUS, "
                        StrSQL &= " replace(convert(varchar(30),(select max(wrk_date) from workflowARF where wrk_arf_id=arf_id),106),' ','/') [ACTIVITY], ARF_USR_NAME [USER], '' [ITEM], 0 [QTY], "
                        StrSQL &= " CONVERT(varchar, CAST(ARF_AMOUNT AS money), 1) [AMOUNT] "
                        StrSQL &= " from ARF  "
                        StrSQL &= " left outer join workflowARF on arf_id=wrk_arf_id and wrk_status='M' and wrk_username='" & Session("sUsr_name") & "' "
                        StrSQL &= " WHERE ARF_FYEAR='" & Session("F_YEAR") & "' "
                    End If
                    HeaderTitle = "Advance Request Form Approval"
                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    NoAddingAllowed = True
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/Expense.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ARF_ID desc "

                Case "PI04013"
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and TCH_DELETED=0 and TCH_STATUS = 'S' and TCH_APR_ID not in (200,380) "
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select USRA_USR_NAME from USERSA where USRA_GRP_ID=TCH_APR_ID) " 'correct intray
                        ApprStatus &= " and (TCH_BSU_ID in (select case when usra_all=1 then TCH_BSU_ID else USRA_BSU_ID end from USERSA where USRA_USR_NAME='" & Session("sUsr_name") & "' and USRA_GRP_ID=TCH_APR_ID)) "
                        NoViewAllowed = False
                        ShowGridCheckBox = True
                        ButtonText1 = "Approve Tenancy Request"
                        StrSortCol = "MESSAGE, ACTIVITY"
                    ElseIf rad2.Checked Then 'as long as the user is there in workflow no need to check BSU
                        ApprStatus = " and TCH_DELETED=0 and TCH_STATUS in ('C','S') " 'and TCH_APR_ID not in (200,380)
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflowTCT where WRK_TCH_ID=TCH_ID and WRK_STATUS in ('C','S')) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "cast(ACTIVITY as smalldatetime) Desc"
                    ElseIf rad3.Checked Then
                        ApprStatus = " and TCH_DELETED=0 and TCH_STATUS = 'R' " 'and TCH_APR_ID not in (200,380)
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflowTCT where WRK_TCH_ID=TCH_ID and WRK_STATUS in ('S','R')) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "cast(ACTIVITY as smalldatetime) Desc"
                    Else
                        ApprStatus &= " and '" & Session("sUsr_name") & "' in (select WRK_USERNAME from workflowTCT where WRK_TCH_ID=TCH_ID and WRK_STATUS in ('S','R')) "
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "cast(ACTIVITY as smalldatetime) Desc"
                    End If
                    If chkSelection1.Checked Then
                    Else
                        If rad4.Checked Then
                            StrSQL = " SELECT TCH_ID,[TCT NO],[DATE],[TYPE],[ACTIVITY],[EMPLOYEE],[BUDGET],[STATUS],MESSAGE,ELIGIBILITY,RENT FROM ("
                        ElseIf rad1.Checked Then
                            StrSQL = " SELECT distinct TCH_ID,[TCT NO],[DATE],[TYPE],[ACTIVITY],[EMPLOYEE],[BUDGET],MESSAGE,ELIGIBILITY,RENT FROM ("
                        Else
                            StrSQL = " SELECT TCH_ID,[TCT NO],[DATE],[TYPE],[ACTIVITY],[EMPLOYEE],[BUDGET],MESSAGE,ELIGIBILITY,RENT FROM ("
                        End If
                        StrSQL &= " select TCH_ID, TCH_NO [TCT NO], rcm_descr CITY, replace(convert(varchar(30),TCH_DATE,106),' ','/') [DATE], TCH_STATUS_STR STATUS, case when TCH_budgeted=1 then 'YES' else 'NO' end+ "
                        StrSQL &= " (selecT case when sum(TCE_DAMOUNT)>0 then '-Dedn:'+cast(sum(TCE_DAMOUNT) as varchar) else '' end from tct_e where TCE_TCH_ID=TCH_ID) BUDGET, "
                        StrSQL &= " '<font color='''+CASE when TCH_MSG_USR='" & Session("sUsr_name") & "' then 'red''>Pending' else case when wrk_TCH_id is not null then 'green''>Actioned' else '#1B80B6''>NA' end end+'</font>' MESSAGE, "
                        StrSQL &= " replace(convert(varchar(30),(select max(wrk_date) from workflowTCT where wrk_TCH_id=TCH_id),106),' ','/') [ACTIVITY], TCH_USR_NAME [USER], "
                        'StrSQL &= " CONVERT(varchar, CAST(TCH_TOTAL AS money), 1)+'-'+(select top 1 reu_name from tct_e inner join REUNITTYPE on reu_id=tce_reu_id where tce_tch_id=tch_id) [RENT],"
                        StrSQL &= " case when TCH_TYPE='N' then 'New' else 'Renewal' end [TYPE],  "
                        StrSQL &= " (select CONVERT(varchar, CAST(sum(tce_lamount) AS money), 1)+'-'+max(reu_name) from tct_e inner join REUNITTYPE on reu_id=tce_reu_id where tce_tch_id=tch_id and tce_deleted=0) RENT, "
                        StrSQL &= " (select CONVERT(varchar, CAST(sum(tce_eamount) AS money), 1)+'-'+max(reu_name) from tct_e inner join REUNITTYPE on reu_id=tce_ereu_id where tce_tch_id=tch_id and tce_deleted=0) ELIGIBILITY, "
                        StrSQL &= "stuff((select isnull(emp_name,'School') +'<br>' from TCT_E left outer join oasis_docs..VW_EMPLOYEE_V on TCE_EMP_ID=EMP_ID where TCH_ID=TCE_TCH_ID and tce_deleted=0 "
                        StrSQL &= "for xml path(''), type ).value('.','varchar(max)'), 1, 0, '') EMPLOYEE "
                        StrSQL &= " from TCT_H inner join recity_m on rcm_id=tch_cit_id left outer join workflowTCT on TCH_id=wrk_TCH_id and wrk_status='M' and wrk_username='" & Session("sUsr_name") & "' "
                        StrSQL &= " WHERE TCH_FYEAR<='" & Session("F_YEAR") & "' "
                    End If
                    HeaderTitle = "Tenancy Contract Approval"
                    If Not rad5.Checked Then
                        StrSQL &= ApprStatus
                    End If
                    NoAddingAllowed = True
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Inventory/TenancyContract.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    Session("prf_id") = 0

                Case "PI02023" ' Advance Request 
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"
                    If rad1.Checked Then
                        StrSQL = "SELECT ID,[AR NO],[DATE],[REQUESTED BY],[PURPOSE],[REMARKS],[AMOUNT],[STATUS] FROM ("
                        StrSQL &= "SELECT ARF_ID ID ,ARF_NO [AR NO],ARF_DATE [DATE],ARF_AMOUNT [AMOUNT],"
                        StrSQL &= " isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') [REQUESTED BY] ,PUR_DESCR [PURPOSE],ARF_REMARKS [REMARKS], "
                        StrSQL &= "ARF_STATUS_STR  AS [STATUS]"
                        StrSQL &= "FROM ARF LEFT OUTER JOIN ADVREQPURPOSE ON PUR_ID=ARF_PUR_ID  "
                        StrSQL &= "INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID  "
                        StrSQL &= "WHERE  ARF_DELETED=0 and ARF_STATUS='N' and ARF_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= ") a where 1=1"
                    ElseIf rad2.Checked Then
                        StrSQL = "SELECT ID,[AR NO],[DATE],[REQUESTED BY],[PURPOSE],[REMARKS],[AMOUNT],[STATUS] FROM ("
                        StrSQL &= "SELECT ARF_ID ID ,ARF_NO [AR NO],ARF_DATE [DATE],ARF_AMOUNT [AMOUNT],"
                        StrSQL &= " isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') [REQUESTED BY]  ,PUR_DESCR [PURPOSE],ARF_REMARKS [REMARKS], "
                        StrSQL &= "ARF_STATUS_STR  AS [STATUS]"
                        StrSQL &= "FROM ARF LEFT OUTER JOIN ADVREQPURPOSE ON PUR_ID=ARF_PUR_ID  "
                        StrSQL &= "INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID  "
                        StrSQL &= "WHERE  ARF_DELETED=0 and ARF_STATUS='S' and ARF_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= ") a where 1=1"
                    ElseIf rad3.Checked Then
                        StrSQL = "SELECT ID,[AR NO],[DATE],[REQUESTED BY],[PURPOSE],[REMARKS],[AMOUNT],[STATUS] FROM ("
                        StrSQL &= "SELECT ARF_ID ID ,ARF_NO [AR NO],ARF_DATE [DATE],ARF_AMOUNT [AMOUNT],"
                        StrSQL &= " isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') [REQUESTED BY]  ,PUR_DESCR [PURPOSE],ARF_REMARKS [REMARKS], "
                        StrSQL &= "ARF_STATUS_STR  AS [STATUS]"
                        StrSQL &= "FROM ARF LEFT OUTER JOIN ADVREQPURPOSE ON PUR_ID=ARF_PUR_ID  "
                        StrSQL &= "INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID  "
                        StrSQL &= "WHERE  ARF_DELETED=0 and ARF_STATUS='R' and ARF_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= ") a where 1=1"
                    Else
                        StrSQL = "SELECT ID,[AR NO],[DATE],[REQUESTED BY],[PURPOSE],[REMARKS],[AMOUNT],[STATUS] FROM ("
                        StrSQL &= "SELECT ARF_ID ID ,ARF_NO [AR NO],ARF_DATE [DATE],ARF_AMOUNT [AMOUNT],"
                        StrSQL &= "isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') [REQUESTED BY] ,PUR_DESCR [PURPOSE],ARF_REMARKS [REMARKS], "
                        StrSQL &= "ARF_STATUS_STR  AS [STATUS]"
                        StrSQL &= "FROM ARF LEFT OUTER JOIN ADVREQPURPOSE ON PUR_ID=ARF_PUR_ID  "
                        StrSQL &= "INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID  "
                        StrSQL &= "WHERE  ARF_DELETED=0  and ARF_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= ") a where 1=1"
                    End If
                    EditPagePath = "../Inventory/Expense.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "[AR NO] DESC"
                    HeaderTitle = "Advance Request Form"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "PI02025" ' Expense Settlement 
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Settled"
                    If rad1.Checked Then
                        StrSQL &= "'' "
                        StrSQL = "SELECT ID,[AR NO],[DATE],[REQUESTED BY],[PURPOSE],[REMARKS],[AMOUNT],[AMOUNT SETTLED] FROM ("
                        StrSQL &= "SELECT ARF_ID ID ,ARF_NO [AR NO],ARF_DATE [DATE],ARF_AMOUNT [AMOUNT],"
                        StrSQL &= "isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') [REQUESTED BY] ,PUR_DESCR [PURPOSE],ARF_REMARKS [REMARKS], "
                        StrSQL &= "isnull([AMOUNT SETTLED],0)[AMOUNT SETTLED],''[ES NO],'' [AR.DATE] "
                        StrSQL &= "FROM ARF INNER join OASIS..EMPLOYEE_M on EMP_ID =ARF_EMP_ID "
                        StrSQL &= "LEFT OUTER JOIN(SELECT isnull(SUM(ESH_TOTAL),0) [AMOUNT SETTLED],ESH_ARF_ID ID FROM ESF_H GROUP BY ESH_ARF_ID)C ON C.ID=ARF_ID "
                        StrSQL &= "LEFT OUTER JOIN ADVREQPURPOSE ON PUR_ID=ARF_PUR_ID  WHERE  ARF_DELETED=0 and ARF_STATUS='S' and ARF_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= ") a where 1=1"
                        HeaderTitle = "Expense Settlement Form"
                        NoViewAllowed = False
                        StrSortCol = "[AR NO] DESC"
                    Else
                        StrSQL &= "'' "
                        StrSQL = "SELECT ID,[ES NO],[DATE],[AR NO],[AR.DATE],[REMARKS],[AMOUNT],[AMOUNT SETTLED] FROM ("
                        StrSQL &= "SELECT ESH_ID ID ,ESH_NO [ES NO],ESH_DATE [DATE],ESH_ARF_NO [AR NO],ESH_ARF_DATE [AR.DATE] ,ESH_REMARKS [REMARKS], "
                        StrSQL &= "isnull([AMOUNT SETTLED],0)[AMOUNT SETTLED],ESH_TOTAL [AMOUNT], "
                        StrSQL &= " ''[Settlement date],''[purpose],'' [REQUESTED BY] FROM ESF_H "
                        StrSQL &= "LEFT OUTER JOIN(SELECT isnull(SUM(ESH_TOTAL),0) [AMOUNT SETTLED],ESH_ARF_ID ID FROM ESF_H GROUP BY ESH_ARF_ID)C ON C.ID=ESH_ARF_ID "
                        StrSQL &= "WHERE ESH_BSU_ID='" & Session("sBsuid") & "' ) a where 1=1"
                        HeaderTitle = "Expense Settlement Form"
                        NoViewAllowed = False
                        StrSortCol = "[ES NO] DESC"
                    End If
                    NoAddingAllowed = True
                    EditPagePath = "../Inventory/Expense.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "PI04009"
                    StrSQL &= "select ID, [DETAILS], [DATE], [USER], [CARD TRN NO], [CARD AMOUNT], [CASH TRN NO], [CASH AMOUNT] from ("
                    StrSQL &= "SELECT SAP_ID ID, SAP_DESCR DETAILS, replace(convert(varchar(30),SAP_DATE,106),' ','/') DATE, SAP_USER [USER], SAP_CCNO [CARD TRN NO], SAP_CCAMOUNT [CARD AMOUNT], SAP_NO [CASH TRN NO], SAP_AMOUNT [CASH AMOUNT] FROM SAL_P where SAP_BSU_ID='" & Session("sBsuid") & "' "
                    StrSQL &= ") a WHERE 1=1 "
                    EditPagePath = "../Inventory/SalesDayEnd.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID DESC"
                    HeaderTitle = "Sales Day End Posting"
                    NoViewAllowed = True

                Case "PI01169"
                    StrSQL &= "select ID, [DETAILS], [PROCESS DATE],[DAYEND DATE], [USER], [CARD TRN NO], [CARD AMOUNT], [CASH TRN NO], [CASH AMOUNT] from ("
                    StrSQL &= "SELECT BSAP_ID ID, BSAP_DESCR DETAILS, replace(convert(varchar(30),BSAP_DATE,106),' ','/') [PROCESS DATE], replace(convert(varchar(30),BSAP_DAYENDDT,106),' ','/') [DAYEND DATE], BSAP_USER [USER], BSAP_CCNO [CARD TRN NO], BSAP_CCAMOUNT [CARD AMOUNT], BSAP_NO [CASH TRN NO], BSAP_AMOUNT [CASH AMOUNT] FROM BOOK_SALE_P where BSAP_BSU_ID='" & Session("sBsuid") & "' "
                    StrSQL &= ") a WHERE 1=1 "
                    EditPagePath = "../Inventory/BookSale/BS_SalesDayEnd.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "ID DESC"
                    HeaderTitle = "Book Sales Day End Posting"
                    NoViewAllowed = True

                Case "A850043"
                    StrSQL &= "select ID, CODE, DESCRIPTION, [AC CODE], ACCOUNT from ("
                    StrSQL &= "SELECT [ID], [ID] CODE, DESCR [DESCRIPTION], ACT_ID [AC CODE], ACT_NAME [ACCOUNT] FROM vw_ACCOUNTS_SUB_ACC_M_list "
                    StrSQL &= "inner join accounts_m on act_id=Account WHERE descr not in ('Common Cost','Unallocated') "
                    StrSQL &= ") a where 1=1 "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Sub Ledger Master"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case "AST0610"
                    ShowGridCheckBox = True
                    CheckBox1Text = "All"
                    CheckBox2Text = "Printed"
                    CheckBox3Text = "Not Printed"
                    StrSQL = "SELECT  ASM_ID,[CATEGORY] ,[SUB CATEGORY],[DESCRIPTION],[MAKE/BRAND],[MODEL/YEAR] ,[CODE], [LOCATION]  FROM ("
                    StrSQL &= "SELECT  ASM_ID,ACM_DESCR [Category] ,ASC_DESCR [Sub Category],MKE_DESCR [MAKE/BRAND],MDL_DESCR [MODEL/YEAR] ,"
                    StrSQL &= "ASM_CODE [Code],ASM_DESCR [Description],LOC_DESCR [Location],isnull(ASM_PRINTED,0) ASM_PRINTED  "
                    StrSQL &= "FROM ASSETS.ASSETS_M  LEFT OUTER JOIN ASSETS.[ASSET_SUBCATEGORY_M] ON ASM_ASC_ID=ASC_ID "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_CATEGORY_M] ON ACM_ID =ASC_ACM_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_MAKE] ON MKE_ID =ASM_MKE_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_MODEL] ON MDL_ID =ASM_MDL_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[LOCATION_M]  ON LOC_ID  =ASM_LOC_ID WHERE ASM_BSU_ID ='" & Session("sBsuid") & "') A"
                    StrSQL &= " WHERE 1 = 1 "
                    If rad2.Checked Then
                        StrSQL &= " AND ASM_PRINTED =1 "
                    ElseIf rad3.Checked Then
                        StrSQL &= " AND ASM_PRINTED =0 "
                    End If
                    EditPagePath = "../Asset/AssetMasterDetail.aspx"
                    NoAddingAllowed = True
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    ButtonText1 = "Print Barcode"
                    StrSortCol = "ASM_ID"
                    HeaderTitle = "Asset Master"
                Case "AST0703"
                    StrSQL = " SELECT  ASI_ID,[CATEGORY] ,[SUB CATEGORY],[DESCRIPTION],[MAKE/BRAND],[MODEL/YEAR] ,[QTY], [LOCATION]  FROM ("
                    StrSQL &= "SELECT  ASI_ID,ACM_DESCR [Category] ,ASC_DESCR [Sub Category],MKE_DESCR [MAKE/BRAND],MDL_DESCR [MODEL/YEAR]  ,"
                    StrSQL &= "CAST(ASI_QTY AS INT) [QTY],ASI_DESCR [Description],LOC_DESCR [Location]  "
                    StrSQL &= "FROM ASSETS.ASSETS_INIT  LEFT OUTER JOIN ASSETS.[ASSET_SUBCATEGORY_M] ON ASI_ASC_ID=ASC_ID "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_CATEGORY_M] ON ACM_ID =ASC_ACM_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_MAKE] ON MKE_ID =ASI_MKE_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_MODEL] ON MDL_ID =ASI_MDL_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[LOCATION_M]  ON LOC_ID  =ASI_LOC_ID WHERE ASI_BSU_ID ='" & Session("sBsuid") & "') A "
                    StrSQL &= "WHERE 1 = 1 "
                    EditPagePath = "../Asset/AssetInitDetail.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "ASI_ID"
                    HeaderTitle = "Create Asset (Stage-1)"
                Case "AST0706"
                    StrSQL = " SELECT  ASI_ID,[CATEGORY] ,[SUB CATEGORY],[DESCRIPTION],[MAKE/BRAND],[MODEL/YEAR] ,[INVOICE NO],[QTY],[INVOICE AMOUNT], [LOCATION]  FROM ("
                    StrSQL &= "SELECT  ASI_ID,ACM_DESCR [Category] ,ASC_DESCR [Sub Category],MKE_DESCR [MAKE/BRAND],MDL_DESCR [MODEL/YEAR]  ,"
                    StrSQL &= "CAST(ASI_QTY AS INT) [QTY],ASI_DESCR [Description],LOC_DESCR [Location] , "
                    StrSQL &= " ASI_INVOICENO [INVOICE NO], cast(ASI_QTY * ASI_PRICE as float) [INVOICE AMOUNT]"
                    StrSQL &= "FROM ASSETS.ASSETS_INIT  LEFT OUTER JOIN ASSETS.[ASSET_SUBCATEGORY_M] ON ASI_ASC_ID=ASC_ID "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_CATEGORY_M] ON ACM_ID =ASC_ACM_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_MAKE] ON MKE_ID =ASI_MKE_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[ASSET_MODEL] ON MDL_ID =ASI_MDL_ID  "
                    StrSQL &= "LEFT OUTER JOIN ASSETS.[LOCATION_M]  ON LOC_ID  =ASI_LOC_ID WHERE ASI_BSU_ID ='" & Session("sBsuid") & "') A "
                    StrSQL &= "WHERE 1 = 1 "
                    EditPagePath = "../Asset/AssetInitDetail.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "ASI_ID"
                    NoAddingAllowed = True
                    HeaderTitle = "Create Asset (Final Stage)"
                Case "AST0710", "AST0715", "AST0720", "AST0725"
                    StrSQL = "SELECT TRH_ID, [DOC NO], [FROM BUSINESS UNIT] ,[TO BUSINESS UNIT],  [TRANSFER TYPE],[TRANSFER DATE], [EMPLOYEE NAME],[REMARKS] FROM ("
                    StrSQL &= " SELECT  TRH_ID,TRH_DOCNO [DOC NO],FROM_BSU.BSU_ID FROM_BSU_ID,FROM_BSU.BSU_NAME [FROM BUSINESS UNIT] , TO_BSU.BSU_ID TO_BSU_ID, TO_BSU.BSU_NAME [TO BUSINESS UNIT],TRH_TYM_ID, TYM_DESC [TRANSFER TYPE],"
                    'StrSQL &= " replace(convert(varchar(30),TRH_FROMDT,106),' ','/')  [TRANSFER DATE], EMP_FNAME + ' ' + EMP_MNAME + ' '  + EMP_LNAME [EMPLOYEE NAME],TRH_REMARKS [REMARKS],isnull(TRH_bApproved,0) TRH_bApproved,isnull(TRH_rApproved,0) TRH_rApproved,isnull(TRH_TO_BSU_bApproved,0) TRH_TO_BSU_bApproved  "
                    StrSQL &= " TRH_FROMDT  [TRANSFER DATE], EMP_FNAME + ' ' + EMP_MNAME + ' '  + EMP_LNAME [EMPLOYEE NAME],TRH_REMARKS [REMARKS],isnull(TRH_bApproved,0) TRH_bApproved,isnull(TRH_rApproved,0) TRH_rApproved,isnull(TRH_TO_BSU_bApproved,0) TRH_TO_BSU_bApproved  "
                    StrSQL &= " FROM ASSETS.[TRANSFER_H]  LEFT OUTER JOIN [ASSETS].[TRANSFERTYPE_M] ON TRH_TYM_ID=TYM_ID "
                    StrSQL &= " LEFT OUTER JOIN OASIS.dbo.EMPLOYEE_M ON TRH_EMP_ID=EMP_ID "
                    StrSQL &= " LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M FROM_BSU ON TRH_BSU_ID=FROM_BSU.BSU_ID "
                    StrSQL &= " LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M TO_BSU ON TRH_TO_BSU_ID=TO_BSU.BSU_ID "
                    If MainMenuCode <> "AST0710" Then
                        CheckBox1Text = "All"
                        CheckBox2Text = "Approved"
                        CheckBox3Text = "Not Approved"
                        NoAddingAllowed = True
                        If Page.IsPostBack = False Then rad3.Checked = True
                    End If
                    If MainMenuCode = "AST0715" Then
                        StrSQL &= " WHERE (FROM_BSU.BSU_ID='" & Session("sBsuid") & "')) A WHERE 1=1 "
                        If rad2.Checked Then
                            StrSQL &= " AND TRH_bApproved =1 "
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND TRH_bApproved =0 "
                        End If
                        StrSQL &= " AND TRH_TYM_ID in (4,5) "
                        HeaderTitle = "Approve IntertUnit Asset Transfer\Loanout"
                    ElseIf MainMenuCode = "AST0720" Then
                        StrSQL &= " WHERE (TO_BSU.BSU_ID='" & Session("sBsuid") & "')) A WHERE 1=1 "
                        If rad2.Checked Then
                            StrSQL &= " AND TRH_TO_BSU_bApproved=1"
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND TRH_TO_BSU_bApproved=0"
                        End If
                        StrSQL &= "  AND TRH_bApproved=1 AND TRH_TYM_ID in (4,5) "
                        HeaderTitle = "Receive IntertUnit Asset Transfer\Loanout"
                    ElseIf MainMenuCode = "AST0725" Then
                        StrSQL &= " WHERE (TO_BSU.BSU_ID='" & Session("sBsuid") & "')) A WHERE 1=1 "
                        If rad2.Checked Then
                            StrSQL &= " AND TRH_rApproved=1"
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND TRH_rApproved=0"
                        End If
                        StrSQL &= "  AND TRH_bApproved=1 AND TRH_TO_BSU_bApproved=1 AND TRH_TYM_ID in (5) "
                        HeaderTitle = "Return IntertUnit Asset loanout"
                    Else
                        StrSQL &= " WHERE (FROM_BSU.BSU_ID='" & Session("sBsuid") & "')) A WHERE 1=1 "
                        HeaderTitle = "Transfer Assets"
                    End If
                    StrSQL &= " AND TRH_TYM_ID not in (6) "
                    EditPagePath = "../Asset/AssetAllocateToLocation.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "TRH_ID"
                Case "AST0730", "AST0735"
                    StrSQL = "SELECT TRH_ID, [DOC NO], [DATE], [REMARKS] FROM "
                    StrSQL &= " ( SELECT  TRH_ID,TRH_DOCNO [DOC NO], replace(convert(varchar(30),TRH_FROMDT,106),' ','/')  [DATE],"
                    StrSQL &= " TRH_REMARKS [REMARKS],isnull(TRH_bApproved,0) TRH_bApproved,isnull(TRH_rApproved,0) TRH_rApproved,"
                    StrSQL &= " isnull(TRH_TO_BSU_bApproved,0) TRH_TO_BSU_bApproved   FROM ASSETS.[TRANSFER_H]  "
                    StrSQL &= " WHERE (TRH_BSU_ID='" & Session("sBsuid") & "'  AND TRH_TYM_ID in (6))) A WHERE 1=1  "
                    If MainMenuCode = "AST0735" Then
                        CheckBox1Text = "All"
                        CheckBox2Text = "Approved"
                        CheckBox3Text = "Not Approved"
                        NoAddingAllowed = True
                        If Page.IsPostBack = False Then rad3.Checked = True
                    End If
                    If MainMenuCode = "AST0735" Then
                        If rad2.Checked Then
                            StrSQL &= " AND TRH_bApproved =1 "
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND TRH_bApproved =0 "
                        End If
                        HeaderTitle = "Approve Asset Condemnation"
                    Else
                        HeaderTitle = "Asset Condemnation"
                    End If
                    EditPagePath = "../Asset/AssetAllocateToLocation.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "TRH_ID"

                Case "AST0603"
                    StrSQL = "SELECT LOC_ID,[LOCATION NAME],[LOCATION TYPE], [OWNERSHIP TYPE], [CITY], [DEPARTMENT] FROM ("
                    StrSQL &= " SELECT LOC_ID,LOC_DESCR [LOCATION NAME],LTM_DESC [LOCATION TYPE],LOM_DESC [OWNERSHIP TYPE],CIT_DESCR [CITY],DPT_DESCR [DEPARTMENT]"
                    StrSQL &= " FROM [ASSETS].[LOCATION_M]"
                    StrSQL &= " LEFT OUTER JOIN [ASSETS].[LOCATIONTYPE_M] ON LOC_TYPE=LTM_ID"
                    StrSQL &= " LEFT OUTER JOIN [ASSETS].[LOCATIONOWNTYPE_M] ON LOC_OWN_TYP =LOM_ID"
                    StrSQL &= " LEFT OUTER JOIN OASIS.dbo.CITY_M ON LOC_CIT_ID  =CIT_ID "
                    StrSQL &= " LEFT OUTER JOIN OASIS.dbo.DEPARTMENT_M  ON LOC_DPT_ID  =DPT_ID WHERE LOC_BSU_ID='" & Session("sBsuid") & "') A WHERE 1=1 "
                    EditPagePath = "../Asset/AssetLocationMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "LOC_ID"
                    HeaderTitle = "Asset Location Master"
                Case "AST0604"
                    StrSQL = "SELECT  ASC_ID, [CATEGORY],  [SUB CATEGORY] , [SUB CATEGORY CODE] FROM ("
                    StrSQL &= " SELECT ASC_ID,ACM_DESCR [CATEGORY], ASC_DESCR [SUB CATEGORY] ,ASC_CODE [SUB CATEGORY CODE]"
                    StrSQL &= " FROM [ASSETS].[ASSET_SUBCATEGORY_M] LEFT OUTER JOIN ASSETS.ASSET_CATEGORY_M  ON ASC_ACM_ID=ACM_ID "
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Asset/AssetSubCategory.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "ASC_ID"
                    HeaderTitle = "Asset Sub Category Master"
                Case "AST0605"
                    StrSQL = "SELECT MKE_ID,[MAKE/BRAND] FROM ("
                    StrSQL &= " SELECT MKE_ID,MKE_DESCR [MAKE/BRAND]"
                    StrSQL &= " FROM [ASSETS].[ASSET_MAKE]) A WHERE 1=1 "
                    EditPagePath = "../Asset/AssetMakeMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "MKE_ID"
                    HeaderTitle = "Asset Make Master"
                Case "AST0607"
                    StrSQL = "SELECT MDL_ID,[SUB CATEGORY],[MODEL/YEAR] FROM ("
                    StrSQL &= " SELECT MDL_ID,ASC_DESCR [SUB CATEGORY] , MDL_DESCR [MODEL/YEAR]"
                    StrSQL &= " FROM [ASSETS].[ASSET_MODEL] LEFT OUTER JOIN ASSETS.ASSET_SUBCATEGORY_M ON ASC_ID=MDL_ASC_ID "
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Asset/AssetModelMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "MDL_ID"
                    HeaderTitle = "Asset Model Master"
                Case "AST0602"
                    StrSQL = "     SELECT LFM_ID,[LOCATION FACILITY] FROM ("
                    StrSQL &= " SELECT LFM_ID,LFM_DESCR [LOCATION FACILITY]"
                    StrSQL &= " FROM [ASSETS].[ASSET_LOC_FACILITY]"
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../Asset/AssetLocFacilityMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "LFM_ID"
                    HeaderTitle = "Location Facility Master"
                Case "B100210"
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Approved"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and BSH_DELETED=0 and BSH_STATUS = 'N' "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and BSH_DELETED=0 and BSH_STATUS in ('S','P') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and BSH_DELETED=0 and BSH_STATUS = 'P' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and BSH_DELETED=0 and BSH_STATUS = 'R' "
                    End If
                    StrSQL = "select BSH_ID, DATE, TERM, [START DATE], [END DATE], STATUS from ( "
                    StrSQL &= "SELECT BSH_DELETED, BSH_STATUS, BSH_STATUS_STR STATUS, BSH_ID, replace(convert(varchar(30), BSH_DATE, 106),' ','/') DATE, BPD_DETAILS TERM, BSU_NAME, "
                    StrSQL &= "replace(convert(varchar(30), BPD_SDATE, 106),' ','/') [START DATE], replace(convert(varchar(30), BPD_EDATE, 106),' ','/') [END DATE] "
                    StrSQL &= "from BudStudent_H inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=BSH_BSU_ID INNER JOIN BUDPERIOD on BPD_ID=BSH_TRM_ID AND BPD_TYPE='S' where BSU_ID='" & Session("sBsuid") & "') a Where 1=1 "
                    StrSQL &= ApprStatus
                    EditPagePath = "../Budget/BudgetStudent.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "BSH_ID"
                    HeaderTitle = "Budget for School Capacity/ Student Enrollment"
                Case "B200210"
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ShowGridCheckBox = True
                        ButtonText1 = "Approve Budgets"
                        ApprStatus = " and BSH_STATUS = 'S' "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and BSH_STATUS = 'P' "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and BSH_STATUS = 'R' "
                    End If
                    StrSQL = "select BSH_ID, DATE, TERM, [START DATE], [END DATE], STATUS, cast(SB as varchar)+'/'+cast(SECTIONS as varchar) SECTIONS, cast(SA as varchar)+'/'+cast(STRENGTH as varchar) STRENGTH, [PERCENT] from ( "
                    StrSQL &= "SELECT BSH_DELETED, BSH_STATUS, BSH_STATUS_STR STATUS, BSH_ID, replace(convert(varchar(30), BSH_DATE, 106),' ','/') DATE, "
                    StrSQL &= "BPD_DETAILS TERM, BSU_NAME, replace(convert(varchar(30), BPD_SDATE, 106),' ','/') [START DATE], "
                    StrSQL &= "replace(convert(varchar(30), BPD_EDATE, 106),' ','/') [END DATE], sum(BSD_SECTIONS1) SB, sum(BSD_SECTIONS1+BSD_SECTIONS2) SECTIONS, sum(BSD_STRENGTH1) SA, SUM(BSD_STRENGTH1+BSD_ADMISSIONS-BSD_TC) STRENGTH, BSH_PERCENT [PERCENT] "
                    StrSQL &= "from BudStudent_H inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=BSH_BSU_ID INNER JOIN BUDPERIOD on BPD_ID=BSH_TRM_ID "
                    StrSQL &= "inner JOIN BudStudent_D on BSD_BSH_ID=BSH_ID "
                    StrSQL &= "where BSU_ID='" & Session("sBsuid") & "'"
                    StrSQL &= "GROUP by BSH_DELETED, BSH_STATUS, BSH_STATUS_STR, BSH_ID, BSH_DATE, BPD_DETAILS, BSU_NAME, BPD_SDATE, BPD_EDATE, BSH_PERCENT "
                    StrSQL &= ") a Where 1=1 " & ApprStatus 'and BSH_STATUS = 'S' "

                    NoAddingAllowed = True
                    EditPagePath = "../Budget/BudgetStudent.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "BSH_ID"
                    HeaderTitle = "Approval for School Capacity/ Student Enrollment Budgets"
                Case "B100200"
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Approved"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " "
                    ElseIf rad2.Checked Then
                        ApprStatus = " "
                    ElseIf rad3.Checked Then
                        ApprStatus = " "
                    ElseIf rad4.Checked Then
                        ApprStatus = " "
                    End If
                    StrSQL = "SELECT CTC_ID,Date, QTY, [PERCENT], [AMOUNT] FROM ("
                    StrSQL &= "select CTC_ID, replace(convert(varchar(30), CTC_DATE, 106),' ','/') Date, sum(case WHEN CTD_CTT_ID=8 then CTD_QTY ELSE 0 end) QTY, sum(CTD_PERCENT) [PERCENT], sum(CTD_AMOUNT) AMOUNT  "
                    StrSQL &= "FROM BudCTC_H inner JOIN BudCTC_D on CTD_CTC_ID=CTC_ID where CTC_BSU_ID='" & Session("sBsuid") & "' "
                    StrSQL &= "GROUP by CTC_ID, CTC_DATE) A WHERE 1=1 "
                    EditPagePath = "../Budget/BudgetCTC.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "CTC_ID"
                    HeaderTitle = "CTC Budget"
                Case "B100215" 'Expense Budget By Shakeel
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Approved"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS = 'N' "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS in ('S','P') "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS = 'P' "
                    ElseIf rad4.Checked Then
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS = 'R' "
                    End If
                    StrSQL = "SELECT BEH_ID,[DATE],[COST CENTER],[ACCOUNT DESCRIPTION],[SUB LEDGER], [2012-13], [2013-14], [2014-15], [2015-16], TOTAL FROM ("
                    StrSQL &= "SELECT BEH_ID, CCT_DESCR [COST CENTER], ACT_NAME [ACCOUNT DESCRIPTION], BED_ASM_ID [SUB LEDGER], replace(convert(varchar(30), BEH_DATE, 106),' ','/') [DATE], sum(BEG_TOTAL) TOTAL,"
                    StrSQL &= "sum(CASE when BEG_PER_ID='2012-13' THEN BEG_TOTAL ELSE 0 END) [2012-13], "
                    StrSQL &= "sum(CASE when BEG_PER_ID='2013-14' THEN BEG_TOTAL ELSE 0 END) [2013-14], BEH_DELETED, BEH_STATUS, "
                    StrSQL &= "sum(CASE when BEG_PER_ID='2014-15' THEN BEG_TOTAL ELSE 0 END) [2014-15], "
                    StrSQL &= "sum(CASE when BEG_PER_ID='2015-16' THEN BEG_TOTAL ELSE 0 END) [2015-16] from BudExpense_H "
                    StrSQL &= "inner JOIN BudExpense_D on BEH_ID=BED_BEH_ID INNER JOIN BudExpense_G on BED_ID=BEG_BED_ID "
                    StrSQL &= "INNER JOIN OASISFIN.dbo.ACCOUNTS_M on ACT_ID=BED_ACT_ID INNER JOIN OASISFIN.dbo.COSTCENTER_M on CCT_ID=BED_CCT_ID "
                    StrSQL &= "WHERE BEH_BSU_ID ='" & Session("sBsuid") & "' and BEH_USR_NAME='" & Session("sUsr_id") & "' "
                    StrSQL &= "GROUP by BEH_DELETED, BEH_STATUS, BEH_ID, CCT_DESCR, ACT_NAME, BED_ASM_ID, BEH_DATE having sum(BEG_TOTAL)>0) A "
                    StrSQL &= " WHERE 1 = 1 " & ApprStatus
                    EditPagePath = "../Budget/ExpBudget.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "BEH_ID"
                    HeaderTitle = "Expense Budget"

                Case "B200220"
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ShowGridCheckBox = True
                        ButtonText1 = "Approve Budgets"
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS = 'S' "
                    ElseIf rad2.Checked Then
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS = 'P' "
                    ElseIf rad3.Checked Then
                        ApprStatus = " and BEH_DELETED=0 and BEH_STATUS = 'R' "
                    End If
                    StrSQL = "SELECT BEH_ID,[DATE],[COST CENTER],[ACCOUNT DESCRIPTION],[SUB LEDGER], [2012-13], [2013-14], [2014-15], [2015-16], TOTAL FROM ("
                    StrSQL &= "SELECT BEH_ID, CCT_DESCR [COST CENTER], ACT_NAME [ACCOUNT DESCRIPTION], BED_ASM_ID [SUB LEDGER], replace(convert(varchar(30), BEH_DATE, 106),' ','/') [DATE], sum(BEG_TOTAL) TOTAL,"
                    StrSQL &= "sum(CASE when BEG_PER_ID='2012-13' THEN BEG_TOTAL ELSE 0 END) [2012-13], "
                    StrSQL &= "sum(CASE when BEG_PER_ID='2013-14' THEN BEG_TOTAL ELSE 0 END) [2013-14], BEH_DELETED, BEH_STATUS, "
                    StrSQL &= "sum(CASE when BEG_PER_ID='2014-15' THEN BEG_TOTAL ELSE 0 END) [2014-15], "
                    StrSQL &= "sum(CASE when BEG_PER_ID='2015-16' THEN BEG_TOTAL ELSE 0 END) [2015-16] from BudExpense_H "
                    StrSQL &= "inner JOIN BudExpense_D on BEH_ID=BED_BEH_ID INNER JOIN BudExpense_G on BED_ID=BEG_BED_ID "
                    StrSQL &= "INNER JOIN OASISFIN.dbo.ACCOUNTS_M on ACT_ID=BED_ACT_ID INNER JOIN OASISFIN.dbo.COSTCENTER_M on CCT_ID=BED_CCT_ID "
                    StrSQL &= "WHERE BEH_BSU_ID ='" & Session("sBsuid") & "' and BEH_USR_NAME='" & Session("sUsr_id") & "' "
                    StrSQL &= "GROUP by BEH_DELETED, BEH_STATUS, BEH_ID, CCT_DESCR, ACT_NAME, BED_ASM_ID, BEH_DATE having sum(BEG_TOTAL)>0) A "
                    StrSQL &= " WHERE 1 = 1 " & ApprStatus
                    EditPagePath = "../Budget/ExpBudget.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "BEH_ID"
                    NoAddingAllowed = True
                    HeaderTitle = "Approval for Expense Budget"

                Case "P130045"
                    StrSQL = "     SELECT PFZ_ID,[FREEZE TYPE],[MONTH],[TRANSACTION DATE],[REMARKS] FROM ("
                    StrSQL &= "   SELECT PFZ_ID ,CASE WHEN PFZ_TYPE='A' THEN 'Attendance' ELSE 'Payroll' END [FREEZE TYPE],datename(month,dateadd(month, PFZ_MONTH-1 , 0)) + ' - ' + cast(PFZ_YEAR as varchar) [MONTH] ,replace(convert(varchar(30),PFZ_TRANDT,106),' ','/') [TRANSACTION DATE],PFZ_REMARKS [REMARKS] "
                    StrSQL &= "    FROM PAYROLL_FREEZE"
                    StrSQL &= " WHERE PFZ_BSU_ID='" & Session("sBsuid") & "') A WHERE 1=1 "
                    EditPagePath = "../Payroll/empFreezePayrollAttendance.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "PFZ_ID"
                    HeaderTitle = "Freeze Payroll /Attendance"
                Case "F733040"
                    StrSQL = "     SELECT SCI_ID,[STUDENT ID],[STUDENT NAME], [ISSUED DATE], [REMARKS]  FROM ("
                    StrSQL &= "   select SCI_ID,STU_NO [STUDENT ID],isnull(STU_FIRSTNAME,'') + ' ' + isnull(STU_MIDNAME,'') + ' ' + isnull(STU_LASTNAME,'')  [STUDENT NAME], replace(convert(varchar(30),SCI_ISSUEDT,106),' ','/') [ISSUED DATE],SCI_REMARKS [REMARKS]  "
                    StrSQL &= "    FROM   [STUDENT_CARDISSUED] INNER JOIN  Student_M SM   ON SCI_STU_ID=STU_ID"
                    StrSQL &= " WHERE SCI_BSU_ID='" & Session("sBsuid") & "') A WHERE 1=1 "
                    EditPagePath = "../Fees/FeeCardIssue.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "SCI_ID"
                    HeaderTitle = "Issue Loyalty Card"
                Case "AST0601"
                    StrSQL = "     SELECT  ASI_ID ,[SUB CATEGORY] ,[MAKE/BRAND],[MODEL/YEAR] FROM ("
                    StrSQL &= "    Select ASI_ID ,ASC_DESCR [SUB CATEGORY] ,ASI_NEW_MKE_DESCR [MAKE/BRAND],ASI_NEW_MDL_DESCR [MODEL/YEAR]"
                    StrSQL &= "   FROM ASSETS.ASSETS_INIT   LEFT OUTER JOIN ASSETS.ASSET_SUBCATEGORY_M ON ASI_ASC_ID = ASC_ID "
                    StrSQL &= "   LEFT OUTER JOIN ASSETS.ASSET_MAKE ON ASI_MKE_ID=MKE_ID "
                    StrSQL &= "   LEFT OUTER JOIN ASSETS.ASSET_MODEL ON ASI_MDL_ID=MDL_ID "
                    StrSQL &= "   WHERE ISNULL(ASI_NEW_MKE_DESCR,'') <>'' OR ISNULL(ASI_NEW_MDL_DESCR,'') <>'' ) A WHERE 1=1 "
                    EditPagePath = "../Asset/AssetRequestToCreateMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "ASI_ID"
                    HeaderTitle = "Create Master"
                    NoAddingAllowed = True
                Case "AST0740", "AST0742", "AST0745"
                    StrSQL = " SELECT  SVH_ID,[DOC NO] ,[REQUEST DATE],[SUPPLIER],[REQUESTED BY],[REMARKS]  FROM ("
                    StrSQL &= " SELECT  SVH_ID,SVH_DOCNO [DOC NO], replace(convert(varchar(30),SVH_REQDT,106),' ','/') [REQUEST DATE] ,"
                    StrSQL &= " SVH_SUP_ACT_ID + ' - ' +  isnull(SUP.ACT_NAME,'') [SUPPLIER], SVH_REQUESTEDBY [REQUESTED BY], SVH_REMARKS [REMARKS],isnull(SVH_bApproved,0) SVH_bApproved "
                    StrSQL &= "FROM ASSETS.[SERVICE_H]  LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M BSU ON SVH_BSU_ID=BSU.BSU_ID "
                    StrSQL &= "LEFT OUTER JOIN [OASIS].dbo.vw_OSF_ACCOUNTS_M SUP  ON SUP.ACT_ID=SVH_SUP_ACT_ID    "
                    StrSQL &= "WHERE SVH_BSU_ID ='" & Session("sBsuid") & "') A "
                    StrSQL &= "WHERE 1 = 1 "

                    If MainMenuCode = "AST0740" Then
                        HeaderTitle = "Asset Service Request"
                    ElseIf MainMenuCode = "AST0742" Then
                        CheckBox1Text = "All"
                        CheckBox2Text = "Approved"
                        CheckBox3Text = "Not Approved"
                        NoAddingAllowed = True
                        If Page.IsPostBack = False Then rad3.Checked = True
                        If rad2.Checked Then
                            StrSQL &= " AND SVH_bApproved =1 "
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND SVH_bApproved =0 "
                        End If
                        HeaderTitle = "Approve Asset Service Request"
                    ElseIf MainMenuCode = "AST0745" Then
                        NoAddingAllowed = True
                        If Page.IsPostBack = False Then rad3.Checked = True
                        StrSQL &= " AND SVH_bApproved =1 "
                        HeaderTitle = "Asset Service"
                    End If
                    EditPagePath = "../Asset/AssetService.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "SVH_ID"
                Case "P130048", "P130198"
                    ' StrSQL = "     SELECT SFH_ID,[MONTH],[TOTAL AMOUNT],[REMARKS],[ENTERED DATE],[APPROVED DATE],[APPROVED BY] FROM ("
                    StrSQL = "     SELECT SFH_ID,[MONTH],[REMARKS],[ENTERED DATE],[APPROVED DATE],[APPROVED BY] FROM ("
                    StrSQL &= "   SELECT SFH_ID,datename(month,dateadd(month, SFH_MONTH-1 , 0)) + ' - ' + cast(SFH_YEAR as varchar) [MONTH],cast(SUM(SFD_EARNED)AS FLOAT) [TOTAL AMOUNT],"
                    StrSQL &= "   SFH_REMARKS [REMARKS],replace(convert(varchar(30),SFH_ENTEREDDT,106),' ','/') [ENTERED DATE],replace(convert(varchar(30),SFH_APPROVEDDT,106),' ','/') [APPROVED DATE],SFH_APPROVEDBY [APPROVED BY],isnull(SFH_bApproved,0) SFH_bApproved,isnull(SFH_bForwarded,0) SFH_bForwarded "
                    StrSQL &= "   FROM SALARY_FREEZE_H LEFT OUTER JOIN SALARY_FREEZE_D ON SFH_ID=SFD_SFH_ID"
                    StrSQL &= " WHERE SFH_BSU_ID='" & Session("sBsuid") & "' "
                    StrSQL &= "   GROUP BY SFH_BSU_ID,SFH_ID,datename(month,dateadd(month, SFH_MONTH-1 , 0)) + ' - ' + cast(SFH_YEAR as varchar),"
                    StrSQL &= "   SFH_REMARKS,SFH_ENTEREDDT, SFH_APPROVEDDT, SFH_APPROVEDBY ,SFH_bApproved,SFH_bForwarded) A WHERE 1=1 "
                    EditPagePath = "../Payroll/empSalaryFreezeApproval.aspx"
                    If MainMenuCode = "P130198" Then
                        CheckBox1Text = "All"
                        CheckBox2Text = "Approved"
                        CheckBox3Text = "Not Approved"
                        NoAddingAllowed = True
                        If Page.IsPostBack = False Then rad3.Checked = True
                        If rad2.Checked Then
                            StrSQL &= " AND SFH_bApproved =1 "
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND SFH_bApproved =0 "
                        End If
                        StrSQL &= " AND SFH_bForwarded =1 "
                        HeaderTitle = "Approve Salary Detail"
                        NoAddingAllowed = True
                    Else
                        CheckBox1Text = "All"
                        CheckBox2Text = "Forwarded"
                        CheckBox3Text = "Not Forwarded"
                        If Page.IsPostBack = False Then rad3.Checked = True
                        If rad2.Checked Then
                            StrSQL &= " AND SFH_bForwarded =1 "
                        ElseIf rad3.Checked Then
                            StrSQL &= " AND SFH_bForwarded =0 "
                        End If
                        HeaderTitle = "Forward Salary For Approval"
                    End If
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "SFH_ID"

                Case "T200105" 'Product Master
                    StrSQL = "select ID,[PRODUCT ID], [PRODUCT NAME] from (select prod_id [id],prod_id [PRODUCT ID], PROD_NAME [product name], PROD_HRS, PROD_KM from TPTHiringProduct) A where 1=1 "
                    EditPagePath = "../Transport/ExtraHiring/tptproduct.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[PRODUCT ID]"
                    HeaderTitle = "Product Master"

                Case "T200115" 'Rate Card and Quotations BSU
                    StrSQL = "select CNB_ID, [Contract No], Date, Client, [Rate/Km], [Rate/Hr] from (select CNB_ID, CNB_numberstr [Contract No], replace(convert(varchar(30), CNB_Date, 106),' ','/') DATE, BSU_NAME Client, CNB_RATEKM [Rate/Km], CNB_RATEHR [Rate/Hr]  "
                    StrSQL &= "FROM TPTCONTRACT_B inner JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=CNB_BSU_ID) A where 1=1 "
                    EditPagePath = "../Transport/ExtraHiring/tptContractRateBSU.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "CNB_ID"
                    HeaderTitle = "Rates and Contracts BSU"

                Case "T200120" 'Rate Card and Quotations
                    StrSQL = "select CNH_ID, [Contract No], Date, "
                    StrSQL &= "[Description],[Party Name], "
                    StrSQL &= "[Sales Man],[SEASON],[VEHICLE TYPE] from (select CNH_ID, CNH_numberstr [Contract No], replace(convert(varchar(30), CNH_Date, 106),' ','/') DATE, "
                    StrSQL &= "TRL_DESCR [DESCRIPTION],"
                    StrSQL &= "stuff((SELECT act_id+'-'+ACT_NAME+ CHAR(13)+CHAR(10) FROM OASIS..fnSplitMe(CNH_CLIENT_ID,',')  inner JOIN oasisfin..ACCOUNTS_M on ID=act_id for xml path(''), type).value('.','varchar(max)'), 1, 0, '') as [PARTY NAME], "
                    StrSQL &= "isnull(SLSM_NAME,'') [SALES MAN] "
                    StrSQL &= ",SEAS_NAME [SEASON],VTYP_DESCRIPTION [VEHICLE TYPE] "
                    StrSQL &= "FROM TPTCONTRACT_H LEFT OUTER JOIN TPTSALESMAN ON SLSM_ID=CNH_SALESMAN_ID  "
                    StrSQL &= "inner JOIN tptSeason on seas_id=CNH_SEAS_ID inner join VEHTYPE_M on VTYP_ID=CNH_VTYP_ID LEFT OUTER JOIN tpt_ratelist ON trl_id=CNH_TRL_ID where cnh_noshow=0) b  "
                    StrSQL &= " where 1 = 1   "
                    EditPagePath = "../Transport/ExtraHiring/tptContractRate.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "CNH_ID DESC"
                    HeaderTitle = "Rates and Contracts"

                Case "T200123" 'DRIVER LEAVE DETAILS
                    StrSQL = "select ID,[JOB TYPE], [DRIVER NAME],[FROM DATE],[TO DATE] from (SELECT TDL_ID ID ,Case when TDL_JOBTYPE=0 then 'NES' else case when TDL_JOBTYPE=1 then 'SCHOOL' else 'SUB CON' end end [Job Type],CASE WHEN TDL_JOBTYPE IN(0,2) THEN TPTD_EMPNO ELSE EMPNO END [EMP NO],"
                    StrSQL &= "CASE WHEN TDL_JOBTYPE IN(0,2) THEN TPTD_NAME ELSE ISNULL(EMP_FNAME+' '+EMP_MNAME+ ' ' + EMP_LNAME,'') END [DRIVER NAME] ,TDL_LEAVE_FDATE [From Date],TDL_LEAVE_TDATE [To Date] "
                    StrSQL &= " FROM TPTDRIVERSLEAVE  LEFT OUTER JOIN OASIS..EMPLOYEE_M ON EMP_ID=TDL_EMP_ID LEFT OUTER JOIN TPTDRIVER_M ON TPTD_ID =TDL_EMP_ID WHERE TDL_DELETED=0) A where 1=1 "
                    EditPagePath = "../Transport/ExtraHiring/tptDriversLeave.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[DRIVER NAME]"
                    HeaderTitle = "Drivers Leave Details"

                Case "T200125" 'Transport Hiring and Job
                    Dim Yearend As Date
                    Dim BsuFreezdt As Date
                    Dim fyear As Integer = Session("F_YEAR")
                    Dim sbsuId As String = Session("sBsuid")
                    Dim whereCon As String = "1=1"

                    Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & fyear & "'", "OASIS_TRANSPORTConnectionString")
                    BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & sbsuId & "'", "OASIS_TRANSPORTConnectionString")
                    StrSQL = "select TRN_No, [Job No],[Job Date],Client,[Group], [Pickup Point],  [DropOff Point], Location,  [Service],[Vehile type] from ("
                    StrSQL &= "select TRN_No,STS_No [Group], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date], LOC_DESCRIPTION Location, "
                    StrSQL &= "isnull(ACT_NAME,BSU_NAME) Client, ISNULL(TSM_DESCR,'') [Service], TPT_PickupPoint + ' -Time:'+ isnull(TPT_JobSt_Time,'') [Pickup Point], TPT_DropOffPoint [DropOff Point],TPT_JobSt_Time [PickUp Time],TPT_JOBCl_km,VTYP_DESCRIPTION[Vehile type],TPT_JobCl_Time [Dropoff Time]   "
                    StrSQL &= "FROM TPTHiring "
                    StrSQL &= "inner JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                    StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                    StrSQL &= "LEFT OUTER JOIN vehtype_m on vtyp_id=TRN_Veh_CatId "
                    StrSQL &= "where  TPT_Deleted=0 and TPT_Bsu_id like '%" & sbsuId & "%' "
                    EditPagePath = "../Transport/ExtraHiring/tptHiringJob.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "TRN_NO DESC"
                    If rad1.Checked Then
                        StrSQL &= "and TPT_Fyear<=" & fyear & " and TRN_STATUS='N') A where " & whereCon
                        HeaderTitle = "Open Job Order List"
                        'NoAddingAllowed = (Session("F_YEAR") < "2016")
                        NoAddingAllowed = Not (BsuFreezdt <= Yearend)
                    Else
                        StrSQL &= "and TPT_Fyear=" & fyear & " and TRN_STATUS='R') A where " & whereCon
                        HeaderTitle = "Declined Job Order List"
                        ShowGridCheckBox = False
                        NoAddingAllowed = True
                        ButtonText1 = ""
                    End If
                    CheckBox1Text = "Open Jobs"
                    CheckBox2Text = "Declined Jobs"

                Case "T200126" 'Cancel Jobs
                    CheckBox1Text = "Open Jobs"
                    CheckBox2Text = "Cancelled Jobs"
                    StrSQL = "select TRN_No, [Job No], [Job Date],[Entry Date], Location, Client,[Group],[Service], [Pickup Point], [DropOff Point] from ("
                    StrSQL &= "select TRN_No, TRN_JobnumberStr [Job No],STS_No [Group], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date], LOC_DESCRIPTION Location, "
                    StrSQL &= "isnull(ACT_NAME,BSU_NAME) Client, ISNULL(TSM_DESCR,'') [Service], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point] "
                    StrSQL &= "FROM TPTHiring "
                    StrSQL &= "inner JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                    StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                    StrSQL &= "where  TPT_Deleted=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%' "
                    EditPagePath = "../Transport/ExtraHiring/tptHiringJob.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "TRN_NO DESC"
                    HeaderTitle = "Open Job Order List"

                    If rad1.Checked Then

                        StrSQL &= "and TPT_Fyear<=" & Session("F_YEAR") & " and TRN_STATUS='') A where 1=1 "
                        HeaderTitle = "Open Job Order List"
                        'ShowGridCheckBox = True
                        'ButtonText1 = "Cancel Job"
                        NoAddingAllowed = True
                    Else
                        StrSQL &= "and TPT_Fyear=" & Session("F_YEAR") & " and TRN_STATUS='C') A where 1=1 "
                        HeaderTitle = "Cancelled Job Order List"
                        ShowGridCheckBox = False
                        NoAddingAllowed = True
                        ButtonText1 = ""
                    End If

                Case "T200127" 'Confirm Multiple Jobs
                    IsStoredProcedure = True
                    StrSQL = "GetNewJObs"
                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)

                    StrSortCol = "TRN_NO desc"
                    CheckBox1Text = "New Job Order List"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    NoAddingAllowed = True
                    ShowGridCheckBox = True
                    StrSortCol = "[NCN_NO] DESC"
                    EditPagePath = "../Transport/ExtraHiring/tptCreditNote.aspx"
                    ButtonText1 = "Confirm Job(s)"
                    HeaderTitle = "List of New Jobs"
                    NoAddingAllowed = True
                    NoViewAllowed = True


                Case "T200130" 'Transport Hiring and Billing jOB Execution
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Executed"
                    Dim showExecuted As Boolean = False
                    If rad1.Checked Then showExecuted = (CheckBox1Text = "Executed")
                    If rad2.Checked Then showExecuted = (CheckBox2Text = "Executed")
                    If showExecuted Then
                        StrSQL = "SELECT TRN_No, [Job No],[Job Date],[Entry Date],[Job Type],Client,[Group], [Service], [Pickup Point],[Amount], [DropOff Point] from ("
                        StrSQL &= "select TRN_No,STS_No [Group], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date], LPO_no as [Ref No],case when tpt_data=0 then 'NES' else case when tpt_data=1  then 'SCHOOL' else case when tpt_data=2 then  'SUB CON' else ''  end end end [Job Type], VEH_REGNO [Vehicle No], TPTD_NAME [Driver], "
                        StrSQL &= "isnull(ACT_NAME,BSU_NAME) Client, ISNULL(TSM_DESCR,'') [Service], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
                        StrSQL &= "(SELECT isnull(sum(INV_AMT),0)Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount "
                        StrSQL &= "FROM TPTHiring "
                        StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M ON VEH_ID=TPT_Veh_id "
                        StrSQL &= "LEFT OUTER JOIN tptdriver_M ON tptd_id=TPT_Drv_id "
                        StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                        StrSQL &= " where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='S' and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "
                        HeaderTitle = "Executed Job Orders List"
                    Else
                        StrSQL = "select TRN_No, [Job No], [Job Date],[Entry Date],[Job Type], Client,[Group], [Service], [Pickup Point], [DropOff Point] from ("
                        StrSQL &= "select TRN_No,STS_No [Group], TRN_JobnumberStr [Job No],replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date], LPO_no as [Ref No],case when tpt_data=0 then 'NES' else case when tpt_data=1  then 'SCHOOL' else case when tpt_data=2 then  'SUB CON' else ''  end end end [Job Type], "
                        StrSQL &= "isnull(ACT_NAME,BSU_NAME) Client, ISNULL(TSM_DESCR,'') [Service], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point],'' Amount "
                        StrSQL &= "FROM TPTHiring "
                        StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                        StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                        StrSQL &= "where TPT_Fyear <=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='' and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "
                        HeaderTitle = "Pending Job Orders List"
                    End If
                    EditPagePath = "../Transport/ExtraHiring/tptHiringBilling.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "TRN_NO Desc"
                    NoAddingAllowed = (("900500900501").ToString.Contains(Session("sBsuid")))

                Case "T200135" 'Transport Hiring and Billing
                    StrSQL = "select TRN_No, [Job No], [Job Date],[Entry Date], Location, Client,[Group], [Service], [Pickup Point], [DropOff Point] from ("
                    StrSQL &= "select TRN_No,STS_No [Group], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date], LOC_DESCRIPTION Location, "
                    StrSQL &= "isnull(ACT_NAME,BSU_NAME) Client, ISNULL(TSM_DESCR,'') [Service], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point] "
                    StrSQL &= "FROM TPTHiring "
                    StrSQL &= "inner JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                    StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                    'StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%' "
                    StrSQL &= "where  TPT_Deleted=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%' "
                    If rad1.Checked Then
                        'StrSQL &= "and TRN_STATUS in('S','A')) A where 1=1 "
                        StrSQL &= "and TPT_Fyear<=" & Session("F_YEAR") & " and TRN_STATUS in('S','A')) A where 1=1 "
                        HeaderTitle = "List of Open Jobs"
                        ButtonText1 = "Change Status to FOC"
                        ShowGridCheckBox = True
                    Else
                        'StrSQL &= "and TRN_STATUS='F') A where 1=1 "
                        StrSQL &= "and TPT_Fyear=" & Session("F_YEAR") & " and TRN_STATUS='F') A where 1=1 "
                        ButtonText1 = ""
                        HeaderTitle = "List of FOC Jobs"
                        ShowGridCheckBox = False
                    End If
                    EditPagePath = "../Transport/ExtraHiring/tptHiringBilling.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "TRN_NO desc"
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "FOC"
                    NoAddingAllowed = True


                Case "T200140" 'Transport Hiring and Billing Invoice
                    CheckBox1Text = "Job Orders"
                    CheckBox2Text = "Invoices"
                    CheckBox3Text = "Pending Approval"
                    CheckBox4Text = "All"
                    lblError.Text = ""
                    ListButton2.Text = "Check Total Amount"
                    ListButton2.Visible = True
                    If rad1.Checked Then

                        StrSQL = "SELECT TRN_No, [Job No], [Job Date],[Entry Date], Client, Amount,[TAX Amount],[Net Amount],[Group],[Emirate\TAX\BusType]  from ("
                        StrSQL &= "select TRN_No,STS_No [Group],isnull([TRN_BATCH],0) [Batch No], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date],isnull(ACT_NAME,BSU_NAME) Client,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount,isnull(TPT_TAX_AMOUNT,0) [TAX Amount],isnull(TPT_TAX_NET_AMOUNT,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no)) [Net Amount], ISNULL(TPTVEHICLE_M.VEH_REGNO,vm2.VEH_REGNO) [Vehicle No],ISNULL(TPTD_NAME,Driver)[Driver], "
                        StrSQL &= " LPO_no as [Ref No], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
                        StrSQL &= "'<a href=../Inventory/IFrameNew.aspx?MODE=e&Id=0&path=TRANSPORT&filename=' + THI_FILENAME + '&contenttype='+ THI_CONTENTTYPE+ '" & ">ViewDocument</a>' as Document, "
                        'StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' Status,'' [Debit Note No],isnull(EMR_DESCR,'NA')+'-'+ isnull(TPT_TAX_CODE,'NA')+'-'+case when (case when isnull(vm2.VEH_PLATE_COLOR,'')='' then case when TPT_Veh_id=0 and isnull(TPT_VEHREGNO,'')<>'' then 'Private Hire' else vm1.VEH_PLATE_COLOR  end else vm2.VEH_PLATE_COLOR end)='Private Hire' THEN 'WHITE BUS' ELSE 'YELLOW BUS' END  [Emirate\TAX\BusType],''[Tax Code]  "
                        StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' Status,'' [Debit Note No],isnull(EMR_DESCR,'NA')+'-'+ isnull(TPT_TAX_CODE,'NA')+'-'+case when (case when isnull(vm2.VEH_PLATE_COLOR,'')='' then case when TPT_Veh_id=0 and isnull(TPT_VEHREGNO,'')<>'' then 'Private Hire' else vm2.VEH_PLATE_COLOR  end else vm2.VEH_PLATE_COLOR end)='Private Hire' THEN 'WHITE BUS' ELSE 'YELLOW BUS' END  [Emirate\TAX\BusType],''[Tax Code]  "
                        StrSQL &= "FROM TPTHiring with(nolock) "
                        StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M with(nolock) on TPT_Loc_id=LOC_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id "
                        StrSQL &= "LEFT OUTER JOIN VEHICLE_M VM1 with(nolock) ON VM1.VEH_REGNO=TPTVEHICLE_M.VEH_REGNO LEFT OUTER JOIN VEHICLE_M VM2 with(nolock) ON VM2.VEH_ID=TPT_Veh_id LEFT OUTER JOIN VW_DRIVER with(nolock) ON DRIVER_EMPID=TPT_Drv_id "
                        StrSQL &= "LEFT OUTER JOIN TPTDRIVER_M with(nolock) ON TPTD_ID=TPT_Drv_id "
                        StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M with(nolock) on ACT_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN oasis.TAX.VW_TAX_EMIRATES with(nolock) on EMR_CODE=TPT_EMR_CODE "
                        StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M with(nolock) on BSU_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M with(nolock) on TSM_ID=TPT_SERVICE_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTHIRING_I with(nolock) on THI_DOCNO=TRN_JobnumberStr "
                        StrSQL &= "where TPT_Fyear <=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='S' and isnull(TPT_BllId,0)=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "

                        HeaderTitle = "Job Order Invoicing"
                        ButtonText1 = "Generate Invoice"
                        ShowGridCheckBox = True
                        NoAddingAllowed = True
                        EditPagePath = "../Transport/ExtraHiring/tptHiringBilling.aspx"
                        strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                        StrSortCol = "TRN_NO Desc"
                    ElseIf rad2.Checked Then
                        StrSQL = "SELECT [Batch],[Batch no],[Batch Date],[Client],[Total Amount],[Tax Code],[Tax Amount],[Net Amount],[Status],[Debit Note No] FROM ( SELECT  B.TRN_BATCHSTR [Batch],B.TRN_BATCHSTR [Batch No],max(TRN_BATCHDATE) [Batch Date],isnull(max(ACT_NAME),'') Client,isnull(SUM(INV_Amt),0)  [Total Amount] "
                        StrSQL &= ",0 TRN_NO,'' [job no],'' date,'' [job date],'' [Entry Date],'' [Group],'' [Vehicle No],'' Driver,'' [Pickup Point],'' [DropOff Point],0 amount,'' Document,''[Emirate\TAX\BusType] "
                        StrSQL &= " ,MAX(CASE WHEN TRN_STATUS='I' THEN 'NOT POSTED' ELSE 'POSTED' END) Status,isnull(max(b.tpt_jhd_docno),'') as [Debit Note No],isnull(max(TPT_TAX_CODE),'NA')[Tax Code],isnull(max(x.TAX_AMOUNT),0)[Tax Amount],isnull(max(x.TPT_TAX_NET_AMOUNT),isnull(SUM(INV_Amt),0))[Net Amount]  from TPTHiring B with(nolock)  "
                        StrSQL &= " LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M with(nolock) on ACT_ID=TPT_Client_id left outer join (select sum(isnull(tpt_tax_amount,0)) TAX_AMOUNT,sum(isnull(TPT_TAX_NET_AMOUNT,0)) TPT_TAX_NET_AMOUNT,trn_batchstr from tpthiring with(nolock) group by trn_batchstr) X  on x.trn_batchstr=b.TRN_BATCHSTR  "
                        StrSQL &= " LEFT OUTER JOIN TPTHiringInvoice with(nolock) ON B.TRN_no =INV_TRN_NO "
                        StrSQL &= " where TPT_Fyear<=" & Session("F_YEAR") & " and Not B.TRN_BATCH Is null AND TPT_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= " GROUP BY B.TRN_BATCH,B.TRN_BATCHSTR ) A WHERE 1=1"
                        HideExportButton = True
                        EditPagePath = "../Transport/ExtraHiring/tptPostingJob.aspx"
                        strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                        NoAddingAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "[Batch NO] DESC"
                    ElseIf rad3.Checked Then
                        StrSQL = "SELECT TRN_No, [Job No], [Job Date],[Entry Date], Client, Amount, [Vehicle No], [Driver], [Group] , [Pickup Point], [DropOff Point] from ("
                        StrSQL &= "select TRN_No,STS_No [Group],isnull([TRN_BATCH],0) [Batch No], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date],isnull(ACT_NAME,BSU_NAME) Client,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount, VEH_REGNO [Vehicle No], TPTD_NAME [Driver], "
                        StrSQL &= "TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
                        StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' Status,'' [Debit Note No],'' Document,''[Emirate\TAX\BusType],''[Tax Code],0 [Tax Amount],0[Net Amount] "
                        StrSQL &= "FROM TPTHiring with(nolock) "
                        StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M with(nolock) on TPT_Loc_id=LOC_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id "
                        StrSQL &= "LEFT OUTER JOIN TPTDRIVER_M with(nolock) ON TPTD_ID=TPT_Drv_id "
                        StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M with(nolock) on ACT_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M with(nolock) on BSU_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M with(nolock) on TSM_ID=TPT_SERVICE_ID "
                        StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='A' and isnull(TPT_BllId,0)=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "
                        HeaderTitle = "Pending Job Orders"
                        EditPagePath = "../Transport/ExtraHiring/tptHiringBilling.aspx"
                        strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                        StrSortCol = "TRN_NO Desc"
                        NoAddingAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                    Else
                        StrSQL = "SELECT TRN_No, [Job No], [Job Date],[Batch No], Client, Amount, [Status], [Driver],[Group] , [Pickup Point], [DropOff Point] from ("
                        StrSQL &= "select TRN_No,STS_No [Group], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],"
                        StrSQL &= "isnull(TRN_BATCHSTR,'') [Batch No],isnull(ACT_NAME,BSU_NAME) Client,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount, "
                        StrSQL &= " case when TRN_STATUS ='' THEN 'Open Job Order' else case when TRN_STATUS='A' THEN 'Waiting For Approval' else case when TRN_STATUS='C' THEN 'Cancelled' "
                        StrSQL &= "else case when TRN_STATUS='I' THEN 'Invoiced' else case when TRN_STATUS='P' THEN 'Posted' else case when TRN_STATUS='S' THEN 'Executed' else case when "
                        StrSQL &= "TRN_STATUS='F' THEN 'Free of Cost' end end end end end end end  Status,VEH_REGNO [Vehicle No], TPTD_NAME [Driver], "
                        StrSQL &= " LPO_no as [Ref No], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
                        StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' [Debit Note No],'' [entry date],'' [Document],''[Emirate\TAX\BusType],0 [Tax Amount],0[Net Amount] "
                        StrSQL &= "FROM TPTHiring with(nolock) "
                        StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M with(nolock) on TPT_Loc_id=LOC_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M with(nolock) ON VEH_ID=TPT_Veh_id "
                        StrSQL &= "LEFT OUTER JOIN TPTDRIVER_M with(nolock) ON TPTD_ID=TPT_Drv_id "
                        StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M with(nolock) on ACT_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M with(nolock) on BSU_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M with(nolock) on TSM_ID=TPT_SERVICE_ID "
                        StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and isnull(TPT_BllId,0)=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "
                        HeaderTitle = "Job Order Listing"
                        ButtonText1 = ""
                        ShowGridCheckBox = False
                        NoAddingAllowed = True
                        NoViewAllowed = True
                        EditPagePath = "../Transport/ExtraHiring/tptHiringBilling.aspx"
                        strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                        StrSortCol = "TRN_NO Desc"
                    End If
                    NoAddingAllowed = True


                Case "T200190"
                    Dim Yearend As Date
                    Dim BsuFreezdt As Date
                    Yearend = Mainclass.getDataValue("select fyr_todt from oasis..financialyear_s where fyr_id='" & Session("F_YEAR") & "'", "OASIS_TRANSPORTConnectionString")
                    BsuFreezdt = Mainclass.getDataValue("select bsu_freezedt from oasis..businessunit_m where bsu_id='" & Session("sBsuid") & "'", "OASIS_TRANSPORTConnectionString")
                    StrSQL = "select TRN_No, [Job No], [Job Date],[Entry Date], Location, Client,[Group], [Service], [Pickup Point], [DropOff Point] from ("
                    StrSQL &= "select TRN_No,STS_No [Group], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date], LOC_DESCRIPTION Location, "
                    StrSQL &= "isnull(ACT_NAME,BSU_NAME) Client, ISNULL(TSM_DESCR,'') [Service], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point] "
                    StrSQL &= "FROM TPTHiring "
                    StrSQL &= "inner JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                    StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                    'StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%' "
                    StrSQL &= "where TPT_Deleted=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%' "
                    StrSQL &= "and TRN_STATUS='' and cast(tpt_fyear as int) <= cast(" & Session("F_YEAR") & " as int) ) A where 1=1 "
                    If MainMenuCode = "T200190" Then
                        EditPagePath = "../Transport/ExtraHiring/tptFleet.aspx"
                    Else
                        EditPagePath = "../Transport/ExtraHiring/tptHiringJob.aspx"
                    End If
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "TRN_NO DESC"
                    HeaderTitle = "Open Job Order List"

                    'NoAddingAllowed = Not (BsuFreezdt <= Yearend)
                    NoAddingAllowed = True

                Case "T200216"
                    'CheckBox1Text = "Pending"
                    'CheckBox2Text = "Invoice"
                    'Dim Qry As New StringBuilder
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'If rad1.Checked Then
                    '    StrSQL = "SELECT [id],[StrNo],[School Amount],[STS Amount],[BBT Amount], [NET Amount],Posted FROM (  "
                    '    StrSQL &= "SELECT LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME id, LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME [NO], '' TRNO, getdate() Date, "
                    '    StrSQL &= "LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME [StrNo], "
                    '    StrSQL &= "SUM(EGC_NETAMOUNT) [NET AMOUNT], "
                    '    StrSQL &= "SUM(case when BSU_SHORTNAME =egc_c_bsu_shortname then EGC_NETAMOUNT else 0 end) [School Amount],SUM(case when egc_c_bsu_shortname ='STS' then EGC_NETAMOUNT else 0 end) [STS Amount],SUM(case when egc_c_bsu_shortname ='BBT' then EGC_NETAMOUNT else 0 end) [BBT Amount],"
                    '    StrSQL &= "(select isnull(SUM(EGI_NET),0) from TPTEXGRATIA_INV where EGI_DELETE=0 and EGI_STRNO =LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME  )[POSTED]  "
                    '    'StrSQL &= "FROM  TPTEXGRATIACHARGES INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID "
                    '    StrSQL &= "FROM  ("
                    '    'StrSQL &= "TPTEXGRATIACHARGES 
                    '    'added on 14-06-2017 Start
                    '    StrSQL &= "select EGC_ID, EGC_TRDATE, egc_bsu_id, EGC_EMP_ID, EGC_DRIVERTYPE, EGC_TSTART, EGC_TEND, EGC_TTOTAL, EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE, EGC_PURPOSE, EGC_OUTSTATION, EGC_C_BSU_SHORTNAME, EGC_TOTAL, EGC_OOTAMOUNT from TPTEXGRATIACHARGES where egc_id  not in(select EGC_ID from TPTEXGRATIACHARGES where egc_bsu_id='125015' and egc_c_bsu_shortname='OOD')"
                    '    StrSQL &= "union "
                    '    StrSQL &= "select EGC_ID, EGC_TRDATE, '121013' egc_bsu_id, EGC_EMP_ID, EGC_DRIVERTYPE, EGC_TSTART, EGC_TEND, EGC_TTOTAL, EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE, EGC_PURPOSE, EGC_OUTSTATION, EGC_C_BSU_SHORTNAME, EGC_TOTAL, EGC_OOTAMOUNT from TPTEXGRATIACHARGES where egc_bsu_id='125015' and egc_c_bsu_shortname='OOD') a"
                    '    'added on 14-06-2017 End
                    '    StrSQL &= " INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID "
                    '    StrSQL &= "WHERE EGC_EGI_ID = 0 AND EGC_DELETED=0 and egc_emp_id<>0 and EGC_BSU_ID IN (SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID FROM vw_oso_SERVICES_BSU_M INNER JOIN BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN OASIS..MENURIGHTS_S  MENURIGHTS_S ON MENURIGHTS_S.MNR_BSU_ID = vw_oso_SERVICES_BSU_M.SVB_BSU_ID AND MENURIGHTS_S.MNR_MNU_ID in ('T200145', 'F300155','F300165','F715065','T200216')"
                    '    StrSQL &= "INNER JOIN OASIS..USERS_M USERS_M ON MENURIGHTS_S.MNR_ROL_ID = USERS_M.USR_ROL_ID INNER JOIN "
                    '    StrSQL &= "OASIS..USERACCESS_S USERACCESS_S ON USERS_M.USR_ID = USERACCESS_S.USA_USR_ID AND "
                    '    StrSQL &= "vw_oso_SERVICES_BSU_M.SVB_BSU_ID = USERACCESS_S.USA_BSU_ID "
                    '    StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='" & Session("sBsuid") & "') AND "
                    '    StrSQL &= "(vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1) AND "
                    '    StrSQL &= "(USERS_M.USR_NAME ='" & Session("sUsr_name") & "') AND "
                    '    StrSQL &= "isnull(SVB_bAVAILABLE,0)=1 and isnull(SVB_BActive,0)=1 "
                    '    StrSQL &= " union select bsu_id from VW_EXGratiaInvoicingBSUids )"
                    '    StrSQL &= " and EGC_TRDATE<=(select FYR_TODT from OASIS..FINANCIALYEAR_S WHERE FYR_ID='" & Session("F_YEAR") & "')"
                    '    StrSQL &= "  Group BY LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME) A WHERE 1=1 "
                    '    EditPagePath = "../Transport/ExtraHiring/tptPostingExGratia.aspx"
                    '    HeaderTitle = "Pending ExGratia Charges for Invoicing"
                    '    StrSortCol = "[StrNo] Desc"
                    '    NoViewAllowed = False

                    '    'Qry.Append("SELECT [id],[StrNo],[School Amount],[STS Amount],[BBT Amount], [NET Amount],Posted FROM (  ")
                    '    'Qry.Append("SELECT case when EGC_TRDATE<='2018-03-15' then LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME else LEFT(case when day(EGC_TRDATE)<=15 then dateadd(m,-1,EGC_TRDATE) else dateadd(m,1,EGC_TRDATE) end,3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME END id, case when EGC_TRDATE<='2018-03-15' then LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME else LEFT(case when day(EGC_TRDATE)<=15 then dateadd(m,-1,EGC_TRDATE) else dateadd(m,1,EGC_TRDATE) end,3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME END [NO], '' TRNO, getdate() Date, ")
                    '    'Qry.Append("case when EGC_TRDATE<='2018-03-15' then LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME else LEFT(case when day(EGC_TRDATE)<=15 then dateadd(m,-1,EGC_TRDATE) else dateadd(m,1,EGC_TRDATE) end,3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME END [StrNo], ")
                    '    'Qry.Append("SUM(EGC_NETAMOUNT) [NET AMOUNT], ")
                    '    'Qry.Append("SUM(case when BSU_SHORTNAME =egc_c_bsu_shortname then EGC_NETAMOUNT else 0 end) [School Amount],SUM(case when egc_c_bsu_shortname ='STS' then EGC_NETAMOUNT else 0 end) [STS Amount],SUM(case when egc_c_bsu_shortname ='BBT' then EGC_NETAMOUNT else 0 end) [BBT Amount],")
                    '    'Qry.Append("(select isnull(SUM(EGI_NET),0) from TPTEXGRATIA_INV where EGI_DELETE=0 and EGI_STRNO =case when EGC_TRDATE<='2018-03-15' then LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME else LEFT(case when day(EGC_TRDATE)<=15 then dateadd(m,-1,EGC_TRDATE) else dateadd(m,1,EGC_TRDATE) end,3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME END  )[POSTED]  ")
                    '    'Qry.Append("FROM  (")
                    '    'Qry.Append("select EGC_ID, EGC_TRDATE, egc_bsu_id, EGC_EMP_ID, EGC_DRIVERTYPE, EGC_TSTART, EGC_TEND, EGC_TTOTAL, EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE, EGC_PURPOSE, EGC_OUTSTATION, EGC_C_BSU_SHORTNAME, EGC_TOTAL, EGC_OOTAMOUNT from TPTEXGRATIACHARGES where egc_id  not in(select EGC_ID from TPTEXGRATIACHARGES where egc_bsu_id='125015' and egc_c_bsu_shortname='OOD')")
                    '    'Qry.Append("union ")
                    '    'Qry.Append("select EGC_ID, EGC_TRDATE, '121013' egc_bsu_id, EGC_EMP_ID, EGC_DRIVERTYPE, EGC_TSTART, EGC_TEND, EGC_TTOTAL, EGC_NETAMOUNT, EGC_REMARKS, EGC_EGI_ID, EGC_FYEAR, EGC_USER, EGC_DELETED, EGC_DATE, EGC_PURPOSE, EGC_OUTSTATION, EGC_C_BSU_SHORTNAME, EGC_TOTAL, EGC_OOTAMOUNT from TPTEXGRATIACHARGES where egc_bsu_id='125015' and egc_c_bsu_shortname='OOD') a")
                    '    'Qry.Append(" INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=EGC_BSU_ID ")
                    '    'Qry.Append("WHERE EGC_EGI_ID = 0 AND EGC_DELETED=0 and egc_emp_id<>0 and EGC_BSU_ID IN (SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID FROM vw_oso_SERVICES_BSU_M INNER JOIN BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN OASIS..MENURIGHTS_S  MENURIGHTS_S ON MENURIGHTS_S.MNR_BSU_ID = vw_oso_SERVICES_BSU_M.SVB_BSU_ID AND MENURIGHTS_S.MNR_MNU_ID in ('T200145', 'F300155','F300165','F715065','T200216')")
                    '    'Qry.Append("INNER JOIN OASIS..USERS_M USERS_M ON MENURIGHTS_S.MNR_ROL_ID = USERS_M.USR_ROL_ID INNER JOIN ")
                    '    'Qry.Append("OASIS..USERACCESS_S USERACCESS_S ON USERS_M.USR_ID = USERACCESS_S.USA_USR_ID AND ")
                    '    'Qry.Append("vw_oso_SERVICES_BSU_M.SVB_BSU_ID = USERACCESS_S.USA_BSU_ID ")
                    '    'Qry.Append("WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='" & Session("sBsuid") & "') AND ")
                    '    'Qry.Append("(vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1) AND ")
                    '    'Qry.Append("(USERS_M.USR_NAME ='" & Session("sUsr_name") & "') AND ")
                    '    'Qry.Append("isnull(SVB_bAVAILABLE,0)=1 and isnull(SVB_BActive,0)=1 union select '888881' union select '900007' union select '999998') ")
                    '    'Qry.Append(" and EGC_TRDATE<=(select FYR_TODT from OASIS..FINANCIALYEAR_S WHERE FYR_ID='" & Session("F_YEAR") & "')")
                    '    'Qry.Append("  Group BY case when EGC_TRDATE<='2018-03-15' then LEFT(DATENAME(M,EGC_TRDATE),3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME else LEFT(case when day(EGC_TRDATE)<=15 then dateadd(m,-1,EGC_TRDATE) else dateadd(m,1,EGC_TRDATE) end,3)+DATENAME(YEAR,EGC_TRDATE )+BSU_SHORTNAME END) A WHERE 1=1 ")
                    '    'StrSQL = Qry.ToString()
                    '    'EditPagePath = "../Transport/ExtraHiring/tptPostingExGratia.aspx"
                    '    'HeaderTitle = "Pending ExGratia Charges for Invoicing"
                    '    'StrSortCol = "[StrNo] Desc"
                    '    'NoViewAllowed = False
                    'ElseIf rad2.Checked Then
                    '    StrSQL = "SELECT [id],[Trno],StrNo,[Date],[Net Amount] FROM (select EGI_ID [id],EGI_TRNO[Trno],EGI_DATE[Date],EGI_STRNO[StrNo], 0 posted,  "
                    '    StrSQL &= "EGI_NET[Net Amount],case when EGI_POSTED=1 then 'Posted' else 'Not Posted' end [Status],EGI_POSTING_DATE [Posting Date],  "
                    '    StrSQL &= "EGI_USER_NAME [user],0[School Amount],0[STS Amount],0 [BBT Amount] from TPTEXGRATIA_INV WHERE EGI_BSU_ID='" & Session("sBsuid") & "' and EGI_FYEAR=" & Session("F_YEAR") & " ) A WHERE 1=1 "
                    '    EditPagePath = "../Transport/ExtraHiring/tptPostingExGratia.aspx"
                    '    HeaderTitle = "ExGratia Charges Invoice"
                    '    StrSortCol = "[ID] Desc"
                    '    NoViewAllowed = False
                    'End If
                    'added on 15Apr2020
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Invoice"
                    Dim Qry As New StringBuilder
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString

                    IsStoredProcedure = True
                    StrSQL = "GETPendingExgratiaEntriesList"
                    ReDim SPParam(3)

                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        SPParam(3) = Mainclass.CreateSqlParameter("@OPTION", 4, SqlDbType.Int)
                        EditPagePath = "../Transport/ExtraHiring/tptPostingExGratia.aspx"
                        HeaderTitle = "Pending ExGratia Charges for Invoicing"
                        StrSortCol = "[StrNo] Desc"
                        NoViewAllowed = False
                    ElseIf rad2.Checked Then
                        SPParam(3) = Mainclass.CreateSqlParameter("@OPTION", 5, SqlDbType.Int)
                        EditPagePath = "../Transport/ExtraHiring/tptPostingExGratia.aspx"
                        HeaderTitle = "ExGratia Charges Invoice"
                        StrSortCol = "[ID] Desc"
                        NoViewAllowed = False
                    End If


                Case "T200219" 'NES Exgratia Posting
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Invoice"
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'If rad1.Checked Then
                    '    StrSQL = "SELECT [id],[StrNo],[NES Amount],[VILLA Amount],[IIT Amount],[OutStation Amount][Out Station],[Over Night Stay Amount][Over Night Stay],[NES Amount]+[VILLA Amount]+[IIT Amount][NET Amount],Posted FROM (  "
                    '    StrSQL &= "SELECT LEFT(DATENAME(M,A.EGN_TRDATE),3)+DATENAME(YEAR,A.EGN_TRDATE )+BSU_SHORTNAME id,"
                    '    StrSQL &= "LEFT(DATENAME(M,A.EGN_TRDATE),3)+DATENAME(YEAR,A.EGN_TRDATE )+BSU_SHORTNAME [NO], '' TRNO,"
                    '    StrSQL &= "getdate() Date, LEFT(DATENAME(M,A.EGN_TRDATE),3)+DATENAME(YEAR,A.EGN_TRDATE )+BSU_SHORTNAME [StrNo],"
                    '    StrSQL &= "SUM(NetAmount ) [NET AMOUNT],SUM(case when cast(EGN_OVERNIGHTSTAY as integer)=1 then case when EGN_DRIVERTYPE='1' then 100 else 50 end  else 0 end ) [Over Night Stay Amount],sum(EGN_OUTSTATION*25)  [OutStation Amount],SUM(case when EGN_DRIVERTYPE  ='1' then NetAmount else 0 end) [NES Amount],"
                    '    StrSQL &= "SUM(case when EGN_DRIVERTYPE ='2' then NetAmount else 0 end) [VILLA Amount],SUM(case when EGN_DRIVERTYPE ='3' then NetAmount else 0 end) [IIT Amount],"
                    '    StrSQL &= "(select isnull(SUM(ENI_NET),0) from TPTEXGRATIANES_INV where ENI_DELETE=0 and ENI_STRNO =LEFT(DATENAME(M,EGN_TRDATE),3)+DATENAME(YEAR,EGN_TRDATE )+BSU_SHORTNAME  )[POSTED],''[Document Number] "
                    '    StrSQL &= "FROM  (select EGN_BSU_ID,EGN_DRIVERTYPE, CASE WHEN egn_drivertype = 1 THEN CASE WHEN Max(Cast(egn_holiday AS INTEGER)) = 1  THEN Sum(Cast(egn_ttotal AS FLOAT)) * 12 ELSE CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 12 THEN (Sum(Cast(egn_ttotal AS FLOAT)) - 12 ) * 12 ELSE 0 END END ELSE CASE WHEN egn_drivertype = 2 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 72 ELSE 0  END  ELSE CASE WHEN egn_drivertype = 3 THEN CASE WHEN Sum(Cast(egn_ttotal AS FLOAT)) >= 1 THEN 150 ELSE 0 END END END END [NetAmount]  "
                    '    StrSQL &= ",EGN_EMP_ID,EGN_TRDATE,sum(cast(EGN_OVERNIGHTSTAY as integer))EGN_OVERNIGHTSTAY,sum(cast(EGN_OUTSTATION as integer))EGN_OUTSTATION from TPTEXGRATIACHARGESNES WHERE EGn_EGI_ID = 0 and egn_emp_id<>0 and EGN_DELETED=0 and egn_bsu_id='" & Session("sBsuid") & "' and EGN_FYEAR='" & Session("F_YEAR") & "'  group by EGN_EMP_ID,EGN_TRDATE,EGN_BSU_ID,EGN_DRIVERTYPE)a "
                    '    StrSQL &= "INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=a.EGN_BSU_ID  Group BY LEFT(DATENAME(M,A.EGN_TRDATE),3)+DATENAME(YEAR,A.EGN_TRDATE  )+BSU_SHORTNAME) A WHERE 1=1  "
                    '    EditPagePath = "../Transport/ExtraHiring/tptPostingExGratiaNES.aspx"
                    '    HeaderTitle = "Pending NES ExGratia Charges for Invoicing"
                    '    StrSortCol = "[StrNo] Desc"
                    '    NoViewAllowed = False
                    'ElseIf rad2.Checked Then
                    '    StrSQL = "SELECT [id],[Trno],StrNo,[Date],[Net Amount],[Document Number] FROM (select ENI_ID [id],ENI_TRNO[Trno],ENI_DATE[Date],ENI_STRNO[StrNo], 0 posted,  ENI_NET[Net Amount],"
                    '    StrSQL &= "case when ENI_POSTED=1 then 'Posted' else 'Not Posted' end [Status],ENI_POSTING_DATE [Posting Date],  ENI_USER_NAME [user],0[NES Amount],0[IIT Amount],0 [VILLA Amount],0 [Out Station],0 [Over Night Stay],ENI_JHD_DOCNO [Document Number]"
                    '    StrSQL &= "from TPTEXGRATIANES_INV WHERE ENI_BSU_ID='" & Session("sBsuid") & "' and ENI_FYEAR='" & Session("F_YEAR") & "') A WHERE 1=1  "
                    '    EditPagePath = "../Transport/ExtraHiring/tptPostingExGratiaNES.aspx"
                    '    HeaderTitle = "NES ExGratia Charges Invoice"
                    '    StrSortCol = "[ID] Desc"
                    '    NoViewAllowed = False
                    'End If
                    'NoAddingAllowed = True

                    IsStoredProcedure = True
                    StrSQL = "LoadingExgratiaEntriesNES"
                    ReDim SPParam(8)
                    NoAddingAllowed = True
                    SPParam(0) = Mainclass.CreateSqlParameter("@USERNAME", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@LBSU", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@SBSU", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@DATE", "01-01-1900", SqlDbType.Date)
                    SPParam(4) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        SPParam(5) = Mainclass.CreateSqlParameter("@Option", 4, SqlDbType.Int)
                        HeaderTitle = "Pending NES ExGratia Charges for Invoicing"
                        StrSortCol = "[StrNo] Desc"
                    Else
                        SPParam(5) = Mainclass.CreateSqlParameter("@Option", 5, SqlDbType.Int)
                        HeaderTitle = "NES ExGratia Charges Invoice"
                        StrSortCol = "[ID] Desc"
                    End If
                    SPParam(6) = Mainclass.CreateSqlParameter("@EGN_ID", 0, SqlDbType.Int)
                    SPParam(7) = Mainclass.CreateSqlParameter("@FDATE", "01-01-1900", SqlDbType.Date)
                    SPParam(8) = Mainclass.CreateSqlParameter("@TDATE", "01-01-1900", SqlDbType.Date)
                    EditPagePath = "../Transport/ExtraHiring/tptPostingExGratiaNES.aspx"
                    NoViewAllowed = False
                    NoAddingAllowed = True
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString


                Case "T200118"
                    StrSQL = "select Id,[Customer], [Account Code],[Email],[Address] from (SELECT [TLC_ID]ID,TLC_DESCR [Customer],TLC_EMAIL[Email],   "
                    StrSQL &= "TLC_ACT_ID [Account Code],[TLC_ADDRESS] ADDRESS FROM TPTLEASECUSTOMER  ) A where 1=1  "
                    EditPagePath = "../Transport/ExtraHiring/tptLeaseCustomer.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[ID] DESC "
                    HeaderTitle = "Customer Details"



                Case "T200124" 'VEHICLE MAINTENANCE DETAILS
                    StrSQL = "select ID,[JOB TYPE], [VEHICLE REG NO],[FROM DATE],[TO DATE] from (SELECT [TVM_ID]ID,Case when TVM_JOBTYPE=0 then 'NES' else case when TVM_JOBTYPE=1 then 'SCHOOL' else 'SUB CON' end end [Job Type], "
                    StrSQL &= "CASE WHEN TVM_JOBTYPE IN(0,2) THEN TPTVEHICLE_M.VEH_REGNO ELSE VEHICLE_M.VEH_REGNO END [VEHICLE REG NO],[TVM_MAINTENANCE_FDATE][FROM DATE],[TVM_MAINTENANCE_TDATE][TO DATE] ,[TVM_USER],[TVM_FYEAR], "
                    StrSQL &= " [TVM_DELETED] FROM [TPTVEHICLEMAINTENANCE] LEFT OUTER JOIN TPTVEHICLE_M ON TPTVEHICLE_M.VEH_ID=TVM_VEH_ID LEFT OUTER JOIN VEHICLE_M ON VEHICLE_M.VEH_ID=TVM_VEH_ID WHERE TVM_DELETED=0) A where 1=1 "
                    EditPagePath = "../Transport/ExtraHiring/tptVehicleMaintenance.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[VEHICLE REG NO]"
                    HeaderTitle = "Vehicle Maintenance Details"

                Case "T200141" ' Approve Jobs
                    StrSQL = "SELECT TRN_No, [Job No], [Job Date],[Entry Date], Client, Amount,isnull([Discounted Amount],0)[Discounted Amount], [Vehicle No], [Driver],[Ref No] , [Pickup Point] from ("
                    StrSQL &= "select TRN_No,isnull([TRN_BATCH],0) [Batch No], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date],isnull(ACT_NAME,BSU_NAME) Client,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount,(SELECT sum(INV_AMT) [Discounted Amount] from tpthiringinvoice where trn_no=inv_trn_no and INV_Amt<0 ) [Discounted Amount], VEH_REGNO [Vehicle No], TPTD_NAME [Driver], "
                    StrSQL &= " LPO_no as [Ref No], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
                    StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' Status "
                    StrSQL &= "FROM TPTHiring "
                    StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                    StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M ON VEH_ID=TPT_Veh_id "
                    StrSQL &= "LEFT OUTER JOIN TPTDRIVER_M ON TPTD_ID=TPT_Drv_id "
                    StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                    StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='A' and isnull(TPT_BllId,0)=0 and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "
                    HeaderTitle = "Job Order Approving"
                    ButtonText1 = "Approve"
                    ShowGridCheckBox = True
                    NoAddingAllowed = True
                    EditPagePath = "../Transport/ExtraHiring/tptHiringBilling.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString


                Case "T200142" 'Extra Trip Invoicing
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Invoice"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    If rad1.Checked Then
                        StrSQL = "SELECT [id],[StrNo],[Gross],[Salik],[Net], Posted FROM ( "
                        StrSQL &= "SELECT LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME id, LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME [NO], '' TRNO, getdate() Date, "
                        StrSQL &= "LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME [StrNo], SUM(TEX_GROSS) [GROSS], SUM(TEX_SALIK)*4 [SALIK], SUM(TEX_NET) [NET], "
                        StrSQL &= "(select isnull(SUM(TEI_NET),0) from TPTEXTRA_INV where TEI_DELETE=0 and TEI_STRNO =LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME  )[POSTED] "
                        StrSQL &= "FROM  TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID "
                        StrSQL &= "WHERE TEX_TEI_ID = 0 AND TEX_BSU_ID IN ("
                        StrSQL &= "SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID FROM vw_oso_SERVICES_BSU_M INNER JOIN "
                        StrSQL &= "BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN "
                        StrSQL &= "OASIS..MENURIGHTS_S  MENURIGHTS_S ON MENURIGHTS_S.MNR_BSU_ID = vw_oso_SERVICES_BSU_M.SVB_BSU_ID "
                        StrSQL &= "AND MENURIGHTS_S.MNR_MNU_ID in ('T200145', 'F300155','F300165','F715065') INNER JOIN "
                        StrSQL &= "OASIS..USERS_M USERS_M ON MENURIGHTS_S.MNR_ROL_ID = USERS_M.USR_ROL_ID INNER JOIN "
                        StrSQL &= "OASIS..USERACCESS_S USERACCESS_S ON USERS_M.USR_ID = USERACCESS_S.USA_USR_ID AND "
                        StrSQL &= "vw_oso_SERVICES_BSU_M.SVB_BSU_ID = USERACCESS_S.USA_BSU_ID "
                        StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='" & Session("sBsuid") & "') AND "
                        StrSQL &= "(vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1) AND (USERS_M.USR_NAME ='" & Session("sUsr_name") & "') AND isnull(SVB_bAVAILABLE,0)=1 and isnull(SVB_BActive,0)=1 union select '888881' "
                        StrSQL &= ") and tex_date<=(select FYR_TODT from OASIS..FINANCIALYEAR_S WHERE FYR_ID='" & Session("F_YEAR") & "') Group BY LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME) A WHERE 1=1 "

                        'StrSQL = "SELECT [id],[StrNo],[Gross],[Salik],[Net Amount], Posted FROM ( "
                        'StrSQL &= "SELECT case when TEX_DATE<='2018-03-15' then LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME else LEFT(case when day(TEX_DATE)<=15 then dateadd(m,-1,TEX_DATE) else dateadd(m,1,TEX_DATE) end,3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME END id, case when TEX_DATE<='2018-03-15' then LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME else LEFT(case when day(TEX_DATE)<=15 then dateadd(m,-1,TEX_DATE) else dateadd(m,1,TEX_DATE) end,3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME END [NO], '' TRNO, getdate() Date, "
                        'StrSQL &= "case when TEX_DATE<='2018-03-15' then LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME else LEFT(case when day(TEX_DATE)<=15 then dateadd(m,-1,TEX_DATE) else dateadd(m,1,TEX_DATE) end,3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME END [StrNo], SUM(TEX_GROSS) [GROSS], SUM(TEX_SALIK)*4 [SALIK], SUM(TEX_NET) [NET AMOUNT], "
                        'StrSQL &= "(select isnull(SUM(TEI_NET),0) from TPTEXTRA_INV where TEI_DELETE=0 and TEI_STRNO =case when TEX_DATE<='2018-03-15' then LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME else LEFT(case when day(TEX_DATE)<=15 then dateadd(m,-1,TEX_DATE) else dateadd(m,1,TEX_DATE) end,3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME END  )[POSTED] "
                        'StrSQL &= "FROM  TPTEXTRA INNER JOIN OASIS..BUSINESSUNIT_M ON BSU_ID=TEX_BSU_ID "
                        'StrSQL &= "WHERE TEX_TEI_ID = 0 AND TEX_BSU_ID IN ("
                        'StrSQL &= "SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID FROM vw_oso_SERVICES_BSU_M INNER JOIN "
                        'StrSQL &= "BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN "
                        'StrSQL &= "OASIS..MENURIGHTS_S  MENURIGHTS_S ON MENURIGHTS_S.MNR_BSU_ID = vw_oso_SERVICES_BSU_M.SVB_BSU_ID "
                        'StrSQL &= "AND MENURIGHTS_S.MNR_MNU_ID in ('T200145', 'F300155','F300165','F715065') INNER JOIN "
                        'StrSQL &= "OASIS..USERS_M USERS_M ON MENURIGHTS_S.MNR_ROL_ID = USERS_M.USR_ROL_ID INNER JOIN "
                        'StrSQL &= "OASIS..USERACCESS_S USERACCESS_S ON USERS_M.USR_ID = USERACCESS_S.USA_USR_ID AND "
                        'StrSQL &= "vw_oso_SERVICES_BSU_M.SVB_BSU_ID = USERACCESS_S.USA_BSU_ID "
                        'StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='" & Session("sBsuid") & "') AND "
                        'StrSQL &= "(vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1) AND (USERS_M.USR_NAME ='" & Session("sUsr_name") & "') AND isnull(SVB_bAVAILABLE,0)=1 and isnull(SVB_BActive,0)=1 union select '888881' "
                        'StrSQL &= ") and tex_date<=(select FYR_TODT from OASIS..FINANCIALYEAR_S WHERE FYR_ID='" & Session("F_YEAR") & "') Group BY case when TEX_DATE<='2018-03-15' then LEFT(DATENAME(M,TEX_DATE),3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME else LEFT(case when day(TEX_DATE)<=15 then dateadd(m,-1,TEX_DATE) else dateadd(m,1,TEX_DATE) end,3)+DATENAME(YEAR,TEX_DATE )+BSU_SHORTNAME END) A WHERE 1=1 "

                        EditPagePath = "../Transport/ExtraHiring/tptPostingExtraTrip.aspx"
                        HeaderTitle = "Pending Extra Trips for Invoicing"
                        StrSortCol = "[StrNo] Desc"
                        NoViewAllowed = False
                    ElseIf rad2.Checked Then
                        StrSQL = "SELECT [id],[Trno],StrNo,[Date],[Gross],[Salik],[Net] FROM (select tei_id [id],tei_trno[Trno],tei_date[Date],tei_strno[StrNo], 0 posted, "
                        StrSQL &= "tei_gross[Gross],tei_salik[Salik],tei_net[Net],case when tei_posted=1 then 'Posted' else 'Not Posted' end [Status],tei_posting_date [Posting Date], "
                        StrSQL &= "tei_user_name [user] from TPTEXTRA_INV WHERE TEI_BSU_ID='" & Session("sBsuid") & "' and tei_fyear='" & Session("F_YEAR") & "') A WHERE 1=1 "

                        EditPagePath = "../Transport/ExtraHiring/tptPostingExtraTrip.aspx"
                        HeaderTitle = "Extra Trip Invoice"
                        StrSortCol = "[ID] Desc"
                        NoViewAllowed = False
                    End If
                    NoAddingAllowed = True


                Case "T200200" 'Proforma Invoice
                    CheckBox1Text = "Job Orders"
                    CheckBox2Text = "Proforma Invoices"
                    If rad1.Checked Then
                        StrSQL = "SELECT TRN_No, [Job No], [Job Date],[Client], isnull(Amount,0)Amount,[TAX Amount],isnull([Net Amount],0)[Net Amount], isnull([Vehicle No],'')[Vehicle No], [Driver],[Group],[Pickup Point]  from ("
                        StrSQL &= "select TRN_No,STS_No [Group],isnull([TRN_BATCH],0) [Batch No], TRN_JobnumberStr [Job No], replace(convert(varchar(30), TPT_JobDate, 106),' ','/') [Job Date],replace(convert(varchar(30), TPT_Date, 106),' ','/') [Entry Date],isnull(ACT_NAME,BSU_NAME) Client,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no) Amount,isnull(TPT_TAX_AMOUNT,0) [TAX Amount],isnull(TPT_TAX_NET_AMOUNT,(SELECT sum(INV_AMT) Amount from tpthiringinvoice where trn_no=inv_trn_no)) [Net Amount], VEH_REGNO [Vehicle No], TPTD_NAME [Driver], "
                        StrSQL &= " LPO_no as [Ref No], TPT_PickupPoint [Pickup Point], TPT_DropOffPoint [DropOff Point], "
                        StrSQL &= " '' Batch,'' [Batch date],'' [Total Amount],'' Status,'' [Debit Note No],''[Proforma No], '' ProformaNo "
                        StrSQL &= "FROM TPTHiring "
                        StrSQL &= "LEFT OUTER JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTVEHICLE_M ON VEH_ID=TPT_Veh_id "
                        StrSQL &= "LEFT OUTER JOIN TPTDRIVER_M ON TPTD_ID=TPT_Drv_id "
                        StrSQL &= "LEFT OUTER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN OASIS.dbo.BUSINESSUNIT_M on BSU_ID=TPT_Client_id "
                        StrSQL &= "LEFT OUTER JOIN TPT_SERVICE_M on TSM_ID=TPT_SERVICE_ID "
                        StrSQL &= "LEFT OUTER JOIN TPTHIRING_I on THI_DOCNO=TRN_JobnumberStr "
                        StrSQL &= "where TPT_Fyear=" & Session("F_YEAR") & " and TPT_Deleted=0 and TRN_STATUS='' and isnull(TPT_BllId,0)=0 and ISNULL(TRN_numberstr,'')='' and TPT_Bsu_id like '%" & Session("sBsuid") & "%') A where 1=1 "
                        HeaderTitle = "Job Order Proforma"
                        ButtonText1 = "Generate Proforma"
                        ShowGridCheckBox = False
                        NoAddingAllowed = True
                        NoViewAllowed = False
                        EditPagePath = "../Transport/ExtraHiring\tptProforma.aspx"
                        strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                        StrSortCol = "TRN_NO Desc"
                        ListButton1.Visible = False
                    Else
                        StrSQL = "SELECT ProformaNo,[Job No],[Job Date],[Client],[Amount],[TAX Amount],[Net Amount],[Proforma No] FROM ("
                        StrSQL &= "select max(TRN_No)TRN_No,TRN_JobnumberStr [Job No],replace(convert(varchar(30), max(TPT_JobDate), 106),' ','/')[Job Date],isnull(max(ACT_NAME),'') Client,   "
                        StrSQL &= " isnull((SELECT sum(INV_AMT) Amount from tpthiringinvoice where max(trn_no)=inv_trn_no group by INV_TRN_NO),0) Amount,isnull(max(TPT_TAX_AMOUNT),0) [TAX Amount],isnull(max(TPT_TAX_NET_AMOUNT),0)[Net Amount], "
                        StrSQL &= " max(trn_numberstr) ProformaNo,max(trn_numberstr) [Proforma No],''[Entry Date],''[Vehicle No],''Driver,''[Group],''[Pickup Point]   "
                        StrSQL &= " from tpthiring LEFT OUTER JOIN   oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id LEFT OUTER JOIN TPTHiringInvoice ON TRN_no =INV_TRN_NO where TPT_Fyear=" & Session("F_YEAR") & " and isnull(TRN_numberstr,'')<>'' AND TPT_BSU_ID='" & Session("sBsuid") & "'"
                        StrSQL &= " GROUP BY TRN_JobnumberStr ) A WHERE 1=1"
                        HideExportButton = True
                        strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                        NoAddingAllowed = True
                        NoViewAllowed = True
                        ShowGridCheckBox = True
                        ButtonText1 = "Print"
                        StrSortCol = "[Proforma No] DESC"

                    End If
                    NoAddingAllowed = True

                Case "T200500" ' Permit expiry Report
                    lblDate.Text = "Expiry As on Date:"
                    dateCol.Visible = True
                    If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                    StrSQL = "SELECT [ID],[Permit Id],[Permit Type],[Veh.Reg No],[Issue Date], [Expiry Date] , [Permit No] from(SELECT VEP_ID ID,VEP_VPT_ID [Permit Id],VEH_REGNO [Veh.Reg No],VPT_Descr [Permit Type],"
                    StrSQL &= "replace(convert(varchar(30),VEP_ISSDATE , 106),' ','/') [Issue Date], replace(convert(varchar(30), VEP_EXPDATE , 106),' ','/') [Expiry Date], VEP_PERMIT_NO  [Permit No]  "
                    StrSQL &= " FROM VEHICLE_PERMIT left outer join VEHICLE_PERMIT_M on VPT_ID =VEP_VPT_ID left outer join VEHICLE_M on VEH_ID  =VEP_VEH_ID   where VEP_RENEW='YES' AND DATEADD(DAY,-30,VEP_EXPDATE)<='" & txtDate.Text & "'  ) A where 1=1 "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[Permit Type]"
                    HeaderTitle = "Vehicle Permit Expiry Report"
                    NoAddingAllowed = True
                    NoViewAllowed = True



                Case "U000036", "P130019"
                    StrSQL = " SELECT EJD_ID ,[EMPLOYEE NAME],[JOB TYPE], [FROM DATE],[TO DATE],[DELEGATE TO],[STATUS],[DISABLED DATE],[NARRATION] "
                    StrSQL &= "  FROM ( SELECT DISTINCT EJD_ID ,EMP.EMP_FNAME+' ' + EMP.EMP_LNAME [EMPLOYEE NAME],EJM_DESCR [JOB TYPE], EJD_DTFROM [FROM DATE],EJD_DTTO [TO DATE],"
                    StrSQL &= "  DEL.EMP_FNAME+' ' + DEL.EMP_LNAME [DELEGATE TO],CASE WHEN isnull(EJD_bDisable,0)=1 or isnull(EJD_bForward ,0)=0 or isnull(EJD_DTTO,getdate())<getdate() THEN 'Not Active' ELSE 'Active' END [STATUS],EJD_DisableDate [DISABLED DATE], EJD_REMARKS [NARRATION]   "
                    StrSQL &= "  FROM EMPJOBDELEGATE_S LEFT OUTER JOIN EMPLOYEE_M EMP ON EJD_EMP_ID  =EMP.EMP_ID  "
                    StrSQL &= "  LEFT OUTER JOIN EMPLOYEE_M DEL ON EJD_DEL_EMP_ID  =DEL.EMP_ID  "
                    StrSQL &= "  LEFT OUTER JOIN EMPJOB_M  ON EJD_EJM_ID  =EJM_ID "
                    StrSQL &= "  WHERE EJD_BSU_ID='" & Session("sBsuid") & "'"
                    If MainMenuCode = "U000036" Then
                        Dim EMPID As String
                        EMPID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                        If EMPID = "" Then
                            Exit Sub
                        End If
                        StrSQL &= " AND EJD_EMP_ID = '" & EMPID & "'"
                    Else
                        'StrSQL &= " AND EJD_ENTEREDBY = '" & Session("sUsr_name") & "'"
                    End If
                    StrSQL &= " ) A WHERE 1=1 "

                    HideExportButton = True
                    EditPagePath = "../Payroll/empAssignDelegate.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "EJD_ID"
                    HeaderTitle = "Job Delegation"
                Case "P153067"
                    CheckBox1Text = "Not Generated"
                    CheckBox2Text = "Generated"
                    CheckBox3Text = "All"
                    CheckBox4Text = "Generated today"
                    StrSQL = "SELECT EMP_ID,[EMPLOYEE NO],[EMPLOYEE NAME], [CATEGORY] ,[DEPARTMENT],[DESIGNATION] FROM ("
                    StrSQL &= " SELECT EMP_ID,EMPNO [EMPLOYEE NO],EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME [EMPLOYEE NAME], ECT_DESCR [CATEGORY] ,"
                    StrSQL &= " DPT_DESCR [DEPARTMENT],DES_DESCR [DESIGNATION],EMP_PASSWORD,EMP_PWD_GENERATE_LOGDT  "
                    StrSQL &= " FROM dbo.EMPLOYEE_M INNER JOIN dbo.EMPCATEGORY_M ON EMP_ECT_ID = ECT_ID "
                    StrSQL &= " INNER JOIN dbo.DEPARTMENT_M ON EMP_DPT_ID =DPT_ID "
                    StrSQL &= " INNER JOIN dbo.EMPDESIGNATION_M  ON EMP_DES_ID =DES_ID WHERE EMP_STATUS NOT IN (4,5) AND EMP_BSU_ID ='" & Session("sBsuid") & "') A"
                    StrSQL &= " WHERE 1 = 1 "
                    chkSelectAll.Visible = True
                    If rad1.Checked Then
                        StrSQL &= " AND ISNULL(EMP_PASSWORD,'')=''"
                        ButtonText2 = ""
                    ElseIf rad2.Checked Then
                        StrSQL &= " AND ISNULL(EMP_PASSWORD,'')<>'' "
                        ButtonText2 = "Print"
                    ElseIf rad3.Checked Then
                        ButtonText2 = ""
                    ElseIf rad4.Checked Then
                        StrSQL &= " AND ISNULL(EMP_PASSWORD,'')<>'' and DAY(EMP_PWD_GENERATE_LOGDT)=DAY(GETDATE()) AND  MONTH(EMP_PWD_GENERATE_LOGDT)=MONTH(GETDATE()) AND  YEAR(EMP_PWD_GENERATE_LOGDT)=YEAR(GETDATE())   "
                        ButtonText2 = "Print"
                    End If
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    ShowGridCheckBox = True
                    strConn = ConnectionManger.GetOASISConnectionString
                    ButtonText1 = "Generate Password"
                    StrSortCol = "EMP_ID"
                    HeaderTitle = "Generate Password For Employee"
                Case "P153068"
                    IsStoredProcedure = True
                    StrSQL = "GetBSUSalaryDetailList"
                    ReDim SPParam(0)
                    NoAddingAllowed = True
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    EditPagePath = "../Payroll/empExportSalaryDetail.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    HeaderTitle = "Export Employee Salary Data"
                    StrSortCol = "ID"
                Case "U000086" 'Business Travel and Advance 
                    Dim EMPID As String
                    EMPID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    StrSQL = " SELECT BTA_no, [Lpo Number], [BTA Number], [BTA Status], [Employee], [Designation], [BTA Date], [Total A], [Total B], [Total C], Total from "
                    StrSQL &= " (SELECT BTA_no, BTA_NumberStr [bta number], LPO_no [lpo number], case when BTA_Status='O' then 'Open' else case when BTA_Status='A' then 'Accepted' else 'Rejected' end end [bta status], EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME Employee, DES_DESCR Designation, " ', DPT_DESCR Department,
                    StrSQL &= "replace(convert(varchar(30),BTA_DATE,106),' ','/') [BTA Date], TotalA [Total A], Visa_cost [Total B], TotalC [Total C], TotalA+Visa_cost+TotalC Total "
                    StrSQL &= "FROM oasis.dbo.EMPLOYEE_M E inner JOIN oasis.dbo.EMPDESIGNATION_M on EMP_DES_ID=DES_ID and emp_id='" & EMPID & "' "
                    StrSQL &= "INNER JOIN oasis.dbo.DEPARTMENT_M on EMP_DPT_ID=DPT_ID INNER JOIN BTA_Details on E.Emp_id=BTA_Details.Emp_id) A "
                    StrSQL &= "WHERE 1 = 1 "
                    EditPagePath = "../Inventory/BTA.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[BTA Number]"
                    HeaderTitle = "Business Travel and Advance"

                Case "U000088" 'Business Travel and Advance Approval
                    StrSQL = " SELECT BTA_no, [Lpo Number], [BTA Number], [BTA Status], [Employee], [Designation], [BTA Date], [Total A], [Total B], [Total C], Total from "
                    StrSQL &= " (SELECT BTA_no, BTA_NumberStr [bta number], LPO_no [lpo number], case when BTA_Status='O' then 'Open' else case when BTA_Status='A' then 'Accepted' else 'Rejected' end end  [bta status], EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME Employee, DES_DESCR Designation, " ', DPT_DESCR Department,
                    StrSQL &= "replace(convert(varchar(30),BTA_DATE,106),' ','/') [BTA Date], TotalA [Total A], Visa_cost [Total B], TotalC [Total C], TotalA+Visa_cost+TotalC Total "
                    StrSQL &= "FROM oasis.dbo.EMPLOYEE_M E inner JOIN oasis.dbo.EMPDESIGNATION_M on EMP_DES_ID=DES_ID "
                    StrSQL &= "INNER JOIN oasis.dbo.DEPARTMENT_M on EMP_DPT_ID=DPT_ID INNER JOIN BTA_Details on E.Emp_id=BTA_Details.Emp_id) A "
                    StrSQL &= "WHERE 1 = 1 "
                    EditPagePath = "../Inventory/BTA.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[BTA Number]"
                    HeaderTitle = "Business Travel and Advance Approval"
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Accepted"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"
                    NoAddingAllowed = True
                    If rad1.Checked Then
                        StrSQL &= " and [BTA Status]='Open'"
                    ElseIf rad2.Checked Then
                        StrSQL &= " and [BTA Status]='Accepted'"
                    ElseIf rad3.Checked Then
                        StrSQL &= " and [BTA Status]='Rejected'"
                    End If
                Case "S200456" 'Report Lost card
                    IsStoredProcedure = True
                    StrSQL = "GET_LOST_IDCARD_REQUEST_LISTS"
                    ReDim SPParam(0)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    EditPagePath = "../Students/StudentLostCard.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    HeaderTitle = "Student Lost Card"
                    StrSortCol = "SLC_ID DESC"
                Case "A200385"
                    Dim thisMonth As New DateTime(DateTime.Today.Year, DateTime.Today.Month, 1)

                    lblDate.Text = "Department Cost as on:"
                    dateCol.Visible = True
                    If txtDate.Text = "" Then txtDate.Text = thisMonth.AddDays(-1).ToString("dd/MMM/yyyy")

                    IsStoredProcedure = True
                    StrSQL = "rptDepartmentCost"
                    ReDim SPParam(1)
                    NoAddingAllowed = True
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@ASONDATE", txtDate.Text, SqlDbType.VarChar)

                    strConn = ConnectionManger.GetOASISFINConnectionString
                    HeaderTitle = "Department Cost List"
                    StrSortCol = "ID"
                    NoViewAllowed = True
                    NoAddingAllowed = True

                Case "FD00011"
                    StrSQL = "SELECT Iss_Id, [Document Issuing Authority], [City], [Phone#], [Fax#] from ("
                    StrSQL &= "SELECT Iss_Id, iss_Descr [Document Issuing Authority], Cit_Name [City], iss_Phone [Phone#],iss_Fax [Fax#] "
                    StrSQL &= "from Docissuer inner join DocCity on Iss_CityId=Cit_id) a Where 1=1 "
                    EditPagePath = "../FileMgmt/DocIssuer.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Document Issuing Authority]"
                    HeaderTitle = "Document Issuing Authority"
                Case "FD00012"
                    'StrSQL = "SELECT Dot_Id, [Document Type], [Document Issuing Authority], [Lead Days], [Enable Expiry] from "
                    'StrSQL &= "(SELECT dot_id, dot_Descr [Document Type], (select Iss_Descr from DocIssuer where iss_id=dot_iss_id) [Document Issuing Authority], "
                    'StrSQL &= "DOT_LEADDAYS [LEAD DAYS], CASE when dot_expiry=0 THEN 'No' ELSE 'Yes' END [Enable Expiry] from doctype) a Where 1=1 "
                    'EditPagePath = "../FileMgmt/DocType.aspx"
                    'strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    'StrSortCol = "[Document Type]"
                    'HeaderTitle = "Document Type Master"
                    IsStoredProcedure = True
                    StrSQL = "GetDocumentTypeMasterList"
                    ReDim SPParam(1)
                    NoAddingAllowed = False
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    EditPagePath = "../FileMgmt/DocType.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Document Type]"
                    HeaderTitle = "Document Type Master"


                Case "FD00013"
                    IsStoredProcedure = True
                    StrSQL = "GetContactistBSUWise"
                    ReDim SPParam(1)
                    NoAddingAllowed = False
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    EditPagePath = "../FileMgmt/ContactListBSUWise.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[SCHOOL NAME]"
                    HeaderTitle = "Email Notification Setup"

                Case "FD00020"
                    StrSQL = "SELECT DGR_Id, [Document Group], [Short Name],Details from ("
                    StrSQL &= "SELECT DGR_Id, DGR_Descr [Document Group], Cit_Name [Country],DGR_ShortName [Short Name],DGR_Details Details  "
                    StrSQL &= "from DocGroup inner join DocCity on DGR_CityId=Cit_id) a Where 1=1 "
                    EditPagePath = "../FileMgmt/DocumentGroup.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Document Group]"
                    HeaderTitle = "Document Group Master"





                Case "FD00032"
                    'CheckBox1Text = "All"
                    'CheckBox2Text = "Active"
                    'CheckBox3Text = "Expired"
                    'StrSQL = "SELECT [Document Id], [Document Type], [Document No], [Expiry Date], [Document Issuer], Details from ( "
                    'StrSQL &= "SELECT Doc_id [Document Id], Doc_Bsu_id, Dot_Descr [Document Type], Iss_Descr [Document Issuer], "
                    'StrSQL &= "Doi_No [Document No], replace(convert(varchar(30), Doi_IssDate, 106),' ','/') [Issue Date], replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') [Expiry Date], "
                    'StrSQL &= "Doc_Narration Details FROM DocUpload INNER join DocType on Dot_Id=Doc_Dot_id INNER JOIN DocIssuer on Dot_Iss_Id=Iss_id "
                    'StrSQL &= "LEFT JOIN DocImage on Doi_Doc_id=Doc_id "
                    'StrSQL &= "Where (doc_olu_name=doc_olu_name" & IIf(Session("sBSUID") = "999998", " or 1=1 ", "") & ") "
                    'StrSQL &= "AND (Doi_id is null or Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc)) "
                    'If rad1.Checked Then
                    'ElseIf rad2.Checked Then
                    'StrSQL &= " and (Doi_ExpDate-Doc_LeadDays)>=getdate()"
                    'ElseIf rad3.Checked Then
                    'StrSQL &= " and (Doi_ExpDate-Doc_LeadDays)<getdate()"
                    'End If
                    'StrSQL &= ") A where (Doc_Bsu_id='" & Session("sBsuid") & "' " & IIf(Session("sBSUID") = "999998", " or 1=1 ", "") & ") "
                    'EditPagePath = "../FileMgmt/docUpload.aspx"
                    'strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    'StrSortCol = "[Document Type]"
                    'HeaderTitle = "Upload Document Details"

                    CheckBox1Text = "All"
                    CheckBox2Text = "Active"
                    CheckBox3Text = "Expired"
                    StrSQL = "SELECT [Document Id], [Document Type], [Document No], [Expiry Date], [Document Issuer], Details from ( "
                    StrSQL &= "SELECT Doc_id [Document Id], Doc_Bsu_id, Dot_Descr [Document Type], Iss_Descr [Document Issuer], "
                    StrSQL &= "Doi_No [Document No], replace(convert(varchar(30), Doi_IssDate, 106),' ','/') [Issue Date], replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') [Expiry Date], "
                    StrSQL &= "Doc_Narration Details FROM DocUpload INNER join DocType on Dot_Id=Doc_Dot_id INNER JOIN DocIssuer on Dot_Iss_Id=Iss_id "
                    StrSQL &= "LEFT JOIN DocImage on Doi_Doc_id=Doc_id "
                    StrSQL &= "Where (doc_olu_name=doc_olu_name" & IIf(Session("sBSUID") = "999998", " or 1=1 ", "") & ") "
                    StrSQL &= "AND (Doi_id is null or Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc)) "
                    If rad1.Checked Then
                    ElseIf rad2.Checked Then
                        StrSQL &= " and (Doi_ExpDate-Doc_LeadDays)>=getdate()"
                    ElseIf rad3.Checked Then
                        StrSQL &= " and (Doi_ExpDate-Doc_LeadDays)<getdate()"
                    End If
                    StrSQL &= " and doc_dot_id in(select dot_id from doctype where ('||'+Dot_Bsu_apply LIKE '%||" & Session("sBSUID") & "||%'  or  '||'+Dot_Bsu_share LIKE '%||" & Session("sBSUID") & "||%')  and  '||'+ Dot_Rol_share LIKE '%||'+(select top 1 cast(usr_rol_id as varchar(5)) from oasis..users_m  where usr_name='" & Session("sUsr_name") & "')+'||%' )"
                    StrSQL &= " ) A where (Doc_Bsu_id='" & Session("sBsuid") & "' " & IIf(Session("sBSUID") = "999998", " or 1=1 ", "") & ") "
                    EditPagePath = "../FileMgmt/docUpload.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Document Type]"
                    HeaderTitle = "Upload Document Details"

                Case "FD00072"
                    'lblDate.Text = "Expiry As on Date:"
                    'dateCol.Visible = True
                    'If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                    'StrSQL = "SELECT [Document Id], " & IIf(Session("sBSUID") = "999998", " [BSU], ", "") & "[Document Type], [Document Issuer], [Expiry Date], Details from "
                    'StrSQL &= "(SELECT bsu_shortname [BSU], Doc_id [Document Id], Doc_Bsu_id, Dot_Descr [Document Type], Iss_Descr [Document Issuer], "
                    'StrSQL &= " replace(convert(varchar(30), Doi_IssDate, 106),' ','/') [Issue Date], replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') [Expiry Date], Doc_Narration Details "
                    'StrSQL &= " FROM DocUpload inner join DocType on Dot_Id=Doc_Dot_id INNER JOIN DocIssuer on Dot_Iss_Id=Iss_id AND Dot_Id=Doc_Dot_id LEFT JOIN DocImage on Doi_Doc_id=Doc_id "
                    'StrSQL &= " INNER JOIN OASIS.dbo.BUSINESSUNIT_M on Bsu_Id=Doc_Bsu_id "
                    'StrSQL &= "Where (Doi_ExpDate-Doc_LeadDays)<='" & txtDate.Text & "' and (Doc_Bsu_id='" & Session("sBsuid") & "' " & IIf(Session("sBSUID") = "999998", " or 1=1 ", "") & ") "
                    'StrSQL &= "AND Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc) "
                    'StrSQL &= ") A where 1=1 "
                    'EditPagePath = "../FileMgmt/docUpload.aspx"
                    'strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    'StrSortCol = "[Document Type]"
                    'HeaderTitle = "Document Expiry Report"
                    'NoAddingAllowed = True

                    lblDate.Text = "Expiry As on Date:"
                    dateCol.Visible = True
                    If txtDate.Text = "" Then txtDate.Text = Now.ToString("dd/MMM/yyyy")
                    StrSQL = "SELECT [Document Id], " & IIf(Session("sBSUID") = "999998", " [BSU], ", "") & "[Document Type], [Document Issuer], [Expiry Date], Details from "
                    StrSQL &= "(SELECT bsu_shortname [BSU], Doc_id [Document Id], Doc_Bsu_id, Dot_Descr [Document Type], Iss_Descr [Document Issuer], "
                    StrSQL &= " replace(convert(varchar(30), Doi_IssDate, 106),' ','/') [Issue Date], replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') [Expiry Date], Doc_Narration Details "
                    StrSQL &= " FROM DocUpload inner join DocType on Dot_Id=Doc_Dot_id INNER JOIN DocIssuer on Dot_Iss_Id=Iss_id AND Dot_Id=Doc_Dot_id LEFT JOIN DocImage on Doi_Doc_id=Doc_id "
                    StrSQL &= " INNER JOIN OASIS.dbo.BUSINESSUNIT_M on Bsu_Id=Doc_Bsu_id "
                    StrSQL &= "Where (Doi_ExpDate-Doc_LeadDays)<='" & txtDate.Text & "' and (Doc_Bsu_id='" & Session("sBsuid") & "' " & IIf(Session("sBSUID") = "999998", " or 1=1 ", "") & ") "
                    StrSQL &= "AND Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc) "
                    StrSQL &= " and doc_dot_id in(select dot_id from doctype where ('||'+Dot_Bsu_apply LIKE '%||" & Session("sBSUID") & "||%'  or  '||'+Dot_Bsu_share LIKE '%||" & Session("sBSUID") & "||%')  and  '||'+ Dot_Rol_share LIKE '%||'+(select top 1 cast(usr_rol_id as varchar(5)) from oasis..users_m  where usr_name='" & Session("sUsr_name") & "')+'||%' )"
                    StrSQL &= ") A where 1=1 "
                    EditPagePath = "../FileMgmt/docUpload.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Document Type]"
                    HeaderTitle = "Document Expiry Report"
                    NoAddingAllowed = True
                Case "FD00074"
                    StrSQL = "SELECT [Document Id], [Upload Date], [Expiry Date], [BSU], [Document Type], [Document Issuer], [Document Issued To], Details from "
                    StrSQL &= "(SELECT Doc_id [Document Id], Doc_Bsu_id, cast(Doc_SysDate as varchar(12)) [Upload Date], "
                    StrSQL &= "(select Dot_Descr from DocType where Dot_Id=Doc_Dot_id) [Document Type], Iss_Descr [Document Issuer], "
                    StrSQL &= "bsu_name [Document Issued To], bsu_shortname [BSU], "
                    StrSQL &= "replace(convert(varchar(30), Doi_IssDate, 106),' ','/') [Issue Date], replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') [Expiry Date], "
                    StrSQL &= "Doc_Narration Details "
                    StrSQL &= "FROM DocUpload INNER JOIN DocType on Doc_Dot_id=Dot_Id and ('||'+Dot_Bsu_share LIKE '%||" & Session("sBsuid") & "||%' or '||'+Dot_Rol_share LIKE '%||1||%' or Doc_Bsu_id='" & Session("sBsuid") & "'  or doc_olu_name='" & Session("sUsr_id") & "') "
                    StrSQL &= "INNER JOIN DocIssuer on Dot_Iss_Id=Iss_id AND Dot_Id=Doc_Dot_id LEFT JOIN DocImage on Doi_Doc_id=Doc_id "
                    StrSQL &= "INNER JOIN OASIS.dbo.BUSINESSUNIT_M on Bsu_Id=Doc_Bsu_id "
                    StrSQL &= "WHERE Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc)) A where 1=1 "
                    EditPagePath = "../FileMgmt/docUpload.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Upload Date] Desc"
                    HeaderTitle = "Shared Documents List"
                    NoAddingAllowed = True
                Case "FD00076"
                    StrSQL = "select BSU_ID, BSU, [Business Unit], [Document Count], [Uploaded Recently-Past Week], [Docs Close to Expiry] from "
                    StrSQL &= "(select BSU_ID, BSU_NAME [Business Unit], BSU_SHORTNAME [BSU], "
                    StrSQL &= "cast(isnull((select Count(DISTINCT Doc_Dot_id) from DocUpload where Doc_id IN (SELECT Doi_Doc_id from DocImage) AND Doc_Bsu_Id=bsu_id),0) as varchar)+'/'+cast((SELECT count(*) from DocType where '||'+Dot_Bsu_apply LIKE '%||'+BSU_ID+'||%') as varchar) [Document COunt], "
                    StrSQL &= "isnull((select Count(DISTINCT Doc_Dot_id) from DocUpload where Doc_id IN (SELECT Doi_Doc_id from DocImage where Doi_SysDate>(getdate()-7)) AND Doc_Bsu_Id=bsu_id),0) [Uploaded Recently-Past Week], "
                    StrSQL &= "isnull((select Count(DISTINCT Doc_Dot_id) from DocUpload inner JOIN DocImage on Doc_id=Doi_Doc_id where (Doi_ExpDate-Doc_LeadDays)<getdate() AND Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc) and Doc_id IN (SELECT Doi_Doc_id from DocImage) AND Doc_Bsu_Id=bsu_id),0) [Docs Close to Expiry] "
                    StrSQL &= "FROM OASIS.dbo.BUSINESSUNIT_M) A where 1=1 "
                    EditPagePath = "ListReportView.aspx"
                    MainMenuCode = "FD00078"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Business Unit]"
                    HeaderTitle = "Shared Documents Summary Business Unit wise"
                    NoAddingAllowed = True
                Case "FD00078"
                    StrSQL = "select BSU_ID, BSU, [Business Unit], [Document Issuer], [Document Count], [Uploaded Recently-Past Week], [Docs Close to Expiry] from "
                    StrSQL &= "(select BSU_ID, BSU_NAME [Business Unit], BSU_SHORTNAME [BSU], Iss_Descr [Document Issuer], "
                    StrSQL &= "cast(isnull((select count(DISTINCT Doc_Dot_id) from DocUpload inner join DocType on Doc_Dot_id=Dot_Id and Dot_Iss_Id=Iss_id AND Doc_id IN (SELECT Doi_Doc_id from DocImage) AND Doc_Bsu_Id=bsu_id),0) as varchar)+'/'+cast((SELECT count(*) from DocType where '||'+Dot_Bsu_apply LIKE '%||'+BSU_ID+'||%' and Dot_Iss_Id=Iss_id) as varchar) [Document Count], "
                    StrSQL &= "isnull((select count(DISTINCT Doc_Dot_id) from DocUpload inner join DocType on Doc_Dot_id=Dot_Id and Dot_Iss_Id=Iss_id AND Doc_id IN (SELECT Doi_Doc_id from DocImage where Doi_SysDate>(getdate()-7)) AND Doc_Bsu_Id=bsu_id),0) [Uploaded Recently-Past Week], "
                    StrSQL &= "isnull((select Count(DISTINCT Doc_Dot_id) from DocUpload inner JOIN DocImage on Doc_id=Doi_Doc_id where (Doi_ExpDate-Doc_LeadDays)<getdate() AND Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc) and Doc_id IN (SELECT Doi_Doc_id from DocImage) AND Doc_Bsu_Id=bsu_id),0) [Docs Close to Expiry]"
                    StrSQL &= "FROM OASIS.dbo.BUSINESSUNIT_M cross JOIN DocIssuer WHERE BSU_ID='" & Encr_decrData.Decrypt(Request.QueryString("Viewid").Replace(" ", "+")) & "') A where 1=1 "
                    EditPagePath = "ListReportView.aspx"
                    MainMenuCode = "FD00080"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Business Unit], [Document Issuer]"
                    HeaderTitle = "Shared Documents Summary Issuer wise"
                    NoAddingAllowed = True
                Case "FD00080"
                    StrSQL = "SELECT [Document Id], [Upload Date], [Expiry Date], [BSU], [Document Type], [Document Issuer], [Document Issued To], Details from "
                    StrSQL &= "(SELECT Doc_id [Document Id], cast(Doc_SysDate as varchar(12)) [Upload Date], Dot_Descr [Document Type], "
                    StrSQL &= "Iss_Descr [Document Issuer], bsu_name [Document Issued To], bsu_shortname [BSU], "
                    StrSQL &= "replace(convert(varchar(30), Doi_IssDate, 106),' ','/') [Issue Date], replace(convert(varchar(30), Doi_ExpDate, 106),' ','/') [Expiry Date], "
                    StrSQL &= "Doc_Narration Details "
                    StrSQL &= "FROM DocUpload INNER JOIN DocType on Doc_Dot_id=Dot_Id "
                    StrSQL &= "INNER JOIN DocIssuer on Dot_Iss_Id=Iss_id AND Dot_Id=Doc_Dot_id LEFT JOIN DocImage on Doi_Doc_id=Doc_id "
                    StrSQL &= "INNER JOIN OASIS.dbo.BUSINESSUNIT_M on Bsu_Id=Doc_Bsu_id "
                    StrSQL &= "AND Doi_id = (SELECT top 1 Doi_id from docImage where Doi_Doc_id=Doc_id order by doi_expdate desc) "
                    StrSQL &= "and Doc_BSU_ID='" & Encr_decrData.Decrypt(Request.QueryString("Viewid").Replace(" ", "+")) & "' and ('||'+Dot_Bsu_share LIKE '%||" & Session("sBsuid") & "||%' or '||'+Dot_Rol_share LIKE '%||1||%' or doc_olu_name='" & Session("sUsr_id") & "')) A where 1=1 "
                    MainMenuCode = "FD00074"
                    EditPagePath = "../FileMgmt/docUpload.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[Upload Date] desc, [Document Issuer]"
                    HeaderTitle = "Shared Documents List"
                    NoAddingAllowed = True

                Case "FD00014"
                    StrSQL = " SELECT ACT_ID,[ACCOUNT DESCRIPTION] FROM ("
                    StrSQL &= " SELECT ACT_ID,ACT_DESCR [ACCOUNT DESCRIPTION]"
                    StrSQL &= " FROM [OASIS_DOCS].dbo.[VISACCOUNT]"
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../FileMgmt/VisaActMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "ACT_ID"
                    HeaderTitle = "Account Master"
                Case "FD00016"
                    StrSQL = " SELECT TYP_ID,[ACCOUNT], [DESCRIPTION], RATE FROM ("
                    StrSQL &= " select TYP_ID, ACT_DESCR [ACCOUNT], TYP_DESCR [DESCRIPTION], TYP_RATE RATE "
                    StrSQL &= " FROM [OASIS_DOCS].dbo.VISTRNTYPE inner JOIN [OASIS_DOCS].dbo.VISACCOUNT on ACT_ID=TYP_ACT_ID "
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../FileMgmt/TrnTypeMaster.aspx"
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "TYP_ID"
                    HeaderTitle = "Transaction Type Master"
                Case "FD00017"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Processed"
                    CheckBox3Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and TRN_STATUS=0 and TRN_TYP_ID in (select TYP_ID from vw_typingOnline) "
                        ShowGridCheckBox = True
                        ButtonText1 = "Process"
                        HeaderTitle = "Pending Transaction Details Online"
                    ElseIf rad2.Checked Then
                        ApprStatus = " and TRN_STATUS=1 and TRN_TYP_ID in (select TYP_ID from vw_typingOnline) "
                        ShowGridCheckBox = True
                        ButtonText1 = "Un Process"
                        HeaderTitle = "Pending Transaction Details Online"
                    ElseIf rad3.Checked Then
                        ApprStatus = " and TRN_TYP_ID in (select TYP_ID from vw_typingOnline) "
                        ShowGridCheckBox = False
                        HeaderTitle = "All Transaction Details Online"
                    End If
                    StrSQL = " SELECT TRN_ID, [DATE], BSU, ACCOUNT, [TRANSACTION], EMPLOYEE, FEES FROM ("
                    StrSQL &= " select TRN_ID, replace(convert(varchar(30), TRN_DATE, 106),' ','/') [Date], TRN_BSU_ID BSU, ACT_DESCR Account, "
                    StrSQL &= " TYP_DESCR [Transaction], EMP_NAME Employee, TRN_RATE FEES, TRN_DESCR Details, TRN_TYP_ID, isnull(TRN_STATUS,0) TRN_STATUS "
                    StrSQL &= " from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VISACCOUNT on TYP_ACT_ID=ACT_ID "
                    StrSQL &= " INNER JOIN VW_EMPLOYEE_V on TRN_EMP_ID=EMP_ID inner join oasis..businessunit_m on bsu_shortname=trn_bsu_id "
                    StrSQL &= " AND BSU_ID IN (select USA_BSU_ID FROM oasis..USERACCESS_S where USA_USR_ID='" & Session("sUsr_id") & "')"
                    StrSQL &= " ) A WHERE 1=1 "
                    StrSQL &= ApprStatus
                    EditPagePath = "../FileMgmt/TypingDetails.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "TRN_ID"
                Case "FD00018"
                    ddlFilter.Visible = True
                    StrSQL = " SELECT ~ TRN_ID, [DATE], BSU, ACCOUNT, [TRANSACTION], EMPLOYEE, FEES, CHARGE, [USER] FROM ("
                    StrSQL &= " select TRN_ID, TRN_DATE, replace(convert(varchar(30), TRN_DATE, 106),' ','/') [Date], TRN_BSU_ID BSU, ACT_DESCR Account, "
                    StrSQL &= " TYP_DESCR [Transaction], EMP_NAME Employee, TRN_RATE FEES, cast(TYP_CHARGE as int) CHARGE, TRN_DESCR Details, TRN_USRID [USER] "
                    StrSQL &= " from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VISACCOUNT on TYP_ACT_ID=ACT_ID "
                    StrSQL &= " INNER JOIN VW_EMPLOYEE_V on TRN_EMP_ID=EMP_ID inner join oasis..businessunit_m on bsu_shortname=trn_bsu_id "
                    StrSQL &= " AND BSU_ID IN (select USA_BSU_ID FROM oasis..USERACCESS_S where TRN_DELETED=0 and USA_USR_ID='" & Session("sUsr_id") & "')"
                    StrSQL &= " ) A WHERE 1=1 "
                    EditPagePath = "../FileMgmt/TypingDetails.aspx"
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "[TRN_DATE] desc"
                    HeaderTitle = "Typing Transaction Details"
                Case "FD00019"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Processed"
                    CheckBox3Text = "All"
                    Dim ApprStatus As String = ""
                    If rad1.Checked Then
                        ApprStatus = " and TRN_STATUS=0 and TRN_TYP_ID in (select TYP_ID from vw_typingOnline) "
                        HeaderTitle = "Pending Transaction Details Online"
                    ElseIf rad2.Checked Then
                        ApprStatus = " and TRN_STATUS=1 and TRN_TYP_ID in (select TYP_ID from vw_typingOnline) "
                        HeaderTitle = "Pending Transaction Details Online"
                    ElseIf rad3.Checked Then
                        ApprStatus = " and TRN_TYP_ID in (select TYP_ID from vw_typingOnline) "
                        HeaderTitle = "All Transaction Details Online View"
                    End If
                    StrSQL = " SELECT TRN_ID, [DATE], BSU, ACCOUNT, [TRANSACTION], EMPLOYEE, FEES FROM ("
                    StrSQL &= " select TRN_ID, replace(convert(varchar(30), TRN_DATE, 106),' ','/') [Date], TRN_BSU_ID BSU, ACT_DESCR Account, "
                    StrSQL &= " TYP_DESCR [Transaction], EMP_NAME Employee, TRN_RATE FEES, TRN_DESCR Details, TRN_TYP_ID, isnull(TRN_STATUS,0) TRN_STATUS "
                    StrSQL &= " from VISTRANS INNER JOIN VISTRNTYPE on TYP_ID=TRN_TYP_ID INNER JOIN VISACCOUNT on TYP_ACT_ID=ACT_ID "
                    StrSQL &= " INNER JOIN VW_EMPLOYEE_V on TRN_EMP_ID=EMP_ID inner join oasis..businessunit_m on bsu_shortname=trn_bsu_id "
                    StrSQL &= " AND BSU_ID IN (select USA_BSU_ID FROM oasis..USERACCESS_S where USA_USR_ID='" & Session("sUsr_id") & "')"
                    StrSQL &= " ) A WHERE 1=1 "
                    StrSQL &= ApprStatus
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "TRN_ID"
                Case "FD00080"
                    ddlFilter.Visible = True
                    StrSQL = " SELECT ~ cus_no, [STUDENT ID], [NAME], GRADE, SECTION, [CREDITS], DEBITS, BALANCE FROM ("
                    StrSQL &= "select cus_no, cus_no [STUDENT ID], cus_name NAME, cus_info1 GRADE, cus_info2 SECTION, cdx_debit DEBITS, cdx_credit CREDITS, "
                    StrSQL &= "cdx_credit-cdx_debit BALANCE from posdata..cardex inner join posdata..customer on cdx_cus_id=cus_id "
                    StrSQL &= "and (cdx_debit>0 or cdx_credit>0) and cus_bsu_id='" & Session("sBsuid") & "' "
                    StrSQL &= " ) A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "NAME"
                    HeaderTitle = "Accuro Catering Balance"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                Case "FD00082"
                    ddlFilter.Visible = True
                    StrSQL = " SELECT ~ cus_no, [STUDENT ID], [NAME], GRADE, SECTION, DATE, ITEM, QUANTITY, PRICE, TOTAL FROM ("
                    StrSQL &= "select cus_no, cus_no [STUDENT ID], cus_name [NAME], cus_info1 GRADE, cus_info2 SECTION, xthn_date, replace(convert(varchar(30), date, 106),' ','/') [DATE], ITEM, QUANTITY, PRICE, TOTAL "
                    StrSQL &= "from posdata..vw_mealsConsumption with (nolock) inner join posdata..customer on cus_olu_id=xolu_id and cus_bsu_id='" & Session("sBsuid") & "'"
                    StrSQL &= " ) A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "xthn_date DESC"
                    HeaderTitle = "Accuro Consumption Report"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                Case "FD00084"
                    StrSQL = " SELECT cus_no, [STUDENT ID], [NAME], GRADE, SECTION, [DATE], CASH, COUPONS, [MEAL PLAN], STATUS FROM ("
                    StrSQL &= "select cus_no, cus_no [STUDENT ID], cus_name NAME, cus_info1 GRADE, cus_info2 SECTION, [CASH], DATE, xCRT_REGNDATE, "
                    StrSQL &= "[coupon amt] [COUPONS], [meal plan amt] [MEAL PLAN], [cc status] STATUS from posdata..vw_repcardledger with (nolock) "
                    StrSQL &= "inner join posdata..customer on cus_olu_id=xolu_id and cus_bsu_id='" & Session("sBsuid") & "' "
                    StrSQL &= " ) A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "xCRT_REGNDATE desc"
                    HeaderTitle = "Accuro Card Recharge"
                    NoAddingAllowed = True
                    NoViewAllowed = True

                Case ("F300231")
                    IsStoredProcedure = True
                    StrSQL = "FEES.GetFEEProformaTemplates"
                    ReDim SPParam(0)
                    NoAddingAllowed = False
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    EditPagePath = "../Fees/FeeProformaInvPrintTemplate.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    HeaderTitle = "ProformaInvoice Template"
                    StrSortCol = "VersionNo"
                Case "F300230"
                    StrSQL = "SELECT  FRM_ID ," & _
                             "CASE WHEN FRM_Level = 1 THEN 'First Reminder' " & _
                             "WHEN FRM_Level = 2 THEN 'Second Reminder' " & _
                             "WHEN FRM_Level = 3 THEN 'Third Reminder' END AS FRM_Level ," & _
                             "FRM_REMARKS ,FRM_SHORT_DESCR ,BUSINESSUNIT_M.BSU_NAME " & _
                             "FROM FEES.FEE_REMINDER_M INNER JOIN BUSINESSUNIT_M ON " & _
                             "FEES.FEE_REMINDER_M.FRM_BSU_ID = BUSINESSUNIT_M.BSU_ID WHERE FRM_BSU_ID = '" & Session("sBsuid") & "'"
                    EditPagePath = "../Fees/FeeReminderTemplateforTransport.aspx"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "FRM_ID"
                    HeaderTitle = "Transport Fee Reminder Template"
                Case "A100095"
                    StrSQL &= " select  ID,[CURRENCY CODE],[EXCHANGE RATE],[FROM DATE],[TO DATE] from (select ETR_ID ID,ETR_CUR_ID [CURRENCY CODE],ETR_RATE [EXCHANGE RATE],ETR_FDATE [FROM DATE],ETR_TDATE [TO DATE] from EXGRATE_TRN where ETR_BSU_ID='" & Session("sBSUId") & "' ) a where 1=1"
                    EditPagePath = "../Accounts/AccExchangeRateMaster.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Exchange Rate Master "
                    NoAddingAllowed = False
                    NoViewAllowed = False

                Case "A550024"
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    StrSQL &= "select  ID,[CURRENCY CODE],[EXCHANGE RATE],[FROM DATE],[TO DATE],[APPROVED BY],[APPROVAL DATE] from (select ETR_ID ID,ETR_CUR_ID [CURRENCY CODE],ETR_RATE [EXCHANGE RATE],ETR_FDATE [FROM DATE],ETR_TDATE [TO DATE],ETR_APPROVEDBY [APPROVED BY],ETR_APPROVEDDATE [APPROVAL DATE] from EXGRATE_TRN where ETR_BSU_ID='" & Session("sBSUId") & "' and ETR_APPROVE="
                    If rad1.Checked Then
                        StrSQL &= "0"
                    ElseIf rad2.Checked Then
                        StrSQL &= "1"
                    End If
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Accounts/AccExchangeRateMaster.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
                    StrSortCol = "ID"
                    HeaderTitle = "Exchange Rate Master - Approval"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                Case "F300162" 'Fee PaymentPlan Request
                    CheckBox1Text = "Workflow status"
                    CheckBox2Text = "Action requested"
                    CheckBox3Text = "Agreed payment plan"
                    StrSQL = "SELECT ID,StudentID,StudentName,Grade,ParentName,DocumentDate,Amount, [Status] FROM "
                    StrSQL &= "(SELECT FPP_ID AS ID,FPP_STU_ID AS STU_ID,STU_NO AS StudentID,StudentName,Grade,ParentName,FPP_DATE AS DocumentDate," & _
                        "FPD_AMOUNT AS Amount, [PLAN_STATUS_DESCRIPTION] As [Status]  FROM vwFEE_PAYMENTPLAN_H WHERE CAST(FPP_DATE AS DATE)>='01/SEP/2020' AND FPP_BSU_ID='" & Session("sBSUID") & "' "
                    If rad1.Checked Then
                        StrSQL &= " AND ISNULL(FPP_STATUS,0) IN (1,2) "
                    ElseIf rad2.Checked Then
                        StrSQL &= " AND ISNULL(FPP_STATUS,0) IN (0,3) "
                    ElseIf rad3.Checked Then
                        StrSQL &= " AND ISNULL(FPP_STATUS,0)=4 "
                    End If
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Fees/FeePaymentPlanner.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "DocumentDate DESC,ID DESC"
                    HeaderTitle = "Fee Payment Plan"
                    NoAddingAllowed = False
                    NoViewAllowed = False
                Case "F300171" 'Approve Fee Payment plan by Finance
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "On Hold"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "Approved"
                    StrSQL = "SELECT ID,StudentID,StudentName,Grade,ParentName,DocumentDate,Amount, [Status] FROM "
                    StrSQL &= "(SELECT FPP_ID AS ID,FPP_STU_ID AS STU_ID,STU_NO AS StudentID,StudentName,Grade,ParentName,FPP_DATE AS DocumentDate," & _
                        "FPD_AMOUNT AS Amount, [PLAN_STATUS_DESCRIPTION] As [Status] FROM vwFEE_PAYMENTPLAN_H WHERE CAST(FPP_DATE AS DATE)>='01/SEP/2020' AND FPP_BSU_ID='" & Session("sBSUID") & "' "
                    If rad1.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='' "
                    ElseIf rad2.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FO' "
                    ElseIf rad3.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FR' "
                    ElseIf rad4.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FA' "
                    End If
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Fees/FeePaymentPlanner.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "DocumentDate DESC,ID DESC"
                    HeaderTitle = "Payment Plan Approval (Finance)"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                Case "F300271" 'Approve Fee Payment plan by School Principal
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "On Hold"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "Approved"
                    StrSQL = "SELECT ID,StudentID,StudentName,Grade,ParentName,DocumentDate,Amount, [Status] FROM "
                    StrSQL &= "(SELECT FPP_ID AS ID,FPP_STU_ID AS STU_ID,STU_NO AS StudentID,StudentName,Grade,ParentName,FPP_DATE AS DocumentDate," & _
                        "FPD_AMOUNT AS Amount, [PLAN_STATUS_DESCRIPTION] As [Status] FROM vwFEE_PAYMENTPLAN_H WHERE CAST(FPP_DATE AS DATE)>='01/SEP/2020' AND FPP_BSU_ID='" & Session("sBSUID") & "' "
                    If rad1.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'') IN ('FA','FR') AND ISNULL(FPP_LEVEL2_APPROVED,'')='' "
                    ElseIf rad2.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FA' AND ISNULL(FPP_LEVEL2_APPROVED,'')='PO' "
                    ElseIf rad3.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FA' AND ISNULL(FPP_LEVEL2_APPROVED,'')='PR' "
                    ElseIf rad4.Checked Then
                        StrSQL &= " AND ISNULL(FPP_APPROVED,'')='FA' AND ISNULL(FPP_LEVEL2_APPROVED,'')='PA' "
                    End If
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Fees/FeePaymentPlanner.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "DocumentDate DESC,ID DESC"
                    HeaderTitle = "Payment Plan Approval (Principal)"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                Case "F300151", "F300183" 'Accept fee adjustment transfer from other BSU
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Received"
                    'CheckBox3Text = "Rejected"
                    StrSQL = "SELECT ID,[From.BU],StudentID,StudentName,AdjDate,Remarks,AMOUNT FROM "
                    StrSQL &= "(SELECT  FAI_ID AS ID,FAI_BSU_ID AS BSU_ID,DR_BSU_NAME AS [From.BU],STU_NO AS StudentID,STU_NAME AS StudentName, " & _
                        "FAI_DATE AS AdjDate,FAI_REMARKS AS Remarks,SUM(B.FEE_AMOUNT) AMOUNT " & _
                        "FROM FEES.[vw_FEE_ADJ_INTERUNIT] A LEFT JOIN FEES.vw_FEE_ADJ_INTERUNIT_D B ON A.FAI_ID = B.FAID_FAI_ID AND FAID_DRCR='DR' " & _
                        "WHERE ISNULL(A.FAI_bDELETED, 0) <> 1 AND ISNULL(A.FAI_APPR_STATUS, '') = 'A' AND ISNULL(A.FAI_CR_BSU_ID,'0')='" & Session("sBSUID") & "' AND "
                    If MainMenuCode = "F300183" Then
                        StrSQL &= "ISNULL(FAI_CR_IsProvider,0) = 1 "
                    Else
                        StrSQL &= "ISNULL(FAI_CR_IsProvider,0) = 0 "
                    End If
                    If rad1.Checked Then
                        StrSQL &= " AND ISNULL(FAI_CR_STU_ID,0) = 0 "
                    ElseIf rad2.Checked Then
                        StrSQL &= " AND ISNULL(FAI_CR_STU_ID,0)<>0 "
                    End If
                    StrSQL &= "GROUP BY FAI_ID ,FAI_BSU_ID ,DR_BSU_NAME ,STU_NO ,STU_NAME ,FAI_DATE ,FAI_REMARKS "
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Fees/Fee_Rec_Adj_IU.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "AdjDate DESC,ID DESC"
                    HeaderTitle = "Receive Interunit fee Adjustment"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                    HideExportButton = True
                Case "F300153", "F300181" 'Approve/Reject inter unit adjustment receipt
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    StrSQL = "SELECT ID,[From.BU],StudentID,StudentName,AdjDate,Remarks,AMOUNT FROM "
                    StrSQL &= "(SELECT  FAI_ID AS ID,FAI_BSU_ID AS BSU_ID,DR_BSU_NAME AS [From.BU],STU_NO AS StudentID,STU_NAME AS StudentName, " & _
                        "FAI_DATE AS AdjDate,FAI_REMARKS AS Remarks,SUM(B.FEE_AMOUNT) AMOUNT " & _
                        "FROM FEES.[vw_FEE_ADJ_INTERUNIT] A LEFT JOIN FEES.vw_FEE_ADJ_INTERUNIT_D B ON A.FAI_ID = B.FAID_FAI_ID AND FAID_DRCR='DR' " & _
                        "WHERE ISNULL(A.FAI_bDELETED, 0) <> 1 AND ISNULL(A.FAI_APPR_STATUS, '') = 'A' AND ISNULL(A.FAI_CR_BSU_ID,'0')='" & Session("sBSUID") & "' " & _
                        "AND ISNULL(FAI_CR_STU_ID,0)<>0 AND "
                    If rad1.Checked Then
                        StrSQL &= "ISNULL(FAI_CR_APPR_STATUS,'N') = 'N' "
                    ElseIf rad2.Checked Then
                        StrSQL &= "ISNULL(FAI_CR_APPR_STATUS,'')='A' "
                    ElseIf rad3.Checked Then
                        StrSQL &= "ISNULL(FAI_CR_APPR_STATUS,'')='R' "
                    End If

                    If MainMenuCode = "F300181" Then
                        StrSQL &= " AND ISNULL(FAI_CR_IsProvider,0) = 1 "
                    Else
                        StrSQL &= " AND ISNULL(FAI_CR_IsProvider,0) = 0 "
                    End If


                    StrSQL &= "GROUP BY FAI_ID ,FAI_BSU_ID ,DR_BSU_NAME ,STU_NO ,STU_NAME ,FAI_DATE ,FAI_REMARKS "
                    StrSQL &= ") a where 1=1"
                    EditPagePath = "../Fees/Fee_Rec_Adj_IU.aspx"
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "AdjDate DESC,ID DESC"
                    HeaderTitle = "Receive Interunit fee Adjustment"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                    HideExportButton = True
                Case "P170006" 'Approve/Reject Blackberry Ent.Server Service 
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    StrSQL = "SELECT ID,[Req.RefNo],[Req.Date],EmployeeNo,EmployeeName,Remarks,CurrentStatus FROM "
                    StrSQL &= "(SELECT GSR_ID as ID,GSR_ID as [Req.RefNo],GSR_DATE as [Req.Date],EMPNO as EmployeeNo," & _
                        "EMPNAME as EmployeeName,[STATUS] as CurrentStatus,GSR_REMARKS as REMARKS FROM HR.vwGEN_SERVICE_REQUEST_H " & _
                        "WHERE GSR_BSU_ID='" & Session("sBsuid") & "' AND ISNULL(GSR_bDeleted,0)<>1 AND "
                    If rad1.Checked Then
                        StrSQL &= "ISNULL(GSR_STATUS,'P') = 'P' "
                    ElseIf rad2.Checked Then
                        StrSQL &= "ISNULL(GSR_STATUS,'')='A' "
                    ElseIf rad3.Checked Then
                        StrSQL &= "ISNULL(GSR_STATUS,'')='R' "
                    End If
                    StrSQL &= ") a WHERE 1=1 "
                    EditPagePath = "../Payroll/empBESServiceReq.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "[Req.Date] DESC,[Req.RefNo] DESC"
                    HeaderTitle = "Blackberry Enterprise Server(BES) Service Access Form"
                    NoAddingAllowed = True
                    NoViewAllowed = False
                    HideExportButton = True
                Case "F300285"
                    IsStoredProcedure = True
                    StrSQL = "SP_GET_DATA_REQUEST_ENTRY_INIT_LIST"
                    ReDim SPParam(2)
                    NoAddingAllowed = False
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Forwarded"
                    Dim Status As String
                    If rad1.Checked Then
                        Status = "P"
                    ElseIf rad2.Checked Then
                        Status = "F"
                    ElseIf rad3.Checked Then
                        Status = ""
                    End If
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", Status, SqlDbType.VarChar)
                    EditPagePath = "../Common/UserDataRequestInit.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    HeaderTitle = Mainclass.GetMenuCaption("F300285")
                    StrSortCol = "DRI_ID"
                Case "F300287"
                    IsStoredProcedure = True
                    StrSQL = "SP_GET_DATA_REQUEST_ENTRY_LIST_FOR_BSU"
                    ReDim SPParam(2)
                    NoAddingAllowed = True
                    CheckBox1Text = "Pending"
                    CheckBox2Text = "Submitted"
                    Dim Status As String
                    If rad1.Checked Then
                        Status = "P"
                    ElseIf rad2.Checked Then
                        Status = "F"
                    ElseIf rad3.Checked Then
                        Status = ""
                    End If
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", Status, SqlDbType.VarChar)
                    EditPagePath = "../Common/UserDataRequestEntry.aspx"
                    strConn = ConnectionManger.GetOASISConnectionString
                    HeaderTitle = Mainclass.GetMenuCaption("F300287")
                    StrSortCol = "DRI_ID DESC"
                Case "H000119" 'Evaluation Group TYpe
                    'StrSQL = "SELECT [ID],[Group Description],[Group Type] from(select GTH_ID Id,GTH_DESCR [Group Description],case when [GTH_TYPE]=1 then 'Observer' else 'Staff' end [Group Type] from [EVA].[EVAL_GROUPTYPE_H] where GTH_BSU_ID='" & Session("sBSUID") & "'  ) A where 1=1 "
                    StrSQL = "SELECT [ID],[Group Description],[Group Type],[Staff List] from(select GTH_ID Id,GTH_DESCR [Group Description],case when [GTH_TYPE]=1 then 'Observer' else 'Staff' end [Group Type],"
                    StrSQL &= "stuff((select isnull(emp_name,'School') +'<br>' FROM [EVA].[EVAL_GROUPTYPE_D]  left outer join oasis_docs..VW_EMPLOYEE_V on GTD_EMP_ID=EMP_ID where GTH_ID=GTD_GTH_ID  for xml path(''), type ).value('.','varchar(max)'), 1, 0, '') [Staff List]"
                    StrSQL &= " from [EVA].[EVAL_GROUPTYPE_H] where GTH_BSU_ID='" & Session("sBSUID") & "'  ) A where 1=1 "
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "[Group Description]"
                    HeaderTitle = "Evaluation Group Master"
                    EditPagePath = "../Staff_Evaluation/Staff_Evaluation/M_EvaluationGroup.aspx"
                    NoAddingAllowed = False
                    NoViewAllowed = False
                Case "H000992" 'Evaluation Group TYpe from phoenix
                    'StrSQL = "SELECT [ID],[Group Description],[Group Type] from(select GTH_ID Id,GTH_DESCR [Group Description],case when [GTH_TYPE]=1 then 'Observer' else 'Staff' end [Group Type] from [EVA].[EVAL_GROUPTYPE_H] where GTH_BSU_ID='" & Session("sBSUID") & "'  ) A where 1=1 "
                    StrSQL = "SELECT [ID],[Group Description],[Group Type],[Staff List] from(select GTH_ID Id,GTH_DESCR [Group Description],case when [GTH_TYPE]=1 then 'Observer' else 'Staff' end [Group Type],"
                    StrSQL &= "stuff((select isnull(emp_name,'School') +'<br>' FROM [EVA].[EVAL_GROUPTYPE_D]  left outer join oasis_docs..VW_EMPLOYEE_V on GTD_EMP_ID=EMP_ID where GTH_ID=GTD_GTH_ID  for xml path(''), type ).value('.','varchar(max)'), 1, 0, '') [Staff List]"
                    StrSQL &= " from [EVA].[EVAL_GROUPTYPE_H] where GTH_BSU_ID='" & Session("sBSUID") & "'  ) A where 1=1 "
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "[Group Description]"
                    HeaderTitle = "Evaluation Group Master"
                    EditPagePath = "../Staff_Evaluation/Staff_Evaluation/M_EvaluationGroup.aspx"
                    NoAddingAllowed = False
                    NoViewAllowed = False

                    'DAX Error Log
                Case "H150015"
                    IsStoredProcedure = True
                    StrSQL = "getERRORLOG"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    'EditPagePath = "../Payroll/empExportSalaryDetail.aspx"
                    strConn = ConnectionManger.OASIS_DAX_AUDIT_ConnectionString
                    HeaderTitle = "DAX Error Log"
                    StrSortCol = "ID"

                    'DAX Integration Log
                Case "H150025"
                    IsStoredProcedure = True
                    StrSQL = "getAXINTEGRATIONLOG"
                    NoAddingAllowed = True
                    NoViewAllowed = True
                    strConn = ConnectionManger.OASIS_DAX_ConnectionString
                    HeaderTitle = "DAX Integration Log"
                    StrSortCol = "ID"
                Case "A100003" 'Bank Holidays
                    StrSQL = "SELECT ID,[Event],[Country],[From Date],[To Date] FROM " & _
                        "(SELECT BHY_ID AS ID,BHY_FDT AS [From Date],BHY_TDT AS [To Date],BHY_COMMENTS AS [Event] ,CTY_DESCR AS [Country] FROM dbo.VW_BANKHOLIDAYS_M " & _
                        "WHERE BHY_bDELETED = 0 AND BHY_CTY_ID IN (SELECT BSU_COUNTRY_ID FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuId") & "'))AS A WHERE 1=1"
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "[From Date]"
                    HeaderTitle = "Bank Holidays"
                    EditPagePath = "../Accounts/BankHolidayMaster.aspx"
                    NoAddingAllowed = False
                    NoViewAllowed = False
                Case "F300137"
                    StrSQL = "SELECT ID, [Doc No],[Doc Date],[StudentId],[Name],[Receipt No],[Cheque Nos],[Status] FROM " & _
                        "(SELECT FPC_ID ID, FPC_ID [Doc No], FPC_DATE [Doc Date],ST.STU_NO [StudentId],ST.NAME [Name],FC.FCL_RECNO [Receipt No], " & _
                        "CASE ISNULL(H.FPC_STATUS ,'N') WHEN 'N' THEN 'PENDING' WHEN 'A' THEN 'APPROVED' WHEN 'D' THEN 'DELETED' END [Status] ," & _
                        "STUFF(( SELECT  ',' + FCQ_CHQNO FROM FEES.FEE_COLL_PDC_CANCEL_D AS CD WITH ( NOLOCK ) " & _
                        "INNER JOIN OASIS.FEES.FEECOLLCHQUES_D WITH ( NOLOCK ) ON FPC_BSU_ID = FCQ_BSU_ID AND CD.FPCD_FCQ_ID = FCQ_ID WHERE CD.FPCD_FPC_ID = H.FPC_ID " & _
                        "FOR XML PATH('')), 1, 1, '') AS [Cheque Nos] FROM FEES.FEE_COLL_PDC_CANCEL_H AS H WITH ( NOLOCK ) INNER JOIN dbo.STUDENTS AS ST ON H.FPC_STU_ID=ST.STU_ID " & _
                        "INNER JOIN FEES.FEECOLLECTION_H AS FC WITH(NOLOCK) ON FC.FCL_BSU_ID=H.FPC_BSU_ID AND FC.FCL_ID=H.FPC_FCL_ID WHERE ISNULL(H.FPC_bDELETED,0)=0 AND H.FPC_BSU_ID='" & Session("sBsuId") & "' ) AS BB WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[Doc Date]"
                    HeaderTitle = "Collection PDC cancellation"
                    EditPagePath = "../Fees/FeePDCCancellation.aspx"
                Case "F300138"
                    StrSQL = "SELECT ID, [Doc No],[Doc Date],[StudentId],[Name],[Receipt No],[Cheque Nos],[Status] FROM " & _
                        "(SELECT FPC_ID ID, FPC_ID [Doc No], FPC_DATE [Doc Date],ST.STU_NO [StudentId],ST.NAME [Name],FC.FCL_RECNO [Receipt No], " & _
                        "CASE ISNULL(H.FPC_STATUS ,'N') WHEN 'N' THEN 'PENDING' WHEN 'A' THEN 'APPROVED' WHEN 'D' THEN 'DELETED' END [Status] ," & _
                        "STUFF(( SELECT  ',' + FCQ_CHQNO FROM FEES.FEE_COLL_PDC_CANCEL_D AS CD WITH ( NOLOCK ) " & _
                        "INNER JOIN OASIS.FEES.FEECOLLCHQUES_D WITH ( NOLOCK ) ON FPC_BSU_ID = FCQ_BSU_ID AND CD.FPCD_FCQ_ID = FCQ_ID WHERE CD.FPCD_FPC_ID = H.FPC_ID " & _
                        "FOR XML PATH('')), 1, 1, '') AS [Cheque Nos] FROM FEES.FEE_COLL_PDC_CANCEL_H AS H WITH ( NOLOCK ) INNER JOIN dbo.STUDENTS AS ST ON H.FPC_STU_ID=ST.STU_ID " & _
                        "INNER JOIN FEES.FEECOLLECTION_H AS FC WITH(NOLOCK) ON FC.FCL_BSU_ID=H.FPC_BSU_ID AND FC.FCL_ID=H.FPC_FCL_ID WHERE ISNULL(H.FPC_bDELETED,0)=0 AND H.FPC_BSU_ID='" & Session("sBsuId") & "' AND ISNULL(H.FPC_STATUS ,'N')='N' ) AS BB WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[Doc Date]"
                    HeaderTitle = "Collection PDC cancellation"
                    EditPagePath = "../Fees/FeePDCCancellation.aspx"

                Case "PI02060" ' Approve Hiring/Dehiring

                    CheckBox1Text = "Pening"
                    CheckBox2Text = "Approved"
                    NoAddingAllowed = True
                    ShowGridCheckBox = False
                    StrSQL = "SELECT ID,[CONTRACT NO],EMPLOYEE, [STATUS],[RENT],[START DATE],[END DATE] FROM ("
                    StrSQL &= "SELECT TCH_ID ID, TCH_NO [CONTRACT NO], TCH_STATUS_STR [STATUS], "
                    StrSQL &= "TCH_CIT_ID, TCH_TOTAL [RENT],TCH_TOTAL, TCH_START [START DATE], TCH_END [END DATE], TCH_ENT_AMOUNT, TCH_ENT_DEDUCTION, TCH_BUDGETED, TCH_TERMS, TCH_RECOVERYPROCESS, "
                    StrSQL &= "TCH_UNIT_PAYING, TCH_OCCUPANTS, TCH_OLDCONTRACT, TCH_REA_ID, TCH_REL_ID, TCH_BSU_ID, TCH_USR_NAME, TCH_FYEAR, TCH_STATUS, TCH_APR_ID, TCH_STATUS_STR, TCH_DELETED, "
                    StrSQL &= "stuff((select isnull(emp_name,'School') +'<br>' FROM TCT_E left outer join oasis_docs..VW_EMPLOYEE_V on TCE_EMP_ID=EMP_ID where TCH_ID=TCE_TCH_ID and tce_deleted=0 "
                    StrSQL &= "for xml path(''), type ).value('.','varchar(max)'), 1, 0, '') EMPLOYEE "
                    StrSQL &= "FROM TCT_H WHERE TCH_STATUS='C' "
                    StrSQL &= " and tch_bsu_id='" & Session("sBsuid") & "'"


                    If rad1.Checked Then
                        StrSQL &= "and isnull(TCH_HD_STATUS,'')='S' "
                        HeaderTitle = "Pending Hiring/Dehiring Contracts"
                    Else
                        StrSQL &= "and isnull(TCH_HD_STATUS,'')='' "
                        HeaderTitle = "Approved Hiring/Dehiring Contracts"
                    End If
                    StrSQL &= ") a where 1=1"

                    StrSortCol = "[CONTRACT NO] DESC"
                    EditPagePath = "../Inventory/TenancyContractHiringDehiring.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "PI02055" ' Housing waiver Form 
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    IsStoredProcedure = True
                    StrSQL = "GetHousingWaiverList"
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)

                    If rad1.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                    ElseIf rad2.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                    ElseIf rad3.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                    ElseIf rad4.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "4", SqlDbType.VarChar)
                    End If
                    StrSortCol = "[THW_NO] DESC"
                    EditPagePath = "../Inventory/HousingWaiver.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    HeaderTitle = "Excess Rent Waiver List"
                    NoAddingAllowed = False

                Case "PI04020" ' Housing waiver Approval 
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"

                    NoViewAllowed = False
                    IsStoredProcedure = True
                    StrSQL = "GetHousingWaiverApproval"
                    ReDim SPParam(3)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)
                    If rad1.Checked Then
                        NoViewAllowed = False
                        ShowGridCheckBox = True
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        ButtonText1 = "Approve Housing waiver Request"
                        StrSortCol = "THW_ID DESC"
                    ElseIf rad2.Checked Then
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        ButtonText1 = ""
                        StrSortCol = "THW_ID Desc"
                    ElseIf rad3.Checked Then
                        NoViewAllowed = True
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "THW_ID Desc"
                    Else

                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "4", SqlDbType.VarChar)
                        ButtonText1 = ""
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "THW_ID DESC"
                    End If
                    HeaderTitle = "Housing Waiver Approval"
                    NoAddingAllowed = True
                    EditPagePath = "../Inventory/HousingWaiver.aspx"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

                Case "PI04030" ' DeHiring Approval 
                    CheckBox1Text = "Forwarded"
                    CheckBox2Text = "Approved"
                    CheckBox3Text = "Rejected"
                    CheckBox4Text = "All"

                    NoViewAllowed = False
                    IsStoredProcedure = True
                    StrSQL = "GetTRFDEHIRINGAPPROVAL"
                    ReDim SPParam(3)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@USER_NAME", Session("sUsr_name"), SqlDbType.VarChar)

                    If rad1.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        NoViewAllowed = False
                        ShowGridCheckBox = True
                        ButtonText1 = "Approve DeHiring Request"
                        StrSortCol = "TCD_ID DESC"
                    ElseIf rad2.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "TCD_ID Desc"
                    ElseIf rad3.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "TCD_ID Desc"
                    ElseIf rad4.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "4", SqlDbType.VarChar)

                        If Session("sUsr_name") = "jemelie.comaingking" Or Session("sUsr_name") = "mahesh.kumar" Then
                            NoViewAllowed = False
                        Else
                            NoViewAllowed = True
                        End If



                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "TCD_ID DESC"
                    Else
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "5", SqlDbType.VarChar)
                        NoViewAllowed = True
                        ShowGridCheckBox = False
                        ButtonText1 = ""
                        StrSortCol = "TCD_ID DESC"


                    End If
                    HeaderTitle = "DeHiring Approval"

                    EditPagePath = "../Inventory/TenancyContractDeHiring.aspx"

                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

                Case "PI02050"
                    CheckBox1Text = "Open"
                    CheckBox2Text = "Forwarded"
                    CheckBox3Text = "Archive"
                    CheckBox4Text = "Rejected"
                    CheckBox5Text = "All"
                    Dim ApprStatus As String = ""
                    NoViewAllowed = False
                    IsStoredProcedure = True
                    StrSQL = "GetDEHIRINGList"
                    'GetHousingWaiverList
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)

                    If rad1.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                        NoViewAllowed = False
                    ElseIf rad2.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                        NoViewAllowed = True
                    ElseIf rad3.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                    ElseIf rad4.Checked Then
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "4", SqlDbType.VarChar)
                        NoViewAllowed = True
                    Else
                        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "5", SqlDbType.VarChar)
                        NoViewAllowed = True
                    End If
                    StrSortCol = "[DeHiring No] DESC"
                    EditPagePath = "../Inventory/TenancyContractDeHiring.aspx"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    HeaderTitle = "De Hiring List"
                    NoAddingAllowed = False

                    'Case "PI02050"
                    '    CheckBox1Text = "Open"
                    '    CheckBox2Text = "Forwarded"
                    '    CheckBox3Text = "Archive"
                    '    CheckBox4Text = "Rejected"
                    '    CheckBox5Text = "All"
                    '    Dim ApprStatus As String = ""
                    '    NoViewAllowed = False
                    '    IsStoredProcedure = True
                    '    StrSQL = "GetDEHIRINGList"
                    '    'GetHousingWaiverList
                    '    ReDim SPParam(2)
                    '    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    '    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)

                    '    If rad1.Checked Then
                    '        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                    '    ElseIf rad2.Checked Then
                    '        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "2", SqlDbType.VarChar)
                    '    ElseIf rad3.Checked Then
                    '        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "3", SqlDbType.VarChar)
                    '    ElseIf rad4.Checked Then
                    '        SPParam(2) = Mainclass.CreateSqlParameter("@OPTION", "4", SqlDbType.VarChar)
                    '    End If
                    '    StrSortCol = "[DeHiring No] DESC"
                    '    EditPagePath = "../Inventory/TenancyContractDeHiring.aspx"
                    '    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    '    HeaderTitle = "De Hiring List List"
                    '    NoAddingAllowed = False
                Case "A100359"

                    NoViewAllowed = False
                    NoAddingAllowed = False
                    IsStoredProcedure = True
                    StrSQL = "[DAX].[SP_GET_CUSTOMER_ACCOUNT_DETAILS_VIEW]"
                    'GetHousingWaiverList
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@CUS_ACT_CODE", "", SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@ACT_ID", "", SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@ACT_NAME", "", SqlDbType.VarChar)

                    StrSortCol = "[ACT_NAME] DESC"
                    EditPagePath = "../Accounts/CustomerAccountCreation.aspx"
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    HeaderTitle = "Customer List"
                    NoAddingAllowed = False
            End Select
            SQLSelect = StrSQL
            ConnectionString = strConn
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Function PrintProforma(ByVal PINo As String) As Boolean
        Try
            If PINo <> "" Then

                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                Dim cmd As New SqlCommand
                cmd.CommandText = "rptTPTProformaInvoice"
                Dim sqlParam(1) As SqlParameter
                sqlParam(0) = Mainclass.CreateSqlParameter("@TRN_numberstr", PINo, SqlDbType.VarChar)
                cmd.Parameters.Add(sqlParam(0))
                cmd.Connection = New SqlConnection(str_conn)
                cmd.CommandType = CommandType.StoredProcedure
                'V1.2 comments start------------
                Dim params As New Hashtable

                params("userName") = Session("sUsr_name")
                params("reportCaption") = "PROFORMA INVOICE"


                Dim repSource As New MyReportClass
                repSource.Command = cmd
                repSource.Parameter = params
                If Session("sBsuid") = "900501" Then
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTProformaInvoice.rpt"
                Else
                    repSource.ResourceName = "../../Transport/ExtraHiring/rptTPTProformaInvoiceBBT.rpt"
                End If

                repSource.IncludeBSUImage = True
                Session("ReportSource") = repSource
                '    Response.Redirect("../Reports/ASPX Report/Rptviewer.aspx", True)
                ReportLoadSelection()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function ApproveNESCreditNote(ByVal TRN_IDs As String) As Boolean
        If TRN_IDs <> "" Then
            Dim strConn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim ID As String, PRF_ID() As String = TRN_IDs.Split("|")
            For Each ID In PRF_ID
                SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, "update NESCreditNote set NCN_STATUS='A' where NCN_POSTED=0 and NCN_DELETED=0 and NCN_ID=" & ID)
                writeWorkFlowTPTH(ID, "Approved", "Credit Note Approved", "A")
            Next
            gridbind(True)
        End If
    End Function

    Private Function writeWorkFlowTPTH(ByVal trnno As String, ByVal strAction As String, ByVal strDetails As String, ByVal strStatus As String) As Integer
        Dim iParms(6) As SqlClient.SqlParameter
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)

        iParms(1) = Mainclass.CreateSqlParameter("@WRK_USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
        iParms(2) = Mainclass.CreateSqlParameter("@WRK_ACTION", strAction, SqlDbType.VarChar)
        iParms(3) = Mainclass.CreateSqlParameter("@WRK_DETAILS", strDetails.Replace("'", "`"), SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@WRK_TRN_NO", trnno, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@WRK_STATUS", strStatus, SqlDbType.VarChar)
        Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "saveWorkFlowTPTH", iParms)
        If RetVal = "-1" Then
            lblError.Text = "Unexpected Error " '!!!
            stTrans.Rollback()
            Exit Function
        Else
            stTrans.Commit()
        End If
    End Function


    Private Sub RememberOldValues()
        Dim itmIdList As New ArrayList()
        Dim bindDt As DataTable
        bindDt = gvDetails.DataSource
        For Each row As GridViewRow In gvDetails.Rows
            Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text

            Dim result As Boolean = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox).Checked
            If ViewState("checked_items") IsNot Nothing Then
                itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
            End If
            If result Then
                If Not itmIdList.Contains(index) Then
                    itmIdList.Add(index)
                End If
            Else
                itmIdList.Remove(index)
            End If
        Next
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk IsNot Nothing Then

                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub RePopulateValues()
        Dim itmIdList As ArrayList = DirectCast(ViewState("checked_items"), ArrayList)
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            For Each row As GridViewRow In gvDetails.Rows
                Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text
                If itmIdList.Contains(index) Then
                    Dim myCheckBox As HtmlInputCheckBox = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub


End Class

