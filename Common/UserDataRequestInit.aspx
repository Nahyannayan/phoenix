<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="UserDataRequestInit.aspx.vb" Inherits="UserDataRequestInit" Title="Untitled Page" %>

<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc1" %>
<%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register TagPrefix="qsf" Namespace="Telerik.QuickStart" %>--%>
<%@ Register Src="~/UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <script src="../Scripts/jquery.mtz.monthpicker.js" type="text/javascript"></script>

    <%--    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" />--%>
    <%--<%@ Register Src="../UserControls/usrSelStudent.ascx" TagName="usrSelStudent" TagPrefix="uc2" %>--%>
    <style>
        /*bootstrap class overrides starts here*/
        .card-body {
            padding: 0.25rem !important;
        }

        table th, table td {
            padding: 0.1rem !important;
        }

        /*bootstrap class overrides ends here*/


        /***/
        html body .riSingle .riTextBox[type="text"] {
            padding: 10px;
        }

        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
        }

        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../images/calendar.gif) !important;
            width: 30px !important;
            height: 30px !important;
            background-position: 0 !important;
        }

        .RadPicker { /*width:80% !important;*/
        }

        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }

        /***/


        .RadGrid {
            border-radius: 0px !important;
            overflow: hidden;
        }

        .RadGrid_Default .rgCommandTable td {
            border: 0;
        }




        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            /*width: 80%;*/
            background-image: none !important;
            background-color: #fff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        .RadComboBox .rcbInputCell {
            width: 100%;
            height: 31px !important;
            background-color: transparent !important;
            border-radius: 6px !important;
            background-image: none !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
            padding: 0px !important;
        }

        .RadComboBox_Default {
            /* color: #333; */
            font: inherit;
            width: 80% !important;
            font-style: normal !important;
        }

            .RadComboBox_Default .rcbEmptyMessage {
                font-style: normal !important;
            }

            .RadComboBox_Default .rcbFocused .rcbInput {
                color: black;
                /*padding: 10px;*/
            }

        .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image: none !important;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0px !important;
            border-width: 0;
        }

        /* overwriting bootstrap css class for caption*/
        .caption {
            padding: 0px inherit !important;
        }

        .RadComboBox .rcbReadOnly .rcbInput {
            cursor: default;
            /*border: 1px solid rgba(0,0,0,0.12) !important;
    padding: 20px 10px !important;*/
        }

        table.RadCalendarMonthView_Default {
            background-color: #ffffff !important;
        }

        .RadGrid_Default {
            border: 0px !important;
        }

            .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
                border: 1px solid rgba(0,0,0,0.16);
            }

        .RadComboBoxDropDown .rcbList {
            line-height: 30px !important;
            font-size: 14px;
        }

        .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table {
            border: 1px solid rgba(0, 0, 0, 0.16) !important;
        }

        .RadGrid_Default .rgGroupPanel {
            background-image: none !important;
        }

            .RadGrid_Default .rgGroupPanel td {
                padding: 0px !important;
            }

        .RadGrid .rgNumPart a {
            line-height: 6px !important;
            padding: 0px 1px !important;
            background-color: transparent !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function GetDesignations() {
            var sFeatures;
            var lstrVal;
            var lintScrVal;
            var pMode;
            var NameandCode;
            var PrevSelectedCode;
            PrevSelectedCode = document.getElementById('<%= h_Designations.ClientID %>').value;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 350px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            pMode = "DESG"
            url = "../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1";
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%= h_Designations.ClientID %>').value = PrevSelectedCode + "|" + NameandCode[0];
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblReportCaption" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="m-auto">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
                <table id="Table1" align="center" width="100%">

                    <tr>
                        <td>
                            <table align="left" width="100%">
                                <tr>
                                    <td align="left">
                                        <table width="100%">
                                            <tr id="tr1" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Title </span>
                                                </td>
                                                <td align="left" runat="server" width="80%">
                                                    <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trSearchStr" runat="server">
                                                <td id="Td9" width="20%" align="left" runat="server"><span class="field-label">Description </span>
                                                </td>
                                                <td id="Td10" align="left" runat="server" width="80%">
                                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trBSUIDs" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Business Unit(s) </span>
                                                </td>
                                                <td align="left" runat="server" width="80%">
                                                    <div class="checkbox-list">
                                                        <telerik:RadTreeView ID="RadBSUTreeView" Enabled="true" runat="server" CausesValidation="false"
                                                            CheckBoxes="True" MultipleSelect="True" CheckChildNodes="True">
                                                        </telerik:RadTreeView>
                                                        <asp:Label ID="lblSelectedBSU" runat="server" class="input-lblMessage"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trDesigID" runat="server">
                                                <td id="Td5" align="left" runat="server" width="20%"><span class="field-label">Designation(s) </span>
                                                </td>
                                                <td id="Td6" align="left" runat="server" width="80%">
                                                    <asp:TextBox ID="TextBox1" runat="server"
                                                        Enabled="false">Please Select the Designation(s).</asp:TextBox>
                                                    <asp:ImageButton ID="imgDesigID" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="return GetDesignations()"></asp:ImageButton><br />
                                                    <telerik:RadGrid ID="RadGridDesignation" runat="server" AllowSorting="True" Width="50%"
                                                        GridLines="Vertical" AllowPaging="True" PageSize="5" EnableTheming="False" CellSpacing="0"
                                                        AutoGenerateColumns="false">
                                                        <ClientSettings EnableRowHoverStyle="True">
                                                        </ClientSettings>
                                                        <AlternatingItemStyle CssClass="input-RadGridAltRow" HorizontalAlign="Left" />
                                                        <MasterTableView GridLines="Both">
                                                            <EditFormSettings>
                                                            </EditFormSettings>
                                                            <ItemStyle CssClass="input-RadGridRow" HorizontalAlign="Left" />
                                                            <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <telerik:GridTemplateColumn UniqueName="TemplateColumn">
                                                                    <HeaderTemplate>
                                                                        Description
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="h_Grd_Desig_ID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ID")%>' />
                                                                        <asp:Label ID="lblDescr" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "DESCR")%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn UniqueName="TemplateColumn">
                                                                    <HeaderTemplate>
                                                                        Delete
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkbtngrdACTDelete" runat="server" OnClick="lnkbtnDesigDelete_Click">Delete</asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                            <tr id="trAsOnDate" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Last Date for Submission </span>
                                                </td>
                                                <td align="left" runat="server" width="80%">
                                                    <telerik:RadDatePicker ID="txtLastDate" runat="server" DateInput-EmptyMessage="MinDate">
                                                        <DateInput ID="DateInput3" EmptyMessage="MinDate" DateFormat="dd/MMM/yyyy" runat="server">
                                                        </DateInput>
                                                        <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                                            ViewSelectorText="x" runat="server">
                                                        </Calendar>
                                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr id="trRowKey" runat="server">
                                                <td align="left" runat="server" width="20%"><span class="field-label">Row Keys </span>
                                                </td>
                                                <td align="left" runat="server" width="80%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="30%">
                                                                <asp:TextBox ID="txtRowKey" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td width="20%">
                                                                <asp:Button ID="btnLstAddRow" runat="server" CssClass="button" Text="Add" />
                                                                <br />
                                                                <asp:Button ID="btnLstRemoveRow" runat="server" CssClass="button" Text="Remove" />
                                                            </td>
                                                            <td width="50%">

                                                                <asp:ListBox ID="lstRowKeyVal" runat="server"></asp:ListBox>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trColumnKey" runat="server">
                                                <td id="Td1" align="left" runat="server" width="20%"><span class="field-label">Column Keys </span>
                                                </td>
                                                <td id="Td2" align="left" runat="server" width="80%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="30%">
                                                                <asp:TextBox ID="txtColumnKey" runat="server"></asp:TextBox>
                                                                <br />
                                                                <telerik:RadComboBox ID="RadColDataType" runat="server" EmptyMessage="Type to search..."
                                                                    EnableScreenBoundaryDetection="False" AutoPostBack="False">
                                                                    <Items>
                                                                        <telerik:RadComboBoxItem Text="Numeric" Value="Numeric" />
                                                                        <telerik:RadComboBoxItem Text="Text" Value="Text" />
                                                                        <telerik:RadComboBoxItem Text="Date" Value="Date" />
                                                                    </Items>
                                                                </telerik:RadComboBox>
                                                            </td>
                                                            <td width="20%">
                                                                <asp:Button ID="btnLstAddCol" runat="server" CssClass="button" Text="Add" />
                                                                <br />
                                                                <asp:Button ID="btnLstRemoveCol" runat="server" CssClass="button" Text="Remove" />
                                                            </td>
                                                            <td width="50%">

                                                                <asp:ListBox ID="lstColumnKeyVal" runat="server"></asp:ListBox>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trStatus" runat="server">
                                                <td id="Td111" align="left" runat="server" width="20%"></td>
                                                <td id="TdChkActive" align="left" runat="server" width="80%">
                                                    <asp:CheckBox ID="chkActive" runat="server" Text="Active" CssClass="field-label" />
                                                    <asp:CheckBox ID="chkForward" runat="server" Text="Forward for data entry" CssClass="field-label" />

                                                    <asp:Label ID="lblEntryForwarded" runat="server" Font-Bold="True" ForeColor="#009900" CssClass="field-label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPendingBSU" runat="server">
                                                <td id="Td1113232" align="left" runat="server" width="20%"><span class="field-label">Pending Business units to Submit </span>
                                                </td>
                                                <td id="TdPendingBSU" align="left" runat="server" width="80%">
                                                    <asp:Label ID="lblPendingBSU" runat="server" class="field-label"></asp:Label>
                                                    <div>
                                                        <asp:Button ID="btnAlertPendingBSU" runat="server" Text="Alert Pending Units" CssClass="button" />
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblAlertMessage" runat="server" Font-Bold="True" ForeColor="#009900" CssClass="field-label"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trGenerateReport" runat="server">
                                                <td align="right" colspan="2">
                                                    <asp:Button ID="btnPreviewTable" runat="server" Text="Preview Table" CssClass="button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <center>
                                            <telerik:RadGrid ID="RadGridReport" runat="server" AllowSorting="True" Width="100%"
                                                GridLines="Vertical" AllowPaging="True" PageSize="100" EnableTheming="False"
                                                CellSpacing="0" AutoGenerateColumns="false">
                                                <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="True">
                                                </ClientSettings>
                                                <AlternatingItemStyle CssClass="input-RadGridAltRow" HorizontalAlign="Left" />
                                                <MasterTableView GridLines="Both" EnableColumnsViewState="false">
                                                    <EditFormSettings>
                                                    </EditFormSettings>
                                                    <ItemStyle CssClass="input-RadGridRow" HorizontalAlign="Left" />
                                                    <HeaderStyle Font-Bold="True" Font-Size="XX-Small" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <telerik:GridTemplateColumn UniqueName="TemplateColumn">
                                                            <HeaderTemplate>
                                                                DESCRIPTION
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDescr" Width="200px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "DESCRIPTION")%>'></asp:Label>
                                                                <asp:HiddenField ID="h_Grd_Row_ID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "ID")%>' />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </center>
                                                </td>
                                            </tr>
                                            <tr id="tr2" runat="server">
                                                <td align="center" colspan="2">
                                                    <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Add" />
                                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" />
                                                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" />
                                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                                                    <asp:Button ID="btnPrint" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Print" />
                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                                        Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
    <div id="popupSiblingPay" runat="server" style="width: 100%; height: 100%; position: fixed; left: 0%; top: 0%; background: url(../Images/dark.png) 0 0 !important; display: none; z-index: 10000;">
        <div id="divbox-panel_confirm" style="height: 80%; width: 80%; background-color: #f0f0f0; border: 1px solid #0092d7; margin: 100px auto 0px auto; padding: 0px 0px 0px 0px;">
            <div style="height: 90%; width: 98%; border: solid 1px #ccc; border-collapse: collapse; margin: 6px 6px 6px 6px; background-color: #FFFFFF; padding-top: 12px; text-align: left;">
                <center>
                    <iframe id="frPop" runat="server" src="about:blank" style="width: 100% !important;
                        height: 100% !important;"></iframe>
                </center>
            </div>
            <div>
                <asp:LinkButton ID="lbtnClose" runat="server" Text="CLOSE" Style="color: Red !important; position: relative; float: right; padding-right: 4px; margin-top: 2px; text-decoration: none; font-family: Helvetica, Arial, sans-serif; font-size: 18px; letter-spacing: 1px; border: 0px none; -webkit-transition: all 0.1s ease-out; -moz-transition: all 0.1s ease-out; -o-transition: all 0.1s ease-out; -ms-transition: all 0.1s ease-out;"
                    CssClass="closelbtnPl"
                    ToolTip="click here to close" OnClientClick="return CloseFrame();" CausesValidation="false"></asp:LinkButton>
            </div>
        </div>
    </div>

    <script type="text/javascript" language="javascript">
        function ShowReport() {
            $("#<%=popupSiblingPay.ClientID %>").css("display", "block");
            var hiddenFieldID = "input[id$=hdn_DRI_ID]";
            var DriId = $(hiddenFieldID).val();
            var url = "../Fees/Reports/ASPX/rptFeeReportViewer.aspx?TYPE=DATAREQ&DRIID=" + DriId;
            $("#<%=frPop.ClientID %>").attr('src', url);
            return false;
        }
        function CloseFrame() {
            $("#<%=frPop.ClientID %>").attr('src', 'about:blank');
            $("#<%=popupSiblingPay.ClientID %>").css("display", "none");
            return false;
        }
    </script>

    <asp:HiddenField ID="h_Sel_Bsu_Id" runat="server" />
    <asp:HiddenField ID="h_Designations" runat="server" />
    <asp:HiddenField ID="hdn_DRI_ID" runat="server" />
    <asp:HiddenField ID="hdn_ColDataTypes" runat="server" />
</asp:Content>
