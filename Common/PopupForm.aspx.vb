Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj

Partial Class SelBussinessUnit
    Inherits BasePage
    'Shared Session("liUserList") As List(Of String)
    Shared multiSel As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If

        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If
        multiSel = IIf(Request.QueryString("multiSelect") = "true", True, False)
        Dim strMultiSel As String = Request.QueryString("multiSelect")
        If strMultiSel <> String.Empty And strMultiSel <> "" Then
            If String.Compare("False", strMultiSel, True) = 0 Then
                gvGroup.Columns(0).Visible = False
                gvGroup.Columns(2).Visible = False
                gvGroup.Columns(3).Visible = True
                'DropDownList1.Visible = False
                btnFinish.Visible = False
                chkSelAll.Visible = False
            Else
                gvGroup.Columns(0).Visible = True
                gvGroup.Columns(2).Visible = True
                gvGroup.Columns(3).Visible = False
                'DropDownList1.Visible = True
                btnFinish.Visible = True
            End If
        Else
            multiSel = True
            gvGroup.Columns(0).Visible = True
            gvGroup.Columns(2).Visible = True
            gvGroup.Columns(3).Visible = False
            'DropDownList1.Visible = True
            btnFinish.Visible = True
        End If
        If Page.IsPostBack = False Then

            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"



            Session("liUserList") = New List(Of String)

            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
            gvGroup.Attributes.Add("bordercolor", "#1b80b6")
        End If
        For Each gvr As GridViewRow In gvGroup.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        ' reg_clientScript()
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvGroup_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvGroup.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblSTU_ID As Label = DirectCast(e.Row.FindControl("Label1"), Label)
            'Dim lblCode As Label = DirectCast(e.Row.FindControl("lblCode"), Label)
            Dim linklblBSUName As LinkButton = DirectCast(e.Row.FindControl("linklblBSUName"), LinkButton)

            If Not linklblBSUName Is Nothing AndAlso Not lblSTU_ID Is Nothing Then
                'hf_STUID.Value = lblCode.Text
                If Request.QueryString("ID") = "COMPANY" Then

                    '    lblSTU_ID.Text = lblSTU_ID.Text & "___" & linklblBSUName.Text.Replace("'", "\'") & ""
                    '    linklblBSUName.Attributes.Add("onClick", "return SetValuetoCompanyParent('" & lblSTU_ID.Text & "','" & Request.QueryString("ID") & "');")
                    'ElseIf Request.QueryString("type") = "CONCESSION" Then
                    '    lblSTU_ID.Text = lblSTU_ID.Text & "||" & lbCodeSubmit.Text & "||" & lblCode.Text & ""
                    '    lbCodeSubmit.Attributes.Add("onClick", "return SetValuetoCancelConParent('" & lblSTU_ID.Text & "');")
                    'ElseIf Request.QueryString("type") = "ALL_SOURCE_STUDENTS" Then
                    '    lblSTU_ID.Text = lblSTU_ID.Text & "||" & lbCodeSubmit.Text & "||" & lblCode.Text & ""
                    '    lbCodeSubmit.Attributes.Add("onClick", "return SetAllStudentsValue('" & lblSTU_ID.Text & "');")
                    'Else
                    '    If Request.QueryString("Mnu_code") = "F300148" Then

                    '        lblSTU_ID.Text = lblSTU_ID.Text & "||" & lbCodeSubmit.Text & "||" & lblCode.Text & ""
                    '        lbCodeSubmit.Attributes.Add("onClick", "return SetStudentValuetoParent('" & lblSTU_ID.Text & "');")
                    '    Else
                    '        lblSTU_ID.Text = lblSTU_ID.Text & "||" & lbCodeSubmit.Text & "||" & lblCode.Text & ""
                    '        lbCodeSubmit.Attributes.Add("onClick", "return SetValuetoParent('" & lblSTU_ID.Text & "');")
                    '    End If
                End If

            End If
        End If
    End Sub

    Sub GridBindCompany()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_orderby As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_filter_code = ""
            str_filter_name = ""
            Dim qBSU_ID As String = IIf(Request.QueryString("BSU_ID") Is Nothing, "", Request.QueryString("BSU_ID"))
            Dim ACY_ID As String = IIf(Request.QueryString("ACY_ID") Is Nothing, "", Request.QueryString("ACY_ID"))

            str_query_header = "SELECT COMP_ID as ID, COMP_NAME AS DESCR FROM COMP_LISTED_M where 1=1 " & _
            "|" & "COMPANY ID|COMPANY DESCRIPTION"
            str_orderby = " ORDER BY COMP_NAME"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("COMP_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("COMP_NAME", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & str_orderby
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                Dim drRow As DataRow = ds.Tables(0).NewRow
                drRow("ID") = "-1"
                drRow("DESCR") = "--"
                ds.Tables(0).Rows.Add(drRow)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            'If Request.QueryString("ccsmode") <> "others" Then

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'Else
            'For j As Integer = 3 To gvGroup.Columns.Count - 1
            '    gvGroup.Columns(j).Visible = False
            'Next
            'lblheader = gvGroup.HeaderRow.FindControl("lblId")
            'lblheader.Text = "PROCESS ID"
            'lblheader = gvGroup.HeaderRow.FindControl("lblName")
            'lblheader.Text = "PROCESS DESCRIPTION"
            'End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindProcesses()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim qBSU_ID As String = IIf(Request.QueryString("BSU_ID") Is Nothing, "", Request.QueryString("BSU_ID"))
            Dim ACD_ID As String = IIf(Request.QueryString("ACD_ID") Is Nothing, "", Request.QueryString("ACD_ID"))

            str_query_header = "SELECT PROCESSFO_SYS_M.PRO_DESCRIPTION as DESCR, PROCESSFO_BSU_M.PRB_ID as ID  " & _
            " FROM PROCESSFO_BSU_M LEFT OUTER JOIN " & _
            " PROCESSFO_SYS_M ON PROCESSFO_BSU_M.PRB_STG_ID = PROCESSFO_SYS_M.PRO_ID " & _
            " WHERE PRB_BSU_ID ='" & qBSU_ID & "' AND PRB_ACD_ID = '" & ACD_ID & "' AND PRB_bREQUIRED = 1" & _
            "|" & "PROCESS ID|PROCESS DESCRIPTION"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("PRB_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("PRO_DESCRIPTION", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            'If Request.QueryString("ccsmode") <> "others" Then

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'Else
            'For j As Integer = 3 To gvGroup.Columns.Count - 1
            '    gvGroup.Columns(j).Visible = False
            'Next
            'lblheader = gvGroup.HeaderRow.FindControl("lblId")
            'lblheader.Text = "PROCESS ID"
            'lblheader = gvGroup.HeaderRow.FindControl("lblName")
            'lblheader.Text = "PROCESS DESCRIPTION"
            'End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub gridbindBSUnit()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_mode As String
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_BSUName As String = String.Empty
            Dim BUnitreaderSuper As SqlDataReader
            str_filter_code = ""
            str_filter_name = ""
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")

            Dim bGetAll As Boolean = IIf(Request.QueryString("getall") = "true", True, False)
            If bGetAll Then
                str_query_header = "select BSU_ID as ID,BSU_NAME as DESCR from BUSINESSUNIT_M where 1=1 " & "|" & "BSU ID|BSU Name"
            Else
                str_mode = ""

                str_txtCode = ""
                str_txtName = ""
                str_BSUName = ""
                If tblbUSuper = True Then
                    BUnitreaderSuper = AccessRoleUser.GetBusinessUnits()
                Else
                    BUnitreaderSuper = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                End If

                Dim strAccessibleBSUIDs As String = String.Empty

                If BUnitreaderSuper.HasRows = True Then
                    Dim bBUnitreaderSuper As Boolean = BUnitreaderSuper.Read()
                    While (bBUnitreaderSuper)
                        strAccessibleBSUIDs += "'" + BUnitreaderSuper(0) + "'"
                        bBUnitreaderSuper = BUnitreaderSuper.Read()
                        If bBUnitreaderSuper Then
                            strAccessibleBSUIDs += ","
                        End If

                    End While
                End If
                str_query_header = "select BSU_ID as ID,BSU_NAME as DESCR from BUSINESSUNIT_M where BSU_ID in(" & strAccessibleBSUIDs & ")" & "|" & "BSU ID|BSU Name"
            End If
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("BSU_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("BSU_NAME", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindCAT()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
             str_query_header = "SELECT ECT_ID as ID, ECT_DESCR as DESCR FROM EMPCATEGORY_M where 1=1 " & "|" & "CAT ID|CAT Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ECT_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ECT_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindEMP()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_filter_code = ""
            str_filter_name = ""
            str_query_header = "select ID, DESCR from (select cast(Replace(empno,'" & Session("sBSUID") & _
            "','') as numeric) as ID , EMP_FNAME + ' ' + EMP_MNAME + ' ' + EMP_LNAME as DESCR from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND") & ") a"

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("a.ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("a.DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & " WHERE 1=1 " & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            For j As Integer = 3 To gvGroup.Columns.Count - 1
                gvGroup.Columns(j).Visible = False
            Next
            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "NAME"
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindUsers()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_filter_code = ""
            str_filter_name = ""
            str_query_header = "SELECT ID, DESCR FROM (select USR_ID ID, USR_NAME DESCR from USERS_M)a"

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("a.ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("a.DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & " WHERE 1=1 " & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                gvGroup.Columns(3).Visible = False
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                gvGroup.Columns(2).Visible = False
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = "ID"
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = "NAME"
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridbindDOCTYPE()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_filter_code = ""
            str_filter_name = ""
            str_query_header = "SELECT ESD_ID as ID, ESD_DESCR as DESCR FROM EMPSTATDOCUMENTS_M WHERE ESD_bCHKEXPIRY =1 " & "|" & "DOC ID|DOC Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("ESD_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("ESD_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindDEPT()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_filter_code = ""
            str_filter_name = ""
            str_query_header = "SELECT DPT_ID as ID, DPT_DESCR as DESCR FROM DEPARTMENT_M where 1=1 " & "|" & "DEPT ID|DEPT Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("DPT_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DPT_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindDESG()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_query_header = "SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M where DES_FLAG = 'ML' " & "|" & "DEPT ID|DEPT Name"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("DES_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("DES_DESCR", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindPDC()

        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_query_header = "SELECT VHS_ID as ID, VHS_DESCRIPTION as DESCR  FROM  VOUCHERSETUP_S WHERE VHS_DOCTYPE='BP'" & "|" & " Setting #|Setting "
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)

            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim

                str_filter_code = set_search_filter("VHS_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim

                str_filter_name = set_search_filter("VHS_DESCRIPTION", str_Sid_search(0), str_txtName)

            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindUser()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
          
            Dim qBSU_ID As String = IIf(Request.QueryString("BSU_ID") Is Nothing, "", Request.QueryString("BSU_ID"))

            str_query_header = "SELECT USERS_M.USR_ID AS ID, USERS_M.USR_NAME AS DESCR, BUSINESSUNIT_M.BSU_NAME, " & _
            " BUSINESSUNIT_M.BSU_ID FROM BUSINESSUNIT_M RIGHT OUTER JOIN" & _
            " USERACCESS_S ON BUSINESSUNIT_M.BSU_ID = USERACCESS_S.USA_BSU_ID RIGHT OUTER JOIN" & _
            " USERS_M ON USERACCESS_S.USA_USR_ID = USERS_M.USR_ID " & _
            " WHERE BSU_ID ='" & qBSU_ID & "'"
            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("USR_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            'If Request.QueryString("ccsmode") <> "others" Then

            lblheader = gvGroup.HeaderRow.FindControl("lblId")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblName")
            lblheader.Text = str_headers(2)
            'Else
            'For j As Integer = 3 To gvGroup.Columns.Count - 1
            '    gvGroup.Columns(j).Visible = False
            'Next
            'lblheader = gvGroup.HeaderRow.FindControl("lblId")
            'lblheader.Text = "PROCESS ID"
            'lblheader = gvGroup.HeaderRow.FindControl("lblName")
            'lblheader.Text = "PROCESS DESCRIPTION"
            'End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindFEE_TRANS_User()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty 
            str_query_header = "select DISTINCT FCL_EMP_ID AS ID, FCL_EMP_ID AS DESCR from FEES.FEECOLLECTION_H WHERE 1 =1 "
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("FCL_EMP_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")

                If multiSel Then
                    str_Sid_search = h_selected_menu_2.Value.Split("__")
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    str_Sid_search = h_Selected_menu_3.Value.Split("__")
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("FCL_EMP_ID", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & " ORDER BY FCL_EMP_ID "
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            'If Request.QueryString("ccsmode") <> "others" Then

            'Else
            'For j As Integer = 3 To gvGroup.Columns.Count - 1
            '    gvGroup.Columns(j).Visible = False
            'Next
            'lblheader = gvGroup.HeaderRow.FindControl("lblId")
            'lblheader.Text = "PROCESS ID"
            'lblheader = gvGroup.HeaderRow.FindControl("lblName")
            'lblheader.Text = "PROCESS DESCRIPTION"
            'End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindFEE_RPT_User()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty 
            str_query_header = "select DISTINCT FCL_EMP_ID AS ID, FCL_EMP_ID AS DESCR from FEES.FEECOLLECTION_H WHERE 1 =1 "
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("FCL_EMP_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")

                If multiSel Then
                    str_Sid_search = h_selected_menu_2.Value.Split("__")
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    str_Sid_search = h_Selected_menu_3.Value.Split("__")
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("FCL_EMP_ID", str_Sid_search(0), str_txtName)
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & " ORDER BY FCL_EMP_ID"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            'If Request.QueryString("ccsmode") <> "others" Then

            'Else
            'For j As Integer = 3 To gvGroup.Columns.Count - 1
            '    gvGroup.Columns(j).Visible = False
            'Next
            'lblheader = gvGroup.HeaderRow.FindControl("lblId")
            'lblheader.Text = "PROCESS ID"
            'lblheader = gvGroup.HeaderRow.FindControl("lblName")
            'lblheader.Text = "PROCESS DESCRIPTION"
            'End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindFEECounter()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            str_filter_code = ""
            str_filter_name = ""
            Dim qBSU_ID As String = IIf(Request.QueryString("BSU_ID") Is Nothing, "", Request.QueryString("BSU_ID"))
            Dim qGETALL As Boolean = IIf(Request.QueryString("GETALL") Is Nothing, False, Request.QueryString("GETALL"))

            If qGETALL Then
                'str_query_header = "SELECT DISTINCT FEES.FEECOUNTER_M.FCM_ID as ID, FEES.FEECOUNTER_M.FCM_DESCR AS DESCR " & _
                '" FROM FEES.FEECOUNTER_M "
                str_query_header = "SELECT DISTINCT FEES.FEECOUNTER_M.FCM_ID AS ID, " & _
                "BUSINESSUNIT_M.BSU_NAME + '-' + FEES.FEECOUNTER_M.FCM_DESCR AS DESCR " & _
                "FROM FEES.FEECOUNTER_M " & _
                "INNER JOIN BUSINESSUNIT_M ON " & _
                "FEES.FEECOUNTER_M.FCM_BSU_ID = BUSINESSUNIT_M.BSU_ID"
            Else
                str_query_header = "SELECT FEES.FEECOUNTER_M.FCM_ID as ID, FEES.FEECOUNTER_M.FCM_DESCR AS DESCR " & _
                " FROM FEES.FEECOUNTER_M INNER JOIN " & _
                "BUSINESSUNIT_M ON FEES.FEECOUNTER_M.FCM_BSU_ID = BUSINESSUNIT_M.BSU_ID  " & _
                "LEFT OUTER JOIN USERS_M ON FEES.FEECOUNTER_M.FCM_ID = USERS_M.USR_FCM_ID  " & _
                "WHERE(USR_NAME Is null) "
            End If
            str_Sql = str_query_header.Split("||")(0)
            'str_Sql = str_Sql.Replace("###", Session("sBSUID"))
            Dim lblheader As New Label
            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("FCM_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If

            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            'If Request.QueryString("ccsmode") <> "others" Then

            'Else
            'For j As Integer = 3 To gvGroup.Columns.Count - 1
            '    gvGroup.Columns(j).Visible = False
            'Next
            'lblheader = gvGroup.HeaderRow.FindControl("lblId")
            'lblheader.Text = "PROCESS ID"
            'lblheader = gvGroup.HeaderRow.FindControl("lblName")
            'lblheader.Text = "PROCESS DESCRIPTION"
            'End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindLOCATION()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_mode As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_BSUName As String = String.Empty
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")

            str_query_header = "SELECT DISTINCT SBL.SBL_DESCRIPTION AS DESCR, RTM.RTm_BSU_ID, " _
                    & " RTM.RTm_AMOUNT, LOC.LOC_DESCRIPTION, SBL.SBL_ID AS ID" _
                    & " FROM TRANSPORT.RATES_S AS RTS INNER JOIN" _
                    & " TRANSPORT.RATES_M AS RTM ON RTS.RTS_RTM_ID = RTM.RTm_ID INNER JOIN" _
                    & " TRANSPORT.SUBLOCATION_M AS SBL ON RTM.RTm_SBL_ID = SBL.SBL_ID INNER JOIN" _
                    & " TRANSPORT.LOCATION_M AS LOC ON SBL.SBL_LOC_ID = LOC.LOC_ID WHERE RTM.RTm_BSU_ID='###'" _
                    & "|" & "Location ID|Location "
           
            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Request.QueryString("bsu"))
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("SBL_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("SBL_DESCRIPTION", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub GridBindLinkToStage()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_mode As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_BSUName As String = String.Empty
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")

            str_query_header = "SELECT PRO_ID ID, PRO_DESCRIPTION DESCR from PROCESSFO_SYS_M WHERE 1=1 " _
                    & "|" & "Location ID|Location "

            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            str_Sql = str_Sql.Replace("###", Request.QueryString("bsu"))
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("PRO_ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("PRO_DESCRIPTION", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & " ORDER BY PRO_ORDER"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Sub TransdocNo()
        Try
            Dim str_query_header As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim str_Sql As String
            Dim str_filter_code As String = String.Empty
            Dim str_filter_name As String = String.Empty
            Dim str_mode As String = String.Empty
            Dim str_txtCode As String = String.Empty
            Dim str_txtName As String = String.Empty
            Dim str_BSUName As String = String.Empty
            Dim tblbUsr_id As String = Session("sUsr_id")
            Dim tblbUSuper As Boolean = Session("sBusper")
            Dim BSU_ID As String = Request.QueryString("bsu")
            Dim FromDt As String = Request.QueryString("FRMDT")
            Dim ToDt As String = Request.QueryString("TODT")

            str_query_header = " SELECT FTA_DESCRIPTION AS ID ,FTA_REF_ID AS DESCR FROM FEES.VW_OSO_FEE_TRNSALL  " _
                              & " WHERE FTA_BSU_ID = '" & BSU_ID & "' " _
                              & " AND FTA_TRANDT BETWEEN '" & FromDt & "' AND '" & ToDt & "'"

            str_Sql = str_query_header.Split("||")(0)

            Dim str_headers As String()
            'str_query_header = str_query_header.Split("||")(2)
            str_headers = str_query_header.Split("|")
            'str_Sql = str_Sql.Replace("###", Request.QueryString("bsu"))
            Dim lblheader As New Label

            Dim txtSearch As New TextBox
            ''''''''''
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("FTA_DESCRIPTION", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_selected_menu_2.Value.Split("__")
                If multiSel Then
                    txtSearch = gvGroup.HeaderRow.FindControl("txtName")
                Else
                    txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
                End If
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("FTA_REF_ID", str_Sid_search(0), str_txtName)

                ''column1
            End If
            Dim ds As New DataSet
            str_Sql = str_Sql & str_filter_code & str_filter_name & " ORDER BY FTA_DESCRIPTION,FTA_REF_ID"
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                'sp_message.InnerHtml = ""
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            If multiSel Then
                txtSearch = gvGroup.HeaderRow.FindControl("txtName")
            Else
                txtSearch = gvGroup.HeaderRow.FindControl("txtBSUName")
            End If

            txtSearch.Text = str_txtName

            ' For iI As Integer = 3 To str_headers.Length - 1

            If Request.QueryString("ccsmode") <> "others" Then

                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = str_headers(1)
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = str_headers(2)
            Else
                For j As Integer = 3 To gvGroup.Columns.Count - 1
                    gvGroup.Columns(j).Visible = False
                Next
                lblheader = gvGroup.HeaderRow.FindControl("lblId")
                lblheader.Text = "ID"
                lblheader = gvGroup.HeaderRow.FindControl("lblName")
                lblheader.Text = "NAME"
            End If
            set_Menu_Img()
            SetChk(Me.Page)
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

    Private Sub SetChk(ByVal Page As Control)
        Try
            For Each ctrl As Control In Page.Controls
                If TypeOf ctrl Is HtmlInputCheckBox Then
                    Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                    If chk.Checked = True Then
                        'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                        'Response.Write(chk.Value.ToString & "->")
                        If list_add(chk.Value) = False Then
                            chk.Checked = True
                        End If
                    Else
                        If list_exist(chk.Value) = True Then
                            chk.Checked = True
                        End If
                        list_remove(chk.Value)
                    End If
                Else
                    If ctrl.Controls.Count > 0 Then
                        SetChk(ctrl)
                    End If
                End If
            Next
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try

    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID").ToString
            Case "BSU"
                gridbindBSUnit()
            Case "DEPT"
                GridBindDEPT()
            Case "CAT"
                GridBindCAT()
            Case "DESG"
                GridBindDESG()
            Case "EMP"
                GridBindEMP()
            Case "DOCTYPE"
                GridbindDOCTYPE()
            Case "PROCESS"
                GridBindProcesses()
            Case "COMPANY"
                GridBindCompany()
            Case "USER"
                GridBindUser()
            Case "PDC"
                GridBindPDC()
            Case "COUNTER"
                GridBindFEECounter()
            Case "FEE_TRN_USER"
                GridBindFEE_TRANS_User()
            Case "FEE_RPT_USER"
                GridBindFEE_RPT_User()
            Case "LOCATION"
                GridBindLOCATION()
            Case "USERS"
                GridBindUsers()
            Case "LINKTOSTAGE"
                GridBindLinkToStage()
            Case "TRANDOCNO"
                TransdocNo()
        End Select
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        'Response.Write("window.close();")
        'Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")

    End Sub

    Protected Sub gvGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvGroup.Load
        'gvGroup.Columns(2).Visible = False
    End Sub

    Protected Sub linklblBSUName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender

        lblcode = sender.Parent.FindControl("Label1")
        lblcode.Text = lblcode.Text.Replace("___", "||")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & lblcode.Text & "___" & lbClose.Text.Replace("'", "\'") & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")

            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & lblcode.Text & "||" & lbClose.Text.Replace("'", "\'") & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged

        If chkSelAll.Checked Then
            Select Case ViewState("ID").ToString
                Case "BSU"

                    Dim tblbUSuper As Boolean = Session("sBusper")
                    Dim BUnitreaderSuper As SqlDataReader
                    Dim tblbUsr_id As String = Session("sUsr_id")

                    If tblbUSuper = True Then
                        BUnitreaderSuper = AccessRoleUser.GetBusinessUnits()
                    Else
                        BUnitreaderSuper = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                    End If

                    Dim strAccessibleBSUIDs As String = String.Empty

                    If BUnitreaderSuper.HasRows = True Then
                        While (BUnitreaderSuper.Read())
                            Session("liUserList").Remove(BUnitreaderSuper(0))
                            Session("liUserList").Add(BUnitreaderSuper(0))
                        End While
                    End If
                Case "DEPT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DPT_ID as ID FROM DEPARTMENT_M where 1=1"
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DOCTYPE"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ESD_ID FROM EMPSTATDOCUMENTS_M WHERE ESD_bCHKEXPIRY =1 "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "CAT"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT ECT_ID as ID FROM EMPCATEGORY_M "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "DESG"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "SELECT DES_ID as ID FROM EMPDESIGNATION_M "
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
                Case "EMP"
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim str_Sql As String = "select EMP_ID as ID from EMPLOYEE_M where 1=1 " & Session("EMP_SEL_COND")
                    Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    While (dr.Read())
                        Session("liUserList").Remove(dr(0))
                        Session("liUserList").Add(dr(0))
                    End While
            End Select

        Else
            Session("liUserList").Clear()
        End If
        GridBind()
    End Sub

End Class

