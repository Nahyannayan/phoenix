<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopUpEmployeesList.aspx.vb" Inherits="Common_PopupFromIDHiddenSeven" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">                  
    </script>
    <script type="text/javascript">
       function GetRadWindow() {
           var oWindow = null;
           if (window.radWindow) oWindow = window.radWindow;
           else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
           return oWindow;
       }
    </script>
</head>
<body onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                        EmptyDataText="No Data" Width="100%" AllowPaging="True" PageSize="18" >
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescr1" runat="server" Text='<%# Bind("DESCR1") %>' Visible="True"></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDESCR1" runat="server" CssClass="gridheader_text" Text="Employee ID"></asp:Label><br />
                                    <asp:TextBox ID="txtDESCR1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkName" runat="server" CausesValidation="false" CommandName="Selected"
                                        Text='<%# Eval("DESCR2") %>' Visible="true" OnClick="lnkName_Click"></asp:LinkButton>

                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDESCR2" Text="Employee Name" runat="server"
                                        CssClass="gridheader_text"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtDESCR2" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch2_Click" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCR3" runat="server" Text='<%# Bind("DESCR3") %>'></asp:Label><br />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDescr3" runat="server" Text="Old Staff No." CssClass="gridheader_text"></asp:Label><br />
                                    <asp:TextBox ID="txtDESCR3" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDESCR4" Text="Category" runat="server" CssClass="gridheader_text"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtDescr4" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCR4" runat="server" Text='<%# Bind("DESCR4") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDESCR5" Text="Designation" runat="server" CssClass="gridheader_text"></asp:Label><br />
                                    <asp:TextBox ID="txtDescr5" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDESCR5" runat="server" Text='<%# Bind("DESCR5") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHDESCR6" Text="Emp ID" runat="server"
                                        CssClass="gridheader_text"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpID" runat="server" Text='<%# Bind("DESCR6") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </td>
            </tr>
            <tr>
                <td align="center">

                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="" /></td>
            </tr>
        </table>

    </form>
</body>
</html>
