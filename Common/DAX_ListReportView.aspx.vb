Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports GridViewHelper
Imports UtilityObj
Imports System.Xml
Imports System.Xml.Xsl
Imports GemBox.Spreadsheet
Imports Lesnikowski.Barcode
Partial Class Common_DAX_ListReportView
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Private Property ConnectionString() As String
        Get
            Return ViewState("ConnectionString")
        End Get
        Set(ByVal value As String)
            ViewState("ConnectionString") = value
        End Set
    End Property
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property FilterString() As String
        Get
            Return ViewState("FilterString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property SQLSelect() As String
        Get
            Return ViewState("SQLSelect")
        End Get
        Set(ByVal value As String)
            ViewState("SQLSelect") = value
        End Set
    End Property
    Private Property SortColString() As String
        Get
            Return ViewState("SortColString")
        End Get
        Set(ByVal value As String)
            ViewState("SortColString") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property FilterTextboxStringSP() As String
        Get
            Return ViewState("FilterTextboxStringSP")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxStringSP") = value
        End Set
    End Property
    Private Property EditPagePath() As String
        Get
            Return ViewState("EditPagePath")
        End Get
        Set(ByVal value As String)
            ViewState("EditPagePath") = value
        End Set
    End Property
    Private Property MainMenuCode() As String
        Get
            Return ViewState("MainMenuCode")
        End Get
        Set(ByVal value As String)
            ViewState("MainMenuCode") = value
        End Set
    End Property
    Private Property Datamode() As String
        Get
            Return ViewState("Datamode")
        End Get
        Set(ByVal value As String)
            ViewState("Datamode") = value
        End Set
    End Property
    Private Property HeaderTitle() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            Page.Title = value
            lblTitle.Text = value
        End Set
    End Property
    Private Property ShowGridCheckBox() As Boolean
        Get
            Return ViewState("ShowGridCheckBox")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShowGridCheckBox") = value
        End Set
    End Property
    Private Property NoAddingAllowed() As Boolean
        Get
            Return ViewState("NoAddingAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoAddingAllowed") = value
        End Set
    End Property
    Private Property NoViewAllowed() As Boolean
        Get
            Return ViewState("NoViewAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoViewAllowed") = value
        End Set
    End Property
    Private Property NoPrintAllowed() As Boolean
        Get
            Return ViewState("NoPrintAllowed")
        End Get
        Set(ByVal value As Boolean)
            ViewState("NoPrintAllowed") = value
        End Set
    End Property
    Private Property HideExportButton() As Boolean
        Get
            Return ViewState("HideExportButton")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideExportButton") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Private Property AllRecordsIDs() As String
        Get
            Return ViewState("AllRecordsIDs")
        End Get
        Set(ByVal value As String)
            ViewState("AllRecordsIDs") = value
        End Set
    End Property
    Private Property FilterText1() As String
        Get
            Return chkSelection1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection1.Text = value
                chkSelection1.Visible = True
            Else
                chkSelection1.Visible = False
                chkSelection1.Text = ""
            End If
        End Set
    End Property
    Private Property FilterText2() As String
        Get
            Return chkSelection2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                chkSelection2.Text = value
                chkSelection2.Visible = True
            Else
                chkSelection2.Visible = False
                chkSelection2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox1Text() As String
        Get
            Return rad1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad1.Text = value
                rad1.Visible = True
            Else
                rad1.Visible = False
                rad1.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox2Text() As String
        Get
            Return rad2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad2.Text = value
                rad2.Visible = True
            Else
                rad2.Visible = False
                rad2.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox3Text() As String
        Get
            Return rad3.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad3.Text = value
                rad3.Visible = True
            Else
                rad3.Visible = False
                rad3.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox4Text() As String
        Get
            Return rad4.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad4.Text = value
                rad4.Visible = True
            Else
                rad4.Visible = False
                rad4.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox5Text() As String
        Get
            Return rad5.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad5.Text = value
                rad5.Visible = True
            Else
                rad5.Visible = False
                rad5.Text = ""
            End If
        End Set
    End Property
    Private Property CheckBox6Text() As String
        Get
            Return rad6.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                rad6.Text = value
                rad6.Visible = True
            Else
                rad6.Visible = False
                rad6.Text = ""
            End If
        End Set
    End Property
    Private Property ButtonText1() As String
        Get
            Return ListButton1.Value
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton1.Value = value
                ListButton1.Visible = True
            Else
                ListButton1.Visible = False
                ListButton1.Value = ""
            End If
        End Set
    End Property
    Private Property ButtonText2() As String
        Get
            Return ListButton2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                ListButton2.Text = value
                ListButton2.Visible = True
            Else
                ListButton2.Visible = False
                ListButton2.Text = ""
            End If
        End Set
    End Property
    Private Property DropDownTitle1() As String
        Get
            Return lblDDDescr1.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                lblDDDescr1.Text = value
                lblDDDescr1.Visible = True
                DropDown1.Visible = True
            Else
                lblDDDescr1.Visible = False
                DropDown1.Visible = False
                lblDDDescr1.Text = ""
            End If
        End Set
    End Property
    Private Property DropDownTitle2() As String
        Get
            Return lblDDDescr2.Text
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                lblDDDescr2.Text = value
                lblDDDescr2.Visible = True
                DropDown2.Visible = True
            Else
                lblDDDescr2.Visible = False
                DropDown2.Visible = False
                lblDDDescr2.Text = ""
            End If
        End Set
    End Property

    Private Property PrintOnLandscape() As Boolean
        Get
            Return IIf(hPrintOnLandscape.Value = "1", True, False)
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                hPrintOnLandscape.Value = "1"
            Else
                hPrintOnLandscape.Value = "0"
            End If
        End Set
    End Property


    Private WriteOnly Property ShowDateRange() As Boolean
        Set(ByVal value As Boolean)
            trDateRange.Visible = value
        End Set
    End Property
    Private WriteOnly Property ShowBSUDD() As Boolean
        Set(ByVal value As Boolean)
            lblBSUDD.Visible = value
            ddlBSU.Visible = value
            If value Then
                FillBSU()
            End If

        End Set
    End Property

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Session("sModule") = "SS" Then
                Me.MasterPageFile = "../mainMasterPageSS.master"
            Else
                Me.MasterPageFile = "../mainMasterPage.master"
            End If
        Catch ex As Exception

        End Try
    End Sub
    ' Dim SPParam(10) As SqlParameter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                gvDetails.Attributes.Add("bordercolor", "#1b80b6")
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"
                lblError.Text = ""
                MainMenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                If Request.QueryString("datamode") <> "" Then
                    Datamode = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                End If
                Dim msgText As String
                If Not Request.QueryString("msgText") Is Nothing And Request.QueryString("msgText") <> "" Then
                    msgText = Encr_decrData.Decrypt(Request.QueryString("msgText").Replace(" ", "+"))
                    lblError.Text = msgText
                End If
                ShowBSUDD = False
                ShowDateRange = False
                ShowGridCheckBox = False
                NoAddingAllowed = False
                NoViewAllowed = False
                HideExportButton = False
                IsStoredProcedure = False
                ButtonText1 = ""
                ButtonText2 = ""
                rad1.Checked = True
                CheckBox1Text = ""
                CheckBox2Text = ""
                CheckBox3Text = ""
                CheckBox4Text = ""
                CheckBox5Text = ""
                CheckBox6Text = ""
                FilterText1 = ""
                FilterText2 = ""
                txtFromDT.Text = Format(Now.Date.AddDays(-1), OASISConstants.DateFormat)
                txtToDt.Text = Format(Now.Date, OASISConstants.DateFormat)

                BuildListView(True)
                gridbind(False)
                btnExport.Visible = Not HideExportButton
                If NoAddingAllowed Then
                    hlAddNew.Visible = False
                Else
                    hlAddNew.Visible = True
                    Dim url As String
                    url = EditPagePath & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                    hlAddNew.NavigateUrl = url
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(cast([" & pField & "] as varchar),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function

    Private Sub ClearHeaderStrings(ByVal GridViewHeaderRow As GridViewRow)
        Try
            Dim txtSearch As New TextBox
            Dim DT As New DataTable
            DT = ViewState("GridTable")
            If DT Is Nothing Then Exit Sub
            Dim MyCols As DataColumn, ControlName, ColumnName As String
            Dim i As Int16
            i = 1
            FilterTextboxStringSP = ""
            For Each MyCols In DT.Columns
                ControlName = "txtCol" & i.ToString
                ColumnName = ViewState("GridTable").Columns(i - 1).ColumnName
                txtSearch = GridViewHeaderRow.FindControl(ControlName)
                i = i + 1
                If txtSearch Is Nothing Then Continue For
                txtSearch.Text = ""
            Next
        Catch ex As Exception
        End Try
    End Sub


    Private Function getColumnFilterStringForSP(ByVal GridViewHeaderRow As GridViewRow) As String
        Try
            Dim txtSearch As New TextBox
            Dim DT As New DataTable
            DT = ViewState("GridTable")
            If DT Is Nothing Then Return ""
            Dim MyCols As DataColumn, ControlName, ColumnName As String
            Dim i As Int16
            i = 1
            FilterTextboxStringSP = ""
            For Each MyCols In DT.Columns
                ControlName = "txtCol" & i.ToString
                ColumnName = ViewState("GridTable").Columns(i - 1).ColumnName
                txtSearch = GridViewHeaderRow.FindControl(ControlName)
                i = i + 1
                If txtSearch Is Nothing Then Continue For
                Dim str_Filter As String = ""
                FilterTextboxStringSP &= ColumnName & "=" & txtSearch.Text & "||"
            Next
            getColumnFilterStringForSP = FilterTextboxStringSP
        Catch ex As Exception
            getColumnFilterStringForSP = ""
        End Try
    End Function

    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Sub gridbind(ByVal NoReset As Boolean, Optional ByVal ForceResetColumns As Boolean = False)
        Try
            Dim GridData As New DataTable
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String

            FilterTextboxString = ""
            If gvDetails.Rows.Count > 0 And Not ForceResetColumns Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If SortColString <> "" Then
                OrderByStr = "order by " & SortColString
            Else
                OrderByStr = "order by  1"
            End If

            If ddlFilter.Visible Then
                If ddlFilter.SelectedItem Is Nothing Then
                    SQLSelect = SQLSelect.Replace("~", " top 10 ")
                ElseIf ddlFilter.SelectedItem.Value = "All" Then
                    SQLSelect = SQLSelect.Replace("~", " top 99999999 ")
                Else
                    SQLSelect = SQLSelect.Replace("~", " top " & ddlFilter.SelectedItem.Value)
                End If
            End If
            If Not IsStoredProcedure Then
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQLSelect & str_Filter & OrderByStr).Tables(0)
            Else
                GridData = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, SQLSelect, CType(SPParam, SqlParameter())).Tables(0)
                str_Filter = " 1 = 1 " & str_Filter
            End If

            Dim bindDT, myData As New DataTable
            bindDT = GridData.Copy
            myData = GridData.Copy

            Try
                Dim mRow As DataRow
                AllRecordsIDs = ""
                If myData.Columns.Count > 0 Then
                    For Each mRow In myData.Rows
                        AllRecordsIDs &= IIf(AllRecordsIDs <> "", "|", "") & mRow(0).ToString
                    Next
                End If
            Catch ex As Exception

            End Try


            If myData.Columns.Count > 0 Then
                myData.Columns.RemoveAt(0)
            End If
            ViewState("GridTable") = GridData
            Session("myData") = myData
            SetExportFileLink()
            GridData.Rows.Clear()
            Dim HeaderLabel As New Label
            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex
            If bindDT.Columns.Count < 10 Then
                For i = 0 To bindDT.Columns.Count
                    gvDetails.Columns(i).Visible = True
                Next
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If
            gvDetails.Columns(0).Visible = ShowGridCheckBox
            gvDetails.Columns(11).Visible = Not NoAddingAllowed
            gvDetails.Columns(1).Visible = False
            gvDetails.Columns(11).Visible = Not NoViewAllowed
            gvDetails.Columns(12).Visible = Not NoPrintAllowed
            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If

            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            SetHeaderFilterString()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim lblItemColID As Label
            lblItemColID = TryCast(e.Row.FindControl("lblItemCol1"), Label)
            Dim cmdCol As Integer = gvDetails.Columns.Count - 1
            Dim hlview As New HyperLink
            hlview = TryCast(e.Row.FindControl("hlView"), HyperLink)
            If hlview IsNot Nothing And lblItemColID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                hlview.NavigateUrl = EditPagePath & "?viewid=" & Encr_decrData.Encrypt(lblItemColID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            End If
            Try
                Dim FloatColIndexArr(), i, ControlName As String
                FloatColIndexArr = FloatColIndex.Split("|")
                Dim lblItemCol As Label
                For Each i In FloatColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsNumeric(lblItemCol.Text) Then
                                lblItemCol.Text = Convert.ToDouble(lblItemCol.Text).ToString("####0.00")
                            End If
                        End If
                    End If
                Next
                Dim DateColIndexArr() As String
                DateColIndexArr = DateColIndex.Split("|")
                For Each i In DateColIndexArr
                    If IsNumeric(i) Then
                        ControlName = "lblItemCol" & i.ToString
                        lblItemCol = TryCast(e.Row.FindControl(ControlName), Label)
                        If Not lblItemCol Is Nothing Then
                            If IsDate(lblItemCol.Text) Then
                                lblItemCol.Text = CDate(lblItemCol.Text).ToString("dd/MMM/yyyy")
                            End If
                        End If
                    End If
                Next

            Catch ex As Exception

            End Try
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsStoredProcedure Then
            getColumnFilterStringForSP(gvDetails.HeaderRow)
            BuildListView(False)
        End If
        gridbind(False)
    End Sub
    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        RememberOldValues()
        gvDetails.PageIndex = e.NewPageIndex
        gridbind(True)
        SetChk(Me.Page)
        RePopulateValues()

    End Sub
    Private Sub SetExportFileLink()
        Try
            btnExport.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("XLSDATA") & "&TITLE=" & Encr_decrData.Encrypt(lblTitle.Text)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub rad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rad1.CheckedChanged, rad2.CheckedChanged, rad3.CheckedChanged, rad4.CheckedChanged, rad5.CheckedChanged, rad6.CheckedChanged
        Try
            BuildListView(False, False)
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub chkSelection_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelection1.CheckedChanged, chkSelection2.CheckedChanged
        Try
            RememberOldValues()
            BuildListView(False, False)
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Dim MySelectedIDS() As String
            Dim itmIdList As New ArrayList()
            If chkSelectAll.Checked Then
                MySelectedIDS = AllRecordsIDs.Split("|")
                If MySelectedIDS.Length > 0 Then
                    Dim MySelectedID As String
                    For Each MySelectedID In MySelectedIDS
                        If Not itmIdList.Contains(MySelectedID) Then
                            itmIdList.Add(MySelectedID)
                        End If
                    Next
                End If
            End If
            ViewState("checked_items") = itmIdList
            Dim chkControl As New HtmlInputCheckBox
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                chkControl.Checked = chkSelectAll.Checked
            Next
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Sub
    Sub FillBSU()
        Dim sql As String
        Dim dtTree As New DataTable
        sql = "SP_GET_BSU_FOR_GEN_REPORTS"
        Dim sqlParam(5) As SqlParameter
        sqlParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
        sqlParam(1) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
        sqlParam(2) = Mainclass.CreateSqlParameter("@MODULE_ID", Session("sModule"), SqlDbType.VarChar)
        sqlParam(3) = Mainclass.CreateSqlParameter("@MNU_CODE", MainMenuCode, SqlDbType.VarChar)
        sqlParam(4) = Mainclass.CreateSqlParameter("@TREEVIEW", False, SqlDbType.Bit)
        sqlParam(5) = Mainclass.CreateSqlParameter("@SHOW_ONLY_AX_BSU", True, SqlDbType.Bit)
        dtTree = Mainclass.getDataTable(sql, sqlParam, ConnectionManger.GetOASISConnectionString)

        ddlBSU.DataSource = dtTree
        ddlBSU.DataTextField = "NAME"
        ddlBSU.DataValueField = "ID"
        ddlBSU.DataBind()
        If ddlBSU.Items.Count > 0 Then
            ddlBSU.SelectedValue = Session("sBsuid")
        End If

    End Sub
    Protected Sub ListButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""

        Dim SelectedIds As String = ""
        RememberOldValues()
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If

        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl IsNot Nothing Then
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                    If Not ("|" & SelectedIds.ToString & "|").ToString.Contains("|" & lblID.Text.ToString & "|") Then
                        SelectedIds &= IIf(SelectedIds <> "", "|", "") & lblID.Text
                    End If
                End If
            End If
        Next
        If IDs = "" And SelectedIds = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "A950025"
                If DropDown2.SelectedValue.ToString.ToUpper = "PENDING" Or DropDown2.SelectedValue.ToString.ToUpper = "CANCELLED" Then
                    PostToOASISStaging(SelectedIds)
                ElseIf DropDown2.SelectedValue.ToString.ToUpper = "POSTED" Then
                    ReverseThePostedJournalInStaging(SelectedIds)
                End If
        End Select
        ViewState("checked_items") = Nothing
    End Sub
    Protected Sub ListButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListButton2.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As Label
        Dim IDs As String = ""
        For Each grow As GridViewRow In gvDetails.Rows
            chkControl = grow.FindControl("chkControl")
            If chkControl.Checked Then
                lblID = TryCast(grow.FindControl("lblItemCol1"), Label)
                IDs &= IIf(IDs <> "", "|", "") & lblID.Text
            End If
        Next

        Dim SelectedIds As String = ""
        RememberOldValues()
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then SelectedIds &= IIf(SelectedIds <> "", "|", "") & ViewState("checked_items")(i).ToString
            Next
        End If


        If IDs = "" Then
            lblError.Text = "No Item Selected !!!"
            Exit Sub
        End If
        Select Case MainMenuCode
            Case "xxxxxxx"
                IDs &= IIf(IDs <> "", "|", "") & SelectedIds
                ' PrintPasswords(IDs)
        End Select
    End Sub

    Private Sub PostToOASISStaging(IDs As String)
        Dim objConn As New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            lblError.Text = ""
            Dim iParms(6) As SqlClient.SqlParameter
            Dim ERROR_MESSAGE As String = ""
            iParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
            iParms(2) = Mainclass.CreateSqlParameter("@DOC_NOS", IDs, SqlDbType.VarChar)
            iParms(3) = Mainclass.CreateSqlParameter("@REPOSTING_ERROR", True, SqlDbType.VarChar)
            iParms(4) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            iParms(5) = Mainclass.CreateSqlParameter("@ERROR_MESSAGE", ERROR_MESSAGE, SqlDbType.VarChar, True, 5000)
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "FIN.SP_OASIS_POST_TO_STAGING", iParms)
            ERROR_MESSAGE = iParms(5).Value
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                If ERROR_MESSAGE <> "" Then
                    lblError.Text = ERROR_MESSAGE
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    BuildListView()
                    gridbind(False)
                    lblError.Text = "Data successfully updated!!!"
                End If
            End If

        Catch ex As Exception
            stTrans.Rollback()
        End Try
    End Sub
    Private Sub ReverseThePostedJournalInStaging(IDs As String)
        Dim objConn As New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
        Try
            lblError.Text = ""
            Dim iParms(6) As SqlClient.SqlParameter
            Dim ERROR_MESSAGE As String = ""

            iParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
            iParms(2) = Mainclass.CreateSqlParameter("@DOC_NOS", IDs, SqlDbType.VarChar)
            iParms(3) = Mainclass.CreateSqlParameter("@REPOSTING_ERROR", True, SqlDbType.VarChar)
            iParms(4) = Mainclass.CreateSqlParameter("@USR_NAME", Session("sUsr_name"), SqlDbType.VarChar)
            iParms(5) = Mainclass.CreateSqlParameter("@ERROR_MESSAGE", ERROR_MESSAGE, SqlDbType.VarChar, True, 5000)
            Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "FIN.SP_OASIS_REVERSE_POSTED_JOURNALS", iParms)
            ERROR_MESSAGE = iParms(5).Value
            If RetVal = "-1" Then
                lblError.Text = "Unexpected Error !!!"
                stTrans.Rollback()
                Exit Sub
            Else
                If ERROR_MESSAGE <> "" Then
                    lblError.Text = ERROR_MESSAGE
                    stTrans.Rollback()
                Else
                    stTrans.Commit()
                    BuildListView()
                    gridbind(False)
                    lblError.Text = "Data successfully updated!!!"
                End If
            End If

        Catch ex As Exception
            stTrans.Rollback()
        End Try
    End Sub
    Private Sub BuildListView(Optional ByVal InitialLoad As Boolean = False, Optional ByVal FullReset As Boolean = False)
        Try
            Dim strConn, StrSQL, StrSortCol As String
            strConn = "" : StrSQL = "" : StrSortCol = ""
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Request.QueryString("id")
            If FullReset Then
                FilterTextboxString = ""
                FilterTextboxStringSP = ""
                ClearHeaderStrings(gvDetails.HeaderRow)
            End If
            Select Case MainMenuCode
                Case "A950005" 'Financial Transaction
                    If InitialLoad Then
                        HeaderTitle = Mainclass.GetMenuCaption(MainMenuCode)
                        DropDownTitle1 = "Source"
                        FillIntegrationTranType(DropDown1)
                        FillStatusDropDown(DropDown2)
                        NoAddingAllowed = True
                        NoViewAllowed = True
                        ShowBSUDD = True
                        ShowDateRange = True
                    End If
                    'DropDownTitle2 = IIf(rad1.Checked, "Posting Status (Staging)", "Posting Status (DAX)")
                    FilterText1 = "Show Account Postings in Report"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
                    DropDownTitle2 = "Posting Status (AX)"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
                    ShowDateRange = True
                    IsStoredProcedure = True
                    StrSQL = "FIN.SP_OASIS_FIN_VOUCHER_VIEW"
                    ReDim SPParam(5)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@TRAN_TYPE", DropDown1.SelectedValue.ToString, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", DropDown2.SelectedValue.ToString, SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@FILTERSTRING", FilterTextboxStringSP, SqlDbType.VarChar)
                    SPParam(4) = Mainclass.CreateSqlParameter("@FROMDT", txtFromDT.Text, SqlDbType.DateTime)
                    SPParam(5) = Mainclass.CreateSqlParameter("@TODT", txtToDt.Text, SqlDbType.DateTime)
                    PrintOnLandscape = True
                Case "A950015" 'Financial Vouchers (OASIS)
                    If InitialLoad Then
                        HeaderTitle = Mainclass.GetMenuCaption(MainMenuCode)
                        DropDownTitle1 = "Source"
                        FillIntegrationTranType(DropDown1)
                        FillStatusDropDown(DropDown2)
                        NoAddingAllowed = True
                        NoViewAllowed = True
                        ShowDateRange = True
                        DropDown2.Items.FindByValue("ALL").Selected = True
                    End If
                    DropDownTitle2 = "Posting Status (AX)"
                    FilterText1 = IIf(rad1.Checked, "Show Account Postings in Report", "")
                    rad1.AutoPostBack = False
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
                    IsStoredProcedure = True
                    StrSQL = "FIN.SP_OASIS_FIN_VOUCHER_VIEW"
                    ReDim SPParam(5)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@TRAN_TYPE", DropDown1.SelectedValue.ToString, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@STATUS", DropDown2.SelectedValue.ToString, SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@FILTERSTRING", FilterTextboxStringSP, SqlDbType.VarChar)
                    SPParam(4) = Mainclass.CreateSqlParameter("@FROMDT", txtFromDT.Text, SqlDbType.DateTime)
                    SPParam(5) = Mainclass.CreateSqlParameter("@TODT", txtToDt.Text, SqlDbType.DateTime)
                    PrintOnLandscape = False
                Case "A950025" 'Post to OASIS Staging
                    If InitialLoad Then
                        HeaderTitle = Mainclass.GetMenuCaption(MainMenuCode)
                        NoAddingAllowed = True
                        NoViewAllowed = True
                        ShowBSUDD = True
                        ShowGridCheckBox = True
                        ButtonText1 = "Post to OASIS Staging"
                        DropDownTitle2 = "Status"
                        FillStatusDropDown(DropDown2)
                    End If
                    If DropDown2.SelectedValue.ToString.ToUpper = "PENDING" Then
                        ButtonText1 = "Post to OASIS Staging"
                    ElseIf DropDown2.SelectedValue.ToString.ToUpper = "CANCELLED" Then
                        ButtonText1 = "Re Post to OASIS Staging"
                    ElseIf DropDown2.SelectedValue.ToString.ToUpper = "POSTED" Then
                        ButtonText1 = "Cancel the Posted Journal"
                    End If
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
                    IsStoredProcedure = True

                    StrSQL = "FIN.SP_OASIS_POST_TO_STAGING_PENDING"
                    ReDim SPParam(3)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", ddlBSU.SelectedValue, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@FILTERSTRING", FilterTextboxStringSP, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@REPOSTING_ERROR", True, SqlDbType.Bit)
                    SPParam(3) = Mainclass.CreateSqlParameter("@TRAN_TYPE", DropDown2.SelectedValue.ToString, SqlDbType.VarChar)
                    PrintOnLandscape = True

            End Select
            SQLSelect = StrSQL
            ConnectionString = strConn
            SortColString = StrSortCol
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function FillIntegrationTranType(ByVal DD As DropDownList) As DataTable
        Dim sql As String
        sql = "SELECT DISTINCT TRN_TYPE,TRN_TYPE TRN_DESCR FROM DAX.DAX_INTGR_TRANTYPE UNION ALL SELECT 'ALL','ALL'"
        Dim myDT As New DataTable
        myDT = Mainclass.getDataTable(sql, WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString)
        DD.Items.Clear()
        DD.DataSource = myDT
        DD.DataTextField = "TRN_TYPE"
        DD.DataValueField = "TRN_DESCR"
        DD.DataBind()
        DD.Items.FindByValue("ALL").Selected = True
    End Function

    Private Function FillStatusDropDown(ByVal DD As DropDownList) As DataTable
        DD.Items.Clear()
        If MainMenuCode = "A950025" Then
            Dim lstItem(2) As ListItem
            lstItem(0) = New ListItem
            lstItem(0).Value = "PENDING"
            lstItem(0).Text = "PENDING/FAILED"
            DD.Items.Add(lstItem(0))
            lstItem(1) = New ListItem
            lstItem(1).Value = "CANCELLED"
            lstItem(1).Text = "CANCELLED"
            DD.Items.Add(lstItem(1))
            lstItem(2) = New ListItem
            lstItem(2).Value = "POSTED"
            lstItem(2).Text = "POSTED"
            DD.Items.Add(lstItem(2))
            DD.DataBind()
            DD.Items.FindByValue("PENDING").Selected = True
        Else
            Dim lstItem(3) As ListItem
            lstItem(0) = New ListItem
            lstItem(0).Value = "PENDING"
            lstItem(0).Text = "PENDING"
            DD.Items.Add(lstItem(0))
            lstItem(1) = New ListItem
            lstItem(1).Value = "FAILED"
            lstItem(1).Text = "FAILED"
            DD.Items.Add(lstItem(1))
            lstItem(2) = New ListItem
            lstItem(2).Value = "SUCCESS"
            lstItem(2).Text = "SUCCESS"
            DD.Items.Add(lstItem(2))
            lstItem(2) = New ListItem
            lstItem(2).Value = "ALL"
            lstItem(2).Text = "ALL"
            DD.Items.Add(lstItem(2))
            DD.DataBind()
            DD.Items.FindByValue("ALL").Selected = True
        End If
    End Function

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles txtFromDT.TextChanged, txtToDt.TextChanged
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub lblPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case MainMenuCode
            Case "A950005"
                Dim BSU_ID As String = Session("sBsuid")
                Dim Doc_No As String
                Dim lblItemColID As Label
                lblItemColID = TryCast(sender.Parent.parent.FindControl("lblItemCol1"), Label)
                If Not lblItemColID Is Nothing Then
                    Doc_No = lblItemColID.Text
                    'If rad1.Checked Then
                    '    PrintFinanceVouchers(BSU_ID, Doc_No)
                    'ElseIf rad2.Checked Then
                    PrintStagingFinanceVouchers(BSU_ID, Doc_No)
                    'End If
                    h_print.Value = "print"
                End If
            Case "A950015", "A950025"
                Dim BSU_ID As String = Session("sBsuid")
                Dim Doc_No As String
                Dim lblItemColID As Label
                lblItemColID = TryCast(sender.Parent.parent.FindControl("lblItemCol1"), Label)
                If Not lblItemColID Is Nothing Then
                    Doc_No = lblItemColID.Text
                    PrintFinanceVouchers(BSU_ID, Doc_No)
                    h_print.Value = "print"
                End If
        End Select


    End Sub
    Private Function PrintStagingFinanceVouchers(ByVal BSU_ID As String, ByVal Doc_no As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
            Dim Connection As New SqlConnection(str_conn)
            Dim cmd As New SqlCommand
            Dim param(1) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@OASIS_BSU_ID", BSU_ID, SqlDbType.VarChar, False, 100)
            cmd.Parameters.Add(param(0))
            param(1) = Mainclass.CreateSqlParameter("@JRH_ID", Doc_no, SqlDbType.Int, False)
            cmd.Parameters.Add(param(1))
            cmd.Connection = Connection
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FIN.SP_RPT_GetStagingData"
            Dim ds As New DataSet, DocType As String, DocName As String
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.SelectCommand.CommandTimeout = 0
            ds.Clear()
            adpt.Fill(ds)
            Dim repSource As New MyReportClass
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                'DocType = param(2).Value
                'DocName = param(3).Value
                DocType = ""
                DocName = ""
                Dim params As New Hashtable
                params("userName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = False
                params("VoucherName") = DocName
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                Dim formulas As New Hashtable
                repSource.Formulas = formulas

                repSource.VoucherName = DocName
                repSource.IncludeBSUImage = True
                If chkSelection1.Checked Then
                    DocType = "JV"
                End If
                Select Case DocType
                    Case "JV"
                        repSource.ResourceName = "../RPT_Files/DAX_StagingJournalData.rpt"
                        params("reportHeading") = DocName
                    Case Else
                        repSource.ResourceName = "../RPT_Files/DAX_StagingJournalData.rpt"
                        params("reportHeading") = DocName
                End Select
                repSource.Command = cmd
                Session("ReportSource") = repSource
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function

    Private Function PrintFinanceVouchers(ByVal BSU_ID As String, ByVal Doc_no As String) As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_DAXConnectionString").ConnectionString
            Dim Connection As New SqlConnection(str_conn)
            Dim cmd As New SqlCommand
            Dim param(4) As SqlParameter
            param(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar, False, 100)
            cmd.Parameters.Add(param(0))
            param(1) = Mainclass.CreateSqlParameter("@DOC_NO", Doc_no, SqlDbType.VarChar, False, 100)
            cmd.Parameters.Add(param(1))
            param(2) = Mainclass.CreateSqlParameter("@DOC_TYPE", "", SqlDbType.VarChar, True, 100)
            cmd.Parameters.Add(param(2))
            param(3) = Mainclass.CreateSqlParameter("@DOC_NAME", "", SqlDbType.VarChar, True, 100)
            cmd.Parameters.Add(param(3))
            param(4) = Mainclass.CreateSqlParameter("@ShowAccPostings", chkSelection1.Checked, SqlDbType.Bit)
            cmd.Parameters.Add(param(4))
            cmd.Connection = Connection
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "FIN.SP_VOUCHER_PRINT_RPT"
            Dim ds As New DataSet, DocType As String, DocName As String
            Dim adpt As New SqlDataAdapter
            adpt.SelectCommand = cmd
            adpt.SelectCommand.CommandTimeout = 0
            ds.Clear()
            adpt.Fill(ds)
            Dim repSource As New MyReportClass
            If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
                DocType = param(2).Value
                DocName = param(3).Value
                Dim params As New Hashtable
                params("userName") = HttpContext.Current.Session("sUsr_name")
                params("Duplicated") = False
                params("VoucherName") = DocName
                params("decimal") = HttpContext.Current.Session("BSU_ROUNDOFF")
                repSource.Parameter = params
                Dim formulas As New Hashtable
                repSource.Formulas = formulas

                repSource.VoucherName = DocName
                repSource.IncludeBSUImage = True
                If chkSelection1.Checked Then
                    DocType = "JV"
                End If
                Select Case DocType
                    Case "JV"
                        repSource.ResourceName = "../RPT_Files/DAX_JournalVoucherReport.rpt"
                        params("reportHeading") = DocName
                    Case "BP"
                        repSource.ResourceName = "../RPT_Files/DAX_BankPaymentReport.rpt"
                        params("Summary") = False
                    Case "BR"
                        repSource.ResourceName = "../RPT_Files/DAX_BankReceiptReport.rpt"
                        params("Summary") = False
                    Case "QR"
                        repSource.ResourceName = "../RPT_Files/DAX_CashPaymentVoucher.rpt"
                        params("Summary") = False
                    Case "CP", "CR"
                        repSource.ResourceName = "../RPT_Files/DAX_CashPaymentVoucher.rpt"
                        params("Summary") = False
                    Case "CC"
                        repSource.ResourceName = "../RPT_Files/DAX_CreditCardReport.rpt"
                        params("Summary") = False
                    Case "DN", "CN"
                        repSource.ResourceName = "../RPT_Files/DAX_CREDITDEBITReport.rpt"
                        params("Summary") = False
                    Case Else
                        repSource.ResourceName = "../RPT_Files/DAX_JournalVoucherReport.rpt"
                        params("reportHeading") = DocName

                End Select
                repSource.Command = cmd
                Session("ReportSource") = repSource
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Function
    Private Sub RememberOldValues()
        Dim itmIdList As New ArrayList()
        Dim bindDt As DataTable
        bindDt = gvDetails.DataSource
        For Each row As GridViewRow In gvDetails.Rows
            Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text

            Dim result As Boolean = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox).Checked
            If ViewState("checked_items") IsNot Nothing Then
                itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
            End If
            If result Then
                If Not itmIdList.Contains(index) Then
                    itmIdList.Add(index)
                End If
            Else
                itmIdList.Remove(index)
            End If
        Next
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk IsNot Nothing Then

                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub RePopulateValues()
        Dim itmIdList As ArrayList = DirectCast(ViewState("checked_items"), ArrayList)
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            For Each row As GridViewRow In gvDetails.Rows
                Dim index As String = DirectCast(row.FindControl("lblItemCol1"), Label).Text
                If itmIdList.Contains(index) Then
                    Dim myCheckBox As HtmlInputCheckBox = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub


    Protected Sub DropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDown1.SelectedIndexChanged, DropDown2.SelectedIndexChanged
        Try
            BuildListView(False, False)
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnGO_Click(sender As Object, e As EventArgs) Handles btnGO.Click
        Try
            BuildListView()
            gridbind(False)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlBSU_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBSU.SelectedIndexChanged
        BuildListView()
        gridbind(False)
    End Sub
End Class

