Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PopupShowData
    Inherits BasePage
    'Version            Date            Author              Change
    '1.1                28-feb-2011     Swapna              Pop -up to show company leave days in grid
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            GridViewShowDetails.Attributes.Add("bordercolor", "#1b80b6")
            gridbind()
        End If
    End Sub

    Sub gridbind()
        Dim str_Transport_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Select Case Mainclass.cleanString(Request.QueryString("id"))
            Case "RECEIPTHISTORY"
                Page.Title = "::::GEMS PHOENIX:::: Payment History"
                gridbindReceiptsHistory()
            Case "CHARGEHISTORY"
                Page.Title = "::::GEMS PHOENIX:::: Charge Details"
                gridbindChargeHistory()
            Case "SIBBLINGS"
                Page.Title = "::::GEMS PHOENIX:::: Sibling Details"
                gridbindViewSibblings()
            Case "CONCESSION_FEES"
                Page.Title = "::::GEMS PHOENIX:::: Concesion Details"
                gridbindViewConcession()
            Case "ADJUSTMENT"
                Page.Title = "::::GEMS PHOENIX:::: Adjustment Details"
                gridbindViewAdjustment()
            Case "COSTCENTER"
                Page.Title = "::::GEMS PHOENIX:::: Cost Center"
                gridbindCostCenter()
            Case "VOUCHER"
                Page.Title = "::::GEMS PHOENIX:::: Voucher"
                gridbindVoucher()
            Case "CHQBOUNCE"
                Page.Title = "::::GEMS PHOENIX:::: Cheque Return History"
                gridbindChequeBounceHistory()
            Case "ADVPAY"
                Page.Title = "::::GEMS PHOENIX:::: Advance Receipt"
                gridbindAdvancedAmount()
            Case "AGING"
                Page.Title = "::::GEMS PHOENIX:::: Ageing Details"
                gridbindAging()
            Case "FEEPRINT"
                Page.Title = "::::GEMS PHOENIX:::: Fee Print Order"
                gridbindFeePrint()
            Case "FEESETTLE"
                Page.Title = "::::GEMS PHOENIX:::: Fee Settle Order"
                gridbindFeeSettle()
            Case "FEEFORGRADE"
                Page.Title = "::::GEMS PHOENIX:::: Fee For a Grade"
                gridbindFeeForGrade()
            Case "CHARGEHISTORY_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Charge Details"
                gridbindChargeHistory_Transport(str_Transport_conn)
            Case "RECEIPTHISTORY_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Payment History"
                gridbindReceiptsHistory_Transport(str_Transport_conn)
            Case "SIBBLINGS_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Sibling Details"
                gridbindViewSibblings_Transport()
            Case "CONCESSION_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Concesion Details"
                gridbindViewConcession_Transport(str_Transport_conn)
            Case "ADJUSTMENT_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Adjustment Details"
                gridbindViewAdjustment_Transport(str_Transport_conn)
            Case "CHQBOUNCE_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Cheque Return History"
                gridbindChequeBounceHistory_Transport(str_Transport_conn)
            Case "ADVPAY_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Advance Receipt"
                gridbindAdvancedAmount_Transport(str_Transport_conn)
            Case "AGING_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Ageing Details"
                gridbindAging_Transport()
            Case "CHARGEHISTORYTERMLY_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Fee (Termly)"
                gridbindAging_CHARGEHISTORYTERMLY(True)
            Case "CHARGEHISTORYMONTHLY_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Fee (Monthly)"
                gridbindAging_CHARGEHISTORYTERMLY(False)
            Case "STUDENTSHORTLIST"
                Page.Title = "::::GEMS PHOENIX:::: Student Due List"
                GET_STUDENTSHORTLIST_DUE()
            Case "PAYMENTHISTORYREFUND"
                Page.Title = "::::GEMS PHOENIX:::: Student Payment History"
                GetPaymentHistoryForRefund()
            Case "LEAVEPLANNER"     'V1.1
                Page.Title = "::::GEMS PHOENIX:::: Holidays List"
                txtFrom.Text = Format(CDate(Mainclass.cleanString(Request.QueryString("StartDate"))), "dd/MMM/yyyy")
                txtDays.Text = Mainclass.cleanString(Request.QueryString("Days"))
                If Mainclass.cleanString(Request.QueryString("IsPlannerEnabled")) = 0 Then
                    Dim dt As New DataTable
                    GridViewShowDetails.DataSource = dt
                    GridViewShowDetails.DataBind()
                    btnPrint.Visible = False
                    trdays.Visible = True
                Else
                    GetBSULeaveDays()
                    trdays.Visible = True
                    btnPrint.Visible = True
                End If
                'txtDays.Attributes.Add("onkeypress", "return  Numeric_Only()")


            Case "EMPLEAVECOUNTS"  'V1.1
                Page.Title = "::::GEMS PHOENIX:::: Leave Details"
                GetEmpLeaveCounts()

            Case "PAYMENTHISTORYREFUND_TRANSPORT"
                Page.Title = "::::GEMS PHOENIX:::: Student Payment History::::Transport"
                GetPaymentHistoryForRefund_TRANSPORT(str_Transport_conn)
        End Select
    End Sub
    Sub GetPaymentHistoryForRefund_TRANSPORT(ByVal str_conn As String)
        Try
            Dim str_Sql As String = " exec FEES.GetPaymentHistoryForRefund '" & Mainclass.cleanString(Request.QueryString("stuid")) & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    'V1.1 starts
    Sub GetBSULeaveDays()
        '@BSU_ID varchar(20) ,
        '@bTerm bit ,
        '@SBL_ID int  ,
        '@STU_ID BIGINT
        If Mainclass.cleanString(Request.QueryString("StartDate")) <> "" And Mainclass.cleanString(Request.QueryString("Days")) <> "" And Mainclass.cleanString(Request.QueryString("BSUID")) <> "" And Mainclass.cleanString(Request.QueryString("IsPlannerEnabled")) = 1 Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(0).Value = CDate(txtFrom.Text)

            pParms(1) = New SqlClient.SqlParameter("@Days", SqlDbType.Int)
            pParms(1).Value = CInt(txtDays.Text)

            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = Mainclass.cleanString(Request.QueryString("BSUID"))

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[GetMyLeaveDays]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                GridViewShowDetails.DataSource = ds
                GridViewShowDetails.DataBind()
                Dim count As Integer = GridViewShowDetails.Rows.Count
                For Each grow As GridViewRow In GridViewShowDetails.Rows
                    If grow.Cells(3).Text.ToLower = "friday" Or grow.Cells(3).Text.ToLower = "saturday" Then
                        grow.BackColor = Drawing.Color.BurlyWood
                    End If
                    If (grow.RowIndex = count - 1) Then
                        lblRejoinDate.Text = "Rejoining Date: " + Format(DateAdd(DateInterval.Day, 1, CDate(grow.Cells(1).Text)), "dd/MMM/yyyy")
                        lblRejoinDate.Visible = True
                    End If
                Next

            End Using
        End If
    End Sub
    Sub GetEmpLeaveCounts()
        '@BSU_ID varchar(20) ,
        '@bTerm bit ,
        '@SBL_ID int  ,
        '@STU_ID BIGINT
        If (Mainclass.cleanString(Request.QueryString("EmpID")) <> "" And Mainclass.cleanString(Request.QueryString("Date")) <> "" And Mainclass.cleanString(Request.QueryString("ELTID")) <> "" And Mainclass.cleanString(Request.QueryString("BDetailed")) <> "") Then
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = Mainclass.cleanString(Request.QueryString("EmpID"))

            pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(1).Value = Mainclass.cleanString(Request.QueryString("Date"))

            pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
            pParms(2).Value = Mainclass.cleanString(Request.QueryString("ELTID"))

            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = Mainclass.cleanString(Request.QueryString("BSUID"))

            pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
            pParms(4).Value = Mainclass.cleanString(Request.QueryString("BDetailed"))

            Dim ds As New DataSet
            Dim dtgrid As New DataTable
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                For Each dt As DataTable In ds.Tables
                    If dt.Columns.Count > 2 Then
                        dtgrid = dt
                    End If
                Next
                GridViewShowDetails.DataSource = dtgrid
                GridViewShowDetails.DataBind()
            End Using
        End If
    End Sub
    Sub gridbindAging_CHARGEHISTORYTERMLY(ByVal p_IsTermly As Boolean)
        '@BSU_ID varchar(20) ,
        '@bTerm bit ,
        '@SBL_ID int  ,
        '@STU_ID BIGINT
        Dim ACD_ID As Integer = 0
        If Not Mainclass.cleanString(Request.QueryString("ACDID")) Is Nothing Then
            ACD_ID = Mainclass.cleanString(Request.QueryString("ACDID").Replace(" ", "+"))
        End If
        If Mainclass.cleanString(Request.QueryString("SBL")) <> "" And Mainclass.cleanString(Request.QueryString("stuid")) <> "" Then
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
            pParms(0).Value = Mainclass.cleanString(Request.QueryString("stuid"))
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = Mainclass.cleanString(Request.QueryString("BSU"))
            pParms(2) = New SqlClient.SqlParameter("@bTerm", SqlDbType.VarChar, 200)
            pParms(2).Value = p_IsTermly
            pParms(3) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = Mainclass.cleanString(Request.QueryString("SBL"))
            pParms(3) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = Mainclass.cleanString(Request.QueryString("SBL"))
            pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
            pParms(4).Value = ACD_ID
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                Dim cmd As New SqlCommand("[TRANSPORT].[GetFeePayableSplitup]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                GridViewShowDetails.DataSource = ds
                GridViewShowDetails.DataBind()
            End Using
        End If
    End Sub

    Sub gridbindAging_Transport()
        Try
            Dim sqlParam(8) As SqlParameter
            sqlParam(1) = New SqlParameter("@Bkt1", 30)
            sqlParam(2) = New SqlParameter("@Bkt2", 60)
            sqlParam(3) = New SqlParameter("@Bkt3", 90)
            sqlParam(4) = New SqlParameter("@Bkt4", 120)
            sqlParam(5) = New SqlParameter("@Bkt5", 180)
            sqlParam(6) = New SqlParameter("@BSU_ID", Session("sBSUID"))
            sqlParam(7) = New SqlParameter("@FROMDOCDT", Format(DateTime.Now, OASISConstants.DateFormat))
            sqlParam(8) = New SqlParameter("@STU_ID", Mainclass.cleanString(Request.QueryString("stuid")))
            sqlParam(0) = New SqlParameter("@STU_BSU_ID", Mainclass.cleanString(Request.QueryString("BSU")))
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISTransportConnection
                Dim cmd As New SqlCommand("FEES.[F_GET_AGING_STUDENTWISE]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(sqlParam)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                GridViewShowDetails.DataSource = ds
                GridViewShowDetails.DataBind()
            End Using
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindAdvancedAmount_Transport(ByVal str_conn As String)
        Try
            Dim str_Sql As String = " exec TRANSPORT.GetStudentData  '" & Session("sBsuid") & "', " & _
            " '" & Mainclass.cleanString(Request.QueryString("stuid")) & "','','ADJUSTMENT_TRANSPORT'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindChequeBounceHistory_Transport(ByVal str_conn As String)
        Try
            Dim str_Sql As String = " exec TRANSPORT.GetStudentData  '" & Session("sBsuid") & "', " & _
             " '" & Mainclass.cleanString(Request.QueryString("stuid")) & "','','CHQBOUNCE_TRANSPORT'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindViewAdjustment_Transport(ByVal str_conn As String)
        Try
            Dim str_Sql As String = " exec TRANSPORT.GetStudentData  '" & Session("sBsuid") & "', " & _
            " '" & Mainclass.cleanString(Request.QueryString("stuid")) & "','','ADJUSTMENT_TRANSPORT'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindViewConcession_Transport(ByVal str_conn As String)
        If Mainclass.cleanString(Request.QueryString("stuid")) <> "" Then
            Try
                Dim str_Sql As String = " exec TRANSPORT.GetStudentData " & _
                 " '" & Session("sBsuid") & "','" & Mainclass.cleanString(Request.QueryString("stuid")) & "','','CONCESSION_TRANSPORT' "
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If ds.Tables.Count > 0 Then
                    GridViewShowDetails.DataSource = ds
                    GridViewShowDetails.DataBind()
                End If
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub gridbindViewSibblings_Transport()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT   STU_NO, STU_NAME, GRD_DISPLAY as 'Grade' " _
            & " FROM VW_OSO_STUDENT_M WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & Mainclass.cleanString(Request.QueryString("bsu")) & "') " _
            & " AND (STU_SIBLING_ID <> STU_ID) AND (STU_ID <> '" & Mainclass.cleanString(Request.QueryString("stuid")) & "') AND (STU_SIBLING_ID =" _
            & " (SELECT     STU_SIBLING_ID FROM STUDENT_M WHERE (STU_ID = '" & Mainclass.cleanString(Request.QueryString("stuid")) & "')))" _
            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF') "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindReceiptsHistory_Transport(ByVal str_conn As String)
        Try
            Dim str_Sql As String = " exec TRANSPORT.GetStudentData " & _
                " '" & Session("sBsuid") & "','" & Mainclass.cleanString(Request.QueryString("stuid")) & "','','RECEIPTHISTORY_TRANSPORT' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindChargeHistory_Transport(ByVal str_conn As String)
        Try
            Dim str_Sql As String = " exec TRANSPORT.GetStudentData " & _
              " '" & Session("sBsuid") & "','" & Mainclass.cleanString(Request.QueryString("stuid")) & "','','CHARGEHISTORY_TRANSPORT' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindFeeForGrade()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT     FEE.FEE_DESCR AS Fee , FSP.FSP_AMOUNT AS Amount, REPLACE(CONVERT(VARCHAR(11), FSP.FSP_FROMDT, 113), ' ', '/') AS 'From Date', " _
             & " GRM.GRD_DISPLAY as Grade,STM.STM_DESCR AS Stream, SBL.SBL_DESCRIPTION AS Location " _
             & " FROM FEES.FEESETUP_S AS FSP INNER JOIN FEES.FEES_M AS FEE ON FSP.FSP_FEE_ID = FEE.FEE_ID " _
             & " LEFT OUTER JOIN STREAM_M AS STM ON ISNULL(FSP.FSP_STM_ID, - 1) = CASE WHEN " _
             & " isnull(FEE_bSETUPBYGRADE, 0) = 1 THEN STM.STM_ID ELSE - 1 END LEFT OUTER JOIN" _
             & " GRADE_M AS GRM ON ISNULL(FSP.FSP_GRD_ID, 'NONE') = CASE WHEN isnull(FEE_bSETUPBYGRADE, 0) = 1 " _
             & " THEN GRM.GRD_ID ELSE 'NONE' END LEFT OUTER JOIN VV_TRANSPORT_SUBLOCATION AS SBL " _
             & " ON FSP.FSP_SBL_ID = SBL.SBL_ID AND FSP.FSP_BSU_ID = SBL.BSU_ID" _
             & " WHERE     (FSP.FSP_ACD_ID = '" & Mainclass.cleanString(Request.QueryString("acd")) & "') AND (FSP.FSP_BSU_ID = '" & Session("sBsuid") & "')" _
             & " AND (FSP.FSP_GRD_ID ='" & Mainclass.cleanString(Request.QueryString("grd")) & "') " _
             & " ORDER BY FEE.FEE_ORDER"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindFeeSettle()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT FEE_SETOFFORDER as 'Set of Order', FEE_DESCR as Fee FROM FEES.FEES_M order by FEE_SETOFFORDER"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindFeePrint()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT     FEE_ORDER as 'Order', FEE_DESCR as Fee FROM FEES.FEES_M order by FEE_ORDER"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindAging()
        Try
            Dim sqlParam(7) As SqlParameter
            'EXEC FEES.AGINGSummary @BSU_ID='125016',
            '@FROMDOCDT='12-AUG-2008' ,@Bkt1=30,
            '@Bkt2 =60,
            '@Bkt3 =90,
            '@Bkt4 =120,
            '@Bkt5 =180
            sqlParam(0) = New SqlParameter("@BSU_ID", Session("sBSUID"))
            sqlParam(1) = New SqlParameter("@Bkt1", 30)
            sqlParam(2) = New SqlParameter("@Bkt2", 60)
            sqlParam(3) = New SqlParameter("@Bkt3", 90)
            sqlParam(4) = New SqlParameter("@Bkt4", 120)
            sqlParam(5) = New SqlParameter("@Bkt5", 180)
            sqlParam(6) = New SqlParameter("@FROMDOCDT", Format(DateTime.Now, OASISConstants.DateFormat))
            sqlParam(7) = New SqlParameter("@STU_ID", Mainclass.cleanString(Request.QueryString("stuid")))
            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASIS_FEESConnection
                Dim cmd As New SqlCommand("FEES.[F_GET_AGING_STUDENTWISE]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(sqlParam)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                GridViewShowDetails.DataSource = ds
                GridViewShowDetails.DataBind()
            End Using
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindAdvancedAmount()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As String = "select FEE_TYPE ,ADV_AMOUNT AMOUNT from (SELECT  ISNULL((SELECT SUM(CASE WHEN FEES.FEECOLLECTION_H.FCL_DRCR = 'DR' THEN - 1 ELSE 1 END * FEES.FEECOLLALLOCATION_D.FCA_AMOUNT) " & _
            " AS AMOUNT FROM VW_OSF_COLLECTIONTYP_M INNER JOIN " & _
            " FEES.FEECOLLSUB_D INNER JOIN " & _
            " FEES.FEECOLLALLOCATION_D ON FEES.FEECOLLSUB_D.FCD_ID = FEES.FEECOLLALLOCATION_D.FCA_FCD_ID INNER JOIN" & _
            " FEES.FEECOLLECTION_H ON FEES.FEECOLLSUB_D.FCD_FCL_ID = FEES.FEECOLLECTION_H.FCL_ID INNER JOIN" & _
            " FEES.FEES_M ON FEES.FEECOLLALLOCATION_D.FCA_FEE_ID = FEES.FEES_M.FEE_ID ON " & _
            " VW_OSF_COLLECTIONTYP_M.CLT_ID = FEES.FEECOLLSUB_D.FCD_CLT_ID" & _
            " WHERE (CASE WHEN FEES.FEECOLLSUB_D.FCD_CLt_ID = 2 THEN FEES.FEECOLLSUB_D.FCD_STATUS ELSE 1 END = 1) AND " & _
            " (FEES.FEECOLLECTION_H.FCL_STU_ID = FEES.FEESCHEDULE.FSH_STU_ID) AND (FEES.FEECOLLECTION_H.FCL_BSU_ID = '" & Session("sBSUID") & "') " & _
            " AND (FEES.FEES_M.FEE_ID = FEES.FEESCHEDULE.FSH_FEE_ID) AND (FEES.FEECOLLECTION_H.FCL_DATE <= '" & DateTime.Now & "') AND " & _
            " FEES.FEECOLLECTION_H.FCL_STU_ID = FSH_STU_ID AND FEES.FEECOLLECTION_H.FCL_STU_TYPE = 'S') , 0) -" & _
            " SUM(CASE FSH_DRCR WHEN 'DR' THEN FSH_AMOUNT ELSE 0 END) AS 'ADV_AMOUNT' , FEES_M_1.FEE_DESCR FEE_TYPE" & _
            " FROM FEES.FEESCHEDULE INNER JOIN" & _
            " FEES.FEES_M AS FEES_M_1 ON FEES.FEESCHEDULE.FSH_FEE_ID = FEES_M_1.FEE_ID" & _
            " WHERE     (FEES.FEESCHEDULE.FSH_STU_TYPE = 'S') AND FEES.FEESCHEDULE.FSH_STU_ID = " & Mainclass.cleanString(Request.QueryString("stuid")) & _
            " GROUP BY  FEES_M_1.FEE_DESCR,FEES.FEESCHEDULE.FSH_FEE_ID, FEES.FEESCHEDULE.FSH_STU_ID	" & _
            " )a WHERE a.ADV_AMOUNT > 0"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindChequeBounceHistory()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT FEES.FEECHEQUERETURN_H.FCR_RECNO REC_NO, " & _
            "REPLACE(CONVERT(VARCHAR(11),FEES.FEECHEQUERETURN_H.FCR_DATE, 113), ' ', '/') as Bounce_Date, " & _
            " FEES.FEECHEQUERETURN_H.FCR_AMOUNT AMOUNT, " & _
            " FEES.FEECOLLCHQUES_D.FCQ_CHQNO CHEQUE_NO, BANK_M.BNK_DESCRIPTION BANK, EMIRATE_M.EMR_DESCR EMIRATE, " & _
            " FEES.FEECHEQUERETURN_H.FCR_NARRATION REMARKS FROM  FEES.FEECHEQUERETURN_H INNER JOIN" & _
            " FEES.FEECOLLCHQUES_D ON FEES.FEECHEQUERETURN_H.FCR_FCQ_ID = FEES.FEECOLLCHQUES_D.FCQ_ID " & _
            " INNER JOIN BANK_M ON FEES.FEECOLLCHQUES_D.FCQ_BANK = BANK_M.BNK_ID INNER JOIN" & _
            " EMIRATE_M ON FEES.FEECOLLCHQUES_D.FCQ_EMR_ID = EMIRATE_M.EMR_CODE" _
            & " WHERE FEES.FEECHEQUERETURN_H.FCR_STU_ID= '" & Mainclass.cleanString(Request.QueryString("stuid")) & "' " & _
            " ORDER BY FCR_DATE DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindReceiptsHistory()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT PAY.FEE_DESCR AS 'Fee', PAY.FCS_AMOUNT as 'Amount', " _
                & " PAY.FCL_RECNO as 'R.No', REPLACE(CONVERT(VARCHAR(11),FCL.FCL_DATE, 113), ' ', '/') as Date" _
                & " FROM FEES.VW_OSO_FEES_FEECOLLECTED AS PAY INNER JOIN" _
                & " FEES.FEECOLLECTION_H AS FCL ON PAY.FCL_ID = FCL.FCL_ID" _
                & " WHERE PAY.FCL_STU_ID= '" & Mainclass.cleanString(Request.QueryString("stuid")) & "' " _
                & " ORDER BY FCL_DATE DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindChargeHistory()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT FEE.FEE_DESCR AS 'Fee', " _
                & " REPLACE(CONVERT(VARCHAR(11), FSH.FSH_DATE, 113), ' ', '/') AS Date, " _
                & " CASE FSH.FSH_DRCR WHEN 'DR' THEN FSH.FSH_AMOUNT  ELSE 0 END AS 'DR'," _
                & " CASE FSH.FSH_DRCR WHEN 'CR' THEN FSH.FSH_AMOUNT ELSE 0 END AS 'CR'," _
                & " REPLACE(CONVERT(VARCHAR(11), FSH.FSH_ACTDATE, 113), ' ', '/') AS [Actual Date], " _
                & " FSH.FSH_SOURCE +' - ' + FSH.FSH_NARRATION as 'Descr' FROM FEES.FEESCHEDULE AS FSH INNER JOIN" _
                & " FEES.FEES_M AS FEE ON FSH.FSH_FEE_ID = FEE.FEE_ID " _
                & " WHERE  FSH.FSH_STU_ID ='" & Mainclass.cleanString(Request.QueryString("stuid")) & "'" _
                & " ORDER BY FSH_DATE DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindVoucher()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            Dim str_Sql As String = "SELECT JNL.JNL_ACT_ID + ' - '+ACT.ACT_NAME AS Account, " _
            & " REPLACE(CONVERT(VARCHAR(11), JNL.JNL_DOCDT, 113), ' ', '/') AS DOCDT, " _
            & " JNL.JNL_DEBIT as Debit, JNL.JNL_CREDIT as Credit, JNL.JNL_NARRATION 'Desc'" _
            & " FROM ACCOUNTS_M AS ACT INNER JOIN JOURNAL_D AS JNL ON ACT.ACT_ID = JNL.JNL_ACT_ID" _
            & " WHERE     (JNL.JNL_DOCNO = '" & Mainclass.cleanString(Request.QueryString("docid")) & "') AND " _
            & " (JNL.JNL_BSU_ID = '" & Mainclass.cleanString(Request.QueryString("bsu")) & "') " _
            & " ORDER BY JNL_DOCDT DESC "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindViewSibblings()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT   STU_NO, STU_NAME, GRD_DISPLAY as 'Grade' " _
            & " FROM VW_OSO_STUDENT_M WHERE     (STU_bActive = 1) AND (STU_BSU_ID = '" & Session("sBsuid") & "') " _
            & " AND (STU_ID <> '" & Mainclass.cleanString(Request.QueryString("stuid")) & "') AND (STU_SIBLING_ID in " _
            & " (SELECT STU_SIBLING_ID FROM STUDENT_M WHERE (STU_ID = '" & Mainclass.cleanString(Request.QueryString("stuid")) & "')))" _
            & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS not in('CN','TF') "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindViewConcession()
        Try
            Dim str_Sql As String = " exec fees.GetStudentData " & _
              " '" & Session("sBsuid") & "','" & Mainclass.cleanString(Request.QueryString("stuid")) & "','" & Mainclass.cleanString(Request.QueryString("acd")) & "','CONCESSION_FEES' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindViewAdjustment()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
            Dim str_Sql As String = "SELECT    FAD.FAD_REMARKS AS Descr, REPLACE(CONVERT(VARCHAR(11), FAH.FAH_DATE, 113), ' ', '/') AS Date ,  FEE.FEE_DESCR AS Fee, " _
             & " FAD.FAD_AMOUNT AS Amount, CASE WHEN FAH.FAH_ARM_ID IN (11,13) THEN 'NON REVENUE' ELSE 'REVENUE' END  'Adj.Type'" _
             & " FROM FEES.FEEADJUSTMENT_H AS FAH INNER JOIN " _
             & " FEES.FEEADJUSTMENT_S AS FAD ON FAH.FAH_ID = FAD.FAD_FAH_ID INNER JOIN" _
             & " FEES.FEES_M AS FEE ON FAD.FAD_FEE_ID = FEE.FEE_ID" _
             & " WHERE   FAH.FAH_STU_ID ='" & Mainclass.cleanString(Request.QueryString("stuid")) & "' AND (FAH.FAH_bDeleted = 0) AND  FAH.FAH_BSU_ID='" & Session("sBsuid") & "'" & _
             " ORDER BY FAH_DATE DESC"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbindCostCenter()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("maindb").ConnectionString
            Dim str_Sql As String = " [dbo].[GetCostCenterForQCCollection]  '" & Mainclass.cleanString(Request.QueryString("vcdid")) & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GET_STUDENTSHORTLIST_DUE()
        Try
            Dim str_Sql As String = "exec FEES.GET_STUDENTSHORTLIST_DUE  '" & Mainclass.cleanString(Request.QueryString("stuid")) & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            If ds.Tables.Count > 0 And Mainclass.cleanString(Request.QueryString("noclose")) <> "1" Then
                If ds.Tables(0).Rows.Count = 0 Then
                    Dim javaSr As String = "<script language=javascript>  window.close();</script>"
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
                End If
            End If
            GridViewShowDetails.EmptyDataText = "No data Found"
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetPaymentHistoryForRefund()
        Try
            Dim str_Sql As String = " exec FEES.GetPaymentHistoryForRefund '" & Mainclass.cleanString(Request.QueryString("stuid")) & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub PrintCurrentPage(ByVal sender As Object, ByVal e As EventArgs)
        GridViewShowDetails.PagerSettings.Visible = False
        GridViewShowDetails.DataBind()
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        GridViewShowDetails.RenderControl(hw)
        Dim gridHTML As String = sw.ToString().Replace("""", "'") _
        .Replace(System.Environment.NewLine, "")
        Dim sb As New StringBuilder()
        sb.Append("< type ='text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=600,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(gridHTML)
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("< /script >")
        ClientScript.RegisterStartupScript(Me.GetType(), "GridPrint", sb.ToString())
        GridViewShowDetails.PagerSettings.Visible = True
        GridViewShowDetails.DataBind()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        GetBSULeaveDays()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        'PrintCurrentPage(sender, e)
    End Sub


    Protected Sub GridViewShowDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewShowDetails.PageIndexChanging
        GridViewShowDetails.PageIndex = e.NewPageIndex
        gridbind()

    End Sub
End Class
