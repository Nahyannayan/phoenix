Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Common_PopupFromIDHiddenSeven
    Inherits BasePage
    '-------------------------------------------------------------------------
    'Author:Swapana.T.V
    'Creation Date:12/Dec/2010
    'Purpose:To show active employees list in pop-up for transferring
    '-------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If 

       
        If Page.IsPostBack = False Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
            Session("liUserList") = New List(Of String)
            ViewState("ID") = IIf(Request.QueryString("ID") Is Nothing, String.Empty, Request.QueryString("ID"))
            GridBind()
        End If
       
        set_Menu_Img()

    End Sub



    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function

  
    Protected Sub gvGroup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvGroup.PageIndexChanging
        gvGroup.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Private Sub GridBind()
        Select Case ViewState("ID").ToString
           
            Case "EMPLOYEES_TOTRANSFER"                 'To show active employees list who can be transferred

                EMPLOYEES_TOTRANSFER()
                gvGroup.Columns(5).Visible = False
            Case "EMPLOYEES_ALL"                 'To show active employees list who can be transferred

                EMPLOYEES_ALL()
                gvGroup.Columns(5).Visible = False
            Case "EMPLOYEES_RESIGNED"
                EMPLOYEES_List("EMPLOYEES_RESIGNED")
                gvGroup.Columns(5).Visible = False

        End Select
    End Sub
    Protected Sub EMPLOYEES_List(ByVal value As String)
        Try
            Dim str_filter As String = String.Empty
            Dim str_txtDescr1 As String = String.Empty
            Dim str_txtDescr2 As String = String.Empty
            Dim str_txtDescr3 As String = String.Empty
            Dim str_txtDescr4 As String = String.Empty
            Dim str_txtDescr5 As String = String.Empty
            Dim str_txtDescr6 As String = String.Empty
            Dim str_txtDescr7 As String = String.Empty
            Dim str_act, str_doctype As String
            str_act = Request.QueryString("act")
            str_doctype = Request.QueryString("type")

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
                str_txtDescr3 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR3", str_Sid_search(0), str_txtDescr3)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
                str_txtDescr4 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR4", str_Sid_search(0), str_txtDescr4)

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
                str_txtDescr5 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR5", str_Sid_search(0), str_txtDescr5)



            End If
            Dim str_Sql As String
            Dim str_query_header As String = String.Empty
            Dim BSU_ID As String = ""
            Dim USR_ID As String = ""
            BSU_ID = Session("sBsuid").ToString
            USR_ID = Session("sUsr_name").ToString
            If (value = "EMPLOYEES_TOTRANSFER") Then
                str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
                " SELECT ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2, isnull(EMP_OLDNO,'') AS DESCR3,  " & _
                " C.ECT_DESCR  AS DESCR4, D.DES_DESCR AS DESCR5," & _
                " EMP_ID as DESCR6 FROM EMPLOYEE_M E LEFT JOIN EMPCATEGORY_M C ON C.ECT_ID=E.EMP_ECT_ID " & _
                " LEFT JOIN EMPDESIGNATION_M D ON D.DES_ID=E.EMP_DES_ID " & _
                " LEFT JOIN BUSINESSUNIT_M B ON B.BSU_ID=E.EMP_BSU_ID " & _
                " WHERE EMP_STATUS not in (4,5) AND EMP_bACTIVE=1 AND emp_bsu_ID='" & BSU_ID & "') DB WHERE 1=1 " & str_filter & _
                "|" & "Employee ID|Employee Name|Old Staff No.|Category|Designation"

            ElseIf (value = "EMPLOYEES_VisaUnitChange") Then
                str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
               " SELECT ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2, isnull(B.BSU_Name,'') AS DESCR3,  " & _
               " C.ECT_DESCR  AS DESCR4, D.DES_DESCR AS DESCR5," & _
               " EMP_ID as DESCR6 FROM EMPLOYEE_M E LEFT JOIN EMPCATEGORY_M C ON C.ECT_ID=E.EMP_ECT_ID " & _
               " LEFT JOIN EMPDESIGNATION_M D ON D.DES_ID=E.EMP_DES_ID " & _
               " LEFT JOIN BUSINESSUNIT_M B ON B.BSU_ID=E.EMP_BSU_ID " & _
               " WHERE EMP_STATUS<>4 AND EMP_bACTIVE=1 AND EMP_VISA_BSU_ID='" & BSU_ID & "') DB WHERE 1=1 " & str_filter & _
               "|" & "Employee ID|Employee Name|Old Staff No.|Category|Designation"

            ElseIf (value = "EMPLOYEES_LeavePlan") Then
                str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
               " SELECT ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2,ELT_DESCR as  DESCR3,  " & _
               " dbo.fnFormatDate(ELA_DTFROM,'DD-MON-YYYY')    AS DESCR4,dbo.fnFormatDate(ELA_DTTO,'DD-MON-YYYY')   AS DESCR5 , DATEDIFF(D, ELA_DTFROM, ELA_DTTO) AS DESCR6 " & _
               " FROM APPROVAL_S A inner join EMPLEAVEAPP L" & _
               " on A.APS_DOC_ID=L.ELA_ID" & _
               " inner join EMPLOYEE_M E on" & _
               " L.ELA_EMP_ID = E.EMP_ID" & _
               " inner join EMPLEAVETYPE_M on" & _
               " L.ELA_ELT_ID=EMPLEAVETYPE_M.ELT_ID WHERE  " & _
              " APS_DOCTYPE='LEAVE' AND  ELA_APPRSTATUS = 'A' AND " & _
              " APS_USR_ID in (select USR_EMP_ID from USERS_M where USR_NAME='" & USR_ID & "')) DB WHERE 1=1 " & str_filter & _
               "|" & "Employee ID|Employee Name|Leave Type|From Date|To Date|Days"

            ElseIf (value = "EMPLOYEES_RESIGNED") Then
                str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
                " SELECT ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2, REPLACE(CONVERT(VARCHAR(11),isnull(EMP_RESGDT,isnull(ereg_regdate,est_dtfrom)),106),' ','/') AS DESCR3,  " & _
                "  REPLACE(CONVERT(VARCHAR(11), isnull(EMP_LastAttDt,isnull(ereg_LASTWORKINGDATE,est_dtto)),106),' ','/') AS DESCR4, D.DES_DESCR AS DESCR5," & _
                " EMP_ID as DESCR6 FROM EMPLOYEE_M E LEFT JOIN EMPTRANTYPE_TRN ON EST_EMP_ID =EMP_ID and est_ttp_id=4 and est_code in ('4','5') " & _
                " LEFT JOIN EMPRESIGNATION_D on EREG_EMP_ID=EMP_ID and ereg_bsu_id=emp_bsu_id " & _
                 " LEFT JOIN EMPDESIGNATION_M D ON D.DES_ID=E.EMP_DES_ID " & _
                " WHERE (EMP_STATUS in (4,5) or emp_tran_status in (4,5) or est_id is not null) AND emp_bsu_ID='" & BSU_ID & "') DB WHERE 1=1 " & str_filter & _
                "|" & "Employee ID|Employee Name|Resignation Date|LWD|Designation"
            End If

            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & "order by DESCR2")
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count

                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
            txtSearch.Text = str_txtDescr3
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
            txtSearch.Text = str_txtDescr4
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
            txtSearch.Text = str_txtDescr5


            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr3")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr4")
            lblheader.Text = str_headers(4)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr5")
            lblheader.Text = str_headers(5)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDESCR6")   'V1.1
            lblheader.Text = str_headers(6)


            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub EMPLOYEES_TOTRANSFER()
        Try
            Dim str_filter As String = String.Empty
            Dim str_txtDescr1 As String = String.Empty
            Dim str_txtDescr2 As String = String.Empty
            Dim str_txtDescr3 As String = String.Empty
            Dim str_txtDescr4 As String = String.Empty
            Dim str_txtDescr5 As String = String.Empty
            Dim str_txtDescr6 As String = String.Empty
            Dim str_txtDescr7 As String = String.Empty
            Dim str_act, str_doctype As String
            str_act = Request.QueryString("act")
            str_doctype = Request.QueryString("type")

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
                str_txtDescr3 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR3", str_Sid_search(0), str_txtDescr3)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
                str_txtDescr4 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR4", str_Sid_search(0), str_txtDescr4)

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
                str_txtDescr5 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR5", str_Sid_search(0), str_txtDescr5)

               

            End If
            Dim str_Sql As String
            Dim str_query_header As String = String.Empty
            Dim BSU_ID As String = ""
            BSU_ID = Session("sBsuid").ToString
            str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
            " SELECT ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2, isnull(EMP_OLDNO,'') AS DESCR3,  " & _
            " C.ECT_DESCR  AS DESCR4, D.DES_DESCR AS DESCR5," & _
            " EMP_ID as DESCR6 FROM EMPLOYEE_M E LEFT JOIN EMPCATEGORY_M C ON C.ECT_ID=E.EMP_ECT_ID " & _
            " LEFT JOIN EMPDESIGNATION_M D ON D.DES_ID=E.EMP_DES_ID " & _
            " LEFT JOIN BUSINESSUNIT_M B ON B.BSU_ID=E.EMP_BSU_ID " & _
            " WHERE EMP_STATUS<>4 AND EMP_bACTIVE=1 AND emp_bsu_ID='" & BSU_ID & "') DB WHERE 1=1 " & str_filter & _
            "|" & "Employee ID|Employee Name|Old Staff No.|Category|Designation"
            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & "order by DESCR2")
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count

                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()
                
            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
            txtSearch.Text = str_txtDescr3
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
            txtSearch.Text = str_txtDescr4
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
            txtSearch.Text = str_txtDescr5


            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr3")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr4")
            lblheader.Text = str_headers(4)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr5")
            lblheader.Text = str_headers(5)
            
            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub EMPLOYEES_ALL()
        Try
            Dim str_filter As String = String.Empty
            Dim str_txtDescr1 As String = String.Empty
            Dim str_txtDescr2 As String = String.Empty
            Dim str_txtDescr3 As String = String.Empty
            Dim str_txtDescr4 As String = String.Empty
            Dim str_txtDescr5 As String = String.Empty
            Dim str_txtDescr6 As String = String.Empty
            Dim str_txtDescr7 As String = String.Empty
            Dim str_act, str_doctype As String
            str_act = Request.QueryString("act")
            str_doctype = Request.QueryString("type")

            Dim txtSearch As New TextBox
            If gvGroup.Rows.Count > 0 Then
                ''code
                Dim str_Sid_search() As String
                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
                str_txtDescr1 = txtSearch.Text.Trim
                str_filter = set_search_filter("DESCR1", str_Sid_search(0), str_txtDescr1)

                str_Sid_search = h_selected_menu_2.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
                str_txtDescr2 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR2", str_Sid_search(0), str_txtDescr2)

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
                str_txtDescr3 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR3", str_Sid_search(0), str_txtDescr3)

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
                str_txtDescr4 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR4", str_Sid_search(0), str_txtDescr4)

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
                str_txtDescr5 = txtSearch.Text.Trim
                str_filter = str_filter & set_search_filter("DESCR5", str_Sid_search(0), str_txtDescr5)



            End If
            Dim str_Sql As String
            Dim str_query_header As String = String.Empty
            Dim BSU_ID As String = ""
            BSU_ID = Session("sBsuid").ToString
            str_query_header = "SELECT DESCR1,DESCR2,DESCR3,DESCR4,DESCR5,DESCR6 FROM ( " & _
            " SELECT distinct ISNULL(EMPNO,'') AS DESCR1, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') AS DESCR2, isnull(EMP_OLDNO,'') AS DESCR3,  " & _
            " C.ECT_DESCR  AS DESCR4, D.DES_DESCR AS DESCR5," & _
            " EMP_ID as DESCR6 FROM EMPLOYEE_M E LEFT JOIN EMPCATEGORY_M C ON C.ECT_ID=E.EMP_ECT_ID " & _
            " LEFT JOIN EMPDESIGNATION_M D ON D.DES_ID=E.EMP_DES_ID " & _
            " inner JOIN EMPSALARYDATA_D ON emp_id=esd_emp_id " & _
            " WHERE  isnull(esd_paid,0)=0  and ESD_BSU_ID = '" & BSU_ID & "') DB WHERE 1=1 " & str_filter & _
            "|" & "Employee ID|Employee Name|Old Staff No.|Category|Designation"
            str_Sql = str_query_header.Split("||")(0)
            Dim str_headers As String()
            str_headers = str_query_header.Split("|")
            Dim ds As New DataSet
            If str_Sql <> "" Then
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql & "order by DESCR2")
            End If
            gvGroup.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvGroup.DataBind()
                Dim columnCount As Integer = gvGroup.Rows(0).Cells.Count

                gvGroup.Rows(0).Cells.Clear()
                gvGroup.Rows(0).Cells.Add(New TableCell)
                gvGroup.Rows(0).Cells(0).ColumnSpan = columnCount
                gvGroup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvGroup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                ' sp_message.InnerHtml = displayMessage("", 50, 6, 50)
                'gvGroup.HeaderRow.Visible = True
            Else
                gvGroup.DataBind()

            End If
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR1")
            txtSearch.Text = str_txtDescr1
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR2")
            txtSearch.Text = str_txtDescr2
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR3")
            txtSearch.Text = str_txtDescr3
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR4")
            txtSearch.Text = str_txtDescr4
            txtSearch = gvGroup.HeaderRow.FindControl("txtDESCR5")
            txtSearch.Text = str_txtDescr5


            Dim lblheader As New Label
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr1")
            lblheader.Text = str_headers(1)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr2")
            lblheader.Text = str_headers(2)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr3")
            lblheader.Text = str_headers(3)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr4")
            lblheader.Text = str_headers(4)
            lblheader = gvGroup.HeaderRow.FindControl("lblHDescr5")
            lblheader.Text = str_headers(5)

            set_Menu_Img()

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnNameSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn1Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn2Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnColumn3Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvGroup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvGroup.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub lnkID_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender.Parent.FindControl("lnkName")
        lblcode = sender.Parent.FindControl("lblEmpID")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "||" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & l_Str_Msg & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub lnkName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New Label
        Dim lbClose As New LinkButton
        lbClose = sender
        If Not ViewState("EMPNO") Is Nothing Then
            If ViewState("EMPNO") = "1" Then
                lbClose = sender.Parent.FindControl("lnkName")
            End If
        End If
        lblcode = sender.Parent.FindControl("lblEmpID")
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "||" & lblcode.Text

        l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")
            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write(" var oArg = new Object();")
            Response.Write("oArg.NameandCode ='" & l_Str_Msg & "' ; ")
            Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg & "');")
            Response.Write("oWnd.close(oArg);")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub btnCodeSearch2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
End Class

