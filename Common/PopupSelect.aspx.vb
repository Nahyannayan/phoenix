Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Common_PopupSelect
    Inherits BasePage
    Dim SearchMode As String
    'Version            Author          Date            Purpose
    ' 1.1               Shakeel          04/Sep/2011      Pop Up to select record
    Private Property FloatColIndex() As String
        Get
            Return ViewState("FloatColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("FloatColIndex") = value
        End Set
    End Property
    Private Property DateColIndex() As String
        Get
            Return ViewState("DateColIndex")
        End Get
        Set(ByVal value As String)
            ViewState("DateColIndex") = value
        End Set
    End Property
    Private Property HideFirstColumn() As Boolean
        Get
            Return ViewState("HideFirstColumn")
        End Get
        Set(ByVal value As Boolean)
            ViewState("HideFirstColumn") = value
        End Set
    End Property
    Private Property MultiSelect() As Boolean
        Get
            Return ViewState("MultiSelect")
        End Get
        Set(ByVal value As Boolean)
            ViewState("MultiSelect") = value
        End Set
    End Property
    Private Property GridListData() As DataTable
        Get
            Return ViewState("GridListData")
        End Get
        Set(ByVal value As DataTable)
            ViewState("GridListData") = value
        End Set
    End Property
    Private Property IsStoredProcedure() As Boolean
        Get
            Return ViewState("IsStoredProcedure")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsStoredProcedure") = value
        End Set
    End Property
    Private Property IsDataTableAsList() As Boolean
        Get
            Return ViewState("IsDataTableAsList")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsDataTableAsList") = value
        End Set
    End Property
    Private Property FilterTextboxString() As String
        Get
            Return ViewState("FilterTextboxString")
        End Get
        Set(ByVal value As String)
            ViewState("FilterTextboxString") = value
        End Set
    End Property
    Private Property SPParam() As SqlParameter()
        Get
            Return Session("SPParam")
        End Get
        Set(ByVal value As SqlParameter())
            Session("SPParam") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Page.DataBind()
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_9.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_10.Value = "LI__../Images/operations/like.gif"
                HideFirstColumn = True

                If Not Mainclass.cleanString(Request.QueryString("title")) Is Nothing Then
                    TitleLabel.Text = Request.QueryString("title").ToString
                End If
                If Not Mainclass.cleanString(Request.QueryString("MultiSelect")) Is Nothing Then

                    If Mainclass.cleanString(Request.QueryString("MultiSelect")) = "1" Or Mainclass.cleanString(Request.QueryString("MultiSelect")) = "true" Then
                        MultiSelect = True
                    Else
                        MultiSelect = False
                    End If
                Else
                    MultiSelect = False
                End If
                IsStoredProcedure = False
                IsDataTableAsList = False
                gridbind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(1, str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(2, str_Sid_img(2))
        str_Sid_img = h_selected_menu_3.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(3, str_Sid_img(2))
        str_Sid_img = h_selected_menu_4.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(4, str_Sid_img(2))
        str_Sid_img = h_selected_menu_5.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(5, str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(6, str_Sid_img(2))
        str_Sid_img = h_selected_menu_7.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(7, str_Sid_img(2))
        str_Sid_img = h_selected_menu_8.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(8, str_Sid_img(2))
        str_Sid_img = h_selected_menu_9.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(9, str_Sid_img(2))
        str_Sid_img = h_selected_menu_10.Value.Split("__")
        If str_Sid_img.Length > 2 Then setID(10, str_Sid_img(2))

    End Sub
    Public Sub setID(ByVal ColumnNo As Int16, Optional ByVal p_imgsrc As String = "")
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvDetails.HeaderRow.FindControl("mnu_" & ColumnNo.ToString & "_img")
                If s Is Nothing Then Exit Sub
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvDetails.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String
            pControl = pImg
            Try
                s = gvDetails.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function SetCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull([" & pField & "],'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function SetDTFilterCondn(ByVal pOprSearch As String, ByVal pField As String, ByVal pVal As String) As String
        Dim lstrSearchCondn As String = ""
        Dim lstrSearchOpr As String = ""
        If pOprSearch = "LI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "NLI" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "%'"
        ElseIf pOprSearch = "SW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "NSW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '" & pVal & "%'"
        ElseIf pOprSearch = "EW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " LIKE '%" & pVal & "'"
        ElseIf pOprSearch = "NEW" Then
            lstrSearchOpr = "isnull(Convert([" & pField & "], 'System.String'),'')" & " NOT LIKE '%" & pVal & "'"
        End If
        If lstrSearchOpr <> "" Then lstrSearchCondn = " AND " & lstrSearchOpr
        Return lstrSearchCondn
    End Function
    Private Function getColumnFilterString(ByVal FilterTypeString As String, ByVal GridViewHeaderRow As GridViewRow, ByVal ControlName As String, ByVal ColumnName As String) As String
        Try
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox
            txtSearch = GridViewHeaderRow.FindControl(ControlName)
            Dim str_Filter As String = ""
            larrSearchOpr = FilterTypeString.Split("__")
            lstrOpr = larrSearchOpr(0)
            str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            If Not IsStoredProcedure And Not IsDataTableAsList Then
                str_Filter = SetCondn(lstrOpr, ColumnName, txtSearch.Text)
            Else
                str_Filter = SetDTFilterCondn(lstrOpr, ColumnName, txtSearch.Text)
            End If
            getColumnFilterString = str_Filter
            FilterTextboxString &= ControlName & "=" & txtSearch.Text & "||"
        Catch ex As Exception
            getColumnFilterString = ""
        End Try
    End Function
    Private Sub SetHeaderFilterString()
        Try
            Dim ColStrFilter(), ColValue(), iVal As String
            ColStrFilter = FilterTextboxString.Split("||")
            Dim iTxtBox As TextBox = Nothing
            For Each iVal In ColStrFilter
                ColValue = iVal.Split("=")
                If ColValue.Length > 0 Then
                    If gvDetails.HeaderRow.FindControl(ColValue(0)) IsNot Nothing Then
                        iTxtBox = CType(gvDetails.HeaderRow.FindControl(ColValue(0)), TextBox)
                    End If
                    If ColValue.Length > 1 And iTxtBox IsNot Nothing Then
                        iTxtBox.Text = ColValue(1)
                    End If
                End If

            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SetSQLAndConnectionString(ByRef StrSQL As String, ByRef strConn As String, ByRef StrSortCol As String)
        Try
            strConn = ConnectionManger.GetOASISConnection.ConnectionString
            Dim SearchMode As String = Mainclass.cleanString(Request.QueryString("id"))
            Select Case SearchMode
                Case "GROUPLIST"
                    StrSQL = "select ID,DESCR, CODE from(select GRPA_ID ID,GRPA_DESCR DESCR, GRPA_ID CODE from GROUPSA  )A where 1=1"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Group  List"
                    TitleLabel.Text = "Group  List"
                Case "USERSEARCHAPPGRPUSER"
                    StrSQL = "select ID,DESCR,EMAIL,NAME,[ROLE] from(select um.usr_ID ID,um.USR_NAME DESCR,isnull(ED.EMD_EMAIL,'') EMAIL,Um.USR_DISPLAY_NAME NAME,UR.ROL_DESCR [ROLE] from oasis..USERS_M UM  left outer join oasis..EMPLOYEE_D ED on um.USR_EMP_ID =ed.emd_emp_id left outer join oasis..ROLES_M UR on um.USR_ROL_ID =UR.ROL_ID where usr_bsu_id='" & Mainclass.cleanString(Request.QueryString("bsuid")) & "') A where 1=1"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "User  List"
                    TitleLabel.Text = "User  List"
                Case "BUSINESSUNITAPPGRPUSER"
                    StrSQL = "select ID,DESCR, CODE from(select BSU_ID ID ,BSU_NAME DESCR, BSU_SHORTNAME CODE from oasis..BUSINESSUNIT_M where BSU_ID in (select distinct PRF_BSU_ID from oasis_pur_inv..prf_h where prf_deleted=0))A where 1=1"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Business Unit  List"
                    TitleLabel.Text = "Business Unit  List"
                Case "FINDOCTYPE"
                    StrSQL = "Select distinct * from(select DOC_ID ID, DOC_NAME DESCR from DOCUMENT_M where DOC_ID IN ('IC', 'BR', 'IJV', 'DN', 'CR', 'PP', 'QR', 'BP', 'CC', 'CP', 'OP', 'CN', 'JV', 'PJ') "
                    StrSQL &= " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    Page.Title = "Document Type"
                    TitleLabel.Text = "Document Type"
                    HideFirstColumn = False
                Case "ITMLIST"
                    HideFirstColumn = True
                    IsStoredProcedure = True
                    StrSQL = "dbo.GET_ITEM_LIST_FOR_PRICING"
                    ReDim SPParam(2)
                    If Not Session("sBSUID") Is Nothing Then
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBSUID"), SqlDbType.VarChar)
                        SPParam(1) = Mainclass.CreateSqlParameter("@PREFIX_TEXT", "", SqlDbType.VarChar)
                    End If
                    'StrSQL = "select ID,DESCR,ISBN from(select itm_descr descr, itm_id id, itm_isbn ISBN FROM ITEM_SALE left outer join ITEM_SALEBSU on ITM_ID=ITB_ITM_ID and ITB_BSU_ID='" & Session("sBSUID") & "' where cast(itm_id as varchar(8)) +cast(" & Session("sBsuid") & " as varchar(10) ) not in (select cast(ITB_ITM_id as varchar(8)) + ITB_BSU_ID  from ITEM_SALEBSU))  A where 1=1"
                    'strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "ITM_DESCR"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    Page.Title = "Item List"
                    TitleLabel.Text = "Item List"
                Case "ACCOUNTSLISTFORUTILITY"
                    StrSQL = "select ID,DESCR, CODE from(select ACT_ID ID ,act_name DESCR, act_id CODE from accounts_m  )A where 1=1"
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Accounts List"
                    TitleLabel.Text = "Accounts List"
                Case "ITMCLIENT"
                    If Mainclass.cleanString(Request.QueryString("jobtype")) = "Business Unit" Then
                        StrSQL = "Select ID,CODE,DESCR, xfdate, xedate from( SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID ID, BUSINESSUNIT_M.BSU_ID CODE, BUSINESSUNIT_M.BSU_NAME  DESCR, replace(convert(varchar(30), getdate(), 106),' ','/') xfdate, '31/dec/2050' xedate "
                        StrSQL &= "FROM vw_oso_SERVICES_BSU_M INNER JOIN  BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID    "
                        StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='900500') AND  (vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1)) A where A.ID<>''  "
                    ElseIf Mainclass.cleanString(Request.QueryString("jobtype")) = "Q" Or Mainclass.cleanString(Request.QueryString("jobtype")) = "N" Or Mainclass.cleanString(Request.QueryString("jobtype")) = "C" Then 'Non Framework effectively all
                        'StrSQL = "Select descr ID,CODE,DESCR,CONTACT,'' TERMS from ( SELECT cast(act_creditdays as varchar)+' days' TERMS, ACT_EMAIL EMAIL, ACT_ID ID,ACT_NAME DESCR,ACT_ID CODE, ACT_ADDRESS1,ACT_ADDRESS2 Address, ACT_CONTACTPERSON Contact, ACT_PHONE,ACT_MOBILE Phone FROM oasisfin.dbo.ACCOUNTS_M WHERE ACT_BACTIVE='TRUE' and ((ACT_CTRLACC='06301000' AND ACT_FLAG='s') or ACT_ID in (select ACT_ID from oasis_pur_inv..VW_OTHERS)) AND ACT_BSU_ID LIKE '%999998%' AND ACT_Bctrlac=0 and ACT_ID<>'063M0177' ) a where 1=1 "
                        'added on 06-dec-2018
                        'StrSQL = "Select descr ID,CODE,DESCR,CONTACT,'' TERMS from ( SELECT cast(act_creditdays as varchar)+' days' TERMS, ACT_EMAIL EMAIL, ACT_ID ID,ACT_NAME DESCR,ACT_ID CODE, ACT_ADDRESS1,ACT_ADDRESS2 Address, ACT_CONTACTPERSON Contact, ACT_PHONE,ACT_MOBILE Phone FROM oasisfin.dbo.ACCOUNTS_M WHERE ACT_BACTIVE='TRUE' and ((ACT_CTRLACC in ('08101000','06301000','08101500') AND ACT_FLAG in ('n','s')) or ACT_ID in (select ACT_ID from oasis_pur_inv..VW_OTHERS)) AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%' AND ACT_Bctrlac=0 and ACT_ID<>'063M0177' ) a where 1=1 "

                        StrSQL = "Select descr ID,CODE,DESCR,CONTACT,TERMS from ( SELECT cast(act_creditdays as varchar)+' days' TERMS, ACT_EMAIL EMAIL, ACT_ID ID,ACT_NAME DESCR,ACT_ID CODE, ACT_ADDRESS1,ACT_ADDRESS2 Address, ACT_CONTACTPERSON+ ', '+ ACT_CONTACTDESIG +'\n'+ACT_ADDRESS1+', '+ACT_ADDRESS2+', '+ CIT_DESCR+', '+ CTY_DESCR +'\n'+ACT_EMAIL Contact, ACT_PHONE,ACT_MOBILE Phone FROM oasisfin.dbo.ACCOUNTS_M left outer join oasis..COUNTRY_M with(nolock) on cty_id=ACT_COUNTRY left outer join oasis..CITY_M with(nolock) on cit_id=ACT_CITY  WHERE ACT_BACTIVE='TRUE' and ((ACT_CTRLACC in ('08101000','06301000','08101500') AND ACT_FLAG in ('n','s')) or ACT_ID in (select ACT_ID from oasis_pur_inv..VW_OTHERS)) AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%' AND ACT_Bctrlac=0 and ACT_ID<>'063M0177' ) a where 1=1 "

                    ElseIf Mainclass.cleanString(Request.QueryString("jobtype")) = "E" Then 'Non Framework effectively all
                        StrSQL = "Select descr ID,CODE,DESCR,CONTACT,EMAIL from ( SELECT cast(act_creditdays as varchar)+' days' TERMS, ACT_EMAIL EMAIL, ACT_ID ID,ACT_NAME DESCR,ACT_ID CODE, ACT_ADDRESS1,ACT_ADDRESS2 Address, ACT_CONTACTPERSON Contact, ACT_PHONE,ACT_MOBILE Phone FROM oasisfin.dbo.ACCOUNTS_M WHERE ACT_BACTIVE='TRUE' and ((ACT_CTRLACC='06301000' AND ACT_FLAG='s') or ACT_ID in (select ACT_ID from oasis_pur_inv..VW_OTHERS)) AND ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%' AND ACT_Bctrlac=0 ) a where 1=1 "
                    ElseIf Mainclass.cleanString(Request.QueryString("jobtype")) = "F" Then 'Framework only those with valid quotations
                        StrSQL = "Select descr ID,CODE,DESCR,CONTACT,TERMS from ( SELECT  AM.ACT_ID ID,AM.ACT_NAME DESCR,AM.ACT_ID CODE, QUO_CONTACT+'\n'+QUO_ADDRESS+'\nTel.'+QUO_PHONE+' Mob.'+QUO_MOBILE+' Fax.'+QUO_FAX Contact, QUO_TERMS Terms FROM oasisfin.dbo.ACCOUNTS_M AM INNER join oasisfin.dbo.ACCOUNTS_M AMC on AMC.ACT_ID=AM.ACT_CTRLACC INNER JOIN OASIS_PUR_INV.dbo.QUOTE_H on AM.ACT_ID=QUO_SUP_ID WHERE AM.ACT_BACTIVE='TRUE' and AM.ACT_BSU_ID LIKE '%" & Session("sBsuid") & "%' AND AM.ACT_Bctrlac=0 AND QUO_DELETED=0 and (QUO_DESCR='' or QUO_DESCR LIKE '%" & Session("BSU_CURRENCY") & "%' or QUO_DESCR LIKE '%" & Session("sBsuid") & "%') AND AM.ACT_FLAG='s') a where 1=1 "
                    ElseIf Mainclass.cleanString(Request.QueryString("jobtype")) = "P" Then 'Purchase
                        StrSQL = "Select CODE ID,CODE,DESCR from( SELECT ACT_ID CODE,ACT_NAME DESCR "
                        StrSQL &= "FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M WHERE ACT_Bctrlac='FALSE' AND ACT_BSU_ID Like '%" & Session("sBsuid") & "%' AND ACT_TYPE IN ('ASSET', 'INCOME') "
                        StrSQL &= "AND ACT_BACTIVE='TRUE' AND ACT_BANKCASH<>'CC') A "
                    Else
                        'StrSQL = "Select ID,CODE,DESCR from( SELECT  AM.ACT_ID ID,AM.ACT_ID CODE,AM.ACT_NAME DESCR FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M AM, oasisfin.dbo.ACCOUNTS_M AMC WHERE AM.ACT_Bctrlac='FALSE' AND AMC.ACT_ID=AM.ACT_CTRLACC AND AM.ACT_BSU_ID Like   '%" & Session("sBsuid") & "%'  AND AM.ACT_BACTIVE='TRUE' AND AM.ACT_BANKCASH<>'CC' union SELECT  '','','STANDARD QUOTATION') A where A.ID<>'' "
                        StrSQL = "Select ID,CODE,DESCR DESCRIPTION,SALESMAN from( SELECT  cast(AM.ACT_ID AS varchar)+';'+cast(isnull(CNH_SALESMAN_ID,0) as varchar)+';'+isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') ID,AM.ACT_ID CODE,AM.ACT_NAME DESCR, isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') SALESMAN "
                        StrSQL &= "FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M AM INNER JOIN oasisfin.dbo.ACCOUNTS_M AMC ON AMC.ACT_ID=AM.ACT_CTRLACC "
                        StrSQL &= "left outer join TPTCONTRACT_H  on AM.ACT_ID=CNH_CLIENT_ID left OUTER JOIN OASIS.dbo.EMPLOYEE_M on EMP_ID=CNH_SALESMAN_ID "
                        StrSQL &= "WHERE AM.ACT_CTRLACC='06301000' and AM.ACT_Bctrlac='FALSE' AND AM.ACT_BSU_ID Like  '%" & Session("sBsuid") & "%'  "
                        StrSQL &= "AND AM.ACT_BACTIVE='TRUE' AND AM.ACT_BANKCASH<>'CC' UNION SELECT '00000000;0', '00000000', 'STANDARD CONTRACT', '') A where A.ID<>'' "
                    End If
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "ID"
                    Page.Title = IIf(Mainclass.cleanString(Request.QueryString("title")) <> "", Mainclass.cleanString(Request.QueryString("title")), "Client")
                    TitleLabel.Text = IIf(Mainclass.cleanString(Request.QueryString("title")) <> "", Mainclass.cleanString(Request.QueryString("title")), "Client")
                Case "ITMACCOUNT"
                    If Mainclass.cleanString(Request.QueryString("bsu_id")) Is Nothing Then
                        StrSQL = "Select ID,CODE,DESCR, FORMAT([MTD BUDGET],'#,###,##0.00' )[MTD BUDGET], FORMAT([YTD BUDGET],'#,###,##0.00' )[YTD BUDGET] from(select VW_ACT_ID ID, VW_ACT_ID CODE, ACT_NAME DESCR, isnull(xBUDMTD,0) [MTD BUDGET], isnull(xBUDYTD,0) [YTD BUDGET] FROM vw_PRF_ACTUAL inner join oasisfin.dbo.accounts_m on ACT_ID=VW_ACT_ID where ACT_BACTIVE='TRUE' and BSU_ID='" & Session("sBsuid") & "' and fyear='" & Session("F_YEAR") & "' AND MTH=month('" & Mainclass.cleanString(Request.QueryString("date")) & "') AND DPT_ID='" & Mainclass.cleanString(Request.QueryString("dpt_id")) & "') A where A.ID<>'' "
                    Else
                        StrSQL = "Select ID,CODE,DESCR, FORMAT([MTD BUDGET],'#,###,##0.00' )[MTD BUDGET], FORMAT([YTD BUDGET],'#,###,##0.00' )[YTD BUDGET] from(select VW_ACT_ID ID, VW_ACT_ID CODE, ACT_NAME DESCR, isnull(xBUDMTD,0) [MTD BUDGET], isnull(xBUDYTD,0) [YTD BUDGET] FROM vw_PRF_ACTUAL inner join oasisfin.dbo.accounts_m on ACT_ID=VW_ACT_ID where ACT_BACTIVE='TRUE' and BSU_ID='" & Mainclass.cleanString(Request.QueryString("bsu_id")) & "' and fyear='" & Session("F_YEAR") & "' AND MTH=month('" & Mainclass.cleanString(Request.QueryString("date")) & "') AND DPT_ID='" & Mainclass.cleanString(Request.QueryString("dpt_id")) & "') A where A.ID<>'' "
                    End If

                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Budget Account"
                    TitleLabel.Text = "Budget Account"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ITMITEM"
                    If Mainclass.cleanString(Request.QueryString("Supplier")) = "none" Then 'non framework
                        StrSQL = "select ID, COMMODITY, DESCRIPTION, RATE from ("
                        StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING DESCRIPTION, cast(isnull(ITM_RATE,0) as varchar) RATE "
                        StrSQL &= "FROM ITEM left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                        StrSQL &= "where ITM_ID NOT in (SELECT distinct QUD_ITM_ID FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID and (QUO_DESCR='' or QUO_DESCR LIKE '%" & Session("BSU_CURRENCY") & "%' or QUO_DESCR LIKE '%" & Session("sBsuid") & "%'))) a where 1=1 "
                        If Mainclass.cleanString(Request.QueryString("DptId")) = "302" Then
                            StrSQL = "select ID, COMMODITY, DESCRIPTION from ("
                            StrSQL &= "select distinct ACM_DESCR COMMODITY, ASC_ID AS ID,ASC_DESCR [DESCRIPTION], '' RATE "
                            StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID and ACT_ID='" & Mainclass.cleanString(Request.QueryString("ActId")) & "'"
                            StrSQL &= ") a where 1=1 "
                        Else
                            StrSQL = "select ID, COMMODITY, DESCRIPTION from ("
                            StrSQL &= "select ACM_DESCR COMMODITY, ASC_ID AS ID,ASC_DESCR [DESCRIPTION], '' RATE "
                            StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID and ACT_ID=''"
                            StrSQL &= ") a where 1=1 "
                        End If
                    ElseIf Mainclass.cleanString(Request.QueryString("Supplier")) = "063C0053" Or Mainclass.cleanString(Request.QueryString("Type")) = "C" Then 'capex
                        StrSQL = "select ID, CATEGORY, DESCRIPTION from ("
                        StrSQL &= "select ACM_DESCR CATEGORY, ASC_ID AS ID,ASC_DESCR [DESCRIPTION], '' RATE "
                        StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID and ACM_ID=26 "
                        StrSQL &= ") a WHERE 1=1 "
                    ElseIf Mainclass.cleanString(Request.QueryString("Supplier")) = "" Then 'non framework ???
                        StrSQL = "select ID, COMMODITY, DESCRIPTION, RATE, xQUO_SUP_ID, xACT_NAME from ( "
                        StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING DESCRIPTION, "
                        StrSQL &= "cast(cast(QUD_RATE as decimal(12,3)) as varchar) Rate, ACT_NAME+';'+QUO_TERMS+';'+QUO_CONTACT+'\n'+QUO_ADDRESS+'\nTel.'+QUO_PHONE+' Mob.'+QUO_MOBILE+' Fax.'+QUO_FAX xACT_NAME, QUO_SUP_ID xQUO_SUP_ID  "
                        StrSQL &= "FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID AND QUO_DELETED=0 AND QUD_DELETED=0 and (QUO_DESCR='' or QUO_DESCR LIKE '%" & Session("BSU_CURRENCY") & "%' or QUO_DESCR LIKE '%" & Session("sBsuid") & "%') INNER JOIN ITEM on ITM_ID=QUD_ITM_ID "
                        StrSQL &= "left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                        StrSQL &= "INNER JOIN OASISFIN.dbo.ACCOUNTS_M on QUO_SUP_ID=ACT_ID "
                        StrSQL &= ") a where 1=1 "
                    ElseIf Mainclass.cleanString(Request.QueryString("Supplier")) = "063C0053" Or Mainclass.cleanString(Request.QueryString("Type")) = "C" Then 'capex
                        StrSQL = "select ID, CATEGORY, DESCRIPTION from ("
                        StrSQL &= "select ACM_DESCR CATEGORY, ASC_ID AS ID,ASC_DESCR [DESCRIPTION], '' RATE "
                        StrSQL &= "from oasis_asset.ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN oasis_asset.ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID and ACM_ID=26 "
                        StrSQL &= ") a WHERE 1=1 "
                    ElseIf Mainclass.cleanString(Request.QueryString("Supplier")) = "" Then 'non framework ???
                        StrSQL = "select ID, COMMODITY, DESCRIPTION, RATE, xQUO_SUP_ID, xACT_NAME from ( "
                        StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING DESCRIPTION, "
                        StrSQL &= "cast(cast(QUD_RATE as decimal(12,3)) as varchar) Rate, ACT_NAME+';'+QUO_TERMS+';'+QUO_CONTACT+'\n'+QUO_ADDRESS+'\nTel.'+QUO_PHONE+' Mob.'+QUO_MOBILE+' Fax.'+QUO_FAX xACT_NAME, QUO_SUP_ID xQUO_SUP_ID  "
                        StrSQL &= "FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID AND QUO_DELETED=0 AND QUD_DELETED=0 and (QUO_DESCR='' or QUO_DESCR LIKE '%" & Session("BSU_CURRENCY") & "%' or QUO_DESCR LIKE '%" & Session("sBsuid") & "%') INNER JOIN ITEM on ITM_ID=QUD_ITM_ID "
                        StrSQL &= "left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                        StrSQL &= "INNER JOIN OASISFIN.dbo.ACCOUNTS_M on QUO_SUP_ID=ACT_ID "
                        StrSQL &= ") a where 1=1 "
                    Else
                        StrSQL = "select ID, COMMODITY, DESCRIPTION, RATE from (" 'framework
                        StrSQL &= "SELECT ITM_ID ID, UCO_DESCR COMMODITY, ITM_DESCR+' '+ITM_DETAILS+' '+ITM_PACKING+' '+ITM_UNIT DESCRIPTION, "
                        StrSQL &= "cast(cast(QUD_RATE as decimal(12,3)) as varchar) Rate "
                        StrSQL &= "FROM QUOTE_H inner JOIN QUOTE_D on QUO_ID=QUD_QUO_ID AND QUO_DELETED=0 AND QUD_DELETED=0 and (QUO_DESCR='' or QUO_DESCR LIKE '%" & Session("BSU_CURRENCY") & "%' or QUO_DESCR LIKE '%" & Session("sBsuid") & "%') INNER JOIN ITEM on ITM_ID=QUD_ITM_ID left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                        StrSQL &= "WHERE QUO_SUP_ID='" & Mainclass.cleanString(Request.QueryString("Supplier")) & "' "
                        StrSQL &= ") a where 1=1 "
                    End If
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

                Case "ITMITEM_NEW"


                    IsStoredProcedure = True
                    StrSQL = "GET_ITEM_FOR_FRAMEWORK_NONFRAMEWORK"
                    ReDim SPParam(6)


                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@ActId", Mainclass.cleanString(Request.QueryString("ActId")), SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@Type", Mainclass.cleanString(Request.QueryString("Type")), SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@BSU_CURRENCY", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(4) = Mainclass.CreateSqlParameter("@Supplier", Mainclass.cleanString(Request.QueryString("Supplier")), SqlDbType.VarChar)
                    SPParam(5) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    SPParam(6) = Mainclass.CreateSqlParameter("@DPT_ID", Mainclass.cleanString(Request.QueryString("DptId")), SqlDbType.VarChar)





                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString


                Case "ITMITEMSALE"
                    HideFirstColumn = True
                    IsStoredProcedure = True
                    StrSQL = "dbo.GET_ITEM_FOR_SALE"
                    ReDim SPParam(3)
                    If Not Request.QueryString("sBsuid") Is Nothing Then
                        Dim BSUID As String = Mainclass.cleanString(Request.QueryString("sBsuid"))
                        SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSUID, SqlDbType.VarChar)
                    End If
                    If Not Request.QueryString("bTaxable") Is Nothing Then
                        Dim bTaxable As Boolean = Convert.ToBoolean(Mainclass.cleanString(Request.QueryString("bTaxable")))
                        SPParam(1) = Mainclass.CreateSqlParameter("@bSTATIONARY", bTaxable, SqlDbType.Bit)
                    End If
                    If Not Request.QueryString("saletype") Is Nothing Then
                        Dim SaleType As String = Mainclass.cleanString(Request.QueryString("saletype"))
                        SPParam(2) = Mainclass.CreateSqlParameter("@SALETYPE", SaleType, SqlDbType.VarChar)
                    End If
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "" 'SORT_ORDER DESC
                    gvDetails.PageSize = 20
                    'StrSQL = "select ID, ISBN, DESCRIPTION, SELL, COST from ( "
                    'StrSQL &= "SELECT ITM_ID ID, ITM_ISBN ISBN, isnull(ITB_DESCR,ITM_DESCR) DESCRIPTION, cast(cast(ITB_SELL as decimal(12,3)) as varchar) SELL, cast(cast(ITB_COST as decimal(12,3)) as varchar) COST "
                    'StrSQL &= "FROM ITEM_SALE inner JOIN ITEM_SALEBSU on ITM_ID=ITB_ITM_ID "
                    'If Mainclass.cleanString(Request.QueryString("saletype")) <> "A" Then StrSQL &= "INNER JOIN SAL_C on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>0 "
                    'StrSQL &= "WHERE ITB_BSU_ID='" & Mainclass.cleanString(Request.QueryString("sBsuid")) & "' "
                    'StrSQL &= "UNION ALL "
                    'StrSQL &= "select -SEH_ID,'',SEH_DESCR, cast(cast(sum(ITB_SELL*SED_QTY) as decimal(12,3)) as varchar), cast(cast(sum(ITB_COST*SED_QTY) as decimal(12,3))AS varchar) "
                    'StrSQL &= "from SET_H inner JOIN SET_D on SEH_ID=SED_SEH_ID INNER JOIN ITEM_SALE on ITM_ID=SED_ITM_ID inner JOIN ITEM_SALEBSU on ITM_ID=ITB_ITM_ID AND ITB_BSU_ID=SEH_BSU_ID "
                    'If Mainclass.cleanString(Request.QueryString("saletype")) <> "A" Then StrSQL &= "INNER JOIN SAL_C on ITB_BSU_ID=SAC_BSU_ID AND ITM_ID=SAC_ITM_ID AND SAC_QTY>=SED_QTY "
                    'StrSQL &= "where SEH_BSU_ID='" & Mainclass.cleanString(Request.QueryString("sBsuid")) & "' group by SEH_ID, SEH_DESCR "
                    'StrSQL &= ") a where 1=1 "
                    'strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString

                Case "ITMUNSPSCSEG"
                    StrSQL = "Select ID,SEGMENT from("
                    StrSQL &= "SELECT distinct UNSPSC_SEGMENT.USG_ID ID, USG_DESCR SEGMENT FROM ITEM left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                    StrSQL &= "INNER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= ") A where ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "SEGMENT"
                    Page.Title = "SEGMENT CODES"
                    TitleLabel.Text = "SEGMENT CODES"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ITMUNSPSC"
                    StrSQL = "Select top 100 CODE,COMMODITY,SEGMENT,FAMILY,CLASS from("
                    StrSQL &= "SELECT UCO_CODE CODE, USG_DESCR SEGMENT, USF_DESCR FAMILY, UCL_DESCR CLASS, UCO_DESCR COMMODITY "
                    StrSQL &= "FROM UNSPSC_COMMODITY INNER JOIN UNSPSC_SEGMENT on UNSPSC_COMMODITY.USG_ID=UNSPSC_SEGMENT.USG_ID "
                    StrSQL &= "INNER JOIN UNSPSC_FAMILY on UNSPSC_COMMODITY.USF_ID=UNSPSC_FAMILY.USF_ID "
                    StrSQL &= "INNER JOIN UNSPSC_CLASS on UNSPSC_CLASS.UCL_ID=UNSPSC_COMMODITY.UCL_ID "
                    If Mainclass.cleanString(Request.QueryString("cat")) <> "" Then StrSQL &= "AND UNSPSC_SEGMENT.USG_ID IN (" & Mainclass.cleanString(Request.QueryString("cat").Replace("|", ",")) & ") AND UCO_CODE IN (SELECT ITM_UNSPSC from ITEM) "
                    StrSQL &= ") A where CODE<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "COMMODITY"
                    Page.Title = "UNSPSC CODES"
                    TitleLabel.Text = "UNSPSC CODES"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ITMCATEGORY"
                    StrSQL = "Select ID,DESCR,TYPE from(select ICM_ID ID, ICM_DESCR DESCR, CASE WHEN ICM_TYPE='A' then 'Asset' ELSE 'Inventory' end TYPE FROM ITEM_CATEGORY_M) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "ITEM CATEGORY"
                    TitleLabel.Text = "ITEM CATEGORY"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ITMSUBCATEGORY"
                    Dim sqlWhere As String
                    If Val(Mainclass.cleanString(Request.QueryString("category"))) > 0 Then sqlWhere = " and xICM_ID=" & Mainclass.cleanString(Request.QueryString("category"))
                    StrSQL = "Select ID,CATEGORY,SUBCATEGORY, xICM_ID from(select ISC_ID ID, ICM_DESCR CATEGORY, ISC_DESCR SUBCATEGORY, ICM_ID xICM_ID FROM ITEM_SUBCATEGORY_M inner JOIN ITEM_CATEGORY_M on ISC_ICM_ID=ICM_ID) A where A.ID<>'' " & sqlWhere
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "SUBCATEGORY"
                    Page.Title = "ITEM SUBCATEGORY"
                    TitleLabel.Text = "ITEM SUBCATEGORY"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ITMMAKE"
                    StrSQL = "Select ID,DESCR from(select MKE_ID ID, MKE_DESCR DESCR FROM ITEM_MAKE) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "ITEM BRAND\MAKE"
                    TitleLabel.Text = "ITEM BRAND\MAKE"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ITMMODEL"
                    StrSQL = "Select ID,DESCR from(select MDL_ID ID, MDL_DESCR DESCR FROM ITEM_MODEL) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "ITEM SUB BRAND\MODEL\YEAR"
                    TitleLabel.Text = "ITEM SUB BRAND\MODEL\YEAR"
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                Case "ASSETCATEGORY"
                    StrSQL = "Select ID,DESCR from(select ACM_ID AS ID,ACM_DESCR AS DESCR from ASSETS.[ASSET_CATEGORY_M] ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Asset Category"
                    TitleLabel.Text = "Asset Category"
                Case "ASSETSUBCATEGORY"
                    Dim CAT As String = Mainclass.cleanString(Request.QueryString("cat"))
                    Dim whrQuery As String = ""
                    If CAT = "" Then
                        whrQuery = " where 1=1 "
                    Else
                        CAT = CAT.Replace("|", ",")
                        whrQuery = " where ACM_ID in (" & CAT & ")  "
                    End If
                    StrSQL = "Select ID,DESCR,CATEGORY from(select ASC_ID AS ID,ASC_DESCR AS DESCR,ACM_DESCR [CATEGORY] from ASSETS.[ASSET_SUBCATEGORY_M] INNER JOIN ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID " & whrQuery & "  ) A where A.ID<>''  "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Asset Sub Category"
                    TitleLabel.Text = "Asset Sub Category"
                Case "ASSETMAKE"
                    Dim SUBCAT As String = Mainclass.cleanString(Request.QueryString("subcat"))
                    Dim whrQuery As String = ""
                    whrQuery = " WHERE " & getLikeWhereQuerry("MKE_ASC_IDs", SUBCAT)
                    StrSQL = "Select distinct ID,DESCR from(select MKE_ID [ID],MKE_DESCR [DESCR] FROM ASSETS.ASSET_MAKE " & whrQuery & " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Asset Make"
                    TitleLabel.Text = "Asset Make"
                Case "ASSETMODEL"
                    Dim SUBCAT As String = Mainclass.cleanString(Request.QueryString("subcat"))
                    Dim MAKE As String = Mainclass.cleanString(Request.QueryString("make"))
                    Dim whrQuery As String = ""
                    whrQuery = " WHERE " & getLikeWhereQuerry("MDL_ASC_ID", SUBCAT)
                    whrQuery &= " AND " & getLikeWhereQuerry("MDL_MKE_IDs", MAKE)
                    StrSQL = "Select distinct ID,DESCR from(select MDL_ID AS ID,MDL_DESCR AS DESCR from ASSETS.ASSET_MODEL " & whrQuery & " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Asset Model"
                    TitleLabel.Text = "Asset Model"
                Case "EMPDEPT"
                    IsStoredProcedure = False
                    MultiSelect = False
                    StrSQL = "SELECT EMP_ID,NAME,[DEPARTMENT] FROM (SELECT EMP_ID,isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') NAME ,DPT_DESCR [DEPARTMENT] from OASIS..EMPLOYEE_M " & _
                    "INNER JOIN OASIS..DEPARTMENT_M  ON DPT_ID =EMP_DPT_ID where EMP_BSU_ID='" & Session("sBsuid") & "') AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = " A.NAME "
                    Page.Title = "Employee List"
                    TitleLabel.Text = "Employee List"
                Case "REAGENT"
                    Dim BSUID As String = Mainclass.cleanString(Request.QueryString("BSUID"))
                    IsStoredProcedure = False
                    MultiSelect = False
                    If BSUID = "999998" Then
                        StrSQL = "SELECT ID,DESCR FROM (select REA_ID ID,REA_NAME DESCR FROM REAGENT WHERE REA_BSU_ID='999998') AS A WHERE 1=1 "
                    Else
                        StrSQL = "SELECT ID,DESCR,CONTACT,MOBILE FROM ("
                        StrSQL &= "select REA_ID ID,REA_NAME DESCR,REA_CONTACT_PERSON CONTACT,REA_MOBILE MOBILE from REAGENT WHERE REA_BSU_ID='" & Session("sBsuid") & "' "
                        StrSQL &= "union select 0, ' NONE','NA','NA' "
                        StrSQL &= "union all select REA_ID ID,REA_NAME DESCR,REA_CONTACT_PERSON CONTACT,REA_MOBILE MOBILE from REAGENT WHERE REA_BSU_ID='999998' and rea_id not in "
                        StrSQL &= "(select rea_rea_id from REAGENT WHERE REA_BSU_ID='" & Session("sBsuid") & "')) AS A WHERE 1=1"
                    End If
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Real Estate Agent List"
                    TitleLabel.Text = "Real Estate Agent List"
                Case "RELANDLORD"
                    Dim BSUID As String = Mainclass.cleanString(Request.QueryString("BSUID"))
                    IsStoredProcedure = False
                    MultiSelect = False
                    If BSUID = "999998" Then
                        StrSQL = "SELECT ID,DESCR FROM (select REL_ID ID,REL_NAME DESCR from RELANDLORD WHERE REL_BSU_ID='999998' ) AS A WHERE 1=1 "
                    Else
                        StrSQL = "SELECT ID,DESCR,CONTACT,MOBILE FROM ("
                        StrSQL &= "select REL_ID ID,REL_NAME DESCR,REL_NAME CONTACT,REL_MOBILE MOBILE from RELANDLORD WHERE REL_BSU_ID='" & Session("sBsuid") & "' "
                        StrSQL &= "union all "
                        StrSQL &= "select REL_ID ID,REL_NAME DESCR,REL_NAME CONTACT,REL_MOBILE MOBILE from RELANDLORD WHERE REL_BSU_ID='999998' and rel_id not in "
                        StrSQL &= "(select rel_rel_id from RELANDLORD WHERE REL_BSU_ID='v')) AS A WHERE 1=1 "
                    End If
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Landlord List"
                    TitleLabel.Text = "Landlord List"
                Case "REAREA"
                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim whereCity As String = "1=1"
                    If Not Mainclass.cleanString(Request.QueryString("CIT_ID")) Is Nothing Then
                        whereCity = " RAR_CIT_ID='" & Mainclass.cleanString(Request.QueryString("CIT_ID")) & "'"
                    End If
                    StrSQL = "SELECT ID,DESCR, CITY FROM (select RAR_ID ID,RAR_NAME DESCR, RCM_DESCR CITY from REAREA LEFT OUTER JOIN RECITY_M ON RCM_ID=RAR_CIT_ID WHERE RAR_DELETED=0 and " & whereCity & ") AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Area List"
                    TitleLabel.Text = "Area List"
                Case "REBUILDING"
                    IsStoredProcedure = False
                    MultiSelect = False
                    StrSQL = "SELECT ID,DESCR,DMNO,ADDRESS,[AREA NAME],[AREA] FROM (select BLD_ID ID,BLD_NAME DESCR,BLD_DMNO DMNO ,BLD_ADDRESS ADDRESS,RAR_NAME [AREA NAME],BLD_RAR_ID AREA from BUILDING INNER JOIN  REAREA ON RAR_ID=BLD_RAR_ID  WHERE BLD_BSU_ID='" & Session("sBsuid") & "' AND BLD_DELETED=0 ) AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Building List"
                    TitleLabel.Text = "Building List"
                Case "TENANCYEMPLOYEE"
                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim Sbsu As String = ""
                    Dim Lbsu As String = ""
                    Sbsu = Mainclass.cleanString(Request.QueryString("SBsuid").ToString())
                    Lbsu = Mainclass.cleanString(Request.QueryString("LBsuid").ToString())

                    StrSQL = "SELECT EMP_ID,NAME,DESIGNATION,[MARTIAL STATUS],DEPENDANTS,DESID FROM ("
                    If Sbsu <> Lbsu Then
                        StrSQL &= "SELECT EMP_ID,NAME,''[DESIGNATION],''[MARTIAL STATUS],''[DEPENDANTS],0 [DESID]"
                    Else
                        StrSQL &= "SELECT EMP_ID,NAME,[DESIGNATION],[MARTIAL STATUS],[DEPENDANTS],[DESID]"
                    End If
                    StrSQL &= " FROM (select e1.EMP_ID,EMP_NAME NAME,EMP_DES_DESCR DESIGNATION,CASE WHEN EMP_MARITALSTATUS IN('0','MAR','Married') THEN 'MARRIED' ELSE  "
                    StrSQL &= " CASE WHEN EMP_MARITALSTATUS IN('4') THEN 'SEPARATED' ELSE  CASE WHEN EMP_MARITALSTATUS IN('2','WID') THEN 'WIDOWED' ELSE  "
                    StrSQL &= " CASE WHEN EMP_MARITALSTATUS IN('3','DIV') THEN 'DIVORCED' ELSE 'SINGLE' END END END END  [MARTIAL STATUS],ISNULL((SELECT COUNT(*)  "
                    StrSQL &= " FROM OASIS..EMPDEPENDANTS_D WHERE EDD_EMP_ID =e1.EMP_ID),0) DEPENDANTS,EMP_DES_ID [DESID] "
                    StrSQL &= " from oasis_docs..VW_EMPLOYEE_V e1 inner join oasis..employee_m e2 on e1.emp_id=e2.emp_id "
                    StrSQL &= " WHERE e1.EMP_BSU_ID ='" & Sbsu & "' or e1.EMP_WORK_BSU_ID ='" & Sbsu & "' ) AS A WHERE 1=1 "
                    'If Sbsu = Lbsu Then
                    '    StrSQL &= " union all  select 0 EMP_ID, BSU_NAME NAME,''[DESIGNATION],''[MARTIAL STATUS],''[DEPENDANTS],0 [DESID] from oasis..BUSINESSUNIT_M where BSU_ID='" & Lbsu & "'"
                    'End If
                    StrSQL &= " union all select 0,'TBC','','','',0 union all select 1,'Vacant','','','',0 "
                    StrSQL &= ") AS A WHERE 1=1 "
                    'StrSQL &= ") AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "EMP_ID"
                    Page.Title = "Employee List"
                    TitleLabel.Text = "Employee List"
                Case "TENANCYDESIGNATION"
                    IsStoredProcedure = False
                    MultiSelect = False

                    StrSQL = "select ID,DESCR from (select DES_ID ID ,DES_DESCR DESCR  from oasis..EMPDESIGNATION_M )as A where 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Designation List"
                    TitleLabel.Text = "Designation List"
                Case "EXPENSES"
                    IsStoredProcedure = False
                    MultiSelect = False
                    StrSQL = "SELECT ID,DESCR FROM (SELECT PUR_ID ID,PUR_DESCR DESCR from ADVREQPURPOSE ) AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Expense List"
                    TitleLabel.Text = "Expense List"
                Case "PAYMENT"
                    IsStoredProcedure = False
                    MultiSelect = False
                    StrSQL = "SELECT ID,[DOC NO], TYPE, DESCR FROM (SELECT JHD_DOCNO ID, JHD_DOCNO [DOC NO], JHD_DOCTYPE TYPE, JHD_NARRATION DESCR from OASISFIN..JOURNAL_H where JHD_FYEAR='" & Session("F_YEAR") & "' and JHD_BSU_ID='" & Session("sBsuid") & "' and JHD_DOCTYPE in ('BP','CP')  AND JHD_bPOSTED=1 and JHD_bDELETED=0) AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Payment List"
                    TitleLabel.Text = "Payment List"
                Case "HHTDATA"
                    Dim BSU As String = Mainclass.cleanString(Request.QueryString("bsu"))
                    Dim TransferType As String = Mainclass.cleanString(Request.QueryString("type"))
                    StrSQL = "select ceh_id, ceh_id [TRAN ID], replace(convert(varchar(30), ceh_Date, 106),' ','/') [DATE], "
                    StrSQL &= "tym_desc [TRANSFER TYPE], LOC_DESCR [LOCATION], isnull(CASE when CEH_TYPE=2 then EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME ELSE CASE when CEH_TYPE=4 THEN BSU_NAME ELSE '' end end,'') Details "
                    StrSQL &= "from ASSETS.TRANSFERCE_H INNER JOIN ASSETS.TRANSFERTYPE_M on CEH_TYPE=TYM_ID "
                    StrSQL &= "INNER JOIN ASSETS.LOCATION_M on CEH_LOC_ID=LOC_ID "
                    StrSQL &= "left JOIN VW_EMPLOYEE ON CEH_OTH_ID=EMP_ID "
                    StrSQL &= "LEFT JOIN BUSINESSUNIT_M on cast(CEH_OTH_ID AS varchar)=BSU_ID WHERE CEH_USED_DATE is null and CEH_BSU_ID='" & BSU & "' and CEH_TYPE=" & TransferType
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    Page.Title = "Hand Held Terminal Data"
                    TitleLabel.Text = "Hand Held Terminal Data"
                Case "LOCFACILITY"
                    StrSQL = "Select ID,DESCR from(SELECT LFM_ID ID,LFM_DESCR DESCR FROM ASSETS.[ASSET_LOC_FACILITY]) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Location Facilities"
                    TitleLabel.Text = "Location Facilities"
                Case "ASSETLOCATION"
                    Dim BSU As String = Session("sBsuid")
                    Dim whrQuery As String = ""
                    If BSU Is Nothing Then
                        whrQuery = " where 1=1 "
                    Else
                        whrQuery = " where LOC_BSU_ID='" & BSU & "'"
                    End If
                    StrSQL = "Select ID,DESCR,TYPE,[OWN TYPE] from(SELECT LOC_ID ID,LOC_DESCR DESCR,LTM_DESC  [TYPE], LOM_DESC   [OWN TYPE] FROM ASSETS.[LOCATION_M] LEFT OUTER JOIN ASSETS.LOCATIONTYPE_M ON LOC_TYPE=LTM_ID LEFT OUTER JOIN ASSETS.LOCATIONOWNTYPE_M ON LOM_ID=LOC_OWN_TYP   " & whrQuery & " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Asset Location"
                    TitleLabel.Text = "Asset Location"
                Case "ASSETMASTER"
                    Dim whrQuery As String = ""
                    Dim BSU As String = Mainclass.cleanString(Request.QueryString("bsu"))
                    Dim loc As String = Mainclass.cleanString(Request.QueryString("loc"))
                    Dim SUBCAT As String = Mainclass.cleanString(Request.QueryString("subcat"))
                    Dim MAKE As String = Mainclass.cleanString(Request.QueryString("make"))
                    Dim MODEL As String = Mainclass.cleanString(Request.QueryString("model"))
                    If SUBCAT = "" Then
                        whrQuery = " where 1=1 "
                    Else
                        SUBCAT = SUBCAT.Replace("|", ",")
                        MAKE = MAKE.Replace("|", "','")
                        MODEL = MODEL.Replace("|", "','")
                        whrQuery = " where ASM_ASC_ID in (" & SUBCAT & ") and ASM_MKE_ID in ('" & MAKE & "') and ASM_MDL_ID in ('" & MODEL & "')"
                        'whrQuery = " where 1=1 "
                    End If
                    StrSQL = "Select  ID,[CATEGORY],[SUB CATEGORY], DESCR,"
                    StrSQL &= " [MODEL], CODE,[LOCATION],[EMPLOYEE NAME] from(select ASM_ID AS ID,ACM_DESCR [CATEGORY],ASC_DESCR [SUB CATEGORY],MKE_DESCR + ' - ' + MDL_DESCR [MODEL],"
                    StrSQL &= "   ASM_CODE CODE,ASM_DESCR  AS DESCR ,LOC_DESCR [LOCATION]"
                    StrSQL &= " ,EMP_PASSPORTNAME [EMPLOYEE NAME] "
                    StrSQL &= " FROM ASSETS.[ASSETS_M] "
                    StrSQL &= " LEFT OUTER JOIN ASSETS.ASSET_SUBCATEGORY_M ON  ASM_ASC_ID=ASC_ID LEFT OUTER JOIN ASSETS.ASSET_CATEGORY_M ON ASC_ACM_ID=ACM_ID  "
                    StrSQL &= " LEFT OUTER JOIN ASSETS.ASSET_MAKE  ON MKE_ID =ASM_MKE_ID  "
                    StrSQL &= " LEFT OUTER JOIN ASSETS.ASSET_MODEL  ON MDL_ID =ASM_MDL_ID "
                    StrSQL &= " LEFT OUTER JOIN ASSETS.LOCATION_M ON LOC_ID=ASM_LOC_ID "
                    StrSQL &= " LEFT OUTER JOIN OASIS.dbo.EMPLOYEE_M ON EMP_ID IN (SELECT TOP 1 TRD_EMP_ID  from ASSETS.TRANSFER_D where TRD_ASM_ID = ASSETS.[ASSETS_M].ASM_ID ORDER BY TRD_FROMDT desc) "
                    StrSQL &= " " & whrQuery & " and ASM_STATUS=0 and  ASM_BSU_ID ='" & BSU & "' "
                    If Not loc Is Nothing And loc <> "" Then StrSQL &= "  and  ASM_LOC_ID ='" & loc & "'  "
                    StrSQL &= "  ) A where A.ID<>''  "
                    strConn = ConnectionManger.GetOASIS_ASSETConnectionString
                    HideFirstColumn = True
                    StrSortCol = "DESCR"
                    Page.Title = "Asset"
                    Page.Title = "Asset Category"
                    TitleLabel.Text = "Asset Category"
                Case "INVSUBCATEGORY"
                    Dim CAT As String = Mainclass.cleanString(Request.QueryString("cat"))
                    Dim whrQuery As String = ""
                    If CAT = "" Then
                        whrQuery = " where 1=1 "
                    Else
                        CAT = CAT.Replace("|", ",")
                        whrQuery = " where ICM_ID in (" & CAT & ")  "
                    End If
                    StrSQL = "Select ID,DESCR,CATEGORY from(select ISC_ID AS ID,ISC_DESCR AS DESCR,ICM_DESCR [CATEGORY] from [ITEM_SUBCATEGORY_M] INNER JOIN ITEM_CATEGORY_M ON ISC_ICM_ID=ICM_ID " & whrQuery & "  ) A where A.ID<>''  "
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Sub Category"
                    TitleLabel.Text = "Sub Category"
                Case "INVITEMS"
                    Dim whrQuery As String = ""
                    Dim isIT As String = Mainclass.cleanString(Request.QueryString("isIT"))
                    If isIT = "" Then
                        whrQuery = " where 1=1 "
                    Else
                        whrQuery = " where ITM_bITItem = " & isIT
                    End If
                    StrSQL = "Select distinct ID,DESCR,[CATEGORY],[SUB CATEGORY] from(select distinct ITM_ID ID,ITM_DESCR DESCR,ICM_DESCR [CATEGORY],ISC_DESCR [SUB CATEGORY] from INVITEMS_M INNER JOIN ITEM_SUBCATEGORY_M ON ITM_ISC_ID = ISC_ID INNER JOIN ITEM_CATEGORY_M ON ISC_ICM_ID = ICM_ID " & whrQuery & "   ) A where A.ID<>'' "
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Items"
                    TitleLabel.Text = "Items"
                Case "BSUDPT"
                    Dim whrQuery As String = ""
                    Dim BSU As String = Mainclass.cleanString(Request.QueryString("bsu"))
                    StrSQL = "Select distinct ID,DESCR from(SELECT DPT_ID ID, DPT_DESCR DESCR FROM  OASIS.dbo.DEPARTMENT_M where DPT_ID in (select distinct DPT_ID from employee_M where EMP_BSU_ID='" & BSU & "') ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Department"
                    TitleLabel.Text = "Department"
                Case "BSUUSER"
                    Dim whrQuery As String = ""
                    Dim BSU As String = Mainclass.cleanString(Request.QueryString("bsu"))
                    StrSQL = "Select distinct ID,DESCR from(SELECT USR_NAME [ID],USR_NAME [DESCR] FROM dbo.USERS_M WHERE USR_EMP_ID IN  (select distinct EMP_ID from employee_M where EMP_BSU_ID='" & BSU & "') ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "User Names"
                    TitleLabel.Text = "User Names"
                Case "PURDOCTYPE"
                    StrSQL = "Select distinct ID,DESCR from(SELECT DISTINCT DOC_ID [ID],DOC_NAME [DESCR]  FROM dbo.DOCUMENT_M  ) A where A.ID<>'' "
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "User Names"
                    TitleLabel.Text = "User Names"
                Case "DESG"
                    Dim DES_FLAG As String = Mainclass.cleanString(Request.QueryString("DES_FLAG"))
                    StrSQL = "Select distinct ID,DESCR from(SELECT DES_ID as ID, DES_DESCR as DESCR FROM EMPDESIGNATION_M "
                    If DES_FLAG <> "" Then
                        StrSQL &= " where DES_FLAG = '" & DES_FLAG & "'  "
                    End If
                    StrSQL &= " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Employee Designation"
                    TitleLabel.Text = "Employee Designation"
                Case "SUBLEDGER"
                    Dim ACT_ID As String = ""
                    If Mainclass.cleanString(Request.QueryString("ACT_ID")) IsNot Nothing Then
                        ACT_ID = Mainclass.cleanString(Request.QueryString("ACT_ID"))
                    End If
                    StrSQL = "Select distinct * from(SELECT * FROM vw_ACCOUNTS_SUB_ACC_M_list "
                    StrSQL &= " WHERE  [ACCOUNT] = '" & ACT_ID & "' OR isnull([ACCOUNT],'')='' "
                    StrSQL &= " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    Page.Title = "Accounts Sub Ledger"
                    TitleLabel.Text = "Accounts Sub Ledger"
                Case "COSTCENTER"
                    StrSQL = "Select distinct * from(select CCT_ID ID,CCT_DESCR DESCR from COSTCENTER_M "
                    StrSQL &= " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    Page.Title = "Cost Center"
                    TitleLabel.Text = "Cost Center"
                Case "MSTSUBLEDGER"
                    StrSQL = "Select distinct * from(SELECT ID,DESCR  FROM vw_ACCOUNTS_SUB_ACC_M_list "
                    StrSQL &= " ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    Page.Title = "Sub Ledgers"
                    TitleLabel.Text = "Sub Ledgers"
                Case "CCSUBLEDGER"
                    Dim ACT_ID As String = ""
                    If Mainclass.cleanString(Request.QueryString("ACT_ID")) IsNot Nothing Then
                        ACT_ID = Mainclass.cleanString(Request.QueryString("ACT_ID"))
                    End If

                    StrSQL = "Select distinct * from(SELECT ASM_ID ID,ASM_NAME DESCR,ParentName [PARENT] "
                    StrSQL &= " FROM VW_ACCOUNTS_SUB_ASM_ACC_LINK"
                    StrSQL &= " WHERE (ACT_ID in (select * from oasisfin..fnSplitMe('" & ACT_ID & "','|')) OR '" & ACT_ID & "'='')"
                    StrSQL &= " ) A where A.ID<>''"

                    strConn = ConnectionManger.GetOASISFINConnectionString
                    Page.Title = "Sub Ledgers"
                    TitleLabel.Text = "Sub Ledgers"
                Case "VISACCOUNT"
                    StrSQL = "Select ID,DESCR from(SELECT ACT_ID ID, ACT_DESCR DESCR FROM VISACCOUNT) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Account"
                    TitleLabel.Text = "Account"
                Case "VISTRNTYPE"
                    StrSQL = "Select ID,DESCR, ACCOUNT from(SELECT TYP_ID ID, TYP_DESCR DESCR, ACT_DESCR ACCOUNT FROM VISTRNTYPE inner join VISACCOUNT on ACT_ID=TYP_ACT_ID) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASIS_DOCSConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Transaction Type"
                    TitleLabel.Text = "Transaction Type"
                Case "COSTELEMENT"
                    IsDataTableAsList = True
                    If Mainclass.cleanString(Request.QueryString("COSTTYPE").ToString) <> "" Then
                        ViewState("COSTTYPE") = Mainclass.cleanString(Request.QueryString("COSTTYPE").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("DOC_DATE").ToString) <> "" Then
                        ViewState("DOC_DATE") = Mainclass.cleanString(Request.QueryString("DOC_DATE").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("COSTOTH").ToString) <> "" Then
                        ViewState("COSTOTH") = Mainclass.cleanString(Request.QueryString("COSTOTH").ToString)
                    End If
                    GridListData = CostCenterFunctions.GetCostCenterData("", ViewState("COSTTYPE"), Session("sBsuid"), ViewState("DOC_DATE"), ViewState("COSTOTH"))
                    StrSortCol = GridListData.Columns(1).ColumnName
                    Page.Title = "Cost Element"
                    TitleLabel.Text = "Cost Element"
                Case "TPTSEASON"
                    Dim CLIENT_ID As String
                    CLIENT_ID = ""
                    CLIENT_ID = Mainclass.cleanString(Request.QueryString("ClinetID").ToString)
                    StrSQL = "select ID,DESCR from(select SEAS_ID ID,SEAS_NAME DESCR from tptSeason  "
                    StrSQL = StrSQL & "WHERE SEAS_ID IN(SELECT  DISTINCT CNH_SEAS_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & CLIENT_ID & ",%' and  cnh_noShow<>1) "
                    StrSQL = StrSQL & ") A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Season  List"
                    TitleLabel.Text = "Season  List"
                Case "TPTSERVICENEW"
                    StrSQL = "Select ID,DESCR from(SELECT TSM_ID ID, TSM_DESCR DESCR FROM TPT_SERVICE_M) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Service"
                    TitleLabel.Text = "Service"
                Case "TPTPRODUCT"
                    ''commented and added by nmahesh 16jan2020
                    'Dim jOBTYPE, CLIENT_ID, VehTypeID, Seasonid, Ratetype As String
                    'jOBTYPE = "" : CLIENT_ID = "" : VehTypeID = "" : Seasonid = ""
                    'jOBTYPE = Mainclass.cleanString(Request.QueryString("jobtype").ToString)
                    'CLIENT_ID = Mainclass.cleanString(Request.QueryString("CLIENTID").ToString)
                    'VehTypeID = Mainclass.cleanString(Request.QueryString("VehTypeID").ToString)
                    'Seasonid = Mainclass.cleanString(Request.QueryString("Seasonid").ToString)
                    'Ratetype = Mainclass.cleanString(Request.QueryString("ratetype").ToString)
                    'If jOBTYPE = "Customer" Then
                    '    StrSQL = "select ID,DESCR,RATE,FlAG from(" _
                    '    & "select cast(prod_id as varchar)+';'+cast(cnh_id as varchar) ID, prod_name DESCR, cast(TPTCONTRACT_D.CND_RATE as nvarchar) Rate,PROD_FLAG FLAG from TPTCONTRACT_H INNER JOIN TPTCONTRACT_D ON CNH_ID=CND_CNH_ID INNER JOIN TPTHiringProduct ON PROD_ID=CND_PROD_ID WHERE CNH_ID IN (SELECT  CNH_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & CLIENT_ID & ",%' AND CNH_NOSHOW <>1 AND  cnh_vtyp_id =" & VehTypeID & " and CNH_SEAS_ID =" & Seasonid & " and CNH_TRL_ID =" & Ratetype & " ) and PROD_FLAG='G' " _
                    '    & " union " _
                    '    & " select cast(prod_id as varchar)+';'+cast(cnh_id as varchar)ID, prod_name DESCR, cast(TPTCONTRACT_D.CND_RATE as nvarchar) Rate,PROD_FLAG FLAG from TPTCONTRACT_H  INNER JOIN TPTCONTRACT_D ON CNH_ID=CND_CNH_ID INNER JOIN TPTHiringProduct ON PROD_ID=CND_PROD_ID WHERE CNH_ID IN (SELECT TOP 1 CNH_ID FROM TPTCONTRACT_H where CNH_CLIENT_ID='" & CLIENT_ID & "' and  CNH_NOSHOW <>1 ORDER BY CNH_SYSDATE DESC) AND PROD_ID NOT IN " _
                    '    & "(select prod_id from TPTCONTRACT_H INNER JOIN TPTCONTRACT_D ON CNH_ID=CND_CNH_ID INNER JOIN TPTHiringProduct ON PROD_ID=CND_PROD_ID WHERE CNH_ID IN (SELECT TOP 1 CNH_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & CLIENT_ID & ",%'  and PROD_FLAG='G' ORDER BY CNH_SYSDATE DESC)) " _
                    '    & " union " _
                    '    & "SELECT cast(PROD_ID AS VARCHAR)+';0' ID,PROD_NAME DESCR,cast(PROD_RATE as nvarchar) rate,PROD_FLAG FLAG FROM TPTHiringProduct WHERE PROD_fLAG='R'" _
                    '    & ") A where 1=1 "
                    'Else
                    '    StrSQL = "select '0;0' ddlId, ' Select Product' DESCR, 0 Rate "
                    '    StrSQL &= "union SELECT cast(CNB_ID as varchar)+';1', 'Rate per hour', CNB_RATEHR FROM TPTCONTRACT_B where CNB_BSU_ID='" & CLIENT_ID & "' and CNB_RATEHR>0 "
                    '    StrSQL &= "union SELECT cast(CNB_ID as varchar)+';2', 'Rate per km', CNB_RATEKM FROM TPTCONTRACT_B where CNB_BSU_ID='" & CLIENT_ID & "' and CNB_RATEKM>0 "
                    'End If
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'StrSortCol = "DESCR"
                    'Page.Title = "Product  List"
                    'TitleLabel.Text = "Product  List"

                    Dim jOBTYPE, CLIENT_ID, VehTypeID, Seasonid, Ratetype As String
                    jOBTYPE = "" : CLIENT_ID = "" : VehTypeID = "" : Seasonid = ""

                    jOBTYPE = Mainclass.cleanString(Request.QueryString("jobtype").ToString)
                    CLIENT_ID = Mainclass.cleanString(Request.QueryString("CLIENTID").ToString)
                    VehTypeID = Mainclass.cleanString(Request.QueryString("VehTypeID").ToString)
                    Seasonid = Mainclass.cleanString(Request.QueryString("Seasonid").ToString)
                    Ratetype = Mainclass.cleanString(Request.QueryString("ratetype").ToString)
                    HideFirstColumn = True
                    IsStoredProcedure = True
                    StrSQL = "TPT_PRODUCT_JOB_ORDER"
                    ReDim SPParam(6)
                    SPParam(0) = Mainclass.CreateSqlParameter("@JOBTYPE", jOBTYPE, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@CLIENT_ID", CLIENT_ID, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@VehTypeID", VehTypeID, SqlDbType.Int)
                    SPParam(3) = Mainclass.CreateSqlParameter("@Seasonid", Seasonid, SqlDbType.Int)
                    SPParam(4) = Mainclass.CreateSqlParameter("@Ratetype", Ratetype, SqlDbType.Int)
                    SPParam(5) = Mainclass.CreateSqlParameter("@User", Session("sUsr_name"), SqlDbType.VarChar)
                    SPParam(6) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)

                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Product  List"
                    TitleLabel.Text = "Product  List"
                Case "TPTRATETYPE"
                    'Dim CLIENT_ID As String
                    'CLIENT_ID = ""
                    'CLIENT_ID = Request.QueryString("ClinetID").ToString
                    'StrSQL = "select ID,DESCR from(select TRL_ID ID,TRL_DESCR DESCR from TPT_RATELIST  "
                    'StrSQL = StrSQL & "WHERE TRL_ID IN(SELECT  DISTINCT CNH_TRL_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & CLIENT_ID & ",%' and CNH_NOSHOW <>1) "
                    'StrSQL = StrSQL & ") A where A.ID<>'' "
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'StrSortCol = "DESCR"
                    'Page.Title = "Rate Type List"
                    ''commented above and added by mahesh on 20nov2016
                    Dim CLIENT_ID As String = ""
                    Dim Jdate As Date = "1900-01-01"
                    CLIENT_ID = Mainclass.cleanString(Request.QueryString("ClinetID").ToString)
                    Jdate = Mainclass.cleanString(Request.QueryString("JobDate").ToString)
                    If Jdate <> "1900-01-01" Then
                        StrSQL = "select ID,DESCR from(select TRL_ID ID,TRL_DESCR DESCR from TPT_RATELIST  "
                        StrSQL = StrSQL & "WHERE TRL_ID IN(SELECT  DISTINCT CNH_TRL_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & CLIENT_ID & ",%' and CNH_NOSHOW <>1 and '" & Jdate & "' <= CNH_EFFECTIVE_DATE) "
                        StrSQL = StrSQL & ") A where A.ID<>'' "
                    Else
                        StrSQL = ""
                    End If

                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Rate Type List"
                    TitleLabel.Text = "Rate Type List"
                Case "TPTDRIVERSCHOOL"
                    StrSQL = "Select ID,[DRIVER NUMBER],[DRIVER NAME] from(SELECT distinct driver_empid id,DRIVER_EMPNO [DRIVER NUMBER], driver [DRIVER NAME] FROM  VW_DRIVER) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[DRIVER NAME]"
                    Page.Title = "Driver"
                    TitleLabel.Text = "Driver"
                Case "TPTDRIVERSCHOOLSDT"
                    StrSQL = "Select ID,[DRIVER NUMBER],[DRIVER NAME] from(SELECT distinct driver_empid id,DRIVER_EMPNO [DRIVER NUMBER], driver [DRIVER NAME] FROM  VW_DRIVER_SDE) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[DRIVER NAME]"
                    Page.Title = "Driver"
                    TitleLabel.Text = "Driver"
                Case "TPTDRIVERNEW"
                    StrSQL = "Select ID,[DRIVER NUMBER],[DRIVER NAME] from(select TPTD_ID ID,TPTD_EMPNO [DRIVER NUMBER],TPTD_NAME [DRIVER NAME] FROM TPTDRIVER_M WHERE TPTD_DELETED=0 ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[DRIVER NAME]"
                    Page.Title = "Driver List"
                    TitleLabel.Text = "Driver List"
                Case "TPTSALESMANNEW"
                    StrSQL = "Select ID,DESCR from(SELECT SLSM_ID ID, SLSM_NAME DESCR FROM TPTSALESMAN) A where A.ID<>''"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Sales Man List"
                    TitleLabel.Text = "Sales Man List"
                Case "TPTSUPPLIER"
                    StrSQL = "Select ID,CODE,DESCR from( SELECT AM.ACT_ID ID, AM.ACT_ID CODE,AM.ACT_NAME DESCR "
                    StrSQL &= "FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M AM "
                    StrSQL &= "WHERE AM.ACT_FLAG='S' and AM.ACT_Bctrlac='FALSE' AND AM.ACT_BSU_ID Like  '%" & Session("sBsuid") & "%'  "
                    StrSQL &= "AND AM.ACT_BACTIVE='TRUE' ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "ID"
                    Page.Title = "Supplier"
                    TitleLabel.Text = "Supplier"
                Case "TPTPURPOSE"
                    HideFirstColumn = True
                    IsStoredProcedure = True
                    StrSQL = "TPT_PURPOSE_EXGRATIA"
                    ReDim SPParam(1)
                    If Mainclass.cleanString(Request.QueryString("DriverType").ToString) = "1" Then
                        SPParam(0) = Mainclass.CreateSqlParameter("@OPTION", "1", SqlDbType.VarChar)
                    Else
                        SPParam(0) = Mainclass.CreateSqlParameter("@OPTION", "0", SqlDbType.VarChar)
                    End If
                    SPParam(1) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "SNO"
                    Page.Title = "Purpose Master"
                    TitleLabel.Text = "Purpose Master"
                Case "TPTCLIENT"
                    'If Mainclass.cleanString(Request.QueryString("jobtype")) = "Business Unit" Then
                    '    StrSQL = "Select ID,CODE,DESCR, xfdate, xedate from( SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID ID, BUSINESSUNIT_M.BSU_ID CODE, BUSINESSUNIT_M.BSU_NAME  DESCR, replace(convert(varchar(30), getdate(), 106),' ','/') xfdate, '31/dec/2050' xedate "
                    '    StrSQL &= "FROM vw_oso_SERVICES_BSU_M INNER JOIN  BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID    "
                    '    StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='900500') AND  (vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1)) A where A.ID<>''  "
                    'Else
                    '    'StrSQL = "Select ID,CODE,DESCR,SALESMAN from( SELECT  cast(AM.ACT_ID AS varchar)+';'+cast(isnull(CNH_SALESMAN_ID,0) as varchar)+';'+isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') ID,AM.ACT_ID CODE,AM.ACT_NAME DESCR, isnull(EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME,'') SALESMAN "
                    '    'StrSQL &= "FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M AM INNER JOIN oasisfin.dbo.ACCOUNTS_M AMC ON AMC.ACT_ID=AM.ACT_CTRLACC "
                    '    'StrSQL &= "left outer join TPTCONTRACT_H  on AM.ACT_ID=CNH_CLIENT_ID left OUTER JOIN OASIS.dbo.EMPLOYEE_M on EMP_ID=CNH_SALESMAN_ID "
                    '    'StrSQL &= " WHERE AM.ACT_ID IN(select DISTINCT ACT_ID  from oasisfin..ACCOUNTS_M inner join TPTCONTRACT_H on ',' + CNH_CLIENT_ID  + ',' like '%,' + act_id  + ',%' )"
                    '    'StrSQL &= "AND AM.ACT_BSU_ID Like  '%" & Session("sBsuid") & "%'  UNION SELECT '00000000;0', '00000000', 'STANDARD CONTRACT', '') A where A.ID<>'' "


                    '    StrSQL = " Select ID,CODE,DESCR,SALESMAN from(select DISTINCT A.ACT_ID+';0;0' ID ,A.ACT_ID CODE,A.ACT_NAME DESCR,''SALESMAN from oasisfin..ACCOUNTS_M  A with(nolock)  left outer join oasisfin.dbo.vw_OSA_ACCOUNTS_M AM with(nolock) on AM.ACT_CTRLACC=A.ACT_ID "
                    '    StrSQL &= "inner join TPTCONTRACT_H with(nolock) on ',' + CNH_CLIENT_ID  + ',' like '%,' + A.ACT_ID  + ',%' UNION SELECT '00000000;0;0', '00000000', 'STANDARD CONTRACT', '') A where A.ID<>'' "

                    'End If
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'StrSortCol = "ID"
                    'Page.Title = "Client"
                    'TitleLabel.Text = "Client"
                    Dim jOBTYPE As String
                    jOBTYPE = ""
                    jOBTYPE = Mainclass.cleanString(Request.QueryString("jobtype"))
                    HideFirstColumn = True
                    IsStoredProcedure = True
                    StrSQL = "TPT_CLIENT_LIST"
                    ReDim SPParam(4)
                    SPParam(0) = Mainclass.CreateSqlParameter("@JOBTYPE", jOBTYPE, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@BSUID", Session("sBsuid"), SqlDbType.Int)
                    SPParam(2) = Mainclass.CreateSqlParameter("@User", Session("sUsr_name"), SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@FYEAR", Session("F_YEAR"), SqlDbType.VarChar)

                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "ID"
                    Page.Title = "Client  List"
                    TitleLabel.Text = "Client  List"

                Case "TPTCLIENTREPORT"
                    If Mainclass.cleanString(Request.QueryString("jobtype")) = "Business Unit" Then
                        StrSQL = "Select ID,CODE,DESCR, xfdate, xedate from( SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID ID, BUSINESSUNIT_M.BSU_ID CODE, BUSINESSUNIT_M.BSU_NAME  DESCR, replace(convert(varchar(30), getdate(), 106),' ','/') xfdate, '31/dec/2050' xedate "
                        StrSQL &= "FROM vw_oso_SERVICES_BSU_M INNER JOIN  BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID    "
                        StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='900500') AND  (vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1)) A where A.ID<>''  "
                    Else
                        StrSQL = "Select ID,DESCR from( select AM.ACT_ID ID,AM.ACT_NAME DESCR FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M AM  "
                        StrSQL &= "INNER JOIN oasisfin.dbo.ACCOUNTS_M AMC ON AMC.ACT_ID=AM.ACT_CTRLACC left outer join TPTCONTRACT_H  on AM.ACT_ID=CNH_CLIENT_ID  "
                        StrSQL &= "left OUTER JOIN OASIS.dbo.EMPLOYEE_M on EMP_ID=CNH_SALESMAN_ID  WHERE AM.ACT_ID IN(select DISTINCT ACT_ID  from oasisfin..ACCOUNTS_M inner join  "
                        StrSQL &= " TPTCONTRACT_H on ',' + CNH_CLIENT_ID  + ',' like '%,' + act_id  + ',%' )AND AM.ACT_BSU_ID Like  '%900501%'  UNION  SELECT  '00000000', 'STANDARD CONTRACT') A where A.ID <>''  "
                    End If
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "ID"
                    Page.Title = "Client"
                    TitleLabel.Text = "Client"
                Case "TPTVEHICLETYPEREPORT"
                    StrSQL = "Select ID,DESCR from(select VTYP_ID ID,VTYP_DESCRIPTION DESCR from VEHTYPE_M "
                    StrSQL = StrSQL & "WHERE VTYP_ID IN(SELECT  DISTINCT CNH_VTYP_ID FROM TPTCONTRACT_H) "
                    StrSQL = StrSQL & ") A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle Category"
                    TitleLabel.Text = "Vehicle Category"
                Case "TPTSEASONREPORT"
                    StrSQL = "select ID,DESCR from(select SEAS_ID ID,SEAS_NAME DESCR from tptSeason  "
                    StrSQL = StrSQL & "WHERE SEAS_ID IN(SELECT  DISTINCT CNH_SEAS_ID FROM TPTCONTRACT_H ) "
                    StrSQL = StrSQL & ") A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Season  List"
                    TitleLabel.Text = "Season  List"
                Case "TPTDRIVERREPORT"
                    StrSQL = "select ID,DESCR from(SELECT TPTD_ID ID,TPTD_NAME DESCR  FROM TPTDRIVER_M WHERE TPTD_DELETED=0  "
                    StrSQL = StrSQL & ") A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Season  List"
                    TitleLabel.Text = "Season  List"
                Case "TPTVEHICLEREPORT"
                    StrSQL = "Select ID,DESCR from(SELECT VEH_ID ID,VEH_REGNO+'-'+VEH_TYPE DESCR  from TPTVEHICLE_M where VEH_DELETED=0  "
                    StrSQL = StrSQL & ") A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicles List"
                    TitleLabel.Text = "Vehicles List"
                Case "TPTADDJOBS"
                    StrSQL = "select TRN_No, replace(convert(varchar(30), TPT_Date, 106),' ','/') DATE, LOC_DESCRIPTION Location, VEH_REGNO [Vehicle No], emp_name [Driver], "
                    StrSQL &= "ACT_NAME Client, PKUP.PNT_DESCRIPTION [Pickup Point], DRP.PNT_DESCRIPTION [DropOff Point], "
                    StrSQL &= "(SELECT sum(INV_AMT) from tpthiringinvoice where trn_no=inv_trn_no) Amount "
                    StrSQL &= "FROM TPTHiring "
                    StrSQL &= "inner JOIN TRANSPORT.LOCATION_M on TPT_Loc_id=LOC_ID "
                    StrSQL &= "inner JOIN VEHICLE_M ON VEH_ID=TPT_Veh_id "
                    StrSQL &= "INNER JOIN TRANSPORT.VEHICLE_MAKE on VEH_VMK_ID=VMK_ID "
                    StrSQL &= "LEFT OUTER JOIN VV_DRIVER ON emp_id=TPT_Drv_id "
                    StrSQL &= "INNER JOIN oasisfin.dbo.vw_OSA_ACCOUNTS_M on ACT_ID=TPT_Client_id "
                    StrSQL &= "LEFT OUTER JOIN TRANSPORT.TRIP_PURPOSE_M on PSE_ID=TPT_Service_id "
                    StrSQL &= "LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M PKUP on PKUP.PNT_ID=TPT_Pkup_id "
                    StrSQL &= "LEFT OUTER JOIN TRANSPORT.PICKUPPOINTS_M DRP on DRP.PNT_ID=TPT_Drop_id "
                    StrSQL &= "where TPT_Bsu_id like '%" & Session("sBsuid") & "%' "
                    StrSQL &= "and TPT_Client_id=(select top 1 TPT_Client_id from TPTHiring where TRN_No in (" & Mainclass.cleanString(Request.QueryString("jobids")) & ")) "
                    StrSQL &= "and (isnull(TPT_BllId,0)=0 or TRN_No in (" & Mainclass.cleanString(Request.QueryString("jobids")) & "))"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    Page.Title = "Hiring and Billing Job Order"
                    StrSortCol = "TRN_NO"
                    TitleLabel.Text = "Hiring and Billing Job Order"
                Case "TPTLOCATION"
                    StrSQL = "Select ID,DESCR from(SELECT LOC_ID ID, LOC_DESCRIPTION DESCR FROM TRANSPORT.LOCATION_M) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Location"
                    TitleLabel.Text = "Location"
                Case "TPTVEHICLETYPE"
                    Dim CLIENT_ID As String
                    CLIENT_ID = ""
                    CLIENT_ID = Mainclass.cleanString(Request.QueryString("ClinetID").ToString)
                    StrSQL = "Select ID,DESCR from(select VTYP_ID ID,VTYP_DESCRIPTION DESCR from VEHTYPE_M "
                    StrSQL = StrSQL & "WHERE VTYP_ID IN(SELECT  DISTINCT CNH_VTYP_ID FROM TPTCONTRACT_H where ','+CNH_CLIENT_ID+',' LIKE '%," & CLIENT_ID & ",%' and  cnh_noShow<>1) "
                    StrSQL = StrSQL & ") A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle Category"
                    TitleLabel.Text = "Vehicle Category"
                Case "TPTVEHICLENEW"
                    StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,VEH_TYPE NAME,VEH_CAPACITY CAPACITY  from TPTVEHICLE_M where VEH_DELETED=0 ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle No."
                    TitleLabel.Text = "Vehicle No."
                Case "TPTVEHICLE"
                    StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,CAT_DESCRIPTION NAME,VEH_CAPACITY CAPACITY  from VEHICLE_M with(nolock) LEFT OUTER JOIN CATEGORY_M with(nolock) ON CAT_ID=VEH_CAT_ID where VEH_ALTO_BSU_ID<>'500907' ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle No."
                    TitleLabel.Text = "Vehicle No."
                Case "TPTDRIVER"
                    StrSQL = "Select ID,DESCR from(select emp_name descr, emp_id id FROM VV_DRIVER where bsu_alto_id Like '%" & Session("sBsuid") & "%') A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Driver"
                    TitleLabel.Text = "Driver"
                Case "TPTVEHICLE_EXCESSCAPACITY"
                    StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,VEH_TYPE NAME,left(VEH_CAPACITY,2) CAPACITY  from TPTVEHICLE_M where VEH_DELETED=0 ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle No."
                    TitleLabel.Text = "Vehicle No."
                Case "TPTBUDGET"
                    StrSQL = "SELECT ID, ACCOUNT, [MAIN BUDGET], [SUB BUDGET], xACT_ID from ("
                    StrSQL &= "SELECT BUD_ID ID, ACT_ID xACT_ID, ACT_NAME ACCOUNT, BUD_MAIN [MAIN BUDGET], BUD_SUBHEAD [SUB BUDGET] "
                    StrSQL &= "FROM oasisfin.dbo.BUDGET_H inner JOIN oasisfin.dbo.BUDGET_D on BUH_ID=BUD_BUH_ID "
                    StrSQL &= "INNER JOIN oasisfin.dbo.ACCOUNTS_M on ACT_ID=BUD_ACT_ID WHERE BUH_BSU_ID='" & Session("sBsuid") & "' AND BUH_FYEAR='2011') a where 1=1 "
                    strConn = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
                    StrSortCol = "[MAIN BUDGET]"
                    Page.Title = "Budget Selection"
                    TitleLabel.Text = "Budget Selection"
                Case "TPTSERVICE"
                    StrSQL = "Select ID,DESCR from(SELECT PSE_ID ID, PSE_DESCR DESCR FROM TRANSPORT.TRIP_PURPOSE_M) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Service"
                    TitleLabel.Text = "Service"
                Case "TPTPICKUP", "TPTDROPOFF"
                    Dim locationId As Integer = Mainclass.cleanString(Request.QueryString("location"))
                    StrSQL = "Select ID,DESCR from(SELECT PNT_ID ID, PNT_DESCRIPTION DESCR FROM TRANSPORT.PICKUPPOINTS_M where PNT_LOC_ID=" & locationId & " AND PNT_BSU_ID like '%125012%') A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = IIf(SearchMode = "TPTPICKUP", "Pickup Point", "Drop off Point")
                    TitleLabel.Text = IIf(SearchMode = "TPTPICKUP", "Pickup Point", "Drop off Point")
                Case "TPTSALESMAN"
                    StrSQL = "Select ID,DESCR from(SELECT EMP_ID ID, EMP_FNAME+' '+EMP_MNAME+' '+EMP_LNAME DESCR FROM OASIS.dbo.EMPLOYEE_M where EMP_BSU_ID='" & Session("sBsuid") & "') A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = IIf(SearchMode = "TPTPICKUP", "Pickup Point", "Drop off Point")
                    TitleLabel.Text = IIf(SearchMode = "TPTPICKUP", "Pickup Point", "Drop off Point")
                Case "TPTBSU"
                    StrSQL = "Select ID,DESCR from( SELECT BSU_ID as ID,BSU_NAME as DESCR from BUSINESSUNIT_M "
                    If Not Mainclass.cleanString(Request.QueryString("userid")) Is Nothing Then
                        StrSQL &= "where BSU_ID<>'XXXXXX' and BSU_ID IN (select USA_BSU_ID FROM oasis..USERACCESS_S  where USA_USR_ID='" & Mainclass.cleanString(Request.QueryString("userid")) & "')"
                    End If
                    StrSQL &= ") A where 1=1  "
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Client"
                    TitleLabel.Text = "Client"
                Case "TPTVEHICLEFLEET"
                    'StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY,ISNULL([ASSIGNED JOB DETAILS],'')[ASSIGNED JOB DETAILS]   from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,CAT_DESCRIPTION NAME,VEH_CAPACITY CAPACITY,(SELECT  'Job Time: '+ left(right(CONVERT(VARCHAR(24),TPT_JobSt_Time,106),8),5) +' To ' + left(right(CONVERT(VARCHAR(24),TPT_JOBCl_Time,106),8),5) + '  ;  '     FROM tpthiring b    WHERE  b.TPT_Veh_id  = VEHICLE_M.veh_id and b.TPT_DATA =1 and TPT_JobDate='" & Request.QueryString("jobDate").ToString() & "' FOR XML PATH('')) [ASSIGNED JOB DETAILS]  from VEHICLE_M LEFT OUTER JOIN CATEGORY_M ON CAT_ID=VEH_CAT_ID  ) A where A.ID<>''"
                    'strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    'StrSortCol = "DESCR"
                    'Page.Title = "Vehicle No."
                    'on 05-Feb-2018
                    Dim J_STime As String = ""
                    Dim J_ETime As String = ""
                    If Not Mainclass.cleanString(Request.QueryString("STime")) Is Nothing Then
                        J_STime = Mainclass.cleanString(Request.QueryString("STime"))
                    End If
                    If Not Mainclass.cleanString(Request.QueryString("ETime")) Is Nothing Then
                        J_ETime = Mainclass.cleanString(Request.QueryString("ETime"))
                    End If
                    StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY,ISNULL([ASSIGNED JOB DETAILS],'')[ASSIGNED JOB DETAILS],isnull([Job Assigned],'')[Job Assigned] from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,isnull(CAT_DESCRIPTION,'') NAME,VEH_CAPACITY CAPACITY,(SELECT  'Job Time: '+ left(right(CONVERT(VARCHAR(24),TPT_JobSt_Time,106),8),5) +' To ' + left(right(CONVERT(VARCHAR(24),TPT_JOBCl_Time,106),8),5)  + '  ;  '     FROM tpthiring b with (nolock)    WHERE  b.TPT_Veh_id  = VEHICLE_M.veh_id and b.TPT_DATA in(0,2) and TPT_JobDate='" & Mainclass.cleanString(Request.QueryString("jobDate").ToString()) & "' FOR XML PATH('')) [ASSIGNED JOB DETAILS],(SELECT  case when ('" & J_STime & "' < TPT_JobSt_Time and '" & J_ETime & "'< TPT_JobSt_Time) or ('" & J_STime & "' > TPT_JOBCl_Time and '" & J_ETime & "'> TPT_JOBCl_Time) and TPT_JobDate='" & Mainclass.cleanString(Request.QueryString("jobDate").ToString()) & "'  then 'No' else 'Yes' end FROM tpthiring  b with(nolock)     WHERE  b.TPT_Veh_id  = VEHICLE_M.veh_id and b.TPT_DATA in(0,2) and TPT_JobDate='" & Mainclass.cleanString(Request.QueryString("jobDate").ToString()) & "' FOR XML PATH('')) [Job Assigned]      from VEHICLE_M with(nolock) LEFT OUTER JOIN CATEGORY_M with(nolock) ON CAT_ID=VEHICLE_M.VEH_CAT_ID where VEH_ALTO_BSU_ID<>'500907'  ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle No."
                    TitleLabel.Text = "Vehicle No."
                Case "TPTVEHICLEFLEETNEW"
                    '    StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY,ISNULL([ASSIGNED JOB DETAILS],'')[ASSIGNED JOB DETAILS] from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,VEH_TYPE NAME,VEH_CAPACITY CAPACITY,(SELECT  'Job Time: '+ left(right(CONVERT(VARCHAR(24),TPT_JobSt_Time,106),8),5) +' To ' + left(right(CONVERT(VARCHAR(24),TPT_JOBCl_Time,106),8),5)  + '  ;  '     FROM tpthiring b    WHERE  b.TPT_Veh_id  = TPTVEHICLE_M.veh_id and b.TPT_DATA in(0,2) and TPT_JobDate='" & Request.QueryString("jobDate").ToString() & "' FOR XML PATH('')) [ASSIGNED JOB DETAILS]  from TPTVEHICLE_M where VEH_DELETED=0  ) A where A.ID<>'' "
                    '    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    '    StrSortCol = "DESCR"
                    '    Page.Title = "Vehicle No."
                    Dim J_STime As String = ""
                    Dim J_ETime As String = ""

                    If Not Mainclass.cleanString(Request.QueryString("STime")) Is Nothing Then
                        J_STime = Mainclass.cleanString(Request.QueryString("STime"))
                    End If
                    If Not Mainclass.cleanString(Request.QueryString("ETime")) Is Nothing Then
                        J_ETime = Mainclass.cleanString(Request.QueryString("ETime"))
                    End If
                    StrSQL = "Select ID,DESCR,MODEL,NAME, CAPACITY,ISNULL([ASSIGNED JOB DETAILS],'')[ASSIGNED JOB DETAILS],isnull([Job Assigned],'')[Job Assigned] from(select VEH_ID ID,VEH_REGNO DESCR,VEH_MODEL MODEL,VEH_TYPE NAME,VEH_CAPACITY CAPACITY,(SELECT  'Job Time: '+ left(right(CONVERT(VARCHAR(24),TPT_JobSt_Time,106),8),5) +' To ' + left(right(CONVERT(VARCHAR(24),TPT_JOBCl_Time,106),8),5)  + '  ;  '     FROM tpthiring b    WHERE  b.TPT_Veh_id  = TPTVEHICLE_M.veh_id and b.TPT_DATA in(0,2) and TPT_JobDate='" & Mainclass.cleanString(Request.QueryString("jobDate").ToString()) & "' FOR XML PATH('')) [ASSIGNED JOB DETAILS],(SELECT  case when ('" & J_STime & "' < TPT_JobSt_Time and '" & J_ETime & "'< TPT_JobSt_Time) or ('" & J_STime & "' > TPT_JOBCl_Time and '" & J_ETime & "'> TPT_JOBCl_Time) and TPT_JobDate='" & Mainclass.cleanString(Request.QueryString("jobDate").ToString()) & "'  then 'No' else 'Yes' end FROM tpthiring b    WHERE  b.TPT_Veh_id  = TPTVEHICLE_M.veh_id and b.TPT_DATA in(0,2) and TPT_JobDate='" & Mainclass.cleanString(Request.QueryString("jobDate").ToString()) & "' FOR XML PATH('')) [Job Assigned]      from TPTVEHICLE_M where VEH_DELETED=0  ) A where A.ID<>'' "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "Vehicle No."
                    TitleLabel.Text = "Vehicle No."
                Case "TPTFLEETDRIVERSCHOOL"
                    StrSQL = "Select ID,[DRIVER NUMBER],[DRIVER NAME],ISNULL([DRIVER LEAVE DETAILS],'')[DRIVER LEAVE DETAILS] from(SELECT distinct driver_empid id,DRIVER_EMPNO [DRIVER NUMBER], driver [DRIVER NAME],(SELECT  convert(varchar(12),TDL_LEAVE_FDATE) + ' To  ' + convert(varchar(12),TDL_LEAVE_TDATE) FROM TPTDRIVERSLEAVE b  WHERE   '" & Mainclass.cleanString(Request.QueryString("jobDate")) & "' between TDL_LEAVE_FDATE  and TDL_LEAVE_TDATE  and b.TDL_EMP_ID  =DRIVER_EMPID  and b.TDL_JOBTYPE=1    FOR XML PATH('')) [DRIVER LEAVE DETAILS] FROM  VW_DRIVER) A where A.ID<>''"
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[DRIVER NAME]"
                    Page.Title = "Driver"

                Case "TPTFLEETDRIVERNEW"
                    StrSQL = "Select ID,[DRIVER NUMBER],[DRIVER NAME],ISNULL([DRIVER LEAVE DETAILS],'')[DRIVER LEAVE DETAILS] from(select TPTD_ID ID,TPTD_EMPNO [DRIVER NUMBER],TPTD_NAME [DRIVER NAME], (SELECT  convert(varchar(12),TDL_LEAVE_FDATE) + ' To  ' + convert(varchar(12),TDL_LEAVE_TDATE) FROM TPTDRIVERSLEAVE b  WHERE   '" & Mainclass.cleanString(Request.QueryString("jobDate")) & "' between TDL_LEAVE_FDATE  and TDL_LEAVE_TDATE  and b.TDL_EMP_ID  =TPTD_ID  and b.TDL_JOBTYPE in(0,2)    FOR XML PATH('')) [DRIVER LEAVE DETAILS]    FROM TPTDRIVER_M WHERE TPTD_DELETED=0 ) A where A.ID<>''  "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[DRIVER NAME]"
                    Page.Title = "Driver List"
                    TitleLabel.Text = "Driver List"
                Case "CCALLOCJV"
                    HideFirstColumn = False
                    IsStoredProcedure = True
                    Dim BSUID, SUBID, DOC_TYPE, FYEAR As String
                    BSUID = "" : SUBID = "" : DOC_TYPE = "" : FYEAR = ""
                    StrSQL = "GetJVListForCCAllocation"
                    If Mainclass.cleanString(Request.QueryString("bsu").ToString) <> "" Then
                        BSUID = Mainclass.cleanString(Request.QueryString("bsu").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("SUBID").ToString) <> "" Then
                        SUBID = Mainclass.cleanString(Request.QueryString("SUBID").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("docType").ToString) <> "" Then
                        DOC_TYPE = Mainclass.cleanString(Request.QueryString("docType").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("FYEAR").ToString) <> "" Then
                        FYEAR = Mainclass.cleanString(Request.QueryString("FYEAR").ToString)
                    End If
                    ReDim SPParam(3)
                    SPParam(0) = Mainclass.CreateSqlParameter("@SUB_ID", SUBID, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@BSU_ID", BSUID, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@DOC_TYPE", DOC_TYPE, SqlDbType.VarChar)
                    SPParam(3) = Mainclass.CreateSqlParameter("@FYEAR", FYEAR, SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = "[DOC DATE] desc"
                    Page.Title = "Cost Center Allocation"
                    TitleLabel.Text = "Cost Center Allocation"
                Case "UNPAIDPROFORMAINVOICE"
                    StrSQL = "Select ID,[Invoice No.],[Invoice Date],Company from(  "
                    StrSQL &= "SELECT DISTINCT 0 AS Id , FPH.FPH_INOICENO AS [Invoice No.] ,replace(CONVERT(VARCHAR(10), FPH.FPH_DT, 106) ,' ','/')  AS [Invoice Date] , C.COMP_NAME AS Company FROM    FEES.FEE_PERFORMAINVOICE_H FPH INNER JOIN OASIS.dbo.COMP_LISTED_M C ON FPH.FPH_COMP_ID = C.COMP_ID WHERE ISNULL(FPH.FPH_FCL_ID, 0) = 0 and FPH_BSU_ID='" & Session("sBsuid") & "') A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[Invoice No.] DESC"
                    Page.Title = "Unpaid Pro Forma Invoice"
                    TitleLabel.Text = "Unpaid Pro Forma Invoice"
                Case "VOUCHERADMINISTRATION"
                    HideFirstColumn = False
                    IsStoredProcedure = False
                    Dim BSUID, SUBID, DOC_TYPE, FYEAR As String
                    BSUID = "" : SUBID = "" : DOC_TYPE = "" : FYEAR = ""
                    BSUID = Mainclass.cleanString(Request.QueryString("bsu").ToString)
                    DOC_TYPE = Mainclass.cleanString(Request.QueryString("docType").ToString)
                    FYEAR = Mainclass.cleanString(Request.QueryString("FYEAR").ToString)
                    StrSQL = "SELECT VHH_DOCNO,CONVERT(VARCHAR(13),VHH_DOCDT,106)VHH_DOCDT,VHH_DOCTYPE,REPLACE(VHH_NARRATION,'''','') VHH_NARRATION FROM dbo.VOUCHER_H WHERE VHH_BSU_ID='" & BSUID & "' AND VHH_FYEAR='" & FYEAR & "' AND VHH_DOCTYPE='" & DOC_TYPE & "' "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = "VHH_DOCNO DESC"
                    Page.Title = "Vouchers"
                    TitleLabel.Text = "Vouchers"
                Case "EMPSEARCH"
                    IsStoredProcedure = False
                    'Dim BSUID As String
                    'BSUID = ""
                    'BSUID = Request.QueryString("bsu").ToString
                    StrSQL = "Select EMP_ID,[EMP NO],[EMP NAME],[REPORT TO],[ATT.APPROVED BY] from(  "
                    StrSQL &= "SELECT A.EMP_ID,A.EMPNO as [EMP NO],LTRIM(RTRIM(ISNULL(A.EMP_FNAME,'')))+' '+ISNULL(A.EMP_MNAME,'')+' '+ISNULL(A.EMP_LNAME,'') [EMP NAME],B.ECT_DESCR as [CATEGORY], " & _
                    "ISNULL(RTO.EMP_FNAME,'')+' '+ISNULL(RTO.EMP_MNAME,'')+' '+ISNULL(RTO.EMP_LNAME,'') [REPORT TO]," & _
                    "ISNULL(AAP.EMP_FNAME,'')+' '+ISNULL(AAP.EMP_MNAME,'')+' '+ISNULL(AAP.EMP_LNAME,'') [ATT.APPROVED BY] " & _
                    "FROM  dbo.EMPLOYEE_M A INNER JOIN EMPCATEGORY_M B ON A.EMP_ECT_ID = B.ECT_ID " & _
                    "LEFT JOIN dbo.EMPLOYEE_M RTO ON A.EMP_REPORTTO_EMP_ID=RTO.EMP_ID " & _
                    "LEFT JOIN dbo.EMPLOYEE_M AAP ON A.EMP_ATT_Approv_EMP_ID=AAP.EMP_ID " & _
                    "WHERE A.EMP_bACTIVE=1 AND A.EMP_BSU_ID='" & Session("sBsuid") & "' AND A.EMP_STATUS IN (1,2) ) AS A WHERE 1=1 "


                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = " [EMP NAME]"
                    Page.Title = "Employees"
                    TitleLabel.Text = "Employees"
                Case "APPEMPSEARCH"
                    IsStoredProcedure = False
                    MultiSelect = False
                    Dim AllStaff As String
                    'BSUID = ""
                    AllStaff = ""
                    If Not Mainclass.cleanString(Request.QueryString("AllStaff")) Is Nothing Then
                        AllStaff = Mainclass.cleanString(Request.QueryString("AllStaff").ToString)
                    End If

                    StrSQL = "Select top 1000 EMP_ID,[EMP NO],[EMP NAME],[CATEGORY],[DESIGNATION],[BSU] from( "
                    StrSQL &= "SELECT A.EMP_ID,A.EMPNO as [EMP NO],LTRIM(RTRIM(ISNULL(A.EMP_FNAME,'')))+' '+ISNULL(A.EMP_MNAME,'')+' '+ISNULL(A.EMP_LNAME,'') [EMP NAME], " & _
                    "B.ECT_DESCR as [CATEGORY],DN.DES_DESCR [DESIGNATION],BU.BSU_SHORTNAME [BSU] " & _
                    "FROM  dbo.EMPLOYEE_M A INNER JOIN EMPCATEGORY_M B ON A.EMP_ECT_ID = B.ECT_ID " & _
                    "LEFT JOIN dbo.EMPDESIGNATION_M DN ON EMP_DES_ID=DN.DES_ID " & _
                    "INNER JOIN dbo.BUSINESSUNIT_M BU ON BU.BSU_ID=A.EMP_BSU_ID " & _
                    "WHERE A.EMP_bACTIVE=1 AND (A.EMP_BSU_ID='" & Session("sBsuid") & "' OR '" & AllStaff & "'='1') AND A.EMP_STATUS IN (1,2) ) AS A WHERE 1=1 "


                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = " [BSU],[EMP NO]"
                    Page.Title = "Employees"
                    TitleLabel.Text = "Employees"
                Case "VOUCHERADMINISTRATIONIJV"
                    HideFirstColumn = False
                    IsStoredProcedure = False
                    Dim BSUID, SUBID, DOC_TYPE, FYEAR As String
                    BSUID = "" : SUBID = "" : DOC_TYPE = "" : FYEAR = ""
                    BSUID = Mainclass.cleanString(Request.QueryString("bsu").ToString)
                    'DOC_TYPE = Request.QueryString("docType").ToString
                    FYEAR = Mainclass.cleanString(Request.QueryString("FYEAR").ToString)
                    StrSQL = "Select [DOCNO],[DOC DT],[DOC TYPE],[NARRATION] from ( "
                    StrSQL &= " SELECT IJH_DOCNO [DOCNO],CONVERT(VARCHAR(13),IJH_DOCDT,106) [DOC DT],IJH_DOCTYPE [DOC TYPE],IJH_NARRATION [NARRATION] FROM dbo.IJOURNAL_H WHERE (IJH_CR_BSU_ID='" & BSUID & "' or IJH_DR_BSU_ID='" & BSUID & "') AND IJH_FYEAR='" & FYEAR & "' AND IJH_DOCTYPE='IJV' "
                    StrSQL &= " )  A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = "[DOCNO] DESC"
                    Page.Title = "IJV Vouchers"
                    TitleLabel.Text = "IJV Vouchers"
                Case "REGENERATEREVENUE"
                    IsStoredProcedure = False
                    MultiSelect = False
                    StrSQL = "SELECT JNL_DOCNO,JVNO,DESCRIPTION FROM (SELECT DISTINCT AM.JNL_DOCNO,AM.JNL_DOCNO  as [JVNO],AM.JNL_NARRATION as [DESCRIPTION]" & _
                     " FROM OASISFIN..JOURNAL_D AM WITH(NOLOCK) WHERE ISNULL(JNL_REFDOCTYPE,'') = 'FEESMONTHEND' "
                    If Not Mainclass.cleanString(Request.QueryString("BSUID")) Is Nothing Then
                        StrSQL &= " AND AM.JNL_BSU_ID = '" & Mainclass.cleanString(Request.QueryString("BSUID")) & "' "
                    End If
                    If Not Mainclass.cleanString(Request.QueryString("date")) Is Nothing AndAlso CStr(Mainclass.cleanString(Request.QueryString("date"))).Trim <> "" Then
                        Dim JNL_Date As String = Mainclass.cleanString(Request.QueryString("date"))
                        Dim Month As String = JNL_Date.Split("/")(0)
                        Dim Year As String = JNL_Date.Split("/")(1)
                        Dim DOC_DT As String = Month & "/01/" & Year
                        StrSQL = StrSQL & " AND JNL_DOCDT BETWEEN dbo.fN_GetFirstDayOFmonth('" & DOC_DT & "') AND dbo.fN_GetLastDayOFmonth('" & DOC_DT & "') "
                    End If
                    StrSQL = StrSQL & ") AS A WHERE 1=1 "
                    strConn = ConnectionManger.GetOASISFINConnectionString
                    StrSortCol = " A.JNL_DOCNO DESC "
                    Page.Title = "Regenerate Revenue"
                    TitleLabel.Text = "Regenerate Revenue"
                Case "CCCollection"
                    HideFirstColumn = False
                    IsStoredProcedure = True
                    Dim BSUID, STU_TYPE As String
                    BSUID = "" : STU_TYPE = "S"
                    StrSQL = "[FEES].[GetCCCollection_Detail_For_Void]"
                    If Mainclass.cleanString(Request.QueryString("BSU").ToString) <> "" Then
                        BSUID = Mainclass.cleanString(Request.QueryString("BSU").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("STU_TYPE").ToString) <> "" Then
                        STU_TYPE = Mainclass.cleanString(Request.QueryString("STU_TYPE").ToString)
                    End If
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", BSUID, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@STU_TYPE", STU_TYPE, SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[REC NO] desc"
                    Page.Title = "Credit Card Collections"
                    TitleLabel.Text = "Credit Card Collections"
                Case "ALL_SOURCE_STUDENTS"
                    IsStoredProcedure = True
                    Dim ProviderBSUID, STU_TYPE, Stu_Bsu_Id As String
                    ProviderBSUID = "" : STU_TYPE = "S" : Stu_Bsu_Id = ""
                    StrSQL = "Get_All_Source_Student_List"

                    If Mainclass.cleanString(Request.QueryString("ProviderBSUID").ToString) <> "" Then
                        ProviderBSUID = Mainclass.cleanString(Request.QueryString("ProviderBSUID").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("STU_TYPE").ToString) <> "" Then
                        STU_TYPE = Mainclass.cleanString(Request.QueryString("STU_TYPE").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("Stu_Bsu_Id").ToString) <> "" Then
                        Stu_Bsu_Id = Mainclass.cleanString(Request.QueryString("Stu_Bsu_Id").ToString)
                    End If
                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@ProviderBsu_id", ProviderBSUID, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@Stu_Bsu_Id", Stu_Bsu_Id, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@STU_TYPE", STU_TYPE, SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = "[STU NO] desc"
                    Page.Title = "Student List"
                    TitleLabel.Text = "Student List"
                Case "TPTCARDCHARGES"
                    StrSQL = "Select ID,[CARD TYPE],[RATE],[DESCRIPTION] from(select TCT_ID ID ,TCT_DESCR [CARD TYPE],CAST(TCT_PRICE as VARCHAR) [RATE],TCT_REMARKS [DESCRIPTION]    FROM TPTCARDTYPE_M WHERE TCT_DELETED=0 "
                    If Mainclass.cleanString(Request.QueryString("Type")) = "S" Then
                        StrSQL = StrSQL & " and TCT_TYPE='S'"
                    Else
                        StrSQL = StrSQL & " and TCT_TYPE='E'"
                    End If
                    StrSQL = StrSQL & ") A where A.ID<>''  "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[CARD TYPE]"
                    Page.Title = "Card Type List"
                    TitleLabel.Text = "Card Type List"
                Case "TPTINVOICESFORCREDITNOTE"
                    Dim Client_id As String = ""
                    Dim InvType As String = ""
                    Client_id = Mainclass.cleanString(Request.QueryString("CLIENTID").ToString)
                    InvType = Mainclass.cleanString(Request.QueryString("InvType").ToString)

                    If Mainclass.cleanString(Request.QueryString("CNType")) = "Business Unit" Then
                        StrSQL = "select JHD_ID,substring(jnl_narration,12,12) [Invoice No.],JNL_CREDIT[Amount],JHD_DOCNO [Document Number],JNL_DOCTYPE,JHD_BSU_ID  "
                        StrSQL = StrSQL & " from OASIS_DAX.FIN.JOURNAL_H inner join OASIS_DAX.FIN.JOURNAL_D on JHD_DOCNO=JNL_DOCNO and JHD_BSU_ID=JNL_BSU_ID "
                        StrSQL = StrSQL & "where JHD_DAX_REF_DOCTYPE like '%STS%'  and JNL_BSU_ID not in('900501','900500') and left(JNL_ACT_ID,2)='08' "
                        StrSQL = StrSQL & " and  right(JHD_DAX_REF_DOCTYPE,len(JHD_DAX_REF_DOCTYPE)-3)='" & InvType & "'"
                        StrSQL = StrSQL & " and JNL_BSU_ID='" & Client_id & "' "
                    Else
                        StrSQL = "select JHD_ID,substring(jnl_narration,12,12) [Invoice No.],JNL_DEBIT[Amount],JHD_DOCNO [Document Number],JNL_DOCTYPE,JHD_BSU_ID  "
                        StrSQL = StrSQL & " from OASIS_DAX.FIN.JOURNAL_H inner join OASIS_DAX.FIN.JOURNAL_D on JHD_DOCNO=JNL_DOCNO and JHD_BSU_ID=JNL_BSU_ID "
                        StrSQL = StrSQL & "where JHD_DAX_REF_DOCTYPE like '%STS%'  and JNL_DOCTYPE='DN' and JNL_DEBIT>0 and left(JNL_ACT_ID,2)<>'08' "
                        StrSQL = StrSQL & " and  right(JHD_DAX_REF_DOCTYPE,len(JHD_DAX_REF_DOCTYPE)-3)='" & InvType & "'"
                        StrSQL = StrSQL & " and JNL_ACT_ID='" & Client_id & "' "
                    End If
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "[Invoice No.]"
                    Page.Title = "Invoice List"
                    TitleLabel.Text = "Invoice List"
                Case "NESCUSTOMERFORCREDITNOTE"
                    If Mainclass.cleanString(Request.QueryString("CNType")) = "Business Unit" Then
                        StrSQL = "Select ID,CODE,DESCR from( SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID ID, BUSINESSUNIT_M.BSU_ID CODE, BUSINESSUNIT_M.BSU_NAME  DESCR, replace(convert(varchar(30), getdate(), 106),' ','/') xfdate, '31/dec/2050' xedate "
                        StrSQL &= "FROM vw_oso_SERVICES_BSU_M INNER JOIN  BUSINESSUNIT_M ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID    "
                        StrSQL &= "WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID ='900500') AND  (vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1)) A where A.ID<>''  "
                    Else
                        StrSQL = "Select distinct ID,CODE,DESCR from( SELECT  AM.ACT_ID ID,AM.ACT_ID CODE,AM.ACT_NAME DESCR "
                        StrSQL &= "FROM oasisfin.dbo.vw_OSA_ACCOUNTS_M AM INNER JOIN oasisfin.dbo.ACCOUNTS_M AMC ON AMC.ACT_ID=AM.ACT_CTRLACC "
                        StrSQL &= "left outer join TPTCONTRACT_H  on AM.ACT_ID=CNH_CLIENT_ID left OUTER JOIN OASIS.dbo.EMPLOYEE_M on EMP_ID=CNH_SALESMAN_ID "
                        StrSQL &= " WHERE AM.ACT_ID IN(select DISTINCT ACT_ID  from oasisfin..ACCOUNTS_M inner join TPTCONTRACT_H on ',' + CNH_CLIENT_ID  + ',' like '%,' + act_id  + ',%' )"
                        StrSQL &= "AND AM.ACT_BSU_ID Like  '%" & Session("sBsuid") & "%' and Left(am.act_name,5)<>'INTER' UNION SELECT TLC_ACT_ID ID,TLC_ACT_ID,TLC_DESCR FROM TPTLEASECUSTOMER where TLC_ACT_ID not  in(select BSU_CONTRA_ACT_ID from oasis..businessunit_m where not BSU_CONTRA_ACT_ID is null )   ) A where A.ID<>'' "
                    End If
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "ID"
                    Page.Title = "Client"
                    TitleLabel.Text = "Client"
                Case "TPTGLLISTFORCREDITNOTE"
                    StrSQL = "Select ID,CODE,DESCR from( select ACT_ID ID, ACT_ID CODE,ACT_NAME Descr from  VW_GLLISTFORCREDITNOTE where act_bsu_id like '%" & Session("sBsuid") & "%') a where 1=1  "
                    strConn = ConnectionManger.GetOASISTRANSPORTConnectionString
                    StrSortCol = "DESCR"
                    Page.Title = "GL List"
                    TitleLabel.Text = "GL List"
                Case "ACCOTHFCNEW"
                    IsStoredProcedure = True
                    HideFirstColumn = True
                    Dim BSUID, COLLID, DT As String
                    BSUID = "" : COLLID = "" : DT = ""
                    StrSQL = "FEES.GET_OTH_FEECOLLECTION_MAPPING_ACCOUNTS_FOR_AX"

                    If Mainclass.cleanString(Request.QueryString("bsu").ToString) <> "" Then
                        BSUID = Mainclass.cleanString(Request.QueryString("bsu").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("collid").ToString) <> "" Then
                        COLLID = Mainclass.cleanString(Request.QueryString("collid").ToString)
                    End If
                    If Mainclass.cleanString(Request.QueryString("docdt").ToString) <> "" Then
                        DT = Mainclass.cleanString(Request.QueryString("docdt").ToString)
                    End If

                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@Bsu_id", BSUID, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@COL_ID", COLLID, SqlDbType.Int)
                    SPParam(2) = Mainclass.CreateSqlParameter("@DT", DT, SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASIS_FEESConnectionString
                    StrSortCol = ""
                    Page.Title = "Account List"
                    TitleLabel.Text = "Account List"
                Case "VENDOR"
                    IsStoredProcedure = True
                    HideFirstColumn = True
                    StrSQL = "FEES.GET_OTH_FEECOLLECTION_MAPPING_VENDOR_FOR_AX"

                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuId"), SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASISConnectionString
                    StrSortCol = ""
                    Page.Title = "Vendor"
                    TitleLabel.Text = "Vendor"
                Case "NON_PRF_ITEMLIST"
                    StrSQL = "select  ITM_ID, DESCRIPTION, COMMODITY, UNIT, PACKING, DETAILS, RATE from ("
                    StrSQL &= "select ITM_ID, ITM_DESCR DESCRIPTION, isnull(UCO_DESCR,'') COMMODITY, ITM_UNIT UNIT, ITM_PACKING PACKING, ITM_DETAILS DETAILS, cast(ITM_RATE as varchar) RATE "
                    StrSQL &= "FROM ITEM left OUTER JOIN UNSPSC_COMMODITY on ITM_UNSPSC=UCO_CODE "
                    StrSQL &= ") a where 1=1"
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = "DESCRIPTION"
                    Page.Title = "GL List"
                    TitleLabel.Text = "GL List"
                Case "TENANCYEMPLOYEENEW"

                    IsStoredProcedure = True
                    MultiSelect = False
                    Dim Sbsu As String = ""
                    Dim Lbsu As String = ""
                    StrSQL = "GET_EMP_TRF_LIST"

                    StrSortCol = "NAME"
                    Page.Title = "Employee List-TRF"
                    Sbsu = Mainclass.cleanString(Request.QueryString("SBsuid").ToString())
                    Lbsu = Mainclass.cleanString(Request.QueryString("LBsuid").ToString())

                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@SBSU_ID", Sbsu, SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@LBSU_ID", Lbsu, SqlDbType.VarChar)
                    SPParam(2) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)

                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString

                Case "REVENDOR"
                    IsStoredProcedure = True
                    MultiSelect = False
                    Dim Sbsu As String = ""
                    Dim Lbsu As String = ""
                    StrSQL = "GET_TRF_VENDOR_LIST"

                    StrSortCol = "NAME"
                    Page.Title = "Vendor List-TRF"

                    ReDim SPParam(2)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)


                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                Case "EMPHOUSEWAIVER"
                    IsStoredProcedure = True
                    'HideFirstColumn = True
                    StrSQL = "GET_EMP_HOUSEWAIVER_LIST"

                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuId"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = ""
                    Page.Title = "Employee List"

                Case "TRFLISTFORDEHIRING"
                    IsStoredProcedure = True
                    'HideFirstColumn = True
                    StrSQL = "GET_TRF_LIST"

                    ReDim SPParam(1)
                    SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuId"), SqlDbType.VarChar)
                    SPParam(1) = Mainclass.CreateSqlParameter("@USERNAME", Session("sUsr_name"), SqlDbType.VarChar)
                    strConn = ConnectionManger.GetOASIS_PUR_INVConnectionString
                    StrSortCol = ""
                    Page.Title = "TRF List"
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function getLikeWhereQuerry(ByVal ColumnName As String, ByVal LikeStr As String) As String
        Dim whrQuery As String = ""
        Dim Arr() As String
        Arr = LikeStr.Split("|")
        Dim i As Int16
        For i = 0 To Arr.Length - 1
            whrQuery &= IIf(whrQuery <> "", " OR ", "") & "',' + cast(" & ColumnName & " as varchar) + ',' LIKE '%" & Arr(i) & "%'"
        Next
        If whrQuery = "" Then
            whrQuery = " (1 = 1) "
        Else
            whrQuery = " ( " & whrQuery & " ) "
        End If
        getLikeWhereQuerry = whrQuery
    End Function
    Private Sub gridbind()
        Try
            Dim GridData As New DataTable
            Dim str_conn As String = ""
            Dim str_Sql As String = ""
            Dim str_SortCol As String = ""
            SetSQLAndConnectionString(str_Sql, str_conn, str_SortCol)
            Dim lblID As New Label
            Dim lblName As New Label
            Dim str_Filter As String = ""
            Dim i As Int16
            Dim ColName As String
            If gvDetails.Rows.Count > 0 Then
                If ViewState("GridTable").Columns.count > 0 Then str_Filter &= getColumnFilterString(h_selected_menu_1.Value, gvDetails.HeaderRow, "txtCol1", ViewState("GridTable").Columns(0).ColumnName)
                If ViewState("GridTable").Columns.count > 1 Then str_Filter &= getColumnFilterString(h_Selected_menu_2.Value, gvDetails.HeaderRow, "txtCol2", ViewState("GridTable").Columns(1).ColumnName)
                If ViewState("GridTable").Columns.count > 2 Then str_Filter &= getColumnFilterString(h_selected_menu_3.Value, gvDetails.HeaderRow, "txtCol3", ViewState("GridTable").Columns(2).ColumnName)
                If ViewState("GridTable").Columns.count > 3 Then str_Filter &= getColumnFilterString(h_selected_menu_4.Value, gvDetails.HeaderRow, "txtCol4", ViewState("GridTable").Columns(3).ColumnName)
                If ViewState("GridTable").Columns.count > 4 Then str_Filter &= getColumnFilterString(h_selected_menu_5.Value, gvDetails.HeaderRow, "txtCol5", ViewState("GridTable").Columns(4).ColumnName)
                If ViewState("GridTable").Columns.count > 5 Then str_Filter &= getColumnFilterString(h_selected_menu_6.Value, gvDetails.HeaderRow, "txtCol6", ViewState("GridTable").Columns(5).ColumnName)
                If ViewState("GridTable").Columns.count > 6 Then str_Filter &= getColumnFilterString(h_selected_menu_7.Value, gvDetails.HeaderRow, "txtCol7", ViewState("GridTable").Columns(6).ColumnName)
                If ViewState("GridTable").Columns.count > 7 Then str_Filter &= getColumnFilterString(h_selected_menu_8.Value, gvDetails.HeaderRow, "txtCol8", ViewState("GridTable").Columns(7).ColumnName)
                If ViewState("GridTable").Columns.count > 8 Then str_Filter &= getColumnFilterString(h_selected_menu_9.Value, gvDetails.HeaderRow, "txtCol9", ViewState("GridTable").Columns(8).ColumnName)
                If ViewState("GridTable").Columns.count > 9 Then str_Filter &= getColumnFilterString(h_selected_menu_10.Value, gvDetails.HeaderRow, "txtCol10", ViewState("GridTable").Columns(9).ColumnName)
            End If
            Dim OrderByStr As String = ""
            If str_SortCol <> "" Then
                OrderByStr = "order by " & str_SortCol
            Else
                OrderByStr = "order by  1"
            End If
            If Not IsStoredProcedure And Not IsDataTableAsList Then
                GridData = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_Filter & OrderByStr).Tables(0)
            ElseIf IsDataTableAsList Or IsStoredProcedure Then
                If IsStoredProcedure Then
                    GridData = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_Sql, CType(SPParam, SqlParameter())).Tables(0)
                    GridListData = GridData
                Else
                    GridData = GridListData
                End If
                str_Filter = " 1 = 1 " & str_Filter
                Dim mySelectDT As New DataTable
                Dim mRow As DataRow
                mySelectDT = GridListData.Clone
                Dim FilterCol As String
                If str_SortCol <> "" Then
                    FilterCol = str_SortCol
                Else
                    FilterCol = GridListData.Columns(0).ColumnName
                End If
                For Each mRow In GridListData.Select(str_Filter, FilterCol)
                    mySelectDT.ImportRow(mRow)
                Next
                GridData = mySelectDT.Copy()
            End If


            Dim bindDT As New DataTable
            bindDT = GridData.Copy
            ViewState("GridTable") = GridData
            Dim HeaderLabel As New Label
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
            Next
            If bindDT.Columns.Count < 10 Then
                For i = bindDT.Columns.Count + 1 To 10
                    ColName = "Col" & i.ToString
                    bindDT.Columns.Add(ColName, System.Type.GetType("System.String"))
                    gvDetails.Columns(i).Visible = False
                Next
            End If

            Dim DColIndex, FColIndex As String
            FColIndex = "" : DColIndex = ""
            For i = 1 To GridData.Columns.Count
                ColName = "Col" & i.ToString
                bindDT.Columns(i - 1).ColumnName = ColName
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Int32") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Or bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Decimal") Then
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.Double") Then
                        FColIndex &= "|" & (i).ToString
                    End If
                Else
                    gvDetails.Columns(i).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End If
                If bindDT.Columns(i - 1).DataType Is System.Type.GetType("System.DateTime") Then
                    DColIndex &= "|" & (i).ToString
                End If
            Next
            FloatColIndex = FColIndex
            DateColIndex = DColIndex

            gvDetails.DataSource = bindDT
            If bindDT.Rows.Count = 0 Then
                bindDT.Rows.Add(bindDT.NewRow())
                gvDetails.DataBind()
                Dim columnCount As Integer = gvDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvDetails.Rows(0).Cells.Clear()
                gvDetails.Rows(0).Cells.Add(New TableCell)
                gvDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvDetails.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvDetails.DataBind()
            End If
            For i = 1 To GridData.Columns.Count
                HeaderLabel = gvDetails.HeaderRow.FindControl("lblHeaderCol" & i)
                If Not HeaderLabel Is Nothing Then
                    HeaderLabel.Text = GridData.Columns(i - 1).ColumnName
                End If
            Next
            gvDetails.Columns(1).Visible = Not HideFirstColumn
            gvDetails.Columns(0).Visible = MultiSelect
            btnSelect.Visible = MultiSelect
            chkSelAll.Visible = MultiSelect
            'txtSearch = gvDetails.HeaderRow.FindControl("txtName")
            'txtSearch.Text = str_txtName
            SetHeaderFilterString()
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub LinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim javaSr As String
        If MultiSelect Then Exit Sub
        Dim lnk1 As New LinkButton
        Dim lnk2 As New LinkButton
        Dim lnk3 As New LinkButton
        Dim lnk4 As New LinkButton
        Dim lnk5 As New LinkButton
        Dim lnk6 As New LinkButton
        Dim lnk7 As New LinkButton
        Dim lnk8 As New LinkButton
        Dim lnk9 As New LinkButton
        Dim lnk10 As New LinkButton

        lnk1 = sender.Parent.FindControl("Lnk1")
        lnk2 = sender.Parent.FindControl("Lnk2")
        lnk3 = sender.Parent.FindControl("Lnk3")
        lnk4 = sender.Parent.FindControl("Lnk4")
        lnk5 = sender.Parent.FindControl("Lnk5")
        lnk6 = sender.Parent.FindControl("Lnk6")
        lnk7 = sender.Parent.FindControl("Lnk7")
        lnk8 = sender.Parent.FindControl("Lnk8")
        lnk9 = sender.Parent.FindControl("Lnk9")
        lnk10 = sender.Parent.FindControl("Lnk10")

        Dim l_Str_Msg As String = ""
        If Not lnk1 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk1.Text
        If Not lnk2 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk2.Text
        If Not lnk3 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk3.Text
        If Not lnk4 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk4.Text
        If Not lnk5 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk5.Text
        If Not lnk6 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk6.Text
        If Not lnk7 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk7.Text
        If Not lnk8 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk8.Text
        If Not lnk9 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk9.Text
        If Not lnk10 Is Nothing Then l_Str_Msg &= IIf(l_Str_Msg <> "", "||", "") & lnk10.Text
        ' l_Str_Msg = l_Str_Msg.Replace("'", "\'")
        'If (Not lblcode Is Nothing) Then
        l_Str_Msg = UtilityObj.CleanupStringForJavascript(l_Str_Msg)
        l_Str_Msg = l_Str_Msg.Replace(vbCrLf, "")
        l_Str_Msg = l_Str_Msg.Replace(vbCr, "")
        l_Str_Msg = l_Str_Msg.Replace(vbLf, "")

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & l_Str_Msg & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")
        'h_SelectedId.Value = "Close"

        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write(" var oArg = new Object();")
        'Response.Write("oArg.NameandCode ='" & l_Str_Msg + "||" + l_Str_Msg & "' ; ")
        'Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg + "||" + l_Str_Msg & "');")
        'Response.Write("oWnd.close(oArg);")
        'Response.Write("} </script>")


        javaSr = "function listen_window(){"
        javaSr += " var oArg = new Object();"
        javaSr += "oArg.NameandCode ='" & l_Str_Msg & "' ; "
        javaSr += "var oWnd = GetRadWindow('" & l_Str_Msg & "');"
        javaSr += "oWnd.close(oArg);"
        javaSr += "}"
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "POPUP", javaSr, True)

        h_SelectedId.Value = "Close"

        'End If
    End Sub
    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim chkControl As New HtmlInputCheckBox
        Dim lblID As LinkButton
        Dim IDs As String = ""
        If chkSelAll.Checked Then
            IDs = hdnSelIDs.Value
        Else
            For Each grow As GridViewRow In gvDetails.Rows
                chkControl = grow.FindControl("chkControl")
                If chkControl.Checked Then
                    lblID = TryCast(grow.FindControl("Lnk1"), LinkButton)
                    IDs &= IIf(IDs <> "", "|", "") & lblID.Text
                End If
            Next
        End If
        IDs = IDs.Replace("'", "\'")

        RememberOldValues()
        h_SelectedId.Value = ""
        If ViewState("checked_items") IsNot Nothing Then
            For i As Integer = 0 To ViewState("checked_items").Count - 1
                If ViewState("checked_items")(i).ToString.Length > 0 Then h_SelectedId.Value &= IIf(h_SelectedId.Value <> "", "||", "") & ViewState("checked_items")(i).ToString
            Next
        End If

        h_SelectedId.Value = h_SelectedId.Value.Replace("'", "\'")

        'If (Not lblcode Is Nothing) Then
        'Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write("window.returnValue = '" & h_SelectedId.Value & "';")
        'Response.Write("window.close();")
        'Response.Write("} </script>")
        'ts
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"
    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        RememberOldValues()
        gvDetails.PageIndex = e.NewPageIndex
        gridbind()
        SetChk(Me.Page)
        RePopulateValues()
    End Sub

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            Dim FloatColIndexArr(), i, ControlName As String
            FloatColIndexArr = FloatColIndex.Split("|")
            Dim lnk As LinkButton
            For Each i In FloatColIndexArr
                If IsNumeric(i) Then
                    ControlName = "lnk" & i.ToString
                    lnk = TryCast(e.Row.FindControl(ControlName), LinkButton)
                    If Not lnk Is Nothing Then
                        If IsNumeric(lnk.Text) Then
                            lnk.Text = Convert.ToDouble(lnk.Text).ToString("####0.00")
                        End If
                    End If
                End If
            Next
            Dim DateColIndexArr() As String
            DateColIndexArr = DateColIndex.Split("|")
            For Each i In DateColIndexArr
                If IsNumeric(i) Then
                    ControlName = "lnk" & i.ToString
                    lnk = TryCast(e.Row.FindControl(ControlName), LinkButton)
                    If Not lnk Is Nothing Then
                        If IsDate(lnk.Text) Then
                            lnk.Text = CDate(lnk.Text).ToString("dd/MMM/yyyy")
                        End If
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged
        Try
            hdnSelIDs.Value = ""
            If chkSelAll.Checked Then
                Dim mTTable As New DataTable
                mTTable = ViewState("GridTable")
                If mTTable Is Nothing Then Exit Sub
                Dim mRow As DataRow
                For Each mRow In mTTable.Rows
                    hdnSelIDs.Value &= IIf(hdnSelIDs.Value <> "", "|", "") & mRow(0)
                Next
            End If
            SetChk(Me.Page)
            RememberAllValues()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk IsNot Nothing Then
                    chk.Checked = chkSelAll.Checked
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Sub RememberOldValues()
        Dim itmIdList As New ArrayList()
        Dim bindDt As DataTable
        bindDt = gvDetails.DataSource
        For Each row As GridViewRow In gvDetails.Rows
            Dim index As String = DirectCast(row.FindControl("lnk1"), LinkButton).Text
            Dim result As Boolean = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox).Checked
            If ViewState("checked_items") IsNot Nothing Then
                itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
            End If
            If result Then
                If Not itmIdList.Contains(index) Then
                    itmIdList.Add(index)
                End If
            Else
                itmIdList.Remove(index)
            End If
        Next
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub
    Private Sub RememberAllValues()
        Dim itmIdList As New ArrayList()
        If ViewState("checked_items") IsNot Nothing Then
            itmIdList = DirectCast(ViewState("checked_items"), ArrayList)
        End If
        itmIdList.Clear()
        If chkSelAll.Checked Then
            Dim mTTable As New DataTable
            mTTable = ViewState("GridTable")
            If mTTable Is Nothing Then Exit Sub
            Dim mRow As DataRow
            For Each mRow In mTTable.Rows
                If Not itmIdList.Contains(mRow(0)) Then
                    itmIdList.Add(mRow(0))
                End If
            Next
        End If
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            ViewState("checked_items") = itmIdList
        End If
    End Sub
    Private Sub RePopulateValues()
        Dim itmIdList As ArrayList = DirectCast(ViewState("checked_items"), ArrayList)
        If itmIdList IsNot Nothing AndAlso itmIdList.Count > 0 Then
            For Each row As GridViewRow In gvDetails.Rows
                Dim index As String = DirectCast(row.FindControl("lnk1"), LinkButton).Text
                If itmIdList.Contains(index) Then
                    Dim myCheckBox As HtmlInputCheckBox = DirectCast(row.FindControl("chkControl"), HtmlInputCheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub
End Class
