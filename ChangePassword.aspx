<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="ChangePassword"   Title="::::PHOENIX:::: Online Student Administration System::::"%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <title>Login Page</title>
     <base target="_self" />
     <script Language="javascript" type="text/javascript">
    
function CloseWindow()
    {
    window.open('','_self','');
    window.close();
    }
  
function ClearAll()
    { document.getElementById('<%=txtConfPassword.ClientID %>').value ='';
    document.getElementById('<%=txtCurrentPassword.ClientID %>').value ='';
    
    document.getElementById('<%=txtNewpassword.ClientID %>').value ='';
    }
        
    
     </script>
     
     <SCRIPT> 
function doBlink() { 
  var i =  document.getElementById("BLINK").style.color ; 
  if ( i=='#ff0000')
   document.getElementById("BLINK").style.color ='#ffffff';
   else
   document.getElementById("BLINK").style.color ='#ff0000';
}

function startBlink() { 
 document.getElementById("BLINK").style.color ='#ff0000';
  if (document.all)
    setInterval("doBlink()",800)
}
window.onload = startBlink;
    </SCRIPT>
      <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon"> 
      <!-- Bootstrap core JavaScript-->
                <script src="/PHOENIXBETA/vendor/jquery/jquery.min.js"></script>
                <script src="/PHOENIXBETA/vendor/jquery-ui/jquery-ui.min.js"></script>
                <script src="/PHOENIXBETA/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
              
    
    <!-- Bootstrap core CSS-->
    <link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/PHOENIXBETA/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="/cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">

</head>
<body class="bg-dark bg-white" id="page-top">


    <div class="card m-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> Change Password
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <form id="form1" defaultfocus="imgBtnLogin" runat="server"> 
        <table width="100%">
                        <tr> 
                            <td id="BLINK" width="100%" class="error">
                                <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                                </ajaxToolkit:ToolkitScriptManager>
                                Your Password is Expired
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
        <table align="center" width="100%">
        
               <tr valign="top" >
                    
                        <td align="left" width="30%">
                           <span class="field-label"> Current Password</span><ajaxToolkit:PasswordStrength
                                    ID="PasswordStrength1" runat="server" DisplayPosition="RightSide" HelpStatusLabelID="TextBox1_HelpLabel"
                                    MinimumLowerCaseCharacters="2" MinimumNumericCharacters="2" MinimumSymbolCharacters="0"
                                    PreferredPasswordLength="6" PrefixText="&nbsp;Strength:" RequiresUpperAndLowerCaseCharacters="false"
                                    StrengthIndicatorType="Text" StrengthStyles="red;blue;grey;yellow;green" TargetControlID="txtNewpassword"
                                    TextStrengthDescriptions="Very Poor&nbsp;;Weak&nbsp;;Average&nbsp;;Strong;Excellent">
                                </ajaxToolkit:PasswordStrength>
                            </td>
                        
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCurrentPassword" runat="server" CssClass="inputbox" MaxLength="100"
                                TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtCurrentPassword"
                                Display="Dynamic" ErrorMessage="CurrentPassword can not be left empty" SetFocusOnError="True" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                            </td>
                    
                    </tr>
                <tr>
                    
                    <td align="left" width="20%">
                        <span class="field-label">New Password</span></td>
                    
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtNewpassword" runat="server" CssClass="inputbox" MaxLength="100"
                            TextMode="Password"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtNewpassword"
                            Display="Dynamic" ErrorMessage="Your password must contain atleast  6 characters!"
                            SetFocusOnError="True" ValidationExpression="[a-zA-Z0-9.~!@#$%^&*()_+;:<>,?/\|]{6,20}" CssClass="error" ForeColor="">*</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewpassword"
                            ErrorMessage="Please enter your new password" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator><br />
                        <asp:Label ID="TextBox1_HelpLabel" runat="server" CssClass="error_password"></asp:Label></td>
                     
                </tr>
                <tr>
                   
                    <td align="left" width="20%">
                        <span class="field-label">Confirm Password</span></td>
                   
                    <td align="left" width="30%">
                        <asp:TextBox ID="txtConfPassword" runat="server" CssClass="inputbox" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1"
                                runat="server" ControlToCompare="txtNewpassword" ControlToValidate="txtConfPassword"
                                ErrorMessage="Password do not match" CssClass="error" Display="Dynamic" ForeColor="">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfPassword"
                            ErrorMessage="Confirm password can not be left empty" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator></td>
                     
                </tr>
                </table>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:" ForeColor="" SkinID="error" CssClass="error" />
                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td valign="bottom" align="center" colspan="2">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                <asp:Button ID="btnClear" runat="server" CausesValidation="False" CssClass="button"
                    Text="Clear" OnClientClick="ClearAll();return false;" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Close" OnClientClick="CloseWindow();" /></td>
        </tr>
    
                    </table>
    </form>

            </div>
        </div>
    </div>
</body>
</html>
