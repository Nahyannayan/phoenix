﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Partial Class QuestionBank_UserControls_usrQuestionBank
    Inherits System.Web.UI.UserControl

    Public Property sbm_id()
        Get

        End Get
        Set(ByVal value)
            h_SBM_ID.Value = value
        End Set
    End Property

    Public Property grd_id()
        Get

        End Get
        Set(ByVal value)
            h_GRD_ID.Value = value
        End Set
    End Property

    Dim intShow As Integer
    Public Property Showpanel() As Integer
        Get
            Return intShow
        End Get
        Set(ByVal value As Integer)
            intShow = value
        End Set
    End Property

    Public Property skl_id()
        Get

        End Get
        Set(ByVal value)
            h_SKL_ID.Value = value
        End Set
    End Property

    Public Property dl_id()
        Get

        End Get
        Set(ByVal value)
            h_DL_ID.Value = value
        End Set
    End Property

    Public Property topic_id()
        Get

        End Get
        Set(ByVal value)
            h_Topic_ID.Value = value
        End Set
    End Property

    Public Property qpm_id()
        Get

        End Get
        Set(ByVal value)
            h_QPM_ID.Value = value
        End Set
    End Property


    Dim strMainId As String
    Public Property main_id() As String
        Get

        End Get
        Set(ByVal value As String)
            strMainId = value
        End Set
    End Property

    Dim strAcdId As String
    Public Property acd_id() As String
        Get

        End Get
        Set(ByVal value As String)
            strAcdId = value
        End Set
    End Property

    Dim strQpsId As String
    Public Property qps_id() As String
        Get

        End Get
        Set(ByVal value As String)
            strQpsId = value
        End Set
    End Property

#Region "Private Methods"

    Function getSerialNo() As String
        ViewState("slno") = ViewState("slno") + 1
        Return ViewState("slno").ToString + ")"
    End Function



    Sub BindQuestion()
        ViewState("slno") = 0

        dlQuestions.DataSource = Nothing
        dlQuestions.DataBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSM_ID,QSM_DESCR,QSM_MAXMARK FROM QST.QUESTIONBANK_M WHERE " _
                                  & " QSM_BSU_ID='" + Session("SBSUID") + "'" _
                                  & " AND QSM_GRD_ID='" + h_GRD_ID.Value + "'" _
                                  & " AND QSM_SBM_ID='" + h_SBM_ID.Value + "'" _
                                  & " AND QSM_ID NOT IN(SELECT QPD_QSM_ID FROM QSP.QUESTIONPAPER_D WHERE QPD_QPM_ID=" + h_QPM_ID.Value + " AND QPD_QSM_ID IS NOT NULL)"
        If h_SKL_ID.Value <> "0" Then
            str_query += " AND QSM_SKL_ID='" + h_SKL_ID.Value + "'"
        End If
        If h_DL_ID.Value <> "0" Then
            str_query += " AND QSM_DL_ID='" + h_DL_ID.Value + "'"
        End If
        If (h_Topic_ID.Value <> "0" And h_Topic_ID.Value <> "") Then
            str_query += " AND QSM_TOP_ID='" + h_Topic_ID.Value + "'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        dlQuestions.DataSource = ds
        dlQuestions.DataBind()
    End Sub

    Sub BindOptionImage(ByVal qsd_id As String, ByVal imgOpt As RadBinaryImage)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSD_IMAGE FROM  QST.QUESTIONBANK_D WHERE  QSD_ID=" + qsd_id + " AND QSD_IMAGE IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            imgOpt.DataValue = ds.Tables(0).Rows(0).Item(0)
            imgOpt.Visible = True
        End If
    End Sub

    Sub BindQuestionImage(ByVal qsm_id As String, ByVal imgQuest As RadBinaryImage)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSM_IMAGE FROM QST.QUESTIONBANK_M WHERE QSM_ID=" + qsm_id + " AND QSM_IMAGE IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            imgQuest.DataValue = ds.Tables(0).Rows(0).Item(0)
            imgQuest.Visible = True
        End If
    End Sub
    Sub BindQuestionOptions(ByVal qsm_id As String, ByVal gvOption As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSD_ID,QSD_SLNO,QSD_DESCR,ISNULL(QSD_bANSWER,'FALSE') QSD_bANSWER FROM QST.QUESTIONBANK_D WHERE QSD_QSM_ID=" + qsm_id

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOption.DataSource = ds
        gvOption.DataBind()
        If gvOption.Rows.Count > 0 Then
            gvOption.HeaderRow.Visible = False
        End If
    End Sub

    Sub saveQuestionPaperFromBank()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString

        Dim strQsm As String = ""

        Dim i As Integer
        Dim lblQsmId As Label
        Dim chkSelect As CheckBox

        For i = 0 To dlQuestions.Items.Count - 1
            With dlQuestions.Items(i)
                chkSelect = .FindControl("chkSelect")
                lblQsmId = .FindControl("lblQsmId")
                If chkSelect.Checked = True Then
                    If strQsm <> "" Then
                        strQsm += "|"
                    End If
                    strQsm += lblQsmId.Text
                End If
            End With
        Next

        Dim str_query As String = "exec QSP.saveQUESTIONSFROMBANK " _
                                & " @ACD_ID=" + strAcdId + "," _
                                & " @QPM_ID=" + h_QPM_ID.Value + "," _
                                & " @QSM_IDS='" + strQsm + "'," _
                                & " @QPS_ID=" + Val(strQpsId).ToString + "," _
                                & " @QPD_PARENT_ID=" + strMainId + ""

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region
    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblQsd As Label = e.Row.FindControl("lblQsdId")
            Dim imgOpt As RadBinaryImage = e.Row.FindControl("imgOpt")
            BindOptionImage(lblQsd.Text, imgOpt)
        End If
    End Sub


    Protected Sub dlQuestions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlQuestions.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblQsmId As Label = e.Item.FindControl("lblQsmId")
            Dim imgQuest As RadBinaryImage = e.Item.FindControl("imgQuest")
            Dim gvOptions As GridView = e.Item.FindControl("gvOptions")
            BindQuestionImage(lblQsmId.Text, imgQuest)
            BindQuestionOptions(lblQsmId.Text, gvOptions)
        End If
    End Sub
End Class
