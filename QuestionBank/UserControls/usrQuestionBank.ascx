﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrQuestionBank.ascx.vb" Inherits="QuestionBank_UserControls_usrQuestionBank" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

 <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
   <script language="javascript" type="text/javascript">
      

        function getImage() {
            var sFeatures;
            sFeatures = "dialogWidth: 350px; ";
            sFeatures += "dialogHeight: 120px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = "../QuestionBank/uploadImage.aspx"
            window.showModalDialog(url, "_self", sFeatures);

            return false;

        }


        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') {
                    curr_elem.checked = !master_box.checked;
                }
            }
            master_box.checked = !master_box.checked;
        }
    </script>


                            <div style="height: 450px;Width:800px;overflow: scroll">
                                 <asp:CheckBox ID="chkAll" runat="server" Text="Select All" onclick="javascript:fnSelectAll(this);" />
                                 <asp:DataList ID="dlQuestions" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                    ShowHeader="False" Width="600px">
                                    <ItemTemplate>
                                        <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblSerialNo" Font-Size="14px" runat="server" Text='<%# getSerialNo() %>'></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblQuestion" runat="server" Text='<%# BIND("QSM_DESCR") %>' Width="500px"
                                                        Font-Size="14px"></asp:Label>
                                                    <asp:Label ID="lblQsmId" runat="server" Text='<%# BIND("QSM_ID") %>' Visible="false"
                                                        Width="118px"></asp:Label>
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <telerik:RadBinaryImage ID="imgQuest" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td valign="top">
                                                    <asp:GridView ID="gvOptions" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                        BorderStyle="None" OnRowDataBound="gvOptions_RowDataBound">
                                                        <RowStyle HorizontalAlign="Center" BorderStyle="None" BorderWidth="0px" />
                                                        <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <table border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblQsdId" runat="server" Text='<%# Bind("QSD_ID") %>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lblSlno" Font-Size="14px" runat="server" Text='<%# Bind("QSD_SLNO") %>'></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblDescription" Font-Size="14px" runat="server" Text='<%# Bind("QSD_DESCR") %>'></asp:Label>
                                                                                <asp:Image ID="imgAnswer" runat="server" ImageUrl="~/Images/bluetick.jpg" Visible='<%# Bind("QSD_bANSWER") %>' />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <telerik:RadBinaryImage ID="imgOpt" runat="server" Visible="false" />
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" BorderStyle="None"></ItemStyle>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle Wrap="False" Height="0px" BorderStyle="None" />
                                                        <EditRowStyle Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                   
        
   <asp:HiddenField ID="h_SBM_ID" runat="server" />
    <asp:HiddenField ID="h_SKL_ID" runat="server" />
     <asp:HiddenField ID="h_DL_ID" runat="server" />
    <asp:HiddenField ID="h_GRD_ID" runat="server" />
    <asp:HiddenField ID="h_Topic_ID" runat="server" />
    <asp:HiddenField ID="h_QPD_ID" runat="server" />
     <asp:HiddenField ID="h_QPM_ID" runat="server" />
      <asp:HiddenField ID="h_QPS_ID" runat="server" />
       <asp:HiddenField ID="h_MAIN_QPD_ID" runat="server" />
         