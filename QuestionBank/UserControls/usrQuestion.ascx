﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrQuestion.ascx.vb"
    Inherits="QuestionBank_UserControls_usrQuestion" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<%-- Set the icons for the WIRIS editor tools--%>
<style type="text/css">
    .reToolbar.Default .WIRIScas
    {
        background-image: url(pluginwiris/core/wiris-cas.gif);
    }
    .reToolbar.Default .WIRISformula
    {
        background-image: url(pluginwiris/core/wiris-formula.gif);
    }
</style>
<%-- Load the core WIRIS script --%>

<script type="text/javascript"  src="../pluginwiris/core/core.js"></script>


<table>
    <tr>
        <td>
         
            <telerik:RadEditor ID="rdEditor" runat="server" ToolsFile="../QuestionBank/XML/Qtools.xml"
                Height="200px" Width="400px" EditModes="Design">

                <Tools>
                    <telerik:EditorToolGroup>
                        <telerik:EditorTool Name="WIRISformula" Text="Insert/edit a WIRIS Formula" />
                        <telerik:EditorTool Name="WIRIScas" Text="Insert/Edit with WIRIS CAS" />
                        <telerik:EditorTool Enabled="true" Name="FontName" />
                        <telerik:EditorTool Enabled="true" Name="FontSize" />
                        <telerik:EditorTool Enabled="true" Name="Bold" />
                         <telerik:EditorTool Enabled="true" Name="Italic" />
                          <telerik:EditorTool Enabled="true" Name="Underline" />
                    </telerik:EditorToolGroup>
                </Tools>
                  <FontNames>
                    <telerik:EditorFont Value="Times New Roman" />
                    <telerik:EditorFont Value="Arial" />
                    <telerik:EditorFont Value="Aparajita" />
                  </FontNames>
                  
                <Content>
                

                </Content>
            </telerik:RadEditor>
            <%-- Load the client script for the WIRIS tools. Note that this must be after the RadEditor instance on the page. --%>

            <script type="text/javascript" src="../pluginwiris/RadEditor_demo.js"></script>

        </td>
    </tr>
 
</table>
