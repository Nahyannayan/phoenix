﻿
Partial Class QuestionBank_UserControls_usrQuestion
    Inherits System.Web.UI.UserControl


    Public Property TxtHeight() As Integer
        Get

        End Get
        Set(ByVal value As Integer)
            rdEditor.Height = value
        End Set
    End Property

    Public Property TxtValue() As String
        Get
            Return rdEditor.Content.ToString()
        End Get
        Set(ByVal value As String)
            rdEditor.Content = value
        End Set
    End Property
End Class
