﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="qbSkills_M.aspx.vb" Inherits="QuestionBank_qbSkills_M" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Skills
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="center" valign="bottom" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"  Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table id="Table2" runat="server" align="center" width="100%">                            

                                <tr>
                                    <td align="center" width="100%">
                                        <asp:GridView ID="gvSkill" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords."
                                             PageSize="20" >
                                            <RowStyle Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSklId" runat="server" Text='<%# Bind("SKL_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Skill">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSkill" runat="server" Width="300px" Text='<%# Bind("SKL_DESCR") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Short Name">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtShort" runat="server" Width="100px" Text='<%# Bind("SKL_SHORTNAME") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Order">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOrder" runat="server" Width="50px" Text='<%# Bind("SKL_ORDER") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="objid" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDelete" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="index" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="delete"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                            TabIndex="7" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div> 
</asp:Content>

