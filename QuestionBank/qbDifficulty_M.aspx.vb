﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data

Partial Class QuestionBank_qbDifficulty_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C700010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    GridBind()
                    gvDifficulty.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub


#Region "Private Methods"



    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DL_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DL_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DL_ORDER"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DL_SHORTNAME"
        dt.Columns.Add(column)

        Return dt
    End Function
    Sub GridBind()
        Dim dt As DataTable = SetDataTable()
        Dim bShowSlno As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT DL_ID,ISNULL(DL_DESCR,'') DL_DESCR,ISNULL(DL_ORDER,0) DL_ORDER,ISNULL(DL_SHORTNAME,'') DL_SHORTNAME FROM " _
                                & " QST.DIFFICULTYLEVEL_M WHERE " _
                                & " DL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY DL_ORDER"

        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = "edit"
                dr.Item(3) = i.ToString
                dr.Item(4) = .Item(2)
                dr.Item(5) = .Item(3)
                dt.Rows.Add(dr)
            End With
        Next

        If ds.Tables(0).Rows.Count > 0 Then
            bShowSlno = True
        End If

        'add empty rows to show 50 rows
        For i = 0 To 15 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = "add"
            dr.Item(3) = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item(4) = ""
            dr.Item(5) = ""
            dt.Rows.Add(dr)
        Next

        Session("dtUnit") = dt



        gvDifficulty.DataSource = dt
        gvDifficulty.DataBind()


    End Sub

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString

        Dim str_query As String

        Dim i As Integer
        Dim lblDLId As Label
        Dim txtDifficulty As TextBox
        Dim txtOrder As TextBox
        Dim txtShort As TextBox
        Dim lblDelete As Label
        Dim mode As String = ""

        For i = 0 To gvDifficulty.Rows.Count - 1
            With gvDifficulty.Rows(i)
                lblDLId = .FindControl("lblDLId")
                txtDifficulty = .FindControl("txtDifficulty")
                txtOrder = .FindControl("txtOrder")
                lblDelete = .FindControl("lblDelete")
                txtShort = .FindControl("txtShort")
                If lblDelete.Text = "1" Then
                    mode = "Delete"
                ElseIf lblDLId.Text = "0" And txtDifficulty.Text <> "" Then
                    mode = "Add"
                ElseIf txtDifficulty.Text <> "" Then
                    mode = "Edit"
                Else
                    mode = ""
                End If

                If mode <> "" Then
                    str_query = "exec [QST].[saveDIFFICULTYLEVEL_M] " _
                   & " @DL_ID=" + lblDLId.Text + "," _
                   & " @DL_BSU_ID='" + Session("SBSUID") + "'," _
                   & " @DL_DESCR='" + txtDifficulty.Text + "'," _
                   & " @DL_SHORTNAME='" + txtShort.Text + "'," _
                   & " @DL_ORDER='" + Val(txtOrder.Text).ToString + "'," _
                   & " @MODE='" + mode + "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End With
        Next

        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvDifficulty.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#End Region
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
    End Sub
End Class
