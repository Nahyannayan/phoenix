<%@ Page Language="VB" AutoEventWireup="false" CodeFile="qbshowTopics.aspx.vb" Inherits="QuestionBank_qbshowTopics" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 <base target="_self" /> 
    <title>Select Topics</title>
   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
     <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css"/>

     <script language="javascript" type="text/javascript">
            
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
    </script>
 </head>
<body onload="listen_window();" >
    <form id="form2" runat="server"> 
    <table class="BlueTable" width ="85%" align="center" >   
    <tr><td> 
    <asp:TreeView ID="tvTopic" ImageSet="BulletedList3"  runat="server" OnSelectedNodeChanged="tvTopic_SelectedNodeChanged" ExpandDepth="FullyExpand" ShowLines="True">
                 
                </asp:TreeView>
         </td></tr></table> 
        <asp:HiddenField ID="h_SBM_ID" runat="server" />
        <asp:HiddenField ID="h_GRD_ID" runat="server" />
    </form>
</body>
</html>
