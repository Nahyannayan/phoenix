﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="qbQuestionpaper_M.aspx.vb" Inherits="QuestionBank_qbQuestionpaper_M"
    EnableViewState="true" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/QuestionBank/UserControls/usrQuestion.ascx" TagName="usrQuestion"
    TagPrefix="uQ" %>
<%@ Register Src="~/QuestionBank/UserControls/usrQuestionBank.ascx" TagName="usrQuestionBank"
    TagPrefix="uQb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetTopics() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var sbmId;
            var grdId;
            sbmId = document.getElementById('<%=h_SBM_ID.ClientID %>').value;
            grdId = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
            var oWnd = radopen("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", "pop_topic3");
            //alert(syllabusId);
           <%-- result = window.showModalDialog("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", sFeatures)
            if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
            }
            return false;--%>
        }

         function OnClientClose3(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.Topic.split('||');
             document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
              __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }

        function GetqbTopics() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var sbmId;
            var grdId;
            sbmId = document.getElementById('<%=h_SBM_ID.ClientID %>').value;
            grdId = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
            var oWnd = radopen("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", "pop_topic");
            //alert(syllabusId);
            <%--result = window.showModalDialog("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", sFeatures)
            if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtqbTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_qbTopic_ID.ClientID %>').value = NameandCode[0];
            }
            return false;--%>
        }

        function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.Topic.split('||');
             document.getElementById('<%=txtqbTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_qbTopic_ID.ClientID %>').value = NameandCode[0];
              __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function getImage() {
            var sFeatures;
            sFeatures = "dialogWidth: 350px; ";
            sFeatures += "dialogHeight: 120px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = "../QuestionBank/uploadImage.aspx"
            //window.showModalDialog(url, "_self", sFeatures);
            var oWnd = radopen(url, "pop_topic2");
            return false;

        }

        function showTable(chk) {
            if (chk.checked == true) {
                tblTopic.style.display = "block";
            }
            else {
                tblTopic.style.display = "none";
            }

            return true;
        }
        
        
        function showOptions(chk) {
            if (chk.checked == true) {
                document.getElementById('<%=trOpt1.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt2.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt3.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt4.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt5.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt6.ClientID %>').style.display = "block";
            }
            else {
                 document.getElementById('<%=trOpt1.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt2.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt3.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt4.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt5.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt6.ClientID %>').style.display = "none";
            }

            return true;
        }
        
         function hideOptions(chk) {
            if (chk.checked == true) {
                document.getElementById('<%=trOpt1.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt2.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt3.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt4.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt5.ClientID %>').style.display = "none";
                document.getElementById('<%=trOpt6.ClientID %>').style.display = "none";
                }
               
            else {
       
              document.getElementById('<%=trOpt1.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt2.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt3.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt4.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt5.ClientID %>').style.display = "block";
                document.getElementById('<%=trOpt6.ClientID %>').style.display = "block";
                      }

            return true;
        }
        
         function OnClientClose2(oWnd, args) {
        //get the transferred arguments
       <%-- var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.Topic.split('||');
            document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
              __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }--%>
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
        <Windows>
            <telerik:RadWindow ID="pop_topic3" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
    </telerik:RadWindowManager>
     <telerik:RadWindowManager ID="RadWindowManager2" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_topic2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Question Paper
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr id="trError" runat="server">
                        <td id="Td1" align="left"  runat="server">
                            <asp:Label ID="lblError" SkinID="Error" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:LinkButton ID="lnkAddQuestions" runat="server" Text="Add From Question Bank"></asp:LinkButton>
                        </td>
                        
                    </tr>
                    <tr>
                        <td valign="top" >
                            <table id="tbQuestion" width="100%">
                                <tr id="Tr1" runat="server">
                                    <td id="Td2" valign="top" runat="server">
                                        <table id="tQ" runat="server">
                                            <tr id="Tr2" runat="server">
                                                <td id="Td3" align="left" runat="server"><span class="field-label">Slno.</span> 
                                        <asp:TextBox ID="txtSlno" runat="server"  />
                                                </td>
                                                <td id="Td4" valign="top" runat="server">
                                                    <uQ:usrQuestion ID="uQuestion" runat="server" TxtHeight="100" />
                                                </td>
                                                <td id="Td5" runat="server">
                                                    <span class="field-label">Max Marks :</span>
                                                    <asp:TextBox ID="txtMaxMark" runat="server" Width="30px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Tr3" runat="server">
                                                <td id="Td6" align="left" colspan="2" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:LinkButton ID="lnkQuestion" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                                <telerik:RadBinaryImage ID="imgQuestion" Width="300px" Height="50px" runat="server"
                                                                    Visible="False" /></td>
                                                            <td align="right">
                                                                <asp:RadioButton ID="rdMCQ" Text="MCQ" Checked="true" GroupName="m1" onclick="javascript:showOptions(this);" runat="server" />
                                                                <asp:RadioButton ID="rdDescriptive" Text="Descriptive" GroupName="m1" onclick="javascript:hideOptions(this);" runat="server" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="left" id="tdQuestionGrid" valign="top" width="70%" rowspan="10" runat="server">
                                        <div style="height: 850px; overflow: scroll">
                                            <asp:DataList ID="dlQuestions" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                                ShowHeader="False" Width="100%">
                                                <ItemTemplate>
                                                    <table border="0" cellpadding="1" cellspacing="0">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" Font-Size="14px" Visible="false" runat="server" Text='<%# BIND("QPD_SLNO") %>'></asp:Label>
                                                                <asp:Label ID="lblSlnoDisplay" Width="10px" Font-Size="14px" runat="server" Text='<%# Bind("QPD_SLNO_DISPLAY") %>'></asp:Label>
                                                            </td>
                                                            <td align="left" width="500px" style="vertical-align: top">
                                                                <asp:Label ID="lblQuestion" runat="server" Text='<%# BIND("QPD_DESCR") %>' Width="500px"
                                                                    Font-Size="14px"></asp:Label>
                                                                <asp:Label ID="lblQpdId" runat="server" Text='<%# BIND("QPD_ID") %>' Visible="false"
                                                                    Width="118px"></asp:Label>
                                                            </td>
                                                            <td style="vertical-align: top">
                                                                <asp:Label ID="Label1" runat="server" Text='<%# BIND("QPD_MAXMARK") %>' Width="20px"
                                                                    Font-Size="14px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/EditImage.jpg" OnClick="imgEdit_Click"
                                                                    ToolTip="Edit" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/delete.jpg" OnClick="imgDelete_Click"
                                                                    ToolTip="Delete" /><ajaxToolkit:ConfirmButtonExtender ID="c2" TargetControlID="imgDelete"
                                                                        ConfirmText="Selected question will be deleted permanently.Are you sure you want to continue? "
                                                                        runat="server">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <telerik:RadBinaryImage ID="imgQuest" runat="server" Visible="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&#160;&nbsp;
                                                            </td>
                                                            <td valign="top">
                                                                <asp:GridView ID="gvOptions" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                    BorderStyle="None" OnRowDataBound="gvOptions_RowDataBound" CssClass="table table-bordered table-row">
                                                                    <RowStyle HorizontalAlign="Center" BorderStyle="None" BorderWidth="0px" />
                                                                    <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="lblQpoId" runat="server" Text='<%# Bind("QPO_ID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lblSlno" Font-Size="14px" runat="server" Text='<%# Bind("QPO_SLNO") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lblDescription" Font-Size="14px" runat="server" Text='<%# Bind("QPO_DESCR") %>'></asp:Label><asp:Image
                                                                                                ID="imgAnswer" runat="server" ImageUrl="~/Images/bluetick.jpg" Visible='<%# Bind("QPO_bANSWER") %>' />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <telerik:RadBinaryImage ID="imgOpt" runat="server" Visible="false" />
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" BorderStyle="None"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <HeaderStyle Wrap="False" Height="0px" BorderStyle="None" />
                                                                    <EditRowStyle Wrap="False" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trOpt1" runat="server">
                                    <td id="Td7" runat="server" colspan ="2">
                                        <table id="tbOpt1">
                                            <tr id="Tr5" runat="server">
                                                <td id="Td8" width="20px" runat="server">
                                                    <asp:TextBox ID="txtOpt1" runat="server" Text="a)" Width="20px"></asp:TextBox>
                                                </td>
                                                <td id="Td9" runat="server">
                                                    <uQ:usrQuestion ID="uOpt1" runat="server" TxtHeight="60" />
                                                </td>
                                                <td id="Td10" runat="server">
                                                    <asp:RadioButton ID="rdOpt1" runat="server" GroupName="gOpt" Text="Answer" />
                                                </td>
                                            </tr>
                                            <tr id="Tr6" runat="server">
                                                <td id="Td11" runat="server">&#160;&nbsp;
                                                </td>
                                                <td id="Td12" align="left" runat="server">
                                                    <asp:LinkButton ID="lnkOpt1" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt1" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trOpt2" runat="server">
                                    <td id="Td13" runat="server" colspan="2">
                                        <table id="tbOpt2" runat="server">
                                            <tr id="Tr8" runat="server">
                                                <td id="Td14" width="20px" runat="server">
                                                    <asp:TextBox ID="txtOpt2" runat="server" Text="b)" Width="20px"></asp:TextBox>
                                                </td>
                                                <td id="Td15" runat="server">
                                                    <uQ:usrQuestion ID="uOpt2" runat="server" TxtHeight="60" />
                                                </td>
                                                <td id="Td16" runat="server">
                                                    <asp:RadioButton ID="rdOpt2" runat="server" GroupName="gOpt" Text="Answer" />
                                                </td>
                                            </tr>
                                            <tr id="Tr9" runat="server">
                                                <td id="Td17" runat="server">&#160;&nbsp;
                                                </td>
                                                <td id="Td18" align="left" runat="server">
                                                    <asp:LinkButton ID="lnkOpt2" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt2" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trOpt3" runat="server">
                                    <td id="Td19" runat="server" colspan="2">
                                        <table id="tbOpt3" runat="server">
                                            <tr id="Tr11" runat="server">
                                                <td id="Td20" width="20px" runat="server">
                                                    <asp:TextBox ID="txtOpt3" runat="server" Text="c)" Width="20px"></asp:TextBox>
                                                </td>
                                                <td id="Td21" runat="server">
                                                    <uQ:usrQuestion ID="uOpt3" runat="server" TxtHeight="60" />
                                                </td>
                                                <td id="Td22" runat="server">
                                                    <asp:RadioButton ID="rdOpt3" runat="server" GroupName="gOpt" Text="Answer" />
                                                </td>
                                            </tr>
                                            <tr id="Tr12" runat="server">
                                                <td id="Td23" runat="server">&#160;&nbsp;
                                                </td>
                                                <td id="Td24" align="left" runat="server">
                                                    <asp:LinkButton ID="lnkOpt3" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Images</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt3" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trOpt4" runat="server">
                                    <td id="Td25" runat="server" colspan="2">
                                        <table id="tbOpt4" runat="server">
                                            <tr id="Tr14" runat="server">
                                                <td id="Td26" width="20px" runat="server">
                                                    <asp:TextBox ID="txtOpt4" runat="server" Text="d)" Width="20px"></asp:TextBox>
                                                </td>
                                                <td id="Td27" runat="server">
                                                    <uQ:usrQuestion ID="uOpt4" runat="server" TxtHeight="60" />
                                                </td>
                                                <td id="Td28" runat="server">
                                                    <asp:RadioButton ID="rdOpt4" runat="server" GroupName="gOpt" Text="Answer" />
                                                </td>
                                            </tr>
                                            <tr id="Tr15" runat="server">
                                                <td id="Td29" runat="server">&#160;&nbsp;
                                                </td>
                                                <td id="Td30" align="left" runat="server">
                                                    <asp:LinkButton ID="lnkOpt4" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Images</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt4" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trOpt5" runat="server">
                                    <td id="Td31" runat="server" colspan="2">
                                        <table id="tbOpt5" runat="server">
                                            <tr id="Tr17" runat="server">
                                                <td id="Td32" width="20px" runat="server">
                                                    <asp:TextBox ID="txtOpt5" runat="server" Text="e)" Width="20px"></asp:TextBox>
                                                </td>
                                                <td id="Td33" runat="server">
                                                    <uQ:usrQuestion ID="uOpt5" runat="server" TxtHeight="60" />
                                                </td>
                                                <td id="Td34" runat="server">
                                                    <asp:RadioButton ID="rdOpt5" runat="server" GroupName="gOpt" Text="Answer" />
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td runat="server">&#160;&nbsp;
                                                </td>
                                                <td align="left" runat="server">
                                                    <asp:LinkButton ID="lnkOpt5" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Images</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt5" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trOpt6" runat="server">
                                    <td runat="server" colspan="2">
                                        <table id="tbOpt6" runat="server">
                                            <tr runat="server">
                                                <td width="20px" runat="server">
                                                    <asp:TextBox ID="txtOpt6" runat="server" Text="f)" Width="20px"></asp:TextBox>
                                                </td>
                                                <td runat="server">
                                                    <uQ:usrQuestion ID="uOpt6" runat="server" TxtHeight="60" />
                                                </td>
                                                <td runat="server">
                                                    <asp:RadioButton ID="rdOpt6" runat="server" GroupName="gOpt" Text="Answer" />
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td runat="server">&#160;&nbsp;
                                                </td>
                                                <td align="left" runat="server">
                                                    <asp:LinkButton ID="lnkOpt6" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Images</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt6" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br /><br />  <br /><br /> <br /><br /> 
                                    </td>
                                </tr>
                                <tr id="Tr18" runat="server">
                                    <td id="Td35" runat="server" align="left" colspan="2"> <span class="field-label">Main Question :</span> 
                            <asp:DropDownList ID="ddlMainQuestion" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td runat="server" align="left" colspan="2"><span class="field-label">Section :</span> 
                            <asp:DropDownList ID="ddlQuestionSection" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td runat="server" align="left" colspan="2">
                                        <asp:CheckBox ID="chkAdd" runat="server" Text="Add to QuestionBank" class="field-label" onclick="javascript:showTable(this);" />
                                        <table id="tblTopic" align="center" cellpadding="5"
                                            cellspacing="0" style="border-collapse: collapse; display: none;">
                                            <tr runat="server">
                                                <td runat="server"><span class="field-label">Topic</span>
                                                </td>
                                                <td runat="server">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtTopic" runat="server" Width="300px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgTopic" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="javascript:return GetTopics(); return false;"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td runat="server"><span class="field-label">Skill</span>
                                                </td>

                                                <td runat="server">
                                                    <asp:DropDownList ID="ddlSkill" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td runat="server"><span class="field-label">Difficulty Level</span>
                                                </td>

                                                <td runat="server">
                                                    <asp:DropDownList ID="ddlDifficulty" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td runat="server" align="right" valign="top">
                                                    <asp:Button ID="btnSaveQuestion" runat="server" CssClass="button" Text="Save"></asp:Button>
                                                    <asp:Button ID="btnCancelQuestion" runat="server" CssClass="button" Text="Cancel"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr> 
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="panelQB" runat="server" Style="background-color: White">
                    <table id="Table1" runat="server" align="center" class="table table-bordered table-row"
                        cellpadding="5" cellspacing="0" style="border-collapse: collapse" >
                        <tr>
                            <td ><span class="field-label">Topic</span> 
                            </td>
                          
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtqbTopic" runat="server" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgqbTopic" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetqbTopics(); return false;"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td ><span class="field-label">Skill</span> 
                            </td>
                            
                            <td>
                                <asp:DropDownList ID="ddlqbSkill" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td ><span class="field-label">Difficulty Level</span> 
                            </td>
                           
                            <td>
                                <asp:DropDownList ID="ddlqbDifficulty" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" id="td36" valign="top" width="600px" colspan="9">
                                <uQb:usrQuestionBank ID="usrQuestionBank1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td ><span class="field-label">Section</span> 
                            </td>
                          
                            <td colspan="2">
                                <asp:DropDownList ID="ddlqbQuestionSection" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td ><span class="field-label">Main Question</span> 
                            </td>
                         
                            <td colspan="2">
                                <asp:DropDownList ID="ddlQbMainQuestion" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9">
                                <asp:Button ID="btnSaveQB" runat="server" CssClass="button" Text="Save"></asp:Button>
                                <asp:Button ID="btnCancelQB" runat="server" CssClass="button" Text="Close"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:ModalPopupExtender ID="MPOI" runat="server" PopupControlID="panelQB"
                    CancelControlID="btnCancelQuestion" RepositionMode="RepositionOnWindowResizeAndScroll"
                    TargetControlID="lnkAddQuestions">
                </ajaxToolkit:ModalPopupExtender>
                <asp:HiddenField ID="h_QPM_ID" runat="server" />
                <asp:HiddenField ID="h_SBM_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_ACD_ID" runat="server" />
                <asp:HiddenField ID="h_QPD_ID" runat="server" />
                <asp:HiddenField ID="h_Topic_ID" runat="server" />
                <asp:HiddenField ID="h_qbTopic_ID" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>
