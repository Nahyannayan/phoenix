﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient
Partial Class QuestionBank_qbQuestionpaperSettings_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '   Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            Dim datamode As String = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = datamode
            ViewState("questiondatamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C700025") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindGrade()
                ViewState("slno") = "0"

                '  tblTopic.Visible = False
                If datamode = "add" Then
                    ViewState("QPM_ID") = 0
                    BindSubject()
                Else
                    Dim showpaper As String = Encr_decrData.Decrypt(Request.QueryString("showpaper").Replace(" ", "+"))

                    If showpaper = "no" Then
                        btnSaveSettings.Visible = False
                        btnSetQuestion.Visible = False
                        gvSections.Enabled = False
                    End If
                    ViewState("QPM_ID") = CInt(Encr_decrData.Decrypt(Request.QueryString("qpmid").Replace(" ", "+"))).ToString
                    GetQuestionSettins()
                    Dim li As New ListItem
                    li.Text = Encr_decrData.Decrypt(Request.QueryString("subject").Replace(" ", "+"))
                    li.Value = Encr_decrData.Decrypt(Request.QueryString("sbmid").Replace(" ", "+"))
                    ddlSubject.Items.Add(li)
                    ddlSubject.Enabled = False


                    BindReleaseGroup()
                    BindSection()


                End If
                BindQuestionSectionsGrid()
                h_SBM_ID.Value = ddlSubject.SelectedValue.ToString
                h_GRD_ID.Value = ddlGrade.SelectedValue.ToString
                gvSections.Attributes.Add("bordercolor", "#1b80b6")
                gvReleastoClass.Attributes.Add("bordercolor", "#1b80b6")
                gvReleasetoStudent.Attributes.Add("bordercolor", "#1b80b6")
            End If
            ' Catch ex As Exception
            '  UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '   lblError.Text = "Request could not be processed"
            'End Try
        End If


    End Sub

#Region "Common Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Question Settions"

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M  " _
                                  & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
          & " WHERE GRM_ACD_ID=" + Session("CURRENT_ACD_ID") _
         & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
        h_GRD_ID.Value = 0
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE " _
          & " SBG_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
          & " AND SBG_DESCR NOT IN('Theory','Pract.')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

        h_SBM_ID.Value = 0
    End Sub

    Sub SaveQuestionPaperSettings()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim mode As String

        If ViewState("QPM_ID") = "0" Then
            mode = "add"
        Else
            mode = "edit"
        End If

        Dim str_query As String = " exec QSP.saveQUESTIONPAPER_M " _
                                & "@QPM_ID=" + ViewState("QPM_ID").ToString + "," _
                                & "@QPM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                & "@QPM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                & "@QPM_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
                                & "@QPM_DESCR='" + txtDescr.Text.Replace("'", "''") + "'," _
                                & "@QPM_DISPLAYNAME='" + txtDisplay.Text.Replace("'", "''") + "'," _
                                & "@QPM_bOPENFORALL='" + chkOpen.Checked.ToString + "'," _
                                & "@QPM_bSHOWRESULT='" + chkResult.Checked.ToString + "'," _
                                & "@QPM_SHOWANSWER='" + chkAnser.Checked.ToString + "'," _
                                & "@QPM_DATE='" + txtDate.Text + "'," _
                                & "@QPM_DURATION='" + txtDuration.Text + "'," _
                                & "@QPM_MAXMARK='" + Val(txtMax.Text).ToString + "'," _
                                & "@QPM_PASSMARK='" + Val(txtPassMark.Text).ToString + "'," _
                                & "@QPM_TYPE='MCQ'," _
                                & "@QPM_bAUTOEVALUATE='true'," _
                                & "@QPM_INSTRUCTION='" + txtInstructions.Text.Replace("'", "''") + "'," _
                                & "@QPM_EMP_ID='" + Session("EmployeeId") + "'," _
                                & "@MODE='" + mode + "'," _
                                & "@QPM_bTIMER='" + chkTimer.Checked.ToString + "'," _
                                & "@QPM_TIME=" + Val(txtTime.Text).ToString

        ViewState("QPM_ID") = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub GetQuestionSettins()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = " SELECT QPM_ACD_ID,QPM_GRD_ID,QPM_SBM_ID,QPM_DESCR,QPM_DISPLAYNAME," _
                             & " QPM_bOPENFORALL,QPM_bSHOWRESULT,QPM_SHOWANSWER,QPM_DATE,QPM_DURATION,QPM_MAXMARK,QPM_PASSMARK,QPM_TYPE,QPM_bAUTOEVALUATE, " _
                             & " QPM_INSTRUCTION, QPM_EMP_ID,ISNULL(QPM_bTIMER,0) QPM_bTIMER,ISNULL(QPM_TIME,0) QPM_TIME FROM QSP.QUESTIONPAPER_M WHERE QPM_ID=" + ViewState("QPM_ID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Rows(0)
                ' ddlAcademicYear.Items.FindByValue(.Item("QPM_ACD_ID")).Selected = True
                ddlAcademicYear.Enabled = False

                ddlGrade.Items.FindByValue(.Item("QPM_GRD_ID")).Selected = True
                ddlGrade.Enabled = False



                txtDescr.Text = .Item("QPM_DESCR")
                txtDisplay.Text = .Item("QPM_DISPLAYNAME")
                chkOpen.Checked = .Item("QPM_bOPENFORALL")
                chkResult.Checked = .Item("QPM_bSHOWRESULT")
                chkAnser.Checked = .Item("QPM_SHOWANSWER")
                txtDate.Text = Format(Date.Parse(.Item("QPM_DATE")), "dd/MMM/yyyy")
                txtDuration.Text = .Item("QPM_DURATION")
                txtMax.Text = .Item("QPM_MAXMARK")
                txtPassMark.Text = .Item("QPM_PASSMARK")
                txtInstructions.Text = .Item("QPM_INSTRUCTION")
                chkTimer.Checked = .Item("QPM_bTIMER")
                txtTime.Text = .Item("QPM_TIME")
                If chkTimer.Checked = True Then
                    txtTime.Enabled = True
                End If
            End With
        End If

    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QPS_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QPS_QPM_ID"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QPS_ORDER"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QPS_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MODE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "index"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "QPS_INSTRUCTIONS"
        dt.Columns.Add(column)


        Return dt
    End Function

    Sub BindQuestionSectionsGrid()
        Dim dt As DataTable = SetDataTable()
        Dim bShowSlno As Boolean = False

        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPS_ID,QPS_QPM_ID,QPS_ORDER,QPS_DESCR,QPS_INSTRUCTIONS " _
                                & " FROM QSP.QUESTIONPAPER_SECTIONS WHERE QPS_QPM_ID='" + ViewState("QPM_ID").ToString + "'"

        Dim i As Integer
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                dr = dt.NewRow
                dr.Item(0) = .Item(0)
                dr.Item(1) = .Item(1)
                dr.Item(2) = .Item(2)
                dr.Item(3) = .Item(3)
                dr.Item(4) = "edit"
                dr.Item(5) = i.ToString
                dr.Item(6) = .Item(4)
                dt.Rows.Add(dr)
            End With
        Next

        'add empty rows to show 50 rows
        For i = 0 To 10 - ds.Tables(0).Rows.Count
            dr = dt.NewRow
            dr.Item(0) = "0"
            dr.Item(1) = ""
            dr.Item(2) = (ds.Tables(0).Rows.Count + i + 1).ToString
            dr.Item(3) = ""
            dr.Item(4) = "add"
            dr.Item(5) = (ds.Tables(0).Rows.Count + i).ToString
            dr.Item(6) = ""
            dt.Rows.Add(dr)
        Next

        Session("dtSections") = dt

        gvSections.DataSource = dt
        gvSections.DataBind()

    End Sub

    Sub SaveQuestionSections()

        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String

        Dim i As Integer

        Dim lbQpsId As Label
        Dim lblQpmId As Label
        Dim txtOrder As TextBox
        Dim txtSecDescription As TextBox
        Dim txtSecInstructions As TextBox
        Dim lblDelete As Label
        Dim mode As String

        For i = 0 To gvSections.Rows.Count - 1
            With gvSections.Rows(i)
                lbQpsId = .FindControl("lbQpsId")
                lblQpmId = .FindControl("lblQpmId")
                txtOrder = .FindControl("txtOrder")
                txtSecDescription = .FindControl("txtSecDescription")
                txtSecInstructions = .FindControl("txtSecInstructions")
                lblDelete = .FindControl("lblDelete")
            End With

            If lblDelete.Text = "1" Then
                mode = "delete"
            ElseIf lbQpsId.Text = "0" And txtSecDescription.Text <> "" Then
                mode = "add"
            Else
                mode = "edit"
            End If

            str_query = "exec QSP.saveQUESTIONPAPER_SECTIONS" _
                    & " @QPS_ID=" + lbQpsId.Text + "," _
                    & " @QPS_QPM_ID=" + ViewState("QPM_ID").ToString + "," _
                    & " @QPS_DESCR='" + txtSecDescription.Text.Replace("'", "''") + "'," _
                    & " @QPS_INSTRUCTIONS='" + txtSecInstructions.Text.Replace("'", "''") + "'," _
                    & " @QPS_ORDER=" + txtOrder.Text + "," _
                    & " @MODE='" + mode + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next



    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblindex As Label = TryCast(sender.FindControl("lblindex"), Label)
            Dim lblDelete As Label = TryCast(sender.FindControl("lblDelete"), Label)
            lblDelete.Text = "1"
            gvSections.Rows(Val(lblindex.Text)).Visible = False
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#End Region

#Region "ReleasetoGroup"
    Sub BindReleaseToGroupGrid()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String

        If rdGroup.Checked = True Then
            str_query = "SELECT SGR_ID,SGR_DESCR,ISNULL(QPG_ID,0) QPG_ID,ISNULL(QPG_bRELEASE,0) QPG_bRELEASE," _
                      & " ISNULL(QPG_STARTDATE," + IIf(rtClassStart.SelectedDate.ToString = "", "Getdate()", "'" + rtClassStart.SelectedDate.ToString + "'") + ") QPG_STARTDATE," _
                      & " ISNULL(QPG_ENDDATE," + IIf(rtClassEnd.SelectedDate.ToString = "", "Getdate()", "'" + rtClassEnd.SelectedDate.ToString + "'") + ") QPG_ENDDATE FROM GROUPS_M " _
                      & " LEFT OUTER JOIN QSP.QUESTIONPAPER_RELEASETOCLASS " _
                      & " ON SGR_ID=QPG_SGR_ID AND QPG_QPM_ID=" + ViewState("QPM_ID").ToString _
                      & " INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID" _
                      & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SBG_SBM_ID=" + ddlSubject.SelectedValue.ToString
        ElseIf rdSection.Checked = True Then
            str_query = "SELECT SCT_ID SGR_ID,SCT_DESCR SGR_DESCR,ISNULL(QPG_ID,0) QPG_ID,ISNULL(QPG_bRELEASE,0) QPG_bRELEASE," _
                      & " ISNULL(QPG_STARTDATE," + IIf(rtClassStart.SelectedDate.ToString = "", "Getdate()", "'" + rtClassStart.SelectedDate.ToString + "'") + ") QPG_STARTDATE," _
                      & " ISNULL(QPG_ENDDATE," + IIf(rtClassEnd.SelectedDate.ToString = "", "Getdate()", "'" + rtClassEnd.SelectedDate.ToString + "'") + ") QPG_ENDDATE FROM OASIS..SECTION_M " _
                      & " LEFT OUTER JOIN QSP.QUESTIONPAPER_RELEASETOCLASS " _
                      & " ON SCT_ID=QPG_SCT_ID AND QPG_QPM_ID=" + ViewState("QPM_ID").ToString _
                      & " WHERE SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                      & " AND SCT_DESCR<>'TEMP'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvReleastoClass.DataSource = ds
        gvReleastoClass.DataBind()

        If gvReleastoClass.Rows.Count > 0 Then
            Dim lblHdrGroup As Label = gvReleastoClass.HeaderRow.FindControl("lblHdrGroup")

            If rdGroup.Checked = True Then
                lblHdrGroup.Text = "Group"
            Else
                lblHdrGroup.Text = "Section"
            End If

        End If

    End Sub

    Sub SaveReleasetoClass()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim lbQpgId As Label
        Dim lbSgrIdId As Label
        Dim chkRelease As CheckBox
        Dim rdStart As RadDateTimePicker
        Dim rdEnd As RadDateTimePicker

        For i = 0 To gvReleastoClass.Rows.Count - 1
            With gvReleastoClass.Rows(i)
                lbQpgId = .FindControl("lbQpgId")
                lbSgrIdId = .FindControl("lbSgrIdId")
                chkRelease = .FindControl("chkRelease")
                rdStart = .FindControl("rdStart")
                rdEnd = .FindControl("rdEnd")

                str_query = "exec QSP.saveQUESTIONPAPERRELEASETOCLASS " _
                                      & " @QPG_ID=" + lbQpgId.Text + "," _
                                      & " @QPG_QPM_ID=" + ViewState("QPM_ID").ToString + "," _
                                      & " @QPG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                                      & " @QPG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                      & " @QPG_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
                                      & " @QPG_SGR_ID=" + IIf(rdGroup.Checked = True, lbSgrIdId.Text, "0") + "," _
                                      & " @QPG_SCT_ID=" + IIf(rdSection.Checked = True, lbSgrIdId.Text, "0") + "," _
                                      & " @QPG_bRELEASE=" + chkRelease.Checked.ToString + "," _
                                      & " @QPG_STARTDATE='" + rdStart.SelectedDate.ToString + "'," _
                                      & " @QPG_ENDDATE='" + rdEnd.SelectedDate.ToString + "'"

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End With
        Next
    End Sub
#End Region

#Region "Release to Student"

    Sub BindReleaseGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M INNER JOIN SUBJECTS_GRADE_S ON SGR_SBG_ID=SBG_ID" _
                                & " WHERE SBG_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SBG_SBM_ID=" + ddlSubject.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlReleaseGroup.DataSource = ds
        ddlReleaseGroup.DataTextField = "SGR_DESCR"
        ddlReleaseGroup.DataValueField = "SGR_ID"
        ddlReleaseGroup.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlReleaseGroup.Items.Insert(0, li)
    End Sub


    Sub BindSection()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM OASIS..SECTION_M WHERE SCT_ACd_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' AND SCT_DESCR<>'TEMP'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    Sub BindReleaseToStudentGrid()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String
        If ddlReleaseGroup.SelectedValue.ToString <> "0" Then
            str_query = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME" _
                                   & " ,ISNULL(QSR_ID,0) QSR_ID,ISNULL(QSR_bRELEASE,0) QSR_bRELEASE,ISNULL(QSR_STARTDATE," + IIf(rtStartStudent.SelectedDate.ToString = "", "Getdate()", "'" + rtStartStudent.SelectedDate.ToString + "'") + ") QSR_STARTDATE," _
                                   & " ISNULL(QSR_ENDDATE," + IIf(rtEndStudent.SelectedDate.ToString = "", "Getdate()", "'" + rtEndStudent.SelectedDate.ToString + "'") + ") QSR_ENDDATE " _
                                   & " FROM STUDENT_M AS A INNER JOIN STUDENT_GROUPS_S ON STU_ID=SSD_STU_ID" _
                                   & " LEFT OUTER JOIN QSP.QUESTIONPAPER_RELEASETOSTUDENTS ON STU_ID=QSR_STU_ID AND QSR_QPM_ID=" + ViewState("QPM_ID").ToString _
                                   & " WHERE SSD_SGR_ID='" + ddlReleaseGroup.SelectedValue.ToString + "'" _
                                   & " ORDER BY ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') "
        Else
            str_query = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME" _
                                  & " ,ISNULL(QSR_ID,0) QSR_ID,ISNULL(QSR_bRELEASE,0) QSR_bRELEASE,ISNULL(QSR_STARTDATE," + IIf(rtStartStudent.SelectedDate.ToString = "", "Getdate()", "'" + rtStartStudent.SelectedDate.ToString + "'") + ") QSR_STARTDATE," _
                                  & " ISNULL(QSR_ENDDATE," + IIf(rtEndStudent.SelectedDate.ToString = "", "Getdate()", "'" + rtEndStudent.SelectedDate.ToString + "'") + ") QSR_ENDDATE " _
                                  & " FROM STUDENT_M AS A " _
                                  & " LEFT OUTER JOIN QSP.QUESTIONPAPER_RELEASETOSTUDENTS ON STU_ID=QSR_STU_ID AND QSR_QPM_ID=" + ViewState("QPM_ID").ToString _
                                  & " WHERE STU_SCT_ID='" + ddlSection.SelectedValue.ToString + "'" _
                                  & " ORDER BY ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvReleasetoStudent.DataSource = ds
        gvReleasetoStudent.DataBind()
    End Sub


    Sub SaveReleasetoStudent()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String
        Dim i As Integer

        Dim lbQsrId As Label
        Dim lbStuId As Label
        Dim lblStuName As Label
        Dim chkRelease As CheckBox
        Dim rdStart As RadDateTimePicker
        Dim rdEnd As RadDateTimePicker

        For i = 0 To gvReleasetoStudent.Rows.Count - 1
            With gvReleasetoStudent.Rows(i)
                lbQsrId = .FindControl("lbQsrId")
                lbStuId = .FindControl("lbStuId")
                lblStuName = .FindControl("lblStuName")
                chkRelease = .FindControl("chkRelease")
                rdStart = .FindControl("rdStart")
                rdEnd = .FindControl("rdEnd")

                str_query = "exec QSP.saveQESTIONPAPERRELEASETOSTUDENT" _
                          & " @QSR_ID=" + lbQsrId.Text + "," _
                          & " @QSR_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + "," _
                          & " @QSR_GRD_ID=" + ddlGrade.SelectedValue.ToString + "," _
                          & " @QSR_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
                          & " @QSR_QPM_ID=" + ViewState("QPM_ID").ToString + "," _
                          & " @QSR_STU_ID=" + lbStuId.Text + "," _
                          & " @QSR_bRELEASE='" + chkRelease.Checked.ToString + "'," _
                          & " @QSR_STARTDATE='" + rdStart.SelectedDate.ToString + "'," _
                          & " @QSR_ENDDATE='" + rdEnd.SelectedDate.ToString + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End With
        Next
    End Sub
#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubject()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        h_SBM_ID.Value = ddlSubject.SelectedValue.ToString
        h_GRD_ID.Value = ddlGrade.SelectedValue.ToString
    End Sub
    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        h_SBM_ID.Value = ddlSubject.SelectedValue.ToString
    End Sub

    Protected Sub btnSaveSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        SaveQuestionPaperSettings()
        SaveQuestionSections()
        BindQuestionSectionsGrid()
        ddlAcademicYear.Enabled = False
        ddlGrade.Enabled = False
        ddlSubject.Enabled = False

        If chkTimer.Checked = True Then
            txtTime.Enabled = True
        Else
            txtTime.Enabled = False
        End If
    End Sub


    Protected Sub btnSetQuestion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetQuestion.Click
        Dim url As String
        'define the datamode to Add if Add New is clicked
        ViewState("datamode") = "edit"
        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        url = String.Format("qbQuestionpaper_M.aspx?MainMnu_code={0}&datamode={1}" _
        & "&qpmid=" + ViewState("QPM_ID").ToString _
        & "&sbmid=" + ddlSubject.SelectedValue.ToString _
        & "&grdid=" + ddlGrade.SelectedValue.ToString _
        & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
        & "&subject=" + ddlSubject.SelectedItem.Text, ViewState("MainMnu_code"), ViewState("datamode"))

        Response.Redirect(url)
    End Sub

    Protected Sub btnReleasetoClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReleasetoClass.Click
        SaveReleasetoClass()
    End Sub

    Protected Sub ddlReleaseGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReleaseGroup.SelectedIndexChanged
        BindReleaseToStudentGrid()
    End Sub

    Protected Sub btnReleasetoStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReleasetoStudent.Click
        SaveReleasetoStudent()
    End Sub

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        BindReleaseToStudentGrid()
    End Sub

    Protected Sub btnClassApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClassApply.Click
        BindReleaseToGroupGrid()
    End Sub
End Class
