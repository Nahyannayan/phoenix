﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class QuestionBank_qbQuestionPaper_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C700025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    BindGrade()
                    BindSubject()
                    GridBind()
                    gvQuestion.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub
    Protected Sub lnkSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblQpmId As Label = TryCast(sender.findcontrol("lblQpmId"), Label)
        Dim lblSbmId As Label = TryCast(sender.findcontrol("lblSbmId"), Label)
        Dim lblSubject As Label = TryCast(sender.findcontrol("lblSubject"), Label)
        Dim lnkQuestionPaper As LinkButton = TryCast(sender.findcontrol("lnkQuestionPaper"), LinkButton)

        Dim showPaper As String

        If lnkQuestionPaper.Enabled = True Then
            showPaper = "yes"
        Else
            showPaper = "no"
        End If

        Dim url As String
        'define the datamode to Add if Add New is clicked
        ViewState("datamode") = "edit"
        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        url = String.Format("qbQuestionpaperSettings_M.aspx?MainMnu_code={0}&datamode={1}:" _
                            & "&qpmid=" + Encr_decrData.Encrypt(lblQpmId.Text) _
                            & "&sbmid=" + Encr_decrData.Encrypt(lblSbmId.Text) _
                            & "&subject=" + Encr_decrData.Encrypt(lblSubject.Text) _
                            & "&showpaper=" + Encr_decrData.Encrypt(showPaper), ViewState("MainMnu_code"), ViewState("datamode"))

        Response.Redirect(url)

    End Sub

    Protected Sub lnkQuestionPaper_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblQpmId As Label = TryCast(sender.findcontrol("lblQpmId"), Label)
        Dim lblSbmId As Label = TryCast(sender.findcontrol("lblSbmId"), Label)
        Dim lblSubject As Label = TryCast(sender.findcontrol("lblSubject"), Label)

        Dim url As String
        'define the datamode to Add if Add New is clicked
        ViewState("datamode") = "edit"
        'Encrypt the data that needs to be send through Query String
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        url = String.Format("qbQuestionpaper_M.aspx?MainMnu_code={0}&datamode={1}" _
        & "&qpmid=" + lblQpmId.Text _
        & "&sbmid=" + lblSbmId.Text _
        & "&grdid=" + ddlGrade.SelectedValue.ToString _
        & "&acdid=" + ddlAcademicYear.SelectedValue.ToString _
        & "&subject=" + lblSubject.Text, ViewState("MainMnu_code"), ViewState("datamode"))

        Response.Redirect(url)

    End Sub


#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M  " _
                                  & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
          & " WHERE GRM_ACD_ID=" + Session("CURRENT_ACD_ID") _
         & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S  WHERE " _
          & " SBG_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
          & " AND SBG_DESCR NOT IN('Theory','Pract.')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        'Dim str_query As String = "SELECT QPM_ID,QPM_DESCR,QPM_SBM_ID,ISNULL(EMP_DISPLAYNAME,ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')) EMP_NAME,SBG_DESCR " _
        '                        & " FROM QSP.QUESTIONPAPER_M INNER JOIN OASIS..EMPLOYEE_M ON QPM_EMP_ID=EMP_ID " _
        '                        & " INNER JOIN SUBJECTS_GRADE_S ON QPM_SBM_ID=SBG_SBM_ID AND QPM_ACD_ID=SBG_ACD_ID AND QPM_GRD_ID=SBG_GRD_ID WHERE " _
        '                        & " QPM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND QPM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"


        Dim str_query As String = "SELECT QPM_ID,QPM_DESCR,QPM_SBM_ID,CASE WHEN CREATEDSCHOOL='' THEN ISNULL(EMP_DISPLAYNAME,ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')) ELSE CREATEDSCHOOL END EMP_NAME,SBG_DESCR, " _
                                & " CASE ISNULL(bOTHERSCHOOL,0) WHEN 0 THEN 'TRUE' ELSE 'FALSE' END bPAPER FROM [QSP].[FN_QUESTIONPAPER_M](" + ddlAcademicYear.SelectedValue.ToString + ",'" + ddlGrade.SelectedValue.ToString + "') INNER JOIN OASIS..EMPLOYEE_M ON QPM_EMP_ID=EMP_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S ON QPM_SBM_ID=SBG_SBM_ID AND QPM_ACD_ID=SBG_ACD_ID AND QPM_GRD_ID=SBG_GRD_ID  " _
                                '& " QPM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND QPM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"



        If ddlSubject.SelectedValue <> "0" Then
            str_query += " AND QPM_SBM_ID=" + ddlSubject.SelectedValue.ToString
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvQuestion.DataSource = ds
        gvQuestion.DataBind()
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubject()
        GridBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        GridBind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("qbQuestionpaperSettings_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
End Class
