<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="qbQuestionPaper_View.aspx.vb" Inherits="QuestionBank_qbQuestionPaper_View" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Question Paper
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" width="100%"
                    cellspacing="0">
                    <tr>
                        <td >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center"  valign="top">

                            <table id="tblClm" runat="server" align="center"  cellpadding="5" cellspacing="0" width="100%">
                               
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:LinkButton ID="lnkAdd" Text="Add New" runat="server"></asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span> </td>                               
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span> </td>
                                  
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                                        </asp:DropDownList></td>
                                    
                                </tr>                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Subject</span> 
                                    </td>
                                 
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                                        </asp:DropDownList></td>
                                    <td colspan="2"></td>
                                </tr>

                               
                                <tr>
                                    <td align="center" colspan="4" valign="top">
                                        <asp:GridView ID="gvQuestion" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                           CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <RowStyle  />
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQpmId" runat="server" Text='<%# Bind("QPM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSbmId" runat="server" Text='<%# Bind("QPM_SBM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Subject" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDescr" runat="server" Text='<%# Bind("QPM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Created By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmployee" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkSettings" OnClick="lnkSettings_Click" runat="server">Settings</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkQuestionPaper" OnClick="lnkQuestionPaper_Click" Enabled='<%# Bind("bPAPER") %>' runat="server">Question Paper</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle  />
                                        </asp:GridView>

                                    </td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>
            </div>
        </div>

    </div>
</asp:Content>

