﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Partial Class QuestionBank_qbQuestionBank
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C700020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Topic_ID.Value = "0"
                    trError.Visible = False
                    BindGrade()
                    BindSubject()
                    BindSkill()
                    BindDifficulty()
                    BindQuestion()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkQuestion)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkOpt1)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkOpt2)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkOpt3)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkOpt4)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkOpt5)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkOpt6)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(imgQuestion)
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function getSerialNo() As String
        ViewState("slno") = ViewState("slno") + 1
        Return ViewState("slno").ToString + ")"
    End Function


    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M  " _
                                  & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
                                  & " WHERE GRM_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)

        h_GRD_ID.Value = 0


    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE " _
                                & " SBG_ACD_ID=" + Session("CURRENT_ACD_ID") + " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SBG_DESCR NOT IN('Theory','Pract.')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

        h_SBM_ID.Value = 0
    End Sub

    Sub BindSkill()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT SKL_ID,ISNULL(SKL_DESCR,'') SKL_DESCR,ISNULL(SKL_ORDER,0) SKL_ORDER,ISNULL(SKL_SHORTNAME,'') SKL_SHORTNAME FROM " _
                                & " QST.SKILLS_M WHERE " _
                                & " SKL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY SKL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSkill.DataSource = ds
        ddlSkill.DataTextField = "SKL_DESCR"
        ddlSkill.DataValueField = "SKL_ID"
        ddlSkill.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSkill.Items.Insert(0, li)
    End Sub

    Sub BindDifficulty()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT DL_ID,ISNULL(DL_DESCR,'') DL_DESCR,ISNULL(DL_ORDER,0) DL_ORDER,ISNULL(DL_SHORTNAME,'') DL_SHORTNAME FROM " _
                                & " QST.DIFFICULTYLEVEL_M WHERE " _
                                & " DL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY DL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDifficulty.DataSource = ds
        ddlDifficulty.DataTextField = "DL_DESCR"
        ddlDifficulty.DataValueField = "DL_ID"
        ddlDifficulty.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlDifficulty.Items.Insert(0, li)
    End Sub

    Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString

        'Dim str_query As String = " exec  QST.saveQUESTIONBANK_M " _
        '                        & " @QSM_ID=" + h_QSM_ID.Value + "," _
        '                        & " @QSM_SBM_ID=" + ddlSubject.SelectedValue.ToString + "," _
        '                        & " @QSM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
        '                        & " @QSM_TOP_ID=" + h_Topic_ID.Value + "," _
        '                        & " @QSM_SKL_ID=" + ddlSkill.SelectedValue.ToString + "," _
        '                        & " @QSM_DL_ID=" + ddlDifficulty.SelectedValue.ToString + "," _
        '                        & " @QSM_DESCR='" + uQuestion.TxtValue + "'," _
        '                        & " @QSM_IMAGE='" + IIf(ViewState("imgquestion") Is Nothing, "NULL", ViewState("imgquestion").ToString) + "'," _
        '                        & " @MODE='" + ViewState("datamode") + "'"

        Using connection As SqlConnection = ConnectionManger.GetOASIS_QBConnection()
            Try
                Dim pParms(11) As SqlClient.SqlParameter
                Dim qsmId As Integer
                If ViewState("datamode") <> "add" Then
                    qsmId = CInt(h_QSM_ID.Value)
                End If
                pParms(0) = New SqlClient.SqlParameter("@QSM_ID", qsmId)
                pParms(0).Direction = ParameterDirection.InputOutput
                pParms(1) = New SqlClient.SqlParameter("@QSM_SBM_ID", ddlSubject.SelectedValue)
                pParms(2) = New SqlClient.SqlParameter("@QSM_BSU_ID", Session("SBSUID"))
                pParms(3) = New SqlClient.SqlParameter("@QSM_GRD_ID", ddlGrade.SelectedValue)
                pParms(4) = New SqlClient.SqlParameter("@QSM_TOP_ID", h_Topic_ID.Value)
                pParms(5) = New SqlClient.SqlParameter("@QSM_SKL_ID", ddlSkill.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@QSM_DL_ID", ddlDifficulty.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@QSM_DESCR", uQuestion.TxtValue)
                pParms(8) = New SqlClient.SqlParameter("@QSM_MAXMARK", CDbl(IIf(txtMax.Text = "", "0", txtMax.Text)))
                pParms(9) = New SqlClient.SqlParameter("@MODE", ViewState("datamode"))
                If Not ViewState("imgquestion") Is Nothing Then
                    pParms(10) = New SqlClient.SqlParameter("@QSM_IMAGE", ViewState("imgquestion"))
                    pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(11).Direction = ParameterDirection.ReturnValue
                Else
                    pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(10).Direction = ParameterDirection.ReturnValue
                End If
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "QST.saveQUESTIONBANK_M", pParms)
                If ViewState("datamode") = "add" Then
                    h_QSM_ID.Value = pParms(0).Value
                End If
            Catch ex As Exception

            End Try
           
        End Using

        ' h_QSM_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, )
    End Sub

    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Sub SaveOptions(ByVal opt As String)
        Using connection As SqlConnection = ConnectionManger.GetOASIS_QBConnection()
            Try
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@QSD_QSM_ID", h_QSM_ID.Value)
                If opt = "1" Then
                    If uOpt1.TxtValue = "" And ViewState("imgOpt1") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QSD_SLNO", txtOpt1.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QSD_DESCR", uOpt1.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QSD_bANSWER", rdOpt1.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QSD_OPTION", 1)
                    If Not ViewState("imgOpt1") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QSD_IMAGE", ViewState("imgOpt1"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "2" Then
                    If uOpt2.TxtValue = "" And ViewState("imgOpt2") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QSD_SLNO", txtOpt2.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QSD_DESCR", uOpt2.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QSD_bANSWER", rdOpt2.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QSD_OPTION", 2)
                    If Not ViewState("imgOpt2") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QSD_IMAGE", ViewState("imgOpt2"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "3" Then
                    If uOpt3.TxtValue = "" And ViewState("imgOpt3") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QSD_SLNO", txtOpt3.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QSD_DESCR", uOpt3.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QSD_bANSWER", rdOpt3.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QSD_OPTION", 3)
                    If Not ViewState("imgOpt3") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QSD_IMAGE", ViewState("imgOpt3"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "4" Then
                    If uOpt4.TxtValue = "" And ViewState("imgOpt4") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QSD_SLNO", txtOpt4.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QSD_DESCR", uOpt4.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QSD_bANSWER", rdOpt4.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QSD_OPTION", 4)
                    If Not ViewState("imgOpt4") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QSD_IMAGE", ViewState("imgOpt4"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "5" Then
                    If uOpt5.TxtValue = "" And ViewState("imgOpt5") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QSD_SLNO", txtOpt5.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QSD_DESCR", uOpt5.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QSD_bANSWER", rdOpt5.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QSD_OPTION", 5)
                    If Not ViewState("imgOpt5") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QSD_IMAGE", ViewState("imgOpt5"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "6" Then
                    If uOpt6.TxtValue = "" And ViewState("imgOpt6") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QSD_SLNO", txtOpt6.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QSD_DESCR", uOpt6.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QSD_bANSWER", rdOpt6.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QSD_OPTION", 6)
                    If Not ViewState("imgOpt6") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QSD_IMAGE", ViewState("imgOpt6"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                End If
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "QST.saveQUESTIONBANK_D", pParms)
            Catch ex As Exception

            End Try
        End Using
    End Sub

    Sub BindQuestion()
        ViewState("slno") = 0
        h_QSM_ID.Value = 0
        dlQuestions.DataSource = Nothing
        dlQuestions.DataBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSM_ID,QSM_DESCR FROM QST.QUESTIONBANK_M WHERE " _
                                  & " QSM_BSU_ID='" + Session("SBSUID") + "'" _
                                  & " AND QSM_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                  & " AND QSM_SBM_ID='" + ddlSubject.SelectedValue.ToString + "'" _
                                  & " AND QSM_SKL_ID='" + ddlSkill.SelectedValue.ToString + "'" _
                                  & " AND QSM_DL_ID='" + ddlDifficulty.SelectedValue.ToString + "'"

        If h_Topic_ID.Value <> "0" Then
            str_query += " AND QSM_TOP_ID='" + h_Topic_ID.Value + "'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        dlQuestions.DataSource = ds
        dlQuestions.DataBind()
    End Sub

    Sub BindQuestionImage(ByVal qsm_id As String, ByVal imgQuest As RadBinaryImage)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSM_IMAGE FROM QST.QUESTIONBANK_M WHERE QSM_ID=" + qsm_id + " AND QSM_IMAGE IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            imgQuest.DataValue = ds.Tables(0).Rows(0).Item(0)
            imgQuest.Visible = True
        End If
    End Sub
    Sub BindQuestionOptions(ByVal qsm_id As String, ByVal gvOption As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSD_ID,QSD_SLNO,QSD_DESCR,ISNULL(QSD_bANSWER,'FALSE') QSD_bANSWER FROM QST.QUESTIONBANK_D WHERE QSD_QSM_ID=" + qsm_id

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOption.DataSource = ds
        gvOption.DataBind()
        If gvOption.Rows.Count > 0 Then
            gvOption.HeaderRow.Visible = False
        End If
    End Sub

    Sub BindOptionImage(ByVal qsd_id As String, ByVal imgOpt As RadBinaryImage)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSD_IMAGE FROM  QST.QUESTIONBANK_D WHERE  QSD_ID=" + qsd_id + " AND QSD_IMAGE IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            imgOpt.DataValue = ds.Tables(0).Rows(0).Item(0)
            imgOpt.Visible = True
        End If
    End Sub

    Sub getQusetionDetails(ByVal qsm_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QSM_DESCR,QSM_IMAGE,ISNULL(QSM_MAXMARK,0) QSM_MAXMARK FROM QST.QUESTIONBANK_M WHERE QSM_ID=" + qsm_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        With ds.Tables(0).Rows(0)
            uQuestion.TxtValue = .Item(0)
            If IsDBNull(.Item(1)) = False Then
                ViewState("imgQuestion") = .Item(1)
                imgQuestion.DataValue = ViewState("imgQuestion")
                imgQuestion.Visible = True
            End If
            TXTMAX.TEXT = .Item(2)
        End With


        str_query = "SELECT QSD_SLNO,QSD_DESCR,QSD_IMAGE,QSD_OPTION,ISNULL(QSD_bANSWER,'FALSE') QSD_bANSWER FROM QST.QUESTIONBANK_D WHERE QSD_QSM_ID=" + qsm_id
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                If .Item(3).ToString = "1" Then
                    txtOpt1.Text = .Item(0)
                    uOpt1.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt1") = .Item(2)
                        imgOpt1.DataValue = ViewState("imgOpt1")
                        imgOpt1.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt1.Checked = True
                    End If
                ElseIf .Item(3).ToString = "2" Then
                    txtOpt2.Text = .Item(0)
                    uOpt2.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt2") = .Item(2)
                        imgOpt2.DataValue = ViewState("imgOpt2")
                        imgOpt2.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt2.Checked = True
                    End If
                ElseIf .Item(3).ToString = "3" Then
                    txtOpt3.Text = .Item(0)
                    uOpt3.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt3") = .Item(2)
                        imgOpt3.DataValue = ViewState("imgOpt3")
                        imgOpt3.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt3.Checked = True
                    End If
                ElseIf .Item(3).ToString = "4" Then
                    txtOpt4.Text = .Item(0)
                    uOpt4.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt4") = .Item(2)
                        imgOpt4.DataValue = ViewState("imgOpt4")
                        imgOpt4.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt4.Checked = True
                    End If
                ElseIf .Item(3).ToString = "5" Then
                    txtOpt5.Text = .Item(0)
                    uOpt5.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt5") = .Item(2)
                        imgOpt5.DataValue = ViewState("imgOpt5")
                        imgOpt5.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt5.Checked = True
                    End If
                ElseIf .Item(3).ToString = "6" Then
                    txtOpt6.Text = .Item(0)
                    uOpt6.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt6") = .Item(2)
                        imgOpt6.DataValue = ViewState("imgOpt6")
                        imgOpt6.Visible = True
                    End If
                    If .Item(6) = True Then
                        rdOpt1.Checked = True
                    End If
                End If
            End With
        Next
    End Sub

#End Region

    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblQsd As Label = e.Row.FindControl("lblQsdId")
            Dim imgOpt As RadBinaryImage = e.Row.FindControl("imgOpt")
            BindOptionImage(lblQsd.Text, imgOpt)
        End If
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim lblQsmId As Label = TryCast(sender.findcontrol("lblQsmId"), Label)
        h_QSM_ID.Value = lblQsmId.Text
        ViewState("datamode") = "edit"
        getQusetionDetails(lblQsmId.Text)
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim lblQsmId As Label = TryCast(sender.findcontrol("lblQsmId"), Label)
        ViewState("datamode") = "delete"
        h_QSM_ID.Value = lblQsmId.Text
        SaveData()
        ClearControls()
        BindQuestion()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_GRD_ID.Value = ddlGrade.SelectedValue.ToString
        BindSubject()
        h_Topic_ID.Value = "0"
        txtTopic.Text = ""
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        h_SBM_ID.Value = ddlSubject.SelectedValue.ToString
        h_Topic_ID.Value = "0"
        txtTopic.Text = ""
        BindQuestion()
    End Sub

    Protected Sub lnkQuestion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQuestion.Click
        If Not Session("qImage") Is Nothing Then
            'imgQuestion.ImageUrl = Session("qImage")
            ' Label1.Text = Session("qImage").ToString
            imgQuestion.Visible = True
            imgQuestion.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgquestion") = imgQuestion.DataValue
            If imgQuestion.Height.Value > 150 Or imgQuestion.Width.Value > 100 Then
                '  imgQuestion.i()


            End If
        End If
    End Sub

    Protected Sub lnkOpt1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt1.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt1.Visible = True
            imgOpt1.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt1") = imgOpt1.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt2.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt2.Visible = True
            imgOpt2.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt2") = imgOpt2.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt3.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt3.Visible = True
            imgOpt3.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt3") = imgOpt3.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt4.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt4.Visible = True
            imgOpt4.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt4") = imgOpt4.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt5.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt5.Visible = True
            imgOpt5.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt5") = imgOpt5.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt6.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt6.Visible = True
            imgOpt6.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt6") = imgOpt6.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Sub ClearControls()
        h_QSM_ID.Value = 0
        uQuestion.TxtValue = ""
        uOpt1.TxtValue = ""
        uOpt2.TxtValue = ""
        uOpt3.TxtValue = ""
        uOpt4.TxtValue = ""
        uOpt5.TxtValue = ""
        uOpt6.TxtValue = ""
        txtMax.Text = ""

        ViewState("imgquestion") = Nothing
        ViewState("imgOpt1") = Nothing
        ViewState("imgOpt2") = Nothing
        ViewState("imgOpt3") = Nothing
        ViewState("imgOpt4") = Nothing
        ViewState("imgOpt5") = Nothing
        ViewState("imgOpt6") = Nothing

        imgQuestion.Visible = False
        imgOpt1.Visible = False
        imgOpt2.Visible = False
        imgOpt3.Visible = False
        imgOpt4.Visible = False
        imgOpt5.Visible = False
        imgOpt6.Visible = False

        ViewState("datamode") = "add"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim strText As String = ""
        If ddlGrade.SelectedValue = "0" Then
            strText = "Please select the grade  . "
        End If
        If ddlSubject.SelectedValue = "0" Then
            strText += "Please select the Subject .  "
        End If

        If txtTopic.Text = "" Then
            strText += "Please select the Topic .  "
        End If

        If ddlSkill.SelectedValue = "0" Then
            strText += "Please select the Skill  . "
        End If

        If ddlDifficulty.SelectedValue = "0" Then
            strText += "Please select the Difficulty level .  "
        End If

        If strText <> "" Then
            lblError.Text = strText
            trError.Visible = True
            Exit Sub
        Else
            trError.Visible = False
        End If

        SaveData()
        SaveOptions(1)
        SaveOptions(2)
        SaveOptions(3)
        SaveOptions(4)
        SaveOptions(5)
        SaveOptions(6)
        ClearControls()
        BindQuestion()
    End Sub


    Protected Sub ddlDifficulty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDifficulty.SelectedIndexChanged
        BindQuestion()
    End Sub

    Protected Sub ddlSkill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSkill.SelectedIndexChanged
        BindQuestion()
    End Sub

    Protected Sub dlQuestions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlQuestions.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblQsmId As Label = e.Item.FindControl("lblQsmId")
            Dim imgQuest As RadBinaryImage = e.Item.FindControl("imgQuest")
            Dim gvOptions As GridView = e.Item.FindControl("gvOptions")
            BindQuestionImage(lblQsmId.Text, imgQuest)
            BindQuestionOptions(lblQsmId.Text, gvOptions)
        End If
    End Sub

    Protected Sub imgTopic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTopic.Click
        BindQuestion()
        ClearControls()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearControls()
    End Sub

    
End Class
