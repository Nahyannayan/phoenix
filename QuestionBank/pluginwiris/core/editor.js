﻿var opener = window.top;

if (window.attachEvent)
{
	window.attachEvent("onload", initDialog);
}
else if (window.addEventListener)
{
	window.addEventListener("load", initDialog, false);
}

function initDialog()
{
	var applet = document.getElementById('applet');
	var clientParameters = getRadWindow().ClientParameters;
	// Mathml content
	if (clientParameters._wrs_temporalImage) {
		var mathml = opener.wrs_mathmlDecode(clientParameters._wrs_temporalImage.getAttribute(opener._wrs_conf_imageMathmlAttribute));
		
		function setAppletMathml() {
			// Internet explorer fails on "applet.isActive". It only supports "applet.isActive()"
			try {
				if (applet.isActive && applet.isActive()) {
					applet.setContent(mathml);
				}
				else {
					setTimeout(setAppletMathml, 50);
				}
			}
			catch (e) {
				if (applet.isActive()) {
					applet.setContent(mathml);
				}
				else {
					setTimeout(setAppletMathml, 50);
				}
			}
		}

		setAppletMathml();
	}
	
	// Button events
	$addHandler($get("submit"), "click", function()
	{
		var mathml = '';
		
		if (!applet.isFormulaEmpty()) {
			mathml += applet.getContent();					// if isn't empty, get mathml code to mathml variable
			mathml = opener.wrs_mathmlEntities(mathml);		// Apply a parse
		}

		//if (opener.wrs_int_updateFormula)
		//{
		//	opener.wrs_int_updateFormula(mathml);
		//}

		//window.close();
		
		var args = { mathml: mathml };
		getRadWindow().close(args);
	});
	
	$addHandler($get("cancel"), "click", function() { getRadWindow().close() });
};

function getRadWindow() //mandatory for the RadWindow dialogs functionality
{
	if (window.radWindow)
	{
		return window.radWindow;
	}
	if (window.frameElement && window.frameElement.radWindow)
	{
		return window.frameElement.radWindow;
	}
	return null;
}
