var opener = window.top;
function htmlentities(input)
{
	var container = document.createElement('span');
	var text = document.createTextNode(input);
	container.appendChild(text);
	return container.innerHTML.split('"').join('&quot;');
}

function getMathmlFromAppletCode(appletCode)
{
	var optionForm = document.getElementById('optionForm');
	var appletObject = opener.wrs_createObject(appletCode);

	optionForm.width.value = parseInt(appletObject.width);
	optionForm.height.value = parseInt(appletObject.height);

	var params = appletObject.childNodes;
	var mathml = '';

	for (var i = 0; i < params.length; ++i)
	{
		if (params[i].name == 'xmlinitialtext')
		{
			mathml = params[i].value;
		}
		else if (params[i].name == 'requestfirstevaluation')
		{
			optionForm.executeonload.checked = (params[i].value == 'true') ? true : false;
		}
		else if (params[i].name == 'toolbar')
		{
			optionForm.toolbar.checked = (params[i].value == 'floating') ? false : true;
		}
		else if (params[i].name == 'requestfocus')
		{
			optionForm.focusonload.checked = (params[i].value == 'true') ? true : false;
		}
		else if (params[i].name == 'level')
		{
			optionForm.level.checked = (params[i].value == 'primary') ? true : false;
		}
	}

	return mathml;
}

if (window.attachEvent)
{
	window.attachEvent("onload", initDialog);
}
else if (window.addEventListener)
{
	window.addEventListener("load", initDialog, false);
}

function initDialog()
{
	// Waiting for applet load
	var applet = document.getElementById('applet');
	var clientParameters = getRadWindow().ClientParameters;

	/* Getting possible mathml for CAS editing */
	if (clientParameters._wrs_temporalImage)
	{
		var appletCode = clientParameters._wrs_temporalImage.getAttribute(opener._wrs_conf_CASMathmlAttribute);
		var mathml = getMathmlFromAppletCode(opener.wrs_mathmlDecode(appletCode));

		function setAppletMathml()
		{
			// Internet explorer fails on "applet.isActive". It only supports "applet.isActive()"
			try
			{
				if (applet.isActive && applet.isActive())
				{
					applet.setXML(mathml);
				}
				else
				{
					setTimeout(setAppletMathml, 50);
				}
			}
			catch (e)
			{
				if (applet.isActive())
				{
					applet.setXML(mathml);
				}
				else
				{
					setTimeout(setAppletMathml, 50);
				}
			}
		}

		setAppletMathml();
	}
	$addHandler($get("submit"), "click", function()
	{
		/* Creating new applet code */
		var optionForm = document.getElementById('optionForm');
		var newWidth = parseInt(optionForm.width.value);
		var newHeight = parseInt(optionForm.height.value);

		var appletCode = '<applet alt="WIRIS CAS" class="Wiriscas" align="middle" ';
		appletCode += 'codebase="' + applet.getAttribute('codebase') + '" ';
		appletCode += 'archive="' + applet.getAttribute('archive') + '" ';
		appletCode += 'code="' + applet.getAttribute('code') + '" ';
		appletCode += 'width="' + newWidth + '" height="' + newHeight + '">';

		appletCode += '<param name="requestfirstevaluation" value="' + (optionForm.executeonload.checked ? 'true' : 'false') + '"></param>';
		appletCode += '<param name="toolbar" value="' + (optionForm.toolbar.checked ? 'true' : 'floating') + '"></param>';
		appletCode += '<param name="requestfocus" value="' + (optionForm.focusonload.checked ? 'true' : 'false') + '"></param>';
		appletCode += '<param name="level" value="' + (optionForm.level.checked ? 'primary' : 'false') + '"></param>';
		appletCode += '<param name="xmlinitialtext" value="' + htmlentities(applet.getXML()) + '"></param>';
		appletCode += '<param name="interface" value="false"></param><param name="commands" value="false"></param><param name="command" value="false"></param>';

		appletCode += '</applet>';

		/* Getting the image */
		// First, resize applet
		applet.width = newWidth;
		applet.height = newHeight;

		// Waiting for applet resizing
		function finish()
		{
			if (applet.getSize().width != applet.width || applet.getSize().height != applet.height)
			{
				setTimeout(finish, 100);
			}
			else
			{
				// Getting the image
				var image = applet.getImageBase64('png');

				/* Posting formula */
				//opener.wrs_int_updateCAS(appletCode, image, newWidth, newHeight);
				//window.close();
				var args = { appletCode: appletCode, image: image, newWidth: newWidth, newHeight: newHeight };
				getRadWindow().close(args);

			}
		}

		finish();
	});
	$addHandler($get("cancel"), "click", function() { getRadWindow().close() });
}

function getRadWindow() //mandatory for the RadWindow dialogs functionality
{
	if (window.radWindow)
	{
		return window.radWindow;
	}
	if (window.frameElement && window.frameElement.radWindow)
	{
		return window.frameElement.radWindow;
	}
	return null;
}
