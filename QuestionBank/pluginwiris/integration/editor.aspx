﻿<%@ Page Language="c#" CodeFile="editor.aspx.cs" ValidateRequest="false" AutoEventWireup="false"  Inherits="pluginwiris.editor" %>

<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	<script type="text/javascript" src="../core/editor.js"></script>

	<title>WIRIS Formula Editor</title>
</head>
<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
	<form id="form1" runat="server">
	<ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
	</ajaxToolkit:ToolkitScriptManager>
	<table width="100%" height="100%">
		<tr height="100%">
			<td>
				<applet id="applet" codebase="<% this.Response.Write((string)config["wirisformulaeditorcodebase"]); %>"
					archive="<% this.Response.Write((string)config["wirisformulaeditorarchive"]); %>"
					code="<% this.Response.Write((string)config["wirisformulaeditorcode"]); %>" height="100%"
					width="100%" viewastext>
					<param name="lang" value="<% this.Response.Write((string)config["wirisformulaeditorlang"]); %>" />
					<param name="menuBar" value="false" />
				</applet>
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" id="submit" value="Accept" />
				<input type="button" id="cancel" value="Cancel" />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
