﻿/* Configuration */
var _wrs_conf_imageMathmlAttribute = 'alt'; // Specifies the image tag where we should save the formula editor mathml code
var _wrs_conf_CASMathmlAttribute = 'alt'; // Specifies the image tag where we should save the WIRIS CAS mathml code

var _wrs_conf_editorPath = _wrs_currentPath + 'pluginwiris/integration/editor.aspx'; // Specifies where is the editor HTML code (for popup window)
var _wrs_conf_CASPath = _wrs_currentPath + 'pluginwiris/integration/cas.aspx'; 		// Specifies where is the WIRIS CAS HTML code (for popup window)

var _wrs_conf_createimagePath = _wrs_currentPath + 'pluginwiris/integration/createimage.aspx'; 		// Specifies where is createimage script
var _wrs_conf_createcasimagePath = _wrs_currentPath + 'pluginwiris/integration/createcasimage.aspx'; // Specifies where is createcasimage script

function WIRIScas_CallbackFunction(editor, args)
{
	if (args && args.appletCode)
	{
		var imgObject = wrs_appletCodeToImgObject(editor.get_document(), args.appletCode, args.image, args.newWidth, args.newHeight);
		editor.pasteHtml($telerik.getOuterHtml(imgObject));
	}
}

function WIRISformula_CallbackFunction(editor, args)
{
	if (args && args.mathml)
	{
		var imgObject = wrs_mathmlToImgObject(editor.get_document(), args.mathml);
		editor.pasteHtml($telerik.getOuterHtml(imgObject));
	}
}

Telerik.Web.UI.Editor.CommandList["WIRIScas"] = function(commandName, editor, args)
{
	var argument = {};
	var selectedElement = editor.getSelectedElement();
	if (selectedElement && selectedElement.tagName == "IMG" && selectedElement.className == "Wiriscas")
	{
		argument._wrs_temporalImage = selectedElement;
	}
	var callbackFunction = function(sender, args)
	{
		WIRIScas_CallbackFunction(editor, args);
	}
	editor.showExternalDialog(_wrs_conf_CASPath, argument, 680, 530, callbackFunction, null, "OASIS CAS", true, Telerik.Web.UI.WindowBehaviors.Close, false, true);
};

Telerik.Web.UI.Editor.CommandList["WIRISformula"] = function(commandName, editor, args)
{
	var argument = {};
	var selectedElement = editor.getSelectedElement();
	if (selectedElement && selectedElement.tagName == "IMG" && selectedElement.className == "Wirisformula")
	{
		argument._wrs_temporalImage = selectedElement;
	}
	var callbackFunction = function(sender, args)
	{
		WIRISformula_CallbackFunction(editor, args);
	}
	editor.showExternalDialog(_wrs_conf_editorPath, argument, 550, 450, callbackFunction, null, "OASIS Formula Editor", true, Telerik.Web.UI.WindowBehaviors.Close, false, true);
};