﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="qbQuestionBank.aspx.vb" Inherits="QuestionBank_qbQuestionBank" Title="Untitled Page"
    EnableViewState="true" ValidateRequest="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/QuestionBank/UserControls/usrQuestion.ascx" TagName="usrQuestion"
    TagPrefix="uQ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<script language="javascript" type="text/javascript">
    document.oncontextmenu = new Function("return true");
</script>
    <script language="javascript" type="text/javascript">
        function GetTopics() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var type;
            var sbmId;
            var grdId;
            sbmId = document.getElementById('<%=h_SBM_ID.ClientID %>').value;
            grdId = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
            var oWnd = radopen("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "pop_topic");
            //alert(syllabusId);
           <%-- result = window.showModalDialog("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", sFeatures)
            if (result != "" && result != "undefined") {
                NameandCode = result.split('||');
                document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
                document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
            }--%>
           //' return false;
        }
        function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.Topic.split('||');
            document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
              __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }



         var crtl_id;
         function getImage() {
            $('a').click(function () {
                crtl_id = $(this).attr('id');
            });
            var NameandCode;
            var result;
            var url;
            url = "../QuestionBank/uploadImage.aspx"
            //'window.showModalDialog(url, "_self", sFeatures);
            var oWnd = radopen(url, "pop_topic2");
            return false;           
        }
         function OnClientClose2(oWnd, args) {
            //get the transferred arguments
             <%-- var arg = args.get_argument();
             if (arg) {
             NameandCode = arg.Topic.split('||');
             document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
             document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
               __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
             }--%>           
              __doPostBack(crtl_id, 'OnClick');
            
        }
    </script>
   <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>       
        <Windows>
            <telerik:RadWindow ID="pop_topic2" runat="server" Behaviors="Close,Move" Width="700px" Height="620px"
               OnClientAutoSizeEnd="autoSizeWithCalendar" OnClientClose="OnClientClose2">
            </telerik:RadWindow>          
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Question bank
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr id="trError" runat="server">
                        <td align="left">
                            <asp:Label ID="lblError" SkinID="Error" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="Table1" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                <tr>
                                    <td width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%"><span class="field-label">Subject</span>
                                    </td>

                                    <td width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%"><span class="field-label">Topic</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTopic" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgTopic" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetTopics(); return false;"></asp:ImageButton>
                                        <%--<table>
                                            <tr>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </td>
                                    <td width="20%"><span class="field-label">Skill</span>
                                    </td>

                                    <td>
                                        <asp:DropDownList ID="ddlSkill" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <span class="field-label">Difficulty Level</span>
                                    </td>

                                    <td>
                                        <asp:DropDownList ID="ddlDifficulty" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="border-color: #eceef1">
                            <table id="tbQuestion" runat="server" width="100%" border="1" style="border-color: #eceef1">
                                <tr>
                                    <td valign="top" style="border-color: #eceef1" width="50%">
                                        <table id="tQ" runat="server" width="100%">
                                            <tr>
                                                <td width="10%"></td>
                                                <td valign="top">
                                                    <uQ:usrQuestion ID="uQuestion" runat="server" TxtHeight="100" />
                                                </td>
                                                <td>
                                                    <span class="field-label">Max Marks</span>
                                                    <asp:TextBox ID="txtMax" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                               <td width="10%"></td>
                                                <td align="left" colspan="2">
                                                    <asp:LinkButton ID="lnkQuestion" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgQuestion" Width="300px" Height="50px" AutoAdjustImageControlSize="true" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="left" id="tdQuestionGrid" valign="top" width="50%" style="border-color: #eceef1; overflow:scroll;" rowspan="8" >
                                        <%--<div style="overflow: scroll">--%>
                                            <asp:DataList ID="dlQuestions" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                                ShowHeader="False">
                                                <ItemTemplate>
                                                    <table border="0" cellpadding="1" cellspacing="0" width="100%" style="border-color: #eceef1">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="lblSerialNo" Font-Size="14px" runat="server" Text='<%# getSerialNo() %>'></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblQuestion" runat="server" Text='<%# BIND("QSM_DESCR") %>' Width="500px"
                                                                    Font-Size="14px"></asp:Label>
                                                                <asp:Label ID="lblQsmId" runat="server" Text='<%# BIND("QSM_ID") %>' Visible="false"
                                                                    Width="118px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/EditImage.jpg" OnClick="imgEdit_Click"
                                                                    ToolTip="Edit" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/delete.jpg" OnClick="imgDelete_Click"
                                                                    ToolTip="Delete" />
                                                                <ajaxToolkit:ConfirmButtonExtender ID="c2" TargetControlID="imgDelete" ConfirmText="Selected question will be deleted permanently.Are you sure you want to continue? "
                                                                    runat="server">
                                                                </ajaxToolkit:ConfirmButtonExtender>
                                                            </td>
                                                            </t>
                                            <tr>
                                                <td colspan="2">
                                                    <telerik:RadBinaryImage ID="imgQuest" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:GridView ID="gvOptions" runat="server" AllowPaging="false" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                                                        BorderStyle="None" OnRowDataBound="gvOptions_RowDataBound">
                                                                        <RowStyle HorizontalAlign="Center" BorderStyle="None" BorderWidth="0px" />
                                                                        <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <table border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lblQsdId" runat="server" Text='<%# Bind("QSD_ID") %>' Visible="false"></asp:Label>
                                                                                                <asp:Label ID="lblSlno" Font-Size="14px" runat="server" Text='<%# Bind("QSD_SLNO") %>'></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Label ID="lblDescription" Font-Size="14px" runat="server" Text='<%# Bind("QSD_DESCR") %>'></asp:Label>
                                                                                                <asp:Image ID="imgAnswer" runat="server" ImageUrl="~/Images/bluetick.jpg" Visible='<%# Bind("QSD_bANSWER") %>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <telerik:RadBinaryImage ID="imgOpt" runat="server" Visible="false" />
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" BorderStyle="None"></ItemStyle>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <HeaderStyle Wrap="False" Height="0px" BorderStyle="None" />
                                                                        <EditRowStyle Wrap="False" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                       <%-- </div>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #eceef1" >
                                        <table id="tOpt1" runat="server" width="100%">
                                            <tr>
                                                <td width="10%">
                                                    <asp:TextBox ID="txtOpt1" runat="server" Text="a)"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <uQ:usrQuestion ID="uOpt1" runat="server" TxtHeight="60" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdOpt1" runat="server" GroupName="gOpt" Text="Answer" CssClass="field-label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkOpt1" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt1" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #eceef1">
                                        <table id="tbOpt2" runat="server" width="100%">
                                            <tr>
                                                <td  width="10%">
                                                    <asp:TextBox ID="txtOpt2" runat="server" Text="b)"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <uQ:usrQuestion ID="uOpt2" runat="server" TxtHeight="60" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdOpt2" runat="server" GroupName="gOpt" Text="Answer" CssClass="field-label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkOpt2" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt2" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #eceef1">
                                        <table id="tbOpt3" runat="server" width="100%">
                                            <tr>
                                                <td  width="10%">
                                                    <asp:TextBox ID="txtOpt3" runat="server" Text="c)"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <uQ:usrQuestion ID="uOpt3" runat="server" TxtHeight="60" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdOpt3" runat="server" GroupName="gOpt" Text="Answer" CssClass="field-label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkOpt3" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt3" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #eceef1">
                                        <table id="tbOpt4" runat="server" width="100%">
                                            <tr>
                                                <td  width="10%">
                                                    <asp:TextBox ID="txtOpt4" runat="server" Text="d)"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <uQ:usrQuestion ID="uOpt4" runat="server" TxtHeight="60" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdOpt4" runat="server" GroupName="gOpt" Text="Answer" CssClass="field-label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkOpt4" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt4" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #eceef1">
                                        <table id="tbOpt5" runat="server" width="100%"> 
                                            <tr>
                                                <td  width="10%">
                                                    <asp:TextBox ID="txtOpt5" runat="server" Text="e)"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <uQ:usrQuestion ID="uOpt5" runat="server" TxtHeight="60" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdOpt5" runat="server" GroupName="gOpt" Text="Answer" CssClass="field-label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkOpt5" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;" OnClick="lnkOpt5_Click">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt5" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-color: #eceef1">
                                        <table id="tbOpt6" runat="server" width="100%">
                                            <tr>
                                                <td  width="10%">
                                                    <asp:TextBox ID="txtOpt6" runat="server" Text="f)"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <uQ:usrQuestion ID="uOpt6" runat="server" TxtHeight="60" />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdOpt6" runat="server" GroupName="gOpt" Text="Answer" CssClass="field-label" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td align="left">
                                                    <asp:LinkButton ID="lnkOpt6" Style="color: #1b80b6" runat="server" OnClientClick="javascript:getImage(); return false;">Upload Image</asp:LinkButton>
                                                    <telerik:RadBinaryImage ID="imgOpt6" runat="server" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_SBM_ID" runat="server" />
                <asp:HiddenField ID="h_GRD_ID" runat="server" />
                <asp:HiddenField ID="h_Topic_ID" runat="server" />
                <asp:HiddenField ID="h_QSM_ID" runat="server" />

            </div>
        </div>
    </div> 
     
</asp:Content>
