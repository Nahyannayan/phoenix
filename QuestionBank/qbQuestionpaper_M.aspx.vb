﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports Telerik.Web.UI
Imports System.Data.SqlClient
Partial Class QuestionBank_qbQuestionpaper_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                Dim datamode As String = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = datamode
                ViewState("questiondatamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C700025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                       ViewState("slno") = "0"

                    '  tblTopic.Visible = False
                
                    ViewState("QPM_ID") = CInt(Request.QueryString("qpmid")).ToString
                    BindQuestionSectionDropdown()
                    BindqbQuestionSectionDropdown()
                    BindQuestion()



                    h_SBM_ID.Value = Request.QueryString("sbmid")
                    h_GRD_ID.Value = Request.QueryString("grdid")
                    h_ACD_ID.Value = Request.QueryString("acdid")

                    BindqbDifficulty()
                    BindqbSkill()

                    usrQuestionBank1.sbm_id = h_SBM_ID.Value
                    usrQuestionBank1.grd_id = h_GRD_ID.Value
                    usrQuestionBank1.skl_id = "0"
                    usrQuestionBank1.dl_id = "0"
                    usrQuestionBank1.topic_id = "0"
                    usrQuestionBank1.qpm_id = ViewState("QPM_ID")
                    usrQuestionBank1.BindQuestion()

                    BindDifficulty()
                    BindSkill()

                    txtSlno.Text = getSerialNo()
                    BindMainQuestion()
                    BindqbMainQuestion()

                End If

                ViewState("ShowPanel") = 0
            Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            '   lblError.Text = "Request could not be processed"
        End Try
        End If


      
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSaveQuestion)
    End Sub

#Region "Common Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region
#Region "Question Paper"

    Sub BindQuestionSectionDropdown()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPS_ID,QPS_DESCR " _
                                & " FROM QSP.QUESTIONPAPER_SECTIONS WHERE QPS_QPM_ID='" + ViewState("QPM_ID") + "'" _
                                & " ORDER BY QPS_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlQuestionSection.DataSource = ds
        ddlQuestionSection.DataTextField = "QPS_DESCR"
        ddlQuestionSection.DataValueField = "QPS_ID"
        ddlQuestionSection.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlQuestionSection.Items.Insert(0, li)
    End Sub
    Sub BindqbQuestionSectionDropdown()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPS_ID,QPS_DESCR " _
                                & " FROM QSP.QUESTIONPAPER_SECTIONS WHERE QPS_QPM_ID='" + ViewState("QPM_ID") + "'" _
                                & " ORDER BY QPS_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlqbQuestionSection.DataSource = ds
        ddlqbQuestionSection.DataTextField = "QPS_DESCR"
        ddlqbQuestionSection.DataValueField = "QPS_ID"
        ddlqbQuestionSection.DataBind()
        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlqbQuestionSection.Items.Insert(0, li)
    End Sub
    Function getSerialNo() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT  ISNULL(MAX(QPD_SLNO),0)+1 FROM QSP.QUESTIONPAPER_D WHERE QPD_QPM_ID=" + ViewState("QPM_ID")
        Dim slno As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If slno = 0 Then
            Return 1
        Else
            Return slno
        End If
    End Function

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim lblQpdId As Label = TryCast(sender.findcontrol("lblQpdId"), Label)
        h_QPD_ID.Value = lblQpdId.Text
        ViewState("questiondatamode") = "edit"
        getQusetionDetails(lblQpdId.Text)
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim lblQpdId As Label = TryCast(sender.findcontrol("lblQpdId"), Label)
        ViewState("questiondatamode") = "delete"
        h_QPD_ID.Value = lblQpdId.Text
        SaveQuestions()
        ClearControls()
        BindQuestion()
    End Sub

    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblQpoId As Label = e.Row.FindControl("lblQpoId")
            Dim imgOpt As RadBinaryImage = e.Row.FindControl("imgOpt")
            BindOptionImage(lblQpoId.Text, imgOpt)
        End If
    End Sub

    Sub getQusetionDetails(ByVal qpd_id As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT  QPD_DESCR,QPD_IMAGE,ISNULL(QPD_MAXMARK,0) QPD_MAXMARK,QPD_SLNO,ISNULL(QPD_PARENT_ID,0) QPD_PARENT_ID,ISNULL(QPD_QPS_ID,0) QPD_QPS_ID,QPD_TYPE  FROM QSP.QUESTIONPAPER_D WHERE QPD_ID=" + qpd_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        With ds.Tables(0).Rows(0)
            uQuestion.TxtValue = .Item(0)
            If IsDBNull(.Item(1)) = False Then
                ViewState("imgQuestion") = .Item(1)
                imgQuestion.DataValue = ViewState("imgQuestion")
                imgQuestion.Visible = True
            End If
            txtMaxMark.Text = .Item(2)
            txtSlno.Text = .Item(3)
            If Not ddlMainQuestion.Items.FindByValue(.Item(4)) Is Nothing Then
                ddlMainQuestion.ClearSelection()
                ddlMainQuestion.Items.FindByValue(.Item(4)).Selected = True
            End If

            If Not ddlqbQuestionSection.Items.FindByValue(.Item(5)) Is Nothing Then
                ddlQuestionSection.ClearSelection()
                ddlQuestionSection.Items.FindByValue(.Item(5)).Selected = True
            End If

            If .Item(6) = "MCQ" Then
                rdMCQ.Checked = True
                rdDescriptive.Checked = False

                trOpt1.Visible = True
                trOpt2.Visible = True
                trOpt3.Visible = True
                trOpt4.Visible = True
                trOpt5.Visible = True
                trOpt6.Visible = True
            Else
                rdMCQ.Checked = False
                rdDescriptive.Checked = True

                trOpt1.Visible = False
                trOpt2.Visible = False
                trOpt3.Visible = False
                trOpt4.Visible = False
                trOpt5.Visible = False
                trOpt6.Visible = False
            End If

        End With


        str_query = "SELECT QPO_SLNO,QPO_DESCR,QPO_IMAGE,QPO_OPTION,ISNULL(QPO_bANSWER,'FALSE') QPO_bANSWER FROM QSP.QUESTIONPAPER_OPTIONS WHERE QPO_QPD_ID=" + qpd_id
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            With ds.Tables(0).Rows(i)
                If .Item(3).ToString = "1" Then
                    txtOpt1.Text = .Item(0)
                    uOpt1.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt1") = .Item(2)
                        imgOpt1.DataValue = ViewState("imgOpt1")
                        imgOpt1.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt1.Checked = True
                    End If
                ElseIf .Item(3).ToString = "2" Then
                    txtOpt2.Text = .Item(0)
                    uOpt2.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt2") = .Item(2)
                        imgOpt2.DataValue = ViewState("imgOpt2")
                        imgOpt2.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt2.Checked = True
                    End If
                ElseIf .Item(3).ToString = "3" Then
                    txtOpt3.Text = .Item(0)
                    uOpt3.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt3") = .Item(2)
                        imgOpt3.DataValue = ViewState("imgOpt3")
                        imgOpt3.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt3.Checked = True
                    End If
                ElseIf .Item(3).ToString = "4" Then
                    txtOpt4.Text = .Item(0)
                    uOpt4.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt4") = .Item(2)
                        imgOpt4.DataValue = ViewState("imgOpt4")
                        imgOpt4.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt4.Checked = True
                    End If
                ElseIf .Item(3).ToString = "5" Then
                    txtOpt5.Text = .Item(0)
                    uOpt5.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt5") = .Item(2)
                        imgOpt5.DataValue = ViewState("imgOpt5")
                        imgOpt5.Visible = True
                    End If
                    If .Item(4) = True Then
                        rdOpt5.Checked = True
                    End If
                ElseIf .Item(3).ToString = "6" Then
                    txtOpt6.Text = .Item(0)
                    uOpt6.TxtValue = .Item(1)
                    If IsDBNull(.Item(2)) = False Then
                        ViewState("imgOpt6") = .Item(2)
                        imgOpt6.DataValue = ViewState("imgOpt6")
                        imgOpt6.Visible = True
                    End If
                    If .Item(6) = True Then
                        rdOpt1.Checked = True
                    End If
                End If
            End With
        Next
    End Sub
    Sub BindQuestion()
        ViewState("slno") = 0
        h_QPD_ID.Value = 0
        dlQuestions.DataSource = Nothing
        dlQuestions.DataBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT ISNULL(QPD_SLNO,'1')QPD_SLNO,QPD_SLNO_DISPLAY+')' QPD_SLNO_DISPLAY ,QPD_ID,QPD_DESCR,'('+REPLACE(CONVERT(VARCHAR(100),ISNULL(QPD_MAXMARK,0)),'.00','')+')' QPD_MAXMARK " _
                                  & " FROM QSP.QUESTIONPAPER_D WHERE QPD_QPM_ID=" + ViewState("QPM_ID") _
                                  & " ORDER BY CONVERT(NUMERIC(16,3),QPD_SLNO_DISPLAY)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlQuestions.DataSource = ds
        dlQuestions.DataBind()
    End Sub

    Sub BindQuestionImage(ByVal qpd_id As String, ByVal imgQuest As RadBinaryImage)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPD_IMAGE FROM QSP.QUESTIONPAPER_D WHERE QPD_ID=" + qpd_id + " AND QPD_IMAGE IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            imgQuest.DataValue = ds.Tables(0).Rows(0).Item(0)
            imgQuest.Visible = True
        End If
    End Sub

    Sub BindQuestionOptions(ByVal qpd_id As String, ByVal gvOption As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPO_ID,QPO_SLNO,QPO_DESCR,ISNULL(QPO_bANSWER,'FALSE') QPO_bANSWER FROM QSP.QUESTIONPAPER_OPTIONS WHERE QPO_QPD_ID=" + qpd_id

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvOption.DataSource = ds
        gvOption.DataBind()
        If gvOption.Rows.Count > 0 Then
            gvOption.HeaderRow.Visible = False
        End If
    End Sub

    Sub BindOptionImage(ByVal qpo_id As String, ByVal imgOpt As RadBinaryImage)
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPO_IMAGE FROM  QSp.QUESTIONPAPER_OPTIONS WHERE  QPO_ID=" + qpo_id + " AND QPO_IMAGE IS NOT NULL"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            imgOpt.DataValue = ds.Tables(0).Rows(0).Item(0)
            imgOpt.Visible = True
        End If
    End Sub

    Sub BindDifficulty()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT DL_ID,ISNULL(DL_DESCR,'') DL_DESCR,ISNULL(DL_ORDER,0) DL_ORDER,ISNULL(DL_SHORTNAME,'') DL_SHORTNAME FROM " _
                                & " QST.DIFFICULTYLEVEL_M WHERE " _
                                & " DL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY DL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDifficulty.DataSource = ds
        ddlDifficulty.DataTextField = "DL_DESCR"
        ddlDifficulty.DataValueField = "DL_ID"
        ddlDifficulty.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlDifficulty.Items.Insert(0, li)
    End Sub

    Sub BindSkill()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT SKL_ID,ISNULL(SKL_DESCR,'') SKL_DESCR,ISNULL(SKL_ORDER,0) SKL_ORDER,ISNULL(SKL_SHORTNAME,'') SKL_SHORTNAME FROM " _
                                & " QST.SKILLS_M WHERE " _
                                & " SKL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY SKL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSkill.DataSource = ds
        ddlSkill.DataTextField = "SKL_DESCR"
        ddlSkill.DataValueField = "SKL_ID"
        ddlSkill.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSkill.Items.Insert(0, li)
    End Sub

    Sub SaveQuestions()
        Using connection As SqlConnection = ConnectionManger.GetOASIS_QBConnection()
            Try
                Dim pParms(14) As SqlClient.SqlParameter
                Dim qpdId As Integer
                If ViewState("questiondatamode") <> "add" Then
                    qpdId = CInt(h_QPD_ID.Value)
                End If
                Dim qpmId As Integer = CInt(ViewState("QPM_ID"))
                pParms(0) = New SqlClient.SqlParameter("@QPD_ID", qpdId)
                pParms(0).Direction = ParameterDirection.InputOutput
                pParms(1) = New SqlClient.SqlParameter("@QPD_QPM_ID", qpmId)
                pParms(2) = New SqlClient.SqlParameter("@QPD_QPS_ID", CInt(ddlQuestionSection.SelectedValue))
                pParms(3) = New SqlClient.SqlParameter("@QPD_SBM_ID", CInt(h_SBM_ID.Value))
                pParms(4) = New SqlClient.SqlParameter("@QPD_ACD_ID", CInt(h_ACD_ID.Value))
                pParms(5) = New SqlClient.SqlParameter("@QPD_GRD_ID", h_GRD_ID.Value)
                pParms(6) = New SqlClient.SqlParameter("@QPD_SLNO", CInt(txtSlno.Text))
                pParms(7) = New SqlClient.SqlParameter("@QPD_DESCR", uQuestion.TxtValue)
                pParms(8) = New SqlClient.SqlParameter("@QPD_MAXMARK", CDbl(IIf(txtMaxMark.Text = "", "0", txtMaxMark.Text)))
                pParms(9) = New SqlClient.SqlParameter("@QPD_PARENT_ID", CInt(ddlMainQuestion.SelectedValue))
                pParms(10) = New SqlClient.SqlParameter("@QPD_PARENT_SLNO", CInt(IIf(ddlMainQuestion.SelectedItem.Text = "--", "0", ddlMainQuestion.SelectedItem.Text)))
                pParms(11) = New SqlClient.SqlParameter("@MODE", ViewState("questiondatamode"))
                If Not ViewState("imgquestion") Is Nothing Then
                    pParms(12) = New SqlClient.SqlParameter("@QPD_IMAGE", ViewState("imgquestion"))
                    pParms(13) = New SqlClient.SqlParameter("@QPD_TYPE", IIf(rdDescriptive.Checked = True, "DES", "MCQ"))
                    pParms(14) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(14).Direction = ParameterDirection.ReturnValue
                Else
                    pParms(12) = New SqlClient.SqlParameter("@QPD_TYPE", IIf(rdDescriptive.Checked = True, "DES", "MCQ"))
                    pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms(13).Direction = ParameterDirection.ReturnValue
                End If
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "QSP.saveQUESTIONPAPER_D", pParms)
                If ViewState("questiondatamode") = "add" Then
                    h_QPD_ID.Value = pParms(0).Value
                End If
            Catch ex As Exception

            End Try
        End Using
    End Sub

    Sub BindMainQuestion()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPD_SLNO,QPD_ID FROM QSP.QUESTIONPAPER_D WHERE QPD_QPM_ID=" + ViewState("QPM_ID") _
                                & " AND ISNULL(QPD_PARENT_ID,0)=0 ORDER BY QPD_SLNO"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMainQuestion.DataSource = ds
        ddlMainQuestion.DataTextField = "QPD_SLNO"
        ddlMainQuestion.DataValueField = "QPD_ID"
        ddlMainQuestion.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlMainQuestion.Items.Insert(0, li)
    End Sub

    Sub BindqbMainQuestion()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPD_SLNO,QPD_ID FROM QSP.QUESTIONPAPER_D WHERE QPD_QPM_ID=" + ViewState("QPM_ID") _
                                & " AND ISNULL(QPD_PARENT_ID,0)=0 ORDER BY QPD_SLNO"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlQbMainQuestion.DataSource = ds
        ddlQbMainQuestion.DataTextField = "QPD_SLNO"
        ddlQbMainQuestion.DataValueField = "QPD_ID"
        ddlQbMainQuestion.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlQbMainQuestion.Items.Insert(0, li)
    End Sub



    Sub SaveOptions(ByVal opt As String)
        Using connection As SqlConnection = ConnectionManger.GetOASIS_QBConnection()
            Try
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@QPO_QPD_ID", h_QPD_ID.Value)
                If opt = "1" Then
                    If uOpt1.TxtValue = "" And ViewState("imgOpt1") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QPO_SLNO", txtOpt1.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QPO_DESCR", uOpt1.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QPO_bANSWER", rdOpt1.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QPO_OPTION", 1)
                    If Not ViewState("imgOpt1") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QPO_IMAGE", ViewState("imgOpt1"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "2" Then
                    If uOpt2.TxtValue = "" And ViewState("imgOpt2") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QPO_SLNO", txtOpt2.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QPO_DESCR", uOpt2.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QPO_bANSWER", rdOpt2.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QPO_OPTION", 2)
                    If Not ViewState("imgOpt2") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QPO_IMAGE", ViewState("imgOpt2"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "3" Then
                    If uOpt3.TxtValue = "" And ViewState("imgOpt3") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QPO_SLNO", txtOpt3.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QPO_DESCR", uOpt3.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QPO_bANSWER", rdOpt3.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QPO_OPTION", 3)
                    If Not ViewState("imgOpt3") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QPO_IMAGE", ViewState("imgOpt3"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "4" Then
                    If uOpt4.TxtValue = "" And ViewState("imgOpt4") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QPO_SLNO", txtOpt4.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QPO_DESCR", uOpt4.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QPO_bANSWER", rdOpt4.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QPO_OPTION", 4)
                    If Not ViewState("imgOpt4") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QPO_IMAGE", ViewState("imgOpt4"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "5" Then
                    If uOpt5.TxtValue = "" And ViewState("imgOpt5") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QPO_SLNO", txtOpt5.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QPO_DESCR", uOpt5.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QPO_bANSWER", rdOpt5.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QPO_OPTION", 5)
                    If Not ViewState("imgOpt5") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QPO_IMAGE", ViewState("imgOpt5"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                ElseIf opt = "6" Then
                    If uOpt6.TxtValue = "" And ViewState("imgOpt6") Is Nothing Then
                        Exit Sub
                    End If
                    pParms(1) = New SqlClient.SqlParameter("@QPO_SLNO", txtOpt6.Text)
                    pParms(2) = New SqlClient.SqlParameter("@QPO_DESCR", uOpt6.TxtValue)
                    pParms(3) = New SqlClient.SqlParameter("@QPO_bANSWER", rdOpt6.Checked)
                    pParms(4) = New SqlClient.SqlParameter("@QPO_OPTION", 6)
                    If Not ViewState("imgOpt6") Is Nothing Then
                        pParms(5) = New SqlClient.SqlParameter("@QPO_IMAGE", ViewState("imgOpt6"))
                        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(6).Direction = ParameterDirection.ReturnValue
                    Else
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                    End If
                End If
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "QSP.saveQUESTIONPAPER_OPTIONS", pParms)
            Catch ex As Exception

            End Try
        End Using
    End Sub

    Sub ClearControls()
        h_QPD_ID.Value = 0
        uQuestion.TxtValue = ""
        uOpt1.TxtValue = ""
        uOpt2.TxtValue = ""
        uOpt3.TxtValue = ""
        uOpt4.TxtValue = ""
        uOpt5.TxtValue = ""
        uOpt6.TxtValue = ""
        txtMaxMark.Text = ""
        txtSlno.Text = getSerialNo()

        ViewState("imgquestion") = Nothing
        ViewState("imgOpt1") = Nothing
        ViewState("imgOpt2") = Nothing
        ViewState("imgOpt3") = Nothing
        ViewState("imgOpt4") = Nothing
        ViewState("imgOpt5") = Nothing
        ViewState("imgOpt6") = Nothing

        imgQuestion.Visible = False
        imgOpt1.Visible = False
        imgOpt2.Visible = False
        imgOpt3.Visible = False
        imgOpt4.Visible = False
        imgOpt5.Visible = False
        imgOpt6.Visible = False

        ViewState("questiondatamode") = "add"
    End Sub

    Public Shared Function ConvertImageFiletoBytes(ByVal ImageFilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(ImageFilePath) = True Then
            Throw New ArgumentNullException("Image File Name Cannot be Null or Empty", "ImageFilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(ImageFilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(ImageFilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Protected Sub lnkQuestion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQuestion.Click
        If Not Session("qImage") Is Nothing Then
            'imgQuestion.ImageUrl = Session("qImage")
            ' Label1.Text = Session("qImage").ToString
            imgQuestion.Visible = True
            imgQuestion.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgquestion") = imgQuestion.DataValue
            If imgQuestion.Height.Value > 150 Or imgQuestion.Width.Value > 100 Then
                '  imgQuestion.i()


            End If
        End If
    End Sub

    Protected Sub lnkOpt1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt1.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt1.Visible = True
            imgOpt1.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt1") = imgOpt1.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt2.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt2.Visible = True
            imgOpt2.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt2") = imgOpt2.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt3.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt3.Visible = True
            imgOpt3.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt3") = imgOpt3.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt4.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt4.Visible = True
            imgOpt4.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt4") = imgOpt4.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt5.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt5.Visible = True
            imgOpt5.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt5") = imgOpt5.DataValue
            Session("qImage") = Nothing
        End If
    End Sub

    Protected Sub lnkOpt6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOpt6.Click
        If Not Session("qImage") Is Nothing Then
            imgOpt6.Visible = True
            imgOpt6.DataValue = ConvertImageFiletoBytes(Server.MapPath(Session("qImage")))
            ViewState("imgOpt6") = imgOpt6.DataValue
            Session("qImage") = Nothing
        End If
    End Sub
#End Region

#Region "QuestionBank"
    Sub BindqbSkill()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT SKL_ID,ISNULL(SKL_DESCR,'') SKL_DESCR,ISNULL(SKL_ORDER,0) SKL_ORDER,ISNULL(SKL_SHORTNAME,'') SKL_SHORTNAME FROM " _
                                & " QST.SKILLS_M WHERE " _
                                & " SKL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY SKL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlqbSkill.DataSource = ds
        ddlqbSkill.DataTextField = "SKL_DESCR"
        ddlqbSkill.DataValueField = "SKL_ID"
        ddlqbSkill.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlqbSkill.Items.Insert(0, li)
    End Sub

    Sub BindqbDifficulty()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT DL_ID,ISNULL(DL_DESCR,'') DL_DESCR,ISNULL(DL_ORDER,0) DL_ORDER,ISNULL(DL_SHORTNAME,'') DL_SHORTNAME FROM " _
                                & " QST.DIFFICULTYLEVEL_M WHERE " _
                                & " DL_BSU_ID=" + Session("SBSUID") _
                                & " ORDER BY DL_ORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlqbDifficulty.DataSource = ds
        ddlqbDifficulty.DataTextField = "DL_DESCR"
        ddlqbDifficulty.DataValueField = "DL_ID"
        ddlqbDifficulty.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlqbDifficulty.Items.Insert(0, li)
    End Sub

    Sub CopyQuestionstoBank()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "exec QSP.copyQUESTIONTOQUESTIONBANK " _
                               & " @BSU_ID='" + Session("sbsuid") + "'," _
                               & " @QPD_ID=" + h_QPD_ID.Value + "," _
                               & " @TOP_ID=" + h_Topic_ID.Value + "," _
                               & " @DL_ID=" + ddlDifficulty.SelectedValue.ToString + "," _
                               & " @SKL_ID=" + ddlSkill.SelectedValue.ToString
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region

    Protected Sub dlQuestions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlQuestions.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblQpdId As Label = e.Item.FindControl("lblQpdId")
            Dim imgQuest As RadBinaryImage = e.Item.FindControl("imgQuest")
            Dim gvOptions As GridView = e.Item.FindControl("gvOptions")
            BindQuestionImage(lblQpdId.Text, imgQuest)
            BindQuestionOptions(lblQpdId.Text, gvOptions)
        End If
    End Sub
    Protected Sub btnSaveQuestion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveQuestion.Click
        SaveQuestions()
        SaveOptions(1)
        SaveOptions(2)
        SaveOptions(3)
        SaveOptions(4)
        SaveOptions(5)
        SaveOptions(6)
        If chkAdd.Checked = True Then
            CopyQuestionstoBank()
        End If
        ClearControls()
        BindQuestion()
        BindMainQuestion()
        BindqbMainQuestion()
    End Sub

    Protected Sub lnkAddQuestions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddQuestions.Click

    End Sub

    Protected Sub ddlqbSkill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlqbSkill.SelectedIndexChanged
        usrQuestionBank1.skl_id = ddlqbSkill.SelectedValue
        usrQuestionBank1.BindQuestion()
        MPOI.Show()
    End Sub

    Protected Sub ddlqbDifficulty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlqbDifficulty.SelectedIndexChanged
        usrQuestionBank1.dl_id = ddlqbDifficulty.SelectedValue
        usrQuestionBank1.BindQuestion()
        MPOI.Show()
    End Sub

    Protected Sub imgqbTopic_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgqbTopic.Click
        usrQuestionBank1.topic_id = h_qbTopic_ID.Value
        usrQuestionBank1.BindQuestion()
        MPOI.Show()
    End Sub

    Protected Sub btnSaveQB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveQB.Click
        usrQuestionBank1.acd_id = h_ACD_ID.Value
        usrQuestionBank1.qps_id = ddlqbQuestionSection.SelectedValue.ToString
        usrQuestionBank1.main_id = ddlQbMainQuestion.SelectedValue.ToString
        usrQuestionBank1.saveQuestionPaperFromBank()
        usrQuestionBank1.BindQuestion()
        BindQuestion()
        BindMainQuestion()
        BindqbMainQuestion()
    End Sub
End Class
