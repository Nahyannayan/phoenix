﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="qbQuestionpaperSettings_M.aspx.vb" Inherits="QuestionBank_qbQuestionpaperSettings_M"
    Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkRelease") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }


  function enableTime(chk) {
            if (chk.checked == true) {
            
                 document.getElementById('<%=txtTime.ClientID %>').disabled=false;
            }
            else {
                 document.getElementById('<%=txtTime.ClientID %>').disabled=true;
            }

            return true;
        }
    </script>
    
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> Question Paper </div>
         <div class="card-body">
            <div class="table-responsive m-auto">
    <ajaxToolkit:TabContainer ID="tabQuestionpaper" runat="server" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel ID="tpSetting" runat="server">
            <ContentTemplate>
                <table id="tblClm" runat="server" align="center" cellpadding="5" cellspacing="0">
                    <tr runat="server">
                        <td align="left" runat="server">
                           <span class="field-label"> Select Academic Year </span> 
                        </td>
                       
                        <td align="left" runat="server">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>
                        <td align="left" runat="server">
                           <span class="field-label"> Grade</span> 
                        </td>
                      
                        <td align="left" runat="server">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" runat="server">
                           <span class="field-label"> Select Subject</span> 
                        </td>
                       
                        <td align="left" runat="server">
                            <asp:DropDownList ID="ddlSubject" AutoPostBack="True" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" runat="server">
                         <span class="field-label">   Description</span> 
                        </td>
                       
                        <td colspan="3" runat="server">
                            <asp:TextBox ID="txtDescr"  runat="server"></asp:TextBox>
                        </td>
                        <td rowspan="9" colspan="4" runat="server">
                            <div >
                                <asp:GridView ID="gvSections" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    PageSize="20">
                                    <RowStyle  Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="index" Visible="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblIndex" runat="server" Text='<%# Bind("index") %>'></asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="objid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lbQpsId" runat="server" Text='<%# Bind("QPS_ID") %>'></asp:Label></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="objid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQpmId" runat="server" Text='<%# Bind("QPS_QPM_ID") %>'></asp:Label></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sl.No">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtOrder" runat="server" Width="30px" Text='<%# Bind("QPS_ORDER") %>'></asp:TextBox></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Section/Part">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSecDescription" runat="server" Width="150px" Text='<%# Bind("QPS_DESCR") %>'></asp:TextBox></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Instructions">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSecInstructions" runat="server" Width="250px" Text='<%# Bind("QPS_INSTRUCTIONS") %>'
                                                    TextMode="MultiLine" SkinID="MultiText"></asp:TextBox></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="objid" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDelete" runat="server"></asp:Label></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label></HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle  Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle Wrap="False" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" runat="server">
                           <span class="field-label"> Display Name </span> 
                        </td>
                       
                        <td colspan="3" runat="server">
                            <asp:TextBox ID="txtDisplay"  runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" runat="server">
                            <span class="field-label">Max Marks</span> 
                        </td>
                       
                        <td align="left" colspan="3" runat="server">
                            <asp:TextBox ID="txtMax" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" runat="server">
                           <span class="field-label"> Min Pass Mark</span> 
                        </td>
                       
                        <td align="left" colspan="3" runat="server">
                            <asp:TextBox ID="txtPassMark" runat="server" AutoCompleteType="Disabled" Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" runat="server">
                         <span class="field-label">   Date</span> 
                        </td>
                       
                        <td align="left" colspan="3" runat="server">
                            <asp:TextBox ID="txtDate" Width="100px" runat="server"></asp:TextBox>
                            &nbsp;<asp:ImageButton ID="imgDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="left" runat="server">
                           <span class="field-label"> Duration </span> 
                        </td>
                      
                        <td align="left" colspan="3" runat="server">
                            <asp:TextBox ID="txtDuration" Width="100px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                   
                    <tr runat="server">
                        <td runat="server" align="left">
                            <span class="field-label">Instructions</span> 
                        </td>
                     
                        <td runat="server" align="left" colspan="3">
                            <asp:TextBox ID="txtInstructions" runat="server" SkinID="MultiText_Large" TextMode="MultiLine"
                                Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                     <tr id="Tr1" runat="server" align="left">
                            <td id="Td3" colspan="5" runat="server" align="left" >
                            <asp:CheckBox ID="chkTimer" Text="Set Timer"  runat="server" onclick="javascript:enableTime(this);" CssClass="field-label" />
                            <span class="field-label">Time in mitutes :</span> 
                            <asp:TextBox ID="txtTime" runat="server" Enabled="False" Width="100px"></asp:TextBox></td>
                    </tr>
                    <tr runat="server" align="left">
                        <td colspan="5" runat="server">
                            <asp:CheckBox ID="chkOpen" runat="server" Text="Open to All Classes"  CssClass="field-label" />
                            <asp:CheckBox ID="chkAnser" runat="server" Text="Display Answer"  CssClass="field-label" />
                            <asp:CheckBox ID="chkResult" runat="server" Text="Display Result"  CssClass="field-label" />
                        </td>
                    </tr>
                    <tr runat="server">
                        <td runat="server" colspan="9">
                            <asp:Button ID="btnSaveSettings" runat="server" CssClass="button" TabIndex="4" Text="Save"
                                Width="51px" />
                            <asp:Button ID="btnSetQuestion" runat="server" CssClass="button" TabIndex="4" Text="Set Questions"
                                Width="100px" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgDate"
                    TargetControlID="txtDate" Enabled="True">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calDate2" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDate"
                    Enabled="True">
                </ajaxToolkit:CalendarExtender>
            </ContentTemplate>
            <HeaderTemplate>
               Settings 
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="tpReleaseClass" runat="server">
            <ContentTemplate>
                <table width="100%" cellpadding="5"  cellspacing="0"
                   >
                   
                    <tr>
                        <td>
                                <asp:RadioButton ID="rdSection" runat="server" GroupName="g1" Text="Release by Section" CssClass="field-label" />
                                <asp:RadioButton ID="rdGroup" Checked="true" runat="server" GroupName="g1" Text="Release by Group"  CssClass="field-label"/>
                        </td>
                        <td>
                           <span class="field-label"> Release Start Date </span> 
                        </td>
                       
                        <td>
                            <telerik:RadDateTimePicker ID="rtClassStart"  Width="200px" runat="server" Culture="English (United States)">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" DisplayDateFormat="dd/MMM/yyyy HH:MM tt"
                                    LabelWidth="64px" Width="">
                                
                                </DateInput>
                                 <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                               
                            </telerik:RadDateTimePicker>
                        </td>
                        <td>
                           <span class="field-label"> Release End Date</span> 
                        </td>
                       
                        <td>
                            <telerik:RadDateTimePicker ID="rtClassEnd" Width="200px" runat="server" Culture="English (United States)">
                                                            <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" DisplayDateFormat="dd/MMM/yyyy HH:MM tt"
                                    LabelWidth="64px" Width="">
                                                                   </DateInput>
                                                                   
                              <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                                </telerik:RadDateTimePicker>
                        </td>
                        <td>
                            <asp:Button ID="btnClassApply" runat="server" Text="Apply" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <asp:GridView ID="gvReleastoClass" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20">
                                <RowStyle  Wrap="False" />
                                <AlternatingRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="objid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbQpgId" runat="server" Text='<%# Bind("QPG_ID") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSgrIdId" runat="server" Text='<%# Bind("SGR_ID") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField >
                                    <HeaderTemplate>
                                      <asp:Label ID="lblHdrGroup" Text="Group" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Release">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRelease" Checked='<%# Bind("QPG_bRELEASE") %>' runat="server">
                                            </asp:CheckBox></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Date">
                                        <ItemTemplate>
                                            <telerik:RadDateTimePicker SelectedDate='<%# Bind("QPG_STARTDATE") %>' ID="rdStart"
                                                Width="200px" runat="server">
                                                <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                                </Calendar>
                                                <DateInput ID="dtStart" DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" runat="server">
                                                </DateInput>
                                                <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                                            </telerik:RadDateTimePicker>
                                            <itemstyle horizontalalign="Left"></itemstyle>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Date">
                                        <ItemTemplate>
                                            <telerik:RadDateTimePicker ID="rdEnd" Width="200px" SelectedDate='<%# Bind("QPG_ENDDATE") %>'
                                                runat="server">
                                                <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                                </Calendar>
                                                <DateInput ID="dtEnd" DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" runat="server">
                                                </DateInput>
                                                <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                                            </telerik:RadDateTimePicker>
                                            <itemstyle horizontalalign="Left"></itemstyle>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <asp:Button ID="btnReleasetoClass" runat="server" Text="Save" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <HeaderTemplate>
                Release to Class
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="tpReleaseStudent" runat="server">
            <ContentTemplate>
                <table width="100%"  cellpadding="5" cellspacing="0"
                   >
                    <tr>
                        <td colspan="7">
                          <i><span style="font-size:14px;">Note : Please select a class or group</span></i> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                         <span class="field-label">   Select Section</span> 
                        </td>
                       
                        <td align="left">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                        </td>
                        <td>
                           <span class="field-label"> Select Group</span>
                        </td>
                        
                        <td colspan="2" align="left">
                            <asp:DropDownList ID="ddlReleaseGroup" runat="server" AutoPostBack="false">
                            </asp:DropDownList>
                        </td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <span class="field-label">  Release Start Date</span> 
                        </td>
                      
                        <td>
                            <telerik:RadDateTimePicker ID="rtStartStudent" Width="200px" runat="server" Culture="English (United States)">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" DisplayDateFormat="dd/MMM/yyyy HH:MM tt"
                                    LabelWidth="64px" Width="">
                                </DateInput>
                                <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                            </telerik:RadDateTimePicker>
                        </td>
                        <td>
                         <span class="field-label">   Release End Date</span>
                        </td>
                       
                        <td>
                            <telerik:RadDateTimePicker ID="rtEndStudent" Width="200px" runat="server" Culture="English (United States)">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" DisplayDateFormat="dd/MMM/yyyy HH:MM tt"
                                    LabelWidth="64px" Width="">
                                </DateInput>
                                <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                            </telerik:RadDateTimePicker>
                        </td>
                        <td>
                            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center">
                            <asp:GridView ID="gvReleasetoStudent" runat="server" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="objid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbQsrId" runat="server" Text='<%# Bind("QSR_ID") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="objid" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Release">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        Select
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRelease" Checked='<%# Bind("QSR_bRELEASE") %>' runat="server">
                                            </asp:CheckBox></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Date">
                                        <ItemTemplate>
                                            <telerik:RadDateTimePicker SelectedDate='<%# Bind("QSR_STARTDATE") %>' ID="rdStart"
                                                Width="200px" runat="server">
                                                <Calendar ID="Calendar2" runat="server">
                                                </Calendar>
                                                <DateInput ID="dtStart" DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" runat="server">
                                                </DateInput>
                                                <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                                            </telerik:RadDateTimePicker>
                                            <itemstyle horizontalalign="Left"></itemstyle>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Date">
                                        <ItemTemplate>
                                            <telerik:RadDateTimePicker ID="rdEnd" Width="200px" SelectedDate='<%# Bind("QSR_ENDDATE") %>'
                                                runat="server">
                                                <Calendar ID="Calendar2" runat="server">
                                                </Calendar>
                                                <DateInput ID="dtEnd" DateFormat="dd/MMM/yyyy HH:MM tt" ToolTip="Date input" runat="server">
                                                </DateInput>
                                                <TimeView StartTime="07:00:00" EndTime="23:15:00" Interval="0:15:0" Columns="6" />
                                            </telerik:RadDateTimePicker>
                                            <itemstyle horizontalalign="Left"></itemstyle>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="trSave" runat="server">
                        <td colspan="7">
                            <asp:Button ID="btnReleasetoStudent" runat="server" Text="Save" CssClass="button"
                                TabIndex="4" Width="51px" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <HeaderTemplate>
                Release to Students
            </HeaderTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:HiddenField ID="h_QPM_ID" runat="server" />
    <asp:HiddenField ID="h_SBM_ID" runat="server" />
    <asp:HiddenField ID="h_GRD_ID" runat="server" />
    <asp:HiddenField ID="h_QPD_ID" runat="server" />
    <asp:HiddenField ID="h_Topic_ID" runat="server" />
                </div> 
             </div> 
         </div> 
</asp:Content>
