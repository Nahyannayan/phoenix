﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="qbTopic_M.aspx.vb" Inherits="QuestionBank_qbTopic_M" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
<script language="javascript" type="text/javascript">
    function GetTopics1() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var type;
        var sbmId;
        var grdId;
        sbmId = document.getElementById('<%=h_SBM_ID.ClientID %>').value;
        grdId = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
        //alert(syllabusId);
      <%--  result = window.showModalDialog("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", sFeatures)
        if (result != "" && result != "undefined") {
            NameandCode = result.split('||');
            document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
        }
        return false;--%>
        var oWnd = radopen("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "pop_topic");
    }
    function OnClientClose3(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {
            NameandCode = arg.Topic.split('||');
            document.getElementById('<%=txtTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_Topic_ID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtTopic.ClientID%>', 'TextChanged');
            }
        }

    function GetTopics2() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var type;
        var sbmId;
        var grdId;
        sbmId = document.getElementById('<%=h_SBM_ID.ClientID %>').value;
        grdId = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
        //alert(syllabusId);
        <%--result = window.showModalDialog("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "", sFeatures)
        if (result != "" && result != "undefined") {
            NameandCode = result.split('||');
            document.getElementById('<%=txtParentTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_ParentTopic_ID.ClientID %>').value = NameandCode[0];
        }
        return false;--%>
        var oWnd = radopen("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "pop_topic2");
    }
    function OnClientClose2(oWnd, args) {
        //get the transferred arguments

        var arg = args.get_argument();

        if (arg) {
            NameandCode = arg.Topic.split('||');
            document.getElementById('<%=txtParentTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_ParentTopic_ID.ClientID %>').value = NameandCode[0];
                __doPostBack('<%= txtParentTopic.ClientID%>', 'TextChanged');
            }
        }
    
    function openWin() {
        var sFeatures;
        sFeatures = "dialogWidth: 729px; ";
        sFeatures += "dialogHeight: 445px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var sbmId;
        var grdId;
        sbmId = document.getElementById('<%=h_SBM_ID.ClientID %>').value;
        grdId = document.getElementById('<%=h_GRD_ID.ClientID %>').value;
        var oWnd = radopen("qbshowTopics.aspx?sbmId=" + sbmId + "&grdId=" + grdId, "RadWindow1");
                 

    }
    function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.Topic.split('||');
            document.getElementById('<%=txtParentTopic.ClientID %>').value = NameandCode[1];
            document.getElementById('<%=h_ParentTopic_ID.ClientID %>').value = NameandCode[0];
            <%-- document.getElementById("<%=btnCheck.ClientID %>").click();--%>
              __doPostBack('<%= txtParentTopic.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
</script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_topic" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose3" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
         <Windows>
            <telerik:RadWindow ID="pop_topic2" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose2" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>          
        </Windows>
    </telerik:RadWindowManager>    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server" Text="Topic Master"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
    <table id="tbl_AddGroup" runat="server" align="center"  cellpadding="0"
        cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" ></asp:Label>
                <asp:Button ID="btnCheck" style="display:none;"  runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table2" runat="server" align="center" width="100%">
                   
                    <tr>
                        <td  align="left" width="20%">
                            <span class="field-label">Select Grade</span>
                        </td>
                        
                        <td  align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server"  
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td  align="left" width="20%">
                            <span class="field-label">Subject</span>
                        </td>
                        
                        <td  align="left" width="30%">
                            <asp:DropDownList ID="ddlSubject" runat="server" 
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td  align="left">
                            <span class="field-label">Topic</span>
                        </td>
                       
                          
                            <td align="left" >
                                <asp:TextBox ID="txtTopic" runat="server" ></asp:TextBox>
                                <asp:ImageButton ID="imgTopic" runat="server" ImageUrl="../Images/cal.gif" OnClientClick="GetTopics1(); return false;">
                                </asp:ImageButton>
                            </td>
                            <td  align="left">
                               <span class="field-label"> Parent Topic</span>
                            </td>
                            
                           
                                <td align="left"   >
                                    <asp:TextBox ID="txtParentTopic" runat="server"  Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="imgParentTopic" runat="server" 
                                        ImageUrl="../Images/cal.gif" OnClientClick="GetTopics2(); return false;">
                                    </asp:ImageButton>
                                </td>
                   
                        
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1"
                                TabIndex="7" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:GridView ID="gvTopic" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                 CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records.
                                 Kindly try with some other keywords." PageSize="20">
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="objid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTopId" runat="server" Text='<%# Bind("TOP_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Skill">
                                        <ItemTemplate>
                                            <asp:Label ID="txtTopic" runat="server"  Text='<%# Bind("TOP_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                            
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEditH" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text="Delete"></asp:LinkButton>
                                             <ajaxToolkit:ConfirmButtonExtender ID="c2" TargetControlID="lnkDelete" 
                                    ConfirmText="Topic will be deleted permanently.Are you sure you want to continue? " runat="server" >
                                    </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <asp:HiddenField ID="h_SBM_ID" runat="server" />
                            <asp:HiddenField ID="h_GRD_ID" runat="server" />
                            <asp:HiddenField ID="h_Topic_ID" runat="server" />
                            <asp:HiddenField ID="h_ParentTopic_ID" runat="server" />
                            <br />
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
    </table>
                </div>
            </div>
        </div>
</asp:Content>
