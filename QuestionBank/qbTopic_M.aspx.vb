﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class QuestionBank_qbTopic_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C700015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Topic_ID.Value = "0"
                    h_ParentTopic_ID.Value = "0"

                    BindGrade()
                    BindSubject()
                    GridBind()
                    gvTopic.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If


    End Sub




#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M  " _
                                  & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
                                  & " WHERE GRM_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        h_GRD_ID.Value = ddlGrade.SelectedValue.ToString
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE " _
                                & " SBG_ACD_ID=" + Session("CURRENT_ACd_ID") + " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SBG_DESCR NOT IN('Theory','Pract.')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
        h_SBM_ID.Value = ddlSubject.SelectedValue.ToString
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT TOP_ID,ISNULL(TOP_DESCR,'') TOP_DESCR FROM " _
                                & " QST.TOPIC_M WHERE " _
                                & " TOP_BSU_ID=" + Session("SBSUID") _
                                & " AND TOP_SBM_ID='" + ddlSubject.SelectedValue.ToString + "' AND TOP_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvTopic.DataSource = ds
        gvTopic.DataBind()
    End Sub

    Sub SaveData(ByVal mode As String)

        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String

        str_query = "exec [QST].[saveTOPIC_M] " _
       & " @TOP_ID=" + h_Topic_ID.Value + "," _
       & " @TOP_BSU_ID='" + Session("SBSUID") + "'," _
       & " @TOP_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
       & " @TOP_SBM_ID='" + ddlSubject.SelectedValue.ToString + "'," _
       & " @TOP_DESCR='" + txtTopic.Text + "'," _
       & " @TOP_PARENT_ID='" + h_ParentTopic_ID.Value + "'," _
       & " @MODE='" + mode + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        lblError.Text = "Record Saved Successfully"
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblTopId As Label = TryCast(sender.FindControl("lblTopId"), Label)
            h_Topic_ID.Value = lblTopId.Text
            SaveData("delete")
            h_Topic_ID.Value = 0
            txtTopic.Text = ""
            h_ParentTopic_ID.Value = 0
            txtParentTopic.Text = ""
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#End Region


    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        h_GRD_ID.Value = ddlGrade.SelectedValue.ToString
        BindSubject()
        GridBind()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        h_SBM_ID.Value = ddlSubject.SelectedValue.ToString
        GridBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If h_Topic_ID.Value = "0" Then
            SaveData("add")
        Else
            SaveData("edit")
        End If
        GridBind()
    End Sub
End Class
