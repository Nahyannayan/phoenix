Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class QuestionBank_qbshowTopics
    Inherits System.Web.UI.Page

    Protected Sub tvTopic_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvTopic.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
      ByVal parentNode As TreeNode)

        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_Sql As String

        str_Sql = "SELECT  TOP_ID,TOP_DESCR FROM QST.TOPIC_M WHERE TOP_BSU_ID='" + Session("SBSUID") + "' AND TOP_PARENT_ID=" + parentid + " ORDER BY TOP_DESCR "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)
    End Sub

    Private Sub PopulateRootLevel()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_Sql As String
        tvTopic.Nodes.Clear()

        str_Sql = "SELECT TOP_ID,TOP_DESCR FROM QST.TOPIC_M WHERE TOP_PARENT_ID=0 AND " _
                 & " TOP_BSU_ID='" + Session("SBSUID") + "' AND " _
                 & " TOP_SBM_ID=" + h_SBM_ID.Value _
                 & " AND TOP_GRD_ID='" + h_GRD_ID.Value + "' ORDER BY TOP_DESCR "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvTopic.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("TOP_DESCR").ToString()
            tn.Value = dr("TOP_ID").ToString().Trim()
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = True
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        tvTopic.Nodes.Clear()
        PopulateRootLevel()
        tvTopic.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            h_SBM_ID.Value = Request.QueryString("sbmId")
            h_GRD_ID.Value = Request.QueryString("grdId")
            'h_TermId.Value = Request.QueryString("TermId")
            PopulateTree()
        End If
    End Sub

    Private Sub CloseWndow()
        ' Dim javaSr As String
        'javaSr = "<script language='javascript'> function listen_window(){"
        'javaSr += "window.returnValue = '" & tvTopic.SelectedNode.Value & "||" & tvTopic.SelectedNode.Text.Replace("'", "\'") & "';"
        'javaSr += "window.close();"
        'javaSr += "} </script>"
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.Topic ='" & tvTopic.SelectedNode.Value & "||" & tvTopic.SelectedNode.Text.Replace("'", "\'") & "';")
        Response.Write("var oWnd = GetRadWindow('" & tvTopic.SelectedNode.Value + "||" + tvTopic.SelectedNode.Text & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
        ' Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
    End Sub

    Protected Sub tvTopic_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CloseWndow()
    End Sub
End Class
