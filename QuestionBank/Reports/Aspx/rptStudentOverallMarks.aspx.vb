Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports GemBox.Spreadsheet
Imports System.IO

Partial Class QuestionBank_Reports_Aspx_rptStudentOverallMarks
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C710005" And ViewState("MainMnu_code") <> "C710015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), ddlBsu.SelectedValue.ToString)
                    BindGrade()
                    BindSubject()
                    BindQuestionPaper()
                    BindSection()

                    If Session("sbsuid") <> "111001" Then
                        trbsu.Visible = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                '  lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReport)

    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_SHORTNAME FROM BUSINESSUNIT_M WHERE BSU_ID IN("

        If Session("SBSUID") = "111001" Then
            str_query += "'121012','121013','123006','121014','131001','131002','141001','151001','111001'"
        Else
            str_query += "'" + Session("SBSUID") + "'"
        End If
        str_query += ") ORDER BY BSU_SHORTNAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_SHORTNAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()

        If Session("SBSUID") = "111001" Then
            ddlBsu.Items.FindByValue("111001").Selected = True
        End If
    End Sub
    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M  " _
                                  & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
                                  & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub BindSubject()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_DESCR,SBG_SBM_ID FROM SUBJECTS_GRADE_S WHERE " _
                                & " SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND SBG_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                & " AND SBG_DESCR NOT IN('Theory','Pract.')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_SBM_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindQuestionPaper()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String = "SELECT QPM_ID,QPM_DESCR,QPM_SBM_ID,CASE WHEN CREATEDSCHOOL='' THEN ISNULL(EMP_DISPLAYNAME,ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')) ELSE CREATEDSCHOOL END EMP_NAME,SBG_DESCR, " _
                                & " CASE ISNULL(bOTHERSCHOOL,0) WHEN 0 THEN 'TRUE' ELSE 'FALSE' END bPAPER FROM [QSP].[FN_QUESTIONPAPER_M](" + ddlAcademicYear.SelectedValue.ToString + ",'" + ddlGrade.SelectedValue.ToString + "') INNER JOIN OASIS..EMPLOYEE_M ON QPM_EMP_ID=EMP_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S ON QPM_SBM_ID=SBG_SBM_ID AND QPM_ACD_ID=SBG_ACD_ID AND QPM_GRD_ID=SBG_GRD_ID  "
        If ddlSubject.SelectedValue <> "0" Then
            str_query += " AND QPM_SBM_ID=" + ddlSubject.SelectedValue.ToString
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlQuestionpaper.DataSource = ds
        ddlQuestionpaper.DataTextField = "QPM_DESCR"
        ddlQuestionpaper.DataValueField = "QPM_ID"
        ddlQuestionpaper.DataBind()
    End Sub

    Sub BindSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SCT_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub

    Sub ExportToExcel()
        Dim str_conn As String = ConnectionManger.GetOASIS_QBConnectionString
        Dim str_query As String

        If ViewState("MainMnu_code") = "C710005" Then
            str_query = "EXEC QSP.RPTSTUDENTOVERALLMARKS " _
                     & ddlQuestionpaper.SelectedValue.ToString + "," _
                     & ddlAcademicYear.SelectedValue.ToString + "," _
                     & "'" + ddlGrade.SelectedValue.ToString + "'," _
                     & ddlSection.SelectedValue.ToString
        Else
            str_query = "EXEC QSP.RPTSTUDENTDETAILEDMARKS " _
                         & ddlQuestionpaper.SelectedValue.ToString + "," _
                         & ddlAcademicYear.SelectedValue.ToString + "," _
                         & "'" + ddlGrade.SelectedValue.ToString + "'," _
                         & ddlSection.SelectedValue.ToString
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable

        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()


        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        '' HttpContext.Current.Response.Flush()
        ''HttpContext.Current.Response.Close()
        'HttpContext.Current.Response.End()


        System.IO.File.Delete(tempFileName)
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSubject()
        BindQuestionPaper()
        BindSection()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSubject()
        BindQuestionPaper()
        BindSection()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindQuestionPaper()
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        ExportToExcel()
    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), ddlBsu.SelectedValue.ToString)
        BindGrade()
        BindSubject()
        BindQuestionPaper()
        BindSection()
    End Sub
End Class
