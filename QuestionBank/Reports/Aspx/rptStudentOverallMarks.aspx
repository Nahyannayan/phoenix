<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptStudentOverallMarks.aspx.vb" Inherits="QuestionBank_Reports_Aspx_rptStudentOverallMarks"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Question Paper
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblClm" runat="server" align="center" width="100%"
                    cellpadding="5" cellspacing="0">

<%--                    <tr>
                        <td align="left"  colspan="4">
                            <asp:LinkButton ID="lnkAdd" Text="Add New" runat="server"></asp:LinkButton>
                        </td>
                    </tr>--%>
                    <tr id="trbsu" runat="server">
                        <td align="left" width="20%" ><span class="field-label">Select Business Unit</span></td>

                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>
                        <td align="left" width="20%" ><span class="field-label">Select Academic Year</span></td>

                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Grade</span></td>

                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" >
                            </asp:DropDownList></td>
                        <td align="left" width="20%" ><span class="field-label">Select Subject</span>
                        </td>

                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlSubject" AutoPostBack="true" runat="server" >
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" width="20%" ><span class="field-label">Select QuestionPaper</span>
                        </td>

                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlQuestionpaper" runat="server" >
                            </asp:DropDownList></td>
                        <td align="left" width="20%" ><span class="field-label">Select Section</span></td>

                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlSection" runat="server" >
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="center"  colspan="4">
                            <asp:Button ID="btnReport" runat="server" CssClass="button" Text="Generate Report"
                                 />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
