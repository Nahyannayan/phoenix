﻿Imports system.io
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Partial Class QuestionBank_uploadImage
    Inherits System.Web.UI.Page
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim fileName As String = hdnImageFileName1.Value.Substring(hdnImageFileName1.Value.LastIndexOf("\") + 1)
        ' ImageButton1.ImageUrl = Server.MapPath("Upload") + "\" + fileName
        'ImageButton1.ImageUrl = "~/QuestionBank/Upload/" + hfUsr.Value.TrimStart.TrimEnd + fileName.TrimStart.TrimEnd
        Session("qImage") = "../QuestionBank/Upload/" + fileName
        ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "close", "window.close();", True)
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        ' File.Copy(Server.MapPath("~/QuestionBank/Upload/"), fuUploadPhoto.FileName)
        ' ImageButton1.ImageUrl = "~/QuestionBank/Upload/studImage.jpg)"
        Dim strFilePath As String = Now.ToString.Replace("/", "").Replace(":", "").Replace(" ", "") + ".jpg"
        fuUploadPhoto.PostedFile.SaveAs(Server.MapPath("../QuestionBank/Upload/" + strFilePath))
        ' ImageButton1.ImageUrl = "~/QuestionBank/Upload/" + strFilePath
        Session("qImage") = "../QuestionBank/Upload/" + strFilePath
        ReduceImage(Server.MapPath("../QuestionBank/Upload/" + strFilePath), 400, 200)
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write("var oWnd = GetRadWindow('" & strFilePath & "');")
        Response.Write("oWnd.close();")
        Response.Write(" } </script>")
        '  ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "close", "window.close();", True)
    End Sub

    Sub ResizeImage(ByVal originalFile As String)
        Dim w As Integer = 300
        Dim h As Integer = 100
        Dim X As Integer = 0
        Dim y As Integer = 0
        Using img As Image = Image.FromFile(originalFile)
            If img.Width > 300 And img.Height > 100 Then

                Using _bitmap As New System.Drawing.Bitmap(w, h)
                    _bitmap.SetResolution(img.HorizontalResolution, img.VerticalResolution)
                    Using _graphic As Graphics = Graphics.FromImage(_bitmap)
                        _graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
                        _graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality
                        _graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality
                        _graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality
                        _graphic.DrawImage(img, 0, 0, w, h)
                        _graphic.DrawImage(img, New Rectangle(0, 0, w, h), X, y, w, h, _
                        GraphicsUnit.Pixel)
                        Dim extension As String = Path.GetExtension(originalFile)
                        Using encoderParameters As New EncoderParameters(1)
                            encoderParameters.Param(0) = New EncoderParameter(Encoder.Quality, 100L)
                            _bitmap.Save("C:\Application\OasisSource\QuestionBank\Upload\test123.gif", GetImageCodec(extension), encoderParameters)
                        End Using

                        ' lblCroppedImage.Text = String.Format("<img src='{0}' alt='Cropped image'>", strPath)
                    End Using
                End Using
            End If
        End Using
    End Sub

    Sub ReduceImage(ByVal fullpath As String, ByVal WIDTH As Integer, ByVal HEIGHT As Integer)
            Dim oBitmap As Bitmap
        oBitmap = New Bitmap(fullpath)
        If oBitmap.Height > HEIGHT Or oBitmap.Width > WIDTH Then
            Dim bmpNew As Bitmap = New Bitmap(WIDTH, HEIGHT)
            If bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format1bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format4bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format8bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Undefined Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.DontCare Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppArgb1555 Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppGrayScale Then
                Throw New NotSupportedException("Pixel format of the image is not supported.")
            End If

            Dim oGraphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bmpNew)
            oGraphic.SmoothingMode = Drawing.Drawing2D.SmoothingMode.HighQuality
            oGraphic.InterpolationMode = Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
            oGraphic.DrawImage(oBitmap, New Rectangle(0, 0, bmpNew.Width, bmpNew.Height), 0, 0, oBitmap.Width, oBitmap.Height, GraphicsUnit.Pixel)
            oGraphic.Dispose()
            oBitmap.Dispose()
            oBitmap = bmpNew

            Dim oBrush As New SolidBrush(Color.Black)
            Dim ofont As New Font("Arial", 8)
            oGraphic.Dispose()
            ofont.Dispose()
            oBrush.Dispose()

            File.Delete(fullpath)
            oBitmap.Save(fullpath)
            oBitmap.Dispose()
        Else
            oBitmap.Dispose()
        End If
    End Sub

    Public Shared Function GetImageCodec(ByVal extension As String) As ImageCodecInfo
        extension = extension.ToUpperInvariant()
        Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
        For Each codec As ImageCodecInfo In codecs
            If codec.FilenameExtension.Contains(extension) Then
                Return codec
            End If
        Next
        Return codecs(1)
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Not Page.IsPostBack Then

        End If
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Button1)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Button2)
    End Sub

End Class
