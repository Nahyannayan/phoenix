<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="AXMigrationDataTemplate.aspx.vb" Inherits="AXMigrationDataTemplate"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="../UserControls/usrBSUnits.ascx" TagName="usrBSUnits" TagPrefix="uc1" %>
<%@ Register Src="../Asset/UserControls/usrDatePicker.ascx" TagName="usrDatePicker" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <table align="center" style="width: 50%">
        <tr align="left">
            <td style="width: 557px">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                <asp:Label ID="lblError" runat="server" CssClass="error" Width="600px" Style="word-wrap: break-word;"></asp:Label>
            </td>
        </tr>
        <tr align="left">
            <td style="width: 557px">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 100%;">
                    <tr>
                        <td align="left" style="height: 1px" valign="middle">
                            <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0">
                                <ajaxToolkit:TabPanel ID="HT1" runat="server">
                                    <ContentTemplate>
                                        <div style="height: 300px; width: 500px; overflow: auto">
                                            <table cellpadding="5" cellspacing="5" border="1" style="width: 100%; border-collapse: collapse">
                                                <tr id="trBusinessUnit1" runat="server">
                                                    <td class="subheader_img" colspan="3" height="22" runat="server">Create Position
                                                    </td>

                                                </tr>
                                                <tr id="trBusinessUnit2" runat="server">
                                                    <td align="left" class="matters" runat="server">Business Unit
                                                    </td>
                                                    <td align="center" class="matters" runat="server">:
                                                    </td>
                                                    <td align="left" class="matters" runat="server">

                                                        <asp:CheckBoxList ID="Bsu_Create" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" CssClass="checkbox">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </tr>
                                                <tr id="tr8" runat="server">
                                                    <td align="left" class="matters" runat="server">Starting Position
                                                    </td>
                                                    <td align="center" class="matters" runat="server">:
                                                    </td>
                                                    <td align="left" class="matters" runat="server">

                                                        <asp:TextBox ID="txtPosition" runat="server" CssClass="checkbox">
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trBusinessUnit3" runat="server">

                                                    <td align="left" class="matters" runat="server" colspan="3">&nbsp;<asp:Button ID="btnCreatePosition" CssClass="button" runat="server" Text="Create" />
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHdrObjectives" runat="server" Text="Create Position"></asp:Label>
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="HT2" runat="server">
                                    <ContentTemplate>
                                        <div style="height: 300px; width: 500px; overflow: auto">

                                            <table cellpadding="5" cellspacing="5" border="1" style="width: 100%; border-collapse: collapse">
                                                <tr id="tr1" runat="server">
                                                    <td class="subheader_img" colspan="3" height="22">Delete Position
                                                    </td>

                                                </tr>
                                                <tr id="tr2" runat="server">
                                                    <td align="left" class="matters">Business Unit
                                                    </td>
                                                    <td align="center" class="matters">:
                                                    </td>
                                                    <td align="left" class="matters">

                                                        <asp:CheckBoxList ID="Bsu_Delete" RepeatColumns="10" RepeatDirection="Horizontal" runat="server" CssClass="checkbox">
                                                        </asp:CheckBoxList>

                                                    </td>
                                                </tr>
                                                <tr id="tr3" runat="server">

                                                    <td align="left" colspan="3">
                                                        <asp:Button ID="btnDeletePosition" CssClass="button" runat="server" Text="Delete" />
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHdrTargets" runat="server" Text="Delete Position"></asp:Label>
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="HT3" runat="server">
                                    <ContentTemplate>
                                        <div style="height: 300px; width: 600px; overflow: auto">

                                            <table cellpadding="5" cellspacing="5" border="1" style="width: 100%; border-collapse: collapse">
                                                <tr id="tr4" runat="server">
                                                    <td class="subheader_img" colspan="3" height="22">Download Employee Details
                                                    </td>
                                                </tr>
                                                <tr id="tr5" runat="server">
                                                    <td align="left" class="matters">Business Unit
                                                    </td>
                                                    <td align="center" class="matters">:
                                                    </td>
                                                    <td align="left" class="matters">
                                                        <asp:CheckBoxList ID="Bsu_Download" RepeatColumns="10" RepeatDirection="Horizontal" runat="server" CssClass="checkbox">
                                                        </asp:CheckBoxList>

                                                    </td>
                                                </tr>
                                                <tr id="tr7" runat="server">
                                                    <td align="left" class="matters">&nbsp; Template</td>
                                                    <td align="center" class="matters">:</td>
                                                    <td align="left" class="matters">
                                                        <asp:DropDownList ID="ddlTemplates" runat="server" Height="22px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="tr6" runat="server">
                                                    <td colspan="3">
                                                        <asp:Button ID="btnDownload" CssClass="button" runat="server" Text="Download" />
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        Download Employee Details
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                                <ajaxToolkit:TabPanel ID="HT4" runat="server" Visible="true">
                                    <ContentTemplate>
                                        <div style="height: 300px; width: 600px; overflow: auto">

                                            <table cellpadding="5" cellspacing="5" border="1" style="width: 100%; border-collapse: collapse">
                                                <tr id="tr9" runat="server">
                                                    <td class="subheader_img" colspan="6" height="22">Download Applicant Details
                                                    </td>
                                                </tr>

                                                <tr id="tr11" runat="server">
                                                    <td align="left" class="matters">&nbsp; Template</td>
                                                    <td align="center" class="matters">:</td>
                                                    <td align="left" class="matters">
                                                        <asp:DropDownList ID="ddlApplTemplate" runat="server" Height="22px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td align="left" class="matters">Start Date</td>
                                                    <td align="center" style="width: 2px; height: 21px" class="matters">:</td>
                                                    <td align="left" class="matters">
                                                        <uc1:usrDatePicker ID="usrDPAttStDt" runat="server" />
                                                    </td>
                                                    <td align="left" class="matters">End Date</td>
                                                    <td align="center" style="width: 2px; height: 21px" class="matters">:</td>
                                                    <td align="left" class="matters">
                                                        <uc1:usrDatePicker ID="usrDPAttEndDt" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr id="tr12" runat="server" class="matters">
                                                    <td colspan="6">
                                                        <asp:Button ID="btnDownloadAppl" CssClass="button" runat="server" Text="Download" />
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>
                                    </ContentTemplate>
                                    <HeaderTemplate>
                                        Download Applicant Details
                                    </HeaderTemplate>
                                </ajaxToolkit:TabPanel>
                            </ajaxToolkit:TabContainer>

                        </td>
                    </tr>
                    <%--<tr id ="trSelDepartment" >
            <td align="left" class="matters" colspan="3" valign="top">
                <asp:ImageButton ID="imgPlusDepartment" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                Select Department Filter</td>
        </tr>--%><%--<tr id ="trDepartment"  style="display:none;">--%>
                    <%-- <tr id ="trSelRole" >
            <td align="left" class="matters" colspan="3" valign="top">
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchDepartment();return false;" CausesValidation="False" />
                Select Role Filter</td>
        </tr>--%><%--<tr id ="trRole"  style="display:none;">--%>
                </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_DEPTID" runat="server" />
                <asp:HiddenField ID="h_ROLEID" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />
            </td>
        </tr>
    </table>
    <input id="hfDepartment" runat="server" type="hidden" />
</asp:Content>
