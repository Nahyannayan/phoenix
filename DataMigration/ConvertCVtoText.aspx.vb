﻿Imports System
Imports System.Linq
Imports System.Text
Imports GemBox.Document
Imports GemBox.Document.Tables
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Partial Class DataMigration_ConvertCVtoText
    Inherits BasePage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


        ' If using Professional version, put your serial key below.
      
        UpdateCVtext()

    End Sub

    Sub UpdateCVtext()
        Dim str_conn As String = "Data Source=10.100.150.16\HRCOASISINST01;Initial Catalog=OASIS;Persist Security Info=True;User ID=oasis_app;Password=xf6mt"
        Dim str_query = "SELECT TOP 2000 REGID,ISNULL(RESUME_NAME,'') FROM OASIS_HR_V2_NEW3..USER_RESUME INNER JOIN DATAMIGRATION_NEW..AX_APPLICANTS ON REGID=ApplicantID WHERE CVTEXT IS NULL "
        Dim strQuery1 As String
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim i As Integer
        Dim resumename As String
        Dim ApplicantId As String
        Dim cvText, cvPath As String
        For i = 0 To ds.Tables(0).Rows.Count - 1
            ApplicantId = ds.Tables(0).Rows(i).Item(0)
            resumename = ds.Tables(0).Rows(i).Item(1)
            If resumename = "" Then
                cvText = ""
            Else
                cvPath = "\\172.25.26.20\oasisfiles\hr_files\Resume\" + resumename
                Try
                    cvText = GetCVText(cvPath)
                Catch
                    cvText = ""
                End Try
               
            End If
            strQuery1 = "UPDATE  DATAMIGRATION_NEW..AX_APPLICANTS SET CVTEXT=N'" + cvText.Replace("'", "''") + "' WHERE ApplicantID=" + ApplicantId
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery1)
        Next
    End Sub

    Function GetCVText(cvPath As String) As String
        ComponentInfo.SetLicense("DH1L-PVDJ-TLGW-LD76")
        Dim document As DocumentModel
        Try
            document = DocumentModel.Load(cvPath)
        Catch
            document = DocumentModel.Load(cvPath + "x")
        End Try
        Dim sb As New StringBuilder()
        For Each paragraph As Paragraph In document.GetChildElements(True, ElementType.Paragraph)
            For Each run As Run In paragraph.GetChildElements(True, ElementType.Run)
                Dim isBold As Boolean = run.CharacterFormat.Bold
                Dim text As String = run.Text
                sb.Append(text)
            Next
            sb.AppendLine()
        Next

        Return sb.ToString
    End Function

End Class
