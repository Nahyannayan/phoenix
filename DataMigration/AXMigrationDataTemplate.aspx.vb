Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports ResponseHelper
Imports GemBox.Spreadsheet
'Imports Docs.Excel
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Spreadsheet
Imports SpreadsheetLight


Partial Class AXMigrationDataTemplate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetExpires(Now.AddSeconds(-1))
        'Response.Cache.SetNoStore()
        'Response.AppendHeader("Pragma", "no-cache")

        'tr_AsOnDate.Visible = False
        'tr_ToDate.Visible = False

        Try

            If Page.IsPostBack = False Then
                'txtUser.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                'Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME.ToLower = "pwc_guest" Then
                    tabPopup.Tabs(0).Visible = False
                    tabPopup.Tabs(1).Visible = False
                End If
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'UsrBSUnits1.MenuCode = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then ' Or (ViewState("MainMnu_code") <> "S200477" And ViewState("MainMnu_code") <> "LB20477" And ViewState("MainMnu_code") <> "H020477" And ViewState("MainMnu_code") <> "IR11025" And ViewState("MainMnu_code") <> "TP00477" And ViewState("MainMnu_code") <> "ER20477") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If Session("USR_MIS") <> "2" Then
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    End If
                End If
                Me.LoadBsu()
                Me.LoadMigrationTemplates()
            Else
                'FillDeptNames(h_DEPTID.Value)
                'FillRoleNames(h_ROLEID.Value)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadAppl)

    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Private Sub LoadBsu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Try
            'load business unit list for creation & download
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "oasis_dax.[AXMIGRATION].[GET_MIGRATION_BSU] 'C'")
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.Bsu_Create.DataValueField = "BSU_ID"
                Me.Bsu_Create.DataTextField = "BSU"
                Me.Bsu_Create.DataSource = ds.Tables(0)
                Me.Bsu_Create.DataBind()

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows(i).Item("bCreatePosition") = 0 Then
                        Me.Bsu_Create.Items(i).Enabled = False
                    End If
                Next

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "oasis_dax.[AXMIGRATION].[GET_MIGRATION_BSU] 'DL'")


            Me.Bsu_Download.DataValueField = "BSU_ID"
            Me.Bsu_Download.DataTextField = "BSU"
            Me.Bsu_Download.DataSource = ds.Tables(0)
            Me.Bsu_Download.DataBind()

            'load business unit list for deletion
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "oasis_dax.[AXMIGRATION].[GET_MIGRATION_BSU] 'D'")
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.Bsu_Delete.DataValueField = "BSU_ID"
                Me.Bsu_Delete.DataTextField = "BSU"
                Me.Bsu_Delete.DataSource = ds.Tables(0)
                Me.Bsu_Delete.DataBind()
            End If
        Catch ex As Exception
            Me.lblError.Text = "Error occured loading business units"
        End Try
    End Sub

    Private Sub LoadMigrationTemplates()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Try
            'load business unit list for creation & download
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "SELECT  SHORTCODE, TEMPLATE FROM OASIS_DAX.AXMIGRATION.MIGRATION_TEMPLATES WHERE ISNULL(bSUPPRESS,0) =0 AND TEMPLATETYPE='EMPLOYEE'  ORDER BY ID")
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.ddlTemplates.DataValueField = "SHORTCODE"
                Me.ddlTemplates.DataTextField = "TEMPLATE"
                Me.ddlTemplates.DataSource = ds
                Me.ddlTemplates.DataBind()
            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, "SELECT  SHORTCODE, TEMPLATE FROM OASIS_DAX.AXMIGRATION.MIGRATION_TEMPLATES WHERE ISNULL(bSUPPRESS,0) =0 AND TEMPLATETYPE='APPLICANT' ORDER BY ID")
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Me.ddlApplTemplate.DataValueField = "SHORTCODE"
                Me.ddlApplTemplate.DataTextField = "TEMPLATE"
                Me.ddlApplTemplate.DataSource = ds
                Me.ddlApplTemplate.DataBind()
            End If

        Catch ex As Exception
            Me.lblError.Text = "Error occured loading migration templates"
        End Try
    End Sub



    Protected Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.Click

        Dim Bsu_Ids As String = ""
        Try
            For Each Item As ListItem In Me.Bsu_Download.Items
                If Item.Selected = True Then
                    Bsu_Ids &= Item.Value & "|"
                End If
            Next
            If Bsu_Ids.EndsWith("|") Then
                Bsu_Ids = Bsu_Ids.TrimEnd("|")
            End If
            lblError.Text = ""
            If Bsu_Ids = "" Then
                lblError.Text = "Please choose a business unit"
                Exit Sub
            Else
                lblError.Text = ""
            End If

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim ds As DataSet

            'Select Case Me.ddlTemplates.SelectedValue
            '    Case "POS"
            '        str_query = "Exec [AXMIGRATION].[GETPOSITIONDETAILS] '" & Bsu_Ids & "'"
            '    Case "RTO"
            '        str_query = "Exec [AXMIGRATION].[GETPOSITIONHEIRARCHY] '" & Bsu_Ids & "'"
            '    Case "EMP"
            '        str_query = "Exec [AXMIGRATION].[GETEMPLOYEEDATA] '" & Bsu_Ids & "'"
            '    Case "EPI"
            '        str_query = "Exec [AXMIGRATION].[GETEMPLOYEEPASSPORTDETAILS] '" & Bsu_Ids & "'"
            '    Case "EBK"
            '        str_query = "Exec [AXMIGRATION].[GETEMPLOYEEACCOUNTDETAILS] '" & Bsu_Ids & "'"
            '    Case "EDD"
            '        str_query = "Exec [AXMIGRATION].[GEREMPLOYEEDEPENDANTS] '" & Bsu_Ids & "'"
            '    Case "DES"
            '        str_query = "EXEC AXMIGRATION.GETEMPLOYEEDESIGNATION"
            '    Case Else
            'End Select

            str_query = "AXMIGRATION.GETDATA " _
                     & " @REPORTTYPE='EMPLOYEE'," _
                     & " @DATATYPE='" + ddlTemplates.SelectedValue + "'," _
                     & " @BSU_IDS='" + Bsu_Ids + "'"


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            Dim dt As DataTable

            dt = ds.Tables(0)

            Dim tempFileName As String = Server.MapPath("~/DataMigration/ExcelDownloads/") + ddlTemplates.SelectedItem.Text.Replace(" ", "") + "_" + Session("sUsr_name") + "_" + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True
            ef.Save(tempFileName)
            Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(tempFileName)
            'HttpContext.Current.Response.Flush()
            'HttpContext.Current.Response.Close()

            'System.IO.File.Delete(tempFileName)
        Catch ex As Exception
            Me.lblError.Text = "Error downloading the data"
        End Try
    End Sub

    Protected Sub btnCreatePosition_Click(sender As Object, e As EventArgs) Handles btnCreatePosition.Click
        lblError.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim bsuids As String
        Dim i As Integer
        For i = 0 To Bsu_Create.Items.Count - 1
            If Bsu_Create.Items(i).Selected = True Then
                If bsuids <> "" Then
                    bsuids += "|"
                End If
                bsuids += Bsu_Create.Items(i).Value
            End If
        Next
        If bsuids = "" Then
            lblError.Text = "Please select a business unit"
            Exit Sub
        End If

        If txtPosition.Text = "" Then
            lblError.Text = "Please select the start position"
            Exit Sub
        End If
        Try
            Dim str_query As String = "exec oasis_dax.[AXMIGRATION].[AUTOCREATEPOSITIONS] '" + bsuids + "','" + txtPosition.Text + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnDeletePosition_Click(sender As Object, e As EventArgs) Handles btnDeletePosition.Click
        lblError.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim bsuids As String
        Dim i As Integer
        For i = 0 To Bsu_Delete.Items.Count - 1
            If Bsu_Delete.Items(i).Selected = True Then
                If bsuids <> "" Then
                    bsuids += "|"
                End If
                bsuids += Bsu_Delete.Items(i).Value
            End If
        Next
        If bsuids = "" Then
            lblError.Text = "Please select a business unit"
            Exit Sub
        End If

        Try
            Dim str_query As String = "exec oasis_dax.[AXMIGRATION].[DELETEPOSITION] '" + bsuids + "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnDownloadAppl_Click(sender As Object, e As EventArgs) Handles btnDownloadAppl.Click

        Dim Bsu_Ids As String = ""
        Try

            lblError.Text = ""

            If usrDPAttStDt.SelectedText = "" Then
                lblError.Text = "Please select the start date"
                Exit Sub
            Else
                lblError.Text = ""
            End If

            If usrDPAttEndDt.SelectedText = "" Then
                lblError.Text = "Please select the end date"
                Exit Sub
            Else
                lblError.Text = ""
            End If

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim ds As DataSet

            str_query = "AXMIGRATION.GETDATA " _
                     & " @REPORTTYPE='APPLICANT'," _
                     & " @DATATYPE='" + ddlApplTemplate.SelectedValue + "'," _
                     & " @BSU_IDS=''," _
                     & " @STARTDATE='" + usrDPAttStDt.SelectedText + "'," _
                     & " @ENDDATE='" + usrDPAttEndDt.SelectedText + "'"


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            Dim dt As DataTable

            dt = ds.Tables(0)
            Dim tempFileName As String = Server.MapPath("~/DataMigration/ExcelDownloads/") + ddlApplTemplate.SelectedItem.Text.Replace(" ", "") + "_" + Session("sUsr_name") + "_" + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
            ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

            ef.Save(tempFileName)
            Dim bytes() As Byte = File.ReadAllBytes(tempFileName)

            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            ' System.IO.File.Delete(tempFileName)
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub
End Class
