﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports Telerik
Partial Class GemsEss_GemsEss
    Inherits System.Web.UI.MasterPage
    Dim userSuper As Boolean
    Dim Encr_decrData As New Encryption64



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        'If Not Request.QueryString("MainMnu_code") Is Nothing Then
        '    If UtilityObj.CheckUSerAccessRights(Session("sBsuid"), Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")),
        '           Session("sModule"), Session("sUsr_name"), Session("sBusper")) <> 0 Then
        '        Response.Redirect("~/login.aspx")
        '    End If
        'End If


        'If Request.QueryString("MainMnu_code") <> "" Then
        '    Session("lastMnuCode") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        'End If

        If Not IsPostBack Then
            Try
                Session("sModule") = "SS"
                Session("Modulename") = "Employee Self Service"
                Page.Title = OASISConstants.Gemstitle
                If Session("sUsr_name") & "" = "" Then
                    Response.Redirect("~/login.aspx")
                End If
                If Session("ReportSel") = "" Or Session("ReportSel") Is Nothing Then
                    Session("ReportSel") = "PDF"
                End If
                myTeamMenuHide()
                BIND_ESS_MENUS(Session("sBsuid"), Session("sroleid"), Session("sModule"), Session("sBusper")) 'Added by vikranth on 6th Feb 2020 
                GetPendingRquestCount()
                bind_Notification()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message + ":Master", System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If


    End Sub
    Public Sub myTeamMenuHide()
        Try
            liTeam.Visible = False
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EmpId", SqlDbType.VarChar)
            pParms(0).Value = Session("EmployeeId")

            pParms(1) = New SqlClient.SqlParameter("@BsuId", SqlDbType.VarChar)
            pParms(1).Value = Session("SbSUID")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_ReportingEmployees_Count", pParms)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item(0).ToString() <> "" Then
                        If Convert.ToInt32(ds.Tables(0).Rows(0).Item(0).ToString()) > 0 Then
                            liTeam.Visible = True
                            notificationIcon1.Visible = True
                            notificationIcon.Visible = True
                        Else liTeam.Visible = False
                            notificationIcon1.Visible = False
                            notificationIcon.Visible = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message + ":Master", System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub lnklogoff_Click(sender As Object, e As EventArgs) Handles lnklogoff.Click
        Dim url As String
        url = String.Format("/PHOENIXBETA/login.aspx?logoff=1")
        'url = String.Format("/login.aspx?logoff=1")
        Session.Clear()
        Response.Redirect(url)
    End Sub


    Protected Sub lnkPhoenixPD_Click(sender As Object, e As EventArgs)
        Dim url As String = String.Empty
        Session("sModule") = "CD"
        url = String.Format("/Phoenixbeta/PDTellal/Tellal_PD_Calendar.aspx?MainMnu_code=GVlkv1cOcok=&datamode=Zo4HhpVNpXc=")
        Response.Redirect(url, False)
    End Sub

    'Added by vikranth on 5th Feb 2020
    Protected Sub lnkPM_Click(sender As Object, e As EventArgs)
        Dim url As String = String.Empty
        Session("sModule") = "CD"
        url = String.Format("/Phoenixbeta/pdp_v2/pdpdashboard.aspx?MainMnu_code=xYNDeT925sY=&datamode=Zo4HhpVNpXc=")
        Response.Redirect(url, False)
    End Sub

    Protected Sub lnkPMSL_Click(sender As Object, e As EventArgs)
        Dim url As String = String.Empty
        Session("sModule") = "CD"
        url = String.Format("/Phoenixbeta/pdp/pdpdashboard_hos.aspx?MainMnu_code=I5YQ9tIhYXo=&datamode=Zo4HhpVNpXc=")
        Response.Redirect(url, False)
    End Sub

    Protected Sub lnkPMHOS_Click(sender As Object, e As EventArgs)
        Dim url As String = String.Empty
        Session("sModule") = "CD"
        url = String.Format("/Phoenixbeta/pdp/pdpdashboard_hos.aspx?MainMnu_code=Tem+lm6bdZw=&datamode=Zo4HhpVNpXc=")
        Response.Redirect(url, False)
    End Sub

    Protected Sub lnkPMNonTeach_Click(sender As Object, e As EventArgs)
        Dim url As String = String.Empty
        Session("sModule") = "CD"
        url = String.Format("/Phoenixbeta/pdp_v2/pdpdashboard.aspx?MainMnu_code=kN3OCspCvDg=&datamode=Zo4HhpVNpXc=")
        Response.Redirect(url, False)
    End Sub
    'End by vikranth on 5th Feb 2020

    'Added by vikranth on 6th Feb 2020 , for menu 
    Private Sub BIND_ESS_MENUS(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal userSuper As Integer)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(6) As SqlParameter
            param(0) = New SqlParameter("@bunit", bunit)
            param(1) = New SqlParameter("@userMod", userMod)
            param(2) = New SqlParameter("@userRol", userRol)
            param(3) = New SqlParameter("@userSuper", userSuper)
            param(4) = New SqlParameter("@order", 3)
            param(5) = New SqlParameter("@emp_id", Session("EmployeeId"))
            Using ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GET_ESS_MENUS", param)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If dr("ESM_MNU_NAME") = "Professional Development" Then
                                LiPD.Visible = True
                            ElseIf dr("ESM_MNU_NAME") = "Performance Management" Then
                                LiPM.Visible = True
                            ElseIf dr("ESM_MNU_NAME") = "Performance Management - School Leadership" Then
                                LiPMSL.Visible = True
                            ElseIf dr("ESM_MNU_NAME") = "Performance Management - HOS" Then
                                LiPMHOS.Visible = True
                            ElseIf dr("ESM_MNU_NAME") = "Performance Management - Non Teaching" Then
                                LiPMNonTeach.Visible = True
                            End If
                        Next
                    End If
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog("From BIND_ESS_MENUS()" + ex.Message, "GemsEssMaster")
        End Try
    End Sub
    'End by vikranth on 6th Feb 2020
    Sub GetPendingRquestCount()
        Try

            Dim ds As DataSet

            Dim str_Sql As String = "exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','Open',''"
            str_Sql = str_Sql & " exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','M',''"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable = ds.Tables(0)
            dt.Merge(ds.Tables(1), True)

            If dt.Rows.Count = 0 Then
                notificationCount1.Visible = False
                notificationCount.Visible = False
            Else notificationCount.Visible = True
                notificationCount1.Visible = True
                notificationCount.InnerText = dt.Rows.Count
                notificationCount1.InnerText = dt.Rows.Count
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message + ":Master", System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub bind_Notification()
        Dim ds As New DataSet

        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString


        Dim sqlString As String = ""
        Dim PARAM(3) As SqlParameter

        PARAM(0) = New SqlParameter("@USERNAME", Session("sUsr_name"))
        PARAM(1) = New SqlParameter("@BSUID", Session("sBsuid"))
        PARAM(2) = New SqlParameter("@MODULE", Session("sModule"))

        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "GET_DOC_PENDING_APPROVALS_LIST", PARAM)
        If ds.Tables(1).Rows.Count > 0 Then
            rpNotification.DataSource = ds.Tables(1)
            rpNotification.DataBind()
        End If

    End Sub
End Class

