﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" MasterPageFile="~/GemsEss/GemsEss.master" CodeFile="FeeConcessionPopup_ESS.aspx.vb" Inherits="GemsEss_FeeConcessionPopup_ESS" EnableEventValidation="false" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script type="text/javascript">

        // Confirm on ApplyConcession
        function ConfirmApplyConcession(id, empid) {
            $("#divalert").css("display", "none");
            $('#h_stu_NO').val(id);
            $('#h_emp_id').val(empid);
            $("#confirmModal").modal('show');

            //if (confirm('Are you sure?')) {
            //    ApplyConcession(id, empid);
            //} else {

            //}

        }

        function hideConfirmModal() {
            $("#confirmModal").modal('hide');
            $('.modal-backdrop').remove();

            //alert($('#h_stu_NO').val());
            //alert($('#h_emp_id').val());

            ApplyConcession($('#h_stu_NO').val(), $('#h_emp_id').val());
        }



        function closeAlertPopup() {
            $("#divalert").css("display", "none");
        }
    </script>


    <style>
        #divalert {
            position: fixed;
            margin: auto;
            left: 40%;
            z-index: 1100;
            top: 9%;
            padding: 8px !important;
        }

        .profile-page .profile-header .profile-image img {
            border-radius: 50%;
            width: 100px;
            border: 3px solid #fff;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
        }
    </style>

    <%--style for calender start--%>
    <style>
        #divalert2, #divalert3 {
            /*position: fixed;
    top: 100px;
    left: 45%;
    z-index: 1000;
    min-width: 200px;*/
            position: fixed;
            margin: auto;
            left: 40%;
            z-index: 1100;
            top: 35%;
            padding: 8px !important;
        }

        * {
            box-sizing: border-box;
        }

        ul {
            list-style-type: none;
        }


        .month {
            /*padding: 70px 25px;*/
            width: 100%;
            /*background: #1abc9c;*/
            text-align: center;
        }

            .month ul {
                margin: 0;
                padding: 0;
            }

                .month ul li {
                    color: white;
                    font-size: 20px;
                    text-transform: uppercase;
                    letter-spacing: 3px;
                }

            .month .prev {
                float: left;
                padding-top: 10px;
            }

            .month .next {
                float: right;
                padding-top: 10px;
            }

        .weekdays {
            margin: 0;
            padding: 0px;
            background-color: #ddd;
            border: 1px solid #ccc;
            width: 280px;
        }

            .weekdays li {
                /*display: inline-block;
                width: 11.5%;
                color: #000;
                text-align: center;*/
                list-style-type: none;
                display: inline-block;
                width: 35px;
                text-align: center;
                font-size: 14px;
                margin: 0px;
                margin-right: 2px;
                color: #000;
                padding: 8px 2px;
                /*border: 1px solid #ccc;*/
            }

        .days {
            /*padding: 10px 0;
            background: #eee;*/
            margin: 0;
            padding: 0;
            border: 1px solid #ccc;
            /*width: 102%;*/
            width: 282px;
        }

            .days li {
                list-style-type: none;
                display: inline-block;
                /*width: 13%;*/
                width: 40px;
                text-align: center;
                /*margin-bottom: 5px;*/
                font-size: 12px;
                color: #000;
                padding: 6px;
                border: 1px solid #ccc;
            }

                .days li .activeli {
                    padding: 9px;
                    background: #1abc9c;
                    color: white !important;
                }

        select.calndrMonth {
            font-size: 80% !important;
        }
        /* Add media queries for smaller screens */
        @media screen and (max-width:720px) {
            .days li {
                width: 40px;
            }

            .weekdays li {
                width: 35px;
            }
        }

        @media screen and (max-width: 420px) {
            .days li {
                width: 40px;
            }

            .weekdays li {
                width: 35px;
            }

            .days li .activeli {
                padding: 2px;
            }
        }

        @media screen and (max-width: 290px) {
            .days li {
                width: 40px;
            }

            .weekdays li {
                width: 35px;
            }
        }

        .fancybox-inner {
            width: 100% !important;
        }
    </style>
    <%--style for calender ends--%>
    <style>
        .alert {
            padding: 0px !important;
        }

        .auto-comp-popup {
            background-color: #ffffff;
            border: 1px solid rgba(0,0,0,0.07);
            overflow: hidden;
            overflow-y: scroll;
            max-height: 250px;
            position: absolute;
            z-index: 1000;
            max-width: 354px;
            min-width: 354px;
        }

        .container .auto-comp-link {
            background-color: transparent;
            border: 0;
            padding: 4px 0;
            display: block;
        }



        /*Calendar css style*/


        .calPrevMth {
            text-decoration: none;
            font-size: 12px;
            padding-left: 10px;
            padding-right: 6px;
            /*background: transparent url(../Images/common/MthPrev.png) no-repeat -1px -1px;*/
            color: black;
            float: left;
            line-height: 23px;
        }

        .calNxtMth {
            text-decoration: none;
            font-size: 12px;
            /*background: transparent url(../Images/common/MthNext.png) no-repeat 15px 0px;*/
            color: black;
            padding-left: 6px;
            padding-right: 10px;
            /*background: transparent url(../Images/common/MthNext.png) no-repeat 30px -1px;*/
            float: right;
            line-height: 23px;
        }

        .calMthYear {
            margin: 0px;
            width: 100%;
            height: 24px;
            text-align: left;
            color: #1d4070;
            padding-left: 1px;
            padding-top: 2px;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            border-bottom: 1px solid #b9b9b9;
            border-left: 1px solid #e6e6e6;
            border-right: 1px solid #e6e6e6;
            border-top: 1px solid #e6e6e6;
            font-size: 18px;
        }

        .calDayStyle {
            border: 1px solid #e6e6e6;
            height: 24px;
            width: 20px;
            text-decoration: none;
            font-size: 14px;
            text-align: center;
            color: #000;
            background-color: #ffffff;
        }

            .calDayStyle:hover {
                color: #000 !important;
                background: #e7fd7e;
                background: -moz-linear-gradient(top, #e7fd7e 0%, #e0fb5e 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e7fd7e), color-stop(100%,#e0fb5e));
                background: -webkit-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: -o-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: -ms-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: linear-gradient(to bottom, #e7fd7e 0%,#e0fb5e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e7fd7e', endColorstr='#e0fb5e',GradientType=0 );
            }

        .calDayToday {
            color: #000;
            font-size: 13px;
            text-decoration: none;
            background: #85b9ff;
            border: none;
        }

            .calDayToday a {
                color: #000 !important;
            }

        .calWeekTitle {
            text-align: center;
            color: #fff;
            font-weight: bold;
            border: 0px none;
            font-size: 14px;
            padding: 6px;
            background: #75a9d1;
        }

        .calWeekendstyle {
            color: #ffffff !important;
            font-weight: bold;
            border-color: #0c0;
            background: #88e888;
            text-align: center !important;
            vertical-align: middle !important;
        }

        .calOtherDay {
            border: 1px solid #e6e6e6;
            height: 24px;
            width: 20px;
            text-decoration: none;
            font-size: 14px;
            text-align: center;
            background: #eeeeee;
        }

            .calOtherDay a {
                color: #8c8c8a !important;
            }

        .calEventDay {
            float: left;
            font-size: 12px;
            color: /*#317abf*/ #222222;
            font-weight: normal;
            text-align: center;
            line-height: 18px;
        }

        .calEventName {
            float: left;
            padding-left: 4px;
            padding-bottom: 1px;
            font-size: 12px;
            color: /*#2b8dd8*/ #222222;
            font-weight: normal;
            letter-spacing: 1px;
        }

        .calEventBoxMain {
            margin-left: 5px;
            margin-top: 7px;
            margin-bottom: 7px;
        }

        .calEventBoxSub {
            border-bottom: 1px solid #e6e6e6;
            width: 100%;
            float: left;
            vertical-align: middle;
        }

        .calEventColor {
            /*border: 1px solid #fff;*/
            width: 8px;
            height: 8px;
            display: block;
            float: left;
            margin: 6px 0;
        }

        .calEventDate {
            font-size: 11px;
            color: rgba(62, 64, 65, 1);
            font-weight: normal;
            padding-left: 12px;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #f5f5f5 !important;
        }
    </style>
    <div class="page-wrapper">
        <input type="hidden" id="h_Action" value="0" />
        <asp:HiddenField ID="h_EmpNoDesc" runat="server" />
        <div class="content container-fluid">

            <div class="row">
                <!-- Concession Error -->
                <div class="col-lg-12 col-md-12 col-12">
                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                    <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
                </div>
            </div>

            <div class="row">
                <!-- Concession Staff Profile starts here -->
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="profile-page" style="padding: 0px;">
                        <div class="row">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                                <div class="card profile-header card-border">
                                    <div class="card-header">
                                        <h3 class="card-title mb-0">Apply Tuition Fees Concession                               
                                        </h3>
                                    </div>

                                    <div class="bg-inverse-dark card-body">
                                        <div class="row">

                                            <div class="col-lg-2 col-md-2 col-12">
                                                <div class="profile-image float-md-center text-center mb-3">
                                                    <asp:Image ID="imgEmpImage" runat="server" ImageUrl='' ToolTip='' Style="height: 100px;" />
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-12 m-auto">

                                                <div class="row mb-3">
                                                    <div class="col-lg-5 col-md-5 col-6">
                                                        <span class="font-weight-bold">Employee Name :</span>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-6">
                                                        <span>
                                                            <asp:Label ID="LBL_EMP_NAME" runat="server"></asp:Label>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-lg-5 col-md-5 col-6">
                                                        <span class="font-weight-bold">Designation :</span>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-6">
                                                        <span>
                                                            <asp:Label ID="LBL_EMP_DESIG" runat="server"></asp:Label>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-lg-5 col-md-5 col-6">
                                                        <span class="font-weight-bold">Group Join Date :</span>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-6">
                                                        <span>
                                                            <asp:Label ID="LBLGRPDOJ" runat="server"></asp:Label>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-12 m-auto">
                                                <div class="row mb-3">
                                                    <div class="col-lg-5 col-md-5 col-6">
                                                        <span class="font-weight-bold">Employee ID :</span>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-6">
                                                        <span>
                                                            <asp:Label ID="LBL_EMP_NO" runat="server"></asp:Label>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-lg-5 col-md-5 col-6">
                                                        <span class="font-weight-bold">Department :</span>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-6">
                                                        <span>
                                                            <asp:Label ID="LBL_DPT" runat="server"></asp:Label>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="row mb-3">
                                                    <div class="col-lg-5 col-md-5 col-6">
                                                        <span class="font-weight-bold">Date Of Join :</span>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-6">
                                                        <span>
                                                            <asp:Label ID="LBL_EMP_DOJ" runat="server"></asp:Label>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- Concession Staff Profile ends here -->
            </div>


            <div class="row mb-3">
                <!-- Add Dependants -->
                <div class="col-lg-12 col-md-12 col-12 font-italic">
                    If your child is not listed below, <a href="#" onclick="showAddDependant();">click here </a>to add or link the existing dependent to a GEMS Student.
                </div>
                <div class="col-lg-12 col-md-12 col-12">

                    <div id="divalert" style="display: none">
                        <span id="LBLError2"></span>
                        <i class="fa fa-times text-default pl-5 float-right cursor-pointer" onclick="closeAlertPopup()"></i>
                    </div>

                </div>
            </div>

            <div class="row justify-content-center">
                <!-- Children Profile starts here -->

                <asp:Repeater ID="repInfo" runat="server">
                    <ItemTemplate>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div id="" class="card card-border">
                                <div class="card-header">

                                    <div class="row ">
                                        <div class="col-lg-8 col-md-12 col-12">
                                            <h3 class="card-title mb-0">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Student_Name")%>'></asp:Label>
                                                <%--<input class="btn btn-primary btn-round float-right btn-sm" style="margin-left: 10px;" data-toggle="modal" data-target="#Apply_concession" type="button" value="Apply Concession">--%>
                                       
                                            </h3>
                                        </div>
                                        <div class="col-lg-4 col-md-12 col-12">
                                            <a id='<%# String.Format("IMG{0}", Eval("STU_NO"))%>' class="btn btn-white btn-sm btn-rounded btnWithoutCursor pl-2 pr-2 float-right" href="#" data-toggle="dropdown" aria-expanded="false" style='<%# String.Format("{0}", Eval("STAMP_CLASS"))%>'>
                                                <i class="fa fa-dot-circle-o text-info"></i>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("STAMP_TEXT")%>'></asp:Label>
                                            </a>
                                            <a style='<%# String.Format("{0}", Eval("APPLY_BTN_STYLE"))%>' class="btn btn-primary btn-round float-right btn-sm cursor-pointer" id='<%# String.Format("{0}", Eval("STU_NO"))%>' onclick="ConfirmApplyConcession('<%# String.Format("{0}", Eval("STU_NO"))%>', '<%# String.Format("{0}", Eval("EDD_EMP_ID"))%>'); ">Apply Concession</a>
                                        </div>
                                    </div>


                                </div>
                                <div class="card-body pb-0 pt-1">
                                    <div class="row staff-grid-row">
                                        <div class="col-lg-4 col-md-4 col-12">
                                            <div class="profile-image float-md-center text-center p-3">
                                                <a data-toggle="modal" data-target="#View_Profile" class="avatar-lg">
                                                    <img src='<%# String.Format("../Payroll/ImageHandler.ashx?ID={0}&TYPE=EDD", Eval("EDD_ID"))%>' alt="" style="height: 100px; width: 100px;"></a>
                                            </div>
                                            <%-- <div class="row mb-1 text-center">
                                               
                                            </div>--%>
                                        </div>

                                        <div class="col-lg-8 col-md-8 col-12 mt-3">


                                            <div class="row mb-1">
                                                <div class="col-lg-6 col-md-6 col-12">
                                                    <span class="font-weight-bold">Name :</span>
                                                    <br />
                                                    <span>
                                                        <asp:Label ID="lbSName" runat="server" Text='<%# Bind("Student_Name")%>'></asp:Label></span>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-12">
                                                    <span class="font-weight-bold">Student ID :</span><br />

                                                    <span>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label></span>
                                                </div>
                                            </div>
                                            <div class="row mb-1">
                                                <div class="col-lg-6 col-md-6 col-12">
                                                    <span class="font-weight-bold">Grade :</span><br />
                                                    <span>
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("[Grade/Section]")%>'></asp:Label></span>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-12">
                                                    <span class="font-weight-bold">Date Of Join :</span><br />
                                                    <span>
                                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("DOJ")%>'></asp:Label></span>
                                                </div>
                                            </div>

                                            <div class="row mb-1">
                                                <div class="col-lg-12 col-md-12 col-12">
                                                    <span class="font-weight-bold">School :</span>
                                                    <br />
                                                    <span>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("BSU_NAME")%>'></asp:Label></span>
                                                </div>
                                            </div>


                                        </div>
                                        <%--<div class="col-12 col-lg-2 col-md-2 min-max-vh-280 mt-1 text-center">
                              <a href="#" data-toggle="tooltip" title='<%# String.Format("{0}", Eval("STAMP_TITLE"))%>'><span id="ID_STAMP" runat="server" class='<%# String.Format("{0}", Eval("STAMP_CLASS"))%>'><asp:Label ID="Label7" runat="server" Text='<%# Bind("STAMP_TEXT")%>'></asp:Label></span></a>
                                <div id='<%# String.Format("IMG{0}", Eval("STU_NO"))%>' style='<%# String.Format("{0}", Eval("APPLY_IMG_STYLE"))%>'><asp:image id="IMG_STAMP" runat="server" imageurl='<%# String.Format("{0}", Eval("STAMP_PATH"))%>' tooltip='<%# String.Format("{0}", Eval("STAMP_TITLE"))%>' style="height: 100px;" /></div>
                                <a style='<%# String.Format("{0}", Eval("APPLY_BTN_STYLE"))%>'  class="btn btn-primary btn-round float-right btn-sm cursor-pointer" id='<%# String.Format("{0}", Eval("STU_NO"))%>' onclick="ConfirmApplyConcession('<%# String.Format("{0}", Eval("STU_NO"))%>', '<%# String.Format("{0}", Eval("EDD_EMP_ID"))%>'); ">Apply Concession</a>
                            </div>--%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>



                <!-- Children Profile ends here -->
            </div>

        </div>
    </div>


    <div id="Child_List" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnChildList" onclick="Closesa_childList();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border" id="Div1" runat="server">
                                <div class="card-header">
                                    <h3 class="card-title mb-0">Dependent Details
                                        
                                <input id="btn_add_new2" class="btn btn-primary btn-round float-right btn-sm btn_add_new2" style="margin-left: 10px;" data-toggle="modal" data-target="#Add_Family" onclick="Closesa_childList()" type="button" value="Add New">
                                    </h3>

                                </div>
                                <div class="card-body pb-0 ">
                                    <div class="row staff-grid-row">
                                        <asp:Repeater ID="rptrChildList" runat="server" OnItemDataBound="rptrChildList_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3">
                                                    <%--<div class=" col-lg-2 col-xl-2 col-md-2 col-sm-4 col-12">--%>
                                                    <div class="profile-widget">
                                                        <div class="profile-img">
                                                            <a class="avatar" data-toggle="modal" data-target="#profile_infos" onclick="MyConfirmMethod('<%# Eval("EDD_ID")%>'); return false;">
                                                                <img src='<%# String.Format("../Payroll/ImageHandler.ashx?ID={0}&TYPE=EDD", Eval("EDD_ID"))%>' alt="" class="height-80">
                                                            </a>
                                                        </div>
                                                        <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a data-toggle="modal"><%# Eval("EDD_NAME")%></a></h4>
                                                        <div class="small"><%# Eval("EDD_RELATION_DESCR")%></div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <div style="padding: 0px 0px 10px 25px !important">
                                                    <asp:Label ID="lblEmptyData"
                                                        Text="No data available..." runat="server" Visible="false">
                                                    </asp:Label>
                                                </div>
                                                </table>           
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <!--<div class="card-footer">
                            <br />
                            <a href="#">View all</a>
                        </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="btn_Show_ChildList" class="btn btn-primary btn-round float-right btn-sm" style="margin-left: 10px; display: none" data-toggle="modal" data-target="#Child_List" type="button" value="Add New">
    <input type="hidden" id="h_company_ID" />
    <input type="hidden" id="h_stu_ID" />
    <input type="hidden" id="h_stu_NO" />
    <input type="hidden" id="h_emp_id" />
    <input type="hidden" id="h_cont_id" />
    <input type="hidden" id="h_file_path" />
    <input type="text" id="file_path" style="display: none;" />
    <input type="hidden" id="file_ext" />
    <input type="hidden" id="h_bsu_cty_id" runat="server" />
    <input type="hidden" id="h_EMP_SEX" runat="server" />
    <!-- /Profile Modal -->
    <!-- Add Profile Modal -->
    <div id="Add_Family" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Dependent Details
			<br />
                        <%-- <span class="text-muted" style="font-size: small !important; text-align: center;"><i>* All fields are mandatory</i></span>--%>
                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnsave2" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div id="divalert277">
                        <span id="ADDError"></span>
                        <i style="display: none;" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup2()"></i>
                    </div>
                    <form id="AddFamilyForm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap edit-img">
                                    <img class="inline-block" src="/PHOENIXBETA/GemsEss/assets/img/profiles/avatar-02.jpg" alt="" id="imgAddProfile">
                                    <div class="fileupload btn">
                                        <span class="btn-text">Add</span>
                                        <input class="upload" type="file" id="uploadAddPhoto">
                                        <%--<asp:FileUpload CssClass="upload" ID="fileupload" runat="server" />--%>
                                    </div>

                                </div>
                                <div style="text-align: center;">
                                    <span class="text-muted" style="font-size: small !important;"><i>Note:File of type .png, .jpeg or .jpg, and of size max 5 MB</i></span>
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Relation<span class="text-danger">*</span></label>
                                            <select class="select form-control slctAddRltion" id="slctAddRltion">
                                                <option selected="selected" value="-1">--Select--</option>
                                                <%--<option value="0">Spouse</option>--%>
                                                <option value="1">Child</option>
                                                <%--<option value="2">Father</option>
                                                <option value="3">Mother</option>--%>
                                                <%--<option value="4">Grand Child</option>
                                                <option value="5">Grand Parent</option>--%>
                                                <%--<option value="6">Other</option>--%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth<span class="text-danger">*</span></label>
                                            <div class="cal-icon-pick">
                                                <i class="fa fa-calendar textbox-cal-icon"></i>
                                                <input class="form-control datetimepicker1 txtAddDob" type="text" value="" id="txtAddDob" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="padding-top: 35px;">
                                            <input type="radio" id="radADDNGems" name="IsGemsAdd" class="GEMSADD" value="Non Gems" onclick="RadChange('0', 'Add');">
                                            Non GEMS
                                    <input type="radio" id="radADDSGems" name="IsGemsAdd" class="GEMSADD" value="Gems Staff" onclick="RadChange('1', 'Add');">
                                            GEMS Staff
                                    <input type="radio" id="radADDStGems" name="IsGemsAdd" class="GEMSADD" value="Gems Student" onclick="RadChange('2', 'Add');">
                                            GEMS Student                                       
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label>Select Unit<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" value="" id="txtAddUnit" style="display: none" onkeyup="InitCompanyAutoComplete(1);" data-toggle="dropdown" autocomplete="off" placeholder="Enter Unit Code">
                                            <div id="divCompany1" class="auto-comp-popup" style="display: none"></div>
                                            <asp:DropDownList ID="ddlAddUnit" class="select form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label id="lblAddGemsStaffStudent" class="lblAddGemsStaffStudent"></label>
                                            <span class="text-danger">*</span>&nbsp;&nbsp;<i class="lblGemsStudId" style='font-size: 10px; display: none'>(Please enter 14 digit number prinited on school Id card)</i>
                                            <input type="text" class="form-control txtAddGEMSStaffIdStudentId" id="txtAddGEMSStaffIdStudentId" autocomplete="off" onchange="ChktxtAddGemsStaffIdStudentId();">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name<span class="text-danger">*</span></label><i class="lblGemsStudId" style='font-size: 10px; display: none'>&nbsp;</i>
                                            <input style="display: none;" type="text" class="form-control" value="" id="txtAddName" onkeyup="InitStudentAutoComplete(1);" data-toggle="dropdown" autocomplete="off" onchange="ChktxtAddName();" placeholder="Enter Name">
                                            <div style="display: none;" id="divStudent1" class="auto-comp-popup"></div>
                                            <input type="text" class="form-control" id="txtAddDepedentName" disabled="disabled" autocomplete="off">
                                        </div>
                                    </div>
                                    <!--<div class="col-md-6">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="cal-icon">
                                                <input class="form-control datetimepicker" type="text" value="05/06/1985">
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gender<span class="text-danger">*</span></label>
                                            <select class="select form-control" id="slctAddGender">
                                                <option selected="selected" value="0">--Select Gender--</option>
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="display: none;">Marital Status<span class="text-danger">*</span></label>
                                            <select style="display: none;" class="select form-control" id="slctAddMar">
                                                <%-- <option selected="selected" value="-1">--Select Marital Status--</option>
                                                <option value="0">Married</option>--%>
                                                <option value="1">Single</option>
                                                <%-- <option value="2">Widowed</option>
                                                <option value="3">Divorced</option>
                                                <option value="4">Separated</option>--%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nationality<span class="text-danger">*</span></label>
                                    <input style="display: none" type="text" class="form-control" value="" id="txtAddNAt" onkeyup="InitCounrtyInfo(1);" data-toggle="dropdown" autocomplete="off">
                                    <div style="display: none" id="divCountry1" class="auto-comp-popup"></div>
                                    <asp:DropDownList ID="ddlAddNationality" class="select form-control" runat="server" onchange="changeEvent(0);"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Passport No</label>
                                    <input type="text" class="form-control" value="" id="txtAddPasprtNo" autocomplete="off" onchange="ChktxtAddPasprtNo();">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country of Residence<span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlAddResidenceCountry" runat="server" class="select form-control" onchange="changeEvent(1);"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EmiratesID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddEID" autocomplete="off" onchange="ChktxtAddEID();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EID Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" value="" id="txtEIDExpDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->

                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>UID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddUIDNo" autocomplete="off" onchange="ChktxtAddUIDNo();" />
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" value="" id="txtAddVisaIssueDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="23/Oct/2016">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" value="" id="txtAddVisaExpDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Place<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddVisaIssuePlc" autocomplete="off" onchange="ChktxtAddVisaIssuePlc();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Sponsor<span class="text-danger">*</span></label>
                                    <select class="select form-control" id="slctAddVisaSpnsr">
                                        <option selected="selected" value="0">--Select Visa Sponsor--</option>
                                        <option value="1">Company</option>
                                        <option value="2">Spouse</option>
                                        <option value="3">Father</option>
                                        <option value="4">Mother</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Existing Insurance Card No<span class="text-danger"></span></label>
                                    <input type="text" class="form-control" value="" id="txtAddInsCrdNo" autocomplete="off" onchange="ChktxtAddInsCrdNo();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa File Number<span class="text-danger" runat="server" visible="false" id="spanAddVisFileNoCheck">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddVisaFileNumber" autocomplete="off" onkeypress="return IsValidVisaFileNo(event);" onchange="ChktxtAddVisaFileNumber();">
                                </div>
                            </div>

                        </div>
                        <div id="divalert2770" class="mb-2">
                            <span id="ADDError0"></span>
                            <i style="display: none;" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup2()"></i>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <input id="btnAddSaveDep" type="button" class="btn btn-primary" value="Save" onclick="SaveDependantDetails(0);" />
                            <a onclick="Closesa_AddDep();" class="btn btn-warning">Close</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <!--Add cash vo-->
    <!-- Profile Modal -->
    <div id="profile_infos" class="modal custom-modal fade" role="dialog">
        <%--..GemsEss/--%>
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dependent Details
                        <br />
                        <%--<span class="text-muted" style="font-size: small !important; text-align: center;"><i>* All fields are mandatory</i></span>--%>
                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnSave3" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divalert377">
                        <span id="ErrorEditFamily"></span>
                        <i style="display: none;" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup3()"></i>
                    </div>

                    <form id="EditFamilyForm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap edit-img">
                                    <img class="inline-block" src="/PHOENIXBETA/GemsEss/assets/img/profiles/staff4.png" alt="user" id="imgDDProfile">
                                    <div class="fileupload btn">
                                        <span class="btn-text"><i class="fa fa-pencil"></i>Edit</span>
                                        <input class="upload" type="file" id="uploadDDPhoto">
                                    </div>

                                </div>
                                <div style="text-align: center;">
                                    <span class="text-muted" style="font-size: small !important;"><i>Note:File of type .png, .jpeg or .jpg, and of size max 5 MB</i></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Relation<span class="text-danger">*</span></label>
                                            <select class="select form-control" id="slctDDRel">
                                                <%--<option selected="selected" value="-1">--Select--</option>--%>
                                                <%--<option value="0">Spouse</option>--%>
                                                <option value="1">Child</option>
                                                <%--<option value="2">Father</option>
                                                <option value="3">Mother</option>--%>
                                                <%--<option value="4">Grand Child</option>
                                                <option value="5">Grand Parent</option>--%>
                                                <%--<option value="6">Other</option>--%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth<span class="text-danger">*</span></label>
                                            <div class="cal-icon-pick">
                                                <i class="fa fa-calendar textbox-cal-icon"></i>
                                                <input class="form-control datetimepicker1 txtDDDob" type="text" id="txtDDDOB" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="padding-top: 35px;">
                                            <input type="radio" id="radDDNGems" name="IsGems" value="Non Gems" onclick="RadChange('0', 'DD');">
                                            Non GEMS
                                    <input type="radio" id="radDDSGems" name="IsGems" value="Gems Staff" onclick="RadChange('1', 'DD');">
                                            GEMS Staff
                                    <input type="radio" id="radDDStGems" name="IsGems" value="Gems Student" onclick="RadChange('2', 'DD');">
                                            GEMS Student                                       
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label>Select Unit<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="txtDDUnit" style="display: none" onkeyup="InitCompanyAutoComplete(0);" data-toggle="dropdown" autocomplete="off" placeholder="Enter Unit Code">
                                            <div id="divCompany" class="auto-comp-popup" style="display: none"></div>
                                            <asp:DropDownList ID="ddlDDUnit" class="select form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label id="lblDDGemsStaffStudent" class="lblDDGemsStaffStudent"></label>
                                            <span class="text-danger">*</span><i class="lblGemsStudId" style='font-size: 10px; display: none;'>(Please enter 14 digit number prinited on school Id card)</i>
                                            <input type="text" class="form-control txtDDGEMSStaffIdStudentId" id="txtDDGEMSStaffIdStudentId" autocomplete="off" onchange="ChktxtDDGemsStaffIdStudentId();">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name<span class="text-danger">*</span></label><i class="lblGemsStudId" style='font-size: 10px; display: none'>&nbsp;</i>
                                            <input style="display: none;" type="text" class="form-control" id="txtDDName" onkeyup="InitStudentAutoComplete(0);" data-toggle="dropdown" autocomplete="off" onchange="ChktxtDDName();" placeholder="Enter Name">
                                            <div id="divStudent" style="display: none;" class="auto-comp-popup"></div>
                                            <input type="text" class="form-control" id="txtDDDepedentName" disabled="disabled" autocomplete="off">
                                        </div>
                                    </div>
                                    <!--<div class="col-md-6">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="cal-icon">
                                                <input class="form-control datetimepicker" type="text" value="05/06/1985">
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gender<span class="text-danger">*</span></label>
                                            <select class="select form-control slctDDGender" id="slctDDGender">
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="display: none;">Marital Status<span class="text-danger">*</span></label>
                                            <select style="display: none;" class="select form-control slctDDMrtsts" id="slctDDMrtsts">
                                                <%--<option value="-1">-Select-</option>
                                                <option value="0">Married</option>--%>
                                                <option value="1">Single</option>
                                                <%--<option value="2">Widowed</option>
                                                <option value="3">Divorced</option>
                                                <option value="4">Separated</option>--%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nationality<span class="text-danger">*</span></label>
                                    <input style="display: none" type="text" class="form-control" id="txtDDNationality" onkeyup="InitCounrtyInfo(0);" data-toggle="dropdown" autocomplete="off">
                                    <div style="display: none" id="divCountry" class="auto-comp-popup"></div>
                                    <asp:DropDownList ID="ddlDDNationality" class="select form-control" runat="server" onchange="changeEvent(0);"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Passport No</label>
                                    <input type="text" class="form-control" id="txtDDPasprtNo" autocomplete="off" onchange="ChktxtDDPasprtNo();">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country of Residence<span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlDDResidenceCountry" class="select form-control" runat="server" onchange="changeEvent(0);"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EmiratesID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="txtDDEIDNo" autocomplete="off" onchange="ChktxtDDEIDNo();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EID Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" id="txtDDEDIEXPDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->

                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>UID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="txtDDUID" autocomplete="off" onchange="ChktxtDDUID();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" id="txtDDVIsaIsueDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="23/Oct/2016">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" id="txtDDVIsaExpDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Place<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="txtDDVisaIsuePlc" autocomplete="off" onchange="ChktxtDDVisaIsuePlc();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Sponsor<span class="text-danger">*</span></label>
                                    <select class="select form-control" id="slctDDVisSpnsr">
                                        <option value="0">Select</option>
                                        <option value="1">Company</option>
                                        <option selected="selected" value="2">Spouse</option>
                                        <option value="3">Father</option>
                                        <option value="4">Mother</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Existing Insurance Card No<span class="text-danger"></span></label>
                                    <input type="text" class="form-control" id="txtDDInsCrdNo" autocomplete="off" onchange="ChktxtDDInsCrdNo();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa File Number<span class="text-danger" runat="server" visible="false" id="spanVisFileNoCheck">*</span></label>
                                    <input type="text" class="form-control" id="txtvisafilenumber" autocomplete="off" onkeypress="return IsValidVisaFileNo(event);" onchange="ChktxtVisaFileNumber();">
                                </div>
                            </div>
                        </div>
                        <div id="divalert3770">
                            <span id="ErrorEditFamily0"></span>
                            <i style="display: none;" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup3()"></i>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <input type="button" class="btn btn-primary" id="btnProfSave" onclick="SaveDependantDetails(0);" value="Save" />
                            <input type="button" class="btn btn-danger" id="btnProfDelete" onclick="ConfirmDelete(0);" value="Delete" />
                            <a onclick="Closesa_EditDep();" class="btn btn-warning">Close</a>
                        </div>
                        <%--<span id="ErrorEditFamily" class=""></span>--%>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmModal" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-md vertical-align-center">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btn_close_modal_info"><span aria-hidden="true">&times;</span></button>
                        <div id="confirmContent" style="font-weight: normal;">Request for tuition fee concession is subject to approval from HR based on the HR policy. Do you want to continue? </div>
                    </div>
                    <div class="modal-footer text-center" id="footer_modal">
                        <button type="button" class="btn btn-primary btn_yes_confirm" onclick="hideConfirmModal();">Yes</button>
                        <button type="button" class="btn btn-primary btn_no_confirm" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>



        $(document).ready(function () {

            //GetMenuAccess();

            $(".datetimepicker1").datetimepicker({
                format: 'DD/MMM/YYYY'
            });

            var icurYear = '<%= Session("BSU_PAYYEAR")%>'
            var setyear = Number(icurYear) + 1;

            //$(document).on("click", function () {
            //    $("#divCompany,#divCompany1, #divStudent, #divStudent1, #divCountry, #divCountry1").hide("fast");
            //});



            $('#h_Action').val('0');
            $('#h_cont_id').val(0);
            $('#h_stu_ID').val(0);
            $('#h_company_ID').val(0);
            $('#h_emp_id').val(0);





            GetCountry()

        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $('#<%= ddlDDResidenceCountry.ClientID %>').change(function (event) {
            var e = document.getElementById('<%=ddlDDResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');
            if (e.options[e.selectedIndex].value == "172") {
                $(".divUAE").css("display", "block");
            } else $(".divUAE").css("display", "none");
        })
        $('#<%= ddlAddResidenceCountry.ClientID %>').change(function (event) {
            var e = document.getElementById('<%=ddlAddResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');            
            if (e.options[e.selectedIndex].value == 172) {
                $(".divUAE").css("display", "block");
            } else $(".divUAE").css("display", "none");
        })



        function Popup(url) {
            $.fancybox({
                'width': '100%',
                'height': '100%',
                'autoScale': false,
                'fitToView': false,
                'autoSize': false,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
        function ClosePopup(a) {
            parent.$.fancybox.close()
            parent.AcceptValues(a)

        }

        function ClosePopup2() {
            parent.$.fancybox.close();

            //window.location.href = "index.aspx";
            //parent.FeeConcessionPopup(24322);
            //parent.$("#Add_Family").show();
            parent.showAddDependant();
        }

        function showAddDependant() {
            //$("#Add_Family").toggle('modal');
            ////$("#Add_Family").removeClass('fade');
            //$("#Add_Family").addClass('show');
            //$('#Add_Family').attr("aria-modal", "true");
            //$('#btn_add_new').click();
            $('#btn_Show_ChildList').click();
        }
        function Closesa() {

            if (document.getElementById('h_Action').value == '1') {
                window.location.href = "FeeConcessionPopup_ESS.aspx?id=" + '<%= Session("Encr_Employee_ID")%>';
            }
        }
        function Closesa_childList() {
            document.getElementById('btnChildList').click();
            $(".modal").css("overflow-x", "hidden");
            $(".modal").css("overflow-y", "auto");
            $('.divGems').css("display", "none");
            $(".lblGemsStudId").css('display', 'none');
            $('#radADDNGems').prop("checked", false)
            $('#radADDSGems').prop("checked", false)
            $('#radADDStGems').prop("checked", false)
        }
        function closeAlertPopup() {
            $("#divalert").css("display", "none");
        }
        function closeAlertPopup3() {
            $("#divalert3").css("display", "none");
        }
        function closeAlertPopup2() {
            $("#divalert2").css("display", "none");
        }

        function RadChange(id, controlType) {

            if (id == '0') {
                $('#txtDDUnit').attr("disabled", "disabled");
                $('#txtAddUnit').attr("disabled", "disabled");
                $('#txtDDUnit').val("");
                $('#txtAddUnit').val("");
                $(".divGems").css('display', 'none');
                $('#txtAddDepedentName').removeAttr("disabled");
                $('#txtDDDepedentName').removeAttr("disabled");
                $(".lblGemsStudId").css('display', 'none');
                if (controlType == "DD") {
                    $('#txtDDDepedentName').val("");
                    $('#txtDDGEMSStaffIdStudentId').val("");
                } else {
                    $('#txtAddDepedentName').val("");
                    $('#txtAddGEMSStaffIdStudentId').val("");
                }
            } else if (id == '1') {
                $('#txtDDUnit').removeAttr("disabled");
                $('#txtAddUnit').removeAttr("disabled");
                $(".divGems").css('display', 'block');
                $('#txtAddDepedentName').attr("disabled", "disabled");
                $('#txtDDDepedentName').attr("disabled", "disabled");
                if (controlType == "DD")
                    getDependentName(0)
                else getDependentName(1)
                $(".lblDDGemsStaffStudent").text("Staff Id");
                $(".lblGemsStudId").css('display', 'none');
                $(".lblAddGemsStaffStudent").text("Staff Id");
                GetBusinessUnits("E")
            } else if (id == '2') {
                $('#txtDDUnit').removeAttr("disabled");
                $('#txtAddUnit').removeAttr("disabled");
                $(".divGems").css('display', 'block');
                $(".lblGemsStudId").css('display', 'block');
                $('#txtAddDepedentName').attr("disabled", "disabled");
                $('#txtDDDepedentName').attr("disabled", "disabled");
                if (controlType == "DD")
                    getDependentName(0)
                else getDependentName(1)
                $(".lblDDGemsStaffStudent").text("Student Id");
                $(".lblAddGemsStaffStudent").text("Student Id");
                GetBusinessUnits("S")
            }
        }

        function InitCompanyAutoComplete(id) {

            //$('#txtDDUnit').autocomplete({
            //source: function (request, response) {
            var prefix;
            $('#h_company_ID').val(0);
            var SearchFor;
            if (id == 0) {
                prefix = $('#txtDDUnit').val();
            } else if (id == 1) {
                prefix = $('#txtAddUnit').val();
            }

            if (isNaN(prefix) == true) {
                SearchFor = 'BSU_NAME';
            }
            else {
                SearchFor = 'BSU_ID';
            }
            $.ajax({
                url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/GetCompany",
                data: "{ 'prefix': '" + prefix + "','userName':'" + '<%= Session("sUsr_name")%>' + "', 'SearchFor':'" + SearchFor + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;

                    if (id == 0) {
                        $("#divCompany").empty();
                        $("#divCompany").css("display", "block");
                    } else if (id == 1) {
                        $("#divCompany1").empty();
                        $("#divCompany1").css("display", "block");
                    }

                    if (arraylength > 0) {
                        var html = "<div class='container'>";
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var value = response.d[i].split("||")[0];
                            var cid = response.d[i].split("||")[1];
                            var value2 = value.replace(/\s+/g, '_');
                            html = html + "<input type='button' class='auto-comp-link' value='";
                            html = html + value + "' title='" + value + "' onclick=clickOnLink('" + id + "','" + cid + "','" + value2 + "');></input>";
                            //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");
                        }
                        html = html + "</div>";

                        if (id == 0) {
                            $('#divCompany').append(html);
                        } else if (id == 1) {
                            $('#divCompany1').append(html);
                        }

                    }
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }

            });
        }
        function ChktxtVisaFileNumber() {
            var control = $('#txtvisafilenumber').val();
            if (control == '') {
                alert("Please Enter Visa File Number");
                $('#txtvisafilenumber').val("");
                return true;
            }
            if (control.trim() == '') {
                alert("Invalid Visa File Number Entered");
                $('#txtvisafilenumber').val("");
                return true;
            }
        }
        function InitStudentAutoComplete(id) {
            var prefix;
            var Type;
            $('#h_stu_ID').val(0);
            $('#h_emp_id').val(0);

            var SearchFor;
            if (id == 0) {
                prefix = $('#txtDDName').val();
                Type = $("#EditFamilyForm input[type='radio']:checked").val();

            } else if (id == 1) {
                prefix = $('#txtAddName').val();
                Type = $("#AddFamilyForm input[type='radio']:checked").val();
            }


            if (isNaN(prefix) == true) {
                SearchFor = 'NAME';
            }
            else {
                SearchFor = 'ID';
            }

            $.ajax({
                url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/GetStudents",
                data: "{'Type': '" + Type + "','prefix': '" + prefix + "','COMP_ID':'" + $('#h_company_ID').val() + "','ACD_ID':'" + '<%=Session("Current_ACD_ID")%>' + "','SearchFor':'" + SearchFor + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;

                    if (id == 0) {
                        $("#divStudent").empty();
                        $("#divStudent").css("display", "block");
                    } else if (id == 1) {
                        $("#divStudent1").empty();
                        $("#divStudent1").css("display", "block");
                    }

                    if (arraylength > 0) {
                        var html = "<div class='container'>";
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {

                            if (SearchFor == 'NAME') {
                                var value = response.d[i].split("||")[0];
                                var cid = response.d[i].split("||")[1];
                                var value2 = value.replace(/\s+/g, '_');
                                var Type2 = Type.replace(/\s+/g, '_');

                                html = html + "<input type='button' class='auto-comp-link' value='";
                                html = html + value + "'" + " onclick=clickOnLinkStud('" + id + "','" + cid + "','" + value2 + "','" + Type2 + "');></input>";
                                //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");
                            } else if (SearchFor = 'ID') {
                                var value = response.d[i].split("||")[2];
                                var cid = response.d[i].split("||")[1];
                                var value2 = value.replace(/\s+/g, '_');
                                var Type2 = Type.replace(/\s+/g, '_');

                                html = html + "<input type='button' class='auto-comp-link' value='";
                                html = html + value + "'" + " onclick=clickOnLinkStud('" + id + "','" + cid + "','" + value2 + "','" + Type2 + "');></input>";
                                //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");
                            }

                        }
                        html = html + "</div>";

                        if (id == 0) {
                            $('#divStudent').append(html);
                        } else if (id == 1) {
                            $('#divStudent1').append(html);
                        }
                    }
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });

        }
        function clickOnLinkStud(id, stuid, stuname, type) {

            stuname = stuname.replace(/_/g, ' ');
            if (id == 0) {
                $('#txtDDName').val(stuname);
                $("#divStudent").css("display", "none");
            } else if (id == 1) {
                $('#txtAddName').val(stuname);
                $("#divStudent1").css("display", "none");
            }

            if (type == 'Gems_Student') {
                $('#h_stu_ID').val(stuid);

            } else if (type == 'Gems_Staff') {
                $('#h_emp_id').val(stuid);

            }

        }

        function ChktxtAddName() {
            var control = $('#txtAddDepedentName').val();
            if (control == '') {
                alert("Please Enter Name");
                $('#txtAddDepedentName').val("");
                return true;
            }
            if (control.trim() == '') {
                alert("Invalid Name Entered");
                $('#txtAddDepedentName').val("");
                return true;
            }
        }

        function InitCounrtyInfo(id) {
            var prefix;
            $('#h_cont_id').val(0);
            if (id == 0) {
                prefix = $('#txtDDNationality').val();
            } else if (id == 1) {
                prefix = $('#txtAddNAt').val();
            }

            $.ajax({
                url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/GetCountry",
                data: "{ 'prefix': '" + prefix + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;

                    if (id == 0) {
                        $("#divCountry").empty();
                        $("#divCountry").css("display", "block");
                    } else if (id == 1) {
                        $("#divCountry1").empty();
                        $("#divCountry1").css("display", "block");
                    }

                    if (arraylength > 0) {
                        var html = "<div class='container'>";
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var value = response.d[i].split("||")[0];
                            var cid = response.d[i].split("||")[1];
                            var value2 = value.replace(/\s+/g, '_');

                            html = html + "<input type='button' class='auto-comp-link' value='";
                            html = html + value + "'" + " onclick=clickOnLinkCont('" + id + "','" + cid + "','" + value2 + "');></input>";
                            //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");                                                                                      
                        }
                        html = html + "</div>";

                        if (id == 0) {
                            $('#divCountry').append(html);
                        } else if (id == 1) {
                            $('#divCountry1').append(html);
                        }
                    }
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });

        }

        function changeEvent(id) {

            if (id == 0) {
                var e = document.getElementById('<%=ddlDDResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');

                console.log("console values 1 " + e);
                console.log("values are" + e.options[e.selectedIndex].value);
                if (e.options[e.selectedIndex].value == "172") {


                    $(".divUAE").css("display", "block");
                } else $(".divUAE").css("display", "none");
            } else if (id == 1) {
                var e = document.getElementById('<%=ddlAddResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit'); 

                    console.log("console values 2" + e);
                    console.log("values are" + e.options[e.selectedIndex].value);
                    if (e.options[e.selectedIndex].value == 172) {
                        $(".divUAE").css("display", "block");
                    } else $(".divUAE").css("display", "none");
                }
        }


        $('#<%= ddlDDUnit.ClientID %>').change(function (event) {
            getDependentName(0)
        })
        $('#<%= ddlAddUnit.ClientID %>').change(function (event) {
            getDependentName(1)
        })

        $('.txtDDDob').change(function (event) {
            getDependentName(0)
        })
        $('.txtAddDob').change(function (event) {
            getDependentName(1)
        })

        $('.slctAddRltion').change(function () {
            var e = document.getElementById('slctAddRltion');
            var Relation = e.options[e.selectedIndex].value;
            if ((Relation == "0" || Relation == "2") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "False") {

                $('#slctAddGender').val("M");
                $('#slctAddMar').val(0);
            }
            else if ((Relation == "0" || Relation == "3") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "False") {

                $('#slctAddGender').val("F");
                $('#slctAddMar').val(0);
            } else if ((Relation == "0" || Relation == "2") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "True") {
                $('#slctAddGender').val("M");
                $('#slctAddMar').val(0);
            } else if ((Relation == "0" || Relation == "3") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "True") {
                $('#slctAddGender').val("F");
                $('#slctAddMar').val(0);
            } else {
                $('#slctAddGender').val(0);
                $('#slctAddMar').val(-1);
            }
        })

function getDependentName(id) {

    var Type = "", unitVal = "", DOBVal = "", Idval = ""
    if (id == 0) {
        Type = $("#EditFamilyForm input[type='radio']:checked").val();
        var e = document.getElementById('<%=ddlDDUnit.ClientID%>');//document.getElementById('ddlDDUnit');
        if (e.selectedIndex != -1)
            unitVal = e.options[e.selectedIndex].value;
        DOBVal = $(".txtDDDob").val();// document.getElementById('txtDDDob').value;
        Idval = $(".txtDDGEMSStaffIdStudentId").val();//document.getElementById('txtDDGEMSStaffIdStudentId').value;
    } else if (id == 1) {
        //Type = $("#AddFamilyForm input[type='radio']:checked").val();
        Type = $(".GEMSADD:checked").val();
        console.log($(".GEMSADD:checked").val());
        var e = document.getElementById('<%=ddlAddUnit.ClientID%>');//document.getElementById('ddlAddUnit');
        if (e.selectedIndex != -1)
            unitVal = e.options[e.selectedIndex].value;
        DOBVal = $(".txtAddDob").val();//document.getElementById('txtAddDob').value;
        Idval = $(".txtAddGEMSStaffIdStudentId").val();//document.getElementById('txtAddGEMSStaffIdStudentId').value;
    }
    if (unitVal != "" && DOBVal != "" && Idval != "") {
        $.ajax({
            url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/GetDependentName",
            data: "{'Type': '" + Type + "','Dob': '" + DOBVal + "','StaffIdORStuId':'" + Idval + "','BSU_ID':'" + unitVal + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (response) {

                var arraylength = response.d.length;
                if (arraylength > 0) {
                    if (id == 0) {
                        $('#txtDDDepedentName').val(response.d[0].split("||")[0]);
                        if (response.d[0].split("||")[2] == "M") {
                            $('#slctDDGender').val("M");
                        } else { $('#slctDDGender').val("F"); }
                        $('#slctDDMrtsts').val(response.d[0].split("||")[3]);
                    } else if (id == 1) {
                        $('#txtAddDepedentName').val(response.d[0].split("||")[0]);
                        if (response.d[0].split("||")[2] == "M") {
                            $('#slctAddGender').val("M");
                        } else { $('#slctAddGender').val("F"); }
                        $('#slctAddMar').val(response.d[0].split("||")[3]);
                    }
                    if (Type == "Gems Student") {
                        $('#h_stu_ID').val(response.d[0].split("||")[1]);
                    } else if (Type == "Gems Staff") {
                        $('#h_emp_id').val(response.d[0].split("||")[1]);
                    }

                } else {
                    if (id == 0) {
                        $('#txtDDDepedentName').val("");
                        $('#slctDDGender').val(0);
                        $('#slctDDMrtsts').val(-1);
                    } else if (id == 1) {
                        $('#txtAddDepedentName').val("");
                        $('#slctADDGender').val(0);
                        $('#slctAddMar').val(-1);
                    }
                    $('#h_emp_id').val(0);
                    $('#h_stu_ID').val(0);
                    if (Type == "Gems Student") {
                        alert("Please enter valid Student Id");
                    } else if (Type == "Gems Staff") {
                        alert("Please enter valid Staff Id");
                    }
                }
            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });
    }

}


function ChktxtAddPasprtNo() {
    var control = $('#txtAddPasprtNo').val();
    if (control == '') {
        alert("Please Enter Passport Number");
        $('#txtAddPasprtNo').val("");
        return true;
    }
    if (control.trim() == '') {
        alert("Invalid Passport Number");
        $('#txtAddPasprtNo').val("");
        return true;
    }
}
function ChktxtAddEID() {
    var control = $('#txtAddEID').val();
    if (control == '') {
        alert("Please Enter Emirates ID Number");
        $('#txtAddEID').val("");
        return true;
    }
    if (control.trim() == '') {
        alert("Invalid Emirates ID Number Entered");
        $('#txtAddEID').val("");
        return true;
    }
}

function ChktxtAddUIDNo() {
    var control = $('#txtAddUIDNo').val();
    if (control == '') {
        alert("Please Enter UID Number");
        $('#txtAddUIDNo').val("");
        return true;
    }
    if (control.trim() == '') {
        alert("Invalid UID Number Entered");
        $('#txtAddUIDNo').val("");
        return true;
    }
}
function ChktxtAddVisaIssuePlc() {
    var control = $('#txtAddVisaIssuePlc').val();
    if (control == '') {
        alert("Please Enter Visa Issued Place");
        $('#txtAddVisaIssuePlc').val("");
        return true;
    }
    if (control.trim() == '') {
        alert("Invalid Visa Issued Place Entered");
        $('#txtAddVisaIssuePlc').val("");
        return true;
    }
}
function ChktxtDDGemsStaffIdStudentId() {
    var control = $('#txtDDGEMSStaffIdStudentId').val();
    if (control == '') {
        alert("Please Enter " + $(".lblDDGemsStaffStudent").text());
        $('#txtDDGEMSStaffIdStudentId').val("");
        return true;
    }
    if (control.trim() == '') {
        alert("Invalid " + $(".lblDDGemsStaffStudent").text());
        $('#txtDDGEMSStaffIdStudentId').val("");
        return true;
    } else {
        getDependentName(0)
    }
}

function ChktxtAddGemsStaffIdStudentId() {
    var control = $('#txtAddGEMSStaffIdStudentId').val();
    if (control == '') {
        alert("Please Enter " + $(".lblAddGemsStaffStudent").text());
        $('#txtAddGEMSStaffIdStudentId').val("");
        return true;
    } else if (control.trim() == '') {
        alert("Invalid " + $(".lblAddGemsStaffStudent").text());
        $('#txtAddGEMSStaffIdStudentId').val("");
        return true;
    } else {
        getDependentName(1)
    }
}



function ChktxtAddInsCrdNo() {
    //var control = $('#txtAddInsCrdNo').val();
    //if (control == '') {
    //    alert("Please Enter Insurance Number");
    //    $('#txtAddInsCrdNo').val("");
    //    return true;
    //}
    //if (control.trim() == '') {
    //    alert("Invalid Insurance Number Entered");
    //    $('#txtAddInsCrdNo').val("");
    //    return true;
    //}
}
function ChktxtAddVisaFileNumber() {
    var control = $('#txtAddVisaFileNumber').val();
    if (control == '') {
        alert("Please Enter Visa File Number");
        $('#txtAddVisaFileNumber').val("");
        return true;
    }
    if (control.trim() == '') {
        alert("Invalid Visa File Number Entered");
        $('#txtAddVisaFileNumber').val("");
        return true;
    }
}

function SaveDependantDetails(id) {

    if (id == 0) {
        //var Type = $("#AddFamilyForm input[type='radio']:checked").val();
        var Type = $(".GEMSADD:checked").val();
        var Name = document.getElementById('txtAddDepedentName').value;
        var e = document.getElementById('slctAddRltion');
        var Relation = e.options[e.selectedIndex].value;
        var DOB = document.getElementById('txtAddDob').value;
        var r = document.getElementById('slctAddGender');
        var Gen = r.options[r.selectedIndex].value;
        var p = document.getElementById('slctAddMar');
        var Mar = p.options[p.selectedIndex].value;
        var Nationlty = document.getElementById('txtAddNAt').value;
        var PassportNO = document.getElementById('txtAddPasprtNo').value;
        var txtAddEID = document.getElementById('txtAddEID').value;
        var txtEIDExpDt = document.getElementById('txtEIDExpDt').value;
        var txtAddUIDNo = document.getElementById('txtAddUIDNo').value;
        var txtAddVisaIssueDt = document.getElementById('txtAddVisaIssueDt').value;
        var txtAddVisaExpDt = document.getElementById('txtAddVisaExpDt').value;
        var txtAddVisaIssuePlc = document.getElementById('txtAddVisaIssuePlc').value;
        var m = document.getElementById('slctAddVisaSpnsr');
        var slctAddVisaSpnsr = m.options[m.selectedIndex].value;
        var txtAddInsCrdNo = document.getElementById('txtAddInsCrdNo').value;
        var StuID = $('#h_stu_ID').val();//document.getElementById('txtAddGEMSStaffIdStudentId').value;
        var DependMStatus = p.options[p.selectedIndex].text;
        var bu = document.getElementById('<%=ddlAddUnit.ClientID%>');
        //var Unit = document.getElementById('txtAddUnit').value;
        if (bu.selectedIndex != -1) {
            var Unit = bu.options[bu.selectedIndex].text;
            var bsuid = bu.options[bu.selectedIndex].value;
        } else {
            var Unit = "";
            var bsuid = "";
        }
        var employeeID = $('#h_emp_id').val();
        //var employeeID = document.getElementById('txtAddGEMSStaffIdStudentId').value;
        var city = document.getElementById('<%=ddlAddNationality.ClientID%>');
        var contryID = city.options[city.selectedIndex].value;//$('#h_cont_id').val();
        //var contryID = $('#h_cont_id').val();
        var visafilenumber = document.getElementById('txtAddVisaFileNumber').value;
        var rc = document.getElementById('<%=ddlAddResidenceCountry.ClientID%>');
        var ResindenceCountryId = rc.options[rc.selectedIndex].value;
        var GemsStaffIdorStuId = document.getElementById('txtAddGEMSStaffIdStudentId').value;
    }
    else {
        var Type = $("#EditFamilyForm input[type='radio']:checked").val();
        var Name = document.getElementById('txtDDDepedentName').value;
        var e = document.getElementById('slctDDRel');
        var Relation = e.options[e.selectedIndex].value;
        var DOB = document.getElementById('txtDDDOB').value;
        var r = document.getElementById('slctDDGender');
        var Gen = r.options[r.selectedIndex].value;
        var p = document.getElementById('slctDDMrtsts');
        var Mar = p.options[p.selectedIndex].value;
        var Nationlty = document.getElementById('txtDDNationality').value;
        var PassportNO = document.getElementById('txtDDPasprtNo').value;
        var txtAddEID = document.getElementById('txtDDEIDNo').value;
        var txtEIDExpDt = document.getElementById('txtDDEDIEXPDt').value;
        var txtAddUIDNo = document.getElementById('txtDDUID').value;
        var txtAddVisaIssueDt = document.getElementById('txtDDVIsaIsueDt').value;
        var txtAddVisaExpDt = document.getElementById('txtDDVIsaExpDt').value;
        var txtAddVisaIssuePlc = document.getElementById('txtDDVisaIsuePlc').value;
        var m = document.getElementById('slctDDVisSpnsr');
        var slctAddVisaSpnsr = m.options[m.selectedIndex].value;
        var txtAddInsCrdNo = document.getElementById('txtDDInsCrdNo').value;
        var StuID = $('#h_stu_ID').val();//document.getElementById('txtDDGEMSStaffIdStudentId').value//
        var DependMStatus = p.options[p.selectedIndex].text;
        //var Unit = document.getElementById('txtDDUnit').value;
        //var bsuid = $('#h_company_ID').val();
        var bu = document.getElementById('<%=ddlDDUnit.ClientID%>');
        //var Unit = document.getElementById('txtAddUnit').value;
        if (bu.selectedIndex != -1) {
            var Unit = bu.options[bu.selectedIndex].text;
            var bsuid = bu.options[bu.selectedIndex].value;
        } else {
            var Unit = "";
            var bsuid = "";
        }
        var employeeID = $('#h_emp_id').val();
        //var employeeID = document.getElementById('txtDDGEMSStaffIdStudentId').value;

        var city = document.getElementById('<%=ddlDDNationality.ClientID%>');
        var contryID = city.options[city.selectedIndex].value;//$('#h_cont_id').val();
        var visafilenumber = document.getElementById('txtvisafilenumber').value;
        var rc = document.getElementById('<%=ddlDDResidenceCountry.ClientID%>');
        var ResindenceCountryId = rc.options[rc.selectedIndex].value;
        var GemsStaffIdorStuId = document.getElementById('txtDDGEMSStaffIdStudentId').value;
    }

    var filepath = getBase64Image(id);


    $.ajax({
        type: "POST",
        url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/AddSaveDependantDetails",
        data: '{EDDID: "' + id + '", bsuid: "' + bsuid + '", usrName: "' + '<%= Session("sUsr_name")%>' + '", Name: "' + Name + '", DOBDate: "' + DOB + '", Relation: "' + Relation + '", StuID: "' + StuID + '", PassportNo: "' + PassportNO + '", UIDNo: "' + txtAddUIDNo + '", VisaIssueDt: "' + txtAddVisaIssueDt + '", VisaExpDate: "' + txtAddVisaExpDt + '", VisaIssuePlc: "' + txtAddVisaIssuePlc + '", EIDExpryDt: "' + txtEIDExpDt + '", Gender: "' + Gen + '", MarStatus: "' + Mar + '", DependMStatus: "' + DependMStatus + '", CityID: "' + contryID + '", Nationality: "' + Nationlty + '", Sponser: "' + slctAddVisaSpnsr + '", InsuranceNumber: "' + txtAddInsCrdNo + '", EIDNo: "' + txtAddEID + '", GemsType: "' + Type + '", Unit: "' + Unit + '", EmployeeID: "' + employeeID + '", Filepath: "' + filepath + '", Visafilenumber: "' + visafilenumber + '", ResindenceCountryId: "' + ResindenceCountryId + '", GemsStaffIdorStuId: "' + GemsStaffIdorStuId + '" }',
        //data: '{ EDDID: "' + '0' + '", bsuid: "' + '0' + '", usrName: "' + '0' + '", Name: "' + '0' + '", DOBDate: "' + '0' + '", Relation: "' + '0' + '", StuID: "' + '0' + '", PassportNo: "' + '0' + '", UIDNo: "' + '0' + '", VisaIssueDt: "' + '0' + '", VisaExpDate: "' + '0' + '", VisaIssuePlc: "' + '0' + '", EIDExpryDt: "' + '0' + '", Gender: "' + '0' + '", MarStatus: "' + '0' + '", DependMStatus: "' + '0' + '", CityID: "' + '0' + '", Nationality: "' + '0' + '", Sponser: "' + '0' + '", InsuranceNumber: "' + '0' + '", EIDNo: "' + '0' + '" }', //, GemsType: "' + Type + ', Unit: "' + Unit + ', EmployeeID: "' + employeeID + '"                
        contentType: "application/json; charset=utf-8",
        success: function (results) {
            var a = results.d.split('||');
            if (a[0] == '0') {
                if (id == 0) {
                    $('#ADDError').text(a[1]);
                    $('#ADDError').attr("class", "alert alert-info");
                    $('#ADDError0').text(a[1]);
                    $('#ADDError0').attr("class", "alert alert-info");
                    //$('#ADDError').attr("class", "alert alert-success");
                    //$('#divalert2').attr("class", "alert alert-success");
                    //$('#divalert2').css("display", "block");
                } else {
                    //$('#ADDError').attr("class", "alert alert-error");
                    $('#ErrorEditFamily').text(a[1]);
                    $('#ErrorEditFamily').attr("class", "alert alert-info");
                    $('#ErrorEditFamily0').text(a[1]);
                    $('#ErrorEditFamily0').attr("class", "alert alert-info");
                    //$('#divalert3').attr("class", "alert alert-success");
                    //$('#divalert3').css("display", "block");
                }
                if (id == 0) {
                    $('#btnProfSave').css("display", "none");
                    $('#btnAddSaveDep').css("display", "none");
                    $('#btnProfDelete').css("display", "none");
                }
                $('#h_Action').val('1');
            } else {
                if (id == 0) {
                    $('#ADDError').text(a[1]);
                    $('#ADDError').attr("class", "alert alert-danger");
                    $('#ADDError0').text(a[1]);
                    $('#ADDError0').attr("class", "alert alert-danger");
                    //$('#ADDError').attr("class", "alert alert-error");
                    //$('#divalert2').attr("class", "alert alert-danger");
                    //$('#divalert2').css("display", "block");
                } else {
                    $('#ErrorEditFamily').text(a[1]);
                    $('#ErrorEditFamily').attr("class", "alert alert-danger");
                    $('#ErrorEditFamily0').text(a[1]);
                    $('#ErrorEditFamily0').attr("class", "alert alert-danger");
                    //$('#divalert3').attr("class", "alert alert-danger");
                    //$('#divalert3').css("display", "block");
                }
                $('#h_Action').val('1');
            }
        }
    });
}

function Closesa_AddDep() {
    document.getElementById('btnsave2').click();
    if (document.getElementById('h_Action').value == '1') {
        window.location.href = "FeeConcessionPopup_ESS.aspx?id=" + '<%= Session("Encr_Employee_ID")%>';
    }
}

// Confirm on Delete
function ConfirmDelete(id) {
    if (confirm('Are you sure?')) {
        DeleteDependantDetails(id);
    } else {

    }

}

function DeleteDependantDetails(id) {
    if (id != 0) {
        $.ajax({
            type: "POST",
            url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/DeleteDependantDetails",
            data: '{EDDID: "' + id + '" }',
            contentType: "application/json; charset=utf-8",
            success: function (results) {
                var a = results.d.split('||');
                console.log(a[0]);
                console.log(a[1]);
                if (a[0] == '0') {
                    $('#ErrorEditFamily').text(a[1]);
                    $('#ErrorEditFamily').attr("class", "alert alert-info");
                    $('#ErrorEditFamily0').text(a[1]);
                    $('#ErrorEditFamily0').attr("class", "alert alert-info");
                    //$('#divalert3').attr("class", "alert alert-success");
                    //$('#divalert3').css("display", "block");
                    $('#btnProfSave').css("display", "none");
                    $('#btnProfDelete').css("display", "none");
                    $('#h_Action').val('1');
                }
                else {
                    $('#ErrorEditFamily').text(a[1]);
                    $('#ErrorEditFamily').attr("class", "alert alert-danger");
                    $('#ErrorEditFamily0').text(a[1]);
                    $('#ErrorEditFamily0').attr("class", "alert alert-danger");
                    //$('#divalert3').attr("class", "alert alert-danger");
                    //$('#divalert3').css("display", "block");
                    $('#h_Action').val('1');
                }
            }
        });
    }
}

function MyConfirmMethod(hv) {
    Closesa_childList();
    $.ajax({
        type: "POST",
        url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/a",
        data: '{ Id: " ' + hv + ' "}',
        contentType: "application/json; charset=utf-8",
        success: function (results) {
            //useresult(results.d);           
            var a = results.d.split('||');

            $('#slctDDRel').val(a[0]);
            $('#txtDDDOB').val(a[1]);
            $('#slctDDGender').val(a[2]);
            $('#slctDDMrtsts').val(a[3]);
            $('#txtDDUnit').val(a[4]);
            $('#txtDDDepedentName').val(a[5]);
            $('#txtDDNationality').val(a[6]);
            $('#txtDDPasprtNo').val(a[7]);
            $('#txtDDEIDNo').val(a[8]);
            $('#txtDDEDIEXPDt').val(a[9]);
            $('#txtDDUID').val(a[10]);
            $('#txtDDVIsaIsueDt').val(a[11]);
            $('#txtDDVIsaExpDt').val(a[12]);
            $('#txtDDVisaIsuePlc').val(a[13]);
            $('#slctDDVisSpnsr').val(a[14]);
            $('#txtDDInsCrdNo').val(a[15]);
            $('#imgDDProfile').attr("src", "../Payroll/ImageHandler.ashx?ID=" + hv + "&TYPE=EDD");
            $('#btnProfSave').attr('onclick', 'SaveDependantDetails(' + hv + ')');
            $('#btnProfDelete').attr('onclick', 'ConfirmDelete(' + hv + ')');
            $('#h_cont_id').val(a[17]);
            $('#h_stu_ID').val(a[18]);
            $('#h_company_ID').val(a[19]);
            $('#h_emp_id').val(a[20]);
            $('#txtvisafilenumber').val(a[21]);

            if (a[16] == 'N') {
                $('#radDDNGems').prop("checked", true)
                $('#radDDSGems').prop("checked", false)
                $('#txtDDUnit').attr("disabled", "disabled");
                $('#radDDStGems').prop("checked", false)
                $("#txtDDDepedentName").removeAttr("disabled");
                $('.divGems').css("display", "none");
                $(".lblGemsStudId").css('display', 'none');
            } else if (a[16] == 'E') {
                $('#radDDSGems').prop("checked", true)
                $('#radDDNGems').prop("checked", false)
                $('#txtDDUnit').removeAttr("disabled");
                $('#radDDStGems').prop("checked", false)
                $('.divGems').css("display", "block");
                $(".lblDDGemsStaffStudent").text("Staff Id");
                $(".lblGemsStudId").css('display', 'none');
                GetBusinessUnits("E")
            } else if (a[16] == 'S') {
                $('#radDDStGems').prop("checked", true)
                $('#radDDNGems').prop("checked", false)
                $('#txtDDUnit').removeAttr("disabled");
                $('#radDDSGems').prop("checked", false)
                $('.divGems').css("display", "block");
                $(".lblDDGemsStaffStudent").text("Student Id");
                $(".lblGemsStudId").css('display', 'block');
                GetBusinessUnits("S")
            }
            $('#txtDDGEMSStaffIdStudentId').val(a[23]);
            //$("#ddlDDResidenceCountry").val(a[22])
            document.getElementById('<%=ddlDDResidenceCountry.ClientID%>').value = parseInt(a[22])
            document.getElementById('<%=ddlDDNationality.ClientID%>').value = parseInt(a[17])
            setTimeout(function () {
                document.getElementById('<%=ddlDDUnit.ClientID%>').value = parseInt(a[19])
            }, 1000);

            $('#txtDDDepedentName').val(a[5]);

            if (parseInt(a[22]) == 172) {
                $('.divUAE').css("display", "block");
                //changeEvent(0);
            } else $('.divUAE').css("display", "none");
            //if (a[16] == 'E' || a[16] == 'S') {
            //    getDependentName(0);
            //}
            //changeEvent(0);

            //$("#ddlDDUnit").val(a[19])
                    <%--$('#<%= txtEventName.ClientID%>').val(a[0]);
                    $('#<%= txtEventDesc.ClientID%>').val(a[1]);
                    $('#<%= txtAmount.ClientID%>').val(a[3]);
                    if (String(a[4]) == "Yes") {
                        $('#<%=radioButtonList.ClientID%>').find("input[value='Yes']").prop("checked", true);
                    }
                    else {
                        $('#<%=radioButtonList.ClientID%>').find("input[value='No']").prop("checked", true);
                    }
                    $("#<%= ddlActivtyTypes.ClientID%>").val('MUSIC').attr("selected", "selected");--%>
        }
    });
    return true;
}

function GetBusinessUnits(prefix) {

    //$('#txtDDUnit').autocomplete({
    //source: function (request, response) {
    var SearchFor = ""
    //var prefix = "";
    $('#h_company_ID').val(0);

    $.ajax({
        url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/GetCompany",
        data: "{ 'prefix': '" + prefix + "','userName':'" + '<%= Session("sUsr_name")%>' + "', 'SearchFor':'" + SearchFor + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (response) {

            var arraylength = response.d.length;
            $('#<%= ddlDDUnit.ClientID %>').empty();
            $('#<%= ddlAddUnit.ClientID %>').empty();
            if (arraylength > 0) {
                //$('#dvStudents').append("<div class='container'>");
                for (var i = 0; i < arraylength; i++) {
                    var newOption = "<option value='" + response.d[i].split("||")[1] + "'>" + response.d[i].split("||")[0] + "</option>";
                    $('#<%= ddlDDUnit.ClientID %>').append(newOption);
                    $('#<%= ddlAddUnit.ClientID %>').append(newOption);
                }
            }
        },
        error: function (response) {
            alert(response.responseText);
        },
        failure: function (response) {
            alert(response.responseText);
        }

    });
}
function GetCountry() {
    var prefix = "";
    $('#h_cont_id').val(0);
    $.ajax({
        url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/GetCountry",
        data: "{ 'prefix': '" + prefix + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var arraylength = response.d.length;
            $('#<%= ddlDDResidenceCountry.ClientID %>').empty();
            $('#<%= ddlAddResidenceCountry.ClientID %>').empty();
            $('#<%= ddlDDNationality.ClientID %>').empty();
            $('#<%= ddlAddNationality.ClientID %>').empty();

            if (arraylength > 0) {

                //$('#dvStudents').append("<div class='container'>");
                for (var i = 0; i < arraylength; i++) {
                    var newOption = "<option value='" + response.d[i].split("||")[1] + "'>" + response.d[i].split("||")[0] + "</option>";
                    $('#<%= ddlDDResidenceCountry.ClientID %>').append(newOption);
                    $('#<%= ddlAddResidenceCountry.ClientID %>').append(newOption);
                    $('#<%= ddlDDNationality.ClientID %>').append(newOption);
                    $('#<%= ddlAddNationality.ClientID %>').append(newOption);
                }
                document.getElementById('<%=ddlAddResidenceCountry.ClientID%>').value = parseInt(document.getElementById('<%=h_bsu_cty_id.ClientID%>').value)
                $('#<%= ddlDDResidenceCountry.ClientID %>').change();
                $('#<%= ddlAddResidenceCountry.ClientID %>').change();
            }
        },
        error: function (response) {
            alert(response.responseText);
        },
        failure: function (response) {
            alert(response.responseText);
        }
    });

}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


    <script>

        function ClosesApplyAddDepdentant() {
            ClosePopup2();
        }
        function ApplyConcession(id, emp_id) {
            //alert(emp_id);
            if (id != '') {
                var Idval = id
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/GemsEss/FeeConcessionPopup_ESS.aspx/ApplyConcession")%>',
                    data: '{EMP_ID: "' + emp_id + '", STU_NO: "' + id + '"  }',
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                        var a = results.d.split('||');
                        if (a[0] == '0') {
                            $('#' + id).css("display", "none");
                            $('#' + 'IMG' + id).css("display", "block");
                            $('#LBLError2').text(a[1]);
                            $('#divalert').attr("class", "alert alert-success");
                            $('#divalert').css("display", "block");

                        }
                        else {

                            $('#LBLError2').text(a[1]);
                            $('#divalert').attr("class", "alert alert-danger");
                            $('#divalert').css("display", "block");
                        }
                    }
                });
            }
        }


        function CallGrid() {

            $.ajax({
                url: "/PHOENIXBETA/GemsEss/FeeConcessionPopup_ESS.aspx/CallGrid",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var arraylength = response.d.length;

                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }

        // Close Edit Dep

        function Closesa_EditDep() {
            document.getElementById('btnSave3').click();
            if (document.getElementById('h_Action').value == '1') {
                window.location.href = "FeeConcessionPopup_ESS.aspx?id=" + '<%= Session("Encr_Employee_ID")%>';
            }
            //FeeConcessionPopup('<%= Session("EmployeeID")%>');
        }
        function ChktxtDDPasprtNo() {
            var control = $('#txtDDPasprtNo').val();
            if (control == '') {
                alert("Please Enter Passport Number");
                $('#txtDDPasprtNo').val("");
                return true;
            }
            if (control.trim() == '') {
                alert("Invalid Passport Number");
                $('#txtDDPasprtNo').val("");
                return true;
            }
        }



       <%-- function Callupdatecurrentmonthandyear() {
            var icurYear = '<%= Session("BSU_PAYYEAR")%>'
            var d = new Date();
            var n = d.getMonth();
            var Y = d.getFullYear();
            n = n + 1;
            if (n == 1) {
                $("#calndrMonth option[value='Jan']").attr('selected', 'selected');

            } else if (n == 2) {
                $("#calndrMonth option[value='Feb']").attr('selected', 'selected');

            } else if (n == 3) {
                $("#calndrMonth option[value='Mar']").attr('selected', 'selected');

            } else if (n == 4) {
                $("#calndrMonth option[value='Apr']").attr('selected', 'selected');

            } else if (n == 5) {
                $("#calndrMonth option[value='May']").attr('selected', 'selected');

            } else if (n == 6) {
                $("#calndrMonth option[value='Jun']").attr('selected', 'selected');

            } else if (n == 7) {
                $("#calndrMonth option[value='Jul']").attr('selected', 'selected');

            } else if (n == 8) {
                $("#calndrMonth option[value='Aug']").attr('selected', 'selected');

            } else if (n == 9) {
                $("#calndrMonth option[value='Sep']").attr('selected', 'selected');

            } else if (n == 10) {
                $("#calndrMonth option[value='Oct']").attr('selected', 'selected');

            } else if (n == 11) {
                $("#calndrMonth option[value='Nov']").attr('selected', 'selected');

            } else if (n == 12) {
                $("#calndrMonth option[value='Dec']").attr('selected', 'selected');

            }


            $("#calndrYear option[value='" + icurYear + "']").attr('selected', 'selected');

            loadcalenderwithcolor();
        }

        function loadcalenderwithcolor() {

        }--%>





        //function CalnderMonthhange() {
        //    // alert($('#calndrMonth').val());
        //    var e = document.getElementById("calndrMonth");
        //    var strUser = e.options[e.selectedIndex].value;


        //    var e1 = document.getElementById("calndrYear");
        //    var strUser1 = e1.options[e1.selectedIndex].value;


        //    var monthsel;
        //    if (strUser == "Jan") {
        //        monthsel = 0;
        //    } else if (strUser == "Feb") {
        //        monthsel = 1;
        //    } else if (strUser == "Mar") {
        //        monthsel = 2;
        //    } else if (strUser == "Apr") {
        //        monthsel = 3;
        //    } else if (strUser == "May") {
        //        monthsel = 4;
        //    } else if (strUser == "Jun") {
        //        monthsel = 5;
        //    } else if (strUser == "Jul") {
        //        monthsel = 6;
        //    } else if (strUser == "Aug") {
        //        monthsel = 7;
        //    } else if (strUser == "Sep") {
        //        monthsel = 8;
        //    } else if (strUser == "Oct") {
        //        monthsel = 9;
        //    } else if (strUser == "Nov") {
        //        monthsel = 10;
        //    } else if (strUser == "Dec") {
        //        monthsel = 11;
        //    }

        //    //var date = new Date();
        //    var firstDay = new Date(strUser1, monthsel, 1);
        //    var lastDay = new Date(strUser1, monthsel + 1, 0);
        //    var dayse = new Date(strUser1, monthsel + 1, 0);
        //    var firstDayWithSlashes = ((firstDay.getMonth() + 1) + '/' + firstDay.getDate()) + '/' + firstDay.getFullYear();
        //    var lastDayWithSlashes = ((lastDay.getMonth() + 1) + '/' + lastDay.getDate()) + '/' + lastDay.getFullYear();


        //    $.ajax({
        //        type: "POST",
        //        url: "/PHOENIXBETA/GemsEss/Index.aspx/ShowLeaveCalculationWeb",
        //        data: '{FromDate: "' + firstDayWithSlashes + '", ToDate: "' + lastDayWithSlashes + '"}',
        //        contentType: "application/json; charset=utf-8",
        //        success: function (response) {
        //            var arraylength = response.d.length;
        //            var caldays = document.getElementById('caldays')
        //            caldays.innerHTML = ""
        //            var date1 = new Date();
        //            var todaydate = date1.getDate();
        //            if (arraylength > 0) {
        //                for (var i = 0; i < arraylength; i++) {
        //                    var dated = response.d[i].split("||")[0];
        //                    var dayd = response.d[i].split("||")[1];
        //                    var Remarks = response.d[i].split("||")[2];
        //                    var backcolor = response.d[i].split("||")[3];
        //                    var forecolor = response.d[i].split("||")[4];
        //                    var id = response.d[i].split("||")[5];
        //                    if (i < dayse.getDate()) {

        //                        if (dayd == "Sunday") {
        //                            var lis = document.createElement('li');
        //                            lis.value = id;
        //                            lis.innerHTML = id;
        //                            lis.id = "lvli" + id;
        //                            if (backcolor != "") {
        //                                lis.style.backgroundColor = backcolor;
        //                                lis.style.color = forecolor;
        //                            }
        //                            if (Remarks != "") {
        //                                lis.title = Remarks;
        //                            }
        //                            if (id == todaydate.toString()) {
        //                                // lis.attributes("class", ".days li .active");
        //                                //  lis.className = ".days li .activeli";
        //                                lis.className = "activeli"
        //                            }
        //                            caldays.appendChild(lis);

        //                        } else if (dayd == "Monday") {
        //                            if (id == "1") {
        //                                var lis = document.createElement('li');
        //                                lis.value = "";
        //                                lis.className = "border-0";
        //                                lis.innerHTML = "";
        //                                caldays.appendChild(lis);

        //                                var lis1 = document.createElement('li');
        //                                lis1.value = id;
        //                                lis1.innerHTML = id;
        //                                lis1.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis1.style.backgroundColor = backcolor;
        //                                    lis1.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis1.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis1.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis1);
        //                            }
        //                            else {
        //                                var lis1 = document.createElement('li');
        //                                lis1.value = id;
        //                                lis1.innerHTML = id;
        //                                lis1.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis1.style.backgroundColor = backcolor;
        //                                    lis1.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis1.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis1.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis1);
        //                            }
        //                        } else if (dayd == "Tuesday") {
        //                            if (id == "1") {
        //                                var lis = document.createElement('li');
        //                                lis.value = "";
        //                                lis.className = "border-0";
        //                                lis.innerHTML = "";
        //                                caldays.appendChild(lis);

        //                                var lis1 = document.createElement('li');
        //                                lis1.value = "";
        //                                lis1.className = "border-0";
        //                                lis1.innerHTML = "";
        //                                caldays.appendChild(lis1);

        //                                var lis2 = document.createElement('li');
        //                                lis2.value = id;
        //                                lis2.innerHTML = id;
        //                                lis2.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis2.style.backgroundColor = backcolor;
        //                                    lis2.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis2.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis2.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis2);
        //                            }
        //                            else {
        //                                var lis2 = document.createElement('li');
        //                                lis2.value = id;
        //                                lis2.innerHTML = id;
        //                                lis2.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis2.style.backgroundColor = backcolor;
        //                                    lis2.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis2.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis2.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis2);
        //                            }

        //                        } else if (dayd == "Wednesday") {
        //                            if (id == "1") {
        //                                var lis = document.createElement('li');
        //                                lis.value = "";
        //                                lis.className = "border-0";
        //                                lis.innerHTML = "";
        //                                caldays.appendChild(lis);

        //                                var lis1 = document.createElement('li');
        //                                lis1.value = "";
        //                                lis1.className = "border-0";
        //                                lis1.innerHTML = "";
        //                                caldays.appendChild(lis1);

        //                                var lis2 = document.createElement('li');
        //                                lis2.value = "";
        //                                lis2.className = "border-0";
        //                                lis2.innerHTML = "";
        //                                caldays.appendChild(lis2);

        //                                var lis3 = document.createElement('li');
        //                                lis3.value = id;
        //                                lis3.innerHTML = id;
        //                                lis3.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis3.style.backgroundColor = backcolor;
        //                                    lis3.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis3.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis3.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis3);
        //                            }
        //                            else {
        //                                var lis3 = document.createElement('li');
        //                                lis3.value = id;
        //                                lis3.innerHTML = id;
        //                                lis3.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis3.style.backgroundColor = backcolor;
        //                                    lis3.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis3.title = Remarks;
        //                                }
        //                                if (id == todaydate) {

        //                                    //  lis.attributes("class", ".days li .active");
        //                                    // lis3.className = ".days li .activeli";
        //                                    lis3.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis3);
        //                            }

        //                        } else if (dayd == "Thursday") {
        //                            if (id == "1") {
        //                                var lis = document.createElement('li');
        //                                lis.value = "";
        //                                lis.className = "border-0";
        //                                lis.innerHTML = "";
        //                                caldays.appendChild(lis);

        //                                var lis1 = document.createElement('li');
        //                                lis1.value = "";
        //                                lis1.className = "border-0";
        //                                lis1.innerHTML = "";
        //                                caldays.appendChild(lis1);

        //                                var lis2 = document.createElement('li');
        //                                lis2.value = "";
        //                                lis2.className = "border-0";
        //                                lis2.innerHTML = "";
        //                                caldays.appendChild(lis2);

        //                                var lis3 = document.createElement('li');
        //                                lis3.value = "";
        //                                lis3.className = "border-0";
        //                                lis3.innerHTML = "";
        //                                caldays.appendChild(lis3);

        //                                var lis4 = document.createElement('li');
        //                                lis4.value = id;
        //                                lis4.innerHTML = id;
        //                                lis4.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis4.style.backgroundColor = backcolor;
        //                                    lis4.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis4.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis4.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis4);
        //                            }
        //                            else {
        //                                var lis4 = document.createElement('li');
        //                                lis4.value = id;
        //                                lis4.innerHTML = id;
        //                                lis4.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis4.style.backgroundColor = backcolor;
        //                                    lis4.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis4.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    lis4.className = "activeli";

        //                                }
        //                                caldays.appendChild(lis4);
        //                            }
        //                        } else if (dayd == "Friday") {
        //                            if (id == "1") {
        //                                var lis = document.createElement('li');
        //                                lis.value = "";
        //                                lis.className = "border-0";
        //                                lis.innerHTML = "";
        //                                caldays.appendChild(lis);

        //                                var lis1 = document.createElement('li');
        //                                lis1.value = "";
        //                                lis1.className = "border-0";
        //                                lis1.innerHTML = "";
        //                                caldays.appendChild(lis1);

        //                                var lis2 = document.createElement('li');
        //                                lis2.value = "";
        //                                lis2.className = "border-0";
        //                                lis2.innerHTML = "";
        //                                caldays.appendChild(lis2);

        //                                var lis3 = document.createElement('li');
        //                                lis3.value = "";
        //                                lis3.className = "border-0";
        //                                lis3.innerHTML = "";
        //                                caldays.appendChild(lis3);

        //                                var lis4 = document.createElement('li');
        //                                lis4.value = "";
        //                                lis4.className = "border-0";
        //                                lis4.innerHTML = "";
        //                                caldays.appendChild(lis4);

        //                                var lis5 = document.createElement('li');
        //                                lis5.value = id;
        //                                lis5.innerHTML = id;
        //                                lis5.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis5.style.backgroundColor = backcolor;
        //                                    lis5.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis5.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis5.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis5);
        //                            }
        //                            else {
        //                                var lis5 = document.createElement('li');
        //                                lis5.value = id;
        //                                lis5.innerHTML = id;
        //                                lis5.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis5.style.backgroundColor = backcolor;
        //                                    lis5.style.color = forecolor;
        //                                }

        //                                if (Remarks != "") {
        //                                    lis5.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis5.className = "activeli"
        //                                }
        //                                caldays.appendChild(lis5);
        //                            }

        //                        } else if (dayd == "Saturday") {
        //                            if (id == "1") {
        //                                var lis = document.createElement('li');
        //                                lis.value = "";
        //                                lis.className = "border-0";
        //                                lis.innerHTML = "";
        //                                caldays.appendChild(lis);

        //                                var lis1 = document.createElement('li');
        //                                lis1.value = "";
        //                                lis1.className = "border-0";
        //                                lis1.innerHTML = "";
        //                                caldays.appendChild(lis1);

        //                                var lis2 = document.createElement('li');
        //                                lis2.value = "";
        //                                lis2.className = "border-0";
        //                                lis2.innerHTML = "";
        //                                caldays.appendChild(lis2);

        //                                var lis3 = document.createElement('li');
        //                                lis3.value = "";
        //                                lis3.className = "border-0";
        //                                lis3.innerHTML = "";
        //                                caldays.appendChild(lis3);

        //                                var lis4 = document.createElement('li');
        //                                lis4.value = "";
        //                                lis4.className = "border-0";
        //                                lis4.innerHTML = "";
        //                                caldays.appendChild(lis4);

        //                                var lis5 = document.createElement('li');
        //                                lis5.value = "";
        //                                lis5.className = "border-0";
        //                                lis5.innerHTML = "";
        //                                caldays.appendChild(lis5);

        //                                var lis6 = document.createElement('li');
        //                                lis6.value = id;
        //                                lis6.innerHTML = id;
        //                                lis6.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis6.style.backgroundColor = backcolor;
        //                                    lis6.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis6.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis6.className = "activeli"

        //                                }
        //                                caldays.appendChild(lis6);
        //                            }
        //                            else {
        //                                var lis6 = document.createElement('li');
        //                                lis6.value = id;
        //                                lis6.innerHTML = id;
        //                                lis6.id = "lvli" + id;
        //                                if (backcolor != "") {
        //                                    lis6.style.backgroundColor = backcolor;
        //                                    lis6.style.color = forecolor;
        //                                }
        //                                if (Remarks != "") {
        //                                    lis6.title = Remarks;
        //                                }
        //                                if (id == todaydate.toString()) {
        //                                    // lis.attributes("class", ".days li .active");
        //                                    //  lis.className = ".days li .activeli";
        //                                    lis6.className = "activeli"

        //                                }
        //                                caldays.appendChild(lis6);
        //                            }
        //                        }
        //                    }
        //                    else {

        //                        if ((dated.indexOf(strUser) != -1) && (dated.split('/')[2].indexOf(strUser1.slice(-2)) != -1)) {

        //                            var datetocheck = dated.split('/')[0];
        //                            var idtocheck = 'lvli' + (Number(datetocheck)).toString();
        //                            var leavedateli = document.getElementById(idtocheck);
        //                            leavedateli.style.backgroundColor = backcolor;
        //                            leavedateli.style.color = forecolor;
        //                            leavedateli.title = Remarks;
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //    });
        //    ShowCalendarEvents(firstDayWithSlashes, lastDayWithSlashes);
        //}




        //function ShowCalendarEvents(fromdate, todate) {

        //    $.ajax({
        //        type: "POST",
        //        url: "/PHOENIXBETA/GemsEss/Index.aspx/ShowCalendarEvents",
        //        data: '{FromDate: "' + fromdate + '", ToDate: "' + todate + '"}',
        //        contentType: "application/json; charset=utf-8",
        //        success: function (results) {
        //            var a = results.d.split('||');
        //            if (a[0] != "") {
        //                $('#spnShowEvents').html(a[0]);

        //            } else {
        //                $('#spnShowEvents').html("");
        //            }
        //        }
        //    });
        //}

        function ChktxtDDInsCrdNo() {
            //var control = $('#txtDDInsCrdNo').val();
            //if (control == '') {
            //    alert("Please Enter Insurance Number");
            //    $('#txtDDInsCrdNo').val("");
            //    return true;
            //}
            //if (control.trim() == '') {
            //    alert("Invalid Insurance Number Entered");
            //    $('#txtDDInsCrdNo').val("");
            //    return true;
            //}
        }

        function getBase64Image(id) {
            if (id == 0) {
                var canvas = document.createElement("canvas");
                img1 = document.getElementById('imgAddProfile');

                canvas.width = img1.width;
                canvas.height = img1.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img1, 0, 0, img1.width, img1.height);
                var dataURL = canvas.toDataURL("image/png");
                return dataURL;
            } else {
                var canvas = document.createElement("canvas");
                img1 = document.getElementById('imgDDProfile');

                canvas.width = img1.width;
                //alert(img1.width);
                canvas.height = img1.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img1, 0, 0, img1.width, img1.height);
                var dataURL = canvas.toDataURL("image/png");
                return dataURL;
            }
        }
    </script>

    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsValidVisaFileNo(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 43) || (keyCode == 47) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            //document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        }
    </script>
</asp:Content>
