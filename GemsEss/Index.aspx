﻿<%@ Page Title="" Language="VB" MasterPageFile="~/GemsEss/GemsEss.master" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="GemsEss_Index" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .ModalConfirmCls {
            position: absolute !important;
            left: 30% !important;
            top: 50% !important;
            height: 125px !important;
            border-radius: 6px;
            border: 1px solid rgba(0,0,0,0.1);
            box-shadow: 8px 8px 7px rgba(0,0,0,0.03);
            z-index: 99999 !important;
        }

        .form-control:disabled, .form-control[readonly] {
            background-color: #f5f5f5 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--style for calender start--%>
    <style>
        #divalert, #divalert2 {
            /*position: fixed;
    top: 100px;
    left: 45%;
    z-index: 1000;
    min-width: 200px;*/
            position: fixed;
            margin: auto;
            left: 40%;
            z-index: 1100;
            top: 35%;
            padding: 8px !important;
        }

        * {
            box-sizing: border-box;
        }

        ul {
            list-style-type: none;
        }


        .month {
            /*padding: 70px 25px;*/
            width: 100%;
            /*background: #1abc9c;*/
            text-align: center;
        }

            .month ul {
                margin: 0;
                padding: 0;
            }

                .month ul li {
                    color: white;
                    font-size: 20px;
                    text-transform: uppercase;
                    letter-spacing: 3px;
                }

            .month .prev {
                float: left;
                padding-top: 10px;
            }

            .month .next {
                float: right;
                padding-top: 10px;
            }

        .weekdays {
            margin: 0;
            padding: 0px;
            background-color: #ddd;
            border: 1px solid #ccc;
            width: 280px;
        }

            .weekdays li {
                /*display: inline-block;
                width: 11.5%;
                color: #000;
                text-align: center;*/
                list-style-type: none;
                display: inline-block;
                width: 35px;
                text-align: center;
                font-size: 14px;
                margin: 0px;
                margin-right: 2px;
                color: #000;
                padding: 8px 2px;
                /*border: 1px solid #ccc;*/
            }

        .days {
            /*padding: 10px 0;
            background: #eee;*/
            margin: 0;
            padding: 0;
            border: 1px solid #ccc;
            /*width: 102%;*/
            width: 282px;
        }

            .days li {
                list-style-type: none;
                display: inline-block;
                /*width: 13%;*/
                width: 40px;
                text-align: center;
                /*margin-bottom: 5px;*/
                font-size: 12px;
                color: #000;
                padding: 6px;
                border: 1px solid #ccc;
            }

                .days li .activeli {
                    padding: 9px;
                    background: #1abc9c;
                    color: white !important;
                }

        select.calndrMonth {
            font-size: 80% !important;
        }
        /* Add media queries for smaller screens */
        @media screen and (max-width:720px) {
            .days li {
                width: 40px;
            }

            .weekdays li {
                width: 35px;
            }
        }

        @media screen and (max-width: 420px) {
            .days li {
                width: 40px;
            }

            .weekdays li {
                width: 35px;
            }

            .days li .activeli {
                padding: 2px;
            }
        }

        @media screen and (max-width: 290px) {
            .days li {
                width: 40px;
            }

            .weekdays li {
                width: 35px;
            }
        }

        .fancybox-inner {
            width: 100% !important;
        }
    </style>
    <%--style for calender ends--%>
    <style>
        .alert {
            padding: 0px !important;
        }

        .auto-comp-popup {
            background-color: #ffffff;
            border: 1px solid rgba(0,0,0,0.07);
            overflow: hidden;
            overflow-y: scroll;
            max-height: 250px;
            position: absolute;
            z-index: 1000;
            max-width: 354px;
            min-width: 354px;
        }

        .container .auto-comp-link {
            background-color: transparent;
            border: 0;
            padding: 4px 0;
            display: block;
        }



        /*Calendar css style*/


        .calPrevMth {
            text-decoration: none;
            font-size: 12px;
            padding-left: 10px;
            padding-right: 6px;
            /*background: transparent url(../Images/common/MthPrev.png) no-repeat -1px -1px;*/
            color: black;
            float: left;
            line-height: 23px;
        }

        .calNxtMth {
            text-decoration: none;
            font-size: 12px;
            /*background: transparent url(../Images/common/MthNext.png) no-repeat 15px 0px;*/
            color: black;
            padding-left: 6px;
            padding-right: 10px;
            /*background: transparent url(../Images/common/MthNext.png) no-repeat 30px -1px;*/
            float: right;
            line-height: 23px;
        }

        .calMthYear {
            margin: 0px;
            width: 100%;
            height: 24px;
            text-align: left;
            color: #1d4070;
            padding-left: 1px;
            padding-top: 2px;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            border-bottom: 1px solid #b9b9b9;
            border-left: 1px solid #e6e6e6;
            border-right: 1px solid #e6e6e6;
            border-top: 1px solid #e6e6e6;
            font-size: 18px;
        }

        .calDayStyle {
            border: 1px solid #e6e6e6;
            height: 24px;
            width: 20px;
            text-decoration: none;
            font-size: 14px;
            text-align: center;
            color: #000;
            background-color: #ffffff;
        }

            .calDayStyle:hover {
                color: #000 !important;
                background: #e7fd7e;
                background: -moz-linear-gradient(top, #e7fd7e 0%, #e0fb5e 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e7fd7e), color-stop(100%,#e0fb5e));
                background: -webkit-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: -o-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: -ms-linear-gradient(top, #e7fd7e 0%,#e0fb5e 100%);
                background: linear-gradient(to bottom, #e7fd7e 0%,#e0fb5e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e7fd7e', endColorstr='#e0fb5e',GradientType=0 );
            }

        .calDayToday {
            color: #000;
            font-size: 13px;
            text-decoration: none;
            background: #85b9ff;
            border: none;
        }

            .calDayToday a {
                color: #000 !important;
            }

        .calWeekTitle {
            text-align: center;
            color: #fff;
            font-weight: bold;
            border: 0px none;
            font-size: 14px;
            padding: 6px;
            background: #75a9d1;
        }

        .calWeekendstyle {
            color: #ffffff !important;
            font-weight: bold;
            border-color: #0c0;
            background: #88e888;
            text-align: center !important;
            vertical-align: middle !important;
        }

        .calOtherDay {
            border: 1px solid #e6e6e6;
            height: 24px;
            width: 20px;
            text-decoration: none;
            font-size: 14px;
            text-align: center;
            background: #eeeeee;
        }

            .calOtherDay a {
                color: #8c8c8a !important;
            }

        .calEventDay {
            float: left;
            font-size: 12px;
            color: /*#317abf*/ #222222;
            font-weight: normal;
            text-align: center;
            line-height: 18px;
        }

        .calEventName {
            float: left;
            padding-left: 4px;
            padding-bottom: 1px;
            font-size: 12px;
            color: /*#2b8dd8*/ #222222;
            font-weight: normal;
            letter-spacing: 1px;
        }

        .calEventBoxMain {
            margin-left: 5px;
            margin-top: 7px;
            margin-bottom: 7px;
        }

        .calEventBoxSub {
            border-bottom: 1px solid #e6e6e6;
            width: 100%;
            float: left;
            vertical-align: middle;
        }

        .calEventColor {
            /*border: 1px solid #fff;*/
            width: 8px;
            height: 8px;
            display: block;
            float: left;
            margin: 6px 0;
        }

        .calEventDate {
            font-size: 11px;
            color: rgba(62, 64, 65, 1);
            font-weight: normal;
            padding-left: 12px;
        }
    </style>
    <!-- Page Wrapper -->

    <%-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>--%>



    <div class="page-wrapper">
        <input type="hidden" id="h_Action" value="0" />
        <asp:HiddenField ID="h_EmpNoDesc" runat="server" />
        <!--style="background-image: linear-gradient(#eef1f3,#deeaf5,#deeaf5,#e8eef5);"-->
        <!-- Page Content -->

        <div class="content container-fluid">
            <div class="row">
                <%-- Left Side Starts here --%>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="row" id="MainProfileDiv" runat="server">
                                <div class="col-lg-12">
                                    <div class="profile-page" style="padding: 0px;">
                                        <div class="row">

                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                                <div class="card profile-header card-border">
                                                    <div class="body">
                                                        <div class="row">
                                                            <asp:Repeater ID="rptView_profile" runat="server">
                                                                <ItemTemplate>
                                                                    <div class="col-lg-2 col-md-2 col-2">
                                                                        <div class="profile-image float-md-center">
                                                                            <a data-toggle="modal" data-target="#View_Profile">
                                                                                <img src='<%# Eval("EMD_PHOTO")%>' alt="" style="height: 100px;"></a>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-2-offset col-md-2 col-2">
                                                                    </div>

                                                                    <div class="col-lg-8 col-md-8 col-8">

                                                                        <h4 class="m-t-10 m-b-5"><strong><%# Eval("EMP_NAME")%></strong></h4>
                                                                        <span><%# Eval("DES_DESCR")%></span>
                                                                        <br />
                                                                        <%# Eval("DPT_DESCR")%>
                                                                        <div class="row">
                                                                            <div runat="server" id="divMyTeam" class="col-6 text-right my-team-btn p-0" style="display: none;"><a href="MyTeamDetails.aspx" style="color: white !important;" class="btn btn-primary btn-round float-right btn-sm cursor-pointer" id="myTeamBtn">My Team</a></div>
                                                                            <div class="col text-right pl-0">
                                                                                <input type="button" value="View Profile" class="btn btn-primary btn-round float-right btn-sm btnEmployee" data-toggle="modal" data-target="#View_Profile">
                                                                            </div>
                                                                            <!--<button class="btn btn-primary btn-round btn-simple">Message</button>-->
                                                                            <%--           <asp:Button ID="btnPopup" runat="server" class="btn btn-primary btn-round float-md-right btn-sm" data-toggle="modal" data-target="#View_Profile" Text="View Profile" />--%>
                                                                        </div>

                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <div>
                                                            </div>
                                                            <%--    <input class="btn btn-primary btn-round float-md-right btn-sm" data-toggle="modal" data-target="#View_Profile" type="button" value="View Profile" id="GetEmployeeDates" runat="server"  />--%>
                                                            <%--    <asp:Button ID="GetEmployeeDates" runat="server" class="btn btn-primary btn-round float-md-right btn-sm" data-toggle="modal" data-target="#View_Profile" Text="View Profile" />--%>
                                                            <%-- <asp:Button ID="Button1" runat="server" Text="Button"  data-toggle="modal" data-target="#View_Profile"  />--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="MainPayslipDiv" runat="server">
                                <div class="col-md-12">
                                    <div class="card-box" style="padding: 12px;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-6 col-lg-6 mb-4 m-auto">
                                                        <h3 class="card-title mb-3">My Payslips</h3>
                                                    </div>
                                                    <%-- <asp:dropdownlist runat="server" class="form-control" id="slctYear"  AutoPostBack="true"  >                                            
                                        </asp:dropdownlist>--%>
                                                    <div class="col-6 col-lg-6 mb-4">
                                                        <select id="slctYear" runat="server" class="form-control" onchange="ChangeYear();">
                                                        </select>
                                                    </div>
                                                </div>
                                                <article id="demo-carousel" class="demo custom-table mb-0">
                                                    <div id="carousel">
                                                        <asp:HiddenField ID="hdnYear" runat="server" Value="" />
                                                        <asp:HiddenField ID="hdnMonth" runat="server" Value="" />
                                                        <ul>

                                                            <li id="liJan" data-flip-title="Jan">
                                                                <a class="ButtonFlipster Block" id="payJan" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payJan_ServerClick"--%>
                                                                    <h1><span id="spnJan" runat="server"></span></h1>
                                                                </a>
                                                                <h1>JAN</h1>
                                                            </li>
                                                            <li id="liFeb" data-flip-title="Feb">
                                                                <a class="ButtonFlipster Block" id="payFeb" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payFeb_ServerClick"--%>
                                                                    <h1><span id="spnfeb" runat="server"></span></h1>
                                                                </a>
                                                                <h1>FEB</h1>
                                                            </li>
                                                            <li id="liMar" data-flip-title="Mar">
                                                                <a class="ButtonFlipster Block" id="payMar" runat="server" data-toggle="modal" data-target="#No_Salary"><%-- onserverclick="payMar_ServerClick"--%>
                                                                    <h1><span id="spnmar" runat="server"></span></h1>
                                                                </a>
                                                                <h1>MAR</h1>
                                                            </li>
                                                            <li id="liApr" data-flip-title="Apr">
                                                                <a class="ButtonFlipster Block" id="payApr" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payApr_ServerClick"--%>
                                                                    <h1><span id="spnapr" runat="server"></span></h1>
                                                                </a>
                                                                <h1>APR</h1>
                                                            </li>
                                                            <li id="liMay" data-flip-title="May">
                                                                <a class="ButtonFlipster Block" id="payMay" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payMay_ServerClick"--%>
                                                                    <h1><span id="spnmay" runat="server"></span></h1>
                                                                </a>
                                                                <h1>MAY</h1>
                                                            </li>
                                                            <li id="liJun" data-flip-title="Jun">
                                                                <a class="ButtonFlipster Block" id="payJun" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payJun_ServerClick"--%>
                                                                    <h1><span id="spnJun" runat="server"></span></h1>
                                                                </a>
                                                                <h1>JUN</h1>
                                                            </li>
                                                            <li id="liJul" data-flip-title="Jul">
                                                                <a class="ButtonFlipster Block" id="payJul" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payJul_ServerClick"--%>
                                                                    <h1><span id="spnJul" runat="server"></span></h1>
                                                                </a>
                                                                <h1>JUL</h1>
                                                            </li>
                                                            <li id="liAug" data-flip-title="Aug">
                                                                <a class="ButtonFlipster Block" id="payAug" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payAug_ServerClick"--%>
                                                                    <h1><span id="spnaug" runat="server"></span></h1>
                                                                </a>
                                                                <h1>AUG</h1>
                                                            </li>
                                                            <li id="liSep" data-flip-title="Sep">
                                                                <a class="ButtonFlipster Block" id="paySep" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="paySep_ServerClick"--%>
                                                                    <h1><span id="spnsep" runat="server"></span></h1>
                                                                </a>
                                                                <h1>SEP</h1>
                                                            </li>
                                                            <li id="liOct" data-flip-title="Oct">
                                                                <a class="ButtonFlipster Block" id="payOct" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payOct_ServerClick"--%>
                                                                    <h1><span id="spnoct" runat="server"></span></h1>
                                                                </a>
                                                                <h1>OCT</h1>
                                                            </li>
                                                            <li id="liNov" data-flip-title="Nov">
                                                                <a class="ButtonFlipster Block" id="payNov" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payNov_ServerClick"--%>
                                                                    <h1><span id="spnnov" runat="server"></span></h1>
                                                                </a>
                                                                <h1>NOV</h1>
                                                            </li>
                                                            <li id="liDec" data-flip-title="Dec">
                                                                <a class="ButtonFlipster Block" id="payDec" runat="server" data-toggle="modal" data-target="#No_Salary"><%--onserverclick="payDec_ServerClick"--%>
                                                                    <h1><span id="spndec" runat="server"></span></h1>
                                                                </a>
                                                                <h1>DEC</h1>
                                                            </li>
                                                        </ul>
                                                    </div>


                                                </article>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row" id="show_leave_status" runat="server">
                        <div class="col-lg-12">
                            <div class="card-box" id="MainLeaveStatusDiv" runat="server">
                                <div class="row">
                                    <div class="col-6 col-lg-6 mb-2">
                                        <h3 class="card-title">Leave Status</h3>
                                    </div>
                                    <%-- <asp:dropdownlist runat="server" class="form-control" id="slctYear"  AutoPostBack="true"  >                                            
                                        </asp:dropdownlist>--%>
                                    <div class="col-6 col-lg-6 mb-4 pr-2">
                                        <a style="color: white !important;" class="btn btn-primary btn-round float-right btn-sm cursor-pointer" id="LoadLeave" onclick="LeaveHistoryPopup('<%= Session("EmployeeID")%>'); ">Leave History</a>
                                    </div>
                                </div>


                                <div id="pie-charts" style="height: 200px;">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row" id="divMyTeam" runat="server">
                        <div class="col-lg-12">
                            <div class="card-box" id="Div2" runat="server">
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="card-title mb-3">My Team Leave Request </h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive min-max-vh-280">
                                            <table id="tbl_Request1" class="table table-striped custom-table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Team Member</th>
                                                        <th>Leave Type</th>
                                                        <th class="text-center">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tbody>
                                                    <asp:Repeater ID="rpt_LeaveRequest" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <h2 class="table-avatar">
                                                                        <a href="#" class="avatar">
                                                                            <asp:Image ID="img_emp" runat="server" ImageUrl='<%# Bind("EMD_PHOTO")%>' />
                                                                        </a>
                                                                        <a><%# Eval("EMP_FIRST_NAME")%>  
                                                                        </a>
                                                                    </h2>
                                                                </td>
                                                                <td><%# Eval("ELT_DESCR")%>(<%# Eval("LEAVEDAYS")%>)</td>
                                                                <td class="text-center">
                                                                    <div class="dropdown action-label">
                                                                        <a class="btn btn-white btn-sm btn-rounded" href="#" data-toggle="modal" onclick="ApprovalPopup('<%# Eval("username")%>','<%# Eval("ID")%>','R')" data-target="#Approve_leave" aria-expanded="false" style="background-color: <%# Eval("APS_STATUS_COLOR")%>">
                                                                            <i class="fa fa-dot-circle-o text-info"></i><%# Eval("APS_STATUS")%>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <tr id="trLeaveHis" runat="server" visible="false">
                                                        <td colspan="3" style="text-align: center">
                                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-table card-border" id="divCovidTest" runat="server">
                                <div class="card-body" style="padding: 12px;">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h3 class="card-title mb-3">COVID 19 Test Details</h3>
                                        </div>
                                        <div class="col-lg-6">
                                            <input id="btn_add_covid" class="btn btn-primary btn-round float-right btn-sm btn_add_new2" style="margin-left: 10px;" data-toggle="modal" data-target="#Add_COVIDTest" type="button" value="Upload">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table class="table table-striped custom-table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Test Date</th>
                                                            <th>Result</th>
                                                            <th class="text-center">View</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tbody>
                                                        <asp:Repeater ID="rpt_Covid" runat="server" OnItemDataBound="OnItemDataBound">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Eval("COV_TEST_DATE")%></td>
                                                                    <td><%# Eval("COV_RESULT")%></td>
                                                                    <td style="text-align:center;">
                                                                        <%-- <asp:HyperLink runat="server" ID="hlView" Text="View"></asp:HyperLink>--%>
                                                                        <asp:HyperLink ID="imgDoc" runat="server" ImageUrl="../Images/ViewDoc.png" Visible="false" ToolTip="Click To View Document">View</asp:HyperLink>
                                                                        <asp:HiddenField runat="server" ID="hfCOVDOC_ID" Value='<%# Eval("DOC_Id")%>' />

                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <tr id="trCovid" runat="server" visible="false">
                                                            <td colspan="3" style="text-align: center">
                                                                <asp:Label ID="lblCovidMsg" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="attendanceLog">
                        <div class="col-lg-12 mb-2">
                            <div class="card att-statistics" id="MainAttLogDiv" runat="server" visible="false">
                                <div class="card-box mb-0">
                                    <div class="row">
                                        <div class="col-6 col-lg-6 mb-4">
                                            <h3 class="card-title">Attendance Logs</h3>
                                        </div>
                                        <%-- <asp:dropdownlist runat="server" class="form-control" id="slctYear"  AutoPostBack="true"  >                                            
                                        </asp:dropdownlist>--%>
                                        <div class="col-6 col-lg-6 mb-4 pr-2">
                                            <a style="color: white !important;" class="btn btn-primary btn-round float-right btn-sm cursor-pointer" id="LoadAttendanceLog" onclick="AttendanceLogPopup('<%= Session("EmployeeID")%>'); ">Attendance Report</a>
                                        </div>
                                    </div>
                                    <%-- <h5 class="card-title" style="margin-top: -10px; margin-bottom: 4px;"></h5>--%>
                                    <div class="stats-list">
                                        <asp:Repeater ID="rptrAttLog" runat="server">
                                            <ItemTemplate>
                                                <div class="stats-info">
                                                    <p><%# Eval("DESCR")%> <small><%# Eval("CAME")%><strong>/<%# Eval("TOTAL")%></strong></small></p>
                                                    <div class="progress">
                                                        <div class='<%# String.Format("progress-bar {0}", Eval("CLASS"))%>' role="progressbar" style="width: <%# Eval("PERC")%>%" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <%-- <div class="stats-info">
                                    <p>This Week <strong><small></small></strong></p>
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 31%" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="stats-info">
                                    <p>This Month <strong><small></small></strong></p>
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 62%" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--<div class="row" style="display:none;">
                        <div class="col-lg-12 text-center mt-2">
                            <div class="card-box" id="MainPDDiv" runat="server">
                                <h3 class="card-title text-left">Professional Development</h3>
                                <a href="https://school.gemsoasis.com/PDTellal/Tellal_PD_Calendar.aspx?MainMnu_code=GVlkv1cOcok=&datamode=Zo4HhpVNpXc="><i class="fa fa-line-chart" aria-hidden="true" style="font-size: 150px;"></i></a>
                            </div>
                        </div>
                    </div>--%>
                </div>
                <%-- Left Side Ends here --%>

                <%-- Right Side Starts here --%>
                <div class="col-lg-8">

                    <div class="row" id="show_leave_apply" runat="server">
                        <div class="col-lg-12">
                            <div class="card card-border" id="MainLeaveDiv" runat="server">
                                <div class="card-body" style="padding: 12px;">

                                    <div class="row">
                                        <div class="col-8">
                                            <h3 class="card-title mb-3">My Leave Status 
                                            </h3>
                                            <%--  <input type="button" class="btn btn-primary btn-round float-md-right btn-sm" style="margin-left: 10px;" runat="server" UseSubmitBehavior="false" onserverclick="Unnamed_ServerClick" value="Apply Leave"></h3>--%>
                                        </div>
                                        <div class="col-4">
                                            <input type="button" class="btn btn-primary btn-round float-right btn-sm btnapplyleave" style="margin-left: 10px;" data-toggle="modal" data-target="#add_leave" value="Apply Leave">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table id="telecom" class="table  custom-table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <!--<th>Employee</th>-->
                                                            <th style="width: 15%">Leave Type</th>
                                                            <th style="width: 20%">From</th>
                                                            <th style="width: 20%">To</th>
                                                            <th class="text-center" style="width: 15%">Days</th>
                                                            <th class="text-center" style="width: 20%">Status</th>
                                                            <th class="text-right" style="width: 10%">Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%--</table>--%>
                                                        <asp:Repeater ID="rptrLeaveStatus" runat="server">
                                                            <HeaderTemplate>
                                                                <%-- <table class="table  table-striped custom-table mb-0">
                                                            <tbody>--%>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>

                                                                <tr>
                                                                    <!--<td>
                                                        <h2 class="table-avatar">
                                                            <a href="profile.html" class="avatar"><img alt="" src="assets/img/profiles/staff4.png"></a>
                                                            <a>Dhanya  <span> Developer</span></a>
                                                        </h2>
                                                    </td>-->
                                                                    <td style="width: 15%"><%# Eval("ELT_DESCR")%> </td>
                                                                    <td style="width: 20%"><%# Format(Eval("ELA_DTFROM"), "dd MMM yyyy")%></td>
                                                                    <td style="width: 20%"><%# Format(Eval("ELA_DTTO"), "dd MMM yyyy")%></td>
                                                                    <td class="text-center" style="width: 15%"><%# Eval("LEAVEDAYS")%></td>
                                                                    <td class="text-center" style="width: 20%">
                                                                        <div class="dropdown action-label">
                                                                            <a id="aStatusLeave" class="btn btn-white btn-sm btn-rounded btnWithoutCursor" href="#" data-toggle="dropdown" aria-expanded="false" style="background-color: <%# Eval("Color")%>">
                                                                                <i class="fa fa-dot-circle-o text-info"></i><%# Eval("ELA_APPRSTATUS")%>
                                                                            </a>
                                                                            <asp:HiddenField ID="hidLeaveAppId" runat="server" Value='<%# Eval("ELA_ID")%>' />
                                                                            <!--<div class="dropdown-menu dropdown-menu-right">
                                                                <a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-info"></i> Pending</a>
                                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#approve_leave"><i class="fa fa-dot-circle-o text-success"></i> Approved</a>
                                                                <a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-danger"></i> Declined</a>
                                                            </div>-->
                                                                        </div>

                                                                    </td>
                                                                    <td class="text-right" style="width: 10%">
                                                                        <div class="dropdown dropdown-action">
                                                                            <a href="#" class="action-icon" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <a class="dropdown-item" href="#" data-toggle="modal" style="display: <%# Eval("DisplayEdit")%>" id="aApplyLeave" data-target="#add_leave" onclick="FillLeaveValues('<%# Eval("ELA_ID")%>','<%# Eval("LeaveAppID")%>'); return false;"><i class="fa fa-pencil m-r-5"></i>Edit</a>
                                                                                <a class="dropdown-item" href="#" data-toggle="modal" style="display: <%# Eval("DisplayCancel")%>" id="acnclLeave" data-target="" onclick="CancelLeave('<%# Eval("ELA_ID")%>','<%# Eval("LeaveAppID")%>','<%# Eval("ELA_APPRSTATUS")%>'); return false;"><i class="fa fa-trash-o m-r-5"></i>Cancel</a>
                                                                                <a class="dropdown-item" href="#" data-toggle="modal" id="aViewLeave" data-target="#view_leave_approval" onclick="ViewLeaveStatus('<%# Eval("ELA_ID")%>');"><i class="fa fa-eye m-r-5"></i>View Approvals</a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </ItemTemplate>
                                                            <FooterTemplate>

                                                                <%-- </table>--%>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <%--<table id ="tblLeave" runat="server" visible="false" class="table  table-striped custom-table mb-0">--%>
                                                        <tr id="tblLeave" runat="server" visible="false">
                                                            <td colspan="6" style="text-align: center;">
                                                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <%--</table>--%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border" id="MainDepDetDiv" runat="server">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-12">

                                            <h3 class="card-title mb-0">Dependent Details  </h3>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12 m-auto">

                                            <input id="btn_add_new" class="btn btn-primary btn-round float-right btn-sm btndependant" style="margin-left: 10px;" data-toggle="modal" data-target="#Add_Family" onclick="hidedivGems()" type="button" value="Add New">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body pb-0">
                                    <div class="row staff-grid-row">
                                        <asp:Repeater ID="rptrDpndt" runat="server" OnItemDataBound="rptrDpndt_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3">
                                                    <div class="profile-widget">
                                                        <div class="profile-img">
                                                            <a class="avatar" data-toggle="modal" data-target="#profile_infos" onclick="MyConfirmMethod('<%# Eval("EDD_ID")%>'); return false;">
                                                                <img src='<%# String.Format("../Payroll/ImageHandler.ashx?ID={0}&TYPE=EDD", Eval("EDD_ID"))%>' alt="" class="height-80">
                                                            </a>
                                                        &nbsp;&nbsp;&nbsp;</div>
                                                        <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a data-toggle="modal"><%# Eval("EDD_NAME")%></a></h4>
                                                        <div class="small"><%# Eval("EDD_RELATION_DESCR")%></div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <div style="padding: 0px 0px 10px 25px !important">
                                                    <asp:Label ID="lblEmptyData"
                                                        Text="No data available..." runat="server" Visible="false">
                                                    </asp:Label>
                                                </div>
                                                </table>           
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <!--<div class="card-footer">
                            <br />
                            <a href="#">View all</a>
                        </div>-->
                            </div>
                        </div>
                    </div>


                    <div class="row" id="showFeeApply" runat="server">
                        <div class="col-lg-12">
                            <div class="card card-table card-border" id="Div3" runat="server">
                                <div class="card-body" style="padding: 12px;">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="card-title mb-3">Tuition Fee Concession Request
                                <a style="color: white !important; margin-left: 10px;" class="btn btn-primary btn-round float-right btn-sm cursor-pointer" id="feeconcession" onclick="FeeConcessionPopup('<%= Session("Encr_Employee_ID")%>'); ">Apply</a></h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table class="table table-striped custom-table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 15%;">Student ID</th>
                                                            <th style="width: 30%;">Student Name</th>
                                                            <th style="width: 10%;">DOJ</th>
                                                            <th style="width: 10%;">Grade</th>
                                                            <th style="width: 10%;">School</th>
                                                            <th style="width: 25%;">Status</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>



                                                        <asp:Repeater ID="rptrFeeConcDetails" runat="server">
                                                            <HeaderTemplate>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 15%;">
                                                                        <h2 class="table-avatar">
                                                                            <%# Eval("STU_NO")%>
                                                                        </h2>
                                                                    </td>
                                                                    <td style="width: 30%;"><%# Eval("Student_Name")%></td>
                                                                    <td style="width: 10%;"><%# Eval("DOJ")%></td>
                                                                    <td style="width: 10%;"><%# Eval("[Grade/Section]")%>
                                                                    </td>
                                                                    <td style="width: 10%;"><%# Eval("BSU_SHORTNAME")%></td>
                                                                    <td style="width: 25%;">

                                                                        <a id="aStatusLeave" class="btn btn-white btn-sm btn-rounded btnWithoutCursor" href="#" data-toggle="dropdown" aria-expanded="false" style='<%# Eval("ESS_FEE_CONC_STATUS_STYLE")%>'>
                                                                            <i class="fa fa-dot-circle-o text-info"></i><%# Eval("ESS_FEE_CONC_STATUS")%>
                                                                        </a>

                                                                    </td>

                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                        <tr id="tblFeeCo" runat="server" visible="false">
                                                            <td colspan="6" style="text-align: center;">
                                                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-table card-border" id="MainPnODiv" runat="server">
                                <div class="card-body" style="padding: 12px;">
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="card-title mb-3">P & O Document Request
                                <input class="btn btn-primary btn-round float-right btn-sm btnPnO" style="margin-left: 10px;" data-toggle="modal" data-target="#Add_PODOC" type="button" value="Add New"></h3>

                                             <asp:HiddenField ID="h_Emp_No" runat="server" />
                <asp:HiddenField ID="h_emp_bsu" runat="server" />
                <asp:HiddenField ID="h_emp_bsu_code" runat="server" />
                <asp:HiddenField ID="h_empnumValue" runat="server" />
                <asp:HiddenField ID="h_DaxRecId" runat="server" />
                <asp:HiddenField ID="h_docStatusID" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table class="table table-striped custom-table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 15%;">Document ID</th>
                                                            <th style="width: 25%;">Document Type</th>
                                                            <th style="width: 20%;">Applied On</th>
                                                            <th style="width: 15%;">Status</th>
                                                            <th style="width: 15%;">HR Note</th>
                                                            <th class="text-right" style="width: 10%;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%-- </table>--%>


                                                        <asp:Repeater ID="rptrPODocDetails" runat="server">
                                                            <HeaderTemplate>
                                                                <%-- <table class="table table-striped custom-table mb-0">
                                                    <tbody>--%>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 15%;">
                                                                        <h2 class="table-avatar">
                                                                            <%# Eval("EPnO_AX_RECID")%>
                                                                        </h2>
                                                                    </td>
                                                                    <td style="width: 25%;"><%# Eval("PnO_Doc_Description")%></td>
                                                                    <td style="width: 20%;"><%# Format(Eval("EPnO_LOGDT"), "dd MMM yyyy hh:mm:ss")%>
                                                                    </td>
                                                                    <td style="width: 15%;"><%# Eval("PnOStatus_Descr")%></td>
                                                                    <td style="width: 15%;"><%# Eval("EPnO_HR_NOTES")%></td>
                                                                    <td class="text-right" style="width: 10%;">
                                                                        <div class="dropdown dropdown-action">
                                                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#Add_PODOC" onclick="GetPnODocDetails('<%# Eval("EPnO_REQUEST_ID")%>','<%# Eval("DisplayEdit")%>'); return false;"><i class="fa fa-pencil m-r-5"></i><%# Eval("DisplayEdit")%></a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                        <%--<table id="tblPnO" runat="server" visible="false" class="table  table-striped custom-table mb-0">--%>
                                                        <tr id="tblPnO" runat="server" visible="false">
                                                            <td colspan="6" style="text-align: center;">
                                                                <asp:Label ID="lblMsg2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%--                      <div class="card-footer">
                            <br />
                            <a href="#">View all</a>
                        </div>
                    </div>--%>
                                </div>
                                <!--<canvas id="chDonut1" class="m-auto" width="280" height="280" style="display: block; width: 280px; height: 280px;"></canvas>-->
                                <!--<a href="#" class="btn btn-primary float-right">View More</a>-->
                            </div>
                        </div>
                    </div>






                </div>
                <%-- Right Side Starts here --%>
            </div>
        </div>

    </div>
    <!-- /Page Wrapper -->
    <!--View Profile-->
    <div class="row">
        <%-- <button type="button" style="display: none;" id="btnShowPopup" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#View_Profile">
            Launch demo modal</button> --%>

        <div id="View_Profile" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnClose1" onclick="revertcontrols();Closesa();">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="card-box mb-0">
                            <span id="ErrorEditProf" class=""></span>

                            <div id="lblReqfield" runat="server" class="text-muted mt-3" style="font-size: small !important; text-align: center; display: none;"><i>All fields are mandatory</i></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-view">
                                        <div class="profile-img-wrap">
                                            <div class="profile-img">
                                                <a href="#" id="ref" runat="server" class="btnWithoutCursor">
                                                    <img id="imgPro" runat="server" alt="No Image" src='<%= ResolveUrl("~/GemsEss/assets/img/user.jpg")%>'></a>
                                            </div>
                                        </div>
                                        <div class="profile-basic">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="profile-info-left">
                                                        <ul class="personal-info">
                                                            <li>
                                                                <div class="title">Name:</div>
                                                                <div class="text">
                                                                    <span id="txtEmpName" class="field-label" runat="server"></span>
                                                                    <asp:TextBox ID="txtEmpNameEdit" runat="server" Style="display: none; background-color: rgba(0,0,0,0.03);" Enabled="false" autocomplete="off"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Employee No:</div>
                                                                <div class="text">
                                                                    <span id="txtEmpNo" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtEmpNoEdit" runat="server" Style="display: none; background-color: rgba(0,0,0,0.03);" Enabled="false" autocomplete="off"> </asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Emirates ID No:</div>
                                                                <div class="text">
                                                                    <span id="txtEmrId" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtEmrIdEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtEmrIdEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">EID Expiry Date:</div>
                                                                <div class="text">
                                                                    <span id="txtEidExpDt" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtEidExpDtEdit" CssClass="form-control datetimepicker1" autocomplete="off" runat="server" Style="display: none;"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Passport No:</div>
                                                                <div class="text">
                                                                    <span id="txtPassportNo" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPassportNoEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtPassportNoEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Passport Issued Place:</div>
                                                                <div class="text">
                                                                    <span id="txtPassprtIsuePlc" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPassprtIsuePlcEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtPassprtIsuePlcEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Passport Issued Date:</div>
                                                                <div class="text">
                                                                    <span id="txtPasprtIsueDt" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPasprtIsueDtEdit" CssClass="form-control datetimepicker1" autocomplete="off" runat="server" Style="display: none;"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Passport Expiry Date:</div>
                                                                <div class="text">
                                                                    <span id="txtPasprtexpdt" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPasprtexpdtEdit" CssClass="form-control datetimepicker1" autocomplete="off" runat="server" Style="display: none;"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Residence Emirate:</div>
                                                                <div class="text">
                                                                    <span id="ddlEmirate1" runat="server" class="field-value"></span>
                                                                    <asp:DropDownList ID="ddlEmirate" runat="server" Style="display: none" />
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Existing Insurance Card No:</div>
                                                                <div class="text">
                                                                    <span id="txtEmpInsuNo" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtEmpInsuNoEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtEmpInsuNoEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Area:</div>
                                                                <div class="text">
                                                                    <span id="ddlArea1" runat="server" class="field-value"></span>
                                                                    <asp:DropDownList ID="ddlArea" Style="display: none" runat="server"></asp:DropDownList>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-info-left border-0">
                                                        <ul class="personal-info">
                                                            <li>
                                                                <div class="title">UID No:</div>
                                                                <div class="text">
                                                                    <span id="txtUIDNo" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtUIDNoEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtUIDNoEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Visa Issued Place:</div>
                                                                <div class="text">
                                                                    <span id="txtVisaIsuePlc" runat="server" class="field-label"></span>
                                                                    <asp:TextBox runat="server" ID="txtVisaIsuePlcEdit" Style="display: none; background-color: rgba(0,0,0,0.03);" Enabled="false" autocomplete="off" onchange="javascript: ChktxtVisaIsuePlcEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Visa Issued Date:</div>
                                                                <div class="text">
                                                                    <span id="txtVisaIsueDt" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtVisaIsueDtEdit" CssClass="form-control datetimepicker1" autocomplete="off" runat="server" Style="display: none; background-color: rgba(0,0,0,0.03);" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Visa Expiry Date:</div>
                                                                <div class="text">
                                                                    <span id="txtVisaExpDt" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtVisaExpDtEdit" CssClass="form-control datetimepicker1" autocomplete="off" runat="server" Style="display: none; background-color: rgba(0,0,0,0.03);" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Visa Sponsor:</div>
                                                                <div class="text">
                                                                    <span id="ddlsponser" runat="server" class="field-label"></span>
                                                                    <asp:DropDownList ID="ddlsponserEdit" runat="server" Style="display: none">
                                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Company" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Spouse" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="Father/Mother" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="Mother" Value="4"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Alternate Email:</div>
                                                                <div class="text">
                                                                    <span id="txtAlEmail" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtAlEmailEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtAlEmailEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Alternate Phone Number:</div>
                                                                <div class="text">
                                                                    <span id="txtPhnNum" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPhnNumEdit" runat="server" Style="display: none" autocomplete="off" onchange="javascript: chktxtPhnNumEdit();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Emergency Contact Person (Local):</div>
                                                                <div class="text">
                                                                    <span id="spnEmrCnt1" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtEmrCnt1" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtEmrCnt1();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Emergency Phone Number(Local):</div>
                                                                <div class="text">
                                                                    <span id="spnPhnEmerCont1" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPhnEmerCont1" runat="server" Style="display: none" autocomplete="off" onchange="javascript: chktxtPhnEmerCont1();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="title">Emergency Contact Person (Home):</div>
                                                                <div class="text">
                                                                    <span id="spnEmrCnt2" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtEmrCnt2" runat="server" Style="display: none" autocomplete="off" onchange="javascript: ChktxtEmrCnt2();"></asp:TextBox>
                                                                </div>
                                                            </li>


                                                            <li>
                                                                <div class="title">Emergency Phone Number(Home):</div>
                                                                <div class="text">
                                                                    <span id="spnPhnEmerCont2" runat="server" class="field-label"></span>
                                                                    <asp:TextBox ID="txtPhnEmerCont2" runat="server" Style="display: none" autocomplete="off" onchange="javascript: chktxtPhnEmerCont2();"></asp:TextBox>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%-- <asp:button  ID="btnProfEdit" runat="server" onclick="btnProfEdit_Click" />--%>
                                        <div class="pro-edit">
                                            <%--<input type="button" id="btnProEdit" runat="server" class="edit-icon" onclick="EditProfilePage(0);" data-toggle="tooltip" title="Edit Profile" /> 
                                            --%>
                                            <a id="lnkEdit" href="#" data-toggle="tooltip" title="Edit Profile" onclick="EditProfilePage(0);">
                                                <span id="spnEditIcon" class="edit-icon mr-2"></span>
                                                <br />
                                                <span id="spnEdit" class="d-block font-weight-light small text-center">Edit Profile</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--View Profile-->

    <!-- View Leave Approvals -->

    <div id="view_leave_approval" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnsave6" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="width: 100%">
                        <%-- <asp:GridView ID="gvApprovals"  AutoGenerateColumns="False" EmptyDataText="History empty"
                            Width="100%" CssClass="table table-bordered table-row">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />

                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_pop" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <Columns>
                                <asp:BoundField DataField="APS_NAME" HeaderText="Name" ReadOnly="True" SortExpression="APS_NAME" />
                                <asp:BoundField DataField="APS_STATUS" HeaderText="Status" ReadOnly="True" SortExpression="APS_STATUS" />
                                <asp:BoundField DataField="APS_REMARKS" HeaderText="Remarks" SortExpression="APS_REMARKS" />
                                <asp:BoundField DataField="APS_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"
                                    HtmlEncode="False" SortExpression="APS_DATE">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PREV" HeaderText="Req Prev Approval" ReadOnly="True" SortExpression="PREV">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FINAL" HeaderText="Req Higher Approval" ReadOnly="True"
                                    SortExpression="FINAL">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>--%>
                        <div id="gvApprovals" style="width: 100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of Leave Approvals -->

    <!-- Add Leave Modal -->
    <div id="add_leave" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnSave5" onclick="ClearLeaveControls();Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <span id="LeaveError" class=""></span>
                            <div class="card">
                                <article style="margin: 20px 20px">
                                    <h4 class="card-title mt-3 text-left">Employee Leave Application Form</h4>

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-clipboard"></i></span>
                                                </div>
                                                <select class="form-control" id="slctLeaveAppltype" onchange="LeaveTypeSelectionChange(); return false;">
                                                </select>
                                            </div>
                                            <!-- form-group end.// -->
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar calFrom"></i></span>
                                                </div>
                                                <input class="form-control datetimepicker1" id="fromdateLeave" placeholder="From Date" autocomplete="off" type="text" onclick="LeaveDateChange(0); return false;" onblur="LeaveDateChange(0); return false;" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar calTo"></i></span>
                                                </div>
                                                <input class="form-control datetimepicker1" id="todateLeave" placeholder="To Date" autocomplete="off" type="text" onclick="LeaveDateChange(1); return false;" onblur="LeaveDateChange(1); return false;" />
                                            </div>
                                            <!-- form-group// -->
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-lg-4 col-md-4 col-sm-12" id="rowLeaveCount1">
                                            <span class="field-label-form">Total Days :</span> <span class="field-value" id="totalDaysL">0</span>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12" id="rowLeaveCount2">
                                            <span class="field-label-form">Actual Days :</span> <span class="field-value" id="ActualDaysL">0</span>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12" style="display: none" id="rowLeaveCount3">
                                            <span class="field-label-form">Balance Leave :</span> <span class="field-value" id="leaveBal"></span>
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="rowLeaveML">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="text-muted">File Upload <span class="text-muted" style="font-size: xx-small !important;"><i>Note:File of size max 10 MB</i></span></div>

                                            <div class="form-group input-group">
                                                <input class="upload" type="file" id="fluplodLeaveML" />
                                            </div>
                                            <!-- form-group// -->
                                        </div>
                                    </div>
                                    <div class="row" style="display: none" id="rowLeaveMLShowFile">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group input-group">
                                                <a id="showLeavefile">View document</a>
                                                <a href="#" id="deleteLeaveFile" onclick="DeleteMedcalLeaveFile();"><i class="fa fa-remove pl-3 text-danger"></i></a>
                                            </div>
                                            <!-- form-group// -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="text-muted">Remarks<span class="text-danger">*</span> <span class="text-muted" style="font-size: xx-small !important;"><i>(Max. 200 Characters)</i></span></div>
                                            <div class="form-group input-group">
                                                <textarea id="leaveDetail" name="leavedetail" maxlength="200" cols="40" rows="2" class="form-control" placeholder="Leave Details" onchange="checkForSpace();"></textarea>
                                            </div>
                                            <!-- form-group// -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="text-muted">Handover Details<span class="text-muted" style="font-size: xx-small !important;"><i>(Max. 200 Characters)</i></span></div>
                                            <div class="form-group input-group">
                                                <textarea id="workhandover" name="workhandover" maxlength="200" cols="40" rows="2" class="form-control" placeholder="Work Will be handed over to ...." onchange="checkForHandover();"></textarea>
                                            </div>
                                            <!-- form-group// -->
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="text-muted">Phone</div>
                                            <div class="form-group input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                </div>
                                                <input name="" class="form-control" placeholder="Contact number when on leave" style="height: 57px;" autocomplete="off" type="text" id="LeavePhone" onchange="CheckNumber();"><br />

                                            </div>
                                            <!-- form-group// -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="text-muted">Address<span class="text-muted" style="font-size: xx-small !important;"><i>(Max. 200 Characters)</i></span></div>
                                            <div class="form-group input-group">
                                                <textarea id="addressLeave" name="address" maxlength="200" cols="40" rows="2" class="form-control" placeholder="Address ...." onchange="checkForAddress();"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <input type="button" id="btnSaveLeave" class="btn btn-primary" onclick="SaveLeave(0);" value="Apply" />
                                            <a onclick="ClearLeaveControls();Closesa_Leave();" class="btn btn-warning">Close</a>
                                        </div>
                                    </div>

                                </article>


                            </div>
                            <!-- LEAVE STATISTICS START HERE -->
                            <div class="card att-statistics">
                                <div class="card-body">
                                    <h5 class="card-title">Leave Statistics</h5>
                                    <div class="stats-list row">
                                        <asp:Repeater ID="rptrLeaveLog" runat="server">
                                            <ItemTemplate>
                                                <div class="stats-info-popup col-lg-3" data-toggle="tooltip" data-html="true" title="<strong><%# Eval("Taken")%> <small>/ <%# Eval("Eligible")%></small></strong>">

                                                    <div class="row">
                                                        <div class="col-lg-12 col-12 pl-2"><%# Eval("LeaveType")%></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-3 p-0 pl-2"><strong><%# Eval("Taken")%> <small>/ <%# Eval("Eligible")%></small></strong></div>
                                                        <div class="col-lg-9 col-9 m-auto">
                                                            <div class="progress">
                                                                <div id="divLogLeave" class="progress-bar bg-success" role="progressbar" style="width: <%# Eval("Taken")%>%" aria-valuenow='<%# Eval("Taken")%>' aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <!-- LEAVE STATISTICS ENDS HERE -->
                            <!-- card.// -->
                        </div>
                        <div class="col-md-4">

                            <%--CALENDER STARTS HERE--%>
                            <div>
                                <div class="card att-statistics">
                                    <div class="card-body p-0">
                                        <h5 class="card-title pl-4 pt-4">Leave Calendar</h5>
                                        <table class="calMthYear m-auto" style="width: 280px;">

                                            <tr>
                                                <th>
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-1 col-lg-1 col-md-1 col-sm-1 m-0 p-0">
                                                                <asp:LinkButton ID="lbtnPreMth" runat="server" Text="Nov" CssClass="calPrevMth" Style="display: none;"></asp:LinkButton>
                                                            </div>
                                                            <div class="col-10 col-lg-10 col-md-10 col-sm-10 text-center m-0 p-0">
                                                                <%--<telerik:RadDropDownList ID="rddlMth" runat="server" BackColor="White" BorderColor="#d8d8d8" Width="53px"
                                                    Font-Size="10px" AutoPostBack="True">
                                                </telerik:RadDropDownList>--%>
                                                                <select id="calndrMonth" onchange="CalnderMonthhange();" class="cal-drop">
                                                                    <option value="Jan">Jan</option>
                                                                    <option value="Feb">Feb</option>
                                                                    <option value="Mar">Mar</option>
                                                                    <option value="Apr">Apr</option>
                                                                    <option value="May">May</option>
                                                                    <option value="Jun">Jun</option>
                                                                    <option value="Jul">Jul</option>
                                                                    <option value="Aug">Aug</option>
                                                                    <option value="Sep">Sep</option>
                                                                    <option value="Oct">Oct</option>
                                                                    <option value="Nov">Nov</option>
                                                                    <option value="Dec">Dec</option>
                                                                </select>
                                                                &nbsp;
                                                <select id="calndrYear" onchange="CalnderMonthhange();" class="cal-drop">
                                                </select>
                                                                <%--   <telerik:RadDropDownList ID="rddlYear" runat="server" BackColor="White" BorderColor="#d8d8d8" Width="63px" Font-Size="10px" AutoPostBack="True">
                            </telerik:RadDropDownList>--%>
                                                            </div>
                                                            <div class="col-1 col-lg-1 col-md-1 col-sm-1 m-0 p-0">
                                                                <asp:LinkButton ID="lbtnNxtMth" runat="server" Text="Jan" CssClass="calNxtMth" Style="display: none;"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </th>
                                            </tr>

                                        </table>
                                        <ul class="weekdays m-auto">
                                            <li>Su</li>
                                            <li>Mo</li>
                                            <li>Tu</li>
                                            <li>We</li>
                                            <li>Th</li>
                                            <li>Fr</li>
                                            <li>Sa</li>
                                        </ul>

                                        <ul class="days m-auto" id="caldays">
                                        </ul>

                                        <%-- <asp:Calendar ID="CalDatepicker" runat="server" CssClass="calCss"
                                            NextPrevFormat="ShortMonth" SelectionMode="Day" ShowTitle="true"
                                            ShowGridLines="True" Width="100%" ShowNextPrevMonth="true" OnDayRender="CalDatepicker_DayRender">

                                            <SelectedDayStyle CssClass="calDaySelected" />

                                            <SelectorStyle BackColor="#d82133" />

                                            <TodayDayStyle CssClass="calDayToday" />

                                            <OtherMonthDayStyle CssClass="calOtherDay" />

                                            <DayStyle CssClass="calDayStyle" />

                                            <DayHeaderStyle CssClass="calWeekTitle" />

                                        </asp:Calendar>--%>
                                    </div>
                                </div>
                            </div>
                            <%--CALENDER ENDS HERE--%>

                            <%--EVENTS STARTS HERE--%>
                            <div class="divBoxFrame_Remarks" style="width: 100%; height: 100%;">

                                <%--<div class="divBoxFrame_Title">
                                <span class="spanFrameTitle">Events</span></div>--%>
                                <div class="card att-statistics">
                                    <div class="card-body">
                                        <h5 class="card-title">Events Calendar</h5>
                                        <div class="divBoxScroll" style="width: 99%;">

                                            <div class="divBoxScrollInside" style="width: 106%;">

                                                <span id="spnShowEvents"></span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--EVENTS ENDS HERE--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Leave Modal -->

    <!--Add P & O doc-->
    <div id="Add_PODOC" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add P & O Document</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnSave4" onclick="ClearPnO();Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <span id="ErrorPnOAdd" class=""></span>
                    <form>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group dropdown">
                                    <label>Document Type <span class="text-danger">*</span></label>
                                    <select class="select  form-control" id="slctPnOtype">
                                        <option value="0">Select Document Type</option>
                                        <option value="1">NO Objection for Sponsoring </option>
                                        <option value="2">NOC Driving</option>
                                        <option value="3">NOC Embassy</option>
                                        <option value="4">NOC Liquor</option>
                                        <option value="5">Residence Confirmation</option>
                                        <option value="6">Salary Certificate</option>
                                        <option value="7">Salary Certificate CC Loan</option>
                                        <option value="8">Salary Transfer – New Bank Account</option>
                                        <option value="9">Salary Transfer Loan</option>
                                        <option value="10">Salary Transfer with Bank Account</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Address To<span class="text-danger">*</span></label>
                                    <div class="">
                                        <input class="form-control" type="text" id="PnOAddTo" onchange="CheckAddTopno();" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Employee Notes <span class="text-danger">*</span></label>
                                    <div class="">
                                        <input class="form-control " type="text" id="PnOEmpNotes" onchange="CheckEmpNotepno();" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Addressing Notes <span class="text-danger">*</span></label>
                                    <input class="form-control" type="text" id="PnOAddressNotes" onchange="CheckAddreNotes();" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <input type="button" class="btn btn-primary" value="Apply" id="btnSavePnO" onclick="SavePnODocuments(0);" />
                                <a onclick="ClearPnO();ClosePnOWindow();" class="btn btn-warning">Close</a>
                                <input type="hidden" name="RecordDax" id="hdnRecordDax" />
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--Add P & O doc-->
    <!--Add cash Vo-->


    <!--Add cash vo-->
    <!-- Profile Modal -->
    <div id="profile_infos" class="modal custom-modal fade" role="dialog">
        <%--..GemsEss/--%>
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dependent Details
                        <br />
                        <%--<span class="text-muted" style="font-size: small !important; text-align: center;"><i>* All fields are mandatory</i></span>--%>
                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnSave3" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="divalert777">
                        <span id="ErrorEditFamily"></span>
                        <i style="display: none;" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup()"></i>
                    </div>

                    <form id="EditFamilyForm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap edit-img">
                                    <img class="inline-block" src="<%= ResolveUrl("~/GemsEss/assets/img/profiles/staff4.png")%>" alt="user" id="imgDDProfile">
                                    <div class="fileupload btn">
                                        <span class="btn-text"><i class="fa fa-pencil"></i>Edit</span>
                                        <input class="upload" type="file" id="uploadDDPhoto">
                                    </div>

                                </div>
                                <div style="text-align: center;">
                                    <span class="text-muted" style="font-size: small !important;"><i>Note:File of type .png, .jpeg or .jpg, and of size max 5 MB</i></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Relation<span class="text-danger">*</span></label>
                                            <select class="select form-control" id="slctDDRel">
                                                <%--<option selected="selected" value="-1">--Select--</option>--%>
                                                <option value="0">Spouse</option>
                                                <option value="1">Child</option>
                                                <option value="2">Father</option>
                                                <option value="3">Mother</option>
                                                <%-- <option value="4">Grand Child</option>
                                                <option value="5">Grand Parent</option>--%>
                                                <option value="6">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth<span class="text-danger">*</span></label>
                                            <div class="cal-icon-pick">
                                                <i class="fa fa-calendar textbox-cal-icon"></i>
                                                <input class="form-control datetimepicker1 txtDDDob" type="text" id="txtDDDOB" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="padding-top: 35px;">
                                            <input type="radio" id="radDDNGems" name="IsGems" value="Non Gems" onclick="RadChange('0', 'DD');">
                                            Non GEMS
                                    <input type="radio" id="radDDSGems" name="IsGems" value="Gems Staff" onclick="RadChange('1', 'DD');">
                                            GEMS Staff
                                    <input type="radio" id="radDDStGems" name="IsGems" value="Gems Student" onclick="RadChange('2', 'DD');">
                                            GEMS Student                                       
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label>Select Unit<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="txtDDUnit" style="display: none" onkeyup="InitCompanyAutoComplete(0);" data-toggle="dropdown" autocomplete="off" placeholder="Enter Unit Code">
                                            <div id="divCompany" class="auto-comp-popup" style="display: none"></div>
                                            <asp:DropDownList ID="ddlDDUnit" class="select form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label id="lblDDGemsStaffStudent" class="lblDDGemsStaffStudent"></label>
                                            <span class="text-danger">*</span><i class="lblGemsStudId" style='font-size: 10px; display: none;'>(Please enter 14 digit number prinited on school Id card)</i>
                                            <input type="text" class="form-control txtDDGEMSStaffIdStudentId" id="txtDDGEMSStaffIdStudentId" autocomplete="off" onchange="ChktxtDDGemsStaffIdStudentId();">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name<span class="text-danger">*</span></label><i class="lblGemsStudId" style='font-size: 10px; display: none'>&nbsp;</i>
                                            <input style="display: none;" type="text" class="form-control" id="txtDDName" onkeyup="InitStudentAutoComplete(0);" data-toggle="dropdown" autocomplete="off" onchange="ChktxtDDName();" placeholder="Enter Name">
                                            <div id="divStudent" style="display: none;" class="auto-comp-popup"></div>
                                            <input type="text" class="form-control" id="txtDDDepedentName" disabled="disabled" autocomplete="off">
                                        </div>
                                    </div>
                                    <!--<div class="col-md-6">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="cal-icon">
                                                <input class="form-control datetimepicker" type="text" value="05/06/1985">
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gender<span class="text-danger">*</span></label>
                                            <select class="select form-control slctDDGender" id="slctDDGender">
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Marital Status<span class="text-danger">*</span></label>
                                            <select class="select form-control slctDDMrtsts" id="slctDDMrtsts">
                                                <option value="-1">-Select-</option>
                                                <option value="0">Married</option>
                                                <option value="1">Single</option>
                                                <option value="2">Widowed</option>
                                                <option value="3">Divorced</option>
                                                <option value="4">Separated</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nationality<span class="text-danger">*</span></label>
                                    <input style="display: none" type="text" class="form-control" id="txtDDNationality" onkeyup="InitCounrtyInfo(0);" data-toggle="dropdown" autocomplete="off">
                                    <div style="display: none" id="divCountry" class="auto-comp-popup"></div>
                                    <asp:DropDownList ID="ddlDDNationality" class="select form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Passport No</label>
                                    <input type="text" class="form-control" id="txtDDPasprtNo" autocomplete="off" onchange="ChktxtDDPasprtNo();">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country of Residence<span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlDDResidenceCountry" class="select form-control" runat="server" onchange="changeEvent(this);"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EmiratesID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="txtDDEIDNo" autocomplete="off" onchange="ChktxtDDEIDNo();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EID Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" id="txtDDEDIEXPDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->

                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>UID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="txtDDUID" autocomplete="off" onchange="ChktxtDDUID();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" id="txtDDVIsaIsueDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="23/Oct/2016">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" id="txtDDVIsaExpDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Place<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="txtDDVisaIsuePlc" autocomplete="off" onchange="ChktxtDDVisaIsuePlc();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Sponsor<span class="text-danger">*</span></label>
                                    <select class="select form-control" id="slctDDVisSpnsr">
                                        <option value="0">Select</option>
                                        <option value="1">Company</option>
                                        <option selected="selected" value="2">Spouse</option>
                                        <option value="3">Father</option>
                                        <option value="4">Mother</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Existing Insurance Card No<span class="text-danger"></span></label>
                                    <input type="text" class="form-control" id="txtDDInsCrdNo" autocomplete="off" onchange="ChktxtDDInsCrdNo();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa File Number<span class="text-danger" runat="server" visible="false" id="spanVisFileNoCheck">*</span></label>
                                    <input type="text" class="form-control" id="txtvisafilenumber" autocomplete="off" onkeypress="return IsValidVisaFileNo(event);" onchange="ChktxtVisaFileNumber();">
                                </div>
                            </div>
                        </div>
                        <div id="divalert7770">
                            <span id="ErrorEditFamily0"></span>
                            <i style="display: none;" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup()"></i>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <input type="button" class="btn btn-primary" id="btnProfSave" onclick="SaveDependantDetails(0);" value="Save" />
                            <input type="button" class="btn btn-danger" id="btnProfDelete" onclick="ConfirmDelete(0);" value="Delete" />
                            <a onclick="Closesa_EditDep();" class="btn btn-warning">Close</a>
                        </div>
                        <%--<span id="ErrorEditFamily" class=""></span>--%>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- /Profile Modal -->
    <!-- Add Profile Modal -->
    <div id="Add_Family" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Dependent Details
			<br />
                        <%-- <span class="text-muted" style="font-size: small !important; text-align: center;"><i>* All fields are mandatory</i></span>--%>
                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnsave2" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div id="divalert277">
                        <span id="ADDError"></span>
                        <i style="display: none" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup2()"></i>
                    </div>
                    <form id="AddFamilyForm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap edit-img">
                                    <img class="inline-block" src="<%= ResolveUrl("~/GemsEss/assets/img/profiles/avatar-02.jpg")%>" alt="" id="imgAddProfile">
                                    <div class="fileupload btn">
                                        <span class="btn-text">Add</span>
                                        <input class="upload" type="file" id="uploadAddPhoto">
                                        <%--<asp:FileUpload CssClass="upload" ID="fileupload" runat="server" />--%>
                                    </div>

                                </div>
                                <div style="text-align: center;">
                                    <span class="text-muted" style="font-size: small !important;"><i>Note:File of type .png, .jpeg or .jpg, and of size max 5 MB</i></span>
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Relation<span class="text-danger">*</span></label>
                                            <select class="select form-control slctAddRltion" id="slctAddRltion">
                                                <option selected="selected" value="-1">--Select--</option>
                                                <option value="0">Spouse</option>
                                                <option value="1">Child</option>
                                                <option value="2">Father</option>
                                                <option value="3">Mother</option>
                                                <%--<option value="4">Grand Child</option>
                                                <option value="5">Grand Parent</option>--%>
                                                <option value="6">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth<span class="text-danger">*</span></label>
                                            <div class="cal-icon-pick">
                                                <i class="fa fa-calendar textbox-cal-icon"></i>
                                                <input class="form-control datetimepicker1 txtAddDob" type="text" value="" id="txtAddDob" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" style="padding-top: 35px;">
                                            <input type="radio" id="radADDNGems" name="IsGemsAdd" value="Non Gems" onclick="RadChange('0', 'Add');">
                                            Non GEMS
                                    <input type="radio" id="radADDSGems" name="IsGemsAdd" value="Gems Staff" onclick="RadChange('1', 'Add');">
                                            GEMS Staff
                                    <input type="radio" id="radADDStGems" name="IsGemsAdd" value="Gems Student" onclick="RadChange('2', 'Add');">
                                            GEMS Student                                       
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label>Select Unit<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" value="" id="txtAddUnit" style="display: none" onkeyup="InitCompanyAutoComplete(1);" data-toggle="dropdown" autocomplete="off" placeholder="Enter Unit Code">
                                            <div id="divCompany1" class="auto-comp-popup" style="display: none"></div>
                                            <asp:DropDownList ID="ddlAddUnit" class="select form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 divGems" style="display: none;">
                                        <div class="form-group">
                                            <label id="lblAddGemsStaffStudent" class="lblAddGemsStaffStudent"></label>
                                            <span class="text-danger">*</span>&nbsp;&nbsp;<i class="lblGemsStudId" style='font-size: 10px; display: none'>(Please enter 14 digit number prinited on school Id card)</i>
                                            <input type="text" class="form-control txtAddGEMSStaffIdStudentId" id="txtAddGEMSStaffIdStudentId" autocomplete="off" onchange="ChktxtAddGemsStaffIdStudentId();">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name<span class="text-danger">*</span></label><i class="lblGemsStudId" style='font-size: 10px; display: none'>&nbsp;</i>
                                            <input style="display: none;" type="text" class="form-control" value="" id="txtAddName" onkeyup="InitStudentAutoComplete(1);" data-toggle="dropdown" autocomplete="off" onchange="ChktxtAddName();" placeholder="Enter Name">
                                            <div style="display: none;" id="divStudent1" class="auto-comp-popup"></div>
                                            <input type="text" class="form-control" id="txtAddDepedentName" disabled="disabled" autocomplete="off">
                                        </div>
                                    </div>
                                    <!--<div class="col-md-6">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="cal-icon">
                                                <input class="form-control datetimepicker" type="text" value="05/06/1985">
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gender<span class="text-danger">*</span></label>
                                            <select class="select form-control" id="slctAddGender">
                                                <option selected="selected" value="0">--Select Gender--</option>
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Marital Status<span class="text-danger">*</span></label>
                                            <select class="select form-control" id="slctAddMar">
                                                <option selected="selected" value="-1">--Select Marital Status--</option>
                                                <option value="0">Married</option>
                                                <option value="1">Single</option>
                                                <option value="2">Widowed</option>
                                                <option value="3">Divorced</option>
                                                <option value="4">Separated</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nationality<span class="text-danger">*</span></label>
                                    <input style="display: none" type="text" class="form-control" value="" id="txtAddNAt" onkeyup="InitCounrtyInfo(1);" data-toggle="dropdown" autocomplete="off">
                                    <div style="display: none" id="divCountry1" class="auto-comp-popup"></div>
                                    <asp:DropDownList ID="ddlAddNationality" class="select form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Passport No</label>
                                    <input type="text" class="form-control" value="" id="txtAddPasprtNo" autocomplete="off" onchange="ChktxtAddPasprtNo();">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Country of Residence<span class="text-danger">*</span></label>
                                    <asp:DropDownList ID="ddlAddResidenceCountry" runat="server" class="select form-control" onchange="changeEvent(this);"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EmiratesID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddEID" autocomplete="off" onchange="ChktxtAddEID();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>EID Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" value="" id="txtEIDExpDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->

                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>UID No<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddUIDNo" autocomplete="off" onchange="ChktxtAddUIDNo();" />
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" value="" id="txtAddVisaIssueDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="23/Oct/2016">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Expiry Date<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1" type="text" value="" id="txtAddVisaExpDt" autocomplete="off">
                                    </div>
                                    <!--<input type="text" class="form-control" value="22/Oct/2018">-->
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Issued Place<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddVisaIssuePlc" autocomplete="off" onchange="ChktxtAddVisaIssuePlc();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa Sponsor<span class="text-danger">*</span></label>
                                    <select class="select form-control" id="slctAddVisaSpnsr">
                                        <option selected="selected" value="0">--Select Visa Sponsor--</option>
                                        <option value="1">Company</option>
                                        <option value="2">Spouse</option>
                                        <option value="3">Father</option>
                                        <option value="4">Mother</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Existing Insurance Card No<span class="text-danger"></span></label>
                                    <input type="text" class="form-control" value="" id="txtAddInsCrdNo" autocomplete="off" onchange="ChktxtAddInsCrdNo();">
                                </div>
                            </div>
                            <div class="col-md-6 divUAE">
                                <div class="form-group">
                                    <label>Visa File Number<span class="text-danger" runat="server" visible="false" id="spanAddVisFileNoCheck">*</span></label>
                                    <input type="text" class="form-control" value="" id="txtAddVisaFileNumber" autocomplete="off" onkeypress="return IsValidVisaFileNo(event);" onchange="ChktxtAddVisaFileNumber();">
                                </div>
                            </div>

                        </div>
                        <div id="divalert2770" class="mb-2">
                            <span id="ADDError0"></span>
                            <i style="display: none" class="fa fa-times text-default pl-5 float-right closeAlertpopup cursor-pointer" onclick="closeAlertPopup2()"></i>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <input id="btnAddSaveDep" type="button" class="btn btn-primary" value="Save" onclick="SaveDependantDetails(0);" />
                            <a onclick="Closesa_AddDep();" class="btn btn-warning">Close</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div id="Confirm_Cancel" class="modal custom-modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <h5 class="modal-title border-bottom p-2">Enter Your Reason for Cancellation<span style="color: red">*</span></h5>
                <div>
                    <span style="color: red; font-size: xx-small !important;"><i></i></span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btncancel_c">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body text-center">
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div id="Confirm_Salary" class="modal custom-modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width: 400px; margin: auto;">
            <div class="modal-content">
                <h5 class="modal-title border-bottom p-2">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnsavepay_c">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body text-center">
                    <span class="font-weight-bold">Do you want to download your salary slip now?</span>

                </div>

                <div class="modal-footer">
                    <%--<a id="btnConSalDwnld" runat="server" onclick="onloadbutton();" onserverclick="btnConSalDwnld_ServerClick" class="btn btn-primary" >Yes</a>--%>
                    <asp:Button runat="server" ID="btnConSalDwnld" OnClientClick="onloadbutton();" Style="width: 100px;" OnClick="btnConSalDwnld_Click" CssClass="btn btn-primary" Text="Yes" />
                    <a href="#" class="btn btn-warning" id="lnkNo" data-dismiss="modal" aria-label="Close" style="width: 100px;">No </a>
                    <%--Loader for salary slip download--%>
                    <div id="lblSalLoader" class="loader-ellips loader-ellips-payslip text-center" style="display: none">
                        <span class="loader-ellips__dot"></span>
                        <span class="loader-ellips__dot"></span>
                        <span class="loader-ellips__dot"></span>
                        <span class="loader-ellips__dot"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="No_Salary" class="modal custom-modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width: 400px; margin: auto;">
            <div class="modal-content">
                <h5 class="modal-title border-bottom p-2">Information</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnsavepay_n">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body text-center">
                    <span class="font-weight-bold">There is no salary slip available for this month</span>
                    <%--<a id="btnConSalDwnld" runat="server" onserverclick="btnConSalDwnld_ServerClick" style="display:none;" >Download</a>--%>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-leave-cancel custom-modal" id="divLCan">
        <div class="popup-leave-cancel-body bg-white border">
            <h5 class="modal-title border-bottom p-2">Reason For Cancellation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnCloseCancel" onclick="CancelClose();" style="position: absolute; top: 10px; right: 10px;">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="p-3">
                <span>Your Comments<span class="text-danger">*</span> :</span>
                <textarea id="txtcomnts_can" maxlength="200" cols="100" rows="4" class="form-control" placeholder="Please enter your comments for cancellation ...."></textarea>
            </div>
            <div class="text-center text-white pt-3 pb-2">
                <a id="btnokComCan" class="btn btn-primary" onclick="CancelLeave(0,0,0); return false;">OK </a>
            </div>
        </div>
    </div>
    <div id="Child_List" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnChildList" onclick="Closesa_childList();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-border" id="Div1" runat="server">
                                <div class="card-header">
                                    <h3 class="card-title mb-0">Dependent Details
                                        
                                <input id="btn_add_new2" class="btn btn-primary btn-round float-right btn-sm btn_add_new2" style="margin-left: 10px;" data-toggle="modal" data-target="#Add_Family" onclick="Closesa_childList()" type="button" value="Add New">
                                    </h3>

                                </div>
                                <div class="card-body pb-0">
                                    <div class="row staff-grid-row">
                                        <asp:Repeater ID="rptrChildList" runat="server" OnItemDataBound="rptrChildList_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3">
                                                    <%--<div class=" col-lg-2 col-xl-2 col-md-2 col-sm-4 col-12">--%>
                                                    <div class="profile-widget">
                                                        <div class="profile-img">
                                                            <a class="avatar" data-toggle="modal" data-target="#profile_infos" onclick="MyConfirmMethod('<%# Eval("EDD_ID")%>'); return false;">
                                                                <img src='<%# String.Format("../Payroll/ImageHandler.ashx?ID={0}&TYPE=EDD", Eval("EDD_ID"))%>' alt="" class="height-80">
                                                            </a>
                                                        &nbsp;&nbsp;&nbsp;</div>
                                                        <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a data-toggle="modal"><%# Eval("EDD_NAME")%></a></h4>
                                                        <div class="small"><%# Eval("EDD_RELATION_DESCR")%></div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <div style="padding: 0px 0px 10px 25px !important">
                                                    <asp:Label ID="lblEmptyData"
                                                        Text="No data available..." runat="server" Visible="false">
                                                    </asp:Label>
                                                </div>
                                                </table>           
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>

                                <!--<div class="card-footer">
                            <br />
                            <a href="#">View all</a>
                        </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--pop for Leave Approval-->
    <div id="Approve_leave" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Leave Request Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-success alert-dismissible col-6 d-none" id="divSuccessMsg">
                        <a href="#" class="bg-transparent close" data-dismiss="alert" aria-label="close">&times;</a>
                        <label><span class="" id="lbl_Message"></span></label>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group dropdown">
                                <label>Employee Name  <span class="text-danger"></span></label>
                                <div class="">
                                    <input id="APP_NAME" class="form-control" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Leave Type <span class="text-danger"></span></label>
                                <div class="">
                                    <input id="APP_TYPE" class="form-control" type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6" id="modal_Approve_orignal" style="display: none">
                            <div class="form-group">
                                <label>Original Leave <span class="text-danger"></span></label>
                                <div class="">
                                    <textarea id="APP_ORG" class="form-control" readonly></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Leave Period  <span class="text-danger"></span></label>
                                <div class="">
                                    <input id="APP_PERIOD" class="form-control " type="text" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Leave Days <span class="text-danger"></span></label>
                                <input id="APP_DAYS" class="form-control" type="text" readonly>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Details  <span class="text-danger"></span></label>
                                <input id="APP_DETAILS" class="form-control" type="text" readonly>
                            </div>
                        </div>
                        <div class="col-lg-6" id="modal_Cancel_reason" style="display: none">
                            <div class="form-group">
                                <label>Reason For Cancellation  <span class="text-danger"></span></label>
                                <%--<input id="APP_CANCEL_REMARKS" class="form-control" type="text" readonly>--%>
                                <textarea id="APP_CANCEL_REMARKS" class="form-control" readonly></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6" id="modal_Approve_remarks">
                            <div class="form-group">
                                <label>Remarks   <span class="text-danger"></span></label>
                                <input id="txt_Remarks" class="form-control" type="text">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <a class="btn btn-primary" id="btnApprove" onclick="SaveApproval('A')" style="width: 100px;">Approve</a>
                            <a class="btn btn-danger text-white" id="btnReject" onclick="SaveApproval('R')" style="width: 100px;">Reject </a>
                            <a href="#" class="btn btn-warning" onclick="CloseModal();Closesa();" style="width: 100px;">Close </a>

                        </div>
                    </div>
                    <input type="hidden" id="hf_EMP_ID" />
                    <input type="hidden" id="hf_ELT_ID" />
                    <input type="hidden" id="hf_ActualELA_ID" />
                    <input type="hidden" id="hf_APS_ID" />
                    <input type="hidden" id="hf_FROM_DT" />
                    <input type="hidden" id="hf_TO_DT" />

                </div>
            </div>
        </div>
    </div>

     <%--Add COVID Test--%>
    <div id="Add_COVIDTest" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add COVID 19 Test Details
			<br />
                        <%-- <span class="text-muted" style="font-size: small !important; text-align: center;"><i>* All fields are mandatory</i></span>--%>
                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnCloseCovid" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="AddCOVIDForm">
                        <div class="row">
                            <div class="col-md-12 m-b-5"><span id="CovidError" class=""></span></div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Date of COVID Test<span class="text-danger">*</span></label>
                                    <div class="cal-icon-pick">
                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                        <input class="form-control datetimepicker1 txtDOCTestDate" type="text" value="" id="txtDOCTestDate" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Test Result<span class="text-danger">*</span></label>
                                    <select class="select form-control slctAddCovidTestResult" id="slctAddCovidTestResult">
                                        <option selected="selected" value="0">--Select--</option>
                                        <option value="NEGATIVE">NEGATIVE</option>
                                        <option value="POSITIVE">POSITIVE</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <label>COVID Test Upload <span class="text-muted" style="font-size: xx-small !important;"><i>Note:File of size max 10 MB</i></span></label>

                                <div class="form-group input-group">
                                    <input class="upload" type="file" id="fluplodCovidTest" />
                                </div>
                                <!-- form-group// -->
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 m-t-5">
                                <input id="btnAddCovidTest" type="button" class="btn btn-primary" value="Save" onclick="SaveCOVIDData(0);" />
                                <a onclick="Close_CovidPopUp();" class="btn btn-warning">Close</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <input id="btn_Show_ChildList" class="btn btn-primary btn-round float-right btn-sm" style="margin-left: 10px; display: none" data-toggle="modal" data-target="#Child_List" type="button" value="Add New">
    <input type="hidden" id="h_company_ID" />
    <input type="hidden" id="h_stu_ID" />
    <input type="hidden" id="h_emp_id" />
    <input type="hidden" id="h_cont_id" />
    <input type="hidden" id="h_file_path" />
    <input type="text" id="file_path" style="display: none;" />
    <input type="hidden" id="file_ext" />
    <input type="hidden" id="h_bsu_cty_id" runat="server" />
    <input type="hidden" id="h_EMP_SEX" runat="server" />
    <input runat="server" type="hidden" id="hfRequestVal" class="hfRequestVal" value="" />
    <asp:Button ID="btnJustClick" runat="server" OnClick="btnJustClick_Click" Text="click" />
    <input type="hidden" id="h_EMP_VISASTATUS" runat="server" />
    <input type="hidden" id="h_EMP_CTYId" runat="server" />

    <!-- / ADD Calendar icon click event -->
    <script type="text/javascript">
        $('.fa-calendar').on('click', function () {
            $(this).next().focus();
        });

        //Employee Apply Leave From date calendar icon trigger
        $('.calFrom').on('click', function () {

            $('#fromdateLeave').focus();
        });

        //Employee Apply Leave To date calendar icon trigger
        $('.calTo').on('click', function () {
            $('#todateLeave').focus();
        });
    </script>

    <!-- / ADD Profile Modal -->

    <script type="text/javascript">

        $(document).ready(function () {
            if ($(".hfRequestVal").val() == 'leave') {
                $(".btnapplyleave").click()
            } else if ($(".hfRequestVal").val() == 'pnorequest') {
                $(".btnPnO").click()
            }
            else if ($(".hfRequestVal").val() == 'employee') {
                $(".btnEmployee").click()
            }
            else if ($(".hfRequestVal").val() == 'dependant') {
                $(".btndependant").click()
            }
        })

        // Confirm on Delete
        function ConfirmDelete(id) {
            if (confirm('Are you sure?')) {
                DeleteDependantDetails(id);
            } else {

            }

        }

        // Close Add Dep

        function Closesa_AddDep() {
            document.getElementById('btnsave2').click();
            <%--if (document.getElementById('h_Action').value == '1') {
                //window.location.href = "index.aspx";
                document.getElementById('<%=btnJustClick.ClientID %>').click();
                //SetPieChart();
            }--%>

        }

         function Close_CovidPopUp() {
            document.getElementById('btnCloseCovid').click();
            <%--if (document.getElementById('h_Action').value == '1') {
                //window.location.href = "index.aspx";
                document.getElementById('<%=btnJustClick.ClientID %>').click();
                //SetPieChart();
            }--%>

        }

        // Close Edit Dep

        function Closesa_EditDep() {
            document.getElementById('btnSave3').click();
            <%-- if (document.getElementById('h_Action').value == '1') {
                //window.location.href = "index.aspx";
                document.getElementById('<%=btnJustClick.ClientID %>').click();
                // SetPieChart();
            }--%>
            //FeeConcessionPopup('<%= Session("EmployeeID")%>');
        }

        //Close Leave Window  

        function Closesa_Leave() {
            document.getElementById('btnSave5').click();
            <%--if (document.getElementById('h_Action').value == '1') {
               // window.location.href = "index.aspx";
                document.getElementById('<%=btnJustClick.ClientID %>').click();  //SetPieChart();
            }--%>
        }

        //Close  pno window 

        function ClosePnOWindow() {
            document.getElementById('btnSave4').click();
           <%-- if (document.getElementById('h_Action').value == '1') {
                //window.location.href = "index.aspx";
                document.getElementById('<%=btnJustClick.ClientID %>').click();
                  //SetPieChart();
            }--%>
        }

        //salary slip
        function onunloadbutton() {
            document.getElementById('<%=btnConSalDwnld.ClientID%>').style.display = "block";
            $("#lnkNo").show();
            // document.getElementById('lblSalLoader').style.display = "none";
            document.getElementById('btnsavepay_c').click();
            // document.getElementById('<%=btnJustClick.ClientID %>').click();
        }

        function onloadbutton() {
            // ShowGif();
            // document.getElementById('btnsavepay_c').click();
            document.getElementById('<%=btnConSalDwnld.ClientID%>').style.display = "none";
            $("#lnkNo").hide();
            //document.getElementById('lblSalLoader').style.display = "block";
            setTimeout(function () { onunloadbutton(); }, 4000);
        }
        //closing the cancel pop up
        function CancelClose() {
            $('#txtcomnts_can').val('');
            $('#divLCan').css('display', 'none');
        }


        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validateProfileMail() {
            var email = document.getElementById('<%=txtAlEmailEdit.ClientID%>').value;
            if (validateEmail(email)) {

            } else {
                ShowModalALert("Invalid Alternate Email Entered");
                document.getElementById('<%=txtAlEmailEdit.ClientID%>').value = "";
            }
            return false;
        }

        function ChktxtEmpInsuNoEdit() {
          <%--  var control = document.getElementById('<%=txtEmpInsuNoEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Insurance Number");
                document.getElementById('<%=txtEmpInsuNoEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Insurance Number Entered");
                document.getElementById('<%=txtEmpInsuNoEdit.ClientID%>').value = "";
                return true;
            }--%>
        }
        function ChktxtVisaIsuePlcEdit() {
            var control = document.getElementById('<%=txtVisaIsuePlcEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter VISA Issue Place");
                document.getElementById('<%=txtVisaIsuePlcEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid VISA Issue Place Entered");
                document.getElementById('<%=txtVisaIsuePlcEdit.ClientID%>').value = "";
                return true;
            }
        }
        function ChktxtUIDNoEdit() {
            var control = document.getElementById('<%=txtUIDNoEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter UID Details");
                document.getElementById('<%=txtUIDNoEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid UID Details Entered");
                document.getElementById('<%=txtUIDNoEdit.ClientID%>').value = "";
                return true;
            }
        }
        function ChktxtEmrCnt2() {
            var control = document.getElementById('<%=txtEmrCnt2.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Home Contact Details");
                document.getElementById('<%=txtEmrCnt2.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Home Contact Details");
                document.getElementById('<%=txtEmrCnt2.ClientID%>').value = "";
                return true;
            }
        }
        function ChktxtEmrCnt1() {
            var control = document.getElementById('<%=txtEmrCnt1.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Local Contact Details");
                document.getElementById('<%=txtEmrCnt1.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Local Contact Details");
                document.getElementById('<%=txtEmrCnt1.ClientID%>').value = "";
                return true;
            }
        }
        function ChktxtAlEmailEdit() {
            var control = document.getElementById('<%=txtAlEmailEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Alternate Email");
                document.getElementById('<%=txtAlEmailEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Alternate Email Entered");
                document.getElementById('<%=txtAlEmailEdit.ClientID%>').value = "";
                return true;
            }
            validateProfileMail();
        }

        function ChktxtPassprtIsuePlcEdit() {
            var control = document.getElementById('<%=txtPassprtIsuePlcEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Passport Issued Place");
                document.getElementById('<%=txtPassprtIsuePlcEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Passport Issued Place");
                document.getElementById('<%=txtPassprtIsuePlcEdit.ClientID%>').value = "";
                return true;
            }
        }

        function ChktxtPassportNoEdit() {
            var control = document.getElementById('<%=txtPassportNoEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Passport Number");
                document.getElementById('<%=txtPassportNoEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Passport Number");
                document.getElementById('<%=txtPassportNoEdit.ClientID%>').value = "";
                return true;
            }
        }

        function ChktxtEmrIdEdit() {
            var control = document.getElementById('<%=txtEmrIdEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Emirates ID Number");
                document.getElementById('<%=txtEmrIdEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Emirates ID Number Entered");
                document.getElementById('<%=txtEmrIdEdit.ClientID%>').value = "";
                return true;
            }
        }

        function chktxtPhnEmerCont2() {
            var control = document.getElementById('<%=txtPhnEmerCont2.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Emergency Phone Number(Home)");
                document.getElementById('<%=txtPhnEmerCont2.ClientID%>').value = "";
                return true;
            }
            if (isNaN(control) == true) {
                ShowModalALert("Invalid Emergency Phone Number(Home)");
                document.getElementById('<%=txtPhnEmerCont2.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Emergency Phone Number(Home)");
                document.getElementById('<%=txtPhnEmerCont2.ClientID%>').value = "";
                return true;
            }
        }

        function chktxtPhnEmerCont1() {
            var control = document.getElementById('<%=txtPhnEmerCont1.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Emergency Phone Number(Local)");
                document.getElementById('<%=txtPhnEmerCont1.ClientID%>').value = "";
                return true;
            }
            if (isNaN(control) == true) {
                ShowModalALert("Invalid Emergency Phone Number(Local)");
                document.getElementById('<%=txtPhnEmerCont1.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Emergency Phone Number(Local)");
                document.getElementById('<%=txtPhnEmerCont1.ClientID%>').value = "";
                return true;
            }
        }

        function chktxtPhnNumEdit() {
            var control = document.getElementById('<%=txtPhnNumEdit.ClientID%>').value;
            if (control == '') {
                ShowModalALert("Please Enter Alternate Phone Number");
                document.getElementById('<%=txtPhnNumEdit.ClientID%>').value = "";
                return true;
            }
            if (isNaN(control) == true) {
                ShowModalALert("Invalid Alternate Phone Number");
                document.getElementById('<%=txtPhnNumEdit.ClientID%>').value = "";
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Alternate Phone Number");
                document.getElementById('<%=txtPhnNumEdit.ClientID%>').value = "";
                return true;
            }
        }

        function ClearPnO() {
            $('#slctPnOtype').removeAttr('disabled');
            $('#slctPnOtype').val(0);
            $('#PnOAddTo').val("");
            $('#PnOEmpNotes').val("");
            $('#PnOAddressNotes').val("");
        }

        function ChktxtAddInsCrdNo() {
            //var control = $('#txtAddInsCrdNo').val();
            //if (control == '') {
            //    ShowModalALert("Please Enter Insurance Number");
            //    $('#txtAddInsCrdNo').val("");
            //    return true;
            //}
            //if (control.trim() == '') {
            //    ShowModalALert("Invalid Insurance Number Entered");
            //    $('#txtAddInsCrdNo').val("");
            //    return true;
            //}
        }

        function ChktxtAddVisaIssuePlc() {
            var control = $('#txtAddVisaIssuePlc').val();
            if (control == '') {
                ShowModalALert("Please Enter Visa Issued Place");
                $('#txtAddVisaIssuePlc').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Visa Issued Place Entered");
                $('#txtAddVisaIssuePlc').val("");
                return true;
            }
        }

        function ChktxtAddUIDNo() {
            var control = $('#txtAddUIDNo').val();
            if (control == '') {
                ShowModalALert("Please Enter UID Number");
                $('#txtAddUIDNo').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid UID Number Entered");
                $('#txtAddUIDNo').val("");
                return true;
            }
        }

        function ChktxtAddEID() {
            var control = $('#txtAddEID').val();
            if (control == '') {
                ShowModalALert("Please Enter Emirates ID Number");
                $('#txtAddEID').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Emirates ID Number Entered");
                $('#txtAddEID').val("");
                return true;
            }
        }

        function ChktxtAddPasprtNo() {
            var control = $('#txtAddPasprtNo').val();
            if (control == '') {
                ShowModalALert("Please Enter Passport Number");
                $('#txtAddPasprtNo').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Passport Number");
                $('#txtAddPasprtNo').val("");
                return true;
            }
        }
        function ChktxtDDGemsStaffIdStudentId() {
            var control = $('#txtDDGEMSStaffIdStudentId').val();
            if (control == '') {
                ShowModalALert("Please Enter " + $(".lblDDGemsStaffStudent").text());
                $('#txtDDGEMSStaffIdStudentId').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid " + $(".lblDDGemsStaffStudent").text());
                $('#txtDDGEMSStaffIdStudentId').val("");
                return true;
            } else {
                getDependentName(0)
            }
        }

        function ChktxtAddGemsStaffIdStudentId() {
            var control = $('#txtAddGEMSStaffIdStudentId').val();
            if (control == '') {
                ShowModalALert("Please Enter " + $(".lblAddGemsStaffStudent").text());
                $('#txtAddGEMSStaffIdStudentId').val("");
                return true;
            } else if (control.trim() == '') {
                ShowModalALert("Invalid " + $(".lblAddGemsStaffStudent").text());
                $('#txtAddGEMSStaffIdStudentId').val("");
                return true;
            } else {
                getDependentName(1)
            }
        }



        function ChktxtAddName() {
            var control = $('#txtAddDepedentName').val();
            if (control == '') {
                ShowModalALert("Please Enter Name");
                $('#txtAddDepedentName').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Name Entered");
                $('#txtAddDepedentName').val("");
                return true;
            }
        }

        function ChktxtDDInsCrdNo() {
            //var control = $('#txtDDInsCrdNo').val();
            //if (control == '') {
            //    ShowModalALert("Please Enter Insurance Number");
            //    $('#txtDDInsCrdNo').val("");
            //    return true;
            //}
            //if (control.trim() == '') {
            //    ShowModalALert("Invalid Insurance Number Entered");
            //    $('#txtDDInsCrdNo').val("");
            //    return true;
            //}
        }

        function ChktxtVisaFileNumber() {
            var control = $('#txtvisafilenumber').val();
            if (control == '') {
                ShowModalALert("Please Enter Visa File Number");
                $('#txtvisafilenumber').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Visa File Number Entered");
                $('#txtvisafilenumber').val("");
                return true;
            }
        }

        function ChktxtAddVisaFileNumber() {
            var control = $('#txtAddVisaFileNumber').val();
            if (control == '') {
                ShowModalALert("Please Enter Visa File Number");
                $('#txtAddVisaFileNumber').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Visa File Number Entered");
                $('#txtAddVisaFileNumber').val("");
                return true;
            }
        }

        function ChktxtDDVisaIsuePlc() {
            var control = $('#txtDDVisaIsuePlc').val();
            if (control == '') {
                ShowModalALert("Please Enter Visa Issued Place");
                $('#txtDDVisaIsuePlc').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Visa Issued Place Entered");
                $('#txtDDVisaIsuePlc').val("");
                return true;
            }
        }

        function ChktxtDDUID() {
            var control = $('#txtDDUID').val();
            if (control == '') {
                ShowModalALert("Please Enter UID Number");
                $('#txtDDUID').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid UID Number Entered");
                $('#txtDDUID').val("");
                return true;
            }
        }

        function ChktxtDDEIDNo() {
            var control = $('#txtDDEIDNo').val();
            if (control == '') {
                ShowModalALert("Please Enter Emirates ID Number");
                $('#txtDDEIDNo').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Emirates ID Number Entered");
                $('#txtDDEIDNo').val("");
                return true;
            }
        }

        function ChktxtDDPasprtNo() {
            var control = $('#txtDDPasprtNo').val();
            if (control == '') {
                ShowModalALert("Please Enter Passport Number");
                $('#txtDDPasprtNo').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Passport Number");
                $('#txtDDPasprtNo').val("");
                return true;
            }
        }

        function ChktxtDDName() {
            var control = $('#txtDDDepedentName').val();
            if (control == '') {
                ShowModalALert("Please Enter Name");
                $('#txtDDDepedentName').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Name Entered");
                $('#txtDDDepedentName').val("");
                return true;
            }
        }

        function CheckAddreNotes() {
            var control = $('#PnOAddressNotes').val();
            if (control == '') {
                ShowModalALert("Please Enter Addressing Notes");
                $('#PnOAddressNotes').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Addressing Notes Entered");
                $('#PnOAddressNotes').val("");
                return true;
            }
        }

        function CheckEmpNotepno() {
            var control = $('#PnOEmpNotes').val();
            if (control == '') {
                ShowModalALert("Please Enter Employee Notes Details");
                $('#PnOEmpNotes').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Employee Notes Details Entered");
                $('#PnOEmpNotes').val("");
                return true;
            }
        }

        function CheckAddTopno() {
            var control = $('#PnOAddTo').val();
            if (control == '') {
                ShowModalALert("Please Enter Address To Details");
                $('#PnOAddTo').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Address To Details Entered");
                $('#PnOAddTo').val("");
                return true;
            }
        }

        function checkForAddress() {
            //Commented by vikranth on 13th Feb 2020 for removing mandatory filed check, as confirmed by Charan  
            //var control = $('#addressLeave').val();
            //if (control == '') {
            //    ShowModalALert("Please Enter Address Details");
            //    $('#addressLeave').val("");
            //    return true;
            //}
            //if (control.trim() == '') {
            //    ShowModalALert("Invalid Address Details Entered");
            //    $('#addressLeave').val("");
            //    return true;
            //}
        }

        function checkForHandover() {
            //Commented by vikranth on 13th Feb 2020 for removing mandatory filed check, as confirmed by Charan  
            //var control = $('#workhandover').val();
            //if (control == '') {
            //    ShowModalALert("Please Enter Handover Details");
            //    $('#workhandover').val("");
            //    return true;
            //}
            //if (control.trim() == '') {
            //    ShowModalALert("Invalid Handover Details Entered");
            //    $('#workhandover').val("");
            //    return true;
            //}
        }

        function checkForSpace() {

            var control = $('#leaveDetail').val();
            if (control == '') {
                ShowModalALert("Please Enter Remarks");
                $('#leaveDetail').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Remarks Entered");
                $('#leaveDetail').val("");
                return true;
            }

        }

        function CheckNumber() {
            var control = $('#LeavePhone').val();
            if (control == '') {
                ShowModalALert("Please Enter Phone Number");
                $('#LeavePhone').val("");
                return true;
            }
            if (isNaN(control) == true) {
                ShowModalALert("Invalid Phone number");
                $('#LeavePhone').val("");
                return true;
            }
            if (control.trim() == '') {
                ShowModalALert("Invalid Phone Number Entered");
                $('#LeavePhone').val("");
                return true;
            }
        }
        //function GetMenuAccess() {
        //    $.ajax({
        //        type: "POST",
        //        url: "Index.aspx/getAccessDetailstoMenu",
        //       // data: '{myELAID: "' + ELA_ID + '"}',
        //        contentType: "application/json; charset=utf-8",
        //        success: function (response) {
        //            var a = results.d.split('||');
        //            ShowModalALert(a);
        //        }
        //    });
        //}

        function AttendanceLogPopup(emp_id) {
            var url = "CORP_StaffAttendance_popup.aspx"
            Popup(url);
        }

        function ViewLeaveStatus(ELA_ID) {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/getLeaveApprovalLeavel") %>',
                data: '{myELAID: "' + ELA_ID + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    //var a = results.d.split('||');
                    var arraylength = response.d.length;
                    $("#gvApprovals").empty();

                    if (arraylength > 0) {
                        $("#gvApprovals").append("<br/><div class='modal-title'>Leave Approvals</div>");
                        $("#gvApprovals").append("<table width=100%><tbody><tr width='100%' class='table'><th width='20%'>Approver</th><th width='20%'>Status</th><th width='20%'>Remarks</th><th width='20%'>Applied Date</th><th width='20%'>Approved Date</th></tr></tbody></table>")
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {

                            $("#gvApprovals").append("<table width=100%><tbody><tr width='100%' class='table'><td width='20%'>" + response.d[i].split("||")[0] + "</td><td width='20%'>" + response.d[i].split("||")[1] + "</td><td width='20%'>" + response.d[i].split("||")[2] + "</td><td width='20%'>" + response.d[i].split("||")[3] + "</td><td width='20%'>" + response.d[i].split("||")[4] + "</td></tr></tbody></table>");
                        }
                    }

                }
            });

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/getLeaveApplicationHistory") %>',
                data: '{myELAID: "' + ELA_ID + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    //var a = results.d.split('||');
                    var arraylength = response.d.length;


                    if (arraylength > 0) {
                        $("#gvApprovals").append("<br/><div class='modal-title'>Leave Application History</div>");
                        $("#gvApprovals").append("<table width=100%><tbody><tr width='100%' class='table'><th width='20%'>Leave Type</th><th width='10%'>Request From</th><th width='10%'>Request To</th><th width='20%'>Remarks</th><th width='10%'>Status</th><th width='20%'>Action</th><th width='10%'>Log Date</th></tr></tbody></table>")

                        for (var i = 0; i < arraylength; i++) {

                            $("#gvApprovals").append("<table width=100%><tbody><tr width='100%' class='table'><td width='20%'>" + response.d[i].split("||")[0] + "</td><td width='10%'>" + response.d[i].split("||")[1] + "</td><td width='10%'>" + response.d[i].split("||")[2] + "</td><td width='20%'>" + response.d[i].split("||")[3] + "</td><td width='10%'>" + response.d[i].split("||")[4] + "</td><td width='20%'>" + response.d[i].split("||")[6] + "</td><td width='10%'>" + response.d[i].split("||")[5] + "</td></tr></tbody></table>");
                        }
                    }

                }
            });
        }

        function CalnderMonthhange() {
            // ShowModalALert($('#calndrMonth').val());
            var e = document.getElementById("calndrMonth");
            var strUser = e.options[e.selectedIndex].value;


            var e1 = document.getElementById("calndrYear");
            var strUser1 = e1.options[e1.selectedIndex].value;


            var monthsel;
            if (strUser == "Jan") {
                monthsel = 0;
            } else if (strUser == "Feb") {
                monthsel = 1;
            } else if (strUser == "Mar") {
                monthsel = 2;
            } else if (strUser == "Apr") {
                monthsel = 3;
            } else if (strUser == "May") {
                monthsel = 4;
            } else if (strUser == "Jun") {
                monthsel = 5;
            } else if (strUser == "Jul") {
                monthsel = 6;
            } else if (strUser == "Aug") {
                monthsel = 7;
            } else if (strUser == "Sep") {
                monthsel = 8;
            } else if (strUser == "Oct") {
                monthsel = 9;
            } else if (strUser == "Nov") {
                monthsel = 10;
            } else if (strUser == "Dec") {
                monthsel = 11;
            }

            //var date = new Date();
            var firstDay = new Date(strUser1, monthsel, 1);
            var lastDay = new Date(strUser1, monthsel + 1, 0);
            var dayse = new Date(strUser1, monthsel + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1) + '/' + firstDay.getDate()) + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1) + '/' + lastDay.getDate()) + '/' + lastDay.getFullYear();


            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/ShowLeaveCalculationWeb") %>',
                data: '{FromDate: "' + firstDayWithSlashes + '", ToDate: "' + lastDayWithSlashes + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var arraylength = response.d.length;
                    var caldays = document.getElementById('caldays')
                    caldays.innerHTML = ""
                    var date1 = new Date();
                    var todaydate = date1.getDate();
                    if (arraylength > 0) {
                        for (var i = 0; i < arraylength; i++) {
                            var dated = response.d[i].split("||")[0];
                            var dayd = response.d[i].split("||")[1];
                            var Remarks = response.d[i].split("||")[2];
                            var backcolor = response.d[i].split("||")[3];
                            var forecolor = response.d[i].split("||")[4];
                            var id = response.d[i].split("||")[5];
                            if (i < dayse.getDate()) {

                                if (dayd == "Sunday") {
                                    var lis = document.createElement('li');
                                    lis.value = id;
                                    lis.innerHTML = id;
                                    lis.id = "lvli" + id;
                                    if (backcolor != "") {
                                        lis.style.backgroundColor = backcolor;
                                        lis.style.color = forecolor;
                                    }
                                    if (Remarks != "") {
                                        lis.title = Remarks;
                                    }
                                    if (id == todaydate.toString()) {
                                        // lis.attributes("class", ".days li .active");
                                        //  lis.className = ".days li .activeli";
                                        lis.className = "activeli"
                                    }
                                    caldays.appendChild(lis);

                                } else if (dayd == "Monday") {
                                    if (id == "1") {
                                        var lis = document.createElement('li');
                                        lis.value = "";
                                        lis.className = "border-0";
                                        lis.innerHTML = "";
                                        caldays.appendChild(lis);

                                        var lis1 = document.createElement('li');
                                        lis1.value = id;
                                        lis1.innerHTML = id;
                                        lis1.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis1.style.backgroundColor = backcolor;
                                            lis1.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis1.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis1.className = "activeli"
                                        }
                                        caldays.appendChild(lis1);
                                    }
                                    else {
                                        var lis1 = document.createElement('li');
                                        lis1.value = id;
                                        lis1.innerHTML = id;
                                        lis1.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis1.style.backgroundColor = backcolor;
                                            lis1.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis1.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis1.className = "activeli"
                                        }
                                        caldays.appendChild(lis1);
                                    }
                                } else if (dayd == "Tuesday") {
                                    if (id == "1") {
                                        var lis = document.createElement('li');
                                        lis.value = "";
                                        lis.className = "border-0";
                                        lis.innerHTML = "";
                                        caldays.appendChild(lis);

                                        var lis1 = document.createElement('li');
                                        lis1.value = "";
                                        lis1.className = "border-0";
                                        lis1.innerHTML = "";
                                        caldays.appendChild(lis1);

                                        var lis2 = document.createElement('li');
                                        lis2.value = id;
                                        lis2.innerHTML = id;
                                        lis2.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis2.style.backgroundColor = backcolor;
                                            lis2.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis2.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis2.className = "activeli"
                                        }
                                        caldays.appendChild(lis2);
                                    }
                                    else {
                                        var lis2 = document.createElement('li');
                                        lis2.value = id;
                                        lis2.innerHTML = id;
                                        lis2.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis2.style.backgroundColor = backcolor;
                                            lis2.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis2.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis2.className = "activeli"
                                        }
                                        caldays.appendChild(lis2);
                                    }

                                } else if (dayd == "Wednesday") {
                                    if (id == "1") {
                                        var lis = document.createElement('li');
                                        lis.value = "";
                                        lis.className = "border-0";
                                        lis.innerHTML = "";
                                        caldays.appendChild(lis);

                                        var lis1 = document.createElement('li');
                                        lis1.value = "";
                                        lis1.className = "border-0";
                                        lis1.innerHTML = "";
                                        caldays.appendChild(lis1);

                                        var lis2 = document.createElement('li');
                                        lis2.value = "";
                                        lis2.className = "border-0";
                                        lis2.innerHTML = "";
                                        caldays.appendChild(lis2);

                                        var lis3 = document.createElement('li');
                                        lis3.value = id;
                                        lis3.innerHTML = id;
                                        lis3.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis3.style.backgroundColor = backcolor;
                                            lis3.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis3.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis3.className = "activeli"
                                        }
                                        caldays.appendChild(lis3);
                                    }
                                    else {
                                        var lis3 = document.createElement('li');
                                        lis3.value = id;
                                        lis3.innerHTML = id;
                                        lis3.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis3.style.backgroundColor = backcolor;
                                            lis3.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis3.title = Remarks;
                                        }
                                        if (id == todaydate) {

                                            //  lis.attributes("class", ".days li .active");
                                            // lis3.className = ".days li .activeli";
                                            lis3.className = "activeli"
                                        }
                                        caldays.appendChild(lis3);
                                    }

                                } else if (dayd == "Thursday") {
                                    if (id == "1") {
                                        var lis = document.createElement('li');
                                        lis.value = "";
                                        lis.className = "border-0";
                                        lis.innerHTML = "";
                                        caldays.appendChild(lis);

                                        var lis1 = document.createElement('li');
                                        lis1.value = "";
                                        lis1.className = "border-0";
                                        lis1.innerHTML = "";
                                        caldays.appendChild(lis1);

                                        var lis2 = document.createElement('li');
                                        lis2.value = "";
                                        lis2.className = "border-0";
                                        lis2.innerHTML = "";
                                        caldays.appendChild(lis2);

                                        var lis3 = document.createElement('li');
                                        lis3.value = "";
                                        lis3.className = "border-0";
                                        lis3.innerHTML = "";
                                        caldays.appendChild(lis3);

                                        var lis4 = document.createElement('li');
                                        lis4.value = id;
                                        lis4.innerHTML = id;
                                        lis4.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis4.style.backgroundColor = backcolor;
                                            lis4.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis4.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis4.className = "activeli"
                                        }
                                        caldays.appendChild(lis4);
                                    }
                                    else {
                                        var lis4 = document.createElement('li');
                                        lis4.value = id;
                                        lis4.innerHTML = id;
                                        lis4.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis4.style.backgroundColor = backcolor;
                                            lis4.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis4.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            lis4.className = "activeli";

                                        }
                                        caldays.appendChild(lis4);
                                    }
                                } else if (dayd == "Friday") {
                                    if (id == "1") {
                                        var lis = document.createElement('li');
                                        lis.value = "";
                                        lis.className = "border-0";
                                        lis.innerHTML = "";
                                        caldays.appendChild(lis);

                                        var lis1 = document.createElement('li');
                                        lis1.value = "";
                                        lis1.className = "border-0";
                                        lis1.innerHTML = "";
                                        caldays.appendChild(lis1);

                                        var lis2 = document.createElement('li');
                                        lis2.value = "";
                                        lis2.className = "border-0";
                                        lis2.innerHTML = "";
                                        caldays.appendChild(lis2);

                                        var lis3 = document.createElement('li');
                                        lis3.value = "";
                                        lis3.className = "border-0";
                                        lis3.innerHTML = "";
                                        caldays.appendChild(lis3);

                                        var lis4 = document.createElement('li');
                                        lis4.value = "";
                                        lis4.className = "border-0";
                                        lis4.innerHTML = "";
                                        caldays.appendChild(lis4);

                                        var lis5 = document.createElement('li');
                                        lis5.value = id;
                                        lis5.innerHTML = id;
                                        lis5.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis5.style.backgroundColor = backcolor;
                                            lis5.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis5.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis5.className = "activeli"
                                        }
                                        caldays.appendChild(lis5);
                                    }
                                    else {
                                        var lis5 = document.createElement('li');
                                        lis5.value = id;
                                        lis5.innerHTML = id;
                                        lis5.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis5.style.backgroundColor = backcolor;
                                            lis5.style.color = forecolor;
                                        }

                                        if (Remarks != "") {
                                            lis5.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis5.className = "activeli"
                                        }
                                        caldays.appendChild(lis5);
                                    }

                                } else if (dayd == "Saturday") {
                                    if (id == "1") {
                                        var lis = document.createElement('li');
                                        lis.value = "";
                                        lis.className = "border-0";
                                        lis.innerHTML = "";
                                        caldays.appendChild(lis);

                                        var lis1 = document.createElement('li');
                                        lis1.value = "";
                                        lis1.className = "border-0";
                                        lis1.innerHTML = "";
                                        caldays.appendChild(lis1);

                                        var lis2 = document.createElement('li');
                                        lis2.value = "";
                                        lis2.className = "border-0";
                                        lis2.innerHTML = "";
                                        caldays.appendChild(lis2);

                                        var lis3 = document.createElement('li');
                                        lis3.value = "";
                                        lis3.className = "border-0";
                                        lis3.innerHTML = "";
                                        caldays.appendChild(lis3);

                                        var lis4 = document.createElement('li');
                                        lis4.value = "";
                                        lis4.className = "border-0";
                                        lis4.innerHTML = "";
                                        caldays.appendChild(lis4);

                                        var lis5 = document.createElement('li');
                                        lis5.value = "";
                                        lis5.className = "border-0";
                                        lis5.innerHTML = "";
                                        caldays.appendChild(lis5);

                                        var lis6 = document.createElement('li');
                                        lis6.value = id;
                                        lis6.innerHTML = id;
                                        lis6.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis6.style.backgroundColor = backcolor;
                                            lis6.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis6.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis6.className = "activeli"

                                        }
                                        caldays.appendChild(lis6);
                                    }
                                    else {
                                        var lis6 = document.createElement('li');
                                        lis6.value = id;
                                        lis6.innerHTML = id;
                                        lis6.id = "lvli" + id;
                                        if (backcolor != "") {
                                            lis6.style.backgroundColor = backcolor;
                                            lis6.style.color = forecolor;
                                        }
                                        if (Remarks != "") {
                                            lis6.title = Remarks;
                                        }
                                        if (id == todaydate.toString()) {
                                            // lis.attributes("class", ".days li .active");
                                            //  lis.className = ".days li .activeli";
                                            lis6.className = "activeli"

                                        }
                                        caldays.appendChild(lis6);
                                    }
                                }
                            }
                            else {

                                if ((dated.indexOf(strUser) != -1) && (dated.split('/')[2].indexOf(strUser1.slice(-2)) != -1)) {

                                    var datetocheck = dated.split('/')[0];
                                    var idtocheck = 'lvli' + (Number(datetocheck)).toString();
                                    var leavedateli = document.getElementById(idtocheck);
                                    leavedateli.style.backgroundColor = backcolor;
                                    leavedateli.style.color = forecolor;
                                    leavedateli.title = Remarks;
                                }
                            }
                        }
                    }
                }

            });
            ShowCalendarEvents(firstDayWithSlashes, lastDayWithSlashes);
        }


        function ShowCalendarEvents(fromdate, todate) {

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/ShowCalendarEvents") %>',
                data: '{FromDate: "' + fromdate + '", ToDate: "' + todate + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] != "") {
                        $('#spnShowEvents').html(a[0]);

                    } else {
                        $('#spnShowEvents').html("");
                    }
                }
            });
        }

        function DeleteMedcalLeaveFile(id) {
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/DeleteFileFromDB") %>',
                data: '{DocId: "' + id + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] == "0") {
                        $('#LeaveError').text("Document deleted successfully.");
                        $('#LeaveError').attr("class", "alert alert-info");
                        $('#showLeavefile').css('display', 'none');
                        $('#deleteLeaveFile').css('display', 'none');
                    }
                    else {
                        $('#LeaveError').text("Could not process your request.");
                        $('#LeaveError').attr("class", "alert alert-info");
                    }
                }
            });
        }

        function RadChange(id, controlType) {

            if (id == '0') {
                $('#txtDDUnit').attr("disabled", "disabled");
                $('#txtAddUnit').attr("disabled", "disabled");
                $('#txtDDUnit').val("");
                $('#txtAddUnit').val("");
                $(".divGems").css('display', 'none');
                $('#txtAddDepedentName').removeAttr("disabled");
                $('#txtDDDepedentName').removeAttr("disabled");
                $(".lblGemsStudId").css('display', 'none');
                if (controlType == "DD") {
                    $('#txtDDDepedentName').val("");
                    $('#txtDDGEMSStaffIdStudentId').val("");
                } else {
                    $('#txtAddDepedentName').val("");
                    $('#txtAddGEMSStaffIdStudentId').val("");
                }
            } else if (id == '1') {
                $('#txtDDUnit').removeAttr("disabled");
                $('#txtAddUnit').removeAttr("disabled");
                $(".divGems").css('display', 'block');
                $('#txtAddDepedentName').attr("disabled", "disabled");
                $('#txtDDDepedentName').attr("disabled", "disabled");
                if (controlType == "DD")
                    getDependentName(0)
                else getDependentName(1)
                $(".lblDDGemsStaffStudent").text("Staff Id");
                $(".lblGemsStudId").css('display', 'none');
                $(".lblAddGemsStaffStudent").text("Staff Id");
                GetBusinessUnits("E")
            } else if (id == '2') {
                $('#txtDDUnit').removeAttr("disabled");
                $('#txtAddUnit').removeAttr("disabled");
                $(".divGems").css('display', 'block');
                $(".lblGemsStudId").css('display', 'block');
                $('#txtAddDepedentName').attr("disabled", "disabled");
                $('#txtDDDepedentName').attr("disabled", "disabled");
                if (controlType == "DD")
                    getDependentName(0)
                else getDependentName(1)
                $(".lblDDGemsStaffStudent").text("Student Id");
                $(".lblAddGemsStaffStudent").text("Student Id");
                GetBusinessUnits("S")
            }
        }



        function CancelLeave(Leave_Id, LeaveApp_Id, Leave_Status) {
            var comments = "";
            var ReqCan = '0';
            if (Leave_Status == '0') {
                Leave_Status = 'C';
            }
            if (Leave_Status.toLowerCase() == 'approved') {

                $('#divLCan').css('display', 'block');
                $('#btnokComCan').attr('onclick', 'CancelLeave(' + '0' + ',' + LeaveApp_Id + ',' + '0' + ');');

            } else {

                if (Leave_Status == 'C') {
                    ReqCan = '1';

                    comments = $('#txtcomnts_can').val();
                    if (comments == '') {
                        ShowModalALert("Please enter your reason for cancellation");
                        return true;
                    }
                }

                var canObj = {};
                canObj.ID = LeaveApp_Id;
                canObj.LeaveCan = ReqCan;
                canObj.Comments = comments;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/GemsEss/Index.aspx/CancelLeave") %>',
                    data: JSON.stringify(canObj),
                    //data: "{'ID': '" + LeaveApp_Id + "','LeaveCan': '" + ReqCan + "','Comments':'" + comments + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                        var a = results.d.split('||');
                        if (a == '0') {
                            $('#h_Action').val('1');
                            Closesa();
                        } else {
                            $('#h_Action').val('1');
                            Closesa();
                        }
                    }
                });
            }
        }

        function InitCounrtyInfo(id) {
            var prefix;
            $('#h_cont_id').val(0);
            if (id == 0) {
                prefix = $('#txtDDNationality').val();
            } else if (id == 1) {
                prefix = $('#txtAddNAt').val();
            }

            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetCountry") %>',
                data: "{ 'prefix': '" + prefix + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;

                    if (id == 0) {
                        $("#divCountry").empty();
                        $("#divCountry").css("display", "block");
                    } else if (id == 1) {
                        $("#divCountry1").empty();
                        $("#divCountry1").css("display", "block");
                    }

                    if (arraylength > 0) {
                        var html = "<div class='container'>";
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var value = response.d[i].split("||")[0];
                            var cid = response.d[i].split("||")[1];
                            var value2 = value.replace(/\s+/g, '_');

                            html = html + "<input type='button' class='auto-comp-link' value='";
                            html = html + value + "'" + " onclick=clickOnLinkCont('" + id + "','" + cid + "','" + value2 + "');></input>";
                            //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");                                                                                      
                        }
                        html = html + "</div>";

                        if (id == 0) {
                            $('#divCountry').append(html);
                        } else if (id == 1) {
                            $('#divCountry1').append(html);
                        }
                    }
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }
            });

        }

        function GetCountry() {
            var prefix = "";
            $('#h_cont_id').val(0);
            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetCountry") %>',
                data: "{ 'prefix': '" + prefix + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var arraylength = response.d.length;
                    $('#<%= ddlDDResidenceCountry.ClientID %>').empty();
                    $('#<%= ddlAddResidenceCountry.ClientID %>').empty();
                    $('#<%= ddlDDNationality.ClientID %>').empty();
                    $('#<%= ddlAddNationality.ClientID %>').empty();

                    if (arraylength > 0) {

                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var newOption = "<option value='" + response.d[i].split("||")[1] + "'>" + response.d[i].split("||")[0] + "</option>";
                            $('#<%= ddlDDResidenceCountry.ClientID %>').append(newOption);
                            $('#<%= ddlAddResidenceCountry.ClientID %>').append(newOption);
                            $('#<%= ddlDDNationality.ClientID %>').append(newOption);
                            $('#<%= ddlAddNationality.ClientID %>').append(newOption);
                        }
                        document.getElementById('<%=ddlAddResidenceCountry.ClientID%>').value = parseInt(document.getElementById('<%=h_bsu_cty_id.ClientID%>').value)
                        $('#<%= ddlDDResidenceCountry.ClientID %>').change();
                        $('#<%= ddlAddResidenceCountry.ClientID %>').change();
                    }
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }
            });

        }

        function GetBusinessUnits(prefix) {

            //$('#txtDDUnit').autocomplete({
            //source: function (request, response) {
            var SearchFor = ""
            //var prefix = "";
            $('#h_company_ID').val(0);

            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetCompany") %>',
                data: "{ 'prefix': '" + prefix + "','userName':'" + '<%= Session("sUsr_name")%>' + "', 'SearchFor':'" + SearchFor + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;
                    $('#<%= ddlDDUnit.ClientID %>').empty();
                    $('#<%= ddlAddUnit.ClientID %>').empty();
                    if (arraylength > 0) {
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var newOption = "<option value='" + response.d[i].split("||")[1] + "'>" + response.d[i].split("||")[0] + "</option>";
                            $('#<%= ddlDDUnit.ClientID %>').append(newOption);
                            $('#<%= ddlAddUnit.ClientID %>').append(newOption);
                        }
                    }
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }

            });
        }

        function clickOnLinkCont(id, cid, countryname) {

            countryname = countryname.replace(/_/g, ' ');
            if (id == 0) {
                $('#txtDDNationality').val(countryname);
                $("#divCountry").css("display", "none");
            } else if (id == 1) {
                $('#txtAddNAt').val(countryname);
                $("#divCountry1").css("display", "none");
            }
            $('#h_cont_id').val(cid);
        }

        function InitStudentAutoComplete(id) {
            var prefix;
            var Type;
            $('#h_stu_ID').val(0);
            $('#h_emp_id').val(0);

            var SearchFor;
            if (id == 0) {
                prefix = $('#txtDDName').val();
                Type = $("#EditFamilyForm input[type='radio']:checked").val();

            } else if (id == 1) {
                prefix = $('#txtAddName').val();
                Type = $("#AddFamilyForm input[type='radio']:checked").val();
            }


            if (isNaN(prefix) == true) {
                SearchFor = 'NAME';
            }
            else {
                SearchFor = 'ID';
            }

            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetStudents") %>',
                data: "{'Type': '" + Type + "','prefix': '" + prefix + "','COMP_ID':'" + $('#h_company_ID').val() + "','ACD_ID':'" + '<%=Session("Current_ACD_ID")%>' + "','SearchFor':'" + SearchFor + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;

                    if (id == 0) {
                        $("#divStudent").empty();
                        $("#divStudent").css("display", "block");
                    } else if (id == 1) {
                        $("#divStudent1").empty();
                        $("#divStudent1").css("display", "block");
                    }

                    if (arraylength > 0) {
                        var html = "<div class='container'>";
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {

                            if (SearchFor == 'NAME') {
                                var value = response.d[i].split("||")[0];
                                var cid = response.d[i].split("||")[1];
                                var value2 = value.replace(/\s+/g, '_');
                                var Type2 = Type.replace(/\s+/g, '_');

                                html = html + "<input type='button' class='auto-comp-link' value='";
                                html = html + value + "'" + " onclick=clickOnLinkStud('" + id + "','" + cid + "','" + value2 + "','" + Type2 + "');></input>";
                                //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");
                            } else if (SearchFor = 'ID') {
                                var value = response.d[i].split("||")[2];
                                var cid = response.d[i].split("||")[1];
                                var value2 = value.replace(/\s+/g, '_');
                                var Type2 = Type.replace(/\s+/g, '_');

                                html = html + "<input type='button' class='auto-comp-link' value='";
                                html = html + value + "'" + " onclick=clickOnLinkStud('" + id + "','" + cid + "','" + value2 + "','" + Type2 + "');></input>";
                                //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");
                            }

                        }
                        html = html + "</div>";

                        if (id == 0) {
                            $('#divStudent').append(html);
                        } else if (id == 1) {
                            $('#divStudent1').append(html);
                        }
                    }
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }
            });

        }

        function clickOnLinkStud(id, stuid, stuname, type) {

            stuname = stuname.replace(/_/g, ' ');
            if (id == 0) {
                $('#txtDDName').val(stuname);
                $("#divStudent").css("display", "none");
            } else if (id == 1) {
                $('#txtAddName').val(stuname);
                $("#divStudent1").css("display", "none");
            }

            if (type == 'Gems_Student') {
                $('#h_stu_ID').val(stuid);

            } else if (type == 'Gems_Staff') {
                $('#h_emp_id').val(stuid);

            }

        }

        function InitCompanyAutoComplete(id) {

            //$('#txtDDUnit').autocomplete({
            //source: function (request, response) {
            var prefix;
            $('#h_company_ID').val(0);
            var SearchFor;
            if (id == 0) {
                prefix = $('#txtDDUnit').val();
            } else if (id == 1) {
                prefix = $('#txtAddUnit').val();
            }

            if (isNaN(prefix) == true) {
                SearchFor = 'BSU_NAME';
            }
            else {
                SearchFor = 'BSU_ID';
            }
            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetCompany") %>',
                data: "{ 'prefix': '" + prefix + "','userName':'" + '<%= Session("sUsr_name")%>' + "', 'SearchFor':'" + SearchFor + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var arraylength = response.d.length;

                    if (id == 0) {
                        $("#divCompany").empty();
                        $("#divCompany").css("display", "block");
                    } else if (id == 1) {
                        $("#divCompany1").empty();
                        $("#divCompany1").css("display", "block");
                    }

                    if (arraylength > 0) {
                        var html = "<div class='container'>";
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var value = response.d[i].split("||")[0];
                            var cid = response.d[i].split("||")[1];
                            var value2 = value.replace(/\s+/g, '_');
                            html = html + "<input type='button' class='auto-comp-link' value='";
                            html = html + value + "' title='" + value + "' onclick=clickOnLink('" + id + "','" + cid + "','" + value2 + "');></input>";
                            //$('#dvStudents').append("<div class='row'>" + re'sponse.d[i]. STU_NAME + "</div>");
                        }
                        html = html + "</div>";

                        if (id == 0) {
                            $('#divCompany').append(html);
                        } else if (id == 1) {
                            $('#divCompany1').append(html);
                        }

                    }
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }

            });
        }

        function clickOnLink(id, bsuid, bsuname) {

            bsuname = bsuname.replace(/_/g, ' ');
            if (id == 0) {
                $('#txtDDUnit').val(bsuname);
                $("#divCompany").css("display", "none");
            } else if (id == 1) {
                $('#txtAddUnit').val(bsuname);
                $("#divCompany1").css("display", "none");
            }
            $('#h_company_ID').val(bsuid);

        }

        function GetPnODocDetails(id, viewedit) {

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetPnODocDtls") %>',
                data: '{ID: "' + id + '", Usrname: "' + '<%= Session("sUsr_name")%>' + '", Bsuid: "' + '<%= Session("sbsuid")%>' + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] == '0') {
                        if (viewedit == 'View & Edit') {
                            $('#btnSavePnO').val("Update");
                            $('#btnSavePnO').attr('onclick', 'SavePnODocuments(' + id + ');');
                            $('#slctPnOtype').val(a[1]);
                            $('#slctPnOtype').attr('disabled', true);
                            $('#PnOAddTo').val(a[4]);
                            $('#PnOEmpNotes').val(a[5]);
                            $('#PnOAddressNotes').val(a[3]);

                            $('#hdnRecordDax').val(a[2]);
                        }
                        else {
                            $('#btnSavePnO').css('display', 'none');
                            $('#slctPnOtype').val(a[1]);
                            $('#slctPnOtype').attr('disabled', true);
                            $('#PnOAddTo').val(a[4]);
                            $('#PnOAddTo').attr('disabled', true);
                            $('#PnOEmpNotes').val(a[5]);
                            $('#PnOEmpNotes').attr('disabled', true);
                            $('#PnOAddressNotes').val(a[3]);
                            $('#PnOAddressNotes').attr('disabled', true);
                        }

                    } else {
                        $('#btnSavePnO').val("Update");
                    }
                }
            });
        }

        function FillLeaveValues(id, leaveappid) {

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetLeaveDtls") %>',
                data: '{ELAID: "' + id + '", Usrname: "' + '<%= Session("sUsr_name")%>' + '", Bsuid: "' + '<%= Session("sbsuid")%>' + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] == '0') {

                        $('#btnSaveLeave').val("Update")
                        $('#btnSaveLeave').attr('onclick', 'SaveLeave(' + leaveappid + ');')
                        $('#slctLeaveAppltype').val(a[1]);
                        $('#fromdateLeave').val(a[2]);
                        $('#todateLeave').val(a[3]);
                        $('#leaveDetail').val(a[4]);
                        $('#workhandover').val(a[5]);
                        $('#addressLeave').val(a[6]);
                        $('#LeavePhone').val(a[7]);
                        if (a[1] == 'ML') {
                            if (a[10] != "0") {
                                $('#rowLeaveMLShowFile').css('display', 'block');
                                var leaveurl = "../Common/FileHandler.ashx?TYPE=" + a[8] + "&DocID=" + a[9];
                                $('#showLeavefile').attr('href', leaveurl);
                                $('#deleteLeaveFile').attr('onclick', 'DeleteMedcalLeaveFile(' + a[10] + ')');
                            }
                        }
                    } else {
                        $('#btnSaveLeave').val("Update");
                    }
                    LeaveDateChange(1);
                    LeaveTypeSelectionChange();
                }
            });
        }



        function Callupdatecurrentmonthandyear() {
            var icurYear = '<%= Session("BSU_PAYYEAR")%>'
            var d = new Date();
            var n = d.getMonth();
            var Y = d.getFullYear();
            n = n + 1;
            if (n == 1) {
                $("#calndrMonth option[value='Jan']").attr('selected', 'selected');

            } else if (n == 2) {
                $("#calndrMonth option[value='Feb']").attr('selected', 'selected');

            } else if (n == 3) {
                $("#calndrMonth option[value='Mar']").attr('selected', 'selected');

            } else if (n == 4) {
                $("#calndrMonth option[value='Apr']").attr('selected', 'selected');

            } else if (n == 5) {
                $("#calndrMonth option[value='May']").attr('selected', 'selected');

            } else if (n == 6) {
                $("#calndrMonth option[value='Jun']").attr('selected', 'selected');

            } else if (n == 7) {
                $("#calndrMonth option[value='Jul']").attr('selected', 'selected');

            } else if (n == 8) {
                $("#calndrMonth option[value='Aug']").attr('selected', 'selected');

            } else if (n == 9) {
                $("#calndrMonth option[value='Sep']").attr('selected', 'selected');

            } else if (n == 10) {
                $("#calndrMonth option[value='Oct']").attr('selected', 'selected');

            } else if (n == 11) {
                $("#calndrMonth option[value='Nov']").attr('selected', 'selected');

            } else if (n == 12) {
                $("#calndrMonth option[value='Dec']").attr('selected', 'selected');

            }


            $("#calndrYear option[value='" + icurYear + "']").attr('selected', 'selected');

            loadcalenderwithcolor();
        }

        function loadcalenderwithcolor() {

        }





        function CallLeaveTypes() {
            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetLeaveTypes") %>',
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var arraylength = response.d.length;
                    if (arraylength > 0) {
                        $('#slctLeaveAppltype').empty();
                        //$('#dvStudents').append("<div class='container'>");
                        for (var i = 0; i < arraylength; i++) {
                            var newOption = "<option value='" + response.d[i].split("||")[1] + "'>" + response.d[i].split("||")[0] + "</option>";
                            $('#slctLeaveAppltype').append(newOption);
                        }
                    }
                    LeaveTypeSelectionChange();
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }
            });
        }

        function ChangeYear() {
            var selectBox = document.getElementById('<%=slctYear.ClientID %>');
            var slctAddVisaSpnsr = selectBox.options[selectBox.selectedIndex].value;
            document.getElementById('<%=hdnYear.ClientID %>').value = slctAddVisaSpnsr;
            for (var i = 1; i <= 12; i++) {
                GetCOnfirmAll(i);
            }



            document.getElementById('<%=spnJan.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnfeb.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnmar.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnmay.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnapr.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnJun.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnJul.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnaug.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnsep.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnoct.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spnnov.ClientID %>').innerHTML = slctAddVisaSpnsr;
            document.getElementById('<%=spndec.ClientID %>').innerHTML = slctAddVisaSpnsr;

        }

        function ShowPopup() {
            $("#View_Profile").click();
        }

        function LeaveDateChange(testid) {
            if (((document.getElementById('fromdateLeave').value == null) || (document.getElementById('fromdateLeave').value == "")) || ((document.getElementById('todateLeave').value == null) || (document.getElementById('todateLeave').value == ""))) {
                document.getElementById('totalDaysL').innerHTML = "0";
                document.getElementById('ActualDaysL').innerHTML = "0";
            }
            else {
                if (testid == 0) {
                    document.getElementById('todateLeave').value = document.getElementById('fromdateLeave').value;
                }

                if ((document.getElementById('todateLeave').value == null) || (document.getElementById('todateLeave').value == "")) {
                    // document.getElementById('totalDaysL').innerHTML = 1;
                    // document.getElementById('ActualDaysL').innerHTML = 1;
                    $('#totalDaysL').text("1");
                    $('#ActualDaysL').text("1");

                }
                else {
                    var initial = (document.getElementById('fromdateLeave').value).split(/\/ /);
                    var final = (document.getElementById('todateLeave').value).split(/\/ /);
                    var Month, Month_F;

                    if (initial[1] == "Jan") {
                        Month = "1";
                    } else if (initial[1] == "Feb") {
                        Month = "2";
                    }
                    else if (initial[1] == "Mar") {
                        Month = "3";
                    }
                    else if (initial[1] == "Apr") {
                        Month = "4";
                    }
                    else if (initial[1] == "May") {
                        Month = "5";
                    } else if (initial[1] == "Jun") {
                        Month = "6";
                    }
                    else if (initial[1] == "Jul") {
                        Month = "7";
                    }
                    else if (initial[1] == "Aug") {
                        Month = "8";
                    }
                    else if (initial[1] == "Sep") {
                        Month = "9";
                    }
                    else if (initial[1] == "Oct") {
                        Month = "10";
                    }
                    else if (initial[1] == "Nov") {
                        Month = "11";
                    }
                    else if (initial[1] == "Dec") {
                        Month = "12";
                    }

                    if (final[1] == "Jan") {
                        Month_F = "1";
                    } else if (final[1] == "Feb") {
                        Month_F = "2";
                    }
                    else if (final[1] == "Mar") {
                        Month_F = "3";
                    }
                    else if (final[1] == "Apr") {
                        Month_F = "4";
                    }
                    else if (final[1] == "May") {
                        Month_F = "5";
                    } else if (final[1] == "Jun") {
                        Month_F = "6";
                    }
                    else if (final[1] == "Jul") {
                        Month_F = "7";
                    }
                    else if (final[1] == "Aug") {
                        Month_F = "8";
                    }
                    else if (final[1] == "Sep") {
                        Month_F = "9";
                    }
                    else if (final[1] == "Oct") {
                        Month_F = "10";
                    }
                    else if (final[1] == "Nov") {
                        Month_F = "11";
                    }
                    else if (final[1] == "Dec") {
                        Month_F = "12";
                    }

                    //var dateadd = (Month + '/' + initial[0]) + '/' + initial[2];
                    //var dateend = (Month_F + '/' + final[0]) + '/' + final[2];

                    var dateadd = (initial[2], Month - 1, initial[0]);
                    var dateend = (final[2], Month_F - 1, final[0]);

                    const date1 = new Date(dateadd);
                    const date2 = new Date(dateend);
                    //alert(date1); alert(date2);
                    if (date2 < date1) {
                        $('#LeaveError').text("End date is lesser than start date. Please select another date");
                        $('#LeaveError').attr("class", "alert alert-info");
                        $('#totalDaysL').text("");
                        $('#ActualDaysL').text("");
                    } else {
                        const diffTime = Math.abs(date2.getTime() - date1.getTime());
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                        //alert(diffDays);
                        $('#LeaveError').text("");
                        $('#LeaveError').attr("class", "");
                        $('#totalDaysL').text((diffDays + 1));
                        CallActualDays(document.getElementById('fromdateLeave').value, document.getElementById('todateLeave').value, $('#slctLeaveAppltype').val());
                    }
                }
            }
        }

        function CallActualDays(startdate, enddate, leavetype) {

            $.ajax({
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetActualDays") %>',
                data: '{StartDate: "' + startdate + '", EndDate: "' + enddate + '", Selectedval: "' + leavetype + '"}',
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var days = response.d.split('|');
                    $('#ActualDaysL').text(parseInt(days));
                },
                error: function (response) {
                    ShowModalALert(response.responseText);
                },
                failure: function (response) {
                    ShowModalALert(response.responseText);
                }
            });
        }


       function SavePnODocuments(id) {
           $('#btnSavePnO').prop("disabled", true);
            var slct;
            var e = document.getElementById('slctPnOtype');
            if (e.options[e.selectedIndex].value == "0") {
                slct = "0"
            }
            else {
                slct = e.options[e.selectedIndex].value;
            }
            var f = document.getElementById('slctPnOtype');
            var docnumber = f.options[f.selectedIndex].value;
            var PnOAddTo = document.getElementById('PnOAddTo').value;
            var empNotes = document.getElementById('PnOEmpNotes').value;
            var AddressNotes = document.getElementById('PnOAddressNotes').value;

            var DocDAXID = document.getElementById('hdnRecordDax').value;

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/savePnODocReq") %>',
                data: '{ ReqID: "' + id + '", DocNumber: "' + docnumber + '", AddressTo: "' + PnOAddTo + '",  empNotes: "' + empNotes + '", AddressNotes: "' + AddressNotes + '", usrName: "' + '<%= Session("sUsr_name")%>' + '", bsuid: "' + '<%= Session("sbsuid")%>' + '",docDAXID: "' + DocDAXID + '" }',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    $('#btnSavePnO').prop("disabled", false);
                    var a = results.d.split('||');
                    if (a[0] == '0') {
                        $('#ErrorPnOAdd').text(a[1]);
                        $('#ErrorPnOAdd').attr("class", "alert alert-info");
                        if (id == 0) {
                            $('#btnSavePnO').css("display", "none");
                        }
                        $('#h_Action').val('1');
                    } else {
                        $('#ErrorPnOAdd').text(a[1]);
                        $('#ErrorPnOAdd').attr("class", "alert alert-info");
                        $('#h_Action').val('1');
                    }
                }, error: function (results) {
                    $('#btnSavePnO').prop("disabled", false);
                }, failure: function (results) {
                    $('#btnSavePnO').prop("disabled", false);
                }
            });
        }

        function SaveLeave(id) {
            var fromdate, todate, slct;
            var e = document.getElementById('slctLeaveAppltype');

            if (e.options[e.selectedIndex].value == "0") {
                slct = "0";
            } else {
                slct = e.options[e.selectedIndex].value;
            }

            if (document.getElementById('fromdateLeave').value == null || document.getElementById('fromdateLeave').value == "") {
                fromdate = "0";
            }
            else {
                fromdate = document.getElementById('fromdateLeave').value;
            }

            if (document.getElementById('todateLeave').value == null || document.getElementById('todateLeave').value == "") {
                todate = "0";
            }
            else {
                todate = document.getElementById('todateLeave').value;
            }

            var remarks = document.getElementById('leaveDetail').value;
            var handover = document.getElementById('workhandover').value;
            var mobile = document.getElementById('LeavePhone').value;
            var address = document.getElementById('addressLeave').value;

            var svObj = {};
            svObj.ELAID = id;
            svObj.bsuid = '<%= Session("sbsuid")%>';
            svObj.fromdate = fromdate;
            svObj.todate = todate;
            svObj.leavetype = slct;
            svObj.usrname = '<%= Session("sUsr_name")%>';
            svObj.remarks = remarks;
            svObj.address = address;
            svObj.mobile = mobile;
            svObj.handover = handover;
            svObj.filepath = $('#file_path').val();
            svObj.fileext = $('#file_ext').val();
            svObj.filename = $('#h_file_path').val();

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/SaveLeaveA") %>',
                //data: '{ ELAID: "' + id + '", bsuid: "' + '<%= Session("sbsuid")%>' + '", fromdate: "' + fromdate + '", todate: "' + todate + '",  leavetype: "' + slct + '", usrname: "' + '<%= Session("sUsr_name")%>' + '", remarks: "' + remarks + '", address: "' + address + '", mobile: "' + mobile + '",  handover: "' + handover + '", filepath: "' + $('#file_path').val() + '", fileext: "' + $('#file_ext').val() + '", filename: "' + $('#h_file_path').val() + '"}',
                data: JSON.stringify(svObj),
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] == '0') {
                        $('#LeaveError').text(a[2]);
                        $('#LeaveError').attr("class", "alert alert-info");
                        if (id == 0) {
                            $('#btnSaveLeave').css("display", "none");
                        }
                        $('#h_Action').val('1');
                    } else {
                        //  alert(a[2]);
                        $('#LeaveError').text(a[2]);
                        $('#LeaveError').attr("class", "alert alert-info");
                        $('#h_Action').val('1');
                    }
                }
            });
        }


        function LeaveTypeSelectionChange() {

            var fromdate, todate, slct;
            var e = document.getElementById('slctLeaveAppltype');
            var loadvalue = '<%= Session("hidLoadValue")%>';
            try {
                if (e.options[e.selectedIndex].value == null) {
                    if (loadvalue == "") {
                        slct = "";

                    } else {
                        slct = loadvalue;
                    }

                } else {
                    slct = e.options[e.selectedIndex].value;
                }
            } catch (e) {
                slct = "";
            }


            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/getLeaveD") %>',
                data: '{ bsuid: "' + '<%= Session("sbsuid")%>' + '", SelectVal: "' + slct + '", usrname: "' + '<%= Session("sUsr_name")%>' + '" }',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if ((a[0]) == 'AL') {
                        $('#rowLeaveCount1').css("display", "inline");
                        $('#rowLeaveCount2').css("display", "inline");
                        $('#rowLeaveCount3').css("display", "inline");
                        $('#leaveBal').text(a[3]);
                        $('#rowLeaveML').css("display", "none");
                    }
                    else if (a[0] == 'ML') {
                        //$('#rowLeaveCount1').css("display", "none");
                        //  $('#rowLeaveCount2').css("display", "none");
                        $('#rowLeaveCount3').css("display", "none");
                        $('#rowLeaveML').css("display", "block");
                        $('#leaveBal').text("");
                    }
                    else {
                        // $('#rowLeaveCount1').css("display", "none");
                        // $('#rowLeaveCount2').css("display", "none");
                        $('#rowLeaveCount3').css("display", "none");
                        $('#leaveBal').text("");
                        $('#rowLeaveML').css("display", "none");
                    }
                }
            });

            CallActualDays(document.getElementById('fromdateLeave').value, document.getElementById('todateLeave').value, $('#slctLeaveAppltype').val());
        }

        function DeleteDependantDetails(id) {
            if (id != 0) {
                $.ajax({
                    type: "POST",
                    url: "/PHOENIXBETA/GemsEss/Index.aspx/DeleteDependantDetails",
                    data: '{EDDID: "' + id + '" }',
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                        var a = results.d.split('||');
                        if (a[0] == '0') {
                            $('#ErrorEditFamily').text(a[1]);
                            $('#ErrorEditFamily').attr("class", "alert alert-info");
                            $('#ErrorEditFamily0').text(a[1]);
                            $('#ErrorEditFamily0').attr("class", "alert alert-info");
                            //$('#divalert').attr("class", "alert alert-success");
                            //$('#divalert').css("display", "block");
                            $('#btnProfSave').css("display", "none");
                            $('#btnProfDelete').css("display", "none");
                            $('#h_Action').val('1');
                        }
                        else {
                            $('#ErrorEditFamily').text(a[1]);
                            $('#ErrorEditFamily').attr("class", "alert alert-info");
                            $('#ErrorEditFamily0').text(a[1]);
                            $('#ErrorEditFamily0').attr("class", "alert alert-info");
                            //$('#divalert').attr("class", "alert alert-danger");
                            //$('#divalert').css("display", "block");
                            $('#h_Action').val('1');
                        }
                    }
                });
            }
        }

        function SaveDependantDetails(id) {

            if (id == 0) {
                var Type = $("#AddFamilyForm input[type='radio']:checked").val();
                var Name = document.getElementById('txtAddDepedentName').value;
                var e = document.getElementById('slctAddRltion');
                var Relation = e.options[e.selectedIndex].value;
                var DOB = document.getElementById('txtAddDob').value;
                var r = document.getElementById('slctAddGender');
                var Gen = r.options[r.selectedIndex].value;
                var p = document.getElementById('slctAddMar');
                var Mar = p.options[p.selectedIndex].value;
                var Nationlty = document.getElementById('txtAddNAt').value;
                var PassportNO = document.getElementById('txtAddPasprtNo').value;
                var txtAddEID = document.getElementById('txtAddEID').value;
                var txtEIDExpDt = document.getElementById('txtEIDExpDt').value;
                var txtAddUIDNo = document.getElementById('txtAddUIDNo').value;
                var txtAddVisaIssueDt = document.getElementById('txtAddVisaIssueDt').value;
                var txtAddVisaExpDt = document.getElementById('txtAddVisaExpDt').value;
                var txtAddVisaIssuePlc = document.getElementById('txtAddVisaIssuePlc').value;
                var m = document.getElementById('slctAddVisaSpnsr');
                var slctAddVisaSpnsr = m.options[m.selectedIndex].value;
                var txtAddInsCrdNo = document.getElementById('txtAddInsCrdNo').value;
                var StuID = $('#h_stu_ID').val();//document.getElementById('txtAddGEMSStaffIdStudentId').value;
                var DependMStatus = p.options[p.selectedIndex].text;
                var bu = document.getElementById('<%=ddlAddUnit.ClientID%>');
                //var Unit = document.getElementById('txtAddUnit').value;
                if (bu.selectedIndex != -1) {
                    var Unit = bu.options[bu.selectedIndex].text;
                    var bsuid = bu.options[bu.selectedIndex].value;
                } else {
                    var Unit = "";
                    var bsuid = "";
                }
                var employeeID = $('#h_emp_id').val();
                //var employeeID = document.getElementById('txtAddGEMSStaffIdStudentId').value;
                var city = document.getElementById('<%=ddlAddNationality.ClientID%>');
                var contryID = city.options[city.selectedIndex].value;//$('#h_cont_id').val();
                //var contryID = $('#h_cont_id').val();
                var visafilenumber = document.getElementById('txtAddVisaFileNumber').value;
                var rc = document.getElementById('<%=ddlAddResidenceCountry.ClientID%>');
                var ResindenceCountryId = rc.options[rc.selectedIndex].value;
                var GemsStaffIdorStuId = document.getElementById('txtAddGEMSStaffIdStudentId').value;
            }
            else {
                var Type = $("#EditFamilyForm input[type='radio']:checked").val();
                var Name = document.getElementById('txtDDDepedentName').value;
                var e = document.getElementById('slctDDRel');
                var Relation = e.options[e.selectedIndex].value;
                var DOB = document.getElementById('txtDDDOB').value;
                var r = document.getElementById('slctDDGender');
                var Gen = r.options[r.selectedIndex].value;
                var p = document.getElementById('slctDDMrtsts');
                var Mar = p.options[p.selectedIndex].value;
                var Nationlty = document.getElementById('txtDDNationality').value;
                var PassportNO = document.getElementById('txtDDPasprtNo').value;
                var txtAddEID = document.getElementById('txtDDEIDNo').value;
                var txtEIDExpDt = document.getElementById('txtDDEDIEXPDt').value;
                var txtAddUIDNo = document.getElementById('txtDDUID').value;
                var txtAddVisaIssueDt = document.getElementById('txtDDVIsaIsueDt').value;
                var txtAddVisaExpDt = document.getElementById('txtDDVIsaExpDt').value;
                var txtAddVisaIssuePlc = document.getElementById('txtDDVisaIsuePlc').value;
                var m = document.getElementById('slctDDVisSpnsr');
                var slctAddVisaSpnsr = m.options[m.selectedIndex].value;
                var txtAddInsCrdNo = document.getElementById('txtDDInsCrdNo').value;
                var StuID = $('#h_stu_ID').val();//document.getElementById('txtDDGEMSStaffIdStudentId').value//
                var DependMStatus = p.options[p.selectedIndex].text;
                //var Unit = document.getElementById('txtDDUnit').value;
                //var bsuid = $('#h_company_ID').val();
                var bu = document.getElementById('<%=ddlDDUnit.ClientID%>');
                //var Unit = document.getElementById('txtAddUnit').value;
                if (bu.selectedIndex != -1) {
                    var Unit = bu.options[bu.selectedIndex].text;
                    var bsuid = bu.options[bu.selectedIndex].value;
                } else {
                    var Unit = "";
                    var bsuid = "";
                }
                var employeeID = $('#h_emp_id').val();
                //var employeeID = document.getElementById('txtDDGEMSStaffIdStudentId').value;

                var city = document.getElementById('<%=ddlDDNationality.ClientID%>');
                var contryID = city.options[city.selectedIndex].value;//$('#h_cont_id').val();
                var visafilenumber = document.getElementById('txtvisafilenumber').value;
                var rc = document.getElementById('<%=ddlDDResidenceCountry.ClientID%>');
                var ResindenceCountryId = rc.options[rc.selectedIndex].value;
                var GemsStaffIdorStuId = document.getElementById('txtDDGEMSStaffIdStudentId').value;
            }

            var filepath = getBase64Image(id);


            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/AddSaveDependantDetails") %>',
                data: '{EDDID: "' + id + '", bsuid: "' + bsuid + '", usrName: "' + '<%= Session("sUsr_name")%>' + '", Name: "' + Name + '", DOBDate: "' + DOB + '", Relation: "' + Relation + '", StuID: "' + StuID + '", PassportNo: "' + PassportNO + '", UIDNo: "' + txtAddUIDNo + '", VisaIssueDt: "' + txtAddVisaIssueDt + '", VisaExpDate: "' + txtAddVisaExpDt + '", VisaIssuePlc: "' + txtAddVisaIssuePlc + '", EIDExpryDt: "' + txtEIDExpDt + '", Gender: "' + Gen + '", MarStatus: "' + Mar + '", DependMStatus: "' + DependMStatus + '", CityID: "' + contryID + '", Nationality: "' + Nationlty + '", Sponser: "' + slctAddVisaSpnsr + '", InsuranceNumber: "' + txtAddInsCrdNo + '", EIDNo: "' + txtAddEID + '", GemsType: "' + Type + '", Unit: "' + Unit + '", EmployeeID: "' + employeeID + '", Filepath: "' + filepath + '", Visafilenumber: "' + visafilenumber + '", ResindenceCountryId: "' + ResindenceCountryId + '", GemsStaffIdorStuId: "' + GemsStaffIdorStuId + '" }',
                //data: '{ EDDID: "' + '0' + '", bsuid: "' + '0' + '", usrName: "' + '0' + '", Name: "' + '0' + '", DOBDate: "' + '0' + '", Relation: "' + '0' + '", StuID: "' + '0' + '", PassportNo: "' + '0' + '", UIDNo: "' + '0' + '", VisaIssueDt: "' + '0' + '", VisaExpDate: "' + '0' + '", VisaIssuePlc: "' + '0' + '", EIDExpryDt: "' + '0' + '", Gender: "' + '0' + '", MarStatus: "' + '0' + '", DependMStatus: "' + '0' + '", CityID: "' + '0' + '", Nationality: "' + '0' + '", Sponser: "' + '0' + '", InsuranceNumber: "' + '0' + '", EIDNo: "' + '0' + '" }', //, GemsType: "' + Type + ', Unit: "' + Unit + ', EmployeeID: "' + employeeID + '"                
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var a = results.d.split('||');
                    if (a[0] == '0') {
                        if (id == 0) {
                            $('#ADDError').text(a[1]);
                            $('#ADDError').attr("class", "alert alert-info");
                            $('#ADDError0').text(a[1]);
                            $('#ADDError0').attr("class", "alert alert-info");
                            //$('#ADDError').attr("class", "alert alert-success");
                            //$('#divalert2').attr("class", "alert alert-success");
                            //$('#divalert2').css("display", "block");
                        } else {
                            //$('#ADDError').attr("class", "alert alert-error");
                            $('#ErrorEditFamily').text(a[1]);
                            $('#ErrorEditFamily').attr("class", "alert alert-info");
                            $('#ErrorEditFamily0').text(a[1]);
                            $('#ErrorEditFamily0').attr("class", "alert alert-info");
                            //$('#divalert').attr("class", "alert alert-success");
                            //$('#divalert').css("display", "block");
                        }
                        if (id == 0) {
                            $('#btnProfSave').css("display", "none");
                            $('#btnAddSaveDep').css("display", "none");
                            $('#btnProfDelete').css("display", "none");
                        }
                        $('#h_Action').val('1');
                    } else {
                        if (id == 0) {
                            $('#ADDError').text(a[1]);
                            $('#ADDError').attr("class", "alert alert-danger");
                            $('#ADDError0').text(a[1]);
                            $('#ADDError0').attr("class", "alert alert-danger");
                            //$('#ADDError').attr("class", "alert alert-error");
                            //$('#divalert2').attr("class", "alert alert-danger");
                            //$('#divalert2').css("display", "block");
                        } else {
                            $('#ErrorEditFamily').text(a[1]);
                            $('#ErrorEditFamily').attr("class", "alert alert-danger");
                            $('#ErrorEditFamily0').text(a[1]);
                            $('#ErrorEditFamily0').attr("class", "alert alert-danger");
                            //$('#divalert').attr("class", "alert alert-danger");
                            //$('#divalert').css("display", "block");
                        }
                        $('#h_Action').val('1');
                    }
                }
            });
        }

        $(function () {
            $('#uploadDDPhoto').change(function (event) {


                var filename = event.target.files[0].name;
                var file_ext = filename.substr(filename.lastIndexOf('.'), filename.length);
                if (file_ext.toLowerCase() == '.png' || file_ext.toLowerCase() == '.jpg' || file_ext.toLowerCase() == '.jpeg') {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#imgDDProfile").attr('src', tmppath);
                } else {
                    ShowModalALert("Image file type is not proper");
                }
            });

            $('#uploadAddPhoto').change(function (event) {
                var filename = event.target.files[0].name;
                var file_ext = filename.substr(filename.lastIndexOf('.'), filename.length);
                if (file_ext.toLowerCase() == '.png' || file_ext.toLowerCase() == '.jpg' || file_ext.toLowerCase() == '.jpeg') {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#imgAddProfile").attr('src', tmppath);
                }
                else {
                    ShowModalALert("Image file type is not proper");
                }

            });

            $('#<%= ddlEmirate.ClientID %>').change(function (event) {

                $.ajax({
                    url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GetAreas") %>',
                    data: "{ 'SelectedEmirateID': '" + $('#<%= ddlEmirate.ClientID %>').val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var arraylength = response.d.length;
                        $('#<%= ddlArea.ClientID %>').empty();
                        if (arraylength > 0) {
                            $('#<%= ddlArea.ClientID %>').css("background", "white");
                            $('#<%= ddlArea.ClientID %>').removeAttr("disabled");

                            //$('#dvStudents').append("<div class='container'>");
                            for (var i = 0; i < arraylength; i++) {
                                var newOption = "<option value='" + response.d[i].split("||")[1] + "'>" + response.d[i].split("||")[0] + "</option>";
                                $('#<%= ddlArea.ClientID %>').append(newOption);
                            }
                        }
                        else {
                            $('#<%= ddlArea.ClientID %>').css("background", "rgba(0,0,0,0.03)");
                            $('#<%= ddlArea.ClientID %>').attr("disabled", "disabled");
                        }
                    },
                    error: function (response) {
                        ShowModalALert(response.responseText);
                    },
                    failure: function (response) {
                        ShowModalALert(response.responseText);
                    }
                });
            });
        });

        function LeaveHistoryPopup(id) {
            var url = 'empLeaveApplicationListPopup_ESS.aspx?id=' + id;
            //   $('#View_History').modal('show').find('.modal-content').load(url);  -- for static pages this is an option
            PopupIndex(url);
        }
        function PopupIndex(url) {
            $.fancybox({
                'width': '100%',
                'height': '100%',
                'autoScale': true,
                'fitToView': true,
                'autoSize': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };
        function FeeConcessionPopup(id) {
            //var url = 'FeeConcessionPopup_ESS.aspx?id=' + id;
            //   $('#View_History').modal('show').find('.modal-content').load(url);  -- for static pages this is an option
            //PopupConcession(url);
            window.location.href = "FeeConcessionPopup_ESS.aspx?id=" + id + "";
        }

        function showAddDependant() {
            //$("#Add_Family").toggle('modal');
            ////$("#Add_Family").removeClass('fade');
            //$("#Add_Family").addClass('show');
            //$('#Add_Family').attr("aria-modal", "true");
            //$('#btn_add_new').click();
            $('#btn_Show_ChildList').click();
        }

        function showChildList() {
            //$("#Add_Family").toggle('modal');
            ////$("#Add_Family").removeClass('fade');
            //$("#Add_Family").addClass('show');
            //$('#Add_Family').attr("aria-modal", "true");
            $('#btn_Show_ChildList').click();
        }

        function Closesa_childList() {
            document.getElementById('btnChildList').click();
            $(".modal").css("overflow-x", "hidden");
            $(".modal").css("overflow-y", "auto");
            $('.divGems').css("display", "none");
            $(".lblGemsStudId").css('display', 'none');
            $('#radADDNGems').prop("checked", false)
            $('#radADDSGems').prop("checked", false)
            $('#radADDStGems').prop("checked", false)
        }

        function hidedivGems() {
            $('.divGems').css("display", "none");
            $(".lblGemsStudId").css('display', 'none');
            $('#radADDNGems').prop("checked", false)
            $('#radADDSGems').prop("checked", false)
            $('#radADDStGems').prop("checked", false)
        }

        function PopupConcession(url) {
            $.fancybox({
                'width': '70%',
                'height': '60%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
        };

        function getBase64Image(id) {
            if (id == 0) {
                var canvas = document.createElement("canvas");
                img1 = document.getElementById('imgAddProfile');

                canvas.width = img1.width;
                canvas.height = img1.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img1, 0, 0, img1.width, img1.height);
                var dataURL = canvas.toDataURL("image/png");
                return dataURL;
            } else {
                var canvas = document.createElement("canvas");
                img1 = document.getElementById('imgDDProfile');

                canvas.width = img1.width;
                //alert(img1.width);
                canvas.height = img1.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img1, 0, 0, img1.width, img1.height);
                var dataURL = canvas.toDataURL("image/png");
                return dataURL;
            }
        }

        $('#fluplodLeaveML').change(function (event) {

            var tmppath = URL.createObjectURL(event.target.files[0]);
            convertToBase64();
            //   $('#h_file_path').val(tmppath);
        });

        function convertToBase64() {
            //Read File
            var selectedFile = document.getElementById('fluplodLeaveML').files;
            var filename = document.getElementById('fluplodLeaveML').value;

            //Check File is not Empty
            if (selectedFile.length > 0) {
                var file_ext = filename.substr(filename.lastIndexOf('.'), filename.length);
                var filename1 = filename.substr(filename.lastIndexOf('\\') + 1, filename.length);

                $('#file_ext').val(file_ext);
                $('#h_file_path').val(filename1);
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                var base64;
                var returnstring;
                // Onload of file read the file content
                fileReader.onload = function (fileLoadedEvent) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    console.log(base64);
                    $('#file_path').val(base64);
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);

            }
        }


        function MyConfirmMethod(hv) {
            Closesa_childList();
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/a") %>',
                data: '{ Id: " ' + hv + ' "}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    //useresult(results.d);           
                    var a = results.d.split('||');

                    $('#slctDDRel').val(a[0]);
                    $('#txtDDDOB').val(a[1]);
                    $('#slctDDGender').val(a[2]);
                    $('#slctDDMrtsts').val(a[3]);
                    $('#txtDDUnit').val(a[4]);
                    $('#txtDDDepedentName').val(a[5]);
                    $('#txtDDNationality').val(a[6]);
                    $('#txtDDPasprtNo').val(a[7]);
                    $('#txtDDEIDNo').val(a[8]);
                    $('#txtDDEDIEXPDt').val(a[9]);
                    $('#txtDDUID').val(a[10]);
                    $('#txtDDVIsaIsueDt').val(a[11]);
                    $('#txtDDVIsaExpDt').val(a[12]);
                    $('#txtDDVisaIsuePlc').val(a[13]);
                    $('#slctDDVisSpnsr').val(a[14]);
                    $('#txtDDInsCrdNo').val(a[15]);
                    $('#imgDDProfile').attr("src", "../Payroll/ImageHandler.ashx?ID=" + hv + "&TYPE=EDD");
                    $('#btnProfSave').attr('onclick', 'SaveDependantDetails(' + hv + ')');
                    $('#btnProfDelete').attr('onclick', 'ConfirmDelete(' + hv + ')');
                    $('#h_cont_id').val(a[17]);
                    $('#h_stu_ID').val(a[18]);
                    $('#h_company_ID').val(a[19]);
                    $('#h_emp_id').val(a[20]);
                    $('#txtvisafilenumber').val(a[21]);

                    if (a[16] == 'N') {
                        $('#radDDNGems').prop("checked", true)
                        $('#radDDSGems').prop("checked", false)
                        $('#txtDDUnit').attr("disabled", "disabled");
                        $('#radDDStGems').prop("checked", false)
                        $("#txtDDDepedentName").removeAttr("disabled");
                        $('.divGems').css("display", "none");
                        $(".lblGemsStudId").css('display', 'none');
                    } else if (a[16] == 'E') {
                        $('#radDDSGems').prop("checked", true)
                        $('#radDDNGems').prop("checked", false)
                        $('#txtDDUnit').removeAttr("disabled");
                        $('#radDDStGems').prop("checked", false)
                        $('.divGems').css("display", "block");
                        $(".lblDDGemsStaffStudent").text("Staff Id");
                        $(".lblGemsStudId").css('display', 'none');
                        GetBusinessUnits("E")
                    } else if (a[16] == 'S') {
                        $('#radDDStGems').prop("checked", true)
                        $('#radDDNGems').prop("checked", false)
                        $('#txtDDUnit').removeAttr("disabled");
                        $('#radDDSGems').prop("checked", false)
                        $('.divGems').css("display", "block");
                        $(".lblDDGemsStaffStudent").text("Student Id");
                        $(".lblGemsStudId").css('display', 'block');
                        GetBusinessUnits("S")
                    }
                    $('#txtDDGEMSStaffIdStudentId').val(a[23]);
                    //$("#ddlDDResidenceCountry").val(a[22])
                    document.getElementById('<%=ddlDDResidenceCountry.ClientID%>').value = parseInt(a[22])
                    document.getElementById('<%=ddlDDNationality.ClientID%>').value = parseInt(a[17])
                    setTimeout(function () {
                        document.getElementById('<%=ddlDDUnit.ClientID%>').value = parseInt(a[19])
                    }, 1000);

                    $('#txtDDDepedentName').val(a[5]);

                    if (parseInt(a[22]) == 172) {
                        $('.divUAE').css("display", "block");
                        //changeEvent(0);
                    } else $('.divUAE').css("display", "none");
                    //if (a[16] == 'E' || a[16] == 'S') {
                    //    getDependentName(0);
                    //}
                    //changeEvent(0);

                    //$("#ddlDDUnit").val(a[19])
                    <%--$('#<%= txtEventName.ClientID%>').val(a[0]);
                    $('#<%= txtEventDesc.ClientID%>').val(a[1]);
                    $('#<%= txtAmount.ClientID%>').val(a[3]);
                    if (String(a[4]) == "Yes") {
                        $('#<%=radioButtonList.ClientID%>').find("input[value='Yes']").prop("checked", true);
                    }
                    else {
                        $('#<%=radioButtonList.ClientID%>').find("input[value='No']").prop("checked", true);
                    }
                    $("#<%= ddlActivtyTypes.ClientID%>").val('MUSIC').attr("selected", "selected");--%>
                }
            });
            return true;
        }

        $('#<%=payJan.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(1);
        });
        $('#<%=payFeb.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(2);
        });
        $('#<%=payMar.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(3);
        });
        $('#<%=payApr.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(4);
        });
        $('#<%=payMay.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(5);
        });
        $('#<%=payJun.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(6);
        });
        $('#<%=payJul.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(7);
        });
        $('#<%=payAug.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(8);
        });
        $('#<%=paySep.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(9);
        });
        $('#<%=payOct.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(10);
        });
        $('#<%=payNov.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(11);
        });
        $('#<%=payDec.ClientID %>').click(function () {
            $('#<%=hdnMonth.ClientID %>').val(12);
        });


        function GetCOnfirmAll(paymonth) {

            var ddlPayYear = document.getElementById('<%=hdnYear.ClientID %>').value;

<%--            alert(ddlPayYear);
             alert($('#<%=hdnMonth.ClientID %>').val());--%>
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/GenerateSalarySlip") %>',
                data: '{paymonth: "' + paymonth + '", ddlPayYear: "' + ddlPayYear + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {
                    var cal = results.d.split('||');
                    //if (confirm('Are you sure?')) {
                    //document.getElementById('<%=payJan.ClientID %>').setAttribute('data-target', '#Confirm_Salary');

                    //return;
                    if (paymonth == '1') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payJan.ClientID %>').setAttribute('data-target', '#No_Salary');
                        } else {

                            document.getElementById('<%=payJan.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '2') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payFeb.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payFeb.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '3') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payMar.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payMar.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '4') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payApr.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payApr.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '5') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payMay.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payMay.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '6') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payJun.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payJun.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '7') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payJul.ClientID %>').setAttribute('data-target', '#No_Salary');
                        } else {

                            document.getElementById('<%=payJul.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '8') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payAug.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payAug.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '9') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=paySep.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=paySep.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '10') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payOct.ClientID %>').setAttribute('data-target', '#No_Salary');
                        } else {
                            document.getElementById('<%=payOct.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '11') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payNov.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payNov.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                    if (paymonth == '12') {
                        if (cal[0] != 'True') {
                            document.getElementById('<%=payDec.ClientID %>').setAttribute('data-target', '#No_Salary');
                        }
                        else {
                            document.getElementById('<%=payDec.ClientID %>').setAttribute('data-target', '#Confirm_Salary');
                        }
                    }
                }
            });

        }
        function returnfalse() { return false; }
        function Closesa() {


            if (document.getElementById('h_Action').value == '1') {
                // window.location.href = "index.aspx";
                ShowGif();
                document.getElementById('<%=btnJustClick.ClientID %>').click();
            }
            // SetPieChart();
        }
        function ClearLeaveControls() {
            $('#slctLeaveAppltype').val('AL');
            $('#fromdateLeave').val('');
            $('#todateLeave').val('');
            $('#rowLeaveCount1').text = '';
            $('#rowLeaveCount2').text = '';
            $('#rowLeaveCount3').text = '';
            $('#rowLeaveML').css('display', 'none');
            $('#rowLeaveMLShowFile').css('display', 'none');
            $('#leaveDetail').val('');
            $('#workhandover').val('');
            $('#LeavePhone').val('');
            $('#addressLeave').val('');
            $('#btnSaveLeave').attr('onclick', 'SaveLeave(0)');
            $('#btnSaveLeave').val("Save");
            $('#totalDaysL').text("0");
            $('#ActualDaysL').text("0");

        }

        function revertcontrols() {

            document.getElementById('<%=txtEmpName.ClientID %>').style.display = "block";
            document.getElementById('<%=txtEmpNameEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtEmpNo.ClientID %>').style.display = "block";
            document.getElementById('<%=txtEmpNoEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtEmrId.ClientID %>').style.display = "block";
            document.getElementById('<%=txtEmrIdEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtEidExpDt.ClientID %>').style.display = "block";
            document.getElementById('<%=txtEidExpDtEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtPassportNo.ClientID %>').style.display = "block";
            document.getElementById('<%=txtPassportNoEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtPassprtIsuePlc.ClientID%>').style.display = "block";
            document.getElementById('<%=txtPassprtIsuePlcEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtPasprtIsueDt.ClientID%>').style.display = "block";
            document.getElementById('<%=txtPasprtIsueDtEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtPasprtexpdt.ClientID%>').style.display = "block";
            document.getElementById('<%=txtPasprtexpdtEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtAlEmail.ClientID%>').style.display = "block";
            document.getElementById('<%=txtAlEmailEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtUIDNo.ClientID%>').style.display = "block";
            document.getElementById('<%=txtUIDNoEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtVisaIsuePlc.ClientID%>').style.display = "block";
            document.getElementById('<%=txtVisaIsuePlcEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtVisaIsueDt.ClientID%>').style.display = "block";
            document.getElementById('<%=txtVisaIsueDtEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=txtVisaExpDt.ClientID%>').style.display = "block";
            document.getElementById('<%=txtVisaExpDtEdit.ClientID %>').style.display = "none";

            document.getElementById('<%=ddlsponser.ClientID%>').style.display = "block";
            document.getElementById('<%=ddlsponserEdit.ClientID%>').style.display = "none";

            document.getElementById('<%=txtEmpInsuNo.ClientID%>').style.display = "block";
            document.getElementById('<%=txtEmpInsuNoEdit.ClientID%>').style.display = "none";

            document.getElementById('<%=ddlEmirate1.ClientID%>').style.display = "block";
            document.getElementById('<%=ddlEmirate.ClientID%>').style.display = "none";

            document.getElementById('<%=ddlArea1.ClientID%>').style.display = "block";
            document.getElementById('<%=ddlArea.ClientID%>').style.display = "none";

            document.getElementById('<%=txtPhnNum.ClientID%>').style.display = "block";
            document.getElementById('<%=txtPhnNumEdit.ClientID%>').style.display = "none";

            document.getElementById('<%=spnEmrCnt1.ClientID%>').style.display = "block";
            document.getElementById('<%=txtEmrCnt1.ClientID%>').style.display = "none";

            document.getElementById('<%=spnEmrCnt2.ClientID%>').style.display = "block";
            document.getElementById('<%=txtEmrCnt2.ClientID%>').style.display = "none";

            document.getElementById('<%=spnPhnEmerCont1.ClientID%>').style.display = "block";
            document.getElementById('<%=txtPhnEmerCont1.ClientID%>').style.display = "none";

            document.getElementById('<%=spnPhnEmerCont2.ClientID%>').style.display = "block";
            document.getElementById('<%=txtPhnEmerCont2.ClientID%>').style.display = "none";

           <%-- document.getElementById('<%=btnProEdit.ClientID%>').className = "edit-icon";--%>
            $("#spnEditIcon").attr("class", "edit-icon mr-2");
            document.getElementById('<%=lblReqfield.ClientID%>').style.display = "none";
            <%--document.getElementById('<%=btnProEdit.ClientID%>').setAttribute("onclick", "EditProfilePage(0);");
            document.getElementById('<%=btnProEdit.ClientID%>').setAttribute("Title", "Edit Profile");--%>
            //spnEditIcon spnEdit
            $("#lnkEdit").attr("onclick", "EditProfilePage(0);");
            $("#lnkEdit").attr("Title", "Edit Profile");
            $("#spnEdit").html("Edit Profile");

        }

        function EditProfilePage(id) {
            if (id == 0) {
                document.getElementById('<%=txtEmpName.ClientID %>').style.display = "none";
                document.getElementById('<%=txtEmpNameEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtEmpNo.ClientID %>').style.display = "none";
                document.getElementById('<%=txtEmpNoEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtEmrId.ClientID %>').style.display = "none";
                document.getElementById('<%=txtEmrIdEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtEidExpDt.ClientID %>').style.display = "none";
                document.getElementById('<%=txtEidExpDtEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtPassportNo.ClientID %>').style.display = "none";
                document.getElementById('<%=txtPassportNoEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtPassprtIsuePlc.ClientID%>').style.display = "none";
                document.getElementById('<%=txtPassprtIsuePlcEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtPasprtIsueDt.ClientID%>').style.display = "none";
                document.getElementById('<%=txtPasprtIsueDtEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtPasprtexpdt.ClientID%>').style.display = "none";
                document.getElementById('<%=txtPasprtexpdtEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtAlEmail.ClientID%>').style.display = "none";
                document.getElementById('<%=txtAlEmailEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtUIDNo.ClientID%>').style.display = "none";
                document.getElementById('<%=txtUIDNoEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtVisaIsuePlc.ClientID%>').style.display = "none";
                document.getElementById('<%=txtVisaIsuePlcEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtVisaIsueDt.ClientID%>').style.display = "none";
                document.getElementById('<%=txtVisaIsueDtEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=txtVisaExpDt.ClientID%>').style.display = "none";
                document.getElementById('<%=txtVisaExpDtEdit.ClientID %>').style.display = "block";

                document.getElementById('<%=ddlsponser.ClientID%>').style.display = "none";
                document.getElementById('<%=ddlsponserEdit.ClientID%>').style.display = "block";

                document.getElementById('<%=txtEmpInsuNo.ClientID%>').style.display = "none";
                document.getElementById('<%=txtEmpInsuNoEdit.ClientID%>').style.display = "block";

                document.getElementById('<%=ddlEmirate1.ClientID%>').style.display = "none";
                document.getElementById('<%=ddlEmirate.ClientID%>').style.display = "block";

                document.getElementById('<%=ddlArea1.ClientID%>').style.display = "none";
                document.getElementById('<%=ddlArea.ClientID%>').style.display = "block";

                document.getElementById('<%=txtPhnNum.ClientID%>').style.display = "none";
                document.getElementById('<%=txtPhnNumEdit.ClientID%>').style.display = "block";

                document.getElementById('<%=spnEmrCnt1.ClientID%>').style.display = "none";
                document.getElementById('<%=txtEmrCnt1.ClientID%>').style.display = "block";

                document.getElementById('<%=spnEmrCnt2.ClientID%>').style.display = "none";
                document.getElementById('<%=txtEmrCnt2.ClientID%>').style.display = "block";

                document.getElementById('<%=spnPhnEmerCont1.ClientID%>').style.display = "none";
                document.getElementById('<%=txtPhnEmerCont1.ClientID%>').style.display = "block";

                document.getElementById('<%=spnPhnEmerCont2.ClientID%>').style.display = "none";
                document.getElementById('<%=txtPhnEmerCont2.ClientID%>').style.display = "block";

                <%--document.getElementById('<%=btnProEdit.ClientID%>').className = "edit-icon-update";--%>
                document.getElementById('<%=lblReqfield.ClientID%>').style.display = "block";
                <%--document.getElementById('<%=btnProEdit.ClientID%>').setAttribute("onclick", "EditProfilePage(1);");
                document.getElementById('<%=btnProEdit.ClientID%>').removeAttribute("data-original-title", "");
                document.getElementById('<%=btnProEdit.ClientID%>').setAttribute("data-original-title", "Update Profile");--%>

                $("#spnEditIcon").attr("class", "edit-icon-update mr-2");
                $("#lnkEdit").attr("onclick", "EditProfilePage(1);");
                $("#lnkEdit").attr("data-original-title", "Update Profile");
                $("#lnkEdit").attr("Title", "Update Profile");
                $("#spnEdit").html("Update Profile");
            }
            else {

                var Name = document.getElementById('<%=txtEmpNameEdit.ClientID %>').value;
                var empno = document.getElementById('<%=txtEmpNoEdit.ClientID %>').value;
                var txtAddEID = document.getElementById('<%=txtEmrIdEdit.ClientID %>').value;
                var txtEIDExpDt = document.getElementById('<%=txtEidExpDtEdit.ClientID %>').value;
                var txtAddUIDNo = document.getElementById('<%=txtUIDNoEdit.ClientID %>').value;
                var PassportNO = document.getElementById('<%=txtPassportNoEdit.ClientID %>').value;
                var PassportIssuePlc = document.getElementById('<%=txtPassprtIsuePlcEdit.ClientID %>').value;
                var PassportIsDt = document.getElementById('<%=txtPasprtIsueDtEdit.ClientID %>').value;
                var PassportExpDt = document.getElementById('<%=txtPasprtexpdtEdit.ClientID %>').value;
                var AlEmail = document.getElementById('<%=txtAlEmailEdit.ClientID %>').value;
                var txtAddVisaIssuePlc = document.getElementById('<%=txtVisaIsuePlcEdit.ClientID %>').value;
                var txtAddVisaIssueDt = document.getElementById('<%=txtVisaIsueDtEdit.ClientID %>').value;
                var txtAddVisaExpDt = document.getElementById('<%=txtVisaExpDtEdit.ClientID %>').value;
                var m = document.getElementById('<%=ddlsponserEdit.ClientID%>');
                var slctAddVisaSpnsr = m.options[m.selectedIndex].value;
                var txtAddInsCrdNo = document.getElementById('<%=txtEmpInsuNoEdit.ClientID%>').value;
                var r = document.getElementById('<%=ddlEmirate.ClientID%>');
                var Emirate = r.options[r.selectedIndex].value;
                var p = document.getElementById('<%=ddlArea.ClientID%>');
                try {
                    var Area = p.options[p.selectedIndex].value;
                } catch (e) { Area = "0" }
                var Phone = document.getElementById('<%=txtPhnNumEdit.ClientID%>').value;
                var emergencycontact1 = document.getElementById('<%=txtEmrCnt1.ClientID%>').value;
                var emergencyphone1 = document.getElementById('<%=txtPhnEmerCont1.ClientID%>').value;
                var emergencycontact2 = document.getElementById('<%=txtEmrCnt2.ClientID%>').value;
                var emergencyphone2 = document.getElementById('<%=txtPhnEmerCont2.ClientID%>').value;
                var EmpVisaStatus = document.getElementById('<%=h_EMP_VISASTATUS.ClientID%>').value;
                var EmpCTYId=document.getElementById('<%=h_EMP_CTYId.ClientID%>').value;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/GemsEss/Index.aspx/UpdateProfileDetails") %>',
                    data: '{bsuid: "' + '<%= Session("sbsuid")%>' + '", usrName: "' + '<%= Session("sUsr_name")%>' + '", Name: "' + Name + '", empno: "' + empno + '", txtAddEID: "' + txtAddEID + '", txtEIDExpDt: "' + txtEIDExpDt + '", txtAddUIDNo: "' + txtAddUIDNo + '", PassportNO: "' + PassportNO + '", PassportIssuePlc: "' + PassportIssuePlc + '", PassportIsDt: "' + PassportIsDt + '", PassportExpDt: "' + PassportExpDt + '", AlEmail: "' + AlEmail + '", txtAddVisaIssuePlc: "' + txtAddVisaIssuePlc + '", txtAddVisaIssueDt: "' + txtAddVisaIssueDt + '", txtAddVisaExpDt: "' + txtAddVisaExpDt + '", slctAddVisaSpnsr: "' + slctAddVisaSpnsr + '", txtAddInsCrdNo: "' + txtAddInsCrdNo + '", Emirate: "' + Emirate + '", Area: "' + Area + '", Phone: "' + Phone + '", EmergencyContactLocal: "' + emergencycontact1 + '", EmergencyContactHome: "' + emergencycontact2 + '", EmergencyPhoneLocal: "' + emergencyphone1 + '", EmergencyPhoneHome: "' + emergencyphone2 + '", EmpVisaStatus: "' + EmpVisaStatus + '", EmpCTYId: "' + EmpCTYId + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                        var a = results.d.split('||');
                        if (a[0] == '0') {
                            $('#ErrorEditProf').text("Successfully updated record");
                            $('#ErrorEditProf').attr("class", "alert alert-info");
                            $('#h_Action').val('1');
                        } else {
                            $('#ErrorEditProf').text(a[1]);
                            $('#ErrorEditProf').attr("class", "alert alert-info");
                            $('#h_Action').val('1');
                        }
                    }
                });
            }
        }



        // Tooltips Initialization
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $('#<%= ddlDDResidenceCountry.ClientID %>').change(function (event) {
            var e = document.getElementById('<%=ddlDDResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');
            if (e.options[e.selectedIndex].value == "172") {
                $(".divUAE").css("display", "block");
            } else $(".divUAE").css("display", "none");
        })
        $('#<%= ddlAddResidenceCountry.ClientID %>').change(function (event) {
            var e = document.getElementById('<%=ddlAddResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');            
            if (e.options[e.selectedIndex].value == 172) {
                $(".divUAE").css("display", "block");
            } else $(".divUAE").css("display", "none");
        })

        function changeEvent(obj) {

            <%-- if (id == 0) {
                var e = document.getElementById('<%=ddlDDResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');
                alert(e.options[e.selectedIndex].value);
                if (e.options[e.selectedIndex].value == "172") {
                    $(".divUAE").css("display", "block");
                } else $(".divUAE").css("display", "none");
            } else--%>
            // if (id == 1) {
            //var e = document.getElementById('<%=ddlAddResidenceCountry.ClientID%>');//document.getElementById('ddlAddUnit');            
            if ($(obj).val() == "172") {
                $(".divUAE").css("display", "block");
            } else {
                $(".divUAE").css("display", "none");
            }
            //}
        }

        $('#<%= ddlDDUnit.ClientID %>').change(function (event) {
            getDependentName(0)
        })
        $('#<%= ddlAddUnit.ClientID %>').change(function (event) {
            getDependentName(1)
        })

        $('.txtDDDob').change(function (event) {
            getDependentName(0)
        })
        $('.txtAddDob').change(function (event) {
            getDependentName(1)
        })

        $('.slctAddRltion').change(function () {
            var e = document.getElementById('slctAddRltion');
            var Relation = e.options[e.selectedIndex].value;
            if ((Relation == "0" || Relation == "2") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "False") {

                $('#slctAddGender').val("M");
                $('#slctAddMar').val(0);
            }
            else if ((Relation == "0" || Relation == "3") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "False") {

                $('#slctAddGender').val("F");
                $('#slctAddMar').val(0);
            } else if ((Relation == "0" || Relation == "2") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "True") {
                $('#slctAddGender').val("M");
                $('#slctAddMar').val(0);
            } else if ((Relation == "0" || Relation == "3") && document.getElementById('<%=h_EMP_SEX.ClientID%>').value == "True") {
                $('#slctAddGender').val("F");
                $('#slctAddMar').val(0);
            } else {
                $('#slctAddGender').val(0);
                $('#slctAddMar').val(-1);
            }
        })

function getDependentName(id) {
    var Type = "", unitVal = "", DOBVal = "", Idval = ""
    if (id == 0) {
        Type = $("#EditFamilyForm input[type='radio']:checked").val();
        var e = document.getElementById('<%=ddlDDUnit.ClientID%>');//document.getElementById('ddlDDUnit');
        if (e.selectedIndex != -1)
            unitVal = e.options[e.selectedIndex].value;
        DOBVal = $(".txtDDDob").val();// document.getElementById('txtDDDob').value;
        Idval = $(".txtDDGEMSStaffIdStudentId").val();//document.getElementById('txtDDGEMSStaffIdStudentId').value;
    } else if (id == 1) {
        Type = $("#AddFamilyForm input[type='radio']:checked").val();
        var e = document.getElementById('<%=ddlAddUnit.ClientID%>');//document.getElementById('ddlAddUnit');
        if (e.selectedIndex != -1)
            unitVal = e.options[e.selectedIndex].value;
        DOBVal = $(".txtAddDob").val();//document.getElementById('txtAddDob').value;
        Idval = $(".txtAddGEMSStaffIdStudentId").val();//document.getElementById('txtAddGEMSStaffIdStudentId').value;
    }
    if (unitVal != "" && DOBVal != "" && Idval != "") {
        $.ajax({
            url: "/PHOENIXBETA/GemsEss/Index.aspx/GetDependentName",
            data: "{'Type': '" + Type + "','Dob': '" + DOBVal + "','StaffIdORStuId':'" + Idval + "','BSU_ID':'" + unitVal + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (response) {

                var arraylength = response.d.length;
                if (arraylength > 0) {
                    if (id == 0) {
                        $('#txtDDDepedentName').val(response.d[0].split("||")[0]);
                        if (response.d[0].split("||")[2] == "M") {
                            $('#slctDDGender').val("M");
                        } else { $('#slctDDGender').val("F"); }
                        $('#slctDDMrtsts').val(response.d[0].split("||")[3]);
                    } else if (id == 1) {
                        $('#txtAddDepedentName').val(response.d[0].split("||")[0]);
                        if (response.d[0].split("||")[2] == "M") {
                            $('#slctAddGender').val("M");
                        } else { $('#slctAddGender').val("F"); }
                        $('#slctAddMar').val(response.d[0].split("||")[3]);
                    }
                    if (Type == "Gems Student") {
                        $('#h_stu_ID').val(response.d[0].split("||")[1]);
                    } else if (Type == "Gems Staff") {
                        $('#h_emp_id').val(response.d[0].split("||")[1]);
                    }

                } else {
                    if (id == 0) {
                        $('#txtDDDepedentName').val("");
                        $('#slctDDGender').val(0);
                        $('#slctDDMrtsts').val(-1);
                    } else if (id == 1) {
                        $('#txtAddDepedentName').val("");
                        $('#slctADDGender').val(0);
                        $('#slctAddMar').val(-1);
                    }
                    $('#h_emp_id').val(0);
                    $('#h_stu_ID').val(0);
                    if (Type == "Gems Student") {
                        alert("Please enter valid Student Id");
                    } else if (Type == "Gems Staff") {
                        alert("Please enter valid Staff Id");
                    }
                }
            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });
    }

}

function closeAlertPopup() {
    $("#divalert").css("display", "none");
}
function closeAlertPopup2() {
    $("#divalert2").css("display", "none");
}

    </script>
    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        function IsValidVisaFileNo(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 43) || (keyCode == 47) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            //document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        }
    </script>

    <div id="modalAlert" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width: 80%;">
                <div class="alert-danger modal-body" style="">
                    <div class="row">
                        <div style="width: 95%;">
                            <p id="pModalAlert" class="ml-2"></p>
                        </div>
                        <div>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>

        function ShowModalALert(msg) {
            $('#modalAlert').modal('show');
            $("#pModalAlert").html(msg);
        }

    </script>


    <script>

        $(document).ready(function () {
            //alert("saddas");
            SetPieChart();
            SetLoad();
            GetMonthSelected();
            SerCarosel();
            ChangeYear();
            $(".datetimepicker1").datetimepicker({
                format: 'DD MMM YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            SetPieChart();
            SetLoad();
            GetMonthSelected();
            SerCarosel();
            ChangeYear();
            $(".datetimepicker1").datetimepicker({
                format: 'DD MMM YYYY'
            });
        });

        function SetPieChart() {

            setTimeout(function () {
                var points = [5];
                var pointss = [5];
                var PieChartData = '<%= Session("LogtoPieChart")%>';
                //alert(PieChartData);
                var a = PieChartData.split('|');

                var i;
                var datas;
                for (i = 0; i < a.length; ++i) {

                    var c = a[i].split(':');
                    //points[i] = c[1];
                    //pointss[i] = c[0];

                    //if (i == 0) {
                    //    datas = "[{ label:'" + pointss[i] + "', value:" + points[i] + " },"                                      
                    //}
                    //else if (i > 0 && i < (a.length - 1))
                    //{
                    //    datas = datas + "{ label:'" + pointss[i] + "', value:" + points[i] + " },"
                    //}
                    //else if (i == (a.length - 1))
                    //{
                    //    datas = datas + "{ label:'" + pointss[i] + "', value:" + points[i] + " }]"
                    //}

                    if (c[0] == 'Balance') {
                        points[0] = c[1];
                        pointss[0] = c[0];// "Annual Leave Balance";  
                    }

                    else if (c[0] == 'Annual Leave') {
                        points[1] = c[1];
                        pointss[1] = c[0];
                    }
                    else if (c[0] == 'Medical Leave') {
                        points[2] = c[1];
                        pointss[2] = c[0];
                    }
                    else if (c[0] == 'Emergency Leave') {
                        points[3] = c[1];
                        pointss[3] = c[0];
                    }
                    //else if (c[0] == 'Paternity Leave' || c[0] == 'Maternity Leave') {
                    //    points[4] = c[1];
                    //    pointss[4] = c[0];
                    //}
                }
                //  alert(datas);
                var ba = points[0];
                var al = points[1];
                var ml = points[2];
                var el = points[3];
                // var pl = points[4];
                // alert(ba); alert(al); alert(ml); alert(el); //alert(pl);
                //alert(pointss[0]); alert(pointss[1]); alert(pointss[2]); alert(pointss[3]); //alert(pointss[0]);
                Morris.Donut({
                    element: 'pie-charts',
                    colors: [
                        //'#00c5fb',
                        //'#0253cc',
                        //'#80e3ff',
                        //'#81b3fe'    
                        '#6FDB59',
                        '#3FA33C',
                        '#76E072',
                        '#5FAD56'

                    ],
                    data: [
                        { label: pointss[0], value: ba },
                        { label: pointss[1], value: al },
                        { label: pointss[2], value: ml },
                        { label: pointss[3], value: el }
                        // { label: pointss[4], value: pl },
                    ],
                    resize: true,
                    redraw: true
                });
            }, 1000);
        }


        function GetMonthSelected() {

            var datemonth = new Date().getMonth();
            if (datemonth > 0) {
                datemonth = datemonth - 1;
            }
            switch (datemonth) {
                case 0:
                    $('#liJan').addClass('monthSelected');
                    break;
                case 1:
                    $('#liFeb').addClass('monthSelected');
                    break;
                case 2:
                    $('#liMar').addClass('monthSelected');
                    break;
                case 3:
                    $('#liApr').addClass('monthSelected');
                    break;
                case 4:
                    $('#liMay').addClass('monthSelected');
                    break;
                case 5:
                    $('#liJun').addClass('monthSelected');
                    break;
                case 6:
                    $('#liJul').addClass('monthSelected');
                    break;
                case 7:
                    $('#liAug').addClass('monthSelected');
                    break;
                case 8:
                    $('#liSep').addClass('monthSelected');
                    break;
                case 9:
                    $('#liOct').addClass('monthSelected');
                    break;
                case 10:
                    $('#liNov').addClass('monthSelected');
                    break;
                case 11:
                    $('#liDec').addClass('monthSelected');
                    break;
            }


        }

        function SerCarosel() {
            var carousel = $("#carousel").flipster({
                style: 'carousel',
                spacing: -0.5,
                nav: true,
                buttons: true,
                // onItemSwitch: function () { alert("callback");},                                                          
            });
            carousel.flipster('jump', $('.monthSelected'));

        }

        function SetLoad() {
            var icurYear = '<%= Session("BSU_PAYYEAR")%>'
            var setyear = Number(icurYear) + 1;
            var selectBox = document.getElementById('<%=slctYear.ClientID%>');
            var selectBox2 = document.getElementById('calndrYear');

            // loop through years
            for (var i = icurYear; i >= 2010; i--) {
                // create option element
                var option = document.createElement('option');
                // add value and text name
                option.value = i;
                option.innerHTML = i;
                // add the option element to the selectbox
                selectBox.appendChild(option);
            }
            // ChangeYear();

            for (var j = setyear; j >= 2010; j--) {
                // create option element
                var option2 = document.createElement('option');
                // add value and text name
                option2.value = j;
                option2.innerHTML = j;
                if (j == icurYear) {
                    option2.selected = true;
                }
                // add the option element to the selectbox               
                selectBox2.appendChild(option2);
            }



            Callupdatecurrentmonthandyear();
            CalnderMonthhange();

            $(document).on("click", function () {
                $("#divCompany,#divCompany1, #divStudent, #divStudent1, #divCountry, #divCountry1").hide("fast");
            });



            $('#h_Action').val('0');
            $('#h_cont_id').val(0);
            $('#h_stu_ID').val(0);
            $('#h_company_ID').val(0);
            $('#h_emp_id').val(0);

            CallLeaveTypes();


            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1) + '/' + firstDay.getDate()) + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1) + '/' + lastDay.getDate()) + '/' + lastDay.getFullYear();
            ShowCalendarEvents(firstDayWithSlashes, lastDayWithSlashes);
            GetCountry();
        }
    </script>
    <script>
        function ApprovalPopup(username, id, type) {
            //FOR LOADING THE LEAVE APPROVAL POPUP
            if (type == 'H') {
                $('#btnApprove').hide();
                $('#btnReject').hide();
                $('#modal_Approve_remarks').hide();

            }
            else {
                $('#btnApprove').show();
                $('#btnReject').show();
                $('#modal_Approve_remarks').show();
            }

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/MyTeamDetails.aspx/GetApprovalStatus")%>',
                data: '{ username: " ' + username + ' " , id:"' + id + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {

                    var a = results.d.split('||');
                    // var fields = JSON.parse(results.d);
                    //alert(fields);DOC_ID,APS_DOCTYPE,APS_STATUS,EMP_NAME,ELT_DESCR,FROM_DATE,TO_DATE,LEAVEDAYS,ELA_REMARKS,EMP_ID,ELA_ELT_ID,ACTUAL_ELA_ID,APS_ID,ORIGINAL_LEAVE,ELM_CANCELLATION_REMARKS,DOCUMENT
                    $("#APP_NAME").attr("placeholder", a[0]);
                    $('#APP_NAME').attr("disabled", "disabled");
                    $("#APP_TYPE").attr("placeholder", a[1]);
                    $('#APP_TYPE').attr("disabled", "disabled");
                    $("#APP_PERIOD").attr("placeholder", "From " + a[2] + " To " + a[3]);
                    $('#APP_PERIOD').attr("disabled", "disabled");
                    $("#APP_DAYS").attr("placeholder", a[4]);
                    $('#APP_DAYS').attr("disabled", "disabled");
                    $("#APP_DETAILS").attr("placeholder", a[5]);
                    $("#APP_DETAILS").attr("disabled", "disabled");
                    $("#hf_EMP_ID").val(a[6]);
                    $("#hf_ELT_ID").val(a[7]);
                    $("#hf_ActualELA_ID").val(a[8]);
                    $("#hf_APS_ID").val(a[9]);
                    $("#hf_FROM_DT").val(a[2]);
                    $("#hf_TO_DT").val(a[3]);
                    $('#lbl_Message').text("");

                    if (a[10] == 'LEAVE-M') {
                        $("#modal_Approve_orignal").css('display', 'block');
                        $("#APP_ORG").attr("placeholder", a[14]);
                        $("#APP_ORG").attr("disabled", "disabled");

                        if (a[11].toLowerCase() == 'request for cancellation') {
                            $("#modal_Cancel_reason").css('display', 'block');
                            $("#APP_CANCEL_REMARKS").attr("placeholder", a[15]);
                            $("#APP_CANCEL_REMARKS").attr("disabled", "disabled");
                        }
                        else {
                            $("#modal_Cancel_reason").css('display', 'none');
                        }

                    } else {
                        $("#modal_Approve_orignal").css('display', 'none');
                        // $("#modal_Cancel_reason").css('display', 'none');
                    }

                    if (a[7] == 'ML') {
                        if (a[12] == 0 || a[12] == '0') {
                            var leaveurl = ""
                        }
                        else {
                            var leaveurl = "../Common/FileHandler.ashx?" + a[13];
                            $('#showLeavefile').css('display', 'block');
                            $('#showLeavefile').attr('href', leaveurl);
                        }
                    } else {
                        var leaveurl = ""
                        $('#showLeavefile').css('display', 'none');
                    }
                }
            });
            return true;
        }

        $(document).ready(function () {

            $("#Approve_leave").on('hidden.bs.modal', function () {
                $('[id=Modal_confirm]').remove();
                $('.modal-backdrop').remove();
                $('#Approve_leave').css("z-index", "1050");
            });
        });
        function SaveApproval(type) {
            //FOR LOADING THE LEAVE APPROVAL - APPROVE OR REJECT BUTTON CLICK
            var message1 = "";
            if (type == "R") {
                message1 = "Are you sure you want to reject this leave request ?"
            } else {
                message1 = "Are you sure you want to approve this leave request ?"
            }
            var callback = function () {
                SaveApprovalConfirm(type);
            };
            confirms("", message1, "Cancel", "Confirm", callback);

            return false;

        }
        function confirms(heading, question, cancelButtonTxt, okButtonTxt, callback) {
            $('#Approve_leave').css("z-index", "1000");
            if ($('[id=Modal_confirm]').length > 0) {
                $('[id=Modal_confirm]').remove();
            }
            var hidecls = "";
            if (okButtonTxt == "") {
                hidecls = "d-none";
            }
            var confirmModal =
                $('<div class="modal hide fade col-4 ModalConfirmCls bg-white" id="Modal_confirm">' +
                    //'<div class="modal-header" style="background-color:white;">' +
                    //'<a class="close">&times;</a>' +
                    // '<h3>' + heading + '</h3>' +
                    //'</div>' +

                    '<div class="modal-body" style="background-color:white;font-size: large;">' +
                    '<p>' + question + '</p>' +
                    '</div>' +

                    '<div class="modal-footer text-left" style="background-color:white;">' +
                    '<a href="#" class="btn btn-warning" id="btnCancel">' +
                    cancelButtonTxt +
                    '</a>' +
                    '<a href="#" id="okButton" class="btn btn-primary ' + hidecls + '"  >' +
                    okButtonTxt +
                    '</a>' +
                    '</div>' +
                    '</div>');


            confirmModal.find('#okButton').click(function (event) {
                if ($('[id=Modal_confirm]').length > 0) {
                    $('[id=Modal_confirm]').remove();
                }
                $('.modal-backdrop:first').remove();
                $('#Approve_leave').css("z-index", "1050");
                callback();

            });
            confirmModal.find('#btnCancel').click(function (event) {
                if ($('[id=Modal_confirm]').length > 0) {
                    $('[id=Modal_confirm]').remove();
                }
                $('.modal-backdrop:first').remove();
                $('#Approve_leave').css("z-index", "1050");
            });
            confirmModal.modal('show');

        };
        function CloseModal() {
            $('#Approve_leave').modal('hide');
            $('.modal-backdrop').remove();
            $('[id=Modal_confirm]').remove();
        }


        function SaveApprovalConfirm(type) {

            var _bsu_id = '<%= Session("sbsuid") %>';
            var _username = '<%= Session("sUsr_name") %>';
            var _EMP_ID = $('#hf_EMP_ID').val();
            var _ELT_ID = $('#hf_ELT_ID').val();
            var _Actual_ELA_ID = $('#hf_ActualELA_ID').val();
            var _APS_ID = $('#hf_APS_ID').val();
            var _Fromdt = $('#hf_FROM_DT').val();
            var _Todt = $('#hf_TO_DT').val();
            var _Remarks = $('#txt_Remarks').val();
            var _ApprovalMode = type;



            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/MyTeamDetails.aspx/SAVE_LEAVE_APPROVAL") %>',
                data: '{ bsu_id: "' + _bsu_id + '", username : "' + _username + '", EMP_ID :"' + _EMP_ID + '", Fromdt :"' + _Fromdt + '", Todt :"' + _Todt + '", APS_ID : "' + _APS_ID + '", Actual_ELA_ID : "' + _Actual_ELA_ID + '", ELT_ID : "' + _ELT_ID + '", Remarks : "' + _Remarks + '", ApprovalMode : "' + _ApprovalMode + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (results) {

                    $('#lbl_Message').text(results.d);
                    //$('#lbl_Message').attr("class", "alert alert-info");
                    $("#divSuccessMsg").removeClass("d-none");
                    //confirms("", results.d, "Close", "", "")


                    $('#btnApprove').css('display', 'none');
                    $('#btnReject').css('display', 'none');
                    $('#h_Action').val('1');
                }
            });
            return true;
        }

         //added vikranth on Aug 25th 2020 for Covid test
        $('#fluplodCovidTest').change(function (event) {
            debugger;
            var tmppath = URL.createObjectURL(event.target.files[0]);
            convertToBase64Covid();
            //   $('#h_file_path').val(tmppath);
        });

        function convertToBase64Covid() {
            //Read File
            var selectedFile = document.getElementById('fluplodCovidTest').files;
            var filename = document.getElementById('fluplodCovidTest').value;

            //Check File is not Empty
            if (selectedFile.length > 0) {
                var file_ext = filename.substr(filename.lastIndexOf('.'), filename.length);
                var filename1 = filename.substr(filename.lastIndexOf('\\') + 1, filename.length);

                $('#file_ext').val(file_ext);
                $('#h_file_path').val(filename1);
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                var base64;
                var returnstring;
                // Onload of file read the file content
                fileReader.onload = function (fileLoadedEvent) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    console.log(base64);
                    $('#file_path').val(base64);
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);

            }
        }

         function SaveCOVIDData(id) {
            var COV_TEST_DATE, slct;
             var e = document.getElementById('slctAddCovidTestResult');

             if (document.getElementById('txtDOCTestDate').value == null || document.getElementById('txtDOCTestDate').value == "") {
                COV_TEST_DATE = "0";
                 $('#CovidError').text("Please select date of  COVID test"); 
                  $('#CovidError').attr("class", "alert alert-info");
                return;
            }
            else {
                COV_TEST_DATE = document.getElementById('txtDOCTestDate').value;
            }

            if (e.options[e.selectedIndex].value == "0") {
                slct = "0";
                $('#CovidError').text("Please select the test result"); 
                 $('#CovidError').attr("class", "alert alert-info");
                return;
            } else {
                slct = e.options[e.selectedIndex].value;
            }


             //if ($('#file_path').val() == "") {
             //    $('#CovidError').text("Please upload the file"); 
             //    $('#CovidError').attr("class", "alert alert-info");
             //    return;
             //}


            var svObj = {};
            svObj.bsuid = '<%= Session("sbsuid")%>';
            svObj.COV_TEST_DATE = COV_TEST_DATE;
            svObj.COV_RESULT = slct;
            svObj.usrname = '<%= Session("sUsr_name")%>';
            svObj.filepath = $('#file_path').val();
            svObj.fileext = $('#file_ext').val();
            svObj.filename = $('#h_file_path').val();

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/GemsEss/Index.aspx/SaveCOVIDData") %>',
                //data: '{ ELAID: "' + id + '", bsuid: "' + '<%= Session("sbsuid")%>' + '", fromdate: "' + fromdate + '", todate: "' + todate + '",  leavetype: "' + slct + '", usrname: "' + '<%= Session("sUsr_name")%>' + '", remarks: "' + remarks + '", address: "' + address + '", mobile: "' + mobile + '",  handover: "' + handover + '", filepath: "' + $('#file_path').val() + '", fileext: "' + $('#file_ext').val() + '", filename: "' + $('#h_file_path').val() + '"}',
                 data: JSON.stringify(svObj),
                 contentType: "application/json; charset=utf-8",
                 success: function (results) {
                     var a = results.d.split('||');
                     if (a[0] == '0') {
                         $('#CovidError').text(a[2]);
                         $('#CovidError').attr("class", "alert alert-info");
                         if (id == 0) {
                             $('#btnAddCovidTest').css("display", "none");
                         }
                         $('#h_Action').val('1');
                         $('#file_path').val("");
                         $('#file_ext').val("");
                         $('#h_file_path').val("");
                         window.location.href = "index.aspx"
                     } else {
                         //  alert(a[2]);
                         $('#CovidError').text(a[2]);
                         $('#CovidError').attr("class", "alert alert-info");
                         $('#h_Action').val('1');
                     }
                     
                 }
             });
        }
    </script>

</asp:Content>

