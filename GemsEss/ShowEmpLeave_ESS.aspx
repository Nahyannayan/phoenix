<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowEmpLeave_ESS.aspx.vb" Inherits="Payroll_ShowEmpLeave" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Leave Details</title>
    <%--    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />--%>

    <%--   <script type="../text/javascript" src="../cssfiles/tabber.js"></script>--%>

    <%-- <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">--%>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../GemsEss/assets/css/bootstrap.min.css">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="../GemsEss/assets/css/font-awesome.min.css">

    <!-- Lineawesome CSS -->
    <link rel="stylesheet" href="../GemsEss/assets/css/line-awesome.min.css">

    <!-- Chart CSS -->
    <link rel="stylesheet" href="../GemsEss/assets/plugins/morris/morris.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="../GemsEss/assets/css/style.css">

    <!-- Bootstrap DataTables CSS -->
    <link href="../GemsEss/assets/css/dataTables.bootstrap4.min.css" rel="stylesheet" media="all" />

    <!-- Bootstrap Datetimepicker CSS -->
    <link href="../GemsEss/assets/css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" />
    <link href="../GemsEss/assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <link rel="stylesheet" href="../GemsEss/assets/css/semantic.min.css">
    <link rel="stylesheet" href="../GemsEss/assets/css/prism.min.css">


    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <%-- <div class="card mb-3">
        <div class="card-body">
            <div class="table-responsive">--%>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-11 mt-3">
                    <div class="row">
                        <div class="col-lg-6 col-11">
                            <div class="card-box table-responsive">
                                <asp:FormView ID="fvLeave" runat="server" Width="100%" GridLines="None" CssClass="mb-0">
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card-title">
                                                    Leave Details
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Emp No</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblEmpno" runat="server" ReadOnly="True" CssClass="form-control" Text='<%# Bind("EMPNO") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Emp Name</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblEMP_NAME" runat="server" ReadOnly="True" CssClass="form-control" Text='<%# Bind("EMP_NAME") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Leave Type</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblELT_DESCR" runat="server" CssClass="form-control" ReadOnly="True" Text='<%#  Bind("ELT_DESCR") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Date From</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblELA_DTFROM" runat="server" CssClass="form-control" ReadOnly="True" Text='<%# Bind("ELA_DTFROM", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Date To</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblELA_DTTO" runat="server" CssClass="form-control" ReadOnly="True" Text='<%# Bind("ELA_DTTO", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Leave days</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblLEAVEDAYS" runat="server" CssClass="form-control" ReadOnly="True" Text='<%#  Bind("LEAVEDAYS") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Remarks</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblELA_REMARKS" runat="server" CssClass="form-control" ReadOnly="True" Text='<%#  Bind("ELA_REMARKS") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 m-auto">
                                                <span class="field-label">Work Handed to</span>
                                            </div>
                                            <div class="col-lg-6 form-group input-group">
                                                <asp:TextBox ID="lblhandover" runat="server" CssClass="form-control" ReadOnly="True" Text='<%#  Bind("ELA_HANDOVERTXT") %>'></asp:TextBox>
                                            </div>
                                        </div>


                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>

                        <div class="col-lg-6 col-11">
                            <div class="card-box table-responsive">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card-title">
                                            Leave History
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 p-0">
                                        <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" BorderColor="#f3f3f3" Width="100%"
                                            EmptyDataText="No Data Found" CssClass="table table-responsive table-striped custom-table mb-0">
                                            <Columns>
                                                <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR" HeaderStyle-Width="30%" />
                                                <asp:BoundField DataField="MonthYear" HeaderText="Month Year" ReadOnly="True" SortExpression="MonthYear" HeaderStyle-Width="30%" />
                                                <asp:BoundField DataField="Days" HeaderText="Days" ReadOnly="True" SortExpression="Days" HeaderStyle-Width="10%" />
                                            </Columns>
                                            <EmptyDataRowStyle Wrap="True" />

                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>

                <div class="col-lg-12 col-11 mt-3">
                    <div class="row">
                        <div class="col-lg-12 col-11">
                            <div class="card-box table-responsive">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card-title">
                                            Approval History
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-lg-12 p-0">    
                                <asp:GridView ID="gvApprovals" runat="server" AutoGenerateColumns="False" BorderColor="#f3f3f3" EmptyDataText="History empty"
                                    Width="100%" CssClass="table table-responsive table-striped custom-table mb-0">
                                    <EmptyDataRowStyle Wrap="True" />


                                    <Columns>
                                        <asp:BoundField DataField="APS_NAME" HeaderText="Name" ReadOnly="True" SortExpression="APS_NAME" HeaderStyle-Width="20%" />
                                        <asp:BoundField DataField="APS_STATUS" HeaderText="Status" ReadOnly="True" SortExpression="APS_STATUS" HeaderStyle-Width="10%" />
                                        <asp:BoundField DataField="APS_REMARKS" HeaderText="Remarks" SortExpression="APS_REMARKS" HeaderStyle-Width="30%" />
                                        <asp:BoundField DataField="APS_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date"
                                            HtmlEncode="False" SortExpression="APS_DATE" HeaderStyle-Width="10%">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PREV" HeaderText="Req Prev Approval" ReadOnly="True" SortExpression="PREV" HeaderStyle-Width="15%">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FINAL" HeaderText="Req Higher Approval" ReadOnly="True"
                                            SortExpression="FINAL" HeaderStyle-Width="15%">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="trLeaveAppHistoryHeader" runat="server" class="col-lg-12 col-11 mt-3">
                    <div class="row">
                        <div class="col-lg-12 col-11">
                            <div class="card-box table-responsive">
                                 <div class="row">
                                    <div class="col-lg-12">
                                <div class="card-title">
                                    Leave Application History
                                </div>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-lg-12 p-0">
                                <asp:GridView ID="gvLeaveAppHistory" runat="server" AutoGenerateColumns="False" BorderColor="#f3f3f3" EmptyDataText="History empty"
                                    Width="100%" CssClass="table table-responsive table-striped custom-table mb-0">
                                    <EmptyDataRowStyle Wrap="True" />

                                    <HeaderStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                    <Columns>
                                        <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" HeaderStyle-Width="10%" />
                                        <asp:BoundField DataField="LAH_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date"
                                            HtmlEncode="False" SortExpression="LAH_DTFROM" HeaderStyle-Width="10%">
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LAH_DTTO" HeaderText="To Date" SortExpression="LAH_DTTO"
                                            DataFormatString="{0:dd/MMM/yyyy}" HeaderStyle-Width="10%"></asp:BoundField>
                                        <asp:BoundField DataField="LAH_REMARKS" HeaderText="Remarks" SortExpression="LAH_REMARKS" HeaderStyle-Width="30%" />
                                        <asp:BoundField DataField="LAH_Action" HeaderText="Action" HeaderStyle-Width="20%" />
                                        <asp:BoundField DataField="LAH_LOGDT" HeaderText="Log Date" ReadOnly="True" SortExpression="LAH_LOGDT" HeaderStyle-Width="10%">
                                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="APS_STATUS" HeaderText="Status" ReadOnly="True" SortExpression="APS_STATUS"
                                            Visible="False" HeaderStyle-Width="10%" />
                                    </Columns>
                                </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <%-- </div></div></div>--%>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OASISConnectionString %>"
            SelectCommand="GetLeaveHistory" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="ELA_ID" QueryStringField="elaid" Type="Int32" />
                <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
