﻿<%@ Page Title="" Language="VB" MasterPageFile="~/GemsEss/GemsEss.master" AutoEventWireup="false" CodeFile="MyTeamDetails.aspx.vb" Inherits="GemsEss_MyTeamDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <input type="hidden" id="h_Action" value="0" />
        <!-- Page Content -->
        <div class="container-fluid">
            <!-- Page Title -->
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-title" style="padding: 5px;">My Team</h4>
                </div>

            </div>
            <!-- /Page Title -->
            <!-- Search Filter -->

            <div class="row">
                <div class="col-lg-9">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-0">
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="card-title mb-3">Leave Request </h3>

                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-lg-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table id="tbl_Request1" class="table table-striped custom-table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Employee</th>
                                                            <th>Leave Type</th>
                                                            <th>From</th>
                                                            <th>To</th>
                                                            <th>Days</th>

                                                            <th class="text-center">Status</th>
                                                            <%--<th class="text-right">Actions</th>--%>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       
                                                        <asp:Repeater ID="rpt_LeaveRequest" runat="server" >
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <h2 class="table-avatar">
                                                                            <a href="#" class="avatar">
                                                                                <asp:Image ID="img_emp" runat="server" ImageUrl='<%# Bind("EMD_PHOTO")%>' />
                                                                            </a>
                                                                            <a><%# Eval("EMP_NAME")%> <span><%# Eval("EMP_DESIGNATION")%></span></a>
                                                                        </h2>
                                                                    </td>
                                                                    <td><%# Eval("ELT_DESCR")%></td>
                                                                    <td><%# Eval("FROM_DATE")%></td>
                                                                    <td><%# Eval("TO_DATE")%></td>
                                                                    <td><%# Eval("LEAVEDAYS")%></td>


                                                                    <td class="text-center">
                                                                        <div class="dropdown action-label">
                                                                            <a class="btn btn-white btn-sm btn-rounded" href="#" data-toggle="modal" onclick="ApprovalPopup('<%# Eval("username")%>','<%# Eval("ID")%>','R')" data-target="#Approve_leave" aria-expanded="false" style="background-color: <%# Eval("APS_STATUS_COLOR")%>">
                                                                                <i class="fa fa-dot-circle-o text-info"></i><%# Eval("APS_STATUS")%>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <!--<a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-purple"></i> New</a>
                                                                            <a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-info"></i> Pending</a>-->

                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <%--   <td class="text-right">
                                                                        <div class="dropdown dropdown-action">
                                                                            <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <a class="dropdown-item" onclick="ApprovalPopup('<%# Eval("username")%>','<%# Eval("ID")%>')" data-toggle="modal" data-target="#Approve_leave"><i class="fa fa-binoculars m-r-5"></i>View</a>
                                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#Approve_leave"><i class="fa fa-binoculars m-r-5"></i>Approve</a>
                                                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#Approve_leave"><i class="fa fa-trash-o m-r-5"></i>Reject</a>
                                                                            </div>
                                                                        </div>
                                                                    </td>--%>
                                                                </tr>
                                                            </ItemTemplate>                                                                                                                        
                                                        </asp:Repeater>
                                                        <tr id="trLeaveHis" runat="server" visible="false">
                                                            <td colspan="6" style="text-align:center">
                                                                <asp:Label ID="lblMessage" runat="server" text=""  ></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    
                    <%--COMPARE PORTION --%>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="card-box p-0">
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-12">
                                            <!--<h3 class="card-title mb-3">compare leave</h3>-->

                                        </div>
                                    </div>
                                    <!-- Search Filter -->
                                    <div class="row filter-row">
                                        <div class="col-lg-2 m-auto mobile-mb-3">
                                            <h2 class="card-title">Compare leave</h2>
                                        </div>
                                        <div class="col-lg-3">

                                            <div class="form-group ">
                                                <label>From <span class="text-danger">*</span></label>
                                                <div class="cal-icon-pick">
                                                    <i class="fa fa-calendar textbox-cal-icon"></i>
                                                    <input id="Compare_Fromdt" class="form-control floating datetimepicker1" placeholder="From" type="text" onchange="checkleaves();" onblur="checkleaves();">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group ">
                                                <label>To <span class="text-danger">*</span></label>
                                                <div class="cal-icon-pick">
                                                        <i class="fa fa-calendar textbox-cal-icon"></i>
                                                    <input id="Compare_Todt" class="form-control floating datetimepicker1" placeholder="To" type="text" onchange="checkleaves();" onblur="checkleaves();">
                                                </div>
                                            </div>
                                        </div> 
                                         <div class="col-lg-2">
                                            <div class="form-group mb-5">
                                                <label>Show All </label>
                                                <div class="cal-icon-pick">
                                                    <label class="check-container">
                                                      <input type="checkbox" id="chkShowAll"  title="Show All" /> 
                                                    <span class="checkmark"></span>
                                                        </label>                                                     
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="col-lg-2 col-md-2 m-auto mobile-mb-3">
                                            <a onclick="CompareLeave()" class="continue-btn btn-primary btn-sm">Compare</a>
                                        </div>
                                    </div>
                                    <!-- /Search Filter -->

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table id="tbl_CompareLeave" class="table table-striped custom-table table-nowrap mb-0" <%--style="font-size: 0.60rem;"--%>>
                                                    <thead>
                                                        <tr id="CompareLeave_H_tr" class="text-dark">
                                                        </tr>
                                                    </thead>
                                                    <tbody id="CompareLeave_B_tbody" >
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%-- Leave History --%>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-0">
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="card-title mb-3">Leave History</h3>

                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-lg-12">
                                            <div class="table-responsive min-max-vh-280">
                                                <table id="tbl_History1" class="table table-striped custom-table mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Employee</th>
                                                            <th>Leave Type</th>
                                                            <th>From</th>
                                                            <th>To</th>
                                                            <th>Days</th>


                                                            <th class="text-center">Status</th>
                                                            <!--<th class="text-right">Actions</th>-->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptLeaveHistory" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <h2 class="table-avatar">
                                                                            <a href="#" class="avatar">
                                                                                <asp:Image ID="img_emp" runat="server" ImageUrl='<%# Bind("EMD_PHOTO")%>' />
                                                                            </a>
                                                                            <a><%# Eval("EMP_NAME")%> <span><%# Eval("EMP_DESIGNATION")%></span></a>
                                                                        </h2>
                                                                    </td>
                                                                    <td><%# Eval("ELT_DESCR")%></td>
                                                                    <td><%# Eval("FROM_DATE")%></td>
                                                                    <td><%# Eval("TO_DATE")%></td>
                                                                    <td><%# Eval("LEAVEDAYS")%></td>


                                                                    <td class="text-center">
                                                                        <div class="dropdown action-label">
                                                                            <a class="btn btn-white btn-sm btn-rounded" href="#" onclick="ApprovalPopup('<%# Eval("username")%>','<%# Eval("ID")%>','H')" data-toggle="modal" data-target="#Approve_leave" aria-expanded="false" style="background-color: <%# Eval("APS_STATUS_COLOR")%>">
                                                                                <i class="fa fa-dot-circle-o text-info"></i><%# Eval("APS_STATUS")%>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <!--<a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-purple"></i> New</a>
                                                                            <a class="dropdown-item" href="#"><i class="fa fa-dot-circle-o text-info"></i> Pending</a>-->

                                                                            </div>
                                                                        </div>
                                                                    </td>

                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <tr id="trApprl" runat="server" visible="false">
                                                            <td colspan="6" style="text-align:center">
                                                                <asp:Label ID="lblMessage2" runat="server" Text =""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                        <%--             <script>
                                            $(document).ready(function () {
                                                $('#telecom').DataTable({
                                                    "order": [[0, "desc"]]
                                                });
                                            });
                                        </script>--%>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

               
                <div class="col-lg-3">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="card-title pb-3 border-bottom">Team Member Details</h3>
                            </div>
                        </div>
                        <div class="row">
                              
                            <%-- <div class="col-md-12">
                                <div class="navbar ">
                                    <div class="search-box m-t-0">
                                       <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" placeholder="Search">
                                            <span class="input-group-append">
                                                <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="contacts-list col-md-12">
                                <ul class="contact-list">
                                    <asp:Repeater ID="rpt_Profile" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <div class="contact-cont">
                                                    <div class="float-left user-img">
                                                        <a href="" data-toggle="modal" data-target="#View_Profile" class="avatar" onclick="ProfilePopup('<%# Eval("EMP_ID")%>'); return false;">

                                                            <asp:Image ID="img_emp" runat="server" CssClass="rounded-circle" ImageUrl='<%# Bind("EMP_PHOTO")%>' />
                                                            <%--<span class="status online"></span>--%>
                                                        </a>
                                                    </div>
                                                    <div class="contact-info">
                                                        <span class="contact-name text-ellipsis"><%# Eval("EMP_NAME")%>

                                                        </span>
                                                        <span class="contact-date"><%# Eval("EMP_DESIGNATION")%></span>
                                                    </div>
                                                    <ul class="contact-action">
                                                        <li class="dropdown dropdown-action">
                                                            <a href="" class="dropdown-toggle action-icon" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <a class="dropdown-item" href="#" onclick="ProfilePopup('<%# Eval("EMP_ID")%>'); " data-toggle="modal" data-target="#View_Profile">Basic Details</a>
                                                                <a class="dropdown-item" href="#" id="LoadLeave" onclick="LeaveHistoryPopup('<%# Eval("EMP_ID")%>','<%# Eval("EMP_bOn_Eleave")%>'); ">Leave History</a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:Label ID="lblMsg3" runat="server" Text =""></asp:Label>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- /Page Content -->

    </div>
    <!-- /Page Wrapper -->
    <!--View Profile-->
    <div id="View_Profile" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="card-box mb-0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-view">
                                    <div class="profile-img-wrap">
                                        <div class="profile-img btnWithoutCursor">                                            
                                                <img id="EMP_PHOTO" alt="" />
                                        </div>
                                    </div>
                                    <div class="profile-basic">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="profile-info-left">
                                                    <ul class="personal-info">
                                                        <li>
                                                            <div class="title">Name:</div>
                                                            <div class="text"><span id="EMP_NAME"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Employee No:</div>
                                                            <div class="text"><span id="EMP_NO"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Department:</div>
                                                            <div class="text"><span id="EMP_DEP"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Gender:</div>
                                                            <div class="text"><span id="EMP_GENDER"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Date of Birth:</div>
                                                            <div class="text"><span id="EMP_DOB"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Marital Status:</div>
                                                            <div class="text"><span id="EMP_MSTATUS"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Mobile Number:</div>
                                                            <div class="text"><span id="EMP_MOBILE"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Grade-level:</div>
                                                            <div class="text"><span id="EMP_GRDLEVEL"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Emergency Contact Person (Local):</div>
                                                            <div class="text"><span id="EMP_EMERLOCCONT"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Emergency Contact Person (Home):</div>
                                                            <div class="text"><span id="EMP_EMERHOMECONT"></span></div>
                                                        </li>
                                                       <%-- <li>
                                                            <div class="title">Visa Issued Date:</div>
                                                            <div class="text"><span id="EMP_VISA_IDATE"></span></div>
                                                        </li>
                                                        <li>
                                                            <div class="title">Visa Issued Place:</div>
                                                            <div class="text"><span id="EMP_VISA_PLACE"></span></div>
                                                        </li>--%>
                                                        <%--     <li>
                                                            <div class="title">Residence Emirate:</div>
                                                            <div class="text"><span id="EMP_RES_EMIRATE"></span></div>
                                                        </li>--%>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="profile-info-left border-0">
                                                <ul class="personal-info">
                                                    <li>
                                                        <div class="title">Nationality:</div>
                                                        <div class="text"><span id="EMP_NATIONALITY"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Group Joining Date:</div>
                                                        <div class="text"><span id="EMP_GRPJOINDT"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Unit Joining Date:</div>
                                                        <div class="text"><span id="EMP_UNITJOINDT"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Category:</div>
                                                        <div class="text"><span id="EMP_CAT"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Position Description:</div>
                                                        <div class="text"><span id="EMP_POSDESCR"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Official Email ID:</div>
                                                        <div class="text"><span id="EMP_OFFEMAIL"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Personal Email ID:</div>
                                                        <div class="text"><span id="EMP_PEREMAIL"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title"></div>
                                                        <div class="text"><span id="EMP_NODATA"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Contact Number:</div>
                                                        <div class="text"><span id="EMP_EMERLOCCONNUM"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Contact Number:</div>
                                                        <div class="text"><span id="EMP_EMERHOMRCONTNUM"></span></div>
                                                    </li>
                                                    <%--<li>
                                                        <div class="title">Passport No:</div>
                                                        <div class="text"><span id="EMP_PP_NO"></span></div>
                                                    </li>--%>
                                                   <%-- <li>
                                                        <div class="title">EmiratesID No:</div>
                                                        <div class="text"><span id="EMP_EMIRATES_ID"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">EID Expiry Date:</div>
                                                        <div class="text"><span id="EMP_EID_EXPIRY"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">UID No:</div>
                                                        <div class="text"><span id="EMP_UID"></span></div>
                                                    </li>


                                                    <li>
                                                        <div class="title">Visa Expiry Date:</div>
                                                        <div class="text"><span id="EMP_VISA_EDATE"></span></div>
                                                    </li>

                                                    <li>
                                                        <div class="title">Visa Sponsor:</div>
                                                        <div class="text"><span id="EMP_SPONSOR"></span></div>
                                                    </li>
                                                    <li>
                                                        <div class="title">Existing Insurance Card No:</div>
                                                        <div class="text"><span id="EMP_INSURANCE"></span></div>
                                                    </li>--%>
                                                </ul>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!--View Profile-->
    <!--View Leave History Popup-->
    <div id="View_History" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-content">
                    <%--DYNAMIC URL LOADING--%>
                </div>

            </div>
        </div>

    </div>
    <!--View Leave History Popup-->


    <!--pop for Leave Approval-->
    <div id="Approve_leave" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Leave Request Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="Closesa();">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-lg-12">
                                <label><span class="" id="lbl_Message"></span></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group dropdown">
                                    <label>Employee Name  <span class="text-danger"></span></label>
                                    <div class="">
                                        <input id="APP_NAME" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Leave Type <span class="text-danger"></span></label>
                                    <div class="">
                                        <input id="APP_TYPE" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6" id="modal_Approve_orignal" style="display:none">
                                <div class="form-group">
                                    <label>Original Leave <span class="text-danger"></span></label>
                                    <div class="">
                                        <textarea id="APP_ORG" class="form-control"  readonly></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Leave Period  <span class="text-danger"></span></label>
                                    <div class="">
                                        <input id="APP_PERIOD" class="form-control " type="text" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Leave Days <span class="text-danger"></span></label>
                                    <input id="APP_DAYS" class="form-control" type="text" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Details  <span class="text-danger"></span></label>
                                    <input id="APP_DETAILS" class="form-control" type="text" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6" id="modal_Cancel_reason" style="display:none">
                                <div class="form-group">
                                    <label>Reason For Cancellation  <span class="text-danger"></span></label>
                                    <%--<input id="APP_CANCEL_REMARKS" class="form-control" type="text" readonly>--%>
                                    <textarea id="APP_CANCEL_REMARKS" class="form-control"  readonly></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6" id="modal_Approve_remarks">
                                <div class="form-group">
                                    <label>Remarks   <span class="text-danger"></span></label>
                                    <input id="txt_Remarks" class="form-control" type="text">
                                </div>
                            </div>                                                        
                            <div class="col-lg-6" id="modal_Approve_document">
                                <div class="form-group">
                                   <a id="showLeavefile"  style="display:none" >Download document</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <a class="btn btn-primary" id="btnApprove" onclick="SaveApproval('A')" style="width: 100px;">Approve</a>
                                <a class="btn btn-danger" id="btnReject" onclick="SaveApproval('R')" style="width: 100px;">Reject </a>
                                <a href="#" class="btn btn-warning" onclick="CloseModal();Closesa();" style="width: 100px;">Close </a>

                            </div>
                        </div>
                        <input type="hidden" id="hf_EMP_ID" />
                        <input type="hidden" id="hf_ELT_ID" />
                        <input type="hidden" id="hf_ActualELA_ID" />
                        <input type="hidden" id="hf_APS_ID" />
                        <input type="hidden" id="hf_FROM_DT" />
                        <input type="hidden" id="hf_TO_DT" />
                    </form>
                </div>
            </div>
        </div>
    </div>


     <!-- / ADD Calendar icon click event -->
    <script type="text/javascript">
        $('.fa-calendar').on('click', function () {          
            $(this).next().focus();  
    });
    </script>


    <!--pop for Leave Approval-->
    <script type="text/javascript">

        function checkleaves() {
            var fromdate = $('#Compare_Fromdt').val();
           
            var todate = $('#Compare_Todt').val();
            
            var initial = fromdate.split(/\//);
            var final = todate.split(/\//);
            var Month, Month_F;

            if (initial[1] == "Jan") {
                Month = "1";
            } else if (initial[1] == "Feb") {
                Month = "2";
            }
            else if (initial[1] == "Mar") {
                Month = "3";
            }
            else if (initial[1] == "Apr") {
                Month = "4";
            }
            else if (initial[1] == "May") {
                Month = "5";
            } else if (initial[1] == "Jun") {
                Month = "6";
            }
            else if (initial[1] == "Jul") {
                Month = "7";
            }
            else if (initial[1] == "Aug") {
                Month = "8";
            }
            else if (initial[1] == "Sep") {
                Month = "9";
            }
            else if (initial[1] == "Oct") {
                Month = "10";
            }
            else if (initial[1] == "Nov") {
                Month = "11";
            }
            else if (initial[1] == "Dec") {
                Month = "12";
            }

            if (final[1] == "Jan") {
                Month_F = "1";
            } else if (final[1] == "Feb") {
                Month_F = "2";
            }
            else if (final[1] == "Mar") {
                Month_F = "3";
            }
            else if (final[1] == "Apr") {
                Month_F = "4";
            }
            else if (final[1] == "May") {
                Month_F = "5";
            } else if (final[1] == "Jun") {
                Month_F = "6";
            }
            else if (final[1] == "Jul") {
                Month_F = "7";
            }
            else if (final[1] == "Aug") {
                Month_F = "8";
            }
            else if (final[1] == "Sep") {
                Month_F = "9";
            }
            else if (final[1] == "Oct") {
                Month_F = "10";
            }
            else if (final[1] == "Nov") {
                Month_F = "11";
            }
            else if (final[1] == "Dec") {
                Month_F = "12";
            }

            //var dateadd = (Month + '/' + initial[0]) + '/' + initial[2];
            //var dateend = (Month_F + '/' + final[0]) + '/' + final[2];

            var dateadd = (initial[2], Month - 1, initial[0]);
            var dateend = (final[2], Month_F - 1, final[0]);

            const date1 = new Date(dateadd);
            const date2 = new Date(dateend);
            if (date2 < date1) {
                alert("End date is lesser than start date. Please select another date");
                $('#Compare_Fromdt').val('');
                $('#Compare_Todt').val('');
            }
        }

        function CloseModal()
        {
            $('#Approve_leave').modal('hide');
        }
        function ProfilePopup(emp_id) {
            //FOR LOADING THE EMPLOYEE BASIC DETAILS
            $.ajax({
                type: "POST",
                url: "/PHOENIXBETA/GemsEss/MyTeamDetails.aspx/GetProfileInfo",
                data: '{ emp_id: " ' + emp_id + ' "}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    //useresult(results.d); 
                    //alert(results.d);
                    var fields = JSON.parse(results.d);
                    $('#EMP_NAME').text(fields.EMP_DISPLAYNAME);
                    $('#EMP_NO').text(fields.EMPNO);
                    $('#EMP_GENDER').text(fields.EMP_GENDER); //MISSING
                    $('#EMP_DOB').text(fields.EMP_DOB);//MISSING
                    $('#EMP_MSTATUS').text(fields.EMP_MSTATUS);//MISSING
                    $("#EMP_PHOTO").attr("src", fields.EMD_PHOTO);
                    $('#EMP_NATIONALITY').text(fields.EMP_NATIONALITY); //MISSING


                    //$('#EMP_VISA_IDATE').text(fields.VISAISUEDATE);
                    //$('#EMP_VISA_PLACE').text(fields.VISAISSUEPLACE);
                    // $('#EMP_RES_EMIRATE').text(fields.EMP_RES_EMIRATE); //MISSING                    
                   // $('#EMP_PP_NO').text(fields.PASSPORTNO);
                   // $('#EMP_EMIRATES_ID').text(fields.EIDNO);
                   // $('#EMP_EID_EXPIRY').text(fields.EIDEXPDATE);
                    //$('#EMP_UID').text(fields.EMP_UIDNO);
                   // $('#EMP_VISA_EDATE').text(fields.VISAEXPDATE);
                   // $('#EMP_SPONSOR').text(fields.EMP_VISA_SPONSOR);
                   // $('#EMP_INSURANCE').text(fields.emp_insurance_number);
                   

                }
            });

            $.ajax({
                type: "POST",
                url: "/PHOENIXBETA/GemsEss/MyTeamDetails.aspx/GetProfileInfo2",
                data: '{ emp_id: " ' + emp_id + ' "}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    //useresult(results.d); 
                    //alert(results.d);
                    var fields = JSON.parse(results.d);
                    $('#EMP_DEP').text(fields.EMP_DEPT_DESCR);
                    $('#EMP_GRDLEVEL').text(fields.GRADE_LEVEL);
                    $('#EMP_CAT').text(fields.CATEGORY_DESC); 
                    $('#EMP_POSDESCR').text(fields.POS_DESCRIPTION);                    
                    $('#EMP_UNITJOINDT').text(fields.BSU_JOIN_DATE);
                    $('#EMP_GRPJOINDT').text(fields.JOIN_DATE);                
                }
            });

            $.ajax({
                type: "POST",
                url: "/PHOENIXBETA/GemsEss/MyTeamDetails.aspx/GetProfileInfo3",
                data: '{ emp_id: " ' + emp_id + ' "}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    //useresult(results.d); 
                    //alert(results.d);
                    var fields = JSON.parse(results.d);
                    $('#EMP_MOBILE').text(fields.EMD_CUR_MOBILE);
                    $('#EMP_OFFEMAIL').text(fields.EMD_EMAIL);
                    $('#EMP_PEREMAIL').text(fields.EMD_Personal_EmailID);
                    $('#EMP_EMERLOCCONT').text(fields.EMD_CONTACT_LOCAL);
                    $('#EMP_EMERHOMECONT').text(fields.EMD_CONTACT_HOME);
                    $('#EMP_EMERLOCCONNUM').text(fields.EMD_CONTACT_LOCAL_NUM);
                    $('#EMP_EMERHOMRCONTNUM').text(fields.EMD_CONTACT_HOME_NUM);
                }
            });
            return true;
        }

        function ApprovalPopup(username, id,type) {
            //FOR LOADING THE LEAVE APPROVAL POPUP
            if (type=='H')
            {
                $('#btnApprove').hide();
                $('#btnReject').hide();
                $('#modal_Approve_remarks').hide();
                
            }
            else
            {
                $('#btnApprove').show();
                $('#btnReject').show();
                $('#modal_Approve_remarks').show();
            }
           
            $.ajax({
                type: "POST",
                url: "/PHOENIXBETA/GemsEss/MyTeamDetails.aspx/GetApprovalStatus",
                data: '{ username: " ' + username + ' " , id:"' + id + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                   
                    var a = results.d.split('||');
                   // var fields = JSON.parse(results.d);
                    //alert(fields);DOC_ID,APS_DOCTYPE,APS_STATUS,EMP_NAME,ELT_DESCR,FROM_DATE,TO_DATE,LEAVEDAYS,ELA_REMARKS,EMP_ID,ELA_ELT_ID,ACTUAL_ELA_ID,APS_ID,ORIGINAL_LEAVE,ELM_CANCELLATION_REMARKS,DOCUMENT
                    $("#APP_NAME").attr("placeholder", a[0]);
                    $('#APP_NAME').attr("disabled", "disabled");
                    $("#APP_TYPE").attr("placeholder", a[1]);
                    $('#APP_TYPE').attr("disabled", "disabled");
                    $("#APP_PERIOD").attr("placeholder", "From " + a[2] + " To " + a[3]);
                    $('#APP_PERIOD').attr("disabled", "disabled");
                    $("#APP_DAYS").attr("placeholder", a[4]);
                    $('#APP_DAYS').attr("disabled", "disabled");
                    $("#APP_DETAILS").attr("placeholder", a[5]);
                    $("#APP_DETAILS").attr("disabled", "disabled");
                    $("#hf_EMP_ID").val(a[6]);
                    $("#hf_ELT_ID").val(a[7]);
                    $("#hf_ActualELA_ID").val(a[8]);
                    $("#hf_APS_ID").val(a[9]);
                    $("#hf_FROM_DT").val(a[2]);
                    $("#hf_TO_DT").val(a[3]);
                    $('#lbl_Message').text("");
                   
                    if (a[10] == 'LEAVE-M') {
                        $("#modal_Approve_orignal").css('display', 'block');
                        $("#APP_ORG").attr("placeholder", a[14]);
                        $("#APP_ORG").attr("disabled", "disabled");
                        
                        if (a[11].toLowerCase() == 'request for cancellation') {
                            $("#modal_Cancel_reason").css('display', 'block');
                            $("#APP_CANCEL_REMARKS").attr("placeholder", a[15]);
                            $("#APP_CANCEL_REMARKS").attr("disabled", "disabled");
                        }
                        else
                        {
                            $("#modal_Cancel_reason").css('display', 'none');
                        }
                        
                    } else
                    {
                        $("#modal_Approve_orignal").css('display', 'none');
                       // $("#modal_Cancel_reason").css('display', 'none');
                    }

                    if (a[7] == 'ML') {
                        if (a[12] == 0 || a[12] == '0') {
                            var leaveurl = ""                                                      
                        }
                        else {
                            var leaveurl = "../Common/FileHandler.ashx?" + a[13];
                            $('#showLeavefile').css('display', 'block');
                            $('#showLeavefile').attr('href', leaveurl);
                        }
                    }else
                    {
                        var leaveurl = ""
                        $('#showLeavefile').css('display', 'none');
                    }
                }
            });
            return true;
        }


        function SaveApproval(type) {
            //FOR LOADING THE LEAVE APPROVAL - APPROVE OR REJECT BUTTON CLICK
            if (confirm('Are your sure?')) {
                var _bsu_id = '<%= Session("sbsuid") %>';
                var _username = '<%= Session("sUsr_name") %>';
                var _EMP_ID = $('#hf_EMP_ID').val();
                var _ELT_ID = $('#hf_ELT_ID').val();
                var _Actual_ELA_ID = $('#hf_ActualELA_ID').val();
                var _APS_ID = $('#hf_APS_ID').val();
                var _Fromdt = $('#hf_FROM_DT').val();
                var _Todt = $('#hf_TO_DT').val();
                var _Remarks = $('#txt_Remarks').val();
                var _ApprovalMode = type;
                $.ajax({
                    type: "POST",
                    url: "/PHOENIXBETA/GemsEss/MyTeamDetails.aspx/SAVE_LEAVE_APPROVAL",
                    data: '{ bsu_id: "' + _bsu_id + '", username : "' + _username + '", EMP_ID :"' + _EMP_ID + '", Fromdt :"' + _Fromdt + '", Todt :"' + _Todt + '", APS_ID : "' + _APS_ID + '", Actual_ELA_ID : "' + _Actual_ELA_ID + '", ELT_ID : "' + _ELT_ID + '", Remarks : "' + _Remarks + '", ApprovalMode : "' + _ApprovalMode + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                        $('#lbl_Message').text(results.d);
                        $('#lbl_Message').attr("class", "alert alert-info");
                        $('#btnApprove').css('display', 'none');
                        $('#btnReject').css('display', 'none');
                        $('#h_Action').val('1');
                    }
                });
                return true;
            }
            else
            {
                return false;
            }
        }
        function LeaveHistoryPopup(id, flag) {
            var date = new Date();
            var firstDayWithSlashes = ((date.getMonth() + 1) + '/' + date.getDate()) + '/' + date.getFullYear();
         
            var url2 = '/Payroll/empLeaveDetailsPopUp.aspx?empid=' + id + '&sdate=' + firstDayWithSlashes;
            var url = 'empLeaveApplicationListPopup_ESS.aspx?id=' + id;
            //   $('#View_History').modal('show').find('.modal-content').load(url);  -- for static pages this is an option
            if (flag.toLowerCase() == 'true') {
                Popup(url);
            } else
            {                
                Popup(url2);
            }          
        }

        function CompareLeave() {
            //FOR LOADING THE Compare Leave Table
            var _bsu_id = '<%= Session("sbsuid") %>';
              var _username = '<%= Session("sUsr_name") %>';
              var _Fromdt = $('#Compare_Fromdt').val();
              var _Todt = $('#Compare_Todt').val();
              if ($('#chkShowAll').prop('checked') == true) {
                  var _ShowAll = "True";
              } else {
                  var _ShowAll = "False";
              }
              $.ajax({
                  type: "POST",
                  url: "/PHOENIXBETA/GemsEss/MyTeamDetails.aspx/GET_LEAVE_PLAN",
                  data: '{ bsu_id: "' + _bsu_id + '", username : "' + _username + '", Fromdt :"' + _Fromdt + '", Todt :"' + _Todt + '", Showall :"' + _ShowAll + '"}',
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (results) {
                      //FOR HEADER
                      $('#CompareLeave_H_tr').html("");
                      var items = JSON.parse(results.d);
                      var lookup = {};
                      var result = [];

                      for (var item, i = 0; item = items[i++];) {
                          var dates = item.FORMATTED_DATE;

                          if (!(dates in lookup)) {
                              lookup[dates] = 1;
                              result.push(dates);
                          }
                      }
                      var tr;
                      for (var i = 0; i < result.length; i++) {
                          tr = $('<th/>');
                          tr.append(result[i]);
                          $('#CompareLeave_H_tr').append(tr);
                      }
                      $('#CompareLeave_H_tr').prepend("<th>Employee</th>");
                      
                      

                      var lookupemp = {};
                      var resultemp = [];
                      for (var item, i = 0; item = items[i++];) {
                          var emp = item.Names;

                          if (!(emp in lookupemp)) {
                              lookupemp[emp] = 1;
                              resultemp.push(emp);
                          }
                      }
                          //FOR THE BODY

                      var layout="";

                      for (var j = 0; j < resultemp.length; j++) {   //loop for tr

                          layout+="<tr>";
                          var data_filter = items.filter(function (element) { return element.Names === resultemp[j]; });
                          
                          layout+=" <td><h2 class='table-avatar'><a class='avatar avatar-xs'><img  src='"+data_filter[0].Photo+"'></a><a>"+data_filter[0].Names+"</a></h2></td>";

                                for (var i = 0; i < data_filter.length; i++) {    //loop for td
                                    //td = $('<td/>');
                                    //$('#CompareLeave_B_tbody').append("<td>");
                                    layout+="<td>";
                                        if (data_filter[i].Y == "0") //NO LEAVE
                                        {
                                            layout+="";
                                        }
                                        else if (data_filter[i].Y == "1") // APPROVED LEAVE
                                        {
                                            layout+="<i class='fa fa-check text-success'></i>";
                                        }
                                        else if (data_filter[i].Y == "2") // REQUEST PENDING
                                        {
                                            layout+="<i class='fa fa-pause text-warning'></i>";
                                        }
                                        else if (data_filter[i].Y == "3") // REJECTED
                                        {
                                            layout+="<i class='fa fa-close text-danger'></i>";
                                        }

                                        layout+="</td>";
                                }
                                layout+="</tr>";
                                
                               
                      }
                      $('#CompareLeave_B_tbody').html(layout);
                  },
                  error: function (error) {
                      //alert(error);
                  }
              });
              return true;
          }


    </script>
    <script>
        $(document).ready(function () {
            $('#tbl_Request').DataTable({
                "paging": true,
                "ordering": false,
                "bLengthChange": false,
                "bInfo": false,

            });
            $('#tbl_History').DataTable({
                "ordering": false,
                "paging": true
            });
         
            $(".datetimepicker1").datetimepicker({
                format: 'DD/MMM/YYYY'
            });
            var date = new Date();
            var FromDate = moment(new Date(date.getFullYear(), date.getMonth(), 1)).format("DD/MMM/YYYY"); 
            var ToDate = moment().format("DD/MMM/YYYY");
            $('#Compare_Todt').val(ToDate);
            $('#Compare_Fromdt').val(FromDate);
            //
            CompareLeave();
            $('#h_Action').val('0');
        });


        function Closesa() {
            $("#APP_NAME").attr("placeholder", "");
            $('#APP_NAME').attr("disabled", "disabled");
            $("#APP_TYPE").attr("placeholder", "");
            $('#APP_TYPE').attr("disabled", "disabled");
            $("#APP_PERIOD").attr("placeholder", "");
            $('#APP_PERIOD').attr("disabled", "disabled");
            $("#APP_DAYS").attr("placeholder", "");
            $('#APP_DAYS').attr("disabled", "disabled");
            $("#APP_DETAILS").attr("placeholder", "");
            $("#APP_DETAILS").attr("disabled", "disabled");
            $("#modal_Approve_orignal").css('display', 'none');
            $("#modal_Cancel_reason").css('display', 'none');
            $("#hf_EMP_ID").val("");
            $("#hf_ELT_ID").val("");
            $("#hf_ActualELA_ID").val("");
            $("#hf_APS_ID").val("");
            $("#hf_FROM_DT").val("");
            $("#hf_TO_DT").val("");
            $('#lbl_Message').text("");
            if (document.getElementById('h_Action').value == '1')
            {                
                window.location.href = "MyTeamDetails.aspx";
            }           
        }
    </script>
</asp:Content>

