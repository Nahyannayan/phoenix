<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="empLeaveApplicationListPopup_ESS.aspx.vb" Inherits="Payroll_empLeaveApplicationListPopup"
    Title="Untitled Page" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html>

     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Lineawesome CSS -->
    <link rel="stylesheet" href="assets/css/line-awesome.min.css">

    <!-- Chart CSS -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Bootstrap DataTables CSS -->
    <link href="assets/css/dataTables.bootstrap4.min.css" rel="stylesheet" media="all" />

    <!-- Bootstrap Datetimepicker CSS -->
    <link href="assets/css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <link rel="stylesheet" href="assets/css/semantic.min.css">
    <link rel="stylesheet" href="assets/css/prism.min.css">


<script language="javascript" type="text/javascript">


    function ShowLeaveDetail(id) {
        //var sFeatures;
        //sFeatures="dialogWidth: 700px; ";
        //sFeatures+="dialogHeight: 450px; ";
        //sFeatures+="help: no; ";
        //sFeatures+="resizable: no; ";
        //sFeatures+="scroll: yes; ";
        //sFeatures+="status: no; ";
        //sFeatures+="unadorned: no; ";
        //var NameandCode;
        //var result;
        //result = window.showModalDialog("ShowEmpLeave.aspx?ela_id="+id,"", sFeatures)

        //return false;
        Popup("ShowEmpLeave_ESS.aspx?ela_id=" + id)
    }
</script>
    <style type="text/css">
        .fancybox-inner{
            width:100% !important;
        }
        .fancybox-skin{
            padding:4px !important;
        }
       .fancybox-close {
            top: -10px !important;
            right: -10px !important;
        }
       .chartjs-render-monitor {
           min-height:250px !important;
       }
       #RadGrid1 tr:last-child {
           background-color:#c3c3c3;
       }
    </style>

<body>
    <form id="form1" runat="server">


        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
        </ajaxToolkit:ToolkitScriptManager>

        <div class="table-responsive">
            <table align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr valign="top">
                    <td valign="top" width="50%" align="left">
                        <asp:HyperLink ID="hlAddNew" runat="server" Visible="false">Add New</asp:HyperLink>
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    <td align="right" width="50%"></td>
                </tr>
            </table>
            <div class="container">                
                    <div class="row">
                        <div class="col-lg-12 col-11">
                    <div class="form-group">

                        <div class="row">
                           
                                
                                <div class="col-lg-3 col-12 text-left m-auto">
                                    <span class="field-label">Select Calendar Year</span>
                                </div>
                                <div class="col-lg-4 col-12 text-left">
                                    <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                </div>
                            <div class="col-lg-5">
                                <asp:Button ID="btnGO" runat="server" CssClass="button" Text="View" Visible="false" />
                                </div>
                                
                            
                        </div>

                        <div class="row mt-3">
                            <div class="col-lg-6 col-12">
                                <div class="card-box table-responsive p-0">
                                <canvas id="annualLeave" runat="server"></canvas> 
                                </div>
                            </div>
                            
                            
                        <div class="col-lg-6 col-12">
                            <div class="card-box table-responsive p-0">
                            <canvas id="leaveSummary" ></canvas>
                            </div>
                        </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-12 col-12">
                              <div class="card-box table-responsive p-0">  
                                <asp:GridView  ID="RadGrid1" runat="server" AllowPaging="false" CellSpacing="0" OnRowDataBound ="RadGrid1_RowDataBound" CssClass="table  table-bordered custom-table mb-0"
                                    PageSize="5" GridLines="Both"  Width="100%">
                                    <HeaderStyle BackColor="#f6f6f6" HorizontalAlign="Center"></HeaderStyle>
                                   <%-- <MasterTableView>
                                    </MasterTableView>
                                    <HeaderStyle BackColor="#8BB174" HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <AlternatingItemStyle HorizontalAlign="Center" />--%>
                                    <%--<PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>--%>
                                </asp:GridView>
                            </div>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <div class="card-box table-responsive"> 
                                
                                <asp:GridView ID="gvLegend" runat="server" AutoGenerateColumns="true" CssClass="table table-striped table-bordered custom-table mb-0" ShowHeader="false" >
                                </asp:GridView>
                                <asp:Label ID="lblLegend" runat="server" Text="" Width="100%"></asp:Label>
                            </div>
                                </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                            <div class="col-lg-12 col-11">
                                <div class="card-box table-responsive p-0">
                        <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False"  BorderColor="#f3f3f3" DataKeyNames="EMPNO"
                            CssClass="table table-striped table-bordered custom-table mb-0" Width="100%" AllowPaging="True" PageSize="30">
                            <Columns>
                                <asp:BoundField DataField="EMP_NAME" HeaderText="Name" ReadOnly="True"
                                    SortExpression="EMP_NAME" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="EMPNO" HeaderText="Staff ID" ReadOnly="True"
                                    SortExpression="EMPNO" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="ELT_DESCR" HeaderText="Leave Type" SortExpression="ELT_DESCR" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="LEAVEDAYS" HeaderText="Leave Days" ReadOnly="True" SortExpression="LEAVEDAYS" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="ELA_DTFROM" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date From"
                                    HtmlEncode="False" SortExpression="ELA_DTFROM" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="ELA_DTTO" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Date To"
                                    HtmlEncode="False" SortExpression="ELA_DTTO" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="ELA_APPRSTATUS" HeaderText="Status" ReadOnly="True" SortExpression="ELA_APPRSTATUS" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:BoundField DataField="ELA_APPRDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Approval Date"
                                    HtmlEncode="False" SortExpression="ELA_APPRDATE" HeaderStyle-Width="10%"></asp:BoundField>
                                <asp:TemplateField HeaderText="Remarks" SortExpression="ELA_REMARKS">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="<%# &quot;ShowLeaveDetail('&quot; & Container.DataItem(&quot;ELA_ID&quot;) & &quot;');return false;&quot; %>"
                                            Text='<%# Bind("ELA_REMARKS") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <%-- <asp:HyperLink ID="hlEdit" runat="server" >View</asp:HyperLink>--%>
                                        <asp:LinkButton ID="lbView" runat="server" Text="View" OnClick="lbView_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                   <HeaderStyle Width="10%" /> 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Print">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="hlPrint" runat="server" Text="Print" OnClick="btnPrint_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="10%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ELA_ID" Visible="False">
                                    <EditItemTemplate>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("ELA_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="10%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="APPRSTATUS" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAPPRSTATUS" runat="server" Text='<%# Bind("APPRSTATUS") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="10%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                                </div>
                            </div>
                    </div>
            </div>
            
        </div>

        <asp:HiddenField ID="hidChart1" runat="server" Value ="" />
        <asp:HiddenField ID="hidChart2" runat="server" Value ="" />
    </form>

     <!-- jQuery -->
    <script src="assets/js/jquery-3.2.1.min.js"></script>

    <!--new-->
    <script src="assets/js/jquery-1.12.4.js"></script>

    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Slimscroll JS -->
    <script src="assets/js/jquery.slimscroll.min.js"></script>

    <!-- Datatable JS -->
    <script src="assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

    <script src="assets/js/moment.js"></script>
    <%--    <script src="assets/js/moment-with-locales.js"></script>--%>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Chart JS -->
    <script src="assets/plugins/morris/morris.min.js"></script>
    <script src="assets/plugins/raphael/raphael.min.js"></script>
    <script src="assets/js/chart.js"></script>

    <!-- Custom JS -->
    <script src="assets/js/app.js"></script>

    <!-- Fancy Box -->
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />

    <script>
        function Popup(url) {
            $.fancybox({
                'width': '100%',
                'height': '100%',
                'autoScale': false,
                'fitToView': false,
                'autoSize': false,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url
            });
           };
    function ClosePopup(a) {
        parent.$.fancybox.close()
        parent.AcceptValues(a)

    }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var chart1 = document.getElementById('<%=hidChart1.ClientID%>').value;
            var chartval = chart1.split('||');
            

          

            var ctx = document.getElementById("annualLeave").getContext('2d');
            ctx.height = 200;
            var myChart = new Chart(ctx, {
                type: 'pie',
                maintainAspectRatio: false,
             data: {
               labels: ["Leave Taken", "Future Leave", "Balance Leave"],
               datasets: [{
                 backgroundColor: [
                   "#2ecc71",
                   "#3498db",
                   "#f1c40f"
                 ],
                 data: [chartval[0], chartval[1], chartval[2]]
               }]
                },
             options: {  
    responsive: true,
    maintainAspectRatio: false
}
            });



            var chart2 = document.getElementById('<%=hidChart2.ClientID%>').value;
            // alert(chart2);
            var fchart = chart2.split(';');
            var fc1 = fchart[0].split('||');
            var fc2 = fchart[1].split('||');
            var fc3 = fchart[2].split('||');
            var fc4 = fchart[3].split('||');


            var ctx = document.getElementById("leaveSummary").getContext('2d');
             ctx.height = 200;
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    datasets: [{
                        label: 'Annual Leave',
                        backgroundColor: "rgba(255, 99, 132, 0.9)",
                        borderWidth: 1,
                        data: [fc1[0], fc1[1], fc1[2], fc1[3], fc1[4], fc1[5], fc1[6], fc1[7], fc1[8], fc1[9], fc1[10], fc1[11]]
                    }, {
                        label: 'Medical Leave',
                        backgroundColor: "rgba(54, 162, 235, 0.9)",
                        borderWidth: 1,
                        data: [fc2[0], fc2[1], fc2[2], fc2[3], fc2[4], fc2[5], fc2[6], fc2[7], fc2[8], fc2[9], fc2[10], fc2[11]]
                    }, {
                        label: 'Emergency Leave',
                        backgroundColor: "rgba(255, 206, 86, 0.9)",
                        borderWidth: 1,
                        data: [fc3[0], fc3[1], fc3[2], fc3[3], fc3[4], fc3[5], fc3[6], fc3[7], fc3[8], fc3[9], fc3[10], fc3[11]]
                        }
                        //, {
                        //label: 'Others',
                        //backgroundColor: "rgba(75, 192, 192, 0.9)",
                        //borderWidth: 1,
                        //data: [fc4[0], fc4[1], fc4[2], fc4[3], fc4[4], fc4[5], fc4[6], fc4[7], fc4[8], fc4[9], fc4[10], fc4[11]]
                        //}
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    </script>
   
</body>
</html>
