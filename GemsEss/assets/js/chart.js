$(document).ready(function() {
	
	// Area Chart
	
    //Morris.Area({
	//	element: 'area-charts',
	//	data: [
	//		{ y: '2006', a: 100, b: 90 },
	//		{ y: '2007', a: 75,  b: 65 },
	//		{ y: '2008', a: 50,  b: 40 },
	//		{ y: '2009', a: 75,  b: 65 },
	//		{ y: '2010', a: 50,  b: 40 },
	//		{ y: '2011', a: 75,  b: 65 },
	//		{ y: '2012', a: 100, b: 90 }
	//	],
	//	xkey: 'y',
	//	ykeys: ['a', 'b'],
	//	labels: ['Total Invoice', 'Pending Invoice'],
	//	lineColors: ['#667eea','#764ba2'],
	//	lineWidth: '3px',
	//	resize: true,
	//	redraw: true
    //});

	//// Bar Chart
	
	//Morris.Bar({
	//	element: 'bar-charts',
	//	data: [
	//		{ y: '2006', a: 100, b: 90 },
	//		{ y: '2007', a: 75,  b: 65 },
	//		{ y: '2008', a: 50,  b: 40 },
	//		{ y: '2009', a: 75,  b: 65 },
	//		{ y: '2010', a: 50,  b: 40 },
	//		{ y: '2011', a: 75,  b: 65 },
	//		{ y: '2012', a: 100, b: 90 }
	//	],
	//	xkey: 'y',
	//	ykeys: ['a', 'b'],
	//	labels: ['Total Income', 'Total Outcome'],
	//	lineColors: ['#667eea','#764ba2'],
	//	lineWidth: '3px',
	//	barColors: ['#667eea','#764ba2'],
	//	resize: true,
	//	redraw: true
	//});
	
	//// Line Chart
	
	//Morris.Line({
	//	element: 'line-charts',
	//	data: [
	//		{ y: '2013', a: 50, b: 55 },
	//		{ y: '2014', a: 75,  b: 65 },
	//		{ y: '2015', a: 50,  b: 40 },
	//		{ y: '2016', a: 75,  b: 65 },
	//		{ y: '2017', a: 50,  b: 40 },
	//		{ y: '2018', a: 75,  b: 65 },
	//		{ y: '2019', a: 100, b: 90 }
	//	],
	//	xkey: 'y',
	//	ykeys: ['a', 'b'],
 //       labels: ['Technical Skills', 'Management Skills'],
 //       lineColors: ['#667eea','#00c5fb'],
	//	lineWidth: '3px',
	//	resize: true,
	//	redraw: true
	//});
	
	// Donut Chart
		
	//Morris.Donut({
	//	element: 'pie-charts',
	//	colors: [
 //           '#00c5fb',
 //           '#0253cc',
 //           '#80e3ff',
 //           '#81b3fe'
	//	],
	//	data: [
	//		{ label: "Balance", value: 23 },
	//		{ label: "Annual", value: 15 },
	//		{ label: "Medical", value: 5 },
	//		{ label: "Emergency", value: 10 }
	//	],
	//	resize: true,
	//	redraw: true
	//});
		
});