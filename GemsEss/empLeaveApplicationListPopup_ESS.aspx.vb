Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Encryption64
Imports Telerik.Web.UI

'Version        Date             Author          Change
'1.1            20-Feb-2011      Swapna          To filter view screen based on logged in user's id
Partial Class Payroll_empLeaveApplicationListPopup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    If Session("sModule") = "SS" Then
    '        Me.MasterPageFile = "../mainMasterPageSS.master"
    '    Else
    '        Me.MasterPageFile = "../mainMasterPage.master"
    '    End If

    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            hlAddNew.NavigateUrl = "empLeaveApplicationView.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")

            Try
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                FillYear()
                LoadData()
                gridbind()


            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim USR_NAME As String
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            If Request.QueryString("id") <> "" Then
                str_Sql = "select USR_NAME from USERS_M where USR_EMP_ID=" + Request.QueryString("id")
                USR_NAME = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
            Else
                USR_NAME = Session("sUsr_name")
            End If

            'str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "'"     'V1.1 comment
            str_Sql = " exec GetLeaveDetails_PopUp '" & Session("SbSUID") & "','" & USR_NAME & "','" & ddlYear.SelectedValue & "'"    'V1.1
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvJournal.DataSource = ds.Tables(0)
            gvJournal.DataBind()
            If Request.QueryString("id") <> "" Then
                gvJournal.Columns(9).Visible = False
                gvJournal.Columns(10).Visible = False
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(e.Row.FindControl("lblELA_ID"), Label)
            'Dim hlEdit As New HyperLink
            'hlEdit = TryCast(e.Row.FindControl("hlEdit"), HyperLink)
            'If hlEdit IsNot Nothing And lblELA_ID IsNot Nothing Then
            '    ViewState("datamode") = Encr_decrData.Encrypt("view")
            '    hlEdit.NavigateUrl = "empLeaveApplicationView.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode")
            'End If

            Dim lbView As New LinkButton
            lbView = TryCast(e.Row.FindControl("lbView"), LinkButton)
            If lbView IsNot Nothing And lblELA_ID IsNot Nothing Then
                ViewState("datamode") = Encr_decrData.Encrypt("view")
                lbView.Attributes.Add("Forward", "empLeaveApplicationView.aspx?viewid=" & Encr_decrData.Encrypt(lblELA_ID.Text) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode"))

            End If


            Dim lblAPPRSTATUS As New Label
            lblAPPRSTATUS = TryCast(e.Row.FindControl("lblAPPRSTATUS"), Label)
            Dim hlPrint As New LinkButton
            hlPrint = TryCast(e.Row.FindControl("hlPrint"), LinkButton)
            If hlPrint IsNot Nothing And lblAPPRSTATUS IsNot Nothing Then
                If lblAPPRSTATUS.Text = "A" Then
                    hlPrint.Enabled = True
                Else
                    hlPrint.Enabled = False
                End If
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim printId As String = ""
            Dim gvCell As DataControlFieldCell
            gvCell = sender.parent
            If gvCell Is Nothing Then Exit Sub
            Dim gvRow As GridViewRow
            gvRow = gvCell.Parent
            If gvRow Is Nothing Then Exit Sub
            Dim lblELA_ID As New Label
            lblELA_ID = TryCast(gvRow.FindControl("lblELA_ID"), Label)
            If lblELA_ID Is Nothing Then Exit Sub
            printId = lblELA_ID.Text
            printId = Replace(printId, "A", "")
            Dim params As New Hashtable
            params.Add("@IMG_BSU_ID", Session("sbsuid"))
            params.Add("@IMG_TYPE", "LOGO")
            params.Add("userName", HttpContext.Current.Session("sUsr_name").ToString)
            params.Add("@ELA_ID", CInt(printId))
            params.Add("@DocType", "LEAVE")
            ' params("VoucherName") = labHead.Text.ToUpper().ToString()   'V1.1
            params("reportHeading") = "LEAVE APPLICATION FORM"
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = "Oasis"
                .reportParameters = params
                'If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../Payroll/Reports/Rpt/rptLeaveAppForm.rpt")
                'End If
            End With
            Session("rptClass") = rptClass
            'If Session("sModule") = "SS" Then
            '    Response.Redirect("~/Reports/ASPX Report/rptReportViewerSS.aspx")
            'Else
            '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            'End If
            ReportLoadSelection()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request cannot be processed"
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Protected Sub lbView_Click(sender As Object, e As EventArgs)
        Dim lbView As New LinkButton
        lbView = TryCast(sender, LinkButton)
        Dim forward As String = lbView.Attributes("forward")
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Closing Function", "ClosePopup('" + forward + "');", True)
    End Sub
    Private Sub LoadData()
        Dim ds As DataSet = GET_LEAVE_DATA()
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                '   RadHtmlChart2.DataSource = ds.Tables(0)
                '  RadHtmlChart2.DataBind()
            End If
            If ds.Tables.Count > 1 AndAlso Not ds.Tables(1) Is Nothing AndAlso ds.Tables(1).Rows.Count > 0 Then
                lblLegend.Text = ds.Tables(1).Rows(0)(0)
            End If
            If ds.Tables.Count > 2 AndAlso Not ds.Tables(2) Is Nothing AndAlso ds.Tables(2).Rows.Count > 0 Then
                RadGrid1.DataSource = ds.Tables(2)
                RadGrid1.DataBind()
                'Dim headeritem As GridHeaderItem = RadGrid1.MasterTableView.GetItems(GridItemType.Header)(0)
                'If Not headeritem Is Nothing Then
                '    For i As Int16 = 0 To headeritem.Cells.Count - 1
                '        Select Case headeritem.Cells(i).Text.Trim
                '            Case "AL"
                '                headeritem.Cells(i).BackColor = Drawing.Color.Violet
                '                headeritem.Cells(i).ForeColor = Drawing.Color.White
                '                Dim a = RadGrid1.MasterTableView.AutoGeneratedColumns.Length '.Columns.Item(i).Visible = False
                '            Case "EL"
                '                headeritem.Cells(i).BackColor = Drawing.Color.Indigo
                '                headeritem.Cells(i).ForeColor = Drawing.Color.White
                '            Case "ML"
                '                headeritem.Cells(i).BackColor = Drawing.Color.Green
                '                headeritem.Cells(i).ForeColor = Drawing.Color.White
                '            Case "PL"
                '                headeritem.Cells(i).BackColor = Drawing.Color.Red
                '                headeritem.Cells(i).ForeColor = Drawing.Color.White
                '            Case "TL"
                '                headeritem.Cells(i).BackColor = Drawing.Color.Orange
                '                headeritem.Cells(i).ForeColor = Drawing.Color.White
                '            Case "LOP"
                '                headeritem.Cells(i).BackColor = Drawing.Color.Blue
                '                headeritem.Cells(i).ForeColor = Drawing.Color.White
                '        End Select
                '    Next
                'End If

                Dim autogeneratecolumns As GridColumn
                For Each autogeneratecolumns In RadGrid1.Columns
                    Select Case autogeneratecolumns.HeaderText.Trim
                        Case "AL"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#426B69") '4472C4
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#FFBC42")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                        Case "EL"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#F5DD90")  '#FFBC01
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#D81159")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                        Case "ML"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#FF5964")  '#9E480D
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#0E9E9B")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                        Case "PL"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#04395E")   '#d2a679
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#d2a679")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                        Case "TL"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#750D37")   '#992600
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#992600")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                        Case "LOP"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#B3DEC1")   '#e6b800
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#e6b800")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                        Case "OTH"
                            autogeneratecolumns.HeaderStyle.BackColor = Drawing.ColorTranslator.FromHtml("#210124")   '#987300
                            autogeneratecolumns.HeaderStyle.ForeColor = Drawing.Color.White
                            autogeneratecolumns.HeaderStyle.Width = Unit.Pixel(25)
                            'autogeneratecolumns.ItemStyle.BackColor = Drawing.ColorTranslator.FromHtml("#38D995")
                            'autogeneratecolumns.ItemStyle.ForeColor = Drawing.Color.White
                    End Select
                Next

            End If
            If ds.Tables.Count > 3 AndAlso Not ds.Tables(3) Is Nothing AndAlso ds.Tables(3).Rows.Count > 0 Then
                ' ColumnChart.Visible = True
                '  ColumnChart.DataSource = ds.Tables(3)
                ' ColumnChart.DataBind()
            Else
                ' ColumnChart.Visible = False
            End If
        End If
    End Sub
    Public Function GET_LEAVE_DATA() As DataSet
        GET_LEAVE_DATA = Nothing
        Dim str_Sql As String
        Dim USR_NAME As String
        Dim ds As New DataSet
        Dim str_conn = ConnectionManger.GetOASISConnectionString

        If Request.QueryString("id") <> "" Then
            str_Sql = "select USR_NAME from USERS_M where USR_EMP_ID=" + Request.QueryString("id")
            USR_NAME = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        Else
            USR_NAME = Session("sUsr_name")
        End If
        Try
            Dim PARAM(3) As SqlParameter
            Dim EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(USR_NAME)

            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuId"))
            PARAM(1) = New SqlParameter("@EMP_ID", EMP_ID)
            PARAM(2) = New SqlParameter("@YEAR", ddlYear.SelectedValue)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_DASHBOARD_ESS_LEAVEHISTORY", PARAM)

            If dsDetails.Tables(0).Rows.Count > 0 Then
                hidChart1.Value = dsDetails.Tables(0).Rows(0)(1).ToString() + "||" + dsDetails.Tables(0).Rows(1)(1).ToString() + "||" + dsDetails.Tables(0).Rows(2)(1).ToString()
            End If
            Dim AnnlCon As String = "", Emercon As String= "", MedicCon As String = "", OtherCon As String = ""
            If dsDetails.Tables(3).Rows.Count > 0 Then
                For Each row As DataRow In dsDetails.Tables(3).Rows
                    If AnnlCon = "" Then
                        AnnlCon = row("Annual_Leave").ToString()
                    Else
                        AnnlCon = AnnlCon + "||" + row("Annual_Leave").ToString()
                    End If

                    If MedicCon = "" Then
                        MedicCon = row("Medical_Leave").ToString()
                    Else
                        MedicCon = MedicCon + "||" + row("Medical_Leave").ToString()
                    End If

                    If Emercon = "" Then
                        Emercon = row("Emergency_Leave").ToString()
                    Else
                        Emercon = Emercon + "||" + row("Emergency_Leave").ToString()
                    End If

                    If OtherCon = "" Then
                        OtherCon = row("Others").ToString()
                    Else
                        OtherCon = OtherCon + "||" + row("Others").ToString()
                    End If

                Next
            End If

            hidChart2.Value = AnnlCon + ";" + MedicCon + ";" + Emercon + ";" + OtherCon

            hidChart1.Value = hidChart1.Value
            Return dsDetails
        Catch ex As Exception

        End Try
    End Function
    Private Sub FillYear()
        Try
            Dim strSql As String
            Dim EMP_ID As String
            EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
            strSql = "SELECT DISTINCT CAST(YEAR(ELS_DTFROM) AS VARCHAR) + case when YEAR(ELS_DTFROM)<> YEAR(ELS_DTTO) THEN   '-' ELSE '' END  +  case when YEAR(ELS_DTFROM)<> YEAR(ELS_DTTO) THEN RIGHT(CAST(YEAR(ELS_DTTO) AS VARCHAR),2) ELSE '' END AS DESCR,YEAR(ELS_DTFROM)AS VALUE " & _
                "FROM EMPLEAVESCHEDULE_D WITH(NOLOCK) WHERE ELS_BSU_ID = '" & Session("sBsuid") & "' AND  ELS_EMP_ID='" & EMP_ID & "' ORDER BY YEAR(ELS_DTFROM) "
            ddlYear.DataSource = Mainclass.getDataTable(strSql, ConnectionManger.GetOASISConnectionString)
            ddlYear.DataTextField = "DESCR"
            ddlYear.DataValueField = "VALUE"
            ddlYear.DataBind()
            If ddlYear.Items.Count < 1 Then
                ddlYear.Items.Insert(0, " ")
                ddlYear.Items(0).Value = "0"
                ddlYear.SelectedValue = "0"
            End If
            If Not Request.QueryString("SelValue") Is Nothing Then
                If ddlYear.Items.FindByValue(Request.QueryString("SelValue").ToString) IsNot Nothing Then
                    ddlYear.SelectedValue = Request.QueryString("SelValue").ToString
                End If
            Else
                If ddlYear.Items.FindByValue(DateTime.Now.Year) IsNot Nothing Then
                    ddlYear.SelectedValue = DateTime.Now.Year
                Else
                    ddlYear.SelectedIndex = ddlYear.Items.Count - 1
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub RadGrid1_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles 


    'End Sub

    Protected Sub btnGO_Click(sender As Object, e As EventArgs) Handles btnGO.Click
        LoadData()
        gridbind()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadData()
        gridbind()
    End Sub

    Protected Sub RadGrid1_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If TypeOf e.Row.DataItem Is GridDataItem Then ' gets the row collection
            Dim item As GridDataItem = CType(e.Row.DataItem, GridDataItem)
            If item.Cells(2).Text.Trim.ToUpper = "TOTAL" Then
                'item.ForeColor = Drawing.Color.Blue
                item.Font.Size = FontUnit.Point(12)
                item.Font.Bold = True
                item.BackColor = Drawing.Color.LightBlue
            End If
            If item.Cells(2).Text.Trim.ToUpper = "BALANCE" Then
                item.ForeColor = Drawing.Color.White
                item.Font.Size = FontUnit.Point(12)
                item.Font.Bold = True
                item.BackColor = Drawing.ColorTranslator.FromHtml("#4472C4")
            End If
            If item.Cells(2).Text.Trim.ToUpper = "BALANCE CARRIED" Then
                item.ForeColor = Drawing.Color.DarkBlue
                item.Font.Size = FontUnit.Point(10)
                item.Font.Bold = True
                item.BackColor = Drawing.Color.LightBlue
            End If
        End If
    End Sub
End Class
