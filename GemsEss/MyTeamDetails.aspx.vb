﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Web.Script.Serialization
Imports System.Collections.Generic

Partial Class GemsEss_MyTeamDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindLeaveRequest()
            BindLeaveHistory()
            BindProfiles()
        End If
    End Sub
    Sub BindLeaveRequest()
        Try

            Dim ds As DataSet

            Dim str_Sql As String = "exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','Open',''"
            str_Sql = str_Sql & " exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','M',''"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable = ds.Tables(0)
            dt.Merge(ds.Tables(1), True)

            If dt.Rows.Count = 0 Then
                trLeaveHis.Visible = True
                lblMessage.Text = "No Data "
            Else
                trLeaveHis.Visible = False
                lblMessage.Text = ""
            End If

            rpt_LeaveRequest.DataSource = dt
            rpt_LeaveRequest.DataBind()


        Catch ex As Exception

        End Try
    End Sub
    Sub BindLeaveHistory()
        Try

            Dim ds As DataSet
            'SHOWING ONLY APPROVED IN HISTORY
            Dim str_Sql As String = "exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','A',''"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count = 0 Then
                trApprl.Visible = True
                lblMessage2.Text = "No Data "
            Else
                trApprl.Visible = False
                lblMessage2.Text = ""
            End If

            
            rptLeaveHistory.DataSource = dt
            rptLeaveHistory.DataBind()


        Catch ex As Exception

        End Try
    End Sub
    Sub BindProfiles()
        Try

            Dim ds As DataSet
            'SHOWING ONLY APPROVED IN HISTORY
            Dim str_Sql As String = "exec GET_IMMEDIATE_EMPLOYEES '" & Session("sUsr_name") & "'"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                lblMsg3.Text = "No Data"
            Else
                lblMsg3.Text = ""
            End If
            rpt_Profile.DataSource = dt
            rpt_Profile.DataBind()


        Catch ex As Exception

        End Try
    End Sub
#Region "WEB METHODS"
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetProfileInfo(ByVal emp_id As String) As String

        Dim ds As DataSet
        'SHOWING ONLY APPROVED IN HISTORY
        Dim str_Sql As String = "exec dbo.GET_EMP_BASIC_DETAILS '" & emp_id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        Dim ReturnString As String = DataSetToJSON(ds.Tables(0))
        Return ReturnString.Substring(1, ReturnString.Length - 2)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetProfileInfo2(ByVal emp_id As String) As String

        Dim ds As DataSet
        'SHOWING ONLY APPROVED IN HISTORY
        Dim str_Sql As String = "select EMP_DEPT_DESCR,CATEGORY_DESC,POS_DESCRIPTION,EMP_EGD_ID,EMP_BSU_JOINDT ,EMP_JOINDT from vw_OSO_EMPLOYEEMASTER where EMP_ID ='" & emp_id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        Dim str_sql2 As String = "EXEC GET_EM_GRADE_LEVEL '" & emp_id & "'"
        Dim grade_level As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql2)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ds.Tables(0).Columns.Add("GRADE_LEVEL")
                ds.Tables(0).Columns.Add("JOIN_DATE")
                ds.Tables(0).Columns.Add("BSU_JOIN_DATE")
                For Each row In ds.Tables(0).Rows
                    row("GRADE_LEVEL") = grade_level
                    row("BSU_JOIN_DATE") = Format(row("EMP_BSU_JOINDT"), "dd/MMM/yyyy")
                    row("JOIN_DATE") = Format(row("EMP_JOINDT"), "dd/MMM/yyyy")
                Next
            End If
        End If
        Dim ReturnString As String = DataSetToJSON(ds.Tables(0))
        Return ReturnString.Substring(1, ReturnString.Length - 2)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetProfileInfo3(ByVal emp_id As String) As String

        Dim ds As DataSet
        'SHOWING ONLY APPROVED IN HISTORY
        Dim str_Sql As String = "select EMD_CUR_MOBILE,EMD_EMAIL,EMD_Personal_EmailID,EMD_CONTACT_LOCAL,EMD_CONTACT_LOCAL_NUM,EMD_CONTACT_HOME,EMD_CONTACT_HOME_NUM from vw_OSO_EMPLOYEECONTACT where EMD_EMP_ID ='" & emp_id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)       
        Dim ReturnString As String = DataSetToJSON(ds.Tables(0))
        Return ReturnString.Substring(1, ReturnString.Length - 2)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetApprovalStatus(ByVal username As String, ByVal id As String) As String

        Dim ds As DataSet
        Dim ReturnString As String = ""
        'SHOWING ONLY APPROVED IN HISTORY
        Dim str_Sql As String = "exec GET_LEAVE_REQUEST_BY_ID '" & username.Trim() & "', '" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim Encr_decrData As New Encryption64
            Dim KEYS As String = Encr_decrData.Encrypt("PCDOCDOWNLOAD")
            Dim FILE As String = Encr_decrData.Encrypt(ds.Tables(0).Rows(0)("DOC_ID"))
            ds.Tables(0).Columns.Add("DOCUMENT")
            ds.Tables(0).Columns.Add("ORIGINAL_LEAVE")
            ds.Tables(0).Rows(0)("DOCUMENT") = "TYPE=" + KEYS + "&DocID=" + FILE

            ' If lblID.Text.StartsWith("M") Or ViewState("MainMnu_code") = "P170007" Then
            If ds.Tables(0).Rows(0)("APS_DOCTYPE") = "LEAVE-M" Then
                Dim myDT As New DataTable
                Dim sqlStr2 As String = "GetOriginalLeavePlanString '" & ds.Tables(0).Rows(0)("ACTUAL_ELA_ID") & "'"
                myDT = Mainclass.getDataTable(sqlStr2, ConnectionManger.GetOASISConnectionString)
                ds.Tables(0).Rows(0)("ORIGINAL_LEAVE") = myDT.Rows(0).Item("LeavePlan")
            Else
                ds.Tables(0).Rows(0)("ORIGINAL_LEAVE") = ""
            End If
            'DOC_ID,APS_DOCTYPE,APS_STATUS,EMP_NAME,ELT_DESCR,FROM_DATE,TO_DATE,LEAVEDAYS,ELA_REMARKS,EMP_ID,ELA_ELT_ID,ACTUAL_ELA_ID,APS_ID,ORIGINAL_LEAVE,ELM_CANCELLATION_REMARKS,DOCUMENT
            ReturnString = ds.Tables(0).Rows(0)("EMP_NAME").ToString + "||" + ds.Tables(0).Rows(0)("ELT_DESCR").ToString + "||" + ds.Tables(0).Rows(0)("FROM_DATE").ToString + "||" + ds.Tables(0).Rows(0)("TO_DATE").ToString + "||" + ds.Tables(0).Rows(0)("LEAVEDAYS").ToString + "||" +
            ds.Tables(0).Rows(0)("ELA_REMARKS").ToString + "||" + ds.Tables(0).Rows(0)("EMP_ID").ToString + "||" + ds.Tables(0).Rows(0)("ELA_ELT_ID").ToString + "||" + ds.Tables(0).Rows(0)("ACTUAL_ELA_ID").ToString + "||" + ds.Tables(0).Rows(0)("APS_ID").ToString + "||" + ds.Tables(0).Rows(0)("APS_DOCTYPE").ToString + "||" +
            ds.Tables(0).Rows(0)("APS_STATUS").ToString + "||" + ds.Tables(0).Rows(0)("DOC_ID").ToString + "||" + ds.Tables(0).Rows(0)("DOCUMENT").ToString + "||" + ds.Tables(0).Rows(0)("ORIGINAL_LEAVE").ToString + "||" + ds.Tables(0).Rows(0)("ELM_CANCELLATION_REMARKS").ToString
            Return ReturnString
        Else

            Return ReturnString = ""
        End If


        '   Dim ReturnString As String = DataSetToJSON(ds.Tables(0))
        ' Return ReturnString.Substring(1, ReturnString.Length - 2)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function SAVE_LEAVE_APPROVAL(ByVal bsu_id As String, ByVal username As String, ByVal EMP_ID As String, ByVal Fromdt As String, ByVal Todt As String, ByVal APS_ID As String, ByVal Actual_ELA_ID As String, ByVal ELT_ID As String, ByVal Remarks As String, ByVal ApprovalMode As String) As String
        Dim SaveMessage = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"

            SaveMessage = GetEmpLeaveCounts(bsu_id, Fromdt, Todt, ELT_ID, EMP_ID, ELT_ID, ApprovalMode)
            Dim isHRApproval As Boolean
            Dim ApprStatus As String = ApprovalMode
            'If ViewState("MainMnu_code") = "P170007" Then
            '    isHRApproval = True
            '    ApprStatus = "A"
            'Else
            '    isHRApproval = False
            'End If

            retval = SaveLEAVEAPPROVAL(bsu_id, EMP_ID, Remarks.Trim, APS_ID, ApprStatus, username, isHRApproval, stTrans, True)
            If retval = "0" Then
                stTrans.Commit()
                If ApprStatus = "A" Then
                    SaveMessage = "Selected request has been Approved"
                ElseIf ApprStatus = "R" Then
                    SaveMessage = "Selected request has been Rejected"
                ElseIf ApprStatus = "H" Then
                    SaveMessage = "Selected request has been set to Hold"
                End If
                '*********************************NEED TO ENABLE THIS FOR THE WEB METHOD************************************************************************************************************************
                'Dim flagAudit As Integer = UtilityObj.operOnAudiTable(CType(Page.Master, Object).MenuName, EMP_ID & "-" & Remarks.Trim & "-" & APS_ID & "-" & _
                'ApprovalMode, "Insert", Page.User.Identity.Name.ToString, Me.Page)
                'If flagAudit <> 0 Then
                '    Throw New ArgumentException("Could not process your request")
                'End If
            Else
                stTrans.Rollback()
                SaveMessage = getErrorMessage("1000")
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Return SaveMessage
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GET_LEAVE_PLAN(ByVal bsu_id As String, ByVal username As String, ByVal Fromdt As String, ByVal Todt As String, ByVal Showall As String) As String

        Dim str_conn = ConnectionManger.GetOASISConnectionString
        'Dim txtFrom As String = "01/Jan/2016"
        'Dim txtTo As String = "05/Feb/2016"
        Try
            Dim PARAM(7) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", bsu_id)
            PARAM(1) = New SqlParameter("@startDate", CDate(Fromdt))
            PARAM(2) = New SqlParameter("@endDate", CDate(Todt))
            PARAM(3) = New SqlParameter("@APS_ID", 0)
            PARAM(4) = New SqlParameter("@username", username)
            If Showall = "True" Then
                PARAM(5) = New SqlParameter("@ShowAll", 1)
            Else
                PARAM(5) = New SqlParameter("@ShowAll", 0)
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.[GetLeavePlanNew_TEST]", PARAM)

            Dim ReturnString As String = DataSetToJSON(ds.Tables(0))
            Return ReturnString '.Substring(1, ReturnString.Length - 2)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Shared Function DataSetToJSON(ByVal dt As DataTable) As String
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)
        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()
            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName, dr(col))
            Next
            rows.Add(row)
        Next
        Return serializer.Serialize(rows)
    End Function



#End Region

    Public Shared Function GetEmpLeaveCounts(ByVal bsu_id As String, ByVal Fromdt As String, ByVal Todt As String, ByVal ELT_ID As String, ByVal EmPID As Integer, ByVal Leavetype As String, ByVal ApprovalMode As String) As String
        Dim SaveMessage = ""
        Try
            Dim rowId As String = ""
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = EmPID

            pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(1).Value = Today.Date()

            pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
            pParms(2).Value = Leavetype '"AL" 'lblELT_ID.Text

            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = bsu_id

            pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
            pParms(4).Value = False

            pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
            pParms(5).Value = True

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)

            End Using

            Dim intLeaveBalnce, days As Integer
            If ds.Tables(0).Rows.Count > 0 Then
                intLeaveBalnce = ds.Tables(0).Rows(0).Item(3)
            End If
            '---------- check leave balance---------------------
            Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(Fromdt) & "','" & CDate(Todt) & "','" & bsu_id & "'," & EmPID & ",'" & ELT_ID & "') as days "
            Dim ds2 As New DataSet
            ds2 = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
            days = ds2.Tables(0).Rows(0).Item("days")
            If ApprovalMode = "A" Then
                If (days > intLeaveBalnce) Then
                    SaveMessage = getErrorMessage("1123")
                End If

            Else
                SaveMessage = ""
            End If

        Catch ex As Exception

            SaveMessage = ex.Message
        End Try
        Return SaveMessage
    End Function
    Public Shared Function SaveLEAVEAPPROVAL(ByVal p_BSU_ID As String, ByVal p_EMP_ID As String, _
   ByVal p_REMARKS As String, ByVal p_APS_ID As Integer, ByVal p_Status As String, _
   ByVal p_USER As String, ByVal isHRApproval As Boolean, ByVal p_stTrans As SqlTransaction, Optional ByVal p_DocCheck As Boolean = 0) As String
        Try
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = p_BSU_ID
            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = p_EMP_ID
            pParms(2) = New SqlClient.SqlParameter("@REMARKS", SqlDbType.VarChar, 200)
            pParms(2).Value = p_REMARKS
            pParms(3) = New SqlClient.SqlParameter("@APS_ID", SqlDbType.Int)
            pParms(3).Value = p_APS_ID
            pParms(4) = New SqlClient.SqlParameter("@STATUS", SqlDbType.VarChar, 1)
            pParms(4).Value = p_Status
            pParms(5) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 20)
            pParms(5).Value = p_USER
            pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            pParms(7) = New SqlClient.SqlParameter("@IsHRApproval", SqlDbType.Bit)
            pParms(7).Value = isHRApproval
            pParms(8) = New SqlClient.SqlParameter("@IsDocVerified", SqlDbType.Bit)
            pParms(8).Value = p_DocCheck
            Dim retval As Integer
            retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "LEAVEAPPROVAL", pParms)
            If pParms(6).Value = "0" Then
                SaveLEAVEAPPROVAL = pParms(6).Value
            Else
                SaveLEAVEAPPROVAL = "1000"
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            SaveLEAVEAPPROVAL = "1000"
        End Try
    End Function





End Class
