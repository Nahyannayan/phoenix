﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Drawing

Partial Class GemsEss_FeeConcessionPopup_ESS
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            Try
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'Dim Employee_ID As String = Request.QueryString("id")
                Dim Employee_ID As String = Mainclass.cleanString(Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")))

                If Employee_ID Is Nothing Then
                    Employee_ID = Session("ESS_FEE_CON_EMP_ID")
                Else
                    Session("ESS_FEE_CON_EMP_ID") = Employee_ID
                End If
                gridDependantDetails(Employee_ID)  '' Bindng dependant details section
                Dim ds As DataSet = GetEMP_Basic_Details()
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("EMP_BSU_COUNTRY_ID") = ds.Tables(0).Rows(0)("BSU_COUNTRY_ID").ToString().Trim()
                        h_bsu_cty_id.Value = ds.Tables(0).Rows(0)("BSU_COUNTRY_ID").ToString().Trim()
                        If Session("EMP_BSU_COUNTRY_ID").ToString().Trim() = "172" Then
                            spanVisFileNoCheck.Visible = True
                            spanAddVisFileNoCheck.Visible = True
                        Else : spanVisFileNoCheck.Visible = False
                            spanAddVisFileNoCheck.Visible = False
                        End If
                    End If
                End If

                Dim dt As DataTable = GET_EMPLOYEE_DETAILS(1, Session("sBsuid"), Employee_ID)
                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then
                        LBL_EMP_NAME.Text = dt.Rows(0)("EMPLOYEE_NAME")
                        LBL_DPT.Text = dt.Rows(0)("DPT_DESCR")
                        LBL_EMP_NO.Text = dt.Rows(0)("EMPNO")
                        LBL_EMP_DOJ.Text = dt.Rows(0)("JOIN_DATE")
                        LBL_EMP_DESIG.Text = dt.Rows(0)("DESIGNATION")
                        imgEmpImage.ImageUrl = dt.Rows(0)("IMG_URL")
                        LBLGRPDOJ.Text = dt.Rows(0)("GRP_JOIN_DATE")
                    End If
                End If

                Gridbind_StuDetails(Employee_ID)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                'lblError.Text = "Request could not be processed "
                usrMessageBar.ShowNotification("Request could not be processed!!", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        End If
    End Sub

    Sub gridDependantDetails(ByVal Employee_ID As String)

        Dim mtable As DataTable
        mtable = EOS_EmployeeDependant.GetChildDetailList(Employee_ID)

        'If mtable.Rows.Count > 0 Then
        rptrChildList.DataSource = mtable
        rptrChildList.DataBind()
        'End If

    End Sub
    Sub Gridbind_StuDetails(ByVal Employee_ID As String)
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))      
            'Dim Employee_ID As String = Request.QueryString("id")

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Employee_ID)

            If True Then
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 1)
            Else
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 2)
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[DBO].[GET_ESS_STUDENT_INFO]", param)

            If ds.Tables(0).Rows.Count > 0 Then
                repInfo.Visible = True
            Else
                repInfo.Visible = False
            End If

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception
            repInfo.DataBind()

        End Try
    End Sub

    Public Shared Function GetDependantDetailList(ByVal EDD_ID As Integer) As DataTable
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim sqlParam(0) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EDD_EMP_ID", EDD_ID, SqlDbType.Int)
            mTable = Mainclass.getDataTable("EOS_GetEmpDependantDetailList", sqlParam, str_conn)
            Return mTable
        Catch ex As Exception
            'Errorlog(ex.Message)
            Throw ex
        End Try
    End Function

    Public Shared Function GetEmployeeDetails(ByVal EMP_ID As String) As DataTable

        Dim str_Sql As String = "SELECT CONCAT(EMP_SALUTE,EMP_FNAME,' ',EMP_MNAME,' ',EMP_LNAME) AS EMP_NAME FROM [OASIS].[DBO].EMPLOYEE_M WHERE  EMP_ID ='" & EMP_ID & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0)
    End Function

    Public Shared Function GET_EMPLOYEE_DETAILS(ByVal OPTIONS As Integer, ByVal BSU_ID As String, Optional ByVal EMP_ID As String = "") As DataTable
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(1).Value = BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = EMP_ID


        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnection, _
          CommandType.StoredProcedure, "[OASIS].DBO.GET_ESS_EMPLOYEE_DETAILS", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub rptrChildList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)

    End Sub

#Region "SHARED FUNCTION"

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function ApplyConcession(ByVal STU_NO As String, ByVal EMP_ID As String) As String
        Dim returnstring As String = ""
        Try
            Dim retval As String = ""
            retval = EOS_EmployeeDependant.ApplyConcession(STU_NO, EMP_ID, HttpContext.Current.Session("sUsr_id"))
            If (retval = "0" Or retval = "") Then
                returnstring = "0" + "||" + "Successfully applied for Fee Concession      "
                'usrMessageBar.ShowNotification("Successfully applied for Fee Concession", UserControls_usrMessageBar.WarningType.Success)
            ElseIf retval = "90101" Then
                returnstring = retval + "||" + "Concession request already exists for this student"
            Else
                returnstring = retval + "||" + "Error occured!!"
                'usrMessageBar.ShowNotification("Error occured!!", UserControls_usrMessageBar.WarningType.Danger)

            End If
        Catch ex As Exception
            returnstring = "1000" + "||" + ex.Message

        End Try
        Return returnstring

    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function CallGrid() As String
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))      
            Dim Employee_ID As String = HttpContext.Current.Request.QueryString("id")

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(1) = New SqlClient.SqlParameter("@EMP_ID", Employee_ID)

            If True Then
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 1)
            Else
                param(0) = New SqlClient.SqlParameter("@OPTIONS", 2)
            End If

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[DBO].[GET_ESS_STUDENT_INFO]", param)

            'If ds.Tables(0).Rows.Count > 0 Then
            '    repInfo.Visible = True
            'Else
            '    repInfo.Visible = False
            'End If

            'repInfo.DataSource = ds.Tables(0)
            'repInfo.DataBind()

        Catch ex As Exception
            'repInfo.DataBind()

        End Try
        Return ""
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function DeleteDependantDetails(ByVal EDDID As String) As String
        Dim returnstring As String = ""
        Try
            Dim retval As String
            retval = EOS_EmployeeDependant.DeleteDependantDetail(EDDID)
            If (retval = "0" Or retval = "") Then
                returnstring = "0" + "||" + "Data Deleted Successfully "
            Else
                returnstring = retval + "||" + "Error occured!!"

            End If
        Catch ex As Exception
            returnstring = "1000" + "||" + ex.Message

        End Try
        Return returnstring

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function a(ByVal Id As String) As String
        Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
        EDD_Detail = EOS_EmployeeDependant.GetDependantDetail(Id)
        Dim ReturnString As String = EDD_Detail.EDD_RELATION + "||" + Format(EDD_Detail.EDD_DOB, "dd/MMM/yyyy") + "||" + EDD_Detail.EDC_Gender + "||" + EDD_Detail.EDD_MARITALSTATUS.ToString + "||" +
            IIf(EDD_Detail.EDD_BSU_NAME = "0", "", EDD_Detail.EDD_BSU_NAME) + "||" + EDD_Detail.EDD_NAME + "||" + EDD_Detail.CTY_NATIONALITY + "||" + EDD_Detail.EDD_PASSPRTNO + "||" + EDD_Detail.EDC_DOCUMENT_NO + "||" + Format(DateTime.Parse(EDD_Detail.EDC_EXP_DT), "dd/MMM/yyyy") + "||" +
            EDD_Detail.EDD_UIDNO + "||" + Format(EDD_Detail.EDD_VISAISSUEDATE, "dd/MMM/yyyy") + "||" + Format(EDD_Detail.EDD_VISAEXPDATE, "dd/MMM/yyyy") + "||" + EDD_Detail.EDD_VISAISSUEPLACE + "||" + EDD_Detail.EDD_VISASPONSOR.ToString + "||" + EDD_Detail.EDD_INSURANCENUMBER + "||" + EDD_Detail.EDD_GEMS_TYPE + "||" + EDD_Detail.EDD_CTY_ID.ToString + "||" + EDD_Detail.EDD_GEMS_ID + "||" + EDD_Detail.EDD_BSU_ID + "||" + EDD_Detail.EDD_GEMS_ID + "||" + EDD_Detail.EDD_FILENO + "||" + EDD_Detail.EDD_RESIDENCE_CTY_ID.ToString + "||" + EDD_Detail.EDD_GEMS_STAFF_STU_NO 'last two params Added by vikranth on 4th mar 2020
        Return ReturnString
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function UpdateProfileDetails(ByVal bsuid As String, ByVal usrName As String, ByVal Name As String, ByVal empno As String, ByVal txtAddEID As String, ByVal txtEIDExpDt As String, ByVal txtAddUIDNo As String, ByVal PassportNO As String, ByVal PassportIssuePlc As String, ByVal PassportIsDt As String, ByVal PassportExpDt As String, ByVal AlEmail As String, ByVal txtAddVisaIssuePlc As String, ByVal txtAddVisaIssueDt As String, ByVal txtAddVisaExpDt As String, ByVal slctAddVisaSpnsr As String, ByVal txtAddInsCrdNo As String, ByVal Emirate As String, ByVal Area As String, ByVal Phone As String, ByVal EmergencyContactLocal As String, ByVal EmergencyContactHome As String, ByVal EmergencyPhoneLocal As String, ByVal EmergencyPhoneHome As String) As String
        Dim callTransaction As String = "0"
        Dim tran As SqlTransaction
        Dim errormsg As String = ""

        Try


            If txtAddEID <> "" Then

            Else
                If bsuid <> "800444" AndAlso bsuid <> "800111" Then
                    errormsg = "Please Enter Emirates Id"
                    callTransaction = "-1"
                End If
            End If


            If txtEIDExpDt = "" Then
                If bsuid <> "800444" AndAlso bsuid <> "800111" Then
                    errormsg = "Please Enter Emirates Id Expiry Date !!!!"
                    callTransaction = "-1"
                End If
            End If

            If PassportNO = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter Passport Number !!!!"
                    callTransaction = "-1"

                End If

            End If

            If PassportIssuePlc = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter Passport Issued Place !!!!"

                    callTransaction = "-1"

                End If
            End If

            If PassportIsDt = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter Passport Issued Date !!!!"

                    callTransaction = "-1"

                End If
            End If

            If PassportExpDt = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter Passport Expiry Date !!!!"

                    callTransaction = "-1"

                End If
            End If

            If txtAddUIDNo = "" Then
                If bsuid <> "800444" AndAlso bsuid <> "800111" AndAlso Emirate <> "172" Then
                    errormsg = "Please Enter UID Number!!!!"

                    callTransaction = "-1"

                End If
            End If

            If txtAddVisaIssueDt = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter VISA Issued Date!!!!"

                    callTransaction = "-1"

                End If
            End If

            If txtAddVisaExpDt = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter VISA Expiry Date!!!!"

                    callTransaction = "-1"

                End If
            End If
            If txtAddVisaIssuePlc = "" Then
                If Emirate <> "172" Then
                    errormsg = "Please Enter VISA Issued Place!!!!"

                    callTransaction = "-1"

                End If
            End If

            If AlEmail = "" Then
                errormsg = "Please Enter Alternate Email!!!!"

                callTransaction = "-1"

            End If

            If Phone = "" Then
                errormsg = "Please Enter Phone Nnumber!!!!"

                callTransaction = "-1"

            End If

            If slctAddVisaSpnsr = "" Or slctAddVisaSpnsr = "0" Or slctAddVisaSpnsr Is Nothing Then
                errormsg = "Please Enter Sponser Details !!!!"

                callTransaction = "-1"

            End If

            If Emirate = "0" Or Emirate = "" Then
                errormsg = "Please Enter Emirates !!!!"

                callTransaction = "-1"

            End If

            If Area = "0" Or Area = "" Then
                errormsg = "Please Enter Area !!!!"

                callTransaction = "-1"

            End If

            If txtAddInsCrdNo = "" Then
                errormsg = "Please Enter Insurance Number !!!!"

                callTransaction = "-1"

            End If

            If (EmergencyContactHome = "" Or EmergencyPhoneHome = "") Then
                errormsg = "Please Enter Emergency Contact(home) Details !!!!"

                callTransaction = "-1"

            End If

            If EmergencyContactLocal = "" Or EmergencyPhoneLocal = "" Then
                errormsg = "Please Enter Emergency Contact(Local) Details!!!!"

                callTransaction = "-1"

            End If

            If (callTransaction = "0") Then
                Using CONN As SqlConnection = ConnectionManger.GetOASISConnection
                    tran = CONN.BeginTransaction("SampleTransaction")
                    Try

                        Dim param(18) As SqlParameter
                        param(0) = New SqlParameter("@EMP_ID", EOS_MainClass.GetEmployeeIDFromUserName(usrName))
                        param(1) = New SqlParameter("@UID_NO", txtAddUIDNo)
                        param(2) = New SqlParameter("@EID_NO", txtAddEID)
                        param(3) = New SqlParameter("@EID_EXP_DATE", txtEIDExpDt)
                        param(4) = New SqlParameter("@VISA_ISSU_PLACE", txtAddVisaIssuePlc)
                        param(5) = New SqlParameter("@VISA_ISSUE_DATE", txtAddVisaIssueDt)
                        param(6) = New SqlParameter("@VIS_EXP_DATE", txtAddVisaExpDt)
                        param(7) = New SqlParameter("@Email", AlEmail)
                        param(8) = New SqlParameter("@cur_mobile", Phone)
                        param(9) = New SqlParameter("@PassportNo", PassportNO)
                        param(10) = New SqlParameter("@PPIssuedPlace", PassportIssuePlc)
                        param(11) = New SqlParameter("@PPIssueDate", PassportIsDt)
                        param(12) = New SqlParameter("@PPEXpiry", PassportExpDt)
                        param(13) = New SqlParameter("@VISASPONSOR", Convert.ToInt32(slctAddVisaSpnsr))
                        param(14) = New SqlParameter("@EMPEMIRATE", Convert.ToInt32(Emirate))
                        param(15) = New SqlParameter("@EMPAREA", Convert.ToInt32(Area))
                        param(16) = New SqlParameter("@INSUR_NO", txtAddInsCrdNo)
                        param(17) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        param(17).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.Update_EMP_VISA_EID_INFO", param)
                        Dim ReturnFlag As Integer = param(17).Value

                        If ReturnFlag = -1 Then
                            callTransaction = "-1"
                            errormsg = "Record cannot be saved. !!!"
                        ElseIf ReturnFlag <> 0 Then
                            callTransaction = "1"
                            errormsg = "Error occured while processing info !!!"
                        Else
                            'ViewState("GCM_ID") = "0"
                            ' ViewState("datamode") = "none"
                            callTransaction = "0"
                            Dim params(5) As SqlParameter
                            params(0) = New SqlParameter("@emp_id", EOS_MainClass.GetEmployeeIDFromUserName(usrName))
                            params(1) = New SqlParameter("@homenum", EmergencyPhoneHome)
                            params(2) = New SqlParameter("@homecont", EmergencyContactHome)
                            params(3) = New SqlParameter("@localnum", EmergencyPhoneLocal)
                            params(4) = New SqlParameter("@localcont", EmergencyContactLocal)
                            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.SAVE_EMERGENCY_FORPROFILE", params)
                            callTransaction = "0"
                            'resetall()
                        End If
                    Catch ex As Exception
                        callTransaction = "1"
                        errormsg = ex.Message
                    Finally
                        If callTransaction = "-1" Then
                            errormsg = "Record cannot be saved. !!!"
                            'UtilityObj.Errorlog(errormsg)
                            tran.Rollback()
                        ElseIf callTransaction <> "0" Then
                            errormsg = "Error occured while saving !!!"
                            'UtilityObj.Errorlog(errormsg)
                            tran.Rollback()
                        Else
                            ' errormsg = ""
                            tran.Commit()
                        End If

                    End Try
                End Using
            End If
            Return callTransaction + "||" + errormsg
        Catch ex As Exception
            Return "-1" + "||" + "Erro"
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCountry(ByVal prefix As String) As String()
        GetCountry = Nothing
        Dim County As New List(Of String)()
        Dim str_Sql As String = ""
        If prefix <> "" Then
            str_Sql = "Select ID, E_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as E_Name FROM COUNTRY_M)a where a.ID<>'' and E_Name <> '-' and a.ID not in (0,5,252) and a.E_Name like '%" + prefix + "%'"
        Else : str_Sql = "Select ID, E_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as E_Name FROM COUNTRY_M)a where a.ID<>'' and E_Name <> '-' and a.ID not in (0,5,252) order by E_Name ASC"
        End If

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql '& str_orderby
                'cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        County.Add(String.Format("{0}||{1}", sdr("E_Name"), sdr("ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return County.ToArray()
        End Using
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetStudents(ByVal Type As String, ByVal prefix As String, ByVal COMP_ID As String, ByVal ACD_ID As String, ByVal SearchFor As String) As String()
        If (Type = "Gems Student") Then
            Return GetStudentComp(COMP_ID, ACD_ID, SearchFor, prefix)
        ElseIf (Type = "Gems Staff") Then
            Return GetEmployee(COMP_ID, ACD_ID, SearchFor, prefix)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetEmployee(ByVal COMP_ID As String, ByVal ACD_ID As String, ByVal SearchFor As String, ByVal prefix As String) As String()
        GetEmployee = Nothing
        Dim Employee As New List(Of String)()
        Dim str_Sql As String = ""
        If SearchFor = "NAME" Then
            str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M )a where a.bActive=1 and a.BSU='" & COMP_ID & "' and a.id<>'' and a.E_name like '%" + prefix + "%'"
        ElseIf SearchFor = "ID" Then
            str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M )a where a.bActive=1 and a.BSU='" & COMP_ID & "' and a.id<>'' and a.NO like '%" + prefix + "%'"
        End If


        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql '& str_orderby
                'cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If SearchFor = "ID" Then
                            Employee.Add(String.Format("{0}||{1}||{2}||{3}||{4}", sdr("No"), sdr("ID"), sdr("E_Name"), "", ""))
                        ElseIf SearchFor = "NAME" Then
                            Employee.Add(String.Format("{0}||{1}||{2}||{3}||{4}", sdr("E_Name"), sdr("ID"), sdr("No"), "", ""))
                        End If
                    End While
                End Using
                conn.Close()
            End Using
            Return Employee.ToArray()
        End Using

    End Function

    Public Shared Function GetStudentComp(ByVal COMP_ID As String, ByVal ACD_ID As String, ByVal SearchFor As String, ByVal prefix As String) As String()
        GetStudentComp = Nothing
        Dim Student As New List(Of String)()
        Dim str_Sql As String = ""
        str_Sql = " SELECT TOP 20 STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER,REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE," _
                 & " PARENT_NAME ,STU_ACD_ID, ACY_DESCR, STU_GRD_ID FROM VW_OSO_STUDENT_M  WHERE" _
                 & " CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN' and STU_bActive=1 AND " _
                 & "  STU_BSU_ID='" & COMP_ID & "' "

        If SearchFor IsNot Nothing Then
            If SearchFor = "ID" Then
                str_Sql &= " AND STU_NO LIKE '%" & prefix & "%' "
                ' str_orderby = " ORDER BY a.STU_NO "
            ElseIf SearchFor = "NAME" Then
                str_Sql &= " AND STU_NAME LIKE '%" & prefix & "%' "
                ' str_orderby = " ORDER BY LTRIM(RTRIM(a.STU_NAME)) "
            End If
        End If

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql '& str_orderby

                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If SearchFor = "ID" Then
                            Student.Add(String.Format("{0}||{1}||{2}||{3}", sdr("STU_NO"), sdr("STU_ID"), sdr("STU_NAME"), sdr("STU_GRD_ID")))
                        ElseIf SearchFor = "NAME" Then
                            Student.Add(String.Format("{0}||{1}||{2}||{3}", sdr("STU_NAME"), sdr("STU_ID"), sdr("STU_NO"), sdr("STU_GRD_ID")))
                        End If
                    End While
                End Using
                conn.Close()
            End Using
            Return Student.ToArray()
        End Using

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCompany(ByVal prefix As String, ByVal userName As String, ByVal SearchFor As String) As String()
        Dim company As New List(Of String)()

        Dim ds As New DataSet
        Dim str_Sql As String = " exec [dbo].[GET_ESS_SCHOOLS] '" & 1 & "','" & SearchFor & "','" & prefix & "'"
        Try
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        Catch ex As Exception

        End Try

        '  If (prefix <> "") Then
        '      If SearchFor = "BSU_NAME" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID as ID,BSU_NAME as E_Name,BSU_CODE AS CODE FROM OASIS_FIM.FIM.BSU_M UNION SELECT BSU_ID as ID,BSU_NAME as E_Name,BSU_SHORTNAME AS CODE  FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN ('999998'    ,'900007'    ,'800075'    ,'900024'    ,'900020'    ,'900001'    ,'800030'    ,'900010'    ,'888889'    ))A where a.ID<>''  and (E_Name like '%" + prefix + "%' OR CODE LIKE '%" + prefix + "%')")
        '      ElseIf SearchFor = "BSU_ID" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID as ID,BSU_NAME as E_Name FROM OASIS_FIM.FIM.BSU_M UNION SELECT BSU_ID as ID,BSU_NAME as E_Name  FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN ('999998'    ,'900007'    ,'800075'    ,'900024'    ,'900020'    ,'900001'    ,'800030'    ,'900010'    ,'888889'    ))A where a.ID<>''  and ID like '%" + prefix + "%'")
        '      ElseIf prefix = "S" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID AS ID, BSU_NAME AS E_Name,BSU_SHORTNAME FROM   OASIS..BUSINESSUNIT_M WHERE  BSU_SHORTNAME  NOT IN ('GVS','XXX','GEP','OOF','MHN','GDF','LLN','ADJ','GSA','PRS') AND ISNULL(BSU_bGEMSSCHOOL ,0)=1 AND BSU_NAME NOT LIKE 'DREAM%'  AND ISNULL(BSU_Bschool ,0)=1)A where a.ID<>'' ORDER By E_Name ASC")
        '      ElseIf prefix = "E" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID AS ID, BSU_NAME AS E_Name,BSU_SHORTNAME FROM   OASIS..BUSINESSUNIT_M WHERE  BSU_SHORTNAME  NOT IN ('GVS','XXX','GEP','OOF','MHN','GDF','LLN','ADJ','GSA','PRS') AND ISNULL(BSU_bGEMSSCHOOL ,0)=1 AND BSU_NAME NOT LIKE 'DREAM%')A where a.ID<>'' ORDER By E_Name ASC")

        '      End If
        '  Else

        '  End If

        If ds.Tables(0).Rows.Count > 0 Then
            For Each Row In ds.Tables(0).Rows
                company.Add(String.Format("{0}||{1}", Row("E_Name"), Row("ID")))
            Next
        Else

        End If

        Return company.ToArray()
        'End Using
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GetDependentName(ByVal Type As String, ByVal Dob As String, ByVal StaffIdORStuId As String, ByVal BSU_ID As String) As String()
        Try
            Dim str_Sql As String = ""
            If (Type = "Gems Student") Then
                str_Sql = "select isnull(STU_firstname,'')+' '+isnull(STU_midname,'')+' '+isnull(STU_lastname,'') Depdent_NAME, STU_ID As Depedent_Id,CASE WHEN STU_GENDER='M' THEN 'M' ELSE 'F' END As Gender,1 AS MaritalStatus  " _
                        & "From Student_m where STU_NO='" & StaffIdORStuId & "' AND STU_BSU_ID='" & BSU_ID & "' AND STU_DOB='" & Dob & "' AND STU_CURRSTATUS='EN'" 'STU_bActive=1"

            ElseIf (Type = "Gems Staff") Then
                str_Sql = "SELECT isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'')  as Depdent_NAME, Emp_ID As Depedent_Id,CASE WHEN EMP_SEX_bMALE=0 THEN 'F' ELSE 'M' END AS Gender,EMP_MARITALSTATUS AS MaritalStatus FROM EMPLOYEE_M " _
                        & "WHERE EmpNo='" & StaffIdORStuId & "' and EMP_BSU_ID='" & BSU_ID & "' and EMP_DOB='" & Dob & "' AND EMP_bACTIVE=1"
            End If
            GetDependentName = Nothing
            Dim Dependent As New List(Of String)()
            Using conn As New SqlConnection()
                conn.ConnectionString = ConnectionManger.GetOASISConnectionString
                Using cmd As New SqlCommand()
                    cmd.CommandText = str_Sql '& str_orderby

                    cmd.Connection = conn
                    conn.Open()
                    Using sdr As SqlDataReader = cmd.ExecuteReader()
                        While sdr.Read()
                            Dependent.Add(String.Format("{0}||{1}||{2}||{3}", sdr("Depdent_NAME"), sdr("Depedent_Id"), sdr("Gender"), sdr("MaritalStatus")))
                        End While
                    End Using
                    conn.Close()
                End Using
                Return Dependent.ToArray()
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function AddSaveDependantDetails(ByVal EDDID As String, ByVal bsuid As String, ByVal usrName As String, Name As String, ByVal DOBDate As String, ByVal Relation As String, ByVal StuID As String, ByVal PassportNo As String, ByVal UIDNo As String, ByVal VisaIssueDt As String, ByVal VisaExpDate As String, ByVal VisaIssuePlc As String, ByVal EIDExpryDt As String, ByVal Gender As String, ByVal MarStatus As String, ByVal DependMStatus As String, ByVal CityID As String, ByVal Nationality As String, ByVal Sponser As String, ByVal InsuranceNumber As String, ByVal EIDNo As String, ByVal GemsType As String, ByVal Unit As String, ByVal EmployeeID As String, ByVal Filepath As String, ByVal Visafilenumber As String, ByVal ResindenceCountryId As String, ByVal GemsStaffIdorStuId As String) As String 'ByVal Filepath As String
        ' Dim Filepath As String = ""
        Try
            Dim DOBDt As Date
            Dim EIDExpiryDate As Date
            Dim visaissuedate As Date
            Dim visaexpdt As Date

            Try
                If Not (DOBDate = "") And Not (DOBDate Is Nothing) Then
                    DOBDt = DateTime.Parse(DOBDate.Trim())
                End If
                If Not (EIDExpryDt = "") And Not (EIDExpryDt Is Nothing) Then
                    EIDExpiryDate = DateTime.Parse(EIDExpryDt.Trim())
                End If
                If Not (VisaIssueDt = "") And Not (VisaIssueDt Is Nothing) Then
                    visaissuedate = DateTime.Parse(VisaIssueDt.Trim())
                End If
                If Not VisaExpDate = "" And Not VisaExpDate Is Nothing Then
                    visaexpdt = DateTime.Parse(VisaExpDate.Trim())
                End If
            Catch ex As Exception

            End Try

            If Relation = "" Or Relation = "-1" Or Relation Is Nothing Then
                Return "1000" + "||" + "Please Select Relation"
            ElseIf DOBDate = "" Or DOBDate Is Nothing Then
                Return "1000" + "||" + "Please Enter Date of Birth"
            ElseIf (DOBDt > Date.Today) Then
                Return "1000" + "||" + "Date of Birth cannot be Future Date."
            ElseIf Gender = "" Or Gender = "0" Or Gender Is Nothing Then
                Return "1000" + "||" + "Please Select Gender"
            ElseIf MarStatus = "" Or MarStatus = "-1" Or MarStatus Is Nothing Then
                Return "1000" + "||" + "Please Select Marital Status"
            ElseIf GemsType = "" Or GemsType = "undefined" Or GemsType Is Nothing Then
                Return "1000" + "||" + "Please Select Dependent Type"
            ElseIf GemsType <> "Non Gems" And (bsuid = "" Or bsuid = "0" Or bsuid Is Nothing) Then
                If Unit = "" Then
                    Return "1000" + "||" + "Please Select Business unit"
                Else
                    Return "1000" + "||" + "Incorrect Business Unit is selected"
                End If
            ElseIf (GemsType <> "Non Gems") And ((EmployeeID = "" Or EmployeeID = "0" Or EmployeeID Is Nothing) And GemsType = "Gems Staff") Then
                If Name = "" Then
                    'Return "1000" + "||" + "Please Select employee from the institute you have selected"
                    Return "1000" + "||" + "Please Enter Name"
                Else
                    Return "1000" + "||" + "Incorrect Staff Name"
                End If
            ElseIf (GemsType <> "Non Gems") And ((StuID = "" Or StuID = "0" Or StuID Is Nothing) And GemsType = "Gems Student") Then
                If Name = "" Then
                    'Return "1000" + "||" + "Please Select  student from the institute you have selected"
                    Return "1000" + "||" + "Please Enter Name"
                Else
                    Return "1000" + "||" + "Incorrect Student Name"
                End If
            ElseIf Name = "" Or Name Is Nothing Then
                Return "1000" + "||" + "Please Enter Name"
            ElseIf CityID = "" Or CityID = "0" Or CityID Is Nothing Then
                If Nationality = "" Then
                    Return "1000" + "||" + "Please Select Nationality"
                Else
                    Return "1000" + "||" + "Incorrect Nationality is selected"
                End If
                'ElseIf PassportNo = "" Or PassportNo Is Nothing Then
                '    Return "1000" + "||" + "Please Enter Passport Number"
            ElseIf ResindenceCountryId = "" Or ResindenceCountryId = "0" Or ResindenceCountryId Is Nothing Then
                Return "1000" + "||" + "Please Enter Residence Country"
            ElseIf Not ResindenceCountryId Is Nothing AndAlso ResindenceCountryId <> "" AndAlso ResindenceCountryId = "172" Then
                If EIDNo = "" Or EIDNo Is Nothing Then
                    Return "1000" + "||" + "Please Enter Emirates ID number"
                ElseIf EIDExpryDt = "" Or EIDExpryDt Is Nothing Then
                    Return "1000" + "||" + "Please Enter Emirates ID Expiry Date"
                ElseIf UIDNo = "" Or UIDNo Is Nothing Then
                    Return "1000" + "||" + "Please Enter UID Number"
                ElseIf VisaIssueDt = "" Or VisaIssueDt Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Issue Date"
                ElseIf visaissuedate > Date.Today Then
                    Return "1000" + "||" + "Visa Issue Date cannot be Future Date"
                ElseIf VisaExpDate = "" Or VisaExpDate Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Expiry Date"
                ElseIf (visaissuedate > visaexpdt) Then
                    Return "1000" + "||" + "Visa Expiry Date is Lesser than Visa Issue Date"
                ElseIf VisaIssuePlc = "" Or VisaIssuePlc Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Issue Place"
                ElseIf Sponser = "" Or Sponser = "0" Or Sponser Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Sponsor Type"
                    'ElseIf InsuranceNumber = "" Or InsuranceNumber Is Nothing Then
                    '    Return "1000" + "||" + "Please Enter Insurance Number"
                    'ElseIf HttpContext.Current.Session("EMP_BSU_COUNTRY_ID") = "172" Then
                ElseIf Visafilenumber = "" Or Visafilenumber Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa File Number"
                End If
            End If


            Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
            If Filepath <> "" Then
                Dim bitmapData() As Byte
                Dim sbText As StringBuilder = New StringBuilder(Filepath, Filepath.Length)
                sbText.Replace("\r\n", String.Empty)
                sbText.Replace(" ", String.Empty)
                sbText.Replace("data:image/png;base64,", String.Empty)
                Try
                    bitmapData = Convert.FromBase64String(sbText.ToString())


                    EDD_Detail.EDD_PHOTO = bitmapData
                Catch ex As Exception

                End Try
            Else
                EDD_Detail.EDD_PHOTO = Nothing
            End If



            If EDDID = "0" Then
                EDD_Detail.EDD_ID = 0
            Else
                EDD_Detail.EDD_ID = EDDID
            End If
            EDD_Detail.EDD_DOB = DOBDate
            EDD_Detail.EDD_EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(usrName)
            EDD_Detail.EDD_NAME = Name
            EDD_Detail.EDD_RELATION = Relation
            EDD_Detail.EDD_STU_ID = 0

            EDD_Detail.bConcession = True
            EDD_Detail.EDD_NoofTicket = 0
            '  Dim GemsType As String = ""
            If GemsType = "Non Gems" Then
                EDD_Detail.EDD_GEMS_TYPE = "N"
            ElseIf GemsType = "Gems Staff" Then
                EDD_Detail.EDD_GEMS_TYPE = "E"
            ElseIf GemsType = "Gems Student" Then
                EDD_Detail.EDD_GEMS_TYPE = "S"
            Else
                EDD_Detail.EDD_GEMS_TYPE = "N"
            End If
            'Dim EmployeeID As String = ""
            'Dim Unit As String = ""
            ' EDD_Detail.EDD_GEMS_TYPE = "N"
            If GemsType = "Gems Staff" Then
                EDD_Detail.EDD_GEMS_ID = EmployeeID
            ElseIf GemsType = "Gems Student" Then
                EDD_Detail.EDD_GEMS_ID = StuID
            Else
                EDD_Detail.EDD_GEMS_ID = 0
                bsuid = ""
            End If

            EDD_Detail.EDD_GEMS_TYPE_DESCR = GemsType
            EDD_Detail.EDD_BSU_ID = bsuid
            EDD_Detail.EDD_BSU_NAME = Unit
            EDD_Detail.EDD_bCompanyInsurance = False

            'If ddlDepInsuCardTypes.SelectedItem.Text <> "--Select--" Then
            '    EDD_Detail.EDD_Insu_id = ddlDepInsuCardTypes.SelectedValue
            '    EDD_Detail.InsuranceCategoryDESC = ddlDepInsuCardTypes.SelectedItem.Text
            'Else
            EDD_Detail.EDD_Insu_id = 0
            EDD_Detail.InsuranceCategoryDESC = ""
            ' End If
            EDD_Detail.EDD_PASSPRTNO = PassportNo
            EDD_Detail.EDD_UIDNO = UIDNo
            Dim defalutDt As Date
            defalutDt = DateTime.Parse("01/Jan/1900")
            If (VisaIssueDt = "") Then
                EDD_Detail.EDD_VISAISSUEDATE = defalutDt
            Else
                EDD_Detail.EDD_VISAISSUEDATE = IIf(VisaIssueDt = "", defalutDt, CDate(VisaIssueDt))
            End If
            If (VisaIssueDt = "") Then
                EDD_Detail.EDD_VISAEXPDATE = defalutDt
            Else : EDD_Detail.EDD_VISAEXPDATE = IIf(VisaExpDate = "", defalutDt, CDate(VisaExpDate))
            End If
            If (EIDExpryDt = "") Then
                EDD_Detail.EDC_EXP_DT = defalutDt
            Else
                EDD_Detail.EDC_EXP_DT = IIf(EIDExpryDt = "", "01/Jan/1900", CDate(EIDExpryDt))
            End If

            EDD_Detail.EDD_VISAISSUEPLACE = VisaIssuePlc
            EDD_Detail.EDD_FILENO = Visafilenumber
            ' EDD_Detail.EDD_bCompanyInsurance = True

            'If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
            '    If txtEIDNo1.Text <> "" Then
            '        If txtEIDNo1.Text.Length = "3" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = txtEIDNo1.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If


            '    End If

            '    If txtEIDNo2.Text <> "" Then

            '        If txtEIDNo2.Text.Length = "4" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo2.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If
            '    End If

            '    If txtEIDNo3.Text <> "" Then

            '        If txtEIDNo3.Text.Length = "7" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo3.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If
            '    End If
            '    If txtEIDNo4.Text <> "" Then
            '        If txtEIDNo4.Text.Length = "1" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo4.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If


            '    End If
            'Else
            EDD_Detail.EDC_DOCUMENT_NO = EIDNo
            'End If
            ''EDD_Detail.EDC_DOCUMENT_NO = Me.txtEIDNo.Text.Trim

            EDD_Detail.EDD_bMALE = IIf(Gender = "M", True, False)
            EDD_Detail.EDD_MARITALSTATUS = Convert.ToInt16(MarStatus)
            EDD_Detail.EDC_Gender = Gender
            EDD_Detail.EDD_MSTATUS = DependMStatus
            EDD_Detail.EDD_CTY_ID = Convert.ToInt16(CityID)
            EDD_Detail.CTY_NATIONALITY = Nationality

            EDD_Detail.EDD_VISASPONSOR = Convert.ToInt32(Sponser)
            EDD_Detail.EDD_EMIRATE = Convert.ToInt32(0)
            EDD_Detail.EDD_EMIRATE_AREA = Convert.ToInt32(0)
            EDD_Detail.EDD_INSURANCENUMBER = InsuranceNumber
            EDD_Detail.EDD_RESIDENCE_CTY_ID = Convert.ToInt32(ResindenceCountryId)
            EDD_Detail.EDD_GEMS_STAFF_STU_NO = GemsStaffIdorStuId
            Dim retval As String
            Dim new_elh_id As Integer = 0
            retval = EOS_EmployeeDependant.SaveDependantDetail(EDD_Detail)
            'retval = "0"
            If (retval = "0" Or retval = "") Then
                Return "0" + "||" + "Data Saved Successfully "
            Else
                Return retval + "||" + UtilityObj.getErrorMessage(retval)
            End If
        Catch ex As Exception
            Return "1000" + "||" + UtilityObj.getErrorMessage(1000) + ", " + ex.Message
        End Try

    End Function
    'Added by vikranth on 15th Jan 2020 for getting EmpId details
    Private Function GetEMP_Basic_Details() As DataSet
        Dim ds As DataSet = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
        Param(0) = Mainclass.CreateSqlParameter("@EMP_ID", ViewState("EmployeeID"), SqlDbType.VarChar)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetEMP_Basic_Details]", Param)
        Return ds
    End Function

#End Region
End Class
