﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CORP_StaffAttendance_popup.aspx.vb" Inherits="OASIS_HR_CORP_StaffAttendance_popup" %>

<%--MasterPageFile="~/mainMasterPage.master"--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<head runat="server">
    <title></title>
    <style>
        table td input, div input {
            background-color: #fff;
        }
    </style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Lineawesome CSS -->
    <link rel="stylesheet" href="assets/css/line-awesome.min.css">

    <!-- Chart CSS -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">

    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Bootstrap DataTables CSS -->
    <link href="assets/css/dataTables.bootstrap4.min.css" rel="stylesheet" media="all" />

    <!-- Bootstrap Datetimepicker CSS -->
    <link href="assets/css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" />

    <link rel="stylesheet" href="assets/css/semantic.min.css">
    <link rel="stylesheet" href="assets/css/prism.min.css">

    <%--<link href="https://school.gemsoasis.com/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="https://school.gemsoasis.com/PHOENIXBETA/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />--%>
    <link href="assets/css/Accordian.css" rel="stylesheet" />

    

     <!-- jQuery -->
    <script src="assets/js/moment.js"></script>  
    <script src="assets/js/jquery-3.2.1.min.js"></script>  
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
    
</head>

<body>
    <form runat="server" id="form1">
        <ajaxToolkit:ToolkitScriptManager runat="server" ID="toolscriptmanager"></ajaxToolkit:ToolkitScriptManager>
        <style>
            .legend {
                height: 25px;
                width: 25px;
                display: inline-block;
            }

                .legend.annual {
                    background-color: #33ccff;
                }

                .legend.emergency {
                    background-color: #ff751a;
                }

                .legend.medical {
                    background-color: #004d99;
                }

                .legend.maternity {
                    background-color: #ff33cc;
                }

                .legend.holiday {
                    background-color: #adb8b7;
                }

            .not-logged {
                font: bolder;
                color: red;
            }

            .validation-warning {
                color: red;
            }

            #divCorpStaffAttendanceReport td span {
                display: inline-block;
                float: left;
                line-height: 25px;
            }

                #divCorpStaffAttendanceReport td span.legend, .not-logged {
                    margin-right: 8px;
                }

            .error {
                font-family: Raleway,sans-serif;
                font-size: 7pt;
                font-weight: normal;
            }

            /*.RadGrid_Default .rgHeader {
            background-color: #92D050;
            background: #92D050;
        }*/
            .RadGrid_Default .rgHeader {               
                height: 35px !important;
            }

            .RadGrid_Default {
                width:100% !important;
            }

            .RadGrid_Default .rgAltRow td, .RadGrid_Default .rgRow td, .RadGrid_Default .rgMasterTable td.rgExpandCol {
                border-color: #949494;
            }

            .RadGrid_Default .rgRow td, .RadGrid_Default .rgAltRow td {
                border-width: 0 1px 1px 0;
                font-weight: normal;
            }

            .RadGrid_Default .rgAltRow {
                background: none;
            }

            .RadGrid_Default .rgHeader {
                border-right: solid 1px #669435;
            }

            .RadGrid table.rgMasterTable tr .rgExpandCol {
                border-right-width: 1px;
                border-left-width: 0;
            }

            .btn-primary {
                /*background-color: #249af1 !important;
                border: 1px solid #249af1 !important;*/
                background-color: #8dc24c !important;
                border: 1px solid #8dc24c !important;
            }

            /*overwriting the bootstrap class*/
            .card-header:first-child, .card {
                border-radius: 0px !important;
            }
            .card-header {
                    /*background: #667eea  !important;
    background: -moz-linear-gradient(left, #667eea 0%, #764ba2 100%)  !important;
    background: -webkit-linear-gradient(left, #667eea 0%, #764ba2 100%)  !important;
    background: -ms-linear-gradient(left, #667eea 0%, #764ba2 100%)  !important;*/
    /* background: linear-gradient(to right, #667eea 0%, #764ba2 100%); */
    /*background: linear-gradient(to right, #249bf2 0%, #2177de 100%)  !important;*/
    background: rgb(149,193,91);
    background: -moz-linear-gradient(left, rgba(149,193,91,1) 0%, rgba(141,194,76,1) 48%, rgba(106,146,58,1) 100%);
    background: -webkit-linear-gradient(left, rgba(149,193,91,1) 0%,rgba(141,194,76,1) 48%,rgba(106,146,58,1) 100%);
    background: linear-gradient(to right, rgba(149,193,91,1) 0%,rgba(141,194,76,1) 48%,rgba(106,146,58,1) 100%);
            }

            .textbox-cal-icon-corp {
    z-index: 1000;
    position: absolute;
    right: 30px;
    top: 13px;
    color: #9e9e9e;
    font-size: 16px;
}
             th.prev::before {
  content: "<" !important;
}

th.next::before {
  content: ">" !important;
}
span.glyphicon.glyphicon-chevron-right, span.glyphicon.glyphicon-chevron-left {
    display:none !important;
}
        </style>
        <script type="text/javascript">
            $(window).on('load', function () {
                
                var i = $('#<%= hidOnLoad.ClientID%>').val();               
                if (i == "0") {
                    var ToDayDate = moment().format("DD/MMM/YYYY");
                    $('#<%= txtFrom.ClientID%>').val(ToDayDate);
                    $('#<%= txtTo.ClientID%>').val(ToDayDate);
                   
                }              
            });


           $(document).ready(function () {
             
               //GetMenuAccess();

             //  var FromDate = moment(new Date()).subtract('months', 2).format("DD/MMM/YYYY");
              

               $('#loader').delay(100).fadeOut('slow');
               $('#loader-wrapper').delay(500).fadeOut('slow');
            $(".datetimepickerPopup").datetimepicker({
                format: 'DD/MMM/YYYY'
            });


                function checkDate(sender, args) {
                    var fromDate = new Date($('#<%= txtFrom.ClientID %>').val());
                    if (sender._selectedDate < fromDate) {
                        alert("You cannot select a day before 'From Date'!");
                        sender._selectedDate = new Date();
                        // set the date back to the current date
                        sender._textbox.set_Value(fromDate.format(sender._format))
                        return;
                    }
                    console.log(sender._selectedDate);
                }

                function validateFromDate(sender, args) {
                    var fromDate = sender._selectedDate;
                    var toDate = new Date($('#<%= txtTo.ClientID%>').val());
                    if (toDate != undefined && toDate != '') {
                        if (fromDate > toDate) {
                            $('#<%= txtTo.ClientID%>').val(fromDate.format(sender._format))
                        }
                        console.log(sender._selectedDate);
                    }
                }



                function getDateInMonthFirstFormat(dateInStringFormat) {
                    var currentDate = dateInStringFormat;
                    return currentDate.substring(3, 5) + "/" + currentDate.substring(0, 2) + "/" + currentDate.substring(6);
                }
           });



            function VerifyDates() {
                
                var fromdate = new Date($('#<%= txtFrom.ClientID%>').val());
                var todate = new Date($('#<%= txtTo.ClientID%>').val());
               
                if (fromdate != '' && todate !='' && fromdate > todate)
                {
                    alert("From Date is Greater Than To Date");
                    $('#<%= txtFrom.ClientID%>').val('');
                    $('#<%= txtTo.ClientID%>').val('');
                }
               
            }

            function LoaderStart() {
              
                $('#loader').delay(100).fadeIn('slow');
                $('#loader-wrapper').delay(500).fadeIn('slow');            
            }
        </script>
        <div class="card mb-3 form-horizontal">
            <div class="card-header letter-space text-white p-3">
                <i class="fa fa-file mr-3"></i>CORP Staff Attendance Report
            </div>
             <!-- Loader -->
            <div id="loader-wrapper">
                <div id="loader">
                    <div class="loader-ellips">
                        <span class="loader-ellips__dot"></span>
                        <span class="loader-ellips__dot"></span>
                        <span class="loader-ellips__dot"></span>
                        <span class="loader-ellips__dot"></span>
                    </div>
                </div>
            </div>
            <!-- /Loader -->
            <div class="card-body">
                <div class="form-group">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="lblError" class="text-danger" Text="" runat="server" ></asp:Label>

                            <div class="row">
                                <asp:HiddenField ID="hidOnLoad" runat="server" Value="0" />
                                <div class="col-lg-2 col-12 mt-3">
                                    <span class="field-label">From Date<span class="text-danger">*</span></span>
                                </div>
                                <div class="col-lg-3 col-12">
                                     <div class="cal-icon-pick">
                                                <i class="fa fa-calendar textbox-cal-icon-corp"></i>
                                    <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control datetimepickerPopup" AutoComplete="off" onblur="VerifyDates();"></asp:TextBox></div><br />
                                    <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup1" CssClass="error"
                                            ForeColor=""></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-2 col-12 mt-3">
                                    <span class="field-label">To Date<span class="text-danger">*</span></span>
                                </div>
                                <div class="col-lg-3 col-12">
                                     <div class="cal-icon-pick">
                                                <i class="fa fa-calendar textbox-cal-icon-corp"></i>
                                    <asp:TextBox ID="txtTo" runat="server" AutoComplete="off" CssClass="form-control datetimepickerPopup" onblur="VerifyDates();"></asp:TextBox></div><br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTo"
                                            Display="Dynamic" ErrorMessage="To Date required" ValidationGroup="AttGroup2" CssClass="error"
                                            ForeColor=""></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-2 col-12 text-center">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btn-round" Text="Search" OnClientClick="LoaderStart();" OnClick="btnSearch_Click" CausesValidation="true" ValidationGroup="AttGroup1,AttGroup2,AttGroup3,DateValidation1,DateValidation2" />
                                        <%--<asp:Button ID="btnDownloadPdf" runat="server" CssClass="btn btn-primary btn-round" Text="Pdf" OnClick="btnDownloadPdf_Click" Visible="false" />--%>
                                </div>                                
                            </div>
                            

                        </div>
                    </div>
                </div>
                <%--table-striped custom-table mb-0--%>
                <div id="divCorpStaffAttendanceReport" class="form-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <telerik:RadGrid ID="gv_CorpStaffAttendanceReport" runat="server" AllowPaging="True" Width="100%" 
                                    OnNeedDataSource="gv_CorpStaffAttendanceReport_NeedDataSource" OnDetailTableDataBind="gv_CorpStaffAttendanceReport_DetailTableDataBind"
                                    PageSize="10" AllowSorting="True" MasterTableView-AllowPaging="true"
                                    AutoGenerateColumns="False" CellSpacing="0" GridLine="">
                                    <ClientSettings AllowDragToGroup="True" >
                                      <ClientEvents  OnCommand="LoaderStart"/>
                                    </ClientSettings>
                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" AllowPaging="true" DataKeyNames="EmployeeId,StaffId,AttendanceDate" ClientDataKeyNames="StaffId" HierarchyLoadMode="ServerBind"  >
                                        <DetailTables>
                                            <telerik:GridTableView DataKeyNames="EmployeeId,StaffId,AttendanceDate" Name="StaffDetails" AutoGenerateColumns="false" Width="100%" HierarchyLoadMode="ServerOnDemand" AllowPaging="true">
                                                <ParentTableRelation>
                                                    <telerik:GridRelationFields DetailKeyField="StaffId" MasterKeyField="StaffId"></telerik:GridRelationFields>
                                                    <telerik:GridRelationFields DetailKeyField="AttendanceDate" MasterKeyField="AttendanceDate"></telerik:GridRelationFields>
                                                </ParentTableRelation>
                                                <Columns>
                                                    <telerik:GridBoundColumn HeaderText="In Time" DataField="InTime" UniqueName="column11">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="In Terminal" DataField="DepartmentDescription" UniqueName="column12">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Out Time" DataField="OutTime" UniqueName="column13">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Out Terminal" DataField="DepartmentDescription" UniqueName="column14">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="In Minutes" DataField="TotalMinutes" UniqueName="column15">
                                                    </telerik:GridBoundColumn>
                                                </Columns>
                                            </telerik:GridTableView>
                                        </DetailTables>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="StaffName" AllowFiltering="false"
                                                HeaderText="Staff Name"
                                                UniqueName="column0">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AttendanceDate" AllowFiltering="false"
                                                HeaderText="Log Date"
                                                UniqueName="column1">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LoggedInTime"
                                                HeaderText="First In"
                                                UniqueName="column2">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LoggedOutTime"
                                                HeaderText="Last Out"
                                                UniqueName="column3">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TotalHours"
                                                HeaderText="Total Time"
                                                UniqueName="column4">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LeaveType"
                                                HeaderText="Leave Type"
                                                UniqueName="column5">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StaffId" AllowFiltering="false"
                                                HeaderText="Staff Id"
                                                UniqueName="column6" Visible="false">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <HeaderStyle Font-Bold="true" HorizontalAlign="Left" />
                                    <ItemStyle Font-Bold="true" HorizontalAlign="Left" />                                    
                                   
                                    <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                                </telerik:RadGrid>
                            </div>
                            <table width="80%" style="margin-left: 10%;">
                                <tr class="">
                                    <td align="left"><span class="legend annual"></span><span>Annual Leave</span></td>
                                    <td align="left"><span class="legend emergency"></span><span>Emergency Leave</span></td>
                                    <td align="left"><span class="legend medical"></span><span>Medical Leave</span></td>
                                    <td align="left"><span class="legend maternity"></span><span>Paternity/Maternity Leave</span></td>
                                    <td align="left"><span class="legend holiday"></span><span>Holiday</span></td>
                                    <td align="left"><span class="not-logged">NL</span><span>Not Logged</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body">
                            <p>
                                <small>Disclaimer:-
                                <br />
                                    - The report is based on access / attendance records for 3 work locations only - SSC, Al Joud and McLaren offices<br />
                                    - Time spent at the schools is not captured as part of the attendance logs<br />
                                    - The daily work hours is calculated based on the sum total of time spent at individual offices. (excludes travel time and meetings attended in schools)<br />
                                    - The report is based on the in and out entries logs captured at each work location. Attendance report may not accurately reflect the total time spent at work if the user does not properly log the in /out entries
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
        </CR:CrystalReportSource>
        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                function UpdateItemCountField(sender, args) {
                    //Set the footer text.
                    sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
                }
                function Combo_OnClientItemsRequesting(sender, eventArgs) {
                    var combo = sender;
                    ComboText = combo.get_text();
                    ComboInput = combo.get_element().getElementsByTagName('input')[0];
                    ComboInput.focus = function () { this.value = ComboText };

                    if (ComboText != '') {
                        window.setTimeout(TrapBlankCombo, 100);
                    };
                }

                function TrapBlankCombo() {
                    if (ComboInput) {
                        if (ComboInput.value == '') {
                            ComboInput.value = ComboText;
                        }
                        else {
                            window.setTimeout(TrapBlankCombo, 100);
                        };
                    };
                }

            </script>
        </telerik:RadScriptBlock>
    </form>

    <script type="text/javascript">
        $('.fa-calendar').on('click', function () {          
            $(this).next().focus();  
    });
</script>
</body>




