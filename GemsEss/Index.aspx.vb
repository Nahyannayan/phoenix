﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports System.Drawing

Partial Class GemsEss_Index
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Public Property MenuSet() As DataTable
        Get
            Return ViewState("MenuSet")
        End Get
        Set(ByVal value As DataTable)
            ViewState("MenuSet") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Page.Header.DataBind()
        Session("hidLoadValue") = ""

        If Not Page.IsPostBack Then
            If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                Response.Redirect("~\noAccess.aspx")
            End If
            Dim smScriptManager As ScriptManager = Master.FindControl("ScriptManager1")
            smScriptManager.RegisterPostBackControl(btnConSalDwnld)
            subPageLoad()
            hdnMonth.Value = Convert.ToString((DateTime.Now.Month - 1))
            ' hdnYear.Value = Convert.ToString(DateTime.Now.Year)
        End If
    End Sub


#Region "PAGE METHODES"
    Sub BindCovidTests()
        Try

            Dim ds As DataSet

            Dim str_Sql As String = "exec Get_EMP_COVID_TESTS '" & HttpContext.Current.Session("EmployeeID") & "', '" & Session("sUsr_name") & "'"
            'Session("sUsr_id")

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count = 0 Then
                trCovid.Visible = True
                lblCovidMsg.Text = "No Data "
            Else
                trCovid.Visible = False
                lblCovidMsg.Text = ""
            End If
            If dt.Rows.Count > 0 Then
                rpt_Covid.DataSource = dt
                rpt_Covid.DataBind()
                'Else
                '    divMyTeam.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub BindLeaveRequest()
        Try

            Dim ds As DataSet

            Dim str_Sql As String = "exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','Open',''"
            str_Sql = str_Sql & " exec GET_LEAVE_REQUEST '" & Session("sUsr_name") & "', 'leave','" & Session("sbsuid") & "','M',''"
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            Dim dt As DataTable = ds.Tables(0)
            dt.Merge(ds.Tables(1), True)

            If dt.Rows.Count = 0 Then
                trLeaveHis.Visible = True
                lblMessage.Text = "No Data "
            Else
                trLeaveHis.Visible = False
                lblMessage.Text = ""
            End If
            If dt.Rows.Count > 0 Then
                rpt_LeaveRequest.DataSource = dt
                rpt_LeaveRequest.DataBind()
            Else
                divMyTeam.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Function BIND_ESS_MENUS(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal userSuper As Integer, ByVal order As Integer, ByVal employeeid As String) As DataSet
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@bunit", bunit)
        param(1) = New SqlParameter("@userMod", userMod)
        param(2) = New SqlParameter("@userRol", userRol)
        param(3) = New SqlParameter("@userSuper", userSuper)
        param(4) = New SqlParameter("@order", order)
        param(5) = New SqlParameter("@emp_id", employeeid)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GET_ESS_MENUS", param)
        Return ds
    End Function


    Public Sub getAccessDetailstoMenu()
        Try
            Dim order As Integer = 1

            Dim MenuSet As DataTable = Nothing
            Dim ds As DataSet
            While order < 5
                ds = BIND_ESS_MENUS(HttpContext.Current.Session("sBsuid"), HttpContext.Current.Session("sroleid"), HttpContext.Current.Session("sModule"), HttpContext.Current.Session("sBusper"), order, HttpContext.Current.Session("EmployeeID"))
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        If MenuSet Is Nothing Then
                            MenuSet = ds.Tables(0)
                        Else
                            MenuSet.Merge(ds.Tables(0))
                        End If

                    End If
                End If
                order = order + 1
            End While
            If Not MenuSet Is Nothing Then
                Dim drow1() As DataRow = MenuSet.Select("MNU_CODE='U000022'")
                Dim drow2() As DataRow = MenuSet.Select("MNU_CODE='U000084'")
                Dim drow3() As DataRow = MenuSet.Select("MNU_CODE='U000064'")
                Dim drow4() As DataRow = MenuSet.Select("MNU_CODE='U000069'")
                Dim drow5() As DataRow = MenuSet.Select("MNU_CODE='U000082'")
                Dim drow7() As DataRow = MenuSet.Select("MNU_CODE='U000025'")
                Dim drow8() As DataRow = MenuSet.Select("MNU_CODE='U000777'")

                If drow1.GetLength(0) > 0 Then
                    MainLeaveDiv.Visible = True
                Else
                    MainLeaveDiv.Visible = False
                End If
                If drow2.GetLength(0) > 0 Then
                    MainLeaveStatusDiv.Visible = True
                Else
                    MainLeaveStatusDiv.Visible = False
                End If
                If drow3.GetLength(0) > 0 Then
                    MainDepDetDiv.Visible = True
                Else
                    MainDepDetDiv.Visible = False
                End If
                If drow4.GetLength(0) > 0 Then
                    MainProfileDiv.Visible = True
                Else
                    MainProfileDiv.Visible = False
                End If
                If drow5.GetLength(0) > 0 Then
                    MainPayslipDiv.Visible = True
                Else
                    MainPayslipDiv.Visible = False
                End If

                If drow7.GetLength(0) > 0 Then
                    MainPnODiv.Visible = True
                Else
                    MainPnODiv.Visible = False
                End If
                MainAttLogDiv.Visible = False
                'If drow8.GetLength(0) > 0 Then
                '    MainAttLogDiv.Visible = True
                'Else
                '    MainAttLogDiv.Visible = False
                'End If
            Else

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Private Function GetLeaveMatrix(ByVal StartDate As Date, ByVal Days As Decimal, ByVal BsuId As String, ByVal IsPlannerEnabled As Integer) As DataSet
        GetLeaveMatrix = Nothing
        If IsPlannerEnabled = 1 Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(0).Value = StartDate

            pParms(1) = New SqlClient.SqlParameter("@Days", SqlDbType.Int)
            pParms(1).Value = Days

            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = BsuId

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
                Dim cmd As New SqlCommand("[GetMyLeaveDays]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                Return ds
            End Using
        End If
    End Function


    Protected Sub BINDaTTENDANCElOG()
        Dim DS As DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@EMP_ID", Session("EmployeeID"))
        param(1) = New SqlParameter("@iSENG", True)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "dbo.GET_ATTENDANCE_LOGS", param)
        If DS.Tables(0).Rows.Count > 0 Then
            rptrAttLog.DataSource = DS.Tables(0)
            rptrAttLog.DataBind()
        End If
    End Sub
    Private Sub BIND_BSU(ByVal bsu As String, ByVal user As String)
        bindEmirate()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@BSU", bsu)
        param(1) = New SqlParameter("@USR_NAME", user)

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "Get_Businessunit", param)
            rptView_profile.DataSource = datareader
            rptView_profile.DataBind()
        End Using

        Dim ColorLightGray As System.Drawing.Color = System.Drawing.Color.FromArgb(243, 243, 243)
        Dim params(2) As SqlClient.SqlParameter
        params(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeID"))
        Dim dt As DataTable
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "dbo.GET_EMP_VISA_EID_DETAILS", params)
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                If Not dt.Rows(0)("EMP_DISPLAYNAME") Is Nothing And dt.Rows(0)("EMP_DISPLAYNAME") <> "" Then
                    txtEmpName.InnerHtml = dt.Rows(0)("EMP_DISPLAYNAME")
                    txtEmpNameEdit.Text = dt.Rows(0)("EMP_DISPLAYNAME")
                    txtEmpNameEdit.Enabled = False
                Else
                    txtEmpName.InnerHtml = ""
                    txtEmpNameEdit.Text = ""
                End If

                If Not dt.Rows(0)("EMPNo") Is Nothing And dt.Rows(0)("EMPNo") <> "" Then
                    h_EmpNoDesc.Value = dt.Rows(0)("EMPNo")
                    txtEmpNo.InnerHtml = dt.Rows(0)("EMPNo")
                    txtEmpNoEdit.Text = dt.Rows(0)("EMPNo")
                    txtEmpNoEdit.Enabled = False
                Else
                    txtEmpNo.InnerHtml = ""
                    txtEmpNoEdit.Text = ""
                End If

                If Not dt.Rows(0)("EIDNO") Is Nothing And dt.Rows(0)("EIDNO") <> "" Then
                    txtEmrId.InnerHtml = dt.Rows(0)("EIDNO")
                    txtEmrIdEdit.Text = dt.Rows(0)("EIDNO")
                Else
                    txtEmrId.InnerHtml = ""
                    txtEmrIdEdit.Text = ""
                End If

                If Not dt.Rows(0)("EIDEXPDATE") Is Nothing And dt.Rows(0)("EIDEXPDATE") <> "" Then
                    txtEidExpDt.InnerHtml = dt.Rows(0)("EIDEXPDATE")
                    txtEidExpDtEdit.Text = dt.Rows(0)("EIDEXPDATE")
                Else
                    txtEidExpDt.InnerHtml = ""
                    txtEidExpDtEdit.Text = ""
                End If

                If Not dt.Rows(0)("PASSPORTNO") Is Nothing And dt.Rows(0)("PASSPORTNO") <> "" Then
                    txtPassportNo.InnerHtml = dt.Rows(0)("PASSPORTNO")
                    txtPassportNoEdit.Text = dt.Rows(0)("PASSPORTNO")
                Else
                    txtPassportNo.InnerHtml = ""
                    txtPassportNoEdit.Text = ""
                End If

                If Not dt.Rows(0)("PASSPORTISSUEPLACE") Is Nothing And dt.Rows(0)("PASSPORTISSUEPLACE") <> "" Then
                    txtPassprtIsuePlc.InnerHtml = dt.Rows(0)("PASSPORTISSUEPLACE")
                    txtPassprtIsuePlcEdit.Text = dt.Rows(0)("PASSPORTISSUEPLACE")
                Else
                    txtPassprtIsuePlc.InnerHtml = ""
                    txtPassprtIsuePlcEdit.Text = ""
                End If

                If Not dt.Rows(0)("PASSPORTISUEDATE") Is Nothing And dt.Rows(0)("PASSPORTISUEDATE") <> "" Then
                    txtPasprtIsueDt.InnerHtml = dt.Rows(0)("PASSPORTISUEDATE")
                    txtPasprtIsueDtEdit.Text = dt.Rows(0)("PASSPORTISUEDATE")
                Else
                    txtPasprtIsueDt.InnerHtml = ""
                    txtPasprtIsueDtEdit.Text = ""
                End If

                If Not dt.Rows(0)("PASSPORTEXPDATE") Is Nothing And dt.Rows(0)("PASSPORTEXPDATE") <> "" Then
                    txtPasprtexpdt.InnerHtml = dt.Rows(0)("PASSPORTEXPDATE")
                    txtPasprtexpdtEdit.Text = dt.Rows(0)("PASSPORTEXPDATE")
                Else
                    txtPasprtexpdt.InnerHtml = ""
                    txtPasprtexpdtEdit.Text = ""
                End If

                If Not dt.Rows(0)("EMP_UIDNO") Is Nothing And dt.Rows(0)("EMP_UIDNO") <> "" Then
                    txtUIDNo.InnerHtml = dt.Rows(0)("EMP_UIDNO")
                    txtUIDNoEdit.Text = dt.Rows(0)("EMP_UIDNO")
                Else
                    txtUIDNo.InnerHtml = ""
                    txtUIDNoEdit.Text = ""
                End If

                If Not dt.Rows(0)("VISAISSUEPLACE") Is Nothing And dt.Rows(0)("VISAISSUEPLACE") <> "" Then
                    txtVisaIsuePlc.InnerHtml = dt.Rows(0)("VISAISSUEPLACE")
                    txtVisaIsuePlcEdit.Text = dt.Rows(0)("VISAISSUEPLACE")
                Else
                    txtVisaIsuePlc.InnerHtml = ""
                    txtVisaIsuePlcEdit.Text = ""
                End If

                If Not dt.Rows(0)("VISAISUEDATE") Is Nothing And dt.Rows(0)("VISAISUEDATE") <> "" Then
                    txtVisaIsueDt.InnerHtml = dt.Rows(0)("VISAISUEDATE")
                    txtVisaIsueDtEdit.Text = dt.Rows(0)("VISAISUEDATE")
                Else
                    txtVisaIsueDt.InnerHtml = ""
                    txtVisaIsueDtEdit.Text = ""
                End If

                If Not dt.Rows(0)("VISAEXPDATE") Is Nothing And dt.Rows(0)("VISAEXPDATE") <> "" Then
                    txtVisaExpDt.InnerHtml = dt.Rows(0)("VISAEXPDATE")
                    txtVisaExpDtEdit.Text = dt.Rows(0)("VISAEXPDATE")
                Else
                    txtVisaExpDt.InnerHtml = ""
                    txtVisaExpDtEdit.Text = ""
                End If

                If dt.Rows(0)("EMP_VISA_SPONSOR").ToString <> "" Then
                    If dt.Rows(0)("EMP_VISA_SPONSOR") = 1 Then
                        ddlsponser.InnerHtml = "Company"
                    ElseIf dt.Rows(0)("EMP_VISA_SPONSOR") = 2 Then
                        ddlsponser.InnerHtml = "Spouse"
                    ElseIf dt.Rows(0)("EMP_VISA_SPONSOR") = 3 Then
                        ddlsponser.InnerHtml = "Father/Mother"
                    ElseIf dt.Rows(0)("EMP_VISA_SPONSOR") = 4 Then
                        ddlsponser.InnerHtml = "Mother"
                    Else
                        ddlsponser.InnerHtml = ""
                    End If
                    ddlsponserEdit.ClearSelection()
                    If dt.Rows(0)("EMP_VISATYPE").ToString = "0" Then
                        ddlsponserEdit.Items.FindByValue("1").Selected = True
                        ddlsponserEdit.Enabled = False
                        ddlsponserEdit.BackColor = ColorLightGray
                    Else
                        ddlsponserEdit.Enabled = True
                        ddlsponserEdit.Items.FindByValue(dt.Rows(0)("EMP_VISA_SPONSOR").ToString).Selected = True
                        ddlsponserEdit.BackColor = Drawing.Color.White
                    End If
                Else
                    ddlsponserEdit.ClearSelection()
                    If dt.Rows(0)("EMP_VISATYPE").ToString = "0" Then
                        ddlsponserEdit.Items.FindByValue("1").Selected = True
                        ddlsponserEdit.Enabled = False

                        ddlsponserEdit.BackColor = ColorLightGray

                    End If
                End If

                h_EMP_VISASTATUS.Value = dt.Rows(0)("EMP_VISASTATUS").ToString.Trim()
                h_EMP_CTYId.Value = dt.Rows(0)("emp_Cty_id").ToString.Trim()
                If dt.Rows(0)("EMP_VISATYPE").ToString = "0" AndAlso dt.Rows(0)("EMP_VISASTATUS").ToString = "2" Then
                    txtVisaIsueDtEdit.Enabled = False
                    txtVisaIsueDtEdit.BackColor = ColorLightGray
                    txtVisaExpDtEdit.Enabled = False
                    txtVisaExpDtEdit.BackColor = ColorLightGray
                    txtVisaIsuePlcEdit.Enabled = False
                    txtVisaIsuePlcEdit.BackColor = ColorLightGray
                Else
                    txtVisaIsueDtEdit.Enabled = True
                    txtVisaIsueDtEdit.BackColor = Drawing.Color.White
                    txtVisaIsueDtEdit.Style.Remove("background-color")
                    txtVisaExpDtEdit.Enabled = True
                    txtVisaExpDtEdit.BackColor = Drawing.Color.White
                    txtVisaExpDtEdit.Style.Remove("background-color")
                    txtVisaIsuePlcEdit.Enabled = True
                    txtVisaIsuePlcEdit.BackColor = Drawing.Color.White
                    txtVisaIsuePlcEdit.Style.Remove("background-color")
                End If


                If Not dt.Rows(0)("emp_insurance_number") Is Nothing And dt.Rows(0)("emp_insurance_number") <> "" Then
                    txtEmpInsuNo.InnerHtml = dt.Rows(0)("emp_insurance_number")
                    txtEmpInsuNoEdit.Text = dt.Rows(0)("emp_insurance_number")
                Else
                    txtEmpInsuNo.InnerHtml = ""
                    txtEmpInsuNoEdit.Text = ""
                End If


                ddlEmirate.ClearSelection()
                If Not dt.Rows(0)("EMP_EMR_ID") Is Nothing And dt.Rows(0)("EMP_EMR_ID") <> 0 Then
                    ddlEmirate.Items.FindByValue(dt.Rows(0)("EMP_EMR_ID")).Selected = True
                    ddlEmirate1.InnerHtml = ddlEmirate.SelectedItem.Text
                Else
                    ddlEmirate1.InnerHtml = ""
                End If


                If Not dt.Rows(0)("EMP_EMA_ID") Is Nothing And dt.Rows(0)("EMP_EMA_ID") <> 0 Then
                    bindArea()
                    ddlArea.ClearSelection()
                    ddlArea.Items.FindByValue(dt.Rows(0)("EMP_EMA_ID")).Selected = True
                    ddlArea1.InnerHtml = ddlArea.SelectedItem.Text
                Else
                    ddlArea1.InnerHtml = ""
                    If dt.Rows(0)("EMP_EMR_ID") <> "0" Or dt.Rows(0)("EMP_EMR_ID") <> 0 Then
                        ddlArea.Enabled = True
                        ddlArea.Attributes.Add("style", "display: none;background:white")
                    Else
                        ddlArea.Enabled = False
                        ddlArea.Attributes.Add("style", "display: none;background:rgb(243,243,243);")
                    End If
                End If


                If Not dt.Rows(0)("EMD_Personal_EmailID") Is Nothing And dt.Rows(0)("EMD_Personal_EmailID") <> "" Then
                    txtAlEmail.InnerHtml = dt.Rows(0)("EMD_Personal_EmailID")
                    txtAlEmailEdit.Text = dt.Rows(0)("EMD_Personal_EmailID")
                Else
                    txtAlEmail.InnerHtml = ""
                    txtAlEmailEdit.Text = ""
                End If

                If Not dt.Rows(0)("EMD_CUR_MOBILE") Is Nothing And dt.Rows(0)("EMD_CUR_MOBILE") <> "" Then
                    txtPhnNum.InnerHtml = dt.Rows(0)("EMD_CUR_MOBILE")
                    txtPhnNumEdit.Text = dt.Rows(0)("EMD_CUR_MOBILE")
                Else
                    txtPhnNum.InnerHtml = ""
                    txtPhnNumEdit.Text = ""
                End If

                If Not dt.Rows(0)("EMD_PHOTO") Is Nothing And dt.Rows(0)("EMD_PHOTO") <> "" Then
                    imgPro.Src = dt.Rows(0)("EMD_PHOTO")
                Else
                    imgPro.Src = ""
                End If
                'Added by vikranth on 8th Mar 2020
                If Not dt.Rows(0)("EMP_SEX_bMALE") Is Nothing Then
                    h_EMP_SEX.Value = dt.Rows(0)("EMP_SEX_bMALE")
                End If
            End If
        End If
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = String.Empty
        str_Sql = "select * from vw_OSO_EMPLOYEECONTACT where EMD_EMP_ID = " & Session("EmployeeID")
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While dr.Read()
            spnEmrCnt1.InnerHtml = dr("EMD_CONTACT_LOCAL").ToString
            spnPhnEmerCont1.InnerHtml = dr("EMD_CONTACT_LOCAL_NUM").ToString
            spnEmrCnt2.InnerHtml = dr("EMD_CONTACT_HOME").ToString
            spnPhnEmerCont2.InnerHtml = dr("EMD_CONTACT_HOME_NUM").ToString

            txtEmrCnt1.Text = dr("EMD_CONTACT_LOCAL").ToString
            txtPhnEmerCont1.Text = dr("EMD_CONTACT_LOCAL_NUM").ToString
            txtEmrCnt2.Text = dr("EMD_CONTACT_HOME").ToString
            txtPhnEmerCont2.Text = dr("EMD_CONTACT_HOME_NUM").ToString
        End While
    End Sub
    Sub bindEmirate()
        ddlEmirate.Items.Clear()
        Dim ds As New DataSet
        Try
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", 0)
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "EMIRATE")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
            CommandType.StoredProcedure, "dbo.GET_EMIRATE_CITY", param)
            ddlEmirate.DataSource = ds.Tables(0)
            ddlEmirate.DataTextField = "EMR_DESCR"
            ddlEmirate.DataValueField = "EMR_ID"
            ddlEmirate.DataBind()
            ddlEmirate.SelectedIndex = -1
            ddlEmirate.Items.Insert(0, New WebControls.ListItem("Select", "0"))
            ddlEmirate_SelectedIndexChanged(ddlEmirate, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind emirate")
        End Try
    End Sub
    Sub bindArea()
        ddlArea.Items.Clear()
        Dim ds As New DataSet
        Try
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", Convert.ToInt32(ddlEmirate.SelectedValue))
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "AREA")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
            CommandType.StoredProcedure, "GET_EMIRATE_CITY", param)
            ddlArea.DataSource = ds.Tables(0)
            ddlArea.DataTextField = "EMA_DESCR"
            ddlArea.DataValueField = "EMA_ID"
            ddlArea.DataBind()
            ddlArea.SelectedIndex = -1
            ddlArea.Items.Insert(0, New WebControls.ListItem("Select", "0"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind area")
        End Try
    End Sub
    Protected Sub ddlEmirate_SelectedIndexChanged(sender As Object, e As EventArgs)
        If ddlEmirate.SelectedValue <> "0" Then
            bindArea()
        End If
    End Sub
    Protected Sub btnProfEdit_Click(sender As Object, e As EventArgs)

    End Sub
    Public Function getleaves(ByVal str_conn As String, ByVal USR_NAME As String, ByVal leave_id As String) As DataSet
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "','" & USR_NAME & "','" & leave_id & "'"    'V1.1
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Return ds
    End Function
    Sub gridbind_LeaveStatus()
        Try
            Dim str_Sql As String
            Dim USR_NAME As String
            Dim ds As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            If Request.QueryString("id") <> "" Then
                str_Sql = "select USR_NAME from USERS_M where USR_EMP_ID=" + Request.QueryString("id")
                USR_NAME = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
            Else
                USR_NAME = Session("sUsr_name")
            End If
            Dim leave_id As String = IIf(Request.QueryString("FilterId") = "", "All", Request.QueryString("FilterId"))

            ds = getleaves(str_conn, USR_NAME, leave_id)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    tblLeave.Visible = False
                    lblMsg.Text = ""

                    ds.Tables(0).Columns.Add("Color")
                    ds.Tables(0).Columns.Add("DisplayEdit")
                    ds.Tables(0).Columns.Add("DisplayCancel")

                    For Each row In ds.Tables(0).Rows
                        If (row("APPRSTATUS") = "N") Then
                            If row("ELA_APPRSTATUS") = "Requested for Cancellation" Then
                                row("Color") = "rgba(255, 193, 7, 0.8)"
                                row("DisplayEdit") = "none"
                                row("DisplayCancel") = "none"
                            Else
                                row("Color") = "rgba(255, 193, 7, 0.8)"
                                row("DisplayEdit") = "block"
                                row("DisplayCancel") = "block"
                            End If
                        ElseIf (row("APPRSTATUS") = "A") Then
                            row("Color") = "rgba(40, 167, 69, 0.5)"
                            row("DisplayEdit") = "block"
                            row("DisplayCancel") = "block"
                        ElseIf (row("APPRSTATUS") = "C") Then
                            row("Color") = "rgba(251, 57, 57, 0.5)"
                            row("DisplayEdit") = "none"
                            row("DisplayCancel") = "none"
                        ElseIf (row("APPRSTATUS") = "R") Then
                            row("Color") = "rgba(255, 7, 7, 0.5)"
                            row("DisplayEdit") = "none"
                            row("DisplayCancel") = "none"
                        End If
                    Next
                    rptrLeaveStatus.DataSource = ds.Tables(0)
                    rptrLeaveStatus.DataBind()
                Else
                    tblLeave.Visible = True
                    lblMsg.Text = "No Data"
                End If
            Else
                tblLeave.Visible = True
                lblMsg.Text = "No Data"
            End If
            bindLeaveTypes()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub gridDependantDetails()

        Dim mtable As DataTable
        mtable = EOS_EmployeeDependant.GetDependantDetailList(Session("EmployeeID"))

        'If mtable.Rows.Count > 0 Then
        rptrDpndt.DataSource = mtable
        rptrDpndt.DataBind()
        rptrChildList.DataSource = mtable
        rptrChildList.DataBind()
        'End If

    End Sub

    Sub gridbindPanODetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        Dim str_sql As String
        Dim ds As DataSet
        str_sql = "exec DAX.GETemployeePnO_DocumentsRequestList '" & Session("sbsuid") & "','" & Session("EmployeeID") & "',0" '
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                tblPnO.Visible = False
                lblMsg2.Text = ""

                ds.Tables(0).Columns.Add("DisplayEdit")
                For Each row In ds.Tables(0).Rows
                    If (row("PnOStatus_Descr") = "Rejected" Or row("PnOStatus_Descr") = "Issued") Then
                        row("DisplayEdit") = "View"

                    Else
                        row("DisplayEdit") = "View & Edit"

                    End If
                Next

                rptrPODocDetails.DataSource = ds.Tables(0)
                rptrPODocDetails.DataBind()
            Else
                tblPnO.Visible = True
                lblMsg2.Text = "No Data"
            End If
        Else
            tblPnO.Visible = True
            lblMsg2.Text = "No Data"
        End If
    End Sub
    Sub ShowApplyLeave()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_sql As String
        Dim ds As DataSet
        str_sql = "exec [DBO].[IS_SHOW_ESS_APPLY_LEAVE] 1,'" & Session("sBsuid") & "'" '
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim K As Integer = ds.Tables(0).Rows(0)("IS_SHOW")
                If K = 1 Then
                    show_leave_apply.Visible = True
                    show_leave_status.Visible = True
                Else
                    show_leave_apply.Visible = False
                    show_leave_status.Visible = False
                End If

            End If
        End If
    End Sub
    Sub ShowApplyFeeConcession()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_sql As String
        Dim ds As DataSet
        str_sql = "exec [DBO].[IS_SHOW_ESS_APPLY_FEE_CONCESSION] 1,'" & Session("sUsr_name") & "'" '
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim K As Integer = ds.Tables(0).Rows(0)("IS_SHOW")
                If K = 1 Then
                    showFeeApply.Visible = True
                Else
                    showFeeApply.Visible = False
                End If

            End If
        End If
    End Sub
    Sub gridFeeConcessionDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_sql As String
        Dim ds As DataSet
        str_sql = "exec [DBO].[GET_ESS_STUDENT_INFO] 3,'" & Session("EmployeeID") & "'" '
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                tblFeeCo.Visible = False
                lblMsg2.Text = ""
                rptrFeeConcDetails.DataSource = ds.Tables(0)
                rptrFeeConcDetails.DataBind()
            Else
                tblFeeCo.Visible = True
                lblMsg2.Text = "No Data"
            End If
        Else
            tblFeeCo.Visible = True
            lblMsg2.Text = "No Data"
        End If
    End Sub
    Private Function bindLeaveTypes() As DataSet
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As DataSet

        Try
            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "LeaveTypes"
            params(1) = New SqlClient.SqlParameter("@Usr_Id", SqlDbType.VarChar)
            params(1).Value = Session("sUsr_Id")
            ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
            Return ds

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Function

    Protected Sub GetLeaveLogCount()
        Dim dsExistingLeaves As DataSet

        dsExistingLeaves = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[USR_SS].[GetAppliedLeaveTypeCount]", Nothing)

        Dim dt1 As DataTable = New DataTable
        dt1.Columns.Add("LeaveType")
        dt1.Columns.Add("Eligible")
        dt1.Columns.Add("Taken")
        dt1.Columns.Add("Balance")
        dt1.Columns.Add("BShowLeaveBalance")
        Session("LogtoPieChart") = ""

        Dim dsTypes As DataSet = bindLeaveTypes()

        If dsTypes.Tables.Count > 0 Then

            If (dsTypes.Tables(0).Rows.Count > 0) Then

                Dim rowal() As DataRow = dsTypes.Tables(0).Select("ELT_ID = 'AL'")
                Dim rowEl() As DataRow = dsTypes.Tables(0).Select("ELT_ID = 'EL'")
                Dim rowMl() As DataRow = dsTypes.Tables(0).Select("ELT_ID = 'ML'")
                Dim rowMPl() As DataRow = dsTypes.Tables(0).Select("ELT_ID = 'TL' OR ELT_ID = 'PL'")

                If (rowal.Length = 0) Then
                    dsTypes.Tables(0).Rows.Add("AL", "Annual Leave")
                ElseIf rowEl.Length = 0 Then
                    dsTypes.Tables(0).Rows.Add("EL", "Emergency Leave")
                ElseIf rowMl.Length = 0 Then
                    dsTypes.Tables(0).Rows.Add("ML", "Medical Leave")
                ElseIf rowMPl.Length = 0 Then

                End If

                For Each Row As DataRow In dsTypes.Tables(0).Rows
                    Dim pParms(6) As SqlClient.SqlParameter
                    pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                    pParms(0).Value = Session("EmployeeID")

                    pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                    pParms(1).Value = Today.Date

                    pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
                    pParms(2).Value = Row("ELT_ID")

                    pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                    pParms(3).Value = Session("sbsuid")

                    pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
                    pParms(4).Value = False

                    pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
                    If Row("ELT_ID") = "AL" Then
                        pParms(5).Value = False
                    Else
                        pParms(5).Value = True
                    End If
                    pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                    pParms(6).Value = DBNull.Value

                    Dim ds As New DataSet
                    Dim dtgrid As New DataTable
                    Dim adpt As New SqlDataAdapter
                    Using conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
                        Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddRange(pParms)
                        adpt.SelectCommand = cmd
                        adpt.Fill(ds)
                        For Each dt As DataTable In ds.Tables
                            If dt.Columns.Count > 2 Then
                                dtgrid = dt
                            End If
                        Next
                        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each Rowss As DataRow In ds.Tables(0).Rows
                                If Rowss("BShowLeaveBalance") = 1 Then
                                    dt1.Rows.Add(Rowss("Leave Type"), Rowss("Eligible"), Rowss("Taken"), Rowss("Balance"), Rowss("BShowLeaveBalance"))
                                Else

                                End If
                            Next
                        End If
                    End Using
                Next
                rptrLeaveLog.DataSource = dt1
                rptrLeaveLog.DataBind()

                For Each rows As DataRow In dt1.Rows
                    If rows("LeaveType") = "Annual Leave" Or rows("LeaveType") = "Medical Leave" Or rows("LeaveType") = "Emergency Leave" Then
                        If Session("LogtoPieChart") = "" Then
                            Session("LogtoPieChart") = rows("LeaveType") + ":" + rows("Taken")
                        Else
                            Session("LogtoPieChart") = Session("LogtoPieChart") + "|" + rows("LeaveType") + ":" + rows("Taken")
                        End If

                        If (rows("LeaveType") = "Annual Leave") Then
                            Session("LogtoPieChart") = Session("LogtoPieChart") + "|" + "Balance" + ":" + rows("Balance")
                        End If
                    End If
                Next
                If (Session("LogtoPieChart").ToString.IndexOf("Balance") = -1) Then
                    If Session("LogtoPieChart") = "" Then
                        Session("LogtoPieChart") = "Balance" + ":" + "30"
                    Else
                        Session("LogtoPieChart") = Session("LogtoPieChart") + "|" + "Balance" + ":" + "30"
                    End If
                End If
                If (Session("LogtoPieChart").ToString.IndexOf("Annual Leave") = -1) Then
                    If Session("LogtoPieChart") = "" Then
                        Session("LogtoPieChart") = "Annual Leave" + ":" + "0"
                    Else
                        Session("LogtoPieChart") = Session("LogtoPieChart") + "|" + "Annual Leave" + ":" + "0"
                    End If
                End If
                If (Session("LogtoPieChart").ToString.IndexOf("Medical Leave") = -1) Then
                    If Session("LogtoPieChart") = "" Then
                        Session("LogtoPieChart") = "Medical Leave" + ":" + "0"
                    Else
                        Session("LogtoPieChart") = Session("LogtoPieChart") + "|" + "Medical Leave" + ":" + "0"
                    End If
                End If
                If (Session("LogtoPieChart").ToString.IndexOf("Emergency Leave") = -1) Then
                    If Session("LogtoPieChart") = "" Then
                        Session("LogtoPieChart") = "Emergency Leave" + ":" + "0"
                    Else
                        Session("LogtoPieChart") = Session("LogtoPieChart") + "|" + "Emergency Leave" + ":" + "0"
                    End If
                End If
            End If
        End If
    End Sub

#End Region

#Region "SHARED FUNCTION"

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function DeleteDependantDetails(ByVal EDDID As String) As String
        Dim returnstring As String = ""
        Try
            Dim retval As String
            retval = EOS_EmployeeDependant.DeleteDependantDetail(EDDID)
            If (retval = "0" Or retval = "") Then
                returnstring = "0" + "||" + "Data Deleted Successfully "
            Else
                returnstring = retval + "||" + "Error occured!!"

            End If
        Catch ex As Exception
            returnstring = "1000" + "||" + ex.Message
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return returnstring

    End Function



    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function getLeaveApplicationHistory(ByVal myELAID As String) As String()
        Dim str_sql As String = ""
        Dim ds As DataSet
        Dim Sql As String = ""
        Dim DocType As String = "LEAVE"
        Dim myID As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim AppHistoryLevel As New List(Of String)()
        If Not myELAID.StartsWith("A") And Not myELAID.StartsWith("M") Then
            myID = myELAID
            DocType = "LEAVE"
        Else
            If myELAID.StartsWith("A") Or myELAID = "0" Then 'Not edited One
                myELAID = Replace(myELAID, "A", "")
                myID = Replace(myELAID, "A", "")
                DocType = "LEAVE"
            Else ' Edited record
                myID = Replace(myELAID, "M", "")
                Sql = "SELECT ELM_ELA_ID FROM EMPLEAVEAPP_MODIFIED where ELM_ID='" & Replace(myELAID, "M", "") & "'"
                myELAID = Mainclass.getDataValue(Sql, "OASISConnectionString")
                DocType = "LEAVE-M"
            End If
        End If
        str_sql = "SELECT  DISTINCT LAH.LAH_ELT_ID,ELT_DESCR,LAH.LAH_DTFROM,LAH.LAH_DTTO,LAH.LAH_REMARKS,"
        str_sql &= " CASE LAH_APPRSTATUS WHEN 'N' THEN 'PENDING FOR APPROVAL'"
        str_sql &= " WHEN 'A' THEN 'APPROVED'"
        str_sql &= " WHEN 'C' THEN 'CANCELLED'"
        str_sql &= " END AS APS_STATUS, LAH_LOGDT,CASE WHEN isnull(LAH_Action,'') ='C' THEN 'Applied For Cancellation' ELSE 'Applied For Amendment' END LAH_ACTION "
        str_sql &= " FROM    dbo.EMPLEAVEAPP_TRAN AS LAH"
        str_sql &= " INNER JOIN dbo.EMPLEAVETYPE_M  AS EL ON LAH.LAH_ELT_ID =EL.ELT_ID  "
        str_sql &= " WHERE LAH.LAH_ELA_ID = '" & Val(myELAID) & "'"
        str_sql &= " ORDER BY LAH_LOGDT ASC"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        Dim RowDateFrom As String = ""
        Dim RowDateTo As String = ""
        Dim RowLogDt As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            For Each Row In ds.Tables(0).Rows
                If (Row("LAH_DTFROM").ToString = "" Or Row("LAH_DTFROM") Is Nothing) Then
                    RowDateFrom = ""
                Else
                    RowDateFrom = Format(Row("LAH_DTFROM"), "dd MMM yyyy hh:mm:ss")
                End If
                If (Row("LAH_DTTO").ToString = "" Or Row("LAH_DTTO") Is Nothing) Then
                    RowDateTo = ""
                Else
                    RowDateTo = Format(Row("LAH_DTTO"), "dd MMM yyyy hh:mm:ss")
                End If

                If Row("LAH_LOGDT").ToString = "" Or Row("LAH_LOGDT") Is Nothing Then
                    RowLogDt = ""
                Else
                    RowLogDt = Format(Row("LAH_LOGDT"), "dd MMM yyyy hh:mm:ss")
                End If

                AppHistoryLevel.Add(String.Format("{0}||{1}||{2}||{3}||{4}||{5}||{6}", Row("ELT_DESCR"), RowDateFrom, RowDateTo, Row("LAH_REMARKS"), Row("APS_STATUS"), RowLogDt, Row("LAH_ACTION")))
            Next
        End If

        Return AppHistoryLevel.ToArray()
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function getLeaveApprovalLeavel(ByVal myELAID As String) As String()
        Dim str_sql As String = ""
        Dim Sql As String = ""
        Dim ds As DataSet
        Dim DocType As String = "LEAVE"
        Dim myID As String
        Dim ApprLevel As New List(Of String)()
        If Not myELAID.StartsWith("A") And Not myELAID.StartsWith("M") Then
            myID = myELAID
            DocType = "LEAVE"
        Else
            If myELAID.StartsWith("A") Or myELAID = "0" Then 'Not edited One
                myELAID = Replace(myELAID, "A", "")
                myID = Replace(myELAID, "A", "")
                DocType = "LEAVE"
            Else ' Edited record
                myID = Replace(myELAID, "M", "")
                Sql = "SELECT ELM_ELA_ID FROM EMPLEAVEAPP_MODIFIED where ELM_ID='" & Replace(myELAID, "M", "") & "'"
                myELAID = Mainclass.getDataValue(Sql, "OASISConnectionString")
                DocType = "LEAVE-M"
            End If
        End If



        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        str_sql = "select * from vw_GetApprovalDetail where APS_DOC_ID='" & myID & "' and APS_DOCTYPE='" & DocType & "' order by APS_ORDERID "

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        If ds.Tables(0).Rows.Count = 0 Then
            str_sql = "SELECT  ISNULL(EM.EMP_FNAME, '') + " _
            & " ISNULL(EM.EMP_MNAME, '') + ISNULL(EM.EMP_LNAME, '') AS APS_NAME," _
            & " 'REJECTED' AS APS_STATUS,''AS FINAL,'' AS PREV, " _
            & " AR.ARS_REMARKS AS APS_REMARKS, AR.ARS_DATE AS APS_DATE," _
            & " AR.ARS_DOC_ID   AS APS_DOC_ID" _
            & " FROM APPROVAL_REJECTION AS AR INNER JOIN" _
            & " EMPLOYEE_M AS EM ON AR.ARS_USR_ID = EM.EMP_ID" _
            & " WHERE  AR.ARS_bACTIVE =1  AND  AR.ARS_DOC_ID = '" & myELAID & "'"
            ds.Tables.Clear()
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)


        End If
        Dim RowDateApplied As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            For Each Row In ds.Tables(0).Rows
                Dim RowDate As String = ""
                If (Row("APS_DATE").ToString = "" Or Row("APS_DATE") Is Nothing) Then
                    RowDate = ""
                Else
                    RowDate = Format(Row("APS_DATE"), "dd MMM yyyy hh:mm:ss")
                End If

                If (Row("APS_ORDERID") = "1") Then
                    RowDateApplied = RowDate
                Else
                    ApprLevel.Add(String.Format("{0}||{1}||{2}||{3}||{4}", Row("APS_NAME"), Row("APS_STATUS"), Row("APS_REMARKS"), RowDateApplied, RowDate))
                End If


            Next
        Else

        End If


        Return ApprLevel.ToArray()
    End Function
    Public Shared Function DataSetToJSON(ByVal dt As DataTable) As String
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)
        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()
            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName, dr(col))
            Next
            rows.Add(row)
        Next
        Return serializer.Serialize(rows)
    End Function
    Private Shared Function GetLeaveMatrix2(ByVal StartDate As Date, ByVal Days As Decimal, ByVal BsuId As String, ByVal IsPlannerEnabled As Integer) As DataSet
        GetLeaveMatrix2 = Nothing

        If IsPlannerEnabled = 1 Then
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(0).Value = StartDate

            pParms(1) = New SqlClient.SqlParameter("@Days", SqlDbType.Int)
            pParms(1).Value = Days

            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = BsuId

            Dim ds As New DataSet
            Dim adpt As New SqlDataAdapter
            Using conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
                Dim cmd As New SqlCommand("[GetMyLeaveDays]", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddRange(pParms)
                adpt.SelectCommand = cmd
                adpt.Fill(ds)
                Return ds
            End Using
        End If
    End Function

    Private Shared Function GetBsuWeekEnd() As String
        Dim Wk1 As String = ""
        Dim Wk2 As String = ""
        Dim dr As SqlDataReader
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "BsuWeekEnd"
            params(1) = New SqlClient.SqlParameter("@Bsu_Id", SqlDbType.VarChar)
            params(1).Value = HttpContext.Current.Session("sbsuid")

            dr = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
            Do While dr.Read
                Wk1 = dr.Item("WeekEnd1")
                Wk2 = dr.Item("WeekEnd2")
            Loop
            If Not dr.IsClosed Then dr.Close()

            Return Wk1 + "||" + Wk2
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ""
        End Try

    End Function

    Private Shared Function GetLeaveTypeColorCodes() As DataSet
        Dim ds As New DataSet
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim params(0) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "LeaveTypeColorCodes"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
            If Not ds Is Nothing Then
                Return ds
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ds
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function ShowLeaveCalculationWeb(ByVal FromDate As Date, ByVal ToDate As Date) As String()
        '  GetBsuWeekEnd()
        Dim Weekend As String() = GetBsuWeekEnd.Split("||")
        Dim StartDate, EndDate, LeaveStartMonthStartDate, LeaveEndMonthEndDate, ReturnDate As Date
        Dim dsLeaveMatrix As DataSet = Nothing



        StartDate = CDate(FromDate)

        EndDate = CDate(ToDate)


        LeaveStartMonthStartDate = New Date(StartDate.Year, StartDate.Month, 1)
        LeaveEndMonthEndDate = New Date(EndDate.Year, EndDate.Month, Date.DaysInMonth(EndDate.Year, EndDate.Month))

        ' Dim ActualLeaveDays, RemainingDaysBeforeApplying, RemainingDays As Decimal
        Dim TotalLeaveDays As Decimal
        '  Dim ShowBalance As Boolean
        Dim TextToRender As String = ""
        ReturnDate = DateAdd(DateInterval.Day, 1, EndDate)
        TotalLeaveDays = DateDiff(DateInterval.Day, StartDate, EndDate) + 1
        If TotalLeaveDays = 0 Then
            Exit Function
        End If


        dsLeaveMatrix = GetLeaveMatrix2(StartDate, TotalLeaveDays, HttpContext.Current.Session("sbsuid"), 1)
        If Not dsLeaveMatrix Is Nothing Then
            TotalLeaveDays = dsLeaveMatrix.Tables(0).Rows.Count
            ReturnDate = DateAdd(DateInterval.Day, 1, CDate(dsLeaveMatrix.Tables(0).Rows(dsLeaveMatrix.Tables(0).Rows.Count - 1).Item("Date")))
        End If

        Dim ActualTakenLeaves As Integer = 0

        Dim tmpDays As Integer = 0
        tmpDays = DateDiff(DateInterval.Day, LeaveStartMonthStartDate, LeaveEndMonthEndDate) + 1
        dsLeaveMatrix = GetLeaveMatrix2(LeaveStartMonthStartDate, tmpDays, HttpContext.Current.Session("sbsuid"), 1)

        If Not dsLeaveMatrix Is Nothing Then
            For i = dsLeaveMatrix.Tables(0).Rows.Count - 1 To 0 Step -1
                If dsLeaveMatrix.Tables(0).Rows(i).Item("Date") > LeaveEndMonthEndDate Then
                    dsLeaveMatrix.Tables(0).Rows.RemoveAt(i)
                End If
            Next
            dsLeaveMatrix.AcceptChanges()

            'Add DayType, ColorCode, Status column to dsLeaveMatrixTable
            dsLeaveMatrix.Tables(0).Columns.Add("DayType", GetType(String))
            dsLeaveMatrix.Tables(0).Columns.Add("Back_Color", GetType(String))
            dsLeaveMatrix.Tables(0).Columns.Add("Fore_Color", GetType(String))
            dsLeaveMatrix.Tables(0).Columns.Add("Status", GetType(String))
            dsLeaveMatrix.AcceptChanges()
        End If


        Dim params(4) As SqlParameter
        params(0) = New SqlClient.SqlParameter("@Bsu_Id", SqlDbType.VarChar)
        params(0).Value = HttpContext.Current.Session("sbsuid")
        params(1) = New SqlClient.SqlParameter("@Emp_Id", SqlDbType.Int)
        params(1).Value = HttpContext.Current.Session("EmployeeID")
        params(2) = New SqlClient.SqlParameter("@Elt_Id", SqlDbType.VarChar)

        params(2).Value = "" 'get all leave types
        params(3) = New SqlClient.SqlParameter("@LeaveStartMonthStartDate", SqlDbType.VarChar)
        params(3).Value = Format(LeaveStartMonthStartDate, "dd/MMM/yy")
        params(4) = New SqlClient.SqlParameter("@LeaveEndMonthEndDate", SqlDbType.VarChar)
        params(4).Value = Format(LeaveEndMonthEndDate, "dd/MMM/yy")

        With SqlHelper.ExecuteReader(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString(), CommandType.StoredProcedure, "[USR_SS].[GetAlreadyAppliedLeaves]", params)

            Dim Days As Integer = 0
            Dim tmpRows() As DataRow

            Do While .Read

                Days = .Item("Days") + 1
                For i As Integer = 1 To Days
                    Dim row As DataRow = dsLeaveMatrix.Tables(0).NewRow
                    row.Item("Id") = dsLeaveMatrix.Tables(0).Rows.Count + 1
                    row.Item("Date") = Format(DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")), "dd/MMM/yy")
                    row.Item("Day") = DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")).DayOfWeek
                    row.Item("Remarks") = ""
                    row.Item("DayType") = .Item("ELA_ELT_Id")
                    tmpRows = dsLeaveMatrix.Tables(0).Select("Date = '" & Format(DateAdd(DateInterval.Day, i - 1, .Item("ELA_DTFROM")), "dd/MMM/yy") & "'")
                    If tmpRows.Length > 0 Then
                        For Each tmprow In tmpRows
                            If Not IsDBNull(tmprow.Item("Remarks")) AndAlso Not tmprow.Item("Remarks") = "" Then
                                If tmprow.Item("Remarks") = Weekend(0) Or tmprow.Item("Remarks") = Weekend(1) Then
                                    row.Item("DayType") = "WK"
                                Else
                                    row.Item("DayType") = "HOL_STAFF"
                                End If
                            End If
                        Next
                    End If
                    row.Item("Back_Color") = ""
                    row.Item("Fore_Color") = ""
                    row.Item("Status") = .Item("ELA_APPRSTATUS")
                    dsLeaveMatrix.Tables(0).Rows.Add(row)
                Next
            Loop
            'End If
            ''above end if by nahyan on 24dec2018
            If Not dsLeaveMatrix Is Nothing Then
                dsLeaveMatrix.AcceptChanges()
            End If
        End With

        ''Now update DayType column in dsLeaveMatrix dataset where DayType <> ""
        If Not dsLeaveMatrix Is Nothing Then
            For Each row As DataRow In dsLeaveMatrix.Tables(0).Rows
                If IsDBNull(row.Item("DayType")) OrElse row.Item("DayType") = "" Then
                    If Not row.Item("Remarks") Is Nothing Then
                        If row.Item("Remarks") = Weekend(0) Or row.Item("Remarks") = Weekend(1) Then
                            row.Item("DayType") = "WK" 'weekend
                        ElseIf row.Item("Remarks") <> "" Then
                            row.Item("DayType") = "HOL_STAFF" 'holiday
                        Else

                        End If
                    End If
                End If
            Next
            dsLeaveMatrix.AcceptChanges()
        End If
        Dim ColorCode As New List(Of String)()
        Dim dsColor As DataSet = GetLeaveTypeColorCodes()

        'If Not HttpContext.Current.Session("LeaveTypeColorCodes") Is Nothing Then
        '    dsColor = CType(HttpContext.Current.Session("LeaveTypeColorCodes"), DataSet)
        'End If

        Dim ColorIndex, TotalColors As Integer
        Dim ColorRows() As DataRow

        If Not dsColor Is Nothing And Not dsLeaveMatrix Is Nothing Then
            ColorRows = dsColor.Tables(0).Select("ELT_ID='HOL_STAFF'")
            TotalColors = ColorRows.Length

            For Each row As DataRow In dsLeaveMatrix.Tables(0).Rows
                If Not IsDBNull(row.Item("DayType")) AndAlso Not row.Item("DayType") Is Nothing Then
                    If row.Item("DayType") = "HOL_STAFF" Then
                        row.Item("Back_Color") = ColorRows(ColorIndex).Item("Back_Color")
                        row.Item("Fore_Color") = ColorRows(ColorIndex).Item("Fore_Color")


                    Else
                        Dim tmpRows() As DataRow = dsColor.Tables(0).Select("ELT_ID = '" & row.Item("DayType") & "'")
                        If tmpRows.Length > 0 Then
                            row.Item("Back_Color") = tmpRows(0).Item("Back_Color")
                            row.Item("Fore_Color") = tmpRows(0).Item("Fore_Color")
                        End If
                    End If
                End If
            Next
            dsLeaveMatrix.AcceptChanges()
        End If

        Dim Monthtocheck As String = ""
        If (StartDate.Month = 1) Then
            Monthtocheck = "Jan"
        ElseIf (StartDate.Month = 2) Then
            Monthtocheck = "Feb"
        ElseIf (StartDate.Month = 3) Then
            Monthtocheck = "Mar"
        ElseIf (StartDate.Month = 4) Then
            Monthtocheck = "Apr"
        ElseIf (StartDate.Month = 5) Then
            Monthtocheck = "May"
        ElseIf (StartDate.Month = 6) Then
            Monthtocheck = "Jun"
        ElseIf (StartDate.Month = 7) Then
            Monthtocheck = "Jul"
        ElseIf (StartDate.Month = 8) Then
            Monthtocheck = "Aug"
        ElseIf (StartDate.Month = 9) Then
            Monthtocheck = "Sep"
        ElseIf (StartDate.Month = 10) Then
            Monthtocheck = "Oct"
        ElseIf (StartDate.Month = 11) Then
            Monthtocheck = "Nov"
        ElseIf (StartDate.Month = 12) Then
            Monthtocheck = "Dec"
        End If
        'HttpContext.Current.Session("LeaveMatrix") = dsLeaveMatrix
        If dsLeaveMatrix.Tables(0).Rows.Count > 0 Then
            For Each Row In dsLeaveMatrix.Tables(0).Rows
                If (Row("Date").ToString.Contains(Monthtocheck) And Row("Date").ToString.Split("/")(2).Contains(StartDate.Year.ToString.Substring(2))) Then
                    ColorCode.Add(String.Format("{0}||{1}||{2}||{3}||{4}||{5}", Row("Date"), Row("Day"), Row("Remarks"), Row("Back_Color"), Row("Fore_Color"), Row("ID")))
                End If

            Next
        Else
            ' company.Add(String.Format("{0}-{1}", "", ""))
        End If

        Return ColorCode.ToArray


    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function ShowCalendarEvents(ByVal FromDate As String, ByVal ToDate As String) As String
        Dim tmpStr As String = ""
        Dim conn As SqlConnection = New SqlConnection(ConnectionManger.GetOASISConnectionString)
        conn.Open()
        Try
            'First render holidays

            Dim ds As DataSet
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = HttpContext.Current.Session("sbsuid")

            pParms(1) = New SqlClient.SqlParameter("@ECT_ID", SqlDbType.Int)
            pParms(1).Value = getemployeeDetails()

            pParms(2) = New SqlClient.SqlParameter("@From_Date", SqlDbType.Date)
            pParms(2).Value = FromDate

            pParms(3) = New SqlClient.SqlParameter("@To_Date", SqlDbType.Date)
            pParms(3).Value = ToDate




            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[EmpLeave_CalendarEvents_Holidays]", pParms)
            'ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[EmpLeave_CalendarEvents_Holidays]", pParms)

            Dim Hol_Back_Color, Hol_Fore_Color As String
            Dim ColorRows() As DataRow
            Dim ColorIndex, TotalColors As Integer
            Dim dscolor As DataSet = GetLeaveTypeColorCodes()
            If Not ds Is Nothing And Not dscolor Is Nothing Then
                'Dim tmpRows() As DataRow
                With dscolor
                    ColorRows = .Tables(0).Select("ELT_ID = 'HOL_STAFF'")
                    If ColorRows.Length > 0 Then
                        Hol_Back_Color = ColorRows(0).Item("Back_Color")
                        Hol_Fore_Color = ColorRows(0).Item("Fore_Color")
                    End If
                End With
            End If

            ColorIndex = 0
            TotalColors = ColorRows.Length - 1


            Dim KeepGeneratingRandomColors As Boolean
            If Hol_Back_Color = "RAND" Then
                KeepGeneratingRandomColors = True
            End If

            Randomize()
            For Each row As DataRow In ds.Tables(0).Rows
                If KeepGeneratingRandomColors Then
                    'Hol_Back_Color = "#ee541e"

                    Dim rand As New Random()


                    '"#A197B9"
                    Hol_Back_Color = "#A" & rand.Next(50, 254) & "B" & rand.Next(1, 9)
                End If

                Hol_Back_Color = ColorRows(ColorIndex).Item("Back_Color")
                ColorIndex += 1
                If ColorIndex >= TotalColors Then
                    ColorIndex = 0
                End If

                tmpStr &= "<div class=" & Chr(34) & "calEventBoxMain" & Chr(34) & ">"
                tmpStr &= "<div class=" & Chr(34) & "calEventBoxSub" & Chr(34) & ">"
                tmpStr &= "<div style=" & Chr(34) & "background-color:" & Hol_Back_Color & ";" & Chr(34) & " class=" & Chr(34) & "calEventColor" & Chr(34) & "></div> <div "
                tmpStr &= " class=" & Chr(34) & "calEventName" & Chr(34) & ">" & row.Item("Mod_Remarks") & "</div><div  class=" & Chr(34) & "calEventDay" & Chr(34) & ">" & IIf(row.Item("Days") <= 1, "&nbsp; 1 Day", Convert.ToInt32(row.Item("Days")) & " Days") & "</div></div>"
                tmpStr &= "<span class=" & Chr(34) & "calEventDate" & Chr(34) & ">Date : " & Format(row.Item("Mod_Date"), "dd/MMM/yy") & "</span>"
                tmpStr &= "</div>"
            Next

            'Now render applied leaves

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = HttpContext.Current.Session("sbsuid")

            pParms(1) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(1).Value = HttpContext.Current.Session("EmployeeID")

            pParms(2) = New SqlClient.SqlParameter("@From_Date", SqlDbType.Date)
            pParms(2).Value = FromDate

            pParms(3) = New SqlClient.SqlParameter("@To_Date", SqlDbType.Date)
            pParms(3).Value = ToDate


            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[USR_SS].[EmpLeave_CalendarEvents_AppliedLeaves]", pParms)

            Dim back_color As String = "#FF0000"
            If Not ds Is Nothing And Not dscolor Is Nothing Then
                Dim tmpRows() As DataRow
                For Each row As DataRow In ds.Tables(0).Rows
                    tmpRows = dscolor.Tables(0).Select("elt_id='" & row.Item("ela_elt_id") & "'")
                    If tmpRows.Length > 0 Then
                        back_color = tmpRows(0).Item("Back_Color")
                    End If
                    tmpStr &= "<div class=" & Chr(34) & "calEventBoxMain" & Chr(34) & ">"
                    tmpStr &= "<div class=" & Chr(34) & "calEventBoxSub" & Chr(34) & ">"
                    tmpStr &= "<div style=" & Chr(34) & "background-color:" & back_color & ";" & Chr(34) & " class=" & Chr(34) & "calEventColor" & Chr(34) & "></div> <div "
                    tmpStr &= " class=" & Chr(34) & "calEventName" & Chr(34) & ">" & row.Item("elt_descr") & "</div><div  class=" & Chr(34) & "calEventDay" & Chr(34) & ">" & "  (" & IIf(row.Item("ela_leavedays") <= 1, "&nbsp; 1 Day", row.Item("ela_leavedays") & " Days") & ")</div></div>"
                    tmpStr &= "<span class=" & Chr(34) & "calEventDate" & Chr(34) & ">Date : " & Format(row.Item("ela_dtfrom"), "dd/MMM/yy") & " to " & Format(row.Item("ela_dtto"), "dd/MMM/yy") & "</span>"
                    tmpStr &= "</div>"
                Next
            End If

            'Me.LiteralCalendarEvents.Text = tmpStr


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            tmpStr = ""
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
        Return tmpStr
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function a(ByVal Id As String) As String
        Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
        EDD_Detail = EOS_EmployeeDependant.GetDependantDetail(Id)
        Dim ReturnString As String = EDD_Detail.EDD_RELATION + "||" + Format(EDD_Detail.EDD_DOB, "dd MMM yyyy") + "||" + EDD_Detail.EDC_Gender + "||" + EDD_Detail.EDD_MARITALSTATUS.ToString + "||" +
            IIf(EDD_Detail.EDD_BSU_NAME = "0", "", EDD_Detail.EDD_BSU_NAME) + "||" + EDD_Detail.EDD_NAME + "||" + EDD_Detail.CTY_NATIONALITY + "||" + EDD_Detail.EDD_PASSPRTNO + "||" + EDD_Detail.EDC_DOCUMENT_NO + "||" + Format(DateTime.Parse(EDD_Detail.EDC_EXP_DT), "dd MMM yyyy") + "||" +
            EDD_Detail.EDD_UIDNO + "||" + Format(EDD_Detail.EDD_VISAISSUEDATE, "dd MMM yyyy") + "||" + Format(EDD_Detail.EDD_VISAEXPDATE, "dd MMM yyyy") + "||" + EDD_Detail.EDD_VISAISSUEPLACE + "||" + EDD_Detail.EDD_VISASPONSOR.ToString + "||" + EDD_Detail.EDD_INSURANCENUMBER + "||" + EDD_Detail.EDD_GEMS_TYPE + "||" + EDD_Detail.EDD_CTY_ID.ToString + "||" + EDD_Detail.EDD_GEMS_ID + "||" + EDD_Detail.EDD_BSU_ID + "||" + EDD_Detail.EDD_GEMS_ID + "||" + EDD_Detail.EDD_FILENO + "||" + EDD_Detail.EDD_RESIDENCE_CTY_ID.ToString + "||" + EDD_Detail.EDD_GEMS_STAFF_STU_NO 'last two params Added by vikranth on 4th mar 2020
        Return ReturnString
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetActualDays(ByVal StartDate As String, ByVal EndDate As String, ByVal Selectedval As String) As String
        Try
            If StartDate = "" And EndDate = "" Then
                Return "0"
            Else
                Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & StartDate & "','" & EndDate & "','" & HttpContext.Current.Session("sbsuid") & "'," & EOS_MainClass.GetEmployeeIDFromUserName(HttpContext.Current.Session("sUsr_name")) & ",'" & Selectedval & "') as days"
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
                Dim days As String = ds.Tables(0).Rows(0).Item("days")
                Return days
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ""
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetLeaveTypes() As String()
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As DataSet
        Dim LeaveTypes As New List(Of String)()
        'Dim query As String = "Select isnull(EMP_SEX_bMALE,0) as emp_sex_bmale From Employee_M Where Emp_Id = (Select Usr_emp_id From Users_m where usr_id ='" & Session("sUsr_Id") & "')"
        Try
            Dim params(1) As SqlParameter
            params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
            params(0).Value = "LeaveTypes"
            params(1) = New SqlClient.SqlParameter("@Usr_Id", SqlDbType.VarChar)
            params(1).Value = HttpContext.Current.Session("sUsr_Id")
            ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)

            If ds.Tables(0).Rows.Count > 0 Then
                HttpContext.Current.Session("hidLoadValue") = ds.Tables(0).Rows(0)("ELT_ID")
                For Each Row In ds.Tables(0).Rows
                    LeaveTypes.Add(String.Format("{0}||{1}", Row("ELT_DESCR"), Row("ELT_ID")))
                Next
            Else
                ' company.Add(String.Format("{0}-{1}", "", ""))
            End If
            Return LeaveTypes.ToArray()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetAreas(ByVal SelectedEmirateID As String) As String()
        Dim ds As New DataSet
        GetAreas = Nothing
        Dim Areas As New List(Of String)()
        Try
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@CTY_ID", 172)
            param(1) = New SqlClient.SqlParameter("@EMIRATE_ID", SelectedEmirateID)
            param(2) = New SqlClient.SqlParameter("@INFO_TYPE", "AREA")
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
            CommandType.StoredProcedure, "GET_EMIRATE_CITY", param)
            If ds.Tables(0).Rows.Count > 0 Then
                For Each Row In ds.Tables(0).Rows
                    Areas.Add(String.Format("{0}||{1}", Row("EMA_DESCR"), Row("EMA_ID")))
                Next
            Else
                ' company.Add(String.Format("{0}-{1}", "", ""))
            End If
            Return Areas.ToArray()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return Nothing
        End Try
    End Function
    Public Shared Function SaveDocument(ByVal fuUpload As String, ByVal con As SqlConnection, tran As SqlTransaction, ByVal PCD_ID As String, ByVal bsuid As String, ByVal fileext As String, ByVal filename As String) As Boolean
        Try


            Dim contenttype As String = String.Empty


            Select Case fileext.ToLower()
                Case ".doc"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".docx"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".xls"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".xlsx"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".jpg"

                    contenttype = "image/jpg"
                    Exit Select

                Case ".jpeg"

                    contenttype = "image/jpeg"
                    Exit Select

                Case ".tif"

                    contenttype = "image/tiff"
                    Exit Select

                Case ".png"

                    contenttype = "image/png"
                    Exit Select

                Case ".gif"

                    contenttype = "image/gif"
                    Exit Select

                Case ".pdf"

                    contenttype = "application/pdf"
                    Exit Select



            End Select

            If contenttype <> String.Empty Then

                Dim bitmapData() As Byte
                Dim sbText As StringBuilder = New StringBuilder(fuUpload, fuUpload.Length)
                sbText.Replace("\r\n", String.Empty)
                sbText.Replace(" ", String.Empty)
                Dim firststring As String = fuUpload.Substring(0, fuUpload.IndexOf(",") + 1)
                sbText.Replace(firststring, String.Empty)
                bitmapData = Convert.FromBase64String(sbText.ToString())

                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", "LEAVE")
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bitmapData)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", bsuid)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Transaction = tran
                cmd.ExecuteNonQuery()
                Return True

            Else
                '   lblmsg.CssClass = "divInValidMsg"
                '  lblmsg.Text = "<i class='fa fa-2x fa-info text-danger v-align-middle mr-3'></i> File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function SaveLeaveA(ByVal ELAID As String, ByVal bsuid As String, ByVal fromdate As String, ByVal todate As String, ByVal leavetype As String, ByVal usrname As String, ByVal remarks As String, ByVal address As String, ByVal mobile As String, ByVal handover As String, ByVal filepath As String, ByVal fileext As String, ByVal filename As String) As String
        Dim returnstring As String = ""

        If fromdate = "0" Or fromdate = "" Or fromdate Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter from date"
            Return returnstring
        ElseIf todate = "0" Or todate = "" Or todate Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter to date"
            Return returnstring
        ElseIf leavetype = "" Or leavetype Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please select leave type"
            Return returnstring
        ElseIf remarks = "" Or remarks Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter your remarks"
            Return returnstring
            'Commented by vikranth on 13th Feb 2020 for removing mandatory filed check, as confirmed by Charan  
            'ElseIf handover = "" Or handover Is Nothing Then
            '    returnstring = "1000" + "||" + "0" + "||" + "Please enter your handover details"
            '    Return returnstring
            'ElseIf address = "" Or address Is Nothing Then
            '    returnstring = "1000" + "||" + "0" + "||" + "Please enter your address"
            '    Return returnstring
            'End by vikranth on 13th Feb 2020
        End If

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim retMsg As String = ""
            Dim bIsLeaveExceeded As Boolean = False

            If leavetype = "AL" Then
                Dim pParms(7) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
                pParms(0).Value = EOS_MainClass.GetEmployeeIDFromUserName(usrname)

                pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
                pParms(1).Value = Today.Date

                pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)
                pParms(2).Value = leavetype

                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = bsuid

                pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
                pParms(4).Value = False
                pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
                pParms(5).Value = False
                If (ELAID = "0") Then
                    pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                    pParms(6).Value = DBNull.Value
                Else
                    pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
                    pParms(6).Value = ELAID

                End If
                pParms(7) = New SqlClient.SqlParameter("@ELA_FROMDT", SqlDbType.DateTime)
                pParms(7).Value = fromdate.Trim

                Dim intLeaveBalnce, days As Integer
                Dim ds As New DataSet
                Dim dtgrid As New DataTable
                'Dim adpt As New SqlDataAdapter
                Dim str_conn2 = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                'Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                '    Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
                '    cmd.CommandType = CommandType.StoredProcedure
                '    cmd.Parameters.AddRange(pParms)
                '    adpt.SelectCommand = cmd
                '    adpt.Fill(ds)
                'End Using

                ds = SqlHelper.ExecuteDataset(str_conn2, CommandType.StoredProcedure, "[EmpLeaveStatusWhileApplying]", pParms)

                If ds.Tables(0).Rows.Count > 0 Then
                    intLeaveBalnce = ds.Tables(0).Rows(0).Item("Balance")
                End If
                'intLeaveBalnce = 10

                Dim Sql_Query = "select [dbo].[fn_GetLeaveDaysCount]('" & CDate(fromdate) & "','" & CDate(todate) & "','" & bsuid & "'," & EOS_MainClass.GetEmployeeIDFromUserName(usrname) & ",'" & leavetype & "') as days"
                Dim ds2 As New DataSet
                ds2 = SqlHelper.ExecuteDataset(str_conn2, CommandType.Text, Sql_Query)
                ' Dim days As String = DateDiff(DateInterval.Day, CDate(txtFrom.Text), CDate(txtTo.Text)) + 1
                days = ds2.Tables(0).Rows(0).Item("days")

                If (days > intLeaveBalnce) Then
                    bIsLeaveExceeded = True
                Else
                    bIsLeaveExceeded = False
                End If

            Else

            End If

            If bIsLeaveExceeded Then
                retval = 2580

            Else
                retval = 0
            End If

            If retval = 0 Then
                Dim pParms(19) As SqlClient.SqlParameter 'V1.2 ,V1.3
                pParms(0) = Mainclass.CreateSqlParameter("@ELA_BSU_ID", bsuid, SqlDbType.VarChar)
                pParms(1) = Mainclass.CreateSqlParameter("@ELA_EMP_ID", EOS_MainClass.GetEmployeeIDFromUserName(usrname), SqlDbType.VarChar)
                pParms(2) = Mainclass.CreateSqlParameter("@ELA_ELT_ID", leavetype, SqlDbType.VarChar)
                pParms(3) = Mainclass.CreateSqlParameter("@ELA_DTFROM", fromdate, SqlDbType.DateTime)
                pParms(4) = Mainclass.CreateSqlParameter("@ELA_DTTO", todate, SqlDbType.DateTime)
                pParms(5) = Mainclass.CreateSqlParameter("@ELA_APPRSTATUS", "N", SqlDbType.VarChar)
                If ELAID = "0" Then
                    pParms(6) = Mainclass.CreateSqlParameter("@bEdit", False, SqlDbType.Bit)
                Else
                    pParms(6) = Mainclass.CreateSqlParameter("@bEdit", True, SqlDbType.Bit)
                End If
                pParms(7) = Mainclass.CreateSqlParameter("@ELA_STATUS", 0, SqlDbType.Int)
                'pParms(8) = Mainclass.CreateSqlParameter("@return_value", "0", SqlDbType.Int, True)
                pParms(9) = Mainclass.CreateSqlParameter("@ELA_REMARKS", remarks, SqlDbType.VarChar)
                If ELAID = "0" Then
                    pParms(10) = Mainclass.CreateSqlParameter("@ELA_ID", 0, SqlDbType.Int, True)
                Else
                    pParms(10) = Mainclass.CreateSqlParameter("@ELA_ID", Convert.ToInt32(ELAID), SqlDbType.Int, True)
                End If
                pParms(11) = Mainclass.CreateSqlParameter("@ELA_ADDRESS", address, SqlDbType.VarChar)
                pParms(12) = Mainclass.CreateSqlParameter("@ELA_PHONE", mobile, SqlDbType.VarChar)
                pParms(13) = Mainclass.CreateSqlParameter("@ELA_MOBILE", mobile, SqlDbType.VarChar)
                pParms(14) = Mainclass.CreateSqlParameter("@ELA_POBOX", "0", SqlDbType.VarChar)
                pParms(15) = Mainclass.CreateSqlParameter("@APS_bFORWARD", True, SqlDbType.Bit)
                pParms(16) = Mainclass.CreateSqlParameter("@UserName", usrname, SqlDbType.VarChar)
                pParms(17) = Mainclass.CreateSqlParameter("@Handovertxt", handover, SqlDbType.VarChar)
                pParms(18) = Mainclass.CreateSqlParameter("@RetMsg", "", SqlDbType.VarChar, True, 200)
                pParms(19) = Mainclass.CreateSqlParameter("@RetVal", "", SqlDbType.VarChar, True, 200)
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SaveEMPLEAVEAPP_V2", pParms)
                retval = pParms(19).Value.ToString & "||" & pParms(18).Value.ToString
                '  p_ELA_ID = pParms(10).Value.ToString


                Dim separator As String() = New String() {"||"}
                Dim myValarr() As String = retval.Split(separator, StringSplitOptions.None)
                retval = myValarr(0)
                If myValarr.Length > 1 Then
                    retMsg = myValarr(1)
                Else
                    retMsg = ""
                End If

                If Not filepath Is Nothing Then
                    If (filepath <> "") Then
                        If Not SaveDocument(filepath, objConn, stTrans, pParms(10).Value, bsuid, fileext, filename) Then
                            retval = "991"
                        End If
                    End If
                End If


                If retval = "0" Then

                    stTrans.Commit()
                    returnstring = retval + "||" + pParms(10).ToString + "||" + UtilityObj.getErrorMessage(7070)

                Else
                    If pParms(18).Value <> "" Then
                        returnstring = retval + "||" + pParms(10).ToString + "||" + pParms(18).Value
                    Else
                        returnstring = retval + "||" + pParms(10).ToString + "||" + UtilityObj.getErrorMessage(retval)
                    End If

                    stTrans.Rollback()
                End If
            Else
                returnstring = retval + "||" + "0" + "||" + UtilityObj.getErrorMessage(retval)
                stTrans.Rollback()
                'Leave Exceeded
            End If
        Catch ex As Exception
            If ex.Message <> "" Then
                returnstring = "1000" + "||" + "0" + "||" + ex.Message
            Else
                returnstring = "1000" + "||" + "0" + "||" + UtilityObj.getErrorMessage(1000)
            End If

            stTrans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return returnstring
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function savePnODocReq(ByVal ReqID As String, ByVal DocNumber As String, ByVal AddressTo As String, ByVal empNotes As String, ByVal AddressNotes As String, ByVal usrName As String, ByVal bsuid As String, ByVal docDAXID As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        'Dim objConn As New SqlConnection(str_conn) '

        If DocNumber = "" Or DocNumber = "0" Or DocNumber Is Nothing Then
            Return "1000" + "||" + "Please select document type"
        ElseIf AddressTo = "" Or AddressTo Is Nothing Then
            Return "1000" + "||" + "Please enter Address To "
        ElseIf empNotes = "" Or empNotes Is Nothing Then
            Return "1000" + "||" + "Please enter employee notes "
        ElseIf AddressNotes = "" Or AddressNotes Is Nothing Then
            Return "1000" + "||" + "Please enter address notes "
        End If

        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()

        Dim str_err As String = "1000"
        Dim lstrNewDocNo As String = ""
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try


            '''''
            Dim str_conn1 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim empid As String
            Dim empbsucode As String
            Dim empnum As String
            Dim empbsu As String
            Dim ds As New DataSet
            str_Sql = "SELECT " _
             & " EM.EMP_ID, ISNULL(EM.EMP_FNAME,'')+' '+" _
             & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
             & " AS EMP_NAME,EMPNO,EMP_BSU_ID ,bsu_shortname FROM  EMPLOYEE_M EM INNER JOIN USERS_M" _
             & " ON EM.EMP_ID=USERS_M.USR_EMP_ID inner join businessunit_m on bsu_id=emp_bsu_id " _
             & " WHERE USERS_M.USR_NAME ='" & HttpContext.Current.Session("sUsr_name") & "' and em.emp_bsu_id ='" & HttpContext.Current.Session("sBsuid") & "'"
            'V1.4
            ds = SqlHelper.ExecuteDataset(str_conn1, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then

                empid = ds.Tables(0).Rows(0)("EMP_ID")
                empbsu = ds.Tables(0).Rows(0)("emp_bsu_id")
                empnum = ds.Tables(0).Rows(0)("EMPNO")
                empbsucode = ds.Tables(0).Rows(0)("bsu_shortname")
            End If




            ''''''



            Dim axrecordId As String = SaveDocumentDetailstoAX(AddressTo, empNotes, AddressNotes, DocNumber, empnum, empbsucode, docDAXID)
            Dim SqlCmd As New SqlCommand("DAX.SaveEMP_PnODocumentRequests", objConn, stTrans)
            SqlCmd.CommandType = CommandType.StoredProcedure

            Dim pParms(8) As SqlClient.SqlParameter 'V1.2 ,V1.3
            If ReqID = 0 Then
                SqlCmd.Parameters.AddWithValue("@EPnO_REQUEST_ID", 0)
            Else
                SqlCmd.Parameters.AddWithValue("@EPnO_REQUEST_ID", Convert.ToInt32(ReqID))
            End If

            SqlCmd.Parameters.AddWithValue("@EPnO_PnO_DOC_ID", Convert.ToInt16(DocNumber))
            SqlCmd.Parameters.AddWithValue("@EPnO_ADDRESS_TO", AddressTo)
            SqlCmd.Parameters.AddWithValue("@EPnO_EMP_NOTES", empNotes)
            'SqlCmd.Parameters.AddWithValue("@EPnO_HR_NOTES", p_EPnO_HR_NOTES)
            SqlCmd.Parameters.AddWithValue("@EPnO_ADDRESS_NOTES", AddressNotes)
            SqlCmd.Parameters.AddWithValue("@EPnO_EMP_ID", EOS_MainClass.GetEmployeeIDFromUserName(usrName))
            SqlCmd.Parameters.AddWithValue("@EPnO_BSU_ID", bsuid)
            SqlCmd.Parameters.AddWithValue("@EPnO_AX_RECID", axrecordId)

            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            str_err = CInt(SqlCmd.Parameters("@ReturnValue").Value)

            'lstrNewDocNo = CStr(SqlCmd.Parameters("@VHH_NEWDOCNO").Value)

            SqlCmd.Parameters.Clear()
            If str_err = "0" Then
                stTrans.Commit()
                Return "0" + "||" + UtilityObj.getErrorMessage(0)
            Else
                stTrans.Rollback()
                Return str_err + "||" + UtilityObj.getErrorMessage(str_err)
            End If

            'SaveEMPPnO_DocumentRequest = pParms(19).Value.ToString & "||" & pParms(18).Value.ToString
        Catch ex As Exception

            stTrans.Rollback()
            ' Errorlog(ex.Message)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "1000" + "||" + UtilityObj.getErrorMessage(1000) + ", " + ex.Message
        Finally
            objConn.Close() 'Finally, close the connection

        End Try
        Return "1000" + "||" + UtilityObj.getErrorMessage(1000)
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function DeleteFileFromDB(ByVal DocId As String) As String
        Try

            Dim conn As String = ""

            conn = ConfigurationManager.ConnectionStrings("OASIS_DOCSConnectionString").ConnectionString

            Dim objcon As SqlConnection = New SqlConnection(conn)
            objcon.Open()
            Dim cmd As New SqlCommand("DeleteDocumentFromDB", objcon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@DOC_ID", DocId)
            cmd.Parameters.Add("@retVal", SqlDbType.BigInt)
            cmd.Parameters("@retVal").Direction = ParameterDirection.ReturnValue
            cmd.ExecuteNonQuery()
            objcon.Close()
            Dim ret As Integer = 0
            ret = cmd.Parameters("@retVal").Value

            If ret <> 0 Then
                Return "1000"

            Else
                Return "0"

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "1000"

        End Try

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function getLeaveD(ByVal bsuid As String, ByVal SelectVal As String, ByVal usrname As String) As String
        Dim dsLeaveStatus As New DataSet
        Dim Returnstring As String = ""
        Try
            dsLeaveStatus.Tables.Add()
            dsLeaveStatus.Tables(0).Columns.Add("elt_id", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("elt_descr", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("bShow_Details", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("Tot_Taken", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("Tot_Balance", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("bPrev_Record", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("elt_Order", GetType(Integer))
            dsLeaveStatus.Tables(0).Columns.Add("Elt_BackColor", GetType(String))
            dsLeaveStatus.Tables(0).Columns.Add("Elt_ForeColor", GetType(String))
            dsLeaveStatus.AcceptChanges()


            Dim tmpDs, dsExistingLeaves As DataSet

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim objConn As New SqlConnection(str_conn) ' 
            dsExistingLeaves = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[USR_SS].[GetAppliedLeaveTypeCount]", Nothing)


            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
            pParms(0).Value = EOS_MainClass.GetEmployeeIDFromUserName(usrname)

            pParms(1) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
            pParms(1).Value = Today.Date

            pParms(2) = New SqlClient.SqlParameter("@ELT_ID", SqlDbType.VarChar, 10)

            pParms(2).Value = SelectVal

            pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(3).Value = bsuid

            pParms(4) = New SqlClient.SqlParameter("@BDetailed", SqlDbType.Bit)
            pParms(4).Value = False

            pParms(5) = New SqlClient.SqlParameter("@IsApproval", SqlDbType.Bit)
            If SelectVal = "AL" Then
                pParms(5).Value = False
            Else
                pParms(5).Value = True
            End If
            pParms(6) = New SqlClient.SqlParameter("@Ela_ID", SqlDbType.Int)
            pParms(6).Value = DBNull.Value


            Dim ds As New DataSet
            Dim dtgrid As New DataTable
            Dim adpt As New SqlDataAdapter
            ds = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString(),
                                          CommandType.StoredProcedure, "[EmpLeaveStatusWhileApplying]", pParms)

            'Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            '    Dim cmd As New SqlCommand("[EmpLeaveStatusWhileApplying]", conn)
            '    cmd.CommandType = CommandType.StoredProcedure
            '    cmd.Parameters.AddRange(pParms)
            '    adpt.SelectCommand = cmd
            '    adpt.Fill(ds)
            For Each dt As DataTable In ds.Tables
                If dt.Columns.Count > 2 Then
                    dtgrid = dt
                End If
            Next
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Returnstring = SelectVal + "||" + Convert.ToDecimal(ds.Tables(0).Rows(0).Item("Eligible")).ToString + "||" + Convert.ToDecimal(ds.Tables(0).Rows(0).Item("Taken")).ToString + "||" + Convert.ToDecimal(ds.Tables(0).Rows(0).Item("Balance")).ToString + "||" + ds.Tables(0).Rows(0).Item("BShowLeaveBalance").ToString
            Else
                Returnstring = SelectVal + ""
            End If
            tmpDs = ds
            'End Using
            Return Returnstring
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ""
        End Try
        Return ""
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetPnODocDtls(ByVal ID As String, Usrname As String, Bsuid As String) As String
        Try
            Dim returnstring As String = ""
            returnstring = "0"
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            'str_Sql = " exec GetLeaveDetails '" & Session("SbSUID") & "'"     'V1.1 comment
            str_Sql = " exec DAX.GetEmployeePnO_DocumentsRequestList '" & Bsuid & "','" & EOS_MainClass.GetEmployeeIDFromUserName(Usrname) & "','" & ID & "'" ' & Session("SbSUID") & "','" & Session("sUsr_name") & "'"    'V1.1
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                returnstring = returnstring + "||" + ds.Tables(0).Rows(0)("EPnO_PnO_DOC_ID").ToString + "||" + ds.Tables(0).Rows(0)("EPnO_AX_RECID").ToString + "||" + ds.Tables(0).Rows(0)("EPnO_ADDRESS_NOTES").ToString + "||" + ds.Tables(0).Rows(0)("EPnO_ADDRESS_TO").ToString + "||" + ds.Tables(0).Rows(0)("EPnO_EMP_NOTES").ToString + "||" + ds.Tables(0).Rows(0)("PnOStatus_Descr").ToString
                Return returnstring
            Else
                Return "1000"
            End If
            returnstring = returnstring + ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "1000"
        End Try
    End Function
    Public Shared Function getemployeeDetails() As String

        Dim ds As New DataSet()

        Dim params(2) As SqlParameter
        params(0) = New SqlClient.SqlParameter("@Fetch_Type", SqlDbType.VarChar)
        params(0).Value = "EmpLeaveAppDetails"
        params(1) = New SqlClient.SqlParameter("@Emp_Id", SqlDbType.VarChar)
        params(1).Value = HttpContext.Current.Session("EmployeeID")
        params(2) = New SqlClient.SqlParameter("@Bsu_Id", SqlDbType.VarChar)
        params(2).Value = HttpContext.Current.Session("sbsuid")
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "[USR_SS].[SelfService_Lookup]", params)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("EMP_ECT_ID")
        Else
            Return ""
        End If
    End Function



    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GetLeaveDtls(ByVal ELAID As String, Usrname As String, Bsuid As String) As String
        Try
            Dim IsELA As Boolean = False
            Dim returnstring As String = ""
            Dim ds As New DataSet
            returnstring = "0"
            Dim str_Sql1 As String = ""
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            '  V1 0.2   'IsNull(EM.EMP_LASTREJOINDT, EM.EMP_JOINDT) changed to EMP_BSU_JOINDT
            str_Sql = "SELECT   ELA.ELA_ID, ELA.ID, ELA.ELA_EMP_ID," _
             & " ELA.ELA_ELT_ID,ELA.ELA_HANDOVERTXT, ELA.ELA_DTFROM, ELA.ELA_DTTO, " _
             & " ELA.ELA_REMARKS, ELA.ELA_ADDRESS, ELA.ELA_PHONE, " _
             & " ELA.ELA_MOBILE, ELA.ELA_POBOX,ELA.ELA_APPRSTATUS,isnull(EM.emp_fname,'')+'  '+isnull(EM.emp_mname,'')+'  '+isnull(EM.emp_lname,'') as EMP_NAME,IsNull(EM.EMP_BSU_JOINDT, EM.EMP_JOINDT) EMP_LASTREJOINDT,ELA_bFORWARD,IsNull(EM.EMP_ECT_ID,0) EMP_ECT_ID, CASE ELA_bFORWARD WHEN 0 THEN 'yes' else 'no' end as canedit,Isnull(FD.DOC_ID,0) DOC_ID ," _
             & " isnull(ELA_bReqCancellation,0) ReqCanc,case when isnull(ELA_APPRSTATUS,'')='C' then 1 else 0 end as IsCancelled   " _
             & " FROM VW_EmployeeLeaveApp AS ELA INNER JOIN" _
             & " EMPLOYEE_M AS EM ON ELA.ELA_EMP_ID = EM.EMP_ID left outer join OASIS_DOCS.FIN.FDOCUMENTS FD ON " _
             & "ELA.ELA_ID=FD.DOC_PCD_ID AND FD.DOC_TYPE='LEAVE' "
            If IsELA Then
                str_Sql &= " WHERE ELA.ELA_ID='" & ELAID & "' "
            Else
                str_Sql &= " WHERE ELA.ID='" & ELAID & "' "
            End If
            '  str_Sql &= " WHERE ELA.ELA_ID='" & ELAID & "' "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ' str_Sql &= " WHERE ELA.ELA_ID='" & p_Modifyid & "' "
            Dim Encr_decrData As New Encryption64
            Dim KEYS As String = Encr_decrData.Encrypt("PCDOCDOWNLOAD")
            Dim FILE As String = Encr_decrData.Encrypt(ds.Tables(0).Rows(0)("DOC_ID"))

            If (ds.Tables(0).Rows.Count > 0) Then
                ' HttpContext.Current.Session("EMP_ECT_ID") = ds.Tables(0).Rows(0)("EMP_ECT_ID")
                returnstring = returnstring + "||" + ds.Tables(0).Rows(0)("ELA_ELT_ID") + "||" + Format(ds.Tables(0).Rows(0)("ELA_DTFROM"), "dd MMM yyyy") + "||" + Format(ds.Tables(0).Rows(0)("ELA_DTTO"), "dd MMM yyyy") + "||" + ds.Tables(0).Rows(0)("ELA_REMARKS") + "||" + ds.Tables(0).Rows(0)("ELA_HANDOVERTXT") + "||" + ds.Tables(0).Rows(0)("ELA_ADDRESS") + "||" + ds.Tables(0).Rows(0)("ELA_MOBILE") + "||" + KEYS + "||" + FILE + "||" + ds.Tables(0).Rows(0)("DOC_ID").ToString
            Else
                Return "1000"
            End If

            Return returnstring
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "1000"
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCountry(ByVal prefix As String) As String()
        GetCountry = Nothing
        Dim County As New List(Of String)()
        Dim str_Sql As String = ""
        If prefix <> "" Then
            str_Sql = "Select ID, E_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as E_Name FROM COUNTRY_M)a where a.ID<>'' and E_Name <> '-' and a.ID not in (0,5,252) and a.E_Name like '%" + prefix + "%'"
        Else : str_Sql = "Select ID, E_Name from(SELECT  CTY_ID as ID,CTY_NATIONALITY as E_Name FROM COUNTRY_M)a where a.ID<>'' and E_Name <> '-' and a.ID not in (0,5,252) order by E_Name ASC"
        End If

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql '& str_orderby
                'cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        County.Add(String.Format("{0}||{1}", sdr("E_Name"), sdr("ID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return County.ToArray()
        End Using
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetStudents(ByVal Type As String, ByVal prefix As String, ByVal COMP_ID As String, ByVal ACD_ID As String, ByVal SearchFor As String) As String()
        If (Type = "Gems Student") Then
            Return GetStudentComp(COMP_ID, ACD_ID, SearchFor, prefix)
        ElseIf (Type = "Gems Staff") Then
            Return GetEmployee(COMP_ID, ACD_ID, SearchFor, prefix)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetEmployee(ByVal COMP_ID As String, ByVal ACD_ID As String, ByVal SearchFor As String, ByVal prefix As String) As String()
        GetEmployee = Nothing
        Dim Employee As New List(Of String)()
        Dim str_Sql As String = ""
        If SearchFor = "NAME" Then
            str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M )a where a.bActive=1 and a.BSU='" & COMP_ID & "' and a.id<>'' and a.E_name like '%" + prefix + "%'"
        ElseIf SearchFor = "ID" Then
            str_Sql = "Select ID,No,E_Name from(Select emp_ID as ID,ISNULL(EMPNO,'') No, isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M )a where a.bActive=1 and a.BSU='" & COMP_ID & "' and a.id<>'' and a.NO like '%" + prefix + "%'"
        End If


        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql '& str_orderby
                'cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If SearchFor = "ID" Then
                            Employee.Add(String.Format("{0}||{1}||{2}||{3}||{4}", sdr("No"), sdr("ID"), sdr("E_Name"), "", ""))
                        ElseIf SearchFor = "NAME" Then
                            Employee.Add(String.Format("{0}||{1}||{2}||{3}||{4}", sdr("E_Name"), sdr("ID"), sdr("No"), "", ""))
                        End If
                    End While
                End Using
                conn.Close()
            End Using
            Return Employee.ToArray()
        End Using

    End Function

    Public Shared Function GetStudentComp(ByVal COMP_ID As String, ByVal ACD_ID As String, ByVal SearchFor As String, ByVal prefix As String) As String()
        GetStudentComp = Nothing
        Dim Student As New List(Of String)()
        Dim str_Sql As String = ""
        str_Sql = " SELECT TOP 20 STU_ID, STU_NO, STU_BSU_ID, STU_NAME, STU_GENDER,REPLACE(PARENT_MOBILE, '-', '') AS PARENT_MOBILE," _
                 & " PARENT_NAME ,STU_ACD_ID, ACY_DESCR, STU_GRD_ID FROM VW_OSO_STUDENT_M  WHERE" _
                 & " CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN' and STU_bActive=1 AND " _
                 & "  STU_BSU_ID='" & COMP_ID & "' "

        If SearchFor IsNot Nothing Then
            If SearchFor = "ID" Then
                str_Sql &= " AND STU_NO LIKE '%" & prefix & "%' "
                ' str_orderby = " ORDER BY a.STU_NO "
            ElseIf SearchFor = "NAME" Then
                str_Sql &= " AND STU_NAME LIKE '%" & prefix & "%' "
                ' str_orderby = " ORDER BY LTRIM(RTRIM(a.STU_NAME)) "
            End If
        End If

        Using conn As New SqlConnection()
            conn.ConnectionString = ConnectionManger.GetOASISConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = str_Sql '& str_orderby

                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        If SearchFor = "ID" Then
                            Student.Add(String.Format("{0}||{1}||{2}||{3}", sdr("STU_NO"), sdr("STU_ID"), sdr("STU_NAME"), sdr("STU_GRD_ID")))
                        ElseIf SearchFor = "NAME" Then
                            Student.Add(String.Format("{0}||{1}||{2}||{3}", sdr("STU_NAME"), sdr("STU_ID"), sdr("STU_NO"), sdr("STU_GRD_ID")))
                        End If
                    End While
                End Using
                conn.Close()
            End Using
            Return Student.ToArray()
        End Using

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCompany(ByVal prefix As String, ByVal userName As String, ByVal SearchFor As String) As String()
        Dim company As New List(Of String)()

        Dim ds As New DataSet

        Dim str_Sql As String = " exec [dbo].[GET_ESS_SCHOOLS] '" & 1 & "','" & SearchFor & "','" & prefix & "'"
        Try
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        Catch ex As Exception

        End Try


        '  If (prefix <> "") Then
        '      If SearchFor = "BSU_NAME" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID as ID,BSU_NAME as E_Name,BSU_CODE AS CODE FROM OASIS_FIM.FIM.BSU_M UNION SELECT BSU_ID as ID,BSU_NAME as E_Name,BSU_SHORTNAME AS CODE  FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN ('999998'    ,'900007'    ,'800075'    ,'900024'    ,'900020'    ,'900001'    ,'800030'    ,'900010'    ,'888889'    ))A where a.ID<>''  and (E_Name like '%" + prefix + "%' OR CODE LIKE '%" + prefix + "%')")
        '      ElseIf SearchFor = "BSU_ID" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID as ID,BSU_NAME as E_Name FROM OASIS_FIM.FIM.BSU_M UNION SELECT BSU_ID as ID,BSU_NAME as E_Name  FROM OASIS..BUSINESSUNIT_M WHERE BSU_ID IN ('999998'    ,'900007'    ,'800075'    ,'900024'    ,'900020'    ,'900001'    ,'800030'    ,'900010'    ,'888889'    ))A where a.ID<>''  and ID like '%" + prefix + "%'")
        '      ElseIf prefix = "S" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID AS ID, BSU_NAME AS E_Name,BSU_SHORTNAME FROM   OASIS..BUSINESSUNIT_M WHERE  BSU_SHORTNAME  NOT IN ('GVS','XXX','GEP','OOF','MHN','GDF','LLN','ADJ','GSA','PRS') AND ISNULL(BSU_bGEMSSCHOOL ,0)=1 AND BSU_NAME NOT LIKE 'DREAM%'  AND ISNULL(BSU_Bschool ,0)=1)A where a.ID<>'' ORDER By E_Name ASC")
        '      ElseIf prefix = "E" Then
        '          ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString,
        'CommandType.Text, "SELECT ID,A.E_Name FROM (SELECT BSU_ID AS ID, BSU_NAME AS E_Name,BSU_SHORTNAME FROM   OASIS..BUSINESSUNIT_M WHERE  BSU_SHORTNAME  NOT IN ('GVS','XXX','GEP','OOF','MHN','GDF','LLN','ADJ','GSA','PRS') AND ISNULL(BSU_bGEMSSCHOOL ,0)=1 AND BSU_NAME NOT LIKE 'DREAM%')A where a.ID<>'' ORDER By E_Name ASC")

        '      End If
        '  Else

        '  End If

        If ds.Tables(0).Rows.Count > 0 Then
            For Each Row In ds.Tables(0).Rows
                company.Add(String.Format("{0}||{1}", Row("E_Name"), Row("ID")))
            Next
        Else

        End If

        Return company.ToArray()
        'End Using
    End Function


    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function AddSaveDependantDetails(ByVal EDDID As String, ByVal bsuid As String, ByVal usrName As String, Name As String, ByVal DOBDate As String, ByVal Relation As String, ByVal StuID As String, ByVal PassportNo As String, ByVal UIDNo As String, ByVal VisaIssueDt As String, ByVal VisaExpDate As String, ByVal VisaIssuePlc As String, ByVal EIDExpryDt As String, ByVal Gender As String, ByVal MarStatus As String, ByVal DependMStatus As String, ByVal CityID As String, ByVal Nationality As String, ByVal Sponser As String, ByVal InsuranceNumber As String, ByVal EIDNo As String, ByVal GemsType As String, ByVal Unit As String, ByVal EmployeeID As String, ByVal Filepath As String, ByVal Visafilenumber As String, ByVal ResindenceCountryId As String, ByVal GemsStaffIdorStuId As String) As String 'ByVal Filepath As String
        ' Dim Filepath As String = ""
        Try
            Dim DOBDt As Date
            Dim EIDExpiryDate As Date
            Dim visaissuedate As Date
            Dim visaexpdt As Date

            Try
                If Not (DOBDate = "") And Not (DOBDate Is Nothing) Then
                    DOBDt = DateTime.Parse(DOBDate.Trim())
                End If
                If Not (EIDExpryDt = "") And Not (EIDExpryDt Is Nothing) Then
                    EIDExpiryDate = DateTime.Parse(EIDExpryDt.Trim())
                End If
                If Not (VisaIssueDt = "") And Not (VisaIssueDt Is Nothing) Then
                    visaissuedate = DateTime.Parse(VisaIssueDt.Trim())
                End If
                If Not VisaExpDate = "" And Not VisaExpDate Is Nothing Then
                    visaexpdt = DateTime.Parse(VisaExpDate.Trim())
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

            If Relation = "" Or Relation = "-1" Or Relation Is Nothing Then
                Return "1000" + "||" + "Please Select Relation"
            ElseIf DOBDate = "" Or DOBDate Is Nothing Then
                Return "1000" + "||" + "Please Enter Date of Birth"
            ElseIf (DOBDt > Date.Today) Then
                Return "1000" + "||" + "Date of Birth cannot be Future Date."
            ElseIf Gender = "" Or Gender = "0" Or Gender Is Nothing Then
                Return "1000" + "||" + "Please Select Gender"
            ElseIf MarStatus = "" Or MarStatus = "-1" Or MarStatus Is Nothing Then
                Return "1000" + "||" + "Please Select Marital Status"
            ElseIf GemsType = "" Or GemsType = "undefined" Or GemsType Is Nothing Then
                Return "1000" + "||" + "Please Select Dependent Type"
            ElseIf GemsType <> "Non Gems" And (bsuid = "" Or bsuid = "0" Or bsuid Is Nothing) Then
                If Unit = "" Then
                    Return "1000" + "||" + "Please Select Business unit"
                Else
                    Return "1000" + "||" + "Incorrect Business Unit is selected"
                End If
            ElseIf (GemsType <> "Non Gems") And ((EmployeeID = "" Or EmployeeID = "0" Or EmployeeID Is Nothing) And GemsType = "Gems Staff") Then
                If Name = "" Then
                    'Return "1000" + "||" + "Please Select employee from the institute you have selected"
                    Return "1000" + "||" + "Please Enter Name"
                Else
                    Return "1000" + "||" + "Incorrect Staff Name"
                End If
            ElseIf (GemsType <> "Non Gems") And ((StuID = "" Or StuID = "0" Or StuID Is Nothing) And GemsType = "Gems Student") Then
                If Name = "" Then
                    'Return "1000" + "||" + "Please Select  student from the institute you have selected"
                    Return "1000" + "||" + "Please Enter Name"
                Else
                    Return "1000" + "||" + "Incorrect Student Name"
                End If
            ElseIf Name = "" Or Name Is Nothing Then
                Return "1000" + "||" + "Please Enter Name"
            ElseIf CityID = "" Or CityID = "0" Or CityID Is Nothing Then
                If Nationality = "" Then
                    Return "1000" + "||" + "Please Select Nationality"
                Else
                    Return "1000" + "||" + "Incorrect Nationality is selected"
                End If
                'ElseIf PassportNo = "" Or PassportNo Is Nothing Then
                '    Return "1000" + "||" + "Please Enter Passport Number"
            ElseIf ResindenceCountryId = "" Or ResindenceCountryId = "0" Or ResindenceCountryId Is Nothing Then
                Return "1000" + "||" + "Please Enter Residence Country"
            ElseIf Not ResindenceCountryId Is Nothing AndAlso ResindenceCountryId <> "" AndAlso ResindenceCountryId = "172" Then
                If EIDNo = "" Or EIDNo Is Nothing Then
                    Return "1000" + "||" + "Please Enter Emirates ID number"
                ElseIf EIDExpryDt = "" Or EIDExpryDt Is Nothing Then
                    Return "1000" + "||" + "Please Enter Emirates ID Expiry Date"
                ElseIf UIDNo = "" Or UIDNo Is Nothing Then
                    Return "1000" + "||" + "Please Enter UID Number"
                ElseIf VisaIssueDt = "" Or VisaIssueDt Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Issue Date"
                ElseIf visaissuedate > Date.Today Then
                    Return "1000" + "||" + "Visa Issue Date cannot be Future Date"
                ElseIf VisaExpDate = "" Or VisaExpDate Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Expiry Date"
                ElseIf (visaissuedate > visaexpdt) Then
                    Return "1000" + "||" + "Visa Expiry Date is Lesser than Visa Issue Date"
                ElseIf VisaIssuePlc = "" Or VisaIssuePlc Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Issue Place"
                ElseIf Sponser = "" Or Sponser = "0" Or Sponser Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa Sponsor Type"
                    'ElseIf InsuranceNumber = "" Or InsuranceNumber Is Nothing Then
                    '    Return "1000" + "||" + "Please Enter Insurance Number"
                    'ElseIf HttpContext.Current.Session("EMP_BSU_COUNTRY_ID") = "172" Then
                ElseIf Visafilenumber = "" Or Visafilenumber Is Nothing Then
                    Return "1000" + "||" + "Please Enter Visa File Number"
                End If
            End If


            Dim EDD_Detail As New EOS_EmployeeDependant.DependantDetail
            If Filepath <> "" Then
                Dim bitmapData() As Byte
                Dim sbText As StringBuilder = New StringBuilder(Filepath, Filepath.Length)
                sbText.Replace("\r\n", String.Empty)
                sbText.Replace(" ", String.Empty)
                sbText.Replace("data:image/png;base64,", String.Empty)
                bitmapData = Convert.FromBase64String(sbText.ToString())
                Try

                    EDD_Detail.EDD_PHOTO = bitmapData
                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            Else
                EDD_Detail.EDD_PHOTO = Nothing
            End If



            If EDDID = "0" Then
                EDD_Detail.EDD_ID = 0
            Else
                EDD_Detail.EDD_ID = EDDID
            End If
            EDD_Detail.EDD_DOB = DOBDate
            EDD_Detail.EDD_EMP_ID = EOS_MainClass.GetEmployeeIDFromUserName(usrName)
            EDD_Detail.EDD_NAME = Name
            EDD_Detail.EDD_RELATION = Relation
            EDD_Detail.EDD_STU_ID = 0

            EDD_Detail.bConcession = True
            EDD_Detail.EDD_NoofTicket = 0
            '  Dim GemsType As String = ""
            If GemsType = "Non Gems" Then
                EDD_Detail.EDD_GEMS_TYPE = "N"
            ElseIf GemsType = "Gems Staff" Then
                EDD_Detail.EDD_GEMS_TYPE = "E"
            ElseIf GemsType = "Gems Student" Then
                EDD_Detail.EDD_GEMS_TYPE = "S"
            Else
                EDD_Detail.EDD_GEMS_TYPE = "N"
            End If
            'Dim EmployeeID As String = ""
            'Dim Unit As String = ""
            ' EDD_Detail.EDD_GEMS_TYPE = "N"
            If GemsType = "Gems Staff" Then
                EDD_Detail.EDD_GEMS_ID = EmployeeID
            ElseIf GemsType = "Gems Student" Then
                EDD_Detail.EDD_GEMS_ID = StuID
            Else
                EDD_Detail.EDD_GEMS_ID = 0
                bsuid = ""
            End If

            EDD_Detail.EDD_GEMS_TYPE_DESCR = GemsType
            EDD_Detail.EDD_BSU_ID = bsuid
            EDD_Detail.EDD_BSU_NAME = Unit
            EDD_Detail.EDD_bCompanyInsurance = False

            'If ddlDepInsuCardTypes.SelectedItem.Text <> "--Select--" Then
            '    EDD_Detail.EDD_Insu_id = ddlDepInsuCardTypes.SelectedValue
            '    EDD_Detail.InsuranceCategoryDESC = ddlDepInsuCardTypes.SelectedItem.Text
            'Else
            EDD_Detail.EDD_Insu_id = 0
            EDD_Detail.InsuranceCategoryDESC = ""
            ' End If
            EDD_Detail.EDD_PASSPRTNO = PassportNo
            EDD_Detail.EDD_UIDNO = UIDNo
            Dim defalutDt As Date
            defalutDt = DateTime.Parse("01/Jan/1900")
            If (VisaIssueDt = "") Then
                EDD_Detail.EDD_VISAISSUEDATE = defalutDt
            Else
                EDD_Detail.EDD_VISAISSUEDATE = IIf(VisaIssueDt = "", defalutDt, CDate(VisaIssueDt))
            End If
            If (VisaIssueDt = "") Then
                EDD_Detail.EDD_VISAEXPDATE = defalutDt
            Else : EDD_Detail.EDD_VISAEXPDATE = IIf(VisaExpDate = "", defalutDt, CDate(VisaExpDate))
            End If
            If (EIDExpryDt = "") Then
                EDD_Detail.EDC_EXP_DT = defalutDt
            Else
                EDD_Detail.EDC_EXP_DT = IIf(EIDExpryDt = "", "01/Jan/1900", CDate(EIDExpryDt))
            End If

            EDD_Detail.EDD_VISAISSUEPLACE = VisaIssuePlc
            EDD_Detail.EDD_FILENO = Visafilenumber
            ' EDD_Detail.EDD_bCompanyInsurance = True

            'If Session("sbsuid") <> "800444" AndAlso Session("sbsuid") <> "800111" Then
            '    If txtEIDNo1.Text <> "" Then
            '        If txtEIDNo1.Text.Length = "3" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = txtEIDNo1.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If


            '    End If

            '    If txtEIDNo2.Text <> "" Then

            '        If txtEIDNo2.Text.Length = "4" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo2.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If
            '    End If

            '    If txtEIDNo3.Text <> "" Then

            '        If txtEIDNo3.Text.Length = "7" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo3.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If
            '    End If
            '    If txtEIDNo4.Text <> "" Then
            '        If txtEIDNo4.Text.Length = "1" Then
            '            EDD_Detail.EDC_DOCUMENT_NO = EDD_Detail.EDC_DOCUMENT_NO + txtEIDNo4.Text
            '        Else
            '            lblError.Text = "Please enter emirates Id number in valid format"
            '            Exit Function
            '        End If


            '    End If
            'Else
            EDD_Detail.EDC_DOCUMENT_NO = EIDNo
            'End If
            ''EDD_Detail.EDC_DOCUMENT_NO = Me.txtEIDNo.Text.Trim

            EDD_Detail.EDD_bMALE = IIf(Gender = "M", True, False)
            EDD_Detail.EDD_MARITALSTATUS = Convert.ToInt16(MarStatus)
            EDD_Detail.EDC_Gender = Gender
            EDD_Detail.EDD_MSTATUS = DependMStatus
            EDD_Detail.EDD_CTY_ID = Convert.ToInt16(CityID)
            EDD_Detail.CTY_NATIONALITY = Nationality

            EDD_Detail.EDD_VISASPONSOR = Convert.ToInt32(Sponser)
            EDD_Detail.EDD_EMIRATE = Convert.ToInt32(0)
            EDD_Detail.EDD_EMIRATE_AREA = Convert.ToInt32(0)
            EDD_Detail.EDD_INSURANCENUMBER = InsuranceNumber
            EDD_Detail.EDD_RESIDENCE_CTY_ID = Convert.ToInt32(ResindenceCountryId)
            EDD_Detail.EDD_GEMS_STAFF_STU_NO = GemsStaffIdorStuId
            Dim retval As String
            Dim new_elh_id As Integer = 0
            retval = EOS_EmployeeDependant.SaveDependantDetail(EDD_Detail)
            'retval = "0"
            If (retval = "0" Or retval = "") Then
                Return "0" + "||" + UtilityObj.getErrorMessage(0)
            Else
                Return retval + "||" + UtilityObj.getErrorMessage(retval)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "1000" + "||" + UtilityObj.getErrorMessage(1000) + ", " + ex.Message
        End Try

    End Function
    'Added by vikranth on 15th Jan 2020 for getting EmpId details
    Private Function GetEMP_Basic_Details() As DataSet
        Dim ds As DataSet = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
        Param(0) = Mainclass.CreateSqlParameter("@EMP_ID", ViewState("EmployeeID"), SqlDbType.VarChar)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetEMP_Basic_Details]", Param)
        Return ds
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function CancelLeave(ByVal ID As String, ByVal LeaveCan As String, ByVal Comments As String) As String
        Dim stTrans As SqlTransaction
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Try
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ELA_ID", SqlDbType.VarChar, 50)
            pParms(0).Value = ID ' Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

            pParms(1) = New SqlClient.SqlParameter("@retVal", SqlDbType.VarChar)
            pParms(1).Direction = ParameterDirection.ReturnValue

            pParms(2) = New SqlClient.SqlParameter("@comments", SqlDbType.VarChar)
            pParms(2).Value = Comments '

            Dim retval As Integer = 0
            stTrans = conn.BeginTransaction
            retval = SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[CancelEmpLeaveApp]", pParms)
            If pParms(1).Value = 0 Then
                stTrans.Commit()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Return "0"
            Else
                stTrans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                Return "1000"


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            stTrans.Rollback()
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return "1000"

        End Try

    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function UpdateProfileDetails(ByVal bsuid As String, ByVal usrName As String, ByVal Name As String, ByVal empno As String, ByVal txtAddEID As String, ByVal txtEIDExpDt As String, ByVal txtAddUIDNo As String, ByVal PassportNO As String, ByVal PassportIssuePlc As String, ByVal PassportIsDt As String, ByVal PassportExpDt As String, ByVal AlEmail As String, ByVal txtAddVisaIssuePlc As String, ByVal txtAddVisaIssueDt As String, ByVal txtAddVisaExpDt As String, ByVal slctAddVisaSpnsr As String, ByVal txtAddInsCrdNo As String, ByVal Emirate As String, ByVal Area As String, ByVal Phone As String, ByVal EmergencyContactLocal As String, ByVal EmergencyContactHome As String, ByVal EmergencyPhoneLocal As String, ByVal EmergencyPhoneHome As String, ByVal EmpVisaStatus As String, ByVal EmpCTYId As String) As String
        Dim callTransaction As String = "0"
        Dim tran As SqlTransaction
        Dim errormsg As String = ""

        Try

            If EmpVisaStatus <> "0" AndAlso EmpVisaStatus <> "1" Then

                If txtAddEID <> "" Then

                Else
                    'If bsuid <> "800444" AndAlso bsuid <> "800111" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") = "172" Then
                        errormsg = "Please Enter Emirates ID Number"
                        callTransaction = "-1"
                    End If
                End If


                If txtEIDExpDt = "" Then
                    'If bsuid <> "800444" AndAlso bsuid <> "800111" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") = "172" Then
                        errormsg = "Please Enter Emirates Id Expiry Date !!!!"
                        callTransaction = "-1"
                    End If
                End If

                If PassportNO = "" Then
                    'If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter Passport Number"
                        callTransaction = "-1"

                    End If

                End If

                If PassportIssuePlc = "" Then
                    ' If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter Passport Issued Place"

                        callTransaction = "-1"

                    End If
                End If

                If PassportIsDt = "" Then
                    'If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter Passport Issued Date !!!!"

                        callTransaction = "-1"

                    End If
                End If

                If PassportExpDt = "" Then
                    'If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter Passport Expiry Date !!!!"

                        callTransaction = "-1"

                    End If
                End If

                If txtAddUIDNo = "" Then
                    'If bsuid <> "800444" AndAlso bsuid <> "800111" AndAlso Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") = "172" Then
                        errormsg = "Please Enter UID Number!!!!"

                        callTransaction = "-1"

                    End If
                End If

                If txtAddVisaIssueDt = "" Then
                    'If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter VISA Issued Date!!!!"

                        callTransaction = "-1"

                    End If
                End If

                If txtAddVisaExpDt = "" Then
                    'If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter VISA Expiry Date!!!!"

                        callTransaction = "-1"

                    End If
                End If
                If txtAddVisaIssuePlc = "" Then
                    'If Emirate <> "172" Then
                    If HttpContext.Current.Session("BSU_COUNTRY_ID") <> EmpCTYId Then
                        errormsg = "Please Enter VISA Issued Place!!!!"

                        callTransaction = "-1"

                    End If
                End If

                If AlEmail = "" Then
                    errormsg = "Please Enter Alternate Email!!!!"

                    callTransaction = "-1"

                End If

                If Phone = "" Then
                    errormsg = "Please Enter Phone Number!!!!"

                    callTransaction = "-1"

                End If

                If slctAddVisaSpnsr = "" Or slctAddVisaSpnsr = "0" Or slctAddVisaSpnsr Is Nothing Then
                    errormsg = "Please Enter Sponser Details !!!!"

                    callTransaction = "-1"

                End If

                If Emirate = "0" Or Emirate = "" Then
                    errormsg = "Please Enter Emirates !!!!"

                    callTransaction = "-1"

                End If

                If Area = "0" Or Area = "" Then
                    errormsg = "Please Enter Area !!!!"

                    callTransaction = "-1"

                End If

                If txtAddInsCrdNo = "" Then
                    errormsg = "Please Enter Insurance Number !!!!"

                    callTransaction = "-1"

                End If

                If (EmergencyContactHome = "" Or EmergencyPhoneHome = "") Then
                    errormsg = "Please Enter Emergency Contact(home) Details !!!!"

                    callTransaction = "-1"

                End If

                If EmergencyContactLocal = "" Or EmergencyPhoneLocal = "" Then
                    errormsg = "Please Enter Emergency Contact(Local) Details!!!!"

                    callTransaction = "-1"

                End If
            End If

            If (callTransaction = "0") Then
                Using CONN As SqlConnection = ConnectionManger.GetOASISConnection
                    tran = CONN.BeginTransaction("SampleTransaction")
                    Try

                        Dim param(18) As SqlParameter
                        param(0) = New SqlParameter("@EMP_ID", EOS_MainClass.GetEmployeeIDFromUserName(usrName))
                        param(1) = New SqlParameter("@UID_NO", txtAddUIDNo)
                        param(2) = New SqlParameter("@EID_NO", txtAddEID)
                        param(3) = New SqlParameter("@EID_EXP_DATE", txtEIDExpDt)
                        param(4) = New SqlParameter("@VISA_ISSU_PLACE", txtAddVisaIssuePlc)
                        param(5) = New SqlParameter("@VISA_ISSUE_DATE", txtAddVisaIssueDt)
                        param(6) = New SqlParameter("@VIS_EXP_DATE", txtAddVisaExpDt)
                        param(7) = New SqlParameter("@Email", AlEmail)
                        param(8) = New SqlParameter("@cur_mobile", Phone)
                        param(9) = New SqlParameter("@PassportNo", PassportNO)
                        param(10) = New SqlParameter("@PPIssuedPlace", PassportIssuePlc)
                        param(11) = New SqlParameter("@PPIssueDate", PassportIsDt)
                        param(12) = New SqlParameter("@PPEXpiry", PassportExpDt)
                        param(13) = New SqlParameter("@VISASPONSOR", Convert.ToInt32(slctAddVisaSpnsr))
                        param(14) = New SqlParameter("@EMPEMIRATE", Convert.ToInt32(Emirate))
                        param(15) = New SqlParameter("@EMPAREA", Convert.ToInt32(Area))
                        param(16) = New SqlParameter("@INSUR_NO", txtAddInsCrdNo)
                        param(17) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                        param(17).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.Update_EMP_VISA_EID_INFO", param)
                        Dim ReturnFlag As Integer = param(17).Value

                        If ReturnFlag = -1 Then
                            callTransaction = "-1"
                            errormsg = "Record cannot be saved. !!!"
                        ElseIf ReturnFlag <> 0 Then
                            callTransaction = "1"
                            errormsg = "Error occured while processing info !!!"
                        Else
                            'ViewState("GCM_ID") = "0"
                            ' ViewState("datamode") = "none"
                            callTransaction = "0"
                            Dim params(5) As SqlParameter
                            params(0) = New SqlParameter("@emp_id", EOS_MainClass.GetEmployeeIDFromUserName(usrName))
                            params(1) = New SqlParameter("@homenum", EmergencyPhoneHome)
                            params(2) = New SqlParameter("@homecont", EmergencyContactHome)
                            params(3) = New SqlParameter("@localnum", EmergencyPhoneLocal)
                            params(4) = New SqlParameter("@localcont", EmergencyContactLocal)
                            SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "dbo.SAVE_EMERGENCY_FORPROFILE", params)
                            callTransaction = "0"
                            'resetall()
                        End If
                    Catch ex As Exception
                        callTransaction = "1"
                        errormsg = ex.Message
                        UtilityObj.Errorlog(ex.Message + ":1", System.Reflection.MethodBase.GetCurrentMethod().Name)
                    Finally
                        If callTransaction = "-1" Then
                            errormsg = "Record cannot be saved. !!!"
                            'UtilityObj.Errorlog(errormsg)
                            tran.Rollback()
                        ElseIf callTransaction <> "0" Then
                            errormsg = "Error occured while saving !!!"
                            'UtilityObj.Errorlog(errormsg)
                            tran.Rollback()
                        Else
                            ' errormsg = ""
                            tran.Commit()
                        End If
                        If CONN.State = ConnectionState.Open Then
                            CONN.Close()
                        End If
                    End Try
                End Using
            End If
            Return callTransaction + "||" + errormsg
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "-1" + "||" + "Erro"
        End Try
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Function GenerateSalarySlip(ByVal paymonth As String, ByVal ddlPayYear As String) As String

        Dim ESD_IDs As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = ""
        str_Sql = "select DISTINCT ESD_ID,ESD_BSU_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE isnull(esd_paid,0)=1 and Isnull(esd_bposted,0)=1 and esd_month = " & paymonth & " and esd_year= " & ddlPayYear &
           " and esd_emp_id= " & HttpContext.Current.Session("EmployeeID")
        '& dRow.Item("ESD_ID")
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

        Dim comma As String = String.Empty
        While (dr.Read())
            ESD_IDs += comma + dr(0).ToString
            comma = ","
        End While
        If ESD_IDs = "" Then

            Return "False"
        Else


            Return "True"

        End If
    End Function
#End Region


#Region "PaySlip Generation"
    Public Sub loadSpans(ByVal year As String)
        spnJan.InnerHtml = year
        spnfeb.InnerHtml = year
        spnmar.InnerHtml = year
        spnapr.InnerHtml = year
        spnmay.InnerHtml = year
        spnJun.InnerHtml = year
        spnJul.InnerHtml = year
        spnaug.InnerHtml = year
        spnsep.InnerHtml = year
        spnoct.InnerHtml = year
        spnnov.InnerHtml = year
        spndec.InnerHtml = year
    End Sub


    Private Shared Sub AddPasswordToPDF(ByVal sourceStream As System.IO.Stream, ByVal outputFile As String, ByVal password As String)
        Try
            Dim fsStream As New FileStream(outputFile, FileMode.Create)
            fsStream.Close()
            Dim pReader As PdfReader = New PdfReader(sourceStream)
            PdfEncryptor.Encrypt(pReader, New FileStream(outputFile, FileMode.Open), PdfWriter.STRENGTH128BITS, password, password, PdfWriter.AllowCopy)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Function GeneratePayslipPDF(ByVal paymonth As Integer, ByVal ddlPayYear As String, ByVal empno As String) As Integer
        'Return 0
        GeneratePayslipPDF = 0
        Dim cryRpt As New ReportDocument
        Dim ESD_IDs As String = String.Empty
        Dim EMAIL_ID As String = String.Empty
        Dim ESD_BSUID As String = String.Empty
        Dim EML_ID As String = String.Empty
        Dim str_Sql As String
        Dim pdfFile As String
        Dim strESD_ID As String = String.Empty
        Try
            Dim str_conn As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            ' Dim objConn As SqlConnection = New SqlConnection(str_conn)

            pdfFile = System.Configuration.ConfigurationManager.AppSettings("TempFileFolder").ToString()
            If Not Directory.Exists(pdfFile & "\SalaryPayslip") Then
                Directory.CreateDirectory(pdfFile & "\SalaryPayslip")
            End If
            pdfFile = pdfFile & "SalaryPayslip\"
            Dim abc As String = pdfFile & paymonth & ddlPayYear & "_" & empno & ".pdf"
            Dim abc2 As String = pdfFile & paymonth & ddlPayYear & "_" & empno & "Copy.pdf"
            Dim filePath As String = HttpContext.Current.Server.MapPath("~/PAYROLL/REPORTS/RPT/rptEmailSalarySlip.rpt") '-- System.Configuration.ConfigurationManager.AppSettings("salaryReportPath").ToString


            ' Dim abc As String = pdfFile & "_" & strESD_ID & ".pdf"
            ESD_IDs = strESD_ID

            str_Sql = "select DISTINCT ESD_ID,ESD_BSU_ID from vw_OSO_EMPSALARYHEADERDETAILS WHERE isnull(esd_paid,0)=1 and Isnull(esd_bposted,0)=1 and esd_month = " & paymonth & " and esd_year= " & ddlPayYear &
            " and esd_emp_id= " & Session("EmployeeID")


            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            ESD_BSUID = Session("sBsuid")
            Dim comma As String = String.Empty
            While (dr.Read())
                ESD_IDs += comma + dr(0).ToString
                comma = ","
            End While
            If ESD_IDs = "" Then
                Return 1000
            Else


            End If
            If ESD_BSUID = "900510" Or ESD_BSUID = "900500" Or ESD_BSUID = "900501" Then
                filePath = HttpContext.Current.Server.MapPath("~/PAYROLL/REPORTS/RPT/rptEmailSalarySlip_STS.rpt")

            End If
            cryRpt.Load(filePath)



            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New  _
            DiskFileDestinationOptions()
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = pdfFile
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            str_Sql = "SELECT * FROM vw_OSO_EMPSALARYHEADERDETAILS WHERE ESD_ID  in (" & ESD_IDs & ")"

            Dim dsMain As New DataSet
            dsMain = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not dsMain Is Nothing And dsMain.Tables(0).Rows.Count > 0 Then
                cryRpt.SetDataSource(dsMain.Tables(0))
            End If

            Dim ds1 As New DataSet
            str_Sql = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='D' AND ESS_ESD_ID   in (" & ESD_IDs & ")" ' (" & ESD_IDs & ")"

            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If Not ds1 Is Nothing And ds1.Tables.Count > 0 Then
                cryRpt.Subreports(0).SetDataSource(ds1.Tables(0))
            End If

            Dim ds2 As New DataSet

            str_Sql = "select * from dbo.vw_OSO_EMPSALARYSUBDETAILS WHERE ESS_TYPE ='E' AND ESS_ESD_ID   in (" & ESD_IDs & ")" '(" & ESD_IDs & ")"

            ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not ds2 Is Nothing And ds2.Tables.Count > 0 Then
                cryRpt.Subreports(1).SetDataSource(ds2.Tables(0))
            End If
            Try
                Dim ds3 As New DataSet
                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = Web.Configuration.WebConfigurationManager.AppSettings("InstanceName").ToString()
                myConnectionInfo.DatabaseName = "OASIS"
                myConnectionInfo.UserID = Web.Configuration.WebConfigurationManager.AppSettings("UserName").ToString()
                myConnectionInfo.Password = Web.Configuration.WebConfigurationManager.AppSettings("Password").ToString()
                'str_Sql = "exec [dbo].[getBsuInFoWithImage_PaySlip]  '" & ESD_BSUID & "','logo' "


                str_Sql = "exec [dbo].[getBsuInFoWithImage_PaySlip] '" & Session("sBsuid") & "','logo' "

                ds3 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                If Not ds3 Is Nothing And ds3.Tables.Count > 0 Then
                    'cryRpt.Subreports(2).SetDataSource(ds3.Tables(0))
                    cryRpt.Subreports(2).SetDatabaseLogon(myConnectionInfo.UserID, myConnectionInfo.Password, myConnectionInfo.ServerName, myConnectionInfo.DatabaseName)
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

            '  cryRpt.SetParameterValue("userName", "Gems Education")
            '  cryRpt.SetParameterValue("bMultiMonth", False)
            Try
                cryRpt.SetParameterValue("userName", "System Generated")
                cryRpt.SetParameterValue("bMultiMonth", False)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try


            Dim abc1 As String = ""

            Dim pdfPassword As String = ""
            Dim pwdDS As New DataSet

            str_Sql = "getEmployeeSalarySlipPassword '" & Session("EmployeeID") & "','" & ESD_IDs & "'"

            pwdDS = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If Not pwdDS Is Nothing And pwdDS.Tables(0).Rows.Count > 0 Then
                pdfPassword = pwdDS.Tables(0).Rows(0).Item(0).ToString
                Dim passwordEncr As New Encryption64
                If pdfPassword <> "" Then
                    pdfPassword = passwordEncr.Decrypt(pdfPassword).Replace(" ", "+")
                End If
            End If


            If pdfPassword <> "" Then
                Dim SourceStream As Stream
                SourceStream = cryRpt.ExportToStream(ExportFormatType.PortableDocFormat)

                AddPasswordToPDF(SourceStream, abc, pdfPassword)
                SourceStream.Dispose()


            Else

                cryRpt.ExportToDisk(ExportFormatType.PortableDocFormat, abc)


            End If
            Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Payslip", Session("sUsr_name"), "Insert",
                                           Page.User.Identity.Name.ToString, Me.Page, "Payslip Generated for-" & empno & "-" & paymonth & "-" & ddlPayYear)



            Dim bytes() As Byte = File.ReadAllBytes(abc)

            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(abc))
            Response.BinaryWrite(bytes)

            Response.Flush()
            Response.Close()
            System.IO.File.Delete(abc)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Payslip01")
            Return 1000
        Finally
            cryRpt.Close()
            cryRpt.Dispose()
            'subPageLoad()
        End Try
    End Function
    Protected Sub btnConSalDwnld_ServerClick(sender As Object, e As EventArgs)
        GeneratePayslipPDF(hdnMonth.Value, hdnYear.Value, h_EmpNoDesc.Value)
    End Sub
    Protected Sub btnConSalDwnld_Click(sender As Object, e As EventArgs)
        GeneratePayslipPDF(hdnMonth.Value, hdnYear.Value, h_EmpNoDesc.Value)
    End Sub
#End Region


    Protected Sub rptrDpndt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If rptrDpndt.Items.Count < 1 Then
            If e.Item.ItemType = ListItemType.Footer Then
                Dim lblFooter As Label = CType(e.Item.FindControl("lblEmptyData"), Label)
                lblFooter.Visible = True
            End If
        End If
    End Sub

    'Added  by vikranth on 5th Feb 2020, for hiding the My Team meny
    Protected Sub rptView_profile_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptView_profile.ItemDataBound
        Try
            If e.Item.DataItem Is Nothing Then
                Return
            Else
                Dim divMyTeam As HtmlGenericControl = DirectCast(e.Item.FindControl("divMyTeam"), HtmlGenericControl)
                myTeamMenuHide(divMyTeam)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub myTeamMenuHide(ByVal divMyTeam As HtmlGenericControl)
        Try
            divMyTeam.Visible = False
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EmpId", SqlDbType.VarChar)
            pParms(0).Value = Session("EmployeeId")

            pParms(1) = New SqlClient.SqlParameter("@BsuId", SqlDbType.VarChar)
            pParms(1).Value = Session("SbSUID")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_ReportingEmployees_Count", pParms)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item(0).ToString() <> "" Then
                        If Convert.ToInt32(ds.Tables(0).Rows(0).Item(0).ToString()) > 0 Then
                            divMyTeam.Visible = True
                        Else : divMyTeam.Visible = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    'End by vikrnath on 5th Feb 2020
    'Added by vikranth on 4th Mar 2020
    <System.Web.Services.WebMethod()>
    Public Shared Function GetDependentName(ByVal Type As String, ByVal Dob As String, ByVal StaffIdORStuId As String, ByVal BSU_ID As String) As String()
        Try
            Dim str_Sql As String = ""
            If (Type = "Gems Student") Then
                str_Sql = "select isnull(STU_firstname,'')+' '+isnull(STU_midname,'')+' '+isnull(STU_lastname,'') Depdent_NAME, STU_ID As Depedent_Id,CASE WHEN STU_GENDER='M' THEN 'M' ELSE 'F' END As Gender,1 AS MaritalStatus  " _
                        & "From Student_m where STU_NO='" & StaffIdORStuId & "' AND STU_BSU_ID='" & BSU_ID & "' AND STU_DOB='" & Dob & "' AND STU_CURRSTATUS='EN'" 'STU_bActive=1

            ElseIf (Type = "Gems Staff") Then
                str_Sql = "SELECT isnull(emp_fname,'')+' '+isnull(emp_mname,'')+' '+isnull(emp_lname,'')  as Depdent_NAME, Emp_ID As Depedent_Id,CASE WHEN EMP_SEX_bMALE=0 THEN 'F' ELSE 'M' END AS Gender,EMP_MARITALSTATUS AS MaritalStatus FROM EMPLOYEE_M " _
                        & "WHERE EmpNo='" & StaffIdORStuId & "' and EMP_BSU_ID='" & BSU_ID & "' and EMP_DOB='" & Dob & "' AND EMP_bACTIVE=1"
            End If
            GetDependentName = Nothing
            Dim Dependent As New List(Of String)()
            Using conn As New SqlConnection()
                conn.ConnectionString = ConnectionManger.GetOASISConnectionString
                Using cmd As New SqlCommand()
                    cmd.CommandText = str_Sql '& str_orderby

                    cmd.Connection = conn
                    conn.Open()
                    Using sdr As SqlDataReader = cmd.ExecuteReader()
                        While sdr.Read()
                            Dependent.Add(String.Format("{0}||{1}||{2}||{3}", sdr("Depdent_NAME"), sdr("Depedent_Id"), sdr("Gender"), sdr("MaritalStatus")))
                        End While
                    End Using
                    conn.Close()
                End Using
                Return Dependent.ToArray()
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return Nothing
        End Try
    End Function
    Protected Sub rptrChildList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)

    End Sub
    Protected Sub btnJustClick_Click(sender As Object, e As EventArgs)
        subPageLoad()
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "HideGif();", True)
    End Sub

    Sub subPageLoad()
        Session("hidLoadValue") = ""


        Dim Encr_Employee_ID As String = Encr_decrData.Encrypt(Session("EmployeeID"))
        Session("Encr_Employee_ID") = Encr_Employee_ID

        'Session("LeaveMatrix") = Nothing
        BIND_BSU(Session("sbsuid"), Session("sUsr_name")) '' profile details loading
        getAccessDetailstoMenu()     '' access restriction setting for each block    
        gridbind_LeaveStatus() '' binding leave history section
        gridbindPanODetails() '' binding pno details section
        gridDependantDetails()  '' Bindng dependant details section
        ShowApplyLeave()
        ShowApplyFeeConcession()
        gridFeeConcessionDetails()  '' Bindng fee concession details section
        GetLeaveLogCount()   ''  repeater for leave apply pop up portion loading and leave matrix setting
        BINDaTTENDANCElOG()  '' Attendance bar binding
        BindLeaveRequest()
        BindCovidTests() 'Written by vikranth on 25th Aug 2020
        'Dim smScriptManager As ScriptManager = Master.FindControl("ScriptManager1")
        'smScriptManager.RegisterPostBackControl(btnConSalDwnld)
        'Added by vikranth on 15th Jan 2020, if login employee BSU_Country_Id is UAE then visa file number is mandatory
        Dim ds As DataSet = GetEMP_Basic_Details()
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Session("EMP_BSU_COUNTRY_ID") = ds.Tables(0).Rows(0)("BSU_COUNTRY_ID").ToString().Trim()
                h_bsu_cty_id.Value = ds.Tables(0).Rows(0)("BSU_COUNTRY_ID").ToString().Trim()
                If Session("EMP_BSU_COUNTRY_ID").ToString().Trim() = "172" Then
                    spanVisFileNoCheck.Visible = True
                    spanAddVisFileNoCheck.Visible = True
                Else : spanVisFileNoCheck.Visible = False
                    spanAddVisFileNoCheck.Visible = False
                End If
            End If
        End If
        'End by vikranth

        'Added by vikranth on 4th Feb 2020, as discussed with Nahyan
        'hfRequestVal.Value = "leave" 'pno
        Dim requestVal As String
        If Request.QueryString("request") <> "" Then
            requestVal = Encr_decrData.Decrypt(Request.QueryString("request").Replace(" ", "+"))
            hfRequestVal.Value = requestVal.ToLower()
        End If
        'End by vikranth

    End Sub
    Public Sub GetLoggedInEmployeeDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "SELECT " _
         & " EM.EMP_ID, ISNULL(EM.EMP_FNAME,'')+' '+" _
         & " ISNULL(EM.EMP_MNAME,'')+' '+ISNULL(EM.EMP_LNAME,'')" _
         & " AS EMP_NAME,EMPNO,EMP_BSU_ID ,bsu_shortname FROM  EMPLOYEE_M EM INNER JOIN USERS_M" _
         & " ON EM.EMP_ID=USERS_M.USR_EMP_ID inner join businessunit_m on bsu_id=emp_bsu_id " _
         & " WHERE USERS_M.USR_NAME ='" & Session("sUsr_name") & "' and em.emp_bsu_id ='" & Session("sBsuid") & "'"
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then

            h_Emp_No.Value = ds.Tables(0).Rows(0)("EMP_ID")
            h_emp_bsu.Value = ds.Tables(0).Rows(0)("emp_bsu_id")
            h_empnumValue.Value = ds.Tables(0).Rows(0)("EMPNO")
            h_emp_bsu_code.Value = ds.Tables(0).Rows(0)("bsu_shortname")
        End If

    End Sub

    Public Shared Function SaveDocumentDetailstoAX(ByVal AddressTo As String, ByVal empNotes As String, ByVal AddressNotes As String, ByVal doctype As String, ByVal empno As String, ByVal empbsucode As String, ByVal docDAXId As String) As String
        Try
            Dim AleDocDetails As New PnODocumentService.AxdGMS_HRDocuRequest
            Dim tmp1, tmp2 As String
            tmp1 = ""
            tmp2 = ""

            ' Dim BankFindRequest As PnODocumentService.ALE_EmpBankDetailsServiceFindRequest

            Dim client As New PnODocumentService.GMS_HRDocuRequestServiceClient
            'client.ClientCredentials.Windows.ClientCredential.UserName = "gemserp\swapna.tv"
            'client.ClientCredentials.Windows.ClientCredential.Password = "P@ssw0rd2"
            client.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Item("DAXUserName").ToString
            client.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Item("DAXPassword").ToString

            Dim context As New PnODocumentService.CallContext
            context.Company = empbsucode

            Dim AxdEntity_ALE_HRDocumentRequest(0) As PnODocumentService.AxdEntity_ALE_HRDocumentRequest
            AxdEntity_ALE_HRDocumentRequest(0) = New PnODocumentService.AxdEntity_ALE_HRDocumentRequest
            ' Dim RespAleBankDetails As New PnODocumentService.AxdEntity_HcmWorkerBankAccount

            Dim Criteria As New PnODocumentService.QueryCriteria
            Dim CriteriaElements(0) As PnODocumentService.CriteriaElement
            Dim CriteriaElement As New PnODocumentService.CriteriaElement

            CriteriaElement.DataSourceName = "ALE_HRDocumentRequest"
            CriteriaElement.FieldName = "DocumentRequest"
            CriteriaElement.Operator = PnODocumentService.Operator.Equal
            CriteriaElement.Value1 = docDAXId ' Request.QueryString("Recid")
            'CriteriaElement.Value2 = "0"
            CriteriaElements(0) = CriteriaElement
            Criteria.CriteriaElement = CriteriaElements
            AleDocDetails = client.find(context, Criteria)
            ' Dim RespAleBankDetails = AleBankDetails.HcmWorkerBankAccount

            'Dim RecordID As String = ""

            ' Dim AleBankDetails As New PnODocumentService.AxdALE_EmpBankDetails
            If Not AleDocDetails.ALE_HRDocumentRequest Is Nothing Then 'AndAlso Not RespAleBankDetails Is Nothing Then

                AleDocDetails.ALE_HRDocumentRequest(0).EmployeeNotes = empNotes ' rcbBank.SelectedValue   ' "Test"
                AleDocDetails.ALE_HRDocumentRequest(0).AddressToNotes = AddressNotes
                AleDocDetails.ALE_HRDocumentRequest(0).AddressTo = AddressTo
                AleDocDetails.ALE_HRDocumentRequest(0).class = "entity"
                AleDocDetails.ALE_HRDocumentRequest(0).action = PnODocumentService.AxdEnum_AxdEntityAction.update

                ' As discussed with Puneet and Robbie send blank data if CASH mode"


                Dim ent(0) As PnODocumentService.EntityKey
                Dim KeyData(0) As PnODocumentService.KeyField
                ent(0) = New PnODocumentService.EntityKey
                KeyData(0) = New PnODocumentService.KeyField
                KeyData(0).Field = "DocumentRequest"
                KeyData(0).Value = docDAXId
                ent(0).KeyData = KeyData


                client.update(context, ent, AleDocDetails)
                tmp2 = docDAXId
            Else

                Dim ent(1) As PnODocumentService.EntityKey


                AxdEntity_ALE_HRDocumentRequest(0).EmployeeNotes = empNotes ' rcbBank.SelectedValue   ' "Test"
                AxdEntity_ALE_HRDocumentRequest(0).AddressToNotes = AddressNotes
                AxdEntity_ALE_HRDocumentRequest(0).AddressTo = AddressTo
                AxdEntity_ALE_HRDocumentRequest(0).EmployeeId = empno
                AxdEntity_ALE_HRDocumentRequest(0).DateOfRequestSpecified = False
                AxdEntity_ALE_HRDocumentRequest(0).class = "entity"
                AxdEntity_ALE_HRDocumentRequest(0).action = PnODocumentService.AxdEnum_AxdEntityAction.create
                'AxdEntity_ALE_HRDocumentRequest(0).Status = "0"
                AxdEntity_ALE_HRDocumentRequest(0).CreatedFromPhoenix = PnODocumentService.AxdEnum_NoYes.Yes
                AxdEntity_ALE_HRDocumentRequest(0).CreatedFromPhoenixSpecified = True
                AxdEntity_ALE_HRDocumentRequest(0).DocumentID = GetDAXDocumentTypeMappingID(doctype) ' ddlDocType.SelectedItem.Text


                AleDocDetails.ALE_HRDocumentRequest = AxdEntity_ALE_HRDocumentRequest


                ent = client.create(context, AleDocDetails)
                If ent.Length > 0 Then

                    tmp1 = ent(0).KeyData(0).Field
                    tmp2 = ent(0).KeyData(0).Value
                End If

                'lblError.Text = "Transferred Successfully"
            End If
            Return tmp2
        Catch ex As Exception
            'lblError.Text = ex.Message
            ' ShowInfoLog(ex.Message, "E")
            Return 1000
        End Try
    End Function

    Public Shared Function GetDAXDocumentTypeMappingID(ByVal doctype As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_INTEGRATIONConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As New DataSet
        str_Sql = "exec getDAXDocumentMappingID " & doctype
        'V1.4
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("PnO_Doc_AXID")
        End If

    End Function

    Public Shared Function SaveCOVIDDocument(ByVal fuUpload As String, ByVal con As SqlConnection, tran As SqlTransaction, ByVal PCD_ID As String, ByVal bsuid As String, ByVal fileext As String, ByVal filename As String) As Boolean
        Try


            Dim contenttype As String = String.Empty


            Select Case fileext.ToLower()
                Case ".doc"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".docx"

                    contenttype = "application/vnd.ms-word"
                    Exit Select

                Case ".xls"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".xlsx"

                    contenttype = "application/vnd.ms-excel"
                    Exit Select

                Case ".jpg"

                    contenttype = "image/jpg"
                    Exit Select

                Case ".jpeg"

                    contenttype = "image/jpeg"
                    Exit Select

                Case ".tif"

                    contenttype = "image/tiff"
                    Exit Select

                Case ".png"

                    contenttype = "image/png"
                    Exit Select

                Case ".gif"

                    contenttype = "image/gif"
                    Exit Select

                Case ".pdf"

                    contenttype = "application/pdf"
                    Exit Select



            End Select

            If contenttype <> String.Empty Then

                Dim bitmapData() As Byte
                Dim sbText As StringBuilder = New StringBuilder(fuUpload, fuUpload.Length)
                sbText.Replace("\r\n", String.Empty)
                sbText.Replace(" ", String.Empty)
                Dim firststring As String = fuUpload.Substring(0, fuUpload.IndexOf(",") + 1)
                sbText.Replace(firststring, String.Empty)
                bitmapData = Convert.FromBase64String(sbText.ToString())

                'insert the file into database 

                Dim strQuery As String = "OASIS_DOCS.dbo.InsertDocumenttoDB"
                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@DOC_PCD_ID", PCD_ID)
                cmd.Parameters.AddWithValue("@DOC_DOCNO", filename)
                cmd.Parameters.AddWithValue("@DOC_CONTENT_TYPE", contenttype)
                cmd.Parameters.AddWithValue("@DOC_TYPE", "COVID")
                cmd.Parameters.AddWithValue("@DOC_DOCUMENT", bitmapData)
                cmd.Parameters.AddWithValue("@DOC_BSU_ID", bsuid)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = con
                cmd.Transaction = tran
                cmd.ExecuteNonQuery()
                Return True

            Else
                '   lblmsg.CssClass = "divInValidMsg"
                '  lblmsg.Text = "<i class='fa fa-2x fa-info text-danger v-align-middle mr-3'></i> File format not recognised." & " Upload Image/Word/PDF/Excel formats"
                Return False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function SaveCOVIDData(ByVal bsuid As String, ByVal COV_TEST_DATE As String, ByVal COV_RESULT As String, ByVal usrname As String, ByVal filepath As String, ByVal fileext As String, ByVal filename As String) As String
        Dim returnstring As String = ""

        If COV_TEST_DATE = "0" Or COV_TEST_DATE = "" Or COV_TEST_DATE Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter date of COVID test"
            Return returnstring
        End If

        If COV_RESULT = "0" Or COV_RESULT = "" Or COV_RESULT Is Nothing Then
            returnstring = "1000" + "||" + "0" + "||" + "Please enter test result"
            Return returnstring
        End If

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim retMsg As String = ""
            Dim bIsLeaveExceeded As Boolean = False


            'Dim pParms(7) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@COV_EMP_ID", SqlDbType.BigInt)
            'pParms(0).Value = HttpContext.Current.Session("EmployeeID")

            'pParms(1) = New SqlClient.SqlParameter("@COV_LOGDATE", SqlDbType.DateTime)
            'pParms(1).Value = Today.Date

            'pParms(2) = New SqlClient.SqlParameter("@COV_TEST_DATE", SqlDbType.DateTime)
            'pParms(2).Value = COV_TEST_DATE.Trim

            'pParms(3) = New SqlClient.SqlParameter("@COV_RESULT", SqlDbType.VarChar, 200)
            'pParms(3).Value = COV_RESULT

            'pParms(4) = New SqlClient.SqlParameter("@COV_UPLOAD_FILE", SqlDbType.VarChar, 200)
            'pParms(4).Value = filepath


            Dim pParms(5) As SqlClient.SqlParameter 'V1.2 ,V1.3
            pParms(0) = Mainclass.CreateSqlParameter("@COV_EMP_ID", HttpContext.Current.Session("EmployeeID"), SqlDbType.BigInt)
            ' pParms(1) = Mainclass.CreateSqlParameter("@ELA_EMP_ID", EOS_MainClass.GetEmployeeIDFromUserName(usrname), SqlDbType.VarChar)
            pParms(1) = Mainclass.CreateSqlParameter("@COV_LOGDATE", Today.Date, SqlDbType.DateTime)
            pParms(2) = Mainclass.CreateSqlParameter("@COV_TEST_DATE", COV_TEST_DATE.Trim, SqlDbType.DateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@COV_RESULT", COV_RESULT, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@COV_UPLOAD_FILE", filepath, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@COV_ID", 0, SqlDbType.Int, True)

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Save_EMP_COVID_TESTS", pParms)

            If pParms(5).Value > 0 Then
                If Not filepath Is Nothing Then
                    If (filepath <> "") Then
                        If Not SaveCOVIDDocument(filepath, objConn, stTrans, pParms(5).Value, bsuid, fileext, filename) Then
                            retval = "991"
                        End If
                    End If
                End If
            End If


            If pParms(5).Value > 0 Then
                retval = 0
                stTrans.Commit()
                returnstring = retval + "||" + pParms(5).ToString + "||" + "Successfully Saved, Click close button to proceed"

            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            If ex.Message <> "" Then
                returnstring = "1000" + "||" + "0" + "||" + ex.Message
            Else
                returnstring = "1000" + "||" + "0" + "||" + UtilityObj.getErrorMessage(1000)
            End If

            stTrans.Rollback()
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return returnstring
    End Function

    Protected Sub OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            'Reference the Repeater Item.
            Dim item As RepeaterItem = e.Item

            'Reference the Controls.
            Dim imgDoc As HyperLink = (TryCast(item.FindControl("imgDoc"), HyperLink))
            If (TryCast(item.FindControl("hfCOVDOC_ID"), HiddenField)).Value > 0 Then
                Dim hfCOV_ID As String = (TryCast(item.FindControl("hfCOVDOC_ID"), HiddenField)).Value
                imgDoc.Visible = True
                imgDoc.NavigateUrl = "~/Common/FileHandler.ashx?TYPE=" & Encr_decrData.Encrypt("PCDOCDOWNLOAD") & "&DocID=" & Encr_decrData.Encrypt(hfCOV_ID)
            Else imgDoc.Visible = False
            End If
        End If
    End Sub
End Class


