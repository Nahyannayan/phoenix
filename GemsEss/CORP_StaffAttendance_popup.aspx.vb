﻿Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports System.Collections
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.[Shared]
Imports System.IO

Partial Class OASIS_HR_CORP_StaffAttendance_popup
    Inherits System.Web.UI.Page
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    'Dim smScriptManager As New ScriptManager
    '    'smScriptManager = Master.FindControl("ScriptManager1")

    '    'smScriptManager.EnablePartialRendering = False

    '    Dim form1 As New HtmlForm
    '    form1 = Master.FindControl("form1")
    '    form1.Enctype = "multipart/form-data"
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = True
    '    smScriptManager.RegisterPostBackControl(btnDownloadPdf)
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim todate As Date = Today
            txtFrom.Text = todate.ToString
            txtTo.Text = todate.ToString
            Bind_CorpStaffAttendanceReport()
            '   Bind_Employee()
            ''btnDownloadPdf.Visible = False
            hidOnLoad.Value = 0
        Else
            hidOnLoad.Value = 1

        End If
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownloadPdf)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        If Page.IsValid Then
            ' btnDownloadPdf.Visible = True
            Bind_CorpStaffAttendanceReport()
        Else
            ' btnDownloadPdf.Visible = False
        End If
    End Sub

    Protected Sub gv_CorpStaffAttendanceReport_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        Try
            gv_CorpStaffAttendanceReport.DataSource = Session("DataSetAtt")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gv_CorpStaffAttendanceReport_DetailTableDataBind(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridDetailTableDataBindEventArgs)
      

        Dim parentItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)
        Try
            Select Case e.DetailTableView.Name
                Case "StaffDetails"

                    If True Then
                        Dim detailTable As GridTableView = CType(parentItem.ChildItem.NestedTableViews(0), GridTableView)
                        If Not String.IsNullOrEmpty(parentItem.GetDataKeyValue("StaffId").ToString()) Then
                            Dim employeeId As Integer = Convert.ToInt32(parentItem.GetDataKeyValue("EmployeeId"))
                            Dim staffId As Integer = Convert.ToInt32(parentItem.GetDataKeyValue("StaffId"))
                            Dim currentDate As DateTime = Convert.ToDateTime(parentItem.GetDataKeyValue("AttendanceDate"))
                            detailTable.DataSource = GetCorpAttendancePerStaffData(employeeId, staffId, currentDate)
                        Else
                            detailTable.DataSource = New String() {}
                        End If
                    End If

                    Exit Select
            End Select
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Protected Sub btnDownloadPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim objRptClass As rptClass = New rptClass()
    '        Dim fromDate = If(Not String.IsNullOrEmpty(txtFrom.Text), Convert.ToDateTime(txtFrom.Text), CType(Nothing, DateTime?))
    '        Dim toDate = If(Not String.IsNullOrEmpty(txtTo.Text), Convert.ToDateTime(txtTo.Text), CType(Nothing, DateTime?))
    '        ''Employee dropdown functionality disabled as per the requirements
    '        'Dim employeeId = If(Not String.IsNullOrEmpty(cmbEmployee.SelectedValue), Convert.ToInt32(cmbEmployee.SelectedValue), CType(Nothing, Integer?))
    '        Dim employeeId = Convert.ToString(HttpContext.Current.Session("EmployeeId"))
    '        'objRptClass.crDatabase = "OASIS_CORP_ACCESS_CONTROL"
    '        'Dim param As Hashtable = New Hashtable()
    '        'param.Add("@EMP_ID", employeeId)
    '        'param.Add("@FROM_DATE", fromDate)
    '        'param.Add("@TO_DATE", toDate)
    '        'objRptClass.reportParameters = param
    '        'objRptClass.reportPath = Server.MapPath("~/Reports/RPT_Files/rptWeeklyAttendance.rpt")
    '        'Dim objRptDownload As ReportDownload = New ReportDownload()
    '        'objRptDownload.LoadReports(objRptClass, rs)
    '        'objRptDownload = Nothing


    '        UtilityObj.Errorlog("1", "CORPSTAFF")
    '        Dim param As New Hashtable
    '        param.Add("@EMP_ID", employeeId)
    '        param.Add("@FROM_DATE", fromDate)
    '        param.Add("@TO_DATE", toDate)
    '        UtilityObj.Errorlog(employeeId.ToString, "CORPSTAFF")
    '        Dim rptClass As New rptClass
    '        With rptClass
    '            .crDatabase = "OASIS"
    '            .reportParameters = param
    '            .reportPath = Server.MapPath("Reports/rptWeeklyAttendance.rpt")
    '        End With
    '        UtilityObj.Errorlog("2", "CORPSTAFF")
    '        Dim rptDownload As New ReportDownload
    '        rptDownload.LoadReports(rptClass, rs)
    '        rptDownload = Nothing
    '        UtilityObj.Errorlog("3", "CORPSTAFF")
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "CORPSTAFF")
    '    End Try
    'End Sub

    Protected Sub cmbEmployee_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("Name").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("Id").ToString()
    End Sub

    Private Sub Bind_CorpStaffAttendanceReport()
        Dim fromDate = If(Not String.IsNullOrEmpty(txtFrom.Text), Convert.ToDateTime(txtFrom.Text), CType(Nothing, DateTime?))
        Dim toDate = If(Not String.IsNullOrEmpty(txtTo.Text), Convert.ToDateTime(txtTo.Text), CType(Nothing, DateTime?))

        If (fromDate > toDate) Then
            lblError.Text = " To date cannot be less that from date"
            Exit Sub
        End If
        ''Employee dropdown functionality disabled as per the requirements
        'Dim employeeId = If(Not String.IsNullOrEmpty(cmbEmployee.SelectedValue), Convert.ToInt32(cmbEmployee.SelectedValue), CType(Nothing, Integer?))
        Dim employeeId = Convert.ToString(HttpContext.Current.Session("EmployeeId"))

        Try
            If fromDate.HasValue AndAlso toDate.HasValue Then
                gv_CorpStaffAttendanceReport.DataSource = GetCorpStaffAttendanceData(employeeId, fromDate, toDate)
                gv_CorpStaffAttendanceReport.DataBind()
            Else
                gv_CorpStaffAttendanceReport.DataSource = New String() {}
                ''gv_CorpStaffAttendanceReport.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Overrides Sub Validate(ByVal validationGroup As String)
        If validationGroup.Contains(",") Then
            Dim validationGroups As String() = validationGroup.Split(",".ToCharArray())

            For Each group As String In validationGroups
                Page.Validate(group)
            Next
        End If

        MyBase.Validate(validationGroup)
    End Sub

    Private Function GetCorpStaffAttendanceData(ByVal employeeId As Integer?, ByVal fromDate As DateTime?, ByVal toDate As DateTime?) As DataSet
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@EMP_ID", employeeId)
        parameters(1) = New SqlParameter("@FROM_DATE", fromDate)
        parameters(2) = New SqlParameter("@TO_DATE", toDate)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetCORPAccessConnectionString(), CommandType.StoredProcedure, "[dbo].[GenerateFinalAttendanceLogPerStaffData]", parameters)
        Session("DataSetAtt") = ds
        Return ds
    End Function

    Private Function GetCorpAttendancePerStaffData(ByVal employeeId As Integer, ByVal staffId As Integer, ByVal currentDate As DateTime) As DataSet
        Dim parameters As SqlParameter() = New SqlParameter(4) {}
        parameters(0) = New SqlParameter("@EMP_ID", employeeId)
        parameters(2) = New SqlParameter("@StaffId", staffId)
        parameters(1) = New SqlParameter("@CurrentDate", currentDate)
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetCORPAccessConnectionString(), CommandType.StoredProcedure, "[dbo].[GenerateAttendancePerStaffData]", parameters)
    End Function

    ''Employee dropdown functionality disabled as per the requirements
    'Private Sub Bind_Employee()
    '    Try
    '        Dim employeeId = Convert.ToString(HttpContext.Current.Session("EmployeeId"))
    '        'Dim employeeId = "9235"
    '        Dim query As String = "SELECT Id, Name FROM (" & " SELECT E.EMP_ID As Id, ISNULL(e.EMP_FNAME,'') + ' ' + ISNULL(e.EMP_MNAME,'') + ' ' + ISNULL(e.EMP_LNAME,'') AS Name, 0 AS RowIndex" & " FROM dbo.EMPLOYEE_M E " & " WHERE  (EMP_ID = " & employeeId & ")" & " UNION" & " SELECT E.EMP_ID As Id, ISNULL(e.EMP_FNAME,'') + ' ' + ISNULL(e.EMP_MNAME,'') + ' ' + ISNULL(e.EMP_LNAME,'') AS Name, 1 AS RowIndex" & " FROM dbo.EMPLOYEE_M E" & " WHERE  (EMP_REPORTTO_EMP_ID = " & employeeId & ") AND E.EMP_STATUS in (1,2,9)" & " ) T " & " ORDER BY T.RowIndex, T.Name"
    '        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.Text, query)

    '        If ds IsNot Nothing Then
    '            cmbEmployee.DataSource = ds
    '            cmbEmployee.DataBind()
    '        End If

    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Protected Sub cmbEmployee_DataBound(sender As Object, e As EventArgs)
    '    'set the initial footer label
    '    CType(cmbEmployee.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbEmployee.Items.Count)
    'End Sub

   
End Class
