﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports Telerik
Imports Telerik.Web.UI


Partial Class homepage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim userMod As String = Session("sModule")

        Dim userRol As String = Session("sroleid")
        Dim bsu As String = Session("sBsuid")
        BIND_DASHBOARD(userRol, userMod, bsu)
        isDocumentExpire(Session("sUsr_name"))
    End Sub
    Private Sub BIND_DASHBOARD(ByVal userRol As String, ByVal userMod As String, ByVal Bsu As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlParameter

        param(0) = New SqlParameter("@ROL_ID", userRol)
        param(1) = New SqlParameter("@MODULE", userMod)
        param(2) = New SqlParameter("@BSU_ID", Bsu)

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "BIND_DASHBOARD_PAGE", param)
            rptDashboard.DataSource = datareader
            rptDashboard.DataBind()
        End Using
    End Sub
    Public Function bindChart(ByVal bunit As String, ByVal userRol As String, ByVal userMod As String, ByVal user As String, ByVal id As String) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter


            PARAM(0) = New SqlParameter("@BSU_ID", bunit)
            PARAM(1) = New SqlParameter("@MODULE_CODE", userMod)
            PARAM(2) = New SqlParameter("@ROL_ID", userRol)
            PARAM(3) = New SqlParameter("@USER_NAME", user)
            PARAM(4) = New SqlParameter("@ID", id)
            PARAM(5) = New SqlClient.SqlParameter("@DASHBOARD_NAMES", SqlDbType.VarChar, 1000)
            PARAM(5).Direction = ParameterDirection.Output


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SP_OASIS_GENERIC_DASHBOARD_NEW", PARAM)

            Return dsDetails


        Catch ex As Exception

        End Try
    End Function
    Protected Sub rptDashboard_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDashboard.ItemDataBound
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim item As RepeaterItem = e.Item

            Dim hdnId As New HiddenField
            Dim hdnType As New HiddenField
            Dim hdnTitle As New HiddenField
            Dim hdndrilldown As New HiddenField
            Dim hdnPage As New HiddenField
            Dim hdnRedirect As New HiddenField


            Dim divBar As New HtmlGenericControl
            Dim divPie As New HtmlGenericControl
            Dim divDonut As New HtmlGenericControl
            Dim divGauge As New HtmlGenericControl
            Dim divSeries As New HtmlGenericControl
            Dim divLine As New HtmlGenericControl
            Dim divStacked As New HtmlGenericControl
            Dim divRepeater As New HtmlGenericControl
            Dim divGrid As New HtmlGenericControl

            Dim radhtmlchart1 As New RadHtmlChart
            Dim radhtmlchart2 As New RadHtmlChart
            Dim radhtmlchart3 As New RadHtmlChart
            Dim radhtmlchart4 As New RadHtmlChart
            Dim radhtmlchart5 As New RadHtmlChart
            Dim radhtmlchart6 As New RadHtmlChart
            Dim RadRadialGauge1 As New RadRadialGauge
            Dim rptRepeater As New Repeater
            Dim RadGrid1 As New RadGrid

            Dim lblBar As New Label
            Dim lblPie As New Label
            Dim lblDonut As New Label
            Dim lblSeries As New Label
            Dim lblLine As New Label
            Dim lblStacked As New Label
            Dim lblGauge As New Label
            Dim lblGaugeVal As New Label
            Dim lblRepeater As New Label
            Dim lblGrid As New Label

            Dim btnBar As New Button
            Dim btnPie As New Button
            Dim btnDonut As New Button
            Dim btnGauge As New Button
            Dim btnSeries As New Button
            Dim btnLine As New Button
            Dim btnStacked As New Button
            Dim btnRepeater As New Button
            Dim btnGrid As New Button

            hdnId = TryCast(item.FindControl("hdnId"), HiddenField)
            hdnType = TryCast(item.FindControl("hdnType"), HiddenField)
            hdnTitle = TryCast(item.FindControl("hdnTitle"), HiddenField)
            hdndrilldown = TryCast(item.FindControl("hdndrilldown"), HiddenField)
            hdnPage = TryCast(item.FindControl("hdnPage"), HiddenField)
            hdnRedirect = TryCast(item.FindControl("hdnRedirect"), HiddenField)

            divBar = TryCast(item.FindControl("divBar"), HtmlGenericControl)
            divPie = TryCast(item.FindControl("divPie"), HtmlGenericControl)
            divDonut = TryCast(item.FindControl("divDonut"), HtmlGenericControl)
            divGauge = TryCast(item.FindControl("divGauge"), HtmlGenericControl)
            divSeries = TryCast(item.FindControl("divSeries"), HtmlGenericControl)
            divLine = TryCast(item.FindControl("divLine"), HtmlGenericControl)
            divStacked = TryCast(item.FindControl("divStacked"), HtmlGenericControl)
            divRepeater = TryCast(item.FindControl("divRepeater"), HtmlGenericControl)
            divGrid = TryCast(item.FindControl("divGrid"), HtmlGenericControl)

            radhtmlchart1 = TryCast(item.FindControl("radhtmlchart1"), RadHtmlChart)
            radhtmlchart2 = TryCast(item.FindControl("radhtmlchart2"), RadHtmlChart)
            radhtmlchart3 = TryCast(item.FindControl("radhtmlchart3"), RadHtmlChart)
            radhtmlchart4 = TryCast(item.FindControl("radhtmlchart4"), RadHtmlChart)
            radhtmlchart5 = TryCast(item.FindControl("radhtmlchart5"), RadHtmlChart)
            radhtmlchart6 = TryCast(item.FindControl("radhtmlchart6"), RadHtmlChart)
            RadRadialGauge1 = TryCast(item.FindControl("RadRadialGauge1"), RadRadialGauge)
            rptRepeater = TryCast(item.FindControl("rptRepeater"), Repeater)
            RadGrid1 = TryCast(item.FindControl("RadGrid1"), RadGrid)

            lblBar = TryCast(item.FindControl("lblBar"), Label)
            lblPie = TryCast(item.FindControl("lblPie"), Label)
            lblDonut = TryCast(item.FindControl("lblDonut"), Label)
            lblSeries = TryCast(item.FindControl("lblSeries"), Label)
            lblLine = TryCast(item.FindControl("lblLine"), Label)
            lblStacked = TryCast(item.FindControl("lblStacked"), Label)
            lblGauge = TryCast(item.FindControl("lblGauge"), Label)
            lblGaugeVal = TryCast(item.FindControl("lblGaugeVal"), Label)
            lblRepeater = TryCast(item.FindControl("lblRepeater"), Label)
            lblGrid = TryCast(item.FindControl("lblGrid"), Label)

            btnBar = TryCast(item.FindControl("btnBar"), Button)
            btnPie = TryCast(item.FindControl("btnPie"), Button)
            btnDonut = TryCast(item.FindControl("btnDonut"), Button)
            btnGauge = TryCast(item.FindControl("btnGauge"), Button)
            btnSeries = TryCast(item.FindControl("btnSeries"), Button)
            btnLine = TryCast(item.FindControl("btnLine"), Button)
            btnStacked = TryCast(item.FindControl("btnStacked"), Button)
            btnRepeater = TryCast(item.FindControl("btnRepeater"), Button)
            btnGrid = TryCast(item.FindControl("btnGrid"), Button)

            Dim ds As DataSet = bindChart(Session("sBsuid"), Session("sroleid"), Session("sModule"), Session("sUsr_name"), hdnId.Value)
            If hdnType.Value = "Bar" Then
                divBar.Visible = True
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = False
                radhtmlchart1.DataSource = ds.Tables(0)
                radhtmlchart1.DataBind()
                lblBar.Text = hdnTitle.Value
                btnBar.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnBar.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnBar.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
            If hdnType.Value = "Pie" Then
                divBar.Visible = False
                divPie.Visible = True
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = False
                radhtmlchart2.DataSource = ds.Tables(0)
                radhtmlchart2.DataBind()
                lblPie.Text = hdnTitle.Value
                btnPie.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnPie.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnPie.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
            If hdnType.Value = "Donut" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = True
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = False
                radhtmlchart3.DataSource = ds.Tables(0)
                radhtmlchart3.DataBind()
                lblDonut.Text = hdnTitle.Value
                btnDonut.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnDonut.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnDonut.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
            If hdnType.Value = "Series" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = True
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = False
                radhtmlchart4.DataSource = ds.Tables(0)
                radhtmlchart4.DataBind()
                lblSeries.Text = hdnTitle.Value
                btnSeries.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnSeries.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnSeries.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If

                ''by nahyan on 2dec2018 
                Dim dt As New DataTable
                If Not ds.Tables(0) Is Nothing Then
                    dt = ds.Tables(0)
                    If Not dt Is Nothing Then
                        If dt.Rows.Count > 0 Then
                            radhtmlchart4.PlotArea.Series(0).Name = Convert.ToString(dt.Rows(0)("NAME1"))
                            radhtmlchart4.PlotArea.Series(1).Name = Convert.ToString(dt.Rows(0)("NAME2"))
                            radhtmlchart4.PlotArea.Series(2).Name = Convert.ToString(dt.Rows(0)("NAME3"))
                        End If
                    End If
                End If
            End If
            If hdnType.Value = "Line" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = True
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = False
                radhtmlchart5.DataSource = ds.Tables(0)
                radhtmlchart5.DataBind()
                lblLine.Text = hdnTitle.Value
                btnLine.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnLine.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnLine.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
            If hdnType.Value = "Stacked" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = True
                divRepeater.Visible = False
                divGrid.Visible = False
                radhtmlchart6.DataSource = ds.Tables(0)
                radhtmlchart6.DataBind()
                lblStacked.Text = hdnTitle.Value
                btnStacked.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnStacked.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnStacked.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
                ''by nahyan on 2dec2018 
                Dim dt As New DataTable
                If Not ds.Tables(0) Is Nothing Then
                    dt = ds.Tables(0)
                    If Not dt Is Nothing Then
                        If dt.Rows.Count > 0 Then
                            radhtmlchart6.PlotArea.Series(0).Name = Convert.ToString(dt.Rows(0)("X1"))
                            radhtmlchart6.PlotArea.Series(1).Name = Convert.ToString(dt.Rows(0)("X2"))

                        End If
                    End If
                End If

            End If
            If hdnType.Value = "Repeater" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = True
                divGrid.Visible = False
                rptRepeater.DataSource = ds.Tables(0)
                rptRepeater.DataBind()
                lblRepeater.Text = hdnTitle.Value
                btnRepeater.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnRepeater.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnRepeater.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
            If hdnType.Value = "Grid" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = False
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = True
                RadGrid1.DataSource = ds.Tables(0)
                RadGrid1.DataBind()
                lblGrid.Text = hdnTitle.Value
                btnGrid.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnGrid.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnGrid.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
            If hdnType.Value = "Gauge" Then
                divBar.Visible = False
                divPie.Visible = False
                divDonut.Visible = False
                divGauge.Visible = True
                divSeries.Visible = False
                divLine.Visible = False
                divStacked.Visible = False
                divRepeater.Visible = False
                divGrid.Visible = False
                RadRadialGauge1.Pointer.Value = ds.Tables(0).Rows(0)(0).ToString
                lblGaugeVal.Text = ds.Tables(0).Rows(0)(0).ToString
                lblGauge.Text = hdnTitle.Value
                btnGauge.Visible = hdndrilldown.Value
                If hdnRedirect.Value = True Then
                    btnGauge.Attributes.Add("OnClick", "redirectDetails('" + hdnPage.Value + "');return false;")
                Else
                    btnGauge.Attributes.Add("OnClick", "ViewDetails('" + hdnPage.Value + "');return false;")
                End If
            End If
        End If
    End Sub

    Private Function isDocumentExpire(ByVal username As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select USR_EXPDAYS from USERS_M where USR_NAME='" & username & "'"
        Dim DocExpDate As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not DocExpDate Is DBNull.Value Then
            Session("DocExpDays") = CInt(DocExpDate)
        Else
            Session("DocExpDays") = 30
        End If
      
    End Function
End Class
