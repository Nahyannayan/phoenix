﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="homepage.aspx.vb" Inherits="homepage" %>

<%@ Register TagPrefix="Telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="/cssfiles/box.css" rel="stylesheet" />
    <style>
        .k-chart, .k-gauge, .k-sparkline, .k-stockchart, .k-chart text {
            font-family: Raleway, sans-serif !important;
        }
        .Label1
		{
			position: absolute;
			font-weight: bold;
			font-size: 12px;
			top: 70%;
			left: 47%;
		}
    </style>
     <script>
         function redirectDetails(url) {


             window.open(url, "_self");

         }
     function ViewDetails(url) {
             
           
         PopupHome(url);

     }
     function PopupHome(url) {
         $.fancybox({
             'width': '80%',
             'height': '600px',
             'autoScale': true,
             'transitionIn': 'fade',
             'transitionOut': 'fade',
             'type': 'iframe',
             'href': url
         });
     };
</script>
    <section class="col-md-12 col-lg-12 col-sm-12 colContainerBox">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group">
                <div class="row">
                    <asp:Repeater ID="rptDashboard" runat="server">
                        <ItemTemplate>
                            <div class='<%# Eval("DBS_CLASS")%>' style="padding-bottom: 2%;">
                                <div class="card" style=" box-shadow: 4px 4px 10px rgba(0,0,0,0.16);">
                                        <div id="divBar" runat="server">
                                         <div class="card-header card-dash"><asp:Label id="lblBar" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnBar" class="button float-right" text="View More" /></div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart1" Height="250" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:ColumnSeries Name="X" DataFieldY="Y">
                                                        <TooltipsAppearance  >
                                                            <ClientTemplate>#=dataItem.Z# </ClientTemplate>
                                                            </TooltipsAppearance>
                                                        <LabelsAppearance Visible="true" />
                                                    </Telerik:ColumnSeries>
                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                     <MajorGridLines Visible="false" />
                                                     <MinorGridLines Visible="false"/>
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                    <LabelsAppearance  />
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Visible="false" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divPie" runat="server">
                                        <div class="card-header card-dash"><asp:Label id="lblPie" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnPie" class="button float-right" text="View More"/></div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart2" Height="250" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:PieSeries DataFieldY="Y" NameField="X">
                                                        <LabelsAppearance >
                                                        </LabelsAppearance>
                                                        <TooltipsAppearance>
                                                            <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:PieSeries>
                                                </Series>
                                                <YAxis>
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>

                                                <Appearance Position="Right" Visible="true" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divDonut" runat="server">
                                        <div class="card-header card-dash"><asp:Label id="lblDonut" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnDonut" class="button float-right" text="View More"/></div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart3" Height="250" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:DonutSeries DataFieldY="Y" NameField="X">
                                                        <LabelsAppearance >
                                                        </LabelsAppearance>
                                                        <TooltipsAppearance>
                                                            <ClientTemplate>#=dataItem.X# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:DonutSeries>
                                                </Series>
                                                <YAxis>
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Position="Right" Visible="true" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>

                                    <div id="divGauge" runat="server">
                                        <div class="card-header card-dash"><asp:Label id="lblGauge" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnGauge" class="button float-right" text="View More"/></div>
                                       
                                        <Telerik:RadRadialGauge runat="server" ID="RadRadialGauge1" Height="250px">
                                            <Pointer Value="50.2" >
                                                <Cap Size="0.1"  />
                                            </Pointer>
                                            <Scale Min="0" Max="100" MajorUnit="20">
                                                <Labels Format="{0}" />
                                                <Ranges>
                                                    <Telerik:GaugeRange Color="#c20000" From="20" To="40" />
                                                    <Telerik:GaugeRange Color="#ff7a00" From="40" To="60" />
                                                    <Telerik:GaugeRange Color="#ffc700" From="60" To="80" />
                                                    <Telerik:GaugeRange Color="#8dcb2a" From="80" To="100" />

                                                </Ranges>
                                            </Scale>
                                        </Telerik:RadRadialGauge>
                                         <asp:Label ID="lblGaugeVal" Text="97" runat="server" CssClass="Label1" />
                                    </div>
                                    <div id="divSeries" runat="server">
                                         <div class="card-header card-dash"><asp:Label id="lblSeries" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnSeries" class="button float-right" text="View More"/></div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart4" Height="250" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:ColumnSeries DataFieldY="Y1" Name="">

                                                        <TooltipsAppearance DataFormatString="{0}" />

                                                        <LabelsAppearance Visible="true" />

                                                    </Telerik:ColumnSeries>

                                                    <Telerik:ColumnSeries DataFieldY="Y2" Name="" >
                                                        
                                                        <TooltipsAppearance DataFormatString="{0}" />

                                                        <LabelsAppearance Visible="true" />

                                                    </Telerik:ColumnSeries>
                                                    <Telerik:ColumnSeries DataFieldY="Y3" Name="">

                                                        <TooltipsAppearance DataFormatString="{0}" />

                                                        <LabelsAppearance Visible="true" />

                                                    </Telerik:ColumnSeries>
                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Position="Bottom">
                                                </Appearance>
                                            </Legend>
                                            
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divLine" runat="server">
                                         <div class="card-header card-dash"><asp:Label id="lblLine" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnLine" class="button float-right" text="View More"/></div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart5" Height="250px" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:LineSeries DataFieldY="Y1" Name="2016">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </Telerik:LineSeries>
                                                    <Telerik:LineSeries DataFieldY="Y2" Name="2017">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </Telerik:LineSeries>
                                                    <Telerik:LineSeries DataFieldY="Y3" Name="2018">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </Telerik:LineSeries>
                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </YAxis>
                                            </PlotArea>
                                            
                                            
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divStacked" runat="server">
                                         <div class="card-header card-dash"><asp:Label id="lblStacked" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnStacked" class="button float-right" Text="View More"/></div>
                                        <Telerik:RadHtmlChart runat="server" ID="RadHtmlChart6" Height="250px" Skin="Metro">
                                            <PlotArea>
                                                <Series>
                                                    <Telerik:ColumnSeries  DataFieldY="Y1"  Stacked="true" Name="">
                                                        <LabelsAppearance Visible="true" Position="Center" >  <ClientTemplate>#=dataItem.Y1# </ClientTemplate></LabelsAppearance>
                                                        <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.X1# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:ColumnSeries>
                                                    <Telerik:ColumnSeries DataFieldY="Y2" Stacked="true"  Name="">
                                                        <LabelsAppearance Visible="true" Position="Center"> <ClientTemplate>#=dataItem.Y2# </ClientTemplate></LabelsAppearance>
                                                        <TooltipsAppearance Visible="true">
                                                             <ClientTemplate>#=dataItem.X2# </ClientTemplate>
                                                        </TooltipsAppearance>
                                                    </Telerik:ColumnSeries>

                                                </Series>
                                                <XAxis DataLabelsField="X">
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                    <LabelsAppearance Visible="true"></LabelsAppearance>
                                                </XAxis>
                                                <YAxis>
                                                    <MajorGridLines Visible="false" />
                                                    <MinorGridLines Visible="false" />
                                                </YAxis>
                                            </PlotArea>
                                            <Legend>
                                                <Appearance Position="Bottom" Visible="true" />
                                            </Legend>
                                           
                                        </Telerik:RadHtmlChart>
                                    </div>
                                    <div id="divRepeater" runat="server">
                                        <div class="card-header card-dash"><asp:Label id="lblRepeater" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnRepeater" class="button float-right" text="View More"/></div>
                                        <div class="row">
                                       <asp:Repeater ID="rptRepeater" runat="server">
                                           <ItemTemplate>
                                       <div class="col-sm-2">
					
					               
					                <div class='<%# Eval("CSS")%>'>
					                    <div class="pad-all text-center">
					                        <span class="text-3x text-thin"><%# Eval("X")%></span>
					                        <p><%# Eval("Y")%></p>
					                        
					                    </div>
					                </div>
					               </div>
                                           </ItemTemplate>
                                       </asp:Repeater>
                                            </div>
                                    </div>
                                    <div id="divGrid" runat="server">
                                        <div class="card-header card-dash"><asp:Label id="lblGrid" runat="server" CssClass="card-dash-title"></asp:Label>
                                  <asp:Button runat="server" id="btnGrid" class="button float-right" text="View More"/></div>
                                       <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" AllowPaging="false" CellSpacing="0"
                            PageSize="5" GridLines="Both" CssClass="table table-bordered table-row" Width="100%" >
                            <MasterTableView>
                            </MasterTableView>
                            <HeaderStyle BackColor="#F5F5F5"   HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <AlternatingItemStyle  HorizontalAlign="Center" />
                            <PagerStyle Mode="NextPrevNumericAndAdvanced"></PagerStyle>
                        </telerik:RadGrid>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("DBM_ID")%>' />
                            <asp:HiddenField ID="hdnType" runat="server" Value='<%# Eval("DBM_TYPE")%>' />
                            <asp:HiddenField ID="hdnTitle" runat="server" Value='<%# Eval("DBM_NAME")%>' />
                            <asp:HiddenField ID="hdndrilldown" runat="server" Value='<%# Eval("DBM_bDRILLDOWN")%>' />
                            <asp:HiddenField ID="hdnPage" runat="server" Value='<%# Eval("DBM_DRILLDOWNPAGE")%>' />
                             <asp:HiddenField ID="hdnRedirect" runat="server" Value='<%# Eval("DBM_bRedirect")%>' />
                        </ItemTemplate>

                    </asp:Repeater>




                </div>
            </div>
        </div>
    </section>
</asp:Content>

