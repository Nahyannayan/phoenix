Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Modulelogin
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

 


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Try
        Response.CacheControl = "no-cache"
        Session("sModule") = "S0"
        get_acd()
        Session("modPage") = "0"
        Session("Modulename") = ""
        Session("Menu_text") = ""
        If Page.IsPostBack = True Then

            '   ---- Access Database and Fill The Default Values
            Dim gdsSysInfo As New DataSet
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT * From sysinfo_s"
            gdsSysInfo = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If gdsSysInfo.Tables(0).Rows.Count > 0 Then
                Session("IntrAC") = gdsSysInfo.Tables(0).Rows(0)("SYS_INTEREST_SGP_ID")
                Session("AcrdAc") = gdsSysInfo.Tables(0).Rows(0)("SYS_ACCRINTR_CTRL_ACT_ID")
                Session("PrepdAC") = gdsSysInfo.Tables(0).Rows(0)("SYS_PREEXPPDC_SGP_ID")
                Session("ChqissAC") = gdsSysInfo.Tables(0).Rows(0)("SYS_CHQISSPDC_SGP_ID")
            End If

        Else

            ''nahyan for user with multiple bsu

          

            'Session("sModule") = "SS"
            SetUsrModuleForNonBsu()
            Dim feedback As String = BIND_feedback()

            If feedback = "1" Then
                ' ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "popup();", True)
                Dim script As String = "window.onload = function() { popup(); };"
                ClientScript.RegisterStartupScript(Me.GetType(), "pop", script, True)
            End If

        End If
        'Catch ex As Exception
        '    Dim url As String
        '    url = String.Format("login.aspx?logoff=1")
        '    Response.Redirect(url)
        'End Try
    End Sub
    Private Function BIND_feedback() As String
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            param(1) = New SqlParameter("@USER_NAME", Session("sUsr_name"))
            param(2) = New SqlParameter("@MODULE_ID", "")

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "GET_FEEDBACK_SHOW_STATUS", param)
            If DS.Tables(0).Rows.Count >= 1 Then

                Return DS.Tables(0).Rows(0)("MY_STATUS").ToString()

            End If
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Function
    Private Sub SetUsrModuleForNonBsu()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(2) As SqlClient.SqlParameter

        Try

            param(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar)
            param(0).Value = Session("sUsr_name")
            param(1) = New SqlClient.SqlParameter("@USR_BSU_ID", SqlDbType.Int)
            param(1).Value = Session("sBsuid")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetUserModuleAccessPhoenix", param)

            If Not ds Is Nothing AndAlso Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim Dt As DataTable = ds.Tables(0)

                If Dt.Rows.Count > 0 Then
                    Session("sModule") = Convert.ToString(Dt.Rows(0)("MNU_MODULE").ToString())

                End If

            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try



    End Sub
    Sub redirectToOasis()
        Dim url As String
        url = "https://school.gemsoasis.com/RedirectToOASIS.aspx?U=" + Encr_decrData.Encrypt(Session("sUsr_name")) + "&M=" + Encr_decrData.Encrypt(Session("sModule")) + "&Bs=" + Encr_decrData.Encrypt(Session("sBsuid")) + "&Fy=" + Encr_decrData.Encrypt(Session("F_YEAR")) + "&AxBsu=" + Encr_decrData.Encrypt(Session("BSU_IsOnDAX")) + "&AxBsuHR=" + Encr_decrData.Encrypt(Session("BSU_IsHROnDAX"))
        ''commented by nahyan to open oasis in same window 6nov2018
        'Dim sb As New System.Text.StringBuilder("")
        'sb.Append(Convert.ToString("popUpDetails('" + url + "');"))
        'ScriptManager.RegisterStartupScript(Page, GetType(Page), "reportshow", sb.ToString(), True)
        Response.Redirect(url, False)

    End Sub

    
    Protected Sub lbStud_Mag_Click(sender As Object, e As EventArgs) Handles lbStud_Mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "S0"
        Session("Modulename") = "Student Management"
        StoreSettings()
        Response.Redirect(url)
    End Sub
    Sub StoreSettings()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC update_module '" + Session("sUsr_name") + "','" + Session("sModule") + "'"
        SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Session("modPage") = "1"
    End Sub

    Protected Sub lbCur_mag_Click(sender As Object, e As EventArgs) Handles lbCur_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "C0"
        Session("Modulename") = "Curriculum Management"
        StoreSettings()
        Response.Redirect(url)
    End Sub

    Protected Sub lbCca_mag_Click(sender As Object, e As EventArgs) Handles lbCca_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "CC"
        Session("Modulename") = "Co-Curricular Activities"
        StoreSettings()
        'redirectToOasis()
        Response.Redirect(url)
    End Sub

    Protected Sub lbLib_mag_Click(sender As Object, e As EventArgs) Handles lbLib_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "LB"
        Session("Modulename") = "Library Management"
        StoreSettings()

        ''redirectToOasis()
        Response.Redirect(url)
    End Sub

    Protected Sub lbace_mag_Click(sender As Object, e As EventArgs) Handles lbace_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "SN"
        Session("Modulename") = "ACe"
        StoreSettings()
        Response.Redirect(url)
    End Sub

    

    Protected Sub lbFees_Click(sender As Object, e As EventArgs) Handles lbFees.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "F0"
        Session("Modulename") = "Fees Management"
        StoreSettings()
        Dim FeesAccess = GetAccesstoPHOENIXModule()

        If FeesAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If

        '' redirectToOasis()
        'Response.Redirect(url)
    End Sub

    Protected Sub lbPayroll_Click(sender As Object, e As EventArgs) Handles lbPayroll.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "P0"
        Session("Modulename") = "Payroll"
        StoreSettings()

        Dim payrollAccess = GetAccesstoPHOENIXModule()

        If payrollAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If
        '' redirectToOasis()
        'Response.Redirect(url)
    End Sub

    Protected Sub lbTran_mag_Click(sender As Object, e As EventArgs) Handles lbTran_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "T0"
        Session("Modulename") = "Transport Management"
        StoreSettings()

        Dim TransportAccess = GetAccesstoPHOENIXModule()

        If TransportAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If


        ''redirectToOasis()
        'Response.Redirect(url)
    End Sub

    Protected Sub lbFinance_mag_Click(sender As Object, e As EventArgs) Handles lbFinance_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "A0"
        Session("Modulename") = "Financial Accounting"
        StoreSettings()

        Dim FinAccess = GetAccesstoPHOENIXModule()

        If FinAccess = 1 Then
            Response.Redirect(url)
        ElseIf FinAccess = 2 Then
            ' redirectToOasis()
        ElseIf FinAccess = 0 Then
            redirectToOasis()

        End If
        'Response.Redirect(url)
    End Sub

    Protected Sub lbPur_mag_Click(sender As Object, e As EventArgs) Handles lbPur_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "PI"
        Session("Modulename") = "Purchase"
        StoreSettings()

        Dim PurchaseAccess = GetAccesstoPHOENIXModule()

        If PurchaseAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If

        'If Session("sUsr_name") <> "shakeel.shakkeer" AndAlso Session("sUsr_name") <> "jacob.chacko" AndAlso Session("sUsr_name") <> "sagi" AndAlso Session("sUsr_name") <> "mahesh.kumar" AndAlso Session("sUsr_name") <> "kapil.thomas" Then
        '    redirectToOasis()
        'Else
        '    Response.Redirect(url)
        'End If

        'Response.Redirect(url)
    End Sub

    Protected Sub lbSys_mag_Click(sender As Object, e As EventArgs) Handles lbSys_mag.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "D0"
        Session("Modulename") = "System Administration"
        StoreSettings()
        Dim sysadminAccess = GetAccesstoPHOENIXModule()

        If sysadminAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If
        ' redirectToOasis()
        'Response.Redirect(url)
    End Sub

    Protected Sub lbMessage_Click(sender As Object, e As EventArgs) Handles lbMessage.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "M0"
        Session("Modulename") = "Messaging"
        StoreSettings()
        ''commented and enabled last line redirect bynahyan on 17nov to enable messagnig on phoenix
        '' redirectToOasis()
        Response.Redirect(url)
    End Sub

    Protected Sub lbHR_Click(sender As Object, e As EventArgs) Handles lbHR.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "H0"
        Session("Modulename") = "HR"
        StoreSettings()

        Dim HRAccess = GetAccesstoPHOENIXModule()

        If HRAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If
        '' redirectToOasis()
        'Response.Redirect(url)
    End Sub

    Protected Sub lbPD_Click(sender As Object, e As EventArgs) Handles lbPD.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "CD"
        Session("Modulename") = "Professional Development"
        StoreSettings()

        Response.Redirect(url)
    End Sub

    Protected Sub lbFacility_Click(sender As Object, e As EventArgs) Handles lbFacility.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "FD"
        Session("Modulename") = "Facility & Service Management"
        StoreSettings()
        'redirectToOasis()
        Response.Redirect(url)
    End Sub

    Protected Sub lblEmpSelfSrv_Click(sender As Object, e As EventArgs) Handles lblEmpSelfSrv.Click
        Dim url As String
        url = String.Format("ESSDashboard.aspx")
        Session("sModule") = "SS"
        Session("Modulename") = "Employee Self Service"
        StoreSettings()

        Response.Redirect(url)
    End Sub
    Protected Sub lbDocument_Click(sender As Object, e As EventArgs) Handles lbDocument.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "DM"
        Session("Modulename") = "Contracts & Licensing Management"
        StoreSettings()

        Response.Redirect(url)
    End Sub
    Protected Sub lnkHelpDesk_Click(sender As Object, e As EventArgs) Handles lnkHelpDesk.Click
        Dim url As String
        url = String.Format("homepage.aspx")
        Session("sModule") = "HD"
        Session("Modulename") = "Help Desk"
        StoreSettings()

        Dim HdeskAccess = GetAccesstoPHOENIXModule()

        If HdeskAccess > 0 Then
            Response.Redirect(url)
        Else
            redirectToOasis()
        End If
        '' redirectToOasis()
        'Response.Redirect(url)
    End Sub
    Sub get_acd()
        Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(Session("sBsuid"), Session("CLM"))
            If readerAcademic_Cutoff.HasRows = True Then
                While readerAcademic_Cutoff.Read
                    If IsDate(readerAcademic_Cutoff("ACD_AGE_CUTOFF")) = True Then
                        Session("Cutoff_Age") = Convert.ToDateTime(readerAcademic_Cutoff("ACD_AGE_CUTOFF"))
                    Else
                        Session("Cutoff_Age") = ""
                    End If
                    Session("Current_ACY_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))
                    Session("Current_ACD_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ID"))
                End While
            End If
        End Using

        Using readerPrev_Next As SqlDataReader = AccessStudentClass.getNEXT_CURR_PREV(Session("sBsuid"), Session("CLM"), Session("Current_ACY_ID"))
            If readerPrev_Next.HasRows = True Then
                While readerPrev_Next.Read
                    Session("prev_ACD_ID") = Convert.ToString(readerPrev_Next("prev_ACD_ID"))
                    Session("next_ACD_ID") = Convert.ToString(readerPrev_Next("next_ACD_ID"))
                End While
            End If
        End Using
        If Session("prev_ACD_ID") = "" Then
            Session("prev_ACD_ID") = "0"
        End If
        If Session("next_ACD_ID") = "" Then
            Session("next_ACD_ID") = "0"
        End If
        Dim studClass As New studClass

        Session("EmployeeID") = studClass.GetEmpId(Session("sUsr_name")).ToString
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC checkEMPCURRICULUMCORDINATOR '" + Session("sUsr_name") + "'"
        Session("CurrSuperUser") = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        CheckMultiStream()
        CheckMultiShift()

        Try
            Session("accYear_rule") = Nothing
            Session("term_rule") = Nothing
            Session("grade_rule") = Nothing
            Session("subj_rule") = Nothing
            Session("schedule_rule") = Nothing
            Session("header_rule") = Nothing
        Catch ex As Exception
        End Try
    End Sub
    Sub CheckMultiStream()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT  COUNT(DISTINCT GRM_STM_ID) FROM GRADE_BSU_M WHERE GRM_BSU_ID='" + Session("SBSUID") + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count > 1 Then
            Session("multistream") = 1
        Else
            Session("multistream") = 0
        End If
    End Sub

    Sub CheckMultiShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT  COUNT(SHF_ID) FROM SHIFTS_M WHERE SHF_BSU_ID='" + Session("SBSUID") + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count > 1 Then
            Session("multishift") = 1
        Else
            Session("multishift") = 0
        End If
    End Sub


    Private Function GetAccesstoPHOENIXModule() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim IsAccess As Integer = 0
        Try
            Dim str_query As String = "EXEC GetUsr_Module_Access '" + Session("sUsr_name") + "','" + Session("sModule") + "','" + Session("SBSUID") + "'"

            IsAccess = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            Return IsAccess
        Catch ex As Exception

        End Try
       

    End Function

End Class
