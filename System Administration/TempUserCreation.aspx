﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="TempUserCreation.aspx.vb" Inherits="System_Administration_ProvisionGenericUserFIM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 90%">

        <tr>
            <td align="left" style="height: 19px">
                <table class="BlueTable" style="width: 100%">
                    <tr class="matters">
                        <td style="width: 150px">Business Unit</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:DropDownList ID="ddlbsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrAccountName" class="matters" runat ="server" visible ="false"  >
                        <td style="width: 150px">Account Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAccountName" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>                    
                  
                    <tr class="matters">
                        <td style="width: 150px">First Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFirstName" AutoPostBack ="true" ></asp:TextBox>
                        </td>
                    </tr>
                    
                    <tr class="matters">
                        <td style="width: 150px">Last Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLastName" AutoPostBack ="true"></asp:TextBox>
                        </td>
                    </tr>
                    
                    <tr  class="matters">
                        <td colspan="3">
                            <asp:Label ID="lblErrorMessage" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                        
                    </tr>
                    <tr class="matters">
                        <td align="center" colspan="3">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="100px" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

