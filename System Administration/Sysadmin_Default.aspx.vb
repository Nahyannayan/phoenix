Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.Web.Configuration
Imports System.Web
Imports System.Xml
Imports System
Imports System.Security.Cryptography


Partial Class Sysadmin_Default
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        If Page.IsPostBack = False Then


            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050099") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

            End If
        End If


    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim status As Integer




        Try
            'lblError.Text = "Successfully Updated!!!Please Check."
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = GetOASISConnection()
                transaction = conn.BeginTransaction("SampleTransaction")

                'call the class to insert user password
                status = VerifyandUpdatePassword(Encrypt(txtCurrentPassword.Text), _
                txtUserName.Text, transaction)
                If status = 0 Then
                    Dim vGLGUPDPWD As New com.ChangePWDWebService
                    vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"

                    Dim respon As String = vGLGUPDPWD.ChangePassword(txtUserName.Text, "gems2009", txtCurrentPassword.Text)

                    If respon = "True" Then
                        lblError.Text = respon
                        txtUserName.Text = ""
                        txtCurrentPassword.Text = ""
                    Else
                        lblError.Text = respon
                    End If
                    'Response.Write(respon)
                End If
            End Using



        Catch ex As Exception
            lblError.Text = "Record Could Not Be Updated!!!Please Contact Admin."
        End Try


    End Sub

    Public Shared Function GetOASISConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("GLG_OASISConnectionString").ConnectionString)
        Connection.Open()
        GetOASISConnection = Connection
    End Function

    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, _
        ByVal username As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value
        End Using
    End Function

    Public Function Encrypt(ByVal stringToEncrypt As String, _
      Optional ByVal SEncryptionKey As String = "!#$a54?W") As String


        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes( _
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("~\login.aspx")
    End Sub
End Class
