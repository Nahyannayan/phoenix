<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Sysadmin_ParStudExport.aspx.vb" Inherits="Sysadmin_ParStudExport" Title="::::GEMS OASIS:::: Online Student Administration System::::"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
           
   
         
    </script>

<div class="card mb-3">
          <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>  
            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
          <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="3" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                 <table width="100%">
                     <tr>
                         <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlbsu" runat="server" >
                                </asp:DropDownList>
                          
                                   
                            </td>
                          <td></td>
                          <td></td>
                          
                     </tr>
                     <tr >
                    <td align="left" width="20%" ><span class="field-label"></span></td>
                    <td  align="left" width="30%"><asp:CheckBox ID="check_next_accid" runat="server" 
                    Text="Info of next academic year(New Joiners)" /></td>
                 <td></td>
                        <td></td>
                </tr>
                     <tr>
                         <td align="center"  colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export"
                                ValidationGroup="dayBook" /> <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                                         </td>
                         
                     </tr>
                     </table>
                </div>
              </div>
 </div>
   
</asp:Content>

