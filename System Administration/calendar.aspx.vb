
Partial Class calendar
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hide the title of the calendar control
        'calDatepicker.ShowTitle = False
        'Populate month and year dropdown list boxes which
        'replace the original calendar title
        Dim dtselected As Date
        Try
            dtselected = CDate(Request.QueryString("dt"))
        Catch ex As Exception
            dtselected = Now.Date
        End Try
        If Not Page.IsPostBack Then
            calDatepicker.TodaysDate = dtselected
            Call Populate_MonthList(dtselected)

            Call Populate_YearList(dtselected)
        End If


        Response.Write("<script language='javascript'> function listen_window(){")
        'Response.Write(" alert('uuu');")
        Response.Write("} </script>")
    End Sub

    Protected Sub calDatepicker_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calDatepicker.SelectionChanged

        Dim dates As Date = calDatepicker.SelectedDate
        If dates > Now.Date And Request.QueryString("nofuture") <> "" Then
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("alert('Future Date is not Allowed');")
            Response.Write("} </script>")
        Else
            Dim str As String = dates.ToString("dd/MMM/yyyy")
            h_SelectedDate.Value = str
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & str & "';")
            Response.Write("window.close();")
            Response.Write("} </script>")
        End If



    End Sub

    Sub Set_Calendar(ByVal Sender As Object, ByVal E As EventArgs) Handles drpCalMonth.SelectedIndexChanged

        'Whenever month or year selection changes display the calendar for that month/year        
        calDatepicker.TodaysDate = CDate(drpCalMonth.SelectedItem.Value & " 1, " & drpCalYear.SelectedItem.Value)

    End Sub


    Sub Populate_MonthList(ByVal p_seldt As Date)

        drpCalMonth.Items.Add("January")
        drpCalMonth.Items.Add("February")
        drpCalMonth.Items.Add("March")
        drpCalMonth.Items.Add("April")
        drpCalMonth.Items.Add("May")
        drpCalMonth.Items.Add("June")
        drpCalMonth.Items.Add("July")
        drpCalMonth.Items.Add("August")
        drpCalMonth.Items.Add("September")
        drpCalMonth.Items.Add("October")
        drpCalMonth.Items.Add("November")
        drpCalMonth.Items.Add("December")

        drpCalMonth.Items.FindByValue(MonthName(p_seldt.Month)).Selected = True

    End Sub


    Sub Populate_YearList(ByVal p_seldt As Date)

        'Year list can be extended
        Dim intYear As Integer

        For intYear = DateTime.Now.Year - 20 To DateTime.Now.Year + 20

            drpCalYear.Items.Add(intYear)
        Next

        drpCalYear.Items.FindByValue(p_seldt.Year).Selected = True

    End Sub

    Protected Sub calDatepicker_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles calDatepicker.VisibleMonthChanged
        drpCalMonth.SelectedIndex = -1
        drpCalMonth.Items.FindByValue(MonthName(calDatepicker.VisibleDate.Month)).Selected = True
        drpCalYear.SelectedIndex = -1
        drpCalYear.Items.FindByValue(calDatepicker.VisibleDate.Year).Selected = True
    End Sub
End Class
