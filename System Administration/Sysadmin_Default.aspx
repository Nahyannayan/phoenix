﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Sysadmin_Default.aspx.vb" Inherits="Sysadmin_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<%--	<style type="text/css">
.textbox {
	padding: 1px;
	margin: 8px 2px 2px 0px;
	border: thin groove #FFFFFF;
	font-family: "Trebuchet MS";
	font-size: small;
}
.text {
	font-family: "Trebuchet MS";
	font-size: medium;
	font-weight: 200;
	vertical-align: middle;
	margin-left: 10px;
	margin-bottom: 5px;
	color: #FFFFFF;
}
.error {
	font-family: "Trebuchet MS";
	font-size: medium;
	font-weight: 200;
	vertical-align: middle;
	margin-left: 10px;
	margin-bottom: 5px;
	color: #FFF000;
}
.button {
	font-family: "Trebuchet MS";
	border: thin groove #4694c4;
	background-color: #E2E2E2;
	margin-left: 10px;
	margin-bottom: 10px;
}
.div {
	margin: 5px;
	background-color: #4694c4;
	font-family: Verdana;
	font-size: small;
	text-transform: capitalize;
}
</style>--%>
</head>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<link href="../cssfiles/sb-admin.css" rel="stylesheet" />
<body>




    <form id="form1" runat="server">

        <div class="row mt-5">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card mb-3">

       
          <div class="card-body">
                  <div class="table-responsive m-auto">
                      <table width="100%">
                    <tr valign="bottom">
                        <td align="left" colspan="2" valign="bottom">
                            <asp:Label ID="lblError" runat="server"   EnableViewState="False"></asp:Label></td>
                    </tr>
                </table>
                      <table width="100%">
                          <tr>
                              <td align="left" width="20%"><span class="field-label">GLG User ID</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtUserName" runat="server" MaxLength="100"></asp:TextBox>                                
                              </td>
                                
                                
                          </tr>
                          <tr>
                              <td align="left" width="20%"><span class="field-label">New Password</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtCurrentPassword" runat="server" MaxLength="100"></asp:TextBox>                                
                              </td>
                              
                          </tr>
                          <tr>
                               <td align="center"  colspan="2">
                                   <asp:Button ID="Button1" runat="server" Text="RESET" CssClass="button" />
                                   <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                                   </td>
                               
                          </tr>
                          </table>

                      </div>
</div>

</div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        
    </form>
</body>
</html>
