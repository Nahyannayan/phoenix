﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ProvisionGenericUserFIM.aspx.vb" Inherits="System_Administration_ProvisionGenericUserFIM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 90%">

        <tr>
            <td align="left" style="height: 19px">
                <table class="BlueTable" style="width: 100%">
                    <tr class="matters">
                        <td style="width: 150px">Business Unit</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:DropDownList ID="ddlbsu" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Account Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAccountName"></asp:TextBox>
                        </td>
                    </tr>                    
                    <tr class="matters">
                        <td style="width: 150px">Department</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:DropDownList ID="ddlDepartment" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Designation</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:DropDownList ID="ddlDesignation" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">First Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFirstName"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Middle Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtMiddleName"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Last Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLastName"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Known As</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtKnownAs"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Full Name</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFullName"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Telephone</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtTelephone"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Official Email</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtOfficialEmail"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Gender</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:RadioButtonList ID="rblGender" runat="server">
                                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Date Of Join</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDateOfJoin"></asp:TextBox>
                            <asp:ImageButton ID="imgDOJ" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="calendarDOJ" runat="server" CssClass="MyCalendar"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgDOJ" TargetControlID="txtDateOfJoin">
                                                    </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Personal Email</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtPersonalEmail"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="matters">
                        <td style="width: 150px">Date Of Birth</td>
                        <td style="width: 1px">:</td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDateOfBirth"></asp:TextBox>
                            <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                    <ajaxToolkit:CalendarExtender ID="calendarDOB" runat="server" CssClass="MyCalendar"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgDOB" TargetControlID="txtDateOfBirth">
                                                    </ajaxToolkit:CalendarExtender>

                        </td>
                    </tr>
                    <tr  class="matters">
                        <td colspan="3">
                            <asp:Label ID="lblErrorMessage" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                        
                    </tr>
                    <tr class="matters">
                        <td align="center" colspan="3">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="100px" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

