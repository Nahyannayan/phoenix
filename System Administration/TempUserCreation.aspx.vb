﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class System_Administration_ProvisionGenericUserFIM
    Inherits System.Web.UI.Page
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBsuDropdown(Session("sUsr_name"))
            CheckBsu()


        End If
    End Sub
#End Region

    Private Sub CheckBsu()
        If Not ddlbsu.SelectedItem Is Nothing Then
            If ddlbsu.SelectedItem.Value = "999998" Then
                'txtAccountName.Enabled = True
                txtFirstName.Text = ""
                txtLastName.Text = ""
            Else
                txtAccountName.Text = ""
                'txtAccountName.Enabled = False
                txtFirstName.Text = ""
                txtLastName.Text = ""
            End If
        End If


    End Sub

#Region "Bind BSU Dropdown"
    Private Sub BindBsuDropdown(ByVal UserID As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As New DataSet
        'Dim params(1) As SqlParameter
        'params(0) = New SqlClient.SqlParameter("@USER_ID", UserID)
        Dim sql As String = "select bsu_id,bsu_name from vw_CORP_MENASA_UNITS order by bsu_name"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql)
        ddlbsu.DataSource = ds.Tables(0)
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataBind()
        'ddlbsu.Items.Insert(0, "--")
    End Sub
#End Region



#Region "Clear Fields"
    Private Sub ClearFields()
        txtAccountName.Text = String.Empty
        ddlbsu.SelectedIndex = -1
        txtFirstName.Text = String.Empty
        txtLastName.Text = String.Empty
    End Sub
#End Region

#Region "Function to check if account name already exists"
    Private Function CheckAccountName(AccountName As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim params(1) As SqlParameter
        Dim result As Boolean
        params(0) = New SqlClient.SqlParameter("@ACCOUNT_NAME", AccountName)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FIM.CHECK_ACCOUNT_NAME_EXISTS", params)
        If ds.Tables.Count > 0 Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                result = True
            Else
                result = False
            End If
        Else
            result = False
        End If
        Return result
    End Function
#End Region

#Region "Function to check for mandatory characters in account name"
    Private Function CheckMandatoryCharacters() As Boolean
        Dim strBsuName As String = ddlbsu.SelectedItem.Text
        Dim strBsuCode As String = "_" + strBsuName.Substring(0, 3).ToUpper()
        Dim strAccountName As String
        If txtAccountName.Text.Length < 4 Then
            Return True
            Exit Function
        Else
            strAccountName = txtAccountName.Text.Substring(txtAccountName.Text.Length - 4).ToUpper()
        End If
        If strAccountName = strBsuCode Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Save Button Click"
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)


        Dim tran As SqlTransaction
        Dim CM_ID As String = ViewState("CM_ID")
        Using CONN As SqlConnection = ConnectionManger.GetOASISConnection

            tran = CONN.BeginTransaction("SampleTransaction")

            If ddlbsu.SelectedItem.Value = "999998" Then
                'If txtAccountName.Text = "" Or txtAccountName.Text = String.Empty Then
                '    lblErrorMessage.Text = "Account Name cannot be empty"
                '    Exit Sub
                'ElseIf CheckAccountName(txtAccountName.Text) Then
                '    lblErrorMessage.Text = "Account name exists. Please try another name."
                '    Exit Sub
                'End If
            End If

            If ddlbsu.SelectedValue = "" Or ddlbsu.SelectedValue = String.Empty Then
                lblErrorMessage.Text = "Please select a Business Unit"
            ElseIf txtFirstName.Text = "" Or txtFirstName.Text = String.Empty Then
                lblErrorMessage.Text = "First Name cannot be empty"
            ElseIf txtLastName.Text = "" Or txtLastName.Text = String.Empty Then
                lblErrorMessage.Text = "Last Name cannot be empty"
            Else
                Try

                    Dim str_Conn = ConnectionManger.GetOASISConnectionString
                    Dim param(12) As SqlClient.SqlParameter
                    'param(0) = New SqlClient.SqlParameter("@BSU_ID", "900501")
                    param(0) = New SqlClient.SqlParameter("@BSU_ID", ddlbsu.SelectedValue)
                    param(1) = New SqlClient.SqlParameter("@FIRSTNAME", txtFirstName.Text)
                    param(2) = New SqlClient.SqlParameter("@LASTNAME", txtLastName.Text)
                    param(3) = New SqlClient.SqlParameter("@ACCOUNTNAME", txtAccountName.Text)
                    param(4) = New SqlClient.SqlParameter("@USER", Session("sUsr_name"))
                    param(5) = New SqlClient.SqlParameter("@status", SqlDbType.VarChar, 200)
                    param(5).Direction = ParameterDirection.Output
                    Dim RetVal As Integer
                    RetVal = SqlHelper.ExecuteNonQuery(str_Conn, CommandType.StoredProcedure, "sp_SAVE_GENERIC_USERNAME", param)
                    Dim msg As String = param(5).Value.ToString

                    If RetVal = "2" Then
                        lblErrorMessage.Text = "User Created Sucessfully... User Name: " + msg
                        tran.Commit()
                        'ClearFields()
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable("D050020", txtFirstName.Text + " " + txtLastName.Text, "add", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        btnSave.Enabled = False

                    Else
                        lblErrorMessage.Text = msg
                        tran.Rollback()
                    End If
                Catch ex As Exception

                Finally

                End Try
            End If
        End Using

    End Sub
#End Region
    Protected Sub ddlbsu_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbsu.SelectedIndexChanged
        CheckBsu()
        If btnSave.Enabled = False Then
            btnSave.Enabled = True
        End If
    End Sub
    Protected Sub txtFirstName_TextChanged(sender As Object, e As EventArgs) Handles txtFirstName.TextChanged
        If btnSave.Enabled = False Then
            btnSave.Enabled = True
        End If
    End Sub
    Protected Sub txtLastName_TextChanged(sender As Object, e As EventArgs) Handles txtLastName.TextChanged
        If btnSave.Enabled = False Then
            btnSave.Enabled = True
        End If
    End Sub
End Class
