<%@ Page Language="VB" AutoEventWireup="true" CodeFile="calendar.aspx.vb" Inherits="calendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Date Picker</title>
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
     <base target="_self" />
     
      <script language="javascript" type="text/javascript">
        //not in use
      function validate_add() {     
   
      if (document.getElementById("txtItemname").value=='')
      {
      alert("Kindly enter item Name");
      return false;
      }
      

    
    }  
    function listen_window1()
    {
  alert("uuu");
    }
    
    </script>
</head>
<body onload="listen_window();" scroll="no"  topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
    <form id="form1" runat="server">
<table align="right"   cellspacing="0" cellpadding="0" width="100%" border="0" height="100%" title="Calendar">
        <tr width="100%">
                <td align="center" width="50%">
                    <asp:DropDownList id="drpCalMonth" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="listbox" Width="99%"></asp:DropDownList>
                </td>
                <td align="center" width="50%">
                    <asp:DropDownList id="drpCalYear" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="listbox" Width="99%"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 100%">
                    <asp:Calendar OtherMonthDayStyle-BackColor="White" DayStyle-BackColor="LightYellow" id="calDatepicker" Runat="Server" cssClass="calBody" DayHeaderStyle-BackColor="#eeeeee" Width="100%" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="100%" ShowGridLines="True">
                        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                        <TodayDayStyle BackColor="Red" ForeColor="White" BorderColor="Black" />
                        <SelectorStyle BackColor="#FFCC66" BorderColor="Black" ForeColor="Black" />
                        <OtherMonthDayStyle ForeColor="#CC9966" BackColor="White" />
                        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" BorderColor="Red" />
                        <DayHeaderStyle BackColor="#FFFFC0" Height="1px" Font-Bold="True" BorderColor="Black" />
                        <TitleStyle BackColor="#990000" Font-Bold="True"
                            Font-Size="9pt" ForeColor="#FFFFCC" BorderColor="Black" />
                        <DayStyle BorderColor="Black" BackColor="LightYellow" ForeColor="Black" />
                    </asp:Calendar>
                </td>
            </tr>
            <tr><td colspan="2" style="display:none"><input type="hidden" id="h_SelectedDate" runat="server" /></td></tr>
    </table>   
    </form>
</body>
</html>
