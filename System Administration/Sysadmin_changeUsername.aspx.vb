﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class System_Administration_Sysadmin_changeUsername
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.CacheControl = "no-cache"
        If Page.IsPostBack = False Then
            gvManageUsers.Attributes.Add("bordercolor", "#1b80b6")

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050067") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                bindbsu()
                'gridbind()
            End If
        End If
        set_Menu_Img()
    End Sub
    Private Function querystring() As String
        Dim str_Sql As String = String.Empty
        Dim str_filter_User As String = String.Empty
        Dim str_filter_Ename As String = String.Empty
        Dim str_filter_Role As String = String.Empty
        Dim str_filter_BName As String = String.Empty

        str_Sql = " select * from(SELECT row_number() Over (Order By  EMP_BSU_ID,EMP_ID)SINO,Lower(A.EMP_USR_NAME)usr_name,dbo.ProperCase(Case When Ltrim(Rtrim(dbo.Repl_SpChar(EMP_FNAME)))='' Then NULL Else Ltrim(Rtrim(dbo.Repl_SpChar(EMP_FNAME))) END)+' '+ dbo.ProperCase(Case When Ltrim(Rtrim(dbo.Repl_SpChar(A.EMP_MNAME)))='' Then NULL Else Ltrim(Rtrim(dbo.Repl_SpChar(A.EMP_MNAME))) END)+' '+dbo.ProperCase(Case When Ltrim(Rtrim(dbo.Repl_SpChar(A.EMP_LNAME)))='' Then NULL Else Ltrim(Rtrim(dbo.Repl_SpChar(A.EMP_LNAME))) END)full_name,DES_GLG DESCR,USR_PASSWORD,A.EMP_BSU_ID,businessunit_m.BSU_NAME as BSU_NAME,EMP_ID,EMPNO,CASE ISNULL(businessunit_m.BSU_bEMPUSERNAME,0) WHEN 1 THEN 'INTERNATIONAL'ELSE 'OTHER' END AS BSU_TYPE,'_'+BSU_SHORTNAME AS BSU_SHORTNAME from OASIS..Employee_M A INNER JOIN OASIS..EMPDESIGNATION_M B ON A.EMP_DES_ID=B.DES_ID AND DES_FLAG='SD'INNER JOIN OASIS..USERS_M C ON A.EMP_USR_NAME=C.USR_NAME left JOIN OASIS..Roles_M D ON C.USR_ROL_ID=D.ROL_ID INNER join OASIS..businessunit_m  on A.EMP_BSU_ID=businessunit_m.BSU_ID  AND a.EMP_ECT_ID IN (1,3,4) AND a.EMP_STATUS NOT IN (4,5,6,7,8) AND a.EMP_bACTIVE=1 AND LEN(a.EMP_USR_NAME)<20)a  where  a.EMP_ID<>''"
        Dim ds As New DataSet
        Dim lblID As New Label
        Dim lblName As New Label
        Dim txtSearch As New TextBox
        Dim str_search As String
        Dim str_txtusername As String = String.Empty
        Dim str_txtUserRole As String = String.Empty
        Dim str_txtEmpName As String = String.Empty
        Dim str_txtBusName As String = String.Empty

        If ddlbsu.SelectedValue <> "0" Then
            str_Sql = str_Sql + " AND A.EMP_BSU_ID IN('" & ddlbsu.SelectedValue & "')"
        End If
        If gvManageUsers.Rows.Count > 0 Then
            Dim str_Sid_search() As String

            'str_img = h_selected_menu_1.Value()
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            str_search = str_Sid_search(0)
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
            str_txtusername = txtSearch.Text
            ''code
            If str_search = "LI" Then
                str_filter_User = " AND a.usr_name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "NLI" Then
                str_filter_User = " AND a.usr_name NOT LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "SW" Then
                str_filter_User = " AND a.usr_name LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "NSW" Then
                str_filter_User = " AND a.usr_name NOT LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "EW" Then
                str_filter_User = " AND a.usr_name LIKE '%" & txtSearch.Text & "'"
            ElseIf str_search = "NEW" Then
                str_filter_User = " AND a.usr_name NOT LIKE '%" & txtSearch.Text & "'"
            End If

            ''name
            str_Sid_search = h_Selected_menu_2.Value.Split("__")
            str_search = str_Sid_search(0)

            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
            str_txtUserRole = txtSearch.Text

            If str_search = "LI" Then
                str_filter_Role = " AND a.DESCR LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "NLI" Then
                str_filter_Role = "  AND  NOT a.DESCR LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "SW" Then
                str_filter_Role = " AND a.DESCR  LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "NSW" Then
                str_filter_Role = " AND a.DESCR  NOT LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "EW" Then
                str_filter_Role = " AND a.DESCR LIKE  '%" & txtSearch.Text & "'"
            ElseIf str_search = "NEW" Then
                str_filter_Role = " AND a.DESCR NOT LIKE '%" & txtSearch.Text & "'"
            End If
            str_Sid_search = h_Selected_menu_3.Value.Split("__")
            str_search = str_Sid_search(0)

            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
            str_txtEmpName = txtSearch.Text

            If str_search = "LI" Then
                str_filter_Ename = " AND a.full_name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "NLI" Then
                str_filter_Ename = "  AND  NOT a.full_name LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "SW" Then
                str_filter_Ename = " AND a.full_name  LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "NSW" Then
                str_filter_Ename = " AND a.full_name  NOT LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "EW" Then
                str_filter_Ename = " AND a.full_name LIKE  '%" & txtSearch.Text & "'"
            ElseIf str_search = "NEW" Then
                str_filter_Ename = " AND a.full_name NOT LIKE '%" & txtSearch.Text & "'"
            End If

            str_Sid_search = h_Selected_menu_4.Value.Split("__")
            str_search = str_Sid_search(0)

            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEMPNO")

            str_txtBusName = txtSearch.Text

            If str_search = "LI" Then
                str_filter_BName = " AND a.EMPNO LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "NLI" Then
                str_filter_BName = "  AND  NOT a.EMPNO LIKE '%" & txtSearch.Text & "%'"
            ElseIf str_search = "SW" Then
                str_filter_BName = " AND a.EMPNO  LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "NSW" Then
                str_filter_BName = " AND a.EMPNO  NOT LIKE '" & txtSearch.Text & "%'"
            ElseIf str_search = "EW" Then
                str_filter_BName = " AND a.EMPNO LIKE  '%" & txtSearch.Text & "'"
            ElseIf str_search = "NEW" Then
                str_filter_BName = " AND a.EMPNO NOT LIKE '%" & txtSearch.Text & "'"
            End If
        End If

        str_Sql = str_Sql & str_filter_User & str_filter_Role & str_filter_BName & str_filter_Ename & " order by EMP_BSU_ID,EMP_ID"

        Return str_Sql
    End Function
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringGLG").ToString
            Dim str_Sql As String = String.Empty
            Dim ds As New DataSet
            Dim txtSearch As New TextBox
            Dim str_txtusername As String = String.Empty
            Dim str_txtUserRole As String = String.Empty
            Dim str_txtEmpName As String = String.Empty
            Dim str_txtEMPNO As String = String.Empty
            If gvManageUsers.Rows.Count > 0 Then
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
                str_txtusername = txtSearch.Text
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
                str_txtUserRole = txtSearch.Text
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
                str_txtEmpName = txtSearch.Text
                txtSearch = gvManageUsers.HeaderRow.FindControl("txtEMPNO")
                str_txtEMPNO = txtSearch.Text
            End If


            str_Sql = querystring()

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            gvManageUsers.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvManageUsers.DataBind()
                Dim columnCount As Integer = gvManageUsers.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvManageUsers.Rows(0).Cells.Clear()
                gvManageUsers.Rows(0).Cells.Add(New TableCell)
                gvManageUsers.Rows(0).Cells(0).ColumnSpan = columnCount
                gvManageUsers.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvManageUsers.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvManageUsers.DataBind()
            End If
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtusername")
            txtSearch.Text = str_txtusername
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtUserRole")
            txtSearch.Text = str_txtUserRole
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEmpName")
            txtSearch.Text = str_txtEmpName
            txtSearch = gvManageUsers.HeaderRow.FindControl("txtEMPNO")
            txtSearch.Text = str_txtEMPNO
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvManageUsers.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvManageUsers.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub btnSearchUserName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchUserRole_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchBusUnit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvManageUsers_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvManageUsers.PageIndexChanging
        gvManageUsers.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Sub bindbsu()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT   BSU_ID,BSU_NAME  FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID NOT IN (4,5, 6, 8, 10, 9) ) order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlbsu.DataSource = ds.Tables(0)
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataBind()
        'Dim lst As New ListItem
        'lst.Text = "--------ALL--------"
        'lst.Value = 0
        'ddlbsu.Items.Insert(0, lst)
    End Sub
    Protected Sub ddlbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbsu.SelectedIndexChanged
        gridbind()
    End Sub
    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        gridbind()

    End Sub

    Protected Sub lnkSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

     
            txtnewusername.Text = ""
            txtUsername.Text = ""
            CustomValidator2.ErrorMessage = ""
            hf_username.Value = TryCast(sender.parent.findcontrol("lblUsername"), Label).Text
            HF_EMP_ID.Value = TryCast(sender.parent.findcontrol("HFEMP_ID"), HiddenField).Value
            hf_bsu_type.Value = TryCast(sender.parent.findcontrol("HFBSU_TYPE"), HiddenField).Value
            HF_BSU_SHORTNAME.Value = TryCast(sender.parent.findcontrol("HFBSU_SHORTNAME"), HiddenField).Value
            If hf_bsu_type.Value = "INTERNATIONAL" Then
                CustomValidator2.ErrorMessage = "New Username required '.' in the second place"
            Else
                CustomValidator2.ErrorMessage = "New Username required '.' any where in the username "
            End If
            txtUsername.Text = hf_username.Value

            Panel1_ModalPopupExtender.Show()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If UCase(txtnewusername.Text).ToString.Contains(UCase(HF_BSU_SHORTNAME.Value)) = True Then
                Dim str_Conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGLG").ToString
                Dim param(12) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@EMP_ID", HF_EMP_ID.Value)
                param(1) = New SqlClient.SqlParameter("@BSU_ID", ddlbsu.SelectedValue)
                param(2) = New SqlClient.SqlParameter("@OLD_USERNAME", txtUsername.Text)
                param(3) = New SqlClient.SqlParameter("@NEW_USERNAME", txtnewusername.Text)
                param(4) = New SqlClient.SqlParameter("@ENTEREDBY_USER", Session("sUsr_name"))
                param(5) = New SqlClient.SqlParameter("@RETURN_MSG", SqlDbType.VarChar, 100)
                param(5).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(str_Conn, CommandType.StoredProcedure, "CHANGE_USERNAME", param)
                Dim msg As String = param(5).Value.ToString
                lblerror.Text = msg
                lblErrorMessage.Text = msg
                gridbind()
                Panel1_ModalPopupExtender.Show()
            Else
                lblerror.Text = "User Name should contain " & HF_BSU_SHORTNAME.Value & " in the last "
                lblErrorMessage.Text = "User Name should contain " & HF_BSU_SHORTNAME.Value & " in the last "
                Panel1_ModalPopupExtender.Show()
                Exit Sub
            End If


        Catch ex As Exception

        End Try
    End Sub
End Class
