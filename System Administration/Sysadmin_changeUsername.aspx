﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Sysadmin_changeUsername.aspx.vb" Inherits="System_Administration_Sysadmin_changeUsername" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        function validateOrFields(source, args) {
            var username1 = document.getElementById("<%=hf_username.ClientID %>").value
         var username2 = document.getElementById("<%=txtUsername.ClientID %>").value
         if (username1 == username2) {
             args.IsValid = true;
         }
         else {
             args.IsValid = false;
         }
         return;
     }
     function validateUSERNAME(source, args) {
         var bsu_type = document.getElementById("<%=hf_bsu_type.ClientID %>").value
            var username2 = document.getElementById("<%=txtnewusername.ClientID %>").value

            if (bsu_type == "INTERNATIONAL") {
                if (username2.charAt(1) == ".") {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }

            }
            else {
                if (username2.indexOf('.') > -1) {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }
            }
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            <asp:Label ID="lblHead" runat="server">Change UserName</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4">
                             <asp:Label ID="lblErrorMessage" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlbsu" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnList" runat="server" CssClass="button" Text="List" />
                        </td>

                    </tr>
                    <tr>
                        <td align="center" colspan="4" valign="top">
                            <asp:GridView ID="gvManageUsers" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False"
                                Width="100%" PageSize="20" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="No" Visible="False">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sino" runat="server" Text='<%# Bind("SINO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Staff Number">

                                        <HeaderTemplate>
                                            
                                                        <asp:Label ID="lblbsuNameH" runat="server" Text="Staff Number"
                                                            ></asp:Label><br />
                                                                    <asp:TextBox ID="txtEMPNO" runat="server" Width="80%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchBusUnit" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchBusUnit_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMPNO" runat="server" Text='<%# Bind("EMPNO") %>'></asp:Label>
                                            <asp:HiddenField ID="HFBSU_TYPE" runat="server"
                                                Value='<%# Bind("BSU_TYPE") %>' />
                                            <asp:HiddenField ID="HFEMP_ID" runat="server" Value='<%# Bind("EMP_ID") %>' />
                                            <asp:HiddenField ID="HFBSU_SHORTNAME" runat="server"
                                                Value='<%# Bind("BSU_SHORTNAME") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="USR_NAME">

                                        <HeaderTemplate>
                                           
                                                        <asp:Label ID="lbluserNameH" runat="server"  Text="User Name"></asp:Label>
                                                <br />
                                                                    <asp:TextBox ID="txtUserName" runat="server" Width="80%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchUserName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchUserName_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsername" runat="server" Text='<%# Bind("usr_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Emp Name">

                                        <HeaderTemplate>
                                            
                                                        <asp:Label ID="lblEmpNameH" runat="server" Text="Employee Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtEmpName" runat="server" Width="80%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpName_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("Full_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Role">

                                        <HeaderTemplate>
                                            
                                                        <asp:Label ID="lblRoleH" runat="server" Text="User Role"></asp:Label><br />
                                                                    <asp:TextBox ID="txtUserRole" runat="server" Width="80%"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchUserRole" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchUserRole_Click" />
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserRole" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lnkSelect" runat="server"  OnClick="lnkSelect_Click">Change Username</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
            <td>
                <table align="center" border="0"  width="100%">
                    <tr>
                        <td colspan="4" valign="top" align="center">
                           
                            <ajaxToolkit:ModalPopupExtender ID="Panel1_ModalPopupExtender" runat="server"
                                BackgroundCssClass="modalBackground" CancelControlID="btncancel" PopupControlID="Panel1" TargetControlID="lblpopup">
                            </ajaxToolkit:ModalPopupExtender>
                            <%--style="display: none"--%>
                             
                            <asp:Panel ID="Panel1" runat="server" Width="400px" BackColor="White" CssClass="modalPopup">
                                <div class="card">
                                    <div class="card-header text-left">Update User Name</div>
                                <div class="card-body">
                                <table style="width: 100%">                                     
                                    <tr >
                                        <td align="left" width="40%">Existing User Name</td>
                                      
                                        <td align="left" width="60%">
                                            <asp:TextBox ID="txtUsername" runat="server" ValidationGroup="usrname"></asp:TextBox>
                                            
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="left">New User Name</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtnewusername" runat="server" ValidationGroup="usrname"></asp:TextBox>
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                ControlToValidate="txtnewusername" CssClass="error"
                                                ErrorMessage="New Username Required" ValidationGroup="usrname"></asp:RequiredFieldValidator>
                                            
                                        </td>
                                    </tr>
                                    <tr >
                                        <td align="center" colspan="2">
                                            <asp:Button ID="btnupdate" runat="server" CssClass="button" Text="Update"
                                                ValidationGroup="usrname" />
                                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="txtUsername" CssClass="error"
                                                ErrorMessage="Existing Username Required" ValidationGroup="usrname" Height="0"></asp:RequiredFieldValidator>
                                            <br />
                                            <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                ErrorMessage="Existing User Name is not correct"
                                                ClientValidationFunction="validateOrFields" ControlToValidate="txtUsername"
                                                ValidationGroup="usrname" CssClass="error" Height="0"></asp:CustomValidator>
                                            <asp:HiddenField ID="hf_username" runat="server" />
                                            <asp:HiddenField ID="HF_EMP_ID" runat="server" />
                                            <asp:HiddenField ID="hf_bsu_type" runat="server" />
                                            <asp:HiddenField ID="HF_BSU_SHORTNAME" runat="server" />
                                            <br />
                                            <asp:CustomValidator ID="CustomValidator2" runat="server"
                                                ClientValidationFunction="validateUSERNAME" ControlToValidate="txtnewusername"
                                                CssClass="error" ErrorMessage="*" ValidationGroup="usrname"></asp:CustomValidator>
                                            <br />
                                            <asp:Label ID="lblerror" runat="server" CssClass="error"
                                                EnableViewState="False"></asp:Label>
                                            
                                            <%--<asp:RegularExpressionValidator ID="revPassword" runat="server"
                                                ControlToValidate="txtnewusername" CssClass="error" Display="Dynamic"
                                                ErrorMessage="Your password must contain atleast  6 characters!" ForeColor=""
                                                SetFocusOnError="True" ValidationExpression="[a-zA-Z0-9-.-_]{6,20}"
                                                ValidationGroup="usrname"></asp:RegularExpressionValidator>--%>
                                            
                                            
                                        </td>
                                    </tr>
                                </table>
                                    </div>
                            </div>
                            </asp:Panel>
                                
                            <asp:Label ID="lblpopup" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="0" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
        </tr>
                </table>

            </div>
        </div>






        
    </div>


  
</asp:Content>


