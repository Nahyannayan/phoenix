﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Partial Class System_Administration_ProvisionGenericUserFIM
    Inherits System.Web.UI.Page
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBsuDropdown(Session("sUsr_name"))
            BindDepartmentDropdown()
            BindDesignationDropdown()
            'calendarDOJ.EndDate = DateTime.Now
            calendarDOB.EndDate = DateTime.Now
        End If
    End Sub
#End Region

#Region "Bind BSU Dropdown"
    Private Sub BindBsuDropdown(ByVal UserID As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISFIMConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim params(1) As SqlParameter
        params(0) = New SqlClient.SqlParameter("@USER_ID", UserID)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FIM_GET_USER_BSU", params)
        ddlbsu.DataSource = ds.Tables(0)
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataBind()
        ddlbsu.Items.Insert(0, "--")
    End Sub
#End Region

#Region "Bind Department Dropdown"
    Private Sub BindDepartmentDropdown()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISFIMConnectionString").ConnectionString
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FIM_GET_DEPARTMENTS")
        ddlDepartment.DataSource = ds.Tables(0)
        ddlDepartment.DataValueField = "DPT_ID"
        ddlDepartment.DataTextField = "DPT_DESCR"
        ddlDepartment.DataBind()
    End Sub
#End Region

#Region "Bind Designation Dropdown"
    Private Sub BindDesignationDropdown()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISFIMConnectionString").ConnectionString
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FIM_GET_DESIGNATIONS")
        ddlDesignation.DataSource = ds.Tables(0)
        ddlDesignation.DataValueField = "DES_ID"
        ddlDesignation.DataTextField = "DES_DESCR"
        ddlDesignation.DataBind()
    End Sub
#End Region

#Region "Clear Fields"
    Private Sub ClearFields()
        txtAccountName.Text = String.Empty
        ddlbsu.SelectedIndex = -1
        ddlDepartment.SelectedIndex = -1
        ddlDesignation.SelectedIndex = -1
        txtFirstName.Text = String.Empty
        txtMiddleName.Text = String.Empty
        txtLastName.Text = String.Empty
        txtKnownAs.Text = String.Empty
        txtFullName.Text = String.Empty
        txtTelephone.Text = String.Empty
        txtOfficialEmail.Text = String.Empty
        rblGender.SelectedIndex = -1
        txtDateOfJoin.Text = String.Empty
        txtPersonalEmail.Text = String.Empty
        txtDateOfBirth.Text = String.Empty
    End Sub
#End Region

#Region "Function to check if account name already exists"
    Private Function CheckAccountName(AccountName As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISFIMConnectionString").ConnectionString
        Dim ds As New DataSet
        Dim params(1) As SqlParameter
        Dim result As Boolean
        params(0) = New SqlClient.SqlParameter("@ACCOUNT_NAME", AccountName)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "FIM.CHECK_ACCOUNT_NAME_EXISTS", params)
        If ds.Tables.Count > 0 Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                result = True
            Else
                result = False
            End If
        Else
            result = False
        End If
        Return result
    End Function
#End Region

#Region "Function to check for mandatory characters in account name"
    Private Function CheckMandatoryCharacters() As Boolean
        Dim strBsuName As String = ddlbsu.SelectedItem.Text
        Dim strBsuCode As String = "_" + strBsuName.Substring(0, 3).ToUpper()
        Dim strAccountName As String
        If txtAccountName.Text.Length < 4 Then
            Return True
            Exit Function
        Else
            strAccountName = txtAccountName.Text.Substring(txtAccountName.Text.Length - 4).ToUpper()
        End If
        If strAccountName = strBsuCode Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Save Button Click"
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If txtAccountName.Text = "" Or txtAccountName.Text = String.Empty Then
            lblErrorMessage.Text = "Account Name cannot be empty"
        ElseIf ddlbsu.SelectedValue = "" Or ddlbsu.SelectedValue = String.Empty Or ddlbsu.SelectedIndex = "0" Then
            lblErrorMessage.Text = "Please select a Business Unit"
        ElseIf CheckAccountName(txtAccountName.Text) Then
            lblErrorMessage.Text = "Account name exists. Please try another name."
        ElseIf CheckMandatoryCharacters() Then
            lblErrorMessage.Text = "Account name should end with _ and BSU Code."
        ElseIf ddlDepartment.SelectedValue = "" Or ddlDepartment.SelectedValue = String.Empty Or ddlDepartment.SelectedValue = "1" Then
            lblErrorMessage.Text = "Please select a Department"
        ElseIf ddlDesignation.SelectedValue = "" Or ddlDesignation.SelectedValue = String.Empty Or ddlDesignation.SelectedValue = "295" Then
            lblErrorMessage.Text = "Please select a Designation"
        ElseIf txtFirstName.Text = "" Or txtFirstName.Text = String.Empty Then
            lblErrorMessage.Text = "First Name cannot be empty"
        ElseIf txtKnownAs.Text = "" Or txtKnownAs.Text = String.Empty Then
            lblErrorMessage.Text = "Known As cannot be empty"
        ElseIf txtFullName.Text = "" Or txtFullName.Text = String.Empty Then
            lblErrorMessage.Text = "Full Name cannot be empty"
        ElseIf txtOfficialEmail.Text = "" Or txtOfficialEmail.Text = String.Empty Then
            lblErrorMessage.Text = "Official Email cannot be empty"
        ElseIf rblGender.SelectedValue = "" Or rblGender.SelectedValue = String.Empty Then
            lblErrorMessage.Text = "Please select a Gender"
        ElseIf txtDateOfJoin.Text = "" Or txtDateOfJoin.Text = String.Empty Then
            lblErrorMessage.Text = "Date of Join cannot be empty"
        Else
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISFIMConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim params(15) As SqlParameter
            Try
                params(0) = New SqlClient.SqlParameter("@FPR_ACCOUNT_NAME", txtAccountName.Text)
                params(1) = New SqlClient.SqlParameter("@FPR_BSU_ID", ddlbsu.SelectedValue)
                params(2) = New SqlClient.SqlParameter("@FPR_USR_DEPT", ddlDepartment.SelectedItem.Text)
                params(3) = New SqlClient.SqlParameter("@FPR_USR_GEN_ATTR1", ddlDesignation.SelectedItem.Text)
                params(4) = New SqlClient.SqlParameter("@FPR_F_NAME", txtFirstName.Text)
                params(5) = New SqlClient.SqlParameter("@FPR_M_NAME", txtMiddleName.Text)
                params(6) = New SqlClient.SqlParameter("@FPR_L_NAME", txtLastName.Text)
                params(7) = New SqlClient.SqlParameter("@FPR_KNOWNAS", txtKnownAs.Text)
                params(8) = New SqlClient.SqlParameter("@FPR_FULL_NAME", txtFullName.Text)
                params(9) = New SqlClient.SqlParameter("@FPR_TEL", txtTelephone.Text)
                params(10) = New SqlClient.SqlParameter("@FPR_EMAIL_PRIMARY", txtOfficialEmail.Text)
                params(11) = New SqlClient.SqlParameter("@FPR_GENDER", rblGender.SelectedValue)
                params(12) = New SqlClient.SqlParameter("@FPR_DOJ", txtDateOfJoin.Text)
                params(13) = New SqlClient.SqlParameter("@FPR_EMAIL", txtPersonalEmail.Text)
                params(14) = New SqlClient.SqlParameter("@FPR_DOB", txtDateOfBirth.Text)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "FIM.FIM_GEN_USER_PROVISION", params)
                ClearFields()
                lblErrorMessage.Text = "Data saved successfully. "
            Catch ex As Exception
                lblErrorMessage.Text = "Error occured while saving data. " + ex.Message
            End Try

        End If
    End Sub
#End Region
End Class
