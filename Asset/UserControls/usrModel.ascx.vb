Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
 
Partial Class UserControls_usrBSUnits
    Inherits System.Web.UI.UserControl

    Public Property MenuCode() As String
        Get
            Return h_Modelno.Value
        End Get
        Set(ByVal value As String)
            h_Modelno.Value = value
        End Set
    End Property

    Public Function GetSelectedNode(Optional ByVal seperator As String = "|") As String
        Dim strBSUnits As New StringBuilder
        For Each lst As ListItem In chkb_Model.Items
            If lst.Value <> "-1" Then
                If lst.Selected = True Then
                    strBSUnits.Append(lst.Value)
                    strBSUnits.Append(seperator)
                End If
            End If
        Next
        Return strBSUnits.ToString()
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindFilterControlsMain()
        End If
    End Sub

#Region "Bind Model"
    Public Sub BindFilterControlsMain()
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query = "SELECT DISTINCT MDM_ID,MDM_DESCR FROM MODEL_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkb_Model.DataSource = ds.Tables(0)
        chkb_Model.DataValueField = "MDM_ID"
        chkb_Model.DataTextField = "MDM_DESCR"
        chkb_Model.DataBind()

        For Each clist As ListItem In chkb_Model.Items
            clist.Attributes.Add("onclick", " return ChangeAllCheckBoxModel(this," & ds.Tables(0).Rows.Count & " );")
        Next
        Dim list As New ListItem
        list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStatesModel(this," & ds.Tables(0).Rows.Count & " );")
        chkb_Model.Items.Insert(0, list)
    End Sub

    Public Sub BindFilterControlsMain(ByVal vBSU_IDs As String, ByVal vCAD_IDs As String)
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query = "SELECT DISTINCT MDM_ID,MDM_DESCR FROM MODEL_M INNER JOIN ASSET_M ON " & _
        " AST_MODEL_NO = MDM_DESCR WHERE AST_BSU_ID in (" & Split(vBSU_IDs) & ")" & _
        " AND AST_CAD_ID in (" & Split(vCAD_IDs) & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkb_Model.DataSource = ds.Tables(0)
        chkb_Model.DataValueField = "MDM_ID"
        chkb_Model.DataTextField = "MDM_DESCR"
        chkb_Model.DataBind()

        For Each clist As ListItem In chkb_Model.Items
            clist.Attributes.Add("onclick", " return ChangeAllCheckBoxModel(this," & ds.Tables(0).Rows.Count & " );")
        Next
        Dim list As New ListItem
        list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStatesModel(this," & ds.Tables(0).Rows.Count & " );")
        chkb_Model.Items.Insert(0, list)
    End Sub

    Public Function Split(ByVal vString As String) As String
        Dim vStringOut As String = ""
        Dim vComma As String = ""
        Dim arrList As String()
        arrList = vString.Split("|")
        Dim ienum As IEnumerator
        ienum = arrList.GetEnumerator()
        While ienum.MoveNext
            If ienum.Current.ToString <> "|" AndAlso ienum.Current.ToString <> "" Then
                vStringOut = vStringOut + vComma + "'" + ienum.Current + "'"
                vComma = ", "
            End If
        End While
        If vStringOut = "" Then Return "''"
        Return vStringOut
    End Function

#End Region
End Class

