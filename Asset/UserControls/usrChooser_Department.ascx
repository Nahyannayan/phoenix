﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrChooser_Department.ascx.vb" Inherits="Asset_UserControls_usrChooser_Department" %>
<asp:TextBox ID="txtDescription" runat="server" Width="232px"></asp:TextBox>
<asp:ImageButton ID="imgSelect" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick = "PopupDepartment();"/>
<asp:HiddenField ID="hf_ID" runat="server" />
<asp:HiddenField ID="hf_AutoPostback" runat="server" />
    <script language="javascript" type="text/javascript">
    function PopupDepartment()
    {     
            var sFeatures;
            sFeatures="dialogWidth: 800px; ";
            sFeatures+="dialogHeight: 550px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            result = window.showModalDialog("AssetPopupForm.aspx?ID=DEPT&multiselect=false","", sFeatures)
            //result = window.showModalDialog("../curriculum/clmPopupForm.aspx?ID=CORE_SUBJECT&multiselect=false","", sFeatures)
            if (result=='' || result==undefined)
            {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=hf_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtDescription.ClientID %>').value = NameandCode[1];       
           return false;
        } 
</script>


