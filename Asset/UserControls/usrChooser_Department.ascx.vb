﻿
Partial Class Asset_UserControls_usrChooser_Department
    Inherits System.Web.UI.UserControl

    Dim vID As String
    Dim vText As String

    Public Property SelectedID() As String
        Get
            Return vID
        End Get
        Set(ByVal value As String)
            vID = value
        End Set
    End Property

    Public Property SelectedText() As String
        Get
            Return vText
        End Get
        Set(ByVal value As String)
            vText = value
            txtDescription.Text = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            If hf_AutoPostback.Value = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value Then
                hf_AutoPostback.Value = "1"
            Else
                hf_AutoPostback.Value = "0"
            End If
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDescription.Attributes.Add("ReadOnly", "ReadOnly")
        imgSelect.Attributes.Add("OnClientClick", "SetLocationProperties();")

    End Sub

    Protected Sub imgSelect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSelect.Click
        ViewState("DPT_ID") = hf_ID.Value
    End Sub
End Class
