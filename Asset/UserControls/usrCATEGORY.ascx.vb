Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj

Partial Class UserControls_usrBSUnits
    Inherits System.Web.UI.UserControl
    Public Event CategorySelection_Changed As System.EventHandler

    Public Property MenuCode() As String
        Get
            Return h_Cat.Value
        End Get
        Set(ByVal value As String)
            h_Cat.Value = value
        End Set
    End Property

    Public Function GetSelectedNode(Optional ByVal seperator As String = "|") As String
        Dim strBSUnits As New StringBuilder
        For Each lst As ListItem In chkb_CAT.Items
            If lst.Value <> "-1" Then
                If lst.Selected = True Then
                    strBSUnits.Append(lst.Value)
                    strBSUnits.Append(seperator)
                End If
            End If
        Next
        Return strBSUnits.ToString()
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindFilterControlsMain()
        End If
    End Sub

#Region "Bind CATEGORY"

    Public Sub BindFilterControlsMain()
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query = "SELECT CAD_ID,CAD_DESCR FROM CATEGORY_D"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkb_CAT.DataSource = ds.Tables(0)
        chkb_CAT.DataValueField = "CAD_ID"
        chkb_CAT.DataTextField = "CAD_DESCR"
        chkb_CAT.DataBind()

        For Each clist As ListItem In chkb_CAT.Items
            clist.Attributes.Add("onclick", "   ChangeAllCheckBoxModel(this," & ds.Tables(0).Rows.Count & " );")
        Next
        Dim list As New ListItem
        list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        list.Value = "-1"
        list.Attributes.Add("onclick", "   ChangeAllCheckBoxStatesModel(this," & ds.Tables(0).Rows.Count & " );")
        chkb_CAT.Items.Insert(0, list)
    End Sub

    Public Sub BindFilterControlsMain(ByVal vBSU_ID As String)
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query = "SELECT DISTINCT CAD_DESCR, CAD_ID FROM ASSET_M " & _
        " INNER JOIN CATEGORY_D ON AST_CAD_ID = CAD_ID " & _
        " WHERE AST_BSU_ID in (" & Split(vBSU_ID) & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkb_CAT.DataSource = ds.Tables(0)
        chkb_CAT.DataValueField = "CAD_ID"
        chkb_CAT.DataTextField = "CAD_DESCR"
        chkb_CAT.DataBind()

        For Each clist As ListItem In chkb_CAT.Items
            clist.Attributes.Add("onclick", "   ChangeAllCheckBoxModel(this," & ds.Tables(0).Rows.Count & " );")
        Next
        Dim list As New ListItem
        list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        list.Value = "-1"
        list.Attributes.Add("onclick", "   ChangeAllCheckBoxStatesModel(this," & ds.Tables(0).Rows.Count & " );")
        chkb_CAT.Items.Insert(0, list)
    End Sub

    Public Function Split(ByVal vString As String) As String
        Dim vStringOut As String = ""
        Dim vComma As String = ""
        Dim arrList As String()
        arrList = vString.Split("|")
        Dim ienum As IEnumerator
        ienum = arrList.GetEnumerator()
        While ienum.MoveNext
            If ienum.Current.ToString <> "|" AndAlso ienum.Current.ToString <> "" Then
                vStringOut = vStringOut + vComma + "'" + ienum.Current + "'"
                vComma = ", "
            End If
        End While        

        Return vStringOut
    End Function

#End Region

    Protected Sub chkb_CAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkb_CAT.SelectedIndexChanged
        RaiseEvent CategorySelection_Changed(Me, e)
    End Sub
End Class



