﻿
Partial Class Asset_UserControls_usrChooser_Location
    Inherits System.Web.UI.UserControl

    Dim vID As String
    Dim vText As String

    Public Property SelectedID() As String
        Get
            Return vID
        End Get
        Set(ByVal value As String)
            vID = value
        End Set
    End Property

    Public Property SelectedText() As String
        Get
            Return vText
        End Get
        Set(ByVal value As String)
            vText = value
        End Set
    End Property

    Public Property BSU_ID() As String
        Get
            Return hf_BSU_ID.Value
        End Get
        Set(ByVal value As String)
            hf_BSU_ID.Value = value
        End Set
    End Property

    Public Property DPT_ID() As String
        Get
            Return hf_DPT_ID.Value
        End Get
        Set(ByVal value As String)
            hf_DPT_ID.Value = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            If hf_AutoPostback.Value = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value Then
                hf_AutoPostback.Value = "1"
            Else
                hf_AutoPostback.Value = "0"
            End If
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDescription.Attributes.Add("ReadOnly", "ReadOnly")
    End Sub

End Class
