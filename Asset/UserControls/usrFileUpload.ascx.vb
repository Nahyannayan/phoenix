Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class HelpDesk_UserControls_hdFileUpload
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnadd)
    End Sub

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString

        If FileUploadHd.HasFile Then
            Dim serverpath As String = WebConfigurationManager.AppSettings("HelpdeskAttachments").ToString
            Dim filen As String()
            Dim FileName As String = FileUploadHd.PostedFile.FileName
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)

            Dim extention = GetExtension(FileName)

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FILE_NAME", FileName)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)

            Dim uploadid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_DELETE_FILE_UPLOAD_MASTER", pParms)

            If HiddenUploadid.Value.Trim() <> "" Then
                HiddenUploadid.Value = HiddenUploadid.Value + "," + uploadid
            Else
                HiddenUploadid.Value = uploadid
            End If

            FileUploadHd.SaveAs(serverpath + uploadid + "." + extention)

            bindgrid()
        End If

    End Sub


    Public Sub bindgrid()
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim sql_query = "select * from ASSET_DOCUMENTS where ASD_ID in (" + HiddenUploadid.Value + ")"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        GridUpload.DataSource = ds
        GridUpload.DataBind()

    End Sub
    Private Function GetExtension(ByVal FileName As String) As String

        Dim Extension As String

        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)

        Return Extension


    End Function

    Protected Sub GridUpload_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridUpload.RowCommand

        If e.CommandName = "deleting" Then

            Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@UPLOAD_ID", e.CommandArgument)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "INSERT_DELETE_FILE_UPLOAD_MASTER", pParms)
            bindgrid()
        End If

    End Sub

End Class
