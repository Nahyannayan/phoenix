Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
 
Partial Class UserControls_usrBSUnits
    Inherits System.Web.UI.UserControl
    Public Event BSUSelection_Changed As System.EventHandler

    Public Property MenuCode() As String
        Get
            Return h_MenuCode.Value
        End Get
        Set(ByVal value As String)
            h_MenuCode.Value = value
        End Set
    End Property

    Public Function GetSelectedNode(Optional ByVal seperator As String = "|") As String
        Dim strBSUnits As New StringBuilder
        For Each lst As ListItem In chkb_bsu.Items
            If lst.Value <> "-1" Then
                If lst.Selected = True Then
                    strBSUnits.Append(lst.Value)
                    strBSUnits.Append(seperator)
                End If
            End If
        Next
        Return strBSUnits.ToString()
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ' PopulateTree()
            BindFilterControlsMain()
        End If
    End Sub
#Region "Bind business unit"
    Public Sub BindFilterControlsMain()
        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query = "Select * from STF_REPORT_FILTER_TABLE where STF_FT_SHOW_FLAG='8' ORDER BY STF_ORDER"
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@Usr_name", SqlDbType.NVarChar)
        pParms(0).Value = Session("sUsr_name")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetBsu_WithRights", pParms)
        chkb_bsu.DataSource = ds.Tables(0)
        chkb_bsu.DataValueField = "BSU_ID"
        chkb_bsu.DataTextField = "BSU_NAME"
        chkb_bsu.DataBind()
        'For Each clist As ListItem In chkb_bsu.Items
        '    clist.Attributes.Add("onclick", "  ChangeAllCheckBoxBSU(this," & ds.Tables(0).Rows.Count & " );")
        'Next
        Dim list As New ListItem
        list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        list.Value = "-1"
        list.Attributes.Add("onclick", "  ChangeAllCheckBoxStatesBSU(this," & ds.Tables(0).Rows.Count & " );")
        chkb_bsu.Items.Insert(0, list)
    End Sub
#End Region

    Protected Sub chkb_bsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkb_bsu.SelectedIndexChanged
        If Not chkb_bsu.SelectedItem Is Nothing Then
            If chkb_bsu.SelectedItem.Value = "-1" Then
                For Each clist As ListItem In chkb_bsu.Items
                    clist.Selected = True
                Next
            End If
        End If
        RaiseEvent BSUSelection_Changed(Me, e)
    End Sub
End Class

