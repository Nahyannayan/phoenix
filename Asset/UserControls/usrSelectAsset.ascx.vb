﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Asset_UserControls_usrSelectAsset
    Inherits System.Web.UI.UserControl

    Protected Sub lblAddToList_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ImgAssets_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAssets.Click
        Session("RemovedAssetList") = Nothing
        BindAssets()
    End Sub

    Protected Sub gvSpc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "Remove" Then
            Dim vAST_ID As String = e.CommandArgument
            Dim PAR_ID As String() = vAST_ID.Split("|")
            Dim htList As New Hashtable
            If Session("RemovedAssetList") Is Nothing Then
                Session("RemovedAssetList") = htList
            End If
            htList = Session("RemovedAssetList")
            htList(PAR_ID(1)) = PAR_ID(0)
            Session("RemovedAssetList") = htList
            BindRelatedAsset(PAR_ID(0), sender)
        End If
    End Sub

    Public Function BindAssets() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_Sql As String
        Dim vBSU_ID As String = Session("sBSUID")
        Dim vLOC_ID As String = "" 'hf_LOC_ID.Value
        Dim vINV_NO As String = ""
        Dim vSPL_ID As String = ""
        Dim vPUR_NO As String '= hf_PAR_ID.Value
        Dim vDEL_NO As String '= hf_DEL_ID.Value

        str_Sql = "SELECT AST_ID, AST_INV_NO, AST_DESCR, AST_SL_NO, AST_STATUS FROM ASSET_M "
        Dim str_condition As String

        'If radReceiveToStore.Checked Then
        '    str_condition = " WHERE ISNULL(AST_bAllocated,0) = 1 AND " & _
        '    " ISNULL(AST_bInStore,0) = 0 AND AST_BSU_ID = '" & vBSU_ID & "'"
        'ElseIf radFromStore.Checked Then
        '    str_condition = " WHERE ISNULL(AST_bAllocated,0) = 0 AND " & _
        '    " ISNULL(AST_bInStore,0) = 1 AND AST_BSU_ID = '" & vBSU_ID & "'"
        'End If

        If hfAST_ID.Value <> "" Then
            str_condition += " AND AST_ID in (" & GetstringFromList(hfAST_ID.Value) & ")"
        Else
            gvAssetList.DataSource = Nothing
            gvAssetList.DataBind()
            Exit Function
        End If

        If vINV_NO <> "" Then
            str_condition += " AND AST_INV_NO = '" & vINV_NO & "'"
        End If
        If vSPL_ID <> "" Then
            str_condition += " AND AST_SPL_ID = '" & vSPL_ID & "'"
        End If
        If vPUR_NO <> "" Then
            str_condition += " AND AST_PUR_NO = '" & vPUR_NO & "'"
        End If
        If vDEL_NO <> "" Then
            str_condition += " AND AST_DEL_NO = '" & vDEL_NO & "'"
        End If
        '"' AND AST_LOC_ID = " & vLOC_ID & " AND AST_INV_NO = '" & vINV_NO & "'"

        'If radAllocateToEmployee.Checked Then
        '    str_condition += " AND ISNULL(AST_bResponsibleAsset, 0)= 0 "
        'ElseIf radAllocateToLocation.Checked Then
        '    str_condition += " AND ISNULL(AST_bResponsibleAsset, 0)= 1 "
        'End If

        str_Sql = "SELECT  AST_ID, AST_DESCR, AST_SL_NO, AST_STATUS, MAN_DESCR +' - ' + AST_MODEL_NO AS AST_MODEL_NO " & _
        " FROM ASSET_M INNER JOIN MANUFACTURE_M ON ASSET_M.AST_MAN_ID = MANUFACTURE_M.MAN_ID " & str_condition & _
        " AND ISNULL(AST_DESCR,'') <>'' AND ISNULL(AST_SL_NO,'') <>'' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvAssetList.DataSource = ds
        gvAssetList.DataBind()

    End Function

    Private Function GetstringFromList(ByVal vAssetList As String) As String
        Dim strList As String = String.Empty
        Dim comma As String = String.Empty
        Dim strAssetList As String
        For Each strAssetList In vAssetList.Split("___")
            If strAssetList = "" Then Continue For
            strList += comma + strAssetList
            comma = ","
        Next
        Return strList
    End Function

    Protected Sub gvAssetList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAssetList.RowDataBound
        Dim lblStatus As Label = e.Row.FindControl("lblAST_STATUS")
        If Not lblStatus Is Nothing Then
            Select Case lblStatus.Text
                Case "1"
                    lblStatus.Text = "Good"
                Case "0"
                    lblStatus.Text = "Not Working"
            End Select

        End If
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblAST_ID As New Label

                lblAST_ID = e.Row.FindControl("lblAST_ID")

                Dim gvSpc As New GridView
                gvSpc = e.Row.FindControl("gvSpc")
                If gvSpc Is Nothing Then Exit Sub
                If lblAST_ID.Text <> "" Then
                    BindRelatedAsset(lblAST_ID.Text, gvSpc)
                End If
                If gvSpc.Rows.Count <= 0 Then
                    gvSpc.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub BindRelatedAsset(ByVal PARENT_ID As String, ByVal gvSpc As GridView)
        Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query As String = "SELECT AST_ID, AST_INV_NO, AST_DESCR, AST_SL_NO, AST_SERVICE_TAG,AST_STATUS, " & _
        " MAN_DESCR + ' - ' + AST_MODEL_NO MODEL_DET, CAD_DESCR, " & _
        " CAST(AST_PARENT_ID AS varchar) + '|' + CAST(AST_ID AS varchar) " & _
        " PAR_CHD_AST_ID FROM ASSET_M " & _
        "INNER JOIN MANUFACTURE_M ON ASSET_M.AST_MAN_ID = MANUFACTURE_M.MAN_ID " & _
        " INNER JOIN CATEGORY_D ON ASSET_M.AST_CAD_ID = CATEGORY_D.CAD_ID " & _
        " WHERE  AST_PARENT_ID = " & PARENT_ID

        If Not Session("RemovedAssetList") Is Nothing Then
            Dim htTab As Hashtable = Session("RemovedAssetList")
            str_query += " AND AST_ID not in (" & GetstringFromList(htTab) & ")"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSpc.DataSource = ds
        gvSpc.DataBind()
        If gvSpc.Rows.Count <= 0 Then gvSpc.Visible = False
    End Sub

    Public Function RemoveRelationship(ByVal objConn As SqlConnection, ByVal sqlTran As SqlTransaction) As Integer
        Dim intRetVal As Integer
        Dim htTab As New Hashtable
        If Not Session("RemovedAssetList") Is Nothing Then
            htTab = Session("RemovedAssetList")
        End If
        Dim ienum As IDictionaryEnumerator = htTab.GetEnumerator
        While (ienum.MoveNext())
            Dim SqlCmd As New SqlCommand("RemoveAssetRelationShip", objConn, sqlTran)
            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.Parameters.AddWithValue("@AST_ID", ienum.Key.ToString)
            SqlCmd.Parameters.AddWithValue("@AST_BSU_ID", Session("sBSUID"))
            SqlCmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
            SqlCmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
            SqlCmd.ExecuteNonQuery()
            intRetVal = CInt(SqlCmd.Parameters("@ReturnValue").Value)
            If intRetVal <> 0 Then
                Return intRetVal
            End If
        End While
        Return intRetVal
    End Function

    Public Function GetGridAssetList() As Hashtable
        Dim ht As New Hashtable
        Dim astMovements As New AssetMovements
        Dim lblTemp As Label
        Dim txtTemp As TextBox
        Dim ddlTemp As DropDownList
        Dim vAST_ID As String = String.Empty
        Dim gvRelatedAsset As GridView
        For Each gvRow As GridViewRow In gvAssetList.Rows
            astMovements = New AssetMovements
            lblTemp = gvRow.FindControl("lblAST_ID")
            If Not lblTemp Is Nothing Then
                astMovements.AST_ID = lblTemp.Text
                vAST_ID = lblTemp.Text
            End If
            txtTemp = gvRow.FindControl("txtRemarks")
            If Not txtTemp Is Nothing Then
                astMovements.AST_REMARKS = txtTemp.Text
            End If
            ddlTemp = gvRow.FindControl("ddlStatus")
            If Not ddlTemp Is Nothing Then
                astMovements.AST_STATUS = ddlTemp.SelectedValue
            End If

            If vAST_ID <> "" Then
                ht(vAST_ID) = astMovements
            End If

            '****** Finds the related asset for movement
            gvRelatedAsset = gvRow.FindControl("gvSpc")
            If Not gvRelatedAsset Is Nothing Then
                For Each gvSubRow As GridViewRow In gvRelatedAsset.Rows
                    astMovements = New AssetMovements
                    lblTemp = gvSubRow.FindControl("lblSubAST_ID")
                    If Not lblTemp Is Nothing Then
                        astMovements.AST_ID = lblTemp.Text
                        vAST_ID = lblTemp.Text
                    End If
                    astMovements.AST_REMARKS = ""
                    ddlTemp = gvSubRow.FindControl("ddlSubStatus")
                    If Not ddlTemp Is Nothing Then
                        astMovements.AST_STATUS = ddlTemp.SelectedValue
                    End If
                    If vAST_ID <> "" Then
                        ht(vAST_ID) = astMovements
                    End If
                Next
            End If
        Next
        Return ht
    End Function

    Private Function GetstringFromList(ByVal vAssetList As Hashtable) As String
        Dim strList As String = String.Empty
        Dim comma As String = String.Empty
        Dim ienum As IDictionaryEnumerator = vAssetList.GetEnumerator
        While (ienum.MoveNext())
            strList += comma + ienum.Key
            comma = ","
        End While
        Return strList
    End Function

End Class
