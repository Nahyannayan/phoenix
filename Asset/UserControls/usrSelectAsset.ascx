﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrSelectAsset.ascx.vb" Inherits="Asset_UserControls_usrSelectAsset" %>
    <script language="javascript" type="text/javascript">
       function PopupAsset()
        {     
            var sFeatures;
            sFeatures="dialogWidth: 800px; ";
            sFeatures+="dialogHeight: 550px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var vBSU_ID = document.getElementById('<%=hfBSU_ID.ClientID %>').value;
            var vTRN_TYPE = document.getElementById('<%=hfTRN_TYPE.ClientID %>').value;
            result = window.showModalDialog("PopupFormGetAsset.aspx?ID=ASSET&BSU_ID=" + vBSU_ID + "&TRN_TYPE=" + vTRN_TYPE + "&multiselect=true","", sFeatures)
            if (result=='' || result==undefined)
            {
                document.getElementById('<%=hfAST_ID.ClientID %>').value = '';
                return false;
            }
            document.getElementById('<%=hfAST_ID.ClientID %>').value = result;
            
           return false;
        } 
    </script>        
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width ="100%" >
      <tr >
        <td align="left" class="matters" >
               <asp:Label id="lblAssetIssue2" Text = "Select items You want to Allocate : " runat="server" /> 
                <asp:TextBox ID="txtAssets" runat="server"  Width="350px"></asp:TextBox>
                <asp:ImageButton ID="ImgAssets" runat="server" 
                    ImageUrl="~/Images/forum_search.gif" onclientclick="PopupAsset();" />
        </td>          
      </tr>
      <tr>
         <td align="left" class="matters" >
            <asp:GridView ID="gvAssetList" runat="server" AutoGenerateColumns="False"
                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center"
                PageSize="25" SkinID="GridViewView" RowStyle-VerticalAlign="Top"> 
                <RowStyle VerticalAlign="Top"></RowStyle>

                <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                <Columns>
                    <asp:TemplateField HeaderText="Model"><HeaderTemplate>
                                                        <table style="width: 100%; height: 100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" style="WIDTH: 100px">
                                                                        Model Details</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    
                    </HeaderTemplate>
                    <ItemTemplate>
                                                        <asp:Label ID="Label9" runat="server" Text='<%# Bind("AST_MODEL_NO") %>'></asp:Label>
                                                    
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Student#"><HeaderTemplate>
                    <TABLE style="WIDTH: 100%; HEIGHT: 100%"><TBODY><TR><TD style="WIDTH: 100px" align=center>
                        Serial No</TD></TR></TBODY></TABLE>
                    </HeaderTemplate>
                    <ItemTemplate>
                    <asp:Label id="Label10" runat="server" Text='<%# Bind("AST_SL_NO") %>'></asp:Label> 
                    </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name"><HeaderTemplate>
                                                        <table style="width: 100%; height: 100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" style="WIDTH: 100px">
                                                                        Asset Tag</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    
                    </HeaderTemplate>
                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("AST_DESCR") %>'></asp:Label>
                                                    
                    </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount"><HeaderTemplate>
                                                        <table style="width: 100%; height: 100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" style="WIDTH: 100px">
                                                                        Status</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    
                    </HeaderTemplate>
                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlStatus" runat="server">
                                                            <asp:ListItem Value="1">Good</asp:ListItem>
                                                            <asp:ListItem Value="0">Not Working</asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRemarks" runat="server" SkinID="MultiText_Large" 
                                TextMode="MultiLine" Width="100%" Visible ="false"></asp:TextBox>
                        </ItemTemplate>
                        <HeaderTemplate>
                                                        <table style="width: 100%; height: 100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" style="WIDTH: 100px">
                                                                        Remarks</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    
                    </HeaderTemplate>

                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add To List" Visible="False"><ItemTemplate>
                    <asp:LinkButton id="lblAddToList"  runat="server" Text="Add" OnClick="lblAddToList_Click"></asp:LinkButton>  
                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AST_ID" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblAST_ID" runat="server" Text='<%# Bind("AST_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField >
                      <ItemTemplate>
                        </td></tr>
                         <tr>
                        <td colspan="100%" align="right">
                        <div id="div<%# Eval("AST_ID") %>"  >
                       <asp:GridView ID="gvSpc" runat="server" Width="98%"
                        AutoGenerateColumns="false" EmptyDataText="No Related items for this asset." 
                        EmptyDataRowStyle-HorizontalAlign="Center" OnRowCommand ="gvSpc_RowCommand" >
                         <Columns>
                           <asp:BoundField DataField="CAD_DESCR" HeaderText="Category" HtmlEncode="False"  >
                            <ItemStyle HorizontalAlign="center" />
                           </asp:BoundField>
                           <asp:BoundField DataField="MODEL_DET" HeaderText="Model No." HtmlEncode="False"  >
                            <ItemStyle HorizontalAlign="center" />
                           </asp:BoundField>
                           <asp:TemplateField HeaderText="AST_ID" Visible="false">
                                <ItemTemplate>
                                   <asp:Label id="lblSubAST_ID" runat="server" Text='<%# Bind("AST_ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="SERIAL CODE" Visible="true">
                                <ItemTemplate>
                                   <asp:Label id="lblSubAST_SL_NO" runat="server" Text='<%# Bind("AST_SL_NO") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="ASSET TAG" Visible="true">
                                <ItemTemplate>
                                  <asp:Label id="lblsubAST_DESCR" runat="server" Text='<%# Bind("AST_DESCR") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STATUS">
                               <ItemTemplate>
                                    <asp:DropDownList ID="ddlSubStatus" runat="server">
                                        <asp:ListItem Value="1">Good</asp:ListItem>
                                        <asp:ListItem Value="0">Not Working</asp:ListItem>
                                    </asp:DropDownList>
                               </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>             
                            <asp:TemplateField HeaderText="Remove">
                               <ItemTemplate>
                                   <asp:LinkButton ID="DeleteBtn" runat="server"  OnClientClick ="return confirm_delete();"
                                    CommandArgument='<%# Eval("PAR_CHD_AST_ID") %>' CommandName="Remove">Remove</asp:LinkButton>
                               </ItemTemplate>
                            </asp:TemplateField>             
                            </Columns>
                          <RowStyle CssClass="griditem" Height="20px" Font-Names="Verdana"  /> 
                           <HeaderStyle CssClass="gridheader_new" Height="20px" /> 
                       </asp:GridView>
                      </div>
                      </td>
                       </tr>
                      </ItemTemplate>
                    </asp:TemplateField>                            
                    </Columns>
            </asp:GridView> 
         </td>
      </tr>
 </table>
                <asp:HiddenField ID="hfBSU_ID" runat="server" />
                <asp:HiddenField ID="hfAST_ID" runat="server" />
                <asp:HiddenField ID="hfTRN_TYPE" runat="server" />
                