﻿
Partial Class Asset_UserControls_DatePicker
    Inherits System.Web.UI.UserControl
    Dim vID As String
    Dim vText As String
    Public Event Date_Changed As System.EventHandler

    'Public Property SelectedText() As String
    '    Get
    '        Return vText
    '    End Get
    '    Set(ByVal value As String)
    '        vText = value
    '    End Set
    'End Property

    Public Function SelectedText() As String
        Dim dt As String = txtDT.Text
        Return dt
    End Function
    Public Sub ClearSelection()
        txtDT.Text = ""
    End Sub

    Public Property SetDate() As String
        Get
            Return txtDT.Text
        End Get
        Set(ByVal value As String)
            txtDT.Text = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        '    txtDT.Text = Format(Date.Now, OASISConstants.DateFormat)
        'End If
    End Sub

    Protected Sub txtDT_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDT.TextChanged
        RaiseEvent Date_Changed(Me, e)
    End Sub
End Class
