﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrChooser_Location.ascx.vb" Inherits="Asset_UserControls_usrChooser_Location" %>
<asp:TextBox ID="txtDescription" runat="server" Width="232px"></asp:TextBox>
<asp:ImageButton ID="imgSelect" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick = "PopupLocation();"/>
<asp:HiddenField ID="hf_ID" runat="server" />
<asp:HiddenField ID="hf_AutoPostback" runat="server" />
<asp:HiddenField ID="hf_BSU_ID" runat="server" />
<asp:HiddenField ID="hf_DPT_ID" runat="server" />
    <script language="javascript" type="text/javascript">
    function PopupLocation()
    {     
            var sFeatures;
            sFeatures="dialogWidth: 800px; ";
            sFeatures+="dialogHeight: 550px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var vBSU_ID = document.getElementById('<%=hf_BSU_ID.ClientID %>').value;
            var vDPT_ID = document.getElementById('<%=hf_DPT_ID.ClientID %>').value;
            
            result = window.showModalDialog("AssetPopupForm.aspx?ID=LOCATION&BSU_ID=" + vBSU_ID + "&DPT_ID=" + vDPT_ID + "&multiselect=false","", sFeatures)
            //result = window.showModalDialog("../curriculum/clmPopupForm.aspx?ID=CORE_SUBJECT&multiselect=false","", sFeatures)
            if (result=='' || result==undefined)
            {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=hf_ID.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtDescription.ClientID %>').value = NameandCode[1];       
           return false;
        } 
</script>


