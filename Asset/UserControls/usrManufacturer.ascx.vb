Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports UtilityObj
 
Partial Class UserControls_usrBSUnits
    Inherits System.Web.UI.UserControl

    Public Property MenuCode() As String
        Get
            Return h_MenuCode.Value
        End Get
        Set(ByVal value As String)
            h_MenuCode.Value = value
        End Set
    End Property

    Public Property Enable() As Boolean
        Get
            Return ViewState("bManEnable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bManEnable") = value
        End Set
    End Property

    Public Function GetSelectedNode(Optional ByVal seperator As String = "|") As String
        Dim strBSUnits As New StringBuilder
        For Each lst As ListItem In chkb_manuf.Items
            If lst.Value <> "-1" Then
                If lst.Selected = True Then
                    strBSUnits.Append(lst.Value)
                    strBSUnits.Append(seperator)
                End If
            End If
        Next
        Return strBSUnits.ToString()
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindFilterControlsMain()
            If Not ViewState("bManEnable") Is Nothing Then
                If ViewState("bManEnable") = False Then
                    For Each col As ListItem In chkb_manuf.Items
                        col.Selected = True
                    Next
                    chkb_manuf.Enabled = False
                    Return
                End If
            End If
        End If
    End Sub


#Region "Bind business unit"
    Public Sub BindFilterControlsMain()

        Dim str_conn = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query = "SELECT MAN_ID,MAN_DESCR FROM MANUFACTURE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkb_manuf.DataSource = ds.Tables(0)
        chkb_manuf.DataValueField = "MAN_ID"
        chkb_manuf.DataTextField = "MAN_DESCR"
        chkb_manuf.DataBind()

        For Each clist As ListItem In chkb_manuf.Items
            clist.Attributes.Add("onclick", " return ChangeAllCheckBoxmanuf(this," & ds.Tables(0).Rows.Count & " );")
        Next
        Dim list As New ListItem
        list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
        list.Value = "-1"
        list.Attributes.Add("onclick", " return ChangeAllCheckBoxStatesmanuf(this," & ds.Tables(0).Rows.Count & " );")
        chkb_manuf.Items.Insert(0, list)
    End Sub
#End Region
End Class

