﻿
Partial Class Asset_UserControls_UsrAssetAllocation
    Inherits System.Web.UI.UserControl

    Dim vAST_BSU_ID As String
    Dim vAST_LOC_ID As Integer

    Public Property BSU_ID() As String
        Get
            Return vAST_BSU_ID
        End Get
        Set(ByVal value As String)
            vAST_BSU_ID = value
        End Set
    End Property

    Public Property LOC_ID() As Integer
        Get
            Return vAST_LOC_ID
        End Get
        Set(ByVal value As Integer)
            vAST_LOC_ID = value
        End Set
    End Property

End Class
