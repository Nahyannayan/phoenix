﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="usrDatePicker.ascx.vb" Inherits="Asset_UserControls_DatePicker" %>
                            <asp:TextBox ID="txtDT" runat="server" Width="110px" 
                                meta:resourcekey="txtRegDTResource1"></asp:TextBox>
                            <asp:ImageButton ID="imgDT" runat="server" ImageUrl="~/Images/calendar.gif" 
                                meta:resourcekey="imgRegDTResource1" />
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" TargetControlID="txtDT">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
        Format="dd/MMM/yyyy" PopupButtonID="imgDT" TargetControlID="txtDT">
    </ajaxToolkit:CalendarExtender>