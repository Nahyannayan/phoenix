﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UsrAssetAllocationBulk.ascx.vb" Inherits="Asset_UserControls_UsrAssetAllocation" %>
<%@ Register src="usrChooser_Department.ascx" tagname="usrChooser_Department" tagprefix="uc2" %>
<style type="text/css">


.gridheader_new {   border-style: none;
        border-color: inherit;
        border-width: 0;
        font-family: Verdana, Arial, Helvetica, sans-serif; 
				background-image:url('../../Images/GRIDHEAD.gif') ;
				    background-repeat: repeat-x;    font-size: 11px;
				    font-weight:bold;    color:#1b80b6;
				}

.griditem	
{	
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size: 9px; color: #1b80b6 ; font-weight:bold;
	background-color:#ffffff; 
	text-align:left; border:1; 
	border-color:#1b80b6;
}
.griditem_alternative 
{
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size: 9px; color: #1b80b6 ; font-weight:bold;
	background-color:#CEE3FF; 
	text-align:left; border:1; 
	border-color:#1b80b6;
}
</style>
    <script language="javascript" type="text/javascript">
    function PopupInvoiceNo()
    {     
            var sFeatures;
            sFeatures="dialogWidth: 800px; ";
            sFeatures+="dialogHeight: 550px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var vBSU_ID = document.getElementById('<%=hf_BSU_ID.ClientID %>').value;
            var vDPT_ID = document.getElementById('<%=hf_DPT_ID.ClientID %>').value;
            var vLOC_ID = document.getElementById('<%=hf_LOC_ID.ClientID %>').value;
            
            result = window.showModalDialog("AssetPopupForm.aspx?ID=INV_NO&BSU_ID=" + vBSU_ID + "&DPT_ID=" + vDPT_ID + "&LOC_ID=" + vLOC_ID + "&multiselect=false","", sFeatures)
            //result = window.showModalDialog("../curriculum/clmPopupForm.aspx?ID=CORE_SUBJECT&multiselect=false","", sFeatures)
            if (result=='' || result==undefined)
            {
                return false;
            }
            NameandCode = result.split('___');
            document.getElementById('<%=txtAssetNo.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtAssetNo.ClientID %>').value = NameandCode[1];       
           return false;
        } 
</script>
 
<table border="1" class="BlueTableView" bordercolor="#1b80b6" cellpadding="5" 
    cellspacing="0" >
    <tr>
     <td align="left" class="matters" >
            Asset Invoice No</td>
     <td class="matters" >:</td>
     <td align="left" class="matters" >
         <asp:TextBox ID="txtAssetNo" runat="server" ReadOnly="True" Width="321px"></asp:TextBox>
<asp:ImageButton ID="imgSelect" runat="server" ImageUrl="~/Images/forum_search.gif" 
             OnClientClick = "PopupInvoiceNo();"/>
            </td>          
    </tr>
    <tr>
     <td align="left" class="matters" colspan="3" >
      <asp:GridView ID="gvDTL" runat="server" AutoGenerateColumns="False"
       EmptyDataText="No Transaction details added yet." 
       BorderColor="#1B80B6" Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" 
       >
      <EmptyDataRowStyle CssClass="gridheader" Wrap="True" />
      <Columns>
        <asp:TemplateField HeaderText="Id">
        <ItemStyle Width="1%"></ItemStyle>
        <ItemTemplate>
        <asp:Label id="lblId0" runat="server" Text='<%# Bind("AST_ID") %>'></asp:Label> 
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="False" HeaderText="Unique Code">
            <HeaderTemplate>
                Unique Code
            </HeaderTemplate>
            <ItemTemplate>
                <asp:TextBox ID="TextBox2" runat="server" Width="182px" 
                    Text='<%# Bind("AST_SL_NO") %>'></asp:TextBox>
            </ItemTemplate>

        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Asset Name">
            <HeaderTemplate>
                Asset Name
            </HeaderTemplate>
            <ItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server" Width="184px" 
                    Text='<%# Bind("AST_DESCR") %>'></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Employee">
            <HeaderTemplate>
                Employee
            </HeaderTemplate>
            <ItemTemplate>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Delete" Visible="False">
            <ItemTemplate>
                <asp:LinkButton ID="DeleteBtn" runat="server" 
                    CommandArgument='<%# Eval("id") %>' CommandName="Delete"> Delete</asp:LinkButton>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
        </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="griditem" Height="23px" />
           <SelectedRowStyle BackColor="Aqua" />
                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
           <AlternatingRowStyle CssClass="griditem_alternative"  />
    </asp:GridView>
     </td>
    </tr>
 </table>
    <asp:HiddenField ID="hf_BSU_ID" runat="server" />
    <asp:HiddenField ID="hf_DPT_ID" runat="server" />
    <asp:HiddenField ID="hf_LOC_ID" runat="server" />

