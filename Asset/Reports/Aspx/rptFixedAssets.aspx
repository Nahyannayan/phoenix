<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"  CodeFile="rptFixedAssets.aspx.vb" Inherits="Asset_Reports_Aspx_rptFixedAssets" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  <script language="javascript" type="text/javascript">

      function ToggleSearchCategory() {
          if (document.getElementById('<%=hfCategory.ClientID %>').value == 'display') {
              document.getElementById('<%=trCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfCategory.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = '';
              document.getElementById('<%=hfCategory.ClientID %>').value = 'display';
          }
          return false;
      }
      function ToggleSearchSubCategory() {
          if (document.getElementById('<%=hfSubCategory.ClientID %>').value == 'display') {
              document.getElementById('<%=trSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfSubCategory.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=hfSubCategory.ClientID %>').value = 'display';
          }
          return false;
      }
      function ToggleSearchMake() {
          if (document.getElementById('<%=hfMake.ClientID %>').value == 'display') {
              document.getElementById('<%=trMake.ClientID %>').style.display = '';
              document.getElementById('<%=trSelMake.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusMake.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusMake.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfMake.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trMake.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelMake.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusMake.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusMake.ClientID %>').style.display = '';
              document.getElementById('<%=hfMake.ClientID %>').value = 'display';
          }
          return false;
      }
      function ToggleSearchModel() {
          if (document.getElementById('<%=hfModel.ClientID %>').value == 'display') {
              document.getElementById('<%=trModel.ClientID %>').style.display = '';
              document.getElementById('<%=trSelModel.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusModel.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusModel.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfModel.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trModel.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelModel.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusModel.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusModel.ClientID %>').style.display = '';
              document.getElementById('<%=hfModel.ClientID %>').value = 'display';
          }
          return false;
      }

      function ToggleSearchLocation() {
          if (document.getElementById('<%=hfLocation.ClientID %>').value == 'display') {
              document.getElementById('<%=trLocation.ClientID %>').style.display = '';
              document.getElementById('<%=trSelLocation.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusLocation.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusLocation.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfLocation.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trLocation.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelLocation.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusLocation.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusLocation.ClientID %>').style.display = '';
              document.getElementById('<%=hfLocation.ClientID %>').value = 'display';
          }
          return false;
      }

      function ToggleSearchEmployee() {
          if (document.getElementById('<%=hfEmployee.ClientID %>').value == 'display') {
              document.getElementById('<%=trEmployee.ClientID %>').style.display = '';
              document.getElementById('<%=trSelEmployee.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusEmployee.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusEmployee.ClientID %>').style.display = 'none';
              document.getElementById('<%=hfEmployee.ClientID %>').value = 'none';
          }
          else {
              document.getElementById('<%=trEmployee.ClientID %>').style.display = 'none';
              document.getElementById('<%=trSelEmployee.ClientID %>').style.display = '';
              document.getElementById('<%=imgMinusEmployee.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgPlusEmployee.ClientID %>').style.display = '';
              document.getElementById('<%=hfEmployee.ClientID %>').value = 'display';
          }
          return false;
      }
      function GetCATName() {
          var sFeatures;
          sFeatures = "dialogWidth: 729px; ";
          sFeatures += "dialogHeight: 445px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: yes; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ASSETCATEGORY"
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1";
          result = window.showModalDialog(url, "", sFeatures);
          if (result == '' || result == undefined) {


              return false;
          }
          document.getElementById("<%=h_CATID.ClientID %>").value = result;
          document.forms[0].submit();

      }
      function GetSubCATName() {
          var sFeatures;
          sFeatures = "dialogWidth: 729px; ";
          sFeatures += "dialogHeight: 445px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: yes; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ASSETSUBCATEGORY"
          var cat = document.getElementById('<%= h_CATID.ClientID %>').value;
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1&cat=" + cat; 
          result = window.showModalDialog(url, "", sFeatures);
          if (result == '' || result == undefined) {


              return false;
          }
          document.getElementById("<%=h_SubCATID.ClientID %>").value = result;
          document.forms[0].submit();
      }

      function GetMake() {
          var sFeatures;
          sFeatures = "dialogWidth: 729px; ";
          sFeatures += "dialogHeight: 445px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: yes; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ASSETMAKE"
          var subcat = document.getElementById('<%= h_SubCATID.ClientID %>').value;
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1&subcat=" + subcat; 
          result = window.showModalDialog(url, "", sFeatures);
          if (result == '' || result == undefined) {


              return false;
          }
          document.getElementById("<%=h_Make.ClientID%>").value = result;
          document.forms[0].submit();
      }

      function GetModel() {
          var sFeatures;
          sFeatures = "dialogWidth: 729px; ";
          sFeatures += "dialogHeight: 445px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: yes; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ASSETMODEL"
          var subcat = document.getElementById('<%= h_SubCATID.ClientID %>').value;
          var make = document.getElementById('<%= h_Make.ClientID %>').value;
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1&subcat=" + subcat + "&make=" + make; 
          result = window.showModalDialog(url, "", sFeatures);
          if (result == '' || result == undefined) {


              return false;
          }
          document.getElementById("<%=h_Model.ClientID %>").value = result;
          document.forms[0].submit();
      }
      function GetLocation() {
          var sFeatures;
          sFeatures = "dialogWidth: 729px; ";
          sFeatures += "dialogHeight: 445px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: yes; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          pMode = "ASSETLOCATION"
          url = "../../../common/PopupSelect.aspx?id=" + pMode + "&MultiSelect=1";
          result = window.showModalDialog(url, "", sFeatures);
          if (result == '' || result == undefined) {


              return false;
          }
          document.getElementById("<%=h_LocationID.ClientID %>").value = result;
          document.forms[0].submit();
      }

      function GetEmployees() {
          var sFeatures;
          sFeatures = "dialogWidth: 729px; ";
          sFeatures += "dialogHeight: 445px; ";
          sFeatures += "help: no; ";
          sFeatures += "resizable: no; ";
          sFeatures += "scroll: yes; ";
          sFeatures += "status: no; ";
          sFeatures += "unadorned: no; ";
          var NameandCode;
          var result;
          result = window.showModalDialog("../../../Payroll/Reports/Aspx/SelIDDESC.aspx?ID=EMP", "", sFeatures)
          if (result != '' && result != undefined) {
              document.getElementById('<%=h_EmployeeID.ClientID %>').value = result; //NameandCode[0];
              document.forms[0].submit();
          }
          else {
              return false;
          }
      }

      function CheckOnPostback() {
          //          
          if (document.getElementById('<%=hfCategory.ClientID %>').value == 'none') {
              document.getElementById('<%=trCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusCategory.ClientID %>').style.display = 'none';
          }
          if (document.getElementById('<%=hfSubCategory.ClientID %>').value == 'none') {
              document.getElementById('<%=trSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=trSelSubCategory.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusSubCategory.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusSubCategory.ClientID %>').style.display = 'none';
          }
          if (document.getElementById('<%=hfMake.ClientID %>').value == 'none') {
              document.getElementById('<%=trMake.ClientID %>').style.display = '';
              document.getElementById('<%=trSelMake.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusMake.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusMake.ClientID %>').style.display = 'none';
          }
          if (document.getElementById('<%=hfModel.ClientID %>').value == 'none') {
              document.getElementById('<%=trModel.ClientID %>').style.display = '';
              document.getElementById('<%=trSelModel.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusModel.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusModel.ClientID %>').style.display = 'none';
          }
          if (document.getElementById('<%=hfLocation.ClientID %>').value == 'none') {
              document.getElementById('<%=trLocation.ClientID %>').style.display = '';
              document.getElementById('<%=trSelLocation.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusLocation.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusLocation.ClientID %>').style.display = 'none';
          }
          if (document.getElementById('<%=hfEmployee.ClientID %>').value == 'none') {
              document.getElementById('<%=trEmployee.ClientID %>').style.display = '';
              document.getElementById('<%=trSelEmployee.ClientID %>').style.display = 'none';
              document.getElementById('<%=imgMinusEmployee.ClientID %>').style.display = '';
              document.getElementById('<%=imgPlusEmployee.ClientID %>').style.display = 'none';
          }
          //         
          //         
          return false;
      }
         </script>
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 640px">








        <tr>
         <td align="left"  valign="bottom" style="height: 20px">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" style="text-align: center" Height="16px"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="height: 93px; font-weight: bold;" valign="top">
                <table align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" 
                    cellpadding="5" cellspacing="0"
                     id="TB1" runat="server" width="100%">
                    
                                       
                 
            
            
                    <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle">
                            Assets in Store</td>
                        
                        </tr>   
                       <tr id ="trSelcategory">
            <td align="left" class="matters" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusCategory" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />
                Select Category Filter</td>
        </tr>
        <tr id ="trCategory"  style="display:none;">
            <td align="left" valign ="top" class="matters">
                <asp:ImageButton ID="imgMinusCategory" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchCategory();return false;" CausesValidation="False" />Category</td>
            <td align="left" class="matters" colspan="3" style="text-align: left" 
                nowrap="nowrap">
                (Enter the Category ID you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtCatName" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetCATName()"  /><br />
                <asp:GridView ID="gvCat" runat="server" AutoGenerateColumns="False" Width="498px" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="CAT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="CAT NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdDelete" runat="server" OnClick="lnkbtngrdCATDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        
         <tr id ="trSelSubCategory">
            <td align="left" class="matters" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusSubCategory" runat="server" 
                    ImageUrl="../../../Images/PLUS.jpg" 
                    OnClientClick="return ToggleSearchSubCategory();return false;" 
                    CausesValidation="False" />
                Select Sub Category Filter</td>
        </tr>
        <tr id ="trSubCategory"  style="display:none;">
            <td align="left" valign ="top" class="matters">
                <asp:ImageButton ID="imgMinusSubCategory" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchSubCategory();return false;" CausesValidation="False" />&nbsp;Sub Category</td>
            <td align="left" class="matters" colspan="3" style="text-align: left">
                (Enter the Sub Category ID you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtSubCatName" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddSubCATID" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="ImageButton4" runat="server" 
                    ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetSubCATName()"  /><br />
                <asp:GridView ID="gvSubCat" runat="server" AutoGenerateColumns="False" Width="498px" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="SUB CAT ID">
                            <ItemTemplate>
                                <asp:Label ID="lblSubCATID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCR" HeaderText="SUB CAT NAME" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdSubCATDelete" runat="server" OnClick="lnkbtngrdSubCATDelete_Click">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
        
      
         <tr id ="trSelMake">
            <td align="left" class="matters" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusMake" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchMake();return false;" CausesValidation="False" />
                Select Make Filter</td>
        </tr>
        <tr id ="trMake"  style="display:none;">
            <td align="left" valign ="top" class="matters">
             <asp:ImageButton ID="imgMinusMake" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchMake();return false;" CausesValidation="False" />
                &nbsp;Make</td>
            <td align="left" class="matters" colspan="3" style="text-align: left">
                (Enter the Make you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtMakename" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox>
                <asp:LinkButton ID="lnlbtnAddMake" runat="server" Enabled="False">Add</asp:LinkButton>
                <asp:ImageButton ID="ImageButton7" runat="server" 

ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetMake()"  /><br />
                <asp:GridView ID="gvMake" runat="server" 

AutoGenerateColumns="False" Width="498px" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="MAKE">
                            <ItemTemplate>
                                <asp:Label ID="lblMake" runat="server" Text='<%#Bind("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField >
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkbtngrdMakeDelete" runat="server" OnClick="lnkbtngrMakedelete_Click">Delete</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
       
            <tr id ="trSelModel">
            <td align="left" class="matters" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusModel" runat="server" ImageUrl="../../../Images/PLUS.jpg" OnClientClick="return ToggleSearchModel();return false;" CausesValidation="False" />
                Select Model Filter</td></tr><tr id ="trModel"  style="display:none;">
            <td align="left" valign ="top" class="matters">
                <asp:ImageButton ID="imgMinusModel" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg" OnClientClick="return ToggleSearchModel();return false;" CausesValidation="False" />
                &nbsp;Model</td><td align="left" class="matters" colspan="3" style="text-align: left">
                (Enter the Model you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtModelName" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox><asp:LinkButton ID="lnlbtnAddModel" runat="server" Enabled="False">Add</asp:LinkButton><asp:ImageButton ID="ImageButton9" runat="server" 

ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetModel()"  /><br />
                <asp:GridView ID="gvModel" runat="server" AutoGenerateColumns="False" Width="498px" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="MODEL">
                            <ItemTemplate>
                                <asp:Label ID="lblModel" runat="server" Text='<%# Bind("ID") %>'></asp:Label></ItemTemplate></asp:TemplateField>
                                <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdModelDelete" runat="server" OnClick="lnkbtngrModeldelete_Click">Delete</asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>
       
         <tr id ="trselLocation">
            <td align="left" class="matters" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusLocation" runat="server" 

ImageUrl="../../../Images/PLUS.jpg" 
                    OnClientClick="return ToggleSearchLocation();return false;" 
                    CausesValidation="False" />
                Select Location Filter</td></tr><tr id ="trLocation"  style="display:none;">
            <td align="left" valign ="top" class="matters">
                <asp:ImageButton ID="imgMinusLocation" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg"
                 OnClientClick="return ToggleSearchLocation();return false;" CausesValidation="False" />
                &nbsp;Location</td><td align="left" class="matters" colspan="3" style="text-align: left">
                (Enter the Location you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtLocationName" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox><asp:LinkButton ID="lnlbtnAddLocation" runat="server" Enabled="False">Add</asp:LinkButton><asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetLocation()"  /><br />
                <asp:GridView ID="gvLoc" runat="server" AutoGenerateColumns="False" Width="498px" AllowPaging="True" PageSize="5">

                    <Columns>
                        <asp:TemplateField HeaderText="Location ID">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("ID") %>'></asp:Label></ItemTemplate></asp:TemplateField>
                                  <asp:BoundField DataField="DESCR" HeaderText="Location Desc" />
                                <asp:TemplateField >
                              <ItemTemplate><asp:LinkButton ID="lnkbtngrdLocationDelete"   runat="server" OnClick="lnkbtngrLocationdelete_Click">Delete</asp:LinkButton>
                              </ItemTemplate></asp:TemplateField>
                              </Columns>
                              <HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>    
                 <tr id ="trSelEmployee">
            <td align="left" class="matters" colspan="4" valign="top">
                <asp:ImageButton ID="imgPlusEmployee" runat="server" ImageUrl="../../../Images/PLUS.jpg" 
                    OnClientClick="return ToggleSearchEmployee();return false;" 
                    CausesValidation="False" />
                Select Employee Filter</td></tr><tr id ="trEmployee"  style="display:none;">
            <td align="left" valign ="top" class="matters">
                <asp:ImageButton ID="imgMinusEmployee" runat="server" style="display:none;" ImageUrl="../../../Images/MINUS.jpg"
                 OnClientClick="return ToggleSearchEmployee();return false;" CausesValidation="False" />
                &nbsp;Employee</td><td align="left" class="matters" colspan="3" style="text-align: left">
                (Enter the Employee you want to Add to the Search and click on Add)<br />
                <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="inputbox" Width="446px" ReadOnly="True"></asp:TextBox><asp:LinkButton ID="lnlbtnAddEmployee" runat="server" Enabled="False">Add</asp:LinkButton><asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="return GetEmployees()"  /><br />
                <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" Width="498px" AllowPaging="True" PageSize="5">

                    <Columns>
                        <asp:TemplateField HeaderText="Employee ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEmployee" runat="server" Text='<%# Bind("ID") %>'></asp:Label></ItemTemplate></asp:TemplateField>
                                  <asp:BoundField DataField="DESCR" HeaderText="Employee Desc" />
                                <asp:TemplateField >
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkbtngrdEmployeeDelete"   runat="server" OnClick="lnkbtngrEmployeedelete_Click">Delete</asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><HeaderStyle CssClass="gridheader_new" />
                </asp:GridView>
            </td>
        </tr>    
            
                                       
                         </table>
               </td>
        </tr>
       
        <tr>
            <td class="matters" style="height: 19px" align=center  valign="bottom">
                <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                    Text="Generate Report" UseSubmitBehavior="False" TabIndex="8" />
                </td>
        </tr>
        <tr>
            <td class="matters" valign="bottom" style="height: 52px">
                <asp:HiddenField ID="hfMDM_ID" runat="server" />
                 <input id="hfCategory" runat="server" type="hidden" />
                  <input id="hfEmployee" runat="server" type="hidden" />
                  <input id="hfSubCategory" runat="server" type="hidden" />
                  <input id="hfMake" runat="server" type="hidden" />
                  <input id="hfModel" runat="server" type="hidden" />
                  <input id="hfLocation" runat="server" type="hidden" />
          </td>
          
        </tr>
    </table>
    <asp:HiddenField ID="h_CATID" runat="server" />
    <asp:HiddenField ID="h_SubCATID" runat="server" />
     <asp:HiddenField ID="h_Make" runat="server" />
      <asp:HiddenField ID="h_Model" runat="server" />
       <asp:HiddenField ID="h_LocationID" runat="server" />
          <asp:HiddenField ID="h_EmployeeID" runat="server" />
</asp:Content>



