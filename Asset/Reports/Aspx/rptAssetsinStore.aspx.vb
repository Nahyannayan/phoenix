﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Asset_Reports_Aspx_rptAssetsinStore
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler usrBSUnits1.BSUSelection_Changed, AddressOf usrBSUnits1_BSUSelection_Changed
        AddHandler usrCATEGORY1.CategorySelection_Changed, AddressOf usrCATEGORY1_CategorySelection_Changed

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                usrManufacturer1.Enable = False

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "AST2080" And _
                ViewState("MainMnu_code") <> "AST2085" And ViewState("MainMnu_code") <> "AST2086" _
                And ViewState("MainMnu_code") <> "AST2090") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    If (ViewState("MainMnu_code") = "AST2080" Or ViewState("MainMnu_code") = "AST2086" Or ViewState("MainMnu_code") = "AST2090") Then
                        trassetalloc.Visible = False
                    Else
                        trassetalloc.Visible = True
                    End If
                    If ViewState("MainMnu_code") = "AST2086" Then
                        trSummary.Visible = True
                    Else
                        trSummary.Visible = False
                    End If

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    'BindBsu()
                    'BindManufacturer()
                    'BindCategory()
                    'BindModel()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Protected Sub usrBSUnits1_BSUSelection_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        usrCATEGORY1.BindFilterControlsMain(usrBSUnits1.GetSelectedNode())
    End Sub

    Protected Sub usrCATEGORY1_CategorySelection_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        usrModel1.BindFilterControlsMain(usrBSUnits1.GetSelectedNode(), usrCATEGORY1.GetSelectedNode())
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'Sub BindManufacturer()
    '    chkblMftr.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
    '    Dim str_query As String = "SELECT MAN_ID,MAN_DESCR FROM MANUFACTURE_M"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    chkblMftr.DataSource = ds
    '    chkblMftr.DataTextField = "MAN_DESCR"
    '    chkblMftr.DataValueField = "MAN_ID"
    '    chkblMftr.DataBind()

    '    Dim li As New ListItem
    '    li.Text = "All"
    '    li.Value = "0"
    '    chkblMftr.Items.Insert(0, li)
    'End Sub
    'Sub BindCategory()
    '    chkblCategory.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
    '    Dim str_query As String = "SELECT CAD_ID,CAD_DESCR FROM CATEGORY_D"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    chkblCategory.DataSource = ds
    '    chkblCategory.DataTextField = "CAD_DESCR"
    '    chkblCategory.DataValueField = "CAD_ID"
    '    chkblCategory.DataBind()
    '    Dim li As New ListItem
    '    li.Text = "All"
    '    li.Value = "0"
    '    chkblCategory.Items.Insert(0, li)
    'End Sub
    'Sub BindModel()
    '    chkblModel.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
    '    Dim str_query As String = "SELECT MDM_ID,MDM_DESCR FROM MODEL_M"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    chkblModel.DataSource = ds
    '    chkblModel.DataTextField = "MDM_DESCR"
    '    chkblModel.DataValueField = "MDM_ID"
    '    chkblModel.DataBind()
    '    Dim li As New ListItem
    '    li.Text = "All"
    '    li.Value = "0"
    '    chkblModel.Items.Insert(0, li)
    'End Sub

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", h_BSUID.Value)
        param.Add("@CAD_ID", h_CAT.Value)
        param.Add("@MAN_ID", h_Manu.Value)
        param.Add("@MDM_ID", h_Model.Value)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_asset"
            .reportParameters = param
            If (ViewState("MainMnu_code") = "AST2080") Then
                .reportPath = Server.MapPath("../Rpt/rptAssetsinStore.rpt")
            ElseIf (ViewState("MainMnu_code") = "AST2086") Then
                param.Add("@bSummary", chkSummary.Checked)
                If chkSummary.Checked Then
                    .reportPath = Server.MapPath("../Rpt/rptAsset_count.rpt")
                Else
                    .reportPath = Server.MapPath("../Rpt/rptAsset_CategoryCount.rpt")
                End If
            ElseIf (ViewState("MainMnu_code") = "AST2090") Then
                .reportPath = Server.MapPath("../Rpt/rptAsset_Pur_Details.rpt")
            Else
                param.Add("@LOC_ID", DBNull.Value)
                param.Add("@DPT_ID", DBNull.Value)
                If rdAll.Checked = True Then
                    param.Add("@TYPE", "ALL")
                ElseIf rdLoc.Checked = True Then
                    param.Add("@TYPE", "LOC")
                Else
                    param.Add("@TYPE", "EMP")
                End If

                .reportPath = Server.MapPath("../Rpt/rptAssetAllocated.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    'Sub BindBsu()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M "
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlBsu.DataSource = ds
    '    ddlBsu.DataTextField = "BSU_NAME"
    '    ddlBsu.DataValueField = "BSU_ID"
    '    ddlBsu.DataBind()
    '    Dim li As New ListItem
    '    li.Text = "Select Business Unit"
    '    li.Value = "0"
    '    ddlBsu.Items.Insert(0, li)
    'End Sub
#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        h_BSUID.Value = usrBSUnits1.GetSelectedNode()
        h_Manu.Value = usrManufacturer1.GetSelectedNode()
        h_CAT.Value = usrCATEGORY1.GetSelectedNode
        h_Model.Value = usrModel1.GetSelectedNode
        CallReport()
    End Sub


End Class
