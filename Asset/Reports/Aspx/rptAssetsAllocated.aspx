﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAssetsAllocated.aspx.vb" Inherits="Asset_Reports_Aspx_rptAssetsAllocated" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 550px">
        <tr>
            <td  style="height: 79px">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" Width="342px" />
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
         <td align="center"  valign="bottom" style="height: 20px">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" style="text-align: TB1center" Height="24px"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="height: 93px; font-weight: bold;" valign="top">
                <table align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 600" id="TB1" runat="server">
                    
                                       
                 
            
            
                    <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle">
                            Assets in Store</td>
                    </tr>
                            <tr>
                        <td align="left" class="matters" >
                            Business Unit</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" colspan="4"  >
                                 <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" 
                                     Width="434px" >
                                 </asp:DropDownList>
                       </td>          
                    </tr>
                         <tr>
                        <td align="left" class="matters" >
                            Manufacturer</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters" >
                                 <asp:DropDownList ID="ddlMftr" runat="server" AutoPostBack="false" 
                                     Width="233px">
                                 </asp:DropDownList>
                             </td>          
                             <td class="matters">
                                 Category</td>
                             <td class="matters">
                                 :</td>
                             <td class="matters">
                                 <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="false" 
                                     Width="233px">
                                 </asp:DropDownList>
                             </td>
                    </tr>
            
                                       
                         <tr>
                        <td align="left" class="matters" >
                            Model</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters"  >
                                 <asp:DropDownList ID="ddlModel" runat="server" AutoPostBack="false" 
                                     Width="233px">
                                 </asp:DropDownList>
                       </td>          
                             <td  class="matters">
                                 Location</td>
                             <td  class="matters">
                                 :</td>
                             <td  class="matters">
                                 <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="false" 
                                     Width="233px">
                                 </asp:DropDownList>
                             </td>
                    </tr>
            
                    
                                       
                         <tr>
                        <td align="left" class="matters" >
                            Department</td>
                        <td class="matters" >
                            :</td>
                        <td align="left" class="matters"  >
                                 <asp:DropDownList ID="ddlDpt" runat="server" AutoPostBack="false" 
                                     Width="233px">
                                 </asp:DropDownList>
                       </td>          
                             <td  class="matters" colspan="3">
                                 <asp:RadioButton ID="rdAll" runat="server" AutoPostBack="True" Checked="True" 
                                     GroupName="g1" Text="All" />
                                 <asp:RadioButton ID="rdLoc" runat="server" AutoPostBack="True" GroupName="g1" 
                                     Text="Location Only" />
                                 <asp:RadioButton ID="rdEmp" runat="server" AutoPostBack="True" GroupName="g1" 
                                     Text="Employee Only" />
                             </td>
                    </tr>
            
                    
                                       
                         <tr>
                        <td align="left" class="matters" colspan="6" >
                            &nbsp;</td>
                    </tr>
            
                                       
                         </table>
               </td>
        </tr>
       
        <tr>
            <td class="matters" style="height: 19px" align=center  valign="bottom">
                <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                    Text="Generate Report" UseSubmitBehavior="False" TabIndex="8" />
                </td>
        </tr>
        <tr>
            <td class="matters" valign="bottom" style="height: 52px">
                <asp:HiddenField ID="hfMDM_ID" runat="server" />
          </td>
          
        </tr>
    </table>
</asp:Content>

