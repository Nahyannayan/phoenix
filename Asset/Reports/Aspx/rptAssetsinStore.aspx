﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptAssetsinStore.aspx.vb" Inherits="Asset_Reports_Aspx_rptAssetsinStore" title="Untitled Page" %>
<%@ Register src="../../UserControls/usrManufacturer.ascx" tagname="usrManufacturer" tagprefix="uc2" %>
<%@ Register src="../../UserControls/usrCATEGORY.ascx" tagname="usrCATEGORY" tagprefix="uc3" %>
<%@ Register src="../../UserControls/usrModel.ascx" tagname="usrModel" tagprefix="uc4" %>
<%@ Register src="../../UserControls/usrBSUnitsAsset.ascx" tagname="usrBSUnitsAsset" tagprefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 70%">
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" Width="342px" />
            </td>
        </tr>
        <tr>
         <td align="center"  valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error" Width="133px" style="text-align: TB1center" ></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="font-weight: bold;" valign="top">
                <table align="center" border="1" class="BlueTableView" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 100%" id="TB1" runat="server">
                    <tr class="subheader_img">
                        <td align="left" colspan="6" valign="middle">
                            Assets in Store</td>
                    </tr>
                            <tr>
                        <td align="left" class="matters" valign="top"  style ="width :100px">
                            Business Unit</td>
                        <td class="matters" style ="width :1px">
                            :</td>
                        <td align="left" class="matters" colspan="4" valign="top"  >
                          <asp:Panel ID="Panel4" runat="server" Height="150px" ScrollBars="Auto">
                              <uc5:usrBSUnitsAsset ID="usrBSUnits1" runat="server" />
                                  </asp:Panel>              
                       </td>          
                    </tr>
                         <tr>
                        <td align="left" class="matters" valign="top" style ="width :100px" >
                            Manufacturer</td>
                        <td class="matters" style ="width :1px" >
                            :</td>
                        <td align="left" class="matters" valign="top" style ="width :200px">
                           <asp:Panel ID="Panel3" runat="server" Height="150px" ScrollBars="Auto" style ="width :100%">
                                     <uc2:usrManufacturer ID="usrManufacturer1" runat="server" />
                                 </asp:Panel>
                              
                             </td>          
                             <td class="matters" valign="top" style ="width :100px">
                                 Category</td>
                             <td class="matters" style ="width :1px">
                                 :</td>
                             <td class="matters" align="left" valign="top" style ="width :200px">
                                <asp:Panel ID="Panel2" runat="server" Height="150px" ScrollBars="Auto" style ="width :100%">
                                    <uc3:usrCATEGORY ID="usrCATEGORY1" runat="server" />
                                 </asp:Panel>
                                 
                             </td>
                    </tr>
            
                                       
                         <tr>
                        <td align="left" class="matters" valign="top"  style ="width :100px">
                            Model</td>
                        <td class="matters" style ="width :1px">
                            :</td>
                        <td align="left" class="matters" colspan="4" valign="top"  >
                                 <asp:Panel ID="Panel1" runat="server" Height="150px" ScrollBars="Auto">
                                     <uc4:usrModel ID="usrModel1" runat="server" />
                                 </asp:Panel>
                       </td>       
                          
                    </tr>
                       <tr runat ="server" id="trassetalloc">
                                         <td  class="matters" colspan="6">
                                 <asp:RadioButton ID="rdAll" runat="server"  Checked="True" 
                                     GroupName="g1" Text="All" />
                                 <asp:RadioButton ID="rdLoc" runat="server"  GroupName="g1" 
                                     Text="Location Only" />
                                 <asp:RadioButton ID="rdEmp" runat="server"  GroupName="g1" 
                                     Text="Employee Only" />
                             </td>
                    </tr>
                    <tr runat="server" id ="trSummary">
                        <td class="matters" colspan="6" style="text-align: left">
                            <asp:CheckBox ID="chkSummary" runat="server" Text="Summary" /></td>
                    </tr>
                    
                                       
                         <tr>
                        <td align="center" class="matters" colspan="6" >
                            <asp:Button ID="btnGenerateReport" runat="server" CausesValidation="False" CssClass="button"
                    Text="Generate Report" UseSubmitBehavior="False" TabIndex="8" />
                      <asp:HiddenField ID="hfMDM_ID" runat="server" />
                <asp:HiddenField ID="h_Manu" runat="server" />
                <asp:HiddenField ID="h_CAT" runat="server" />
                <asp:HiddenField ID="h_Model" runat="server" /> <asp:HiddenField ID="h_BSUID" runat="server" />
                </td>
                    </tr>            
                                       
                         </table>
               </td>
        </tr>
    </table>
</asp:Content>

