﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Asset_Reports_Aspx_rptAssetsAllocated
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "AST2085") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBsu()
                    BindManufacturer()
                    BindCategory()
                    BindModel()
                    BindLocation()
                    BindDepartment()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub BindManufacturer()
        ddlMftr.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query As String = "SELECT MAN_ID,MAN_DESCR FROM MANUFACTURE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlMftr.DataSource = ds
        ddlMftr.DataTextField = "MAN_DESCR"
        ddlMftr.DataValueField = "MAN_ID"
        ddlMftr.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlMftr.Items.Insert(0, li)
    End Sub
    Sub BindCategory()
        ddlCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query As String = "SELECT CAD_ID,CAD_DESCR FROM CATEGORY_D"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCategory.DataSource = ds
        ddlCategory.DataTextField = "CAD_DESCR"
        ddlCategory.DataValueField = "CAD_ID"
        ddlCategory.DataBind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlCategory.Items.Insert(0, li)
    End Sub
    Sub BindModel()
        ddlModel.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query As String = "SELECT MDM_ID,MDM_DESCR FROM MODEL_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlModel.DataSource = ds
        ddlModel.DataTextField = "MDM_DESCR"
        ddlModel.DataValueField = "MDM_ID"
        ddlModel.DataBind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlModel.Items.Insert(0, li)
    End Sub

    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", ddlBsu.SelectedValue.ToString)
        param.Add("@CAD_ID", ddlCategory.SelectedValue.ToString)
        param.Add("@MAN_ID", ddlMftr.SelectedValue.ToString)
        param.Add("@MDM_DESCR", ddlModel.SelectedItem.Text)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
        param.Add("@DPT_ID", ddlDpt.SelectedValue.ToString)
        param.Add("@LOC_ID", ddlLocation.SelectedValue.ToString)

        If rdAll.Checked = True Then
            param.Add("@TYPE", "ALL")
        ElseIf rdLoc.Checked = True Then
            param.Add("@TYPE", "LOC")
        Else
            param.Add("@TYPE", "EMP")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_asset"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptAssetAllocated.rpt")
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub

    Sub BindBsu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
        Dim li As New ListItem
        li.Text = "Select Business Unit"
        li.Value = "0"
        ddlBsu.Items.Insert(0, li)
    End Sub

    Sub BindLocation()

        ddlLocation.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_ASSETConnectionString
        Dim str_query As String = "SELECT LOC_ID,LOC_DESCR FROM LOCATION_M WHERE LOC_DESCR='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "LOC_DESCR"
        ddlLocation.DataValueField = "LOC_ID"
        ddlLocation.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlLocation.Items.Insert(0, li)

    End Sub

    Sub BindDepartment()
        ddlDpt.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DPT_ID,DPT_DESCR FROM DEPARTMENT_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlDpt.DataSource = ds
        ddlDpt.DataTextField = "DPT_DESCR"
        ddlDpt.DataValueField = "DPT_ID"
        ddlDpt.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlDpt.Items.Insert(0, li)
    End Sub
#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        BindLocation()
    End Sub
End Class
