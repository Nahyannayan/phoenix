﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Partial Class Asset_Reports_Aspx_rptFixedAssets
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Dim MainMnu_code As String

    Protected Sub lnkbtngrdCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblCATID As New Label
        lblCATID = TryCast(sender.FindControl("lblCATID"), Label)
        If Not lblCATID Is Nothing Then
            h_CATID.Value = h_CATID.Value.Replace(lblCATID.Text, "").Replace("||", "|")
            gvCat.PageIndex = gvCat.PageIndex
            FillCATNames(h_CATID.Value)
        End If
    End Sub

    Protected Sub lnkbtngrdSubCATDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSubCATID As New Label
        lblSubCATID = TryCast(sender.FindControl("lblSubCATID"), Label)
        If Not lblSubCATID Is Nothing Then
            h_SubCATID.Value = h_SubCATID.Value.Replace(lblSubCATID.Text, "").Replace("||", "|")
            gvSubCat.PageIndex = gvSubCat.PageIndex
            FillSubCATNames(h_SubCATID.Value)
        End If
    End Sub
    Protected Sub lnkbtngrMakedelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblMake As New Label
        lblMake = TryCast(sender.FindControl("lblMake"), Label)
        If Not lblMake Is Nothing Then
            h_Make.Value = h_Make.Value.Replace(lblMake.Text, "").Replace("||", "|")
            gvMake.PageIndex = gvMake.PageIndex
            FillMake(h_Make.Value)
        End If
    End Sub
    Protected Sub lnkbtngrModeldelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblModel As New Label
        lblModel = TryCast(sender.FindControl("lblModel"), Label)
        If Not lblModel Is Nothing Then
            h_Model.Value = h_Model.Value.Replace(lblModel.Text, "").Replace("||", "|")
            gvModel.PageIndex = gvModel.PageIndex
            FillModel(h_Model.Value)
        End If
    End Sub
    Protected Sub lnkbtngrLocationdelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblLocation As New Label
        lblLocation = TryCast(sender.FindControl("lblLocation"), Label)
        If Not lblLocation Is Nothing Then
            h_LocationID.Value = h_LocationID.Value.Replace(lblLocation.Text, "").Replace("||", "|")
            gvLoc.PageIndex = gvLoc.PageIndex
            FillLocations(h_LocationID.Value)
        End If
    End Sub
    Protected Sub lnkbtngrEmployeedelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblLocation As New Label
        lblLocation = TryCast(sender.FindControl("lblEmployee"), Label)
        If Not lblLocation Is Nothing Then
            h_EmployeeID.Value = h_LocationID.Value.Replace(lblLocation.Text, "").Replace("||", "|")
            gvEmp.PageIndex = gvLoc.PageIndex
            FillLocations(h_EmployeeID.Value)
        End If
    End Sub

    Private Function FillCATNames(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT  distinct ACM_ID as ID, ACM_DESCR as DESCR FROM ASSETS.ASSET_CATEGORY_M where ACM_ID in (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvCat.DataSource = ds
        gvCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillSubCATNames(ByVal SubCATIDs As String) As Boolean
        Dim IDs As String() = SubCATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT  distinct ASC_ID as ID, ASC_DESCR as DESCR FROM ASSETS.ASSET_SUBCATEGORY_M  WHERE ASC_ID IN (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvSubCat.DataSource = ds
        gvSubCat.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function

    Private Function FillMake(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT  distinct MKE_ID as ID, MKE_DESCR as DESCR FROM ASSETS.ASSET_MAKE where MKE_ID in (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvMake.DataSource = ds
        gvMake.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Private Function FillModel(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT  distinct MDL_ID as ID, MDL_DESCR as DESCR FROM ASSETS.ASSET_MODEL where MDL_ID in (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvModel.DataSource = ds
        gvModel.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Private Function FillEmployees(ByVal EmpIDs As String) As Boolean
        Dim IDs As String() = EmpIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "select distinct empno ID , ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') as DESCR from EMPLOYEE_M where 1=1 and EMP_ID in (" + condition + ")"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvEmp.DataSource = ds
        gvEmp.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Private Function FillLocations(ByVal CATIDs As String) As Boolean
        Dim IDs As String() = CATIDs.Split("|")
        Dim condition As String = String.Empty
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
        Dim str_Sql As String
        Dim ds As DataSet
        For i As Integer = 0 To IDs.Length - 1
            If i <> 0 Then
                condition += ", "
            End If
            condition += "'" & IDs(i) & "'"
        Next
        str_Sql = "SELECT  distinct LOC_ID as ID, LOC_DESCR as DESCR FROM ASSETS.[LOCATION_M] where LOC_ID in (" + condition + ")"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        gvLoc.DataSource = ds
        gvLoc.DataBind()
        If ds Is Nothing Or ds.Tables(0).Rows.Count <= 0 Then
            Return False
        End If
        Return True
    End Function
    Private Sub GenerateAssetDetails()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ASSETConnectionString").ConnectionString
            Dim strABC As String = String.Empty
            Dim strBsuid As String = ""
            Dim strCatIds As String = ""
            Dim strSubCatIDs As String = ""
            Dim strMakeIDs As String = ""
            Dim strModelIDs As String = ""
            Dim strLocaionIDs As String = ""
            Dim strEmployeeIDs As String = ""
            strCatIds = h_CATID.Value.ToString
            strSubCatIDs = h_SubCATID.Value.ToString
            strMakeIDs = h_Make.Value.ToString
            strModelIDs = h_Model.Value.ToString
            strLocaionIDs = h_LocationID.Value.ToString
            strEmployeeIDs = h_EmployeeID.Value.ToString

            Dim str_Sql As String = "[ASSETS].[rptFixedAssetsReport]"
            Dim cmd As New SqlCommand
            cmd.CommandText = str_Sql
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = New SqlConnection(str_conn)
            Dim sqlParam(6) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@CAT_ID", strCatIds, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(0))
            sqlParam(1) = Mainclass.CreateSqlParameter("@SUB_CAT_ID", strSubCatIDs, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(1))
            sqlParam(2) = Mainclass.CreateSqlParameter("@make", strMakeIDs, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(2))
            sqlParam(3) = Mainclass.CreateSqlParameter("@model", strModelIDs, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(3))
            sqlParam(4) = Mainclass.CreateSqlParameter("@locationID", strLocaionIDs, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(4))
            sqlParam(5) = Mainclass.CreateSqlParameter("@bsu_id", Session("sBsuid").ToString, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(5))
            sqlParam(6) = Mainclass.CreateSqlParameter("@EmployeeID", strEmployeeIDs, SqlDbType.VarChar)
            cmd.Parameters.Add(sqlParam(6))

            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            params("RPT_Caption") = "Fixed Asset Details"
            params("userName") = Session("sUsr_name")
            repSource.DisplayGroupTree = False
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.ResourceName = "../../Asset/Reports/RPT/rptFixedAssets.rpt"
            Session("ReportSource") = repSource
            Response.Redirect("../../../Reports/ASPX Report/Rptviewer.aspx", True)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        FillCATNames(h_CATID.Value)
    End Sub
    Protected Sub gvSubCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubCat.PageIndexChanging
        gvSubCat.PageIndex = e.NewPageIndex
        FillSubCATNames(h_SubCATID.Value)
    End Sub
    Protected Sub lnlbtnAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddCATID.Click
        h_CATID.Value += "|" + txtCatName.Text.Replace(",", "|")
        FillCATNames(h_CATID.Value)
    End Sub
    Protected Sub lnlbtnSubAddCATID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddSubCATID.Click
        h_SubCATID.Value += "|" + txtSubCatName.Text.Replace(",", "|")
        FillSubCATNames(h_SubCATID.Value)
    End Sub
    Protected Sub lnlbtnAddMake_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddMake.Click
        h_Make.Value += "|" + txtMakename.Text.Replace(",", "|")
        FillMake(h_Make.Value)
    End Sub

    Protected Sub lnlbtnAddModel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddModel.Click
        h_Model.Value += "|" + txtModelName.Text.Replace(",", "|")
        FillModel(h_Model.Value)
    End Sub

    Protected Sub lnlbtnAddLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlbtnAddLocation.Click
        h_LocationID.Value += "|" + txtLocationName.Text.Replace(",", "|")
        FillModel(h_LocationID.Value)
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        GenerateAssetDetails()
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hfCategory.Value = "display"
                hfEmployee.Value = "display"
            End If
            ClientScript.RegisterStartupScript(Me.GetType(), "script", "<script language='javascript'>  CheckOnPostback(); </script>")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Request.QueryString("MainMnu_code") = "" Then
                Response.Redirect("..\..\noAccess.aspx")
            End If
            MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Page.Title = OASISConstants.Gemstitle

            Dim nodata As Boolean = IIf(Request.QueryString("nodata") Is Nothing, False, True)
            If nodata Then
                lblError.Text = "No Records with specified condition"
            Else
                lblError.Text = ""
            End If
            FillCATNames(h_CATID.Value)
            FillSubCATNames(h_SubCATID.Value)
            FillMake(h_Make.Value)
            FillModel(h_Model.Value)
            FillLocations(h_LocationID.Value)
            FillEmployees(h_EmployeeID.Value)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
