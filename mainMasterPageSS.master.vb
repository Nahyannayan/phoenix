Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports UtilityObj
Partial Class mainMasterPageSS
    Inherits System.Web.UI.MasterPage
    Dim userSuper As Boolean
    Dim Encr_decrData As New Encryption64
    Public Property DependantLoggedIn() As Boolean
        Get
            Return ViewState("DependantLoggedIn")
        End Get
        Set(ByVal value As Boolean)
            ViewState("DependantLoggedIn") = value
        End Set
    End Property
    Public Property DependantID() As String
        Get
            Return ViewState("DependantID")
        End Get
        Set(ByVal value As String)
            ViewState("DependantID") = value
        End Set
    End Property

    Public ReadOnly Property MenuName() As String
        Get
            Return Session("temp")
        End Get
    End Property
    Public Property EmployeeID() As String
        Get
            Return ViewState("EmployeeID")
        End Get
        Set(ByVal value As String)
            ViewState("EmployeeID") = value
        End Set
    End Property

    Public Sub DisableScriptManager()
        ScriptManager1.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblUser.Text = Session("sUsr_Display_Name")
        If Session("sUsr_name") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        If Not Request.QueryString("MainMnu_code") Is Nothing Then
            If UtilityObj.CheckUSerAccessRights(Session("sBsuid"), Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+")), _
                   Session("sModule"), Session("sUsr_name"), Session("sBusper")) <> 0 Then
                Response.Redirect("~/BusinessUnit.aspx")
            End If
        End If
        Page.Title = OASISConstants.Gemstitle
        If Page.IsPostBack = False Then
            Session("temp") = ""
            Dim MainMnu_code, sqlstr As String
            If Not Request.QueryString("MainMnu_code") Is Nothing Then
                MainMnu_code = Request.QueryString("MainMnu_code")
                If MainMnu_code <> "" Then
                    MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                    Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    sqlstr = "SELECT MNU_TEXT FROM MENUS_M where MNU_CODE='" & MainMnu_code & "'"
                    Session("temp") = Mainclass.getDataValue(sqlstr, "OASISConnectionString")
                End If
            End If
            If Session("sUsr_name") & "" <> "" Then
                ' lblFilePath.Text = Session("Menu_text")
                If Session("Menu_text") <> "" And Session("Menu_text") <> String.Empty Then
                    Dim menu_arr() As String = Session("Menu_text").Split("|")
                    Session("temp") = menu_arr(menu_arr.Length - 1)
                End If
                LinkHome.Text = "Home"
                If Session("sModule") = "SS" Then
                    LinkHome.PostBackUrl = "homePageSS.aspx"
                    lnkKKHead.PostBackUrl = "~/Payroll/empDependantsDetail.aspx?MainMnu_code=" & Encr_decrData.Encrypt("U000064") & "&datamode=" & Encr_decrData.Encrypt("view")
                End If
                EmployeeID = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                Try
                    Dim ds As New DataSet
                    Dim moduleDecr As New Encryption64
                    Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                    Dim bunit As String = Session("sBsuid")
                    Dim userRol As String = Session("sroleid")
                    Dim userMod As String = Session("sModule")
                    Dim sqlString As String
                    Dim username As String = Session("sUsr_Display_Name") 'Session("sUsr_name")
                    'lblUserLog.Text = username.ToUpper
                    userSuper = Session("sBusper")
                    Using Imgreader As SqlDataReader = AccessRoleUser.GetBUnitImage(bunit)
                        While Imgreader.Read
                            imgSchool.ImageUrl = Imgreader.GetString(0)
                        End While
                    End Using
                    'Using conn As SqlConnection = New SqlConnection(connStr)
                    '    If userSuper = False Then
                    '        sqlString = "select MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                    '                      " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
                    '                      " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                    '                       " where (mnu_parentid is not null) and mnu_code in(select mnr_mnu_id from menurights_s where mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='" & userMod & "' order by MNU_PARENTID,MNU_ID "
                    '    Else
                    '        sqlString = "select MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                    '                      " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,MNU_IMAGE from MENUS_M" & _
                    '                      " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                    '                       " where (mnu_parentid is not null) and mnu_module='" & userMod & "' order by MNU_PARENTID,MNU_ID "
                    '    End If
                    '    Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, conn)
                    '    da.Fill(ds)
                    '    da.Dispose()
                    '    SqlConnection.ClearPool(conn)
                    'End Using
                    'ds.DataSetName = "MENURIGHTS"
                    'ds.Tables(0).TableName = "MENURIGHT"
                    'Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("MENURIGHT").Columns("MNU_CODE"), ds.Tables("MENURIGHT").Columns("MNU_PARENTID"), True)
                    'relation.Nested = True
                    'ds.Relations.Add(relation)
                    'If ds.Tables(0).Rows.Count > 0 Then
                    '    xmlDataSource.Data = ds.GetXml()
                    '    xmlDataSource.DataBind()
                    'Else
                    '    Response.Redirect("~/Modulelogin.aspx")
                    'End If
                Catch ex As Exception
                    lblMainError.Text = "Your page could not processed"
                End Try
            End If
        End If
        FillProfileData()
    End Sub

    Protected Sub lblModules_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblModules.Click
        Response.Redirect("~/Modulelogin.aspx")
    End Sub
    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        Try
            Dim MainMnu_code As String = e.Item.Value
            Dim strRedirect As String = "#"
            Dim menu_rights As String = ""
            Dim MainMmnr_bsu_id As String = Session("sBsuid")
            Dim MainMmnr_rol_id As String = Session("sroleid")
            Dim usr_id As String = Session("sUsr_id")
            Dim url As String = ""
            userSuper = Session("sBusper")
            If usr_id = "" Or MainMmnr_rol_id = "" Or MainMmnr_bsu_id = "" Then
                Response.Redirect("~/login.aspx")
            Else
                Using readerUserDetail As SqlDataReader = AccessRoleUser.GetRedirectPage(MainMmnr_bsu_id, MainMnu_code, MainMmnr_rol_id, userSuper)
                    If userSuper = False Then
                        While readerUserDetail.Read()
                            menu_rights = Convert.ToString(readerUserDetail(0))
                            ' Menu_text = Convert.ToString(readerUserDetail(1))
                            strRedirect = Convert.ToString(readerUserDetail(2))
                        End While
                    Else
                        While readerUserDetail.Read()
                            ' Menu_text = Convert.ToString(readerUserDetail(0))
                            strRedirect = Convert.ToString(readerUserDetail(1))
                        End While
                        menu_rights = "6"
                    End If
                End Using
                Dim datamode As String = "none"
                Dim encrData As New Encryption64
                Session("Menu_text") = UtilityObj.GetFilepath(e.Item.ValuePath.Split("/"), e.Item.Depth)
                MainMnu_code = encrData.Encrypt(MainMnu_code)
                datamode = encrData.Encrypt(datamode)
                url = String.Format("{0}?MainMnu_code={1}&datamode={2}", strRedirect, MainMnu_code, datamode)
                If Trim(strRedirect) = "#" Then
                Else
                    Response.Redirect(url)
                End If
            End If
        Catch ex As Exception
            lblMainError.Text = "File can not be located"
        End Try
    End Sub
    Sub displayList()
        Dim myTable As New DataTable
        Try
            myTable = getMenuItems("")
            If Not myTable Is Nothing Then
                SideMenu.DataSource = myTable.DefaultView
                SideMenu.DataBind()
                'SideMenu.SelectedIndex = 1
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub FillEmployeeProfileData()
        Dim sqlStr As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        sqlStr = "select  EMP_PASSPORTNAME ,EMD_PHOTO  from  EMPLOYEE_M ,EMPLOYEE_D where EMP_ID=EMD_EMP_ID and EMP_ID='" & EmployeeID & "'"
        Dim EMPTable As New DataTable
        EMPTable = Mainclass.getDataTable(sqlStr, str_conn)
        lnkName.Text = EOS_MainClass.GetEmployeeNameFromID(EmployeeID)
        'Dim imgPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
        If EMPTable.Rows.Count > 0 Then
            Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & EMPTable.Rows(0).Item("EMD_PHOTO").ToString
            imgProfile.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
        End If
        displayList()
        hlAddDoc.NavigateUrl = "~/Payroll/empDocumentAdd.aspx" & "?MainMnu_code=" & Encr_decrData.Encrypt("U000062") & "&datamode=" & Encr_decrData.Encrypt("add") & "&TYPE=" & Encr_decrData.Encrypt("EMP")
        hlViewDoc.NavigateUrl = "~/Payroll/empDocumentDetail.aspx" & "?MainMnu_code=" & Encr_decrData.Encrypt("U000062") & "&datamode=" & Encr_decrData.Encrypt("view") & "&TYPE=" & Encr_decrData.Encrypt("EMP")
        imgProfile.PostBackUrl = "~/homepageSS.aspx"
    End Sub
    Private Sub FillDependantProfileData()
        Dim EOS_Detail As New EOS_EmployeeDependant.DependantDetail
        EOS_Detail = EOS_EmployeeDependant.GetDependantDetail(DependantID)
        lnkName.Text = EOS_Detail.EDD_NAME
        imgProfile.ImageUrl = "~/Payroll/ImageHandler.ashx?ID=" + EOS_Detail.EDD_ID.ToString & "&TYPE=EDD"
        hlAddDoc.NavigateUrl = "~/Payroll/empDocumentAdd.aspx?ProfileID=" & Encr_decrData.Encrypt(DependantID) & "&MainMnu_code=" & Encr_decrData.Encrypt("U000062") & "&datamode=" & Encr_decrData.Encrypt("add") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
        hlViewDoc.NavigateUrl = "~/Payroll/empDocumentDetail.aspx" & "?ProfileID=" & Encr_decrData.Encrypt(DependantID) & "&MainMnu_code=" & Encr_decrData.Encrypt("U000062") & "&datamode=" & Encr_decrData.Encrypt("view") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
        imgProfile.PostBackUrl = "~/Payroll/empProfileView.aspx?ProfileID=" & Encr_decrData.Encrypt(DependantID) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
    End Sub
    'Private Sub FillNotes()
    '    Try
    '        Dim sqlStr As String
    '        sqlStr = "select  isnull(Notes,'') from vw_EOS_DashBoard"
    '        lblNewsLetter.Text = Mainclass.getDataValue(sqlStr, "OASISConnectionString")
    '    Catch ex As Exception

    '    End Try
    'End Sub
    Private Sub FillDashBoard()
        Try
            Dim Result As New DataTable
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim mTable As New DataTable
            Dim mSet As New DataSet
            Dim sqlParam(1) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@EMP_ID", EmployeeID, SqlDbType.Int)
            sqlParam(1) = Mainclass.CreateSqlParameter("@EDD_ID", IIf(DependantID Is Nothing, 0, DependantID), SqlDbType.Int)
            mSet = Mainclass.getDataSet("[EOS_GetDashBoardDetails]", sqlParam, str_conn)
            If mSet.Tables.Count > 0 Then
                If mSet.Tables(0).Rows.Count > 0 Then
                    gvReminders.Visible = True
                    gvReminders.DataSource = mSet.Tables(0)
                    gvReminders.DataBind()
                Else
                    gvReminders.Visible = False
                    gvReminders.DataSource = Nothing
                    gvReminders.DataBind()
                End If
            
                If mSet.Tables.Count > 1 Then
                    If mSet.Tables(0).Rows.Count > 0 Then
                        gvNotes.Visible = True
                        gvNotes.DataSource = mSet.Tables(1)
                        gvNotes.DataBind()
                    Else
                        gvNotes.Visible = False
                        gvNotes.DataSource = Nothing
                        gvNotes.DataBind()
                        '  lblNewsLetter.Text = mSet.Tables(1).Rows(0).Item(0)
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvReminders_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReminders.RowDataBound
        Try
            Dim lblNotes As New Label
            lblNotes = e.Row.FindControl("lblNotes")
            If lblNotes Is Nothing Then Exit Sub
            Dim Notes As String
            Notes = lblNotes.Text
            ' Notes = Notes.Replace("~", Request.ApplicationPath)
            Notes = Notes.Replace("~", WebConfigurationManager.AppSettings.Item("webvirtualURL"))
            Notes = FormatEncryptURL(Notes)
            lblNotes.Text = Notes
        Catch ex As Exception

        End Try
    End Sub
    Private Function FormatEncryptURL(ByVal URL As String) As String
        Try
            Dim mStr As String
            Dim i, StartIndex, EndIndex As Int16
            While i <= URL.Length - 1
                StartIndex = URL.IndexOf("|*", i)
                EndIndex = URL.IndexOf("*|", i)
                If StartIndex = -1 Or EndIndex = -1 Then Exit While
                mStr = URL.Substring(StartIndex + 2, EndIndex - StartIndex - 2)
                URL = URL.Replace("|*" + mStr + "*|", Encr_decrData.Encrypt(mStr))
                i = EndIndex
            End While
        Catch ex As Exception
        Finally
            FormatEncryptURL = URL
        End Try
    End Function
    Private Sub FillProfileData()
        If DependantLoggedIn Then
            FillDependantProfileData()
        Else
            FillEmployeeProfileData()
        End If
        tblPayment.Visible = False 'DependantLoggedIn
        tblReportCard.Visible = False 'DependantLoggedIn
        Dim sqlstr, sqlWhr As String
        Dim bunit As String = Session("sBsuid")
        userSuper = Session("sBusper")
        Dim userRol As String = Session("sroleid")
        Dim userMod As String = Session("sModule")
        sqlstr = "select isnull(count(*),0) from menurights_s   "
        sqlstr &= " where mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0 "
        sqlWhr = " and MNR_MNU_ID='U000062'"

        If Mainclass.getDataValue(sqlstr & sqlWhr, "OASISConnectionString") > 0 Or userSuper Then
            tblOthers.Visible = DependantLoggedIn
        Else
            tblOthers.Visible = False
        End If
        sqlWhr = " and MNR_MNU_ID='U000064'"
        If Mainclass.getDataValue(sqlstr & sqlWhr, "OASISConnectionString") > 0 Or userSuper Then
            lnkKKHead.PostBackUrl = "~/Payroll/empDependantsDetail.aspx?MainMnu_code=" & Encr_decrData.Encrypt("U000064") & "&datamode=" & Encr_decrData.Encrypt("view")
        Else
            lnkKKHead.PostBackUrl = "homePageSS.aspx"
        End If
        BindGrids()
        FillDashBoard()
    End Sub
    Private Sub BindGrids()
        Try
            Dim mTable As New DataTable
            mTable = EOS_EmployeeDependant.GetDependantDetailList(EmployeeID)
            If DependantLoggedIn Then
                If mTable.Select("EDD_ID=" & DependantID).Length > 0 Then
                    mTable.Select("EDD_ID=" & DependantID)(0).Delete()
                    mTable.AcceptChanges()
                End If
            End If
            dlKithKin.DataSource = mTable
            dlKithKin.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub dlKithKin_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlKithKin.ItemDataBound
        Try
            Dim hdnEDDID As New HiddenField
            hdnEDDID = e.Item.FindControl("hdnEDDID")
            If hdnEDDID Is Nothing Then
                Exit Sub
            End If
            Dim hdnEDDname As New HiddenField
            hdnEDDname = e.Item.FindControl("hdnEDDname")
            If hdnEDDname Is Nothing Then
                Exit Sub
            End If
            Dim imgDependant As New ImageButton
            imgDependant = e.Item.FindControl("imgDependant")
            Dim tdItem As New Table
            tdItem = e.Item.FindControl("tblItem")
            Dim NavigateURL As String
            NavigateURL = "~/Payroll/empProfileView.aspx?ProfileID=" & Encr_decrData.Encrypt(hdnEDDID.Value) & "&MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & ViewState("datamode") & "&TYPE=" & Encr_decrData.Encrypt("EDD")
            If Not imgDependant Is Nothing Then
                imgDependant.ImageUrl = "~/Payroll/ImageHandler.ashx?ID=" + hdnEDDID.Value.ToString & "&TYPE=EDD"
                imgDependant.PostBackUrl = NavigateURL
                imgDependant.ToolTip = hdnEDDname.Value
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
            lblMainError.Text = "Unexpected Error !!!"
        End Try
    End Sub
    Private Function getMenuItems(ByVal MenuId As String) As DataTable
        Try
            Dim ds As New DataSet
            Dim moduleDecr As New Encryption64
            Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim bunit As String = Session("sBsuid")
            Dim userRol As String = Session("sroleid")
            Dim userMod As String = Session("sModule")
            Dim sqlString As String
            Dim username As String = Session("sUsr_Display_Name")
            userSuper = Session("sBusper")
            Dim sqlSelect, sqlOrderBy, sqlWhere As String
            sqlWhere = ""
            Dim mnu_DT As New DataTable
            If MenuId = "" Then
                If userSuper = False Then
                    sqlSelect = "select mnu_text,mnu_code,mnu_image from MENUS_M where  isnull(MNU_PARENTID,'')<>'' AND  mnu_code IN (select   distinct MNU_PARENTID from MENUS_M" & _
                                   " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                                   " where (mnu_parentid is not null) and mnu_code in(select mnr_mnu_id from menurights_s where mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='" & userMod & "')"
                    sqlOrderBy = "  order by MNU_PARENTID,MNU_ID "
                Else
                    sqlSelect = "select mnu_text,mnu_code,mnu_image  from MENUS_M where  isnull(MNU_PARENTID,'')<>'' AND  mnu_code IN (select distinct  MNU_PARENTID from MENUS_M" & _
                                    " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                                    " where (mnu_parentid is not null) and mnu_module='" & userMod & "' )"
                    sqlOrderBy = " order by MNU_PARENTID "
                End If
            Else
                If userSuper = False Then
                    sqlSelect = "select MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                                  " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,'Images\ButtonImages\' + MNU_CODE +'.png' MNU_IMAGE from MENUS_M" & _
                                  " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                                   " where (mnu_parentid is not null) and mnu_code in(select mnr_mnu_id from menurights_s where mnu_module='" & userMod & "' and mnr_bsu_id='" & bunit & "' and mnr_rol_id='" & userRol & "' and mnr_right!=0) and mnu_module='" & userMod & "'"
                    sqlOrderBy = "  order by MNU_PARENTID,MNU_ID "
                Else
                    sqlSelect = "select MNU_CODE,case when B.Parent IS not NULL  then '' else MNU_TEXT end MNU_TEXT," & _
                                  " MNU_NAME, case when B.Parent IS NULL then MNU_PARENTID else NULL end MNU_PARENTID,'Images\ButtonImages\' + MNU_CODE +'.png' MNU_IMAGE from MENUS_M" & _
                                  " left join (select MNU_CODE  Parent from MENUS_M  where  mnu_parentid is  null) B on MENUS_M.MNU_PARENTID=B.Parent" & _
                                   " where (mnu_parentid is not null) and mnu_module='" & userMod & "'"
                    sqlOrderBy = " order by MNU_PARENTID "
                End If
                sqlWhere = " AND MNU_PARENTID='" & MenuId & "'"
            End If


            sqlString = sqlSelect & sqlWhere & sqlOrderBy
            mnu_DT = Mainclass.getDataTable(sqlString, connStr)
            Return mnu_DT
        Catch ex As Exception
            lblMainError.Text = "Your page could not processed"
            Return Nothing
        End Try
    End Function

    Protected Sub SideMenu_ItemDataBound(ByVal sender As Object, ByVal e As AjaxControlToolkit.AccordionItemEventArgs) Handles SideMenu.ItemDataBound
        If e.ItemType = AjaxControlToolkit.AccordionItemType.Content Then
            Dim grd As New GridView()
            grd = DirectCast(e.AccordionItem.FindControl("grdSubMenu"), GridView)
            Dim mnu_DT As New DataTable
            mnu_DT = getMenuItems(DirectCast(e.AccordionItem.FindControl("txtMainMenuID"), HiddenField).Value)
            If Not mnu_DT Is Nothing Then
                grd.DataSource = mnu_DT
                grd.DataBind()
            End If
        End If
    End Sub
    Private Sub setImageDisplay(ByVal imgMap As Object, ByVal PhotoBytes As Byte(), Optional ByVal ID As String = "")
        Try
            Dim OC As System.Drawing.Image
            If Not PhotoBytes Is Nothing Then
                OC = EOS_MainClass.ConvertBytesToImage(PhotoBytes)
            Else
                OC = System.Drawing.Image.FromFile(WebConfigurationManager.AppSettings.Item("NoImagePath"))
            End If
            If OC Is Nothing Then Exit Sub
            Dim nameOfImage As String = Session.SessionID & Replace(Now.ToString, ":", "") & ID & "CurrentImage"
            If Not Cache.Get(nameOfImage) Is Nothing Then
                Cache.Remove(nameOfImage)
            End If
            Cache.Insert(nameOfImage, OC, Nothing, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Default, Nothing)
            imgMap.ImageUrl = "~/Payroll/empOrgChartImage.aspx?ID=" & nameOfImage
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub grdSubMenu_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim hlMenuItem As New HyperLink
            Dim hdnMenuItem, hdnMenuCode As New HiddenField
            hlMenuItem = e.Row.FindControl("hlMenuItem")
            hdnMenuItem = e.Row.FindControl("hdnMenuItem")
            hdnMenuCode = e.Row.FindControl("hdnMenuCode")
            If hlMenuItem Is Nothing Then Exit Sub
            hlMenuItem.NavigateUrl = hdnMenuItem.Value & "?MainMnu_code=" & Encr_decrData.Encrypt(hdnMenuCode.Value) & "&datamode=" & Encr_decrData.Encrypt("view")

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub lnkKKHead_Click(sender As Object, e As EventArgs)
    '    Response.Redirect("~/Payroll/empDependantsDetail.aspx?MainMnu_code=" & Encr_decrData.Encrypt("U000064") & "&datamode=" & Encr_decrData.Encrypt("view"), False)
    'End Sub

    Protected Sub imgProfile_Click(sender As Object, e As ImageClickEventArgs) Handles imgProfile.Click
        Dim passwordEncr As New EncryptionHR
        'Dim redirect As String = "<script>window.open('http://gcosql01/OASISNEW/EmpSelfServices/Profile2.aspx');</script>"
        'Response.Write(redirect)
        Dim USRID As String = Encr_decrData.Encrypt(Session("sUsr_id"))
        Dim username As String = Encr_decrData.Encrypt(Session("sUsr_name"))
        Dim BSU_ID As String = Encr_decrData.Encrypt(Session("sBsuid"))
        Dim empid As String = Encr_decrData.Encrypt(Session("EmployeeId"))
        Dim sUsrRoleID As String = Encr_decrData.Encrypt(Session("sroleid"))
        ''Dim bsuname As String = Encr_decrData.Encrypt(Session("BSU_Name"))
        ''Dim designation As String = Encr_decrData.Encrypt(Session("sUsr_Desig_master"))
        Dim url As String
        url = String.Format("https://school.gemsoasis.com/OASISNEWTHEME/EmpSelfServices/Profile2.aspx" & "?USR_ID=" + USRID + "&username=" + username + "&BSU_ID=" + BSU_ID + "&empid=" + empid + "&roleId=" + sUsrRoleID + "")
        '' url = String.Format("http://localhost:59128/EmpSelfServices/Profile2.aspx" & "?USR_ID=" + USRID + "&username=" + username + "&BSU_ID=" + BSU_ID + "&empid=" + empid + "&roleId=" + sUsrRoleID + "")

        Dim str As String = "window.open('" & url + "', 'popup_window', 'width=1400,height=900,left=10,top=10,resizable=yes');"

        'ClientScript.RegisterStartupScript(Me.GetType(), "script", s, True)
        ' Response.Redirect(url)
        'ScriptManager.RegisterStartupScript(Me, typeof(string), "OPEN_WINDOW", "var Mleft = (screen.width/2)-(760/2);var Mtop = (screen.height/2)-(700/2);
        Response.Write("<Script> " & str & " </Script>")



    End Sub
End Class

