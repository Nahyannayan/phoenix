﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class ErrorInfo
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            Dim referrer As String
            If Not Request.UrlReferrer Is Nothing Then
                referrer = Request.UrlReferrer.ToString()
                Response.AppendHeader("Refresh", "5; URL=" & referrer)

            Else

                Response.AppendHeader("Refresh", "5; URL=homepage.aspx")

            End If



        End If
    End Sub
End Class
