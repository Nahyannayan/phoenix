﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FolderGalleryPermission.aspx.vb" Inherits="FolderGallery_FolderGalleryPermission" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
    <link href="/PHOENIXBETA/FolderGallery/css/main-teacher.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/FolderGallery/css/all.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/EnquiryStyle.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/FolderGallery/css/component.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/build.css" rel="stylesheet" />
    <style>
        .RB_Holder label label strong {
            color: #679c26;
        }
        div.checkbox-list-full, .checkbox-list-full
        {
            max-height:250px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Folder Gallery Permission
      
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  text-right">
                </div>



            </div>
        </div>


        <div class="card-body">
            <div class="container-fluid p-0 mt-2 mb-4">

                <div class="container-fluid p-0 mt-2 mb-4 clearfix position-relative teachers-lockers">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="card min-height">
                                <div class="card-body">
                                    <div class="task-search column-padding pt-3 pb-3">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-7 col-12">
                                              
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-12 text-right">
                                              
                                                <div style="display: inline-block;" runat="server" id="DIV_Permission_Folder" visible="false">
                                                  <a href="javascript:void(0)" class="btn btn-primary dropdown-toggle mr-0 mt-lg-0 mb-0 mt-md-0 waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Permission</a>
                                                <div class="dropdown-menu z-depth-1 border-0">
                                                    <a id="btnViewPermission" class="dropdown-item pt-2 pb-2" href="/PHOENIXBETA/FolderGallery/FolderGalleryPermission_View.aspx<%=PID_QueryString %>">View</a>
                                                    <a id="btnAddPermission" class="dropdown-item pt-2 pb-2" href="/PHOENIXBETA/FolderGallery/FolderGalleryPermission.aspx<%=PID_QueryString %>" >Add</a>
                                                </div></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="add-task column-padding pt-3 pb-0">
                                        <div class="row ">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                                                <ol id="folder_nav" class="breadcrumb breadcrumb-arrow" runat="server">
                                                </ol>

                                            </div>

                                        </div>
                                    </div>


                                    <!-- library content start -->
                                    <div class="library-data column-padding pt-3 pb-3">
                                        <div id="library" class="row view-group">
                                            <div class="col-lg-12 col-md-12 col-12">
                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Staff</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12" style="padding: 20px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-md-12 mb-2">
                                                                <div class="form-group">
                                                                    <label class="Ftitle">
                                                                        select user
                                                                   <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                                                    <asp:ListBox ID="ddluser" runat="server" CssClass="form-control js-example-basic-multiple" multiple="multiple" SelectionMode="Multiple"></asp:ListBox>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix col-md-12"></div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <%--                <label class="Ftitle">(Online Enquiry Acknowledgement will be mailed to your E-mail address)</label>--%>
                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Staf_add" runat="server" Text="Permission To <strong> Add </strong> Folder"></asp:CheckBox>
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">

                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Staf_View" runat="server" Text="Permission To <strong> View </strong> folder"></asp:CheckBox>
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">

                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Staf_Rename" runat="server" Text="Permission To <strong> Rename </strong> Folder"></asp:CheckBox>
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">

                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Staf_Delete" runat="server" Text="Permission To <strong> Delete </strong> Folder"></asp:CheckBox>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 text-center mt-3">
                                                                <asp:LinkButton Text="Save Staff Permission" runat="server" ID="btnSaveStuf" CssClass="btn btn-success " OnClick="btnSaveStuf_Click" />

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row mt-3">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Parent</strong>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-12" style="padding: 20px; border: 1px solid #e5ebdd;">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="Ftitle">
                                                                        Academic Year
                                                                          <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                            
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="Ftitle">
                                                                  <asp:Label ID="lblSlctGrade" runat="server" Text="Grade And Sections" CssClass="field-label" />
                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                                                    <asp:TreeView ID="trGrades" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked()" ForeColor="Black"
                                                    runat="server" TabIndex="2">
                                                </asp:TreeView>
                                                                </div>
                                                            </div>
                                                               <div class="clearfix col-md-12"></div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">

                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Parent_View" runat="server" Checked="true" Text="Permission To <strong> View </strong> folder"></asp:CheckBox>
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3" runat="server" id="DIV_Parent_add" visible="false">
                                                                <div class="form-group">
                                                                    <%--                <label class="Ftitle">(Online Enquiry Acknowledgement will be mailed to your E-mail address)</label>--%>
                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Parent_add" runat="server" Text="Permission To <strong> Add </strong> Folder"></asp:CheckBox>
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-md-3" runat="server" id="DIV_Parent_Rename" visible="false">
                                                                <div class="form-group">

                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Parent_Rename" runat="server" Text="Permission To <strong> Rename </strong> Folder"></asp:CheckBox>
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3" runat="server" id="DIV_Parent_Delete" visible="false">
                                                                <div class="form-group">

                                                                    <div class="RB_Holder">
                                                                        <label class="checkbox-inline checkbox checkbox-success">
                                                                            <asp:CheckBox ID="chb_Parent_Delete" runat="server" Text="Permission To <strong> Delete </strong> Folder"></asp:CheckBox>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 text-center mt-3">
                                                                <asp:LinkButton Text="Save Parent Permission" runat="server" ID="btnSaveParent" CssClass="btn btn-success " OnClick="btnSaveParent_Click" />

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
   
    <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    <link href="/PHOENIXBETA/FolderGallery/css/select2.min.css" rel="stylesheet" />
    <script src="/PHOENIXBETA/FolderGallery/js/select2.min.js"></script>
    <script>
         function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.js-example-basic-multiple').select2();
                }
            });
        };

    </script>
</asp:Content>

