﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="FolderGalleryView.aspx.vb" Inherits="FolderGallery_FolderGalleryView" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
    <link href="/PHOENIXBETA/FolderGallery/css/main-teacher.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/FolderGallery/css/all.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/EnquiryStyle.css" rel="stylesheet" />
    <link href="/PHOENIXBETA/FolderGallery/css/component.css" rel="stylesheet" />
    <style type="text/css">
        /* Chart.js */
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }

            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }

            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Folder Gallery
      
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  text-right">
                </div>



            </div>
        </div>

        <div class="card-body">
            <div class="container-fluid p-0 mt-2 mb-4">

                <div class="container-fluid p-0 mt-2 mb-4 clearfix position-relative teachers-lockers">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div class="card min-height">
                                <div class="card-body">

                                    <div class="task-search column-padding pt-3 pb-3">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-7 col-12">
                                                <div class="search-wrapper">
                                                    <i class="fas fa-search active"></i>
                                                    <input id="btnSearchFiles" type="text" class="search-field" placeholder="Search Files">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-12 text-right">
                                                <div style="display: inline-block;" runat="server" id="DIV_Add_Folder" visible="false">
                                                    <a href="javascript:void(0)" class="btn btn-primary dropdown-toggle mr-0 mt-lg-0 mb-0 mt-md-0 waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Add New</a>
                                                    <div class="dropdown-menu z-depth-1 border-0">
                                                        <a runat="server" visible="false" id="btnAddFolder" class="dropdown-item pt-2 pb-2" href="javascript:void(0)" data-toggle="modal" data-target="#FolderModal">Folder</a>
                                                        <a id="btnAddFile" class="dropdown-item pt-2 pb-2" href="javascript:void(0)" data-toggle="modal" data-target="#FileModal">File</a>
                                                    </div>
                                                </div>
                                                <div style="display: inline-block;" runat="server" id="DIV_Permission_Folder" visible="false">
                                                    <a href="javascript:void(0)" class="btn btn-primary dropdown-toggle mr-0 mt-lg-0 mb-0 mt-md-0 waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Permission</a>
                                                    <div class="dropdown-menu z-depth-1 border-0">
                                                        <a id="btnViewPermission" class="dropdown-item pt-2 pb-2" href="/PHOENIXBETA/FolderGallery/FolderGalleryPermission_View.aspx<%=PID_QueryString %>">View</a>
                                                        <a id="btnAddPermission" class="dropdown-item pt-2 pb-2" href="/PHOENIXBETA/FolderGallery/FolderGalleryPermission.aspx<%=PID_QueryString %>">Add</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal -->
                                    <div class="modal fade" id="FolderModal" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="FolderModal_heading">Add Folders</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div id="FolderModal_Loader">
                                                    <div class="popup-form">


                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 ">
                                                                <div class="md-form md-outline p-4">
                                                                    <asp:TextBox runat="server" class="form-control" placeholder="Folder Name" ID="txt_folder_Name" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Folder Name Required " ControlToValidate="txt_folder_Name" runat="server" ValidationGroup="CFolde" Style="color: red" />
                                                                    <label for="CategoryId" class="active">Folder Name</label>
                                                                    <span class="field-validation-valid" data-valmsg-for="FolderName" data-valmsg-replace="true"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="d-flex justify-content-end">
                                                        <button type="button" class="btn btn-outline-primary waves-effect waves-light" data-dismiss="modal">Cancel</button>

                                                        <asp:Button Text="Save" ID="btn_Save_Folder" runat="server" class="btn btn-primary waves-effect waves-light" ValidationGroup="CFolde" OnClick="btn_Save_Folder_Click" />
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- end Modal -->
                                    <!-- Modal -->
                                    <div class="modal fade" id="FileModal" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="FileModal_heading">Add File</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div id="FileModal_Loader">
                                                    <div class="popup-form">


                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 ">
                                                                <div class="md-form md-outline p-4">
                                                                    <div class="box">
                                                                        <%--<input type="file" name="file_7[]" id="file_7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple style="display:none" />--%>
                                                                        <asp:FileUpload runat="server" ID="FileUpload_File" AllowMultiple="true" CssClass="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
                                                                        <label for="ctl00_cphMasterpage_FileUpload_File">
                                                                            <span></span><strong>
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                                                                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" />
                                                                                </svg>
                                                                                Choose a file&hellip;</strong></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="d-flex justify-content-end">
                                                        <button type="button" class="btn btn-outline-primary waves-effect waves-light" data-dismiss="modal">Cancel</button>

                                                        <asp:Button Text="Save" ID="btn_Save_file" runat="server" class="btn btn-primary waves-effect waves-light" OnClick="btn_Save_file_Click" />
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- end Modal -->
                                    <div class="clearfix"></div>
                                    <div class="add-task column-padding pt-3 pb-0">
                                        <div class="row ">
                                            <div class="col-xl-11 col-lg-11 col-md-11 col-10">
                                                <ol id="folder_nav" class="breadcrumb breadcrumb-arrow" runat="server">
                                                </ol>

                                            </div>
                                            <div class="col-xl-1 col-lg-1 col-md-1 col-2 text-lg-right text-md-right text-left">
                                                <div class="task-sorting">
                                                    <div class="toggle-library d-inline-block mt-1">
                                                        <a href="javascript:void(0)" id="list" class="active"><i class="fas fa-list"></i></a>
                                                        <a href="javascript:void(0)" id="grid" class="active"><i class="fas fa-th"></i></a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- library content start -->
                                    <div class="library-data column-padding pt-3 pb-3">
                                        <div id="library" class="row view-group">
                                            <div class="col-lg-12 col-md-12 col-12">
                                                <div id="folder_list" class="row" runat="server">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Folders</strong>
                                                        </h4>
                                                    </div>


                                                    <asp:ListView runat="server" ID="ListView_GetGalleryFolder">
                                                        <ItemTemplate>
                                             
                                                                 
                                                            <div class="item search-item col-xl-2 col-lg-3 col-md-3 col-6 grid-group-item list-group-item">
                                                                <div class="library-content clearfix">
                                                                    <div class="item-icon">
                                                                        <a id="btnViewFolder2" href="/PHOENIXBETA/FolderGallery/FolderGalleryView.aspx?pfid=<%# Encr_id(Eval("FET_ID")) %>">
                                                                            <i class="fas fa-folder-open"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <p id="btnViewFolder1" data-id="NcA%2bk%2bMb5p0ibACSF1ZBKQ%3d%3d">
                                                                            <%# Eval("FET_DISPLAY_NAME") %>
                                                                            <span class="detailInfo">
                                                                                <span data-toggle="tooltip" data-title="Created by" data-original-title="" title=""><%# Eval("FET_CREATED_BY") %></span>
                                                                                <span data-toggle="tooltip" data-title="Created on" data-original-title="" title=""><%# Eval("FET_CREATED_DT", "{0:dd/MMM/yyyy}") %></span>
                                                                            </span>
                                                                        </p>
                                                                        <div class="quick-nav dropleft">
                                                                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fas fa-ellipsis-v"></i>
                                                                                <span class="sr-only">Toggle Dropdown</span>
                                                                            </button>
                                                                            <div class="dropdown-menu z-depth-1">
                                                                                <asp:Label runat="server" ID="btnViewFolder" Style="display: block;">
                                                                                <a   class="dropdown-item" href='/PHOENIXBETA/FolderGallery/FolderGalleryView.aspx?pfid=<%# Encr_id(Eval("FET_ID"))%>'>View</a></asp:Label>
                                                                                <asp:Label runat="server" ID="btnEditFolder" Style="display: block;">
                                                                                <a  class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target='#FolderEditModal<%# Encr_id(Eval("FET_ID")) %>'>Edit</a>
                                                                                </asp:Label>
                                                                                <asp:Label runat="server" ID="btnDeleteFolder" Style="display: block;">
                                                                                     <asp:LinkButton runat="server" id="btnDeleteFolder2" class="dropdown-item" OnClientClick="if ( ! FolderDeleteConfirmation()) return false;" OnClick="btnDeleteFolder2_Click" >Delete</asp:LinkButton>
                                                                          
                                                                                </asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <!-- Modal -->
                                                            <div class="modal fade" id='FolderEditModal<%# Encr_id(Eval("FET_ID")) %>' role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="FolderModal_heading">Rename Folders</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </div>
                                                                        <div id="FolderModal_Loader">
                                                                            <div class="popup-form">


                                                                                <div class="row">
                                                                                    <div class="col-lg-12 col-md-12 ">
                                                                                        <div class="md-form md-outline p-4">
                                                                                            <asp:HiddenField runat="server" ID="HF_ID" Value='<%# Encr_id(Eval("FET_ID")) %>' />
                                                                                             <asp:HiddenField runat="server" ID="HF_P_ID" Value='<%# Encr_id(Eval("FET_PARENT_ID")) %>' Visible="false" />
                                                                                            <asp:TextBox runat="server" class="form-control" placeholder="Folder Name" ID="txt_folder_Edit_Name" Text='<%# Eval("FET_DISPLAY_NAME") %>' />
                                                                                            <asp:RequiredFieldValidator ErrorMessage="Folder Name Required " ControlToValidate="txt_folder_Edit_Name" runat="server" ValidationGroup="CEFolde" Style="color: red" />
                                                                                            <label for="CategoryId" class="active">Folder Name</label>
                                                                                            <span class="field-validation-valid" data-valmsg-for="FolderName" data-valmsg-replace="true"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <div class="d-flex justify-content-end">
                                                                                <button type="button" class="btn btn-outline-primary waves-effect waves-light" data-dismiss="modal">Cancel</button>

                                                                                <asp:Button Text="Save" ID="btn_Edit_Folder" runat="server" class="btn btn-primary waves-effect waves-light" ValidationGroup="CEFolde" OnClick="btn_Edit_Folder_Click" />
                                                                            </div>
                                                                        </div>


                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- end Modal -->
                                                        </ItemTemplate>
                                                        <EmptyDataTemplate>
                                                            <div class="col-md-12 text-center">
                                                                <h4 class="sub-heading">
                                                                    <strong>No folders found</strong>
                                                                </h4>
                                                            </div>
                                                        </EmptyDataTemplate>
                                                    </asp:ListView>



                                                </div>

                                                <div id="file_list" class="row" runat="server">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Files</strong>
                                                        </h4>
                                                    </div>
                                                    <asp:ListView runat="server" ID="ListView_GetGalleryFiles">
                                                        <ItemTemplate>
                                                               <asp:HiddenField runat="server" ID="HF_ID" Value='<%# Encr_id(Eval("FET_ID")) %>' Visible="false" />
                                                                  <asp:HiddenField runat="server" ID="HF_P_ID" Value='<%# Encr_id(Eval("FET_PARENT_ID")) %>' Visible="false" />
                                                            <div class="item search-item col-xl-2 col-lg-3 col-md-3 col-6 grid-group-item list-group-item">
                                                                <div class="library-content clearfix">
                                                                    <div class="item-icon">
                                                                        <a id="btnDownloadFile1" data-id="VtfhDv09k%2f2kn99S5vEzPw%3d%3d" href="javascript:void(0)">
                                                                            <%# Get_Icon_For_EXTENTION(Eval("FET_FILE_EXTENTION")) %>
                                                                        </a>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <p id="btnDownloadFile2" data-id="VtfhDv09k%2f2kn99S5vEzPw%3d%3d">
                                                                            <asp:Label ID="lbl_fileName" Text='<%# Eval("FET_DISPLAY_NAME") + Eval("FET_FILE_EXTENTION") %>' runat="server" />
                                                                            
                                                                            <span class="detailInfo">
                                                                                <span data-toggle="tooltip" data-title="Created by" data-original-title="" title=""><%# Eval("FET_CREATED_BY") %></span>
                                                                                <span data-toggle="tooltip" data-title="Created on" data-original-title="" title=""><%# Eval("FET_CREATED_DT", "{0:dd/MMM/yyyy}") %></span>
                                                                            </span>
                                                                        </p>
                                                                        <div class="quick-nav dropleft">
                                                                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fas fa-ellipsis-v"></i>
                                                                                <span class="sr-only">Toggle Dropdown</span>
                                                                            </button>

                                                                            <div class="dropdown-menu z-depth-1">
                                                                                <asp:Label runat="server" ID="btnDownloadFile" Style="display: block">
                                                                                <a   class="dropdown-item" href=' <%# AzureBlobURL + Eval("FET_PATH").ToString().Replace("~", "") + Eval("FET_DESCR") + Eval("FET_FILE_EXTENTION")%>' download>Download</a>
                                                                                </asp:Label>
                                                                                <asp:Label runat="server" ID="btnDeleteFile" Style="display: block">
                                                                                    <asp:LinkButton runat="server" id="btnDeleteFile2" class="dropdown-item" OnClientClick="if ( ! UserDeleteConfirmation()) return false;" OnClick="btnDeleteFile2_Click" >Delete</asp:LinkButton>
                                                                                </asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </ItemTemplate>
                                                        <EmptyDataTemplate>
                                                            <div id="NoFolderDiv" class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <h4 class="sub-heading">
                                                                        <strong>No Files found</strong>
                                                                    </h4>
                                                                </div>
                                                        </EmptyDataTemplate>
                                                    </asp:ListView>
                                                </div>
                                                <div id="Div_Dont_haveP" class="row" runat="server" visible="false">

                                                    <div class="col-lg-12 col-md-12 col-12">
                                                        <h4 class="sub-heading">
                                                            <strong>Files</strong>
                                                        </h4>
                                                        <div class="col-md-12 text-center">
                                                            <div class="alert alert-warning" role="alert">
  <h4 class="alert-heading">Warning!</h4>
  <p>You do not have permission to view the files inside that folder.</p>
  <hr/>
  <p class="mb-0">Please contact the administrator to view this folder.</p>
</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- library content end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <input type="hidden" id="PageView" value="list" />
    <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    <script src="/PHOENIXBETA/FolderGallery/js/js.cookie.min.js"></script>
    <script type="text/javascript">
        $(function () {


            $('#list').click(function (event) {

                event.preventDefault();
                $('#library .item').addClass('list-group-item');
                $(this).toggleClass('active');
                $("#grid").removeClass('active');
                Cookies.set("selectedView", "0");


            });

            $('#grid').click(function (event) {
                $("#PageView").val("grid");
                event.preventDefault();
                $('#library .item').removeClass('list-group-item');
                $('#library .item').addClass('grid-group-item');
                $(this).toggleClass('active');
                $("#list").removeClass('active');
                Cookies.set("selectedView", "1");

            });
            function get_Page_View() {
                if (Cookies.get('selectedView') == "0") {

                    $('#library .item').addClass('list-group-item');
                    $(this).toggleClass('active');
                    $("#grid").removeClass('active');

                } else {

                    $('#library .item').removeClass('list-group-item');
                    $('#library .item').addClass('grid-group-item');
                    $(this).toggleClass('active');
                    $("#list").removeClass('active');
                }
            }
            get_Page_View();
            $(document).on('click', '#btnViewFolder', function () {
                var id = $(this).data("id");
                //console.log(id);
                var url = window.location.pathname + "?pfid=" + id;
                window.location.href = url;
            });

            $(document).on('click', '#btnRootFolder', function () {

                var grId = $("#SectionId").val();
                //console.log(id);
                var url = window.location.pathname;
                window.location.href = url;
            });

        });
    </script>
    <!-- END PAGE PLUGINS -->

      <script src="/PHOENIXBETA/FolderGallery/js/custom-file-input.js"></script>

    <script src="/PHOENIXBETA/FolderGallery/js/Files.js.download"></script>
    <script src="/PHOENIXBETA/FolderGallery/js/Folders.js.download"></script>



    <!-- END SCRIPTS -->
    <script>
        function Close_model() {
            $('#myModal .close').click();
            try {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                if (prm != null) {
                    prm.add_endRequest(function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            $('#myModal .close').click();
                        }
                    });
                };
            } catch (e) {

            }

        }
                function UserDeleteConfirmation() {
    return confirm("Are you sure you want to delete this row ?");
        }
            function FolderDeleteConfirmation() {
    return confirm("Are you sure you want to delete this Folder  ?");
}
    </script>
</asp:Content>


