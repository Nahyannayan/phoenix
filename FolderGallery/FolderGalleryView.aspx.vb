﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports UserControls_usrMessageBar
Imports StorageHelper

Partial Class FolderGallery_FolderGalleryView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Popup_Message As String = ""
    Public PID_QueryString As String = ""
    Public Popup_Message_Status As WarningType
    Public serverpath As String = WebConfigurationManager.AppSettings("UploadExcelFile").ToString + "enquiry\"
    Public serverpath_Virtual As String = WebConfigurationManager.AppSettings("UploadExcelFilePathVirtual").ToString + "enquiry/"
    Public AzureBlobURL As String = WebConfigurationManager.AppSettings("AzureBlobURL").ToString
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then
            GridBindPayHist()

        End If
        Me.RegisterPostBackControl()

    End Sub
    Private Sub GridBindPayHist()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds_Folder As New DataSet
        Dim ds_Files As New DataSet
        Dim Parent_Id As String = 0
        Dim Paret_Path As String = ""
        Try

            Using objConn
                Dim Home As String = "<li><a id='btnRootFolder' href='/PHOENIXBETA/FolderGallery/FolderGalleryView.aspx'><i class='fas fa-layer-group'></i></a></li>"
                If Request.QueryString("pfid") Is Nothing Then
                    'Parent_Id =

                    Dim pParms_Parent(3) As SqlClient.SqlParameter
                    pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
                    folder_nav.InnerHtml = Home
                    PID_QueryString = ""
                Else
                    Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))

                    Dim test As String = Get_Bread_crumbs(Parent_Id, Parent_Id, str_conn, "")

                    folder_nav.InnerHtml = Home + test
                    PID_QueryString = "?pfid=" + Request.QueryString("pfid").Replace(" ", "+")
                End If


                '' get Folder data
                Dim pParms_Folder(5) As SqlClient.SqlParameter
                pParms_Folder(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Folder(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                pParms_Folder(2) = New SqlClient.SqlParameter("@FET_PARENT_ID", Parent_Id)
                pParms_Folder(3) = New SqlClient.SqlParameter("@FET_IS_DIRECTORY", True)

                ds_Folder = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_DETAIL]", pParms_Folder)
                ListView_GetGalleryFolder.DataSource = ds_Folder
                ListView_GetGalleryFolder.DataBind()


                '' get Files data
                Dim pParms_Files(5) As SqlClient.SqlParameter
                pParms_Files(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Files(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                pParms_Files(2) = New SqlClient.SqlParameter("@FET_PARENT_ID", Parent_Id)
                pParms_Files(3) = New SqlClient.SqlParameter("@FET_IS_DIRECTORY", False)

                ds_Files = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_DETAIL]", pParms_Files)
                ListView_GetGalleryFiles.DataSource = ds_Files
                ListView_GetGalleryFiles.DataBind()
                If ds_Files.Tables(0).Rows.Count() > 0 Then
                    file_list.Visible = True
                    If ds_Folder.Tables(0).Rows.Count() > 0 Then
                        folder_list.Visible = True
                    Else
                        folder_list.Visible = False
                    End If
                Else
                    file_list.Visible = False
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                '' get permations 
                Dim ViewFolder As Boolean = False
                Dim AddFolder As Boolean = False
                Dim RenameFolder As Boolean = False
                Dim DeleteFolder As Boolean = False
                Dim IS_DIRECTORY As Boolean = False
                If Check_If_Admin() Then
                    DIV_Add_Folder.Visible = True
                    DIV_Permission_Folder.Visible = True
                    btnAddFolder.Visible = True
                    ViewFolder = True
                    AddFolder = True
                    RenameFolder = True
                    DeleteFolder = True
                Else

                    DIV_Permission_Folder.Visible = False
                    btnAddFolder.Visible = False
                    Dim pParms_Parent(5) As SqlClient.SqlParameter
                    pParms_Parent(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                    pParms_Parent(1) = New SqlClient.SqlParameter("@FPT_USER_TYPE", "STAFF")
                    pParms_Parent(2) = New SqlClient.SqlParameter("@FPT_FET_ID", Parent_Id)
                    pParms_Parent(3) = New SqlClient.SqlParameter("@USR_NAME_ID", Session("sUsr_name"))
                    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_FILE_PERMISSION_DATA_by_User_ID", pParms_Parent)
                        If reader.HasRows Then
                            While reader.Read
                                ViewFolder = Convert.ToBoolean(reader("FPT_VIEW"))
                                AddFolder = Convert.ToBoolean(reader("FPT_ADD"))
                                RenameFolder = Convert.ToBoolean(reader("FPT_RENAME"))
                                IS_DIRECTORY = Convert.ToBoolean(reader("FET_IS_DIRECTORY"))
                                DeleteFolder = Convert.ToBoolean(reader("FPT_DELETE"))
                            End While
                            Div_Dont_haveP.Visible = False
                        Else
                            file_list.Visible = False
                            Div_Dont_haveP.Visible = True
                        End If

                    End Using
                    DIV_Add_Folder.Visible = AddFolder
                End If
                For Each itemLV As ListViewItem In ListView_GetGalleryFolder.Items
                    Dim btnViewFolder As Label = TryCast(itemLV.FindControl("btnViewFolder"), Label)
                    Dim btnEditFolder As Label = TryCast(itemLV.FindControl("btnEditFolder"), Label)
                    Dim btnDeleteFolder As Label = TryCast(itemLV.FindControl("btnDeleteFolder"), Label)
                    btnViewFolder.Visible = ViewFolder
                    btnEditFolder.Visible = RenameFolder
                    If IS_DIRECTORY Then
                        btnDeleteFolder.Visible = False
                    Else
                        btnDeleteFolder.Visible = DeleteFolder
                    End If

                Next
                For Each itemLV As ListViewItem In ListView_GetGalleryFiles.Items
                    Dim btnDownloadFile As Label = TryCast(itemLV.FindControl("btnDownloadFile"), Label)
                    Dim btnDeleteFile As Label = TryCast(itemLV.FindControl("btnDeleteFile"), Label)
                    btnDownloadFile.Visible = ViewFolder
                    btnDeleteFile.Visible = DeleteFolder
                Next

            End Using
        Catch ex As Exception
            Dim test As String = ex.Message
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Function Get_Icon_For_EXTENTION(EXTENTION As String) As String
        Dim icon As String = ""
        If EXTENTION.ToLower = ".xlsx" Or EXTENTION.ToLower = ".xlx" Then
            icon = "<i class='fas fa-file-excel ic-file-excel'></i>"
        ElseIf EXTENTION.ToLower = ".pdf" Then
            icon = "<i class='fas fa-file-pdf ic-file-pdf'></i>"
        ElseIf EXTENTION.ToLower = ".doc" Or EXTENTION.ToLower = ".docx" Then
            icon = "<i class='fas fa-file-word ic-file-word'></i>"
        ElseIf EXTENTION.ToLower = ".jpeg" Or EXTENTION.ToLower = ".jpg" Or EXTENTION.ToLower = ".gef" Or EXTENTION.ToLower = ".png" Then
            icon = "<i class='fas fa-file-image ic-file-img'></i>"
        ElseIf EXTENTION.ToLower = ".txt" Then
            icon = "<i class='fas fa-file-alt ic-file'></i>"
        ElseIf EXTENTION.ToLower = ".mp4" Then
            icon = "<i class='fas fa-file-video ic-file-video'></i>"
        Else
            icon = "<i class='fas fa-file ic-file'></i>"
        End If

        Return icon
    End Function
    Function Encr_id(id As String) As String
        Dim Enc_ID As String = ""
        Enc_ID = Encr_decrData.Encrypt(id)
        Return Enc_ID
    End Function
    Function Get_Bread_crumbs(id As String, current_id As String, str_conn As String, Full_URL As String) As String
        Try
            Dim result As String = ""
            Dim URL As String = ""
            Dim FET_DESCR As String = String.Empty
            Dim FET_ID As String = String.Empty
            Dim FET_PARENT_ID As String = String.Empty
            Dim pParms_Parent(4) As SqlClient.SqlParameter
            pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent(2) = New SqlClient.SqlParameter("@FET_ID", id)
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_FILE_EXPLORER_PARENT_data", pParms_Parent)
                While reader.Read
                    FET_DESCR = Convert.ToString(reader("FET_DISPLAY_NAME"))
                    FET_ID = Convert.ToString(reader("FET_ID"))
                    FET_PARENT_ID = Convert.ToString(reader("FET_PARENT_ID"))

                    If FET_PARENT_ID <> "0" Then
                        If id = current_id Then
                            URL = "<li class='active'><span>" + FET_DESCR + "</span></li>"
                        Else
                            URL = "<li><a id='btnViewFolder' href='/FolderGallery/FolderGalleryView.aspx?pfid=" + Encr_id(FET_ID) + "' >" + FET_DESCR + "</a></li>"
                        End If
                        Full_URL = URL + Full_URL
                        Full_URL = Get_Bread_crumbs(FET_PARENT_ID, current_id, str_conn, Full_URL)

                    End If

                End While
            End Using
            result = Full_URL
            Return result
        Catch ex As Exception

        End Try
    End Function
    Protected Sub btn_Save_Folder_Click(sender As Object, e As EventArgs)
        If txt_folder_Name.Text <> "" Then
            Dim Parent_Id As String = 0
            Dim Paret_Path As String = ""
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Using objConn
                If Request.QueryString("pfid") Is Nothing Then
                    'Parent_Id =

                    Dim pParms_Parent(3) As SqlClient.SqlParameter
                    pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
                Else
                    Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))
                End If
                If Can_Create_Folder(Parent_Id, str_conn) Then
                    Dim pParms_Parent_path(3) As SqlClient.SqlParameter
                    pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_ID", Parent_Id)
                    Paret_Path = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_PARENT_Path]", pParms_Parent_path)
                    objConn.Open()
                    Dim stTrans As SqlTransaction = objConn.BeginTransaction
                    Try

                        Dim pParms_Folder(14) As SqlClient.SqlParameter
                        pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", "0")
                        pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                        pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                        pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", Parent_Id)
                        pParms_Folder(4) = New SqlClient.SqlParameter("@FET_DESCR", txt_folder_Name.Text)
                        pParms_Folder(12) = New SqlClient.SqlParameter("@FET_DISPLAY_NAME", txt_folder_Name.Text)
                        pParms_Folder(5) = New SqlClient.SqlParameter("@FET_PATH", "~" + Paret_Path.Replace("~", "") + "/" + txt_folder_Name.Text)
                        pParms_Folder(7) = New SqlClient.SqlParameter("@FET_IS_DIRECTORY", True)
                        pParms_Folder(8) = New SqlClient.SqlParameter("@FET_FILE_EXTENTION", "")
                        pParms_Folder(9) = New SqlClient.SqlParameter("@FET_IS_ACTIVE", True)
                        pParms_Folder(10) = New SqlClient.SqlParameter("@LOGIN_USER", Session("sUsr_name"))
                        pParms_Folder(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms_Folder(11).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVE_FILE_EXPLORER_DATA", pParms_Folder)
                        Dim ReturnFlag As Integer = pParms_Folder(11).Value
                        stTrans.Commit()
                        If ReturnFlag = 0 Then

                            'If (Not System.IO.Directory.Exists(serverpath + Paret_Path.Replace("~", "") + "/" + txt_folder_Name.Text)) Then
                            '    System.IO.Directory.CreateDirectory(serverpath + Paret_Path.Replace("~", "") + "/" + txt_folder_Name.Text)
                            'End If
                        End If
                        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Hiding_the_error_class", "$('#myModal .close').click();", True)
                        'ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script111", "Close_model();", True)
                        txt_folder_Name.Text = ""
                        Popup_Message = "Folder has been Created successfully"
                        Popup_Message_Status = WarningType.Success
                        GridBindPayHist()

                    Catch ex As Exception
                        stTrans.Rollback()
                        Popup_Message = "Folder has been not Created successfully  Please try again  "
                        Popup_Message_Status = WarningType.Danger
                    Finally
                        objConn.Close()
                        usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                    End Try
                Else
                    Popup_Message = "you can Create only one leavel folder  "
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                End If
            End Using
        End If
    End Sub

    Protected Sub btn_Save_file_Click(sender As Object, e As EventArgs)
        Dim is_valed = True
        Try
            If FileUpload_File.HasFiles Then

                For Each file In FileUpload_File.PostedFiles

                    Dim FileName As String = file.FileName
                    Dim extention = GetExtension(FileName)
                    If extention.ToLower = "jpg" Or extention.ToLower = "jpeg" Or extention.ToLower = "png" Or extention.ToLower = "gif" Or
                    extention.ToLower = "mp4" Or extention.ToLower = "m4a" Or extention.ToLower = "m4v" Or extention.ToLower = "mov" Or
                                extention.ToLower = "wmv" Or extention.ToLower = "wma" Or extention.ToLower = "flv" Or extention.ToLower = "avi" Then
                        is_valed = True
                    Else

                        is_valed = False
                        Exit For
                    End If
                Next
                If is_valed Then
                    For Each file In FileUpload_File.PostedFiles
                        Dim filen As String()
                        Dim filen_wEx As String()
                        Dim FileName As String = file.FileName
                        Dim FileName_without_Extension As String = ""
                        Dim NameToCheck As String = ""
                        Dim already_exists As String = ""
                        filen = FileName.Split("\")
                        FileName = filen(filen.Length - 1)

                        Dim extention = GetExtension(FileName)

                        Dim Parent_Id As String = 0
                        Dim Paret_Path As String = ""
                        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
                        Dim objConn As New SqlConnection(str_conn)
                        Using objConn
                            '\\\\ Get Parent_Id
                            If Request.QueryString("pfid") Is Nothing Then

                                Dim pParms_Parent(3) As SqlClient.SqlParameter
                                pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                                pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                                Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
                            Else
                                Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))
                            End If
                            If Is_That_root_Folder(Parent_Id, str_conn) Then
                                Popup_Message = "you can not Create Files on Root Folder  "
                                Popup_Message_Status = WarningType.Danger
                                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                            Else


                                '\\\\ Get Paret_Path
                                Dim pParms_Parent_path(3) As SqlClient.SqlParameter
                                pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                                pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                                pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_ID", Parent_Id)
                                Paret_Path = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_PARENT_Path]", pParms_Parent_path)

                                '\\\\ Check_Duplicate_Files
                                filen_wEx = FileName.Split(".")
                                NameToCheck = filen_wEx(0)
                                FileName_without_Extension = filen_wEx(0)
                                If (If_File_Exist(NameToCheck, Parent_Id)) Then
                                    Dim counter As Integer = 2
                                    Dim tempfileName As String = ""
                                    While If_File_Exist(NameToCheck, Parent_Id)
                                        tempfileName = FileName_without_Extension & ("( " + counter.ToString() + " )")
                                        NameToCheck = tempfileName
                                        counter += 1
                                    End While
                                    FileName = tempfileName + "." + extention
                                    already_exists = "A file with the same name already exists." & "<br />Your file was saved as " & FileName
                                Else
                                    already_exists = "file has been Created successfully"
                                End If
                                'Dim pathToCheck As String = serverpath + Paret_Path.Replace("~", "") + "/" + FileName
                                'If (System.IO.File.Exists(pathToCheck)) Then
                                '    'FileName_without_Extension = FileName_without_Extension + "(1)"
                                '    Dim counter As Integer = 2
                                '    Dim tempfileName As String = ""
                                '    While System.IO.File.Exists(pathToCheck)
                                '        tempfileName = ("( " + counter.ToString() + " )") & FileName
                                '        pathToCheck = serverpath + Paret_Path.Replace("~", "") + "/" + tempfileName
                                '        counter += 1
                                '    End While
                                '    FileName = tempfileName
                                '    already_exists = "A file with the same name already exists." & "<br />Your file was saved as " & FileName
                                'Else
                                '    already_exists = "file has been Created successfully"
                                'End If
                                filen_wEx = FileName.Split(".")
                                FileName_without_Extension = filen_wEx(0)
                                objConn.Open()
                                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                                Try

                                    Dim pParms_Folder(14) As SqlClient.SqlParameter
                                    pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", "0")
                                    pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                                    pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                                    pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", Parent_Id)
                                    pParms_Folder(4) = New SqlClient.SqlParameter("@FET_DESCR", FileName_without_Extension)
                                    pParms_Folder(12) = New SqlClient.SqlParameter("@FET_DISPLAY_NAME", FileName_without_Extension)
                                    pParms_Folder(5) = New SqlClient.SqlParameter("@FET_PATH", "~" + Paret_Path.Replace("~", "") + "/")
                                    pParms_Folder(7) = New SqlClient.SqlParameter("@FET_IS_DIRECTORY", False)
                                    pParms_Folder(8) = New SqlClient.SqlParameter("@FET_FILE_EXTENTION", "." + extention)
                                    pParms_Folder(9) = New SqlClient.SqlParameter("@FET_IS_ACTIVE", True)
                                    pParms_Folder(10) = New SqlClient.SqlParameter("@LOGIN_USER", Session("sUsr_name"))
                                    pParms_Folder(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                                    pParms_Folder(11).Direction = ParameterDirection.ReturnValue
                                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVE_FILE_EXPLORER_DATA", pParms_Folder)
                                    Dim ReturnFlag As Integer = pParms_Folder(11).Value
                                    stTrans.Commit()
                                    If ReturnFlag = 0 Then

                                        'FileUpload_File.SaveAs(serverpath + Paret_Path.Replace("~", "") + "/" + FileName_without_Extension + "." + extention)
                                        Dim helper As StorageHelper = New StorageHelper()
                                        'Dim bytes As Byte() = System.IO.File.ReadAllBytes(pathToCheck)
                                        'Dim Stream As Stream = New MemoryStream(bytes)
                                        Dim Stream As Stream = file.InputStream
                                        Dim Path As String = ""
                                        Dim bflag As Boolean = helper.UploadBlob(Stream, FileName, ConfigurationManager.AppSettings("ContainerName"), "Gallery/" + Paret_Path.Replace("~", "") + "/", Path)
                                    End If

                                    Popup_Message = already_exists
                                    Popup_Message_Status = WarningType.Success
                                    GridBindPayHist()
                                Catch ex As Exception
                                    stTrans.Rollback()
                                    Popup_Message = "File has been not upload successfully  Please try again  "
                                    Popup_Message_Status = WarningType.Danger
                                Finally
                                    objConn.Close()
                                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                                End Try
                            End If
                        End Using
                    Next
                Else
                    Popup_Message = "File extention must be <br/> jpg or jpeg or png or gif or <br/> mp4 or m4a or m4v or mov or wmv or wma or flv or avi"
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                End If
            Else
                Popup_Message = "You must select Document to upload  "
                Popup_Message_Status = WarningType.Danger
                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)

            End If
            'If FileUpload_File.HasFile Then

            '    'Dim serverpath As String = WebConfigurationManager.AppSettings("UploadExcelFile").ToString + "enquiry\"
            '    'Dim serverpath As String = Server.MapPath("~/oasisfiles/hr_files/enquiry/")
            '    Dim filen As String()
            '    Dim filen_wEx As String()
            '    Dim FileName As String = FileUpload_File.PostedFile.FileName
            '    Dim FileName_without_Extension As String = ""
            '    Dim NameToCheck As String = ""
            '    Dim already_exists As String = ""
            '    filen = FileName.Split("\")
            '    FileName = filen(filen.Length - 1)

            '    Dim extention = GetExtension(FileName)
            '    'Or extention.ToLower = "doc" Or extention.ToLower = "docx" Or extention.ToLower = "pdf" Or extention.ToLower = "xlx" Or extention.ToLower = "xlsx" 
            '    If extention.ToLower = "jpg" Or extention.ToLower = "jpeg" Or extention.ToLower = "png" Or extention.ToLower = "gif" Or
            '        extention.ToLower = "mp4" Or extention.ToLower = "m4a" Or extention.ToLower = "m4v" Or extention.ToLower = "mov" Or
            '        extention.ToLower = "wmv" Or extention.ToLower = "wma" Or extention.ToLower = "flv" Or extention.ToLower = "avi" Then
            '        '''''''''''''''''''''''''''''''''''
            '        Dim Parent_Id As String = 0
            '        Dim Paret_Path As String = ""
            '        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            '        Dim objConn As New SqlConnection(str_conn)
            '        Using objConn
            '            '\\\\ Get Parent_Id
            '            If Request.QueryString("pfid") Is Nothing Then

            '                Dim pParms_Parent(3) As SqlClient.SqlParameter
            '                pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            '                pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            '                Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
            '            Else
            '                Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))
            '            End If
            '            If Is_That_root_Folder(Parent_Id, str_conn) Then
            '                Popup_Message = "you can not Create Files on Root Folder  "
            '                Popup_Message_Status = WarningType.Danger
            '                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            '            Else


            '                '\\\\ Get Paret_Path
            '                Dim pParms_Parent_path(3) As SqlClient.SqlParameter
            '                pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            '                pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            '                pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_ID", Parent_Id)
            '                Paret_Path = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_PARENT_Path]", pParms_Parent_path)

            '                '\\\\ Check_Duplicate_Files
            '                filen_wEx = FileName.Split(".")
            '                NameToCheck = filen_wEx(0)
            '                FileName_without_Extension = filen_wEx(0)
            '                If (If_File_Exist(NameToCheck, Parent_Id)) Then
            '                    Dim counter As Integer = 2
            '                    Dim tempfileName As String = ""
            '                    While If_File_Exist(NameToCheck, Parent_Id)
            '                        tempfileName = FileName_without_Extension & ("( " + counter.ToString() + " )")
            '                        NameToCheck = tempfileName
            '                        counter += 1
            '                    End While
            '                    FileName = tempfileName + "." + extention
            '                    already_exists = "A file with the same name already exists." & "<br />Your file was saved as " & FileName
            '                Else
            '                    already_exists = "file has been Created successfully"
            '                End If
            '                'Dim pathToCheck As String = serverpath + Paret_Path.Replace("~", "") + "/" + FileName
            '                'If (System.IO.File.Exists(pathToCheck)) Then
            '                '    'FileName_without_Extension = FileName_without_Extension + "(1)"
            '                '    Dim counter As Integer = 2
            '                '    Dim tempfileName As String = ""
            '                '    While System.IO.File.Exists(pathToCheck)
            '                '        tempfileName = ("( " + counter.ToString() + " )") & FileName
            '                '        pathToCheck = serverpath + Paret_Path.Replace("~", "") + "/" + tempfileName
            '                '        counter += 1
            '                '    End While
            '                '    FileName = tempfileName
            '                '    already_exists = "A file with the same name already exists." & "<br />Your file was saved as " & FileName
            '                'Else
            '                '    already_exists = "file has been Created successfully"
            '                'End If
            '                filen_wEx = FileName.Split(".")
            '                FileName_without_Extension = filen_wEx(0)
            '                objConn.Open()
            '                Dim stTrans As SqlTransaction = objConn.BeginTransaction
            '                Try

            '                    Dim pParms_Folder(14) As SqlClient.SqlParameter
            '                    pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", "0")
            '                    pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            '                    pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            '                    pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", Parent_Id)
            '                    pParms_Folder(4) = New SqlClient.SqlParameter("@FET_DESCR", FileName_without_Extension)
            '                    pParms_Folder(12) = New SqlClient.SqlParameter("@FET_DISPLAY_NAME", FileName_without_Extension)
            '                    pParms_Folder(5) = New SqlClient.SqlParameter("@FET_PATH", "~" + Paret_Path.Replace("~", "") + "/")
            '                    pParms_Folder(7) = New SqlClient.SqlParameter("@FET_IS_DIRECTORY", False)
            '                    pParms_Folder(8) = New SqlClient.SqlParameter("@FET_FILE_EXTENTION", "." + extention)
            '                    pParms_Folder(9) = New SqlClient.SqlParameter("@FET_IS_ACTIVE", True)
            '                    pParms_Folder(10) = New SqlClient.SqlParameter("@LOGIN_USER", Session("sUsr_name"))
            '                    pParms_Folder(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            '                    pParms_Folder(11).Direction = ParameterDirection.ReturnValue
            '                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVE_FILE_EXPLORER_DATA", pParms_Folder)
            '                    Dim ReturnFlag As Integer = pParms_Folder(11).Value
            '                    stTrans.Commit()
            '                    If ReturnFlag = 0 Then

            '                        'FileUpload_File.SaveAs(serverpath + Paret_Path.Replace("~", "") + "/" + FileName_without_Extension + "." + extention)
            '                        Dim helper As StorageHelper = New StorageHelper()
            '                        'Dim bytes As Byte() = System.IO.File.ReadAllBytes(pathToCheck)
            '                        'Dim Stream As Stream = New MemoryStream(bytes)
            '                        Dim Stream As Stream = FileUpload_File.PostedFile.InputStream
            '                        Dim Path As String = ""
            '                        Dim bflag As Boolean = helper.UploadBlob(Stream, FileName, ConfigurationManager.AppSettings("ContainerName"), "Gallery/" + Paret_Path.Replace("~", "") + "/", Path)
            '                    End If

            '                    Popup_Message = already_exists
            '                    Popup_Message_Status = WarningType.Success
            '                    GridBindPayHist()
            '                Catch ex As Exception
            '                    stTrans.Rollback()
            '                    Popup_Message = "File has been not upload successfully  Please try again  "
            '                    Popup_Message_Status = WarningType.Danger
            '                Finally
            '                    objConn.Close()
            '                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            '                End Try
            '            End If
            '        End Using
            '        ''''''''''''''''''''''''''''''''''''''

            '    Else
            '        Popup_Message = "File extention must be <br/> jpg or jpeg or png or gif or mp4 or m4a or m4v or mov or wmv or wma or flv or avi"
            '        Popup_Message_Status = WarningType.Danger
            '        usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            '    End If
            'Else
            '    Popup_Message = "You must select Document to upload  "
            '    Popup_Message_Status = WarningType.Danger
            '    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            'End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub FolderGallery_FolderGalleryView_Init(sender As Object, e As EventArgs) Handles Me.Init
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn_Save_Folder)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btn_Save_file)

    End Sub
    Function Can_Create_Folder(Parent_Id As String, str_conn As String) As Boolean
        Dim flag As Boolean = False
        Dim FET_ID As String = String.Empty
        Dim FET_PARENT_ID As String = String.Empty
        Dim pParms_Parent(4) As SqlClient.SqlParameter
        pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
        pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
        pParms_Parent(2) = New SqlClient.SqlParameter("@FET_ID", Parent_Id)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_FILE_EXPLORER_PARENT_data", pParms_Parent)
            While reader.Read
                FET_ID = Convert.ToString(reader("FET_ID"))
                FET_PARENT_ID = Convert.ToString(reader("FET_PARENT_ID"))
                If FET_PARENT_ID = 0 Then
                    flag = True
                Else
                    flag = False
                End If
            End While
        End Using
        Return flag
    End Function
    Function Is_That_root_Folder(Parent_Id As String, str_conn As String) As Boolean
        Dim flag As Boolean = False
        Dim FET_ID As String = String.Empty
        Dim FET_PARENT_ID As String = String.Empty
        Dim pParms_Parent(4) As SqlClient.SqlParameter
        pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
        pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
        pParms_Parent(2) = New SqlClient.SqlParameter("@FET_ID", Parent_Id)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_FILE_EXPLORER_PARENT_data", pParms_Parent)
            While reader.Read
                FET_ID = Convert.ToString(reader("FET_ID"))
                FET_PARENT_ID = Convert.ToString(reader("FET_PARENT_ID"))
                If FET_PARENT_ID = 0 Then
                    flag = True
                Else
                    flag = False
                End If
            End While
        End Using
        Return flag
    End Function

    Private Function GetExtension(ByVal FileName As String) As String

        Dim Extension As String

        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)

        Return Extension


    End Function
    Function If_File_Exist(ByVal FileName_without_Extension As String, ByVal Parent_Id As Integer) As Boolean
        Dim Flag As Boolean = False
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            Dim pParms_Parent_path(5) As SqlClient.SqlParameter
            pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_PARENT_ID", Parent_Id)
            pParms_Parent_path(3) = New SqlClient.SqlParameter("@FET_DESCR", FileName_without_Extension)
            Flag = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[Check_Duplicate_Files]", pParms_Parent_path)
        End Using
        Return Flag
    End Function
#Region "Not used"

    Function MyFuncqqqqq() As Integer
        Dim helper As StorageHelper = New StorageHelper()
        Dim bytes As Byte() = System.IO.File.ReadAllBytes("C:\Users\user\Desktop\Phoenix.PNG")
        Dim Stream As Stream = New MemoryStream(bytes)
        Dim Path As String = ""
        Dim bflag As Boolean = helper.UploadBlob(Stream, "Phoenix1.PNG", ConfigurationManager.AppSettings("ContainerName"), "test/", Path)
        'dowen load 
        Stream = Nothing
        Stream = helper.DownloadBlob("Phoenix.PNG", ConfigurationManager.AppSettings("ContainerName"), "test/")
        Using ms As New MemoryStream
            Stream.CopyTo(ms)
            System.IO.File.WriteAllBytes("C:\Users\user\Desktop\Phoenix1.PNG", ms.ToArray())
        End Using



        Return 0
    End Function
    Function Rename_bolb() As Integer
        Dim Parent_Id As String = 0
        Dim Paret_Path As String = ""
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            If Request.QueryString("pfid") Is Nothing Then
                'Parent_Id =

                Dim pParms_Parent(3) As SqlClient.SqlParameter
                pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                Parent_Id = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_ROOT_PARENT_ID]", pParms_Parent)
            Else
                Parent_Id = Encr_decrData.Decrypt(Request.QueryString("pfid").Replace(" ", "+"))
            End If
            Dim pParms_Parent_path(3) As SqlClient.SqlParameter
            pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_ID", Parent_Id)
            Paret_Path = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_PARENT_Path]", pParms_Parent_path)


            Try


                Dim helper As StorageHelper = New StorageHelper()

                Dim bflag As Boolean = helper.RenameBlob("test/BIMOffer.pdf", "Test1234", ConfigurationManager.AppSettings("ContainerName"), "Gallery/" + Paret_Path.Replace("~", "") + "/")
                Popup_Message = "You must select Document to upload  "
                Popup_Message_Status = WarningType.Success

            Catch ex As Exception

                Popup_Message = "You must select Document to upload  "
                Popup_Message_Status = WarningType.Danger
            Finally

                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            End Try

        End Using
        Return 0
    End Function
#End Region
    Protected Sub btn_Edit_Folder_Click(sender As Object, e As EventArgs)
        Try
            'Dim item As ListView = TryCast((TryCast(sender, Button)).NamingContainer, ListView)
            'Dim saveOrUpdate As Button = (TryCast(sender, Button))
            'Dim item As ListView = TryCast(saveOrUpdate.NamingContainer, ListView)
            Dim button As Button = (TryCast(sender, Button))
            Dim item As ListViewItem = TryCast(button.NamingContainer, ListViewItem)
            Dim txt_folder_Edit_Name As TextBox = TryCast(item.FindControl("txt_folder_Edit_Name"), TextBox)
            Dim HF_ID As HiddenField = TryCast(item.FindControl("HF_ID"), HiddenField)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Using objConn
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim pParms_Folder(14) As SqlClient.SqlParameter
                    pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", Encr_decrData.Decrypt(HF_ID.Value))
                    pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", "0")
                    pParms_Folder(4) = New SqlClient.SqlParameter("@FET_DESCR", txt_folder_Edit_Name.Text)
                    pParms_Folder(12) = New SqlClient.SqlParameter("@FET_DISPLAY_NAME", txt_folder_Edit_Name.Text)
                    pParms_Folder(5) = New SqlClient.SqlParameter("@FET_PATH", "~")
                    pParms_Folder(7) = New SqlClient.SqlParameter("@FET_IS_DIRECTORY", False)
                    pParms_Folder(8) = New SqlClient.SqlParameter("@FET_FILE_EXTENTION", ".")
                    pParms_Folder(9) = New SqlClient.SqlParameter("@FET_IS_ACTIVE", True)
                    pParms_Folder(10) = New SqlClient.SqlParameter("@LOGIN_USER", Session("sUsr_name"))
                    pParms_Folder(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms_Folder(11).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "SAVE_FILE_EXPLORER_DATA", pParms_Folder)
                    Dim ReturnFlag As Integer = pParms_Folder(11).Value
                    stTrans.Commit()

                    Popup_Message = "File has been Updated successfully"
                    Popup_Message_Status = WarningType.Success
                    GridBindPayHist()
                Catch ex As Exception
                    stTrans.Rollback()
                    Popup_Message = "File has been not Updated successfully  Please try again  "
                    Popup_Message_Status = WarningType.Danger
                    UtilityObj.Errorlog(ex.Message)
                Finally
                    objConn.Close()
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)

                End Try

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Sub RegisterPostBackControl()
        For Each row As ListViewItem In ListView_GetGalleryFolder.Items
            Dim btn_Edit_Folder As Button = TryCast(row.FindControl("btn_Edit_Folder"), Button)
            ScriptManager.GetCurrent(Me).RegisterPostBackControl(btn_Edit_Folder)
            Dim btnDeleteFolder2 As LinkButton = TryCast(row.FindControl("btnDeleteFolder2"), LinkButton)
            ScriptManager.GetCurrent(Me).RegisterPostBackControl(btnDeleteFolder2)
        Next

        For Each row As ListViewItem In ListView_GetGalleryFiles.Items
            Dim btnDeleteFile As LinkButton = TryCast(row.FindControl("btnDeleteFile2"), LinkButton)
            ScriptManager.GetCurrent(Me).RegisterPostBackControl(btnDeleteFile)
        Next
    End Sub
    Function Check_If_Admin() As Boolean
        Dim Flag As Boolean = False
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FUPA_BSU_ID", Session("sBsuid"))
            pParms(1) = New SqlClient.SqlParameter("@FUPA_USER_NAME_ID", Session("sUsr_name"))
            Flag = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[Check_If_Admin]", pParms)
        End Using
        Return Flag
    End Function
    Protected Sub btnDeleteFile2_Click(sender As Object, e As EventArgs)
        Try
            'Dim item As ListView = TryCast((TryCast(sender, Button)).NamingContainer, ListView)
            'Dim saveOrUpdate As Button = (TryCast(sender, Button))
            'Dim item As ListView = TryCast(saveOrUpdate.NamingContainer, ListView)
            Dim button As LinkButton = (TryCast(sender, LinkButton))
            Dim item As ListViewItem = TryCast(button.NamingContainer, ListViewItem)
            Dim HF_ID As HiddenField = TryCast(item.FindControl("HF_ID"), HiddenField)
            Dim HF_P_ID As HiddenField = TryCast(item.FindControl("HF_P_ID"), HiddenField)
            Dim lbl_fileName As Label = TryCast(item.FindControl("lbl_fileName"), Label)
            Dim parent_ID = Encr_decrData.Decrypt(HF_P_ID.Value)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Dim objConn As New SqlConnection(str_conn)
            Using objConn

                Dim Paret_Path As String = ""
                Dim pParms_Parent_path(3) As SqlClient.SqlParameter
                pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_ID", parent_ID)
                Paret_Path = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_PARENT_Path]", pParms_Parent_path)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim pParms_Folder(6) As SqlClient.SqlParameter
                    pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", Encr_decrData.Decrypt(HF_ID.Value))
                    pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                    pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                    pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", parent_ID)
                    pParms_Folder(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    pParms_Folder(4).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Delete_FILE_EXPLORER_DATA", pParms_Folder)
                    Dim ReturnFlag As Integer = pParms_Folder(4).Value
                    Dim helper As StorageHelper = New StorageHelper()

                    Dim bflag As Boolean = helper.DeleteBlob(lbl_fileName.Text, ConfigurationManager.AppSettings("ContainerName"), "Gallery/" + Paret_Path.Replace("~", "") + "/")
                    stTrans.Commit()

                    Popup_Message = "File has been Updated successfully"
                    Popup_Message_Status = WarningType.Success
                    GridBindPayHist()
                Catch ex As Exception
                    stTrans.Rollback()
                    Popup_Message = "File has been not Updated successfully  Please try again  "
                    Popup_Message_Status = WarningType.Danger
                    UtilityObj.Errorlog(ex.Message)
                Finally
                    objConn.Close()
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)

                End Try

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnDeleteFolder2_Click(sender As Object, e As EventArgs)
        Try
            Dim button As LinkButton = (TryCast(sender, LinkButton))
            Dim item As ListViewItem = TryCast(button.NamingContainer, ListViewItem)
            Dim HF_ID As HiddenField = TryCast(item.FindControl("HF_ID"), HiddenField)
            Dim HF_P_ID As HiddenField = TryCast(item.FindControl("HF_P_ID"), HiddenField)
            Dim FET_ID = Encr_decrData.Decrypt(HF_ID.Value)
            Dim Parent_ID = Encr_decrData.Decrypt(HF_P_ID.Value)
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
            Delete_Foder(FET_ID, Parent_ID, str_conn)
            Popup_Message = "File has been Deleted successfully"
            Popup_Message_Status = WarningType.Success
            GridBindPayHist()
        Catch ex As Exception

            Popup_Message = "File has been not Deleted successfully  Please try again  "
            Popup_Message_Status = WarningType.Danger
            UtilityObj.Errorlog(ex.Message)
        Finally

            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)

        End Try

    End Sub
    Function Delete_Foder(Current_ID As String, PARENT_ID As String, str_conn As String) As String
        Dim result As String = ""
        Try

            Dim FET_DESCR As String = String.Empty
            Dim FET_ID As String = String.Empty
            Dim FET_PARENT_ID As String = String.Empty
            Dim FileName As String = String.Empty
            Dim FET_IS_DIRECTORY As Boolean = False
            Dim pParms_Parent(4) As SqlClient.SqlParameter
            pParms_Parent(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent(2) = New SqlClient.SqlParameter("@FET_PARENT_ID", Current_ID)
            '' select all data by id 
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[dbo].[GET_FILE_PERMISSION_DATA_BY_PARENT_ID]", pParms_Parent)
                If reader.HasRows Then
                    While reader.Read
                        If FET_PARENT_ID <> "0" Then
                            FET_ID = Convert.ToString(reader("FET_ID"))
                            FET_PARENT_ID = Convert.ToString(reader("FET_PARENT_ID"))
                            FET_IS_DIRECTORY = Convert.ToBoolean(reader("FET_IS_DIRECTORY"))
                            FileName = Convert.ToString(reader("FET_DISPLAY_NAME")) & Convert.ToString(reader("FET_FILE_EXTENTION"))
                            If FET_IS_DIRECTORY = True Then
                                Delete_Foder(FET_ID, FET_PARENT_ID, str_conn)
                                Delte_folder_From_Data(PARENT_ID, FET_ID)
                            Else
                                Delte_File_From_Data_And_Azure(FET_PARENT_ID, FET_ID, FileName)
                            End If


                        End If

                    End While
                End If
                '' delete that row 
            End Using
            Delte_folder_From_Data(PARENT_ID, Current_ID)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        Return result
    End Function

    Private Sub Delte_File_From_Data_And_Azure(parent_ID As String, FET_ID As String, fileName As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn

            Dim Paret_Path As String = ""
            Dim pParms_Parent_path(3) As SqlClient.SqlParameter
            pParms_Parent_path(0) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
            pParms_Parent_path(1) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
            pParms_Parent_path(2) = New SqlClient.SqlParameter("@FET_ID", parent_ID)
            Paret_Path = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[dbo].[GET_FILE_EXPLORER_PARENT_Path]", pParms_Parent_path)
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim pParms_Folder(6) As SqlClient.SqlParameter
                pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", FET_ID)
                pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", parent_ID)
                pParms_Folder(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms_Folder(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Delete_FILE_EXPLORER_DATA", pParms_Folder)
                Dim ReturnFlag As Integer = pParms_Folder(4).Value
                Dim helper As StorageHelper = New StorageHelper()

                Dim bflag As Boolean = helper.DeleteBlob(fileName, ConfigurationManager.AppSettings("ContainerName"), "Gallery/" + Paret_Path.Replace("~", "") + "/")
                stTrans.Commit()

            Catch ex As Exception
                stTrans.Rollback()
                UtilityObj.Errorlog(ex.Message)
            Finally
                objConn.Close()

            End Try

        End Using
    End Sub
    Private Sub Delte_folder_From_Data(parent_ID As String, FET_ID As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PHOENIX_STAGING_DB").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim pParms_Folder(6) As SqlClient.SqlParameter
                pParms_Folder(0) = New SqlClient.SqlParameter("@FET_ID", FET_ID)
                pParms_Folder(1) = New SqlClient.SqlParameter("@FET_BSU_ID", Session("sBsuid"))
                pParms_Folder(2) = New SqlClient.SqlParameter("@FET_FGM_ID", "1")
                pParms_Folder(3) = New SqlClient.SqlParameter("@FET_PARENT_ID", parent_ID)
                pParms_Folder(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms_Folder(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Delete_FILE_EXPLORER_DATA", pParms_Folder)
                Dim ReturnFlag As Integer = pParms_Folder(4).Value
                stTrans.Commit()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                stTrans.Rollback()

            Finally
                objConn.Close()

            End Try

        End Using
    End Sub

End Class
